<?php

class APIStatus {

    const STATUS_SUCCESS               = 'SUCCESS';
    const STATUS_ERROR                 = 'ERROR';
    const ERROR_INVALID_PARAMETERS     = 'Invalid or Incomplete Parameters';
    const ERROR_INVALID_REQUEST_METHOD = 'Invalid Request Method';
    const ERROR_UNAUTHORIZED           = 'Unauthorized';
    const ERROR_IN_DATABASE            = 'Database Error';
    const ERROR_API_ERROR              = 'API Error';

}

class APIException extends Exception {

    public $cause   = null;
    public $message = null;

    const ERROR_INVALID_PARAMETERS     = 'Invalid or Incomplete Parameters';
    const ERROR_INVALID_REQUEST_METHOD = 'Invalid Request Method';
    const ERROR_UNAUTHORIZED           = 'Unauthorized';
    const ERROR_IN_DATABASE            = 'Database Error';

    public function __construct($cause, $message) {
        parent::__construct($message, 0, null);

        $this->cause   = $cause;
        $this->message = $message;
    }

}

class APISearchFilter {

    public $searchRecordsFrom         = 0;  //  from
    public $searchRecordsCount        = 1000;  //  number_of_records
    public $orderByField              = 'DateCreated DESC';
    public $condition                 = null;
    public $dateCreatedComparison     = null;
    public $dateUpdatedComparison     = null;
    private $validComparisonOperators = ['>', '>=', '<', '<=', '='];

    public function validateFields() {

        if (!is_numeric($this->searchRecordsFrom)) {
            throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'The from (range) must be numeric');
        }

        if (!is_numeric($this->searchRecordsCount)) {
            throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'The number_of_records (range) must be numeric');
        }

        if ($this->dateCreatedComparison) {
            if (!is_array($this->dateCreatedComparison)) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'Please provide a valid date_created_comparison field.');
            }

            if (empty($this->dateCreatedComparison['condition'])) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'date_created_comparison must have a condition.');
            } else if (!in_array($this->dateCreatedComparison['condition'], $this->validComparisonOperators)) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'date_created_comparison must have a valid condition, please use >, >=, <, <=, or =.');
            }

            if (!DateTime::createFromFormat($this->databaseDateTimeFormat, $this->dateCreatedComparison['compared_to_date'])) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, $this->dateCreatedComparison['compared_to_date'] . ' The compared_to_date for date created comparison must be a valid date. Valid date format is YYYY-MM-dd HH:mm:ss, ex. 2013-06-13 12:00:00');
            }
        }

        if ($this->dateUpdatedComparison) {
            if (!is_array($this->dateUpdatedComparison)) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'Please provide a valid date_updated_comparison field.');
            }

            if (empty($this->dateUpdatedComparison['condition'])) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'date_updated_comparison must have a condition.');
            } else if (!in_array($this->dateUpdatedComparison['condition'], $this->validComparisonOperators)) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'date_updated_comparison must have a valid condition, please use >, >=, <, <=, or =.');
            }
        }
    }

    public function createDateComparisonCondition($comparison, $fieldName) {
        return $fieldName . $comparison['condition'] . "'" . $comparison['compared_to_date'] . "'";
    }

}

class APIFormSearchFilter extends APISearchFilter {

    public $formIdList = array();

}

class APIRequestSearchFilter extends APISearchFilter {

    public $formIdList = array();
    public $fields     = array();

}

class APISearchParameters {

    //  required parameters
    public $formId;
    public $userId;
    //  optional parameters
    public $userLevelId;
    public $fields                    = [];
    public $searchRecordsFrom         = 0;  //  from
    public $searchRecordsCount        = 1000;  //  number_of_records
    public $orderByField              = 'DateCreated DESC';
    public $condition                 = null;
    //  advanced parameters
    public $extraConditionFields      = array();
    public $includeRequestorInfo      = false;
    public $includeProcessorInfo      = false;
    public $dateCreatedComparison     = null;
    public $dateUpdatedComparison     = null;
    public $starred                   = 'no_filter';
    private $validComparisonOperators = ['>', '>=', '<', '<=', '='];
//    private $validDateFormat = 'm/d/Y';
//    private $validDateTimeFormat = 'm/d/Y H:i:s';
    private $databaseDateTimeFormat   = 'Y-m-d H:i:s';
    public $searchQueryUsed           = "";

    public function __construct($formId, $userId, $userLevelId = 3) {
        $this->formId      = $formId;
        $this->userId      = $userId;
        $this->userLevelId = $userLevelId;
    }

    public function validateFields() {

        if (!is_numeric($this->searchRecordsFrom)) {
            throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'The from (range) must be numeric');
        }

        if (!is_numeric($this->searchRecordsCount)) {
            throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'The number_of_records (range) must be numeric');
        }

        if ($this->dateCreatedComparison) {
            if (!is_array($this->dateCreatedComparison)) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'Please provide a valid date_created_comparison field.');
            }

            if (empty($this->dateCreatedComparison['condition'])) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'date_created_comparison must have a condition.');
            } else if (!in_array($this->dateCreatedComparison['condition'], $this->validComparisonOperators)) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'date_created_comparison must have a valid condition, please use >, >=, <, <=, or =.');
            }

            if (!DateTime::createFromFormat($this->databaseDateTimeFormat, $this->dateCreatedComparison['compared_to_date'])) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, $this->dateCreatedComparison['compared_to_date'] . ' The compared_to_date for date created comparison must be a valid date. Valid date format is YYYY-MM-dd HH:mm:ss, ex. 2013-06-13 12:00:00');
            }
        }

        if ($this->dateUpdatedComparison) {
            if (!is_array($this->dateUpdatedComparison)) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'Please provide a valid date_updated_comparison field.');
            }

            if (empty($this->dateUpdatedComparison['condition'])) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'date_updated_comparison must have a condition.');
            } else if (!in_array($this->dateUpdatedComparison['condition'], $this->validComparisonOperators)) {
                throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'date_updated_comparison must have a valid condition, please use >, >=, <, <=, or =.');
            }
        }
    }

    public function getSearchLibraryCompatibleConditions() {

        $dateComparisonConditions = '';
        if (!empty($this->dateCreatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $this->createDateComparisonCondition($this->dateCreatedComparison, 'DateCreated');
        }

        if (!empty($this->dateUpdatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $this->createDateComparisonCondition($this->dateUpdatedComparison, 'DateUpdated');
        }

        return array(
            "condition" => $dateComparisonConditions,
            "column-sort" => $this->orderByField
        );
    }

    public function createDateComparisonCondition($comparison, $fieldName) {
        return $fieldName . $comparison['condition'] . "'" . $comparison['compared_to_date'] . "'";
    }

}

require_once API_LIBRARY_PATH . '/APIDatabase.php';
require_once API_LIBRARY_PATH . '/APIUtilities.php';

require_once API_LIBRARY_PATH . '/APIFilter.php';

require_once API_LIBRARY_PATH . '/APIManager.php';

require_once API_LIBRARY_PATH . '/Managers/APIRequestManager.php';
require_once API_LIBRARY_PATH . '/Managers/APIUserManager.php';
require_once API_LIBRARY_PATH . '/Managers/APICommentManager.php';
require_once API_LIBRARY_PATH . '/Managers/APIFormsManager.php';

require_once API_LIBRARY_PATH . '/Utilities/APIRequestSearcher.php';
