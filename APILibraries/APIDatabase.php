<?php

/**
 * Description of APIDatabase
 *
 * @author ervinne
 */
class APIDatabase {

    protected $allowed_mysql_functions = array(
        "NOW()", "CURDATE()", "CURTIME()"
    );
    protected $host;
    protected $username;
    protected $password;
    protected $databaseName;

    const QUERY_FETCH_ALL = "array";
    const QUERY_FETCH_ROW = "row";
    const QUERY_INSERT    = "insert";
    const QUERY_UPDATE    = "update";
    const QUERY_ROW_COUNT = "numrows";

    /** @var PDO */
    protected $connection;

    public function __construct($host = null, $username = null, $password = null, $db = null) {
        if (isset($host)) {
            $this->host = $host;
        } else {
            $this->host = DB_HOSTNAME;
        }
        if (isset($username)) {
            $this->username = $username;
        } else {
            $this->username = DB_USERNAME;
        }
        if (isset($password)) {
            $this->password = $password;
        } else {
            $this->password = DB_PASSWORD;
        }
        if (isset($db)) {
            $this->databaseName = $db;
        } else {
            $this->databaseName = DB_NAME;
        }
    }

    public function connect() {
        $con              = mysql_connect($this->host, $this->username, $this->password) or die("Cannot connect to Host");
        mysql_select_db($this->databaseName) or die("Cannot connect to DB Name");
        $this->connection = $con;
    }

    public function get_errors() {
        return [
            'error_number'  => mysql_errno($this->connection),
            'error_message' => mysql_error($this->connection)
        ];
    }

    public function disconnect() {
        mysql_close($this->connection);
    }

    public function query($sql, $fetch_type = APIDatabase::QUERY_FETCH_ALL) {

        if (!$this->connection) {
            throw new Exception('No connection set up');
        }

        $result = mysql_query($sql);
        $data   = array();

        if ($result) {
            if ($fetch_type === APIDatabase::QUERY_FETCH_ALL) {
                while ($row = mysql_fetch_assoc($result)) {
                    $data[] = $row;
                }
            } else if ($fetch_type === APIDatabase::QUERY_FETCH_ROW) {
                $data = mysql_fetch_assoc($result);
            } else if ($fetch_type === APIDatabase::QUERY_UPDATE) {
                $data = mysql_affected_rows($this->connection);
            } else if ($fetch_type === APIDatabase::QUERY_INSERT) {
                $data = mysql_insert_id($this->connection);
            } else if ($fetch_type === APIDatabase::QUERY_ROW_COUNT) {
                $data = mysql_num_rows($result);
            } else {
                return false;
            }
            return $data;
        } else if (mysql_errno()) {
            throw new APIException(
            APIStatus::ERROR_IN_DATABASE, mysql_error() . " Query: " . $sql
            );
        }
        return null;
    }

    public function update($table, $fields = array(), $conditions = array()) {
        $field_string = "";
        $cond_string  = "";
        foreach ($fields as $field => $value) {
            $value = !in_array($value, $this->allowed_mysql_functions) && !is_numeric($value) ?
                    $this->escape($value) : $value;
            $field_string .= $field . "=" . $value . ",";
        }
        foreach ($conditions as $condition => $value) {
            $value = is_numeric($value) ? $value : $this->escape($value);
            $cond_string .= $condition . "=" . $value . " AND ";
        }
        $field_string = substr($field_string, 0, strlen($field_string) - 1);
        $cond_string  = substr($cond_string, 0, strlen($cond_string) - 4);
        $query        = $this->query("UPDATE {$table} SET {$field_string} WHERE {$cond_string}", "update");

//        echo "UPDATE {$table} SET {$field_string} WHERE {$cond_string}";
//        exit();

        return $query;
    }

    public function insert($table, $fields = array()) {
        $field_string = "";
        foreach ($fields as $field => $value) {
            $value = !in_array($value, $this->allowed_mysql_functions) && !is_numeric($value) ?
                    $this->escape($value) : $value;
            $field_string .= "`{$field}` = {$value}, ";
        }

        // trim trailing ", "
        $field_string = substr($field_string, 0, strlen($field_string) - 2);
        $query        = $this->query("INSERT INTO {$table} SET {$field_string}", "insert");

//        echo "INSERT INTO {$table} SET {$field_string}";
//        exit;

        return $query;
    }

    public function delete($table, $conditions = array()) {
        $cond_string = "";
        foreach ($conditions as $condition => $value) {
            $value = is_numeric($value) ? $value : $this->escape($value);
            $cond_string .= $condition . " = " . $value . " AND ";
        }
        $cond_string = substr($cond_string, 0, strlen($cond_string) - 4);
        $query       = $this->query("DELETE FROM {$table} WHERE {$cond_string}", "update");

//        echo "DELETE FROM {$table} WHERE {$cond_string}", "update";
//        exit();

        return $query;
    }

    public function escape($clause) {
//        return $this->connection->escape($clause);
        return "'" . addslashes($clause) . "'";
    }

    public function beginTransaction() {
        mysql_query("BEGIN");
    }

    public function commit() {
        mysql_query("COMMIT");
    }

    public function rollback() {
        mysql_query("ROLLBACK");
    }

}
