<?php

/**
 * Description of APIFilterInterpreter
 *
 * @author ervinne
 */
class APIFilter {

    protected $equalityAndRelationalOperators = [
        '=',
        '>',
        '<',
        '>=',
        '<=',
        '!='
    ];
    protected $conditionalOperators = [
        'AND',
        'OR'
    ];
    protected $supportedFields = [
        'DateCreated',
        'DateUpdated',
    ];
    protected $rawFilter;
    protected $interpretedConditions = [];

    public function __construct($filter) {
        $this->rawFilter = $filter;
    }

    public function interpretAndValidate() {
        $tokenizedFilter = token_get_all($this->rawFilter);

        foreach ($tokenizedFilter as $index => $token) {
            if (is_string($token)) {
                echo $index . ' ' . $token;
            } else {
                // token array
                list($id, $content) = $token;

                echo json_encode($token);

                echo '---' . $id . '---' . $content . '---';
            }
        }
    }

    public function validateDisallowedFields($disallowedFields) {
        
    }

}
