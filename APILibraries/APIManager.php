<?php

/**
 * Description of APIManager
 *
 * @author ervinne
 */
abstract class APIManager {

    /** @var APIDatabase */
    protected $database = null;

    /** @var APIUtilities */
    protected $utilities = null;

    /**
     * 
     * @param APIDatabase $database dependency injection for APIDatabase instance
     * @param APIUtilities $utilities dependency injection for APIUtilities instance
     */
    public function __construct(APIDatabase $database, APIUtilities $utilities) {
        $this->database = $database;
        $this->utilities = $utilities;
    }

    public function refineAPIMySQLError(APIException $APIMySQLException) {
        if (strpos($APIMySQLException->message, 'Unknown column') > -1) {
            throw new APIException(
            APIStatus::ERROR_INVALID_PARAMETERS, 'Unknown field ' . substr($APIMySQLException->message, 15)
            );
        } else {
            //  rethrow as generic mysql error
            throw $APIMySQLException;
        }
    }

}
