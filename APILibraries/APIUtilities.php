<?php

/**
 * Description of APIUtilities
 *
 * @author ervinne
 */
class APIUtilities {

    //  common errors
    const NO_LOGIN_REQUIRED      = true;
    const ALLOW_SELECT_ALL       = true;
    const ACCESS_CONTROL_MAX_AGE = 86400; // cache for 1 day

    public function validate($noAuthorization = false) {
        $response = [
            'status'        => null,
            'error'         => null,
            'error_message' => null,
            'results'       => null
        ];

        $auth        = new Auth();
        $currentUser = $auth->getAuth('current_user');

        //  check the method of request, only POST is allowed
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            $response['status']        = APIStatus::STATUS_ERROR;
            $response['error']         = APIStatus::ERROR_INVALID_REQUEST_METHOD;
            $response['error_message'] = 'Please use POST method for requesting and submitting to formalistics.';
        }

        //  check if the user is logged in
        if (!$currentUser && !$noAuthorization) {
            $response['status']        = APIStatus::STATUS_ERROR;
            $response['error']         = APIStatus::ERROR_UNAUTHORIZED;
            $response['error_message'] = 'You must be logged in to access this service';
        }

        return $response;
    }

    public function generateSelectClause($fields, $exemptedFields = [], $allowSelectAll = false, $alias = false) {
        $cleanFields = $this->cleanupArray($fields, $exemptedFields);

        if (count($cleanFields) > 0) {

            if ($alias) {
                for ($i = 0; $i < count($cleanFields); $i ++) {
                    $cleanFields[$i] = $alias . '.' . $cleanFields[$i];
                }
            }

            return implode(',', $cleanFields);
        } else {
            if ($exemptedFields && count($exemptedFields) > 0) {
                throw new APIException(
                APIStatus::ERROR_INVALID_PARAMETERS, 'Cannot generate a select clause because all fields requested by the user are non selectable fields. Ex. Password.'
                );
            } else if ($allowSelectAll) {
                return $alias ? $alias . '.*' : '*';
            } else {
                throw new APIException(
                APIStatus::ERROR_INVALID_PARAMETERS, 'Cannot generate a select clause because no fields are selected.'
                );
            }
        }
    }

    public function addAPIHeaders() {

        // Allow from any origin
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: ' . APIUtilities::ACCESS_CONTROL_MAX_AGE);
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            }

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }

            exit(0);
        }

        header('Content-Type: application/json');
    }

    /**
     * Removes empty and null items. Also filters items if they are specified in the $allowedArrayItems
     * or removes them if they are included in the $excludedArrayItems
     * @param type $arrayItems
     * @param type $allowedArrayItems
     * @param type $excludedArrayItems
     * @return array
     */
    public function cleanupArray($arrayItems, $excludedArrayItems = []) {
        $partiallyCleanArray = [];
        $cleanArray          = [];

        //  cleanup array, remove blank, and remove excluded items
        foreach ($arrayItems as $field) {
            if (!empty($field)) {
                if (empty($excludedArrayItems) || !in_array($field, $excludedArrayItems)) {
                    array_push($cleanArray, $field);
                }
            }
        }
        //  remove exempted items
        foreach ($partiallyCleanArray as $field) {
            if (empty($excludedArrayItems) || !in_array($field, $excludedArrayItems)) {
                array_push($cleanArray, $field);
            }
        }
        return $cleanArray;
    }

}
