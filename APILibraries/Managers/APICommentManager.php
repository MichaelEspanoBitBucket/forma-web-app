<?php

////  Uncomment the following when creating unit tests
//define('API_LIBRARY_PATH' , dirname(__FILE__) . '/../../APILibraries/');
//include_once realpath(dirname(__FILE__) . '/../API.php');

/**
 * Description of APICommentManager
 *
 * @author ervinne
 */
class APICommentManager extends APIManager {

    public function __construct(APIDatabase $database, APIUtilities $utilities) {
        parent::__construct($database, $utilities);
    }

    public function createComment() {
        error_reporting(E_ALL);

        error_reporting(0);
    }

    public function getAllDeletedCommentsSinceDate($lastUpdateDate) {

        $whereClause = "WHERE c.is_active = 0 "
                . "AND c.date_deactivated >= STR_TO_DATE('{$lastUpdateDate}', '%Y-%m-%d %H:%i:%s')";

        $query = "SELECT u.id AS author_id, 
						 u.email AS author_email,
                         u.display_name AS author_display_name, 
                         u.first_name AS author_first_name, 
                         u.last_name AS author_last_name, 
                         NULL AS author_image_url,
                         c.id as id,
                         c.post_id as request_id,
                         c.fID as form_id,
                         c.comment as text, 
                         c.date_posted as date_created 
                  FROM tbcomment c
                  LEFT JOIN tbuser u on c.postedBy=u.id
                  {$whereClause} ORDER BY c.id DESC";

        $comments = $this->database->query($query, "array");
        return $comments;
    }

    public function getMultipleRequestComments($formId, $requestIdList = array(), $fromDate = null, $toDate = null, $rangeFrom = 0, $rangeTo = 0) {

        if (!empty($requestIdList) && isset($formId)) {

            $postIdClause = join(",", $requestIdList);

            $whereClause = "WHERE (c.post_id IN ({$postIdClause}) AND c.fID={$this->database->escape($formId)}) ";
            if (isset($fromDate)) {
                $whereClause .= "AND c.date_posted > '{$fromDate}' ";
            }

            if (isset($toDate)) {
                $whereClause .= "AND c.date_posted <= '{$toDate}' ";
            }

            $whereClause .= "AND c.is_active = 1";

            if ($rangeFrom > 0 || $rangeTo > 0) {
                $limitClause = "LIMIT " . $rangeFrom . ", " . $rangeTo;
            } else {
                $limitClause = "";
            }

            $query = "SELECT u.id AS author_id, 
							u.email AS author_email,
                            u.display_name AS author_display_name, 
                            u.first_name AS author_first_name, 
                            u.last_name AS author_last_name, 
							u.extension AS author_image_extension,
                            NULL AS author_image_url,
                            c.id as id,
                            c.post_id as request_id,
                            c.fID as form_id,
                            c.comment as text, 
                            c.date_posted as date_created 
                        FROM tbcomment c
                        LEFT JOIN tbuser u on c.postedBy=u.id
                        {$whereClause} ORDER BY c.id DESC {$limitClause}";

            //echo $query;
            $comments = $this->database->query($query, "array");
            return $comments;
        } else {
            throw new APIException(APIException::ERROR_INVALID_PARAMETERS, 'Request ID and form ID must be provided');
        }
    }

    public function getRequestComments($requestId, $formId, $formDate = null, $toDate = null) {

        if (isset($requestId) && isset($formId)) {

            $whereClause = "WHERE (c.post_id={$this->database->escape($requestId)} AND c.fID={$this->database->escape($formId)}) ";
            if (isset($formDate)) {
                $whereClause .= "AND c.date_posted > '{$formDate}'";
            }

            if (isset($toDate)) {
                $whereClause .= "AND c.date_posted <= '{$toDate}'";
            }

            $query = "SELECT u.id AS author_id, 
							u.email AS author_email,
                            u.display_name AS author_display_name, 
                            u.first_name AS author_first_name, 
                            u.last_name AS author_last_name, 
                            NULL AS author_image_url,
                            c.comment as text, 
                            c.date_posted as date_created 
                        FROM tbcomment c
                        LEFT JOIN tbuser u on c.postedBy=u.id
                        {$whereClause} ORDER BY c.id DESC";
            //echo $query;
            $comments = $this->database->query($query, "array");
            return $comments;
        } else {
            throw new APIException(APIException::ERROR_INVALID_PARAMETERS, 'Request ID and form ID must be provided');
        }
    }

    public function getRequestCommentCount($requestId, $formId, $formDate = null, $toDate = null) {

        if (isset($requestId) && isset($formId)) {

            $whereClause = "WHERE (post_id={$this->database->escape($requestId)} "
                    . "AND fID={$this->database->escape($formId)})";
            if (isset($formDate)) {
                $whereClause .= " AND date_posted > '{$formDate}'";
            }

            if (isset($toDate)) {
                $whereClause .= " AND date_posted <= '{$toDate}'";
            }

            $commentCount = $this->database->query(
                    "SELECT COUNT(id) FROM tbcomment {$whereClause} ORDER BY id DESC", "row"
            );

            return $commentCount['COUNT(id)'];
        } else {
            throw new APIException(APIException::ERROR_INVALID_PARAMETERS, 'Request ID and form ID must be provided');
        }
    }

}
