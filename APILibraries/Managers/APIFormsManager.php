<?php

/**
 * Description of APIFormsManager
 *
 * @author Ervinne
 */
class APIFormsManager extends APIManager {

    /**
     * 
     * @param APIFormSearchFilter $searchFilter
     */
    public function searchForms($searchFilter, $currentUser) {

        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $dateComparisonConditions = '';
        if (!empty($searchFilter->dateCreatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchFilter->createDateComparisonCondition($searchFilter->dateCreatedComparison, 'date_created');
        }

        if (!empty($searchFilter->dateUpdatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchFilter->createDateComparisonCondition($searchFilter->dateUpdatedComparison, 'date_updated');
        }

        $getFormsQuery = "SELECT 
                    ws.date_updated as last_modified,
                    ws.Company_ID as company_id,
                    ws.ID as form_id,
                    ws.form_name as form_name,
                    ws.MobileJsonData as mobile_fields,
                    wf.id as workflow_id,
                    ws.form_table_name as form_table_name,
                    wfo.buttonStatus as raw_on_create_actions,
                    wfo.fieldEnabled as on_create_fields_enabled,
                    wfo.fieldRequired as on_create_fields_required,
                    wfo.fieldHiddenValue as on_create_fields_hidden,
                    ws.form_buttons as buttons
                FROM
                    tb_workspace as ws
                        LEFT JOIN
                    tbworkflow wf ON ws.ID = wf.form_id
                        LEFT JOIN
                    tbworkflow_objects wfo ON wfo.workflow_id = wf.id
                WHERE
                    ws.form_name <> '' and ws.Is_Active = 1
                        AND ws.Company_ID = {$currentUser['company_id']}
                        AND wf.is_active = 1
                        AND wfo.type_rel = 1
                        AND ws.MobileJsonData <> '' "
                . $dateComparisonConditions;

        $forms = $this->database->query($getFormsQuery);

        //  format the on create actions of each forms
        for ($i = 0; $i < count($forms); $i ++) {

            $raw_actions       = json_decode($forms[$i]['raw_on_create_actions']);
            $processed_actions = array();
            foreach ($raw_actions as $key => $value) {
                $processed_action = array("label" => $raw_actions->{$key}->{"button_name"},
                    "action" => $raw_actions->{$key}->{"child_id"});
                array_push($processed_actions, $processed_action);
            }
            $forms[$i]['on_create_actions'] = $processed_actions;
        }

        return $forms;
    }

    public function searchFormsWithWorkflow($searchFilter, $currentUser) {
        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $dateComparisonConditions = '';
        if (!empty($searchFilter->dateCreatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchFilter->createDateComparisonCondition($searchFilter->dateCreatedComparison, 'date_created');
        }

        if (!empty($searchFilter->dateUpdatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchFilter->createDateComparisonCondition($searchFilter->dateUpdatedComparison, 'date_updated');
        }

        $search           = new Search();
        $categorizedForms = $search->getModules(
                " AND tbw.MobileJsonData <> '' " . $dateComparisonConditions, 1, array(
            "tbw.date_updated as last_modified",
            "tbw.MobileJsonData as mobile_fields",
            "tbwf.id as workflow_id",
            "tbw.form_table_name as form_table_name",
            "tbw.form_buttons as buttons"
                )
        );

        $formIdList = array();
        $forms      = array();
        foreach ($categorizedForms AS $category => $categoryForms) {
            for ($i = 0; $i < count($categoryForms); $i ++) {
                array_push($formIdList, $categoryForms[$i]["form_id"]);
                array_push($forms, $categoryForms[$i]);
            }
        }

        $this->database->connect();
        $workflowObjects = $this->getFormsWorkflowObjects($formIdList);
        $this->database->disconnect();

        for ($i = 0; $i < count($workflowObjects); $i++) {
            $raw_actions       = json_decode($workflowObjects[$i]['raw_on_create_actions']);
            $processed_actions = array();
            foreach ($raw_actions as $key => $value) {
                $processed_action = array("label" => $raw_actions->{$key}->{"button_name"},
                    "node_id" => $raw_actions->{$key}->{"child_id"});
                array_push($processed_actions, $processed_action);
            }
            $workflowObjects[$i]['actions'] = $processed_actions;
        }

        return array(
            "forms" => $forms,
            "workflow_objects" => $workflowObjects
        );
    }

    public function searchFormsWithWorkflowOld($searchFilter, $currentUser) {
        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $dateComparisonConditions = '';
        if (!empty($searchFilter->dateCreatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchFilter->createDateComparisonCondition($searchFilter->dateCreatedComparison, 'date_created');
        }

        if (!empty($searchFilter->dateUpdatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchFilter->createDateComparisonCondition($searchFilter->dateUpdatedComparison, 'date_updated');
        }

        $getFormsQuery = "SELECT 
                    ws.date_updated as last_modified,
                    ws.Company_ID as company_id,
                    ws.ID as form_id,
                    ws.form_name as form_name,
                    ws.MobileJsonData as mobile_fields,
                    wf.id as workflow_id,
                    ws.form_table_name as form_table_name,
                    wfo.fieldHiddenValue as on_create_fields_hidden,
                    ws.form_buttons as buttons
                FROM
                    tb_workspace as ws
                        LEFT JOIN
                    tbworkflow wf ON ws.ID = wf.form_id
                        LEFT JOIN
                    tbworkflow_objects wfo ON wfo.workflow_id = wf.id
                WHERE
                    ws.form_name <> '' and ws.Is_Active = 1
                        AND ws.Company_ID = {$currentUser['company_id']}
                        AND wf.is_active = 1
                        AND wfo.type_rel = 1
                        AND ws.MobileJsonData <> '' "
                . $dateComparisonConditions;

        $forms = $this->database->query($getFormsQuery);

        $formIdList = array();
        for ($i = 0; $i < count($forms); $i ++) {
            array_push($formIdList, $forms[$i]["form_id"]);
        }

        $workflowObjects = $this->getFormsWorkflowObjects($formIdList);

        for ($i = 0; $i < count($workflowObjects); $i++) {
            $raw_actions       = json_decode($workflowObjects[$i]['raw_on_create_actions']);
            $processed_actions = array();
            foreach ($raw_actions as $key => $value) {
                $processed_action = array("label" => $raw_actions->{$key}->{"button_name"},
                    "node_id" => $raw_actions->{$key}->{"child_id"});
                array_push($processed_actions, $processed_action);
            }
            $workflowObjects[$i]['actions'] = $processed_actions;
        }

        return array(
            "forms" => $forms,
            "workflow_objects" => $workflowObjects
        );
    }

    private function getFormsWorkflowObjects($formIdList = array()) {

        $serializedFormIdList = join(",", $formIdList);

        $getWorkflowObjectsQuery = "SELECT 
                    wo.id AS workflow_object_id,
                    wo.type_rel AS workflow_object_type,
                    w.id AS workflow_id,
                    w.form_id AS workflow_form_id,
                    object_id AS node_id,
                    processorType AS processor_type,
                    processor,
                    status,
                    fieldEnabled AS fields_enabled,
                    fieldRequired AS fields_required,
                    fieldHiddenValue AS fields_hidden,
                    buttonStatus AS raw_on_create_actions
                FROM
                    tbworkflow_objects wo
                        LEFT JOIN
                    tbworkflow w ON w.id = wo.workflow_id
                WHERE
                    w.is_active = 1 
                    AND w.is_delete = 0
                    AND w.form_id IN ({$serializedFormIdList});";

        return $this->database->query($getWorkflowObjectsQuery);
    }

}
