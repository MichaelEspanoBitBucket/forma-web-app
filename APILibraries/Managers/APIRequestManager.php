<?php

/**
 * Description of APIRequestManager
 *
 * @author ervinne
 */
class APIRequestManager extends APIManager {
    /*
     * Type_Rel:
     *  1 = Start Node
     *  2 = Process Node
     *  3 = Condition Node
     *  4 = End Node
     */

    protected $reservedFields = array("TrackNo", "Requestor", "Status", "Processor", "LastAction",
        "DateCreated", "DateUpdated", "CreatedBy", "UpdatedBy", "Unread", "Node_ID", "Workflow_ID",
        "fieldEnabled", "fieldRequired", "fieldHiddenValues", "imported");
    protected $selectableUserColumns = array("company_id", "email", "display_name", "first_name", "last_name", "contact_number", "company_id", "user_level_id", "department_id", "is_active");

    const NEW_REQUEST = 0;
    const EXISTING_REQUEST = 1;
    const FETCH_ALL_FIELDS = 0;

    public function __construct(APIDatabase $database, APIUtilities $utilities) {
        parent::__construct($database, $utilities);
    }

    public function markRequestAsStarred($currentUser, $formId, $requestId) {

        $tableName = $this->getFormTableName($formId);
        $userId = $currentUser['id'];

        $insertId = $this->database->insert('tbstarred', [
            'tablename' => $tableName,
            'type' => 0,
            'data_id' => $requestId,
            'user_id' => $userId,
            'is_active' => 1
        ]);

        if ($insertId) {
            return [
                'insert_id' => $insertId
            ];
        } else {
            throw new APIException(APIException::ERROR_IN_DATABASE, 'Failed to mark request as starred.');
        }
    }

    public function unmarkRequestAsStarred($formId, $requestId) {

        $tableName = $this->getFormTableName($formId);

        $unmarkCount = $this->database->delete('tbstarred', [
            'tablename' => $tableName,
            'data_id' => $requestId,
        ]);

        if ($unmarkCount > 0) {
            return [
                'status' => 'SUCCESS',
                'message' => 'Successfully unmarked request as starred',
            ];
        } else {
            return [
                'status' => 'FAILED',
                'message' => 'No request was unmarked, this probably means the request with id = ' . $requestId . ' is not existing or that request not marked as starred.',
            ];
        }
    }

    public function createRequest($currentUser, $formId, $action, $rawRequestData) {

//        error_reporting(E_ALL);
        $redis_cache = getRedisConnection();
        $initialWorkflowNode = $this->getWorkflowStartNodeByFormId($formId);
        $actionNodeId = $this->getNodeIdFromAvailableActions($action, $initialWorkflowNode['available_actions']);
        $requestData = $this->prepareRequestData(
                $currentUser['id'], $initialWorkflowNode, $rawRequestData, APIRequestManager::NEW_REQUEST
        );

        $requestData['Node_ID'] = $actionNodeId;

        $function = new functions();

        //  add date created & updated
        $requestData['DateCreated'] = $function->currentDateTime();
        $requestData['DateUpdated'] = $function->currentDateTime();

        //  since Request class has errors ignored, this is needed.
        //  TODO: if time allows, ask senior developers about revision of this class
        //  error reporting should only be disabled for view purpose and not when processing
        //  in background.
        //error_reporting(0);
        $request = new Request();
        $request->load($formId, 0);
        $request->data = $requestData;
        $request->save();

        if ($action != 'Save' && $action != 'Cancel') {
            //$myRequest->keywordFields = $keywordsField;
            $request->processWF(false);
        }

        if ($redis_cache) {

            $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

            $deleteMemecachedKeys = array_merge($myrequest_memcached);

            functions::deleteMemcacheKeys($deleteMemecachedKeys);

            functions::clearRequestRelatedCache("request_list", "form_id_" . $formId);
            functions::clearRequestRelatedCache("picklist", "form_id_" . $formId);
            functions::clearRequestRelatedCache("calendar_view", "form_id_" . $formId);
            functions::clearRequestRelatedCache("request_list_count", "form_id_" . $formId);

            //formula
            $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
                "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
                "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
                "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
            functions::clearFormulaLookupCaches($formId, $formula_arr);
        }

        return [
            'request_id' => $request->id,
            'request_tracking_number' => $request->trackNo
        ];
    }

    public function updateRequest($currentUser, $formId, $requestId, $action, $rawRequestData) {

        //error_reporting(E_ALL);
//        $workflowNodeData = $this->getWorkflowNodeByNodeId($request->fields['Workflow_ID'], $request->fields['Node_ID']);
        $redis_cache = getRedisConnection();
        $workflowNodeData = $this->getWorkflowNodeDataFromRequest($formId, $requestId);
        $actionNodeId = $this->getNodeIdFromAvailableActions($action, $workflowNodeData['available_actions']);

        $requestData = $this->prepareRequestData(
                $currentUser['id'], $workflowNodeData, $rawRequestData, APIRequestManager::EXISTING_REQUEST
        );

        if ($actionNodeId != 'Save' && $actionNodeId != 'Cancel') {
            $requestData['Node_ID'] = $actionNodeId;
        }

        //  since Request class has errors ignored, this is needed.
        //  TODO: if time allows, ask senior developers about revision of this class
        //  error reporting should only be disabled for view purpose and not when processing
        //  in background.
        //error_reporting(0);

        $request = new Request();
        $request->load($formId, $requestId);

        if ($action == 'Cancel') {
            $requestData['Status'] = 'Cancelled';
            $requestData['Processor'] = '';
            $requestData['LastAction'] = '';
        }

        if ($action == 'Save' || $action == 'Cancel') {
            $logDoc = new Request_Log($request);
            $logDoc->form_id = $formId;
            $logDoc->request_id = $requestId;

            if ($action == 'Save') {
                $logDoc->details = 'Saved';
            } else {
                $logDoc->details = 'Cancelled';
            }

            $logDoc->created_by_id = $currentUser['id'];
            $functions = new functions();
            $logDoc->date_created = $functions->currentDateTime();
            $logDoc->save();
        }

        $function = new functions();

        //  add date updated
        $requestData['DateUpdated'] = $function->currentDateTime();

        $request->data = $requestData;
        $request->createFieldLevelLogs($workflowNodeData['fields_enabled']);
        $request->modify();

        if ($action != 'Save' && $action != 'Cancel') {
            //$myRequest->keywordFields = $keywordsField;
            $request->processWF(false);
        }

        if ($redis_cache) {

            $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

            $deleteMemecachedKeys = array_merge($myrequest_memcached);

            functions::deleteMemcacheKeys($deleteMemecachedKeys);

            functions::clearRequestRelatedCache("request_list", "form_id_" . $formId);
            functions::clearRequestRelatedCache("picklist", "form_id_" . $formId);
            functions::clearRequestRelatedCache("calendar_view", "form_id_" . $formId);
            functions::clearRequestRelatedCache("request_list_count", "form_id_" . $formId);
            functions::clearRequestRelatedCache("request_details_" . $formId, $requestId);
            functions::clearRequestRelatedCache("request_access_" . $formId, $requestId);

            //formula
            $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
                "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
                "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
                "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
            functions::clearFormulaLookupCaches($formId, $formula_arr);
        }

        return [
            'request_id' => $request->id,
            'request_tracking_number' => $request->trackNo
        ];
    }

    public function getNodeIdFromAvailableActions($action, $availableActions) {

        $availableActionsDecodedLowerCase = json_decode(strtolower($availableActions), true);

        if (!is_array($availableActionsDecodedLowerCase)) {
            throw new APIException(
            'Invalid available actions/buttonStatus', 'The available actions/buttonStatus in the workflow has invalid format and cannot be read.'
            );
        }

        if (empty($action)) {
            throw new APIException(
            'Invalid action', 'You cannot submit a request with a blank action.'
            );
        }

        if ($action != 'Save' && $action != 'Cancel') {
            if (!array_key_exists(strtolower($action), $availableActionsDecodedLowerCase)) {
                throw new APIException(
                'Invalid action', "The submitted action ({$action}) for this request is not yet available in the current workflow or is not existing at all."
                );
            }

            $actionObject = $availableActionsDecodedLowerCase[strtolower($action)];
            $nodeId = $actionObject['child_id'];
        } else {
            $nodeId = $action;
        }

        return $nodeId;
    }

    //  <editor-fold desc="Workflow Methods">

    public function getWorkflowStartNodeByWorkflowId($workflowId) {
        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $query = "SELECT id, workflow_id, object_id AS node_id, type_rel, buttonStatus AS available_actions, "
                . "fieldEnabled AS fields_enabled, fieldRequired AS fields_required FROM tbworkflow_objects "
                . "WHERE workflow_id = '{$workflowId}' AND type_rel = '1'";

        $nodeData = $this->database->query($query, 'row');

        if ($nodeData) {

            $nodeData['fields_enabled'] = json_decode($nodeData['fields_enabled']);
            $nodeData['available_actions'] = json_decode($nodeData['available_actions']);

            return $nodeData;
        } else {
            throw new APIException(
            'Node not found', 'Initial node of workflow ' . $workflowId . ' not found.'
            );
        }
    }

    /**
     * @todo Create unit test for this function
     */
    public function getWorkflowNodeDataFromRequest($formId, $requestId) {

        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $workflowAndFormInfo = $this->getWorkflowIdAndFormTableName($formId);
        $query = "SELECT Node_ID, Workflow_ID AS workflow_id, LastAction AS available_actions, fieldEnabled AS fields_enabled 
                FROM {$workflowAndFormInfo['form_table_name']}
                WHERE id = {$this->database->escape($requestId)}";

        $node = $this->database->query($query, 'row');

        if (!$node) {
            throw new APIException(
            'Request Not Found', 'Unable to find request with id = ' . $requestId . ' when trying to fetch workflow information from that request.'
            );
        }

        return $node;
    }

    /**
     * @todo Create unit test for this function
     */
    public function getWorkflowNodeByNodeId($workflowId, $nodeId) {

        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $query = "SELECT id, workflow_id, object_id AS node_id, type_rel, buttonStatus AS available_actions, "
                . "fieldEnabled AS fields_enabled, fieldRequired AS fields_required FROM tbworkflow_objects "
                . "WHERE workflow_id = '{$workflowId}' AND object_id = {$this->database->escape(nodeId)}";

        $nodeData = $this->database->query($query, 'row');

        if ($nodeData) {

            $nodeData['fields_enabled'] = json_decode($nodeData['fields_enabled']);
            $nodeData['available_actions'] = json_decode($nodeData['available_actions']);

            return $nodeData;
        } else {
            throw new APIException(
            'Node not found', 'Node ' . $nodeId . ' of workflow ' . $workflowId . ' not found.'
            );
        }
    }

    public function getWorkflowStartNodeByFormId($formId) {

        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $query = "SELECT wfo.id, wfo.type_rel, wfo.workflow_id, wfo.object_id AS node_id, wfo.buttonStatus AS available_actions, wfo.status, wfo.fieldEnabled AS fields_enabled
                    FROM tb_workspace WS 
                    LEFT JOIN tbworkflow wf
                    ON WS.Id = wf.form_id 
                    LEFT JOIN tbworkflow_objects wfo
                    ON wfo.workflow_id = wf.id 
                    WHERE WS.id={$formId} AND type_rel='1' AND wf.Is_Active = '1'";

        $node = $this->database->query($query, 'row');

        if (!$node) {
            throw new APIException(
            'Workflow or form not found', 'Unable to find workflow for form with id = ' . $formId
            );
        }

        return $node;
    }

    public function getWorkflowIdAndFormTableName($formId) {

        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $query = "SELECT workflow.id AS workflow_id, workspace.form_table_name FROM tb_workspace workspace 
                    LEFT JOIN tbworkflow workflow ON workflow.form_id = workspace.id 
                    WHERE workflow.form_id = '{$formId}' AND workflow.is_active = 1;";
        $workflowData = $this->database->query($query, 'row');

        if (!$workflowData['workflow_id']) {
            throw new APIException(
            'Workflow or form not found', 'Unable to find workflow for form with id = ' . $formId
            );
        }

        return $workflowData;
    }

    public function getFormTableName($formId) {
        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $query = "SELECT form_table_name FROM tb_workspace WHERE id = {$formId}";
        $result = $this->database->query($query, 'row');

        if (!$result['form_table_name']) {
            throw new APIException(
            'Form not found', 'Unable to find the table for the form with id = ' . $formId
            );
        }

        return $result['form_table_name'];
    }

    /**
     * 
     * @todo Create a unit test for this function
     */
    public function getAvailableActions($formId, $requestId, $userId) {

        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        $workflowAndFormInfo = $this->getWorkflowIdAndFormTableName($formId);
        $query = "SELECT wo.buttonStatus AS available_actions, request.Processor, wo.type_rel FROM {$workflowAndFormInfo['form_table_name']} request 
                    LEFT JOIN tbworkflow_objects wo 
                    ON request.node_id = wo.object_id AND request.status = wo.status  
                    WHERE request.ID = {$this->database->escape($requestId)}";

        $results = $this->database->query($query, 'row');
        if ($results['Processor'] == $userId && ($results['type_rel'] != 1 || $results['type_rel'] != 4)) {
            return $this->createAvailableActionsArray($results['available_actions']);
        } else {
            return [];
        }
    }

    private function createAvailableActionsArray($actionsJSON) {
        if (!is_array($actionsJSON)) {
            $actionsJSON = json_decode($actionsJSON);
        }

        $actions = [];

        foreach ($actionsJSON as $key => $value) {
            array_push($actions, array(
                'label' => $key,
                'action' => $value->child_id
            ));
        }

        if (count($actions) > 0) {
            array_push($actions, array(
                'label' => 'Save',
                'action' => 'Save'
            ));
            array_push($actions, array(
                'label' => 'Cancel',
                'action' => 'Cancel'
            ));
        }

        return $actions;
    }

    //  </editor-fold>
    //  <editor-fold desc="Request Getters" defaultstate="collapsed">

    /**
     * 
     * @todo Add condition that if the user is on the viewers, it should also be available to him as well.
     * Currently, this only returns requests that are either his own request or he is the processor of that request.
     */
    public function getAvailableRequests(APISearchParameters $searchParameters) {

        if ($searchParameters->userLevelId === 2) {
            $searchParameters->condition = "";
        } else {
            $searchParameters->condition = "request.Requestor = '{$searchParameters->userId}' OR request.Processor = '{$searchParameters->userId}'";
        }

        return $this->searchRequests($searchParameters);
    }

    public function getOwnRequests(APISearchParameters $searchParameters) {

        $searchParameters->condition = "request.Requestor = '{$searchParameters->userId}'";
        return $this->searchRequests($searchParameters);
//        return $this->searchRequests($formId, $userId, $fields, $condition, $lowerLimit, $upperLimit, 'DateCreated DESC');
    }

    public function getForProcessingRequests(APISearchParameters $searchParameters) {

        $searchParameters->condition = "request.Processor = '{$searchParameters->userId}'";
        return $this->searchRequests($searchParameters);
//        return $this->searchRequests($formId, $userId, $fields, $condition, $lowerLimit, $upperLimit, 'DateCreated DESC');
    }

    public function getRequest($formId, $requestId, $userId, $fields = []) {

        if (!$this->database) {
            throw new Exception('No database set in APIRequestManager instance.');
        }

        if ($fields == APIRequestManager::FETCH_ALL_FIELDS) {
            $fields = [];
        }

        $workflowAndFormInfo = $this->getWorkflowIdAndFormTableName($formId);
        $selectClause = $this->utilities->generateSelectClause($fields, [], APIUtilities::ALLOW_SELECT_ALL, 'request');

//        $conditions = "request.ID = {$this->database->escape($requestId)} AND (request.Requestor = {$this->database->escape($userId)} OR request.Processor = {$this->database->escape($userId)})";
        $conditions = "request.ID = {$this->database->escape($requestId)} ";

        $extraJoins = '';

        $selectClause .= $this->generateSelectClauseForRequestRequestors();
        $selectClause .= $this->generateSelectClauseForRequestProcessors();

        $extraJoins .= $this->generateJoinClauseForRequestRequestors();
        $extraJoins .= $this->generateJoinClauseForRequestProcessors();

        $query = "SELECT {$selectClause}, IF(starred.data_id IS NOT NULL, 1, 0) as is_starred "
                . "FROM {$workflowAndFormInfo['form_table_name']} request "
                . "LEFT JOIN tbstarred starred "
                . "ON "
                . "request.id = starred.data_id AND "
                . "starred.tablename = '{$workflowAndFormInfo['form_table_name']}' AND "
                . "starred.user_id = {$this->database->escape($userId)} "
                . "{$extraJoins} "
                . "WHERE {$conditions} ORDER BY DateCreated DESC LIMIT 1";

        $request = $this->database->query($query, 'row');

        if ($request) {

            //  add the available actions in the results
            if ($request['Processor'] == $userId) {
                $availableActions = $this->createAvailableActionsArray($request['LastAction']);
            } else {
                $availableActions = [];
            }

            $results['request'] = $request;
            $results['available_actions'] = $availableActions;
            return $results;
        } else {
            throw new APIException('Request Not Found', "Request with id = {$requestId} is not existing or the user is not allowed to access this request.");
        }
    }

    public function searchRequests(APISearchParameters $searchParameters) {

        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }

        if ($searchParameters->fields == APIRequestManager::FETCH_ALL_FIELDS) {
            $searchParameters->fields = [];
        }

        $workflowAndFormInfo = $this->getWorkflowIdAndFormTableName($searchParameters->formId);
        $selectClause = $this->utilities->generateSelectClause($searchParameters->fields, [], APIUtilities::ALLOW_SELECT_ALL, 'request');

        $extraJoins = '';

//        if ($searchParameters->includeRequestorInfo) {
        $selectClause .= $this->generateSelectClauseForRequestRequestors();
        $extraJoins .= $this->generateJoinClauseForRequestRequestors();
//        }
//        if ($searchParameters->includeProcessorInfo) {
        $selectClause .= $this->generateSelectClauseForRequestProcessors();
        $extraJoins .= $this->generateJoinClauseForRequestProcessors();
//        }

        $dateComparisonConditions = '';
        if (!empty($searchParameters->dateCreatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchParameters->createDateComparisonCondition($searchParameters->dateCreatedComparison, 'DateCreated');
        }

        if (!empty($searchParameters->dateUpdatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchParameters->createDateComparisonCondition($searchParameters->dateUpdatedComparison, 'DateUpdated');
        }

        if (!empty($searchParameters->condition)) {
            $generatedConditions = '(' . $searchParameters->condition . ')';
        }

        if (!empty($searchParameters->extraConditionFields)) {
            if (trim($generatedConditions) == "") {
                $generatedConditions .= $this->generateConditionClauseFromFields($searchParameters->extraConditionFields);
            } else {
                $generatedConditions .= " AND " . $this->generateConditionClauseFromFields($searchParameters->extraConditionFields);
            }
        }

        //  trim the leading ' AND ' if there are no other conditions
        if (empty($generatedConditions)) {
            $dateComparisonConditions = substr($dateComparisonConditions, 5);
        }

        $searchParameters->condition = $generatedConditions . ' ' . $dateComparisonConditions . ' ';

        $query = "SELECT {$selectClause}, IF(starred.data_id IS NOT NULL, 1, 0) as is_starred "
                . "FROM {$workflowAndFormInfo['form_table_name']} request "
                . "LEFT JOIN tbstarred starred "
                . "ON "
                . "request.id = starred.data_id AND "
                . "starred.tablename = '{$workflowAndFormInfo['form_table_name']}' AND "
                . "starred.user_id = {$this->database->escape($userId)} "
                . $extraJoins;

        $currentUserId = $searchParameters->userId;
        $formId = $searchParameters->formId;
        $defaultConditions = "(requestor.id = {$currentUserId} 
	OR FIND_IN_SET({$currentUserId}, getFormUsers({$formId},2))
	OR FIND_IN_SET({$currentUserId}, processor.id)
	OR FIND_IN_SET({$currentUserId}, getRequestUsers({$formId}, request.id, 1))
	OR FIND_IN_SET({$currentUserId}, getRequestUsers({$formId}, request.id, 2))
	OR FIND_IN_SET({$currentUserId}, getRequestViewers({$formId}, request.id))
	OR FIND_IN_SET({$currentUserId}, request.Editor)
	OR FIND_IN_SET({$currentUserId}, request.Viewer))";

        if (trim($searchParameters->condition) != "") {
            $query .= "WHERE {$searchParameters->condition} AND {$defaultConditions}";
        } else {
            $query .= "WHERE {$defaultConditions} ";
        }

        if ($searchParameters->orderByField) {
            $query .= "ORDER BY {$searchParameters->orderByField} LIMIT {$searchParameters->searchRecordsFrom}, {$searchParameters->searchRecordsCount}";
        } else {
            $query .= "LIMIT {$searchParameters->searchRecordsFrom}, {$searchParameters->searchRecordsCount}";
        }

        $searchParameters->searchQueryUsed = $query;
//        echo $query;
//        exit;
        return $this->database->query($query);
    }

    private function generateConditionClauseFromFields($fields) {

        $conditions = "";

        foreach ($fields as $key => $field) {
            if (is_array($field)) {
                $conditions .= $key . " IN ('" . implode("','", $field) . "') OR ";
            } else {
                $conditions .= $key . "=" . $this->database->escape($field) . " OR ";
            }
        }

        //  trim the last " OR " in the generated conditions 
        return "(" . substr($conditions, 0, strlen($conditions) - 3) . ")";
    }

    private function generateSelectClauseForRequestRequestors() {

        $clause = "";

        foreach ($this->selectableUserColumns as $userColumns) {
            $clause .= ", requestor.{$userColumns} AS requestor_{$userColumns} ";
        }

        $clause .= ",requestor.id AS requestor_id, requestor.extension AS requestor_image_extension";

        return $clause;
    }

    private function generateSelectClauseForRequestProcessors() {
        $clause = "";

        foreach ($this->selectableUserColumns as $userColumns) {
            $clause .= ", processor.{$userColumns} AS processor_{$userColumns} ";
        }

        $clause .= ",processor.id AS processor_id, processor.extension AS processor_image_extension";

        return $clause;
    }

    private function generateJoinClauseForRequestRequestors() {
        $clause = "LEFT JOIN tbuser requestor ON request.Requestor = requestor.id ";
        return $clause;
    }

    private function generateJoinClauseForRequestProcessors() {
        $clause = "LEFT JOIN tbuser processor ON request.Processor = processor.id ";
        return $clause;
    }

    //  </editor-fold>
    //  <editor-fold desc="Utility Methods" defaultstate="collapsed">

    public function prepareRequestData($currentUserId, $workflowNodeData, $rawRequestData, $requestType) {

        $fieldsEnabled = (is_array($workflowNodeData['fields_enabled']) ? $workflowNodeData['fields_enabled'] : json_decode($workflowNodeData['fields_enabled'], true));
        $processedRequestData = $this->sanitizeRequestData($rawRequestData, $fieldsEnabled);

        if ($requestType == APIRequestManager::NEW_REQUEST) {
            //  additional data for new requests
            $processedRequestData['Requestor'] = $currentUserId;
            $processedRequestData['CreatedBy'] = $currentUserId;
            $processedRequestData['DateCreated'] = date('Y-m-d H:i:s');
            $processedRequestData['Workflow_ID'] = $workflowNodeData['workflow_id'];
            $processedRequestData['Processor'] = $currentUserId;    //  this will be changed by processWF
        }

        $processedRequestData['UpdatedBy'] = $currentUserId;
        $processedRequestData['DateUpdated'] = date('Y-m-d H:i:s');

        return $processedRequestData;
    }

    /**
     * Removes elements from the $rawRequestData that are either not in the 
     * $fieldsEnabled or is a reserved field.
     * @param Array $rawRequestData the data to sanitize
     * @param Array $fieldsEnabled the fields enabled
     * @return Array the sanitized data, see function description.
     */
    public function sanitizeRequestData($rawRequestData, $fieldsEnabled) {

        $sanitizedData = [];

        if (is_string($rawRequestData)) {
            $rawRequestData = json_decode($rawRequestData);
        }

        foreach ($rawRequestData as $fieldName => $fieldValue) {

            $isFieldEnabled = in_array($fieldName, $fieldsEnabled);
            $isFieldReserved = in_array($fieldName, $this->reservedFields);

            //  only add fields that are specified in fields enabled, and is
            //  not a reserved field
            if ($isFieldEnabled && !$isFieldReserved) {
                $sanitizedData[$fieldName] = $fieldValue;
            }

            /*
                GEOLOCATION Reserve Keywords
                    Geolocation Details
                    This to adapt the values from the incoming document

                handle by: Joshua Reyes [10-25-2016]
            */
            if ($fieldName == 'geolocation_details') {
                $sanitizedData['GeoLocationDetails'] = $fieldValue;
            }
        }

        return $sanitizedData;
    }

    //  </editor-fold>
}
