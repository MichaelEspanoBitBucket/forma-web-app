<?php

/**
 * Description of APIUserManager
 *
 * @author ervinne
 */
class APIUserManager extends APIManager {

    protected $selectableFields = [
        'id',
        'company_id',
        'email',
        'display_name',
        'first_name',
        'middle_name',
        'last_name',
        'contact_number',
        'date_registered',
        'is_active',
        'department_id',
        'user_level_id'
    ];

    public function __construct(APIDatabase $database, APIUtilities $utilities) {
        parent::__construct($database, $utilities);
    }

    public function getOnlineUsers($companyId, $currentUserId) {

        if (!$this->database) {
            throw new APIException(
            APIStatus::ERROR_API_ERROR, 'No database set or disconnected to database in API instance.'
            );
        }

        $userDataList = array();

        $isActive = "1";
        $onlineUsers = $this->database->query("SELECT id, display_name, first_name, last_name, email, user_level_id, extension FROM tbuser "
                . "WHERE company_id={$this->database->escape($companyId)} "
                . "AND id!={$this->database->escape($currentUserId)} "
                . "AND is_available={$this->database->escape($isActive)} "
                . "AND is_active={$this->database->escape($isActive)}", "array");

        //  get the images of each users
        foreach ($onlineUsers as $onlineUser) {

            $encryptID = md5(md5($onlineUser['id']));
            $path = $encryptID . "." . "/small_" . $encryptID . "." . $onlineUser['extension'];
            $imageURL = "/images/formalistics/tbuser/" . $path;

            $userData = array(
                'id' => $onlineUser['id'],
                'displayName' => $onlineUser['display_name'],
                'imageURL' => $imageURL,
				'email' => $onlineUser['email'],
				'user_level_id' => $onlineUser['user_level_id']
            );

            array_push($userDataList, $userData);
        }

        return $userDataList;
    }

    public function getUser($userId, $fields = []) {

        $selectClause = $this->buildSelectClauseForUser($fields);
        $query = "SELECT {$selectClause}, extension FROM tbuser WHERE id = '{$userId}'";

        if (!$this->database) {
            throw new APIException(
            APIStatus::ERROR_API_ERROR, 'No database set or disconnected to database in API instance.'
            );
        }

        try {
            $user = $this->database->query($query, 'row');

            //  user fetched, prepare the response
            if ($user) {

                if ($user['extension'] != '') {
                    $user['image_url'] = $this->generateUserImageURL($userId, $user['extension']);
                } else {
                    $user['image_url'] = null;
                }

                unset($user['password']);
                unset($user['extension']);
            } else {
                throw new APIException(
                'User Not Found', 'User with id = ' . $userId . ' not found.'
                );
            }

            return $user;
        } catch (APIException $e) {
            $this->refineAPIMySQLError($e);
        }
    }

    public function buildSelectClauseForUser($requestedFields = []) {
        if (!$requestedFields || empty($requestedFields)) {
            $requestedFields = $this->selectableFields;
        }

        return $this->utilities->generateSelectClause($requestedFields, ['password']);
    }

    public function generateUserImageURL($userId, $imageExtension) {

        if ($imageExtension == null || $imageExtension == "") {
            return null;
        }

        $table = "tbuser";
        $id_encrypt = md5(md5($userId));
        $size = "small";

        return '/images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $imageExtension;
    }

    public function getSelectableFields() {
        return $this->selectableFields;
    }

}
