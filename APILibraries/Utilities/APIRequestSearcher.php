<?php

/**
 * Description of APIRequestSearcher
 *
 * @author ervinne
 */
class APIRequestSearcher {

    /** @var APIDatabase */
    protected $database;
    protected $searchable_fields = array("ID", "TrackNo", "Requestor", "Status", "Processor", "LastAction",
        "DateCreated", "DateUpdated", "CreatedBy", "UpdatedBy", "Node_ID", "Workflow_ID",
        "fieldEnabled", "fieldRequired", "fieldHiddenValues");

    public function __construct(APIDatabase $database) {
        $this->database = $database;
    }

    public function getFormDataList($form_id_list = array()) {

        $query = "SELECT ws.id AS form_id, ws.form_name, ws.form_table_name, wf.id AS workflow_id 
                    FROM tb_workspace ws 
                    LEFT JOIN tbworkflow wf
                    ON ws.id = wf.form_id
                    WHERE ws.is_active = '1'";

        if (!empty($form_id_list)) {
            $form_id_list_string = implode(',', $form_id_list);
            $query .= " AND id IN ({$form_id_list_string})";
        }

        $this->database->connect();
        $result_set = $this->database->query($query);
        $this->database->disconnect();

        return $result_set;
    }

    public function search(APIRequestSearchFilter $searchFilter, $form_id_list = array()) {

        $this->commonValidation();

        $form_data_list = $this->getFormDataList($form_id_list);

        $generatedConditions = $this->generateDateConditions($searchFilter);



        foreach ($form_data_list AS $form_data) {
            $generated_partial_query = $this->createRequestQueryFromForm($form_data, $searchFilter, $generatedConditions);
            $generated_query .= $generated_partial_query . " UNION";
        }

        //  Remove Trailing UNION
        $generated_query = substr($generated_query, 0, strlen($generatedConditions) - 5);

        return $generated_query;
    }

    private function commonValidation() {
        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }
    }

    private function generateDateConditions(APIRequestSearchFilter $searchFilter) {
        $dateComparisonConditions = '';
        if (!empty($searchFilter->dateCreatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchFilter->createDateComparisonCondition($searchFilter->dateCreatedComparison, 'DateCreated');
        }

        if (!empty($searchFilter->dateUpdatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchFilter->createDateComparisonCondition($searchFilter->dateUpdatedComparison, 'DateUpdated');
        }

        if (!empty($dateComparisonConditions)) {
            return '(' . $dateComparisonConditions . ')';
        } else {
            return '';
        }
    }

    private function createRequestQueryFromForm($form_data, APIRequestSearchFilter $searchParameters, $whereClause) {

        if (!$searchParameters->fields || $searchParameters->fields == []) {
            $column_selection = implode(',', $this->searchable_fields);
        } else {
            $column_selection = implode(',', $searchParameters->fields);
        }

        $form_data_selection = "'{$form_data['form_id']}' AS Form_ID, "
                . "'{$form_data['form_table_name']}' AS Form_Table_Name";

        $query = "(SELECT {$form_data_selection}, {$column_selection} FROM {$form_data['form_table_name']} WHERE {$whereClause})";

        echo $query;

        return $query;
    }

}
