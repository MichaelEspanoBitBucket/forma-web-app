<?php

/**
 * Description of MultiFormRequestSearcher
 *
 * @author ervinne
 */
class MultiFormRequestSearcher {

    /** @var APIDatabase */
    protected $database;
    protected $searchable_fields = array("Form_ID", "ID", "TrackNo", "Requestor", "Status", "Processor", "LastAction",
        "DateCreated", "DateUpdated", "CreatedBy", "UpdatedBy", "Node_ID", "Workflow_ID",
        "fieldEnabled", "fieldRequired", "fieldHiddenValues");

    public function __construct(APIDatabase $database) {
        $this->database = $database;
    }

    private function commonValidation(APISearchParameters $searchParameters) {
        if (empty($searchParameters->condition)) {
            throw new APIException(APIStatus::ERROR_INVALID_PARAMETERS, 'Search request should not have empty conditions.');
        }

        if (!$this->database) {
            throw new APIException(APIStatus::ERROR_API_ERROR, 'No database set in APIRequestManager instance.');
        }
    }

    public function getFormDataList($form_id_list) {

        $query = "SELECT ws.id, ws.form_name, ws.form_table_name, wf.id AS workflow_id 
                    FROM tb_workspace ws 
                    LEFT JOIN tbworkflow wf
                    ON ws.id = wf.form_id
                    WHERE ws.is_active = '1'";

        if (!empty($form_id_list)) {
            $form_id_list_string = implode(',', $form_id_list);
            $query .= " AND id IN ({$form_id_list_string})";
        }

        $this->database->connect();
        $result_set = $this->database->query($query);
        $this->database->disconnect();

        return $result_set;
    }

    private function generateDateConditions(APISearchParameters $searchParameters) {
        $dateComparisonConditions = '';
        if (!empty($searchParameters->dateCreatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchParameters->createDateComparisonCondition($searchParameters->dateCreatedComparison, 'DateCreated');
        }

        if (!empty($searchParameters->dateUpdatedComparison)) {
            $dateComparisonConditions .= ' AND ' . $searchParameters->createDateComparisonCondition($searchParameters->dateUpdatedComparison, 'DateUpdated');
        }

        return '(' . $searchParameters->condition . ')';
    }
    
    private function createRequestQueryFromForm($form_data, APISearchParameters $searchParameters, $whereClause) {
        
        if (!$searchParameters->fields || $searchParameters->fields == []) {
            $column_selection = impload(',', $searchable_fields);
        } else {
            $column_selection = impload(',', $searchParameters->fields);
        }
        
        $query = "(SELECT {$column_selection} FROM {$form_data['form_table_name']} WHERE {$whereClause})";
        
        return $query;
        
    }

    public function search(APISearchParameters $searchParameters, $form_id_list = []) {
        $this->commonValidation($searchParameters);
        
        $form_data_list = $this->getFormDataList($form_id_list);        
        
        $generated_query = '';
        
        $generatedConditions = $this->generateDateConditions($searchParameters);
        if (!empty($searchParameters->extraConditionFields)) {
            $generatedConditions .= " AND (" . $this->generateConditionCluaseFromFields($searchParameters->extraConditionFields);
            //  trim the last " AND " in the generated conditions 
            $generatedConditions = substr($generatedConditions, 0, strlen($generatedConditions) - 4) . ") ";
        }
        
        foreach ($form_data_list AS $form_data) {
            $generated_partial_query = $this->createRequestQueryFromForm($form_data, $searchParameters, $generatedConditions);
            $generated_query .= $generated_partial_query . " UNION";
        }
     
        //  Remove Trailing UNION
        $generated_query = substr($generated_query, 0, strlen($generatedConditions) - 5);
        
        echo $generated_query;
    }

}
