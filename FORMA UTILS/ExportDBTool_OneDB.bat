@echo off
cd C:\wamp\bin\mysql\mysql5.6.12\bin
set /p DBName="Enter database name: "
set mydate=%date:~-4,4%%date:~-10,2%%date:~-7,2%
set mytime=%time:~-11,2%%time:~3,2%
set fname=%DBName%_%mydate%-%mytime%.sql
mysqldump -u root -p --routines %DBName% --result-file="%UserProfile%\Desktop\%fname%"
echo "DB Location --> %UserProfile%\Desktop\%fname%"
pause
exit