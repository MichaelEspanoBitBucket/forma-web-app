@TITLE QUICK MYSQL IMPORT
echo off
cls
set save_point_dir=%cd%
cd\
cd "C:\wamp\bin\mysql\mysql5.6.17\bin"
echo NOTE: Please make sure your import file.sql does not use the clause USE DB_NAME
echo.Please make sure also that this batch file, bin path of the wamp mysql is correct as the path on your C:
echo.
set selected_user_name=root
echo Please enter database name: &set /p selected_db_name=
echo.
@TITLE QUICK MYSQL IMPORT FOR %selected_db_name%
echo Please enter sql file path: &set /p selected_sql_file_path=
echo.
@TITLE QUICK MYSQL IMPORT FOR %selected_db_name% FILE %selected_sql_file_path%
mysql -u %selected_user_name% %selected_db_name% < "%selected_sql_file_path%"
echo Finished importing to %selected_db_name%, press any key...
echo.
cd\
cd %save_point_dir%
pause
echo on