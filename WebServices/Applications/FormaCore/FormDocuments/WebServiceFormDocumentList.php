<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceFormDocumentList
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceFacade;

class WebServiceFormDocumentList extends WebServiceApplicationAbstract {

	protected $formId;
	protected $offset;
	protected $fetchCount;
	protected $sortType;
	protected $returnFields;

	public function initializeFormDocumentList($objects) {

		$this->formId  		= $objects['formId'];
		$this->offset 		= $objects['offset'];
		$this->fetchCount 	= $objects['fetchCount'];
		$this->sortType 	= $objects['sortType'];
		$this->returnFields = $objects['returnFields'];
	}

	public function getFormDocumentList() {
        
        $formTableName = $this->dao->getFormTableName($this->formId);
        return $this->dao->getAllDocuments($formTableName, $this->returnFields, $this->sortType, $this->offset, $this->fetchCount);
	}
}