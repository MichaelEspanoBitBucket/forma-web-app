<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceFormDocumentListDAO
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceFacade;

class WebServiceFormDocumentListDAO extends WebServiceApplicationAbstractDAO {

	public function getFormTableName($formId) {
		
		$statement = "SELECT `tb_workspace`.`form_table_name` as formTableName FROM `tb_workspace` WHERE `tb_workspace`.`ID` = :formId";
		$bindParams = array(
			':formId' => $formId
        );	

		return $this->db->query($statement, 'row', $bindParams);
	}

	public function getAllDocuments($formTableName, $returnFields, $sortType, $offset, $fetchCount) {

		$fieldsToSelect = '';

		if (!empty($returnFields)) {
			
			$returnFields = explode(',', $returnFields);
			foreach($returnFields as $field) {
				$fieldsToSelect .= "`{$formTableName['formTableName']}`.`{$field}`,";
			}
			$fieldsToSelect = substr($fieldsToSelect, 0, strlen($fieldsToSelect) - 1);

		} else {
			$fieldsToSelect = '*';
		}

		$sortTypeWhereClause = "ORDER BY `{$formTableName['formTableName']}`.`ID` {$sortType}";

		$statement = "SELECT {$fieldsToSelect} FROM `{$formTableName['formTableName']}` {$sortTypeWhereClause} LIMIT {$offset}, {$fetchCount}";
	
		return $this->db->query($statement, 'array', array());
	}
}