<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceFormList
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceFacade;

class WebServiceFormList extends WebServiceApplicationAbstract {
	
	protected $companyId;
	
	public function initializeFormList($objects) {
		
		$this->companyId = $objects['companyId'];
	}

	public function getFormList() {

		return $this->dao->getAllForms($this->companyId);
	}
}
