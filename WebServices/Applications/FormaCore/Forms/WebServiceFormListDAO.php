<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceFormListDAO
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceFacade;

class WebServiceFormListDAO extends WebServiceApplicationAbstractDAO {
	
	public function getAllForms($companyId) {

		$statement = "SELECT `tb_workspace`.`id` as formId, `tb_workspace`.`form_name` as formName,`tb_workspace`.`form_table_name` as formTableName FROM `tb_workspace` WHERE `tb_workspace`.`company_id` = :companyId";
		$bindParams = array(
			':companyId' => $companyId
        );	

		return $this->db->query($statement, 'array', $bindParams);
	}
}
