<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceLogin
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceFacade;

use WebServices\WebServiceStatus;

class WebServiceLogin extends WebServiceApplicationAbstract {

	protected $email;
	protected $password;
	protected $encryptedPassword;
	protected $auth;

	public function initializeLogin($objects, $auth) {

		$this->email  				= $objects['email'];
		$this->password 			= $objects['password'];
		$this->encryptedPassword 	= $objects['encryptedPassword'];
		$this->auth 				= $auth;
	}

	public function getLoginCredential() {

		$user = $this->dao->getUser($this->email, $this->encryptedPassword);

		if ($user) {
		    return $this->userIsAvailable($user);
		} else {
		    return $this->userNotAvailable();
        }
    }

    private function userIsAvailable($user) {
    	
    	/*
    		Note:
    			this function in auth cut the connection to the DB Query, the next query 
    			will not be available so always remember to reopen again the connection.

    			detected by joshua clifford [09-27-2016]
    	*/
    	$this->auth->login($this->email, $this->password, 'email', 'password', 'tbuser');

	    // compute the image link
	    if ($user['image_extension'] == null || $user['image_extension'] == '') {
	        $imageURL = null;
	    } else {
	        $encryptID = md5(md5($user['id']));
	        $path      = $encryptID.'.'.'/small_'.$encryptID.'.'.$user['image_extension'];
	        $imageURL  = '/images/formalistics/tbuser/'.$path;
	    }

	    $user['image_url'] = $imageURL;
	    unset($user['image_extension']);

	    $user['company'] = array(
	        'id'   => $user['company_id'],
	        'name' => $user['company_name']
	    );

	    // get user groups
	    $user['groups'] = $this->dao->getUserGroup($user['id']);

	    return array(
	    	'status' 	=> WebServiceStatus::WEBSERVICE_STATUS_SUCCESS,
	    	'results' 	=> $user
	    );	
    }

    private function userNotAvailable() {

    	$errorMessage = '';

	    // check if the user is existing
	    $result = $this->dao->getUserEmail($this->email);
	    $errorMessage = 'Unregistered email address';

	    // if the user exists but is not able to login, he probably entered an incorrect password
	    if (count($result) > 0) {
	        $user = $result[0];
	        if ($user['password'] != $encryptedPassword) {
	            $errorMessage = 'Incorrect password';
	        } elseif ($user['is_active'] != 1) {
	            $errorMessage = "User with email {$this->email} is not active";
	        } else {
	            $errorMessage = 'Unknown exception, please try again.';
	        }
	    }

	    return array(
	    	'status' 		=> WebServiceStatus::WEBSERVICE_STATUS_ERROR,
	    	'error'			=> 'Login Failed',
	    	'errorMessage' 	=> $errorMessage
	    );
    }
}
