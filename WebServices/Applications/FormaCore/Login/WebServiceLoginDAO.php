<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceLoginDAO
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceFacade;

class WebServiceLoginDAO extends WebServiceApplicationAbstractDAO {

	public function getUser($email, $password) {

		$statement = "SELECT 
            u.id,
            u.email,
            display_name,
            u.extension AS image_extension,
            position,
            department_id,
            department_position_level,
            du.department_code,
            ochartobjects.department,
            c.id AS company_id,
            c.name AS company_name,
            u.user_level_id
        FROM
            tbuser u
                LEFT JOIN
            tborgchart ochart ON ochart.company_id = u.company_id
                AND ochart.status = 1
                LEFT JOIN
            tbdepartment_users du ON u.id = du.user_id
                AND du.orgChart_id = ochart.id
                LEFT JOIN
            tborgchartobjects ochartobjects ON ochartobjects.orgChart_id = ochart.id
                AND ochartobjects.department_code = du.department_code
                LEFT JOIN
            tbcompany c ON u.company_id = c.id
        WHERE
            (u.email = :email OR u.username = :email)
                AND u.password = :password
                AND u.is_active = 1
        GROUP BY u.id";
        
        $bindParams = array(
            ':email' => $email, 
            ':password' => $password
        );
        
        return $this->db->query($statement, 'row', $bindParams);
	}

	public function getUserGroup($userId) {
        /*
            Function that are affected in old way of DB Query, the function are Auth->login
            so we need to reopen again the connection to the database to be able to run the query again.

            detected by joshua reyes [09-27-2016]
        */
        $this->db->connect();

        $statement = "SELECT group_id, group_name FROM tbform_groups_users gu LEFT JOIN tbform_groups g ON gu.group_id = g.id WHERE user_id = :userId AND gu.is_active = 1 AND g.is_active = 1";
        $bindParams = array(
            ':userId' => $userId
        );

        return $this->db->query($statement, 'array', $bindParams);
	}

	public function getUserEmail($email) {
	   
        $statement = "SELECT password, is_active FROM tbuser WHERE email = :email";
        $bindParams = array(
            ':email' => $email
        );

        return $this->db->query($statement, 'array', $bindParams);
    }
}
