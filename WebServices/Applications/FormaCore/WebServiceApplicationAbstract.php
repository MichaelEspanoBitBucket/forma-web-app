<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceApplicationAbstract
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceFacade;

abstract class WebServiceApplicationAbstract {

    protected $dao;

    public function __construct($dao) {
        $this->dao = $dao;
    }
}