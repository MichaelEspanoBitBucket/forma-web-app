<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceApplicationAbstractDAO
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceFacade;

abstract class WebServiceApplicationAbstractDAO {

    protected $db;

    public function __construct($db) {
        $this->db = $db;
    }
}