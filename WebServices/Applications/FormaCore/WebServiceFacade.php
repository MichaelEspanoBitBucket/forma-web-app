<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceFacade
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceFacade;

use WebServices\WebServicesStatus;
use WebServices\WebServiceException;

use WebServices\WebServiceUtilities;

class WebServiceFacade {

    /**
     * Return Application Class.
     * @return class instance
     */
	public static function searchApplication($application, $db) {
        
        try {
            
            $db->connect();
    		
            return $this->applicationList($application, $db);

        } catch (WebServiceException $e) {

            WebServiceUtilities::setWebServiceResponse(
                500, 
                WebServicesStatus::WEBSERVICE_STATUS_ERROR,
                null,
                $e->error,
                $e->message
            );

            return;
        }
	}

    /**
     * Return Application Instance.
     * @return class instance
     */
    private function applicationList($application, $db) {

        switch ($application) {

            /*
                Forma Core Files
            */
            case 'FormDocumentList':
                include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/FormDocuments/WebServiceFormDocumentList.php';
                include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/FormDocuments/WebServiceFormDocumentListDAO.php';
                return new WebServiceFormDocumentList(new WebServiceFormDocumentListDAO($db));
                
            case 'FormList':
                include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/Forms/WebServiceFormList.php';
                include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/Forms/WebServiceFormListDAO.php';
                return new WebServiceFormList(new WebServiceFormListDAO($db));

            case 'Login':
                include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/Login/WebServiceLogin.php';
                include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/Login/WebServiceLoginDAO.php';
                return new WebServiceLogin(new WebServiceLoginDAO($db));
            
            case 'Navigation':
                include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/Navigation/WebServiceNavigation.php';
                include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/Navigation/WebServiceNavigationDAO.php';
                return new WebServiceNavigation(new WebServiceNavigationDAO($db));

            default:
                return;
        }

        $db->disconnect();
    }
}

include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/WebServiceApplicationAbstract.php';
include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/WebServiceApplicationAbstractDAO.php';