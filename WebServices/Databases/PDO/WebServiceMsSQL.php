<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceMsSQL
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceDatabases;

use \PDO;

class WebServiceMsSQL extends WebServicePDODatabase {

	protected $dbmsType = 'MsSQL';
}