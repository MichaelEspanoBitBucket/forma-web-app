<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceMySQL
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceDatabases;

use \PDO;

class WebServiceMySQL extends WebServicePDODatabase {

	/**
	 *
     * 	===============================================
     *		make sure to enable the following in php.ini depending on the OS use:
	 *			php_pdo_mysql.dll = WIN
	 *			pdo_mysql.so = LINUX
     * 	===============================================
     *
     */
    protected $dbmsType = 'MySQL';

	public function connection() {

		$options = array(
		    PDO::ATTR_PERSISTENT 	=> true, 
		    PDO::ATTR_ERRMODE 		=> PDO::ERRMODE_EXCEPTION
		);

        return new PDO(
        	'mysql:host='.$this->host_name.';dbname='.$this->database_name, 
        	$this->user_name, 
        	$this->password, 
        	$options);
	}
}