<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceOracle
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceDatabases;

use \PDO;

class WebServiceOracle extends WebServicePDODatabase {

	protected $dbmsType = 'Oracle';
}