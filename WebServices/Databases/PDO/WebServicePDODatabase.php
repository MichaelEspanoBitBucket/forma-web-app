<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServicePDODatabase
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceDatabases;

use \PDO;

use WebServices\WebServiceChecker;

class WebServicePDODatabase extends WebServiceDatabaseAbstract {

    public function connect() {
        
        try {
            
            $this->connection = $this->connection();
        
        } catch (PDOException $e) {
            
            die ('Connection Failed: ' + $e->getMessage()); 
        }
    }

    public function disconnect() {
        
        $this->connection = null;
    }

    public function query($clause = '', $fetch_type = 'array', $bindParams = []) {

        WebServiceChecker::validateDatabaseConnection($this->connection);
        
        $statement = $this->connection->prepare($clause);
        
        $this->statement = $statement;
        $this->queryBindParam($bindParams);
        $this->statement->execute();

        try {

            switch ($fetch_type) {
            
                case WebServicePDODatabase::query_fetch_row:
                    $result = $this->statement->fetch(PDO::FETCH_ASSOC);
                    break;
                
                case WebServicePDODatabase::query_row_count:
                    $result = count($this->statement->fetchAll());
                    break;
                
                case WebServicePDODatabase::query_insert:
                    $result = $this->connection->lastInsertId();
                    break;
                
                case WebServicePDODatabase::query_update:
                case WebServicePDODatabase::query_delete:
                    $result = $this->statement->rowCount();
                    break;
                
                case WebServicePDODatabase::query_fetch_all:
                default:
                    $result = $this->statement->fetchAll(PDO::FETCH_ASSOC);
                    break;
            }

            return $result;

        } catch (PDOException $e) {
            
            return 'Error on Query: ' . $e->getMessage() . ' Statement: [' . $this->statement . ']';
        }

        unset($this->statement);
    }

    private function queryBindParam($bindParams) {

        foreach($bindParams as $key => &$value) {
            $this->statement->bindParam($key, $value);
        }
    }

    # BEGIN TRANSACTION
    public function beginTransaction() {
        
        $this->connection->beginTransaction();
    }

    # COMMIT TRANSACTION
    public function commitTransaction() {
        
        $this->connection->commit();
    }

    # ROLLBACK TRANSACTION
    public function rollbackTransaction() {
        
        $this->connection->rollBack();
    }

    # INSERT QUERY
    public function insert($table, $fields = []) {
        
        $fieldString = '';
        $fieldsBindParam = array();
        
        foreach ($fields as $key => $value) {
            $fieldString .= "`{$key}` = :field_{$key},";
            $fieldsBindParam[":field_{$key}"] = $value;
        }
        $fieldsString = substr($fieldString, 0, strlen($fieldString) - 1);

        return $this->query("INSERT INTO {$this->sanitizeSubject($table)} SET {$fieldsString}", 'insert', $fieldsBindParam);
    }

    # UPDATE QUERY
    public function update($table, $fields = [], $conditions = []) {

        $fieldString = '';
        $conditionString = '';
        $fieldsBindParam = array();
        $conditionsBindParam = array();

        foreach ($fields as $field => $value) {
            $fieldString .= "`{$field}` = :field_{$field},";
            $fieldsBindParam[":field_{$field}"] = $value;
        }
        foreach ($conditions as $field => $value) {
            $conditionString .= "`{$field}` = :condition_{$field} AND ";
            $conditionsBindParam[":condition_{$field}"] = $value;
        }
        $fieldsString = substr($fieldString, 0, strlen($fieldString) - 1);
        $conditionsString = substr($conditionString, 0, strlen($conditionString) - 4);

        $bindParams = array_merge($fieldsBindParam, $conditionsBindParam);

        return $this->query("UPDATE {$this->sanitizeSubject($table)} SET {$fieldsString} WHERE {$conditionsString}", 'update', $bindParams);
    }

    # DELETE QUERY
    public function delete($table, $conditions = []) {

        $conditionString = '';
        $conditionsBindParam = array();

        foreach ($conditions as $field => $value) {
            $conditionString .= "`{$field}` = :condition_{$field} AND ";
            $conditionsBindParam[":condition_{$field}"] = $value;
        }
        $conditionsString = substr($conditionString, 0, strlen($conditionString) - 4);

        return $this->query("DELETE FROM {$this->sanitizeSubject($table)} WHERE {$conditionsString}", 'update', $conditionsBindParam);
    }

    private function sanitizeSubject($subject) {
        
        $search   = array('\\', "\0", "\n", "\r", "'", '"', "\x1a", '`', '#', ';', '-');
        $replace  = array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z', '', '', '', '');

        return str_replace($search, $replace, $subject);
    }
}