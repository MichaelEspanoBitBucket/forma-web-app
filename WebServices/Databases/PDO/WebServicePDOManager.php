<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServicePDOManager
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceDatabases;

class WebServicePDOManager {

    public function getConnection($databaseConnection, $options) {

        $hostName       = $options['hostName'];
        $databaseName   = $options['databaseName'];
        $userName       = $options['userName'];
        $password       = $options['password'];

        switch ($databaseConnection) {
            
            case 'MSSQL':
                return new WebServiceMsSQL($hostName, $databaseName, $userName, $password);

            case 'ORACLE':
                return new WebServiceOracle($hostName, $databaseName, $userName, $password);

            case 'MYSQL':
            default:
                return new WebServiceMySQL($hostName, $databaseName, $userName, $password);
        }
    }
}

include_once APPLICATION_PATH.'/WebServices/Databases/PDO/WebServicePDODatabase.php';

include_once APPLICATION_PATH.'/WebServices/Databases/PDO/WebServiceMsSQL.php';
include_once APPLICATION_PATH.'/WebServices/Databases/PDO/WebServiceOracle.php';
include_once APPLICATION_PATH.'/WebServices/Databases/PDO/WebServiceMySQL.php';