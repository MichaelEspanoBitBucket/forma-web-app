<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceDatabaseAbstract
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceDatabases;

use WebServices\WebServiceConstant;

abstract class WebServiceDatabaseAbstract {

    protected $allowed_functions = [
		'NOW()', 'CURDATE()', 'CURTIME()'
	];

	protected $host_name;
	protected $user_name;
	protected $password;
	protected $database_name;
	
	protected $connection;
	protected $statement;

	const query_fetch_all = 'array';
	const query_fetch_row = 'row';
	const query_insert 	  = 'insert';
	const query_update 	  = 'update';
	const query_delete 	  = 'delete';
	const query_row_count = 'numrows';

    public function __construct($host_name = NULL, $database_name = NULL,  $user_name = NULL, $password = NULL) {
		
		$this->host_name 	 = $this->issetFilter($host_name, WebServiceConstant::WEBSERVICE_DATABASE_HOST);
		$this->user_name 	 = $this->issetFilter($user_name, WebServiceConstant::WEBSERVICE_DATABASE_USERNAME);
		$this->password  	 = $this->issetFilter($password, WebServiceConstant::WEBSERVICE_DATABASE_PASSWORD);
		$this->database_name = $this->issetFilter($database_name, WebServiceConstant::WEBSERVICE_DATABASE_NAME);
	}

	private function issetFilter($injectedConfig, $defaultConfig) {
		
		if (isset($injectedConfig)) {
			return $injectedConfig;
		} else {
			return $defaultConfig;
		}
	}
}