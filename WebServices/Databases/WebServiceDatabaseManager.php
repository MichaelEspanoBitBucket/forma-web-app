<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceDatabaseManager
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceDatabases;

use WebServices\WebServiceConstant;
use WebServices\WebServicesStatus;
use WebServices\WebServiceException;

use WebServices\WebServiceUtilities;

use WebServiceDatabases\WebServiceMySQLDatabase;
use WebServiceDatabases\WebServiceMySQLiDatabase;
use WebServiceDatabases\WebServicePDOManager;

class WebServiceDatabaseManager {

    public static function getDatabase() {

        try {

            return WebServiceDatabaseManager::databaseList(WebServiceConstant::WEBSERVICE_DATABASE_PLUGIN); 

        } catch (WebServiceException $e) {

            WebServiceUtilities::setWebServiceResponse(
                500, 
                WebServicesStatus::WEBSERVICE_STATUS_ERROR,
                null,
                $e->error,
                $e->message
            );

            return;
        }  
    }

    private static function databaseList($database) {

        switch ($database) {

            case 'MYSQL':
                include_once APPLICATION_PATH.'/WebServices/Databases/WebServiceMySQLDatabase.php';
                return new WebServiceMySQLDatabase();

            case 'MYSQLi':
                include_once APPLICATION_PATH.'/WebServices/Databases/WebServiceMySQLiDatabase.php';
                return new WebServiceMySQLiDatabase();

            case 'PDO':
                include_once APPLICATION_PATH.'/WebServices/Databases/PDO/WebServicePDOManager.php';
                return new WebServicePDOManager();
            
            default:
                return;
        }
    }
}

include_once APPLICATION_PATH.'/WebServices/Databases/WebServiceDatabaseAbstract.php';