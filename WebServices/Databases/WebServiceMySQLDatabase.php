<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceMySQLDatabase
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceDatabases;

class WebServiceMySQLDatabase extends WebServiceDatabaseAbstract {

	public function connect() {
		
		$connection = mysql_connect($this->host_name, $this->user_name, $this->password) or die ('Cannot connect to Host');
		mysql_select_db($this->database_name) or die ('Cannot connect to Database Name');
		$this->connection = $connection;
	}

	public function get_errors() {
		
		$err_arr = array(
			'error_number'	=> mysql_errno($this->connection),
			'error_message'	=> mysql_error($this->connection)
		);
		
		return $err_arr;
	}

	public function disconnect() {
		
		mysql_close($this->connection);
	}

	public function begin_transaction() {
		
		mysql_query("BEGIN");
	}

	public function commit() {
		
		mysql_query("COMMIT");
	}

	public function roll_back() {
		
		mysql_query("ROLLBACK");
	}


	public function query($sql, $fetch_type = WebServiceMySQLDatabase::query_fetch_all) {

		if (!$this->connection) {
            throw new Exception('No connection set up');
        }

		$result = mysql_query($sql);
		
		$data = array();
		if ($result) {
			if ($fetch_type === WebServiceMySQLDatabase::query_fetch_all) {
				while ($row = mysql_fetch_assoc($result)) {
					$data[] = $row;
				}
			} else if ($fetch_type === WebServiceMySQLDatabase::query_fetch_row) {
				$data = mysql_fetch_assoc($result);
			} else if ($fetch_type === WebServiceMySQLDatabase::query_update) {
				$data = mysql_affected_rows($this->connection);
			} else if ($fetch_type === WebServiceMySQLDatabase::query_insert) {
				$data = mysql_insert_id($this->connection);
			} else if ($fetch_type === WebServiceMySQLDatabase::query_row_count) {
				$data = mysql_num_rows($result);
			} else {
				return false;
			}
			return $data;
		} else if ($this->get_errors['error_number']) {
			# Return Error Message and SQL Code Query
			return $this->get_errors['error_message'] . "Query: " .$sql; 
		}
	}

	public function escape($clause) {
		
		return "'" . addslashes($clause) . "'";
	}

	# UPDATE QUERY
	public function update($table, $fields = [], $conditions = []) {
		
		$fields_string = "";
		$conditions_string = "";

		foreach ($fields as $field => $value) {
			$filtered_value = !in_array($value, $this->allowed_functions) && !is_numeric($value) ? $this->escape($value) : $value;
			$fields_string .= $field . "=" . $filtered_value . ","; 
		}
		foreach ($conditions as $field => $value) {
			$filtered_value = !is_numeric($value) ? $this->escape($value) : $value;
			$conditions_string .= $field . "=" . $filtered_value . " AND "; 
		}
		$fields_string = substr($fields_string, 0, strlen($fields_string) - 1);
		$conditions_string = substr($conditions_string, 0, strlen($conditions_string) - 4);

		$query = $this->query("UPDATE {$table} SET {$fields_string} WHERE {$conditions_string}", "update"); 
		
		return $query;
	}

	# INSERT QUERY
	public function insert($table, $fields = []) {
		
		$fields_string = "";

		foreach ($fields as $field => $value) {
			$filtered_value = !in_array($value, $this->allowed_functions) && !is_numeric($value) ? $this->escape($value) : $value;
			$fields_string .= $field . " = " . $filtered_value . ", ";
		}
		$fields_string = substr($fields_string, 0, strlen($fields_string) - 2);
		var_dump($fields_string);
		$query = $this->query("INSERT INTO {$table} SET {$fields_string}", "insert");

		return $query;
	}

	# DELETE QUERY
	public function delete($table, $conditions = []) {
		
		$conditions_string = "";

		foreach ($conditions as $field => $value) {
			$filtered_value = !is_numeric($value) ? $this->escape($value) : $value;
			$conditions_string .= $field . " = " . $filtered_value . " AND ";
		}
		$conditions_string = substr($conditions_string, 0, strlen($conditions_string) - 4);
		$query = $this->query("DELETE FROM {$table} WHERE {$conditions_string}", "update");

		return $query;
	}
}