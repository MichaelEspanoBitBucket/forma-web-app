<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceMySQLiDatabase
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceDatabases;

class WebServiceMySQLiDatabase extends WebServiceDatabaseAbstract {

	public function connect() {
		
		$connection = mysqli_connect($this->host_name, $this->user_name, $this->password, $this->database_name) or die ('Cannot connect to Host or Database');
		$this->connection = $connection;
	}

	public function get_errors() {
		
		return [
			'error_number'	=> mysqli_connect_errno($this->connection),
			'error_message'	=> mysqli_connect_error($this->connection)
		];
	}

	public function disconnect() {
		
		mysqli_close($this->connection);
	}

	public function begin_transaction() {
		
		mysqli_begin_transaction($this->connection, MYSQLI_TRANS_START_READ_ONLY);
	}

	public function commit() {
		
		mysqli_commit($this->connection);
	}

	public function roll_back() {
		
		mysqli_rollback($this->connection);
	}


	public function query($sql, $fetch_type = WebServiceMySQLiDatabase::query_fetch_all) {
		
		$result = mysqli_query($this->connection, $sql);
		
		$data = array();
		if ($result) {
			if ($fetch_type === WebServiceMySQLiDatabase::query_fetch_all) {
				while ($row = mysqli_fetch_assoc($result)) {
					$data[] = $row;
				}
			} else if ($fetch_type === WebServiceMySQLiDatabase::query_fetch_row) {
				$data = mysqli_fetch_assoc($result);
			} else if ($fetch_type === WebServiceMySQLiDatabase::query_update) {
				$data = mysqli_affected_rows($this->connection);
			} else if ($fetch_type === WebServiceMySQLiDatabase::query_insert) {
				$data = mysqli_insert_id($this->connection);
			} else if ($fetch_type === WebServiceMySQLiDatabase::query_row_count) {
				$data = mysqli_affected_rows($result);
			} else {
				return false;
			}
			return $data;
		} else if ($this->get_errors['error_number']) {
			# Return Error Message and SQL Code Query
			return $this->get_errors['error_message'] . "Query: " .$sql; 
		}
	}

	public function escape($clause) {
		
		return "'" . addslashes($clause) . "'";
	}

	# UPDATE QUERY
	public function update($table, $fields = [], $conditions = []) {
		
		$fields_string = "";
		$conditions_string = "";

		foreach ($fields as $field => $value) {
			$filtered_value = !in_array($value, $this->allowed_functions) && !is_numeric($value) ? $this->escape($value) : $value;
			$fields_string .= $field . "=" . $filtered_value . ","; 
		}
		foreach ($conditions as $field => $value) {
			$filtered_value = !is_numeric($value) ? $this->escape($value) : $value;
			$conditions_string .= $field . "=" . $filtered_value . " AND "; 
		}
		$fields_string = substr($fields_string, 0, strlen($fields_string) - 1);
		$conditions_string = substr($conditions_string, 0, strlen($conditions_string) - 4);

		$query = $this->query("UPDATE {$table} SET {$fields_string} WHERE {$conditions_string}", "update"); 
		
		return $query;
	}

	# INSERT QUERY
	public function insert($table, $fields = []) {
		
		$fields_string = "";

		foreach ($fields as $field => $value) {
			$filtered_value = !in_array($value, $this->allowed_functions) && !is_numeric($value) ? $this->escape($value) : $value;
			$fields_string .= $field . " = " . $filtered_value . ", ";
		}
		$fields_string = substr($fields_string, 0, strlen($fields_string) - 2);
		$query = $this->query("INSERT INTO {$table} SET {$fields_string}", "insert");

		return $query;
	}

	# DELETE QUERY
	public function delete($table, $conditions = []) {
		
		$conditions_string = "";

		foreach ($conditions as $field => $value) {
			$filtered_value = !is_numeric($value) ? $this->escape($value) : $value;
			$conditions_string .= $field . " = " . $filtered_value . " AND ";
		}
		$conditions_string = substr($conditions_string, 0, strlen($conditions_string) - 4);
		$query = $this->query("DELETE FROM {$table} WHERE {$conditions_string}", "update");

		return $query;
	}
}