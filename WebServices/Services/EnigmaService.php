<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EnigmaService
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceServices;

use WebServices\WebServiceConstant;

class EnigmaService {
    
    private $iv;
    private $key;
    private $s2_key;
    private $s4_key;
    
    public function __construct() {
        $this->_hash();
    }

    private function _hash($hash = '') {
        $this->$key      = ($hash) ? $hash : WebServiceConstant::WEBSERVICE_HASH_KEY;
        $this->$iv       = md5(md5(md5($this->$key)));
        $this->$s2_key   = md5(md5(md5($this->$key)));
        $this->$s4_key   = md5(md5(md5(md5($this->$iv))));
    }
    
    public function _decrypt($request, $key) {
        $this->_hash($key);
        $decompose = rtrim(
            mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->$s2_key, 
                base64_decode(base64_decode($request)), MCRYPT_MODE_CBC, 
                $this->$s4_key
            )
        );

        return $decompose;
    }
    
    public function _encrypt($request, $key) {
        $this->_hash($key);
        $compose = base64_encode(
            base64_encode(
                mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->$s2_key, 
                    $request, MCRYPT_MODE_CBC, 
                    $this->$s4_key
                )
            )
        );

        return $compose;
    }
}