<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MailService
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceServices;

use WebServices\WebServiceConstant;
use WebServices\WebServicesStatus;
use WebServices\WebServiceException;

class MailService {
    
    private $mail;

    public function __construct($mail) {
        
        $this->mail = $mail;

        $this->mail->isSMTP();
        $this->mail->SMTPDebug      = WebServiceConstant::WEBSERVICE_MAILER_DEBUG_TYPE; //2 for both client and server side response
        $this->mail->Debugoutput    = WebServiceConstant::WEBSERVICE_MAILER_DEBUG_OUTPUT;
        $this->mail->Host           = WebServiceConstant::WEBSERVICE_MAILER_HOST;
        $this->mail->Port           = WebServiceConstant::WEBSERVICE_MAILER_HOST_PORT;
        $this->mail->SMTPSecure     = WebServiceConstant::WEBSERVICE_MAILER_SECURITY;
        $this->mail->SMTPAuth       = WebServiceConstant::WEBSERVICE_MAILER_AUTH;
        $this->mail->Username       = WebServiceConstant::WEBSERVICE_MAILER_USERNAME;//sender's gmail address
        $this->mail->Password       = WebServiceConstant::WEBSERVICE_MAILER_PASSWORD;//sender's password
    }

    public function sendMail($sender, $receiver, $subject, $message) { 
        
        try {

            $this->mail->setFrom($sender['email'], $sender['displayName']);//sender's incormation
            // $this->mail->addReplyTo('myanotheremail@gmail.com', 'Barack Obama');//if alternative reply to address is being used
            $this->mail->addAddress($receiver['email'], $receiver['displayName']);//receiver's information
            $this->mail->Subject = $subject;//subject of the email
            $this->mail->msgHTML($message);
            
            $this->mail->AltBody = 'If you can\'t view the email, contact us';
            // $this->mail->addAttachment('images/logo.png');//some attachment
            $this->mail->send();

        } catch (WebServiceException $e) {
            
            $response['status']         = WebServicesStatus::WEBSERVICE_STATUS_ERROR;
            $response['error']          = $e->error;
            $response['error_message']  = $e->message;

            echo json_encode($response);
        }
    }
}