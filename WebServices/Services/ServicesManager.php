<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ServicesManager
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServiceServices;

use WebServices\WebServiceStatus;
use WebServices\WebServiceException;

use WebServices\WebServiceUtilities;

use WebServiceServices\EnigmaService;

use \PHPMailer;
use WebServiceServices\MailService;

class ServicesManager {

    public static function getService($service) {
        
        try {

            return $this->serviceList($service);

        } catch (WebServiceException $e) {

            WebServiceUtilities::setWebServiceResponse(
                500, 
                WebServiceStatus::WEBSERVICE_STATUS_ERROR, 
                null, 
                $e->error, 
                $e->message
            );

            return;
        }
    }

    private function serviceList($service) {

    	switch ($service) {
            
            case 'EnigmaService':
                
                include_once APPLICATION_PATH.'/WebServices/Services/EnigmaService.php';
                
                return new EnigmaService();
            
            case 'MailService':
                
                include_once APPLICATION_PATH.'/WebServices/Plugins/PHPMailer-master/PHPMailerAutoload.php';
                include_once APPLICATION_PATH.'/WebServices/Services/MailService.php';
                
                return new MailService(new PHPMailer());
                
            default:
                return;
        }
    }
}