<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebService
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServices;

use \Exception;

/**
 * About WebServices:
 *	- Holds some basics commands and code related to RESTful
 *	implementation or construction of API.
 *	
 *	WebService used IoC (Inversion of Control) and some direct DI (Dependency Injection).
 *
 *	WebService Main Classes List:
 *		WebService, WebServiceStatus, WebServiceConstant, WebServiceException, WebServiceChecker, WebServiceAccessManager, 
 *   	WebServiceDependencies, WebServiceManager, WebServiceSession, WebServiceUtilities.
 *
 */

class WebService {
	
}

class WebServiceStatus {

	# STATUS
	const WEBSERVICE_STATUS_SUCCESS  = 'SUCCESS';
	const WEBSERVICE_STATUS_ERROR 	 = 'ERROR';
	const WEBSERVICE_STATUS_FAILED 	 = 'FAILED';
	
	# ERROR
	const WEBSERVICE_INVALID_METHOD	 = 'INVALID METHOD USED';
	const WEBSERVICE_UNAUTHORIZED 	 = 'UNAUTHORIZED ACCESS';
	const WEBSERVICE_EMPTY_PARAM	 = 'EMPTY PARAMTER(s)';
	const WEBSERVICE_INVALID_PARAM	 = 'INVALID PARAMETER(s) USED';
	const WEBSERVICE_REQUEST_FAILED	 = 'REQUEST FAILED';
}

class WebServiceConstant {

	# VERSION
	const WEBSERVICE_VERSION 						= 'v3.0.1';

	# DATABASE
	const WEBSERVICE_DATABASE_PLUGIN 				= 'PDO'; // [MYSQL, MYSQLi, and PDO (PHP Data Object)]
	const WEBSERVICE_DATABASE_HOST					= WEBSERVICE_DB_HOSTNAME;
	const WEBSERVICE_DATABASE_USERNAME				= WEBSERVICE_DB_USERNAME;
	const WEBSERVICE_DATABASE_PASSWORD				= WEBSERVICE_DB_PASSWORD;
	const WEBSERVICE_DATABASE_NAME 					= WEBSERVICE_DB_NAME;

	# ACCESS
	const WEBSERVICE_HEADER_ACCESS_CONTROL_MAX_AGE 	= 86400;
	const WEBSERVICE_PUBLIC_ACCESS 					= 'PUBLIC';
	const WEBSERVICE_PRIVATE_ACCESS 				= 'PRIVATE';
	
	# SESSION
	const WEBSERVICE_SESSION_HTTPS 					= false; // if host uses HTTPS set true
	const WEBSERVICE_SESSION_JS_PROTECTION 			= true;

	# HASH SERVICE
	const WEBSERVICE_HASH_KEY						= 'bGhmS0VLaFMxL1MxNkVYNi83Q3JmdzhKTGF2SWRNaUhxcXNRUllpM3JuOD0=';

	# MAILER
	const WEBSERVICE_MAILER_AUTH 					= true;
	const WEBSERVICE_MAILER_HOST 					= 'smtp.gmail.com'; //multiple host is ';' seperated
	const WEBSERVICE_MAILER_HOST_PORT 				= 587;
	const WEBSERVICE_MAILER_USERNAME 				= 'mailer@gmail.com';
	const WEBSERVICE_MAILER_PASSWORD 				= 'password';
	const WEBSERVICE_MAILER_DEBUG_TYPE 				= 1; //2 = for both client and server side response, 1 = client side, 0 = none
	const WEBSERVICE_MAILER_DEBUG_OUTPUT 			= 'html';
	const WEBSERVICE_MAILER_SECURITY 				= 'tls'; //this is required for PHP 5.6 up
}

class WebServiceException extends Exception {

	/**
     *  Exception Error.
     *  @var string
     */
	public $error;

	/**
     *  Exception Message.
     *  @var string
     */
	public $message;

	public function __construct($error, $message) {
		$this->error = $error;
		$this->message = $message;
	}
}

class WebServiceChecker {

	/**
     * Validate Database Connection.
     * @return void
     */
	public static function validateDatabaseConnection($connection) {
		if (!$connection) {
			$message = 'No connection to the Database.';
			throw new WebServiceException(WebServiceStatus::WEBSERVICE_STATUS_ERROR, $message);
		}
	}

	/**
     * Validate Input Empty.
     * @return string
     */
	public static function validateEmptyInput($input, $field) {
		
		if (empty($input)) {
			$message = 'The input value is empty! parameter key is ['.$field.']';
			throw new WebServiceException(WebServiceStatus::WEBSERVICE_EMPTY_PARAM, $message);
		} else {
			return $input;
		}
	}

	/**
     * Validate Input Array.
     * @return array
     */
	public static function validateArrayInput($input, $field) {
		
		if (!is_array($input)) {
			$message = 'The input value is not a Array type! parameter key is ['.$field.']';
			throw new WebServiceException(WebServiceStatus::WEBSERVICE_EMPTY_PARAM, $message);
		} else {
			return $input;
		}
	}

	/**
     * Validate Input Integer.
     * @return integer
     */
	public static function validateNumericInput($input, $field) {
		
		if (!is_numeric($input)) {
			$message = 'The input value is not a Numeric type! parameter key is ['.$field.']';
			throw new WebServiceException(WebServiceStatus::WEBSERVICE_EMPTY_PARAM, $message);
		} else {
			return $input;
		}
	}

	/**
     * Validate Input String.
     * @return string
     */
	public static function validateStringInput($input, $field) {
		
		if (!is_string($input)) {
			$message = 'The input value is not a String type! parameter key is ['.$field.']';
			throw new WebServiceException(WebServiceStatus::WEBSERVICE_EMPTY_PARAM, $message);
		} else {
			return $input;
		}
    }

    /**
     * Validate Input Boolean.
     * @return boolean
     */
    public static function validateBooleanInput($input, $field) {
    	
    	if (!is_bool($input)) {
			$message = 'The input value is not a Boolean type! parameter key is ['.$field.']';
			throw new WebServiceException(WebServiceStatus::WEBSERVICE_EMPTY_PARAM, $message);
		} else {
			return $input;
		}
    }
}

include_once APPLICATION_PATH.'/WebServices/WebServiceAccessManager.php';
include_once APPLICATION_PATH.'/WebServices/WebServiceSession.php';
include_once APPLICATION_PATH.'/WebServices/WebServiceManager.php';
include_once APPLICATION_PATH.'/WebServices/WebServiceUtilities.php';
