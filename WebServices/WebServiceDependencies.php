<?php

/*
	WEBSERVICE DEPENDENCIES
		Use this script to add other dependencies
*/
define('APPLICATION_HOST', 'http://local.eformalistics2k16_devt_version:8800/');
define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/..'));

include_once APPLICATION_PATH.'/config/files_config.php';
include_once APPLICATION_PATH.'/config/database_url_config.php';

define('WEBSERVICE_DB_HOSTNAME', DB_HOSTNAME);
define('WEBSERVICE_DB_USERNAME', DB_USERNAME);
define('WEBSERVICE_DB_PASSWORD', DB_PASSWORD);
define('WEBSERVICE_DB_NAME', DB_NAME);

include_once APPLICATION_PATH.'/library/library.php';
include_once APPLICATION_PATH.'/library/functions.php';

include_once APPLICATION_PATH.'/library/Database.php';
include_once APPLICATION_PATH.'/library/DatabaseConnection.php';

include_once APPLICATION_PATH.'/library/MySQLDatabase.php';
include_once APPLICATION_PATH.'/library/MSSQLDatabase.php';
include_once APPLICATION_PATH.'/library/OracleDatabase.php';

include_once APPLICATION_PATH.'/library/Auth.php';



include_once APPLICATION_PATH.'/library/upload.php';
include_once APPLICATION_PATH.'/library/userQueries.php';
include_once APPLICATION_PATH.'/library/post.php';
include_once APPLICATION_PATH.'/library/Redis_Formalistics.php';
include_once APPLICATION_PATH.'/library/Formalistics.php';
include_once APPLICATION_PATH.'/library/Person.php';


//PowerForms EMail
include_once APPLICATION_PATH.'/library/PHPMailer.php';
include_once APPLICATION_PATH.'/library/Mail_Notification.php';


// include_once APPLICATION_PATH.'/config/config.php';