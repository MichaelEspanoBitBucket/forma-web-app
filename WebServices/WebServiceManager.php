<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceManager
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServices;

use WebServiceDatabases\WebServiceDatabaseManager;
use WebServiceServices\ServicesManager;
use WebServiceFacade\WebServiceFacade;

class WebServiceManager {

    /**
     * Return Database Instance.
     * @return class instance
     */
    public static function webServiceDatabase() {
        return WebServiceDatabaseManager::getDatabase();
    }

    /**
     * Return Services Instance.
     * @return class instance
     */
    public static function webServiceServices($service) {
        return ServicesManager::getService($service);   
    }

    /**
     * Return Application Instance.
     * @return class instance
     */
    public static function webServiceApplication($module) {
        
        $db = WebServiceManager::webServiceDatabase();
        
        $database = $db->getConnection('MYSQL', array());
        $utilities = WebServiceManager::webServiceUtilities();

        return WebServiceFacade::searchApplication($module, $database, $utilities);
    }
}

include_once APPLICATION_PATH.'/WebServices/Databases/WebServiceDatabaseManager.php';
include_once APPLICATION_PATH.'/WebServices/Services/ServicesManager.php';
include_once APPLICATION_PATH.'/WebServices/Applications/FormaCore/WebServiceFacade.php';