<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceSession
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServices;

use WebServices\WebServiceConstant;

class WebServiceSession {

	/**
     * Set Session.
     * @return void
     */
	public static function setSession($key, $value) {
		WebServiceSession::startSession();
		$_SESSION[$key] = $value;
	}

	/**
     * Get Session.
     * @return array
     */
	public static function getSession($key) {
		WebServiceSession::startSession();
		return $_SESSION[$key];
	}

	/**
     * Start Session.
     * @return void
     */
	public static function startSession() {
		empty($_SESSION) ? WebServiceSession::sessionConfiguration() : false;
	}

	/**
     * Clear Session.
     * @return void
     */
	public static function clearSession() {
		session_unset();
		session_destroy();
	}

	/**
     * Session Configuration Dynamically.
     * @return void
     */
	private static function sessionConfiguration() {

		ini_set('session.use_trans_sid', 0);					  // Tell the PHP not to include the session identifier in URL
		ini_set('session.use_only_cookies', 1); 				  // Forces sessions to only use cookies. 
		ini_set('session.entropy_file', '/dev/urandom'); 	  	  // better session id's
		ini_set('session.entropy_length', '512'); 				  // and going overkill with entropy length for maximum security

		$cookieParameters = session_get_cookie_params(); 		  // Gets current cookies params.
		session_set_cookie_params(
			$cookieParameters["lifetime"], 
			$cookieParameters["path"], 
			$cookieParameters["domain"], 
			WebServiceConstant::WEBSERVICE_SESSION_HTTPS, 		  // Set to true if using https else leave as false
			WebServiceConstant::WEBSERVICE_SESSION_JS_PROTECTION  // This stops javascript being able to access the session id 
		);
		session_start();
	}
}