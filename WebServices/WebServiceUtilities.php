<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WebServiceUtilities
 *
 * @author Joshua Clifford Reyes
 */

namespace WebServices;

use WebServices\WebServiceConstant;
use WebServices\WebServiceStatus;
use WebServices\WebServiceException;
use WebServices\WebServiceChecker;

class WebServiceUtilities {

    /**
     * Set Configuration for HTTP-Header.
     * @return void
     */
    public static function setWebServiceHeader() {

        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header('Access-Control-Allow-Origin: '.$_SERVER['HTTP_ORIGIN']);
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: '.WebServiceConstant::WEBSERVICE_HEADER_ACCESS_CONTROL_MAX_AGE);
        }

        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
            (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) ? header("Access-Control-Allow-Methods: GET, POST, OPTIONS") : false;
            (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) ? header('Access-Control-Allow-Headers: '.$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']) : false;
            exit(0);
        }

        header('Content-Type: application/json');
    }

    /**
     * Validate Method to Use.
     * @return json
     */
    public static function setWebServiceMethod($method) {

        $response = WebServiceUtilities::getDefaultResponses();

        switch ($method) {
            case 'POST':
                if ($_SERVER['REQUEST_METHOD'] != $method) {
                    $response['error'] = WebServiceStatus::WEBSERVICE_INVALID_METHOD;
                    $response['error_message'] = 'Use POST method to access the webservice.';
                }
                break;
            case 'GET':
                if ($_SERVER['REQUEST_METHOD'] != $method) {
                    $response['error'] = WebServiceStatus::WEBSERVICE_INVALID_METHOD;
                    $response['error_message'] = 'Use GET method to access the webservice.'; 
                }
                break;
            default:
                break;
        }

        if ($response['error']) {
            $response['status'] = WebServiceStatus::WEBSERVICE_STATUS_ERROR;

            echo json_encode($response);
            exit(0);   
        }
    }

    /**
     * Apply RESTful Structure for API
     * @param $status - status of the request
     * @param $result - result data of request
     * @param $error - error type
     * @param $errorMessage - error message
     *
     * @return json
     */
    public static function setWebServiceResponse($httpCode, $status, $result, $error, $errorMessage) {

        header("HTTP/1.1 $httpCode");

        $response = array(
            'status'             => $status,
            'result'             => $result,
            'error'              => $error,
            'error_message'      => $errorMessage,
            'server_datetime'    => WebServiceUtilities::getWebServiceServerTime(),
            'webservice_version' => WebServiceUtilities::getWebServiceVersion(),
        );

        echo json_encode($response);
    }

    /**
     * Response Defaults.
     * @return array
     */
    public static function getDefaultResponses() {
        
        return array(
            'status'             => '',
            'result'             => null,
            'error'              => '',
            'error_message'      => '',
            'server_datetime'    => WebServiceUtilities::getWebServiceServerTime(),
            'webservice_version' => WebServiceUtilities::getWebServiceVersion(),
        );
    }

    /**
     * Webservice Server Time.
     *  Server timezone is set to Asia/Manila
     *
     * @return date
     */
    public static function getWebServiceServerTime() {

        date_default_timezone_set("Asia/Manila");
        
        return date('Y-m-d H:i:s');
    }

    /**
     * WebService Version.
     * @return string
     */
    public static function getWebServiceVersion() {
        return WebServiceConstant::WEBSERVICE_VERSION;
    }

    /**
     * Current Browser Used.
     * @return string
     */
    public static function getWebBrowserNameUsed() {
        
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/'))         return 'Opera';
        elseif (strpos($user_agent, 'Edge'))                                     return 'Edge';
        elseif (strpos($user_agent, 'Chrome'))                                   return 'Chrome';
        elseif (strpos($user_agent, 'Safari'))                                   return 'Safari';
        elseif (strpos($user_agent, 'Firefox'))                                  return 'Firefox';
        elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
        
        return 'Other';
    }

    /**
     * Remove Special Characters listed in the Search.
     * @return string
     */
    public static function sanitizeData($data) {

        $search   = array('\\', "\0", "\n", "\r", "'", '"', "\x1a", '`', '#', ';', '-');
        $replace  = array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z', '', '', '', '');

        return str_replace($search, $replace, $data);
    }

    /**
     * Remove Special Characters and Validate Empty Input.
     * @return string
     */
    public static function sanitizeWebServiceParameter($value, $subject) {
        return WebServiceUtilities::sanitizeData(WebServiceChecker::validateEmptyInput($value, $subject));
    }
}