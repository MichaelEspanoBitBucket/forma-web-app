<?php

$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
if (Auth::hasAuth('current_user')) {
    $company_id = $auth['company_id'];
} else {
    $company_id = COMPANY_ID;
}

$str_sql = "SELECT * FROM tb_app_configuration app
    LEFT JOIN tbcompany c ON app.app_company_id=c.id
    WHERE app.app_company_id={$db->escape($company_id)}";

$get_app = $db->query($str_sql, "row");

$get_app_count = $db->query($str_sql, "numrows");

// For Validation
// $fs->config_validation(array( "name"            =>  $json_app_options['formbuilder_form_events'],
//                                         "default_value"   =>  1,
//                                         "value_to_compare"=>  1,
//                                         "else_value"      =>  0))
//print_r($get_app);
// if($get_app_count == "0"){
//     include_once('config/default_config.php');
// }else{

$app = $get_app;
//foreach($get_app as $app){

$json_decode = json_decode($app['app_module'], true);
$json_app_options = json_decode($app['app_options'], true);
$json_app_active_directory = json_decode($app['app_active_directory'], true);
$json_app_login = json_decode($app['app_login'], true);
$json_app_smtp = json_decode($app['app_smtp'], true);
$json_app_information = json_decode($app['app_information'], true);
$json_app_url = json_decode($app['app_url'], true);
$json_app_days = json_decode($app['app_days'], true);
$json_app_login_page_ui = json_decode($app['app_login_page_ui'], true);
$json_guest = json_decode($app['app_guest'], true);

$Person = new Person($db, $json_guest['guest_email']);

// For K

$k = $get_app['app_key'];
$com_k = $get_app['l_code'];
if ($k == "") {
    define("GO_LITE", 0);
} else if ($com_k == "") {
    define("GO_LITE", 1);
} else {
    if ($k == $com_k) {
        define("GO_LITE", 1);
    } else {
        define("GO_LITE", 0);
    }
}


/* GUEST INFORMATION */

define("GUEST_NAME", $Person->first_name . " " . $Person->last_name);
define("GUEST_ID", $json_guest['guest_email']);
define("GUEST_EMAIL", $Person->email);

/* EXTERNAL IMAGES  */
/* ====================================================== */

define("DIR_EXTERNAL_IMG", $json_app_url['image_link']);

/* EXTERNAL CROSS BROWSING  */
/* ====================================================== */

define("DIR_EXTERNAL_CACHE_FILE", "images/cache/");

/* DEFINE PAGE  */
/* ====================================================== */

define('NOT_ALLOWED_LEFTBAR', serialize(array('application_portal', 'workspace', 'formbuilder', 'workflow', 'organizational_chart', 'report', 'app_configuration', 'organizational-chart', 'generate', 'youre-not-allowed-to-visit-this-page', 'gi-report-designer', 'erp')));
define('ALLOWED_FOLDER', serialize(array('application_portal', 'workspace', 'gi-report-designer')));
define('NOT_ALLOWED_CHAT', serialize(array('messages', 'application_portal', 'workspace', 'formbuilder', 'workflow', 'organizational_chart', 'gi-report-designer')));
define('USER_VIEW', "/user_view/");
define('ALLOW_USER_VIEW', "1");

/* MOBILE */
/* ====================================================== */

define("ENABLE_MOBILE_HOME", ($json_app_options['mobile_home'] == "1" ? 1 : 0)); //ADDED BY: michael esp. 11:39 AM 10/2/2015

/* WORKSPACE ENABLEMENT  */
/* ====================================================== */

define('ALLOW_PROCESSOR_ATTACHMENT', $json_app_options['processor_attachment']);


/* NAVIGATION  */
/* ====================================================== */

// Per Company Modules Configuration
// Default Condition
//($json_decode[''] == "" ? 1 : 0) || ($json_decode[''] == "1" ? 1 : 0)
// For L Version 
// || ((in_array($json_decode['create_forms'], $lite)) ? 1 : 0) ,

$lite = unserialize(LITE_VERSION);

define("ENABLE_COMPANY_MOD", serialize(array("dashboard" => ($json_decode['dashboard'] == "" ? 1 : 0) || ($json_decode['dashboard'] == "1" ? 1 : 0),
    "navigation" => ($json_decode['navigation'] == "" ? 1 : 0) || ($json_decode['navigation'] == "1" ? 1 : 0),
    "organizational_charts" => array("create_organizational_chart" => ($json_decode['create_organizational_chart'] == "" ? 1 : 0) || ($json_decode['create_organizational_chart'] == 1 ? 1 : 0),
        "list_organizational_chart" => ($json_decode['list_organizational_chart'] == "" ? 1 : 0) || ($json_decode['list_organizational_chart'] == 1 ? 1 : 0)
    ),
    "forms" => array("create_forms" => ($json_decode['create_forms'] == "" ? 1 : 0) || ($json_decode['create_forms'] == "1" ? 1 : 0),
        "list_forms" => ($json_decode['list_forms'] == "" ? 1 : 0) || ($json_decode['list_forms'] == "1" ? 1 : 0)
    ),
    "workflows" => array("create_workflow" => ($json_decode['create_workflow'] == "" ? 1 : 0) || ($json_decode['create_workflow'] == "1" ? 1 : 0),
        "list_workflow" => ($json_decode['list_workflow'] == "" ? 1 : 0) || ($json_decode['list_workflow'] == "1" ? 1 : 0)
    ),
    "news_and_discussions" => ($json_decode['news_and_discussions'] == "" ? 1 : 0) || ($json_decode['news_and_discussions'] == "1" ? 1 : 0),
    "reports" => array("create_reports" => ($json_decode['create_reports'] == "" ? 1 : 0) || ($json_decode['create_reports'] == "1" ? 1 : 0),
        "list_reports" => ($json_decode['list_reports'] == "" ? 1 : 0) || ($json_decode['list_reports'] == "1" ? 1 : 0)
    ),
    "gs3_insights" => array(
        "gi-report-designer" => ($json_decode['gi-report-designer'] == "" ? 1 : 0) || ($json_decode['gi-report-designer'] == "1" ? 1 : 0),
        "gi-report-designer-list" => ($json_decode['gi-report-designer-list'] == "" ? 1 : 0) || ($json_decode['gi-report-designer-list'] == "1" ? 1 : 0),
    ),
    "print_form" => ($json_decode['print_form'] == "" ? 1 : 0) || ($json_decode['print_form'] == "1" ? 1 : 0),
    "my-requests" => ($json_decode['my-requests'] == "" ? 1 : 0) || ($json_decode['my-requests'] == "1" ? 1 : 0),
    "suggestion_box" => ($json_decode['suggestion_box'] == "" ? 1 : 0) || ($json_decode['suggestion_box'] == "1" ? 1 : 0),
    // "report-list"            =>      ($json_decode['report-list'] == "" ? 1 : 0) || ($json_decode['report-list'] == "1" ? 1 : 0), 
    "trash_bin" => ($json_decode['trash_bin'] == "" ? 1 : 0) || ($json_decode['trash_bin'] == "1" ? 1 : 0),
    "administer" => array(
        "app_configuration" => ($json_decode['app_configuration'] == "" ? 1 : 0) || ($json_decode['app_configuration'] == "1" ? 1 : 0),
        "delegation" => ($json_decode['delegation'] == "" ? 1 : 0) || ($json_decode['delegation'] == "1" ? 1 : 0),
        "external_database" => ($json_decode['external_database'] == "" ? 1 : 0) || ($json_decode['external_database'] == "1" ? 1 : 0),
        "form_category" => ($json_decode['form_category'] == "" ? 1 : 0) || ($json_decode['form_category'] == "1" ? 1 : 0),
        "group_settings" => ($json_decode['group_settings'] == "" ? 1 : 0) || ($json_decode['group_settings'] == "1" ? 1 : 0),
        "link_maintenance" => ($json_decode['link_maintenance'] == "" ? 1 : 0) || ($json_decode['link_maintenance'] == "1" ? 1 : 0),
        "nav_settings" => ($json_decode['nav_settings'] == "" ? 1 : 0) || ($json_decode['nav_settings'] == "1" ? 1 : 0),
        "position_settings" => ($json_decode['position_settings'] == "" ? 1 : 0) || ($json_decode['position_settings'] == "1" ? 1 : 0),
        "rule_settings" => ($json_decode['rule_settings'] == "" ? 1 : 0) || ($json_decode['rule_settings'] == "1" ? 1 : 0),
        "suggestion_box_list" => ($json_decode['suggestion_box_list'] == "" ? 1 : 0) || ($json_decode['suggestion_box_list'] == "1" ? 1 : 0),
        // "system_logs"       =>  ($json_decode['system_logs'] == "" ? 1 : 0) || ($json_decode['system_logs'] == "1" ? 1 : 0),
        // "forma_chat"       =>  ($json_decode['forma_chat'] == "" ? 1 : 0) || ($json_decode['forma_chat'] == "1" ? 1 : 0),
        "user_settings" => ($json_decode['user_settings'] == "" ? 1 : 0) || ($json_decode['user_settings'] == "1" ? 1 : 0)
    ),
    "others" => array("link" => ($json_decode['link'] == "" ? 1 : 0) || ($json_decode['link'] == "1" ? 1 : 0),
        "starred" => ($json_decode['starred'] == "" ? 1 : 0) || ($json_decode['starred'] == "1" ? 1 : 0),
        "group" => ($json_decode['group'] == "" ? 1 : 0) || ($json_decode['group'] == "1" ? 1 : 0),
        "faq" => ($json_decode['faq'] == "" ? 1 : 0) || ($json_decode['faq'] == "1" ? 1 : 0),
        "message-app" => ($json_decode['message-app'] == "" ? 1 : 0) || ($json_decode['message-app'] == "1" ? 1 : 0),
        "notification" => ($json_decode['notification'] == "" ? 1 : 0) || ($json_decode['notification'] == "1" ? 1 : 0),
        "edit_account" => ($json_decode['edit_account'] == "" ? 1 : 0) || ($json_decode['edit_account'] == "1" ? 1 : 0),
        //"privacy_settings"=>  ($json_decode[''] == "" ? 1 : 0) || ($json_decode[''] == "1" ? 1 : 0),
        "user_org_chart" => ($json_decode['user_org_chart'] == "" ? 1 : 0) || ($json_decode['user_org_chart'] == "1" ? 1 : 0),
        "take_tour" => ($json_decode['take_tour'] == "" ? 1 : 0) || ($json_decode['take_tour'] == "1" ? 1 : 0),
        "need_guide" => ($json_decode['need_guide'] == "" ? 1 : 0) || ($json_decode['need_guide'] == "1" ? 1 : 0),
        "app_theme" => ($json_decode['app_theme'] == "" ? 1 : 0) || ($json_decode['app_theme'] == "1" ? 1 : 0),
    // "online_users"    =>  ($json_decode['online_users'] == "" ? 1 : 0) || ($json_decode['online_users'] == "1" ? 1 : 0)
    )
                )
        )
);

$currentMod = unserialize(ENABLE_COMPANY_MOD);

define("PARENT_MODULE_PROPERTIES", serialize(array("organizational_charts_admin" => array("properties" => array("icon" => "#svg-icon-org-chart", "view_box" => "0 0 100.264 100.597",
            "user" => "admin",
            "visibility" => ($currentMod['organizational_charts']['create_organizational_chart'] == 0 && $currentMod['organizational_charts']['list_organizational_chart'] == 0 ? true : false ))),
    "forms_admin" => array("properties" => array("icon" => "#svg-icon-forms", "view_box" => "0 0 100.264 100.597",
            "user" => "admin",
            "visibility" => ($currentMod['forms']['create_forms'] == 0 && $currentMod['forms']['list_forms'] == 0 ? true : false ))),
    "workflows_admin" => array("properties" => array("icon" => "#svg-icon-workflow", "view_box" => "0 0 100.264 100.597",
            "user" => "admin",
            "visibility" => ($currentMod['workflows']['create_workflow'] == 0 && $currentMod['workflows']['list_workflow'] == 0 ? true : false ))),
    "reports_admin" => array("properties" => array("icon" => "#svg-icon-report", "view_box" => "0 0 100.264 100.597",
            "user" => "admin",
            "visibility" => ($currentMod['reports']['create_reports'] == 0 && $currentMod['reports']['list_reports'] == 0 ? true : false ))),
    "reports_ordinary" => array("properties" => array("icon" => "#svg-icon-report", "view_box" => "0 0 100.264 100.597",
            "user" => "ordinary",
            "visibility" => ($currentMod['reports']['list_reports'] == 0 ? true : false ))),
    "gs3_insights_admin" => array("properties" => array("icon" => "#svg-icon-gs3-insights", "view_box" => "0 0 100.264 100.597",
            "user" => "admin",
            "visibility" => ($currentMod['gs3_insights']['gi-report-designer'] == 0 && $currentMod['gs3_insights']['gi-report-designer-list'] == 0 ? true : false ))),
    "administer_admin" => array("properties" => array("icon" => "#svg-icon-administrator", "view_box" => "0 0 100.264 100.597",
            "user" => "admin",
            "visibility" => ""))
                )
        )
);
/* MemcacheD Server IP */

define("MEMCACHED_SERVER_IP", $json_app_options['redis_server'] ? : "localhost");

/* Formbuilder Events supports: onload, presave, postsave, onchange(field events) */
/* ====================================================== */

define("ENABLE_FORMBUILDER_FORM_EVENTS", ($json_app_options['formbuilder_form_events'] == "1" ? 1 : 0)); //ADDED BY: michael esp. 11:39 AM 10/2/2015

/* Delegation */
/* ====================================================== */

define("ENABLE_DELEGATION", $json_decode['Delegation'] ? : 1);
define("ENABLE_EVENTS", $json_decode['form_events']);

/* Deletion */
/* ====================================================== */

define("ENABLE_DELETION", $json_app_options['record_deletion'] ? : 0);

/* ENABLE RULES FOR MIDDLEWARE  */
/* ====================================================== */

define("ENABLE_RULES", $json_app_options['enable_rules'] ? : 0);

/* ACTIVE DIRECTORY IP  */
/* ====================================================== */

define("AD_IP_ADDRESS", $json_app_active_directory['ad_ip'] ? : "192.168.0.63");
define("DC", $json_app_active_directory['ad_dc'] ? : "DC=gs3,DC=com,DC=ph");
define("ALLOW_ACTIVE_DIRECTORY", ($json_app_active_directory['allow_active_directory'] == "1" ? 1 : 0));

/* GUEST  */
/* ====================================================== */

define("ALLOW_GUEST", ($json_app_options['enable_guest'] == "1" ? 1 : 0));

define("ALLOW_REQUEST_FORM_GUEST", $json_app_options['enable_form_request_guest'] ? : 1);

/* GUEST URL MAIN SITE  */
//define("GUEST_MAIN_PAGE","http://guest.gs3.com.ph:85/");
define("GUEST_MAIN_PAGE", $json_app_url['guest_main'] ? : "http://guest.gs3.com.ph/");
define("GUEST_MAIN_PAGE_1", $json_app_url['guest_admin_main'] ? : "http://eforms.gs3.com.ph/");
define("GUEST_URL", $json_app_url['guest_main'] ? : "http://guest.gs3.com.ph/"); // Login Self

/* CURRENT COMPANY  */
/* ====================================================== */

define('COMPANY_NAME', $app['name'] ? : 'Global Strategic Solution & Services Inc.');
define('COMPANY_ID', $app['id'] ? : COMPANY_ID);

/* SMTP FROM DEFAULT  */
/* ====================================================== */

define('SMTP_USERNAME', $json_app_smtp['username'] ? : 'gs3.aspdevelopment@gmail.com');
define('SMTP_PASSWORD', $json_app_smtp['password'] ? : '@dministr@t0r');
define('SMTP_PORT', $json_app_smtp['port'] ? : 465);
define('SMTP_HOST', $json_app_smtp['host'] ? : 'smtp.gmail.com');
define('SMTP_AUTH', $json_app_smtp['auth'] ? : true);
define('SMTP_SECURE', $json_app_smtp['secure'] ? : 'ssl');
define('SMTP_FROM_NAME', $json_app_smtp['from_name'] ? : 'Formalistics Web Application');
define('SMTP_FROM_EMAIL', $json_app_smtp['fom_mail'] ? : 'formalistics@gmail.com');

/* PAGE CONFIGURATION  */
/* ====================================================== */

define("PAGE_KEYWORD", $json_app_information['page_keywords'] ? : "Your Page Keywords");
define("PAGE_DESC", $json_app_information['page_description'] ? : "Your Page Description Sam");
define("PAGE_TITLE", $json_app_information['page_title'] ? : "Your Page Title");
define("PAGE_AUTHOR", $json_app_information['page_author'] ? : "Your Page Author");

$set_workspace_control = $json_app_options['set_workspace_control'] ? : "default-dark"; // coalesce if null
define('WORKSPACE_CONTROL', $set_workspace_control);

/* FOR LOGIN PAGE DESIGN  */
/* ====================================================== */

define('HAS_THEME', '1'? : '0');
define('HEADER_BG_COLOR', $json_app_login_page_ui['header_background_color'] ? : '#1f1f1f');
define('HEADER_BTN_COLOR', $json_app_login_page_ui['header_button_color'] ? : '#7f8c8d');
define('FOOTER_FONT_COLOR', $json_app_login_page_ui['footer_font_color'] ? : '#A5A5A5');
define('HEADER_LABEL_COLOR', $json_app_login_page_ui['header_label_color'] ? : '#A5A5A5');
define('FOOTER_BG_COLOR', $json_app_login_page_ui['footer_background_color'] ? : '#1f1f1f');
define('HEADER_LOGO_PATH', $json_app_login_page_ui['header_logo_path'] ? : 'css/images/formalistics_logo.png');
define('BODY_WALLPAPER_PATH', $json_app_login_page_ui['body_wallpaper_path'] ? : 'css/images/login_default_wallpaper.jpg');
define('HEADER_BORDER_COLOR', $json_app_login_page_ui['header_border_color'] ? : '#1A1919');
define('BODY_WALLPAPER_HEIGHT', $json_app_login_page_ui['body_wallpaper_height'] ? : "628");


/* FOR DELEGATION  */
/* ====================================================== */

define("DAYS_PENDING", $json_app_days['days_pending'] ? : 3);

/* COMMENT SETTINGS  */
/* ====================================================== */

/**
 * If soft deletion is available, then comments are not deleted right away
 * in the database. Comments will simply be set as inactive and will be
 * deleted by a separate scheduled script.
 */
define("ENABLE_SOFT_DELETE_COMMENTS", ($json_app_options['soft_delete'] == "1" ? 1 : 0));
define("REMOVE_SOFT_DELETED_AFTER_DAYS", $json_app_options['soft_delete_days'] ? : 100);

/* SMS SETTINGS  */
/* ====================================================== */

define("SMS_SERVER", $json_app_url['sms_server'] ? : "tcp://127.0.0.1:3014");

/* PUSH NOTIFICATION / SOCKET SETTINGS  */
/* (SPECIFY 0 FOR INACTIVE AND 1 FOR ACTIVE)  */
/* ====================================================== */
define("CHAT_ACTIVE", $json_app_options['enable_chat'] ? : 0);
define("SOCKET_ACTIVE", $json_app_options['enable_socket'] ? : 0);
define("SOCKET_SERVER", $json_app_options['socket_server'] ? : "http://apps.gs3.com.ph:3013");
define("DOCLOCK_ACTIVE", $json_app_options['enable_document_locking'] ? : 0);
//define("SOCKET_SERVER", "http://116.50.245.157:3013");

/* LOGO  */
/* ====================================================== */

define("COMPANY_STYLES", $json_app_login['login_folder_list']);

/* ANNOTATION  */
/* ====================================================== */

define("ALLOW_FORM_ANNOTATION", $json_app_options['form_annotation'] ? : 0);

/* Hiding application configuration  */
/* ====================================================== */

define("APP_CONFIGURATION_SETUP", $app['hide'] ? : 0);

/* DEFAULT LANDING PAGE FOR ALL USER (IF ADMIN SET IT) */
/* ====================================================== */

$decode_app_page = json_decode($get_app['app_page'], true);
if ($decode_app_page['page'] == "others") {
    $page = $decode_app_page['others'];
} else {
    $page = $decode_app_page['page'];
}

define("APP_OTHERS_LANDING_PAGE", $decode_app_page['page'] ? : '');
define("APP_LANDING", $decode_app_page['page'] ? : 'home');
define("APP_LANDING_PAGE", $page ? : 'home');

//slidemenu added by pao
define("SLIDEMENU", $json_app_options['formbuilder_slide_menu'] ? : 0);

/* MAINTENANCE ON / OFF  */
/* ====================================================== */

define("ENABLE_FORGOT_PASSWORD_URL", ($json_app_options['enable_fp'] == "1" ? 1 : 0));

define("FORGOT_PASSWORD_URL", ($json_app_options['forgot_password_url'] == "#" ? "#" : $json_app_options['forgot_password_url']));

define("ALLOW_EMAIL_PASSWORD", ($json_app_options['enable_password_email'] == "1" ? 1 : 0));

/* MEMCACHED ON / OFF  */

define("ENABLE_MEMCACHED", $json_app_options['enable_redis'] ? : "0");

/* SYSTEM LOGS ON/OFF */

// define("SYSTEM_LOGS", $json_app_options['system_logs'] ?: "1");

/* FORMA CHAT ON/OFF */

define("FORMACHAT", $json_app_options['formalistics_chat'] ? : 0);

//added by japhet morada field themes

define("FIELD_THEMES", $json_app_options['field_themes'] ? : 0);


// }
// }else{
              //ISSUE NI KATH
//     include_once('config/default_config.php');
// }
?>