<?php

/* DATABASE CONFIGURATION  */
/* ====================================================== */    
    define('DB_HOSTNAME', 'localhost');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', '');
    define('DB_NAME', 'gs3_local');//gs3_eforms(local) //gs3_uppc //gs3_staclara
    
	define('LOAD_DATA_INFILE_PATH', 'C:\wamp\bin\mysql\mysql5.6.17\data\gs3_local'); //mysql data path
	define('LOAD_DATA_INFILE_OS_USED', 'windows'); //windows //linux
/* URL MAIN SITE  */
/* ====================================================== */
    
    define("MAIN_PAGE","http://michael.eformalistics.com.ph:83/");
	// define("MAIN_PAGE","http://192.168.0.103:83/");


/* COOKIE URL  */
/* ====================================================== */

define("COOKIE_URL", ".192.168.0.12:8800"); 

/* SEND EMAIL FOR LICENSE KEY (Owner of the Product (GS3) not the client)   */
/* ====================================================== */

$email = "samuel282@gmail.com";
$name = "Administrator";

define("L_KEY_TO", serialize(array($email => $name)));
?>