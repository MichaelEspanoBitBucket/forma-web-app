<?php

function getUserIP()
{
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}
function getServerAddress(){
    return $_SERVER['SERVER_ADDR'];
}

/* FOLDER CONFIGURATION  */
/* ====================================================== */
    
    define('DIR_CSS', 'css/');
    define('DIR_IMG', '/images/');
    define('DIR_ICON', DIR_IMG . '/icon/');
    define('DIR_BUTTON', DIR_IMG . '/button/');
    define('DIR_THEMES', DIR_IMG . '/themes/');
    
    define('DIR_JS', '/js/');
    define('DIR_JSLib', DIR_JS . '/library/');
    
    define('DIR_LAYOUT', '/layout/');
    define('DIR_LIBRARY', '/library/');
    define('DIR_MODULES', '/modules/');

/* SIZES ALLOWED  */
/* ====================================================== */

    define("FORM_IMG_SIZE",2097152); // 2MB
    define("REQUEST_MULTIPLE_FILE_ATTACHMENT",8388608); // 2MB
    define("REQUEST_SINGLE_FILE_ATTACHMENT",8388608); // 2MB

/* LOGO  */
/* ====================================================== */
    define("COMPANY_STYLES", ""); // 

/* SET COMPANY ID  */
/* ====================================================== */ 
    define("COMPANY_ID", "1");

/* DEFAULT WORKSPACE CONTROL  */
/* ====================================================== */

/* ====================================================== */
//define("WORKSPACE_CONTROL", 'default'); // 
    
/* UPLOADING (FILES / IMAGES ALLOWED EXTENSIONS)  */
/* ====================================================== */

    define('FILES_EXTENSION', serialize(array("jpg", "png", "JPG", "PNG", "GIF", "gif", "txt", "zip", "rar", "docx", "doc", "xls", "xlsx", "pdf", "csv", "apk","ppt","pptx")));
    define('IMG_EXTENSION', serialize(array("jpg", "png", "JPG", "PNG", "GIF", "gif")));
        
/* ADMIN RIGHTS  */
/* ====================================================== */
    
    define('ALLOW_ADMIN_LEFTBAR',"1");
    define("ALLOW_APPLICATION_PORTAL","1");
    define('ALLOW_HELP',"1");
    define('ALLOW_USER_ADMIN','1');
    


/* For Notification List of tables */
/* ====================================================== */  
    define('TBL_TASKS', 'tbtasks');
    define('TBL_LIKE','tblike');
    define('TBL_POST','tbpost');
    define('TBL_COMMENT','tbcomment');
  
/* SOFTWARE INFORMATION  */
/* ====================================================== */  
  
    define("SOFTWARE_VERSION", "updated");
    
/* MAINTENANCE ON / OFF  */
/* ====================================================== */  
  
    define("MAINTENANCE", "0");
    
/* COMPANY REGISTRATION */
/* ====================================================== */  
    define("ALLOW_COMPANY_REGISTRATION","1");
    define("LIMIT_USERS","1");
    define("NUM_LIMIT","10");
    define("CURRENT_COMPANY_EMAIL","samuel282@gmail.com");



/* Added by Aaron Tolentino 02/12/2015 */

/* MY REQUEST CUSTOM VIEW ON / OFF  */
/* ====================================================== */  
  
    define("MY_REQUEST_CUSTOM_VIEW", "0");

/* Debug  tool on request */
/* ====================================================== */  
    define("REQUEST_QUERY_DEBUGGER", "0");

    
/* Other details */
/* ===================================================== */
define("OTHER_FORM_DETAILS", "1");

/* Added by Japhet Morada */
/* ===================================================== */
define("ALLOW_EXTERNAL_LINKS", "1");
define("ALLOW_DEFAULT_ACTIONS", "1");
    
    /* Added by Aaron Tolentino 02/18/2015 */
    
/* For Staging Process */
/* ====================================================== */  
    define("ENABLE_STAGING", "1");

    /*Added by Carlo Medina 02/26/2015*/

    /*For Details Panel*/
/*=====================================================*/
    define("ENABLE_DETAILS_PANEL", "0");
    /*FOR IDE and Context-Menu*/
    /*===================================================*/
    define("ENABLE_IDE","1");
    define("ENABLE_CONTEXT_MENU","0");
    define("ENABLE_INLINE_ADD_EMBED","1");
    /*03/04/2015*/
    /*For accordion and landing page*/
/*===================================================*/
    define("ENABLE_ACCORDION", "1");
    define("ENABLE_LANDING_PAGE", "1");



    // Auto logged for request URL using formbuilder
    define("AUTO_LOGGED","1");

/* Added by Aaron Tolentino 03/06/2015 */
    
/* Delegation */
/* ====================================================== */  
    define("ENABLE_DELEGATION", "1");


//FOR BACKGROUND PROCESSING added by jewel tolentino 11/26/2015
define("PHP_BIN_PATH", 'C:\\wamp\\bin\\php\\php5.5.12\\');

/* SET DEFAULT THEME COLOR */
define('DEFAULT_THEME_COLOR_LIGHT', '#7f8c8d');
define('DEFAULT_THEME_COLOR_DARK', '#667273');


define('WORKSPACE_CONTROLS_AVAILABILITY', '0');
    
/* NAVIGATION LINKS URL  */
/* ====================================================== */
    // Dont add manually, please contact your support web developer, (PS7)
        // this features is connected to the app configuration if the selected modules enabled / not, those available links will be available 
        // from the selection.
    // param @relation        = Parameters Value (parent or child)
    // param @url             = current links of the formalistics
    // param @name            = Name of the page / HTML tags
    // param @root_user_view  = Parameters Value (1 or 0) 
                                    // if the links requires (/user_view/ or note).
    // @type                  = Parameters Value (click, single and toggle)
                                    // (Click-has javascript,Toggle-has sub menu,Single-has url);
    // @load_type             = Parameters Value (navigation and landing)
                                    // (navigation-for sidebar purposes or others,Landing Page - for the selection of landing page per user);
    // @class                 = (Add class name / selector you want)
    // @navigation            = Parameters Value (parent_icon and icon , or add other)

    // Admin URL LINKS
    define("GET_COMPANY_URL_LINKS",
            serialize(array(    "dashboard"                          =>  array("relation"=>"parent","url"=>"home","name"=> "Dashboard","root_user_view"=>"1","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-dashboard","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                "navigation"                            =>  array("relation"=>"parent","url"=>"","name"=> '<div align="center"><img src="/images/icon/loading.gif"><p style="font-size:10px;">Loading Content...</p></div>',"root_user_view"=>"1","navigation"=>array("parent_icon"=>"#svg-icon-modules","icon"=>"#svg-icon-modules", "view_box"=>"0 0 100.264 100.597"),"class"=>"loading_navigation","type"=>"click","load_type"=>"navigation"),
                                "create_organizational_chart"     =>  array("relation"=>"child","url"=>"organizational_chart","name"=>"Create Organizational Chart","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-org-chart","icon"=>"#svg-icon-create-orgchart","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "list_organizational_chart"       =>  array("relation"=>"child","url"=>"organizational_chart_list","name"=>"Organizational Chart List","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-org-chart","icon"=>"#svg-icon-list-orgchart", "view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "create_forms"                    =>  array("relation"=>"child","url"=>"formbuilder?formID=0","name"=>"Create Form","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-forms","icon"=>"#svg-icon-forms","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "list_forms"                      =>  array("relation"=>"child","url"=>"form_list","name"=>"Form List","root_user_view"=>"0","icon"=>"#svg-icon-list-form","navigation"=>array("parent_icon"=>"#svg-icon-forms","icon"=>"#svg-icon-list-form","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "create_workflow"                 =>  array("relation"=>"child","url"=>"workflow","name"=>"Create Workflow","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-create-workflow","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "list_workflow"                   =>  array("relation"=>"child","url"=>"workflow_list","name"=>"Worflow List","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-list-workflow","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "news_and_discussions"            =>  array("relation"=>"parent","url"=>"portal","name"=>"News and Discussions","root_user_view"=>"1","navigation"=>array("parent_icon"=>"#svg-icon-portal","icon"=>"#svg-icon-portal","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                "create_reports"                  =>  array("relation"=>"child","url"=>"report","name"=>"Create Report","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-create-report","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "list_reports"                    =>  array("relation"=>"child","url"=>"report_list","name"=>"Report List","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-list-report","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "gi-report-designer"              =>  array("relation"=>"child","url"=>"Insights/gi-report-designer","name"=>"Create Reports","root_user_view"=>"1","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-gs3-insights","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "gi-report-designer-list"         =>  array("relation"=>"child","url"=>"Insights/gi-report-designer-list","name"=>"Report List","root_user_view"=>"1","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-gs3-insights","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "print_form"                      =>  array("relation"=>"parent","url"=>"print_form_list","name"=>"Print Form","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-print-form","icon"=>"#svg-icon-print-form","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                "my_request"                      =>  array("relation"=>"parent","url"=>"my-requests","name"=>"My Requests","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-my-request","icon"=>"#svg-icon-my-request","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                            
                                "delegation"                      =>  array("relation"=>"child","url"=>"delegate","name"=>"Delegation","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-delegation","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                "form_category"                   =>  array("relation"=>"child","url"=>"form_category","name"=>"Form Category","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-form-category","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "group_settings"                  =>  array("relation"=>"child","url"=>"group_settings","name"=>"Group Settings","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-group-settings","view_box"=>"0 0 100.264 60.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "link_maintenance"                =>  array("relation"=>"child","url"=>"link_maintenance","name"=>"Link Maintenance","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-link-maintenance","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "my-requests"                     =>  array("relation"=>"child","url"=>"my-requests","name"=>"My Requests","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-my-request","icon"=>"#svg-icon-my-request","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                "position_settings"               =>  array("relation"=>"child","url"=>"position_settings","name"=>"Position Settings","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-position-settings","view_box"=>"0 0 100.264 60.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "rule_settings"                   =>  array("relation"=>"child","url"=>"rule_settings","name"=>"Rule Settings","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-rule-settings","icon"=>"#svg-icon-rule-settings","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "suggestion_box"                  =>  array("relation"=>"child","url"=>"","name"=>"Suggestion Box","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-suggestion","icon"=>"#svg-icon-suggestion","view_box"=>"0 0 100.264 100.597"),"class"=>"suggestionBox","type"=>"click","load_type"=>"navigation"),
                                "suggestion_box_list"             =>  array("relation"=>"child","url"=>"suggestions","name"=>"Suggestion List Box","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-suggestions","view_box"=>"0 0 100.264 100.597"),"class"=>"suggestionBox","type"=>"single","load_type"=>"navigation"),
                                "trash_bin"                       =>  array("relation"=>"parent","url"=>"trash-bin","name"=>"Trash Bin","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-trash-bin","icon"=>"#svg-icon-portal","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                "user_settings"                   =>  array("relation"=>"child","url"=>"user_settings","name"=>"User Settings","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-user-settings","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "nav_settings"                    =>  array("relation"=>"child","url"=>"nav_settings","name"=>"Navigation Settings","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-navigation-settings","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "app_configuration"               =>  array("relation"=>"child","url"=>"app_configuration","name"=>"Application Configuration","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-app-config","icon"=>"#svg-icon-app-config","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "external_database"               =>  array("relation"=>"child","url"=>"external_database","name"=>"External Database","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-app-config","icon"=>"#svg-icon-database","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                // "system_logs"                     =>  array("relation"=>"child","url"=>"system_logs","name"=>"System Logs","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-app-config","icon"=>"#svg-icon-app-config","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                // "forma_chat"                     =>  array("relation"=>"child","url"=>"forma_chat","name"=>"Forma Chat","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-app-config","icon"=>"#svg-icon-app-config","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                                "starred"                         =>  array("relation"=>"parent","url"=>"starred","name"=>"Starred","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                                "group"                           =>  array("relation"=>"parent","url"=>"groups","name"=>"Group","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                                "faq"                             =>  array("relation"=>"parent","url"=>"faq","name"=>"Frequently Ask Question","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                                "message-app"                     =>  array("relation"=>"parent","url"=>"message-app","name"=>"Messages","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>""),"class"=>"","type"=>"click","load_type"=>"landing"),
                                "notification"                    =>  array("relation"=>"parent","url"=>"notifications","name"=>"Notifications","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                                "edit_account"                    =>  array("relation"=>"parent","url"=>"account-settings","name"=>"Edit Account","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                                
                                "user_org_chart"                  =>  array("relation"=>"parent","url"=>"organizational-chart","name"=>"User Organizational Chart View","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                                "take_tour"                       =>  array("relation"=>"parent","url"=>"tour","name"=>"Take a Tour","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing")
                                )
                                
                                
                         )        
    ); 
    // Ordinary User Links
    define("GET_COMPANY_URL_LINKS_ORDINARY",
           serialize(array( 
                            "dashboard"                          =>  array("relation"=>"parent","url"=>"home","name"=> "Dashboard","root_user_view"=>"1","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-dashboard","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                            "news_and_discussions"            =>  array("relation"=>"parent","url"=>"portal","name"=>"News and Discussions","root_user_view"=>"1","navigation"=>array("parent_icon"=>"#svg-icon-portal","icon"=>"#svg-icon-portal","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                            // "reports"                    =>  array("relation"=>"parent","url"=>"reports","name"=>"Report List","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-list-report","icon"=>"#svg-icon-list-report","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                            // "list_reports"                    =>  array("relation"=>"child","url"=>"report_list","name"=>"Report List","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-list-report","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"toggle","load_type"=>"landing"),
                               // "create_reports"                  =>  array("relation"=>"child","url"=>"report","name"=>"Create Report","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-create-report","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                  "list_reports"                    =>  array("relation"=>"parent","url"=>"reports","name"=>"Report List","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-list-report","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),  
                            "my-requests"                     =>  array("relation"=>"child","url"=>"my-requests","name"=>"My Requests","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-my-request","icon"=>"#svg-icon-my-request","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                
                            "starred"                         =>  array("relation"=>"parent","url"=>"starred","name"=>"Starred","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "group"                           =>  array("relation"=>"parent","url"=>"groups","name"=>"Group","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "message-app"                     =>  array("relation"=>"parent","url"=>"message-app","name"=>"Messages","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "notification"                    =>  array("relation"=>"parent","url"=>"notifications","name"=>"Notifications","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "edit_account"                    =>  array("relation"=>"parent","url"=>"account-settings","name"=>"Edit Account","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "suggestion_box"                  =>  array("relation"=>"child","url"=>"","name"=>"Suggestion Box","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-suggestion","icon"=>"#svg-icon-suggestion","view_box"=>"0 0 100.264 100.597"),"class"=>"suggestionBox","type"=>"click","load_type"=>"navigation"),
                            "trash_bin"                       =>  array("relation"=>"parent","url"=>"trash-bin","name"=>"Trash Bin","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-trash-bin","icon"=>"#svg-icon-portal","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                            // "print_form"                      =>  array("relation"=>"parent","url"=>"print_form_list","name"=>"Print Form","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-print-form","icon"=>"#svg-icon-print-form"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                 
                            "user_org_chart"                  =>  array("relation"=>"parent","url"=>"organizational-chart","name"=>"User Organizational Chart View","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "take_tour"                       =>  array("relation"=>"parent","url"=>"tour","name"=>"Take a Tour","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing")
                            )  
                                
                         )
           );
    // Ordinary User Links
    define("GET_COMPANY_URL_LINKS_GUEST",
           serialize(array( "dashboard"                          =>  array("relation"=>"parent","url"=>"home","name"=> "Dashboard","root_user_view"=>"1","navigation"=>array("parent_icon"=>"#svg-icon-dashboard","icon"=>"#svg-icon-dashboard","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                            "news_and_discussions"            =>  array("relation"=>"parent","url"=>"portal","name"=>"News and Discussions","root_user_view"=>"1","navigation"=>array("parent_icon"=>"#svg-icon-portal","icon"=>"#svg-icon-portal","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                            "my-requests"                     =>  array("relation"=>"child","url"=>"my-requests","name"=>"My Requests","root_user_view"=>"0","navigation"=>array("parent_icon"=>"#svg-icon-my-request","icon"=>"#svg-icon-my-request","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"single","load_type"=>"landing"),
                                
                            "starred"                         =>  array("relation"=>"parent","url"=>"starred","name"=>"Starred","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "group"                           =>  array("relation"=>"parent","url"=>"groups","name"=>"Group","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "message-app"                     =>  array("relation"=>"parent","url"=>"message-app","name"=>"Messages","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "notification"                    =>  array("relation"=>"parent","url"=>"notifications","name"=>"Notifications","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "edit_account"                    =>  array("relation"=>"parent","url"=>"account-settings","name"=>"Edit Account","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            
                            "user_org_chart"                  =>  array("relation"=>"parent","url"=>"organizational-chart","name"=>"User Organizational Chart View","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing"),
                            "take_tour"                       =>  array("relation"=>"parent","url"=>"tour","name"=>"Take a Tour","root_user_view"=>"0","navigation"=>array("parent_icon"=>"","icon"=>"","view_box"=>"0 0 100.264 100.597"),"class"=>"","type"=>"click","load_type"=>"landing")
                            )
                                
                                
                         )
           );

    define("LITE_VERSION",
           serialize(array("group","external_database","message-app","link_maintenance","delegation","gs3_insights","gi-report-designer","gi-report-designer-list")));

        // Ordinary User Links
    define("WORKSPACE_AVAILABLE_REDIRECTION",
        serialize(array(
            "Account Settings"              => "account-settings",
            "Dashboard"                     =>      "home",
            "Messages"                      =>  "message-app",
            "My Requests"                   =>      "my-requests",
            "Notifications"                 =>  "notifications",
            "Organizational Chart"          =>  "organizational-chart",
            "Portal"                        => "portal",
            "Report List"                   =>  "report",
            "Current Record"                   =>  "current-record"
        ))
    );

    define("SHOW_ENCRYPTED_PASSWORD","0");

    /* ERP Sidebar */
/* ====================================================== */  
    // define("ERP_MODULES_VISIBILITY", "1");

    /*VELCO FORMS*/
    define("velco_safety_program_frmid", 359);
    define("velco_safety_program_tbl", "1_tbl_safetyprogram");
    define("design_safety_program", serialize(array(
        "top"=>100,
        "left"=>0,
        "frequency_column"=>array("Annually","As arise needed","Continous","Daily","Frequency","Monthly","Once"),
        "resources_needed_column"=>array("accident report","alarm system","bfp navotas","c.member & schedule","certificate","certification fee","checklist","company policy","control plan","dole receiving","drawing","drill","fee","hmo","JD & Committee Members","Leaflets","Leaflets / Safety Policy","Medicines / first aids","posters","memo","resources needed","safety officer","safety program","SP-2015","spill kit","tarpaulin","frame","TC Consultant","training fee","violation ticket"),
        "status_column"=>array("As arise needed","as schedule","complied","continous","continuous implementation","for approval","maintain","on-going","propose","status")
    )));
    define("velco_list_legal_so_frmid", 360);
    define("velco_list_legal_so_tbl", "1_tbl_listoflegalandotherrequirements");
    define("design_list_legal_so", serialize(array(
        "top"=>100,
        "left"=>0,
        "frequency_column"=>array("after training completion","anually","annually and periodically","anually","as need arises","daily","every 6 months","every 20th of the month","every 3 years","if death or permanent disability occured","monthly","once","periodically","yearly"),
        "resources_needed_column"=>array(""),
        "status_column"=>array("As schedule","complied","continuous implementation","for additional","for approval","for claiming","for schedule","history of compliance","not applicable","on-going","propose","still valid")
        
    )));
  
    define("REQUEST_RECORD_MEMCACHED",serialize(array("request_list","picklist","request_list_count","calendar_view")));
    define("BROKEN_IMAGE","'/images/avatar/small.png'");
    define("BROKEN_IMAGE_POST","/css/images/error/broken_image-01.png");

    //form data table added by japhet morada 05-06-2016
    define("ENABLE_FORM_DATATABLE", "0");

    // For Login Captcha
    define("ALLOW_LOGIN_CAPTCHA","1");
?>