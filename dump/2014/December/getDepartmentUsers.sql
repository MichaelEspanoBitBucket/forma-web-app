DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getDepartmentUsers`(department_code VARCHAR(255), position_level INT, company_id INT) RETURNS LONGTEXT CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value LONGTEXT;

	IF position_level = 0 THEN
		SET return_value = (SELECT 
									group_concat(DISTINCT dep_users.user_id)
								FROM 
								tbdepartment_users dep_users
								LEFT JOIN tbuser user
								ON user.id = dep_users.user_id
								WHERE dep_users.department_code = department_code
								AND dep_users.is_active=1 AND user.company_id = company_id
								AND EXISTS(
									SELECT * FROM tborgchart org WHERE org.id = dep_users.orgchart_Id 
									AND org.Status = '1'
									)
							);
	ELSE
		SET return_value = (SELECT 
									group_concat(DISTINCT dep_users.user_id)
								FROM 
								tbdepartment_users dep_users
								LEFT JOIN tbuser user
								ON user.id = dep_users.user_id
								WHERE dep_users.department_position = position_level 
								AND dep_users.department_code = department_code
								AND dep_users.is_active=1 AND user.company_id = company_id
								AND EXISTS(
									SELECT * FROM tborgchart org WHERE org.id = dep_users.orgchart_Id 
									AND org.Status = '1'
									)
							);
	END IF;
	RETURN return_value;
END$$
DELIMITER ;
