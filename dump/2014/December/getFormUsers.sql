DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getFormUsers`(form_id INT, action_type INT) RETURNS LONGTEXT CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value LONGTEXT;

	IF action_type = 0 THEN
		SET return_value = (SELECT GROUP_CONCAT(DISTINCT Viewers) AS Request_Viewers FROM (SELECT 
				CASE formusers.user_type
					WHEN 1 THEN (SELECT getUsersByPosition(formusers.user))
					WHEN 2 THEN (SELECT getDepartmentUsers(formusers.user, 0, ws.company_id))
					WHEN 4 THEN (SELECT getGroupUsers(formusers.user))
					ELSE formusers.user
			END AS Viewers
			FROM tbform_users formusers
			LEFT JOIN tb_workspace ws
			ON ws.id = formusers.form_id
			WHERE formusers.form_id = form_id) A
		);
	ELSE
		SET return_value = (SELECT GROUP_CONCAT(DISTINCT Viewers) AS Request_Viewers FROM (SELECT 
				CASE formusers.user_type
					WHEN 1 THEN (SELECT getUsersByPosition(formusers.user))
					WHEN 2 THEN (SELECT getDepartmentUsers(formusers.user, 0, ws.company_id))
					WHEN 4 THEN (SELECT getGroupUsers(formusers.user))
					ELSE formusers.user
			END AS Viewers
			FROM tbform_users formusers
			LEFT JOIN tb_workspace ws
			ON ws.id = formusers.form_id
			WHERE formusers.form_id = form_id
			AND formusers.action_type = action_type) A
		);
	END IF;
	RETURN return_value;
END$$
DELIMITER ;
