DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getGroupUsers`(groupId INT) RETURNS LONGTEXT CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value VARCHAR(255);
	
	SET return_value = (
		SELECT 
			group_concat(user_id) 
		FROM tbform_groups_users
		WHERE group_id = groupId
	);

	
	RETURN return_value;
END$$
DELIMITER ;
