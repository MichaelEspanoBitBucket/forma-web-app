DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getUsersDepartment`(user_id INT, company_id INT) RETURNS LONGTEXT CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value LONGTEXT;

	SET return_value = (SELECT 
            GROUP_CONCAT(DISTINCT org_obj.department)
        FROM
            tborgchartobjects org_obj
        WHERE
            FIND_IN_SET(user_id,
                    getDepartmentUsers(org_obj.department_code,
                            0,
                            company_id)));

	RETURN return_value;
END$$
DELIMITER ;
