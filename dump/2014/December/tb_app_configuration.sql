-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 04, 2014 at 09:27 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_app_configuration`
--

CREATE TABLE IF NOT EXISTS `tb_app_configuration` (
  `app_id` int(255) NOT NULL AUTO_INCREMENT,
  `app_user_id` int(255) NOT NULL,
  `app_json` longtext NOT NULL,
  `app_module` longtext NOT NULL,
  `app_options` longtext NOT NULL,
  `app_active_directory` longtext NOT NULL,
  `app_adding_options` longtext NOT NULL,
  `app_login` longtext NOT NULL,
  `app_url` longtext NOT NULL,
  `app_information` longtext NOT NULL,
  `app_smtp` longtext NOT NULL,
  `app_type` varchar(100) NOT NULL,
  `app_days` longtext NOT NULL,
  `app_config_type` varchar(100) NOT NULL,
  `app_date` datetime NOT NULL,
  `app_date_modified` datetime NOT NULL,
  `app_company_id` int(255) NOT NULL,
  `app_is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`app_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
