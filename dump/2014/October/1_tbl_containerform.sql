-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 23, 2014 at 05:47 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms_consolidated`
--

-- --------------------------------------------------------

--
-- Table structure for table `1_tbl_containerform`
--

CREATE TABLE IF NOT EXISTS `1_tbl_containerform` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TrackNo` longtext,
  `Requestor` longtext,
  `Status` longtext,
  `Processor` longtext,
  `ProcessorType` longtext,
  `ProcessorLevel` longtext,
  `LastAction` longtext,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` longtext,
  `UpdatedBy` longtext,
  `Unread` longtext,
  `Node_ID` longtext,
  `Workflow_ID` longtext,
  `fieldEnabled` longtext,
  `fieldRequired` longtext,
  `fieldHiddenValues` longtext,
  `imported` longtext,
  `Repeater_Data` longtext,
  `Editor` longtext,
  `Viewer` longtext,
  `middleware_process` longtext,
  `SaveFormula` longtext,
  `CancelFormula` longtext,
  `customer` longtext,
  `width` longtext,
  `length` longtext,
  `description` longtext,
  `height` longtext,
  `referencenumber` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `1_tbl_containerform`
--

INSERT INTO `1_tbl_containerform` (`ID`, `TrackNo`, `Requestor`, `Status`, `Processor`, `ProcessorType`, `ProcessorLevel`, `LastAction`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`, `Unread`, `Node_ID`, `Workflow_ID`, `fieldEnabled`, `fieldRequired`, `fieldHiddenValues`, `imported`, `Repeater_Data`, `Editor`, `Viewer`, `middleware_process`, `SaveFormula`, `CancelFormula`, `customer`, `width`, `length`, `description`, `height`, `referencenumber`) VALUES
(1, 'C0001', '40', 'Active', '40', '4', NULL, '{"Deactivate":{"button_name":"Deactivate","child_id":"node_4","parent_id":"node_3","require_comment":false,"wokflow_button_line_color":"#424242","message":"Are you sure you want to proceed?","message_success":"Request has been deactivated"}}', '2014-10-23 10:12:52', '2014-10-23 13:25:48', '40', '40', '', 'Save', '1248', '["customer","width","length","description","height","referencenumber"]', '', '', '', '[]', NULL, NULL, '0', '', '', 'Jollibee', '1000', '900', 'Jollibee Container ng Burger', '100', 'CF1414013140000'),
(2, 'C0002', '40', 'Active', '40', '4', NULL, '{"Deactivate":{"button_name":"Deactivate","child_id":"node_4","parent_id":"node_3","require_comment":false,"wokflow_button_line_color":"#424242","message":"Are you sure you want to proceed?","message_success":"Request has been deactivated"}}', '2014-10-23 10:13:37', '2014-10-23 10:13:37', '40', '40', '', 'node_3', '1248', '["customer","width","length","description","height","referencenumber"]', '[]', '[]', '', '[]', NULL, NULL, '0', '', '', 'Mcdo', '1000', '800', 'Mcdo Container ng Fries', '100', 'CF1414001581000'),
(3, 'C0003', '40', 'Active', '40', '4', NULL, '{"Deactivate":{"button_name":"Deactivate","child_id":"node_4","parent_id":"node_3","require_comment":false,"wokflow_button_line_color":"#424242","message":"Are you sure you want to proceed?","message_success":"Request has been deactivated"}}', '2014-10-23 10:15:03', '2014-10-23 10:15:03', '40', '40', '', 'node_3', '1248', '["customer","width","length","description","height","referencenumber"]', '[]', '[]', '', '[]', NULL, NULL, '0', '', '', 'Mang Inasal', '1000', '700', 'Mang Inasal container ng Mano', '100', 'CF1414001673000'),
(4, 'C0004', '40', 'Active', '40', '4', NULL, '{"Deactivate":{"button_name":"Deactivate","child_id":"node_4","parent_id":"node_3","require_comment":false,"wokflow_button_line_color":"#424242","message":"Are you sure you want to proceed?","message_success":"Request has been deactivated"}}', '2014-10-23 13:33:51', '2014-10-23 13:33:51', '40', '40', '', 'node_3', '1248', '["customer","width","length","description","height","referencenumber"]', '[]', '[]', '', '[]', NULL, NULL, '0', '', '', 'KFC', '1000', '710', 'KFC Container ng Manok', '100', 'CF1414013605000');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
