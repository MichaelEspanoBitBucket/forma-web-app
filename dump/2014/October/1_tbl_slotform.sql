-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 23, 2014 at 05:47 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms_consolidated`
--

-- --------------------------------------------------------

--
-- Table structure for table `1_tbl_slotform`
--

CREATE TABLE IF NOT EXISTS `1_tbl_slotform` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TrackNo` longtext,
  `Requestor` longtext,
  `Status` longtext,
  `Processor` longtext,
  `ProcessorType` longtext,
  `ProcessorLevel` longtext,
  `LastAction` longtext,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` longtext,
  `UpdatedBy` longtext,
  `Unread` longtext,
  `Node_ID` longtext,
  `Workflow_ID` longtext,
  `fieldEnabled` longtext,
  `fieldRequired` longtext,
  `fieldHiddenValues` longtext,
  `imported` longtext,
  `Repeater_Data` longtext,
  `Editor` longtext,
  `Viewer` longtext,
  `middleware_process` longtext,
  `SaveFormula` longtext,
  `CancelFormula` longtext,
  `customer` longtext,
  `width` longtext,
  `length` longtext,
  `height` longtext,
  `referencenumber` longtext,
  `description` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `1_tbl_slotform`
--

INSERT INTO `1_tbl_slotform` (`ID`, `TrackNo`, `Requestor`, `Status`, `Processor`, `ProcessorType`, `ProcessorLevel`, `LastAction`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`, `Unread`, `Node_ID`, `Workflow_ID`, `fieldEnabled`, `fieldRequired`, `fieldHiddenValues`, `imported`, `Repeater_Data`, `Editor`, `Viewer`, `middleware_process`, `SaveFormula`, `CancelFormula`, `customer`, `width`, `length`, `height`, `referencenumber`, `description`) VALUES
(1, 'S0001', '40', 'Active', '40', '4', NULL, '{"Deactivate":{"button_name":"Deactivate","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242","message":"Are you sure you want to proceed?","message_success":"Request has been submitted."}}', '2014-10-23 10:15:35', '2014-10-23 10:15:35', '40', '40', '', 'node_2', '1249', '["customer","width","length","height"]', '[]', '[]', '', '[]', NULL, NULL, '0', '', '', '', '1000', '950', '150', 'SF1414001722000', 'slot 1'),
(2, 'S0002', '40', 'Active', '40', '4', NULL, '{"Deactivate":{"button_name":"Deactivate","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242","message":"Are you sure you want to proceed?","message_success":"Request has been submitted."}}', '2014-10-23 10:15:53', '2014-10-23 10:15:53', '40', '40', '', 'node_2', '1249', '["customer","width","length","height"]', '[]', '[]', '', '[]', NULL, NULL, '0', '', '', '', '1000', '850', '150', 'SF1414001743000', 'slot 2'),
(3, 'S0003', '40', 'Active', '40', '4', NULL, '{"Deactivate":{"button_name":"Deactivate","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242","message":"Are you sure you want to proceed?","message_success":"Request has been submitted."}}', '2014-10-23 10:16:12', '2014-10-23 10:16:12', '40', '40', '', 'node_2', '1249', '["customer","width","length","height"]', '[]', '[]', '', '[]', NULL, NULL, '0', '', '', '', '1000', '750', '150', 'SF1414001762000', 'slot 3');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
