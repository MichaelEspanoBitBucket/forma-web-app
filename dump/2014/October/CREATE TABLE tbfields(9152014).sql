CREATE TABLE IF NOT EXISTS `tbfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(255) NOT NULL,
  `form_id` int(11) NOT NULL,
  `field_type` varchar(255) NOT NULL,
  `field_input_type` varchar(255) NOT NULL,
  `formula_type` varchar(255) NOT NULL,
  `formula` longtext NOT NULL,
  `visibility_formula` longtext NOT NULL,
  `data_field_label` TEXT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;