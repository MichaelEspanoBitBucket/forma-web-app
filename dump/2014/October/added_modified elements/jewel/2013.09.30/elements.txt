/js/functions/report/report_list.js - (initialize and performs generation of report on client-side)
/layout/leftbar.html - (added report link for the users to be able to create/generate/view reports)
/modules/rep_list/view/index.phtml - (view layout where users may edit or generate report)
/modules/ajax/controller/generateReport.php - (generates report base on specific parameters (formId, reportId, keyword, export option))
/modules/ajax/controller/getReportProperties.php - (get the properties of the selected report)
/modules/ajax/view/generateReport.phtml - (view layout for report generation)
/modules/ajax/view/getReportProperties.phtml - (view layout for report properties)





