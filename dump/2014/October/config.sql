CREATE TABLE IF NOT EXISTS `tbbasic_config` (
  `DIR_EXTERNAL_IMG` varchar(255) NOT NULL,
  `DIR_EXTERNAL_CACHE_FILE` varchar(255) NOT NULL,
  `NOT_ALLOWED_LEFTBAR` text NOT NULL,
  `ALLOWED_FOLDER` text NOT NULL,
  `NOT_ALLOWED_CHAT` text NOT NULL,
  `USER_VIEW` tinyint(1) NOT NULL,
  `ALLOW_HELP` tinyint(1) NOT NULL,
  `ALLOW_ADMIN_LEFTBAR` tinyint(1) NOT NULL,
  `ALLOW_USER_ADMIN` tinyint(1) NOT NULL,
  `ALLOW_APPLICATION_PORTAL` tinyint(1) NOT NULL,
  `ALLOW_PROCESSOR_ATTACHMENT` tinyint(1) NOT NULL,
  `MAIN_PAGE` varchar(255) NOT NULL,
  `COOKIE_URL` varchar(255) NOT NULL,
  `ENABLE_SELECTED_NAVIGATION` text NOT NULL,
  `ENABLE_MODULES` text NOT NULL,
  `FILES_EXTENSION` text NOT NULL,
  `IMG_EXTENSION` text NOT NULL,
  `ENABLE_RULES` tinyint(1) NOT NULL,
  `ALLOW_GUEST` tinyint(1) NOT NULL,
  `GUEST_MAIN_PAGE` varchar(255) NOT NULL,
  `GUEST_URL` varchar(255) NOT NULL,
  `COMPANY_NAME` varchar(255) NOT NULL,
  `DAYS_PENDING` int(11) NOT NULL,
  `ENABLE_SOFT_DELETE_COMMENTS` tinyint(1) NOT NULL,
  `REMOVE_SOFT_DELETED_AFTER_DAYS` int(11) NOT NULL,
  `COMPANY_LOGO` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_updated` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbexternalserver_config`
--

CREATE TABLE IF NOT EXISTS `tbexternalserver_config` (
  `CHAT_ACTIVE` tinyint(1) NOT NULL,
  `SOCKET_ACTIVE` tinyint(1) NOT NULL,
  `SOCKET_SERVER` varchar(255) NOT NULL,
  `AD_IP_ADDRESS` varchar(255) NOT NULL,
  `DC` varchar(255) NOT NULL,
  `ALLOW_ACTIVE_DIRECTORY` tinyint(1) NOT NULL,
  `date_created` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_updated` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbmessaging_config`
--

CREATE TABLE IF NOT EXISTS `tbmessaging_config` (
  `SMS_SERVER` varchar(255) NOT NULL,
  `SMTP_USERNAME` varchar(255) NOT NULL,
  `SMTP_PASSWORD` varchar(255) NOT NULL,
  `SMTP_PORT` varchar(255) NOT NULL,
  `SMTP_HOST` varchar(255) NOT NULL,
  `SMTP_AUTH` varchar(255) NOT NULL,
  `SMTP_SECURE` varchar(255) NOT NULL,
  `SMTP_FROM_NAME` varchar(255) NOT NULL,
  `SMTP_FROM_EMAIL` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_updated` datetime NOT NULL,
  `updated_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbmodules_config`
--

CREATE TABLE IF NOT EXISTS `tbmodules_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `ENABLE_COMPANY_MOD` text NOT NULL,
  `date_created` int(11) NOT NULL,
  `created_by` datetime NOT NULL,
  `date_updated` int(11) NOT NULL,
  `updated_by` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;