SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `tb_threads`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_threads` ;

CREATE TABLE IF NOT EXISTS `tb_threads` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date_posted` DATETIME NOT NULL,
  `is_active` BIT NOT NULL DEFAULT 0,
  `date_updated` DATETIME NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_thread_subscribers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_thread_subscribers` ;

CREATE TABLE IF NOT EXISTS `tb_thread_subscribers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `thread_id` INT NOT NULL,
  `type` VARCHAR(8) NOT NULL DEFAULT 'reader' COMMENT 'Possible Values:' /* comment truncated */ /* - author- the one who started the thread
 - reader - may only read messages from thread
 - writer- can write new messages*/,
  `is_active` BIT NOT NULL DEFAULT 1,
  `date_updated` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_thread_subscribers_tb_threads1_idx` (`thread_id` ASC),
  CONSTRAINT `fk_tb_thread_subscribers_tb_threads1`
    FOREIGN KEY (`thread_id`)
    REFERENCES `tb_threads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_thread_messages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_thread_messages` ;

CREATE TABLE IF NOT EXISTS `tb_thread_messages` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `message` VARCHAR(255) NOT NULL,
  `thread_id` INT NOT NULL,
  `author_id` INT NOT NULL,
  `is_active` BIT NOT NULL DEFAULT 1,
  `date_posted` DATETIME NOT NULL,
  `date_updated` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_thread_messages_tb_thread1_idx` (`thread_id` ASC),
  CONSTRAINT `fk_tb_thread_messages_tb_thread1`
    FOREIGN KEY (`thread_id`)
    REFERENCES `tb_threads` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_deleted_records`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_deleted_records` ;

CREATE TABLE IF NOT EXISTS `tb_deleted_records` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date_deleted` DATETIME NOT NULL,
  `table_name` VARCHAR(32) NOT NULL,
  `record_id` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_attachments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_attachments` ;

CREATE TABLE IF NOT EXISTS `tb_attachments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `thread_message_id` INT NOT NULL,
  `attachment_path` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`, `thread_message_id`),
  INDEX `fk_tb_attachments_tb_thread_messages1_idx` (`thread_message_id` ASC),
  CONSTRAINT `fk_tb_attachments_tb_thread_messages1`
    FOREIGN KEY (`thread_message_id`)
    REFERENCES `tb_thread_messages` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_thread_subscriber_messages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_thread_subscriber_messages` ;

CREATE TABLE IF NOT EXISTS `tb_thread_subscriber_messages` (
  `message_subscriber_user_id` INT NOT NULL,
  `thread_message_id` INT NOT NULL,
  `read` BIT NOT NULL DEFAULT 0,
  `is_active` BIT NOT NULL DEFAULT 1,
  `date_updated` DATETIME NOT NULL,
  INDEX `fk_tb_thread_subscribers_has_tb_thread_messages_tb_thread_m_idx` (`thread_message_id` ASC),
  CONSTRAINT `fk_tb_thread_subscribers_has_tb_thread_messages_tb_thread_mes1`
    FOREIGN KEY (`thread_message_id`)
    REFERENCES `tb_thread_messages` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
