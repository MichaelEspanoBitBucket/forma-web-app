DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getAnnouncementUsers`(announcement_id INT, company_id INT) RETURNS varchar(255) CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value VARCHAR(255);
	
	SET return_value = (SELECT GROUP_CONCAT(Viewers) FROM (SELECT
	CASE tbauser.user_type 
		WHEN 2 THEN (SELECT getDepartmentUsers(tbauser.user, 0, company_id))
		WHEN 3 THEN (SELECT getUsersByPosition(tbauser.user))
	END AS Viewers
	FROM tb_announcement_user tbauser 
	WHERE tbauser.announcement_id = announcement_id) A
	);

	RETURN return_value;
END$$
DELIMITER ;
