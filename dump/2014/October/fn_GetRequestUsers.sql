DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getRequestUsers`(formId INT, requestId INT, action_type INT) RETURNS varchar(255) CHARSET latin1 DETERMINISTIC
BEGIN
	DECLARE return_value VARCHAR(255);

	IF action_type = 0 THEN
		SET return_value =  (SELECT GROUP_CONCAT(DISTINCT Users) AS Request_Users FROM (SELECT 
							CASE requestUser.User_Type
									WHEN 1 THEN (SELECT getUsersByPosition(requestUser.user))
									WHEN 2 THEN (SELECT getDepartmentUsers(requestUser.user, 0, ws.company_id))
									ELSE requestUser.user
							END AS Users
						FROM tbrequest_users requestUser
						LEFT JOIN tb_workspace ws
						ON ws.ID = requestUser.Form_ID
						WHERE requestUser.Form_ID = formId
						AND requestUser.RequestID = requestId) A
					);
	ELSE
		SET return_value =  (SELECT GROUP_CONCAT(DISTINCT Users) AS Request_Users FROM (SELECT 
							CASE requestUser.User_Type
									WHEN 1 THEN (SELECT getUsersByPosition(requestUser.user))
									WHEN 2 THEN (SELECT getDepartmentUsers(requestUser.user, 0, ws.company_id))
									ELSE requestUser.user
							END AS Users
						FROM tbrequest_users requestUser
						LEFT JOIN tb_workspace ws
						ON ws.ID = requestUser.Form_ID
						WHERE requestUser.Form_ID = formId
						AND requestUser.RequestID = requestId
						AND requestUser.Action_Type=action_type) A
					);
	END IF;

	RETURN return_value;
END$$
DELIMITER ;
