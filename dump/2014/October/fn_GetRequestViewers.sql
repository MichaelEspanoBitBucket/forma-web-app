DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getRequestViewers`(formId INT, requestId INT) RETURNS varchar(255) CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value VARCHAR(255);

	SET return_value =  (SELECT GROUP_CONCAT(DISTINCT Users) AS Request_Users FROM (
							SELECT 
							user_id AS Users
						FROM tbrequest_viewer requestViewer
						WHERE requestViewer.Form_ID = formId
						AND requestViewer.Request_ID = requestId) A
					);
	
	RETURN return_value;
END$$
DELIMITER ;
