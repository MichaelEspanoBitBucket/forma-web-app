DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getUsersByPosition`(id INT) RETURNS varchar(255) CHARSET latin1 DETERMINISTIC
BEGIN
	RETURN (SELECT group_concat(user.id) FROM tbuser user WHERE position=id);
END$$
DELIMITER ;
