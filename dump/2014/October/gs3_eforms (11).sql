-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 20, 2013 at 03:06 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms`
--
CREATE DATABASE IF NOT EXISTS `gs3_eforms` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `gs3_eforms`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ProcessWorkflow`(IN tblName VARCHAR(255), IN id VARCHAR(255))
BEGIN
    SET @strSQL = CONCAT(
      'SELECT 
        WFO.type_rel, 
        WFO.Status,
        WFO.buttonStatus, 
        CASE 
        WHEN WFO.ProcessorType = ''1'' THEN 
            CASE 
                WHEN PROCESSOR.department_position_level = ''1'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                       (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''1''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''2'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''3'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''2'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    END
                END
        WHEN WFO.ProcessorType = ''2'' THEN (SELECT group_concat(user_processor.id separator '', '') FROM tbuser user_processor WHERE user_processor.Position = WFO.Processor)
        WHEN WFO.ProcessorType = ''3'' THEN WFO.Processor
        ELSE
        FRM.Requestor END AS Processor,
        WFO.condition_return,
        WFO.field,
        WFO.operator,
        WFO.field_value,
        WFO.fieldEnabled,
        WFO.fieldRequired,
        WFO.fieldHiddenValue
        INTO @NodeType, @Status, @Buttons, @Processor, @ConditionAction, @Field, @Operator, @FieldValue, @FieldEnabled, @FieldRequired, @FieldHiddenValue
      FROM ', tblName,
      ' FRM LEFT JOIN tbworkflow WF 
      ON WF.Id = FRM.Workflow_Id
      LEFT JOIN tbworkflow_objects WFO
      ON WFO.Workflow_Id = WF.Id AND WFO.Object_Id = FRM.Node_Id
      LEFT JOIN tbuser PROCESSOR
      ON PROCESSOR.Id = FRM.Processor
      WHERE FRM.ID = ', id); 
    
    PREPARE strSQl FROM @strSQL;
    EXECUTE strSQL;
    
	SELECT @NodeType as NodeType, @Status as Status, @Processor as Processor, @Buttons as Buttons, @ConditionAction as ConditionAction, @Field as Field , @Operator as Operator, @FieldValue as FieldValue, @FieldEnabled as FieldEnabled, @FieldRequired as FieldRequired, @FieldHiddenValue as FieldHiddenValue ;
	DEALLOCATE  PREPARE strSQl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Sample`()
BEGIN
	SELECT * FROM tbuser;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `1_tbl_discussion`
--

CREATE TABLE IF NOT EXISTS `1_tbl_discussion` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TrackNo` longtext,
  `Requestor` longtext,
  `Status` longtext,
  `Processor` longtext,
  `LastAction` longtext,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` longtext,
  `UpdatedBy` longtext,
  `Unread` longtext,
  `Node_ID` longtext,
  `Workflow_ID` longtext,
  `fieldEnabled` longtext,
  `fieldRequired` longtext,
  `fieldHiddenValues` longtext,
  `Subject` longtext,
  `Message` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `1_tbl_discussion`
--

INSERT INTO `1_tbl_discussion` (`ID`, `TrackNo`, `Requestor`, `Status`, `Processor`, `LastAction`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`, `Unread`, `Node_ID`, `Workflow_ID`, `fieldEnabled`, `fieldRequired`, `fieldHiddenValues`, `Subject`, `Message`) VALUES
(1, 'Discussion0001', '1', '', '1', '', '2013-09-19 18:25:55', NULL, '1', '1', '', 'node_2', '7', '', '', '', 'HELLO', 'HELLO PANDA'),
(2, 'Discussion0002', '1', 'Active', '1', '{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-19 18:28:23', NULL, '1', '1', '', 'node_2', '7', '[]', '[]', '["Subject","Message"]', 'SUBJECT', 'MESSAGE'),
(3, 'Discussion0003', '5', 'Active', '5', '{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-20 08:44:36', NULL, '5', '5', '', 'node_2', '8', '["Subject","Message"]', '[]', '[]', 'panda', 'im awesome');

-- --------------------------------------------------------

--
-- Table structure for table `tbaudit_action`
--

CREATE TABLE IF NOT EXISTS `tbaudit_action` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `value` varchar(1000) NOT NULL,
  `type` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbaudit_logs`
--

CREATE TABLE IF NOT EXISTS `tbaudit_logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `audit_action` int(255) NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `record_id` int(255) NOT NULL,
  `date` datetime NOT NULL,
  `ip` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `tbaudit_logs`
--

INSERT INTO `tbaudit_logs` (`id`, `user_id`, `audit_action`, `table_name`, `record_id`, `date`, `ip`, `is_active`) VALUES
(1, 1, 3, 'tbuser', 1, '2013-09-16 11:45:21', '192.168.0.71', 1),
(2, 9, 3, 'tbuser', 9, '2013-09-16 11:45:46', '192.168.0.71', 1),
(3, 5, 3, 'tbuser', 5, '2013-09-16 13:48:22', '192.168.0.124', 1),
(4, 5, 3, 'tbuser', 5, '2013-09-16 14:14:52', '192.168.0.124', 1),
(5, 1, 2, 'tbuser', 1, '2013-09-16 15:10:09', '127.0.0.1', 1),
(6, 1, 3, 'tbuser', 1, '2013-09-16 16:05:51', '127.0.0.1', 1),
(7, 1, 3, 'tbuser', 1, '2013-09-16 16:08:04', '127.0.0.1', 1),
(8, 3, 3, 'tbuser', 3, '2013-09-16 18:56:39', '127.0.0.1', 1),
(9, 5, 2, 'tbuser', 5, '2013-09-17 16:27:33', '127.0.0.1', 1),
(10, 1, 3, 'tbuser', 1, '2013-09-18 09:13:16', '127.0.0.1', 1),
(11, 0, 3, 'tbuser', 0, '2013-09-18 09:13:18', '127.0.0.1', 1),
(12, 0, 3, 'tbuser', 0, '2013-09-18 09:13:19', '127.0.0.1', 1),
(13, 0, 3, 'tbuser', 0, '2013-09-18 09:13:20', '127.0.0.1', 1),
(14, 0, 3, 'tbuser', 0, '2013-09-18 09:13:20', '127.0.0.1', 1),
(15, 0, 3, 'tbuser', 0, '2013-09-18 09:13:20', '127.0.0.1', 1),
(16, 0, 3, 'tbuser', 0, '2013-09-18 09:13:20', '127.0.0.1', 1),
(17, 5, 3, 'tbuser', 5, '2013-09-18 09:16:37', '127.0.0.1', 1),
(18, 1, 4, 'tbpost', 1, '2013-09-19 10:30:32', '127.0.0.1', 1),
(19, 1, 5, 'tbcomment', 1, '2013-09-19 12:39:27', '127.0.0.1', 1),
(20, 1, 5, 'tbcomment', 2, '2013-09-19 13:21:59', '127.0.0.1', 1),
(21, 5, 2, 'tbuser', 5, '2013-09-20 08:41:59', '127.0.0.1', 1),
(22, 5, 3, 'tbuser', 5, '2013-09-20 08:43:30', '127.0.0.1', 1),
(23, 1, 2, 'tbuser', 1, '2013-09-20 10:39:40', '192.168.0.119', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbcomment`
--

CREATE TABLE IF NOT EXISTS `tbcomment` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `post_id` int(255) NOT NULL,
  `comment` longtext NOT NULL,
  `postedBy` int(255) NOT NULL,
  `commentType` tinyint(1) NOT NULL COMMENT '0 = default, 1 = request',
  `date_posted` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `fID` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbcomment`
--

INSERT INTO `tbcomment` (`id`, `post_id`, `comment`, `postedBy`, `commentType`, `date_posted`, `is_active`, `fID`) VALUES
(1, 4, 'sdfdsf', 1, 1, '2013-09-19 12:39:27', 1, 0),
(2, 4, 'sample comment', 1, 1, '2013-09-19 13:21:59', 1, 0),
(3, 4, 'test comment yehet!', 1, 1, '2013-09-19 14:46:44', 1, 1),
(4, 4, '', 5, 1, '2013-09-19 15:29:28', 1, 1),
(5, 4, 'hkguncxvjhfvjh', 5, 1, '2013-09-19 15:45:33', 1, 1),
(6, 1, 'test comment...\n\n\nusjdgajsbbfjufkd', 5, 1, '2013-09-19 15:47:47', 1, 4),
(7, 1, 'fhvvbj\nbj\ngjkuf\ngukg\n\n\nchkki', 5, 1, '2013-09-19 16:03:40', 1, 4),
(8, 2, 'Nice, we&#039;re looking forward to this.', 5, 1, '2013-09-19 16:16:15', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbcompany`
--

CREATE TABLE IF NOT EXISTS `tbcompany` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact_number` varchar(30) NOT NULL,
  `primary_contact` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `date_registered` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbcompany`
--

INSERT INTO `tbcompany` (`id`, `code`, `name`, `contact_number`, `primary_contact`, `email`, `extension`, `date_registered`, `is_active`) VALUES
(1, 'GS3-2008', 'Global Strategic Solution & Services Inc.', '9999055639', '', 'samuel_boa2000@yahoo.com', '', '2013-08-23 09:41:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblike`
--

CREATE TABLE IF NOT EXISTS `tblike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `likeType` varchar(50) NOT NULL,
  `date_liked` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbmessage`
--

CREATE TABLE IF NOT EXISTS `tbmessage` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) NOT NULL,
  `message` longtext NOT NULL,
  `senderID` int(255) NOT NULL,
  `recipient` varchar(1000) NOT NULL,
  `user_read` tinyint(1) NOT NULL,
  `rep` int(255) NOT NULL,
  `date_submit` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tborgchart`
--

CREATE TABLE IF NOT EXISTS `tborgchart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `date` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `reasonForRevision` longtext NOT NULL,
  `json` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tborgchart`
--

INSERT INTO `tborgchart` (`id`, `company_id`, `title`, `description`, `date`, `status`, `is_active`, `reasonForRevision`, `json`) VALUES
(1, 1, '1', '1', '2013-09-16  11:10:57', 0, 1, '', ''),
(2, 1, 'asd', 'asd', '2013-09-16  11:11:26', 0, 1, '', ''),
(3, 1, 'sa', 'sa', '2013-09-16  11:12:06', 1, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tborgchartline`
--

CREATE TABLE IF NOT EXISTS `tborgchartline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orgChart_id` varchar(50) NOT NULL,
  `parent` varchar(50) NOT NULL,
  `child` varchar(50) NOT NULL,
  `json` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `tborgchartline`
--

INSERT INTO `tborgchartline` (`id`, `orgChart_id`, `parent`, `child`, `json`) VALUES
(1, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":199,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":null}'),
(2, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":null}'),
(3, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":341,"width":3,"height":108,"background-color":"#424242","line-type":"childLine","json_title":null}'),
(4, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":176,"left":334,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":null,"line-type":"arrow"}'),
(5, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":199,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":null}'),
(6, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":null}'),
(7, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":341,"width":3,"height":108,"background-color":"#424242","line-type":"childLine","json_title":null}'),
(8, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":176,"left":334,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":null,"line-type":"arrow"}'),
(9, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":199,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":null}'),
(10, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":null}'),
(11, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":341,"width":3,"height":108,"background-color":"#424242","line-type":"childLine","json_title":null}'),
(12, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":176,"left":334,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":null,"line-type":"arrow"}');

-- --------------------------------------------------------

--
-- Table structure for table `tborgchartobjects`
--

CREATE TABLE IF NOT EXISTS `tborgchartobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orgChart_id` int(11) NOT NULL,
  `object_id` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  `users` text NOT NULL,
  `head` varchar(255) NOT NULL,
  `assistant` varchar(255) NOT NULL,
  `members` varchar(255) NOT NULL,
  `json` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tborgchartobjects`
--

INSERT INTO `tborgchartobjects` (`id`, `orgChart_id`, `object_id`, `department`, `users`, `head`, `assistant`, `members`, `json`) VALUES
(1, 1, 'node_1', 'Admin', '', '', '', '', '{"node_left":61,"node_top":15,"node_data_id":"node_1","node_text":"Admin","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["3"],"orgchart_dept_assistant":["7"],"orgchart_dept_members":["6","9","8"]}'),
(2, 1, 'node_2', 'Members', '', '', '', '', '{"node_left":263,"node_top":163,"node_data_id":"node_2","node_text":"Members","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["5"],"orgchart_dept_assistant":["4"],"orgchart_dept_members":["1","11","10","2"]}'),
(3, 2, 'node_1', 'Admin', '', '', '', '', '{"node_left":61,"node_top":15,"node_data_id":"node_1","node_text":"Admin","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["3"],"orgchart_dept_assistant":["7"],"orgchart_dept_members":["6","9","8"]}'),
(4, 2, 'node_2', 'Members', '', '', '', '', '{"node_left":263,"node_top":163,"node_data_id":"node_2","node_text":"Members","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["5"],"orgchart_dept_assistant":["4"],"orgchart_dept_members":["1","11","10","2"]}'),
(5, 3, 'node_1', 'Admin', '', '', '', '', '{"node_left":61,"node_top":15,"node_data_id":"node_1","node_text":"Admin","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["3"],"orgchart_dept_assistant":["7"],"orgchart_dept_members":["6","9","8"]}'),
(6, 3, 'node_2', 'Members', '', '', '', '', '{"node_left":263,"node_top":163,"node_data_id":"node_2","node_text":"Members","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["5"],"orgchart_dept_assistant":["4"],"orgchart_dept_members":["1","11","10","2"]}');

-- --------------------------------------------------------

--
-- Table structure for table `tbpost`
--

CREATE TABLE IF NOT EXISTS `tbpost` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `post` longtext NOT NULL,
  `postedBy` int(255) NOT NULL,
  `privacy` varchar(100) NOT NULL,
  `date_posted` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbpost`
--

INSERT INTO `tbpost` (`id`, `post`, `postedBy`, `privacy`, `date_posted`, `is_active`) VALUES
(1, '12', 1, '', '2013-09-19 10:30:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbreply_message`
--

CREATE TABLE IF NOT EXISTS `tbreply_message` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `message_id` int(255) NOT NULL,
  `sender` int(255) NOT NULL,
  `message` longtext NOT NULL,
  `reps` int(255) NOT NULL,
  `user_read` tinyint(1) NOT NULL,
  `date_submit` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbuser`
--

CREATE TABLE IF NOT EXISTS `tbuser` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `position` varchar(100) NOT NULL,
  `company_id` int(255) NOT NULL,
  `user_level_id` int(255) NOT NULL,
  `department_position_level` int(255) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `date_registered` datetime NOT NULL,
  `email_activate` tinyint(1) NOT NULL,
  `is_available` tinyint(4) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbuser`
--

INSERT INTO `tbuser` (`id`, `email`, `display_name`, `first_name`, `middle_name`, `last_name`, `contact_number`, `position`, `company_id`, `user_level_id`, `department_position_level`, `password`, `extension`, `date_registered`, `email_activate`, `is_available`, `is_active`, `department_id`) VALUES
(1, 'samuel_boa2000@yahoo.com', 'Ka Admin', 'Jolly', '', 'Dela Cruz', '9999055639', 'CEO', 1, 2, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', 'png', '2013-08-23 09:41:58', 1, 0, 1, 6),
(2, 'samuel.pulta@gs3.com.ph', 'Ka Sammy', 'Samuel', '', 'Pulta', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', 'png', '2013-08-23 09:56:39', 0, 0, 1, 6),
(3, 'Aaron.Tolentino@gs3.com.ph', 'Ka Aaron', 'Aaron', '', 'Tolentino', '', 'Developer', 1, 3, 1, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:56:47', 0, 0, 1, 5),
(4, 'jeweltolentino@gs3.com.ph', 'Ka Jewel', 'Jewel', '', 'Tolentino', '', 'Developer', 1, 3, 2, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:56:56', 0, 0, 1, 6),
(5, 'ervinnesodusta@gs3.com.ph', 'Ka Lalabs', 'Ervinne', '', 'Sodusta', '', 'Developer', 1, 3, 1, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:02', 0, 0, 1, 6),
(6, 'alvin.romero@gs3.com.ph', 'Ka Alvin', 'Alvin', '', 'Romero', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:08', 0, 0, 1, 5),
(7, 'alfredo.magpantay@gs3.com.ph', 'Ka Alfred', 'Alfredo', '', 'Magpantay', '', 'Developer', 1, 3, 2, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:13', 0, 0, 1, 5),
(8, 'edgar.palmon@gs3.com.ph', 'Ka Edgar', 'Edgar', '', 'Palmon', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:19', 0, 0, 1, 5),
(9, 'Aramis.Marquez@gs3.com.ph', 'Ka Aramis', 'Aramis', '', 'Marquez', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:24', 0, 0, 1, 5),
(10, 'rowi.dizon@gs3.com.ph', 'Ka Rowi', 'Rowi', '', 'Dizon', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:29', 0, 0, 1, 6),
(11, 'michael.espano@gs3.com.ph', 'Ka Panda', 'Michael', '', 'Espano', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:33', 0, 0, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbuser_level`
--

CREATE TABLE IF NOT EXISTS `tbuser_level` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_level` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbuser_level`
--

INSERT INTO `tbuser_level` (`id`, `user_level`, `is_active`) VALUES
(1, 'Administrator', 1),
(2, 'Company Admin', 1),
(3, 'Users', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbuser_position_level`
--

CREATE TABLE IF NOT EXISTS `tbuser_position_level` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `position` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbuser_position_level`
--

INSERT INTO `tbuser_position_level` (`id`, `position`, `is_active`) VALUES
(1, 'Head', 1),
(2, 'Assistant Head', 1),
(3, 'Member', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbworkflow`
--

CREATE TABLE IF NOT EXISTS `tbworkflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `date` varchar(20) NOT NULL,
  `delegateRequest` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbworkflow`
--

INSERT INTO `tbworkflow` (`id`, `form_id`, `title`, `description`, `date`, `delegateRequest`, `is_active`) VALUES
(1, 1, 'Discussion', 'Discussion', '2013-09-19  17:51:08', 0, 0),
(2, 1, 'Discussion', 'Discussion', '2013-09-19  17:51:33', 0, 0),
(3, 1, 'TEST', 'TEST', '2013-09-19  18:19:37', 0, 0),
(4, 1, 'test', 'test', '2013-09-19  18:22:01', 0, 0),
(5, 1, 'Discussion', 'Discussion', '2013-09-19  18:23:43', 0, 0),
(6, 1, 'TEST', 'TEST', '2013-09-19  18:25:07', 0, 0),
(7, 1, 'TEST', 'TEST', '2013-09-19  18:25:27', 0, 0),
(8, 1, 'Discussion', 'Discussion', '2013-09-20  08:14:53', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbworkflow_lines`
--

CREATE TABLE IF NOT EXISTS `tbworkflow_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` varchar(50) NOT NULL,
  `parent` varchar(50) NOT NULL,
  `child` varchar(50) NOT NULL,
  `json` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `tbworkflow_lines`
--

INSERT INTO `tbworkflow_lines` (`id`, `workflow_id`, `parent`, `child`, `json`) VALUES
(1, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":98,"width":103,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"POST"}'),
(2, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"POST"}'),
(3, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":201,"width":3,"height":111,"background-color":"#424242","line-type":"childLine","json_title":"POST"}'),
(4, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":169,"left":194,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"POST","line-type":"arrow"}'),
(5, '1', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":261,"width":217,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"REMOVE"}'),
(6, '1', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"REMOVE"}'),
(7, '1', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":478,"width":3,"height":40,"background-color":"#424242","line-type":"childLine","json_title":"REMOVE"}'),
(8, '1', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":249,"left":471,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"REMOVE","line-type":"arrow"}'),
(9, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":98,"width":103,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"POST"}'),
(10, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"POST"}'),
(11, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":201,"width":3,"height":111,"background-color":"#424242","line-type":"childLine","json_title":"POST"}'),
(12, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":169,"left":194,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"POST","line-type":"arrow"}'),
(13, '2', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":261,"width":217,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"REMOVE"}'),
(14, '2', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"REMOVE"}'),
(15, '2', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":478,"width":3,"height":40,"background-color":"#424242","line-type":"childLine","json_title":"REMOVE"}'),
(16, '2', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":249,"left":471,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"REMOVE","line-type":"arrow"}'),
(17, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":98,"width":107,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Submit"}'),
(18, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Submit"}'),
(19, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":205,"width":3,"height":72,"background-color":"#424242","line-type":"childLine","json_title":"Submit"}'),
(20, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":130,"left":198,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Submit","line-type":"arrow"}'),
(21, '3', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":177,"left":265,"width":166,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Approve"}'),
(22, '3', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Approve"}'),
(23, '3', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":177,"left":431,"width":3,"height":35,"background-color":"#424242","line-type":"childLine","json_title":"Approve"}'),
(24, '3', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":205,"left":424,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Approve","line-type":"arrow"}'),
(25, '4', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":98,"width":107,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Submit"}'),
(26, '4', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Submit"}'),
(27, '4', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":205,"width":3,"height":72,"background-color":"#424242","line-type":"childLine","json_title":"Submit"}'),
(28, '4', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":130,"left":198,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Submit","line-type":"arrow"}'),
(29, '4', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":177,"left":265,"width":166,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Approve"}'),
(30, '4', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Approve"}'),
(31, '4', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":177,"left":431,"width":3,"height":35,"background-color":"#424242","line-type":"childLine","json_title":"Approve"}'),
(32, '4', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":205,"left":424,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Approve","line-type":"arrow"}'),
(33, '5', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":98,"width":103,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"POST"}'),
(34, '5', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"POST"}'),
(35, '5', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":201,"width":3,"height":111,"background-color":"#424242","line-type":"childLine","json_title":"POST"}'),
(36, '5', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":169,"left":194,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"POST","line-type":"arrow"}'),
(37, '5', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":261,"width":217,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"REMOVE"}'),
(38, '5', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"REMOVE"}'),
(39, '5', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":478,"width":3,"height":40,"background-color":"#424242","line-type":"childLine","json_title":"REMOVE"}'),
(40, '5', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":249,"left":471,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"REMOVE","line-type":"arrow"}'),
(41, '6', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":98,"width":103,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"POST"}'),
(42, '6', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"POST"}'),
(43, '6', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":201,"width":3,"height":111,"background-color":"#424242","line-type":"childLine","json_title":"POST"}'),
(44, '6', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":169,"left":194,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"POST","line-type":"arrow"}'),
(45, '6', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":261,"width":217,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"REMOVE"}'),
(46, '6', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"REMOVE"}'),
(47, '6', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":478,"width":3,"height":40,"background-color":"#424242","line-type":"childLine","json_title":"REMOVE"}'),
(48, '6', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":249,"left":471,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"REMOVE","line-type":"arrow"}'),
(49, '7', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":98,"width":103,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"POST"}'),
(50, '7', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"POST"}'),
(51, '7', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":201,"width":3,"height":111,"background-color":"#424242","line-type":"childLine","json_title":"POST"}'),
(52, '7', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":169,"left":194,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"POST","line-type":"arrow"}'),
(53, '7', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":261,"width":217,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"REMOVE"}'),
(54, '7', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"REMOVE"}'),
(55, '7', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":478,"width":3,"height":40,"background-color":"#424242","line-type":"childLine","json_title":"REMOVE"}'),
(56, '7', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":249,"left":471,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"REMOVE","line-type":"arrow"}'),
(57, '8', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":98,"width":103,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"POST"}'),
(58, '8', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"POST"}'),
(59, '8', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":65,"left":201,"width":3,"height":111,"background-color":"#424242","line-type":"childLine","json_title":"POST"}'),
(60, '8', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":169,"left":194,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"POST","line-type":"arrow"}'),
(61, '8', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":261,"width":217,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"REMOVE"}'),
(62, '8', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"REMOVE"}'),
(63, '8', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":216,"left":478,"width":3,"height":40,"background-color":"#424242","line-type":"childLine","json_title":"REMOVE"}'),
(64, '8', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":249,"left":471,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"REMOVE","line-type":"arrow"}');

-- --------------------------------------------------------

--
-- Table structure for table `tbworkflow_objects`
--

CREATE TABLE IF NOT EXISTS `tbworkflow_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `object_id` varchar(50) NOT NULL,
  `type_rel` varchar(10) NOT NULL,
  `tops` varchar(50) NOT NULL,
  `lefts` varchar(50) NOT NULL,
  `processorType` varchar(50) NOT NULL,
  `processor` varchar(100) NOT NULL,
  `buttonStatus` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `field` varchar(128) NOT NULL,
  `operator` varchar(128) NOT NULL,
  `field_value` varchar(128) NOT NULL,
  `trues` varchar(128) NOT NULL,
  `falses` varchar(128) NOT NULL,
  `fieldEnabled` text NOT NULL,
  `fieldRequired` text NOT NULL,
  `email_title` text,
  `email_body` text,
  `email_recipient` text,
  `email_cc` text,
  `fieldHiddenValue` text,
  `email_bcc` text,
  `field_trigger` text,
  `json` longtext NOT NULL,
  `condition_return` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `tbworkflow_objects`
--

INSERT INTO `tbworkflow_objects` (`id`, `workflow_id`, `object_id`, `type_rel`, `tops`, `lefts`, `processorType`, `processor`, `buttonStatus`, `status`, `field`, `operator`, `field_value`, `trues`, `falses`, `fieldEnabled`, `fieldRequired`, `email_title`, `email_body`, `email_recipient`, `email_cc`, `fieldHiddenValue`, `email_bcc`, `field_trigger`, `json`, `condition_return`) VALUES
(1, 1, 'node_1', '1', '', '', '', '', '{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","buttonStatus":{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(2, 1, 'node_2', '2', '', '', '4', '1', '{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Active', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":123,"node_top":156,"node_data_id":"node_2","node_text":"Requestor","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"4","processor":"1","status":"Active","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":[],"buttonStatus":{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(3, 1, 'node_3', '4', '', '', '', '', 'null', 'REMOVED', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":420,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"REMOVED"}', 'null'),
(4, 2, 'node_1', '1', '', '', '', '', '{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Subject","Message"]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","buttonStatus":{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}},"fieldEnabled":["Subject","Message"],"fieldRequired":[],"fieldHiddenValue":[]}', 'null'),
(5, 2, 'node_2', '2', '', '', '4', '1', '{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Active', '', '', '', '', '', '[]', '["Subject","Message"]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":123,"node_top":156,"node_data_id":"node_2","node_text":"Requestor","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"4","processor":"1","status":"Active","fieldEnabled":[],"fieldRequired":["Subject","Message"],"fieldHiddenValue":[],"buttonStatus":{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(6, 2, 'node_3', '4', '', '', '', '', 'null', 'REMOVED', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":420,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"REMOVED"}', 'null'),
(7, 3, 'node_1', '1', '', '', '', '', '{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["textbox_1"]', '["textbox_1"]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","buttonStatus":{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}},"fieldEnabled":["textbox_1"],"fieldRequired":["textbox_1"],"fieldHiddenValue":[]}', 'null'),
(8, 3, 'node_2', '2', '', '', '3', '5', '{"Approve":{"button_name":"Approve","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Submitted', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '["textbox_1","checkbox_2"]', NULL, NULL, '{"node_left":127,"node_top":117,"node_data_id":"node_2","node_text":"Ervinne  Sodusta","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"3","processor":"5","status":"Submitted","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":["textbox_1","checkbox_2"],"buttonStatus":{"Approve":{"button_name":"Approve","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(9, 3, 'node_3', '4', '', '', '', '', 'null', 'End', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":373,"node_top":192,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"End"}', 'null'),
(10, 4, 'node_1', '1', '', '', '', '', '{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Subject"]', '["Subject"]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","buttonStatus":{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}},"fieldEnabled":["Subject"],"fieldRequired":["Subject"],"fieldHiddenValue":[]}', 'null'),
(11, 4, 'node_2', '2', '', '', '3', '5', '{"Approve":{"button_name":"Approve","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Submitted', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '["textbox_1","checkbox_2"]', NULL, NULL, '{"node_left":127,"node_top":117,"node_data_id":"node_2","node_text":"Ervinne  Sodusta","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"3","processor":"5","status":"Submitted","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":["textbox_1","checkbox_2"],"buttonStatus":{"Approve":{"button_name":"Approve","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(12, 4, 'node_3', '4', '', '', '', '', 'null', 'End', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":373,"node_top":192,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"End"}', 'null'),
(13, 5, 'node_1', '1', '', '', '', '', '{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Subject","Message"]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","buttonStatus":{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}},"fieldEnabled":["Subject","Message"],"fieldRequired":[],"fieldHiddenValue":[]}', 'null'),
(14, 5, 'node_2', '2', '', '', '4', '1', '{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Active', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '["Message"]', NULL, NULL, '{"node_left":123,"node_top":156,"node_data_id":"node_2","node_text":"Requestor","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"4","processor":"1","status":"Active","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":["Message"],"buttonStatus":{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(15, 5, 'node_3', '4', '', '', '', '', 'null', 'REMOVED', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":420,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"REMOVED"}', 'null'),
(16, 6, 'node_1', '1', '', '', '', '', '{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Subject","Message"]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","buttonStatus":{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}},"fieldEnabled":["Subject","Message"],"fieldRequired":[],"fieldHiddenValue":[]}', 'null'),
(17, 6, 'node_2', '2', '', '', '4', '1', '{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Active', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '["Subject","Message"]', NULL, NULL, '{"node_left":123,"node_top":156,"node_data_id":"node_2","node_text":"Requestor","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"4","processor":"1","status":"Active","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":["Subject","Message"],"buttonStatus":{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(18, 6, 'node_3', '4', '', '', '', '', 'null', 'REMOVED', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":420,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"REMOVED"}', 'null'),
(19, 7, 'node_1', '1', '', '', '', '', '{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Subject","Message"]', '["Subject","Message"]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","buttonStatus":{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}},"fieldEnabled":["Subject","Message"],"fieldRequired":["Subject","Message"],"fieldHiddenValue":[]}', 'null'),
(20, 7, 'node_2', '2', '', '', '4', '1', '{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Active', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '["Subject","Message"]', NULL, NULL, '{"node_left":123,"node_top":156,"node_data_id":"node_2","node_text":"Requestor","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"4","processor":"1","status":"Active","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":["Subject","Message"],"buttonStatus":{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(21, 7, 'node_3', '4', '', '', '', '', 'null', 'REMOVED', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":420,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"REMOVED"}', 'null'),
(22, 8, 'node_1', '1', '', '', '', '', '{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Subject","Message"]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","buttonStatus":{"POST":{"button_name":"POST","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}},"fieldEnabled":["Subject","Message"],"fieldRequired":[],"fieldHiddenValue":[]}', 'null'),
(23, 8, 'node_2', '2', '', '', '4', '1', '{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Active', '', '', '', '', '', '["Subject","Message"]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":123,"node_top":156,"node_data_id":"node_2","node_text":"Requestor","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"4","processor":"1","status":"Active","fieldEnabled":["Subject","Message"],"fieldRequired":[],"fieldHiddenValue":[],"buttonStatus":{"REMOVE":{"button_name":"REMOVE","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(24, 8, 'node_3', '4', '', '', '', '', 'null', 'REMOVED', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":420,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"REMOVED"}', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `tb_workspace`
--

CREATE TABLE IF NOT EXISTS `tb_workspace` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `form_name` varchar(100) NOT NULL,
  `form_table_name` varchar(100) NOT NULL,
  `form_description` varchar(100) NOT NULL,
  `form_content` text NOT NULL,
  `form_buttons` longtext NOT NULL,
  `created_by` int(255) NOT NULL,
  `updated_by` int(255) NOT NULL,
  `company_id` int(255) NOT NULL,
  `button_content` longtext NOT NULL,
  `reference_prefix` varchar(100) NOT NULL,
  `reference_type` varchar(100) NOT NULL,
  `version` varchar(100) NOT NULL,
  `form_json` longtext NOT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `active_fields` longtext NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `MobileJsonData` longtext NOT NULL,
  `MobileContent` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_workspace`
--

INSERT INTO `tb_workspace` (`id`, `form_name`, `form_table_name`, `form_description`, `form_content`, `form_buttons`, `created_by`, `updated_by`, `company_id`, `button_content`, `reference_prefix`, `reference_type`, `version`, `form_json`, `date_created`, `is_active`, `active_fields`, `date_updated`, `MobileJsonData`, `MobileContent`) VALUES
(1, 'Discussion', '1_tbl_Discussion', 'Discussion', '\n               <div class="setObject cursor_move ui-draggable ui-resizable" data-type="textbox" data-object-id="3" id="setObject_3" data-toggle="popover" data-placement="right" style="border-color: transparent;"><div class="fields_below" style="min-width:200px;"><div class="setObjects_actions"><div class="pull-left"><img src="/images/loader/load.gif" class="display" id="obj_load_3"></div><div class="obj_actions display" id="obj_actions_3"><i class="icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_3" data-object-id="3" data-object-type="textbox" data-original-title="Properties"></i><i class="icon-remove cursor object_setup object_remove" data-object-id="3" data-original-title="Remove"></i></div></div><div class="label_below obj_label" id="label_3"><label id="lbl_3">Subject</label></div><div class="input_position_below" id="obj_fields_3"><input data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_3" name="Subject" id="getFields_3" data-placement="bottom" placeholder="Subject" data-original-title="Subject"></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="setObject cursor_move ui-draggable ui-resizable" data-type="textArea" data-object-id="4" id="setObject_4" data-toggle="popover" data-placement="right" style="border-color: transparent; left: 3px; top: 102px;"><div class="fields_below" style="min-width:200px;"><div class="setObjects_actions"><div class="pull-left"><img src="/images/loader/load.gif" class="display" id="obj_load_4"></div><div class="obj_actions display" id="obj_actions_4"><i class="icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_4" data-object-id="4" data-object-type="textarea" data-original-title="Properties"></i><i class="icon-remove cursor object_setup object_remove" data-object-id="4" data-original-title="Remove"></i></div></div><div class="label_below obj_label" id="label_4"><label id="lbl_4">Message</label></div><div class="input_position_below" id="obj_fields_4"><textarea data-type="longtext" class="form-textarea getFields getFields_4" disabled="disabled" name="Message" id="getFields_4" style="resize:none;" placeholder="Message" data-original-title="Message"></textarea></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div></div>', 'POST,REMOVE', 1, 1, 1, '\n                           <div data-object-id="1" data-type="button" class="setObject setObject_btn ui-draggable ui-resizable" id="setObject_1" data-toggle="popover" data-placement="right" style="display: inline; margin-left: 3px; margin-top: 5px; position: relative; border-color: transparent;"><input data-type="button" type="button" class="btn-basicBtn " id="btnName_1" value="POST" style="margin:5px 5px 5px 5px;" name="POST"><i class="icon-cogs cursor object_setup object_properties" data-properties-type="4" id="object_properties_1" data-object-id="1" data-object-type="button" data-original-title="Properties"></i><i class="icon-remove cursor object_setup object_remove" data-object-id="1" data-original-title="Remove"></i><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div></div><div data-object-id="2" data-type="button" class="setObject setObject_btn ui-draggable ui-resizable" id="setObject_2" data-toggle="popover" data-placement="right" style="display: inline; margin-left: 3px; margin-top: 5px; position: relative; border-color: transparent;"><input data-type="button" type="button" class="btn-basicBtn " id="btnName_2" value="REMOVE" style="margin:5px 5px 5px 5px;" name="REMOVE"><i class="icon-cogs cursor object_setup object_properties" data-properties-type="4" id="object_properties_2" data-object-id="2" data-object-type="button" data-original-title="Properties"></i><i class="icon-remove cursor object_setup object_remove" data-object-id="2" data-original-title="Remove"></i><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div></div>', 'Discussion', 'Sequential', '1', '{"Title":"Discussion","DisplayName":"Discussion","Prefix":"Discussion","Type":"Sequential","WorkspaceHeight":"600","WorkspaceWidth":"1030","BtnLength":2,"BtnName":"POST,REMOVE","button_content":"\\n                           <div data-object-id=\\"1\\" data-type=\\"button\\" class=\\"setObject setObject_btn ui-draggable ui-resizable\\" id=\\"setObject_1\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"display: inline; margin-left: 3px; margin-top: 5px; position: relative; border-color: transparent;\\"><input data-type=\\"button\\" type=\\"button\\" class=\\"btn-basicBtn \\" id=\\"btnName_1\\" value=\\"POST\\" style=\\"margin:5px 5px 5px 5px;\\" name=\\"POST\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"4\\" id=\\"object_properties_1\\" data-object-id=\\"1\\" data-object-type=\\"button\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"1\\" data-original-title=\\"Remove\\"></i><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div><div data-object-id=\\"2\\" data-type=\\"button\\" class=\\"setObject setObject_btn ui-draggable ui-resizable\\" id=\\"setObject_2\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"display: inline; margin-left: 3px; margin-top: 5px; position: relative; border-color: transparent;\\"><input data-type=\\"button\\" type=\\"button\\" class=\\"btn-basicBtn \\" id=\\"btnName_2\\" value=\\"REMOVE\\" style=\\"margin:5px 5px 5px 5px;\\" name=\\"REMOVE\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"4\\" id=\\"object_properties_2\\" data-object-id=\\"2\\" data-object-type=\\"button\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"2\\" data-original-title=\\"Remove\\"></i><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div>","ObjectLength":4,"WorkspaceContent":"\\n               <div class=\\"setObject cursor_move ui-draggable ui-resizable\\" data-type=\\"textbox\\" data-object-id=\\"3\\" id=\\"setObject_3\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"border-color: transparent;\\"><div class=\\"fields_below\\" style=\\"min-width:200px;\\"><div class=\\"setObjects_actions\\"><div class=\\"pull-left\\"><img src=\\"/images/loader/load.gif\\" class=\\"display\\" id=\\"obj_load_3\\"></div><div class=\\"obj_actions display\\" id=\\"obj_actions_3\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"1\\" id=\\"object_properties_3\\" data-object-id=\\"3\\" data-object-type=\\"textbox\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"3\\" data-original-title=\\"Remove\\"></i></div></div><div class=\\"label_below obj_label\\" id=\\"label_3\\"><label id=\\"lbl_3\\">Subject</label></div><div class=\\"input_position_below\\" id=\\"obj_fields_3\\"><input data-type=\\"longtext\\" type=\\"text\\" disabled=\\"disabled\\" class=\\"tip form-text getFields getFields_3\\" name=\\"Subject\\" id=\\"getFields_3\\" data-placement=\\"bottom\\" placeholder=\\"Subject\\" data-original-title=\\"Subject\\"></div></div><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"setObject cursor_move ui-draggable ui-resizable\\" data-type=\\"textArea\\" data-object-id=\\"4\\" id=\\"setObject_4\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"border-color: transparent; left: 3px; top: 102px;\\"><div class=\\"fields_below\\" style=\\"min-width:200px;\\"><div class=\\"setObjects_actions\\"><div class=\\"pull-left\\"><img src=\\"/images/loader/load.gif\\" class=\\"display\\" id=\\"obj_load_4\\"></div><div class=\\"obj_actions display\\" id=\\"obj_actions_4\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"1\\" id=\\"object_properties_4\\" data-object-id=\\"4\\" data-object-type=\\"textarea\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"4\\" data-original-title=\\"Remove\\"></i></div></div><div class=\\"label_below obj_label\\" id=\\"label_4\\"><label id=\\"lbl_4\\">Message</label></div><div class=\\"input_position_below\\" id=\\"obj_fields_4\\"><textarea data-type=\\"longtext\\" class=\\"form-textarea getFields getFields_4\\" disabled=\\"disabled\\" name=\\"Message\\" id=\\"getFields_4\\" style=\\"resize:none;\\" placeholder=\\"Message\\" data-original-title=\\"Message\\"></textarea></div></div><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div>","form_json":{"1":{"lblFldName":"POST"},"2":{"lblFldName":"REMOVE"},"3":{"lblName":"Subject","lblFldName":"Subject","lblPlaceHolder":"Subject","lblTooltip":"Subject"},"4":{"lblName":"Message","lblFldName":"Message","lblPlaceHolder":"Message","lblTooltip":"Message"},"saving_request_format":{"fields":[{"id":"Subject","label":"Subject","generation":"REQUESTOR","type":"TEXT_FIELD"},{"id":"Message","label":"Message","generation":"REQUESTOR","type":"TEXT_AREA"}]},"saved_mobile_content":"<li class=\\"element-mobile\\" style=\\"position:relative;display:inline-block;width:100%;\\"><div class=\\"setObject cursor_move ui-draggable ui-resizable\\" data-type=\\"textbox\\" data-object-id=\\"3\\" id=\\"setObject_3\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"border-color: transparent; position: relative;\\"><div class=\\"fields_below\\" style=\\"min-width: 200px; width: 100%;\\"><div class=\\"setObjects_actions\\"><div class=\\"pull-left\\"><img src=\\"/images/loader/load.gif\\" class=\\"display\\" id=\\"obj_load_3\\"></div><div class=\\"obj_actions display\\" id=\\"obj_actions_3\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"1\\" id=\\"object_properties_3\\" data-object-id=\\"3\\" data-object-type=\\"textbox\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"3\\" data-original-title=\\"Remove\\"></i></div></div><div class=\\"label_below obj_label\\" id=\\"label_3\\"><label id=\\"lbl_3\\">Subject</label></div><div class=\\"input_position_below\\" id=\\"obj_fields_3\\"><input data-type=\\"longtext\\" type=\\"text\\" disabled=\\"disabled\\" class=\\"tip form-text getFields getFields_3\\" name=\\"Subject\\" id=\\"getFields_3\\" data-placement=\\"bottom\\" placeholder=\\"Subject\\" data-original-title=\\"Subject\\"></div></div><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div></li><li class=\\"element-mobile\\" style=\\"position:relative;display:inline-block;width:100%;\\"><div class=\\"setObject cursor_move ui-draggable ui-resizable\\" data-type=\\"textArea\\" data-object-id=\\"4\\" id=\\"setObject_4\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"border-color: transparent; position: relative;\\"><div class=\\"fields_below\\" style=\\"min-width: 200px; width: 100%;\\"><div class=\\"setObjects_actions\\"><div class=\\"pull-left\\"><img src=\\"/images/loader/load.gif\\" class=\\"display\\" id=\\"obj_load_4\\"></div><div class=\\"obj_actions display\\" id=\\"obj_actions_4\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"1\\" id=\\"object_properties_4\\" data-object-id=\\"4\\" data-object-type=\\"textarea\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"4\\" data-original-title=\\"Remove\\"></i></div></div><div class=\\"label_below obj_label\\" id=\\"label_4\\"><label id=\\"lbl_4\\">Message</label></div><div class=\\"input_position_below\\" id=\\"obj_fields_4\\"><textarea data-type=\\"longtext\\" class=\\"form-textarea getFields getFields_4\\" disabled=\\"disabled\\" name=\\"Message\\" id=\\"getFields_4\\" style=\\"resize:none;\\" placeholder=\\"Message\\" data-original-title=\\"Message\\"></textarea></div></div><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div></li>","workspace_title":"Discussion","workspace_displayName":"Discussion","workspace_prefix":"Discussion","workspace_type":"Sequential"},"form_fields":"[{\\"fieldName\\":\\"Subject\\",\\"fieldType\\":\\"longtext\\"},{\\"fieldName\\":\\"Message\\",\\"fieldType\\":\\"longtext\\"}]","MobileJsonData":"{\\"fields\\":[{\\"id\\":\\"Subject\\",\\"label\\":\\"Subject\\",\\"generation\\":\\"REQUESTOR\\",\\"type\\":\\"TEXT_FIELD\\"},{\\"id\\":\\"Message\\",\\"label\\":\\"Message\\",\\"generation\\":\\"REQUESTOR\\",\\"type\\":\\"TEXT_AREA\\"}]}","MobileContent":"<li class=\\"element-mobile\\" style=\\"position:relative;display:inline-block;width:100%;\\"><div class=\\"setObject cursor_move ui-draggable ui-resizable\\" data-type=\\"textbox\\" data-object-id=\\"3\\" id=\\"setObject_3\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"border-color: transparent; position: relative;\\"><div class=\\"fields_below\\" style=\\"min-width: 200px; width: 100%;\\"><div class=\\"setObjects_actions\\"><div class=\\"pull-left\\"><img src=\\"/images/loader/load.gif\\" class=\\"display\\" id=\\"obj_load_3\\"></div><div class=\\"obj_actions display\\" id=\\"obj_actions_3\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"1\\" id=\\"object_properties_3\\" data-object-id=\\"3\\" data-object-type=\\"textbox\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"3\\" data-original-title=\\"Remove\\"></i></div></div><div class=\\"label_below obj_label\\" id=\\"label_3\\"><label id=\\"lbl_3\\">Subject</label></div><div class=\\"input_position_below\\" id=\\"obj_fields_3\\"><input data-type=\\"longtext\\" type=\\"text\\" disabled=\\"disabled\\" class=\\"tip form-text getFields getFields_3\\" name=\\"Subject\\" id=\\"getFields_3\\" data-placement=\\"bottom\\" placeholder=\\"Subject\\" data-original-title=\\"Subject\\"></div></div><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div></li><li class=\\"element-mobile\\" style=\\"position:relative;display:inline-block;width:100%;\\"><div class=\\"setObject cursor_move ui-draggable ui-resizable\\" data-type=\\"textArea\\" data-object-id=\\"4\\" id=\\"setObject_4\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"border-color: transparent; position: relative;\\"><div class=\\"fields_below\\" style=\\"min-width: 200px; width: 100%;\\"><div class=\\"setObjects_actions\\"><div class=\\"pull-left\\"><img src=\\"/images/loader/load.gif\\" class=\\"display\\" id=\\"obj_load_4\\"></div><div class=\\"obj_actions display\\" id=\\"obj_actions_4\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"1\\" id=\\"object_properties_4\\" data-object-id=\\"4\\" data-object-type=\\"textarea\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"4\\" data-original-title=\\"Remove\\"></i></div></div><div class=\\"label_below obj_label\\" id=\\"label_4\\"><label id=\\"lbl_4\\">Message</label></div><div class=\\"input_position_below\\" id=\\"obj_fields_4\\"><textarea data-type=\\"longtext\\" class=\\"form-textarea getFields getFields_4\\" disabled=\\"disabled\\" name=\\"Message\\" id=\\"getFields_4\\" style=\\"resize:none;\\" placeholder=\\"Message\\" data-original-title=\\"Message\\"></textarea></div></div><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div></li>","existing_fields":"Subject,Message"}', '2013-09-19 17:49:02', 1, 'Subject,Message', '2013-09-19 17:49:02', '{"fields":[{"id":"Subject","label":"Subject","generation":"REQUESTOR","type":"TEXT_FIELD"},{"id":"Message","label":"Message","generation":"REQUESTOR","type":"TEXT_AREA"}]}', '<li class="element-mobile" style="position:relative;display:inline-block;width:100%;"><div class="setObject cursor_move ui-draggable ui-resizable" data-type="textbox" data-object-id="3" id="setObject_3" data-toggle="popover" data-placement="right" style="border-color: transparent; position: relative;"><div class="fields_below" style="min-width: 200px; width: 100%;"><div class="setObjects_actions"><div class="pull-left"><img src="/images/loader/load.gif" class="display" id="obj_load_3"></div><div class="obj_actions display" id="obj_actions_3"><i class="icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_3" data-object-id="3" data-object-type="textbox" data-original-title="Properties"></i><i class="icon-remove cursor object_setup object_remove" data-object-id="3" data-original-title="Remove"></i></div></div><div class="label_below obj_label" id="label_3"><label id="lbl_3">Subject</label></div><div class="input_position_below" id="obj_fields_3"><input data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_3" name="Subject" id="getFields_3" data-placement="bottom" placeholder="Subject" data-original-title="Subject"></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div></div></li><li class="element-mobile" style="position:relative;display:inline-block;width:100%;"><div class="setObject cursor_move ui-draggable ui-resizable" data-type="textArea" data-object-id="4" id="setObject_4" data-toggle="popover" data-placement="right" style="border-color: transparent; position: relative;"><div class="fields_below" style="min-width: 200px; width: 100%;"><div class="setObjects_actions"><div class="pull-left"><img src="/images/loader/load.gif" class="display" id="obj_load_4"></div><div class="obj_actions display" id="obj_actions_4"><i class="icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_4" data-object-id="4" data-object-type="textarea" data-original-title="Properties"></i><i class="icon-remove cursor object_setup object_remove" data-object-id="4" data-original-title="Remove"></i></div></div><div class="label_below obj_label" id="label_4"><label id="lbl_4">Message</label></div><div class="input_position_below" id="obj_fields_4"><textarea data-type="longtext" class="form-textarea getFields getFields_4" disabled="disabled" name="Message" id="getFields_4" style="resize:none;" placeholder="Message" data-original-title="Message"></textarea></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div></div></li>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
