-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2013 at 01:48 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms`
--
CREATE DATABASE IF NOT EXISTS `gs3_eforms` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `gs3_eforms`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ProcessWorkflow`(IN tblName VARCHAR(255), IN id VARCHAR(255))
BEGIN
    SET @strSQL = CONCAT(
      'SELECT 
        WFO.type_rel, 
        WFO.Status,
        WFO.buttonStatus, 
        CASE 
        WHEN WFO.ProcessorType = ''1'' THEN 
            CASE 
                WHEN PROCESSOR.department_position_level = ''1'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                       (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''1''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''2'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''3'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''2'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    END
                END
        WHEN WFO.ProcessorType = ''2'' THEN (SELECT group_concat(user_processor.id separator '','') FROM tbuser user_processor WHERE user_processor.Position = WFO.Processor)
        WHEN WFO.ProcessorType = ''3'' THEN WFO.Processor
        ELSE
        FRM.Requestor END AS Processor,
        WFO.condition_return,
        WFO.field,
        WFO.operator,
        WFO.field_value,
        WFO.fieldEnabled,
        WFO.fieldRequired,
        WFO.fieldHiddenValue
        INTO @NodeType, @Status, @Buttons, @Processor, @ConditionAction, @Field, @Operator, @FieldValue, @FieldEnabled, @FieldRequired, @FieldHiddenValue
      FROM ', tblName,
      ' FRM LEFT JOIN tbworkflow WF 
      ON WF.Id = FRM.Workflow_Id
      LEFT JOIN tbworkflow_objects WFO
      ON WFO.Workflow_Id = WF.Id AND WFO.Object_Id = FRM.Node_Id
      LEFT JOIN tbuser PROCESSOR
      ON PROCESSOR.Id = FRM.Processor
      WHERE FRM.ID = ', id); 
    
    PREPARE strSQl FROM @strSQL;
    EXECUTE strSQL;
    
	SELECT @NodeType as NodeType, @Status as Status, @Processor as Processor, @Buttons as Buttons, @ConditionAction as ConditionAction, @Field as Field , @Operator as Operator, @FieldValue as FieldValue, @FieldEnabled as FieldEnabled, @FieldRequired as FieldRequired, @FieldHiddenValue as FieldHiddenValue ;
	DEALLOCATE  PREPARE strSQl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Sample`()
BEGIN
	SELECT * FROM tbuser;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `1_tbl_testing123`
--

CREATE TABLE IF NOT EXISTS `1_tbl_testing123` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TrackNo` longtext,
  `Requestor` longtext,
  `Status` longtext,
  `Processor` longtext,
  `LastAction` longtext,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` longtext,
  `UpdatedBy` longtext,
  `Unread` longtext,
  `Node_ID` longtext,
  `Workflow_ID` longtext,
  `fieldEnabled` longtext,
  `fieldRequired` longtext,
  `fieldHiddenValues` longtext,
  `imported` longtext,
  `textbox_1` longtext,
  `Name` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `1_tbl_testing123`
--

INSERT INTO `1_tbl_testing123` (`ID`, `TrackNo`, `Requestor`, `Status`, `Processor`, `LastAction`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`, `Unread`, `Node_ID`, `Workflow_ID`, `fieldEnabled`, `fieldRequired`, `fieldHiddenValues`, `imported`, `textbox_1`, `Name`) VALUES
(1, 'Testing1230001', '1', 'Submitted', '5', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-22 14:24:54', NULL, '1', '1', '', 'node_2', '1', '[]', '[]', '[]', '', NULL, 'Hello'),
(2, 'Testing1230002', '5', 'Submitted', '5', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-22 15:07:12', NULL, '5', '5', '', 'node_2', '1', '[]', '[]', '[]', '', NULL, 'Hello World'),
(3, 'Testing1230003', '2', 'Submitted', '5', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-22 15:55:19', NULL, '2', '2', '', 'node_2', '1', '[]', '[]', '[]', '', NULL, 'Hello Sample'),
(4, 'Testing1230004', '3', 'Submitted', '5', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-22 15:56:39', NULL, '3', '3', '', 'node_2', '1', '[]', '[]', '[]', '', NULL, 'Testing'),
(5, 'Testing1230005', '1', 'Submitted', '2,3,4,5,6, 7,8,9,10,11', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-22 20:31:58', NULL, '1', '1', '', 'node_2', '2', '[]', '[]', '[]', '', NULL, 'aa'),
(6, 'Testing1230006', '1', 'Submitted', '5', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-22 20:43:16', NULL, '1', '1', '', 'node_2', '3', '[]', '[]', '[]', '', NULL, 'Hello'),
(7, 'Testing1230007', '1', 'Submitted', '5', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-22 21:15:57', NULL, '1', '1', '', 'node_2', '3', '[]', '[]', '[]', '', NULL, 'asd'),
(8, 'Testing1230008', '1', 'Submitted', '2,3,4,5,6,7,8,9,10,11', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', '2013-09-22 21:24:09', NULL, '1', '1', '', 'node_2', '4', '[]', '[]', '[]', '', NULL, 'asd'),
(9, 'Testing1230009', '1', 'Approved', '1', 'null', '2013-09-22 21:29:20', NULL, '1', '1', '', 'node_3', '5', 'null', 'null', 'null', '', NULL, 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `tbaudit_action`
--

CREATE TABLE IF NOT EXISTS `tbaudit_action` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `value` varchar(1000) NOT NULL,
  `type` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbaudit_logs`
--

CREATE TABLE IF NOT EXISTS `tbaudit_logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `audit_action` int(255) NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `record_id` int(255) NOT NULL,
  `date` datetime NOT NULL,
  `ip` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `tbaudit_logs`
--

INSERT INTO `tbaudit_logs` (`id`, `user_id`, `audit_action`, `table_name`, `record_id`, `date`, `ip`, `is_active`) VALUES
(1, 1, 3, 'tbuser', 1, '2013-09-16 11:45:21', '192.168.0.71', 1),
(2, 9, 3, 'tbuser', 9, '2013-09-16 11:45:46', '192.168.0.71', 1),
(3, 5, 3, 'tbuser', 5, '2013-09-16 13:48:22', '192.168.0.124', 1),
(4, 5, 3, 'tbuser', 5, '2013-09-16 14:14:52', '192.168.0.124', 1),
(5, 1, 2, 'tbuser', 1, '2013-09-16 15:10:09', '127.0.0.1', 1),
(6, 1, 3, 'tbuser', 1, '2013-09-16 16:05:51', '127.0.0.1', 1),
(7, 1, 3, 'tbuser', 1, '2013-09-16 16:08:04', '127.0.0.1', 1),
(8, 3, 3, 'tbuser', 3, '2013-09-16 18:56:39', '127.0.0.1', 1),
(9, 5, 2, 'tbuser', 5, '2013-09-17 16:27:33', '127.0.0.1', 1),
(10, 1, 3, 'tbuser', 1, '2013-09-18 09:13:16', '127.0.0.1', 1),
(11, 0, 3, 'tbuser', 0, '2013-09-18 09:13:18', '127.0.0.1', 1),
(12, 0, 3, 'tbuser', 0, '2013-09-18 09:13:19', '127.0.0.1', 1),
(13, 0, 3, 'tbuser', 0, '2013-09-18 09:13:20', '127.0.0.1', 1),
(14, 0, 3, 'tbuser', 0, '2013-09-18 09:13:20', '127.0.0.1', 1),
(15, 0, 3, 'tbuser', 0, '2013-09-18 09:13:20', '127.0.0.1', 1),
(16, 0, 3, 'tbuser', 0, '2013-09-18 09:13:20', '127.0.0.1', 1),
(17, 5, 3, 'tbuser', 5, '2013-09-18 09:16:37', '127.0.0.1', 1),
(18, 1, 4, 'tbpost', 1, '2013-09-19 10:30:32', '127.0.0.1', 1),
(19, 1, 5, 'tbcomment', 1, '2013-09-19 12:39:27', '127.0.0.1', 1),
(20, 1, 5, 'tbcomment', 2, '2013-09-19 13:21:59', '127.0.0.1', 1),
(21, 5, 2, 'tbuser', 5, '2013-09-20 08:41:59', '127.0.0.1', 1),
(22, 5, 3, 'tbuser', 5, '2013-09-20 08:43:30', '127.0.0.1', 1),
(23, 1, 2, 'tbuser', 1, '2013-09-20 10:39:40', '192.168.0.119', 1),
(24, 1, 3, 'tbuser', 1, '2013-09-20 16:23:55', '192.168.0.126', 1),
(25, 5, 3, 'tbuser', 5, '2013-09-20 16:25:15', '192.168.0.126', 1),
(26, 1, 3, 'tbuser', 1, '2013-09-20 16:25:29', '127.0.0.1', 1),
(27, 5, 3, 'tbuser', 5, '2013-09-20 16:44:08', '127.0.0.1', 1),
(28, 5, 3, 'tbuser', 5, '2013-09-22 21:03:15', '127.0.0.1', 1),
(29, 3, 3, 'tbuser', 3, '2013-09-22 21:03:23', '127.0.0.1', 1),
(30, 3, 3, 'tbuser', 3, '2013-09-22 21:29:05', '127.0.0.1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbcomment`
--

CREATE TABLE IF NOT EXISTS `tbcomment` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `post_id` int(255) NOT NULL,
  `comment` longtext NOT NULL,
  `postedBy` int(255) NOT NULL,
  `commentType` tinyint(1) NOT NULL COMMENT '0 = default, 1 = request',
  `date_posted` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `fID` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbcomment`
--

INSERT INTO `tbcomment` (`id`, `post_id`, `comment`, `postedBy`, `commentType`, `date_posted`, `is_active`, `fID`) VALUES
(1, 4, 'sdfdsf', 1, 1, '2013-09-19 12:39:27', 1, 0),
(2, 4, 'sample comment', 1, 1, '2013-09-19 13:21:59', 1, 0),
(3, 4, 'test comment yehet!', 1, 1, '2013-09-19 14:46:44', 1, 1),
(4, 4, '', 5, 1, '2013-09-19 15:29:28', 1, 1),
(5, 4, 'hkguncxvjhfvjh', 5, 1, '2013-09-19 15:45:33', 1, 1),
(6, 1, 'test comment...\n\n\nusjdgajsbbfjufkd', 5, 1, '2013-09-19 15:47:47', 1, 4),
(7, 1, 'fhvvbj\nbj\ngjkuf\ngukg\n\n\nchkki', 5, 1, '2013-09-19 16:03:40', 1, 4),
(8, 2, 'Nice, we&#039;re looking forward to this.', 5, 1, '2013-09-19 16:16:15', 1, 4),
(9, 6, 'asdasd', 1, 1, '2013-09-20 16:55:49', 1, 1),
(10, 8, 'comment 1', 5, 1, '2013-09-20 17:06:43', 1, 1),
(11, 8, 'comment 2', 5, 1, '2013-09-20 17:07:07', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbcompany`
--

CREATE TABLE IF NOT EXISTS `tbcompany` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact_number` varchar(30) NOT NULL,
  `primary_contact` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `date_registered` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbcompany`
--

INSERT INTO `tbcompany` (`id`, `code`, `name`, `contact_number`, `primary_contact`, `email`, `extension`, `date_registered`, `is_active`) VALUES
(1, 'GS3-2008', 'Global Strategic Solution & Services Inc.', '9999055639', '', 'samuel_boa2000@yahoo.com', '', '2013-08-23 09:41:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblike`
--

CREATE TABLE IF NOT EXISTS `tblike` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `post_id` int(255) NOT NULL,
  `likeType` varchar(50) NOT NULL,
  `date_liked` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbmessage`
--

CREATE TABLE IF NOT EXISTS `tbmessage` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `title` varchar(1000) NOT NULL,
  `message` longtext NOT NULL,
  `senderID` int(255) NOT NULL,
  `recipient` varchar(1000) NOT NULL,
  `user_read` tinyint(1) NOT NULL,
  `rep` int(255) NOT NULL,
  `date_submit` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tborgchart`
--

CREATE TABLE IF NOT EXISTS `tborgchart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `date` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `reasonForRevision` longtext NOT NULL,
  `json` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tborgchart`
--

INSERT INTO `tborgchart` (`id`, `company_id`, `title`, `description`, `date`, `status`, `is_active`, `reasonForRevision`, `json`) VALUES
(1, 1, '1', '1', '2013-09-16  11:10:57', 0, 1, '', ''),
(2, 1, 'asd', 'asd', '2013-09-16  11:11:26', 0, 1, '', ''),
(3, 1, 'sa', 'sa', '2013-09-16  11:12:06', 0, 1, '', ''),
(4, 1, 'Orgchart 2', 'Orgchart 2', '2013-09-21  16:37:29', 1, 1, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tborgchartline`
--

CREATE TABLE IF NOT EXISTS `tborgchartline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orgChart_id` varchar(50) NOT NULL,
  `parent` varchar(50) NOT NULL,
  `child` varchar(50) NOT NULL,
  `json` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tborgchartline`
--

INSERT INTO `tborgchartline` (`id`, `orgChart_id`, `parent`, `child`, `json`) VALUES
(1, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":199,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":null}'),
(2, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":null}'),
(3, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":341,"width":3,"height":108,"background-color":"#424242","line-type":"childLine","json_title":null}'),
(4, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":176,"left":334,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":null,"line-type":"arrow"}'),
(5, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":199,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":null}'),
(6, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":null}'),
(7, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":341,"width":3,"height":108,"background-color":"#424242","line-type":"childLine","json_title":null}'),
(8, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":176,"left":334,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":null,"line-type":"arrow"}'),
(9, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":199,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":null}'),
(10, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":null}'),
(11, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":75,"left":341,"width":3,"height":108,"background-color":"#424242","line-type":"childLine","json_title":null}'),
(12, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":176,"left":334,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":null,"line-type":"arrow"}'),
(13, '4', 'node_3', 'node_2', '{"line-class":"node_3_node_2","parent":"node_3","child":"node_2","top":120,"left":302,"width":78,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":null}'),
(14, '4', 'node_3', 'node_2', '{"line-class":"node_3_node_2","parent":"node_3","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":null}'),
(15, '4', 'node_3', 'node_2', '{"line-class":"node_3_node_2","parent":"node_3","child":"node_2","top":120,"left":302,"width":3,"height":84,"background-color":"#424242","line-type":"childLine","json_title":null}'),
(16, '4', 'node_3', 'node_2', '{"line-class":"node_3_node_2","parent":"node_3","child":"node_2","top":197,"left":295,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":null,"line-type":"arrow"}'),
(17, '4', 'node_3', 'node_1', '{"line-class":"node_3_node_1","parent":"node_3","child":"node_1","top":120,"left":510,"width":116,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":null}'),
(18, '4', 'node_3', 'node_1', '{"line-class":"node_3_node_1","parent":"node_3","child":"node_1","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":null}'),
(19, '4', 'node_3', 'node_1', '{"line-class":"node_3_node_1","parent":"node_3","child":"node_1","top":120,"left":626,"width":3,"height":91,"background-color":"#424242","line-type":"childLine","json_title":null}'),
(20, '4', 'node_3', 'node_1', '{"line-class":"node_3_node_1","parent":"node_3","child":"node_1","top":204,"left":619,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":null,"line-type":"arrow"}');

-- --------------------------------------------------------

--
-- Table structure for table `tborgchartobjects`
--

CREATE TABLE IF NOT EXISTS `tborgchartobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orgChart_id` int(11) NOT NULL,
  `object_id` varchar(50) NOT NULL,
  `department` varchar(50) NOT NULL,
  `users` text NOT NULL,
  `head` varchar(255) NOT NULL,
  `assistant` varchar(255) NOT NULL,
  `members` varchar(255) NOT NULL,
  `json` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tborgchartobjects`
--

INSERT INTO `tborgchartobjects` (`id`, `orgChart_id`, `object_id`, `department`, `users`, `head`, `assistant`, `members`, `json`) VALUES
(1, 1, 'node_1', 'Admin', '', '', '', '', '{"node_left":61,"node_top":15,"node_data_id":"node_1","node_text":"Admin","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["3"],"orgchart_dept_assistant":["7"],"orgchart_dept_members":["6","9","8"]}'),
(2, 1, 'node_2', 'Members', '', '', '', '', '{"node_left":263,"node_top":163,"node_data_id":"node_2","node_text":"Members","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["5"],"orgchart_dept_assistant":["4"],"orgchart_dept_members":["1","11","10","2"]}'),
(3, 2, 'node_1', 'Admin', '', '', '', '', '{"node_left":61,"node_top":15,"node_data_id":"node_1","node_text":"Admin","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["3"],"orgchart_dept_assistant":["7"],"orgchart_dept_members":["6","9","8"]}'),
(4, 2, 'node_2', 'Members', '', '', '', '', '{"node_left":263,"node_top":163,"node_data_id":"node_2","node_text":"Members","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["5"],"orgchart_dept_assistant":["4"],"orgchart_dept_members":["1","11","10","2"]}'),
(5, 3, 'node_1', 'Admin', '', '', '', '', '{"node_left":61,"node_top":15,"node_data_id":"node_1","node_text":"Admin","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["3"],"orgchart_dept_assistant":["7"],"orgchart_dept_members":["6","9","8"]}'),
(6, 3, 'node_2', 'Members', '', '', '', '', '{"node_left":263,"node_top":163,"node_data_id":"node_2","node_text":"Members","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["5"],"orgchart_dept_assistant":["4"],"orgchart_dept_members":["1","11","10","2"]}'),
(7, 4, 'node_1', 'I.T.', '', '', '', '', '{"node_left":548,"node_top":188,"node_data_id":"node_1","node_text":"I.T.","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["4"],"orgchart_dept_assistant":["1"],"orgchart_dept_members":["11"]}'),
(8, 4, 'node_2', 'Sales', '', '', '', '', '{"node_left":224,"node_top":181,"node_data_id":"node_2","node_text":"Sales","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["9"],"orgchart_dept_assistant":["8"],"orgchart_dept_members":["5"]}'),
(9, 4, 'node_3', 'Administrator', '', '', '', '', '{"node_left":372,"node_top":57,"node_data_id":"node_3","node_text":"Administrator","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":true,"node_drop":true,"node_settings_property":"organizational_chart","node_hasDrag":true,"type_rel":0,"orgchart_user_head":["3"],"orgchart_dept_assistant":["7"],"orgchart_dept_members":["6"]}');

-- --------------------------------------------------------

--
-- Table structure for table `tbpost`
--

CREATE TABLE IF NOT EXISTS `tbpost` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `post` longtext NOT NULL,
  `postedBy` int(255) NOT NULL,
  `privacy` varchar(100) NOT NULL,
  `date_posted` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbpost`
--

INSERT INTO `tbpost` (`id`, `post`, `postedBy`, `privacy`, `date_posted`, `is_active`) VALUES
(1, '12', 1, '', '2013-09-19 10:30:32', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbreply_message`
--

CREATE TABLE IF NOT EXISTS `tbreply_message` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `message_id` int(255) NOT NULL,
  `sender` int(255) NOT NULL,
  `message` longtext NOT NULL,
  `reps` int(255) NOT NULL,
  `user_read` tinyint(1) NOT NULL,
  `date_submit` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbuser`
--

CREATE TABLE IF NOT EXISTS `tbuser` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `position` varchar(100) NOT NULL,
  `company_id` int(255) NOT NULL,
  `user_level_id` int(255) NOT NULL,
  `department_position_level` int(255) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `date_registered` datetime NOT NULL,
  `email_activate` tinyint(1) NOT NULL,
  `is_available` tinyint(4) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbuser`
--

INSERT INTO `tbuser` (`id`, `email`, `display_name`, `first_name`, `middle_name`, `last_name`, `contact_number`, `position`, `company_id`, `user_level_id`, `department_position_level`, `password`, `extension`, `date_registered`, `email_activate`, `is_available`, `is_active`, `department_id`) VALUES
(1, 'samuel_boa2000@yahoo.com', 'Ka Admin', 'Jolly', '', 'Dela Cruz', '9999055639', 'CEO', 1, 2, 2, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', 'png', '2013-08-23 09:41:58', 1, 0, 1, 7),
(2, 'samuel.pulta@gs3.com.ph', 'Ka Sammy', 'Samuel', '', 'Pulta', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', 'png', '2013-08-23 09:56:39', 0, 0, 1, 6),
(3, 'Aaron.Tolentino@gs3.com.ph', 'Ka Aaron', 'Aaron', '', 'Tolentino', '', 'Developer', 1, 3, 1, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:56:47', 0, 0, 1, 9),
(4, 'jeweltolentino@gs3.com.ph', 'Ka Jewel', 'Jewel', '', 'Tolentino', '', 'Developer', 1, 3, 1, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:56:56', 0, 0, 1, 7),
(5, 'ervinnesodusta@gs3.com.ph', 'Ka Lalabs', 'Ervinne', '', 'Sodusta', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:02', 0, 0, 1, 8),
(6, 'alvin.romero@gs3.com.ph', 'Ka Alvin', 'Alvin', '', 'Romero', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:08', 0, 0, 1, 9),
(7, 'alfredo.magpantay@gs3.com.ph', 'Ka Alfred', 'Alfredo', '', 'Magpantay', '', 'Developer', 1, 3, 2, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:13', 0, 0, 1, 9),
(8, 'edgar.palmon@gs3.com.ph', 'Ka Edgar', 'Edgar', '', 'Palmon', '', 'Developer', 1, 3, 2, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:19', 0, 0, 1, 8),
(9, 'Aramis.Marquez@gs3.com.ph', 'Ka Aramis', 'Aramis', '', 'Marquez', '', 'Developer', 1, 3, 1, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:24', 0, 0, 1, 8),
(10, 'rowi.dizon@gs3.com.ph', 'Ka Rowi', 'Rowi', '', 'Dizon', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:29', 0, 0, 1, 6),
(11, 'michael.espano@gs3.com.ph', 'Ka Panda', 'Michael', '', 'Espano', '', 'Developer', 1, 3, 3, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-08-23 09:57:33', 0, 0, 1, 7);

-- --------------------------------------------------------

--
-- Table structure for table `tbuser_level`
--

CREATE TABLE IF NOT EXISTS `tbuser_level` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_level` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbuser_level`
--

INSERT INTO `tbuser_level` (`id`, `user_level`, `is_active`) VALUES
(1, 'Administrator', 1),
(2, 'Company Admin', 1),
(3, 'Users', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbuser_position_level`
--

CREATE TABLE IF NOT EXISTS `tbuser_position_level` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `position` varchar(100) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbuser_position_level`
--

INSERT INTO `tbuser_position_level` (`id`, `position`, `is_active`) VALUES
(1, 'Head', 1),
(2, 'Assistant Head', 1),
(3, 'Member', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbworkflow`
--

CREATE TABLE IF NOT EXISTS `tbworkflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `date` varchar(20) NOT NULL,
  `delegateRequest` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbworkflow`
--

INSERT INTO `tbworkflow` (`id`, `form_id`, `title`, `description`, `date`, `delegateRequest`, `is_active`) VALUES
(1, 1, 'Testing123', 'Testing123', '2013-09-22  13:19:29', 0, 0),
(2, 1, 'Testing123', 'Testing123', '2013-09-22  20:31:27', 0, 0),
(3, 1, 'Testing123', 'Testing123', '2013-09-22  20:42:59', 0, 0),
(4, 1, 'Testing123', 'Testing123', '2013-09-22  21:23:58', 0, 0),
(5, 1, 'Testing123', 'Testing123', '2013-09-22  21:26:04', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbworkflow_lines`
--

CREATE TABLE IF NOT EXISTS `tbworkflow_lines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` varchar(50) NOT NULL,
  `parent` varchar(50) NOT NULL,
  `child` varchar(50) NOT NULL,
  `json` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `tbworkflow_lines`
--

INSERT INTO `tbworkflow_lines` (`id`, `workflow_id`, `parent`, `child`, `json`) VALUES
(1, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":98,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Submit"}'),
(2, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Submit"}'),
(3, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":240,"width":3,"height":86,"background-color":"#424242","line-type":"childLine","json_title":"Submit"}'),
(4, '1', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":147,"left":233,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Submit","line-type":"arrow"}'),
(5, '1', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":300,"width":141,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Approved"}'),
(6, '1', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Approved"}'),
(7, '1', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":441,"width":3,"height":65,"background-color":"#424242","line-type":"childLine","json_title":"Approved"}'),
(8, '1', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":252,"left":434,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Approved","line-type":"arrow"}'),
(9, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":98,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Submit"}'),
(10, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Submit"}'),
(11, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":240,"width":3,"height":86,"background-color":"#424242","line-type":"childLine","json_title":"Submit"}'),
(12, '2', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":147,"left":233,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Submit","line-type":"arrow"}'),
(13, '2', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":300,"width":141,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Approved"}'),
(14, '2', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Approved"}'),
(15, '2', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":441,"width":3,"height":65,"background-color":"#424242","line-type":"childLine","json_title":"Approved"}'),
(16, '2', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":252,"left":434,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Approved","line-type":"arrow"}'),
(17, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":98,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Submit"}'),
(18, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Submit"}'),
(19, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":240,"width":3,"height":86,"background-color":"#424242","line-type":"childLine","json_title":"Submit"}'),
(20, '3', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":147,"left":233,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Submit","line-type":"arrow"}'),
(21, '3', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":300,"width":141,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Approved"}'),
(22, '3', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Approved"}'),
(23, '3', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":441,"width":3,"height":65,"background-color":"#424242","line-type":"childLine","json_title":"Approved"}'),
(24, '3', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":252,"left":434,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Approved","line-type":"arrow"}'),
(25, '4', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":98,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Submit"}'),
(26, '4', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Submit"}'),
(27, '4', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":240,"width":3,"height":86,"background-color":"#424242","line-type":"childLine","json_title":"Submit"}'),
(28, '4', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":147,"left":233,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Submit","line-type":"arrow"}'),
(29, '4', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":300,"width":141,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Approved"}'),
(30, '4', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Approved"}'),
(31, '4', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":441,"width":3,"height":65,"background-color":"#424242","line-type":"childLine","json_title":"Approved"}'),
(32, '4', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":252,"left":434,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Approved","line-type":"arrow"}'),
(33, '5', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":98,"width":142,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Submit"}'),
(34, '5', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Submit"}'),
(35, '5', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":68,"left":240,"width":3,"height":86,"background-color":"#424242","line-type":"childLine","json_title":"Submit"}'),
(36, '5', 'node_1', 'node_2', '{"line-class":"node_1_node_2","parent":"node_1","child":"node_2","top":147,"left":233,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Submit","line-type":"arrow"}'),
(37, '5', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":300,"width":141,"height":3,"background-color":"#424242","line-type":"parentLine","json_title":"Approved"}'),
(38, '5', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":0,"left":0,"width":0,"height":0,"background-color":"#424242","line-type":"middleLine","json_title":"Approved"}'),
(39, '5', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":194,"left":441,"width":3,"height":65,"background-color":"#424242","line-type":"childLine","json_title":"Approved"}'),
(40, '5', 'node_2', 'node_3', '{"line-class":"node_2_node_3","parent":"node_2","child":"node_3","top":252,"left":434,"border":"border-left: 8px solid transparent;border-right: 8px solid transparent;border-top: 8px solid #424242;display:block;","json_title":"Approved","line-type":"arrow"}');

-- --------------------------------------------------------

--
-- Table structure for table `tbworkflow_objects`
--

CREATE TABLE IF NOT EXISTS `tbworkflow_objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workflow_id` int(11) NOT NULL,
  `object_id` varchar(50) NOT NULL,
  `type_rel` varchar(10) NOT NULL,
  `tops` varchar(50) NOT NULL,
  `lefts` varchar(50) NOT NULL,
  `processorType` varchar(50) NOT NULL,
  `processor` varchar(100) NOT NULL,
  `buttonStatus` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `field` varchar(128) NOT NULL,
  `operator` varchar(128) NOT NULL,
  `field_value` varchar(128) NOT NULL,
  `trues` varchar(128) NOT NULL,
  `falses` varchar(128) NOT NULL,
  `fieldEnabled` text NOT NULL,
  `fieldRequired` text NOT NULL,
  `email_title` text,
  `email_body` text,
  `email_recipient` text,
  `email_cc` text,
  `fieldHiddenValue` text,
  `email_bcc` text,
  `field_trigger` text,
  `json` longtext NOT NULL,
  `condition_return` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `tbworkflow_objects`
--

INSERT INTO `tbworkflow_objects` (`id`, `workflow_id`, `object_id`, `type_rel`, `tops`, `lefts`, `processorType`, `processor`, `buttonStatus`, `status`, `field`, `operator`, `field_value`, `trues`, `falses`, `fieldEnabled`, `fieldRequired`, `email_title`, `email_body`, `email_recipient`, `email_cc`, `fieldHiddenValue`, `email_bcc`, `field_trigger`, `json`, `condition_return`) VALUES
(1, 1, 'node_1', '1', '', '', '', '', '{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Name"]', '["Name"]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","fieldEnabled":["Name"],"fieldRequired":["Name"],"fieldHiddenValue":[],"buttonStatus":{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(2, 1, 'node_2', '2', '', '', '3', '5', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Submitted', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":162,"node_top":131,"node_data_id":"node_2","node_text":"Ervinne  Sodusta","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"3","processor":"5","status":"Submitted","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":[],"buttonStatus":{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(3, 1, 'node_3', '4', '', '', '', '', 'null', 'Approved', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":383,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"Approved"}', 'null'),
(4, 2, 'node_1', '1', '', '', '', '', '{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Name"]', '["Name"]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","fieldEnabled":["Name"],"fieldRequired":["Name"],"fieldHiddenValue":[],"buttonStatus":{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(5, 2, 'node_2', '2', '', '', '2', 'Developer', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Submitted', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":162,"node_top":131,"node_data_id":"node_2","node_text":"Developer","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"2","processor":"Developer","status":"Submitted","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":[],"buttonStatus":{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(6, 2, 'node_3', '4', '', '', '', '', 'null', 'Approved', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":383,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"Approved"}', 'null'),
(7, 3, 'node_1', '1', '', '', '', '', '{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Name"]', '["Name"]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","fieldEnabled":["Name"],"fieldRequired":["Name"],"fieldHiddenValue":[],"buttonStatus":{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(8, 3, 'node_2', '2', '', '', '3', '5', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Submitted', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":162,"node_top":131,"node_data_id":"node_2","node_text":"Ervinne  Sodusta","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"3","processor":"5","status":"Submitted","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":[],"buttonStatus":{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(9, 3, 'node_3', '4', '', '', '', '', 'null', 'Approved', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":383,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"Approved"}', 'null'),
(10, 4, 'node_1', '1', '', '', '', '', '{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Name"]', '["Name"]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","fieldEnabled":["Name"],"fieldRequired":["Name"],"fieldHiddenValue":[],"buttonStatus":{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(11, 4, 'node_2', '2', '', '', '2', 'Developer', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Submitted', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":162,"node_top":131,"node_data_id":"node_2","node_text":"Developer","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"2","processor":"Developer","status":"Submitted","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":[],"buttonStatus":{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(12, 4, 'node_3', '4', '', '', '', '', 'null', 'Approved', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":383,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"Approved"}', 'null'),
(13, 5, 'node_1', '1', '', '', '', '', '{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}', '', '', '', '', '', '', '["Name"]', '["Name"]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":"0px","node_top":"0px","node_data_id":"node_1","node_text":"Start","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":false,"node_settings_property":"workflow_chart-start","node_hasDrag":true,"type_rel":"1","fieldEnabled":["Name"],"fieldRequired":["Name"],"fieldHiddenValue":[],"buttonStatus":{"Submit":{"button_name":"Submit","child_id":"node_2","parent_id":"node_1","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(14, 5, 'node_2', '2', '', '', '3', '5', '{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}', 'Submitted', '', '', '', '', '', '[]', '[]', NULL, NULL, NULL, NULL, '[]', NULL, NULL, '{"node_left":162,"node_top":131,"node_data_id":"node_2","node_text":"Ervinne  Sodusta","node_type":"box","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"50px","node_width":"130px","node_onDrop":"function (parent,child,addline){\\r\\n            \\/\\/get parent id\\r\\n            var node_data_id = $(parent).closest(\\".outer-node\\").attr(\\"id\\")\\r\\n            \\/\\/get child id\\r\\n            var child_id = $(child).closest(\\".outer-node\\").attr(\\"id\\");\\r\\n            \\/\\/modal type\\r\\n            var object_type = \\"workflow_chart_buttons\\";\\r\\n            \\r\\n            \\/\\/set modal\\r\\n            jDialog(object_properties_node(object_type,node_data_id,child_id), \\"\\",\\"500\\", \\"\\", \\"\\", function(){});\\r\\n            \\r\\n            \\r\\n            \\/\\/set color picker for line\\r\\n            $(\\"#wokflow_button-line-color\\").spectrum({\\r\\n                        color: \\"424242\\"\\r\\n            });  \\r\\n            \\r\\n            \\/\\/end of the function\\r\\n            \\r\\n            \\/\\/click to save json to node\\r\\n            workflow_actions.save_node_settings(function(line_color,json_title){\\r\\n                        addline(line_color,json_title);\\r\\n            })\\r\\n        }","node_drop":true,"node_settings_property":"workflow_chart-processor","node_hasDrag":true,"type_rel":"2","processorType":"3","processor":"5","status":"Submitted","fieldEnabled":[],"fieldRequired":[],"fieldHiddenValue":[],"buttonStatus":{"Approved":{"button_name":"Approved","child_id":"node_3","parent_id":"node_2","require_comment":false,"wokflow_button_line_color":"#424242"}}}', 'null'),
(15, 5, 'node_3', '4', '', '', '', '', 'null', 'Approved', '', '', '', '', '', 'null', 'null', NULL, NULL, NULL, NULL, 'null', NULL, NULL, '{"node_left":383,"node_top":236,"node_data_id":"node_3","node_text":"End","node_type":"circle","node_container":".workspace","node_color":"#424242","node_state":true,"node_height":"80px","node_width":"90px","node_onDrop":true,"node_drop":true,"node_settings_property":"workflow_chart-end","node_hasDrag":false,"type_rel":"4","status":"Approved"}', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `tb_workspace`
--

CREATE TABLE IF NOT EXISTS `tb_workspace` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `form_name` varchar(100) NOT NULL,
  `form_table_name` varchar(100) NOT NULL,
  `form_description` varchar(100) NOT NULL,
  `form_content` text NOT NULL,
  `form_buttons` longtext NOT NULL,
  `created_by` int(255) NOT NULL,
  `updated_by` int(255) NOT NULL,
  `company_id` int(255) NOT NULL,
  `button_content` longtext NOT NULL,
  `reference_prefix` varchar(100) NOT NULL,
  `reference_type` varchar(100) NOT NULL,
  `version` varchar(100) NOT NULL,
  `form_json` longtext NOT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `active_fields` longtext NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `MobileJsonData` longtext NOT NULL,
  `MobileContent` longtext NOT NULL,
  `form_authors` longtext NOT NULL,
  `form_viewers` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tb_workspace`
--

INSERT INTO `tb_workspace` (`id`, `form_name`, `form_table_name`, `form_description`, `form_content`, `form_buttons`, `created_by`, `updated_by`, `company_id`, `button_content`, `reference_prefix`, `reference_type`, `version`, `form_json`, `date_created`, `is_active`, `active_fields`, `date_updated`, `MobileJsonData`, `MobileContent`, `form_authors`, `form_viewers`) VALUES
(1, 'Testing123', '1_tbl_Testing123', 'Testing123', '\n         \n         \n         \n         \n         \n         \n         \n         \n         \n         \n         \n         \n         \n         \n               <div class="setObject cursor_move ui-draggable ui-resizable" data-type="textbox" data-object-id="1" id="setObject_1" data-toggle="popover" data-placement="right" style="border-color: transparent; left: 115px; top: 91px;"><div class="fields_below" style="min-width:200px;"><div class="setObjects_actions"><div class="pull-left"><img src="/images/loader/load.gif" class="display" id="obj_load_1"></div><div class="obj_actions display" id="obj_actions_1"><i class="icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_1" data-object-id="1" data-object-type="textbox" data-original-title="Properties"></i><i class="icon-remove cursor object_setup object_remove" data-object-id="1" data-original-title="Remove"></i></div></div><div class="label_below obj_label" id="label_1"><label id="lbl_1">Name</label></div><div class="input_position_below" id="obj_fields_1"><input data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_1" name="Name" id="getFields_1" data-placement="bottom" data-original-title="" title=""></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div></div>                                                                                    <div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div>', 'Submit,Approved', 1, 1, 1, '\n               \n               \n               \n               \n               \n               \n               \n               \n               \n               \n               \n               \n               \n               \n                           <div data-object-id="2" data-type="button" class="setObject setObject_btn ui-draggable ui-resizable" id="setObject_2" data-toggle="popover" data-placement="right" style="display: inline; margin-left: 3px; margin-top: 5px; position: relative; border-color: transparent;"><input data-type="button" type="button" class="btn-basicBtn " id="btnName_2" value="Submit" style="margin:5px 5px 5px 5px;" name="Submit"><i class="icon-cogs cursor object_setup object_properties" data-properties-type="4" id="object_properties_2" data-object-id="2" data-object-type="button" data-original-title="Properties"></i><i class="icon-remove cursor object_setup object_remove" data-object-id="2" data-original-title="Remove"></i><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div></div>                                                                                                <div data-object-id="3" data-type="button" class="setObject setObject_btn ui-draggable ui-resizable" id="setObject_3" data-toggle="popover" data-placement="right" style="display: inline; margin-left: 3px; margin-top: 5px; position: relative; border-color: transparent;"><input data-type="button" type="button" class="btn-basicBtn " id="btnName_3" value="Approved" style="margin:5px 5px 5px 5px;" name="Approved"><i class="icon-cogs cursor object_setup object_properties" data-properties-type="4" id="object_properties_3" data-object-id="3" data-object-type="button" data-original-title="Properties"></i><i class="icon-remove cursor object_setup object_remove" data-object-id="3" data-original-title="Remove"></i><div class="ui-resizable-handle ui-resizable-n" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 90;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 90;"></div></div>                                                                        ', 'Testing123', 'Sequential', '1', '{"Title":"Testing123","DisplayName":"Testing123","Prefix":"Testing123","Type":"Sequential","WorkspaceHeight":"600","WorkspaceWidth":"1030","BtnLength":2,"BtnName":"Submit,Approved","button_content":"\\n               \\n               \\n               \\n               \\n               \\n               \\n               \\n               \\n               \\n               \\n               \\n               \\n               \\n               \\n                           <div data-object-id=\\"2\\" data-type=\\"button\\" class=\\"setObject setObject_btn ui-draggable ui-resizable\\" id=\\"setObject_2\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"display: inline; margin-left: 3px; margin-top: 5px; position: relative; border-color: transparent;\\"><input data-type=\\"button\\" type=\\"button\\" class=\\"btn-basicBtn \\" id=\\"btnName_2\\" value=\\"Submit\\" style=\\"margin:5px 5px 5px 5px;\\" name=\\"Submit\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"4\\" id=\\"object_properties_2\\" data-object-id=\\"2\\" data-object-type=\\"button\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"2\\" data-original-title=\\"Remove\\"></i><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div>                                                                                                <div data-object-id=\\"3\\" data-type=\\"button\\" class=\\"setObject setObject_btn ui-draggable ui-resizable\\" id=\\"setObject_3\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"display: inline; margin-left: 3px; margin-top: 5px; position: relative; border-color: transparent;\\"><input data-type=\\"button\\" type=\\"button\\" class=\\"btn-basicBtn \\" id=\\"btnName_3\\" value=\\"Approved\\" style=\\"margin:5px 5px 5px 5px;\\" name=\\"Approved\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"4\\" id=\\"object_properties_3\\" data-object-id=\\"3\\" data-object-type=\\"button\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"3\\" data-original-title=\\"Remove\\"></i><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div>                                                                        ","ObjectLength":3,"WorkspaceContent":"\\n         \\n         \\n         \\n         \\n         \\n         \\n         \\n         \\n         \\n         \\n         \\n         \\n         \\n         \\n               <div class=\\"setObject cursor_move ui-draggable ui-resizable\\" data-type=\\"textbox\\" data-object-id=\\"1\\" id=\\"setObject_1\\" data-toggle=\\"popover\\" data-placement=\\"right\\" style=\\"border-color: transparent; left: 115px; top: 91px;\\"><div class=\\"fields_below\\" style=\\"min-width:200px;\\"><div class=\\"setObjects_actions\\"><div class=\\"pull-left\\"><img src=\\"/images/loader/load.gif\\" class=\\"display\\" id=\\"obj_load_1\\"></div><div class=\\"obj_actions display\\" id=\\"obj_actions_1\\"><i class=\\"icon-cogs cursor object_setup object_properties\\" data-properties-type=\\"1\\" id=\\"object_properties_1\\" data-object-id=\\"1\\" data-object-type=\\"textbox\\" data-original-title=\\"Properties\\"></i><i class=\\"icon-remove cursor object_setup object_remove\\" data-object-id=\\"1\\" data-original-title=\\"Remove\\"></i></div></div><div class=\\"label_below obj_label\\" id=\\"label_1\\"><label id=\\"lbl_1\\">Name</label></div><div class=\\"input_position_below\\" id=\\"obj_fields_1\\"><input data-type=\\"longtext\\" type=\\"text\\" disabled=\\"disabled\\" class=\\"tip form-text getFields getFields_1\\" name=\\"Name\\" id=\\"getFields_1\\" data-placement=\\"bottom\\" data-original-title=\\"\\" title=\\"\\"></div></div><div class=\\"ui-resizable-handle ui-resizable-n\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-w\\" style=\\"z-index: 90;\\"></div></div>                                                                                    <div class=\\"ui-resizable-handle ui-resizable-s\\" style=\\"z-index: 90;\\"></div><div class=\\"ui-resizable-handle ui-resizable-e\\" style=\\"z-index: 90;\\"></div>","form_json":{"1":{"lblName":"Name","lblFldName":"Name"},"2":{"lblFldName":"Submit"},"3":{"lblFldName":"Approved"},"workspace_title":"Testing123","workspace_displayName":"Testing123","workspace_prefix":"Testing123","workspace_type":"Sequential","form-authors":{"departments":["7","8","9"],"positions":["CEO","Developer"],"users":["1","2","3","4","5","6","7","8","9","10","11"]},"form-readers":{"departments":[],"positions":[],"users":[]}},"form_fields":"[{\\"fieldName\\":\\"Name\\",\\"fieldType\\":\\"longtext\\"}]","MobileJsonData":"","MobileContent":"","existing_fields":"Name","form_authors":"{\\"departments\\":[\\"7\\",\\"8\\",\\"9\\"],\\"positions\\":[\\"CEO\\",\\"Developer\\"],\\"users\\":[\\"1\\",\\"2\\",\\"3\\",\\"4\\",\\"5\\",\\"6\\",\\"7\\",\\"8\\",\\"9\\",\\"10\\",\\"11\\"]}","form_viewers":"{\\"departments\\":[],\\"positions\\":[],\\"users\\":[]}"}', '2013-09-22 21:32:48', 1, 'Name', '2013-09-22 21:32:48', '', '', '{"departments":["7","8","9"],"positions":["CEO","Developer"],"users":["1","2","3","4","5","6","7","8","9","10","11"]}', '{"departments":[],"positions":[],"users":[]}');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
