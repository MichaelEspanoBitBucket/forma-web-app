DEFAULT COMPUTED VALUE REFERENCE

--> @ThisDate(@fn_date)

----> @ThisDate(@fn_date).format("Y-m-d") NOTE: This formula is use for displaying only

----> @ThisDate(@fn_date).adjustDay(+4) "OR" @ThisDate(@fn_date).adjustDay(-4)			the output is an added or deducted date
----> @ThisDate(@fn_date).adjustMonth(+4) "OR" @ThisDate(@fn_date).adjustMonth(-4)		the output is an added or deducted date
----> @ThisDate(@fn_date).adjustYear(+4) "OR" @ThisDate(@fn_date).adjustYear(-4)		the output is an added or deducted date
----> @ThisDate(@fn_date).adjustHours(+4) "OR" @ThisDate(@fn_date).adjustYear(-4)
----> @ThisDate(@fn_date).adjustMinutes(+4) "OR" @ThisDate(@fn_date).adjustYear(-4)

----> @ThisDate(@fn_date1).diffDaysIn(@fn_date2)	the output is number
----> @ThisDate(@fn_date1).diffWeeksIn(@fn_date2)	the output is number
----> @ThisDate(@fn_date1).diffMonthsIn(@fn_date2)	the output is number
----> @ThisDate(@fn_date1).diffYearsIn(@fn_date2)	the output is number


--> @GivenIf( condition , value_true , value_false )	the output is value_true or value_false based on the given condition




=============================================================
--> @StrRight( fieldname/string, number_of_character_to_get )
		1st param		2nd param

sample: @StrRight("MyName", 3) = "ame"
	@StrRight(@fieldName, 3) = "ael"
============================================================
--> @StrLeft( fieldname/string, number_of_character_to_get )
		1st param		2nd param

sample: @StrLeft("MyName", 3) = "MyN"
	@StrLeft(@fieldName, 3) = "MyN"

====================================================================================
--> @StrGet( fieldname/string , start_index_char_from_left , number_of_char_to_get )
		1st param		2nd param		3rd param

sample: @StrGet("MyName", 1, 2) = "My"
	@StrGet(@fieldName, 1, 2) = "My"

====================================================
--> @StrCount(fieldname/string)
sample: @StrCount("MyName") = 6 character all in all
	@StrGet(@fieldName, 1, 2) = 6

