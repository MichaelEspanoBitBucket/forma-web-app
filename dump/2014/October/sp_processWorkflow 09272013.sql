DELIMITER $$

CREATE PROCEDURE sp_ProcessWorkflow(IN tblName VARCHAR(255), IN id VARCHAR(255))
BEGIN
    SET @strSQL = CONCAT(
      'SELECT 
        WFO.type_rel, 
        WFO.Status,
        WFO.buttonStatus, 
        CASE 
        WHEN WFO.ProcessorType = ''1'' THEN 
            CASE 
                WHEN PROCESSOR.department_position_level = ''1'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                       (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''1''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''2'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''3'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''2'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    END
                END
        WHEN WFO.ProcessorType = ''2'' THEN (SELECT group_concat(user_processor.id separator '', '') FROM tbuser user_processor WHERE user_processor.Position = WFO.Processor)
        WHEN WFO.ProcessorType = ''3'' THEN WFO.Processor
		WHEN WFO.ProcessorType = ''4'' THEN FRM.Requestor
        ELSE
        WFO.Processor END AS Processor,
        WFO.ProcessorType, 
        WFO.condition_return,
        WFO.field,
        WFO.operator,
        WFO.field_value,
        WFO.fieldEnabled,
        WFO.fieldRequired,
        WFO.fieldHiddenValue
        INTO @NodeType, @Status, @Buttons, @Processor, @ProcessorType, @ConditionAction, @Field, @Operator, @FieldValue, @FieldEnabled, @FieldRequired, @FieldHiddenValue
      FROM ', tblName,
      ' FRM LEFT JOIN tbworkflow WF 
      ON WF.Id = FRM.Workflow_Id
      LEFT JOIN tbworkflow_objects WFO
      ON WFO.Workflow_Id = WF.Id AND WFO.Object_Id = FRM.Node_Id
      LEFT JOIN tbuser PROCESSOR
      ON PROCESSOR.Id = FRM.Processor
      WHERE FRM.ID = ', id); 
    
    PREPARE strSQl FROM @strSQL;
    EXECUTE strSQL;
    
    
    IF @ProcessorType = '5' THEN
         SET @fieldValueSQL = CONCAT('SELECT id INTO @Processor FROM tbuser WHERE display_name = (','SELECT ', @Processor, ' FROM ', tblName, ' FRM WHERE FRM.id = ', id, ') ');
         
         PREPARE fieldValueSQL FROM @fieldValueSQL;
         EXECUTE fieldValueSQL;
    END IF;
  
  
   SELECT @NodeType as NodeType, @Status as Status, @Processor as Processor, @Buttons as Buttons, @ConditionAction as ConditionAction, @Field as Field , @Operator as Operator, @FieldValue as FieldValue, @FieldEnabled as FieldEnabled, @FieldRequired as FieldRequired, @FieldHiddenValue as FieldHiddenValue ;
    DEALLOCATE  PREPARE strSQl;
    DEALLOCATE PREPARE fieldValueSQL;
    
END