CREATE TABLE IF NOT EXISTS `tbdashboard` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(45) DEFAULT NULL,
  `ApplicationID` varchar(45) DEFAULT NULL,
  `Content` longtext,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `Is_Active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;