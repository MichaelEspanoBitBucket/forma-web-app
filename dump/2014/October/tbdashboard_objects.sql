CREATE TABLE IF NOT EXISTS `tbdashboard_objects` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DashboardID` varchar(45) DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL,
  `ObjectID` varchar(45) DEFAULT NULL,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` varchar(45) DEFAULT NULL,
  `UpdatedBy` varchar(45) DEFAULT NULL,
  `Is_Active` varchar(45) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;
