-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2013 at 04:24 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms`
--
CREATE DATABASE IF NOT EXISTS `gs3_eforms` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `gs3_eforms`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ProcessWorkflow`(IN tblName VARCHAR(255), IN id VARCHAR(255))
BEGIN
    SET @strSQL = CONCAT(
      'SELECT 
        WFO.type_rel, 
        WFO.Status,
        WFO.buttonStatus, 
        CASE 
        WHEN WFO.ProcessorType = ''1'' THEN 
            CASE 
                WHEN PROCESSOR.department_position_level = ''1'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                       (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''1''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''2'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''3'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''2'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    END
                END
        WHEN WFO.ProcessorType = ''2'' THEN (SELECT group_concat(user_processor.id separator '', '') FROM tbuser user_processor WHERE user_processor.Position = WFO.Processor)
        WHEN WFO.ProcessorType = ''3'' THEN WFO.Processor
        ELSE
        FRM.Requestor END AS Processor,
        WFO.condition_return,
        WFO.field,
        WFO.operator,
        WFO.field_value
        INTO @NodeType, @Status, @Buttons, @Processor, @ConditionAction, @Field, @Operator, @FieldValue
      FROM ', tblName,
      ' FRM LEFT JOIN tbworkflow WF 
      ON WF.Id = FRM.Workflow_Id
      LEFT JOIN tbworkflow_objects WFO
      ON WFO.Workflow_Id = WF.Id AND WFO.Object_Id = FRM.Node_Id
      LEFT JOIN tbuser PROCESSOR
      ON PROCESSOR.Id = FRM.Processor
      WHERE FRM.ID = ', id); 
    
    PREPARE strSQl FROM @strSQL;
    EXECUTE strSQL;
    
	SELECT @NodeType as NodeType, @Status as Status, @Processor as Processor, @Buttons as Buttons, @ConditionAction as ConditionAction, @Field as Field , @Operator as Operator, @FieldValue as FieldValue;
	DEALLOCATE  PREPARE strSQl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Sample`()
BEGIN
	SELECT * FROM tbuser;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbform_category`
--

CREATE TABLE IF NOT EXISTS `tbform_category` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(10000) NOT NULL,
  `company_id` int(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbform_category`
--

INSERT INTO `tbform_category` (`id`, `category_name`, `company_id`, `is_active`) VALUES
(1, 'Requests', 1, 1),
(2, 'Human Resource Form', 1, 1),
(3, 'Customer Support', 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
