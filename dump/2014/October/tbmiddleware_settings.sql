-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 29, 2014 at 03:51 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbmiddleware_settings`
--

CREATE TABLE IF NOT EXISTS `tbmiddleware_settings` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `rulename` varchar(1000) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `formname` varchar(1000) NOT NULL,
  `formula` varchar(1000) NOT NULL,
  `actions` varchar(1000) NOT NULL,
  `scheduled_process_start` datetime NOT NULL,
  `scheduled_process_end` datetime NOT NULL,
  `date_created` date NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
