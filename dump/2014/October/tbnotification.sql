
CREATE TABLE IF NOT EXISTS `tbnotification` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `noti_id` int(255) NOT NULL,
  `type` tinyint(10) NOT NULL,
  `userID` int(255) NOT NULL,
  `noti_by` int(255) NOT NULL,
  `user_read` tinyint(1) NOT NULL,
  `table_name` varchar(1000) NOT NULL,
  `date` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;