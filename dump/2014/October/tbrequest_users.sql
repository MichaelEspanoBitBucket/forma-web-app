CREATE TABLE `tbrequest_users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Form_ID` int(11) DEFAULT NULL,
  `RequestID` int(11) DEFAULT NULL,
  `User` varchar(255) DEFAULT NULL,
  `User_Type` int(11) DEFAULT NULL,
  `Action_Type` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=latin1;
