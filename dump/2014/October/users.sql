-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2013 at 09:28 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms`
--
CREATE DATABASE IF NOT EXISTS `gs3_eforms` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `gs3_eforms`;

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ProcessWorkflow`(IN tblName VARCHAR(255), IN id VARCHAR(255))
BEGIN
    SET @strSQL = CONCAT(
      'SELECT 
        WFO.type_rel, 
        WFO.Status,
        WFO.buttonStatus, 
        CASE 
        WHEN WFO.ProcessorType = ''1'' THEN 
            CASE 
                WHEN PROCESSOR.department_position_level = ''1'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                       (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''1''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''2'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        dep_processor.id
                      FROM tbuser user_processor 
                      LEFT JOIN tborgchartobjects CHILD_DEPARTMENT 
                      ON CHILD_DEPARTMENT.Id = user_processor.department_id
                      LEFT JOIN tborgchartline DEPARMENTLINE
                      ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id
                      LEFT JOIN tborgchartobjects PARENT_DEPARTMENT
                      ON PARENT_DEPARTMENT.object_id  = DEPARMENTLINE.parent
                      LEFT JOIN tbuser dep_processor
                      ON dep_processor.department_id = PARENT_DEPARTMENT.id AND dep_processor.department_position_level = ''2''
                      WHERE user_processor.department_id = PROCESSOR.department_id
                      LIMIT 1)
                    END
                WHEN PROCESSOR.department_position_level = ''3'' THEN
                    CASE WHEN WFO.Processor = ''1'' THEN
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''1'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    ELSE
                      (SELECT 
                        user_processor.id
                      FROM tbuser user_processor 
                      WHERE user_processor.department_position_level = ''2'' 
                      AND user_processor.department_id = PROCESSOR.department_id)
                    END
                END
        WHEN WFO.ProcessorType = ''2'' THEN (SELECT group_concat(user_processor.id separator '', '') FROM tbuser user_processor WHERE user_processor.Position = WFO.Processor)
        WHEN WFO.ProcessorType = ''3'' THEN WFO.Processor
        ELSE
        FRM.Requestor END AS Processor,
        WFO.condition_return,
        WFO.field,
        WFO.operator,
        WFO.field_value
        INTO @NodeType, @Status, @Buttons, @Processor, @ConditionAction, @Field, @Operator, @FieldValue
      FROM ', tblName,
      ' FRM LEFT JOIN tbworkflow WF 
      ON WF.Id = FRM.Workflow_Id
      LEFT JOIN tbworkflow_objects WFO
      ON WFO.Workflow_Id = WF.Id AND WFO.Object_Id = FRM.Node_Id
      LEFT JOIN tbuser PROCESSOR
      ON PROCESSOR.Id = FRM.Processor
      WHERE FRM.ID = ', id); 
    
    PREPARE strSQl FROM @strSQL;
    EXECUTE strSQL;
    
	SELECT @NodeType as NodeType, @Status as Status, @Processor as Processor, @Buttons as Buttons, @ConditionAction as ConditionAction, @Field as Field , @Operator as Operator, @FieldValue as FieldValue;
	DEALLOCATE  PREPARE strSQl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_Sample`()
BEGIN
	SELECT * FROM tbuser;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbcompany`
--

CREATE TABLE IF NOT EXISTS `tbcompany` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact_number` varchar(30) NOT NULL,
  `primary_contact` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `date_registered` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `tbcompany`
--

INSERT INTO `tbcompany` (`id`, `code`, `name`, `contact_number`, `primary_contact`, `email`, `extension`, `date_registered`, `is_active`) VALUES
(1, 'GS3-2008', 'Global Strategic Solution & Services Inc.', '9999055639', '', 'jolly.delacruz@gs3.com.ph', '', '2013-08-23 09:41:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbuser`
--

CREATE TABLE IF NOT EXISTS `tbuser` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `display_name` varchar(50) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  `position` varchar(100) NOT NULL,
  `company_id` int(255) NOT NULL,
  `user_level_id` int(255) NOT NULL,
  `department_position_level` int(255) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `date_registered` datetime NOT NULL,
  `email_activate` tinyint(1) NOT NULL,
  `is_available` tinyint(4) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `tbuser`
--

INSERT INTO `tbuser` (`id`, `email`, `display_name`, `first_name`, `middle_name`, `last_name`, `contact_number`, `position`, `company_id`, `user_level_id`, `department_position_level`, `password`, `extension`, `date_registered`, `email_activate`, `is_available`, `is_active`, `department_id`) VALUES
(1, 'AaronTolentino@gs3.com.ph', 'Aaron Tolentino', 'Aaron', '', 'Tolentino', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(2, 'abigail.abdulgapul@gs3.com.ph', 'Abigail Abdulgapul', 'Abigail', '', 'Abdulgapul', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(3, 'Al.Abdulgapul@gs3.com.ph', 'Al Abdulgapul', 'Al', '', 'Abdulgapul', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(4, 'Alfredo.DelaCruz@gs3.com.ph', 'Alfredo Dela Cruz', 'Alfredo', '', 'Cruz', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(5, 'AlfredoIV.Magpantay@gs3.com.ph', 'Alfredo IV Magpantay', 'Alfredo', '', 'Magpantay', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(6, 'Alvin.Romero@gs3.com.ph', 'Alvin Romero', 'Alvin', '', 'Romero', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(7, 'Alyssa.Santos@gs3.com.ph', 'Alyssa Santos', 'Alyssa', '', 'Santos', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(8, 'angelyn.mendez@gs3.com.ph', 'Angelyn Mendez', 'Angelyn', '', 'Mendez', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(9, 'annalyn.atanacio@gs3.com.ph', 'Annalyn Atanacio', 'Annalyn', '', 'Atanacio', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(10, 'Aramis.Marquez@gs3.com.ph', 'Aramis Marquez', 'Aramis', '', 'Marquez', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(11, 'ariane.eladia@gs3.com.ph', 'Ariane Eladia', 'Ariane', '', 'Eladia', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(12, 'CarloGregore@gs3.com.ph', 'Carlo Gregore', 'Carlo', '', 'Gregore', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(13, 'cesar.bailen@gs3.com.ph', 'Cesar Bailen', 'Cesar', '', 'Bailen', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(14, 'Cez.Crisostomo@gs3.com.ph', 'Cez Diane Crisostomo', 'Cez', '', 'Crisostomo', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(15, 'Charnie.Capulong@gs3.com.ph', 'Charnie Capulong', 'Charnie', '', 'Capulong', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(16, 'ChristineTuante@gs3.com.ph', 'Christine Tuante', 'Christine', '', 'Tuante', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(17, 'Dario.Pangan@gs3.com.ph', 'Dario C Pangan', 'Dario', '', 'Pangan', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(18, 'Dominic.Pascasio@gs3.com.ph', 'Dominic Pascasio', 'Dominic', '', 'Pascasio', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(19, 'Donna.Ongmanchi@gs3.com.ph', 'Donna Ongmanchi', 'Donna', '', 'Ongmanchi', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(20, 'Eddie.Marie.Bacani@gs3.com.ph', 'Eddie Marie Bacani', 'Eddie', '', 'Bacani', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(21, 'Edgar.Palmon@gs3.com.ph', 'Edgar Palmon', 'Edgar', '', 'Palmon', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(22, 'elia.mendoza@gs3.com.ph', 'Elia Mendoza', 'Elia', '', 'Mendoza', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(23, 'Epson.Ronquillo@gs3.com.ph', 'Epson Ronquillo', 'Epson', '', 'Ronquillo', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(24, 'Erick.Umali@gs3.com.ph', 'Erick Kristofer Umali', 'Erick', '', 'Umali', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(25, 'Erik.Paredes@gs3.com.ph', 'Erik John Paredes', 'Erik', '', 'Paredes', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(26, 'Ervinne.Sodusta@gs3.com.ph', 'Ervinne Sodusta', 'Ervinne', '', 'Sodusta', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(27, 'Fatima.Gabriel@gs3.com.ph', 'Fatima Gabriel', 'Fatima', '', 'Gabriel', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(28, 'faye.dagos@gs3.com.ph', 'Faye Dagos', 'Faye', '', 'Dagos', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(29, 'Francis.Limgenco@gs3.com.ph', 'Francis Emmanuel Limgenco', 'Francis', '', 'Limgenco', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(30, 'Geraldine.Aligno@gs3.com.ph', 'Geraldine Aligno', 'Geraldine', '', 'Aligno', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(31, 'products@gs3.com.ph', 'Go Negosyo', 'Go', '', 'Negosyo', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(32, 'Grace.Bosque@gs3.com.ph', 'Grace Victoria Bosque', 'Grace', '', 'Bosque', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(33, 'GS3.Accounting@gs3.com.ph', 'GS3 Accounting', 'GS3', '', 'Accounting', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(34, 'GS3Advisory@gs3.com.ph', 'GS3 Advisory', 'GS3', '', 'Advisory', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(35, 'training@gs3.com.ph', 'GS3 Training Center', 'GS3', '', 'Center', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(36, 'harvey.decastro@gs3.com.ph', 'Harvey De Castro', 'Harvey', '', 'Castro', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(37, 'JazelAnn.DeLeon@gs3.com.ph', 'Jazel Ann De Leon', 'Jazel', '', 'Leon', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(38, 'Jennifer.Cunanan@gs3.com.ph', 'Jennifer Cunanan', 'Jennifer', '', 'Cunanan', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(39, 'jess.soriano@gs3.com.ph', 'Jess Soriano', 'Jess', '', 'Soriano', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(40, 'Jewel.Tolentino@gs3.com.ph', 'Jewel Jeims Tolentino', 'Jewel', '', 'Tolentino', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(41, 'joel.mendoza@gs3.com.ph', 'Joel Mendoza', 'Joel', '', 'Mendoza', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(42, 'John.Bautista@gs3.com.ph', 'John Anthony Bautista', 'John', '', 'Bautista', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(43, 'JohnWiroeDizon@gs3.com.ph', 'John Wiroe Dizon', 'John', '', 'Dizon', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(44, 'jolly.delacruz@gs3.com.ph', 'Jolly Dela Cruz', 'Jolly', '', 'Dela Cruz', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(45, 'Jonathan.Borcena@gs3.com.ph', 'Jonathan Borcena', 'Jonathan', '', 'Borcena', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(46, 'Marco.Bigornia@gs3.com.ph', 'Jose Marco Bigornia', 'Jose', '', 'Bigornia', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(47, 'Jouelle.Lava@gs3.com.ph', 'Jouelle Lava', 'Jouelle', '', 'Lava', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(48, 'genevajoyignacio_13@yahoo.com', 'Joy Ignacio', 'Joy', '', 'Ignacio', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(49, 'Katherine.Mae.Pineda@gs3.com.ph', 'Katherine Mae Pineda', 'Katherine', '', 'Pineda', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(50, 'KatrineMarie.Openia@gs3.com.ph', 'Katrine Marie Openia', 'Katrine', '', 'Openia', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(51, 'lady0895@yahoo.com', 'Lady Ellaine Mendoza', 'Lady', '', 'Mendoza', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(52, 'Lemuel.Mahumot@gs3.com.ph', 'Lemuel Mahumot', 'Lemuel', '', 'Mahumot', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(53, 'Lester.Nubla@gs3.com.ph', 'Lester Nubla', 'Lester', '', 'Nubla', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(54, 'lester.sayam@gs3.com.ph', 'Lester Sayam', 'Lester', '', 'Sayam', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(55, 'Lorena.Valencia@gs3.com.ph', 'Lorena Valencia', 'Lorena', '', 'Valencia', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(56, 'Mark.Andrew.Arroyo@gs3.com.ph', 'Mark Andrew Arroyo', 'Mark', '', 'Arroyo', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(57, 'Kevin.Limkinglam@gs3.com.ph', 'Mark Kevin Limkinglam', 'Mark', '', 'Limkinglam', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(58, 'Mark.Dizon@gs3.com.ph', 'Mark Rowi Dizon', 'Mark', '', 'Dizon', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(59, 'May.Rodriguez@gs3.com.ph', 'May Rodriguez', 'May', '', 'Rodriguez', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(60, 'mavie.estacio@gs3.com.ph', 'Ma. Evitha Estacio', 'Ma.', '', 'Estacio', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(61, 'melanie.oliveros@gs3.com.ph', 'Melanie Oliveros', 'Melanie', '', 'Oliveros', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(62, 'Michael.Espano@gs3.com.ph', 'Michael Espano', 'Michael', '', 'Espano', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(63, 'Michelle.Aguilar@gs3.com.ph', 'Michelle Aguilar', 'Michelle', '', 'Aguilar', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(64, 'floresmirasol3@yahoo.com.ph', 'Mirasol Flores', 'Mirasol', '', 'Flores', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(65, 'nesa.ilao@gs3.com.ph', 'Nesa Marasigan Ilao', 'Nesa', '', 'Ilao', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(66, 'nikki.lumanta@gs3.com.ph', 'Nikita Lumanta', 'Nikita', '', 'Lumanta', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(67, 'notes.administrator@gs3.com', 'Notes Administrator', 'Notes', '', 'Administrator', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(68, 'Patricia.Alejandro@gs3.com.ph', 'Patricia Marietta Alejandro', 'Patricia', '', 'Alejandro', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(69, 'pinky.lado@gs3.com.ph', 'Pinky Lado', 'Pinky', '', 'Lado', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(70, 'Rachelle.Villanueva@gs3.com.ph', 'Rachelle Ann Villanueva', 'Rachelle', '', 'Villanueva', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(71, 'Romelyn.Suarez@gs3.com.ph', 'Romelyn Suarez', 'Romelyn', '', 'Suarez', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(72, 'ronilon.ballesta@gs3.com.ph', 'Ronilon Ballesta', 'Ronilon', '', 'Ballesta', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(73, 'Sales.Marketing@gs3.com.ph', 'Sales Marketing', 'Sales', '', 'Marketing', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(74, 'Samuel.Pulta@gs3.com.ph', 'Samuel Pulta', 'Samuel', '', 'Pulta', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(75, 'Service.Desk@gs3.com.ph', 'Service Desk', 'Service', '', 'Desk', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(76, 'SMSUtility.Smart@gs3.com.ph', 'SMSUtility Smart', 'SMSUtility', '', 'Smart', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(77, 'SMSUtility.Sun@gs3.com.ph', 'SMSUtility Sun', 'SMSUtility', '', 'Sun', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(78, 'SMSUtility@gs3.com.ph', 'SMSUtility', 'SMSUtility', '', 'SMSUtility', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(79, 'system.administration@gs3.com.ph', 'System Administration', 'System', '', 'Administration', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(80, 'Thet.Ygana@gs3.com.ph', 'Thet Ygana', 'Thet', '', 'Ygana', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(81, 'Vilma.Quiachon@gs3.com.ph', 'Vilma Quiachon', 'Vilma', '', 'Quiachon', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(82, 'Vincent.DelaCruz@gs3.com.ph', 'Vincent Dela Cruz', 'Vincent', '', 'Cruz', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(83, 'VonFrancis.SanJuan@gs3.com.ph', 'Von Francis San Juan', 'Von', '', 'Juan', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(84, 'willie.co@gs3.com.ph', 'Willie Co', 'Willie', '', 'Co', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(85, 'melissa.espinosa0316@yahoo.com', 'Melissa Espinosa', 'Melissa', '', 'Espinosa', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(86, 'jessicacortez1724@yahoo.com', 'Jessica Cortez', 'Jessica', '', 'Cortez', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(87, 'jenzafranamoro@yahoo.com', 'Jen Namoro', 'Jen', '', 'Namoro', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(88, 'laramay.simanes@yahoo.com', 'Lara May Simanes', 'Lara', '', 'Simanes', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(89, 'leslieannedevera14@yahoo.com', 'Leslie Anne De Vera', 'Leslie', '', 'Vera', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL),
(90, 'ramos.jovelyn72@yahoo.com', 'Jovelyn Ramos', 'Jovelyn', '', 'Ramos', '', '', 1, 3, 0, '7nYJ1JWZTB5xpmbF51wd2VvepNK57abQmjdpqmwBnkQ=', '', '2013-09-26 05:25:57', 1, 0, 1, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
