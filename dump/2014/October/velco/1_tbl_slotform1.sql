-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 23, 2014 at 11:50 AM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms`
--

-- --------------------------------------------------------

--
-- Table structure for table `1_tbl_slotform`
--

CREATE TABLE IF NOT EXISTS `1_tbl_slotform` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TrackNo` longtext,
  `Requestor` longtext,
  `Status` longtext,
  `Processor` longtext,
  `ProcessorType` longtext,
  `ProcessorLevel` longtext,
  `LastAction` longtext,
  `DateCreated` datetime DEFAULT NULL,
  `DateUpdated` datetime DEFAULT NULL,
  `CreatedBy` longtext,
  `UpdatedBy` longtext,
  `Unread` longtext,
  `Node_ID` longtext,
  `Workflow_ID` longtext,
  `fieldEnabled` longtext,
  `fieldRequired` longtext,
  `fieldHiddenValues` longtext,
  `imported` longtext,
  `Repeater_Data` longtext,
  `Editor` longtext,
  `Viewer` longtext,
  `middleware_process` longtext,
  `SaveFormula` longtext,
  `CancelFormula` longtext,
  `customer` longtext,
  `width` longtext,
  `length` longtext,
  `height` longtext,
  `referencenumber` longtext,
  `description` longtext,
  `type` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `time_storage` datetime NOT NULL,
  `start_storage` datetime NOT NULL,
  `top` varchar(255) NOT NULL,
  `left` varchar(255) NOT NULL,
  `slot_height` varchar(255) NOT NULL,
  `slot_width` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `1_tbl_slotform`
--

INSERT INTO `1_tbl_slotform` (`ID`, `TrackNo`, `Requestor`, `Status`, `Processor`, `ProcessorType`, `ProcessorLevel`, `LastAction`, `DateCreated`, `DateUpdated`, `CreatedBy`, `UpdatedBy`, `Unread`, `Node_ID`, `Workflow_ID`, `fieldEnabled`, `fieldRequired`, `fieldHiddenValues`, `imported`, `Repeater_Data`, `Editor`, `Viewer`, `middleware_process`, `SaveFormula`, `CancelFormula`, `customer`, `width`, `length`, `height`, `referencenumber`, `description`, `type`, `class`, `time_storage`, `start_storage`, `top`, `left`, `slot_height`, `slot_width`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Customer 2', '10000', '500000', '2000', 'Customer 2', '', 'Customer 2', 'Customer 2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0px', '362px', '186px', '390px'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Customer 2', '10000', '500000', '2000', 'Customer 2', '', 'Customer 2', 'Customer 2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '42px', '195px', '70px', '100px');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
