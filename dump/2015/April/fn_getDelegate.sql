DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getDelegate`(userId INT) RETURNS longtext CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value LONGTEXT;

	SET return_value =  (SELECT GROUP_CONCAT(DISTINCT Users) AS Request_Users FROM (
							SELECT
								user_delegate_id as Users
							FROM tbdelegate WHERE (DATE(now()) BETWEEN DATE(start_date) AND DATE(end_date)) AND 
							user_id = userId) A
					);
	
	RETURN return_value;
END$$
DELIMITER ;
