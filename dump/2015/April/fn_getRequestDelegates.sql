DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getRequestDelegates`(referenceId INT ) RETURNS longtext CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value LONGTEXT;

	SET return_value =  (SELECT GROUP_CONCAT(DISTINCT Delegates) AS Request_Users FROM (
							SELECT 
								getDelegate(user_delegate.id) as Delegates
							FROM
								tbuser user_delegate
							WHERE
								FIND_IN_SET(id, referenceId)
							) A
					);
	
	RETURN return_value;
END$$
DELIMITER ;
