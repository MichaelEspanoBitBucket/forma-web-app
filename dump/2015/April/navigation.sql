-- --------------------------------------------------------

--
-- Table structure for table `tbnavigation`
--

CREATE TABLE IF NOT EXISTS `tbnavigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` datetime NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbnavigation_object`
--

CREATE TABLE IF NOT EXISTS `tbnavigation_object` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `navigation_id` int(11) NOT NULL,
  `address` tinyint(4) NOT NULL,
  `title` varchar(100) NOT NULL,
  `link_type` tinyint(1) NOT NULL,
  `form_link_type` varchar(100) NOT NULL,
  `form_id` int(11) NOT NULL,
  `other_text` varchar(255) NOT NULL,
  `navigation_users` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbnavigation_users`
--

CREATE TABLE IF NOT EXISTS `tbnavigation_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `navigation_object_id` int(11) NOT NULL,
  `user` text NOT NULL,
  `user_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
