DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `getRequests`(formId INT, userId INT, startRow INT, endRow INT, sort VARCHAR(255), search LONGTEXT)
BEGIN
	DECLARE formName VARCHAR(255);
	DECLARE activeFields VARCHAR(255);
	DECLARE companyID INT;
	DECLARE categoryID INT;
	DECLARE sCount INT;
	
	DROP TABLE IF EXISTS temp_table;
	SELECT 
		form_table_name, 
		company_id,
		category_id
	INTO formName, companyID, categoryID FROM tb_workspace WHERE ID = formId;

	IF categoryID = 0 THEN
		SET @filter = '';
	ELSE
		SET @filter = CONCAT('WHERE (
				A.requestor_id = ', userId ,' 
				OR FIND_IN_SET(', userId ,', getFormUsers(', formId ,',2))
				OR FIND_IN_SET(', userId ,', A.processors_id)
				OR FIND_IN_SET(', userId ,', getRequestUsers(', formId ,', A.request_id, 1))
				OR FIND_IN_SET(', userId ,', getRequestUsers(', formId ,', A.request_id, 2))
				OR FIND_IN_SET(', userId ,', getRequestViewers(', formId ,', A.request_id))
				OR FIND_IN_SET(', userId ,', A.Editor)
				OR FIND_IN_SET(', userId ,', A.Viewer)
				)');

	END IF;

	SET @strSQL = CONCAT('CREATE TEMPORARY TABLE temp_table SELECT * FROM (SELECT 
	request.ID as request_id,
	requester.first_name,
	requester.last_name,
	requester.middle_name,
	request.Status as form_status,
	request.Requestor as requestor_id,
	CASE request.ProcessorType
		WHEN 1 THEN CONCAT(getDepartmentUsers(request.Processor, request.ProcessorLevel, ', companyID ,'),'','' ,
			IFNULL(getRequestDelegates(getDepartmentUsers(request.Processor, request.ProcessorLevel, ', companyID ,')),'''')
		) 
        WHEN 2 THEN CONCAT(getUsersByPosition(request.Processor),'','' ,
			IFNULL(getRequestDelegates(getUsersByPosition(request.Processor)),'''')
		) 
        ELSE
		CONCAT(request.Processor,'','' ,
			IFNULL(getRequestDelegates(request.Processor),'''')
		)
	END AS processors_id,
	requester.display_name AS requestorName,
	requester.display_name AS requestor_display_name,
	CASE request.ProcessorType
		WHEN 1 THEN request.Processor
		WHEN 2 THEN request.Processor
		ELSE processer.display_name
	END AS processors_display_name,
	request.*
	FROM ',formName,
	' request LEFT JOIN tbuser requester
	ON requester.id = request.Requestor
	LEFT JOIN tbuser processer
	ON processer.id = request.Processor ', search ,') A ', @filter, sort);
      
	
	PREPARE stmt FROM @strSQL;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

	SET sCount = (SELECT count(*) FROM temp_table);

	IF endRow !=0 THEN
			SELECT *, sCount as Total FROM temp_table LIMIT startRow, endRow;
		ELSE
			SELECT *, sCount as Total FROM temp_table;
	END IF;

END$$
DELIMITER ;
