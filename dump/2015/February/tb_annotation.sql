-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 11, 2015 at 09:33 PM
-- Server version: 5.6.12-log
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gs3_eforms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_annotation`
--

CREATE TABLE IF NOT EXISTS `tb_annotation` (
  `Anno_UniqueID` bigint(255) NOT NULL AUTO_INCREMENT,
  `Anno_FormID` bigint(255) NOT NULL,
  `Anno_location` varchar(1000) NOT NULL,
  `Anno_RequestID` bigint(255) NOT NULL,
  `Anno_TrackNo` varchar(1000) NOT NULL,
  `Anno_CreatedBy` bigint(20) NOT NULL,
  `Anno_UpdatedBy` bigint(20) NOT NULL,
  `Anno_DataCreated` datetime NOT NULL,
  `Anno_DateUpdated` datetime NOT NULL,
  `Anno_is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`Anno_UniqueID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
