DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getUsersDepartment`(user_id INT, company_id INT) RETURNS longtext CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value LONGTEXT;

	SET return_value = (SELECT 
            GROUP_CONCAT(DISTINCT org_obj.department)
        FROM
            tborgchartobjects org_obj
			LEFT JOIN tborgchart org_
			ON org_.id = org_obj.orgChart_id
        WHERE
            FIND_IN_SET(user_id,
                    getDepartmentUsers(org_obj.department_code,
                            0,
                            company_id))
			AND org_.status = 1
			);

	RETURN return_value;
END$$
DELIMITER ;
