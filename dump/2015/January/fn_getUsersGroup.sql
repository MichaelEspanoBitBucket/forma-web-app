DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getUsersGroup`(user_id INT) RETURNS longtext CHARSET latin1
    DETERMINISTIC
BEGIN
	DECLARE return_value LONGTEXT;

	SET return_value = (SELECT 
			group_concat(DISTINCT form_group.group_name) 
		FROM tbform_groups form_group
		WHERE find_in_set(user_id, getGroupUsers(form_group.ID))
		AND form_group.is_active = 1);

	RETURN return_value;
END$$
DELIMITER ;
