ALTER TABLE `tb_message_board_surveys` 
ADD COLUMN `publish_results` TINYINT UNSIGNED NOT NULL DEFAULT 0 AFTER `enable_multiple_responses`;
