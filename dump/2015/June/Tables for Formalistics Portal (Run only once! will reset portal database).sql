SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `tb_message_board_post_visibility_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_post_visibility_type` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_post_visibility_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(12) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_post_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_post_type` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_post_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_posts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_posts` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_posts` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `author_id` INT UNSIGNED NOT NULL,
  `date_last_commented` DATETIME NULL,
  `date_updated` DATETIME NOT NULL,
  `date_posted` DATETIME NOT NULL,
  `visibility_type_id` INT UNSIGNED NOT NULL COMMENT 'Defaults to PUBLIC',
  `type` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_message_board_posts_tb_post_visibility_type_idx` (`visibility_type_id` ASC),
  INDEX `fk_tb_message_board_posts_tb_message_board_post_type1_idx` (`type` ASC),
  CONSTRAINT `fk_tb_message_board_posts_tb_post_visibility_type`
    FOREIGN KEY (`visibility_type_id`)
    REFERENCES `tb_message_board_post_visibility_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_message_board_posts_tb_message_board_post_type1`
    FOREIGN KEY (`type`)
    REFERENCES `tb_message_board_post_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_announcements`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_announcements` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_announcements` (
  `id` INT UNSIGNED NOT NULL,
  `text_content` LONGTEXT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_announcement_tb_message_board_posts1_idx` (`id` ASC),
  CONSTRAINT `fk_tb_announcement_tb_message_board_posts1`
    FOREIGN KEY (`id`)
    REFERENCES `tb_message_board_posts` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_follower_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_follower_type` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_follower_type` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(12) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_post_followers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_post_followers` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_post_followers` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` INT UNSIGNED NOT NULL,
  `follower_type` INT UNSIGNED NOT NULL,
  `follower` VARCHAR(64) NOT NULL,
  `enable_notification` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  `follower_display_name` VARCHAR(128) NULL,
  PRIMARY KEY (`id`, `follower_type`),
  INDEX `fk_tb_followers_tb_follower_type1_idx` (`follower_type` ASC),
  INDEX `fk_tb_post_followers_tb_message_board_posts1_idx` (`post_id` ASC),
  CONSTRAINT `fk_tb_followers_tb_follower_type1`
    FOREIGN KEY (`follower_type`)
    REFERENCES `tb_message_board_follower_type` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_post_followers_tb_message_board_posts1`
    FOREIGN KEY (`post_id`)
    REFERENCES `tb_message_board_posts` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_post_excluded_followers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_post_excluded_followers` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_post_excluded_followers` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `follower_type` INT UNSIGNED NOT NULL,
  `post_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`, `follower_type`),
  INDEX `fk_tb_followers_tb_follower_type1_idx` (`follower_type` ASC),
  INDEX `fk_tb_excluded_post_followers_tb_message_board_posts1_idx` (`post_id` ASC),
  CONSTRAINT `fk_tb_followers_tb_follower_type10`
    FOREIGN KEY (`follower_type`)
    REFERENCES `tb_message_board_follower_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_excluded_post_followers_tb_message_board_posts1`
    FOREIGN KEY (`post_id`)
    REFERENCES `tb_message_board_posts` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_post_comments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_post_comments` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_post_comments` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `author_id` INT UNSIGNED NOT NULL,
  `date_posted` DATETIME NOT NULL,
  `post_id` INT UNSIGNED NOT NULL,
  `reply_to_comment_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `text_content` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_message_board_post_comments_tb_message_board_posts1_idx` (`post_id` ASC),
  CONSTRAINT `fk_tb_message_board_post_comments_tb_message_board_posts1`
    FOREIGN KEY (`post_id`)
    REFERENCES `tb_message_board_posts` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_surveys`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_surveys` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_surveys` (
  `id` INT UNSIGNED NOT NULL,
  `is_open` TINYINT UNSIGNED NOT NULL DEFAULT 1,
  `auto_close_date` DATETIME NULL,
  `enable_multiple_responses` TINYINT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'disabled by default',
  `text_content` LONGTEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_message_board_survey_tb_message_board_posts1_idx` (`id` ASC),
  CONSTRAINT `fk_tb_message_board_survey_tb_message_board_posts1`
    FOREIGN KEY (`id`)
    REFERENCES `tb_message_board_posts` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_survey_selectable_responses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_survey_selectable_responses` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_survey_selectable_responses` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `response` VARCHAR(128) NOT NULL,
  `survey_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_message_board_survey_selectable_responses_tb_message__idx` (`survey_id` ASC),
  CONSTRAINT `fk_tb_message_board_survey_selectable_responses_tb_message_bo1`
    FOREIGN KEY (`survey_id`)
    REFERENCES `tb_message_board_surveys` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_survey_responses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_survey_responses` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_survey_responses` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `responder_id` INT UNSIGNED NOT NULL,
  `response_id` INT UNSIGNED NOT NULL,
  `survey_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_message_board_post_responses_tb_message_board_selecta_idx` (`response_id` ASC),
  INDEX `fk_tb_message_board_survey_responses_tb_message_board_surve_idx` (`survey_id` ASC),
  CONSTRAINT `fk_tb_message_board_post_responses_tb_message_board_selectabl1`
    FOREIGN KEY (`response_id`)
    REFERENCES `tb_message_board_survey_selectable_responses` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tb_message_board_survey_responses_tb_message_board_surveys1`
    FOREIGN KEY (`survey_id`)
    REFERENCES `tb_message_board_surveys` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tb_message_board_post_comment_followers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tb_message_board_post_comment_followers` ;

CREATE TABLE IF NOT EXISTS `tb_message_board_post_comment_followers` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `follower_user_id` INT UNSIGNED NOT NULL,
  `tb_message_board_posts_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_message_board_post_comment_followers_tb_message_board_idx` (`tb_message_board_posts_id` ASC),
  CONSTRAINT `fk_tb_message_board_post_comment_followers_tb_message_board_p1`
    FOREIGN KEY (`tb_message_board_posts_id`)
    REFERENCES `tb_message_board_posts` (`id`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `tb_message_board_post_visibility_type`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `tb_message_board_post_visibility_type` (`id`, `name`) VALUES (1, 'PUBLIC');
INSERT INTO `tb_message_board_post_visibility_type` (`id`, `name`) VALUES (2, 'COMPANY');
INSERT INTO `tb_message_board_post_visibility_type` (`id`, `name`) VALUES (3, 'CUSTOM');

COMMIT;


-- -----------------------------------------------------
-- Data for table `tb_message_board_post_type`
-- -----------------------------------------------------
START TRANSACTION;

INSERT INTO `tb_message_board_post_type` (`id`, `name`) VALUES (1, 'ANNOUNCEMENT');
INSERT INTO `tb_message_board_post_type` (`id`, `name`) VALUES (2, 'SURVEY');

COMMIT;


-- -----------------------------------------------------
-- Data for table `tb_message_board_follower_type`
-- -----------------------------------------------------
START TRANSACTION;
INSERT INTO `tb_message_board_follower_type` (`id`, `name`) VALUES (1, 'USER');
INSERT INTO `tb_message_board_follower_type` (`id`, `name`) VALUES (2, 'GROUP');
INSERT INTO `tb_message_board_follower_type` (`id`, `name`) VALUES (3, 'DEPARTMENT');
INSERT INTO `tb_message_board_follower_type` (`id`, `name`) VALUES (4, 'POSITION');

COMMIT;

