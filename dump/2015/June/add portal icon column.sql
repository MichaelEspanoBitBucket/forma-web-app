ALTER TABLE `tb_workspace` 
ADD COLUMN `portal_icon` VARCHAR(2) NULL DEFAULT 0 AFTER `allow_portal`;
ALTER TABLE `tb_workspace` 
CHANGE COLUMN `portal_icon` `portal_icon` VARCHAR(255) NULL DEFAULT NULL ;

