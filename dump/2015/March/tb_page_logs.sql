
CREATE TABLE IF NOT EXISTS `tb_page_logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `page` varchar(1000) NOT NULL,
  `main_page` varchar(1000) NOT NULL,
  `date_visit` datetime NOT NULL,
  `date_last_visit` datetime NOT NULL,
  `visit_times` int(255) NOT NULL,
  `visit_by` int(255) NOT NULL,
  `company_id` int(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE  `page_settings` ADD  `main_page` VARCHAR( 1000 ) NOT NULL AFTER  `page` ;