

CREATE TABLE IF NOT EXISTS `tbdashboard_navview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `navuidisplay_a` varchar(255) NOT NULL,
  `navuidisplay_b` varchar(255) NOT NULL,
  `navuitype` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

