--
-- Table structure for table `tbformactiontype`
--

CREATE TABLE IF NOT EXISTS `tbformactiontype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_type` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbformactiontype`
--

INSERT INTO `tbformactiontype` (`id`, `action_type`, `is_active`) VALUES
(1, 'Form Author', 1),
(2, 'Request Viewer', 1),
(3, 'Form User', 1),
(4, 'Form Admin', 1),
(5, 'Custom Print', 1),
(6, 'Delete Access', 1),
(7, 'Navigation Viewers', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbformusertype`
--

CREATE TABLE IF NOT EXISTS `tbformusertype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbformusertype`
--

INSERT INTO `tbformusertype` (`id`, `user_type`, `is_active`) VALUES
(1, 'Positions', 1),
(2, 'Departments', 1),
(3, 'User', 1),
(4, 'Groups', 1);