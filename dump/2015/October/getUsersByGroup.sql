DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getUsersByGroup`(id INT) RETURNS longtext CHARSET latin1
    DETERMINISTIC
BEGIN
	RETURN (SELECT group_concat(fgu.user_id) FROM tbform_groups_users fgu WHERE fgu.group_id=id);
END$$
DELIMITER ;
