
CREATE TABLE IF NOT EXISTS`tb_message_board_post_moods` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` INT UNSIGNED NOT NULL,
  `post_id` INT UNSIGNED NOT NULL,
  `comment_id` INT UNSIGNED NOT NULL DEFAULT 0,
  `mood` VARCHAR(16) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_tb_message_board_post_moods_tb_message_board_posts1_idx` (`post_id` ASC),
  CONSTRAINT `fk_tb_message_board_post_moods_tb_message_board_posts1`
    FOREIGN KEY (`post_id`)
    REFERENCES `tb_message_board_posts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;
