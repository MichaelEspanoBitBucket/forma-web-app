-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2016 at 04:18 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

CREATE TABLE IF NOT EXISTS `tb_form_deletion_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Form_ID` int(11) NOT NULL,
  `Form_Name` varchar(256) NOT NULL,
  `Action` varchar(256) NOT NULL,
  `Affected_ID` varchar(256) NOT NULL,
  `Date_Changed` datetime NOT NULL,
  `Auth_ID` varchar(256) NOT NULL,
  `Auth_User` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;