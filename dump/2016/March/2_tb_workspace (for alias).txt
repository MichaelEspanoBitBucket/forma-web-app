ALTER TABLE `tb_workspace`
ADD `form_name_formula` TEXT NOT NULL AFTER `form_alias`,
ADD `enable_formula` INT NOT NULL AFTER `form_name_formula`,
ADD `formula_is_field` INT NOT NULL AFTER `enable_formula`;