CREATE TABLE `tbemail_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `body` longtext,
  `mail_to` longtext,
  `cc` longtext,
  `bcc` longtext,
  `mail_from` longtext,
  `to_title` longtext,
  `from_title` longtext,
  `mail_title` longtext,
  `status` varchar(45) DEFAULT NULL,
  `status_message` longtext,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
)
