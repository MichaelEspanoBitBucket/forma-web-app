<?php

include_once('config/files_config.php');
include_once('config/database_url_config.php');
include_once('config/other_config.php');

define('APPLICATION_PATH', dirname(__FILE__));
//echo COMPANY_ID;

/* ========== Turning error reporting off php (error_reporting(E_ERROR | E_WARNING | E_PARSE))========== */
error_reporting(0);
/* ========== Garbage Collection ========== */
gc_enable(); // Enable Garbage Collector
gc_collect_cycles(); // # of elements cleaned up
gc_disable(); // Disable Garbage Collector



/* ========== Get all created class from the library ========== */

function _autoloadClass($class) {
    $path = realpath(dirname(__FILE__) . "/library/" . $class . ".php");
    include_once($path);
}

spl_autoload_register("_autoloadClass");

/* ========== Include Configuration files ========== */
include_once('config/config.php');


/* ========== Start REDIS ========== */
define('REDIS_LIBRARY_PATH', dirname(__FILE__) . '/library/Predis/autoload.php');
include_once REDIS_LIBRARY_PATH;

/* ========== Start API ========== */
define('API_LIBRARY_PATH', dirname(__FILE__) . '/APILibraries/');


/* ========== Force Download ========== */
upload::force_download($_POST['attachment_filename'], $_POST['attachment_filename'], $_POST['attachment_location']);

/* ========== load created design ========== */

//$uri = new Layout();
//$uri->renderContent();
if (!Auth::hasAuth('current_user')) {
    Layout::detectLoginModuleExist();
}
/*
Commented by aaron tolentino 2/3/2016
*/
// If application portal is only 1 it will create an application code and redirect it to the home dashboard
// $search = new Search();
// $get_category = $search->countCategory();
// $auth = Auth::getAuth('current_user');
// $count_categoy = count($get_category);
// if ($_SERVER['REQUEST_URI'] == USER_VIEW . "application_portal") {
//     if ($count_categoy == "1") {
//         setcookie('application', $get_category[0], 0, '/', '' . COOKIE_URL);
//         header("Location: /");
//     } else {
//         setcookie('application', null, -1, substr(USER_VIEW, 0, strlen(USER_VIEW) - 1));
//     }
//     //if admin redirect to home and set application id to 0
//     if ($auth['user_level_id'] == 2) {
//         setcookie('application', "0", 0, '/', '' . COOKIE_URL);
//         header("Location: /");
//     } else {
//         if (ALLOW_APPLICATION_PORTAL == 0) {
//             setcookie('application', "false", 0, '/', '' . COOKIE_URL);
//             header("Location: /");
//         }
//     }
// }



/* ========== Load Content ========== */
$uri = new Layout();
$allowed_folder = unserialize(ALLOWED_FOLDER);
$params = $uri->getParams();


//echo $uri->render_dynamic_folder_subView();
//if(!empty($params)){
//    if(in_array($params[1],$allowed_folder)){
//        // Dynamic Load subfolder module
//        $uri->render_dynamic_folder_subView();
//    }else{
//        // Default Module
//        $uri->renderContent();
//    }
//}else{
//    //Default Module
$uri->renderContent();
//}
//

// 





