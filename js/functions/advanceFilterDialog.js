function advanceFilterContent(fields, fields_date) {
    if (typeof fields === "string") {
        fields = JSON.parse(fields);
    }
    if (typeof fields_date === "string") {
        fields_date = JSON.parse(fields_date);
    }
    fields = sortMyStringObjectByKey(fields, "value");
    fields_date = sortMyStringObjectByKey(fields_date, "value");

    var field_label = "";
    // console.log(fields)
    //ret +=' <div class="label_below2">';
    //  ret += ' <label><input type="checkbox" class="includeSearch css-checkbox" id="includeSearch"><label for="includeSearch" class="css-label"></label> Check if you do not want to include this</label>';   
    //ret += '</div>';
    var ret = '<div class="">';
    ret += '<div class="searchFiltereWrapper">';
    ret += '<div class="fL">';
    //ret += '<h1><i class="icon-calendar"></i> Search Fields</h1>';
    ret += '</div>';
    ret += '<div class="fR isTextAlignRight">';
    ret += '    <i class="deleteSearchFilter icon-minus-sign fa fa-minus cursor" style="display:none;"></i>';//
    ret += '    <i class="addSearchFilter icon-plus-sign fa fa-plus cursor"></i>';
    ret += '</div>';
    // ret += '</div>';
    ret += '        <div class="">';
    ret += '        <div class="section clearing fl-field-style">';
    ret += '        <div class="input_position_below column div_1_of_1">';
    ret += '        <span class="isFontWeightBold">Search Field:</span> <font color="red">*</font>';
    ret += '            <select class="input-select search-request-field_filter" style="margin-top:0px;width: 100%;margin-right:0px;padding-top: 5px;">';
    ret += '<option value="Requestor">Requestor[Default]</option>' +
            '<option value="Processor">Processor[Default]</option>' +
            '<option value="Status">Status[Default]</option>' +
            '<option value="TrackNo">Tracking Number[Default]</option>';
    for (var i in fields) {
        if (fields[i]['value']) {
            field_label = fields[i]['value'];
            ret += '<option value="' + fields[i]['value'] + '" field_input_type="' + if_undefinded($(fields[i]).attr("field_input_type"), if_undefinded(fields[i]['field_input_type'], "")) + '" data-type="' + if_undefinded($(fields[i]).attr("data-type"), if_undefinded(fields[i]['type'], "")) + '">' + field_label + '</option>';
        }
    }
    ret += '            </select>';
    ret += '        </div>';
    ret += '        </div>';
    //ret += '        <div class="column div_1_of_9 isTextAlignCenter" style="margin-top: 20px;">';
    //  ret += '        <i class="deleteSearchFilter icon-minus-sign fa fa-minus cursor fl-fa-btn" style="display:none;"></i>';//
    //ret += '        <i class="addSearchFilter icon-plus-sign fa fa-plus cursor fl-fa-btn"></i>';
    //ret += '        </div>';
    ret += '        <div class="section clearing fl-field-style">';
    ret += '        <div class="column div_1_of_1">';//fields_below
    ret += '        <span class="isFontWeightBold">Comparative Operator:</span> <font color="red">*</font>';
    ret += '                <select class="form-select search-request-operator">';
    ret += '                    <option value="=">Equal (==)</option>';
    ret += '                    <option value="<=" class="number_operator">Less than equal (&lt;=)</option>';
    ret += '                    <option value=">=" class="number_operator">Greater than equal (&gt;=)</option>';
    ret += '                    <option value=">" class="number_operator">Greater than(&gt;)</option>';
    ret += '                    <option value="<" class="number_operator">Less than (&lt;)</option>';
    ret += '                    <option value="!=">Not equal(!=)</option>';
    ret += '                    <option value="%">Contains(%)</option>';
    ret += '                </select>';
    ret += '        </div>';
    ret += '        </div>';
    ret += '        <div class="section clearing fl-field-style">';
    ret += '        <div class="column div_1_of_1">';
    ret += '        <span class="isFontWeightBold">Search Value:</span>';
    ret += '            <input type="text" name="" data-type="textbox" class="form-text search-request-value_filter" value="">';
    ret += '        </div>';
    ret += '        </div>';
    ret += '        </div>';
    ret += '<div class="label_below2">';
    ret += ' <label class="isCursorPointer"><input type="checkbox" class="includeSearchMulti css-checkbox" id="includeSearchMulti"><label for="includeSearchMulti" class="css-label"></label> Check if you do not want to include advance search</label>';
    ret += '</div>';
    ret += '        </div>';



    ret += '<div class="dateRangeWrapper">';
    ret += '<h1 style="font-size:17px" class="isFontWeightBold"><i class="fa fa-icon-calendar"></i> Date Range</h1>';
    ret += '        <div class="section clearing fl-field-style"">';
    ret += '        <div class="column div_1_of_1">';
    ret += '        <span class="isFontWeightBold">Search Field (Date):</span> <font color="red">*</font>';
    ret += '        <div class="input_position_below">';
    ret += '            <select class="input-select search-request-field_filter_date" style="margin-top:0px;width: 100%;margin-right:0px;padding-top: 5px;">';
    ret += '<option value="0">-------Select-------</option>';
    ret += "<option value='DateCreated' default='true'>DateCreated[Default]</option>";
    for (var i in fields_date) {
        if (fields[i]['value']) {
            ret += '<option value="' + fields_date[i]['value'] + '">' + fields_date[i]['value'] + '</option>';
        }
    }
    ret += '            </select>';
    ret += '        </div>';
    ret += '        </div>';
    ret += '        </div>';
    ret += '        <div class="" style="border:1px solid transparent">';

    ret += '        </div>';
    ret += '        <div class="section clearing fl-field-style">';
    ret += '        <div class="column div_1_of_1">';
    ret += '        <div class="input_position_below">';
    ret += '        <span class="isFontWeightBold">From:</span> <font color="red">*</font>';
    ret += '            <input type="text" name="" data-type="textbox" class="form-text date_range search-request_date_from" value="">';
    ret += '        </div>';
    ret += '        </div>';
    ret += '        </div>';
    ret += '        <div class="section clearing fl-field-style">';
    ret += '        <div class="column div_1_of_1">';
    ret += '        <div class="input_position_below">';
    ret += '        <span class="isFontWeightBold">To:</span> <font color="red">*</font>';
    ret += '            <input type="text" name="" data-type="textbox" class="form-text date_range search-request_date_to" value="">';
    ret += '        </div>';
    ret += '        </div>';
    ret += '</div>';
    ret += '<div class="label_below2">';
    ret += ' <label class="isCursorPointer"><input type="checkbox" class="includeSearch css-checkbox" id="includeSearch"><label for="includeSearch" class="css-label"></label> Check if you do not want to include date range</label>';
    ret += '</div>';

    return ret;
}
AdvanceFilter2 = {
    showDialog: function (obj) {

        var fields = $(obj).closest(dashboardItemContainer).find(".search-request-field").find("option");
        var fields_date = $(obj).closest(dashboardItemContainer).find(".search-request-field_date").find("option");
        var form_id = $(obj).closest(dashboardItemContainer).attr("form_id");
        var search_value = $(obj).closest(dashboardItemContainer).find(".searchRequestUsersValue").val();
        // var search_field = jsonRequestData['search_field'];
        var self = this;
        var ret = '<div><h3 class="fl-margin-bottom"><i class="fa fa-filter"></i> <span>Advanced Filter</span></h3></div>';
        ret += '<div class="hr"></div>';
        ret += '<div id="advanceFilterContainer" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; overflow: hidden;" class="ps-container">'
        ret += '<div class="content-dialog">';
        ret += '<div class="" style="">';
        ret += advanceFilterContent(fields, fields_date);
        ret += '<div class="">';//fields
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += ' <input type="button" class="btn-blueBtn fl-margin-right fl-positive-btn" id="advanceFilter" value="Filter" form_id="' + form_id + '">';
        ret += ' <input type="button" class="btn-basicBtn fl-margin-right fl-default-btn" id="clearValue" value="Clear">';
        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';

        ret += '</div>';

        var newDialog = new jDialog(ret, "", "600", "450", "70", function () {
        });
        newDialog.themeDialog("modal2");
        // $(".date_range").datetimepicker({
        //     dateFormat: 'yy-mm-dd'
        // });
        $(".search-request_date_from").datetimepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            onClose: function (selectedDate) {
                $(".search-request_date_to").datetimepicker("option", "minDate", selectedDate);
            }
        });
        $(".search-request_date_to").datetimepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            onClose: function (selectedDate) {
                $(".search-request_date_from").datetimepicker("option", "maxDate", selectedDate);
            }
        });
        if (search_value != "") {
            // $(".search-request-field_filter").val(search_field)
            // $(".search-request-value_filter").val(search_value);
        }



        //
        FieldCondition.updatedFieldOperator(".search-request-field_filter", ".search-request-operator", ".search-request-value_filter", ".searchFiltereWrapper");



    },
    updatedFieldOperator: function () {
        $("body").on("change", ".search-request-field_filter", function () {
            FieldCondition.updatedFieldOperator(this, ".search-request-operator", ".search-request-value_filter", ".searchFiltereWrapper");
            $(this).closest(".searchFiltereWrapper").find(".search-request-operator").val("=");
            $(this).closest(".searchFiltereWrapper").find(".search-request-value_filter").val("");
        })
    },
    addFilter: function () {
        $("body").on("click", ".addSearchFilter", function () {
            var html = "";
            html = $(this).closest(".searchFiltereWrapper").clone();
            html.find(".deleteSearchFilter").css("display", "inline-block");

            html.find(".includeSearch").prop("checked", false);
            html.find(".includeSearch").closest("label").css("display", "none")
            html.find(".search-request-field_filter").val("TrackNo");
            html.find(".search-request-operator").val("=");
            html.find(".search-request-value_filter").val("");
            html.css("display", "none");
            $(this).closest(".searchFiltereWrapper").after(html);
            html.fadeIn();
            $(".content-dialog-scroll").perfectScrollbar("update");
        })
    },
    deleteFilter: function () {
        $("body").on("click", ".deleteSearchFilter", function () {
            $(this).closest(".searchFiltereWrapper").remove();
        })
    },
    filter: function (obj) {
        var form_id = $(obj).attr("form_id");
        // var search_value = $(".search-request-value_filter").val();
        // var search_field = $(".search-request-field_filter").val();
        var multi_search = [];

        var date_field = $(".search-request-field_filter_date").val();
        var date_from = $(".search-request_date_from").val();
        var date_to = $(".search-request_date_to").val();
        var wrapper = $("[form_id='" + form_id + "']");

        // jsonRequestData['search_value'] = search_value;
        // jsonRequestData['field'] = search_field;
        $(".searchFiltereWrapper").each(function () {
            if ($(this).find(".includeSearchMulti").prop("checked") == true) {
                return true;
            }
            multi_search.push({
                "search_value": $(this).find(".search-request-value_filter").val(),
                "search_field": $(this).find(".search-request-field_filter").val(),
                "search_operator": $(this).find(".search-request-operator").val()
            })
        })
        console.log(multi_search);
        jsonRequestData['multi_search'] = JSON.stringify(multi_search);
        if ($(".includeSearch").prop("checked") == false) {
            jsonRequestData['date_field'] = date_field;
            jsonRequestData['date_from'] = date_from;
            jsonRequestData['date_to'] = date_to;
        }

        jsonRequestData['isAdvanceSearch'] = "1";



        wrapper.find(".dataTable").dataTable().fnDestroy();
        GetRequestsDataTable.defaultData(jsonRequestData, wrapper.find(".dataTable"), function () {
            // jsonRequestData['isAdvanceSearch'] = "0";
        });
        RequestFilter2.showFilterConfirmation(wrapper.find(".dataTable"));
        $("#popup_cancel").click()
    },
    clearFilterFields: function () {
        $("#advanceFilterContainer").find("input[type='text'], select").each(function () {
            if ($(this).prop("tagName") == "INPUT") {
                $(this).val("")
            } else {
                $(this).val($(this).find("option").eq(0).val());
            }
        })
    }
}
function sortMyStringObjectByKey(arrayObject, keyNameValToSort, flag_sort_type, flag_data_type) {
    var myArrayOfObj = arrayObject;

    if (!flag_sort_type) {
        flag_sort_type = "asc";
    }
    if (!flag_data_type) {
        flag_data_type = "string";
    }
    if (flag_sort_type == "desc") {
        if (flag_data_type == "numeric") {
            return myArrayOfObj.sort(function (x, y) {
                return y[keyNameValToSort] - x[keyNameValToSort];
            });
        } else {
            return myArrayOfObj.sort(function (x, y) {
                return y[keyNameValToSort].toLowerCase().localeCompare(x[keyNameValToSort].toLowerCase());
            });
        }
    } else {
        if (flag_data_type == "numeric") {
            return myArrayOfObj.sort(function (x, y) {
                return x[keyNameValToSort] - y[keyNameValToSort];
            });
        } else {
            return myArrayOfObj.sort(function (x, y) {
                if (x[keyNameValToSort]) {
                    return x[keyNameValToSort].toLowerCase().localeCompare(y[keyNameValToSort].toLowerCase());
                }

            });
        }
    }


}

