$(document).ready(function () {
    configuration.app_configuration();
    configuration.toggle_hidden();
    configuration.hide_config();
    upload_theme.header_logo();
    upload_theme.body_wallpaper();
    upload_theme.onLoadLoginPageData();
    configuration.clear_cache();

    $('select[data-name="auth"]').change(function () {
        var val_sel = $(this).val();
        var name = $(this).attr("name");

        if (val_sel == "true") {

            $("[name='" + name + "']").each(function () {
                $(this).addClass(name + "_frequired-red");
                $(this).removeClass("frequired-red");
            })
        } else {
            $("[name='" + name + "']").each(function () {
                $(this).removeClass(name + "_frequired-red");
                $(this).removeClass("frequired-red");
            })
        }

    });


    // Labor Rate Maintenance
        $("#guest_email").select2({
            placeholder: "Search for a repository",
            minimumInputLength: 1,
            allowClear: true,
            ajax: {
                url: '/ajax/get-user',
                dataType: 'json',
                type: "GET",
                // quietMillis: 50,
                delay: 1000,
                data: function (term) {
                    return {
                        term: term
                    };
                },
                results: function (data) {
                    console.log(data)
                    return {
                        results: $.map(data, function (item) {

                            return {
                                text: item.text,
                                slug: item.slug,
                                id: item.id
                            }
                        })
                    };
                }
            }
        });
        var guest_name = $("#guest_name").val();
        $('#guest_email').select2("data", {  text: guest_name });


        app_k.li_key(".activationKey");
        app_k.get_key(".submit_key");
})
    

app_k = {

    li_key : function(element){
        $("body").on("click",element,function(){
            var ret = "";
            ret += '<div style="float:left;width:50%;">';
                ret += '<h3 class="pull-left fl-margin-bottom">';
                    ret += '<i class="icon-asterisk"></i> ' + "Activation";
                ret += '</h3>';
            ret += '</div>';
            ret += '<div class="hr"></div>';
                    ret += '<div class="fields">';
                        ret += '<div class="label_below2" style="font-weight:bold"> License Key:</div>';
                        ret += '<div class="input_position">';
                            ret += '<input type="text" name="s_k[]" class="tip form-text" maxlength="4" style="width: 23%;text-align:center;"> - ';
                            ret += '<input type="text" name="s_k[]" class="tip form-text" maxlength="4" style="width: 23%;text-align:center;"> - ';
                            ret += '<input type="text" name="s_k[]" class="tip form-text" maxlength="4" style="width: 23%;text-align:center;"> - ';
                            ret += '<input type="text" name="s_k[]" class="tip form-text" maxlength="4" style="width: 23%;text-align:center;">';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields">';
                        ret += '<div class="label_basic"></div>';
                        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                            // ret += '<input type="button" class="btn-blueBtn submit_key fl-margin-right" value="Ok"> ';
                            ret += '<input type="button" class="fl-margin-right submit_key btn-blueBtn fl-positive-btn" value="&nbsp;Ok&nbsp;">';
                            ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
                        ret += '</div>';
                    ret += '</div>';
           var newDialog = new jDialog(ret, "", "",  "", "", function() {
            });
           newDialog.themeDialog("modal2");
           $('[name="s_k[]"]').first().focus();
        })
    },

    get_key : function(element){
        $("body").on("click",element,function(){
            var li = "";
            $("[name='s_k[]']").each(function(id,val){
                li += $(this).val() + "-";
            })
            var k = li.substring(0,li.length - 1)
            console.log(k)

            if(k == ""){
                showNotification({
                    message: 'Please type your input key.',
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
            }else{
                $.ajax({
                    type : "POST",
                    url  : "/ajax/app_configuration",
                    data : {"action":"ac",k:k},
                    success : function(e){
                        // console.log(e)
                        if(e == "same"){
                            showNotification({
                                message: 'You are now using a full version of Formalistics.',
                                type: "success",
                                autoClose: true,
                                duration: 3
                            });
                            $(".fl-closeDialog").trigger("click")
                            setTimeout(function(){
                                location.reload();
                            },2000)
                            

                        }else{
                            showNotification({
                                message: 'Your key is incorrect.',
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        }
                    }
                })
            }   

        })
    }

}

upload_theme = {
    header_logo: function () {

        $("body").on("click",".save_login_data",function(){
            
            boolean = false;
            upload_theme.imageValidation('.header_logo');
            upload_theme.imageValidation('.body_wallpaper');
            console.log("checking image allowed file ", boolean)

            if (boolean == true) {
                var newAlert = new jAlert("The allowed file extensions are JPG, JPEG, PNG.", 'Confirmation Dialog', '', '', '', function (data) {
                });

                newAlert.themeAlert("jAlert2", {
                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                });
            }

            if (boolean == false) {

                var conf = "Are you sure you want to save your changes?";
                var newConfirm = new jConfirm(conf, 'Confirmation Dialog', '', '', '', function (r) {
                
                if (r) {
                        ui.block();
                        $("#login_page").ajaxForm(function (data) {
                           
                            var dataJSON = JSON.parse(data);
                            $('[data-id="header_logo_path"]').attr('value', dataJSON[0]['image_url_header']);
                            $('[data-id="body_wallpaper_path"]').attr('value', dataJSON[0]['image_url_body']);
                            console.log('DATA', data);
                            showNotification({
                                message: 'Login page design has been successfully save.',
                                type: "success",
                                autoClose: true,
                                duration: 3
                            });
                             ui.unblock();
                        }).submit();
                        setTimeout(function(){
                            upload_theme.updateAjaxData();
                        },1000);  
                     }
                    
                });

                newConfirm.themeConfirm("confirm2", {
                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                });

            }

        })

        $("body").on("click", '.cancel_login_data', function(){

            var conf = "Are you sure you want to cancel your recent changes?";
            var newConfirm = new jConfirm(conf, 'Confirmation Dialog', '', '', '', function (r) {
               
                if (r) {
                     upload_theme.cancelChangesData();
                 }
            });

            newConfirm.themeConfirm("confirm2", {
                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
            });

            
        });

        $("body").on("change", ".header_logo", function () {
            upload_theme.readUrl(this, "", '.login-logo');
            // console.log("Ssssshh", $(this).val())
            // var filename = $(this).val();
            // var filenameResult = filename.split('.').pop();
            
            // for (var i = 0; i < notAllowedFileType.length; i++) {
                
            //     console.log("note allowed", notAllowedFileType[i])
            //     if (filenameResult == notAllowedFileType[i]) {
            //         alert("Invalid File Format. jpg,jpeg,png only")
            //     }

            // };

            // $('[name="header_logo"]').val();
            // var image_data = $('[name="header_logo"]').val();
            // $('body').data({'header_logo_path':image_data})
        });
    },
    body_wallpaper: function () {

        var hostname = window.location.hostname;

        $('body').on('change', '.body_wallpaper', function () {
            
            var fl_ui_login_banner = $('.fl-ui-login-banner');
            var previewImg = $('#prevImg');
        
            $(previewImg).on('load', function() { 
                console.log('My width is: ', this.naturalWidth);
                console.log('My height is: ', this.naturalHeight);
                $('[data-name="body_wallpaper_height"]').attr('value', this.naturalHeight);
                $('.fl-ui-login-banner').css({'height': this.naturalHeight + "px"});
            });
        
            upload_theme.readUrl(this, '#prevImg', "");
            
        });
    },
    imageOnChange: function (ele, imgOnChangeUrl) {

        ele.css({
            'background-image': 'url(/' + imgOnChangeUrl + "?" + new Date().getTime() + ')',
            'color': 'red'
        })
    },

    readUrl: function(input,targetEle, asCssbg){

        if (input.files && input.files[0]) {
            
            var reader = new FileReader();

            reader.onload = function(e){
                $(targetEle).attr('src', e.target.result);
                $(asCssbg).css({'background-image': 'url('+e.target.result+')'});
                //console.log(e.target.result);               
            }

            reader.readAsDataURL(input.files[0]);    
            
        };
    },

    imageValidation: function(ele){
        
        var notAllowedFileType = ["txt", "doc", "docs", "docx", "xls", "xlsx", "csv", "ppt", "pptx", "gif"]
        var filename = $(ele).val();
        var eleResult = filename.split('.').pop();
        
        for (var i = 0; i < notAllowedFileType.length; i++) {
            
            //console.log("not allowed", notAllowedFileType[i])
            if (eleResult == notAllowedFileType[i]) {
                //alert("Invalid File Format. jpg,jpeg,png only")
                boolean = true;
            }
        };

    },

    onLoadLoginPageData: function(){
        $('body').data({
            'header_background_color': $('[data-name="header_background_color"]').val(),
            'header_button_color': $('[data-name="header_button_color"]').val(),
            'header_label_color': $('[data-name="header_label_color"]').val(),
            'footer_background_color': $('[data-name="footer_background_color"]').val(),
            'footer_font_color': $('[data-name="footer_font_color"]').val(),
            'header_logo_path': $('[data-id="header_logo_path"]').val(),
            'body_wallpaper_path': $('[data-id="body_wallpaper_path"]').val(),
            'body_wallpaper_height': $('[data-name="body_wallpaper_height"]').val(),
            'header_border_color':$('[data-name="header_border_color"]').val()
        });
    },

    updateAjaxData: function(){

        var data_action = 'save_login_page_ui';
        var data_set = 'set_login_page_ui';
        var company_id =  $("#get_company").val();
        var data_type = $('#set_login_page_ui').attr("type");
        var json_parse = {};
        var body_data = $('body').data();
        json_parse['action'] = data_action;
        json_parse['company_id'] = company_id;
        /*json_parse['header_background_color'] = $('[data-name="header_background_color"]').val();
        json_parse['header_button_color'] = $('[data-name="header_button_color"]').val();
        json_parse['header_label_color'] = $('[data-name="header_label_color"]').val();
        json_parse['footer_background_color'] = $('[data-name="footer_background_color"]').val();
        json_parse['footer_font_color'] = $('[data-name="footer_font_color"]').val();
        json_parse['header_logo_path'] = $('[data-id="header_logo_path"]').val();
        json_parse['body_wallpaper_path'] = $('[data-id="body_wallpaper_path"]').val();
        json_parse['body_wallpaper_height'] = $('[data-name="body_wallpaper_height"]').val();
        json_parse['header_border_color'] = $('[data-name="header_border_color"]').val();*/

        /*json_parse['header_background_color'] = $('body').data('latest_header_bg_color');
        json_parse['header_button_color'] = $('body').data('latest_header_button_color');
        json_parse['header_label_color'] = $('body').data('latest_header_lbl_color');
        json_parse['footer_background_color'] = $('body').data('latest_footer_bg_color');
        json_parse['footer_font_color'] = $('body').data('latest_footer_font_color');
        json_parse['header_border_color'] = $('body').data('latest_header_border_color');*/
        


        //console.log("sad", body_data['latest_header_bg_color'])
        if ($('body').is(":data('latest_header_bg_color')")) {
             json_parse['header_background_color'] = $('body').data('latest_header_bg_color');
             console.log("true", $('body').data('latest_header_bg_color'));
        }else if($('body').is(":data('latest_header_bg_color')") == undefined) {
            json_parse['header_background_color'] = $('[data-name="header_background_color"]').val()
        }else {
            json_parse['header_background_color'] = $('[data-name="header_background_color"]').val()
        }

        if ($('body').is(":data('latest_header_button_color')")) {
             json_parse['header_button_color'] = $('body').data('latest_header_button_color');
         }else if($('body').is(":data('latest_header_button_color')") == undefined) {
            json_parse['header_button_color'] = $('[data-name="header_button_color"]').val();
         }else {
            json_parse['header_button_color'] = $('[data-name="header_button_color"]').val();
         }

         if ($('body').is(":data('latest_header_lbl_color')")) {
            json_parse['header_label_color'] = $('body').data('latest_header_lbl_color');
         }else if($('body').is(":data('latest_header_lbl_color')") == undefined) {
            json_parse['header_label_color'] = $('[data-name="header_label_color"]').val();
         }else {
            json_parse['header_label_color'] = $('[data-name="header_label_color"]').val();
         }

         if ($('body').is(":data('latest_footer_bg_color')")) {
            json_parse['footer_background_color'] = $('body').data('latest_footer_bg_color');
         }else if($('body').is(":data('latest_footer_bg_color')") == undefined) {
            json_parse['footer_background_color'] = $('[data-name="footer_background_color"]').val();
         }else {
            json_parse['footer_background_color'] = $('[data-name="footer_background_color"]').val();
         }

         if ($('body').is(":data('latest_footer_font_color')")) {
            json_parse['footer_font_color'] = $('body').data('latest_footer_font_color');
         }else if($('body').is(":data('latest_footer_font_color')") == undefined) {
            json_parse['footer_font_color'] = $('[data-name="footer_font_color"]').val();
         }else {
            json_parse['footer_font_color'] = $('[data-name="footer_font_color"]').val();
         }

         if ($('body').data('latest_header_border_color')) {
            json_parse['header_border_color'] = $('body').data('latest_header_border_color');
         }else if($('body').data('latest_header_border_color') == undefined){
            json_parse['header_border_color'] = $('[data-name="header_border_color"]').val();
         }else {
            json_parse['header_border_color'] = $('[data-name="header_border_color"]').val();
         }

        json_parse['header_logo_path'] = $('[data-id="header_logo_path"]').val();
        json_parse['body_wallpaper_path'] = $('[data-id="body_wallpaper_path"]').val();
        json_parse['body_wallpaper_height'] = $('[data-name="body_wallpaper_height"]').val();


        $.ajax({
            type:"POST",
            url: "/ajax/app_configuration",
            data:{action:data_action, company_id:company_id, data_json:json_parse},
            success:function(data){
                console.log("ui login page data updated");

            }
        });
    },

    cancelChangesData: function(){
        var hostname = window.location.hostname;
        var body_data = $('body').data();
        var imagelogo_data = $('body').data('header_logo_path')
        var imagewallpaper_data = $('body').data('body_wallpaper_path')
        $.each(body_data, function(key, value){
            console.log(key, ' ',  value)
            $('[data-name="'+key+'"]').parent().find('.sp-preview-inner').css('background-color', value);
        })

        $('.login-logo').css({'background-image':'url("http://'+hostname+'/'+imagelogo_data+'")'})
        $('#prevImg').attr({'src':'http://'+hostname+'/'+imagewallpaper_data})
        $('#fl-ui-login-header-wrapper').css({'background-color':body_data['header_background_color'], 'border-bottom':'2px solid "'+body_data['header_border_color']+'"'})
        $('.fl-header-font-color').css({'color': body_data['header_label_color']});
        $('#fl-btn-submit-login').css({'background-color':body_data['header_button_color']});
        $('.fl-copyright').css({'color':body_data['footer_font_color']});
        $('#fl-ui-login-footer-wrapper').css({'background-color':body_data['footer_background_color']})
    }
}

configuration = {
    hide_config: function () {
        $("body").on("click", ".hide_config", function () {
            var newConfirm = new jConfirm("You are switching to an application specific licensing mode. Proceed?", 'Confirmation Dialog', '', '', '', function (r) {
                if (r == true) {
                    ui.block();
                    $.ajax({
                        type: "POST",
                        url: "/ajax/app_configuration",
                        data: {action: "hide_configuration"},
                        success: function (e) {
                            if (e == "Application configuration was successfully hide.") {
                                showNotification({
                                    message: 'Application Configuration has been successfully hidden.',
                                    type: "success",
                                    autoClose: true,
                                    duration: 3
                                });
                                ui.unblock();
                                window.location.replace('/');
                            }
                        }
                    })

                }
            })
            newConfirm.themeConfirm("confirm2", {'icon': '<i class="fa fa-question-circle fl-margin-right" style="color:#000; font-size:15px;"></i>'});
        })

    },
    app_configuration: function () {
        var url = "/ajax/app_configuration";
        $("body").on("click", ".app_configuration", function () {

            var data_action = $(this).attr("data-action");
            var data_set = $(this).attr("data-set");
            var company_id = $("#get_company").val();
            var json_parse = {};
            //console.log(data_set)
            //console.log(requiredFields("." + data_set + "_regUserField"));
            $('[name="' + data_set + '"]').each(function () {
                var data_type = $(this).attr("type");
                if (data_type == "checkbox") {
                    if ($(this).is(':checked')) {
                        json_parse[$(this).attr("data-name")] = "1";
                    } else {
                        json_parse[$(this).attr("data-name")] = "0";
                    }
                } else {
                    if ($.trim($(this).val()) != "") {
                        json_parse[$(this).attr("data-name")] = $(this).val();
                    }
                }
            })
            json_parse['action'] = data_action;
            json_parse['company_id'] = company_id;
            //Proceed to ajax
            ui.block();
            requiredFields("." + data_set + "_regUserField")
            //console.log($(".frequired-red").length)
            //if ($(".frequired-red").length != "0") {

            if (data_set != "set_smtp") {
                //code

                var required_name = $("." + data_set + "_frequired-red").map(function () {
                    if ($.trim($(this).val()) == "") {
                        if ($(this).attr("data-name") == "image_link") {
                            return "External Image Link";
                        } else {
                            return capitalize($(this).attr("data-name").replace(/_/gi, ' '));
                        }

                    }

                }).get();
                if (required_name.length != "0") {
                    var explode_length = required_name.length;
                    if (explode_length > 1) {
                        var msg = " are mandatory fields.";
                    } else {
                        var msg = " is mandatory fields.";
                    }
                    //console.log(array_required_name)
                    if (required_name == "soft delete days") {
                        var m = "Please specify no. of days before soft-deleted documents are removed.";
                    } else {
                        var m = "Please specify " + required_name + ".";
                    }
                }
            } else {
                if ($('select[data-name="auth"]').val() == "true") {
                    var required_name = $("." + data_set + "_frequired-red").map(function () {
                        if ($.trim($(this).val()) == "") {
                            if ($(this).attr("data-name") == "image_link") {
                                return "External Image Link";
                            } else {
                                return capitalize($(this).attr("data-name").replace(/_/gi, ' '));
                            }
                        }

                    }).get();
                    if (required_name.length != "0") {
                        var explode_length = required_name.length;
                        if (explode_length > 1) {
                            var msg = "Please specify " + required_name + ".";
                            var m = msg;
                        } else {
                            var msg = "Please specify " + required_name + ".";
                            var m = msg;
                        }

                    }
                } else {
                    var required_name = {};
                    var m = "go";
                }

                console.log(required_name.length);
                console.log(m);

            }

            if (required_name.length != "0" && m != "go") {
                showNotification({
                    message: capitaliseFirstLetter(m),
                    type: "error",
                    autoClose: true,
                    duration: 5
                });
                ui.unblock();
            } else {
                console.log(json_parse);
                $.ajax({
                    type: "POST",
                    url: "/ajax/app_configuration",
                    data: {action: data_action, company_id: company_id, data_json: json_parse},
                    success: function (e) {
                        ui.unblock();
                        console.log(e)
                        if (e == "Successfully configured.") {
                            showNotification({
                                message: "Application Configuration has been successfully updated.",
                                type: "success",
                                autoClose: true,
                                duration: 3
                            });
                            ui.unblock();
                        }
                    }
                })
            }

        })
    },
    clear_cache: function () {
        $('body').on('click', '.clear_memcache', function () {
            ui.block();
            $.get('/ajax/clear_mem_cache', {}, function () {
                showNotification({
                    message: "Redis has been successfully cleared.",
                    type: "success",
                    autoClose: true,
                    duration: 3
                });
                ui.unblock();
            });
        });
    },
    toggle_hidden: function () {
        $("body").on("click", "[data-hidden='toggle']", function () {
            var orig_name = $(this).attr("name");
            var name = $(this).attr("data-name");
            var set_true = $(this).attr("data-true");
            var set_name = $(this).attr("data-set-name");

            if (set_true == "yes") {
                $("input[data-set-name=" + set_name + "]").each(function () {
                    $(this).attr("data-true", "no");

                })

                if (orig_name == "set_ad") {
                    $("input[name=" + orig_name + "]").each(function () {
                        $(this).addClass(orig_name + "_regUserField");
                        $(this).addClass(orig_name + "_frequired-red");
                    })
                }
                $("input[data-name=" + set_name + "]").each(function () {
                    $(this).addClass(orig_name + "_regUserField");
                    $(this).addClass(orig_name + "_frequired-red");
                })
                $("." + name).removeClass("display");
            } else {
                $("input[data-set-name=" + set_name + "]").each(function () {
                    $(this).attr("data-true", "yes");

                })
                if (orig_name == "set_ad") {
                    $("input[name=" + orig_name + "]").each(function () {
                        $(this).removeClass(orig_name + "_regUserField");
                        $(this).removeClass(orig_name + "_frequired-red");
                    })
                }

                $("input[data-name=" + set_name + "]").each(function () {
                    $(this).removeClass(orig_name + "_regUserField");
                    $(this).removeClass(orig_name + "_frequired-red");
                })
                $("." + name).addClass("display");
            }
        });
    },
}