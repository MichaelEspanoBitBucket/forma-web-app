$(document).ready(function () {

    $('.switchingAppView').on({
        'click': function () {
            AppCalendarView.toggleCalendar(this);
        }
    });


});

AppCalendarView = {
    toggleCalendar: function (dis) {

        var dashboardItemContainer = ".fl-table-wrapper";
        var calendarWrapper = '.fl-application-calendar';
        var self = $(dis);

        var context = self.closest(dashboardItemContainer);
        var revealWrapperbutton = $(calendarWrapper).parents(dashboardItemContainer).find('.fl-revealWrapperTrigger');
        var revealWrapperbuttonParent = $(revealWrapperbutton).parent();

        //$(revealWrapperbuttonParent).show();
        if (self.attr('view') == 'table-view') {

            self.removeClass('fa-toggle-off').addClass('fa-toggle-on');
            self.attr('view', 'calendar-view');

            self.tooltip('hide')
                    .attr('data-original-title', 'Switch to Table View')
                    .tooltip('fixTitle')
                    .tooltip('show');

            context.find('.dataTable, .dataTables_info, .dataTables_paginate, .fl-table-search-wrapper, .fl-datatable-wrapper, .dataTable_widget, .request_buttons, .spinner').hide();
            context.find('.fl-application-calendar').stop(true, true).fadeIn();
            //$(revealWrapperbuttonParent).hide();
            // context.find('.fl-application-calendar').fullCalendar('render');
            this.setCalendar(dis);
           $(revealWrapperbuttonParent).show();

        } else if (self.attr('view') === 'calendar-view') {

            self.removeClass('fa-toggle-on').addClass('fa-toggle-off');
            self.attr('view', 'table-view');

            self.tooltip('hide')
                    .attr('data-original-title', 'Switch to Calendar View')
                    .tooltip('fixTitle')
                    .tooltip('show');

            context.find('.fl-application-calendar').hide();
            context.find('.dataTable, .dataTables_info, .dataTables_paginate, .fl-table-search-wrapper, .fl-datatable-wrapper, .dataTable_widget, .request_buttons, .spinner').stop(true, true).fadeIn();
            context.find(".searchRequestUsersButton").trigger("click");
            $(revealWrapperbuttonParent).hide();
        }
        ;

        if (self.attr('view') === 'calendar-view') {
            $(revealWrapperbuttonParent).hide();
        }else if(self.attr('view') === 'table-view'){
            $(revealWrapperbuttonParent).show();
        }

    },
    setCalendarDefaultView: function (dis) {
        var self = this;
        $(dis).find(".switchingAppView[default-view='1']").trigger("click");
        $(dis).find(".switchingAppView").attr("default-view", "0");
        $(dis).find(".switchingAppView").tooltip("destroy");
    },
    setCalendar: function (object) {
        var dashboardItemContainer = ".fl-table-wrapper";
        var context = $(object).closest(dashboardItemContainer);
        var form_id = context.attr("form_id");
        context.find('.fl-application-calendar').fullCalendar('destroy');
        context.find('.fl-application-calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay,prevYear,nextYear'
            },
            eventLimit: true,
            nextDayThreshold: '24:00:00',
            events: function (start, end, timezone, callback) {
                var start_date = start.format();
                var end_date = end.format();
                ui.blockPortion(context.find(".fl-main-app-view"))
                $.ajax({
                    url: '/ajax/search',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        action: "getRequestCalendar",
                        id: form_id,
                        "start_date": start_date,
                        "end_date": end_date
                    },
                    success: function (doc) {
                        console.log(doc);
                        ui.unblockPortion(context.find(".fl-main-app-view"))
                        callback(doc);
                    }
                });
            },
            eventClick: function (calEvent, jsEvent, view) {

                // alert('Event: ' + calEvent.link);
                // var win = window.open(calEvent.link,'_blank');
                // win.focus();
                // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                // alert('View: ' + view.name);

            },
            eventMouseover : function(calEvent, jsEvent, view){
                console.log(calEvent)
            },
            eventRender: function(event, element) {
                element.attr("title",event['customized_tooltip']);
                element.tooltip({html: true});
            }
        });
        // context.find('.fl-application-calendar').fullCalendar({
        //     header: {
        //         left: 'prev,next today',
        //         center: 'title',
        //         right: 'month,agendaWeek,agendaDay'
        //     },
        //     defaultDate: '2015-02-12',
        //     editable: true,
        //     eventLimit: true, // allow "more" link when too many events
        //     events: [
        //         {
        //             title: 'All Day Event',
        //             start: '2015-02-01'
        //         },
        //         {
        //             title: 'Long Event',
        //             start: '2015-02-07',
        //             end: '2015-02-10'
        //         },
        //         {
        //             id: 999,
        //             title: 'Repeating Event',
        //             start: '2015-02-09T16:00:00'
        //         },
        //         {
        //             id: 999,
        //             title: 'Repeating Event',
        //             start: '2015-02-16T16:00:00'
        //         },
        //         {
        //             title: 'Conference',
        //             start: '2015-02-11',
        //             end: '2015-02-13'
        //         },
        //         {
        //             title: 'Meeting',
        //             start: '2015-02-12T10:30:00',
        //             end: '2015-02-12T12:30:00'
        //         },
        //         {
        //             title: 'Lunch',
        //             start: '2015-02-12T12:00:00'
        //         },
        //         {
        //             title: 'Meeting',
        //             start: '2015-02-12T14:30:00'
        //         },
        //         {
        //             title: 'Happy Hour',
        //             start: '2015-02-12T17:30:00'
        //         },
        //         {
        //             title: 'Dinner',
        //             start: '2015-02-12T20:00:00'
        //         },
        //         {
        //             title: 'Birthday Party',
        //             start: '2015-02-13T07:00:00'
        //         },
        //         {
        //             title: 'Click for Google',
        //             url: 'http://google.com/',
        //             start: '2015-02-28'
        //         }
        //     ]
        // });
    }

};