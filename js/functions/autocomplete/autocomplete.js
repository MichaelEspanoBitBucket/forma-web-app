jQuery.fn.extend({
	 autoComplete: function (options) {
	 
	    var settings = {
	       table_search : "",
	       search_type : "",
	       event : "",
	       display : "",
	       allow_select : "",
	       allow_delete : ""
	    }
	    options = $.extend(settings, options);
	       var o = options;
	       var ret = "";
	       $(o.event).focus();
	       switch (o.search_type) {
		  case "users":
		     ret += "Name only";
		     break;
		  
		  case "position":
		     ret += "Position Only";
		     break;
		  
		  case "department":
		     ret += "Department Only";
		     break;
	       }
	 
	       
	       $(o.event).keyup(function(e){
		     var searchbox = $(this).val();
		     var listID = $(this).attr("data-tag-id");
		     var event = o.event.replace("#","").replace(".","");
		     var dataString = 'action=getUsers'  + '&searchword=' + searchbox + '&listID=' + listID + '&table=' + o.table_search +
		     '&search_type=' + o.search_type + '&event=' + event;
		     var M = "";
		     
		     if(searchbox==''){
			$(o.display).hide();
			
		     }else if(e.keyCode!="40" && e.keyCode !="38"){
			   $.ajax({
			      type: "POST",
			      url: "/ajax/message",
			      data: dataString,
			      dataType: 'json',
			      cache: false,
			      success: function(data){
			      //console.log(data);
				if (data.length == 0) {
				console.log(data);
				    $(o.display).hide();
				}else{
				    for (var a=0;a<data.length;a++) {
					var users = data[a];
					M += contacts(users,event);
					$(o.display).html(M).show();
					$('.add_' + event).eq(0).addClass('activeS');
					
				    }
				}
				hideRemoving();
				 //$(o.display).html(html).show();
				 //$('.add_' + event).eq(0).addClass('activeS');     
			      }
			   });	
		     }
			return false;   
	       });
	    // Allow Select on showing list  
	       switch (o.allow_select) {
		  case true:
		     var event = o.event.replace("#","").replace(".","");
		     $("body").on("click.adduser_autocomplete","#search_info_" + event,function(){
			var dataID = $(this).attr("data-id");
			$div = $('.add_' + event);
			var name = $("#search_name_" + dataID).attr("data-name");
			
			var listRecipient1 = $("#listRecipient1").attr("data-tag-id")
			if(listRecipient1){
				var explode_list = listRecipient1.split(",");
			
				var data_id = $(this).attr("data-id");
				console.log(explode_list)
				var ok = "";
				$.each(explode_list,function(id,val){
				
				if(val == data_id){
				    showNotification({
						message: "Ooops, this user is already selected.",
						type: "error",
						autoClose: true,
						duration: 3
				    });
				    ok = "";
				    return true;
				}else{
				    
				   ok = "ok";
				}
				
				})
			}else{
				ok = "ok";
			}
			
			
			if(ok == "ok"){
			    addUserTag(name,dataID,o)
			   $div.eq(0).removeClass("activeS"); // remove active user selected on the search
			   showRemoving();
			}else{
				removing(o)
			}
			
			   //addUserTag(name,dataID,o)
			   //$div.eq(0).removeClass("activeS"); // remove active user selected on the search
			   //showRemoving();
		     });
		  //
		    $("body").on("keydown",o.event,function(e) {
			var event = o.event.replace("#","").replace(".","");
			
			var $hlight = $('.activeS'), $div = $('.add_' + event);
			var queryReturn = parseInt($("#displayUser").attr("data-query")) + 1;
					    
			// Down
			if(e.keyCode == 40) {
			    $hlight.removeClass('activeS').next().addClass('activeS');
			    if ($hlight.next().length == 0) {
				$div.eq(0).addClass('activeS') // remove active user selected on the search
			    }
			    
			}
			// Up
			if(e.keyCode == 38) {
			    $hlight.removeClass('activeS').prev().addClass('activeS');
			    if ($hlight.prev().length == 0) {
				$div.eq(-1).addClass('activeS')
			    }
			       
			}
					    
			// Enter
			if(e.keyCode == 13) {
			   dataID = $hlight.attr("data-id"); // user ID
			   name = $("#search_name_" + dataID).attr("data-name"); // get name of the selected user
			   
			   
			   
			   //
			   
			   var listRecipient1 = $("#listRecipient1").attr("data-tag-id")
			   if(listRecipient1){
				    var explode_list = listRecipient1.split(",");
				    
				    var data_id = dataID
				    console.log(explode_list)
				    if(jQuery.inArray(data_id, explode_list) != -1) {
					showNotification({
					    message: "Ooops, this user is already selected.",
					    type: "error",
					    autoClose: true,
					    duration: 3
					});
				    } else {
					addUserTag(name,dataID,o)
				       $div.eq(0).removeClass("activeS"); // remove active user selected on the search
				       showRemoving();
				    }
				}else{
					addUserTag(name,dataID,o)
				       $div.eq(0).removeClass("activeS"); // remove active user selected on the search
				       showRemoving();
				}
			    
			//    var ok = "";
			//    $.each(explode_list,function(id,val){
			//    
			//    if(val == data_id){
			//	showNotification({
			//	    message: "Ooops, this user is already selected.",
			//	    type: "error",
			//	    autoClose: true,
			//	    duration: 3
			//	});
			//    }else{
			//	
			//       ok = "ok";
			//    }
			//    
			//    })
			//    
			//    if(ok == "ok"){
			//	addUserTag(name,dataID,o)
			//       $div.eq(0).removeClass("activeS"); // remove active user selected on the search
			//       showRemoving();
			//    }
			       
			       
			    
			}
		     });
		     break;
	       }
	    
	    // Allow delete for the selected tag
	       switch (o.allow_delete) {
		  case true:
		     //
		     var event = o.event.replace("#","").replace(".","");
		     $("body").on("click",".removeTag_" + event,function(){
			var dataID = $(this).attr("data-id");
			var event = o.event.replace("#","").replace(".","");
			   $("#" + event + "_" + dataID).remove();
			   getTagID(o);
		     });
		     // Escape
			$("body").on("keydown",o.event,function(e){
			    to = $(this).val();
			    if(to==""){
				// Escape
				    if(e.keyCode === 8){
					
					a = parseInt($("." + event).length) - 1;
					if (!$("." + event).eq(a).hasClass("display")) {
						$("." + event).eq(a).remove();
					}
				       
					// get all selected user id
					getTagID(o);
					$(".add_" + event).eq(0).removeClass("activeS");
					
				    }  
			    }
			});  
		     break;
	       }
	 }
});

function contacts(user, event) {
    var ret = '<div class="display_box add_' + event + '" id="search_info_' + event + '" data-id="' + user.userID + '"  align="left">';
    ret += user.img;
    ret += '<div class="search_name" data-name="' + user.name + '" id="search_name_' + user.userID + '">';
    ret += user.name;
    ret += '</div>';
    ret += '</div>';
    return ret;
}

function hideRemoving() {
    $("#msgTitle").hide();
    $("#msgBox").hide();
}
function addUserTag(name, dataID, option) {
    var ret = "";
    var event = option.event.replace("#", "").replace(".", "");
    if (dataID != undefined) {
        ret += '<div class="recipient ' + event + '" id="' + event + '_' + dataID + '" data-id="' + dataID + '">';
        ret += '<span style="margin-right:5px;">' + name + '</span>';
        ret += '<icon class="cursor removeTag_' + event + '" data-id="' + dataID + '">X</icon>';
        ret += '</div>';
        $(ret).insertBefore(option.event);
        getTagID(option);
        removing(option);
    }

    return ret;
}
function getTagID(elements) {
    // get all selected user id

    var event = elements.event.replace("#", "").replace(".", "");

    var listRecipient = $("." + event).map(function() {
        return $(this).attr("data-id"); // Get an ID of the user
    }).get().join(',');

    if (listRecipient != "") {
        $(elements.event).attr("data-tag-id", listRecipient);
    } else {
        $(elements.event).attr("data-tag-id", "0");
    }

}
function removing(option) {
    $(option.display).hide();
    $(option.event).val(null);
    $(option.event).focus();
    $(".add_" + option.event.substring(1)).removeClass("activeS");
    $("#msgTitle").hide();
    $("#msgBox").hide();
}

function showRemoving() {
    $("#msgTitle").show();
    $("#msgBox").show();
}