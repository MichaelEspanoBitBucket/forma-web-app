form_settings_get_objects = {
    undo_me: function (elements) {
        $("body").on("click", elements, function () {
            $('.formbuilder_ws').addClass('formbuilder-ws-active');
            var e = $.Event('keydown');
            e.keyCode = 90;
            e.ctrlKey = true;
            $(this).trigger(e);
        });
    },
    redo_me: function (elements) {
        $("body").on("click", elements, function () {
            $('.formbuilder_ws').addClass('formbuilder-ws-active');
            var e = $.Event('keydown');
            e.keyCode = 89;
            e.ctrlKey = true;
            $(this).trigger(e);
        });
    },
    label_font_family: function (elements) {
        $("body").on("change", elements, function () {
            //alert(elements)
            var fontSelected = $(elements).val();
            var attrfontselectedbold = $('[value="'+fontSelected+'"').attr('attrfontbold');
            var attrfontselectednormal = $('[value="'+fontSelected+'"').attr('attrfontnormal');
            var change_label_font_weight_bold = $('.change_label_font_weight').attr('has-bold', attrfontselectedbold);
            var change_label_font_weight_normal = $('.change_label_font_weight').attr('has-normal', attrfontselectednormal);
            var change_label_font_weight_hasClass = $(this).parents('.multiple-field-property').find('.change_label_font_weight').parent().hasClass('bn-cd');
            if ($('.setObject').length >= 1) {
                save_history();
                var ff = $(this).val();
                //console.log("function label_font_family",ff, ff+'_bold');
                $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {
                    var doid = $(this).attr('data-object-id');
                    var dot = $(this).attr('data-type');
                    var dbd = $('body').data(doid);
                    console.log("deep", $(this).children().find('.input_position_below'));
                    if ($.type(dbd) == "undefined") {
                        $('body').data(doid, {});
                        dbd = $('body').data(doid);
                        dbd['lblFontType'] = 'droid_sansregular';
                        dbd['fieldFontType'] = 'droid_sansregular';
                    }
                    $(this).find('#lbl_' + doid).css('font-family', ff);
                    //console.log(attrfontselectedbold);
                    dbd['lblFontType'] = ff;
                    dbd['lblFontTypeBold'] = ff+'_bold';
                    if (change_label_font_weight_hasClass) {
                        $(this).find('#lbl_' + doid).css('font-family', attrfontselectedbold);

                    };

                    if (dot == 'tab-panel') {
                       
                        if (change_label_font_weight_hasClass) {

                            $(this).children().find('ul li').css('font-family', attrfontselectedbold);
                        
                        }else {

                            $(this).children().find('ul li').css('font-family', ff);
                        }
                    };

                    if (dot == 'accordion') {
                        if (change_label_font_weight_hasClass) {

                            $(this).children().find('.accordion-header-name').css('font-family', attrfontselectedbold);
                        
                        }else {

                            $(this).children().find('.accordion-header-name').css('font-family', ff);
                        }
                    };


                });
            }

        
        });

    },
    field_font_family: function (elements) {
        
        $('body').on('change', elements, function () {
            
            //alert(elements)

            var fontSelected = $(elements).val();
            //console.log(fontSelected)
            
            var fontSelected = $(elements).val();
            var attrfontselectedbold = $('[value="'+fontSelected+'"').attr('attrfontbold');
            var attrfontselectednormal = $('[value="'+fontSelected+'"').attr('attrfontnormal');
            var change_field_font_weight_bold = $('.change_field_font_weight').attr('has-bold', attrfontselectedbold);
            var change_field_font_weight_normal = $('.change_field_font_weight').attr('has-normal', attrfontselectednormal);
            var change_field_font_weight_hasClass = $(this).parents('.multiple-field-property').find('.change_field_font_weight').parent().hasClass('bn-cd');


            var font_style = $(this).val();
            
            if ($('.setObject').length >= 1) {
                save_history();

                $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {
                    var doid = $(this).attr('data-object-id');
                    var dot = $(this).attr('data-type');
                    var dbd = $('body').data(doid);
                    /*console.log(doid);*/
                    if ($.type(dbd) == "undefined") {
                        $('body').data(doid, {});
                        dbd = $('body').data(doid);
                        dbd['lblFontType'] = 'droid_sansregular';
                        dbd['fieldFontType'] = 'droid_sansregular';
                    }

                    dbd['fieldFontType'] = font_style;
                    var aligned_left = $(this).children("div.fields_below").find("div.input_position_below.obj_f-aligned-left");


                    if (aligned_left.length >= 1) {
                        var field_font = aligned_left.find(".obj_f-aligned-left-wrap").children();
                        var field_input = aligned_left.find(".obj_f-aligned-left-wrap").find("input");
                    }
                    else {

                     
                        //Roni  Pinili  to disable following font field by tab 
                        if (dot == 'tab-panel') {
                            var field_font = $("#setObject_" + doid).find('#obj_fields_'+doid);
                            var field_input = $("#setObject_" + doid).find('#obj_fields_'+doid);
                        }else {
                            var field_font = $("#setObject_" + doid).children("div.fields_below").find("div.input_position_below").children();
                            var field_input = $("#setObject_" + doid).children("div.fields_below").find("div.input_position_below").find("input");
                        };

                        /*console.log("parent", $("#setObject_" + doid), "children", $("#setObject_" + doid).find('#obj_fields_'+doid));*/
                    }
                    if (field_font.length >= 1) {
                        field_font.css("font-family", font_style);
                        if ($(this).is('[data-type="embeded-view"]')) {
                            $(this).attr("data-type-fontfamily", font_style);
                        }
                    }
                    if (field_input.length >= 1) {
                        field_input.css("font-family", font_style);
                    }

                    if (change_field_font_weight_hasClass) {
                        field_font.css("font-family", attrfontselectedbold);
                        field_input.css('font-family', attrfontselectedbold);
                    };
                });
            }
        });
    },
    label_text_alignment: function (elements) {
        $('body').on('change', elements, function () {
            var font_alignment = $(this).val();
            if ($('.setObject').length > 0) {
                save_history();
                $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                    var doid = $(this).attr('data-object-id');
                    var dbd = $('body').data(doid);
                    if ($.type(dbd) == "undefined") {
                        $('body').data(doid, {});
                        dbd = $('body').data(doid);
                    }
                    dbd['lblTxtAlignment'] = font_alignment;
                    $(this).find("#lbl_" + doid).css("text-align", font_alignment);
                });
            }


        });
    },
    field_text_alignment: function (elements) {
        $('body').on('change', elements, function () {
            var font_alignment = $(this).val();
            if ($('.setObject').length > 0) {
                save_history();
                $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {
                    var doid = $(this).attr('data-object-id');
                    var dbd = $('body').data(doid);
                    if ($.type(dbd) == "undefined") {
                        $('body').data(doid, {});
                        dbd = $('body').data(doid);
                    }
                    dbd['fieldTxtAlignment'] = font_alignment;
                    var aligned_left = $(this).children("div.fields_below").find("div.input_position_below.obj_f-aligned-left");
                    if (aligned_left.length >= 1) {
                        var field_align = aligned_left.find(".obj_f-aligned-left-wrap").find(".getFields_"+doid);//FS#8737 ... fix added doid
                    }
                    else {
                        var field_align = $(this).children("div.fields_below").find("div.input_position_below").find(".getFields_"+doid);//FS#8737 ... fix added doid
                    }
                    if (field_align.length >= 1) {
                        field_align.css("text-align", font_alignment);
                    }
                });
            }

        });
    },
    field_font_size: function (elements) {
        $('body').on('change', elements, function () {
            var lbl_size = $(this).val();
            if ($('.setObject').length > 0) {
                save_history();
                $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {
                    var doid = $(this).attr('data-object-id');
                    var dbd = $('body').data(doid);
                    if ($.type(dbd) == "undefined") {
                        $('body').data(doid, {});
                        dbd = $('body').data(doid);
                    }
                    dbd['fieldFontSize'] = lbl_size;
                    if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "radio" || $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "checkbox") {
                        $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("font-size", lbl_size);
                        //$("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                    } else {
                        $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("font-size", lbl_size);
                        $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");

                        $("#obj_fields_" + doid).find(".content-editable-field_" + doid).eq(0).css("font-size", lbl_size);
                        $("#obj_fields_" + doid).find(".content-editable-field_" + doid).eq(0).css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                    }
                });
            }
        });
    },
    label_font_size: function (elements) {
        $('body').on('change', elements, function () {
            var lbl_size = $(this).val();
            if ($('.setObject').length > 0) {
                save_history();
                $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {
                    var doid = $(this).attr('data-object-id');
                    var dbd = $('body').data(doid);
                    if ($.type(dbd) == "undefined") {
                        $('body').data(doid, {});
                        dbd = $('body').data(doid);
                    }
                    dbd['lblFontSize'] = lbl_size;
                    $("#lbl_" + doid).css("font-size", lbl_size);
                    $("#lbl_" + doid).css("line-height", (Number(lbl_size.replace("px", "")) - 1) + "px");

                });
            }

        });
    },
    // field_height: function(elements){
    //  $('body').on('change',elements,function(){
    //      var fld_height = $(this).val();
    //      if($('.setObject').length>0){
    //          save_history();
    //          $('.formbuilder_ws').find('.component-ancillary-focus').each(function(){

    //              var doid = $(this).attr('data-object-id');
    //              var dbd = $('body').data(doid);
    //              if($.type(dbd)=="undefined"){
    //                  $('body').data(doid,{});
    //                  dbd = $('body').data(doid);
    //              }
    //              dbd['lblFieldHeight'] = fld_height;
    //                  $("#getFields_" + doid).css("height", fld_height);
    //                  if ($("#getFields_" + doid).prop("tagName") == "TEXTAREA") {
    //                      $("#getFields_" + doid).closest(".setObject").css({"height": ($("#getFields_" + doid).closest(".setObject").outerHeight()) + "px"})
    //                      $("#getFields_" + doid).css({"height": "100%"})
    //                  }
    //          });
    //      }

    //  });
    // },
    font_weight: function (elements) {
        

        $('[value="droid_sansregular"], [value="droid_sansregular_bold"]').attr('attrfontnormal', 'droid_sansregular');
        $('[value="dustismo_regular"], [value="dustismo_regular_bold"]').attr('attrfontnormal', 'dustismo_regular');
        $('[value="open_sanslight"], [value="open_sanslight_bold"]').attr('attrfontnormal', 'open_sanslight');
        $('[value="perspective_sansregular"], [value="perspective_sansregular_bold"]').attr('attrfontnormal', 'perspective_sansregular');
        $('[value="sansumiregular"], [value="sansumiregular_bold"]').attr('attrfontnormal', 'sansumiregular');
        $('[value="vera_humana_95regular"], [value="vera_humana_95regular_bold"]').attr('attrfontnormal', 'vera_humana_95regular');
        $('[value="weblysleeklight"], [value="weblysleeklight_bold"]').attr('attrfontnormal', 'weblysleeklight');

        $('[value="droid_sansregular"], [value="droid_sansregular_bold"]').attr('attrfontbold', 'droid_sansregular_bold');
        $('[value="dustismo_regular"], [value="dustismo_regular_bold"]').attr('attrfontbold', 'dustismo_regular_bold');
        $('[value="open_sanslight"], [value="open_sanslight_bold"]').attr('attrfontbold', 'open_sanslight_bold');
        $('[value="perspective_sansregular"], [value="perspective_sansregular_bold"]').attr('attrfontbold', 'perspective_sansregular_bold');
        $('[value="sansumiregular"], [value="sansumiregular_bold"]').attr('attrfontbold', 'sansumiregular_bold');
        $('[value="vera_humana_95regular"], [value="vera_humana_95regular_bold"]').attr('attrfontbold', 'vera_humana_95regular_bold');
        $('[value="weblysleeklight"], [value="weblysleeklight_bold"]').attr('attrfontbold', 'weblysleeklight_bold');
 
        $('body').on('change', elements, function () {
            
            var self = $(this);   
            if ($(this).is(':checked')) {

                var f_w = $(this).val();

                var change_label_font_family = $('.change_label_font_family');
                /*var fontSelected = change_label_font_family.val();
                $(this).attr('has-bold', $('[value="'+fontSelected+'"]').attr('attrfontbold'));
                $(this).attr('has-normal', $('[value="'+fontSelected+'"]').attr('attrfontnormal'));*/
                var fontBold = $('.change_label_font_family option:selected').attr('attrfontBold');      
    
                $(this).parents('.spanbutton').addClass('bn-cd');

                if ($('.setObject').length > 0) {

                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {


                        var doid = $(this).attr('data-object-id');
                        var dot = $(this).attr('data-type');

                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                            dbd['lblFontType'] = 'droid_sansregular';
                            dbd['fieldFontType'] = 'droid_sansregular';
                            // $('.change_label_font_family').val(dbd['lblFontType']);
                        }

                        dbd['lblFontWg'] = f_w;

                       $("#lbl_" + doid).css({
                        'font-weight': f_w,
                        "font-family":fontBold
                        });

                       if (dot == 'tab-panel' || dot == 'accordion') {
                            $(this).children().find('ul li').css({
                                'font-weight': f_w,
                                'font-family':fontBold
                            });
                        };

                        if (dot == 'accordion') {
                            $(this).children().find('.accordion-header-name').css({
                                'font-weight': f_w,
                                'font-family':fontBold
                            });
                        };
                            
                    
                    });
                }
            }
            else {

                var f_w = "normal";

                var change_label_font_family = $('.change_label_font_family');
                var fontSelected = change_label_font_family.val();
                var fontNormal = $('.change_label_font_family option:selected').attr('attrfontnormal'); 
                self.attr('has-bold', $('[value="'+fontSelected+'"]').attr('attrfontbold'));
                self.attr('has-normal', $('[value="'+fontSelected+'"]').attr('attrfontnormal'));  

                $(this).parents('.spanbutton').removeClass('bn-cd');
                

                if ($('.setObject').length > 0) {
                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dot = $(this).attr('data-type');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['lblFontWg'] = f_w;

                        $("#lbl_" + doid).css({
                            "font-weight": f_w,
                            "font-family":fontNormal
                        });

                        if (dot == 'tab-panel') {
                            $(this).children().find('ul li').css({
                                'font-weight': f_w,
                                'font-family':fontNormal
                            });
                        };

                        if (dot == 'accordion') {
                            $(this).children().find('.accordion-header-name').css({
                                'font-weight': f_w,
                                'font-family':fontNormal
                            });
                        };
                      
                    });
                }
            }

        });
    
    },
    
    field_font_weight: function (elements) {

        var self = $(this);

        $('body').on('change', elements, function () {

            if ($(this).is(':checked')) {
                
                var ffw = $(this).val();
               
                var change_field_font_family = $('.change_field_font_family');
                var fontSelected = change_field_font_family.val();
                self.attr('has-bold', $('[value="'+fontSelected+'"]').attr('attrfontbold'));
                self.attr('has-normal', $('[value="'+fontSelected+'"]').attr('attrfontnormal'));
                var fontBold = $('.change_field_font_family option:selected').attr('attrfontBold');   
                

                $(this).parents('.spanbutton').addClass('bn-cd');

                if ($('.setObject').length > 0) {

                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                            dbd['lblFontType'] = 'droid_sansregular';
                            dbd['fieldFontType'] = 'droid_sansregular';
                        }

                        dbd['fld_fw'] = ffw;
                        /*console.log(dbd);*/
                        if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "radio" || $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "checkbox") {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css({
                                "font-weight": ffw, 
                                "font-family":fontBold
                            });
                           

                            //$("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                        } else {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css({
                                "font-weight": ffw,
                                "font-family":fontBold
                            });


                            $("#obj_fields_" + doid).find(".content-editable-field_" + doid).eq(0).css({
                                "font-weight": ffw,
                                "font-family":fontBold
                            });

                        }
                
                    });
                }
            }
            else {
                var ffw = "normal";

                var change_field_font_family = $('.change_field_font_family');
                var fontSelected = $('.change_field_font_family').val();
                var fontNormal = $('.change_field_font_family option:selected').attr('attrfontnormal'); 
                self.attr('has-bold', $('[value="'+fontSelected+'"]').attr('attrfontbold'));
                self.attr('has-normal', $('[value="'+fontSelected+'"]').attr('attrfontnormal'));  
                
                $(this).parents('.spanbutton').removeClass('bn-cd');
                if ($('.setObject').length > 0) {


                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['fld_fw'] = ffw;
                        /*console.log(dbd);*/
                        if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "radio" || $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "checkbox") {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css({
                                "font-weight": ffw, 
                                "font-family":fontNormal
                            });
                            //$("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                        } else {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css({
                                "font-weight": ffw, 
                                "font-family":fontNormal
                            });


                            $("#obj_fields_" + doid).find(".content-editable-field_" + doid).eq(0).css({
                                "font-weight": ffw, 
                                "font-family":fontNormal
                            });

                        }
         
                    });
                }
            }


        });
    },
    label_font_style: function (elements) {
        $('body').on('change', elements, function () {
            if ($(this).is(':checked')) {
                var lfs = $(this).val();
                $(this).parents('.spanbutton').addClass('bn-cd');
                if ($('.setObject').length > 0) {

                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['lblFontStyle'] = lfs;

                        $("#lbl_" + doid).css("font-style", lfs);


                    });
                }
            }
            else {
                var lfs = "normal";

                $(this).parents('.spanbutton').removeClass('bn-cd');
                if ($('.setObject').length > 0) {


                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['lblFontStyle'] = lfs;

                        $("#lbl_" + doid).css("font-style", lfs);


                    });
                }

            }

        });
    },
    field_font_style: function (elements) {

        $('body').on('change', elements, function () {
            if ($(this).is(':checked')) {
                var ffs = $(this).val();
                $(this).parents('.spanbutton').addClass('bn-cd');
                if ($('.setObject').length > 0) {
                    console.log('bold', ffs);
                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['fld_fs'] = ffs;

                        if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "radio" || $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "checkbox") {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("font-style", ffs);
                            //$("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                        } else {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("font-style", ffs);


                            $("#obj_fields_" + doid).find(".content-editable-field_" + doid).eq(0).css("font-style", ffs);

                        }

                    });
                }
            }
            else {
                var ffs = "normal";

                $(this).parents('.spanbutton').removeClass('bn-cd');
                if ($('.setObject').length > 0) {
                    console.log('bold', ffs);

                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['fld_fs'] = ffs;
                        if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "radio" || $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "checkbox") {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("font-style", ffs);
                            //$("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                        } else {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("font-style", ffs);


                            $("#obj_fields_" + doid).find(".content-editable-field_" + doid).eq(0).css("font-style", ffs);

                        }

                    });
                }

            }

        });

    },
    label_text_decoration: function (elements) {
        $('body').on('change', elements, function () {

            if ($(this).is(':checked')) {
                var ltd = $(this).val();
                $(this).parents('.spanbutton').addClass('bn-cd');
                if ($('.setObject').length > 0) {
                    console.log('bold', ltd);
                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['lblFontStyle'] = ltd;

                        $("#lbl_" + doid).css("text-decoration", ltd);
                        console.log('after mag bold', ltd);

                    });
                }
            }
            else {
                var ltd = "none";

                $(this).parents('.spanbutton').removeClass('bn-cd');
                if ($('.setObject').length > 0) {
                    console.log('bold', ltd);

                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['lblFontStyle'] = ltd;

                        $("#lbl_" + doid).css("text-decoration", ltd);
                        console.log('after mag bold', ltd);

                    });
                }

            }



        });
    },
    field_text_decoration: function (elements) {

        $('body').on('change', elements, function () {

            if ($(this).is(':checked')) {
                var ftd = $(this).val();
                $(this).parents('.spanbutton').addClass('bn-cd');
                if ($('.setObject').length > 0) {
                    console.log('bold', ftd);
                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['fld_td'] = ftd;

                        if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "radio" || $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "checkbox") {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("text-decoration", ftd);
                            //$("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                        } else {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("text-decoration", ftd);


                            $("#obj_fields_" + doid).find(".content-editable-field_" + doid).eq(0).css("text-decoration", ftd);

                        }

                    });
                }
            }
            else {
                var ftd = "none";

                $(this).parents('.spanbutton').removeClass('bn-cd');
                if ($('.setObject').length > 0) {
                    console.log('bold', ftd);

                    save_history();
                    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

                        var doid = $(this).attr('data-object-id');
                        var dbd = $('body').data(doid);
                        if ($.type(dbd) == "undefined") {
                            $('body').data(doid, {});
                            dbd = $('body').data(doid);
                        }

                        dbd['fld_td'] = ftd;

                        if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "radio" || $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "checkbox") {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("text-decoration", ftd);
                            //$("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                        } else {
                            $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("text-decoration", ftd);


                            $("#obj_fields_" + doid).find(".content-editable-field_" + doid).eq(0).css("text-decoration", ftd);

                        }

                    });
                }

            }



        });

    },
    label_color: function (elements) {
        var lbl_color_settings = $.extend({}, default_spectrum_settings);
        lbl_color_settings['change'] = function (a, b) {
            var lbl_color = $(this).val();
            if ($('.setObject').length > 0) {
                save_history();
                $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {
                    var doid = $(this).attr('data-object-id');
                    var dbd = $('body').data(doid);
                    if ($.type(dbd) == "undefined") {
                        $('body').data(doid, {});
                        dbd = $('body').data(doid);
                    }

                    dbd['lblFontolor'] = lbl_color;
                    $("#lbl_" + doid).css("color", lbl_color);
                });
            }
            console.log('lbl_color', lbl_color);
        }
        lbl_color_settings['allowEmpty'] = true;
        lbl_color_settings['cancelFN'] =  function(color){
           var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
           $("#lbl_" + object_id).css("color", revert_color);
           var data_properties_type = $(this).attr("data-properties-type");
           var json = $("body").data();
           if (json['' + object_id + '']) {
               var prop_json = json['' + object_id + ''];
           } else {
               var prop_json = {};
           }

           prop_json['' + data_properties_type + ''] = revert_color;
           json['' + object_id + ''] = prop_json;
           $("body").data(json);
        }
        $(elements).spectrum(lbl_color_settings);
    },
    field_color: function (elements) {

        var fld_color_settings = $.extend({}, default_spectrum_settings);
        fld_color_settings['change'] = function (a, b) {
            var fld_color = $(this).val();
            if ($('.setObject').length > 0) {
                save_history();
                $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {
                    var doid = $(this).attr('data-object-id');
                    var dbd = $('body').data(doid);
                    if ($.type(dbd) == "undefined") {
                        $('body').data(doid, {});
                        dbd = $('body').data(doid);
                    }
                    console.log("ASDADGA", $(".getFields_" + doid))
                    dbd['fld_fc'] = fld_color;
                    if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "radio" || $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "checkbox") {
                        $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("color", fld_color);
                        //$("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                    }
                    else if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("data-type") == "createLine") {
                        $(".getFields_" + doid).css('background-color', fld_color);
                    } else {

                        $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("color", fld_color);
                        $("#obj_fields_" + doid).find(".content-editable-field_" + doid).eq(0).css("color", fld_color);

                    }
                });
            }
            console.log('fld_color', fld_color);
        };
        fld_color_settings['cancelFN'] =  function(color){
           var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
           $("#lbl_" + object_id).css("color", revert_color);
           var data_properties_type = $(this).attr("data-properties-type");
           var json = $("body").data();
           if (json['' + object_id + '']) {
               var prop_json = json['' + object_id + ''];
           } else {
               var prop_json = {};
           }

           prop_json['' + data_properties_type + ''] = revert_color;
           json['' + object_id + ''] = prop_json;
           $("body").data(json);
        };
        fld_color_settings['allowEmpty'] = true;
        $(elements).spectrum(fld_color_settings);


    },
    form_property_pane_fluid: function () {
        $(window).resize(function () {
            var window_outer_height = $(this).outerHeight();
            var form_property_bottom = ($(".form_properties").offset().top + $(".form_properties").outerHeight());
            if (window_outer_height < form_property_bottom) {

            }
            // alert($(this).outerHeight()+" == "+ ($(".form_properties").offset().top+$(".form_properties").outerHeight()))
        })
    },
    get_objects: function (elements) {
        // Upload Image to the form 
        $("body").on("change", "#formImage", function () {
            var self = this;
            ui.block();
            $("#getformImage").ajaxForm(function (data) {

                    var json = $.parseJSON(data);
                    if (json[0].message == "Your new photos was successfully uploaded.") {
                        imageForm = $(getimageForm(count, "imageForm", json[0].img));
                        $("." + "workspace").append(imageForm);
                        $(imageForm).css({
                            "top": (drop_top) + "px",
                            "left": (drop_left) + "px"
                        });
                        dragObects($(imageForm), "." + "workspace", $("." + "workspace").height, count);
                        count++;
                        $(self).val("");
                        ui.unblock();
                    }else{
                        showNotification({
                            message: json[0].message,
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                        $(".filename").html("No file selected");
                        ui.unblock();
                    }


                // if (data == "Invalid File Format") {
                //     showNotification({
                //         message: data,
                //         type: "error",
                //         autoClose: true,
                //         duration: 3
                //     });
                //     $(".filename").html("No file selected");
                // } else if (data == "File too large. File must be less than 4 megabytes") {
                //     showNotification({
                //         message: data,
                //         type: "error",
                //         autoClose: true,
                //         duration: 3
                //     });
                //     $(".filename").html("No file selected");
                // } else {
                    
                // }
            }).submit();
        });





        $("body").on("click", elements, function (ev, data) {

            var object_type = $(this).attr("data-object-type");
            var object_drop = $(this).attr("data-drop");
            var container_height = $("." + object_drop).height();
            //Call a function of object type to add an object to the workspace.
            // if($('.setObject[unpositioned="true"]').length <= 10){

            // }else{
            //  alert("please position first all the unpositioned objects");
            // }
            objType.call(data, object_type, object_drop, count, container_height);
            count++;
            bind_onBeforeOnload(1);
        });
    },
    GetFieldObjectErrors: function(){
        var errors = 0;
        var setObject = $('.setObject[data-type="pickList"]');
        setObject.each(function(e){
            if($(this).attr('data-type') == "pickList"){
                var picklist_button = $(this).find('.pickListButton');
                var field_name = $(this).find('#getFields_' + $(this).attr('data-object-id')).attr('name');
                console.log("selection type: ", picklist_button.attr("picklist_selection_type"));
                if((picklist_button.attr("picklist_selection_type")||"0") == "0"){
                    if($.type(picklist_button.attr('formname')) != "undefined"){
                        if(picklist_button.attr('formname') == "----------------------------Select----------------------------"){
                            NotifyMe($(this),"The reference form is not yet defined in this picklist. Click on pick-icon to set the form.");
                            errors++;
                        }
                    }
                    else{
                        NotifyMe($(this),"The reference form is not yet defined in this picklist. Click on pick-icon to set the form.");
                        errors++;
                    }
                }else{
                    if($.type(picklist_button.attr('external_database_connection')) != "undefined"){
                        if(picklist_button.attr('formname') == "0"){
                            NotifyMe($(this),"The reference form is not yet defined in this picklist. Click on pick-icon to set the form.");
                            errors++;
                        }
                    }
                    else{
                        NotifyMe($(this),"The reference form is not yet defined in this picklist. Click on pick-icon to set the form.");
                        errors++;
                    }
                }
            }
            //else if
        });
        return errors;
    },
    getFieldNameErrors: function () {
        var collect_names = []; //COMPARING IF THERE IS THE SAME NAME
        var errors = 0;
        
        $('.NM-notification').remove();

        $(".workspace.formbuilder_ws").find(".setObject").each(function () {
            $(this).trigger('mouseenter');
            $(this).trigger('mouseleave');
            
            if (
                    $(this).attr("data-type") != "table" &&
                    $(this).attr("data-type") != "tab-panel" &&
                    $(this).attr("data-type") != "labelOnly" &&
                    $(this).attr("data-type") != "createLine" &&
                    // $(this).attr("data-type") != "embeded-view" &&  // validate embed name
                    $(this).attr("data-type") != "imageOnly" &&
                    $(this).attr("data-type") != "accordion" &&
                    $(this).attr("data-type") != "details-panel" && 
                    $(this).attr("data-type") != "formDataTable"
                    ) {
                var dis_id = $(this).attr("data-object-id");
                var dis_fieldname = $(this).find(".btn-basicBtn[name], .getFields_" + dis_id).eq(0).attr("name")||$(this).find(".btn-basicBtn[name], .getFields_" + dis_id).eq(0).attr("embed-name")||undefined; // name must start with characters
                var dis_field_data_type = "";
                if ($(this).find(".getFields_" + dis_id).eq(0).is('select[multiple="multiple"]') ) {
                    if($(this).is('[data-type="selectMany"]')){
                        dis_field_data_type = 'selectMany';
                    }else if($(this).is('[data-type="checkbox"]')){
                        dis_field_data_type = 'checkbox';
                    // }else if($(this).is('[data-type="smart_barcode_field"]')){
                    //     dis_field_data_type = 'smart_barcode_field';
                    }else if($(this).is('[data-type="text-tagging"]')){
                        dis_field_data_type = 'text-tagging';
                    }
                } else {
                    var dis_field_data_type = $(this).find(".getFields_" + dis_id).eq(0).attr("type");
                }
                // alert(typeof dis_field_data_type == "undefined")

                if (collect_names.length >= 1) {

                    // console.log(collect_names[collect_names.length-1].fieldname)
                    // console.log(collect_names.indexOf("fn90"))
                    var founds = 0;
                    for (var ctr = 0; ctr < (collect_names.length); ctr++) {
                        // console.log(collect_names[ctr].fieldname+" == "+dis_fieldname)
                        var dis_fieldname_checker = dis_fieldname.replace('[]', "");
                        var collected_names_checker = collect_names[ctr].fieldname.replace('[]', "");
                        console.log("collect_names[ctr].fieldname", collect_names[ctr].fieldname, dis_fieldname_checker)
                        if (collected_names_checker == dis_fieldname_checker) {

                            // collect_names[ctr].DOMJQuery.css({
                            //     "border":"2px solid red"
                            // });
                            // $(this).css({
                            //     "border":"2px solid red"
                            // });

                            $(this).trigger('mouseenter');
                            NotifyMe($(this), "This name '" + dis_fieldname + "' has an identical name");
                            NotifyMe(collect_names[ctr].DOMJQuery, "This name '" + collect_names[ctr].fieldname + "' has an identical name");
                            founds++;
                            errors++;
                        }
                    }
                    if (founds == 0) {

                        collect_names.push({
                            "fieldname": dis_fieldname,
                            "data_object_id": dis_id,
                            "DOMJQuery": $(this)
                        })
                    }
                } else {
                    collect_names.push({
                        "fieldname": dis_fieldname,
                        "data_object_id": dis_id,
                        "DOMJQuery": $(this)
                    });
                }
                // console.log("ETO")
                // console.log(dis_fieldname.match(/^[A-Za-z_][A-Za-z0-9_]*$/g))
                var reggy_match = /^[A-Za-z_][A-Za-z0-9_]*$/g; //THIS IS FOR CHECKBOX
                if (typeof dis_field_data_type != "undefined") {
                    if (dis_field_data_type == "checkbox" || dis_field_data_type == "selectMany" || dis_field_data_type == "smart_barcode_field" || dis_field_data_type == "text-tagging") {
                        reggy_match = /^[A-Za-z_][A-Za-z0-9_]*\[\]$/g;
                    }
                }
                var fieldname_keywords = [
                    "TrackNo", "Requestor", "Status", "Processor", "LastAction", "DateCreated",
                    "DateUpdated", "CreatedBy", "UpdatedBy", "Unread", "Node_ID", "Workflow_ID",
                    "fieldEnabled", "fieldRequired", "fieldHiddenValues", "imported", "Repeater_Data", "SaveFormula",
                    "CancelFormula", "FormID", "WorkflowId", "ID", "CurrentUser", "Mode",
                    "computedFields", "KeywordsField", "Department_Name", "From", "To", "Range",
                    "requestorname", "Save", "Cancel", "Union", "first_name", "last_name",
                    "Editor","RequestID",
                    "Using" // FS#9129
                ];
                fieldname_keywords = fieldname_keywords.map(function(a){
                    return a.toLowerCase();
                });
                // var fieldname_keywords = [];
                // $(".fieldname-keywords").each(function(){
                //     if($(this).attr("name")){
                //         if($(this).attr("name").length >= 1){
                //             fieldname_keywords.push($(this).attr("name"));
                //         }
                //     }
                // })
                //console.log("FIELDNAMES CHECK");
                //console.log(dis_fieldname)
                //console.log(dis_fieldname.match(reggy_match))
                if (dis_fieldname) {
                    console.log("Sample Name",dis_fieldname,dis_fieldname.match(reggy_match))
                    if (dis_fieldname.match(reggy_match) == null) {

                        NotifyMe($(this), "Invalid field name! -> '" + dis_fieldname + "'");
                        if (dis_fieldname.match(/^[^A-Za-z_]/g) != null) {
                            NotifyMe($(this), "Field name doesn't start with letters!");
                        }
                        if (dis_fieldname.match(/[^A-Za-z0-9_]+/g) != null) {
                            NotifyMe($(this), "Special characters are not allowed!");
                        }

                        errors++;
                    } else if (fieldname_keywords.indexOf(dis_fieldname.toLowerCase()) >= 0) {
                        NotifyMe($(this), "Require to change this name. \"" + dis_fieldname + "\"");

                        errors++;

                    }
                }
                else if (dis_fieldname.match(reggy_match) == null) {
                    NotifyMe($(this), "Invalid field name! -> '" + dis_fieldname + "'");
                    if (dis_fieldname.match(/^[^A-Za-z_]/g) != null) {
                        NotifyMe($(this), "Field name doesn't start with letters!");
                    }
                    if (dis_fieldname.match(/[^A-Za-z0-9_]+/g) != null) {
                        NotifyMe($(this), "Special characters are not allowed!");
                    }

                    errors++;
                } else if (fieldname_keywords.indexOf(dis_fieldname.toLowerCase()) >= 0) {
                    NotifyMe($(this), "Require to change this name.\"" + dis_fieldname + "\"");
                    errors++;

                }
            }
        });

        console.log('collect names length', collect_names)
        $(".formbuilder_page_btn.btn_content").find(".setObject").each(function () {
            var dis_self = $(this);
            var dis_id = dis_self.attr("data-object-id");
            var dis_fieldname = dis_self.find('[name]').attr("name");

            var invalid_button_name = ["save", "cancel"];
            var temp_regexp = new RegExp(invalid_button_name.join("(?![a-zA-Z0-9_])|") + "(?![a-zA-Z0-9_])", "gi");
            var matched_invalid_fields = dis_fieldname.match(temp_regexp);
            if (matched_invalid_fields) {
                if (matched_invalid_fields.length >= 1) {
                    NotifyMe(dis_self, "Save & Cancel are reserve word/button");
                    errors++;
                }
            }

            if (collect_names.length >= 1) {
                // console.log(collect_names[collect_names.length-1].fieldname)
                // console.log(collect_names.indexOf("fn90"))
                var founds = 0;
                for (var ctr = 0; ctr < (collect_names.length); ctr++) {
                    // console.log(collect_names[ctr].fieldname+" == "+dis_fieldname)
                    if (collect_names[ctr].fieldname == dis_fieldname) {
                        // collect_names[ctr].DOMJQuery.css({
                        //     "border":"2px solid red"
                        // });
                        // dis_self.css({
                        //     "border":"2px solid red"
                        // });
                        NotifyMe(dis_self, "This name '" + dis_fieldname + "' has an identical name");
                        NotifyMe(collect_names[ctr].DOMJQuery, "This name '" + collect_names[ctr].fieldname + "' has an identical name");
                        founds++;
                        errors++;
                    }
                }
                if (founds == 0) {

                    collect_names.push({
                        "fieldname": dis_fieldname,
                        "data_object_id": dis_id,
                        "DOMJQuery": dis_self
                    })
                }
            } else {
                collect_names.push({
                    "fieldname": dis_fieldname,
                    "data_object_id": dis_id,
                    "DOMJQuery": dis_self
                });
            }
            // console.log("ETO")
            // console.log(dis_fieldname.match(/^[A-Za-z_][A-Za-z0-9_]*$/g))
            var reggy_match = /^[A-Za-z_][A-Za-z0-9_]*$/g; //THIS IS FOR CHECKBOX
            if (typeof dis_field_data_type != "undefined") {
                if (dis_field_data_type == "checkbox" || dis_field_data_type == "selectMany" || dis_field_data_type == "smart_barcode_field") {
                    reggy_match = /^[A-Za-z_][A-Za-z0-9_]*\[\]$/g;
                }
            }
            if (dis_fieldname) {
                if (dis_fieldname.match(reggy_match) == null) {
                    NotifyMe(dis_self, "Invalid field name! -> '" + dis_fieldname + "'");
                    if (dis_fieldname.match(/^[^A-Za-z_]/g) != null) {
                        NotifyMe(dis_self, "Field name doesn't start with letters!");
                    }
                    if (dis_fieldname.match(/[^A-Za-z0-9_]+/g) != null) {
                        NotifyMe(dis_self, "Special characters are not allowed!");
                    }
                    errors++;
                }
            }
        });
        return errors;
    },

    // Set Size of Form
    form_size: function (elements) {
        $("body").on("change", elements, function () {
            var size = $(this).val();
            var self = $(this);
            //console.log(self);
            // Split Form Size Value
            var form_size = size.split("x");
            var form_size_width = form_size[0];
            var form_size_height = form_size[1];
            
            //console.log(screenPIW);

            var tppi_w = $('<div id="ppitestw" style="width:1in;visible:hidden;padding:0px"></div>');
            var tppi_h = $('<div id="ppitesth" style="height:1in;visible:hidden;padding:0px"></div>');
            $("body").append(tppi_w);
            $("body").append(tppi_h);
            var screenPIW = $("#ppitestw").width();
            var screenPIH = $("#ppitesth").height();
            var form_width = (form_size_width * screenPIW);
            var form_height = (form_size_height * screenPIH);


            if ($(this).children('.fl-opt_custom_size:selected').length >= 1) {

                if ($(this).children('.fl-opt_custom_size:selected').is(':data("custom_set_size")')) {

                    form_width = $(this).children('.fl-opt_custom_size:selected').data('custom_set_size')['width'];
                    form_height = $(this).children('.fl-opt_custom_size:selected').data('custom_set_size')['height'];
                }
            }


            if (form_width <= checkFormMinWidth()) {
                form_width = checkFormMinWidth();
            }
            if (form_height <= checkFormMinHeight()) {
                form_height = checkFormMinHeight();
            }

            // $(".workspace").css({
            //  "width": form_width + "px",
            //  "height": form_height + "px"
            // });
            var formbuilder_ws_ele = $(".formbuilder_ws, .orgchart_ws, .workflow_ws");
            $(formbuilder_ws_ele).attr('data-click-size', 'true');
            var formbuilder_ws_ele_o_width = formbuilder_ws_ele.outerWidth();
            var formbuilder_ws_ele_o_height = formbuilder_ws_ele.outerHeight();

            //tumatagos
            $(".formbuilder_ws, .orgchart_ws, .workflow_ws").resizable('option', 'minWidth', (form_width - formbuilder_ws_ele_o_width));
            $(".formbuilder_ws, .orgchart_ws, .workflow_ws").resizable('option', 'minHeight', (form_height - formbuilder_ws_ele_o_height));
            
            //ruler reset position
            // july 16 2015 -roni
            var topPointerRuler_tip_pos_left = $('.topPointerRuler-tip').position().left;
            var leftPointerRuler_tip_pos_top = $('.leftPointerRuler-tip').position().top;
            if (topPointerRuler_tip_pos_left > 816) {
                $('.topPointerRuler-tip').css({'left':22});
            };
            if (leftPointerRuler_tip_pos_top > 1008) {
                 $('.leftPointerRuler-tip').css({'top':22});
            };  
            

            $(".formbuilder_ws, .orgchart_ws, .workflow_ws").resizable('resizeBy', {
                "width": (form_width - formbuilder_ws_ele_o_width),
                "height": (form_height - formbuilder_ws_ele_o_height)
            });

             tppi_w.remove();
             tppi_h.remove();



             {
                return ; //FS#7484 - Form Builder: Remove the automatic resizing of the form to Portal Size after clicking on "Allow in Portal".
                var selected_option_ele = self.children('option:selected');
                if(selected_option_ele.is('.fl-opt_custom_size')){
                    
                    enableFormSetSizeField();
                    $('#allow-portal').prop('checked', false);
                    $('.choose_app_icon').removeClass('isDisplayBlock').addClass('isDisplayNone');

                
                }else if(selected_option_ele.is('.allow_portal_size')){
                    
                    enableFormSetSizeField();
                    $('#allow-portal').prop('checked', true);
                    $('.choose_app_icon').removeClass('isDisplayNone').addClass('isDisplayBlock');
                    $('.topPointerRuler-tip').css({'left':22});                  

                }else{
    
                    specificSizeForm();
                    $('#allow-portal').prop('checked', false);
                    $('.choose_app_icon').removeClass('isDisplayBlock').addClass('isDisplayNone');

                }
             }

            

        });

    },

    allow_portal_size: function(elements) {

        var form_size_portal  = $('.form_size');
        var allow_portal_prop = $('#allow-portal').prop("checked");
        
        // return ; //FS#7484 - Form Builder: Remove the automatic resizing of the form to Portal Size after clicking on "Allow in Portal".
      
        $(elements).on('change', function(){
            
           if ($(this).prop("checked") == true) {
                
               
                $('.topPointerRuler-tip').css({'left':22});

                // form_size_portal.val("5.59375x10.5");

                // form_size_portal.trigger('change');

                // enableFormSetSizeField();

                $('.choose_app_icon').removeClass('isDisplayNone').addClass('isDisplayBlock');
                
                
                //disableFormSize();
                console.log($('.show-form-design-select').attr('selected-icon'));

           }else if ($(this).prop("checked") == false) {
                //enableFormSize();
                
               //alert($(this).prop("checked"));

                $('.fl-opt_custom_size').prop('selected', true);
                
                if ($('.form_size option:selected').is('.fl-opt_custom_size')) {

                    $('.fl-opt_custom_size').trigger('change');
                    
                    $('.choose_app_icon').removeClass('isDisplayBlock').addClass('isDisplayNone');
                    
                    enableFormSetSizeField();
                
                };

           };
        
        });

    },

    form_set_size: function (element) {
        numericFunction(element);

        var form_size_portal  = $('.form_size');
        var allow_portal_prop = $('#allow-portal').prop("checked");

        $("body").on("keyup", element, function () {
            var this_value = $(this).val();
            var this_value_validation = this_value.match(/[^0-9]*/g)||[];
            this_value_validation = this_value_validation.filter(Boolean);
            if(this_value_validation.length >= 1){
                return false;
            }
            var form_size_width = Number($(".form_size_width").val());
            var form_size_height = Number($(".form_size_height").val());

            if (isNaN(form_size_width)) {
                NotifyMe($(".form_size_width"), "Please enter a number!");

            } else if (isNaN(form_size_height)) {
                NotifyMe($(".form_size_height"), "Please enter a number!");
            } else if (form_size_width >= 196.8 && form_size_height >= 279.36) {
                if (form_size_width <= checkFormMinWidth()) {
                    form_size_width = checkFormMinWidth();
                }
                if (form_size_height <= checkFormMinHeight()) {
                    form_size_height = checkFormMinHeight();
                }

                // $(".workspace").css({
                //  "width": form_size_width + "px",
                //  "height": form_size_height + "px"
                // });
                var formbuilder_ws_ele = $(".formbuilder_ws, .orgchart_ws, .workflow_ws");
                var formbuilder_ws_ele_o_width = formbuilder_ws_ele.outerWidth();
                var formbuilder_ws_ele_o_height = formbuilder_ws_ele.outerHeight();
                $(".formbuilder_ws, .orgchart_ws, .workflow_ws").resizable('resizeBy', {
                    "width": (form_size_width - formbuilder_ws_ele_o_width),
                    "height": (form_size_height - formbuilder_ws_ele_o_height)
                });

                // $(".form_size_width").val(form_size_width);
                // $(".form_size_height").val(form_size_height);
            } else {
                // if (form_size_width <= checkFormMinWidth()) {
                //     form_size_width = checkFormMinWidth();
                // }
                // if (form_size_height <= checkFormMinHeight()) {
                //     form_size_height = checkFormMinHeight();
                // }

                // if(form_size_width <= 100){
                //     form_size_width = 100;
                // }
                // if (form_size_height <= 100) {
                //     form_size_height = 100;
                // }
                // $(".workspace").css({
                //     "width": form_size_width + "px",
                //     "height": form_size_height + "px"
                // });
                // $(".form_size_width").val(form_size_width);
                // $(".form_size_height").val(form_size_height);

            }
            if ($('.form_size').children('.fl-opt_custom_size:selected').length >= 1) {
                $('.form_size').children('.fl-opt_custom_size:selected').data("custom_set_size", {
                    "width": (form_size_width) + "",
                    "height": (form_size_height) + ""
                });
            }
            ;

            if ($('#allow-portal').is(':checked')) {

                enableFormSetSizeField();

                if ($(this).is('.form_size_width')) {

                    disableFormsetsizeWidthConfirm();
                
                };
                
            };

            if ($('#allow-portal').attr('checked') == 'checked') {

                    //$('.fl-opt_custom_size').prop('selected', true);
                    $('.formbuilder_ws').find('.ui-resizable-e').hide();
                    $('.formbuilder_ws').find('.ui-resizable-s').hide();
                    $('.form_size_width').attr('readonly', 'readonly');
                    $('.form_size_width').add('.form_size').css({'opacity': '0.5'});
                    $('.form_size_height').removeAttr('readonly');
                    $('.form_size_height').css({'opacity': ''});

                    form_size_portal.val("5.59375x10.5");
                    $('.form_size').attr('disabled', 'disabled');    

               };

        });


    },    

    clear_all: function (elements) {
        $("body").on("click", elements, function () {
            var setObjs = $(".formbuilder_ws, .formbuilder_page_btn").find('.setObject');
            if (setObjs.length >= 1) {
                var conf = "Are you sure you want to clear all?";
                var newConfirm = new jConfirm(conf, 'Confirmation Dialog', '', '', '', function (r) {
                    if (r == true) {
                        $(".setObject").each(function () {
                            $(this).remove();
                        });
                        //---clear all slide menu layer
                        $(".ghost").each(function(){
                            $(this).remove();
                        });
                    }
                });

                newConfirm.themeConfirm("confirm2", {
                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                });
            }
        });
    },
    delete_objects: function (elements) {
        $("body").on("click", elements, function () {
            var body_data = $("body").data();
            var object_id = $(this).attr("data-object-id");
            // $(".object_properties").popover('hide');
            con = "Are you sure you want to delete this object?";
            var newConfirm = new jConfirm(con, 'Confirmation Dialog', '', '', '', function (r) {
                if (r == true) {
                    save_history();
                    var parent_container = $([]);
                    var set_object_deleted = $('#setObject_' + object_id);
                    if (set_object_deleted.parent().is('.formbuilder_ws')) {
                        parent_container = $('.formbuilder_ws');
                    }
                    if ($("#setObject_" + object_id).parent().hasClass("td-relative")) {
                        $("#setObject_" + object_id).parent().parent().css("min-width", "0px");
                        $("#setObject_" + object_id).parent().parent().css("min-height", "0px");
                        $("#setObject_" + object_id).parent().css("min-width", "0px");
                        $("#setObject_" + object_id).parent().css("min-height", "0px");
                        $("#setObject_" + object_id).parent().parent().resizable('option', 'minWidth', 0);
                        $("#setObject_" + object_id).parent().parent().resizable('option', 'minHeight', 0);
                        parent_container = $("#setObject_" + object_id).parent();
                    }


                   
                    
                    //slide layer panel delete layer

                    $('#setObject_'+object_id).find(".setObject").each(function(){
                            var obj_id = $(this).attr("data-object-id");
                            $('#layer_'+obj_id).remove();                  
                    $('.ps-container').perfectScrollbar("update");
                    })
                    $('#layer_'+ object_id).remove();
                    $('.ps-container').perfectScrollbar("update");
                    //-----------
                    $('.setObj_style_'+object_id).remove();
                    $('#setObject_' + object_id).remove();
                    
                    
                    if (parent_container.length >= 1 && !parent_container.is('.formbuilder_ws')) {

                        var mw = checkParentResizableMinWidth(parent_container.children('.setObject').eq(0));
                        var mh = checkParentResizableMinHeight(parent_container.children('.setObject').eq(0));
                        if(mw <= 0){
                            mw = 70;
                        }
                        parent_container.parent().css({
                            "min-width": (mw) + "px",
                            "min-height": (mh) + "px"
                        });
                        parent_container.parent().resizable('option', 'minWidth', mw);
                        parent_container.parent().resizable('option', 'minHeight', mh);
                    }

                    if (parent_container.is('.formbuilder_ws')) {
                        var fmw = checkParentResizableMinWidth(parent_container.children('.setObject').eq(0));
                        var fmh = checkParentResizableMinHeight(parent_container.children('.setObject').eq(0));

                        $('.formbuilder_ws').resizable('option', 'minWidth', fmw);
                        $('.formbuilder_ws').resizable('option', 'minHeight', fmh);
                        $('.formbuilder_ws').css('min-width', fmw);
                        $('.formbuilder_ws').css('min-height', fmh);
                    }
                    if (typeof body_data[object_id] != "undefined") {
                        delete body_data[object_id];
                        console.log("DELETE")
                        console.log(body_data)
                            
                    }
                    // Get Btn length
                    var btn_length = $(".setObject_btn").length;
                    // Show btn label on formbuilder

                    if (btn_length < 1) {
                        $(".ws_btn_label").show();
                    }
                    if ($(".setObject").length == 0) {
                        bind_onBeforeOnload(0);
                    } else {
                        bind_onBeforeOnload(1);
                    }
                }
            });
            newConfirm.themeConfirm("confirm2", {
                'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'
            })
            $("body").data(body_data);
        });
    },
    save_form_workspace: function (elements) {
        self = this;
        $("body").on("click", elements, function () {
            //---Pao.. Remove Active Accordion Tab Before Saving--//
            $('.ui-icon-triangle-1-s').remove();
            //=======================================================
            var pathname = window.location.pathname;
            if (pathname != "/formbuilder" && pathname != "/user_view/formbuilder") {
                return;
            }
            //if(getParametersName("formID",pathname) === "undefined" || getParametersName("formID",pathname) == undefined){
            //       $("#FormId").val("0");
            //       // document.location.hash = "?formID=0";
            //
            //}



            //
            // $.post("/ajax/formbuilder",{"":""},function(data){//IF SESSION EXPIRES
            // if(data){
            if (form_settings_get_objects.getFieldNameErrors() == 0 && form_settings_get_objects.GetFieldObjectErrors() == 0) {

                var form_type = $(this).attr("data-form-type");
                var form_save_type = $(this).attr("data-form-save-type");


                // Settings
                var workspace_title = $(".workspace_title").val().trim();
                var workspace_displayName = $(".workspace_displayName").val().trim();
                var workspace_aliasName =$(".workspace_aliasName").val().trim();
                var workspace_aliasNameformu =$(".workspace_aliasNameformu").val().trim();
                 //var workspace_enableformu =$(".workspace_enableformu").val();
                var workspace_categoryname = $(".form_category option:selected").val();
                var workspace_buttonName = $(".button_name").val();
                var workspace_prefix = $(".workspace_prefix").val().trim();
                var workspace_type = $(".workspace_type option:selected").val();
                var form_display_type = $(".form_display_type option:selected").val();
                var form_display_category = $(".fm-type-dos:checked").val();
                var form_display_formula = $.trim($("#form_display-dynamic").val());
                var display_import = $(".display_import option:selected").val();
                var workspace_width = $(".form_size_width").val();
                var workspace_height = $(".form_size_height").val();
                var workspace_status = $(".workspace_status").val();
                var workspace_form_size = $('.form_size').val();
                if($("#enableFormula").is(":checked")){
                       var workspace_enableformu = "1"; 
                    }else{
                        var workspace_enableformu = "0";
                    }
                if($("#enableField").is(":checked")){
                       var workspace_enablefield = "1";
                       var workspace_enableformu = "1"; 
                    }else{
                        var workspace_enablefield = "0";
                    }

                // for stagin
                var workspace_version = "1";
                var enable_staging = $("#enable_staging").text();
                if (form_save_type == "2") {
                    workspace_version = $(".workspace_version").val();
                    if (workspace_version == "2" && enable_staging == "1") { // staging or prod
                        workspace_status = "1"; // published or template
                    }
                }

                var myrequest_visibility = $(".myrequest_visibility").val(); //my request visibility


                if (workspace_width == "") {

                    workspace_width = $('.workspace.formbuilder_ws').width();

                }

                if (workspace_height == "") {
                    workspace_height = $('.workspace.formbuilder_ws').height();
                }

                switch (form_type) {

                    case "formbuilder":

                        if ($(".form-builder-ruler").length >= 1) {
                            $(".form-builder-ruler").remove()
                        }

                        // Get Object Length
                        var set_obj_elements = $(".setObject:not(.setObject_btn)");
                        var object_length = set_obj_elements.length;
                        var object_input_fields_length = set_obj_elements.filter(function(){
                            var doi = $(this).attr("data-object-id");
                            return $(this).find(".getFields_"+doi+":eq(0)").is(":input");
                        }).length;

                        // Get Button Length
                        var btn_length = $(".setObject_btn").length;

                        var check_display_category_formula = null;
                        if($("#form_display-dynamic").is(":visible")){
                            if (form_display_category == 1) {
                                if (form_display_formula == '') {
                                    check_display_category_formula = {"status": false, "message": "Error: Specify formula on 'Dynamic' visibility on side bar."};
                                } else {
                                    check_display_category_formula = client_formula.checkFormulaErrors(form_display_formula);
                                }
                            }
                        }else{
                            check_display_category_formula = {"status": true, "message": ""};
                        }
                        

                        // Condition if workspace is null
                        if (object_length < 1) {
                            // Notification
                            showNotification({
                                message: "Workspace is empty.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        }else if(object_input_fields_length <= 0){
                            showNotification({
                                message: "Please add at least one (1) input field.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        } else if (btn_length < 1) {
                            var newAlert = new jAlert("Add at least one button.", 'Confirmation Dialog', '', '', '', function (data) {
                                if (data === true) {
                                    $('.getObjects[data-object-type="button"]').trigger('click');
                                }
                            });

                            newAlert.themeAlert("jAlert2", {
                                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                            });

                        } else if (workspace_title.trim() == "") {
                            showNotification({
                                message: "Title is empty.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        } else if (workspace_title.match(/[^a-zA-Z0-9_ ]*/g).filter(Boolean).length >= 1) {
                            showNotification({
                                message: "Title is not valid.\nSpecial characters not allowed.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        } else if (workspace_aliasName.match(/[^a-zA-Z0-9_ ]*/g).filter(Boolean).length >= 1) {
                            showNotification({
                                message: "Alias is not valid.\nSpecial characters not allowed.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        } else if (workspace_displayName.trim() == "") {
                            showNotification({
                                message: "Description is empty.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        } else if (workspace_prefix.trim() == "") {
                            showNotification({
                                message: "Prefix is empty.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        } else if (workspace_buttonName.trim() == "") {
                            showNotification({
                                message: "Button name is empty.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        } else if (workspace_type == "0") {
                            showNotification({
                                message: "Please select Tracking Number Type.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        } else if (workspace_enableformu == "1"&& workspace_aliasNameformu.trim() == "") {
                            showNotification({
                                message: "Alias Formula enabled. Please input formula.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                        } else if (check_display_category_formula != null && check_display_category_formula["status"] == false) {
                            NotifyMe($("#form_display-dynamic"), check_display_category_formula['message']).css({
                                "min-width": "100px",
                                "left": "200px",
                                "top": "",
                                "margin-top": "-75px"
                            });
                        } else {

                            $(".saveFormLoad").show();
                            var form_Fields_Arr = new Array();
                            // Get Fields Name
                            //    var form_fields = $(".getFields").map(function(){
                            //  return $(this).attr("name"));
                            //    }).get().join();
                            //
                            //

                            //clear saved objects
                            var body_data = $("body").data();
                            var collect_data_id = [];
                            $(".setObject").each(function (eqi) {
                                var this_id = $(this).attr("data-object-id");
                                if (body_data[this_id]) {
                                    var temp = {};
                                    temp[this_id] = body_data[this_id];
                                    collect_data_id.push(temp);
                                }
                            });
                            $.each(body_data, function (index, value) {
                                if ($.isNumeric(String(index))) {
                                    delete body_data[index];
                                }
                            })
                            $.each(collect_data_id, function (index, value) {
                                $.extend(body_data, collect_data_id[index])
                            });
                            // console.log("ETO NA")
                            // console.log(body_data)
                            $("body").data(body_data) //END OF CLEARING THE SAVED OBJECTS

                            $(".getFields").each(function () {
                                //return $(this).attr("name"));
                                var field_name_cleaner = ($(this).attr('name') || "").replace("[]", "");
                                form_Fields_Arr.push({
                                    fieldName: field_name_cleaner,
                                    fieldType: $(this).attr('data-type'),
                                    fieldInputType: $(this).attr('data-input-type')
                                });
                            });
                            //console.log(form_fields)

                            // Get Button
                            var btnName = $(".setObject_btn").map(function () {
                                return $(this).children("[data-type='button']").val();
                            }).get().join();

                            //added by joshua reyes 03/07/2016
                            // Get Button Properties
                            var button_properties_arr = new Array();
                            $(".setObject_btn").each(function(setObject_btn_index, setObject_btn_val) {
                                var btn_id = $(setObject_btn_val).attr('id');
                                var btn_name = $(setObject_btn_val).children("[data-type='button']").val();
                                var btn_visibility_formula = $(setObject_btn_val).attr('data-visibility-button');
                                button_properties_arr.push({
                                    buttonID: btn_id,
                                    buttonName: btn_name,
                                    buttonVisibilityFormula: btn_visibility_formula
                                });
                            })

                            // var btnName = $(".setObject_btn").map(function(){
                            //     return $(this).children().attr("name");
                            // }).get().join();

                            // Get Workspace Content
                            var replace_backslash = new RegExp('\\u005C', 'g');

                            var workspace_content = $(".workspace").html().replace(replace_backslash, "&#92;");
                            var formId = $('#FormId').val();
                            var btnContent = $(".btn_content").html();

                            // Tagging
                            var tag_data = [];
                            var tag_length = $(".configTag");
                            if (tag_length != "0") {
                                var allow_tag = "1";
                                $(".configTag").each(function () {
                                    var data_id = $(this).attr("data-tag-id");
                                    //console.log(data_id);
                                    var data_json = $("body").data("data_" + data_id);
                                    //console.log(data_json)
                                    var tag = {};
                                    tag['tag_customer'] = data_json['tag_customer'];
                                    tag['tag_length'] = data_json['tag_length'];
                                    tag['tag_container_number'] = data_json['tag_container_number'],
                                            tag['tag_description'] = data_json['tag_description'];
                                    tag['tag_type'] = data_json['tag_type'];
                                    tag['tag_class'] = data_json['tag_class'];
                                    tag['tag_time_storage'] = data_json['tag_time_storage'];
                                    tag['tag_start_storage'] = data_json['tag_start_storage'];
                                    tag['tag_slot_width'] = data_json['tag_slot_width'];
                                    tag['tag_slot_height'] = data_json['tag_slot_height'];

                                    tag['tag_id'] = data_id;
                                    tag['tag_top'] = $(this).css("top");
                                    tag['tag_left'] = $(this).css("left");
                                    tag['tag_height'] = $(this).css("height");
                                    tag['tag_width'] = $(this).css("width");
                                    tag_data.push(tag);
                                    console.log(tag_data)
                                })
                            } else {
                                var allow_tag = "0";
                            }

                            //added by paolo, save slidemenu layer 
                            objectlayers.saveSlideMenuLayer();
                            //console.log(tag_data)
                            //console.log(JSON.stringify(tag_data))
                            // Set JSON
                            var workspace = {};
                            workspace["Title"] = workspace_title;
                            workspace["Alias"] = workspace_aliasName;
                            workspace["Aliasformu"] = workspace_aliasNameformu;
                            workspace["Enableformu"] = workspace_enableformu;
                            workspace["Enablefield"] = workspace_enablefield;
                            workspace["DisplayName"] = workspace_displayName;
                            workspace["workspace_buttonName"] = workspace_buttonName;
                            workspace["Prefix"] = workspace_prefix;
                            workspace["Type"] = workspace_type;
                            workspace["workspace_status"] = workspace_status;
                            workspace["workspace_version"] = workspace_version;
                            workspace["WorkspaceHeight"] = workspace_height;
                            workspace["WorkspaceWidth"] = workspace_width;
                            workspace["WorkspaceFormSizeTemplate"] = $('.form_size').val();
                            workspace['BtnLength'] = btn_length;
                            workspace['BtnName'] = btnName;
                            workspace['BtnProperties'] = JSON.stringify(button_properties_arr); //added by joshua reyes 03/07/2016 [Button Properties]
                            workspace['button_content'] = btnContent;
                            workspace['ObjectLength'] = object_length;
                            workspace['WorkspaceContent'] = workspace_content;
                            workspace['form_json'] = $("body").data();
                            workspace['form_fields'] = JSON.stringify(form_Fields_Arr);
                            workspace['categoryName'] = workspace_categoryname;
                            workspace["form_display_type"] = form_display_type;
                            workspace["fm-type-dos"] = form_display_category;
                            workspace["form_display-dynamic"] = form_display_formula;
                            workspace["display_import"] = display_import;
                            workspace["myrequest_visibility"] = myrequest_visibility; //my request visibility
                            workspace["tag_id"] = JSON.stringify(tag_data);
                            workspace["allow_tag"] = allow_tag;
                            workspace["allow_form_tagging"] = $("#allow_form_tagging").val();
                            workspace['form_json']['form_action_panel'] = {
                                'panel_design_type':$('.menu-type').val(),
                                'panel_menu_position':$('.menu-position').val(),
                                'panel_menu_text_alignment':$('.menu-text-alignment').val()
                            };
                            workspace['form_json']['allow_sending_pdf'] = $('.allow-sending-pdf').eq(0).is(':checked');


                            //console.log(workspace)
                            var json_a = $("body").data();

                            json_a['categoryName'] = workspace_categoryname;
                            json_a["form_display_type"] = form_display_type;
                            json_a["fm-type-dos"] = form_display_category;
                            json_a["form_display-dynamic"] = form_display_formula;
                            json_a["display_import"] = display_import;
                            json_a["myrequest_visibility"] = myrequest_visibility; //my request visibility
                            json_a["workspace_title"] = workspace_title;
                            json_a["workspace_status"] = workspace_status;
                            json_a["workspace_version"] = workspace_version;
                            json_a["Aliasformu"] = workspace_aliasNameformu;
                            json_a["Enableformu"] = workspace_enableformu;
                            json_a["Enablefield"] = workspace_enablefield;

                            if (typeof json_a.saving_request_format != "undefined") { //MOBILE JSON DATA
                                json_a.saving_request_format.view_settings = {};
                                if(json_a['collected_data_header_info']){
                                    json_a.saving_request_format.view_settings.collected_data_header_info = json_a['collected_data_header_info'];
                                }
                                if(json_a['allowReorder']){
                                    json_a.saving_request_format.view_settings.allowReorder = json_a['allowReorder'];
                                }
                                if(json_a['allowGeneralSearch']){
                                    json_a.saving_request_format.view_settings.allowGeneralSearch = json_a['allowGeneralSearch'];
                                }
                                if(json_a['allowColumnBorder']){
                                    json_a.saving_request_format.view_settings.allowColumnBorder = json_a['allowColumnBorder'];
                                }
                                if(json_a['fieldDefaultSort']){
                                    json_a.saving_request_format.view_settings.fieldDefaultSort = json_a['fieldDefaultSort'];
                                }
                                if(json_a['fieldDefaultSortType']){
                                    json_a.saving_request_format.view_settings.fieldDefaultSortType = json_a['fieldDefaultSortType'];
                                }
                                if(json_a['headerInfoType']){
                                    json_a.saving_request_format.view_settings.headerInfoType = json_a['headerInfoType'];
                                }
                                if(json_a['viewHighlight_background']){
                                    json_a.saving_request_format.view_settings.viewHighlight_background = json_a['viewHighlight_background'];
                                }
                                if(json_a['viewHighlight_font']){
                                    json_a.saving_request_format.view_settings.viewHighlight_font = json_a['viewHighlight_font'];
                                }
                                if(json_a['form_events']){
                                    if(json_a['ComputedFormulaEventList']){
                                        json_a.saving_request_format.formula_list = json_a['ComputedFormulaEventList'];
                                    }
                                    if(json_a['form_events']['compute_onload']){
                                        if(json_a.saving_request_format.mobile_form_properties){
                                            json_a.saving_request_format.mobile_form_properties.mobile_form_formula.onload.formula = json_a['form_events']['compute_onload'];    
                                        }
                                    }
                                }
                                workspace['MobileJsonData'] = JSON.stringify(json_a.saving_request_format);
                            } else {
                                workspace['MobileJsonData'] = "";
                            }

                            if (typeof json_a.saved_mobile_content != "undefined") {
                                workspace['MobileContent'] = json_a.saved_mobile_content;
                            } else {
                                workspace['MobileContent'] = "";
                            }
                            /*collecting existing fields on the form*/
                            workspace['existing_fields'] = [];
                            $(".setObject").each(function (eqi_setObj) {
                                var self_ele = $(this)
                                counted_id = self_ele.attr("data-object-id");
                                if (GetValidFieldsSOBJC(self_ele).length >= 1) {
                                    if (self_ele.find(".getFields_" + counted_id).attr("name")) {
                                        workspace['existing_fields'].push(self_ele.find(".getFields_" + counted_id).attr("name").replace("[]", ""));
                                    }
                                } else {
                                    return true;
                                }
                            });

                            var get_saved_sequence = $("body").data("saved_seq_field_array");
                            var temp_workspace_exist_field_index = -1;
                            if (get_saved_sequence) {
                                for (var key_index in get_saved_sequence) {
                                    if (workspace['existing_fields'].indexOf(get_saved_sequence[key_index]) >= 0) {
                                        temp_workspace_exist_field_index = workspace['existing_fields'].indexOf(get_saved_sequence[key_index]);
                                        workspace['existing_fields'].splice(temp_workspace_exist_field_index, 1);
                                    }
                                }
                                workspace['existing_fields'] = workspace['existing_fields'].concat(get_saved_sequence);
                            }

                            /*collecting existing fields on the form*/
                            workspace['existing_fields'] = workspace['existing_fields'].join(",");
                            /*
                             * 
                             * Added validation if the workspace type is for development
                             * reset the user settings to the creator only
                             * Added by Aaron Tolentino 02/24/2015 for ERP purpose
                             */
                            if (form_save_type == "2" && workspace_version == "2" && enable_staging == "1") {
                                var current_user_id = $("#current-user-id").text();
                                var json_reset_users = {"users": [current_user_id], "positions": [], "departments": []};
                                workspace['form_authors'] = JSON.stringify(json_reset_users);
                                json_a['form-authors'] = json_reset_users;

                                workspace['form_viewers'] = JSON.stringify(json_reset_users);
                                json_a['form-readers'] = json_reset_users;

                                workspace['form_users'] = JSON.stringify(json_reset_users);
                                json_a['form-users'] = json_reset_users;

                                workspace['customized-print'] = JSON.stringify(json_reset_users);
                                json_a['customized-print'] = json_reset_users;

                                workspace['nav-viewers'] = JSON.stringify(json_reset_users);
                                json_a['nav-viewers'] = json_reset_users;

                                workspace['report-users'] = JSON.stringify(json_reset_users);
                                json_a['report-users'] = json_reset_users;

                                workspace['delete-access'] = JSON.stringify(json_reset_users);
                                json_a['delete-access'] = json_reset_users;

                                workspace['form-admin'] = JSON.stringify([]);
                                json_a['form-admin'] = [];

                            } else {

                                // console.log(get_saved_sequence)
                                // console.log(workspace['existing_fields']);
                                //     return false;
                                //form / request privacy
                                var formUsers = json_a['form-users'];
                                if (json_a['form-authors']) {
                                    // workspace['form_authors'] = JSON.stringify(json_a['form-authors']);
                                    workspace['form_authors'] = JSON.stringify(checkIfFormUser(json_a['form-authors'], formUsers));
                                }

                                if (json_a['form-readers']) {
                                    // workspace['form_viewers'] = JSON.stringify(json_a['form-readers']);
                                    workspace['form_viewers'] = JSON.stringify(checkIfFormUser(json_a['form-readers'], formUsers));
                                }
                                if (json_a['form-users']) {
                                    workspace['form_users'] = JSON.stringify(json_a['form-users']);
                                }
                                if (json_a['customized-print']) {
                                    workspace['customized-print'] = JSON.stringify(json_a['customized-print']);
                                }
                                if (json_a['nav-viewers']) {
                                    workspace['nav-viewers'] = JSON.stringify(json_a['nav-viewers']);
                                }
                                if (json_a['report-users']) {
                                    workspace['report-users'] = JSON.stringify(json_a['report-users']);
                                }

                                if (json_a['delete-access']) {
                                    workspace['delete-access'] = JSON.stringify(json_a['delete-access']);
                                }
                                if (json_a['form-admin']) {
                                    var form_admin = json_a['form-admin'];
                                    if (form_admin != "0") {
                                        // if (form_admin['departments'].length == 0 && form_admin['positions'].length == 0 && form_admin['users'].length == 0) {
                                        //     workspace['form-admin'] = "0";
                                        //     json_a['form-admin'] = "0";
                                        // } else {
                                        workspace['form-admin'] = JSON.stringify(json_a['form-admin']);
                                        // }
                                    } else {
                                        workspace['form-admin'] = "0";
                                        json_a['form-admin'] = "0";
                                    }

                                } else {
                                    json_a['form-admin'] = "0";
                                    workspace['form-admin'] = "0";
                                }
                            }
                            //delete big datas
                            if (typeof workspace.form_json != "undefined") {
                                if (typeof workspace.form_json.manage_field_component_on_form != "undefined") {
                                    delete workspace.form_json.manage_field_component_on_form;
                                }
                                if (typeof workspace.form_json.manage_saved_ui_mobile != "undefined") {
                                    delete workspace.form_json.manage_saved_ui_mobile;
                                }
                            }
                            var collect_middleware = [];
                            $('[default-middleware-value][default-type="middleware"]').each(function () {
                                collect_middleware.push({
                                    "FieldName": $(this).attr('name'),
                                    "Formula": $(this).attr('default-middleware-value'),
                                    "MiddlewareExecutionTag": $(this).attr('middlewaretagging')
                                });
                            });
                            workspace['middleware'] = collect_middleware;

                            for (var key in workspace['form_json']) {
                                if (key.indexOf("notiData_") >= 0) {
                                    delete workspace['form_json'][key];
                                }
                            }
                            workspace["form_size"] = {};
                            if ($('.form_set_size.form_size_width').length >= 1) {
                                workspace["form_size"]["width"] = $('.form_set_size.form_size_width').val();
                            }
                            if ($('.form_set_size.form_size_height').length >= 1) {
                                workspace["form_size"]["height"] = $('.form_set_size.form_size_height').val();
                            }
                            if ($('.form_size').length >= 1) {
                                workspace["form_size"]["form_size_template"] = $('.form_size').val();
                            }

                            // console.log(workspace);
                            var getFieldsDataVar = getFieldsData();
                            // console.log(workspace);
                            workspace_version['fields_data'] = getFieldsDataVar;
                            workspace['fields_data'] = getFieldsDataVar;
                            workspace['allow_portal'] = $("#allow-portal").prop("checked");
                            workspace['portal_icon'] = $("#allow-portal").attr("selected-icon");   


                            // if( $('[data-ide-properties-type="ide-onload"]').is('[value]') ){
                               workspace['form_json'] = workspace['form_json']||{};
                               workspace['form_json']["form_events"] = workspace['form_json']["form_events"]||{};
                               workspace['form_json']["form_events"]["compute_onload"] = $('[data-ide-properties-type="ide-onload"]').val()||"";
                            // }
                            // if( $('[data-ide-properties-type="ide-pre-save"]').is('[value]') ){
                                // workspace['form_json'] = workspace['form_json']||{};
                                // workspace['form_json']["form_events"] = workspace['form_json']["form_events"]||{};
                                workspace['form_json']["form_events"]["compute_presave"] = $('[data-ide-properties-type="ide-pre-save"]').val()||"";
                            // }
                            // if( $('[data-ide-properties-type="ide-post-save"]').is('[value]') ){
                                // workspace['form_json'] = workspace['form_json']||{};
                                // workspace['form_json']["form_events"] = workspace['form_json']["form_events"]||{};
                                workspace['form_json']["form_events"]["compute_postsave"] = $('[data-ide-properties-type="ide-post-save"]').val()||"";
                            // }
                            workspace['form_json'] = workspace['form_json']||{};
                            workspace['form_json']["form_events"] = workspace['form_json']["form_events"]||{};
                            workspace['form_json']["form_events"]["form_formulas"] = scripting_tools.GetFieldsDetails().data;

                            $('body').data('is_active_mobility' , $('body').data('is_active_mobility')||false );
                            workspace['is_active_mobility'] = $('body').data('is_active_mobility');

                            workspace['form_json']['encrypted_fields'] = [];
                            var temporary = Object.keys($('body').data());
                            var temporary2 = null;
                            for(var keys in temporary){
                                if($.isNumeric(temporary[keys])){
                                    temporary2 = $('body').data(temporary[keys]);
                                    if(temporary2){
                                        if(temporary2['set_encryption']){
                                            workspace['form_json']['encrypted_fields'].push({
                                                'id': temporary[keys],
                                                'field_name': $('.getFields_'+temporary[keys]).attr('name'),
                                                'set_encryption': $('body').data(temporary[keys])['set_encryption']
                                            });
                                        }
                                    }
                                }else continue;
                            }
                            var json_encode = JSON.stringify(workspace);
                            //console.log(json_encode)

                            ui.block();
                            console.log("STARTING AJAX", form_save_type, json_encode);
                            console.time("START AJAX SAVING FORM BUILDER");

                            var resetUsersFlag = if_undefinded($(".form-category").attr("resetUsersFlag"), 0);

                            var ajax_formbuilder_save_setting = {
                                action: "saveWorkspace",
                                form_save_type: form_save_type,
                                json_encode: json_encode,
                                formId: formId,
                                resetUsersFlag: resetUsersFlag,
                                user_id: "" + $("html").data("login_expire_security_id")
                            };
                            
                            $.post("/ajax/formbuilder", ajax_formbuilder_save_setting, function (data) {
                                console.timeEnd("START AJAX SAVING FORM BUILDER",data);
                                if (data == "false") {//IF THEERE IS NO SESSION!!
                                    SessionExpire.run();




                                } else if (data == "not same id") {
                                    showNotification({
                                        message: "User working login not match... Reloading page.",
                                        type: "error",
                                        autoClose: true,
                                        duration: 3
                                    });
                                    setTimeout(function () {
                                        var pathname = window.location.pathname;
                                        // Gi and Forma
                                        var gi = $("gi").val();

                                        if (gi != "1-1") {
                                            var load = "/home";
                                            var load_1 = "/user_view/home";
                                        } else {
                                            var load = "/gi-dashboard-home";
                                            var load_1 = "/gi-dashboard-home";
                                        }
                                        if (pathname == "/formbuilder") {
                                            window.location.replace(load);
                                        } else if (pathname == "/user_view/formbuilder") {
                                            window.location.replace(load_1);
                                        }
                                    }, 3000);
                                } else if (data) {
                                    $(".form-category").removeAttr("resetUsersFlag")
                                    try {
                                        data = JSON.parse(data)
                                        console.log(data);
                                        showNotification({
                                            message: data['message'],
                                            type: data['status'],
                                            autoClose: true,
                                            duration: 3
                                        });
                                        if (data['status'] == "success") {
                                            FormUserAccess.removeTempCheckedUncheckedUsers(); // remove temp checked and unchecked users 
                                            FormUserAccess.udpdateOriginalUsersState();
                                            $("#popup_overlay,#popup_container").remove();
                                            bind_onBeforeOnload(0)
                                            // console.log(data)
                                            if (typeof data["saved_form_id"] != "undefined") {
                                                //removed checked
                                                setTimeout(function () {
                                                    var pathname = window.location.pathname;
                                                    if (pathname == "/formbuilder") {
                                                        window.location.replace("/formbuilder?formID=" + data["saved_form_id"] + "&view_type=edit");
                                                    } else if (pathname == "/user_view/formbuilder") {
                                                        window.location.replace("/user_view/formbuilder?formID=" + data["saved_form_id"] + "&view_type=edit");
                                                    }
                                                }, 3000);
                                            }
                                        }else if(data['status'] == 'warning'){
                                            setTimeout(function(){
                                                $('.submit-overlay').remove();
                                                // $('.fl-closeDialog').trigger("click");
                                            },1000);
                                        }
                                        $(".saveFormLoad").hide();
                                    } catch (err) {
                                        console.log(data);
                                    }
                                }
                                if(formId != 0 && formId != "0"){
                                    setTimeout(function(){
                                        ui.unblock();
                                    },3000);
                                }
                                

                            });
                            $('.fl-closeDialog').trigger('click');
                        }
                        break;
                }

            } else {
                $("#popup_overlay").remove();
                $("#popup_container").remove();
                showNotification({
                    message: "Please fix all invalid first before saving!",
                    type: "error",
                    autoClose: true,
                    duration: 5
                });
            }
            // }else{
            // window.open($(this).attr("href"), "popupWindow", "width=600,height=600,scrollbars=yes"); // login new window
            // }
            // })

        });
    },

    load_workspace_state: function () {

        var pathname = window.location.pathname;
        var user_view = $("#user_url_view").val();
        if (pathname == "/formbuilder" || pathname == "/user_view/formbuilder" || pathname == "/generate" || pathname == "/user_view/generate" || pathname == user_view + "create_forms") {

            $(".workspace.formbuilder_ws").droppable({
                accept: function(ele){
                    return ele.is(".setObject,.getObjects") && ele.parents('.public-embed-unused-object-list,.public-embed-object-list').length <= 0;
                },
                drop: function (e, ui) {

                    var side_ruler = $('.leftPointerRuler-tip').position().top;
                    var top_ruler = $('.topPointerRuler-tip ').position().left;
                    var drop_top = $(document).scrollTop();
                    var drop_left = $(document).scrollLeft();
                    var this_position_left = ui.helper.position().left;
                    var this_position_top = ui.helper.position().top;
                    var this_position_left_remainder = this_position_left % 50;
                    var this_position_top_remainder = this_position_top % 50;
                    //console.log("remainder",this_position_left_remainder,this_position_top_remainder)
                    //added by aaron for button droppable
                    if ($(ui.draggable).hasClass("getObjects")) {
                        ButtonDraggableFormObj.workspaceButtonDrop(e, ui, this);
                        return;
                    }



                    //comment for reference use
                    //evtarget = (window.event) ? window.event.srcElement /* for IE */ : e.target;
                    //console.log(e.target)
                    //console.log(ui.draggable[0].parentElement);
                    var target = e.target;
                    var source = ui.draggable[0].parentElement;
                    if (target === source) { //conditional purpose to prevent dropping off the same place
                        //alert('Same droppable container');
                        // if($('.form-builder-ruler.horizontal').exists()){
                        //  if((this_position_left < top_ruler + 25 && this_position_left > top_ruler - 25)){

                        //      $(ui.helper).appendTo(this);
                        //      $(ui.helper).css({

                        //          "left": (top_ruler) + "px"
                        //      });
                        //  }
                        //  if((this_position_top < side_ruler + 25  && this_position_top > side_ruler - 25) ){

                        //      $(ui.helper).appendTo(this);
                        //      $(ui.helper).css({

                        //          "top": (side_ruler) + "px"
                        //      });
                        //  }
                        // }
                        $(ui.helper).appendTo(this);
                        
                        $(ui.helper).snapToGrid();




                    } else {

                        if ($(this).children(".td-relative").eq(0).length == 0) {
                            var pos = ui.draggable.offset();
                            var dpos = $(this).offset();

                            if((pos.top - dpos.top)<11){
                                pos.top = 10;
                                dpos.top = 0;
                            }
                            if((pos.left-dpos.left)<29){
                                pos.left = 28;
                                dpos.left = 0;
                            }
                            drop_top = pos.top - dpos.top;
                            drop_left = pos.left - dpos.left;
                            $(ui.helper).appendTo(this);
                            $(ui.helper).css({
                                "top": (drop_top) + "px",
                                "left": (drop_left) + "px"
                            });
                        } else {
                            var pos = ui.draggable.offset();
                            var dpos = $(this).offset();
                            $(ui.helper).appendTo($(this).children(".td-relative").eq(0));
                            $(ui.helper).css({
                                "top": (drop_top) + "px",
                                "left": (drop_left) + "px"
                            });
                        }

                        //MULTIPLE DROPPABLE PREPAREDNESS
                        // if(e.altKey){
                        //     $('.component-ancillary-focus:not(".component-primary-focus")').map(function(){
                        //         var self = $(this);
                        //         var pos = self.offset();
                        //         self.css({
                        //             "top": pos.top - dpos.top,
                        //             "left":pos.left - dpos.left
                        //         });
                        //         return this;
                        //     }).appendTo($(this))
                        //     .snapToGrid();
                        // }

                        $(ui.helper).snapToGrid();
                    }
                    $(ui.helper).draggable("option", "containment", "parent");
                }
            }).on({
                "click": function (evs) {
                    // alert(13245)
                    //console.log($(evs.toElement).closest(".element"));
                    target = (window.event) ? window.event.srcElement /* for IE */ : evs.target;
                    // console.log($(target).closest(".element").length);
                    if ($(target).closest(".setObject").length >= 1) { //has a parent named class
                        //walang gagawin
                    } else if ($(".dynamic-td-droppable-selected").length >= 1) {
                        $(".dynamic-td-droppable-selected").removeClass("dynamic-td-droppable-selected");
                    }
                    if ($(target).closest(".setObject").length >= 1) {
                        //walang gagawin
                    } else {
                        $(".component-primary-focus").removeClass("component-primary-focus");
                        $(".component-ancillary-focus").removeClass("component-ancillary-focus");
                        $(".layerhighlight").removeClass("layerhighlight");
                    }
                }
            });

            $(".dynamic-td-droppable-selected").removeClass("dynamic-td-droppable-selected");
            if ($("#FormId").val() != 0) {
                var json = $(".form-workspace-json").text();

            var pathname = window.location.pathname;
            if (pathname == "/user_view/formbuilder") {
                $.ajax({
                    url: '/ajax/getFormData?form_id=' + $("#FormId").val(),
                    async: false,
                    type: 'GET',
                    cache: false,
                    success: function (data) {
                        json = data;
                    }
                });
            }

                if (json != "") {
                    //console.log(json)

                    var form_json = JSON.parse(json);
                    $("body").data(form_json['form_json']);
                    setSavedFormUsers();
                    FormUserAccess.removeTempCheckedUncheckedUsers();
                }

                // Tagging

                if (form_json) {
                    if (form_json['allow_form_tagging'] == "1") {
                        $checked = "checked";
                    } else {
                        $checked = "";
                    }
                    $("#allow_form_tagging").val(form_json['allow_form_tagging']);
                    $("#allow_form_tagging").attr("checked", $checked);
                }
                var countings = [];
                
                if ($(".workspace.formbuilder_ws").find(".setObject").length >= 1) {

                    var elements = $('.setObject');
                    //console.log("ID", $(this).attr('data-object-id'));
                    var objID = elements.attr('data-object-id');
                    var setObjIdTag = $('#setObject_'+elements.attr('data-object-id'));
                    var labelTag = $('#label_'+elements.attr('data-object-id'));
                    var fieldTag = $('#getFields_'+elements.attr('data-object-id'));
                    objectCSSProperties(objID, setObjIdTag, labelTag, fieldTag, elements);

                    if (form_json) {

                        if (form_json.WorkspaceWidth) {

                            $(".form_size_width").val(form_json.WorkspaceWidth);
                            $(".form_size_height").val(form_json.WorkspaceHeight);



                            $(".workspace.formbuilder_ws").css({
                                "width": (form_json.WorkspaceWidth) + "px",
                                "height": (form_json.WorkspaceHeight) + "px"
                            });

                            //console.log("HALA KA", $(".fl-input-form-settings .form_size_width"), form_json.WorkspaceWidth) //.fl-form-setting-content 
                            $(".fl-input-form-settings .form_size_width").val(form_json.WorkspaceWidth);
                            $(".fl-input-form-settings .form_size_height").val(form_json.WorkspaceHeight);
                            console.log("size", form_json.WorkspaceFormSizeTemplate)

                            if (form_json.WorkspaceFormSizeTemplate) {


                                $(".form_size").val(form_json.WorkspaceFormSizeTemplate);

                                //added by roni check on first load if selection is null apply below
                                if ($(".form_size").val() === null) {
                                    $('.fl-opt_custom_size').prop('selected', true);
                                    // enableFormSetSizeField();
                                }

                                if ($(".form_size").val() !== null) {

                                    if ($('.form_size').children('option:selected').is(':not(.fl-opt_custom_size)')) {
                                       
                                    };
                                }


                                console.log($('.form_size').children('option').attr("value"));
                            }

                            if ($('.form_size').children('option:selected').is(':not(.fl-opt_custom_size)')) {
                                $(".form_size").val(form_json.WorkspaceFormSizeTemplate);

                            };


                            if ($('.form_size').children('.fl-opt_custom_size:selected').length >= 1) {

                                console.log($('.form_size').children('.fl-opt_custom_size').attr("value"));
                                $('.form_size').children('.fl-opt_custom_size:selected').data("custom_set_size", {
                                    "width": (form_json.WorkspaceWidth) + "",
                                    "height": (form_json.WorkspaceHeight) + ""
                                });
                            } else {
                                $('.form_size').children('.fl-opt_custom_size').data("custom_set_size", {
                                    "width": (form_json.WorkspaceWidth) + "",
                                    "height": (form_json.WorkspaceHeight) + ""
                                });
                            }
                            $(window).trigger('resize'); //FS#8154
                        }
                        try{
                            
                            if($.type(form_json['allow_portal'])!="undefined"){
                                
                                $("#allow-portal").prop("checked",form_json['allow_portal']);
                                $("#allow-portal").attr("selected-icon", form_json['portal_icon']);
                                
                                
                                if(form_json['allow_portal']==true){

                                    // $('.allow_portal_size').prop('selected', true); // FS#7484
                                    $('.choose_app_icon').removeClass('isDisplayNone').addClass('isDisplayBlock');
                                    enableFormSetSizeField();

                                }
                            }

                            { // FS#7484
                               // var fs_ele = $('.form_size ');
                               // var selected_option_ele = fs_ele.children('option:selected');
                               // if(selected_option_ele.is('.fl-opt_custom_size')){
                               //     enableFormSetSizeField();
                               //     $('#allow-portal').prop('checked', false);
                               //     $('.choose_app_icon').removeClass('isDisplayBlock').addClass('isDisplayNone');
                               // }else if(selected_option_ele.is('.allow_portal_size')){
                               //     enableFormSetSizeField();
                               //     $('#allow-portal').prop('checked', true);
                               //     $('.choose_app_icon').removeClass('isDisplayNone').addClass('isDisplayBlock');
                               // }else{
                               //     specificSizeForm();
                               //     $('#allow-portal').prop('checked', false);
                               //     $('.choose_app_icon').removeClass('isDisplayBlock').addClass('isDisplayNone');
                               // }
                            }
                        }catch(e){

                        }
                             
                        $("#allow-portal").attr("selected-icon",$("#saved_portal_icon").val());
                    }


                    /*removing excess ui's and classes UPGRADES*/
                    $(".ui-resizable").removeClass("ui-resizable");
                    $(".ui-resizable-handle").remove();
                    $(".setObject-drag-handle").remove(); // ADDED 12 2 2013
                    $(".component-ancillary-focus").removeClass("component-ancillary-focus");
                    $(".component-primary-focus").removeClass("component-primary-focus");
                    $(".icon-remove").addClass("fa fa-times");
                    $(".icon-cogs").addClass("fa fa-cogs");
                    $(".icon-list-alt").addClass("fa fa-list-alt");
                    $(".NM-redborder").removeClass("NM-redborder");
                    $(".NM-notification").remove();
                    $('.ul-scroll-anchor').remove();

                    /*getting counts and making the objects on the form functional*/

                    /*accordion*/
                    $('.form-accordion').closest('.setObject').each(function () {

                        $(this).rebindDragObects();

                    });


                    /*TAB PANEL*/
                    $(".form-tabbable-pane").each(function (eqi_tab_panel) {
                        $(this).closest('.fields_below').find('.label_below label').css('cursor', 'move');
                        countings.push(parseInt($(this).closest(".setObject").attr("data-object-id")));
                        dragObects($(this).closest(".setObject"), ".workspace", $(".form_set_size.form_size_height").val(), $(this).closest(".setObject").attr("data-object-id"));
                        $(this).find(".li-add-new-tab-panel").remove();
                        $(this).tabPanel({
                            resizableContent: true,
                            resizableContentContainment: ".workspace.formbuilder_ws"
                        });
                    });

                    /*TABLE*/
                    $(".form-table").each(function () {
                        $(this).closest('.fields_below').find('.label_below label').css('cursor', 'move');
                        countings.push(parseInt($(this).closest(".setObject").attr("data-object-id")));
                        dragObects($(this).closest(".setObject"), ".workspace", $(".form_set_size.form_size_height").val(), $(this).closest(".setObject").attr("data-object-id"));
                        if ($(this).closest(".table-wrapper").length >= 1) {
                            $(this).closest(".table-wrapper").find(".table-actions-ra").remove();
                            $(this).find(".dynamic-td-droppable-selected").removeClass("dynamic-td-droppable-selected");
                            $(this).removeClass(".thisDynamicTable");
                            $(this).unwrap();
                            $(this).dynamicTable({
                                "tableBorder": "1",
                                "tHeadTag": false,
                                droppableCell: true,
                                cellDroppable: {
                                    accept: ".setObject",
                                    revert: true,
                                    greedy: true,
                                    drop: function (e, ui) {
                                        //comment for reference use
                                        //evtarget = (window.event) ? window.event.srcElement /* for IE */ : e.target;
                                        //console.log(e.target);
                                        //console.log(ui.draggable[0].parentElement);
                                        var target = e.target;
                                        var source = ui.draggable[0].parentElement;


                                        if (
                                                $(ui.helper).find(".form-table").length == 0 && $(ui.helper).find(".form-tabbable-pane").length == 0 ||
                                                ($(".table-form-design-relative").length >= 1 && $(target).hasClass("fcp-td-relative"))
                                                ) {
                                            if (target === source) { //conditional purpose to prevent dropping off the same place
                                                //alert('Same droppable container');
                                            } else {

                                                // console.log("TRALALA",$(target), $(source), $(this), $(ui.helper));
                                                // console.log("SABOG",$(ui.helper).is('[data-type="noteStyleTextarea"]') , $(target).hasClass('td-relative') , $(target).children('.setObject').length >= 1);
                                                if ($(ui.helper).is('[data-type="noteStyleTextarea"]') && $(target).hasClass('td-relative') && $(target).children('.setObject').length >= 1) {
                                                    // console.log("APPENDTO", source)
                                                    jAlert("Notes Style requires empty cell pane to drop.", "", "", "", "", function () {
                                                    });
                                                    $(ui.helper).appendTo(source);
                                                } else if ($(target).hasClass('td-relative') && $(target).children('.setObject[data-type="noteStyleTextarea"]').length >= 1) {
                                                    jAlert("You are not allowed to drop other fields in a cell pane which a Notes Style is present.", "", "", "", "", function () {
                                                    });
                                                    $(ui.helper).appendTo($(source));
                                                } else if ($(target).hasClass('td-relative') && $(target).children('.setObject[data-type="table"]').length >= 1) {
                                                    jAlert("You are not allow to drop any other fields in that cell pane if a table is present.", "", "", "", "", function () {
                                                    });
                                                    $(ui.helper).appendTo($(source));
                                                } else if ($(target).hasClass('td-relative') && $(target).children('.setObject[data-type="tab-panel"]').length >= 1) {
                                                    jAlert("You are not allow to drop any other fields in that cell pane if a tab panel is present.", "", "", "", "", function () {
                                                    });
                                                    $(ui.helper).appendTo($(source));
                                                } else {
                                                    if ($(this).children(".td-relative").eq(0).length == 0) {
                                                        $(ui.helper).appendTo(this);
                                                        $(ui.helper).css({
                                                            "top": "0px",
                                                            "left": "0px"
                                                        });
                                                    } else {
                                                        $(ui.helper).appendTo($(this).children(".td-relative").eq(0));
                                                        $(ui.helper).css({
                                                            "top": "0px",
                                                            "left": "0px"
                                                        });
                                                    }
                                                    console.log("IMPOSSIBLE", $(ui.helper).outerWidth(), $(this).outerWidth());
                                                    console.log("GRAAA", $(ui.helper), $(this));
                                                    if ($(ui.helper).outerWidth() > $(this).outerWidth()) {
                                                        ung_padding_left = $(ui.helper).css("padding-left").split("px").join("");
                                                        ung_padding_right = $(ui.helper).css("padding-right").split("px").join("");
                                                        draggable_ele_width = $(ui.helper).outerWidth();
                                                        var collect_most_right = [draggable_ele_width];

                                                        if ($(this).hasClass("ui-droppable")) {
                                                            dis_tr_index = $(this).closest("tr").index();
                                                            if ($(this).prop("tagName") == "TD") {
                                                                dis_td_index = $(this).index();
                                                            } else {
                                                                dis_td_index = $(this).closest("td.ui-resizable").index();
                                                            }

                                                            var dropped_column = $(this).closest("tbody").children("tr").children("td.ui-resizable").filter(function (eqi, ele) {
                                                                if ($(ele).index() == dis_td_index) {
                                                                    return true;
                                                                } else {
                                                                    return false;
                                                                }
                                                            });
                                                            dropped_column.children(".td-relative").children(".setObject").each(function (eqi, elem) {
                                                                collect_most_right.push($(this).outerWidth() + $(this).position().left);
                                                            });
                                                            collect_most_right.sort(function (a, b) {
                                                                return b - a;
                                                            });
                                                            dropped_column.css({
                                                                "width": (collect_most_right[0]) + "px"
                                                            });
                                                            dropped_column.children(".td-relative").css({
                                                                "min-width": (collect_most_right[0]) + "px"
                                                            });
                                                            console.log("AMP", dropped_column.children(".td-relative").children(".setObject"))

                                                            // $(this).closest("tbody").children("tr").each(function() {
                                                            //     $(this).children("td.ui-resizable").eq(dis_td_index).css({
                                                            //         "width": draggable_ele_width
                                                            //     });
                                                            //     console.log("TEKA HERE DROPPABLE",$(this).children("td.ui-resizable").eq(dis_td_index))
                                                            //     // $(this).children("td.ui-resizable").eq(dis_td_index).resizable({
                                                            //     //     "minWidth": draggable_ele_width
                                                            //     // });
                                                            // });
                                                        }
                                                    }
                                                    if ($(ui.helper).outerHeight() > $(this).outerHeight()) {
                                                        ung_padding_top = $(ui.helper).css("padding-top").split("px").join("");
                                                        ung_padding_bottom = $(ui.helper).css("padding-bottom").split("px").join("");
                                                        draggable_ele_height = $(ui.helper).outerHeight();
                                                        if ($(this).hasClass("ui-droppable")) {
                                                            $(this).closest("tr").children("td.ui-resizable").css({
                                                                "height": draggable_ele_height
                                                            });
                                                            // $(this).closest("tr").children("td.ui-resizable").resizable({
                                                            //     "minHeight": draggable_ele_height
                                                            // });
                                                        }
                                                    }
                                                    if ($(source).hasClass("td-relative")) {
                                                        var collect_objs_bottom = [];
                                                        var collect_objs_right = [];
                                                        $(source).children(".setObject:visible").each(function () {
                                                            collect_objs_bottom.push($(this).position().top + $(this).outerHeight());
                                                            collect_objs_right.push($(this).position().left + $(this).outerWidth());
                                                        });
                                                        var dis_tr_index = $(source).closest("tr").index();
                                                        if ($(source).prop("tagName") == "TD") {
                                                            var dis_td_index = $(source).index();
                                                        } else {
                                                            var dis_td_index = $(source).closest("td.ui-resizable").index();
                                                        }
                                                        collect_objs_bottom.concat(
                                                                $(source).closest("tr").eq(dis_tr_index).children("td").children(".td-relative").children(".setObject:visible").get().map(function (ele, eqi) {
                                                            return ($(ele).position().top + $(ele).outerHeight());
                                                        })
                                                                );

                                                        collect_objs_right.concat(
                                                                $(source).closest("tbody").children("tr").children("td").filter(function (eqi, ele) {
                                                            if ($(ele).index() == dis_td_index) {
                                                                return true;
                                                            } else {
                                                                return false;
                                                            }
                                                        }).children(".td-relative").children(".setObject:visible").get().map(function (ele, eqi) {
                                                            return ($(ele).position().left + $(ele).outerWidth());
                                                        })
                                                                );

                                                        collect_objs_bottom.sort(function (a, b) {
                                                            return b - a;
                                                        })
                                                        collect_objs_right.sort(function (a, b) {
                                                            return b - a;
                                                        });

                                                        if (collect_objs_bottom.length >= 1 && collect_objs_right.length >= 1) {










                                                            $(source).closest("tbody").children("tr").each(function () {
                                                                $(this).children("td.ui-resizable").eq(dis_td_index).css({
                                                                    "width": collect_objs_right[0]
                                                                });
                                                                $(this).children("td.ui-resizable").eq(dis_td_index).resizable({
                                                                    "minWidth": collect_objs_right[0]
                                                                });
                                                                if ($(this).index() == dis_tr_index) { // buong row dapat magkakapareha ng height
                                                                    $(this).children("td.ui-resizable").each(function () {
                                                                        $(this).css({
                                                                            "height": collect_objs_bottom[0]
                                                                        });
                                                                        $(this).children("td.ui-resizable").resizable({
                                                                            "minHeight": collect_objs_bottom[0]
                                                                        });
                                                                    })
                                                                }
                                                            })
                                                        } else {
                                                            console.log("HEYO! DELO", $(source).closest("td.ui-resizable"))
                                                            $(source).closest("td.ui-resizable").resizable({
                                                                "minHeight": 0,
                                                                "minWidth": 0
                                                            });
                                                        }
                                                    }
                                                }

                                            }


                                            $(ui.helper).draggable("option", "containment", "parent");
                                            if ($(ui.helper).data()) {
                                                if ($(ui.helper).data().uiResizable) {
                                                    $(ui.helper).resizable("option", "containment", "parent");
                                                }
                                            }

                                            // alert("test");
                                            // console.log($(source));
                                            //set a min height and min width of the cell when object leaves a cell

                                        }

                                    }
                                }
                            })
                            // .css({
                            //     "border": "none"
                            // });
                            if ($(this).attr("repeater-table") == "true") {
                                $(this).closest(".table-wrapper").find(".table-actions-ra.action-row-add").hide();
                            }
                        }
                    });

                    //making objects draggable and resizable
                    $('.setObject[data-object-id]').each(function (eqi_setObj) {

                        if ($(this).find(".form-table").length >= 1 || $(this).find(".form-tabbable-pane").length >= 1) {
                            return true;
                        }
                        countings.push(parseInt($(this).attr("data-object-id")));
                        dragObects($(this), ".workspace", $(".form_set_size.form_size_height").val(), $(this).attr("data-object-id"));

                        //set the default values of checkbox and radio
                        var object_id = $(this).attr("data-object-id");
                        var frm_json = $("body").data();
                        if (
                                ($(this).find(".getFields_" + object_id).attr("type") == "radio" || $(this).find(".getFields_" + object_id).attr("type") == "checkbox" || $(this).find(".getFields_" + object_id).is('smart-barcode-field-input-collection')) &&
                                $(this).find(".getFields_" + object_id).prop("tagName") == "INPUT"
                                ) {
                            var self = $(this);
                            if (typeof frm_json[object_id] != "undefined") {
                                if (typeof frm_json[object_id].defaultValues != "undefined") {
                                    $.each(frm_json[object_id].defaultValues, function (index, item) {
                                        var string_value = frm_json[object_id].defaultValues[index];
                                        if (
                                                self.find(".getFields_" + object_id).eq(index).length >= 1 &&
                                                self.find(".getFields_" + object_id).eq(index).val() == string_value
                                                ) {
                                            self.find(".getFields_" + object_id).eq(index).prop("checked", true);
                                        }
                                    });
                                }
                            }
                        } else if ($(this).find(".getFields_" + object_id).prop("tagName") == "SELECT") {
                            var self = $(this);
                            if (typeof frm_json[object_id] != "undefined") {
                                if (typeof frm_json[object_id].defaultValues != "undefined") {
                                    $.each(frm_json[object_id].defaultValues, function (index, item) {
                                        var string_value = frm_json[object_id].defaultValues[index];
                                        if (
                                                self.find(".getFields_" + object_id).children("option").eq(index).length >= 1 &&
                                                self.find(".getFields_" + object_id).children("option").eq(index).val() == string_value
                                                ) {
                                            self.find(".getFields_" + object_id).children("option").eq(index).prop("selected", true);
                                        }

                                    })
                                }
                            }
                        }
                    });

                    $(".lbl-aligned-left").each(function () {
                        $(this).resizable({
                            handles: "e"
                        })
                    })

                    if ($('.table-form-design-relative').length >= 1) {
                        var tfdr_ele = $('.table-form-design-relative');
                        tfdr_ele.off('dblclick.doubleClickProp');
                        tfdr_ele.draggable("destroy");
                        $('.show-form-design-select[value="relative"]').trigger("click");
                        $('.show-form-design-select[value="relative"]').trigger("change", [{"auto": true}]);
                        $('.formbuilder_ws').css({
                            "width": "auto",
                            "height": "auto"
                        });
                    }
                    if ($('.fdr-table-for-tab-panel').length >= 1) {
                        $('.fdr-table-for-tab-panel').each(function () {
                            //
                            $(this).off('dblclick.doubleClickProp');
                            $(this).off('mousedown.fieldObjectFocus');
                            $(this).draggable('destroy');
                        });
                    }

                    countings = countings.sort(function (a, b) {
                        return b - a
                    }); //getting how many objects created on the form and sort this variable to know what is the total countings
                    count = countings[0];

                    count++;
                    /*getting counts and making the objects on the form functional*/



                }
                else {
                    if (form_json) {

                        if (form_json.WorkspaceWidth) {

                            $(".form_size_width").val(form_json.WorkspaceWidth);
                            $(".form_size_height").val(form_json.WorkspaceHeight);
                            $(".workspace.formbuilder_ws").css({
                                "width": (form_json.WorkspaceWidth) + "px",
                                "height": (form_json.WorkspaceHeight) + "px"
                            });

                            console.log("HALA KA", $(".fl-input-form-settings .form_size_width"), form_json.WorkspaceWidth) //.fl-form-setting-content 
                            $(".fl-input-form-settings .form_size_width").val(form_json.WorkspaceWidth); //.fl-form-setting-content 
                            $(".fl-input-form-settings .form_size_height").val(form_json.WorkspaceHeight); //.fl-form-setting-content 

                            if (form_json.WorkspaceFormSizeTemplate) {
                                $(".form_size").val(form_json.WorkspaceFormSizeTemplate);
                                

                            }

                            if ($('.form_size').children('.fl-opt_custom_size:selected').length >= 1) {
                                $('.form_size').children('.fl-opt_custom_size:selected').data("custom_set_size", {
                                    "width": (form_json.WorkspaceWidth) + "",
                                    "height": (form_json.WorkspaceHeight) + ""
                                });
                            }
                            ;

                        }
                    }

                    $('.btn_content').children('[data-type="button"]').each(function () {
                        countings.push(parseInt($(this).attr("data-object-id")));
                        dragObects($(this), '.btn_content', '', $(this).attr('data-object-id'));
                        console.log('doid', $(this).attr('data-object-id'));


                    });
                    countings = countings.sort(function (a, b) {
                        return b - a;
                    }); //getting how many objects created on the form and sort this variable to know what is the total countings
                    console.log('count', countings);
                    count = countings[0];


                    count++;
                }
                if(form_json['form_json']){
                    if(form_json['form_json']['form_events']){
                        if(form_json['form_json']['form_events']['compute_onload']){
                            $('[data-ide-properties-type="ide-onload"]').val(form_json['form_json']['form_events']['compute_onload']);
                        }
                        if(form_json['form_json']['form_events']['compute_presave']){
                            $('[data-ide-properties-type="ide-pre-save"]').val(form_json['form_json']['form_events']['compute_presave']);
                        }
                        if(form_json['form_json']['form_events']['compute_postsave']){
                            $('[data-ide-properties-type="ide-post-save"]').val(form_json['form_json']['form_events']['compute_postsave']);
                        }
                    }
                }
            } else {
                //new form

                RequestPivacy.defaultCheckedNewForm();
            }

            //remove this below after 4-5 months
            $('.getFields[data-type="float"]').each(function () {
                $(this).attr("data-type", "double")
            });
            $('.setObject').find('[name],[embed-name]').each(function () {

                if ($(this).is('[name]')) {
                    var ako_toh = $(this).attr('name');
                    ako_toh = ako_toh.replace("[]", "");

                }
                else {
                    var ako_toh = $(this).attr('embed-name');
                }
                var parent_nya = $(this).parents('.setObject').eq(0);
                var may_handle = parent_nya.children('.fields_below').eq(0).children('.setObject-drag-handle');
                if (parent_nya.is('[data-original-title][data-type="pickList"]') || parent_nya.is('[data-original-title][data-type="button"]')) {
                    parent_nya.removeAttr('data-original-title');
                }

                else if (may_handle.length >= 1) {

                    may_handle.attr('data-original-title', ako_toh).tooltip();
                }
                else {
                    // parent_nya.attr('data-original-title',ako_toh).tooltip();
                }

            });
            $('.setObject[data-type="embeded-view"]').each(function () {
                var self_ele = $(this);//obj_actions_1
                var soid = self_ele.attr('data-object-id');
                var actions_container = self_ele.find('[id="obj_actions_' + soid + '"]');
                if (actions_container.find('[data-properties-type="Embedded View Properties"]').length <= 0) {
                    var button_prop = actions_container.find('[data-properties-type="embedview"]').clone(true);
                    if (actions_container.children('ul').length <= 0) {
                        actions_container.prepend(button_prop);
                        button_prop.attr('data-properties-type', 'Embedded View Properties');

                        button_prop.removeClass('fa fa-cogs').addClass('fa fa-eye').attr('data-original-title', 'Embedded View Properties');
                    } else {

                        actions_container.children('ul').prepend(button_prop);
                        button_prop.wrap('<li/>');
                        button_prop.attr('data-properties-type', 'Embedded View Properties');
                        button_prop.removeClass('fa fa-cogs').addClass('fa fa-eye').attr('data-original-title', 'Embedded View Properties');
                        actions_container.find('[data-properties-type="embedview"]').attr('data-original-title', 'Basic Properties');

                    }
                }
                else if (actions_container.find('[data-properties-type="Embedded View Properties"]').is('.icon-cogs.fa-cogs')) {

                    actions_container.find('[data-properties-type="Embedded View Properties"]').removeClass('fa-cogs').attr('data-original-title', 'Embedded View Properties');
                    actions_container.find('[data-properties-type="embedview"]').attr('data-original-title', 'Basic Properties');
                }

            });
            
            if( $('.table-form-design-relative').length >= 1 ){
                $('[data-object-type="noteStyleField"]').show();
                $('[data-object-type="noteStyleField"]').removeClass('isDisplayNone');
            }
        }

        //$('.form-select.form_size').trigger('change');
        collectNames();
        dropdown_reset();
        if( $('.table-form-design-relative').length >= 1 ){
            $('[data-object-type="noteStyleField"]').removeClass('isDisplayNone');
        }
        if($('.setObject[data-type="imageOnly"]').exists()){
            $('.setObject[data-type="imageOnly"]').each(function(){
                var imageSrc = $(this).find('img.getFields').attr('src');
                if(imageSrc.indexOf('/') != 0){
                    var imageArr = imageSrc.split('/');
                    console.log('imageArr',imageArr);
                    delete imageArr[0];
                    delete imageArr[1];
                    delete imageArr[2];
                    imageArr = imageArr.filter(Boolean);
                    var newSrc = imageArr.join('/');
                    newSrc = "/"+newSrc;
                    $(this).find('img.getFields').attr('src',newSrc);

                }
                
            });
        }
        defaultObjectProperties();

        $('.embed-view-container[embed-row-click-creation-form="true"]').each(function(){ // FS#8105
            var doi = $(this).parents('.setObject[data-object-id]:eq(0)').attr('data-object-id');
            var add_new_request_template = $('<a class="embed_newRequest fl-buttonEffect btn-basicBtn cursor" style="display:inline-block;padding:5px;margin-bottom:5px; font-size:12px; color: #fff;text-decoration:none;">Add Request</a>');
            var temp = $('body').data(doi);
            $(this).parent().children('.embed_newRequest').remove(); //FS#8105 - The default add button was displayed after resaving the form.
            $(this).before(add_new_request_template.clone().text(temp['link_caption']));
        });
        if(form_json){
            if(form_json['form_json']){
                
                if(form_json['form_json']['form_action_panel']){

                    $('.menu-type').val(($.type(form_json['form_json']['form_action_panel'])=='object'?form_json['form_json']['form_action_panel']:{'panel_design_type':'0'}).panel_design_type);
                    if ($('.menu-type').val() == 1) {
                        $('.menu-position-wrapper').removeClass('isDisplayNone').addClass('isDisplayBlock')
                    }
                    
                    $('.menu-position').val(($.type(form_json['form_json']['form_action_panel'])=='object'?form_json['form_json']['form_action_panel']:{'panel_menu_position':'0'}).panel_menu_position);
                }
                if(form_json['form_json']['allow_sending_pdf']){
                    $('.allow-sending-pdf').eq(0).prop('checked', workspace['form_json']['allow_sending_pdf']);
                    $('.menu-text-alignment').val(($.type(form_json['form_json']['form_action_panel'])=='object'?form_json['form_json']['form_action_panel']:{'panel_menu_text_alignment':'left'}).panel_menu_text_alignment)

                }
            }

            
        }
    }
}
$(document).ready(function(){

    $('.menu-type').on('change', function(){

        var menuPosition = $(this).parents('.fl-controls-controlformControlPanel').find('.menu-position-wrapper');
        if ($(this).val() == 1) {
            menuPosition.removeClass('isDisplayNone').addClass('isDisplayBlock')
        }else {
            menuPosition.removeClass('isDisplayBlock').addClass('isDisplayNone')
            menuPosition.find('.menu-position').val(0);
        }
    })

    //event change of processor type
    if(window.pathname.indexOf('rule_settings') < 0){
        $("body").on("change","#workflow-processor-type, .workflow-processor-type",function(){ //from custom.js 12 22 2015 3 05 PM
              var value = $(this).val();
          $("#workflow-processor, #workflow-processor").hide();
          $("#workflow-processor").next().show();
              getprocessor(value,0);
        })

        // //added for ruler initialization by michael 10:59 AM 10/8/2015
        $('.fl-set-ruler').ruler({
            vRuleSize: 18,
            hRuleSize: 18,
            showCrosshair : false,
            showMousePos: false
        });
    }
    control_save_workspace(".save_workspace");


//show ruler formbuilder/workflow
  $(".f-show-ruler[name='show-form-ruler-select']").on({
        "change": function (e) {
            //alert($(this).val())
            selected_val_show = $(this).val();
            if (selected_val_show == "yes") {
                // if($(".topPointerRuler-container").length >= 1){
                //  $(".topPointerRuler-container").show();
                // }
                // if($(".leftPointerRuler-container").length >= 1){
                //  $(".leftPointerRuler-container").show();
                // }
                if ($(".dragging-static-ruler.top").length == 0) {
                    tooltip_ele_top = $("<span class='dragging-static-ruler top'></span>")
                    $("body").append(tooltip_ele_top);
                    tooltip_ele_top.css({
                        "position": "absolute",
                        //"font-weight":"bold",
                        "font-size": "15px",
                        "color": "white",
                        "background-color": "rgba(0,0,0,0.25)",
                        "border": "1px solid rgba(0,0,0,0.5)",
                        "padding": (1) + "px",
                        "display": "none"

                    });
                    $(".topPointerRuler-tip").draggable("option", "tooltip_ele_top", tooltip_ele_top);
                    tooltip_ele_top.html($(".topPointerRuler-tip").position().left + 5)
                    tooltip_ele_top.css({
                        "top": ($(".topPointerRuler-tip").offset().top + 12) /*$(tooltip_ele_top).outerHeight())*/ + "px",
                        "left": ($(".topPointerRuler-tip").offset().left) + "px"
                    })

                    the_ruler_top = $("<div class='form-builder-ruler vertical' style='z-index:100;'></div>");
                    var the_workspace_top = $(".workspace.formbuilder_ws,.workspace.workflow_ws,.workspace.orgchart_ws");
                    the_workspace_top.append(the_ruler_top);
                    var inside_ruler_top = the_ruler_top.clone().addClass('isDisplayNone');
                    $('.hRule').prepend(inside_ruler_top)
                    var the_padding_top = the_workspace_top.css('padding-top');
                    the_ruler_top = the_ruler_top.add(inside_ruler_top);


                    the_ruler_top.css({
                        "position": "absolute",
                        "background-color": "blue",
                        "width": (1) + "px",
                        "height": "100%",
                        "left": ($(".topPointerRuler-tip").position().left + 5) + "px"
                    });
                    the_ruler_top.eq(1).css({
                        "height": "calc( 100% - " + the_padding_top + " )"
                    });
                    $(".topPointerRuler-tip").draggable("option", "formbuilder_ruler_vertical", the_ruler_top);
                    the_ruler_top.draggable({
                        snap: ".setObject, .form-builder-ruler",
                        snapTolerance: 3,
                        disabled: true,
                        snapMode: "outer"
                    });
                }

                if ($(".dragging-static-ruler.left").length == 0) {
                    tooltip_ele_left = $("<span class='dragging-static-ruler left'></span>")
                    $("body").append(tooltip_ele_left);
                    tooltip_ele_left.css({
                        "position": "absolute",
                        //"font-weight":"bold",
                        "font-size": "15px",
                        "color": "white",
                        "background-color": "rgba(0,0,0,0.25)",
                        "border": "1px solid rgba(0,0,0,0.5)",
                        "padding": (1) + "px",
                        "display": "none"
                    });
                    $(".leftPointerRuler-tip").draggable("option", "tooltip_ele_left", tooltip_ele_left);
                    tooltip_ele_left.html($(".leftPointerRuler-tip").position().top + 5)
                    tooltip_ele_left.css({
                        "top": ($(".leftPointerRuler-tip").offset().top) + "px",
                        "left": ($(".leftPointerRuler-tip").offset().left + 13) /*- $(tooltip_ele_left).outerWidth())*/ + "px"
                    })
                    /*
                     the_ruler_top = $("<div class='form-builder-ruler vertical' style='z-index:100;'></div>");
                     var the_workspace_top = $(".workspace.formbuilder_ws,.workspace.workflow_ws,.workspace.orgchart_ws")
                     the_workspace_top.append(the_ruler_top);
                     var inside_ruler_top = the_ruler_top.clone();
                     $('.hRule').prepend(inside_ruler_top)
                     var the_padding_top = the_workspace_top.css('padding-top');
                     the_ruler_top = the_ruler_top.add(inside_ruler_top);
                     */


                    the_ruler_left = $("<div class='form-builder-ruler horizontal' style='z-index:100;'></div>");
                    var the_workspace_left = $(".workspace.formbuilder_ws,.workspace.workflow_ws,.workspace.orgchart_ws")
                    var the_padding_left = the_workspace_left.css('padding-left');
                    the_workspace_left.append(the_ruler_left);
                    var the_inside_ruler_left = the_ruler_left.clone().addClass('isDisplayNone');
                    $('.vRule').prepend(the_inside_ruler_left)
                    the_ruler_left = the_ruler_left.add(the_inside_ruler_left);

                    the_ruler_left.css({
                        "position": "absolute",
                        "background-color": "blue",
                        "width": "100%",
                        "height": (1) + "px",
                        "top": ($(".leftPointerRuler-tip").position().top) + "px"
                    });
                    the_ruler_left.eq(1).css({
                        "width": "calc( 100% - " + the_padding_left + " )"
                    });
                    $(".leftPointerRuler-tip").draggable("option", "formbuilder_ruler_horizontal", the_ruler_left);
                    the_ruler_left.draggable({
                        snap: ".setObject, .form-builder-ruler",
                        snapTolerance: 3,
                        disabled: true,
                        snapMode: "outer"
                    });
                }
            } else if (selected_val_show == "no") {
                // if($(".topPointerRuler-container").length >= 1){
                //  $(".topPointerRuler-container").hide();
                // }
                // if($(".leftPointerRuler-container").length >= 1){
                //  $(".leftPointerRuler-container").hide()
                // }
                if ($(".dragging-static-ruler").length >= 1) {
                    $(".dragging-static-ruler").remove();
                }
                if ($(".form-builder-ruler").length >= 1) {
                    $(".form-builder-ruler").remove();
                }
            }
        }
    });

});


function resizeForm(elements, container_height, type) {
    if ($(".formbuilder_ws").length >= 1) {
        setInterval(function () {
            var window_left = $(window).outerWidth() - 10;
            var workspace_left = $(".formbuilder_ws").offset().left + $(".formbuilder_ws").outerWidth();
            //console.log(window_left, workspace_left);

            if (workspace_left <= window_left) {
                $(".scroll-resize-padding").remove();
            } else {
                if ($(".formbuilder_ws").find(".scroll-resize-padding").length == 0) {
                    $(".formbuilder_ws").append("<div class='scroll-resize-padding' style='visibility:hidden;position:absolute;right: -310px;background-color:red;z-index: 10000;width: 300px;height: 10px;'></div>");
                } else {

                }
            }
        }, 500);
    }


    $(elements).resizable({
        handles: 's,e',
        resize: function (event, ui) {
            //$(".formHeight").val(ui.size.height);
            // $(this).css("min-height", container_height);
            $('.leftPointerRuler-container, .vRule').css({'height': ''});
            console.log("resiiiizer", ui['helper'])
            $('.btn_content').css('max-width', ui['helper'].width());
        },
        start: function () {
            
            //console.log($('.workspacev2').width());
            
            // $('.topPointerRuler-tip').css({
            //     'left': $('.workspacev2').width() - $('.topPointerRuler-tip').width()
            // });

            // var h_ruler_container = $('.topPointerRuler-container');
            // var h_ruler_container_o_width = h_ruler_container.outerWidth();
            // $('.topPointerRuler-container').append("<div data-caption-position='0'>0</div>");
            //alert(1);

            //$('#allow-portal').attr('checked', false);
            //enableFormSize();
            //$('.choose_app_icon').removeClass('isDisplayBlock').addClass('isDisplayNone');



            //$('.fl-opt_custom_size').prop('selected', true);
            //enableFormSetSizeField();

        },
        stop: function (event, ui) {
            // remove default height
             // console.log("Workspace width: ",$('.workspacev2').width(), "Workspace height: ", $('.workspacev2').height());
            $(".setObject.workspace_bckgrnd").find('.uiform-image-resizable').css({
                "height":$('.workspacev2').height(),
                "width":$('.workspacev2').width()
            })
            // $('.topPointerRuler-tip').css({
            //     'left': $('.workspacev2').width() - $('.topPointerRuler-tip').width()
            // });

            $(this).css("height", ui.size.height);
            if (type == "orgchart") {

                $(".form_size_width").val($(".orgchart_ws").width());
                $(".form_size_height").val($(".orgchart_ws").height());

                $(".node-drag").draggable("option", "containment", shrinkSize(".workspace", 20, 20));

                if ($(".orgchart_ws").is('[data-click-size="false"]')) {
                    $('.fl-opt_custom_size').prop('selected', true);
                    enableFormSetSizeField();
                }

                //alert("orgchart resize stop");

            } else if (type == "workflow") {

                $(".form_size_width").val($(".workflow_ws").width());
                $(".form_size_height").val($(".workflow_ws").height());

                $(".node-drag").each(function () {
                    if ($(this).hasClass("ui-draggable")) {
                        $(this).draggable("option", "containment", shrinkSize(".workspace", 20, 20));
                    }
                });

                //$('.leftPointerRuler-container, .vRule').css({'height': + $(this).height()});

                if ($(".workflow_ws").is('[data-click-size="false"]')) {
                    $('.fl-opt_custom_size').prop('selected', true);
                    enableFormSetSizeField();
                }

                //alert("workflow resize stop");

            } else {

                if ($(".formbuilder_ws").is('[data-click-size="false"]')) {
                    $('.fl-opt_custom_size').prop('selected', true);
                    enableFormSetSizeField();
                }

                $(".form_size_width").val($(".formbuilder_ws").width());
                $(".form_size_height").val($(".formbuilder_ws").height());

                
            }
            $(".formbuilder_ws, .orgchart_ws, .workflow_ws").attr('data-click-size', 'false');

            if ($('.form_size').children('.fl-opt_custom_size:selected').length >= 1) {
                $('.form_size').children('.fl-opt_custom_size:selected').data("custom_set_size", {
                    "width": ($(".form_size_width").val()) + "",
                    "height": ($(".form_size_height").val()) + ""
                });
            }
        },
        minWidth: 196.8,
        minHeight: 279.36,
        snapMode: "Outer",
        snap: true,
    });

    // $('.formbuilder_ws').resizable('resizeBy', {
    // 	"width": 0
    // }); // not applicable when you choose specific size it will trigger custom size as default.
}
function checkCollisionDroppableBoundaries(this_element) {
    if ($(this_element).closest(".workspace").length == 0) {
        alert("the element is not in the droppable containment");
        return false;
    }

    // var droppable_top = 0;
    // var droppable_left = 0;
    // var droppable_right = $("#ws-container #droppable").width();
    // var droppable_bottom = $("#ws-container #droppable").height();
    var droppable_ele = $(this_element).parent();
    var droppable_top = 0 + Number( (droppable_ele.css("padding-top")||"").replace("px","")||0 );
    var droppable_left = 0 + Number( (droppable_ele.css("padding-left")||"").replace("px","")||0 );
    var droppable_right = droppable_ele.width() + Number( (droppable_ele.css("padding-left")||"").replace("px","")||0 );
    var droppable_bottom = droppable_ele.height() + Number( (droppable_ele.css("padding-top")||"").replace("px","")||0 );

    var element_right = $(this_element).position().left + $(this_element).outerWidth();
    var element_bottom = $(this_element).position().top + $(this_element).outerHeight();

    var element_top = $(this_element).position().top;
    var element_left = $(this_element).position().left;
    // $(".ws-status-bar").html(
    //  element_right+" <= "+droppable_right+" && "+
    //  "<br/>"+element_bottom+" <= "+droppable_bottom+" && "+
    //  "<br/>"+element_top+" >= "+droppable_top+" && "+
    //  "<br/>"+element_left+" >= "+droppable_left
    // );

    //console.log(droppable_ele,"HALIMAW",this_element,element_right," >= ",droppable_right);
    if (element_right >= droppable_right) {
        $(this_element).css({
            left: droppable_right - $(this_element).outerWidth()
        });
    }
    if (element_bottom >= droppable_bottom) {
        $(this_element).css({
            top: droppable_bottom - $(this_element).outerHeight()
        });
    }
    if (element_top <= droppable_top) {
        $(this_element).css({
            top: droppable_top
        });
    }
    if (element_left <= droppable_left) {
        $(this_element).css({
            left: droppable_left
        });
    }
}

function checkFormMinWidth(type) {
    var add_padding = 0;
    var dragObjRightPosition = [];
    var classWorkspace = ".workspace.formbuilder_ws";
    if (type == "orgchart") {
        classWorkspace = ".workspace.orgchart_ws";
    } else if (type == "workflow") {
        classWorkspace = ".workspace.workflow_ws";
    }
    $(classWorkspace).children(".ui-draggable").each(function () {
        dragObjRightPosition.push(($(this).position().left) + ($(this).width()));
    });

    // if ($('.topPointerRuler-tip').exists()) {
    //     dragObjRightPosition.push($('.topPointerRuler-tip').position().left + $('.topPointerRuler-tip').outerWidth());
    // };




    if (dragObjRightPosition.length >= 1) {
        var rets = dragObjRightPosition.sort(function (a, b) {
            return b - a;
        });
        return rets[0] + add_padding;
    } else {
        return null;
    }
}
function checkFormMinHeight(type) {
    var add_padding = 0;
    var dragObjBottomPosition = [];
    var rets;
    var classWorkspace = ".workspace.formbuilder_ws";
    if (type == "orgchart") {
        classWorkspace = ".workspace.orgchart_ws";
    } else if (type == "workflow") {
        classWorkspace = ".workspace.workflow_ws";
    }
    $(classWorkspace).children(".ui-draggable").each(function () {
        dragObjBottomPosition.push(($(this).position().top) + ($(this).height()));
    });

    // if ($('.leftPointerRuler-tip').exists()) {
    //     dragObjBottomPosition.push($('.leftPointerRuler-tip').outerHeight() + $('.leftPointerRuler-tip').position().top);
    // };




    if (dragObjBottomPosition.length >= 1) {
        rets = dragObjBottomPosition.sort(function (a, b) {
            return b - a;
        });
        return rets[0] + add_padding;
    } else {
        return null;
    }
}
function startIde(element) {
    var ideID = element.attr('data-ide-properties-type');
    var ret = "";
    ret += '<div class="ideoverlay">';
    ret += '<div class="idemiddler">';
    ret += '<div id="' + ideID + '" class="idepos">';
    ret += '<div class="idehead">';//'+ideID+'
    ret += '<i class="idesave  fa fa-save"></i>';
    ret += '<i class="ideclose fa fa-times"></i>';
    ret += '</div>';
    ret += '<div class="clearfix"></div>';
    ret += '<div class="idecontent">';
    ret += '</div>';

    //ret += '<div class="idefoot">';
    //ret += '<button class="idesave">Save</button>';
    //ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

    /**
    added by:
    japhet morada
    05-25-2015
    **/
    //modified by michael dahil may error
    if((window.pathname||"").indexOf('workflow') > -1){
        if(element.attr('id') == "fm-message"){
            if($('[name="fm-type"]').length > 0){
                var get_fm_type = $('[name="fm-type"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "workflow-sms-contact"){
            if($('[name="workflow-sms-recipient-type"]').length > 0){
                var get_fm_type = $('[name="workflow-sms-recipient-type"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "workflow-sms-message"){
            if($('[name="workflow-sms-message-type"]').length > 0){
                var get_fm_type = $('[name="workflow-sms-message-type"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }   
        }
        else if(element.attr('id') == "workflow-mail-otherRecepient_to"){
            if($('[name="workflow-mail-otherRecepient-type_to"]').length > 0){
                var get_fm_type = $('[name="workflow-mail-otherRecepient-type_to"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "workflow-mail-otherRecepient_cc"){
            if($('[name="workflow-mail-otherRecepient-type_cc"]').length > 0){
                var get_fm_type = $('[name="workflow-mail-otherRecepient-type_cc"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "workflow-mail-otherRecepient_bcc"){
            if($('[name="workflow-mail-otherRecepient-type_bcc"]').length > 0){
                var get_fm_type = $('[name="workflow-mail-otherRecepient-type_bcc"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "workflow-email-title"){
            if($('[name="workflow-email-title-type"]').length > 0){
                var get_fm_type = $('[name="workflow-email-title-type"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "workflow-email-message"){
            if($('[name="workflow-email-message-type"]').length > 0){
                var get_fm_type = $('[name="workflow-email-message-type"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "cl-workflow-mail-otherRecepient_to"){
            if($('[name="cl-workflow-mail-otherRecepient-type_to"]').length > 0){
                var get_fm_type = $('[name="cl-workflow-mail-otherRecepient-type_to"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "cl-workflow-mail-otherRecepient_cc"){
            if($('[name="cl-workflow-mail-otherRecepient-type_cc"]').length > 0){
                var get_fm_type = $('[name="cl-workflow-mail-otherRecepient-type_cc"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "cl-workflow-mail-otherRecepient_bcc"){
            if($('[name="cl-workflow-mail-otherRecepient-type_bcc"]').length > 0){
                var get_fm_type = $('[name="cl-workflow-mail-otherRecepient-type_bcc"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "cl-workflow-email-title"){
            if($('[name="cl-workflow-email-title-type"]').length > 0){
                var get_fm_type = $('[name="cl-workflow-email-title-type"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        else if(element.attr('id') == "cl-workflow-email-message"){
            if($('[name="cl-workflow-email-message-type"]').length > 0){
                var get_fm_type = $('[name="cl-workflow-email-message-type"]:checked').val();
                if(get_fm_type == "0"){
                    current_formula_type = "static";
                }
                else{
                    current_formula_type = "computed";
                }
            }
        }
        //workflow-email-title
        else{
            current_formula_type = "computed";
        }
    }
    else if(window.location.pathname.indexOf('formbuilder') > -1){
        if(element.attr('data-properties-type') == "obj_objectCss") {
            current_formula_type = "object_css";
        }
        else if($('.highlight-row-prop').length > 0){
            current_formula_type = element.parent().find('.value-type-HL:checked').val();
        }
        else{
            if($('[name="default_val"]').length > 0){
                current_formula_type = $('[name="default_val"]:checked').val();
            }
        }
    }
    else if(window.location.pathname.indexOf('rule_settings') > -1){
        if(element.parents('.fields_below').find('input[type="radio"]').length > 0){
            var formula_type = element.parents('.fields_below').find('input[type="radio"]:checked').val();
            if(formula_type  == "0"){
                current_formula_type = "static";
            }
            else{
                current_formula_type = "computed";
            }
        }
    }
    if(element.attr('data-ide-properties-type') == 'embed-additional-filter-formula'){
        current_formula_type = element.attr('data-ide-properties-type');
    }
    //====================================//
    var ret_elem = $(ret);
    $('body').prepend(ret_elem);
    ide_selected_title_code = element.attr('data-ide-properties-type');
    if ($(element).attr('data-ide-properties-type') == 'ide-obj_objectCss') {
        $(ret_elem).find('.idecontent').initialize({
            'width': '950px',
            'height': '500px',
            'numbering': true,
            'autocomplete': false,
            'menubar': true,
            'overlay': true,
            'console':false,
            'backgroundcolor': 'rgb(45, 45, 45)',
            'keywordcolor': '#5588BE',
            'font-color': 'white'
        });
    }else {
        $(ret_elem).find('.idecontent').initialize({
            'width': '950px',
            'height': '500px',
            'numbering': true,
            'autocomplete': true,
            'menubar': true,
            'overlay': true,
            'backgroundcolor': 'rgb(45, 45, 45)',
            'keywordcolor': '#5588BE',
            'font-color': 'white'
        });
    }


    var htmlHeight = $(window).height();
    var wrapperDiv = $(ret_elem).find('.idepos');
    var wrapperHeight = $(wrapperDiv).height() / 2;
   /* console.log(wrapperHeight);*/

    $(wrapperDiv).css({
        //'padding-top': htmlHeight / 2 - wrapperHeight
    });
    var idepos_ele = $(ret_elem).find('.idepos');
    var top_pos = idepos_ele.offset().top;
    var left_pos = idepos_ele.offset().left;

    /*idepos_ele.css({
     "top":(top_pos)+"px",
     "left":(left_pos)+"px",
     "position":"absolute"
     })*//*.unwrap().unwrap().draggable({
      handle:'.idehead:eq(0)'
      });*/
    /*console.log("textareaval",$("[data-ide-properties-type='"+ideID+"']").val());
     idepos_ele.find('[contenteditable="true"]').text($("[data-ide-properties-type='"+ideID+"']").val());
     idepos_ele.find('[contenteditable="true"]').recompute();
     */
    $('.codeField').trigger('focus');
    idepos_ele.find('.ideclose').on( {
        "click": function () {
            if ($(this).closest('.ideoverlay').length >= 1) {
                $(this).closest('.ideoverlay').remove();
            } else {
                $(this).closest('.idepos').remove();
            }
            ;
            $(document).off('.classIDE');
        }
    });
    idepos_ele.find('.idesave').on({
        "click": function () {
           
            if ($(this).closest('.idepos').find('[contenteditable="true"]').attr('default-value') != "static") {
                var type = $(this).closest('.idepos').find('[contenteditable="true"]').attr('default-value');
                // console.log($(this).closest('.idepos').find('[contenteditable="true"]').attr('default-value'));
                var formula_test = formulaChecker.checkFormula($(ret_elem).find('.idecontent'));
                $(ret_elem).find('.compile').trigger('click');
                if(type != 'object_css'){
                    if (formula_test.status == true) {
                        var totextarea = $(this).closest('.idepos').find('[contenteditable="true"]').children().map(function (a, b) {
                            return $(b).text()
                        }).get().join("\n");
                        element.val($.trim(getText(idepos_ele)));
                        element.trigger("change");
                        element.trigger("keyup");
                        element.trigger("keydown");
                        var elem = $(this);
                        setTimeout(function () {
                            if ($(elem).closest('.ideoverlay').length >= 1) {
                                $(elem).closest('.ideoverlay').remove();
                            } else {
                                $(elem).closest('.idepos').remove();
                            }
                            $(document).off('.classIDE');
                        }, 1000);
                    }
                }
                else{
                    if($(this).closest('.idepos').find('[contenteditable="true"]').attr('css-error') == "0"){
                        $(this).closest('.idepos').find('[contenteditable="true"]').removeAttr('css-error');
                        var totextarea = $(this).closest('.idepos').find('[contenteditable="true"]').children().map(function (a, b) {
                            return $(b).text()
                        }).get().join("\n");
                        element.val($.trim(getText(idepos_ele)));
                        element.trigger("change");
                        element.trigger("keyup");
                        element.trigger("keydown");
                        var elem = $(this);
                        setTimeout(function () {
                            if ($(elem).closest('.ideoverlay').length >= 1) {
                                $(elem).closest('.ideoverlay').remove();
                            } else {
                                $(elem).closest('.idepos').remove();
                            }
                            $(document).off('.classIDE');
                        }, 1000);
                    }
                }
            }
            else {
                console.log("static!");
                var totextarea = $(this).closest('.idepos').find('[contenteditable="true"]').children().map(function (a, b) {
                    return $(b).text()
                }).get().join("\n");
                element.val($.trim(getText(idepos_ele)));
                element.trigger("change");
                element.trigger("keyup");
                element.trigger("keydown");
                var elem = $(this);
                if ($(elem).closest('.ideoverlay').length >= 1) {
                    $(elem).closest('.ideoverlay').remove();
                } else {
                    $(elem).closest('.idepos').remove();
                }
                $(document).off('.classIDE');
            }
        }
    });
    idepos_ele.find('[contenteditable="true"]').text(element.val());
    idepos_ele.find('[contenteditable="true"]').recompute();
}
var client_formula = {
    "init":function(){
        //
    },
    "checkFormulaErrors":function(formula_field_ele){
        var dis_self = this;
        // var f_e = $(formula_field_ele);
        var collect_message_error = "";
        var default_val_formula = formula_field_ele;//f_e.attr(attribute);
        if(default_val_formula){
            if(default_val_formula.trim() == ""){
                return {
                    "status":true,
                    "message":"Valid"
                };
            }   
        }else{
            return {
                "status":true,
                "message":"Valid"
            };
        }
        var atfnmatches = default_val_formula.match(/@[A-Za-z][0-9a-zA-Z_]*/g);
        var temporary_regex = null;
        var evaluation_status_error = null;
        var evaluation_results = null;
        var evaluation_results2 = null;
        var collect_error_messages = [];

        if(atfnmatches != null){
            atfnmatches = atfnmatches.filter(Boolean);//clean selected array with values
            atfnmatches = atfnmatches.filter(function(value, index){
                if(dis_self["fn_keywords"].indexOf(value) >= 0){
                    return false;
                }else{
                    return true;
                }
            });
            atfnmatches = $.unique(atfnmatches);
            for(var m_index in atfnmatches){
                temporary_regex = new RegExp(atfnmatches[m_index]+"(?![0-9a-zA-Z_])","g");
                default_val_formula = default_val_formula.replace(temporary_regex,"\""+atfnmatches[m_index]+"\"");
            }
            for(var fn_index in dis_self["fn_keywords"]){
                temporary_regex = new RegExp(dis_self["fn_keywords"][fn_index]+"(?![0-9a-zA-Z_])","g");
                if(eval("typeof "+dis_self["fn_keywords"][fn_index].replace("@", "")) === "function"){
                    default_val_formula = default_val_formula.replace(temporary_regex, dis_self["fn_keywords"][fn_index].replace("@", ""));
                }else{
                    default_val_formula = default_val_formula.replace(temporary_regex, "\""+dis_self["fn_keywords"][fn_index].replace("@", "")+"\"");
                }
            }

            var error_definition = null;
            try{//evaluation_status_error
                console.log("property default_val_formula",default_val_formula)
                evaluation_results = eval(default_val_formula);
                console.log("bakit may error","[\n"+default_val_formula+"\n]");
                evaluation_results2 = eval("[\n"+default_val_formula+"\n]");


                if(evaluation_results2.length == 1){
                    evaluation_status_error = true;
                }else if(evaluation_results2.length == 0){
                    evaluation_status_error = true;
                
                }else{
                    evaluation_status_error = false;
                    collect_error_messages.push("Syntax Error! ...<br/>at ,");

                }
                // if(isNaN(evaluation_results)){   
                //  evaluation_status_error = false;
                //  collect_error_messages.push("Is not a number!!");
                // }else{
                // }
            }catch(exce){
                error_definition = exce;
                // console.log(exce)
                var funcNameRegex = /function (.{1,})\(/;
                var fn_name = null;
                fn_name_results = (funcNameRegex).exec(exce.constructor.toString());
                fn_name = (fn_name_results && fn_name_results.length > 1) ? fn_name_results[1] : "";
                
                if(fn_name == "SyntaxError"){
                    collect_error_messages.push("Syntax Error!!");
                }
                collect_error_messages.push(exce.message);
                evaluation_status_error = false;
            }
        }else{
            //walang may at sign
            try{//evaluation_status_error
                console.log("property default_val_formula",default_val_formula)
                evaluation_results = eval(default_val_formula);
                evaluation_results2 = eval("[\n"+default_val_formula+"\n]");

                if(evaluation_results2.length == 1){
                    evaluation_status_error = true;
                }else{
                    evaluation_status_error = true;
                    collect_error_messages.push("Syntax Error! ...<br/>at ,");
                }
            }catch(exce){
                console.log(exce)
                var funcNameRegex = /function (.{1,})\(/;
                var fn_name = null;
                fn_name_results = (funcNameRegex).exec(exce.constructor.toString());
                fn_name = (fn_name_results && fn_name_results.length > 1) ? fn_name_results[1] : "";
                
                if(fn_name == "SyntaxError"){
                    collect_error_messages.push("Syntax Error!!");
                }
                collect_error_messages.push(exce.message);
                evaluation_status_error = false;
            }
        }
        console.log("default_val_formula",default_val_formula)
        return {
            status:evaluation_status_error,
            message:collect_error_messages.join(" ... "),
            error_definition:error_definition,
            formula:default_val_formula
        }
    },
    "fn_keywords": [
        '@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
        '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
        '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
        '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
        '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
        '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now',
        '@GetAuth', "@FormatDate","@NumToWord",
        "@Head","@AssistantHead","@UserInfo","@GetData",
        "@LookupAuthInfo",
        "@GetAVG","@GetMin","@SDev","@GetMax",
        "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
        "@StrFind","@StrReplace","@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
        "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource", "@GetLocation",
        "@GetMonth", "@GetFileName", "@CustomScript","@ArraySearchBy","@FormulaList","@LookupExternalWhere",
        "@LookupGeoLocation", "@IsEmailExist", "@GetParam"
    ]
}

function enableFormSetSizeField() {


    if ($('.form_size option:selected').is('.fl-opt_custom_size')) {

            $('.form_size_width').css('opacity', '');
            $('.form_size_width').removeAttr('readonly');
            $('.form_size_height').css('opacity', '');
            $('.form_size_height').removeAttr('readonly');
            $('.fl-custom-size').add('.form_size').css('opacity', '');

            $('.formbuilder_ws').children('.ui-resizable-e').show();
            $('.formbuilder_ws').children('.ui-resizable-s').show();
            $('.form_size').removeAttr('disabled');
    
    }

    if ($('.form_size option:selected').is('.allow_portal_size')) {

            // $('.formbuilder_ws').children('.ui-resizable-e').hide(); // FS#7484
            // $('.formbuilder_ws').children('.ui-resizable-s').hide(); // FS#7484
            // $('.form_size_width').attr('readonly', 'readonly'); // FS#7484
            // $('.form_size_width').add('.form_size').css({'opacity': '0.5'}); // FS#7484
            $('.form_size_height').removeAttr('readonly');
            $('.form_size_height').css({'opacity': ''});
            // $('.form_size').attr('disabled', 'disabled'); // FS#7484

    };

}

//Ruler drag and click from formbuidler - Roni Pinili 2:27 PM 10/12/2015

$(".topPointerRuler-tip").draggable({
    containment: ".topPointerRuler-container",
    zIndex: 100,
    start: function (event, ui) {
        if ($(".dragging-static-ruler.top").length == 0) {
            tooltip_ele_top = $("<span class='dragging-static-ruler top'></span>")
            $("body").append(tooltip_ele_top);
            tooltip_ele_top.css({
                "position": "absolute",
                //"font-weight":"bold",
                "font-size": "15px",
                "color": "white",
                "background-color": "rgba(0,0,0,0.25)",
                "border": "1px solid rgba(0,0,0,0.5)",
                "padding": (1) + "px",
                "display": "none"
            });
            $(ui.helper).draggable("option", "tooltip_ele_top", tooltip_ele_top);
        }
        if ($(".form-builder-ruler.vertical").length == 0) {
            if ($(".f-show-ruler").val() == "yes") {
                the_ruler = $("<div class='form-builder-ruler vertical'></div>");
                var inside_ruler = the_ruler.clone().addClass('display');
                var the_workspace = $(".workspace.formbuilder_ws,.workspace.workflow_ws,.workspace.orgchart_ws");
                var the_padding_top = the_workspace.css("padding-top");
                the_workspace.append(the_ruler);
                $('.hRule').prepend(inside_ruler);
                the_ruler = the_ruler.add(inside_ruler);
                the_ruler.css({
                    "position": "absolute",
                    "background-color": "blue",
                    "width": (1) + "px",
                    "height": "100%"
                });
                the_ruler.eq(1).css({
                    "height": "calc(100% - " + the_padding_top + ")"
                });
                $(ui.helper).draggable("option", "formbuilder_ruler_vertical", the_ruler);
                the_ruler.draggable({
                    snap: ".setObject, .form-builder-ruler",
                    snapTolerance: 3,
                    disabled: true,
                    snapMode: "outer"
                });
            }
        }
    },
    drag: function (event, ui) {
        if ($(ui.helper).draggable("option", "tooltip_ele_top").length >= 1) {
            tooltip_ele_top = $(ui.helper).draggable("option", "tooltip_ele_top");
            $(tooltip_ele_top).html(ui.position.left + 5);
            $(tooltip_ele_top).css({
                "left": (ui.offset.left) + "px",
                "top": (ui.offset.top + 12) /*- $(tooltip_ele_top).outerHeight()) */ + "px"
            });
        }
        if ($(ui.helper).draggable("option", "formbuilder_ruler_vertical").length >= 1) {
            formbuilder_ruler_vertical = $(ui.helper).draggable("option", "formbuilder_ruler_vertical");
            formbuilder_ruler_vertical.css({
                "left": (ui.position.left + 5) + "px"
            })
        }
    },
    stop: function (event, ui) {
        try {
            if ($(".f-show-ruler:checked[name='show-form-ruler-select']").val() == "yes") {
            } else {
                if ($(ui.helper).draggable("option", "tooltip_ele_top").length >= 1) {
                    $(ui.helper).draggable("option", "tooltip_ele_top").remove();
                }
                if ($(ui.helper).draggable("option", "formbuilder_ruler_vertical").length >= 1) {
                    $(ui.helper).draggable("option", "formbuilder_ruler_vertical").remove();
                }
            }
            $(".workspace.formbuilder_ws.ui-resizable,.workspace.workflow_ws.ui-resizable,.workspace.orgchart_ws.ui-resizable").resizable({
                "minWidth": (ui.position.left + $(ui.helper).width())
            });
        } catch (error) {
            console.log("error on drag of .topPointerRuler-tip")
        }

        var dragFromRulerLeft = (ui.position.left + $(ui.helper).width());

        if ($('.orgchart_ws').length >= 1) {
            var dragFromObject = checkFormMinWidth("orgchart");

            if (dragFromRulerLeft > dragFromObject) {
                $('.workspacev2').resizable("option", "minWidth", dragFromRulerLeft);

            } else {
                $('.workspacev2').resizable("option", "minWidth", dragFromObject);

            }



        } else if ($('.workflow_ws').length >= 1) {

            var dragFromObject = checkFormMinWidth("workflow");

            if (dragFromRulerLeft > dragFromObject) {
                $('.workspacev2').resizable("option", "minWidth", dragFromRulerLeft);
            } else {
                $('.workspacev2').resizable("option", "minWidth", dragFromObject);
            }



        }else {

            var dragFromObject = checkFormMinWidth("formbuilder");

            if (dragFromRulerLeft > dragFromObject) {
                $('.workspacev2').resizable("option", "minWidth", dragFromRulerLeft);

            } else {
                $('.workspacev2').resizable("option", "minWidth", dragFromObject);

            }


        }

        

        //checkStaticRulerTop($(ui.helper));
    }
})
$(".topPointerRuler-container").on({
    "click": function (e) {
        var target = (window.event) ? window.event.srcElement /* for IE */ : e.target;
        if ($(target).hasClass("topPointerRuler-tip")) {

        } else {
            var positionX = e.offsetX;
            $(this).children(".topPointerRuler-tip").css({
                "left": (positionX - 5) + "px"
            });
            if ($(".form-builder-ruler.vertical").length >= 1) {
                $(".form-builder-ruler.vertical").css({
                    "left": (positionX) + "px"
                });
            }
            if ($(".dragging-static-ruler.top").length >= 1) {
                $(".dragging-static-ruler.top").css({
                    "left": ($(this).children(".topPointerRuler-tip").offset().left) + "px"
                });
                $(".dragging-static-ruler.top").html($(this).children(".topPointerRuler-tip").position().left)
            }
        }
    }
})

// Left (target top)
$(".leftPointerRuler-tip").draggable({
    containment: ".leftPointerRuler-container",
    zIndex: 100,
    start: function (event, ui) {

        if ($(".dragging-static-ruler.left").length == 0) {
            tooltip_ele_left = $("<span class='dragging-static-ruler left'></span>")
            $("body").append(tooltip_ele_left);
            tooltip_ele_left.css({
                "position": "absolute",
                //"font-weight":"bold",
                "font-size": "15px",
                "color": "white",
                "background-color": "rgba(0,0,0,0.25)",
                "border": "1px solid rgba(0,0,0,0.5)",
                "padding": (1) + "px",
                "display": "none"
            });
            $(ui.helper).draggable("option", "tooltip_ele_left", tooltip_ele_left);
        }
        if ($(".form-builder-ruler.horizontal").length == 0) {
            if ($(".f-show-ruler").val() == "yes") {
                the_ruler = $("<div class='form-builder-ruler horizontal'></div>");
                var the_workspace = $(".workspace.formbuilder_ws,.workspace.workflow_ws,.workspace.orgchart_ws");
                the_workspace.append(the_ruler);
                var the_padding_left = $(the_workspace).css("padding-left");
                var inside_ruler = the_ruler.clone().addClass('display');
                $('.vRule').prepend(inside_ruler);
                the_ruler = the_ruler.add(inside_ruler);
                the_ruler.css({
                    "position": "absolute",
                    "background-color": "blue",
                    "width": "100%",
                    "height": (1) + "px"
                });
                the_ruler.eq(1).css({
                    "width": "calc(100% - " + the_padding_left + ")"
                });
                $(ui.helper).draggable("option", "formbuilder_ruler_horizontal", the_ruler);
                the_ruler.draggable({
                    snap: ".setObject, .form-builder-ruler",
                    snapTolerance: 3,
                    disabled: true,
                    snapMode: "outer"
                });
            }
        }
    },
    drag: function (event, ui) {
        if ($(ui.helper).draggable("option", "tooltip_ele_left").length >= 1) {
            tooltip_ele_left = $(ui.helper).draggable("option", "tooltip_ele_left");
            $(tooltip_ele_left).html(ui.position.top + 5);
            $(tooltip_ele_left).css({
                "left": (ui.offset.left) + 13 /*$(tooltip_ele_left).outerWidth())*/ + "px",
                "top": (ui.offset.top) + "px"
            });
        }
        if ($(ui.helper).draggable("option", "formbuilder_ruler_horizontal").length >= 1) {
            formbuilder_ruler_horizontal = $(ui.helper).draggable("option", "formbuilder_ruler_horizontal");
            formbuilder_ruler_horizontal.css({
                "top": (ui.position.top + 5) + "px"
            });
        }
    },
    stop: function (event, ui) {
        try {
            if ($(".f-show-ruler:checked[name='show-form-ruler-select']").val() == "yes") {
            } else {
                if ($(ui.helper).draggable("option", "tooltip_ele_left").length >= 1) {
                    $(ui.helper).draggable("option", "tooltip_ele_left").remove();
                }
                if ($(ui.helper).draggable("option", "formbuilder_ruler_horizontal").length >= 1) {
                    $(ui.helper).draggable("option", "formbuilder_ruler_horizontal").remove();
                }
            }


            $(".workspace.formbuilder_ws.ui-resizable,.workspace.workflow_ws.ui-resizable,.workspace.orgchart_ws.ui-resizable").resizable({
                "minHeight": (ui.position.top + $(ui.helper).height())
            });
        } catch (error) {
            console.log("error on drag of .leftPointerRuler-tip")
        }

        var dragFromRuler = (ui.position.top + $(ui.helper).height());
        var dragFromObject = checkFormMinHeight();

        if (dragFromRuler > dragFromObject) {
            $('.workspacev2').resizable("option", "minHeight", dragFromRuler);
        } else {
            $('.workspacev2').resizable("option", "minHeight", dragFromObject);
        }

        if ($('.orgchart_ws').length >= 1) {
            var dragFromObject = checkFormMinHeight("orgchart");

            if (dragFromRuler > dragFromObject) {
                $('.workspacev2').resizable("option", "minHeight", dragFromRuler);

            } else {
                $('.workspacev2').resizable("option", "minHeight", dragFromObject);

            }



        } else if ($('.workflow_ws').length >= 1) {

            var dragFromObject = checkFormMinHeight("workflow");

            if (dragFromRuler > dragFromObject) {
                $('.workspacev2').resizable("option", "minHeight", dragFromRuler);
            } else {
                $('.workspacev2').resizable("option", "minHeight", dragFromObject);
            }

        }

        if (dragFromRuler > dragFromObject) {
            $('.workspacev2').resizable("option", "minHeight", dragFromRuler);

        } else {
            $('.workspacev2').resizable("option", "minHeight", dragFromObject);

        }

        //checkStaticRulerLeft($(ui.helper))
    }
})
$(".leftPointerRuler-container").on({
    "click": function (e) {
        var target = (window.event) ? window.event.srcElement /* for IE */ : e.target;
        if ($(target).hasClass("leftPointerRuler-tip")) {

        } else {
            var positionY = e.offsetY;
            $(this).children(".leftPointerRuler-tip").css({
                "top": (positionY - 5) + "px"
            });
            if ($(".form-builder-ruler.horizontal").length >= 1) {
                $(".form-builder-ruler.horizontal").css({
                    "top": (positionY) + "px"
                });
            }
            if ($(".dragging-static-ruler.left").length >= 1) {
                $(".dragging-static-ruler.left").css({
                    "top": ($(this).children(".leftPointerRuler-tip").offset().top) + "px"
                });
                $(".dragging-static-ruler.left").html($(this).children(".leftPointerRuler-tip").position().top)
            }
        }
    }
});
//from custom.js 10192015 2:04PM
function getprocessor(value,selected) {
   var form_id = $("#form_id").val();
   $.post("/ajax/workflow",{action:"getProcessor",value:value,selected:selected,form_id:form_id},function(data){
        if(data=="0"){
            showNotification({
                 message: "No Orgchart activated",
                 type: "error",
                 autoClose: true,
                 duration: 3
            });
        }else{
            $("#workflow-processor, .workflow-processor").html(data);
        }
        $("#workflow-processor").next().hide();
        $("#workflow-processor, .workflow-processor").show();
   })
}