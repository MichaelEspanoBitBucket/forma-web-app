	
(function($) {

    $.fn.rebindDragObects = function() {

    	if(this.is('[data-type="tab-panel"]')){
    		this.find(".ui-resizable").removeClass("ui-resizable");			
			this.find(".ui-resizable-handle").remove();
			this.find(".setObject-drag-handle").remove(); // ADDED 12 2 2013
			this.find(".component-ancillary-focus").removeClass("component-ancillary-focus");
			this.find(".component-primary-focus").removeClass("component-primary-focus");
			this.find(".icon-remove").addClass("fa fa-times");
			this.find(".icon-cogs").addClass("fa fa-cogs");
			this.find(".icon-list-alt").addClass("fa fa-list-alt");
			this.find(".NM-redborder").removeClass("NM-redborder");
			this.find(".NM-notification").remove();
			this.find('.ul-scroll-anchor').remove();
			this.find(".li-add-new-tab-panel").remove();
			var tap_p = this.children('.fields_below').find('.form-tabbable-pane').eq(0);
			tap_p.each(function(){
				$(this).tabPanel({
					resizableContent: true,
					resizableContentContainment: ".workspace.formbuilder_ws"
				});
			});
				
    	}
    	dragObects(this,'.workspace','',this.attr('data-object-id'));
    	if(this.is('[data-type="table"]')){
    		var table_clone = this.find(".form-table"); 
			table_clone.each(function(){
				dragObects($(this).closest(".setObject"), ".workspace", $(".form_set_size.form_size_height").val(), $(this).closest(".setObject").attr("data-object-id"));
				if ($(this).closest(".table-wrapper").length >= 1) {
					$(this).closest(".table-wrapper").find(".table-actions-ra").remove();
					$(this).find(".dynamic-td-droppable-selected").removeClass("dynamic-td-droppable-selected");
					$(this).removeClass("thisDynamicTable");
					$(this).unwrap();
					$(this).dynamicTable({
						"tableBorder": "1",
						"tHeadTag": false,
						droppableCell: true,
						cellDroppable: {
							accept: ".setObject",
							revert: true,
							greedy: true,
							drop: function(e, ui) {
											//comment for reference use
											//evtarget = (window.event) ? window.event.srcElement /* for IE */ : e.target;
											//console.log(e.target);
											//console.log(ui.draggable[0].parentElement);
								var target = e.target;
								var source = ui.draggable[0].parentElement;
								if (
									$(ui.helper).find(".form-table").length == 0 && $(ui.helper).find(".form-tabbable-pane").length == 0 ||
									($(".table-form-design-relative").length >= 1 && $(target).hasClass("fcp-td-relative"))
									) {
												if (target === source) { //conditional purpose to prevent dropping off the same place
													//alert('Same droppable container');
												} else {
													// console.log("TRALALA",$(target), $(source), $(this), $(ui.helper));
													// console.log("SABOG",$(ui.helper).is('[data-type="noteStyleTextarea"]') , $(target).hasClass('td-relative') , $(target).children('.setObject').length >= 1);
													if ($(ui.helper).is('[data-type="noteStyleTextarea"]') && $(target).hasClass('td-relative') && $(target).children('.setObject').length >= 1) {
														// console.log("APPENDTO", source)
														jAlert("Notes Style requires empty cell pane to drop.", "", "", "", "", function() {
														});
														$(ui.helper).appendTo(source);
													} else if ($(target).hasClass('td-relative') && $(target).children('.setObject[data-type="noteStyleTextarea"]').length >= 1) {
														jAlert("You are not allowed to drop other fields in a cell pane which a Notes Style is present.", "", "", "", "", function() {
														});
														$(ui.helper).appendTo($(source));
													} else if ($(target).hasClass('td-relative') && $(target).children('.setObject[data-type="table"]').length >= 1) {
														jAlert("You are not allow to drop any other fields in that cell pane if a table is present.", "", "", "", "", function() {
														});
														$(ui.helper).appendTo($(source));
													} else if ($(target).hasClass('td-relative') && $(target).children('.setObject[data-type="tab-panel"]').length >= 1) {
														jAlert("You are not allow to drop any other fields in that cell pane if a tab panel is present.", "", "", "", "", function() {
														});
														$(ui.helper).appendTo($(source));
													} else {
														if ($(this).children(".td-relative").eq(0).length == 0) {
															$(ui.helper).appendTo(this);
															$(ui.helper).css({
																"top": "0px",
																"left": "0px"
															});
														} else {
															$(ui.helper).appendTo($(this).children(".td-relative").eq(0));
															$(ui.helper).css({
																"top": "0px",
																"left": "0px"
															});
														}
														console.log("IMPOSSIBLE", $(ui.helper).outerWidth(), $(this).outerWidth());
														console.log("GRAAA", $(ui.helper), $(this));
														if ($(ui.helper).outerWidth() > $(this).outerWidth()) {
															ung_padding_left = $(ui.helper).css("padding-left").split("px").join("");
															ung_padding_right = $(ui.helper).css("padding-right").split("px").join("");
															draggable_ele_width = $(ui.helper).outerWidth();
															var collect_most_right = [draggable_ele_width];

															if ($(this).hasClass("ui-droppable")) {
																dis_tr_index = $(this).closest("tr").index();
																if ($(this).prop("tagName") == "TD") {
																	dis_td_index = $(this).index();
																} else {
																	dis_td_index = $(this).closest("td.ui-resizable").index();
																}

																var dropped_column = $(this).closest("tbody").children("tr").children("td.ui-resizable").filter(function(eqi, ele) {
																	if ($(ele).index() == dis_td_index) {
																		return true;
																	} else {
																		return false;
																	}
																});
																dropped_column.children(".td-relative").children(".setObject").each(function(eqi, elem) {
																	collect_most_right.push($(this).outerWidth() + $(this).position().left);
																});
																collect_most_right.sort(function(a, b) {
																	return b - a;
																});
																dropped_column.css({
																	"width": (collect_most_right[0]) + "px"
																});
																dropped_column.children(".td-relative").css({
																	"min-width": (collect_most_right[0]) + "px"
																});
																console.log("AMP", dropped_column.children(".td-relative").children(".setObject"))

																// $(this).closest("tbody").children("tr").each(function() {
																//     $(this).children("td.ui-resizable").eq(dis_td_index).css({
																//         "width": draggable_ele_width
																//     });
																//     console.log("TEKA HERE DROPPABLE",$(this).children("td.ui-resizable").eq(dis_td_index))
																//     // $(this).children("td.ui-resizable").eq(dis_td_index).resizable({
																//     //     "minWidth": draggable_ele_width
																//     // });
																// });
															}
														}
														if ($(ui.helper).outerHeight() > $(this).outerHeight()) {
															ung_padding_top = $(ui.helper).css("padding-top").split("px").join("");
															ung_padding_bottom = $(ui.helper).css("padding-bottom").split("px").join("");
															draggable_ele_height = $(ui.helper).outerHeight();
															if ($(this).hasClass("ui-droppable")) {
																$(this).closest("tr").children("td.ui-resizable").css({
																	"height": draggable_ele_height
																});
																// $(this).closest("tr").children("td.ui-resizable").resizable({
																//     "minHeight": draggable_ele_height
																// });
															}
														}
														if ($(source).hasClass("td-relative")) {
															var collect_objs_bottom = [];
															var collect_objs_right = [];
															$(source).children(".setObject:visible").each(function() {
																collect_objs_bottom.push($(this).position().top + $(this).outerHeight());
																collect_objs_right.push($(this).position().left + $(this).outerWidth());
															});
															var dis_tr_index = $(source).closest("tr").index();
															if ($(source).prop("tagName") == "TD") {
																var dis_td_index = $(source).index();
															} else {
																var dis_td_index = $(source).closest("td.ui-resizable").index();
															}
															collect_objs_bottom.concat(
																	$(source).closest("tr").eq(dis_tr_index).children("td").children(".td-relative").children(".setObject:visible").get().map(function(ele, eqi) {
																return ($(ele).position().top + $(ele).outerHeight());
															})
																	);

															collect_objs_right.concat(
																	$(source).closest("tbody").children("tr").children("td").filter(function(eqi, ele) {
																if ($(ele).index() == dis_td_index) {
																	return true;
																} else {
																	return false;
																}
															}).children(".td-relative").children(".setObject:visible").get().map(function(ele, eqi) {
																return ($(ele).position().left + $(ele).outerWidth());
															})
																	);

															collect_objs_bottom.sort(function(a, b) {
																return b - a;
															})
															collect_objs_right.sort(function(a, b) {
																return b - a;
															});

															if (collect_objs_bottom.length >= 1 && collect_objs_right.length >= 1) {










																$(source).closest("tbody").children("tr").each(function() {
																	$(this).children("td.ui-resizable").eq(dis_td_index).css({
																		"width": collect_objs_right[0]
																	});
																	$(this).children("td.ui-resizable").eq(dis_td_index).resizable({
																		"minWidth": collect_objs_right[0]
																	});
																	if ($(this).index() == dis_tr_index) { // buong row dapat magkakapareha ng height
																		$(this).children("td.ui-resizable").each(function() {
																			$(this).css({
																				"height": collect_objs_bottom[0]
																			});
																			$(this).children("td.ui-resizable").resizable({
																				"minHeight": collect_objs_bottom[0]
																			});
																		})
																	}
																})
															} else {
																console.log("HEYO! DELO", $(source).closest("td.ui-resizable"))
																$(source).closest("td.ui-resizable").resizable({
																	"minHeight": 0,
																	"minWidth": 0
																});
															}
														}
													}

												}


												$(ui.helper).draggable("option", "containment", "parent");
												if ($(ui.helper).data()) {
													if ($(ui.helper).data().uiResizable) {
														$(ui.helper).resizable("option", "containment", "parent");
													}
												}

												// alert("test");
												// console.log($(source));
												//set a min height and min width of the cell when object leaves a cell

											}

										}
									}
								}).css({
									"border": "none"
								});
								if ($(this).attr("repeater-table") == "true") {
									$(this).closest(".table-wrapper").find(".table-actions-ra.action-row-add").hide();
								}
							}
						
			});
			if($(".table-form-design-relative").length >= 1 && $(".table-form-design-relative").is(':data("uiDraggable")') ){
				$(".table-form-design-relative").draggable('destroy');		
			}
			
    	}
    	if(this.is('[data-type="accordion"]')){
    		var accordions = this.find('.form-accordion:eq(0)');

    		
    		accordions.accordion(default_accordion_settings);
    		$(accordions).find('.form-accordion-section-group').each(function(){
    			 $(this).find('.form-accordion-section-header:eq(0)').removeClass('ui-state-active').removeClass('ui-state-hover');
    			 accordionAction($(this));

    		});
    		accordions.sortable({
		        axis: "y",
		        handle: "h3",
		        stop: function( event, ui ) {
		          // IE doesn't register the blur when sorting
		          // so trigger focusout handlers to remove .ui-state-focus
		          ui.item.children( "h3" ).triggerHandler( "focusout" );
		 		 
		          // Refresh accordion to handle new order
		          $(this).accordion("refresh");
		        }
		    });
		    accordions.accordion("refresh");
    	}
		this.find('[name],[embed-name]').each(function() {

			if ($(this).is('[name]')) {
				var ako_toh = $(this).attr('name');
				ako_toh = ako_toh.replace("[]", "");

			}
			else {
				var ako_toh = $(this).attr('embed-name');
			}
			var parent_nya = $(this).parents('.setObject').eq(0);
			var doi = parent_nya.attr("data-object-id");
			var may_handle = parent_nya.children('.fields_below').eq(0).children('.setObject-drag-handle');
			if (parent_nya.is('[data-original-title][data-type="pickList"]')||parent_nya.is('[data-original-title][data-type="button"]')) {
				parent_nya.removeAttr('data-original-title');
			}
					
			else if (may_handle.length >= 1) {

				may_handle.attr('data-original-title', ako_toh).tooltip();
			}
			else {
				// parent_nya.attr('data-original-title',ako_toh).tooltip();
			}
			if($(this).parent().is('.obj_f-aligned-left-wrap')){ //FS#8042
				parent_nya.find('#label_'+doi).eq(0).resizable({
                    handles: "e"
                });
			}

	});
       
        return this;
    };

    

    $.fn.snapToGrid = function(options){
    	var settings = $.extend({
    		"gridInterval":10,
    		"topSetBack":0,
    		"leftSetBack":0
    	},options);
    	var top_set_back = settings['topSetBack'];
    	var left_set_back = settings['leftSetBack'];
    	var this_position_left = this.position().left + Number(this.css('padding-left').replace('px',""));
    	var this_position_top = this.position().top + Number(this.css('padding-top').replace('px',""));
    	var this_position_left_remainder = this_position_left % Number(settings['gridInterval']);
    	var this_position_top_remainder = this_position_top % Number(settings['gridInterval']);
    	
    	return this.css({
    		'left': this_position_left - this_position_left_remainder - left_set_back,
    		'top':this_position_top - this_position_top_remainder - top_set_back
    	});
    	
    	
    }


    $.fn.testCollision = function(options){
   		var settings = $.extend({
   			"containment": "parent"
   		},options);

   		if(settings['containment'] === "parent"){
   			settings['containment'] = this.parent();
   		}
   		var checked_ele = this.map(function(a,b){
   			var this_element = $(b);
   			var containment = settings['containment'];
   			var this_ele_top = this_element.position().top;
   			var this_ele_left = this_element.position().left;
   			var this_ele_width = this_element.outerWidth();
   			var this_ele_height = this_element.outerHeight();
   			if(this_element.is('[data-type="createLine"]')){
   				this_ele_width = this_element.find('div[data-type="createLine"]').outerWidth();
   				this_ele_height = this_element.find('div[data-type="createLine"]').outerHeight();
   			}
   			var formbuilder_ws_height = containment.height();
   			var formbuilder_ws_width = containment.width();
   			var this_ele_top_collide = this_ele_top + this_ele_height;
   			var this_ele_left_collide = this_ele_left + this_ele_width;
   			console.log("collision log",formbuilder_ws_width,this_ele_width)
   			var c_bot = 0;
   			var o_bot = 0;
   			var c_top = 0;
   			var o_top = 0;
   			var c_left = 0;
   			var o_left = 0;
   			var c_right = 0;
   			var o_right = 0;
   			
   			if(this_ele_top_collide >= formbuilder_ws_height){
   				
   				c_bot = true;
   				o_bot = Math.abs(this_ele_top_collide - formbuilder_ws_height);
   			}
   			else{
   				
   				c_bot= false;
   			}
   			if(this_ele_left_collide >= formbuilder_ws_width){
   				
   				c_right= true;
   				o_right = Math.abs(this_ele_left_collide -formbuilder_ws_width);
   				
   			}
   			else{
   				
   				c_right= false;
   			}
   			if(this_ele_top<=0){
   				
   				c_top = true;
   				o_top = Math.abs(0 - this_ele_top);
   			}
   			else{
   				c_top= false;
   			}
   			if(this_ele_left<=0){
   				c_left= true;
   				o_left = Math.abs(0 - this_ele_left);
   			}
   			else{
   				c_left = false;
   			}
   			return {
   				"collide_top":c_top,
   				"collide_bot":c_bot,
   				"collide_left":c_left,
   				"collide_right":c_right,
   				"collide_overflow_top": o_top,
   				"collide_overflow_bot": o_bot,
   				"collide_overflow_left": o_left,
   				"collide_overflow_right": o_right,
   			}
   		}).get();
   		return {
   			"final_collision_top":checked_ele.map(function(a){ return a["collide_top"]; }).filter(Boolean).length >= 1,
   			"final_collision_bot":checked_ele.map(function(a){ return a["collide_bot"]; }).filter(Boolean).length >= 1,
   			"final_collision_left":checked_ele.map(function(a){ return a["collide_left"]; }).filter(Boolean).length >= 1,
   			"final_collision_right":checked_ele.map(function(a){ return a["collide_right"]; }).filter(Boolean).length >= 1,
   			"final_collide_overflow_top": checked_ele.map(function(a){ return a["collide_overflow_top"]}).sort(function(a,b){ return b-a })[0],
   			"final_collide_overflow_bot": checked_ele.map(function(a){ return a["collide_overflow_bot"]}).sort(function(a,b){ return b-a })[0],
   			"final_collide_overflow_left": checked_ele.map(function(a){ return a["collide_overflow_left"]}).sort(function(a,b){ return b-a })[0],
   			"final_collide_overflow_right": checked_ele.map(function(a){ return a["collide_overflow_right"]}).sort(function(a,b){ return b-a })[0],
   		}
    }


    /*tooltip plugin for events*/
    $.fn.customToolTip = function(options){
    	var settings = $.extend({
    		"event":"resize"
    	},options);
    	var ele = this;

    	/*tooltip for resizable */
    	if(settings['event']==="resize"){
    		$('body').off('mouseup.resizableMouseUP');
    		$('body').on('mouseup.resizableMouseUP',function(){
    			$('.resizeableTooltip-s').removeClass('resizeableTooltip-s').empty();
    			$('.resizeableTooltip-e').removeClass('resizeableTooltip-e').empty();
    		});
    		var resizableTip = '<span></span>';
    		var eleWidth = ele.parent().width();
    		var eleHeight = ele.parent().height();
    		if(ele.is('.ui-resizable-s')){
    			if(!ele.is('.resizeableTooltip-s')){
    				ele.addClass('resizeableTooltip-s').append(resizableTip);
    				ele.children().css('left',eleWidth + 10 +'px').text(eleWidth+'px x '+eleHeight+'px')
    			}
    			else{
    				ele.children().css('left',eleWidth + 10 +'px').text(eleWidth+'px x '+eleHeight+'px')

    			}
    		}
    		//console.log("tipresize",element.find('.resizeableTooltip').length)
    		else if(ele.is('.ui-resizable-e')){
    			if(!ele.is('.resizeableTooltip-e')){
    				ele.addClass('resizeableTooltip-e').append(resizableTip);
    				ele.children().css('bottom',eleHeight + 10 +'px').text(eleWidth+'px x '+eleHeight+'px')
    			}
    			else{
    				ele.children().css('bottom',eleHeight + 10 +'px').text(eleWidth+'px x '+eleHeight+'px')
    			}
    		}
    	}

    	/*tooltip for drag*/
    	if(settings['event']==="drag"){
    		$('body').off('mouseup.draggableMouseUP');
    		$('body').on('mouseup.draggableMouseUP',function(){
    			$('.draggableTooltip').removeClass('draggableTooltip').find('.draggable-tip').remove();
    		});
    		
    		var draggableTip = '<span class="draggable-tip"></span>';
    		var eleLeft = ele.position().left;
    		var eleTop = ele.position().top;
    		if(!ele.is('.draggableTooltip')){
    			ele.addClass('draggableTooltip').append(draggableTip);
    			ele.find('.draggable-tip').css('bottom',ele.outerHeight()+10+"px").text(eleTop+'px x '+eleLeft+'px');
    		}
    		else{
    			ele.find('.draggable-tip').css('bottom',ele.outerHeight()+10+"px").text(eleTop+'px x '+eleLeft+'px');
    		}
    		
    	}
    	
    	return ele;
    }


   
}(jQuery));
