var app = angular.module("chat-collab", ['btford.socket-io','socket_io']);    

controllers= {
    "Online_listController"     :   ['$scope','getChatObject','mySocket', fnOnlineList]
}

app.controller(controllers);

function fnOnlineList($scope,getChatObject,mySocket){
    console.log("AFTER LOAD!",$scope)

    $scope.updateList = function () {
        getChatObject.getOnlineList().then(function(response){
            $scope.onlineList = response.data;
            $scope.OnlineCount = $scope.onlineList.length;
            // console.log("Online List")
        }); 

        getChatObject.totalUnreadMessages().then(function(response){
            $scope.totalUnreadMessages = response.data;
            // console.log("Unread Count")
        })

    	getChatObject.getGroupThreadList().then(function(Threadresponse){
            $scope.groupThreadList = Threadresponse.data;
    	    // console.log("Thread List");
        });
    }
    $scope.search = function (row) {

        return !!((row.thread_name.toLowerCase().indexOf($scope.threadSearch || '') !== -1 || row.title.toLowerCase().indexOf($scope.threadSearch || '') !== -1));
    };
    $scope.activate_thread = function(x){
            console.log("active thread!",x);
            var tid = x;
            $scope.msgIndex = 10;
            if(x.thread_id){
                $scope.activethread = x.thread_id;
                // console.log("Not Empty",x);
                getChatObject.getThreadMessage(tid.thread_id,'10').then(function(response){
                    // console.log("HET",response.data)
                        response.data.threadId = tid.thread_id;
                        response.data.display_name = tid.display_name || tid.thread_name;
                        response.data.recipients = tid.recipients;
                        $scope.activethread= tid.thread_id;

                        $scope.threadMessages[tid.thread_id] =response.data;
                        console.log($scope);
                });
                $scope.recipients = x.recipients;
            }else{
                console.log("Empty",x);
                $scope.activethread= null;
                $scope.newRecipients= x;
            }
    }
    $scope.addMessage = function (){
            if($scope.activethread){
                var tid = $scope.activethread;
                var recipients = $scope.threadMessages[tid].recipients;
                // console.log("Add Message",$scope);

                $scope.threadMessages[tid].messages.unshift({
                            attachment_url_list: null,
                            author_display_name: angular.element('#current-user-display-name').html(), 
                            author_id: angular.element('#current-user-id').html(),
                            author_image_extension: null,
                            author_image_url:angular.element('#current-user-profile-image').find('img').attr('src'),
                            id: '999999',
                            thread_id: $scope.activethread,
                            message : $scope.sampletext,
                            date_posted : 'Sending...'  
                });
                
                // console.log("ADDED",$scope)
                
                getChatObject.sendMessage($scope.threadMessages[tid],$scope.sampletext).then(function(response){
                    
                    $scope.threadMessages[tid].messages= $scope.threadMessages[tid].messages.filter(function(val){
                            return val.id !== '999999';
                    });

                    $scope.threadMessages[tid].messages.unshift({
                            attachment_url_list: response.data.results['attachment_url_list'],
                            author_display_name: response.data.results['author_display_name'], 
                            author_id: response.data.results['author_id'],
                            author_image_extension: response.data.results['author_image_extension'],
                            author_image_url:response.data.results['author_image_url'],
                            id: response.data.results['id'],
                            thread_id: response.data.results['thread_id'],
                            message : response.data.results['message'],
                            date_posted : response.data.results['date_posted']  
                    });
                     console.log('after add',$scope);
                    mySocket.emit("custom_event", {
                        event_triggered: "message_recieved",
                        sendTo: $scope.recipients.split(","),
                        data: {
                            messageJSON:{
                                type : angular,
                                message_raw : $scope.sampletext,                                
                                messageId : response.data.results['id'],
                                threadId : response.data.results['thread_id'],
                                author :{ 
                                    displayName : response.data.results['author_display_name'],
                                    id : response.data.results['author_id'],
                                    imageURL : response.data.results['author_image_url']
                                }
                            },
                            recipients: $scope.recipients,
                            threadId : response.data.results['thread_id'],
                            date_posted : response.data.results['date_posted']
                        }
                    });
                    // $.event.trigger({
                    //     type: "newReplySent",
                    //     recipients:  $scope.recipients.split(","),
                    //     threadId: response.data.results['thread_id'],
                    //     sendTo:  $scope.recipients.split(","),
                    //     messageHTML: null,
                    //     message_raw: $scope.sampletext
                    // });
                    if($scope.threadMessages[tid].messages.length > 10){
                        $scope.msgIndex = $scope.msgIndex + 1;
                        console.log($scope.msgIndex)
                    }

                    $scope.sampletext = '';
                });
            }else{
                $scope.activethread ='999'
                $scope.threadMessages[999] =({
                        current_user :  angular.element('#current-user-id').html(),
                        display_name : $scope.newRecipients.display_name,
                        messages : [],
                        recipients : $scope.newRecipients.recipients, 
                        threadId : '999',
                        thread_title : $scope.newRecipients.display_name,
                        thread_title_full : $scope.newRecipients.display_name
                });
                $scope.threadMessages[999].messages.unshift({
                            attachment_url_list: null,
                            author_display_name: angular.element('#current-user-display-name').html(), 
                            author_id: angular.element('#current-user-id').html(),
                            author_image_extension: null,
                            author_image_url:angular.element('#current-user-profile-image').find('img').attr('src'),
                            id: '999999',
                            thread_id: '999',
                            message : $scope.sampletext,
                            date_posted : null  
                });

                getChatObject.sendMessage($scope.newRecipients,$scope.sampletext).then(function(response){
                    delete $scope.threadMessages["999"];
                    // console.log('response',response);
                    var tid =response.data.results['thread_id'];
                    $scope.activethread = response.data.results['thread_id'];

                    $scope.threadMessages[tid]= {
                        current_user : $scope.newRecipients.current_user,
                        display_name : $scope.newRecipients.display_name,
                        messages : [],
                        recipients : $scope.newRecipients.recipients, 
                        threadId : $scope.newRecipients.threadId,
                        thread_title : $scope.newRecipients.thread_title,
                        thread_title_full : $scope.newRecipients.thread_title_full
                     }
                    $scope.threadMessages[tid].messages.push(response.data.results);
                    // console.log("Scope",$scope);

                    // console.log("WAW!!!",$scope.recipients);
                    mySocket.emit("custom_event", {
                        event_triggered: "message_recieved",
                        sendTo: $scope.recipients.split(","),
                        data: {
                            messageJSON:{
                                type : angular,
                                message_raw : $scope.sampletext,                                
                                messageId : response.data.results['id'],
                                threadId : response.data.results['thread_id'],
                                author :{ 
                                    displayName : response.data.results['author_display_name'],
                                    id : response.data.results['author_id'],
                                    imageURL : response.data.results['author_image_url']
                                }
                            },
                            recipients: $scope.recipients,
                            threadId : response.data.results['thread_id'],
                            date_posted : response.data.results['date_posted']
                        }
                    });

                $scope.sampletext = '';
                });    
            }
    }
    $scope.chat_keypress = function (keyEvent){
        if(keyEvent.keyCode  == '13'){
            // console.log("Press!!!!",$scope.connected.threadMessages)
                 $scope.addMessage();
        }else{
            $scope.readMessage($scope.activethread);
        }
    }
    $scope.readMessage = function (threadId){
        console.log("read!",threadId);
        var unread_thread = $scope.groupThreadList.filter(function(val){
                                return val.thread_id==threadId
                            })[0].unread_messages;

            if(unread_thread > 0){
                getChatObject.markReadThread(threadId).then(function (response){
                    console.log("Message Read!");
                    $scope.decUnread(threadId)
                })
            };
    }
    $scope.incUnread = function (threadId){
            
            $scope.totalUnreadMessages = parseInt($scope.totalUnreadMessages) + 1;

            var unread_thread = $scope.groupThreadList.filter(function(val){
                            return val.thread_id==threadId
                         })[0];
            if(unread_thread){
                unread_thread.unread_messages = parseInt(unread_thread.unread_messages) + 1;
            }
            var unread_online = $scope.onlineList.filter(function(val){
                            return val.thread_id==threadId
                         })[0];
            if(unread_online){
                unread_online.unread_messages = parseInt(unread_online.unread_messages) + 1;
            }
    }
    $scope.decUnread = function (threadId){
            

            var unread_message_count = $scope.groupThreadList.filter(function(val){
                            return val.thread_id==threadId
                         })[0].unread_messages;   
            $scope.groupThreadList.filter(function(val){
                            return val.thread_id==threadId
                         })[0].unread_messages = 0;
            if($scope.onlineList.filter(function(val){
                            return val.thread_id==threadId
                         })[0]){
            $scope.onlineList.filter(function(val){
                            return val.thread_id==threadId
                         })[0].unread_messages = 0;
            }
            $scope.totalUnreadMessages = $scope.totalUnreadMessages - unread_message_count;
            console.log($scope);
    }
    $scope.viewPreviousMsg = function (){
        console.log("MsgIndex",$scope.msgIndex)
        $scope.msgIndex = $scope.msgIndex +10;
        console.log("MsgIndex after",$scope.msgIndex)
        console.log($scope) 
    }

    $scope.updateList();
    $scope.threadMessages = {};
    $scope.msgIndex= 10;

    mySocket.on('message_recieved', function(data){
        console.log("Mejo Badtrip",data);
        if($scope.threadMessages[data.threadId]){
            $scope.threadMessages[data.threadId].messages.unshift({
                            attachment_url_list: null,
                            author_display_name: data.messageJSON.author.displayName, 
                            author_id : data.messageJSON.author.id,
                            author_image_extension : null,
                            author_image_url : data.messageJSON.author.imageURL,
                            id: data.messageJSON.messageId,
                            thread_id: data.threadId,
                            message : data.messageJSON.message_raw,
                            date_posted : data.date_posted  
            });

            $scope.incUnread(data.threadId);
            console.log("weh",$scope)
        }else{
            $scope.updateList();
        }
    });

    mySocket.on('userLoggedIn', function (user){
        console.log("socket user logged in",user);
        getChatObject.getThreadIdBySubscriber(user).then(function (results){

            $scope.onlineList.push(
                {
                    display_name : results.data[0].display_name,
                    extension : results.data[0].extension,
                    id : results.data[0].id,
                    image_url :  angular.element(user.image).attr('src'),
                    recipients : results.data[0].recipients,
                    thread_id : results.data[0].thread_id,
                    unread_messages :results.data[0].unread_messages
            }) 
            $scope.OnlineCount = $scope.OnlineCount + 1; 
            // console.log($scope);
        })
    });

    mySocket.on('userLoggedOut', function (user) {
        console.log("socket user logged out",user);
        $scope.onlineList= $scope.onlineList.filter(function(val){
                return val.id !== user.id ;
        });
        // console.log($scope);
        if($scope.OnlineCount > 0){
            $scope.OnlineCount = $scope.OnlineCount - 1; 
        }
    });
}