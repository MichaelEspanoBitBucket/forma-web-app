var app = angular.module("chat-collab");

app.directive('errSrc', function() {
      return {
        link: function(scope, element, attrs) {
          element.on('error', function() {
            if (attrs.src != attrs.errSrc) {
              attrs.$set('src', attrs.errSrc);
            }
          });
        }
      }
});


app.directive('menuBar',function(){
  return {
    restrict: 'E',
    templateUrl: '/modules/chat_collab/templates/menu-bar.html'
  };
});

app.directive('onlineList',function(){
  return {
    restrict: 'E',
    templateUrl: '/modules/chat_collab/templates/online-list.html'
  };
});

app.directive('threadList',function(){
  return {
    restrict: 'E',
    templateUrl: '/modules/chat_collab/templates/thread-list.html'
  };
});

app.directive('chatWindow',function(){
  return {
    restrict: 'E',
    templateUrl: '/modules/chat_collab/templates/chat-window.html'
  };
});
app.directive('newMessage',function(){
  return {
    restrict: 'E',
    templateUrl: '/modules/chat_collab/templates/new-message.html'
  };
});