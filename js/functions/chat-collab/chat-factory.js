
var app = angular.module("chat-collab");

app.factory('getChatObject', function($http,ctrlConnection){
    var root = {};
    root.init = function(){

        return this;
    };
    root.setModel = function(scopeM){
        this.scopeModel = scopeM;
    };
    root.getOnlineList=function(){
            var self = this;
            var url= "/portal/forma_online_users";
            var params = {
                return_type : "JSON"
            }

            return $http({
                url: url,
                method: "GET",
                params: params
            })
    };
    root.getGroupThreadList=function(){
            var self = this;
            var url = '/portal/forma_group';
            var params = {
                return_type : "JSON"
            }
            
            return $http({
                url: url,
                method: "GET",
                params: params
            })
    }
    root.getThreadMessage=function(threadId,msgIndex){
            var self = this;
            var url = '/messaging/thread_messages';
            var params = {
                requested_response : "JSON",
                index : msgIndex,
                threadId : threadId
            }
            // console.log('hey',ctrlConnection.getConnections());
            return $http({
                url: url,
                method: "GET",
                params: params
            })
    }
    root.sendMessage = function(messageDetails,samplemessage){
            // console.log("DETAILS",messageDetails);
            var self = this;
            var url = '/messaging/write_message';
            var params = {
                message: samplemessage,
                // attachments: attachments,
                requested_response: "JSON",
            };


            if(messageDetails.threadId){
                var triggerType = "newReplySent";
                params.thread_id = messageDetails.threadId;      
                var triggerRecipients = messageDetails.recipients;       
            } else{
                var triggerType = "newMessageSent";
                params.recipients = [{
                    type: "1",
                    display_text: messageDetails.display_name,
                    value: messageDetails.recipients
                }];
            }

            return $http({
                method: "POST",
                url: url,
                data : $.param(params),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            })
    }
    root.markReadThread = function (threadId) {
        var url = "/messaging/change_thread_read_flag";
        var params = {
            thread_id: threadId,
            read_flag: 1,
        };

        return $http({
            method : "POST",
            url : url,
            data : $.param(params),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
    }
    root.totalUnreadMessages = function () {
        var url = "/messaging/unread_messages_count";

        return $http({
            method : "GET",
            url : url,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
    }        
    root.getThreadIdBySubscriber = function (user){
        // console.log(user);
        var url= "/portal/loggedInUserDetails";
        var params = {
            user_id : user.id
        }

        return $http({
                url: url,
                method: "GET",
                params: params
            })
    }
    root.getFactoryStructure=function(){
            return this;
    };
    return root.init();             
});


app.factory('ctrlConnection', function() {
  var ctrlConnections = {
        threadId : "",
        threadMessages : {
            current_user: "",
            messages: [],
            thread_title: "",
            thread_title_full: ""
        },
        totalUnreadMessages : ""
  };

  return {
    setConnection: setConnection,
    getConnections: getConnections
  };

  function setConnection(key,value) {
        ctrlConnections[key]= value;
        // console.log(ctrlConnections);
  }

  function getConnections() {
    return ctrlConnections;
  }
});


