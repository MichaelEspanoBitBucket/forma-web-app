var app = angular.module("chat-collab");
app.filter('sortMe', function(){
    return function (array_data, sort_opts, as_var, by_var) {
        // console.log(arguments);
        var args = Array.prototype.slice.apply( arguments );
        var default_settings = {
            'key':'index',
            'as':'number',
            'by':'asc'
        }
        if(args.length == 2){
            default_settings = angular.extend(default_settings, sort_opts);
        }else if(args.length >= 3){
            default_settings['key'] = sort_opts;
            default_settings['as'] = as_var;
            if(by_var){
                default_settings['by'] = by_var;
            }
        }
        if(!array_data){
            return [];
        }
        if(!default_settings){
            return array_data;
        }
        if(array_data.constructor === Object){
            array_data = Object.keys(array_data).map(function(key){array_data[key]._keyIndex = key; return array_data[key]; });
        }
        var temp_array_data = [].concat(array_data);
        return array_data.sort(function(a,b){
            var a_val = a, b_val = b, temp_a = '', temp_b = '';

            if(default_settings['key'] == 'index'){
                a_val = a['_keyIndex']||temp_array_data.indexOf(a);
                b_val = b['_keyIndex']||temp_array_data.indexOf(b);
            }else if(default_settings['key']){
                a_val = a[default_settings['key']];
                b_val = b[default_settings['key']];
            }
            
            if(default_settings['as'] == 'number'){
                a_val = Number(a_val);
                b_val = Number(b_val);
            }else if(default_settings['as'] == 'time'){

            }else if( (default_settings['as']||"string") == "string" ){
                temp_a = String(a_val).localeCompare(String(b_val));
                temp_b = String(b_val).localeCompare(String(a_val));
                a_val = temp_a;
                b_val = temp_b;
            }

            if(default_settings['by'] == 'desc'){
                return b_val - a_val;
            }else{
                return a_val - b_val;
            }
        })
    }
});

