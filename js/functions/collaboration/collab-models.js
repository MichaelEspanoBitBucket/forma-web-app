var dbModels = angular.module('CollaborationServices',[]);

dbModels.service('CollabService', function($http){

	this.loadGroups = function($query) {
		params = {
			search_string:$query
		}
	    return $http.post('/gi_data/gi-rd-get-all-groups',serialize(params), { 
			headers: {
				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
			}
		}).then(
				function(data){
					for (var i in data.data){
						data.data[i].text = data.data[i].group_name
					}

					return data.data;
				},
				function(data){
					return data;
				}
			)
  	};

	this.loadNames = function($query) {

		params = {
			search_string:$query
		}
	    return $http.post('/modules/dashboard_data/name_selection.php',serialize(params), { 
			headers: {
				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
			}
		}).then(
				function(data){
					
					return data.data;
				},
				function(data){
					return data;
				}
			)
  	};

  	this.saveCollab = function(collab){
		d = {Collab:collab}
		return $http.post('/modules/collaboration_data/save-new-collaboration.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.addCollaborators = function(collab){
		d = {Collab:collab}
		return $http.post('/modules/collaboration_data/add-collaborators.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}
	this.removeCollaborator = function(collab){
		d = {Collab:collab}
		return $http.post('/modules/collaboration_data/remove-collaborator.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}


	this.getCollaboration = function($collabID){
		d = {collabID:$collabID}
		return $http.post('/modules/collaboration_data/get-collaboration.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.getSocketUrl = function(){
		// d = {collabID:$collabID}
		return $http.get('/modules/collaboration_data/get-socket-url.php');
	}

	this.getSchedulerCollaborators = function($collabID){
		d = {collabID:$collabID}
		return $http.post('/modules/collaboration_data/get-collaboration-scheduler-collaborators.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}


})

dbModels.service('SchedulerService', function($http){

  	
	
	this.getSchedulerCollaborators = function($collabID){
		d = {collabID:$collabID}
		return $http.post('/modules/collaboration_data/get-collaboration-scheduler-collaborators.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.getLegends = function($collabID){
		d = {collabID:$collabID}
		return $http.post('/modules/collaboration_data/get-scheduler-legends.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.getSchedules = function($collabID, $from, $to){
		d = {collabID:$collabID, from:$from, to:$to};
		return $http.post('/modules/collaboration_data/get-scheduler-schedules.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.getSchedulesByUser = function($user_id, $from, $to){
		d = {user_id:$user_id, from:$from, to:$to};
		return $http.post('/modules/collaboration_data/get-scheduler-schedules-by-user.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.addSchedulerLegend = function($legend){
		d = {Legend:$legend}
		return $http.post('/modules/collaboration_data/add-scheduler-legend.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.removeSchedule = function($sched_id){
		d = {sched_id:$sched_id}
		return $http.post('/modules/collaboration_data/remove-scheduler-schedule.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.removeSchedulerLegend = function($legendId){
		d = {LegendId:$legendId}
		return $http.post('/modules/collaboration_data/remove-scheduler-legend.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.saveSchedulesByUser = function($user){
		d = {User:$user}
		return $http.post('/modules/collaboration_data/save-schedules-by-user.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}




})


	


function serialize(obj, prefix) {
    var str = [];
    for (var p in obj) {
      if (obj.hasOwnProperty(p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
              v = obj[p];
          str.push(typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}