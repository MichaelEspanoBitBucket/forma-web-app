var app = angular.module('collaboration-scheduler', ['ngDragDrop', 'CollaborationServices',"color.picker", "ui.bootstrap"]);

app.controller('schedulerController', function($scope, $window,  CollabService, SchedulerService, $uibModal,$log, $timeout){

$scope.currentDate = new Date();
$scope.today = new Date();

$scope.weekday = new Array(7);
$scope.weekday[0]=  "S";
$scope.weekday[1] = "M";
$scope.weekday[2] = "T";
$scope.weekday[3] = "W";
$scope.weekday[4] = "T";
$scope.weekday[5] = "F";
$scope.weekday[6] = "S";

$scope.month = new Array();
$scope.month[0] = "January";
$scope.month[1] = "February";
$scope.month[2] = "March";
$scope.month[3] = "April";
$scope.month[4] = "May";
$scope.month[5] = "June";
$scope.month[6] = "July";
$scope.month[7] = "August";
$scope.month[8] = "September";
$scope.month[9] = "October";
$scope.month[10] = "November";
$scope.month[11] = "December";
$scope.days = [];
$scope.sched = this;

$scope.holidays = {
	"2016-08-21":{date:"2016-08-21", name:"Ninoy Aquino Day", description:"Special Non-working Holiday"}

};

//$window.testvar = "bbbb"
$scope.testvar = $window.testvar;
// $scope.collabID = angular.element("#collabID").val();
// $scope.params = $scope.collabID;

// $scope.params = $scope.collabID;
// console.log($scope.params)

this.updateScheds = function(){
	$scope.mySocket.emit("custom_event", {
      event_triggered: "sched_recieved",
      sendTo: "BROADCAST",
      data: {
          id:$scope.collaboration.current_user.id
          }
      });
}

$scope.loadUserById = function($user_id){
	console.log($user_id)
	$scope.resources[$user_id].isEditing=true;
SchedulerService.getSchedulesByUser($user_id, $scope.days[0].yyyymmdd(), $scope.days[$scope.days.length - 1].yyyymmdd()).then(function(data){
		//console.log(data.data);
		var schedules = data.data;
		$scope.resources[$user_id].schedules = {}
		for (var i = schedules.length - 1; i >= 0; i--) {
			$scope.sched.addSchedule(schedules[i].user_id, schedules[i]);
		};
		$timeout(function(){$scope.resources[$user_id].isEditing = false;},200);
	}, function(){})
}

this.loadResources = function(){
	CollabService.getSchedulerCollaborators($scope.params.id).then(function(data){
		$scope.resources = data.data
		$scope.sched.loadSchedules();
		// $scope.$watch(function(){ return $scope.resources[$scope.collaboration.current_user.id].schedules}, function(newValue, oldValue, scope) {
		// 	console.log("asdsad")
		// });

	}, function(){
		$scope.resources = [
			{"display_name":"Mark Rowi Dizon",schedules:{}},
			{"display_name":"Edgar Palmon",schedules:{"2016-04-06":{id:"1","title":"VL","color":"#337ab7"}}},
			{"display_name":"Jeff Jalac",schedules:{"172016":{"id":"1","color":"#03b4d5","title":"1"},"272016":{"id":"2","color":"#ffc107","title":"2"},"372016":{"id":"2","color":"#ffc107","title":"2"},"472016":{"id":"3","color":"#F44336","title":"3"},"572016":{"id":"1","color":"#03b4d5","title":"1"},"872016":{"id":"2","color":"#ffc107","title":"2"},"972016":{"id":"1","color":"#03b4d5","title":"1"},"1072016":{"id":"2","color":"#ffc107","title":"2"},"1172016":{"id":"3","color":"#F44336","title":"3"},"1272016":{"id":"1","color":"#03b4d5","title":"1"},"1572016":{"id":"2","color":"#ffc107","title":"2"},"1672016":{"id":"3","color":"#F44336","title":"3"},"1772016":{"id":"1","color":"#03b4d5","title":"1"},"1872016":{"id":"2","color":"#ffc107","title":"2"},"1972016":{"id":"1","color":"#03b4d5","title":"1"}}},
			{"display_name":"Michelle Anne Onofre",schedules:{"172016":{"id":"1","color":"#03b4d5","title":"1"},"272016":{"id":"3","color":"#F44336","title":"3"}}},
			{"display_name":"Jeff Jalac",schedules:{}},
			{"display_name":"Kath Pineda",schedules:{}},
			{"display_name":"Daniel Matthew Binalia",schedules:{}},
			{"display_name":"Lance Trigo",schedules:{}},
			{"display_name":"Alyssa Santos",schedules:{}}]
			

	})
}
this.loadResources();



// $scope.resources = [
// 		{"display_name":"Mark Rowi Dizon",schedules:{}},
// 		{"display_name":"Edgar Palmon",schedules:{"2016-08-08":{id:"1","title":"VL","color":"#337ab7"}}},
// 		{"display_name":"Jeff Jalac",schedules:{"172016":{"id":"1","color":"#03b4d5","title":"1"},"272016":{"id":"2","color":"#ffc107","title":"2"},"372016":{"id":"2","color":"#ffc107","title":"2"},"472016":{"id":"3","color":"#F44336","title":"3"},"572016":{"id":"1","color":"#03b4d5","title":"1"},"872016":{"id":"2","color":"#ffc107","title":"2"},"972016":{"id":"1","color":"#03b4d5","title":"1"},"1072016":{"id":"2","color":"#ffc107","title":"2"},"1172016":{"id":"3","color":"#F44336","title":"3"},"1272016":{"id":"1","color":"#03b4d5","title":"1"},"1572016":{"id":"2","color":"#ffc107","title":"2"},"1672016":{"id":"3","color":"#F44336","title":"3"},"1772016":{"id":"1","color":"#03b4d5","title":"1"},"1872016":{"id":"2","color":"#ffc107","title":"2"},"1972016":{"id":"1","color":"#03b4d5","title":"1"}}},
// 		{"display_name":"Michelle Anne Onofre",schedules:{"172016":{"id":"1","color":"#03b4d5","title":"1"},"272016":{"id":"3","color":"#F44336","title":"3"}}},
// 		{"display_name":"Jeff Jalac",schedules:{}},
// 		{"display_name":"Kath Pineda",schedules:{}},
// 		{"display_name":"Daniel Matthew Binalia",schedules:{}},
// 		{"display_name":"Lance Trigo",schedules:{}},
// 		{"display_name":"Alyssa Santos",schedules:{}}]

SchedulerService.getLegends($scope.params.id).then(function(data){
	$scope.legends = data.data
})
// $scope.test ="testssss";
// 		$scope.legends = [
// 			{"id":"1","color":"#03b4d5","title":"1", "description":"10 PM to 6 AM"}, 
// 			{"id":"2","color":"#ffc107","title":"X", "description":"6 AM to 2 PM "}, 
// 			{"id":"2","color":"#ffc107","title":"YXY", "description":"6 AM to 2 PM"}, 
// 			{"id":"2","color":"#ffc107","title":"2", "description":"6 AM to 2 PM"}, 
// 			{"id":"3","color":"#F44336","title":"3", "description":"2 PM to 10 PM"}
// 		];

// var modalInstance;
this.openResource = function () {
	//console.log("sd");
    var modalInstance = $uibModal.open({
      animation: false,
      templateUrl: 'myModalAddResource.html',
      controller: 'addResourceController',
      //size: size,
      resolve: {
        items: function () {
          return $scope.resources;
        }
      }
    });
      modalInstance.result.then(function (selectedItem) {
      console.log(selectedItem);
      $scope.addCollaborators(selectedItem).then(function(){
      	$scope.sched.loadResources();
      })
      
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
}

this.removeCollaborator = function($user_id){
	//console.log($scope.resources[$user_id])
	var collab = {
        id:$scope.collaboration.id,
        user_id: $user_id
    }


	CollabService.removeCollaborator(collab).then(function(){
		//$scope.resources.splice($user_id, 1);
		delete $scope.resources[$user_id];
	});
}


this.openLegend = function () {
	//console.log("sd");
    var modalInstance = $uibModal.open({
      animation: false,
      templateUrl: 'myModalAddLegend.html',
      controller: 'addLegendController',
      //size: size,
      resolve: {
        items: function () {
        	
          return $scope.resources;
        }
      }
    });
      modalInstance.result.then(function (item) {

		$scope.schedTitle = item.schedTitle;
		$scope.schedDescription = item.schedDescription;
		$scope.schedColor = item.schedColor.charAt(0) == '#'?item.schedColor:'#' + item.schedColor;

      	$scope.sched.addLegend();
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
}

  


this.nextWeek = function(){
	var date = $scope.days[$scope.days.length-1]
	console.log();
	var newDate = new Date(date.setTime( date.getTime() + 1 * 86400000 ));
	//date.setDate($scope.days[$scope.days.length-1].getDate() + 1);
	$scope.days = getDaysInWeek(newDate);

	$scope.dayWidth = 75/$scope.days.length;
}

this.previousWeek = function(){
	var date = $scope.days[0]

	var newDate = new Date(date.setTime( date.getTime() - 86400000 ));
	//date.setDate($scope.days[$scope.days.length-1].getDate() + 1);
	$scope.days = getDaysInWeek(newDate);

	$scope.dayWidth = 75/$scope.days.length;
}
this.nextMonth = function(){
	$scope.currentDate = new Date($scope.currentDate.setMonth($scope.currentDate.getMonth() + 1));
	$scope.days = getWeeksInMonth($scope.currentDate.getMonth(), $scope.currentDate.getFullYear());
this.loadSchedules();
}
this.previousMonth = function(){
	$scope.load = "loading..."
	$scope.currentDate = new Date($scope.currentDate.setMonth($scope.currentDate.getMonth() - 1));
	$scope.days = getWeeksInMonth($scope.currentDate.getMonth(), $scope.currentDate.getFullYear());
//	$scope.load = "done...";
this.loadSchedules();
}

this.today = function(){
	$scope.load = "loading..."
	var date = new Date;
	$scope.currentDate = date;
	$scope.days = getWeeksInMonth(date.getMonth(), date.getFullYear());
	//$scope.dayWidth = 75/$scope.days.length;
	$scope.load = "done...";
	
}

this.addEvent = function () {
	$scope.resources[3].schedules["2016-08-03"] = {id:"1","title":"VL","color":"#337ab7"};
}

this.addSchedule = function($user_id, $sched){
	if($scope.resources[$user_id].schedules==undefined){
		$scope.resources[$user_id].schedules = {};
	}
	$scope.resources[$user_id]["schedules"][$sched.date] = $sched;
}



this.loadSchedules = function(){
	SchedulerService.getSchedules($scope.params.id, $scope.days[0].yyyymmdd(), $scope.days[$scope.days.length - 1].yyyymmdd()).then(function(data){
		//console.log(data.data);
		var schedules = data.data;
		for (var i = schedules.length - 1; i >= 0; i--) {
			$scope.sched.addSchedule(schedules[i].user_id, schedules[i]);
		};
	}, function(){})
}

this.removeLegend = function($index){
	//$scope.legend[$index]
	$log.info($scope.legends[$index])
	SchedulerService.removeSchedulerLegend($scope.legends[$index].legend_id).then(function(data){
		$scope.legends.splice($index, 1);
	}, function(){

	})
}


this.addLegend = function(){
	var legend = {
		'title':$scope.schedTitle,
		'description':$scope.schedDescription,
		'color':$scope.schedColor.charAt(0) == '#'?$scope.schedColor:'#' + $scope.schedColor,
		'collabId':$scope.collaboration.id
	}

	SchedulerService.addSchedulerLegend(legend).then(function(data){
		$scope.schedTitle = "";
		$scope.schedDescription = "";
		$scope.schedColor = "";
		var legend = {
			legend_id: data.data.id,
		 title: data.data.title,
		  color: data.data.color,
		   description: data.data.description,
		    collaboration_id: data.data.collabId}
	
		$scope.legends.push(legend);
		console.log($scope.legends)
		console.log(data.data)
	}, function(){

	})
	
}



this.saveSchedules = function($user){
	console.log($user.schedules)
	$user.isSaving = true;
	$timeout(function(){
SchedulerService.saveSchedulesByUser($user).then(function(data){
	//console.log(data.data)
	
		angular.forEach($user.schedules, function(value, key){
				if(data.data!=null){
					if(key==data.data.date){
						
						$user.schedules[key].id = data.data.id;
						$user.schedules[key].date = data.data.date;
					}	
				}		
				
				if ($user.schedules[key] == undefined) {
					delete $user.schedules[key];
				};
		});
	
	
		// angular.forEach($user.schedules, function(value, key){
		// 		if(key==data.data.date){
		// 			$user.schedules[key].id = data.data.id;
		// 			$user.schedules[key].date = data.data.date;
		// 		}		
		// });
		$scope.sched.updateScheds();
		$user.isSaving = false;
	})


	},1)
	
}
this.removeSchedule = function($res, $schedule){
	//$resource(url, paramDefaults, actions)
	SchedulerService.removeSchedule($res.schedules[$schedule].id).then(function(){
		delete $res.schedules[$schedule];
		$scope.sched.updateScheds();
	})
}


this.today();

});


app.directive('collabScheduler', function(){

	return {
		restrict:"AE",
		controller:'schedulerController',
		// scope:{
		// 	startDate:"=",
		// 	type:"="
		// },
		templateUrl: '/modules/collaboration/templates/collaboration-scheduler.html',
    	controllerAs:"sched",
		link:function(scope, element, attrs){
			element.find(".slideable").perfectScrollbar();
			angular.element('.sched-td.sched-content').on('scroll', function(e) {
				// console.log(this.scrollTop)
				// console.log(this.scrollLeft)

				angular.element('.sched-td.sched-resource-name').scrollTop(this.scrollTop);
				angular.element('.sched-th.sched-content').scrollLeft(this.scrollLeft);

			})
		}
	};
})


function getWeeksInMonth (month, year) {
	var daysM = this.getDaysInMonth(month, year);
	var daysWf = getDaysInWeek(new Date(daysM[0]));
	var daysWl = getDaysInWeek(new Date(daysM[daysM.length-1]));
	//console.log(daysWl)
	for (var i = daysWf.length - 1; i >= 0; i--) {
		if(daysWf[i].getMonth()!=month){
			daysM.unshift(new Date(daysWf[i]));
		}
		
	};

	for (var i = 0; i <= daysWl.length - 1; i++) {
		if(daysWl[i].getMonth()!=month){
			daysM.push(new Date(daysWl[i]));
		}
		
	};
	
	return daysM;
	
}


function getDaysInMonth(month, year) {
    // Since no $scope.month has fewer than 28 days
    var date = new Date(year, month, 1);
    var days = [];
    //console.log('month', month, 'date.getMonth()', date.getMonth())
    while (date.getMonth() === month) {
    	//console.log(date.getMonth())
        days.push(new Date(date));
        date = new Date(date.setTime( date.getTime() + 1 * 86400000 ));

	   // date.setDate(newDate);

    }
   //console.log(days)
    return days;
}


function getDaysInWeek(date){
	var date =date;
	var dayOfWeek = date.getDay();
	var daysOfWeek = [];
	var d = 0;
    ///console.log(date)
	date.setDate(date.getDate() - dayOfWeek);
	
	while (d <= 6) {
        daysOfWeek.push(new Date(date));
	    date.setDate(date.getDate() + 1);
	    d++;
    }
    return daysOfWeek;
}

app.controller('addLegendController', function ($scope, $uibModalInstance, CollabService, items) {
$scope.colorOptions = {
	format:'hex',
	pos:'top left',
	required:true
}
  $scope.items = items;
  $scope.collaborators;
  $scope.selected = {
    item: $scope.items[0]
  };

   $scope.loadNames = function($query){
   		//console.log($query)
   		return CollabService.loadNames($query);
  	}

  $scope.ok = function () {
    $uibModalInstance.close($scope);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
app.controller('addResourceController', function ($scope, $uibModalInstance, CollabService, items) {

  $scope.items = items;
  $scope.collaborators;
  $scope.selected = {
    item: $scope.items[0]
  };

   $scope.loadNames = function($query){
   		//console.log($query)
   		return CollabService.loadNames($query);
  	}

  $scope.ok = function () {
    $uibModalInstance.close($scope.collaborators);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

app.controller('schedCellController', ['$scope', function($scope){

}])

app.directive("schedCell", function(){
	return {
		restrict:"EA",
		scope:{
			disabled:"=",
			schedule:"=",
			removeSched:"&",
			saveSched:"&"
		},
		controller:'schedCellController',
		controllerAs:'sCell',
		templateUrl:'/modules/collaboration/templates/collaboration-scheduler-cell.html',
		link:function(){

		}
	}
})


app.directive('combineHorizontalScrolls', [function(){
		var scrollLeft = 0;
		function combine(elements){
			elements.on('scroll', function(e){
				if(e.isTrigger){
					e.target.scrollLeft = scrollLeft;
				}else {
					scrollLeft = e.target.scrollLeft;
					elements.each(function (element) {
						if( !this.isSameNode(e.target) ){
							$(this).trigger('scroll');
						}
					});
				}
			});
		}

		return {
			restrict: 'A',
			replace: false,
			compile: function(element, attrs){
				combine(element.find('.'+attrs.combineHorizontalScrolls));
			}
		};
	}]);

	app.directive('combineVerticalScrolls', [function(){
		var scrollTop = 0;
		function combine(elements){
			elements.on('scroll', function(e){
				if(e.isTrigger){
					e.target.scrollTop = scrollTop;
				}else {
					scrollTop = e.target.scrollTop;
					elements.each(function (element) {
						if( !this.isSameNode(e.target) ){
							$(this).trigger('scroll');
						}
					});
				}
			});
		}

		return {
			restrict: 'A',
			replace: false,
			compile: function(element, attrs){
				combine(element.find('.'+attrs.combineVerticalScrolls));
			}
		};
	}]);

Date.prototype.yyyymmdd = function() {
	  var mm = (this.getMonth() + 1) + ""; // getMonth() is zero-based
	  var dd = this.getDate() + "";

	  return this.getFullYear() + "-" + (mm.length==1? "0": "" ) + mm + "-" + (dd.length==1?"0" : "")+ dd ; // padding
};