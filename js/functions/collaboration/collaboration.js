var app = angular.module("collaboration", ["collaboration-scheduler", 'ngTagsInput', 'CollaborationServices', "ui.router", ,'btford.socket-io','socket_io']);
app.config(function($stateProvider){
 
 $stateProvider
    .state('new-collaboration', {
      url: "",
      templateUrl:"templates/collaboration-new.html",
      controller:"collaborationController"
    })
    .state('collaboration', {
      url: "/{id:int}",
      templateUrl:"templates/collaboration-dashboard.html",
      controller:"collaborationController"
    });
});

app.controller('collaborationController',function($scope, CollabService, $state, $stateParams, mySocket){
	$scope.title = "New Collaboration";
	$scope.collabName = "";
	$scope.collaborators = "";
  $scope.params = $stateParams;
$scope.isCreator = false;
 //console.log()
  if($scope.params.id!=undefined){
    CollabService.getCollaboration($scope.params.id).then(function(data){
      $scope.collaboration = data.data
      $scope.isCreator = $scope.collaboration.creator_id==$scope.collaboration.current_user.id;
      var cUser = $scope.collaboration.current_user;
     // console.log($scope.collaboration.SOCKET_SERVER)
      cUser["image"]="";
      cUser["subscriptionAddresses"]=["general_functions_1"];
      cUser["defaultPublishAddress"]="general_functions_1";
      cUser["disconnectionPublishAddress"]="general_functions_1";
      //angular.value("SOCKET_SERVER") = $scope.collaboration.SOCKET_SERVER;
      $scope.mySocket = mySocket.socket(cUser,$scope.collaboration.SOCKET_SERVER);
      if($scope.collaboration.isCollaborator==null){
        $state.go("new-collaboration")
      }

      $scope.mySocket.on('userLoggedIn', function (user){
          console.log("socket user logged in",user);
          getChatObject.getThreadIdBySubscriber(user).then(function (results){

              $scope.onlineList.push(
                  {
                      display_name : results.data[0].display_name,
                      extension : results.data[0].extension,
                      id : results.data[0].id,
                      image_url :  angular.element(user.image).attr('src'),
                      recipients : results.data[0].recipients,
                      thread_id : results.data[0].thread_id,
                      unread_messages :results.data[0].unread_messages
              }) 
              $scope.OnlineCount = $scope.OnlineCount + 1; 

          })
      });



      $scope.mySocket.on('sched_recieved', function(data){
        console.log(data);
        $scope.loadUserById(data.id);
       })

   
    //Check if current user is a collaborator
    }, function(){
   
  })

  }
  
    $scope.loadNames = function($query){
   		return CollabService.loadNames($query);
  	}

  	$scope.saveCollaboration = function(){
  		if($scope.collabName.trim()==""||$scope.collaborators.length==0){
  			return;
  		}

  		var collab = {
  			name:$scope.collabName,
  			collaborators:$scope.collaborators

  		}
  		CollabService.saveCollab(collab).then(
  			function(data){

          $state.go("collaboration",{id:data.data});
  				//window.location="/modules/collaboration/#/" + data.data;
  			},
  			function(data){
  				window.location="/";
  		});
  		
  	}

    $scope.addCollaborators = function($collaborators){
      
      var collab = {
        id:$scope.collaboration.id,
        collaborators: $collaborators

      }

     return CollabService.addCollaborators(collab);
      
    }

    $scope.removeCollaborator = function($user_id){
      
      var collab = {
        id:$scope.collaboration.id,
        user_id: $user_id

      }
      return CollabService.removeCollaborator(collab);
      
    }

    

})
