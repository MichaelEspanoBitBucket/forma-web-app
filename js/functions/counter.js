get_counter = {
    getCount: function() {
        var counter = 0;
        var dateTime = "";
        var console_log_C = 0 ;
        //pa add lang
        var serverTime = $.ajax({
            url: "/ajax/getDateToday",
            type: "POST",
            data: {
                "access": "get_date"
            },
            cache: false,
            async: false
        });
        body_data = $("body").data();
        body_data.get_counter_data = {getDateTime: serverTime.responseText};
        $("body").data(body_data);
        //pa add lang

        //console.log('test');
        setInterval(function() {
            var this_ajax = $.ajax({
                url: "/ajax/counter",
                cache: false,
            }).done(function(result) {
                var data = new Array();
				
                result = JSON.parse(result);
                if(console_log_C == 0){ //CONSOLE LOGS HERE
                    //console.log("counter.js success result",result);                    
                    //console_log_C++;
                }
				
                data.push(result);
                $(".onlineUser").tooltip('destroy');
                $.each(data, function(id, val) {
                    //console.log(data[id].messageCount)
                    $(".countCompanyUser").html(data[id].userCount); // Count number of user on the company
                    $(".msgCount,.inboxCount").html(data[id].messageCount); // Count number of msg user.
                    $(".requestcounter").html(data[id].requestCount); // Count number of msg user.


                    var is_available = data[id].is_available;
                    var home_page = data[id].home_page;
                    var login_page = data[id].login_page;
                    var location = String(window.location.pathname);
                    var userOnline = data[id].onlineUsers;
                    var index_page = data[id].index_page;
                    server_dateTime = data[id].getDateTime;


                    $(".notiCount").html(data[id].notiCount); // Count number of notification

                    // Tasks
                    //$(".notiCount").html(parseInt($(".notiCount").html()) + task_out_of_date[0].numrows)
                    //if(counter==1){
                    //    if (task_out_of_date[0].numrows != 0) {
                    //        $(".notification-container").append("asd");
                    //         
                    //    }
                    //}
                    //counter++;

                    //console.log(location + "===" + home_page + "\n" + location + "===" + login_page + "\n" + location + "===" + index_page);
                    // Not logged uSers                
                    var user_path = $("#user_url_view").val();
                    if (location != home_page && location != index_page && location != login_page && location.indexOf("login") <= -1 && data[id].is_available == "0"
                        && location != user_path + "index") {
                    //if (location != home_page && location != index_page && location != login_page && location.indexOf("login") <= -1 && data[id].is_available == "0") {
                        counter++;

                        if (counter == 1) {
                            jAlert('Please log in to continue', 'Not Logged In', '', '', '', function(ans) {
                                if (ans == true) {
                                    window.location = '/';
                                }
                            });

                        }
                    }

                    /* ========================= */
                    // For Message Trigger
                    var getInboxCount = $(".inboxCount").html();

                    //if (getInboxCount!=data[id].messageCount) {
                    //    $(".inboxCount").trigger("click");
                    //}


                    /* ========================= */
                    // For users online
                    if (userOnline[0]['countUserOnline'] == "0") {
                        $(".onlineUsers").html("<b>No Users Online.");
                    } else {
                        $(".onlineUsers").html(userOnline[0]['images']);
                        $(".onlineUser").tooltip();
                    }
                    $(".countUOnline").html(userOnline[0]['countUserOnline']);
                });
                var body_data = $("body").data();
                body_data.get_counter_data = result;
                $("body").data(body_data);
            });

        }, 3000);
    }
}

$(document).ready(function() {
    var socketActive = $('#current-user-info').attr('data-socket-active');
    if (socketActive == 0 || !socketActive) {
        get_counter.getCount();
    }
});