var form_fields = "";
workflow_actions = {
    save_form_workspace : function(elements){
        $("body").on("click",elements,function(){
        var form_type = $(this).attr("data-form-type");
                
                switch (form_type) {
                        case "workflow":
                                /*
                                    VALIDATE NODE SETTINGS ON WORKFLOW
                                */
                                var json_encode = $("body").data();
                                var json_node = json_encode['nodes'];
                                var json_line = json_encode['lines'];
                                var hasError = false;
                                var workflow_status = $("#workspace_status").val();
                                //check if has nodes or lines
                                    if(json_haselement(json_node)== false || json_haselement(json_line)== false){
                                        hasError = true;
                                    }

                                var ctrEnd = 0;var ctrStart = 0;
                                //check if has correct node properties
                                for(var i in json_node){
                                    if(json_node[i]['type_rel']=="1"){
                                        //start
                                        var buttons = json_haselement(json_node[''+ i +'']['buttonStatus']);
                                        var default_action = json_node[''+ i +'']['workflow-default-action'];
                                        if(typeof json_node[''+ i +'']['buttonStatus'] == "undefined" || buttons == false){
                                            $("#"+json_node[''+ i +'']['node_data_id']).css("border-color","red");
                                            hasError = true;
                                        }
                                        if(typeof default_action == "undefined" || default_action == "0"){
                                            $("#"+json_node[''+ i +'']['node_data_id']).css("border-color","red");
                                            hasError = true;
                                        }
                                        ctrStart++;
                                    }else if(json_node[i]['type_rel']=="2"){
                                        //process
                                        var buttons = json_haselement(json_node[''+ i +'']['buttonStatus']);
                                        if (typeof json_node[''+ i +'']['processorType'] == "undefined" || json_node[''+ i +'']['processorType'] == "" || json_node[''+ i +'']['processorType'] == "0" ||
                                        typeof json_node[''+ i +'']['processor'] == "undefined" || json_node[''+ i +'']['processor'] == "" || json_node[''+ i +'']['processor'] == "0" ||
                                        typeof json_node[''+ i +'']['node_text'] == "undefined" || json_node[''+ i +'']['node_text'] == "" ||
                                        typeof json_node[''+ i +'']['status'] == "undefined" || json_node[''+ i +'']['status'] == "" /*||
                                        typeof json_node[''+ i +'']['buttonStatus'] == "undefined" || buttons == false*/){
                                            hasError = true;
                                            $("#"+json_node[''+ i +'']['node_data_id']).css("border-color","red");
                                        }
                                    }else if(json_node[i]['type_rel']=="3"){
                                        //condition
                                        var condition_return =  json_haselement(json_node[''+ i +'']['condition_return']);
                                        if(typeof json_node[''+ i +'']['field'] == "undefined" || json_node[''+ i +'']['field'] == "" ||
                                        typeof json_node[''+ i +'']['operator'] == "undefined" || json_node[''+ i +'']['operator'] == "" ||
                                        typeof json_node[''+ i +'']['field_value'] == "undefined" ||
                                        condition_return == false){
                                            hasError = true;
                                            $("#"+json_node[''+ i +'']['node_data_id']).css("border-color","red");
                                        }
                                        
                                    }else if(json_node[i]['type_rel']=="4"){
                                        //end
                                        if(typeof json_node[''+ i +'']['status'] == "undefined" || json_node[''+ i +'']['status'] == ""){
                                            hasError = true;
                                            $("#"+json_node[''+ i +'']['node_data_id']).css("border-color","red");
                                        }
                                        ctrEnd++;
                                    }else if(json_node[i]['type_rel']=="5"){
                                        //database node
                                        if(typeof json_node[''+ i +'']['workflow-external-trigger'] == "undefined"){
                                            hasError = true;
                                            $("#"+json_node[''+ i +'']['node_data_id']).find(".node").css("border","2px dotted red");
                                        }else{
                                            $("#"+json_node[''+ i +'']['node_data_id']).find(".node").css("border",false);
                                        }
                                    }
                                }
                                
                                // if status is "NOT" draft proceed with the validation
                                if(workflow_status!=2){
                                    if(hasError==true || ctrEnd==0 || ctrStart == 0){
                                       showNotification({
                                             message: "You have an error on your node settings",
                                             type: "error",
                                             autoClose: true,
                                             duration: 3
                                            });
                                       return false;
                                    }
                                }
                                // if(hasError==true || ctrEnd==0 || ctrStart == 0){
                                //    showNotification({
                                //          message: "You have an error on your node settings",
                                //          type: "error",
                                //          autoClose: true,
                                //          duration: 3
                                //         });
                                // }else{
                                    /*WORKFLOW SAVE*/
                                    var form_id = $("#form_id").val();
                                    json_encode['title'] = $("#workspace_title").val();
                                    json_encode['description'] = $("#workspace_description").val();
                                    json_encode['form_id'] = form_id;
                                    json_encode['width'] = $(".form_size_width").val();
                                    json_encode['height'] = $(".form_size_height").val();
                                    json_encode['template_form_size'] = $('.form_size').val(); // HERE NA
                                    json_encode['status'] = workflow_status;

                                    if (json_encode['width'] == "") {
                                        json_encode['width'] = $('.workspace.workflow_ws').width();
                                    }

                                    if (json_encode['height'] == "") {
                                        json_encode['height'] = $('.workspace.workflow_ws').height();
                                    }

                                    if(json_encode['title']==""){
                                        showNotification({
                                            message: "Please fill out the required fields",
                                            type: "error",
                                            autoClose: true,
                                            duration: 3
                                        });
                                        return;
                                    }
                                    //for (var i in json_encode['nodes']) {
                                    //            json_encode['nodes'][i]['node_onDrop'] = ""+json_encode['nodes'][i]['node_onDrop'];
                                    //}
                                    // = JSON.stringify(json_encode);
                                    json_encode = JSON.stringify(json_encode, function(key, val) {
                                                if (typeof val === 'function') {
                                                  return val + ''; // implicitly `toString` it
                                                }
                                                return val;
                                    })
                                    ui.block();
                                    $.post("/ajax/workflow",{action:"saveWorkflow",json_encode:json_encode},function(data){
                                         ui.unblock();
                                        //console.log(data)
                                        var json = JSON.parse(data);
                                         showNotification({
                                              message: "Your Workflow (" + json['title'] + ") was successfully saved.",
                                              type: "success",
                                              autoClose: true,
                                              duration: 3
                                         });
                                        // $("#popup_overlay,#popup_container").remove();
                                        
                                        bind_onBeforeOnload(0)
                                        var pathname = window.location.pathname;
                                        
                                        setTimeout(function(){
                                            if(pathname=="/user_view/workflow"){
                                                window.location="/user_view/workflow?view_type=edit&form_id="+ form_id +"&id="+json['id'];
                                            }else{
                                                window.location="/workflow?view_type=edit&form_id="+ form_id +"&id="+json['id'];
                                            }
                                        },2000)
                                        
                                    })
                                // }
                        break;
                }
            });
            
    },
    save_node_settings: function(callback){
        var self = this;
            
            $(".settings-to-json").click(function(e){
                var type = $(this).attr("object_type");
                var node_data_id =  $(this).attr("node-data-id");
                var json_nodes = $("body").data();
                var json_nodes_var = json_nodes['nodes'];
                var close_dialog = true;
                if (type == "organizational_chart") {
                    /*ORGCHART*/
                    //values from dialog
                    var orgchart_dept_name = $("#orgchart-dept-name").val();
                    var orgchart_dept_code = $("#orgchart-dept-code").val();
                    var orgchart_node_color = $("#orgchart-node-color").val();
                    var orgchart_dept_code_type = $("#orgchart_dept_code_type").val();
                    var this_dept_code_existing = json_nodes_var[''+ node_data_id +'']['orgchart_dept_code_existing'];

                    if (orgchart_dept_code_type==undefined) {
                        orgchart_dept_code_type = 1;
                    }

                    //if it does'nt have any existing department code.
                    if(this_dept_code_existing==undefined){
                        orgchart_dept_code_type = 0;
                    }
                    
                    var orgchart_user_head = self.head_id;
                    var orgchart_dept_assistant = self.assistantHead_id;
                    var orgchart_dept_members = self.members_id;
                    var workflow_status = $(".head_user:checked").closest(".orgchart_user_wrap").find(".avatar-oc").html();
                    var head_name = $(".head_user:checked").closest(".orgchart_user_wrap").find(".limit-text-wf").text();
                    if(orgchart_dept_name=="" || orgchart_user_head=="" || orgchart_dept_code == ""){
                        // alert("Please complete all required fields")
                        showNotification({
                             message: "Please complete all required fields",
                             type: "error",
                             autoClose: true,
                             duration: 3
                        });
                        return;
                    }
                    var isUnique = 0;
                    $.each(json_nodes_var,function(key, value){
                        if(value['orgchart_dept_code']==undefined){
                            return true;
                        }
                        if(value['node_data_id']!=node_data_id){
                            if(orgchart_dept_code_type==1){
                                if(value['orgchart_dept_code']==orgchart_dept_code){
                                    isUnique++;
                                    return false;
                                }
                            }
                        }
                    })
                    if(isUnique>0){
                        showNotification({
                             message: "Your department code has already been used in other department. Please Change your code",
                             type: "error",
                             autoClose: true,
                             duration: 5
                        });
                        return;
                    }
                    $("#"+json_nodes_var[''+ node_data_id +'']['node_data_id']).css("border-color","transparent");
                    //set value on json['nodes']
                    json_nodes_var[''+ node_data_id +'']['node_text'] = orgchart_dept_name;
                    json_nodes_var[''+ node_data_id +'']['orgchart_dept_code'] = orgchart_dept_code;
                    json_nodes_var[''+ node_data_id +'']['orgchart_dept_code_type'] = orgchart_dept_code_type;
                    json_nodes_var[''+ node_data_id +'']['node_color'] = orgchart_node_color;
                    json_nodes_var[''+ node_data_id +'']['orgchart_user_head'] = orgchart_user_head;
                    json_nodes_var[''+ node_data_id +'']['orgchart_dept_assistant'] = orgchart_dept_assistant;
                    json_nodes_var[''+ node_data_id +'']['orgchart_dept_members'] = orgchart_dept_members
                    json_nodes_var[''+ node_data_id +'']['status'] = workflow_status;

                      
                    if($("#"+node_data_id).hasClass("node-drag-wrapper")){
                        $("#" + node_data_id).find(".node-avatar-wrapper").html(workflow_status);
                        $("#" + node_data_id).find(".node-avatar-wrapper").find("img").attr({
                            "width" : "35",
                            "height" : ""
                        })
                        workflow_status = $("#" + node_data_id).find(".node-avatar-wrapper").html();
                        json_nodes_var[''+ node_data_id +'']['status'] = workflow_status;
                        json_nodes_var[''+ node_data_id +'']['head_name'] = head_name;
                        $("#node_status_" + node_data_id).html(head_name);
                        $("#"+node_data_id).find(".node-bg-color").css({
                            "background-color":orgchart_node_color
                        })
                    }else{
                        $("#node_status_" + node_data_id).html(workflow_status);
                        $("#node_process_" + node_data_id).css("background-color",orgchart_node_color);
                    }

                    // Change the properties of node
                    $("#node_title_" + node_data_id).html(orgchart_dept_name);
                    
                    
                    $(".node-other-text").find("img").css({
                        "z-index": "2",
                        "position": "relative"
                    })
                    $(".node-other-text").find("img").attr({
                        "width":"30",
                        "height":"30"
                    })
                            
                }else if(type == "workflow_chart-processor"){
                    /*WORKFLOW PROCESSSOR*/
                    //values from dialog
                    var workflow_processor_type = $("#workflow-processor-type").val();
                    var workflow_processor = $("#workflow-processor").val();
                    var workflow_processor_text = $("#workflow-processor :selected").text();
                    var workflow_status = $.trim($("#workflow-status").val());
                    var workflow_field_enabled = array_store(".fieldEnabled","checkbox");
                    var workflow_field_required = array_store(".fieldRequired","checkbox");
                    var workflow_field_hidden = array_store(".fieldHiddenValue","checkbox");
                    var workflow_field_read_only = array_store(".fieldReadOnly","checkbox");
                    var workflow_default_action = $("#workflow-default-action").val() 
                    var workflow_escalation_period = $("#workflow-escalation-period").val()
                    var workflow_notification_message_type = $(".fm-type:checked").val() 
                    var workflow_notification_message = $.trim($("#fm-message").val());
                    var workflow_notification_enabled = $("#enabled-notification").prop("checked");
                    var workflow_delegate_enabled = $("#workflow_delegate_enabled").prop("checked");
                    var workflow_guest_enabled = $("#workflow_guest_enabled").prop("checked");
                    var node_del_control = if_undefinded($("#node_del_control:checked").val(),"0");

                    var fieldEnabled_access_type = $("#access_type-field_enabled").val();
                    var fieldRequired_access_type = $("#access_type-field_required").val();
                    var fieldHiddenValue_access_type = $("#access_type-field_hidden").val();
                    //check if valid stat
                    if(CheckKeywordStatus.validateStatus(workflow_status)){
                        close_dialog = false;

                        showNotification({
                             message: CheckKeywordStatus.errorMessage,
                             type: "error",
                             autoClose: true,
                             duration: 5
                        });
                    }

                    //hide button formula
                    var workflow_hw_save = $("#hw-save").val()
                    var workflow_hw_cancel = $("#hw-cancel").val() 
                    //set value on json['nodes']
                    if(workflow_processor_type!="0" && workflow_processor!="0" && workflow_status!=""){
                        json_nodes_var[''+ node_data_id +'']['processorType'] = workflow_processor_type;
                        json_nodes_var[''+ node_data_id +'']['processor'] = workflow_processor;
                        json_nodes_var[''+ node_data_id +'']['node_text'] = workflow_processor_text;
                        json_nodes_var[''+ node_data_id +'']['status'] = workflow_status;
                        json_nodes_var[''+ node_data_id +'']['fieldEnabled'] = workflow_field_enabled;
                        json_nodes_var[''+ node_data_id +'']['fieldRequired'] = workflow_field_required;
                        json_nodes_var[''+ node_data_id +'']['fieldHiddenValue'] = workflow_field_hidden;
                        json_nodes_var[''+ node_data_id +'']['fieldEnabled_access_type'] = fieldEnabled_access_type;
                        json_nodes_var[''+ node_data_id +'']['fieldRequired_access_type'] = fieldRequired_access_type;
                        json_nodes_var[''+ node_data_id +'']['fieldHiddenValue_access_type'] = fieldHiddenValue_access_type;
                        json_nodes_var[''+ node_data_id +'']['fieldReadOnly'] = workflow_field_read_only;
                        json_nodes_var[''+ node_data_id +'']['workflow-default-action'] = workflow_default_action;
                        json_nodes_var[''+ node_data_id +'']['workflow-escalation-period'] = workflow_escalation_period;
                        json_nodes_var[''+ node_data_id +'']['workflow_notification_message_type'] = workflow_notification_message_type;
                        json_nodes_var[''+ node_data_id +'']['workflow_notification_enabled'] = workflow_notification_enabled;
                        json_nodes_var[''+ node_data_id +'']['workflow_delegate_enabled'] = workflow_delegate_enabled;
                        json_nodes_var[''+ node_data_id +'']['workflow_guest_enabled'] = workflow_guest_enabled;
                        json_nodes_var[''+ node_data_id +'']['workflow_notification_message'] = workflow_notification_message;
                        json_nodes_var[''+ node_data_id +'']['workflow_hw_save'] = workflow_hw_save;
                        json_nodes_var[''+ node_data_id +'']['workflow_hw_cancel'] = workflow_hw_cancel;
                        json_nodes_var[''+ node_data_id +'']['node_del_control'] = node_del_control;
                        
                        //
                    
                        if($("#"+node_data_id).hasClass("node-drag-wrapper")){
                            $("#"+node_data_id).find(".node-dept-wrapper").text(workflow_processor_text);
                            $("#"+node_data_id).find(".node-name-wrapper").text(workflow_status);
                        }else{
                            // Change Node Status on the selected node
                            $("#" + node_data_id).find(".node-title").html(workflow_processor_text);
                            $("#node_status_" + node_data_id).html(workflow_status);
                        }
                    }else{
                        close_dialog = false;
                        // alert("Please complete all required fields")
                        showNotification({
                             message: "Please complete all required fields",
                             type: "error",
                             autoClose: true,
                             duration: 3
                        });
                    }
                }else if (type == "workflow_chart-start") {
                            /*WORKFLOW START*/

                            //values from dialog
                            var workflow_field_enabled = array_store(".fieldEnabled","checkbox");
                            var workflow_field_required = array_store(".fieldRequired","checkbox");
                            var workflow_field_hidden = array_store(".fieldHiddenValue","checkbox"); 
                            var workflow_field_read_only = array_store(".fieldReadOnly","checkbox");
                            var workflow_default_action = $("#workflow-default-action").val() 
                            var workflow_escalation_period = $("#workflow-escalation-period").val()
                            var workflow_draft_button = $("#workflow_draft_button").val() 
                            var fieldEnabled_access_type = $("#access_type-field_enabled").val();
                            var fieldRequired_access_type = $("#access_type-field_required").val();
                            var fieldHiddenValue_access_type = $("#access_type-field_hidden").val();
                            
                            if(workflow_default_action=="0"){
                                showNotification({
                                    message: "Please complete all required fields",
                                    type: "error",
                                    autoClose: true,
                                    duration: 3
                               });
                                return;
                            }
                            
                            //set value on json['nodes']
                            json_nodes_var[''+ node_data_id +'']['fieldEnabled'] = workflow_field_enabled;
                            json_nodes_var[''+ node_data_id +'']['fieldRequired'] = workflow_field_required;
                            json_nodes_var[''+ node_data_id +'']['fieldHiddenValue'] = workflow_field_hidden;
                            json_nodes_var[''+ node_data_id +'']['fieldEnabled_access_type'] = fieldEnabled_access_type;
                            json_nodes_var[''+ node_data_id +'']['fieldRequired_access_type'] = fieldRequired_access_type;
                            json_nodes_var[''+ node_data_id +'']['fieldHiddenValue_access_type'] = fieldHiddenValue_access_type;
                            json_nodes_var[''+ node_data_id +'']['fieldReadOnly'] = workflow_field_read_only;
                            json_nodes_var[''+ node_data_id +'']['workflow-default-action'] = workflow_default_action;
                            json_nodes_var[''+ node_data_id +'']['workflow-escalation-period'] = workflow_escalation_period;
                            json_nodes_var[''+ node_data_id +'']['workflow_draft_button'] = workflow_draft_button;
                            
                }else if (type == "workflow_chart-condition") {
                    /*WORKFLOW CONDITION*/
                    //values from dialog
                    var wokflow_condition_fields = $(".wokflow_condition-fields").val();
                    var wokflow_condition_operator = $(".wokflow_condition-operator").val();
                    var wokflow_condition_value = $("#wokflow_condition-value").val();
                    var condition_status = wokflow_condition_fields+" "+wokflow_condition_operator+" "+wokflow_condition_value;
                    //set value on json['nodes']
                    json_nodes_var[''+ node_data_id +'']['field'] = wokflow_condition_fields;
                    json_nodes_var[''+ node_data_id +'']['operator'] = wokflow_condition_operator;
                    json_nodes_var[''+ node_data_id +'']['field_value'] = wokflow_condition_value;
                    json_nodes_var[''+ node_data_id +'']['status'] = condition_status;

                    if($("#"+node_data_id).hasClass("node-drag-wrapper")){
                        $("#" + node_data_id).find(".node-status-container").html(condition_status);
                        $("#" + node_data_id).find(".fl-value-cond").html(wokflow_condition_fields);
                        $("#" + node_data_id).find(".fl-operators-cond").html(wokflow_condition_operator);
                        $("#" + node_data_id).find(".fl-fieldsCondition-cond").html(wokflow_condition_value);
                    }else{
                        $("#node_status_" + node_data_id).html(condition_status);
                    }
                            
                }else if (type == "workflow_chart_buttons") {
                    /*WORKFLOW SET BUTTONS*/
                    var workflow_button = $(".workflow-buttons").val();
                    if(workflow_button==null){
                        workflow_button = $(".workflow-buttons :selected").val();
                    }
                    var workflow_require_comment = $("#wokflow_button-require").prop("checked");
                    var wokflow_button_line_color = $("#wokflow_button-line-color").val();
                    var child_id = $("#wokflow_button-action").attr("child_id");
                    var parent_id = $("#wokflow_button-action").attr("parent_id");
                    var msg_modalProcess = $.trim($("#msgModalProcess").val());
                    var message_success = $.trim($("#message_success").val());
                    var wf_landing_page = $.trim($("#node-landing-page").val());
                    // var workflow_field_enabled = array_store(".fieldEnabled","checkbox");
                    var workflow_field_required = array_store(".fieldRequired","checkbox");
                    // var workflow_field_hidden = array_store(".fieldHiddenValue","checkbox"); 
                    // var workflow_field_read_only = array_store(".fieldReadOnly","checkbox");


                    var line_class = parent_id+"_"+child_id;
                    if (workflow_button==0) {
                                close_dialog = false;
                                showNotification({
                                     message: "Please select button",
                                     type: "error",
                                     autoClose: true,
                                     duration: 3
                                });
                    }else if(msg_modalProcess==""){
                        close_dialog = false;
                        showNotification({
                             message: "Please set Warning Message",
                             type: "error",
                             autoClose: true,
                             duration: 3
                        });
                    }else if(message_success==""){
                        close_dialog = false;
                        showNotification({
                                     message: "Please set Success Message",
                                     type: "error",
                                     autoClose: true,
                                     duration: 3
                                });
                    }else{
                                var dis = $("[line-class='"+ parent_id +"_"+ child_id +"']");
                                deleteLine(dis);
                                //set value on json['nodes']
                                json_nodes_var[''+ node_data_id +'']['buttonStatus'] = (json_nodes_var[''+ node_data_id +'']['buttonStatus']  == null)?{}:json_nodes_var[''+ node_data_id +'']['buttonStatus'];
                                var buttons_json = {};
                                buttons_json['button_name'] = workflow_button;
                                buttons_json['child_id'] = child_id;
                                buttons_json['parent_id'] = parent_id;
                                buttons_json['require_comment'] = workflow_require_comment;
                                buttons_json['wokflow_button_line_color'] = wokflow_button_line_color;
                                buttons_json['message'] = msg_modalProcess;
                                buttons_json['message_success'] = message_success;
                                buttons_json['wf_landing_page'] = wf_landing_page;
                                // buttons_json['fieldEnabled'] = workflow_field_enabled;
                                buttons_json['fieldRequired'] = workflow_field_required;
                                // buttons_json['fieldHiddenValue'] = workflow_field_hidden;
                                // buttons_json['fieldReadOnly'] = workflow_field_read_only;

                                //console.log(buttons_json);
                                if($.type(json_nodes_var[''+ node_data_id +'']['buttonStatus'])=="array"){
                                    json_nodes_var[''+ node_data_id +'']['buttonStatus'] = {};
                                }
                                json_nodes_var[''+ node_data_id +'']['buttonStatus'][''+ workflow_button +''] = buttons_json;

                                
                                //callback
                                callback(wokflow_button_line_color,workflow_button);     

                    }
                    
                }else if (type == "workflow_chart_condition-line") {
                    var wokflow_condition_line_color = $("#wokflow_condition-line-color").val();
                    var wokflow_condition_return_boolean = $(".wokflow_condition-return-boolean:checked").val();
                    var child_id = $("#wokflow_condition-action").attr("child_id");
                    var parent_id = $("#wokflow_condition-action").attr("parent_id");
                    var line_class = parent_id+"_"+child_id;
                    if (typeof wokflow_condition_return_boolean =="undefined") {
                                //code
                                close_dialog = false;
                                // alert("select return");
                                showNotification({
                                     message: "Please select return value",
                                     type: "error",
                                     autoClose: true,
                                     duration: 3
                                });
                    }else{
                                //var wokflow_condition_line_color = $("#wokflow_condition-line-color").val();
                                json_nodes_var[''+ node_data_id +'']['condition_return'] = (json_nodes_var[''+ node_data_id +'']['condition_return']  == undefined)?{}:json_nodes_var[''+ node_data_id +'']['condition_return'];
                                var buttons_json = {};
                                buttons_json['child_id'] = child_id;
                                buttons_json['parent_id'] = parent_id;
                                buttons_json['wokflow_condition_return_boolean'] = wokflow_condition_return_boolean;
                                json_nodes_var[''+ node_data_id +'']['condition_return'][''+ wokflow_condition_return_boolean +''] = buttons_json;
                                

                                callback(wokflow_condition_line_color,wokflow_condition_return_boolean);
                                customTextLine(line_class,wokflow_condition_return_boolean);
                                updateLineLabel(line_class,wokflow_condition_return_boolean)
                    }
                            
                }else if (type == "workflow_chart-end") {
                    var workflow_end_status  = $.trim($("#workflow_end-status").val());
                    var workflow_field_hidden = array_store(".fieldHiddenValue","checkbox");
                    var workflow_notification_message_type = $(".fm-type:checked").val() 
                    var workflow_notification_message = $("#fm-message").val() 
                    var workflow_notification_enabled = $("#enabled-notification").prop("checked");
                    var node_del_control = if_undefinded($("#node_del_control:checked").val(),"0");
                    var fieldHiddenValue_access_type = $("#access_type-field_hidden").val();
                    //check if valid status
                    if(CheckKeywordStatus.validateStatus(workflow_end_status)){
                        close_dialog = false;

                        showNotification({
                             message: CheckKeywordStatus.errorMessage,
                             type: "error",
                             autoClose: true,
                             duration: 5
                        });
                    }

                    if(workflow_end_status!=""){
                        json_nodes_var[''+ node_data_id +'']['status'] = workflow_end_status;
                        json_nodes_var[''+ node_data_id +'']['workflow_notification_message_type'] = workflow_notification_message_type;
                        json_nodes_var[''+ node_data_id +'']['workflow_notification_message'] = workflow_notification_message;
                        json_nodes_var[''+ node_data_id +'']['workflow_notification_enabled'] = workflow_notification_enabled;
                        json_nodes_var[''+ node_data_id +'']['fieldHiddenValue_access_type'] = fieldHiddenValue_access_type;
                        json_nodes_var[''+ node_data_id +'']['fieldHiddenValue'] = workflow_field_hidden;
                        json_nodes_var[''+ node_data_id +'']['node_del_control'] = node_del_control;

                    }else{
                        close_dialog = false;
                        // alert("Please complete all required fields")
                        showNotification({
                             message: "Please complete all required fields",
                             type: "error",
                             autoClose: true,
                             duration: 3
                        });
                        return;
                    }
                  // Change Node Status on the selected node
                  if($("#"+node_data_id).hasClass("node-drag-wrapper")){
                    $("#" + node_data_id).find(".node-circle-status").text(workflow_end_status); 
                  }else{
                    $("#node_status_" + node_data_id).html(workflow_end_status); 
                  }
                  
                   
                }else if(type == "workflow_chart-email"){
                    var workflow_email = {};
                    var workflow_sms = {};
                    var cl_workflow_email = {};


                    //email in processor
                    {
                        //start to
                        var email_to = {};
                        var requestor_to = $(".email-to").find(".email-requestor").prop("checked");
                        var processor_to = $(".email-to").find(".email-processor").prop("checked");

                        var otherRecepient_type_to = $(".email-to").find(".workflow-mail-otherRecepient-type:checked").val();
                        var otherRecepient_to = $(".email-to").find("#workflow-mail-otherRecepient_to").val();
                        var users_to = WorkflowTrigger.setCheckedToJson(".email-to");

                        email_to = {
                            "requestor" : requestor_to,
                            "processor" : processor_to,
                            "otherRecepient_type" : otherRecepient_type_to,
                            "otherRecepient" : otherRecepient_to,
                        };
                        email_to = $.extend(users_to,email_to);
                        //end to
                        //start cc
                        var email_cc = {};

                        var requestor_cc = $(".email-cc").find(".email-requestor").prop("checked");
                        var processor_cc = $(".email-cc").find(".email-processor").prop("checked");
                        var otherRecepient_type_cc = $(".email-cc").find(".workflow-mail-otherRecepient-type:checked").val();
                        var otherRecepient_cc = $(".email-cc").find("#workflow-mail-otherRecepient_cc").val();
                        var users_cc = WorkflowTrigger.setCheckedToJson(".email-cc");

                        email_cc = {
                            "requestor" : requestor_cc,
                            "processor" : processor_cc,
                            "otherRecepient_type" : otherRecepient_type_cc,
                            "otherRecepient" : otherRecepient_cc,
                        };
                        email_cc = $.extend(users_cc,email_cc);

                        //end cc

                        //start bcc
                        var email_bcc = {};

                        var requestor_bcc = $(".email-bcc").find(".email-requestor").prop("checked");
                        var processor_bcc = $(".email-bcc").find(".email-processor").prop("checked");
                        var otherRecepient_type_bcc = $(".email-bcc").find(".workflow-mail-otherRecepient-type:checked").val();
                        var otherRecepient_bcc = $(".email-bcc").find("#workflow-mail-otherRecepient_bcc").val();
                        var users_bcc = WorkflowTrigger.setCheckedToJson(".email-bcc");

                        email_bcc = {
                            "requestor" : requestor_bcc,
                            "processor" : processor_bcc,
                            "otherRecepient_type" : otherRecepient_type_bcc,
                            "otherRecepient" : otherRecepient_bcc,
                        };
                        email_bcc = $.extend(users_bcc,email_bcc);
                        //end bcc

                        //title and message

                        //push to json
                        var email_title = $("#workflow-email-title").val();
                        var title_type = $(".workflow-email-title-type:checked").val();
                        var email_message = $("#workflow-email-message").val();
                        var email_message_type = $(".workflow-email-message-type:checked").val();
                        var incRequesLog = if_undefinded($("#incRequesLog:checked").val(),"0");

                        workflow_email = {
                            "email_recpient" : email_to,
                            "email_cc"  : email_cc,
                            "email_bcc" : email_bcc,
                            "title"     : email_title,
                            "requestLog": incRequesLog,
                            "email-title-type": title_type,
                            "message"   : email_message,
                            "email-message-type" :email_message_type

                        };
                    }

                    //CANCEL Email Notification
                    {
                        //start to
                        var email_to = {};
                        var requestor_to = $(".cl-email-to").find(".cl-email-requestor").prop("checked");
                        var processor_to = $(".cl-email-to").find(".cl-email-processor").prop("checked");

                        var otherRecepient_type_to = $(".cl-email-to").find(".cl-workflow-mail-otherRecepient-type:checked").val();
                        var otherRecepient_to = $(".cl-email-to").find("#cl-workflow-mail-otherRecepient_to").val();
                        var users_to = WorkflowTrigger.setCheckedToJson(".cl-email-to");

                        email_to = {
                            "requestor" : requestor_to,
                            "processor" : processor_to,
                            "otherRecepient_type" : otherRecepient_type_to,
                            "otherRecepient" : otherRecepient_to,
                        };
                        email_to = $.extend(users_to,email_to);
                        //end to
                        //start cc
                        var email_cc = {};

                        var requestor_cc = $(".cl-email-cc").find(".cl-email-requestor").prop("checked");
                        var processor_cc = $(".cl-email-cc").find(".cl-email-processor").prop("checked");
                        var otherRecepient_type_cc = $(".cl-email-cc").find(".cl-workflow-mail-otherRecepient-type:checked").val();
                        var otherRecepient_cc = $(".cl-email-cc").find("#cl-workflow-mail-otherRecepient_cc").val();
                        var users_cc = WorkflowTrigger.setCheckedToJson(".cl-email-cc");

                        email_cc = {
                            "requestor" : requestor_cc,
                            "processor" : processor_cc,
                            "otherRecepient_type" : otherRecepient_type_cc,
                            "otherRecepient" : otherRecepient_cc,
                        };
                        email_cc = $.extend(users_cc,email_cc);

                        //end cc

                        //start bcc
                        var email_bcc = {};

                        var requestor_bcc = $(".cl-email-bcc").find(".cl-email-requestor").prop("checked");
                        var processor_bcc = $(".cl-email-bcc").find(".cl-email-processor").prop("checked");
                        var otherRecepient_type_bcc = $(".cl-email-bcc").find(".cl-workflow-mail-otherRecepient-type:checked").val();
                        var otherRecepient_bcc = $(".cl-email-bcc").find("#cl-workflow-mail-otherRecepient_bcc").val();
                        var users_bcc = WorkflowTrigger.setCheckedToJson(".cl-email-bcc");

                        email_bcc = {
                            "requestor" : requestor_bcc,
                            "processor" : processor_bcc,
                            "otherRecepient_type" : otherRecepient_type_bcc,
                            "otherRecepient" : otherRecepient_bcc,
                        };
                        email_bcc = $.extend(users_bcc,email_bcc);
                        //end bcc

                        //title and message

                        //push to json
                        var email_title = $("#cl-workflow-email-title").val();
                        var title_type = $(".cl-workflow-email-title-type:checked").val();
                        var email_message = $("#cl-workflow-email-message").val();
                        var email_message_type = $(".cl-workflow-email-message-type:checked").val();
                        var incRequesLog = if_undefinded($("#cl-incRequesLog:checked").val(),"0");

                        cl_workflow_email = {
                            "email_recpient" : email_to,
                            "email_cc"  : email_cc,
                            "email_bcc" : email_bcc,
                            "title"     : email_title,
                            "requestLog": incRequesLog,
                            "email-title-type": title_type,
                            "message"   : email_message,
                            "email-message-type" :email_message_type

                        };

                        json_nodes_var[''+ node_data_id +'']['cl_workflow_email'] = cl_workflow_email;
                    }

                    //sms
                    {
                        var requestor_sms = $(".sms-users").find(".sms-requestor").prop("checked");
                        var processor_sms = $(".sms-users").find(".sms-processor").prop("checked");
                        var users_sms = WorkflowTrigger.setCheckedToJson(".sms-users");
                        var sms_contact = $("#workflow-sms-contact").val();
                        var sms_message = $("#workflow-sms-message").val();
                        var sms_message_type = $(".workflow-sms-message-type:checked").val();
                        var sms_recipient_type = $(".workflow-sms-recipient-type:checked").val();
                        workflow_sms = {
                            "requestor" : requestor_sms,
                            "processor" : processor_sms,
                            "contact"   : sms_contact,
                            "message"   : sms_message,
                            "message-type" : sms_message_type,
                            "recipient-type": sms_recipient_type
                        };
                        workflow_sms = $.extend(users_sms,workflow_sms);

                        json_nodes_var[''+ node_data_id +'']['workflow_email'] = workflow_email;
                        json_nodes_var[''+ node_data_id +'']['workflow_sms'] = workflow_sms;
                        
                    }
                // }else if(type=="workflow_chart-fields-trigger"){
                //     //values from dialog
                //     //set value on json['nodes']
                //     var workflow_trigger_action = $(".workflow-trigger-action:checked").val()
                //     if(workflow_trigger_action=="Create"){
                //         json_nodes_var[''+ node_data_id +'']['workflow-form-trigger'] = $("#workflow-form-trigger").val();
                //         json_nodes_var[''+ node_data_id +'']['workflow-reference-fields-update'] = $(".workflow-reference-fields-update").val();
                //     }else{
                        
                //         json_nodes_var[''+ node_data_id +'']['workflow-trigger-fields-update'] = $("#workflow-trigger-fields-update").val();
                //         json_nodes_var[''+ node_data_id +'']['workflow-trigger-fields-filter'] = $("#workflow-trigger-fields-filter").val();
                //         json_nodes_var[''+ node_data_id +'']['workflow-trigger-operator'] = $("#workflow-trigger-operator").val();
                //     }
                //     json_nodes_var[''+ node_data_id +'']['workflow-trigger-action'] = workflow_trigger_action;
                // }
                //multi
                }else if(type=="workflow_chart-fields-trigger"){
                    var json_field_trigger = [];
                    var json_trigger = {};
                    var triggers = $(".trigger-container-fields");
                    var error = 0;
                    $(".trigger-container-fields").each(function(i){
                        if($(this).find(".workflow-form-trigger").val()==0 && $(this).find(".workflow-trigger-action-class:checked").val()!="Post Submit"){
                            error++;
                            return true;
                        }
                        if($(this).find(".workflow-trigger-action-class:checked").val()=="Create" || $(this).find(".workflow-trigger-action-class:checked").val()=="Update Response" || $(this).find(".workflow-trigger-action-class:checked").val()=="Post Submit"){
                            if($(this).find(".workflow-form-trigger").val()!=0 || $(this).find(".workflow-trigger-action-class:checked").val()=="Post Submit"){
                                var fields_formula = [];

                                if($(this).find(".workflow-trigger-action-class:checked").val()=="Post Submit"){
                                    json_field_trigger.push({
                                        "trigger-index":$(this).attr("trigger-index"),
                                        "workflow-trigger-action":$(this).find(".workflow-trigger-action-class:checked").val(),
                                        //"workflow-form-trigger":$(this).find(".workflow-form-trigger").val(),
                                        "workflow-field_formula":fields_formula,
                                        "workflow-ref-field" : "",
                                        "workflow-ref-field-parent":"",
                                        "workflow-postSubmit-action":$(this).find(".workflow-postSubmit-action").val()
                                    });
                                }else{
                                    $(this).find(".fields_formula_row").each(function(){
                                        if($(this).find(".workflow-reference-fields-update").val()==0 && $(this).css("display")!="none"){
                                            error++;
                                            return true;
                                        }
                                        fields_formula.push({
                                            "workflow-field-update" : $(this).find(".workflow-reference-fields-update").val(),
                                            "workflow-field-id" : $(this).find(".workflow-reference-fields-update").attr("id"),
                                            "workflow-field-formula" : $(this).find(".workflow-reference-fields-formula").val(),
                                        })
                                    })
                                    if($(this).find(".workflow-ref-field-parent").val()==0 && $(this).find(".workflow-trigger-action-class:checked").val()=="Update Response"){
                                        error++;
                                    }
                                    json_field_trigger.push({
                                        "trigger-index":$(this).attr("trigger-index"),
                                        "workflow-trigger-action":$(this).find(".workflow-trigger-action-class:checked").val(),
                                        "workflow-form-trigger":$(this).find(".workflow-form-trigger").val(),
                                        "workflow-field_formula":fields_formula,
                                        "workflow-ref-field" : $(this).find(".workflow-ref-field").val(),
                                        "workflow-ref-field-parent":$(this).find(".workflow-ref-field-parent").val(),
                                    });
                                }
                                
                                json_trigger = json_field_trigger;//json_nodes_var[''+ node_data_id +'']['workflow-trigger'] = json_field_trigger;
                            }
                        }else{
                            json_field_trigger.push({
                                "trigger-index":$(this).attr("trigger-index"),
                                "workflow-trigger-action":$(this).find(".workflow-trigger-action-class:checked").val(),
                                "workflow-trigger-fields-update":$(this).find(".workflow-trigger-fields-update").val(),
                                "workflow-trigger-fields-filter":$(this).find(".workflow-trigger-fields-filter").val(),
                                "workflow-trigger-operator":$(this).find(".workflow-trigger-operator").val()
                            });
                            json_trigger = json_field_trigger;//json_nodes_var[''+ node_data_id +'']['workflow-trigger'] = json_field_trigger;
                        }
                    })
                    var validatedOne = 0;
                    if(triggers.length==1){
                        if(triggers.find(".workflow-form-trigger").val()==0 && triggers.find(".workflow-trigger-action-class:checked").val()!="Post Submit"){
                            // delete json_nodes_var[''+ node_data_id +'']['workflow-trigger'];
                            validatedOne++;
                            error = 0;
                        }
                    }
                    if(error>0){
                        showNotification({
                             message: "Please fill out the required fields",
                             type: "error",
                             autoClose: true,
                             duration: 3
                        });
                        return;
                    }else{
                        if(validatedOne==0){
                            json_nodes_var[''+ node_data_id +'']['workflow-trigger'] = json_trigger;
                        }else{
                            try{
                                delete json_nodes_var[''+ node_data_id +'']['workflow-trigger'];
                            }catch(err){
                                consoole.log(err);
                            }
                        }
                    }
                }else if(type=="workflow_chart-database"){
                    var json_trigger = {};
                    var triggers = $(".external_db_triggers .trigger-container-fields");
                    var error = 0;
                    $(".external_db_triggers").each(function(){
                        var trigger_type = $(this).attr("trigger_type");
                        var json_field_trigger = [];

                        $(this).find(".trigger-container-fields").each(function(i){
                            if(($(this).find(".workflow-form-trigger").val()=="0" || $(this).find(".workflow-db-connection").val()=="0") && $(this).find(".workflow-trigger-action-class:checked").val()!="Post Submit"){
                                error++;
                                return true;
                            }
                            if($(this).find(".workflow-trigger-action-class:checked").val()=="Create" || $(this).find(".workflow-trigger-action-class:checked").val()=="Update Response" || $(this).find(".workflow-trigger-action-class:checked").val()=="Post Submit"){
                                if($(this).find(".workflow-form-trigger").val()!=0 || $(this).find(".workflow-trigger-action-class:checked").val()=="Post Submit"){
                                    var fields_formula = [];

                                    if($(this).find(".workflow-trigger-action-class:checked").val()=="Post Submit"){
                                        json_field_trigger.push({
                                            "trigger-index":$(this).attr("trigger-index"),
                                            "workflow-trigger-action":$(this).find(".workflow-trigger-action-class:checked").val(),
                                            //"workflow-form-trigger":$(this).find(".workflow-form-trigger").val(),
                                            "workflow-field_formula":fields_formula,
                                            "workflow-ref-field" : "",
                                            "workflow-ref-field-parent":"",
                                            "workflow-postSubmit-action":$(this).find(".workflow-postSubmit-action").val()
                                        });
                                    }else{
                                        $(this).find(".fields_formula_row").each(function(){
                                            if($(this).find(".workflow-reference-fields-update").val()==0 && $(this).css("display")!="none"){
                                                error++;
                                                return true;
                                            }
                                            fields_formula.push({
                                                "workflow-field-update" : $(this).find(".workflow-reference-fields-update").val(),
                                                "workflow-field-id" : $(this).find(".workflow-reference-fields-update").attr("id"),
                                                "workflow-field-formula" : $(this).find(".workflow-reference-fields-formula").val(),
                                            })
                                        })
                                        if($(this).find(".workflow-ref-field-parent").val()==0 && $(this).find(".workflow-trigger-action-class:checked").val()=="Update Response"){
                                            error++;
                                        }
                                        json_field_trigger.push({
                                            "trigger-index":$(this).attr("trigger-index"),
                                            "workflow-trigger-action":$(this).find(".workflow-trigger-action-class:checked").val(),
                                            "workflow-form-trigger":$(this).find(".workflow-form-trigger").val(),
                                            "workflow-field_formula":fields_formula,
                                            "workflow-ref-field" : $(this).find(".workflow-ref-field").val(),
                                            "workflow-ref-field-parent":$(this).find(".workflow-ref-field-parent").val(),
                                            "workflow-db-connection":$(this).find(".workflow-db-connection").val(),
                                        });
                                    }
                                    
                                    json_trigger[''+ trigger_type +''] = json_field_trigger;//json_nodes_var[''+ node_data_id +'']['workflow-trigger'] = json_field_trigger;
                                }
                            }else{
                                json_field_trigger.push({
                                    "trigger-index":$(this).attr("trigger-index"),
                                    "workflow-trigger-action":$(this).find(".workflow-trigger-action-class:checked").val(),
                                    "workflow-trigger-fields-update":$(this).find(".workflow-trigger-fields-update").val(),
                                    "workflow-trigger-fields-filter":$(this).find(".workflow-trigger-fields-filter").val(),
                                    "workflow-trigger-operator":$(this).find(".workflow-trigger-operator").val()
                                });
                                json_trigger[''+ trigger_type +''] = json_field_trigger;//json_nodes_var[''+ node_data_id +'']['workflow-trigger'] = json_field_trigger;
                            }
                        })
                    })


                    console.log("current_errors", error);
                    //validation and saving
                    var validatedOne = 0;
                    if(triggers.length==1){
                        if(triggers.find(".workflow-db-connection").val()==0 && triggers.find(".workflow-form-trigger").val()==0 && triggers.find(".workflow-trigger-action-class:checked").val()!="Post Submit"){
                            // delete json_nodes_var[''+ node_data_id +'']['workflow-trigger'];
                            validatedOne++;
                        }
                    }
                    console.log("final error coutnt:", error);
                    if(error>0){
                        showNotification({
                             message: "Please fill out the required fields",
                             type: "error",
                             autoClose: true,
                             duration: 3
                        });
                        return;
                    }
                    if(validatedOne==0){
                        json_nodes_var[''+ node_data_id +'']['workflow-external-trigger'] = json_trigger;
                    }else{
                        try{
                            delete json_nodes_var[''+ node_data_id +'']['workflow-external-trigger'];
                        }catch(err){
                            consoole.log(err);
                        }
                    }
                                            {
                                                // if(error>0){
                                                //     showNotification({
                                                //          message: "Please fill out the required fields",
                                                //          type: "error",
                                                //          autoClose: true,
                                                //          duration: 3
                                                //     });
                                                //     return;
                                                // }else{
                                                //     if(validatedOne==0){
                                                //         json_nodes_var[''+ node_data_id +'']['workflow-external-trigger'] = json_trigger;
                                                //     }else{
                                                //         try{
                                                //             delete json_nodes_var[''+ node_data_id +'']['workflow-external-trigger'];
                                                //         }catch(err){
                                                //             consoole.log(err);
                                                //         }
                                                //     }
                                                // }
                                            }
                }else if(type=="workflow_chart-user_setup"){
                    var users = {};
                    var user_requestor = $(".users-editor").find(".user-requestor").prop("checked");
                    var user_processor = $(".users-editor").find(".user-processor").prop("checked");
                    users_editorSpecific = WorkflowTrigger.setCheckedToJson(".users-editor");
                    var user_editor = {
                        "requestor" : user_requestor,
                        "processor" : user_processor,
                    };
                    var user_viewer = {};
                    var users_viewerSpecific = WorkflowTrigger.setCheckedToJson(".users-viewer");
                    user_editor = $.extend(user_editor,users_editorSpecific);
                    user_viewer = $.extend(user_viewer,users_viewerSpecific);
                    users = {
                        "user_editor": user_editor,
                        "user_viewer": user_viewer
                    }
                    json_nodes_var[''+ node_data_id +'']['users'] = users;

                }
                
                if(close_dialog==true){
                            //set json on body
                            $("body").data(json_nodes);
                            // console.log($("body").data())
                            //remove dialog
                            $('.fl-closeDialog').click();
                            //$("#popup_overlay,#popup_container").remove();
                            //callback
                }
                        
            });
    },
    head_id : [],
    assistantHead_id : [],
    members_id : [],
    setMembers : function(){
        var self = this;
        self.head_id = array_store(".head_user","checkbox");
        self.assistantHead_id = array_store(".asst_head_user","checkbox");
        self.members_id = array_store(".member_user","checkbox");
        

    },
    setMembersFunction : function(){ 
        var self = this;
        var index_arr = "";
        $("body").on("click",".member_user",function(){
            var value = $(this).val();
            if($(this).prop("checked")){
                self.members_id.push(value);
            }else{
                index_arr = self.members_id.indexOf(value);
                self.members_id.splice(index_arr,1);
            }
            $(".orgchart-user-search").data({
                "members_id":self.members_id
            })
        })
        $("body").on("click",".head_user",function(){
            var value = $(this).val();
            self.head_id = [];
            self.head_id.push(value);
        })
        $("body").on("click",".asst_head_user",function(){
            var value = $(this).val();
            self.assistantHead_id = [];
            self.assistantHead_id.push(value);
        })
    },
    
    save_node_mail : function(node_data_id){
        var json = $("body").data();
        ui.block();
       var newDialog = new jDialog(object_properties_node("workflow_chart-email",node_data_id,""), "","700", "", "30", function(){});
        //$('#popup_container').addClass('bounceIn');
        newDialog.themeDialog("modal2");
        converBreakLineTextbox("#workflow-sms-message");
        converBreakLineTextbox("#workflow-email-message");
        limitText(".limit-text-wf",20); // set limit of text in the name   
        // $(".wfSettings").mention_post({
        //             json : formulaJson, // from local file fomula.json
        //             highlightClass:"",
        //             prefix : "",
        //             countDisplayChoice : "4"
        //         });


        $(".tip").tooltip(); 
        $( "#accordion" ).accordion({
            heightStyle: "fill",
            activate: function( event, ui ) {
                // $('.content-dialog-scroll').perfectScrollbar()
                // $(".ps-scrollbar-x").hide();    
                //alert(1)
                // $('.content-dialog-scroll').perfectScrollbar('destroy');
                // $('.content-dialog-scroll').perfectScrollbar()
                // $(".ps-scrollbar-x").hide();    
                $('.content-dialog-scroll').perfectScrollbar('update');
            }
        });
        $( "#cl-accordion" ).accordion({
            heightStyle: "fill",
            activate: function( event, ui ) {
                // $('.content-dialog-scroll').perfectScrollbar()
                // $(".ps-scrollbar-x").hide();    
                //alert(1)
                // $('.content-dialog-scroll').perfectScrollbar('destroy');
                // $('.content-dialog-scroll').perfectScrollbar()
                // $(".ps-scrollbar-x").hide();    
                $('.content-dialog-scroll').perfectScrollbar('update');
            }
        });
        // $( "#accordion-container" ).accordion({
        //     heightStyle: "fill",
        //     activate: function( event, ui ) {
        //         // $('.content-dialog-scroll').perfectScrollbar()
        //         // $(".ps-scrollbar-x").hide();    
        //         //alert(1)
        //         // $('.content-dialog-scroll').perfectScrollbar('destroy');
        //         // $('.content-dialog-scroll').perfectScrollbar()
        //         // $(".ps-scrollbar-x").hide();    
        //         $('.content-dialog-scroll').perfectScrollbar('update');
        //         $('.content-dialog-scroll-container').perfectScrollbar('update');
        //     }
        // });
        $( "#accordion-container" ).tabs({
            activate : function(){
                $('.content-dialog-scroll').perfectScrollbar('update');
                $('.content-dialog-scroll-container').perfectScrollbar('update');
            }
        })
        $(".ui-accordion-content").css({
            "height":"auto",
            "font-size":"11px",
        })
        this.save_node_settings()
        //object_properties_node(object_type,parent_id,child_id)

        //set json to fields
        var json_nodes = $("body").data();
        var json_nodes_var = json_nodes['nodes'];

        var workflow_email  = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow_email'],"");
        var workflow_sms  = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow_sms'],"");

        if(workflow_email){
            //to
            var to = workflow_email['email_recpient'];
            $(".email-to").find(".email-requestor").prop("checked",to['requestor']);
            $(".email-to").find(".email-processor").prop("checked",to['processor']);
            $(".email-to").find(".workflow-mail-otherRecepient-type[value='"+ to['otherRecepient_type'] +"']").prop("checked",true);

            RequestPivacy.setCheckAllChecked($("#fp-allUsers5"));
            //cc
            var cc = workflow_email['email_cc'];
            $(".email-cc").find(".email-requestor").prop("checked",cc['requestor']);
            $(".email-cc").find(".email-processor").prop("checked",cc['processor']);
            $(".email-cc").find(".workflow-mail-otherRecepient-type[value='"+ cc['otherRecepient_type'] +"']").prop("checked",true);
            RequestPivacy.setCheckAllChecked($("#fp-allUsers6"));
            //bcc
            var bcc = workflow_email['email_bcc'];
            $(".email-bcc").find(".email-requestor").prop("checked",bcc['requestor']);
            $(".email-bcc").find(".email-processor").prop("checked",bcc['processor']);  
            $(".email-bcc").find(".workflow-mail-otherRecepient-type[value='"+ bcc['otherRecepient_type'] +"']").prop("checked",true);          
            RequestPivacy.setCheckAllChecked($("#fp-allUsers1"));


            if(workflow_email['email-title-type']=="1"){
                // $("#workflow-email-title").mention_post({
                //     json : formulaJson, // from local file fomula.json
                //     highlightClass:"",
                //     prefix : "",
                //     countDisplayChoice : "4"
                // });
            }

            if(workflow_email['email-message-type']=="1"){
                // $("#workflow-email-message").mention_post({
                //     json : formulaJson, // from local file fomula.json
                //     highlightClass:"",
                //     prefix : "",
                //     countDisplayChoice : "4"
                // });
            }

        }

        // CANCEL Email Notification
        var cl_workflow_email  = if_undefinded(json_nodes_var[''+ node_data_id +'']['cl_workflow_email'],"");

        //remove cancel mail settings in end node
        if(json_nodes_var[''+ node_data_id +'']['type_rel']=="4"){
            $(".cl-email").remove();
//            $(".wf-next-processor").closest("div").remove();
        }
        if(cl_workflow_email){
            //to
            var cl_to = cl_workflow_email['email_recpient'];
            console.log($(".cl-email-to").find(".cl-email-requestor"))
            $(".cl-email-to").find(".cl-email-requestor").prop("checked",cl_to['requestor']);
            $(".cl-email-to").find(".cl-email-processor").prop("checked",cl_to['processor']);
            $(".cl-email-to").find(".cl-workflow-mail-otherRecepient-type[value='"+ cl_to['otherRecepient_type'] +"']").prop("checked",true);

            RequestPivacy.setCheckAllChecked($("#cl-fp-allUsers5"));
            //cc
            var cl_cc = cl_workflow_email['email_cc'];
            $(".cl-email-cc").find(".cl-email-requestor").prop("checked",cl_cc['requestor']);
            $(".cl-email-cc").find(".cl-email-processor").prop("checked",cl_cc['processor']);
            $(".cl-email-cc").find(".cl-workflow-mail-otherRecepient-type[value='"+ cl_cc['otherRecepient_type'] +"']").prop("checked",true);
            RequestPivacy.setCheckAllChecked($("#cl-fp-allUsers6"));
            //bcc
            var cl_bcc = cl_workflow_email['email_bcc'];
            $(".cl-email-bcc").find(".cl-email-requestor").prop("checked",cl_bcc['requestor']);
            $(".cl-email-bcc").find(".cl-email-processor").prop("checked",cl_bcc['processor']);  
            $(".cl-email-bcc").find(".cl-workflow-mail-otherRecepient-type[value='"+ cl_bcc['otherRecepient_type'] +"']").prop("checked",true);          
            RequestPivacy.setCheckAllChecked($("#cl-fp-allUsers1"));


            if(cl_workflow_email['email-title-type']=="1"){
                // $("#cl-workflow-email-title").mention_post({
                //     json : formulaJson, // from local file fomula.json
                //     highlightClass:"",
                //     prefix : "",
                //     countDisplayChoice : "4"
                // });
            }

            if(cl_workflow_email['email-message-type']=="1"){
                // $("#cl-workflow-email-message").mention_post({
                //     json : formulaJson, // from local file fomula.json
                //     highlightClass:"",
                //     prefix : "",
                //     countDisplayChoice : "4"
                // });
            }

        }

        if(workflow_sms){
            //sms
            $(".sms-users").find(".sms-requestor").prop("checked",workflow_sms['requestor']);
            $(".sms-users").find(".sms-processor").prop("checked",workflow_sms['processor']);         
            RequestPivacy.setCheckAllChecked($("#fp-allUsers4"));

            if(workflow_sms['message-type']=="1"){
                // $("#workflow-sms-message").mention_post({
                //     json : formulaJson, // from local file fomula.json
                //     highlightClass:"",
                //     prefix : "",
                //     countDisplayChoice : "4"
                // });
            }
            var recipient_type = if_undefinded(workflow_sms['recipient-type'],"0");
            if(recipient_type=="1"){
                // $("#workflow-sms-contact").mention_post({
                //     json : formulaJson, // from local file fomula.json
                //     highlightClass:"",
                //     prefix : "",
                //     countDisplayChoice : "4"
                // });
            }

        }
        TriggerAutoComplete.init();
        ui.unblock();
    },
    // field_trigger_settings : function(node_data_id){
    //     var json = $("body").data();
    //     var json_nodes_var = json['nodes'];
    //     jDialog(object_properties_node("workflow_chart-fields-trigger",node_data_id,"field-trigger"), "","650", "200", "", function(){});


    //     var workflow_trigger_action = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-trigger-action'],"");
    //     var workflow_trigger_fields_update = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-trigger-fields-update'],"");
    //     var workflow_trigger_fields_filter = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-trigger-fields-filter'],"");
    //     var workflow_trigger_operator = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-trigger-operator'],"");

    //     var workflow_form_trigger = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-form-trigger'],"");
    //     var workflow_reference_fields_update = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-reference-fields-update'],"");
    //     // setting up values
        
    //     if(workflow_trigger_action){
    //         $(".workflow-trigger-action[value='"+ workflow_trigger_action +"']").attr("checked","checked");
    //         if(workflow_trigger_action=="Create"){
    //             $(".forUpdateTrgger").addClass("display")
    //             $(".forCreateTrgger").removeClass("display")
    //             if(workflow_form_trigger){
    //                 $("#workflow-form-trigger").val(workflow_form_trigger)
    //             }
    //             getFormFields(workflow_form_trigger,workflow_reference_fields_update)
    //         }else{
    //             $(".forCreateTrgger").addClass("display")
    //             $(".forUpdateTrgger").removeClass("display")

    //             if(workflow_trigger_fields_filter){
    //                 $("#workflow-trigger-fields-filter").val(workflow_trigger_fields_filter)
    //             }
    //             if(workflow_trigger_fields_update){
    //                 $("#workflow-trigger-fields-update").val(workflow_trigger_fields_update)
    //             }
    //             if(workflow_trigger_operator){
    //                 $("#workflow-trigger-operator").val(workflow_trigger_operator)
    //             }
    //         }
    //     }
    //     //call function for saving
    //     this.save_node_settings()
    //     this.updateTrigger();
    //     this.updateFieldReference();
    // },
   
    //multi
    
    user_setup : function(node_data_id){
        ui.block();
       var newDialog = new jDialog(object_properties_node("workflow_chart-user_setup",node_data_id,""), "","700", "", "70", function(){});
        //$('#popup_container').addClass('bounceIn');
        newDialog.themeDialog("modal2");
        this.save_node_settings()
        var json = $("body").data();
        var json_nodes_var = json['nodes'];
        var users  = if_undefinded(json_nodes_var[''+ node_data_id +'']['users'],"");

        if(users){
            //editor
            var user_editor = users['user_editor'];
            $(".users-editor").find(".user-requestor").prop("checked",user_editor['requestor']);
            $(".users-editor").find(".user-processor").prop("checked",user_editor['processor']);
            RequestPivacy.setCheckAllChecked($(".users-editor"));

            //viewer
            RequestPivacy.setCheckAllChecked($(".users-viewer"));
            
        }
        ui.unblock();
    }
    
}

$(document).ready(function(){
    ButtonDraggableNodeObj.init();
    ClearNodesSettings.init();
    WorkflowTrigger.updateTrigger();
    WorkflowTrigger.updateFieldReference();

    workspace_functions.preview_form(".preview_form");
    form_settings_get_objects.form_size(".form_size");
    form_settings_get_objects.form_set_size(".form_set_size");
    //dbnode
    


    var pathname = window.location.pathname;

    if(pathname == "/user_view/workflow"){
        formulaJson = UpdateMentionDataWorkflow.init();
    }

    resizeForm($(".workflow_ws"), "600","workflow");
    
    ChangeFormulaType.init();

    //container
    var node_container = '.workspace';

    workflow_actions.setMembersFunction();

    
    //to make the workspace of workflow resizable
    $(".workflow_ws").resizable({
    handles:"s,e"
    })
    
    /* ORGCHART */
    //get and set nodes from database
    var workflow_lines = $("#workflow_lines").text();
    if (workflow_lines) {
        workflow_lines = JSON.parse(workflow_lines);
        var json_body = $("body").data();
        var json_linedata = {};
        
        for (var a=0; a<workflow_lines.length;a++) {
            var lines = JSON.parse(workflow_lines[a]);
            var json_title = lines['json_title'];
            if(lines['line-type']!="arrow"){
            //append line
            $(node_container).append('<span class="lines '+ lines['line-type'] +
                                        '" json_title="'+ json_title +
                                        '" line-class="'+ lines['line-class'] +
                                        '" parent="'+ lines['parent'] +'" child="'+ lines['child'] +
                                        '" style="top:'+ lines['top'] +'px;left:'+
                                        lines['left'] +'px;width:'+ lines['width'] +'px;height:'+
                                        lines['height'] +'px;background-color:'+
                                        lines['background-color'] +';"></span>');

            }else{
            //appendline
            $(node_container).append('<span class="lines arrow" json_title="'+ json_title +'" line-class="'+ lines['line-class'] +
                                             '" parent="'+ lines['parent'] +
                                             '" child="'+ lines['child'] +
                                             '" style="top:'+ lines['top'] +'px;left:'+ lines['left'] +'px;'+ lines['border'] +'"></span>');
            }
                json_linedata[''+ lines['line-class'] +'_'+lines['line-type']] = {};
                json_linedata[''+ lines['line-class'] +'_'+lines['line-type']] =  lines;
        }
        json_body['lines'] = json_linedata;
        json_body['workspace_title'] = $("#workflow_title").text();
        json_body['workspace_description'] = $("#workflow_description").text();
        $("body").data(json_body);
    
    }
    
    //get and set nodes from database
    var workflow_nodes = $("#workflow_nodes").text();
    // console.log(workflow_nodes)
    if (workflow_nodes) {
        workflow_nodes = JSON.parse(workflow_nodes);
        var ctr = 1;
        $.each(workflow_nodes, function(key, value){
            var json_nodes = JSON.parse(value)
            var line_class = "";
            if(json_nodes['node_type']=="circle"){
                json_nodes['node_type'] = "circle2";
            }else if(json_nodes['node_type']=="box" ){
                if(json_nodes['type_rel']==2){
                    json_nodes['node_type'] = "processor";
                }else if(json_nodes['type_rel']==3){
                    json_nodes['node_type'] = "condition";
                }
            }
            if(json_nodes['type_rel']==3){
                json_nodes['node_type'] = "condition_diamond";
            }
            nodeDesign(json_nodes);
            if(json_nodes['type_rel']==5){
                //walang gagawin
            }else{
                if(json_nodes['buttonStatus']){
                    $.each(json_nodes['buttonStatus'], function(btnKey, btnValue){
                        line_class = json_nodes['buttonStatus'][''+ btnKey +'']['parent_id']+"_"+json_nodes['buttonStatus'][''+ btnKey +'']['child_id'];
                        customTextLine(line_class,btnKey);
                    })
                }
            }
            if(json_nodes['condition_return']){
                $.each(json_nodes['condition_return'], function(cdtnkey, cdtnValue){
                    line_class = json_nodes['condition_return'][''+ cdtnkey +'']['parent_id']+"_"+json_nodes['condition_return'][''+ cdtnkey +'']['child_id'];
                    customTextLine(line_class,cdtnkey);
                })
            }
            if(ctr==workflow_nodes.length){
                for(var i in workflow_nodes){
                    var json_nodes2 = JSON.parse(workflow_nodes[i]);
                    nodeId = json_nodes2['node_data_id'];
                    moveLines(nodeId,"");

                }
            }
            ctr++;
        })
        // for (var a=0; a<workflow_nodes.length;a++) {
     //        var json_nodes = JSON.parse(workflow_nodes[a])
        //     nodeDesign(json_nodes);
     //        if(json_nodes['buttonStatus']){
     //            var line_class = json_nodes['buttonStatus']
     //            console.log(line_class)
     //            //customTextLine(line_class,"Sample");
     //        }
        // }
    }
    //variables in nodes function
    
    var node_linecolor = '#424242';
    //initialize node functions on load
    node.mouseoverNode();
    node.mouseleaveNode();
    node.mousedown();
    node.activateNode();
    node.dragAlso();
    node.dropToNode('.outer-node',node_container,node_linecolor);
    node.dragNode(".outer-node",node_container,true,node_linecolor);
    node.resizeNode(node_container,node_linecolor);
    node.resizeStartNode(node_container,node_linecolor);
    //Node Settings
    nodeSettings.minimizeNode(node_linecolor);
    nodeSettings.maximizeNode(node_linecolor);
    //Line Trigger Drag
    // Control Save Workspace
    // Main Function of save is on the formbuilder.js
    /*-----------------------------------------------------*/
    
    
/*
 * Add node to the workspace Workflow Chart
 * ('Start Node','End Node','Process Node','Conditional')
 * 
 */
    
    $(".process_node").node({
        event       : 'click',
        container   : '.workspace',
        startCount  : 1,
        type        : "processor",
        defaultText : "Processor",
        nodeColor   : "#424242",
        lineColor   : node_linecolor,
        type_rel    : "2",
        enableRuler : true,
        onDrop          : function(parent,child,addline){
            //get parent id
            var node_data_id = $(parent).closest(".outer-node").attr("id")
            //get child id
            var child_id = $(child).closest(".outer-node").attr("id");
            //modal type
            var object_type = "workflow_chart_buttons";
            // alert(123123)
            //set modal
            var newDialog = new jDialog(object_properties_node(object_type,node_data_id,child_id), "","800", "", "70", function(){});
            newDialog.themeDialog("modal2");

            $( "#accordion-container" ).tabs({
                activate : function(){
                    $('.content-dialog-scroll').perfectScrollbar('update');
                }
            })
            //field enabled
            // CheckProperties.setCheckAllChecked(".fieldEnabled",".f_enabled",".checkAllFields");
            // CheckProperties.bindOnClickCheckBox(".fieldEnabled",".f_enabled",".checkAllFields");
            //field required
            CheckProperties.setCheckAllChecked(".fieldRequired",".f_required",".checkAllFields");
            CheckProperties.bindOnClickCheckBox(".fieldRequired",".f_required",".checkAllFields");
            //field hidden
            // CheckProperties.setCheckAllChecked(".fieldHiddenValue",".f_hidden",".checkAllFields");
            // CheckProperties.bindOnClickCheckBox(".fieldHiddenValue",".f_hidden",".checkAllFields");
            //field read only
            // CheckProperties.setCheckAllChecked(".fieldReadOnly",".f_readOnly",".checkAllFields");
            // CheckProperties.bindOnClickCheckBox(".fieldReadOnly",".f_readOnly",".checkAllFields");

            // RequiredFieldValidation.action(0);
            //set color picker for line
            $("#wokflow_button-line-color").spectrum({
                        color: "424242",
                        appendTo: $("#wokflow_button-line-color").parent()
            });  
            
            //end of the function
            
            //click to save json to node
            workflow_actions.save_node_settings(function(line_color,json_title){
                        addline(line_color,json_title);
            })
        },
        settings_property : "workflow_chart-processor"
    });

    //Database Node
    $(".database_node").node({
        event       : 'click',
        container   : '.workspace',
        startCount  : 1,
        type        : "database",
        defaultText : "Database",
        nodeColor   : "#424242",
        lineColor   : node_linecolor,
        type_rel    : "5",
        enableRuler : true,
        // onDrop          : function(parent,child,addline){
        //     //get parent id
        //     var node_data_id = $(parent).closest(".outer-node").attr("id")
        //     //get child id
        //     var child_id = $(child).closest(".outer-node").attr("id");
        //     //modal type
        //     var object_type = "workflow_chart_buttons";
        //     // alert(123123)
        //     //set modal
        //     var newDialog = new jDialog(object_properties_node(object_type,node_data_id,child_id), "","500", "", "70", function(){});
        //     newDialog.themeDialog("modal2");
            
        //     //set color picker for line
        //     $("#wokflow_button-line-color").spectrum({
        //                 color: "424242"
        //     });  
            
        //     //end of the function
            
        //     //click to save json to node
        //     workflow_actions.save_node_settings(function(line_color,json_title){
        //                 addline(line_color,json_title);
        //     })
        // },
        settings_property : "workflow_chart-database"
    });
    
    //START NODE
    $(".start_node").node({
        event       : 'click',
        container   : '.workspace',
        startCount  : 1,
        type        : "circle2",
        defaultText : "Start",
        nodeColor   : "#424242",
        lineColor   : node_linecolor,
        type_rel    : "1",
        enableRuler : false,
        height  : "80px",
        width   : "90px",
        maxCount    : 1,
        onDrop      : function(parent,child,addline){
                //get parent id
                var node_data_id = $(parent).closest(".outer-node").attr("id")
                //get child id
                var child_id = $(child).closest(".outer-node").attr("id");
                //modal type
                var object_type = "workflow_chart_buttons";
                
                //set modal
               var newDialog = new jDialog(object_properties_node(object_type,node_data_id,child_id), "","800", "", "", function(){});
                newDialog.themeDialog("modal2");
                //set color picker for line
                $("#wokflow_button-line-color").spectrum({
                            color: "424242",
                            appendTo: $("#wokflow_button-line-color").parent()
                });  
                $( "#accordion-container" ).tabs({
                    activate : function(){
                        $('.content-dialog-scroll').perfectScrollbar('update');
                    }
                })

                //field required
                CheckProperties.setCheckAllChecked(".fieldRequired",".f_required",".checkAllFields");
                CheckProperties.bindOnClickCheckBox(".fieldRequired",".f_required",".checkAllFields");
                
                //end of the function
                
                                
                //click to save json to node
                workflow_actions.save_node_settings(function(line_color,json_title){
                            addline(line_color,json_title);
                })
        },
        settings_property : "workflow_chart-start",
        drop            : false
    });
    

    

    //CONDITION NODE
    //conditional_node
    $(".conditional_node").node({
    event       : 'click',
    container   : '.workspace',
    startCount  : 1,
    type        : "condition_diamond",
    defaultText : "Condition",
    nodeColor   : "#424242",
    type_rel    : "3",
    lineColor   : node_linecolor,
        enableRuler : false,
        onDrop            : function(parent,child,addline){
            //get parent id
            var node_data_id = $(parent).closest(".outer-node").attr("id")
            //get child id
            var child_id = $(child).closest(".outer-node").attr("id");
            //modal type
            var object_type = "workflow_chart_condition-line";
            //set modal
           var newDialog = new jDialog(object_properties_node(object_type,node_data_id,child_id), "","500", "", "", function(){});
            newDialog.themeDialog("modal2");
            //set color picker for line
            $("#wokflow_condition-line-color").spectrum({
                        color: "424242"
            });  
            
            //end of the function
            //click to save json to node
            workflow_actions.save_node_settings(function(line_color,json_title){
                        addline(line_color,json_title);
            })
        },
        settings_property : "workflow_chart-condition"
    });
    
    //END NODE
    
    $(".end_node").node({
    event       : 'click',
    container   : '.workspace',
    startCount  : 1,
    type        : "circle2",
    defaultText : "End",
    nodeColor   : "#424242",
    type_rel    : "4",
    lineColor   : node_linecolor,
    height  : "80px",
    width   : "90px",
    maxCount    : 1,
        settings_property : "workflow_chart-end",
        enableRuler : false,
        hasDrag         : false
    });
    
    
    {//MOVED TO BUILDER COMMON JS 12 22 2015 3 03 PM
        //event change of processor type
        // $("body").on("change.","#workflow-processor-type, .workflow-processor-type",function(){
        //       var value = $(this).val();
        //   $("#workflow-processor, #workflow-processor").hide();
        //   $("#workflow-processor").next().show();
        //       getprocessor(value,0);
        // })
    }
    
    if(pathname == "/user_view/workflow"){
        // OOP JS Functions Call
            /* Main Workspace */
                workspace_functions.view_save(".save_workspace");
            /* Main Workflow */
                workflow_actions.save_form_workspace(".save_form_workspace");/* Main Workflow */
                /* Save Settings */
                //workflow_actions.save_node_settings();/* Main Workflow */
    }
    
    //GET PROCESSOR TYPE
    

    //Email
    $("body").on("click",".node-email",function(){

        var node_id = $(this).closest(".outer-node").attr("id");
        workflow_actions.save_node_mail(node_id);

        RequestPivacy.bindOnClickCheckBox();
        // $('.content-dialog-scroll').perfectScrollbar();
        // $('.content-dialog-scroll-container').perfectScrollbar();
        $('.content-dialog-scroll, .content-dialog-scroll-container').perfectScrollbar({
              wheelSpeed: 1,
              wheelPropagation: false,
              suppressScrollX: true,
              minScrollbarLength:50,
              scroll : "x"

        });   
        
        // $(".ps-scrollbar-x").hide();

    })

    // Users set up

    $("body").on("click",".node-user-setup",function(){
        var node_id = $(this).closest(".outer-node").attr("id");
        workflow_actions.user_setup(node_id);
        RequestPivacy.bindOnClickCheckBox();
        $('.tip').tooltip();
        // $('.content-dialog-scroll').perfectScrollbar({
        //     "scroll" : "x"
        // });
        // $('.content-dialog-scroll-container').perfectScrollbar();
        $('.content-dialog-scroll, .content-dialog-scroll-container').perfectScrollbar({
              wheelSpeed: 1,
              wheelPropagation: false,
              suppressScrollX: true,
              minScrollbarLength:50,
              scroll : "x"

        });
        $( "#accordion-container" ).tabs({
            activate : function(){
                $('.content-dialog-scroll').perfectScrollbar('update');
                $('.content-dialog-scroll-container').perfectScrollbar('update');
            },
        })
    })

    $("body").on("click",".node-field-trigger",function(){
        var node_id = $(this).closest(".outer-node").attr("id");
        WorkflowTrigger.field_trigger_settings(node_id);
    })
    
 

    $('body').on('dblclick',".outer-node", function (ev) {
        var pathname = window.location.pathname;
        if(!$(this).hasClass('ui-draggable') && pathname=="/workspace"){
            // return;
        }
        if(pathname=="/user_view/workspace"){
            return;
        }
        // $(".node-setting").click();
        var node_data_id = $(this).attr("data-node-id");
        var object_type = $(this).find(".node-setting").attr("data-object-type");
        setNodesData(node_data_id,object_type)
        $(this).removeClass("activateNode");
    }) 
    //NODE SETTINGS
    $('body').on('click',".node-setting", function (e) {
        var node_data_id = $(this).closest(".outer-node").attr("data-node-id");
        var object_type = $(this).attr("data-object-type");
        setNodesData(node_data_id,object_type)

    });

    if ($(".workspace.workflow_ws").find(".outer-node").length >= 1) {
         var ws_width = $("#workflow_width").text();
         var ws_height = $("#workflow_height").text();
        if (checkFormMinWidth("workflow") != null) {
            $(".workspace.workflow_ws").resizable("option", "minWidth", checkFormMinWidth("workflow"));
        }
        if (checkFormMinHeight("workflow") != null) {
            $(".workspace.workflow_ws").resizable("option", "minHeight", checkFormMinHeight("workflow"));
        }
        $(".workspace.workflow_ws").css({
            "height": $("#workflow_height").text()+"px",
            "width": $("#workflow_width").text()+"px", // dito nag lalagay width height sa workflow_w

        });
        
        if ($('#tpl_form_size:not(:contains("custom_size"))').length > 0) {
            //alert($('#tpl_form_size').text());
            $(".form_size").val($('#tpl_form_size').text());
            enableFormSetSizeField();
        }
        //added by roni check on first load if selection is null apply below
        if ($(".form_size").val() === null) {
               
            $('.fl-opt_custom_size').prop('selected', true);
            enableFormSetSizeField();
            $(".form_size_width").val(ws_width);
            $(".form_size_height").val(ws_height);
        }

        if ($(".form_size").val() !== null) { 

            if ($('.form_size').children('option:selected').is(':not(.fl-opt_custom_size)')) {
                enableFormSetSizeField();
            };

             if($('.form_size').children('.fl-opt_custom_size:selected').length >= 1){ 
                enableFormSetSizeField();
                $(".form_size_width").val(ws_width);
                $(".form_size_height").val(ws_height);
             }
        }

        
        //  if ($('#tpl_form_size:contains("custom_size")').length > 0) {
           
        //     enableFormSetSizeField();
        //     $(".form_size").val($('#tpl_form_size').text());
           
        // }
        
        // if($('.form_size').children('.fl-opt_custom_size:selected').length >= 1){
        //     //alert('.fl-opt_custom_size:selected');
            
        //     $('.form_size').children('.fl-opt_custom_size:selected').data("custom_set_size",{
        //             "width":(ws_width)+"",
        //             "height":(ws_height)+""
        //     });
            
        //     $(".form_size_width").val(ws_width);
        //     $(".form_size_height").val(ws_height);
        
        // }
        // else{

           
        //     $('.form_size').children('.fl-opt_custom_size').data("custom_set_size",{
        //             "width":(ws_width)+"",
        //             "height":(ws_height)+""
        //     });
            
        // }
        
        $(".leftPointerRuler-container").css({
            "height": $("#workflow_height").text()+"px",
        })
    }


    //multi
    WorkflowTrigger.field_trigger_custom();  
    WorkflowTrigger.change_referenceFieldsUpdate();  


    $("body").on("click",".clear_all_nodes",function(){
        var nodes = $(".outer-node").length;
        if(nodes==0){
            return;
        }
        con = "Are you sure you want to remove all nodes??";
       var newConfirm = new jConfirm(con, 'Confirmation Dialog','', '', '', function(r){
            if(r==true){
                removeAllNodes();
            }
        });
       newConfirm.themeConfirm("confirm2", {
            'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
       });
    })
    var pathname = window.location.pathname;
    if(pathname!="/workflow" || pathname!="/user_view/workflow"){
        // return;
    }
    var ws_width = $(".workflow_ws").outerWidth();
    var ws_height = $(".workflow_ws").outerHeight();

    if( pathname == "/workflow" || pathname == "/user_view/workflow" || pathname == "/user_view/organizational_chart" ){
       // $(".form_size_width").val(ws_width);
        //$(".form_size_height").val(ws_height)//Comment for now
    
    }
    
})
function setNodesData(node_data_id,object_type){
    ui.block();
    var json_nodes = $("body").data();
        var json_nodes_var = json_nodes['nodes'];
    // Remove other open popover
    //$('.object_properties').not(this).popover('hide');
    var top = "70";
    var width = "600";
    if(object_type=="workflow_chart-processor" || object_type=="workflow_chart-start"){
        width = "750";
    }else if(object_type=="organizational_chart"){
        width = "800";
        top = "25";
    }
    var newDialog = new jDialog(object_properties_node(object_type,node_data_id,""), "",width, "", top, function(){});
    //$('#popup_container').addClass('bounceIn');
    newDialog.themeDialog("modal2");
    //field enabled
    CheckProperties.setCheckAllChecked(".fieldEnabled",".f_enabled",".checkAllFields");
    CheckProperties.bindOnClickCheckBox(".fieldEnabled",".f_enabled",".checkAllFields");
    //field required
    CheckProperties.setCheckAllChecked(".fieldRequired",".f_required",".checkAllFields");
    CheckProperties.bindOnClickCheckBox(".fieldRequired",".f_required",".checkAllFields");
    //field hidden
    CheckProperties.setCheckAllChecked(".fieldHiddenValue",".f_hidden",".checkAllFields");
    CheckProperties.bindOnClickCheckBox(".fieldHiddenValue",".f_hidden",".checkAllFields");
    //field read only
    CheckProperties.setCheckAllChecked(".fieldReadOnly",".f_readOnly",".checkAllFields");
    CheckProperties.bindOnClickCheckBox(".fieldReadOnly",".f_readOnly",".checkAllFields");

    //validation in required field
    RequiredFieldValidation.action(node_data_id);


    limitText(".limit-text-wf",25); // set limit of text in the fields  
    if (object_type=="organizational_chart") {
        getUser(node_data_id,"",0,function(){
            $('.content-dialog-scroll').perfectScrollbar("update");
            limitText(".limit-text-wf",19); // set limit of text in the name  
            $(".tip").tooltip(); 
            ui.unblock();
            workflow_actions.setMembers();
            if($(".userLevel").val()!=2){
                $(".settings-to-json").remove();
                $("#orgchart-dept-name, .member_user, .asst_head_user, .head_user").attr("disabled","disabled");
                $(".chngColor[data-properties-type='nodeColor']").spectrum({
                    disabled: true,
                });
                $(".cancel_dialog").val("Close")
            }
            ui.unblock();
        });
    }else if (object_type=="workflow_chart-processor" || object_type=="workflow_chart-start" || object_type=="workflow_chart-end" || object_type=="workflow_chart-condition" || object_type=="workflow_chart-database") {
        //getFlds();
        // $('.content-dialog-scroll').perfectScrollbar();
        ui.unblock();
        if(object_type=="workflow_chart-processor" || object_type=="workflow_chart-end" || object_type=="workflow_chart-database"){
            $("#fm-type0").prop("checked",true);
            var workflow_notification_message_type = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow_notification_message_type'],"");
            if(workflow_notification_message_type=="1"){
                $("#fm-type1").prop("checked",true);
                // $("#fm-message").mention_post({
                //     json : formulaJson, // from local file fomula.json
                //     highlightClass:"",
                //     prefix : "",
                //     countDisplayChoice : "4"
                // });
            }
            OtherAutoComplete.init();
            //check notification part if enabled
            CheckIfEnabled.init(".enabled-notification",[".formulaContainer .fm-type","#fm-message"]);
            CheckIfEnabled.clickEvent(".enabled-notification",[".formulaContainer .fm-type","#fm-message"]);

            $( "#accordion-container" ).tabs({
                activate : function(){
                    $('.content-dialog-scroll').perfectScrollbar('update');
                }
            })
        }


        if(object_type=="workflow_chart-condition"){
            FieldCondition.updatedFieldOperator(".wokflow_condition-fields",".wokflow_condition-operator","#wokflow_condition-value",".object_properties_wrapper");
            $(".wokflow_condition-fields").change(function(){
                FieldCondition.updatedFieldOperator(this,".wokflow_condition-operator","#wokflow_condition-value",".object_properties_wrapper");
                $(".wokflow_condition-operator").val("==");
                $("#wokflow_condition-value").val("");
            })
        }else if(object_type=="workflow_chart-database"){
            $('.content-dialog-scroll').css("max-height","500px");
            $(".fields_formula_row").show();
            DatabaseNodeProperties.init();

            WorkflowTrigger.external_db_data(node_data_id);
        }
    }
    

    
    $(".tip").tooltip(); 
    //for processor
    var workflow_processor_type = if_undefinded(json_nodes_var[''+ node_data_id +'']['processorType'],"");
    var workflow_processor = if_undefinded(json_nodes_var[''+ node_data_id +'']['processor'],"");
    getprocessor(workflow_processor_type,workflow_processor);


    //hide scroll x
    
    $(".ps-scrollbar-x").hide();
    
    var node_color = if_undefinded(json_nodes_var[''+ node_data_id +'']['node_color'],"#424242");
    $(".chngColor[data-properties-type='nodeColor']").spectrum({
        color: node_color
    });
    //enable save json properties
    workflow_actions.save_node_settings(null);


    $('.content-dialog-scroll').perfectScrollbar('update');
}
//getprocessor moved to custom.js 10192015 2:04PM

//for multi

function getFlds() {
 //    var pathname = window.location;
 //    var formID = getParametersName("form_id",pathname);
    
 //    $.post("/ajax/getFormProperties",{action:"getFlds",formID:formID},function(result){
    // var flds = JSON.parse(result);
    // var ret = "";
    //     for (i in flds) {
    //      ret += '<input type="checkbox" name="field_' + flds[i].fieldName + '" value="' + flds[i].fieldName + '" /> ' + flds[i].fieldName + '<br />';
            
    //     }    
    // $(".fieldEnabled").append(ret);
    // $(".fieldHiddenValue").append(ret);
    // //
    
 //    });
}

function json_haselement(json){
    var ctr = 0;
    var ret = false;
    for(var i in json){
        ctr++;
    }
    if(ctr>0){
        ret =  true;
    }
    return ret;
}
// function triggerHtml(json){
//     var ret = "";
//     var disp = "";
//     var form_fields = $(".form_fields").val();
//     var forms = $(".company_forms").text();
//     var label = [];
//     var margin = "";
//     var cssDisplayCreate = "";var cssDisplayCreate = "";
//     form_fields = JSON.parse(form_fields);
//     forms = JSON.parse(forms);
    
//     for(var i in json){
//         disp = "";
//         label = ["","","","","",""]
//         margin = "margin-top:10px";
//         if(i==0){
//             disp = "display";
//             label = ['Action: <font color="red">*</font>','Field Filter: <font color="red">*</font>','Field Update: <font color="red">*</font>','Operator: <font color="red">*</font>','Form Trigger: <font color="red">*</font>','Refence Field: <font color="red">*</font>'];
//             margin = "margin-top:30px";
//         }
//         if(json[i]['workflow-trigger-action']=="Create"){
//             cssDisplayCreate = "";
//             cssDisplayUpdate = "display replDisp";
//         }else{
//             cssDisplayCreate = "display replDisp";
//             cssDisplayUpdate = "";
//         }
//         ret += '<div class="trigger-container-fields" style="">';
//         ret += '<div style="float: left;width:23%;" class="fl-margin-bottom">';
//             ret += '<div class="fields_below" style="width:98%;">';
//                 ret += '<div class="label_below2">'+ label[0] +'</div>';
//                 ret += '<div class="input_position_below">';
//                 ret += '<div style="font-size:11px"><label><input type="radio" '+ setChecked("Update", json[i]['workflow-trigger-action']) +' name="workflow-trigger-action_'+ i +'" value="Update" checked="checked" class="workflow-trigger-action-class"><span> Update Field</span></label></div>';
//                 ret += '<div style="font-size:11px"><label><input type="radio" '+ setChecked("Create", json[i]['workflow-trigger-action']) +' name="workflow-trigger-action_'+ i +'" value="Create" class="workflow-trigger-action-class"><span> Create Request</span></label></div>';
//                 ret += '</div>';
//             ret += '</div>';
//         ret += '</div>';

//         //FOR UPDATE TRIGGER
//         ret += '<div class="forUpdateTrgger '+ cssDisplayUpdate +'">';
//             ret += '<div style="float:left;width:23%;">';
//                 ret += '<div class="fields_below" style="width:98%;">';
//                     ret += '<div class="label_below2">'+ label[1] +'</div>';
//                     ret += '<div class="input_position_below">';
//                         ret += '<select class="form-select workflow-trigger-fields-filter" id="" style="margin-top:0px">';
//                         for (var j in form_fields) {
//                             // console.log(form_fields)
//                             ret += '<option '+ setSelected(form_fields[j], json[i]['workflow-trigger-fields-filter']) +'>'+ form_fields[j] +'</option>';
//                         }
//                         ret += '</select>';
//                     ret += '</div>';
//                 ret += '</div>';
//             ret += '</div>';
//             ret += '<div style="float:left;width:23%;">';
//                 ret += '<div class="fields_below" style="width:98%;">';
//                     ret += '<div class="label_below2">'+ label[2] +'</div>';
//                     ret += '<div class="input_position_below">';
//                         ret += '<select class="form-select workflow-trigger-fields-update" id="" style="margin-top:0px">';
//                         for (var j in form_fields) {
//                             // console.log(form_fields)
//                             ret += '<option '+ setSelected(form_fields[j], json[i]['workflow-trigger-fields-update']) +'>'+ form_fields[j] +'</option>';
//                         }
//                         ret += '</select>';
//                     ret += '</div>';
//                 ret += '</div>';
//             ret += '</div>';
//             ret += '<div style="float:left;width:23%;">';
//                 ret += '<div class="fields_below" style="width:98%;">';
//                     ret += '<div class="label_below2">'+ label[3] +'</div>';
//                     ret += '<div class="input_position_below">';
//                         ret += '<select class="form-select workflow-trigger-operator" id="" style="margin-top:0px">';
//                             ret += '<option value="Addition" '+ setSelected("Addition", json[i]['workflow-trigger-operator']) +'>Addition</option>'
//                             ret += '<option value="Subtraction" '+ setSelected("Subtraction", json[i]['workflow-trigger-operator']) +'>Subtraction</option>'
//                             ret += '<option value="Multiplication" '+ setSelected("Multiplication", json[i]['workflow-trigger-operator']) +'>Multiplication</option>'
//                             ret += '<option value="Division" '+ setSelected("Division", json[i]['workflow-trigger-operator']) +'>Division</option>'
//                         ret += '</select>';
//                     ret += '</div>';
//                 ret += '</div>';
//             ret += '</div>';
//         ret += '</div>';
//         //FOR CREATE TRIGGER
//         ret += '<div class="forCreateTrgger '+ cssDisplayCreate +'">';
//             ret += '<div style="float:left;width:32.5%;margin-right:10px">';
//                  ret += '<div class="fields_below" id="forFormTrigger">';
//                     ret += '<div class="label_below2">'+ label[4] +'</div>';
//                     ret += '<div class="input_position_below">';
//                         ret += '<select class="form-select workflow-form-trigger" id="workflow-form-trigger_'+ i +'" style="margin-top:0px">';
//                             ret += '<option value="0">--Select Form--</option>';
//                         for (var j in forms) {
//                             // console.log(form_fields)
//                             ret += '<option '+ setSelected(forms[j]['id'], json[i]['workflow-form-trigger']) +' value="'+ forms[j]['id'] +'">'+ forms[j]['form_name'] +'</option>';
//                         }
//                         ret += '</select>';
//                         ret += '<img src="/images/loader/loading.gif" class="pull-left display replDisp processorLoad"/>';
//                     ret += '</div>';
//                 ret += '</div>';
//             ret += '</div>';
//             ret += '<div style="float:left;width:33%;margin-right:10px">';
//                 ret += '<div class="fields_below" id="forReferenceField">';
//                     ret += '<div class="label_below2">'+ label[5] +'</div>';
//                     ret += '<div class="input_position_below">';
//                         ret += '<select class="form-select workflow-reference-fields-update" id="workflow-reference-fields-update" style="margin-top:0px">';
//                         ret += '<option value="0">--Select Field--</option>';
//                         ret += '</select>';
//                         ret += '<img src="/images/loader/loading.gif" class="pull-left display replDisp processorLoad"/>';
//                     ret += '</div>';
//                 ret += '</div>';
//             ret += '</div>';
//         ret += '</div>';
//         ret += '<div style="float:left;width:8%;font-size: 15px;position: relative;">';
//             ret += '<div style="float:left;width:100%;margin-left:5px;'+ margin +'">';
//                 ret += '<i class="addtrigger icon-plus-sign fa fa-plus cursor"></i>';
//                 ret += '<i class="minusTrigger icon-minus-sign fa fa-minus cursor '+ disp +'" style="margin-left:5px;"></i>';
//             ret += '</div>';  
//         ret += '</div>';    
//     ret += '</div>';    
//     }
//     return ret;
// }
function removeAllNodes(){
    var json_nodes = $("body").data();
    delete json_nodes['nodes'];
    delete json_nodes['lines'];

    $(".outer-node").remove();
    $(".lines").remove();

}
function converBreakLineTextbox(ele){
    value = $(ele).val();
    value = converBreakLine(value);
    $(ele).val(value);
}
function converBreakLine(value){
    return value.replace(/<br\s?\/>/g,"\n");
}

ChangeFormulaType = {
    "init" : function(){
        $("body").on("mousedown",'label:has(".formulaType")',function(e){
            var self0 = $(this);
            self = $('label:has(".formulaType")').filter(function(){ return self0[0] == $(this)[0]; });
            // alert("sfsdsd"+self.parent().children('input[type="radio"]').val()   )
            var current_textarea_ele = self.parents('.input_position_below:eq(0)').find('textarea:eq(0)');
            if(!current_textarea_ele.is(':data("temp_last_value")')){
                current_textarea_ele.data('temp_last_value',{});
            }
            current_textarea_ele.data('temp_last_value')["selected"] = self.parent().find('input[type="radio"]:checked').val();
            current_textarea_ele.data('temp_last_value')[""+current_textarea_ele.data('temp_last_value')["selected"]] = current_textarea_ele.val();
        });
        $("body").on("change",".formulaType",function(e){
            var self = $(this);
            var formulaObj = $(this).closest(".formulaContainer").find(".wfSettings");
            var current_textarea_ele = self.parents('.input_position_below:eq(0)').find('textarea:eq(0)');
            current_textarea_ele.val((current_textarea_ele.data("temp_last_value")||{})[""+self.val()]||"");
        })
    },
}

OtherAutoComplete = {
    "init" : function(){
        // $(".wf-button-formula").mention_post({
        //     json : formulaJson, // from local file fomula.json
        //     highlightClass:"",
        //     prefix : "",
        //     countDisplayChoice : "4"
        // });
    }
}
RequiredFieldValidation = {
    "action" : function(node_data_id){
        var self = this;
        $(".fieldEnabled").change(function(){
            var name = $(this).attr("name");
            if($(this).prop("checked")==false || $(".f_hidden").find("[name='"+ name +"']").prop("checked")==true){
                self.requiredFielddisabled(name);
            }else{
                self.requiredFieldenable(name);
            }
        })

        $("#f_enabled_2, #f_enabled").change(function(){
            var name = "";
            var self_checkall = this;
            $(".fieldEnabled").each(function(){
                name = $(this).attr("name");
                if($(self_checkall).prop("checked")==false || $(".f_hidden").find("[name='"+ name +"']").prop("checked")==true){
                    self.requiredFielddisabled(name);
                }else{
                    self.requiredFieldenable(name);
                }
            })
        })

        $(".fieldHiddenValue").change(function(){
            var name = $(this).attr("name");
            if($(this).prop("checked")==true || $(".f_enabled").find("[name='"+ name +"']").prop("checked")==false){
                self.requiredFielddisabled(name);
            }else{
                self.requiredFieldenable(name);
            }
        })

        $("#f_hidden_2, #f_hidden").change(function(){
            var name = "";
            var self_checkall = this;
            $(".fieldHiddenValue").each(function(){
                name = $(this).attr("name");
                if($(self_checkall).prop("checked")==true || $(".f_enabled").find("[name='"+ name +"']").prop("checked")==false){
                    self.requiredFielddisabled(name);
                }else{
                    self.requiredFieldenable(name);
                }
            })
        })
    },
    "requiredFieldenable" : function(name){
        $(".f_required").find("[name='"+ name +"']").prop("disabled",false);
        $(".f_required").find("[name='"+ name +"']").closest("label").css({
            "opacity": false,
            "cursor": false
        })
        var totalCheckBox = $(".f_required").find(".fieldRequired").length;
        var totalCheckBoxChecked = $(".f_required").find(".fieldRequired:disabled").length

        if (totalCheckBox != totalCheckBoxChecked) {
            // $(".f_required").find(".f_required_2").prop("checked", true);
            $(".f_required").find("#f_required_2, #f_required").prop("disabled",false);
            $(".f_required").find("#f_required_2, #f_required").closest("label").css({
                "opacity": false,
                "cursor": false
            })
        }
    },
    "requiredFielddisabled" : function(name){
        $(".f_required").find("[name='"+ name +"']").prop("checked",false);
        $(".f_required").find("[name='"+ name +"']").prop("disabled",true);
        $(".f_required").find("[name='"+ name +"']").closest("label").css({
            "opacity": "0.4",
            "cursor": "not-allowed"
        })
        var totalCheckBox = $(".f_required").find(".fieldRequired").length;
        var totalCheckBoxChecked = $(".f_required").find(".fieldRequired:disabled").length

        if (totalCheckBox == totalCheckBoxChecked) {
            $(".f_required").find("#f_required_2, #f_required").prop("checked", false);
            $(".f_required").find("#f_required_2, #f_required").prop("disabled",true);
            $(".f_required").find("#f_required_2, #f_required").closest("label").css({
                "opacity": "0.4",
                "cursor": "not-allowed"
            })
        }
    }
}
UpdateMentionDataWorkflow = {
    init : function(){
        var active_fields = $("#active_fields").text();
        var formulaJsonUpdated = [];
        var returnFormula = [];
        var count = formulaJson.length;
        if(active_fields){
            active_fields = active_fields.split(",");
            for(var i in active_fields){
                var fields = active_fields[i];
                if(typeof fields==="undefined"){
                    return true;
                }
                // console.log(fields.attr("name"))
                formulaJsonUpdated.push({
                     "id": count,
                     "name": "@"+fields,
                     "first_name":"",
                     "last_name":"",
                     "ws" : "1"
                })
                count++;
            }
        }
        returnFormula = formulaJsonUpdated.concat(formulaJson);
        return returnFormula;
    }
}

ButtonDraggableNodeObj = {
    "init" : function(){
        return;
        var self = this;
        /* WORKFLOW */
        //start
        $('.start_node').draggable( {
            cursor: 'move',
            containment: 'document',
            helper: function(e){
                return self.helperObjectDesign(e,'circle2','Start');
            },
            cancel:false,
            opacity:".5"
        });

        //end
        $('.end_node').draggable( {
            cursor: 'move',
            containment: 'document',
            helper: function(e){
                return self.helperObjectDesign(e,'circle2','End');
            },
            cancel:false,
            opacity:".5"
        });

        //process
        $('.process_node').draggable( {
            cursor: 'move',
            containment: 'document',
            helper: function(e){
                return self.helperObjectDesign(e,'processor','Processor');
            },
            cancel:false,
            opacity:".5"
        });

        //condition
        $('.conditional_node').draggable( {
            cursor: 'move',
            containment: 'document',
            helper: function(e){
                return self.helperObjectDesign(e,'condition','Condition');
            },
            cancel:false,
            opacity:".5"
        });


        /* ORGCHART */

        //department 
        $('.orgchart_node').draggable( {
            cursor: 'move',
            containment: 'document',
            helper: function(e){
                return self.helperObjectDesign(e,'department','Department Name');
            },
            cancel:false,
            opacity:".5"
        });

        this.makeWorkspaceDroppable();
    },
    "makeWorkspaceDroppable" : function(){
        $(".workflow_ws, .orgchart_ws").droppable({
            drop : function(event,ui){
                //tagname of drag element
                var tagname = $(ui.draggable).prop("tagName");
                //for button draggble drop only
                if(tagname=="BUTTON"){
                    //declaring variable for needed adjustment
                    var helper = $(ui.helper);
                    var parent = $(this);
                    var docs = $(document);

                    // top and left adjustment
                    var leftAdjust = (helper.position().left - parent.offset().left+3)+docs.scrollLeft();
                    var topAdjust = (helper.position().top - parent.offset().top+12)+docs.scrollTop();

                    //counter in creating of node
                    //var count is global variable
                    var oldCount = counter;

                    $(ui.draggable).trigger("click");
                    $("#node_"+oldCount).css({
                        top:topAdjust+"px",
                        left:leftAdjust+"px"
                    });
                }
            }
        });
    },
    helperObjectDesign : function(event,type,text){
        //helper for the design of cloned object
        var json = {
            "node_left"     : "0px",
            "node_top"  : "0px",
            "node_data_id"  : "node_0",
            "node_text"     : text,
            "node_type"     : type,
            "node_container": "workflow_ws",
            "node_color"    : "",
            "node_state"    : "",
            "node_height"   : "",
            "node_width"    : "",
            "node_onDrop"   : "",
            "node_drop" : "",
            "node_settings_property":"",
            "node_hasDrag"  :"",
            "type_rel"  :""
        }
        //design of ele
        var nodeDes = nodeDesign(json);
        //get the inner node of object
        var ret = nodeDes.find(".node").wrap("<div />").parent().html();
        return ret;
    }
}

CheckIfEnabled = {
    "init" : function(selector,obj){
        if($(selector).prop("checked")==true){
            for(var i in obj){
                $(obj[i]).prop("disabled",false);
                $(obj[i]).closest("label, div").css({
                    "opacity":1
                })
            }
        }else{
            for(var i in obj){
                $(obj[i]).prop("disabled",true);
                $(obj[i]).closest("label, div").css({
                    "opacity":0.6
                })
            }
        }
    },
    "clickEvent" : function(selector,obj){
        var self = this;
        $(selector).click(function(){
            self.init(selector,obj);
        })
    }
}

CheckKeywordStatus = {
    "keyword" : ["draft","cancelled"],
    "validateStatus" : function(status){
        var self = this;
        status = status.toLowerCase();
        if(self.keyword.indexOf(status)>=0){
            return true;
        }else{
            return false;
        }
    },
    "errorMessage" : "Your chosen status is one of our system keyword. Please use different word to continue your setup."

}

ClearNodesSettings = {
    "init" : function(){
        var self = this;
        $("body").on("click",".clear-node-settings",function(){
            var object_type = $(this).attr("object_type")

            //for basic text, radio, checkbox and textare
            $("#popup_container input, #popup_container textarea").each(function(){
                if($(this).attr("type")=="checkbox" || $(this).attr("type")=="radio"){
                    $(this).prop("checked",false);
                }else if($(this).attr("type")=="text"){
                    $(this).val("");
                }

                if($(this).prop("tagName")=="TEXTAREA"){
                    $(this).val("");
                }
            })

            //for checkbox with check all functionality
            $("#popup_container .checkAllFields").prop("checked",true).trigger("click");

            //for formula
            $("#popup_container .formulaType[value='0']").prop("checked",true).trigger("change");

            //for combo box
            $("#popup_container select").each(function(){
                $(this).val($(this).find("option").eq(0).val());
                if($(this).attr("id")=="workflow-processor-type"){
                    $(this).trigger("change");
                }
            })

            //line color 
            var node_color = "#424242";
            $(".chngColor[data-properties-type='nodeColor']").spectrum({
                color: node_color
            });
            $(".chngColor[data-properties-type='nodeColor']").val(node_color);

            //code type on department
            $("#orgchart_dept_code_type").trigger("change");

            //assistant head defaul checked
            $(".asst_head_user").eq(0).prop("checked",true);

            //reset
            workflow_actions.head_id = [];
            workflow_actions.assistantHead_id = ["0"];
            workflow_actions.members_id = [];
            //for trigger
            if(object_type=="workflow_chart-fields-trigger" || object_type == "workflow_chart-database"){
                $(".workflow-trigger-action-class").eq(0).prop("checked",true).trigger("change");
                $(".minusTrigger, .minusFieldTrigger").trigger("click");
                $(".content-dialog-scroll").perfectScrollbar("update")
                $("#workflow-form-trigger").eq(0).trigger("change");
            }
        })
    },
}

TriggerAutoComplete = {
    init : function(){
        $(".formulaContainer").each(function(){
            if($(this).find(".formulaType[value='1']").prop("checked")){
                // $(this).find(".wfSettings").mention_post({
                //     json : formulaJson, // from local file fomula.json
                //     highlightClass:"",
                //     prefix : "",
                //     countDisplayChoice : "4"
                // });
            }
        })
    }
}

TriggerUniqueFields = {
    init : function(ele){
        var reference_fields = $(ele).closest(".trigger-container-fields").find(".workflow-reference-fields-update");
        var reference_fields_values = [];
        var val = "";var id = "";var other_id = "";
        // console.log("reference_fields",reference_fields)
        reference_fields.find("option").removeClass("display");
        reference_fields.each(function(){
            val = $(this).val();
            id = $(this).attr("id");
            reference_fields.each(function(){
                other_id = $(this).attr("id");
                if(id!=other_id){
                    if(val!=0){
                        $("#"+other_id).find("option[value='"+ val +"']").addClass("display");
                    }
                }
            })
        })
        console.log(reference_fields_values);
    }
}

DatabaseNodeProperties  = {
    init : function(){
        this.makePlusMinus();
    },
    makePlusMinus : function(){
        MakePlusPlusRowV2({
            "tableElement": '.fl-database-upload-container',
            "rules":[{
                "eleMinus":".database-upload-db-section-minus",
                "eleMinusTarget":".database-group",
                "elePlus":".database-upload-db-section-plus",
                "elePlusTarget":".database-group",
                "elePlusTargetFn":function(){
                    $('.content-dialog-scroll').perfectScrollbar('update');
                },
                "eleMinusTargetFn":function(){
                    $('.content-dialog-scroll').perfectScrollbar('update');
                },
                "eleMinusTargetFnBefore":function(){
                },
                "elePlusTargetFnBefore" : function(){
                }
            },
            {
                "eleMinus":".database-upload-table-section-minus",
                "eleMinusTarget":".table-group",
                "elePlus":".database-upload-table-section-plus",
                "elePlusTarget":".table-group",
                "elePlusTargetFn":function(){
                    $('.content-dialog-scroll').perfectScrollbar('update');
                },
                "eleMinusTargetFn":function(){
                    $('.content-dialog-scroll').perfectScrollbar('update');
                },
                "eleMinusTargetFnBefore":function(){
                },
                "elePlusTargetFnBefore" : function(){
                }
            },
            {
                "eleMinus":".database-upload-field-section-minus",
                "eleMinusTarget":".fields-group",
                "elePlus":".database-upload-field-section-plus",
                "elePlusTarget":".fields-group",
                "elePlusTargetFn":function(){
                    $('.content-dialog-scroll').perfectScrollbar('update');
                },
                "eleMinusTargetFn":function(){
                    $('.content-dialog-scroll').perfectScrollbar('update');
                },
                "eleMinusTargetFnBefore":function(){
                },
                "elePlusTargetFnBefore" : function(){
                }
            }]
        });
    }
}