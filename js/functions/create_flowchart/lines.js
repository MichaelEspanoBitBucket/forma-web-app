var arrowSize = 15;
var arrow_size = "6px";
var defaultLineSize = 2;
$(document).ready(function(){
	line.removeLine();
	line.lineProperties();
	line.lineReassign();
	$("body").on("mouseover",".lines", function(){
		var line_class = $(this).attr("line-class");
		var parent = $(this).attr("parent");
		var child = $(this).attr("child");
		$(".lines[parent='"+ parent +"'][child='"+ child +"']").not(".arrow").css({
			"box-shadow": "1px 1px 10px #33b5e5",
			"-webkit-transition-duration": "0.5s",
			"-moz-transition-duration": "0.5s",
			"-o-transition-duration": "0.5s",
			"transition-duration": "0.5s",
			"z-index":"2"
		})
		var pathname = window.location.pathname;
		if (pathname!="/user_view/organizational_chart") {
			 $(".lines[parent='"+ parent +"'][child='"+ child +"']").find(".customLineText").addClass("customLineTextActive")	
		}
	})
	$("body").on("mouseleave",".lines", function(){
		var parent = $(this).attr("parent");
		var child = $(this).attr("child");
		$(".lines[parent='"+ parent +"'][child='"+ child +"']").not(".arrow").css({
			"box-shadow": "",
			"z-index":"",
			"-webkit-transition-duration": "",
			"-moz-transition-duration": "",
			"-o-transition-duration": "",
			"transition-duration": "",
		})
		  $(".lines[parent='"+ parent +"'][child='"+ child +"']").find(".customLineText").removeClass("customLineTextActive")	
	})
})
line = {
    startLine:function(element){
		if($(".userLevel").val()!=2){
			return;
		}
		var json_body = $("body").data();
		var json_node = json_body['nodes'];
		//parent id
		var hasDrag = true;
		var element_drag = $(element).find(".node-drag");
		var drop_node_id_parent = $(element_drag).closest(".outer-node").attr("id");
		// console.log(drop_node_id_parent)
		if (typeof json_body['nodes']!="undefined") {
		    var node_json_parent = json_node[''+ drop_node_id_parent +''];
		    if (node_json_parent['node_hasDrag']==false) {
				hasDrag = false;
		    }
		}
		if (hasDrag==true) {
			if(!$("#"+drop_node_id_parent).hasClass("node-drag-wrapper")){
				$(element_drag).draggable({
					revert: "valid",
					containment:shrinkSize(".workspace", 20, 20),
					cursorAt:{"top":10,"left":10},
					start: function(event, ui){
					    // var parentOffset = $(ui.helper).offset();
					    // var relX = event.pageX - parentOffset.left;
					    // var relY = event.pageY - parentOffset.top;
					    $(ui.helper).css({
							"border-top":"none",
					    })
					    $(ui.helper).addClass("node-start-dragging");
					},
					drag: function(event, ui){
					    
					},
					stop: function(event, ui){
					    $(ui.helper).removeClass("node-start-dragging");
					    $(ui.helper).css({
						"margin-left":"0px",
						"margin-top":"0px",
						"border-top":"dotted 2px #a6a6a6",
						"top" : "",
						"left" : ""
					    })
					},
					zIndex:"100",
					opacity: 0.7
				})
			}else{
				$(element_drag).draggable({
					cursorAt:{"top":10,"left":10},
					helper: "clone",
					start : function(event, ui){
						// $(".node-drag-wrapper").unbind('mouseenter mouseleave')
						node.mouseLeaveFN(element,"drag");
						$(ui.helper).addClass("ele-dragging");
					},
					stop : function(event, ui){
						// $(".node-drag-wrapper").bind('mouseenter mouseleave')
						$(ui.helper).removeClass("ele-dragging");
						node.mouseoverNode();
						node.mouseleaveNode();
						node.mouseLeaveFN(element,"hover");
					},
					opacity: 0.5
				})
			}
			
		}
    },
    createLine:function(parent_args,child_args,container,lineColor,json_title){
	var parent_id = parent_args.attr("data-node-id");
	var child_id = $(child_args).closest(".outer-node").attr("data-node-id");
	
	var json = setLineAttribute(parent_id,child_id,lineColor);
	var json_body = $("body").data();
	    var line_class = parent_id+"_"+child_id;
	
	json_linedata = (json_body['lines'] == null)?{}:json_body['lines'];
	if (json_title==null) {
	    json_title = "";
	}
	//for parent
	json_linedata[''+ line_class +'_parentLine'] = {};
	json_linedata[''+ line_class +'_parentLine']['line-class'] = line_class;
	json_linedata[''+ line_class +'_parentLine']['parent'] = json.parent_id;
	json_linedata[''+ line_class +'_parentLine']['child'] = json.child_id;
	json_linedata[''+ line_class +'_parentLine']['top'] = json.parentLineTop;
	json_linedata[''+ line_class +'_parentLine']['left'] = json.parentLineLeft;
	json_linedata[''+ line_class +'_parentLine']['width'] = json.parentLineWidth;
	json_linedata[''+ line_class +'_parentLine']['height'] = json.parentLineHeight;
	json_linedata[''+ line_class +'_parentLine']['background-color'] = lineColor;
	json_linedata[''+ line_class +'_parentLine']['line-type'] = "parentLine";
	json_linedata[''+ line_class +'_parentLine']['json_title'] = json_title;
	
	//for middle
	json_linedata[''+ line_class +'_middleLine'] = {};
	json_linedata[''+ line_class +'_middleLine']['line-class'] = line_class;
	json_linedata[''+ line_class +'_middleLine']['parent'] = json.parent_id;
	json_linedata[''+ line_class +'_middleLine']['child'] = json.child_id;
	json_linedata[''+ line_class +'_middleLine']['top'] = json.middleLineTop;
	json_linedata[''+ line_class +'_middleLine']['left'] = json.middleLineLeft;
	json_linedata[''+ line_class +'_middleLine']['width'] = json.middleLineWidth;
	json_linedata[''+ line_class +'_middleLine']['height'] = json.middleLineHeight;
	json_linedata[''+ line_class +'_middleLine']['background-color'] = lineColor;
	json_linedata[''+ line_class +'_middleLine']['line-type'] = "middleLine";
	json_linedata[''+ line_class +'_middleLine']['json_title'] = json_title;
	
	//for child
	json_linedata[''+ line_class +'_childLine'] = {};
	json_linedata[''+ line_class +'_childLine']['line-class'] = line_class;
	json_linedata[''+ line_class +'_childLine']['parent'] = json.parent_id;
	json_linedata[''+ line_class +'_childLine']['child'] = json.child_id;
	json_linedata[''+ line_class +'_childLine']['top'] = json.childLineTop;
	json_linedata[''+ line_class +'_childLine']['left'] = json.childLineLeft;
	json_linedata[''+ line_class +'_childLine']['width'] = json.childLineWidth;
	json_linedata[''+ line_class +'_childLine']['height'] = json.childLineHeight;
	json_linedata[''+ line_class +'_childLine']['background-color'] = lineColor;
	json_linedata[''+ line_class +'_childLine']['line-type'] = "childLine";
	json_linedata[''+ line_class +'_childLine']['json_title'] = json_title;
	
	//for arrow
	json_linedata[''+ line_class +'_arrow'] = {};
	json_linedata[''+ line_class +'_arrow']['line-class'] = line_class;
	json_linedata[''+ line_class +'_arrow']['parent'] = json.parent_id;
	json_linedata[''+ line_class +'_arrow']['child'] = json.child_id;
	json_linedata[''+ line_class +'_arrow']['top'] = json.arrowLineTop;
	json_linedata[''+ line_class +'_arrow']['left'] = json.arrowLineLeft;
	json_linedata[''+ line_class +'_arrow']['border'] = json.arrowLineBorder;
	json_linedata[''+ line_class +'_arrow']['json_title'] = json_title;
	json_linedata[''+ line_class +'_arrow']['line-type'] = "arrow";
	json_body['lines'] = json_linedata;


	$("body").data(json_body);
	$(container).append('<span class="lines parentLine" json_title="'+ json_title +'" line-class="'+ line_class +'" parent="'+ json.parent_id +'" child="'+ json.child_id +'" style="top:'+ json.parentLineTop +'px;left:'+ json.parentLineLeft +'px;width:'+ json.parentLineWidth +'px;height:'+ json.parentLineHeight +'px;background-color:'+ lineColor +';"></span>'+
			    '<span class="lines middleLine" json_title="'+ json_title +'" line-class="'+ line_class +'" parent="'+ json.parent_id +'" child="'+ json.child_id +'" style="top:'+ json.middleLineTop +'px;left:'+ json.middleLineLeft +'px;width:'+ json.middleLineWidth +'px;height:'+ json.middleLineHeight +'px;background-color:'+ lineColor +';"></span>'+
			    '<span class="lines childLine" json_title="'+ json_title +'" line-class="'+ line_class +'" parent="'+ json.parent_id +'" child="'+ json.child_id +'" style="top:'+ json.childLineTop +'px;left:'+ json.childLineLeft +'px;width:'+ json.childLineWidth +'px;height:'+ json.childLineHeight +'px;background-color:'+ lineColor +';"></span>'+
			    '<span class="lines arrow" json_title="'+ json_title +'" line-class="'+ line_class +'" parent="'+ json.parent_id +'" child="'+ json.child_id +'" style="top:'+ json.arrowLineTop +'px;left:'+ json.arrowLineLeft +'px;'+ json.arrowLineBorder +'"></span>');
	//append button name
                                customTextLine(line_class,json_title);
	if(json_body['lines']){
		var lines_data = json_body['lines'];
		var jsonAppendAttr = getNodeAttribute(json.parent_id,json.child_id); // to get position of parent and child of the just appended line
		var jsonDataAttr = "";

		
	}
	moveLines(json.parent_id,lineColor);
	moveLines(json.child_id,lineColor);
	line.lineReassign();
    },
    removeLine : function(){
		$("body").on("click","#deleteLines",function(){
			var userLevel = $(".userLevel").val();
			if(userLevel!=2){
				return;
			}
			var parent_id = $(this).attr("parent");
    		var child_id = $(this).attr("child");
		    var dis = $("[line-class='"+ parent_id +"_"+ child_id +"']");
		    var conf = "Are you sure you want to remove this line?";
		    var newConfirm = new jConfirm(conf, 'Confirmation Dialog','', '', '', function(r){
			
				if(r==true){
				    deleteLine(dis);
				}
		    });
			newConfirm.themeConfirm("confirm2", {
				'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>' 
			});
		})
    },
    lineProperties : function(){
    	$("body").on("click",".lines",function(){
    		var userLevel = $(".userLevel").val();
			if(userLevel!=2){
				return;
			}

    		var parent_id = $(this).attr("parent");
    		var child_id = $(this).attr("child");
    		var body_data = $("body").data();
    		var line_class = parent_id+"_"+child_id;
    		var lines = body_data['lines'];
			var nodes = body_data['nodes'];

			if(nodes[''+ parent_id +'']['type_rel']=="3" || nodes[''+ parent_id +'']['type_rel']=="0" || nodes[''+ parent_id +'']['type_rel']=="5"){

				// for condition
				var dis = $("[line-class='"+ parent_id +"_"+ child_id +"']");;
			    var conf = "Are you sure you want to remove this line?";
			    var newConfirm = new jConfirm(conf, 'Confirmation Dialog','', '', '', function(r){
				
					if(r==true){
					    deleteLine(dis);
					}
			    });
				newConfirm.themeConfirm("confirm2", {
					'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>' 
				});

				return;
			}


			//for buttons
			var json_title = lines[''+ parent_id +'_'+ child_id +'_arrow']['json_title'];

    		// button lines
    		object_type = "workflow_chart_buttons";
    		var newDialog = new jDialog(object_properties_node(object_type,parent_id,child_id), "","800", "", "", function(){});
            newDialog.themeDialog("modal2");
            $(".fl-footDialog").append('<input type="button" class="fl-margin-right fl-buttonEffect btn-basicBtn fl-default-btn" value="&nbsp;Delete this Line&nbsp;" id="deleteLines" parent="'+ parent_id +'" child="'+ child_id +'" style="width:auto">');
             var button_selected = if_undefinded(nodes[''+ parent_id +'']['buttonStatus'][''+ json_title +''],"");

             
         		
	            var uiHelper = $("#"+parent_id);
	            var drop_node_this = $("#"+child_id);
	            var container = '.workspace';
	            
	            var dis = this;
	             //click to save json to node
	            
					
	            workflow_actions.save_node_settings(function(line_color,json_title){

                    line.createLine(uiHelper,drop_node_this,container,line_color,json_title);
                    // customTextLine(line_class,json_title);

	            })
	         if(button_selected){
	         	 //set color picker for line
	         	 if(button_selected['message']){
	         	 	$("#msgModalProcess").val(button_selected['message']);
	         	 }
	         	 if(button_selected['message_success']){
	         	 	$("#message_success").val(button_selected['message_success'])
	         	 }
               
	            $("#wokflow_button-line-color").val(button_selected['wokflow_button_line_color'])
	            $("#wokflow_button-line-color").spectrum({
	                color: button_selected['wokflow_button_line_color'],
	                appendTo: $("#wokflow_button-line-color").parent()
	            });
	            if (button_selected['require_comment']==true) {
					$("#wokflow_button-require").prop("checked",true);
				}
				var wf_landing_page = if_undefinded(button_selected['wf_landing_page'],false);
				if(wf_landing_page){
					$("#node-landing-page option").removeAttr("selected");
					$("#node-landing-page").val(button_selected['wf_landing_page']);
				}
             }else{
				$("#wokflow_button-line-color").spectrum({
	                color: "#424242",
	                appendTo: $("#wokflow_button-line-color").parent()
	            });
			 }

			$( "#accordion-container" ).tabs({
                activate : function(){
                    $('.content-dialog-scroll').perfectScrollbar('update');
                }
            })

            CheckProperties.setCheckAllChecked(".fieldRequired",".f_required",".checkAllFields");
            CheckProperties.bindOnClickCheckBox(".fieldRequired",".f_required",".checkAllFields");

    		// condition lines
		})
    },

    "lineReassign" : function(){
    	$(".lines").draggable({
			cursorAt:{"top":10,"left":10},
			helper: function( event ) {
	        	// return $( "<div class='ui-widget-header line-reassign' parent='"+ $(this).attr("parent") +"' child='"+ $(this).attr("child") +"' style='z-index:2;'>I'm a custom helper</div>" );
	        	return $( '<div class="node-circle-dragtopoint-wrapper border-circle-node-top line-reassign" parent="'+ $(this).attr("parent") +'" child="'+ $(this).attr("child") +'"  style="z-index:2;"><span class="dragtext"> Drag here to connect</span></div>' );
	      	},
			start : function(event, ui){
				// $(".node-drag-wrapper").unbind('mouseenter mouseleave')
				// node.mouseLeaveFN(element,"drag");
				// $(ui.helper).addClass("ele-dragging");
			},
			stop : function(event, ui){
				// $(".node-drag-wrapper").bind('mouseenter mouseleave')
				// $(ui.helper).removeClass("ele-dragging");
				// node.mouseoverNode();
				// node.mouseleaveNode();
				// node.mouseLeaveFN(element,"hover");
			},
			opacity: 0.5
		})
    }
}
function setLineAttribute(parent_id,child_id,lineColor){
    // Other Vars
    var padding = 5;
    // container of node
    var containerTop = $("#"+parent_id).closest(".workspace").offset().top;
    var containerLeft = $("#"+parent_id).closest(".workspace").offset().left;
    
    // Parent Attributes
    var parent_left = $("#"+parent_id).find(".node").offset().left - containerLeft;
    var parent_top = $("#"+parent_id).find(".node").offset().top - containerTop;
    var parent_width = $("#"+parent_id).find(".node").width();
    var parent_height = $("#"+parent_id).find(".node").height();
    var parent_right = $("#"+parent_id).find(".node").width()+parent_left;
    var parent_bottom = $("#"+parent_id).find(".node").height()+parent_top;
    var parent_halfwidth = parent_width/2;
    var parent_halfheight = parent_height/2;
    var parent_leftCenter = parseInt(parent_left)+parseInt(parent_halfwidth)+padding;
    var parent_topCenter = parseInt(parent_top)+parseInt(parent_halfheight)+padding;
    // Child Attributes
    var child_left = $("#"+child_id+"").find(".node").offset().left - containerLeft;
    var child_top = $("#"+child_id+"").find(".node").offset().top - containerTop;
    var child_width = $("#"+child_id+"").find(".node").width();
    var child_height = $("#"+child_id+"").find(".node").height();
    var child_right = $("#"+child_id+"").find(".node").width()+child_left;
    var child_bottom = $("#"+child_id+"").find(".node").height()+child_top;
    var child_halfwidth = child_width/2;
    var child_halfheight = child_height/2;
    var child_leftCenter = parseInt(child_left)+parseInt(child_halfwidth)+padding;
    var child_topCenter = parseInt(child_top)+parseInt(child_halfheight)+padding;
    
    /* Line Attributes */
    // Parent Line Attributes
    var parentLineLeft = 0;
    var parentLineTop = 0;
    var parentLineWidth = defaultLineSize;
    var parentLineHeight = defaultLineSize;
    var parentLineClass = "";
    var parentLineAttached = 0;
    // Mddle Line Attributes
    var middleLineLeft = 0;
    var middleLineTop = 0;
    var middleLineWidth = defaultLineSize;
    var middleLineHeight = defaultLineSize;
    
    // Child Line Attributes
    var childLineLeft = 0;
    var childLineTop = 0;
    var childLineWidth = defaultLineSize;
    var childLineHeight = defaultLineSize;
    
    //bisaya moves
    var line_class = parent_id+"_"+child_id;
    var json_body = $("body").data();
    var line_class = parent_id+"_"+child_id;
    var json_linedata = (json_body['lines'] == null)?{}:json_body['lines'];
    if (typeof json_linedata[''+ line_class +'_childLine'] === "undefined") {
	var arrowLineColor = lineColor;
    }else{
	var arrowLineColor = json_linedata[''+ line_class +'_childLine']['background-color']
    }
    
    
    
    //For arrow color
    
    // Arrow Line Attributes
    var arrowLineLeft;
    var arrowLineTop;
    var arrowBorder = "";
    var arrowLineAddedCSS = arrowLineColor+";display:block;";
    var arrowLineType = "";
    var arrowAdjustment = 5;
    /* Computations and Conditions */
    var lineAdj = checkAttachedLine(parent_id,child_id)
    var parent_child_type = "";
    if(
	parent_bottom <= child_top &&
	parent_right <= child_left
    ){
    	// checkAttachedLine(parent_id,child_id)
    	
		//console.log("child bottom right");
		parent_child_type = "child bottom right";
		//parent\
		parentLineLeft = parent_right;
		parentLineTop = parent_topCenter+lineAdj['ctrA_parent'];
		parentLineWidth = Math.abs(parent_right-child_leftCenter)-lineAdj['ctrA'];
		parentLineClass = "childBottomRight";
		
		//child
		childLineTop = parent_topCenter+lineAdj['ctrA_parent'];
		childLineLeft = child_leftCenter-lineAdj['ctrA'];
		childLineHeight = Math.abs(parent_topCenter-child_top)-lineAdj['ctrA_parent'];
		
		//arrow "down"
		arrowLineTop = Math.abs(child_top)-arrowAdjustment;
		arrowLineLeft = child_leftCenter-arrowAdjustment-lineAdj['ctrA'];
		arrowLineBorder = "border-left: "+ arrow_size +" solid transparent;border-right: "+ arrow_size +" solid transparent;border-top: "+ arrow_size +" solid "+ arrowLineAddedCSS +"";
		arrowLineType = "arrow-down";

    }else if(
		parent_bottom <= child_top &&
		parent_right >= child_left &&
		parent_left <= child_right
    ){
		//console.log("child bottom bottom");
		parent_child_type = "child bottom bottom";
		//parent
		parentLineLeft = parent_leftCenter;
		parentLineTop = parent_bottom;
		parentLineHeight = Math.abs(parent_bottom-child_top)/2;
		//middle
		middleLineTop = parentLineHeight+parentLineTop;
		middleLineLeft = validatePosition(parent_leftCenter,child_leftCenter,"horizontal");
		middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter);
		// middleLineHeight = 3;
		//child
		childLineLeft = child_leftCenter
		childLineTop = middleLineTop;
		childLineHeight = Math.abs(parent_bottom-child_top)/2;
		//arrow "down"
		arrowLineTop = Math.abs(child_top)-arrowAdjustment;
		arrowLineLeft = child_leftCenter-arrowAdjustment;
		arrowLineBorder = "border-left: "+ arrow_size +" solid transparent;border-right: "+ arrow_size +" solid transparent;border-top: "+ arrow_size +" solid "+ arrowLineAddedCSS +"";
		arrowLineType = "arrow-down";
		// console.log(lineAdj['jsonE']['ctrE_1_middle'])
		if(lineAdj['jsonE']['pos']=="left"){
			arrowLineLeft = child_leftCenter-7-lineAdj['jsonE']['ctrE_1'];
			middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter)-lineAdj['jsonE']['ctrE_1_middle'];
			childLineLeft = child_leftCenter-lineAdj['jsonE']['ctrE_1'];
			if(String(middleLineWidth).substr(0,1)=="-"){
				var json = getNegative(middleLineWidth,middleLineLeft);
				middleLineLeft = json['x_y'];
				middleLineWidth = json['size']+defaultLineSize;
			}
		}else if(lineAdj['jsonE']['pos']=="right"){
			arrowLineLeft = child_leftCenter-7+lineAdj['jsonE']['ctrE_2'];
			middleLineLeft = validatePosition(parent_leftCenter,child_leftCenter,"horizontal")+lineAdj['jsonE']['ctrE_2'];
			middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter)-lineAdj['jsonE']['ctrE_2'];
			childLineLeft = child_leftCenter+lineAdj['jsonE']['ctrE_2'];
			if(String(middleLineWidth).substr(0,1)=="-"){
				var json = getNegative(middleLineWidth,middleLineLeft);
				middleLineLeft = json['x_y']-defaultLineSize;
				middleLineWidth = json['size'];
			}
		}
		if(lineAdj['jsonE_parent']['pos']=="left"){
			parentLineLeft = parent_leftCenter+lineAdj['jsonE_parent']['ctrE_parent_1'];
			middleLineLeft = validatePosition(parent_leftCenter,child_leftCenter,"horizontal")+lineAdj['jsonE_parent']['ctrE_parent_1'];
			middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter)-lineAdj['jsonE']['ctrE_1_middle'];
			if(String(middleLineWidth).substr(0,1)=="-"){
				var json = getNegative(middleLineWidth,middleLineLeft);
				middleLineLeft = json['x_y'];
				middleLineWidth = json['size']+defaultLineSize;
			}
		}else if(lineAdj['jsonE_parent']['pos']=="right"){
			parentLineLeft = parent_leftCenter-lineAdj['jsonE_parent']['ctrE_parent_2'];
			middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter)-lineAdj['jsonE_parent']['ctrE_parent_2'];
			if(String(middleLineWidth).substr(0,1)=="-"){
				var json = getNegative(middleLineWidth,middleLineLeft);
				middleLineLeft = json['x_y']-defaultLineSize;
				middleLineWidth = json['size'];
			}
		}
    }else if(
		parent_bottom <= child_top &&
		parent_left >= child_right
    ){
		//console.log("child bottom left");
		parent_child_type = "child bottom left";
		//parent
		parentLineLeft = child_leftCenter+lineAdj['ctrB'];
		parentLineTop = parent_topCenter+lineAdj['ctrB_parent'];
		parentLineWidth = Math.abs(parent_left-child_leftCenter)-lineAdj['ctrB'];
		
		//child
		childLineTop = parent_topCenter+lineAdj['ctrB_parent'];
		childLineLeft = child_leftCenter+lineAdj['ctrB'];
		childLineHeight = Math.abs(parent_topCenter-child_top)-lineAdj['ctrB_parent'];;
		//arrow "down"
		arrowLineTop = Math.abs(child_top)-arrowAdjustment;
		arrowLineLeft = child_leftCenter-arrowAdjustment+lineAdj['ctrB']
		arrowLineBorder = "border-left: "+ arrow_size +" solid transparent;border-right: "+ arrow_size +" solid transparent;border-top: "+ arrow_size +" solid "+ arrowLineAddedCSS +"";
		arrowLineType = "arrow-down";
    }else if(
		parent_bottom >= child_top &&
		parent_left >= child_right &&
		parent_top <= child_bottom 
    ){
		//console.log("child left left");
		parent_child_type = "child left left";
		//parent
		parentLineLeft = child_right+Math.abs(parent_left-child_right)/2;
		parentLineTop = parent_topCenter;
		parentLineWidth = Math.abs(parent_left-child_right)/2;
		//middle
		middleLineTop = validatePosition(parent_topCenter,child_topCenter,"vertical");
		middleLineHeight = Math.abs(parent_topCenter-child_topCenter);
		// middleLineWidth = 3;
		middleLineLeft = child_right+Math.abs(parent_left-child_right)/2;
		//child
		childLineLeft = child_right;
		childLineTop = child_topCenter;
		childLineWidth = Math.abs(parent_left-child_right)/2;
		//arrow "left"
		arrowLineTop = Math.abs(child_topCenter)-arrowAdjustment;
		arrowLineLeft = child_leftCenter+child_halfwidth-arrowAdjustment;
		arrowLineBorder = "border-top: "+ arrow_size +" solid transparent;border-bottom: "+ arrow_size +" solid transparent;border-right: "+ arrow_size +" solid "+ arrowLineAddedCSS +"";
		arrowLineType = "arrow-left";
		if(lineAdj['jsonF']['pos']=="top"){
			arrowLineTop = Math.abs(child_topCenter)-arrowAdjustment-lineAdj['jsonF']['ctrF_1'];
			childLineTop = child_topCenter-lineAdj['jsonF']['ctrF_1'];
			middleLineHeight = Math.abs(parent_topCenter-child_topCenter)-lineAdj['jsonF']['ctrF_1'];
			if(String(middleLineHeight).substr(0,1)=="-"){
				var json = getNegative(middleLineHeight,middleLineTop);
				middleLineTop = json['x_y']-defaultLineSize;
				middleLineHeight = json['size'];
			}
		}else if(lineAdj['jsonF']['pos']=="bottom"){
			arrowLineTop = Math.abs(child_topCenter)-arrowAdjustment+lineAdj['jsonF']['ctrF_2'];
			childLineTop = child_topCenter+lineAdj['jsonF']['ctrF_2'];
			middleLineTop = validatePosition(parent_topCenter,child_topCenter,"vertical")+lineAdj['jsonF']['ctrF_2'];
			middleLineHeight = Math.abs(parent_topCenter-child_topCenter)-lineAdj['jsonF']['ctrF_2'];
			if(String(middleLineHeight).substr(0,1)=="-"){
				var json = getNegative(middleLineHeight,middleLineTop);
				middleLineTop = json['x_y'];
				middleLineHeight = json['size']+defaultLineSize;
			}
		}
		if(lineAdj['jsonF_parent']['pos']=="top"){
			parentLineTop = parent_topCenter+lineAdj['jsonF_parent']['ctrF_parent_1'];
			middleLineTop = validatePosition(parent_topCenter,child_topCenter,"vertical")+lineAdj['jsonF_parent']['ctrF_parent_1'];
			middleLineHeight = Math.abs(parent_topCenter-child_topCenter)-lineAdj['jsonF_parent']['ctrF_parent_1'];
			if(String(middleLineHeight).substr(0,1)=="-"){
				var json = getNegative(middleLineHeight,middleLineTop);
				middleLineTop = json['x_y']-defaultLineSize;
				middleLineHeight = json['size'];
			}
		}else if(lineAdj['jsonF_parent']['pos']=="bottom"){
			parentLineTop = parent_topCenter-lineAdj['jsonF_parent']['ctrF_parent_2'];
			middleLineHeight = Math.abs(parent_topCenter-child_topCenter)-lineAdj['jsonF_parent']['ctrF_parent_2'];
			if(String(middleLineHeight).substr(0,1)=="-"){
				var json = getNegative(middleLineHeight,middleLineTop);
				middleLineTop = json['x_y'];
				middleLineHeight = json['size']+defaultLineSize;
			}
		}
    }else if(
		parent_top >= child_bottom &&
		parent_left >= child_right
    ){

		//console.log("child top left");
		parent_child_type = "child top left";
		//parent
		parentLineLeft = parent_leftCenter-lineAdj['ctrC_parent'];
		parentLineTop = child_topCenter+lineAdj['ctrC'];
		parentLineHeight = Math.abs(parent_top-child_topCenter)-lineAdj['ctrC'];
		//child
		childLineLeft = child_right;
		childLineTop = child_topCenter+lineAdj['ctrC'];
		childLineWidth = Math.abs(parent_leftCenter-child_right)-lineAdj['ctrC_parent'];
		//arrow "left"
		arrowLineTop = Math.abs(child_topCenter)-arrowAdjustment+lineAdj['ctrC'];
		arrowLineLeft = child_leftCenter+child_halfwidth-arrowAdjustment;
		arrowLineBorder = "border-top: "+ arrow_size +" solid transparent;border-bottom: "+ arrow_size +" solid transparent;border-right: "+ arrow_size +" solid "+ arrowLineAddedCSS +"";
		arrowLineType = "arrow-left";

    }else if(
		parent_top >= child_bottom &&
		parent_left <= child_right &&
		parent_right >= child_left
    ){
		//console.log("child top top");
		parent_child_type = "child top top";
		//parent
		parentLineLeft = parent_leftCenter;
		parentLineTop = child_bottom + Math.abs(parent_top-child_bottom)/2;
		parentLineHeight = Math.abs(parent_top-child_bottom)/2;
		//middle
		middleLineTop = child_bottom + Math.abs(parent_top-child_bottom)/2;
		middleLineLeft = validatePosition(parent_leftCenter,child_leftCenter,"vertical");
		middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter);
		// middleLineHeight = 3;
		//child
		childLineTop = child_bottom;
		childLineLeft = child_leftCenter;
		childLineHeight = (Math.abs(parent_top-child_bottom)/2)+defaultLineSize;
		//arrow "up"
		arrowLineTop = child_topCenter+child_halfheight-arrowAdjustment;
		arrowLineLeft = child_leftCenter-arrowAdjustment;
		arrowLineBorder = "border-left: "+ arrow_size +" solid transparent;border-right: "+ arrow_size +" solid transparent;border-bottom: "+ arrow_size +" solid "+ arrowLineAddedCSS +"";
		arrowLineType = "arrow-up";
		if(lineAdj['jsonG']['pos']=="left"){
			arrowLineLeft = child_leftCenter-arrowAdjustment-lineAdj['jsonG']['ctrG_1'];
			middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter)-lineAdj['jsonG']['ctrG_1'];
			childLineLeft = child_leftCenter-lineAdj['jsonG']['ctrG_1'];
			if(String(middleLineWidth).substr(0,1)=="-"){
				var json = getNegative(middleLineWidth,middleLineLeft);
				middleLineLeft = json['x_y'];
				middleLineWidth = json['size'];
			}
		}else if(lineAdj['jsonG']['pos']=="right"){
			arrowLineLeft = child_leftCenter-arrowAdjustment+lineAdj['jsonG']['ctrG_2'];
			middleLineLeft = validatePosition(parent_leftCenter,child_leftCenter,"horizontal")+lineAdj['jsonG']['ctrG_2'];
			middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter)-lineAdj['jsonG']['ctrG_2'];
			childLineLeft = child_leftCenter+lineAdj['jsonG']['ctrG_2'];
			if(String(middleLineWidth).substr(0,1)=="-"){
				var json = getNegative(middleLineWidth,middleLineLeft);
				middleLineLeft = json['x_y'];
				middleLineWidth = json['size'];
			}
		}
		if(lineAdj['jsonG_parent']['pos']=="left"){
			parentLineLeft = parent_leftCenter+lineAdj['jsonG_parent']['ctrG_parent_1'];
			middleLineLeft = validatePosition(parent_leftCenter,child_leftCenter,"horizontal")+lineAdj['jsonG_parent']['ctrG_parent_1'];
			middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter)-lineAdj['jsonG_parent']['ctrG_parent_1'];
			if(String(middleLineWidth).substr(0,1)=="-"){
				var json = getNegative(middleLineWidth,middleLineLeft);
				middleLineLeft = json['x_y'];
				middleLineWidth = json['size'];
			}
		}else if(lineAdj['jsonG_parent']['pos']=="right"){
			parentLineLeft = parent_leftCenter-lineAdj['jsonG_parent']['ctrG_parent_2'];
			middleLineWidth = Math.abs(parent_leftCenter-child_leftCenter)-lineAdj['jsonG_parent']['ctrG_parent_2'];
			if(String(middleLineWidth).substr(0,1)=="-"){
				var json = getNegative(middleLineWidth,middleLineLeft);
				middleLineLeft = json['x_y'];
				middleLineWidth = json['size'];
			}
		}
    }else if(
		parent_top >= child_bottom &&
		parent_right <= child_left
    ){
		// console.log("child top right");
		parent_child_type = "child top right";
		//parent
		parentLineLeft = parent_leftCenter+lineAdj['ctrD_parent'];
		parentLineTop = child_topCenter+lineAdj['ctrD'];
		parentLineHeight = Math.abs(parent_top-child_topCenter)-lineAdj['ctrD'];;
		//child
		childLineTop = child_topCenter+lineAdj['ctrD'];;
		childLineLeft = parent_leftCenter+lineAdj['ctrD_parent'];
		childLineWidth = Math.abs(parent_leftCenter-child_left)-lineAdj['ctrD_parent'];
		//arrow "right"
		arrowLineTop = child_topCenter-arrowAdjustment+lineAdj['ctrD'];;
		arrowLineLeft = child_left-arrowAdjustment;
		arrowLineBorder = "border-top: "+ arrow_size +" solid transparent;border-bottom: "+ arrow_size +" solid transparent;border-left: "+ arrow_size +" solid "+ arrowLineAddedCSS +"";
		arrowLineType = "arrow-right";
    }else if(
		parent_top <= child_bottom &&
		parent_right <= child_left &&
		parent_bottom >= child_top
    ){
		//console.log("child right right");
		parent_child_type = "child right right";
		//parent
		parentLineLeft = parent_right; //child_leftCenter+Math.abs(parent_leftCenter-child_leftCenter)/2;
		parentLineTop = parent_topCenter;
		parentLineWidth = Math.abs(parent_right-child_left)/2+defaultLineSize;
		//middle
		middleLineTop = validatePosition(parent_topCenter,child_topCenter,"horizontal");
		middleLineHeight = Math.abs(parent_topCenter-child_topCenter);
		// middleLineWidth = 3;
		middleLineLeft = parent_right+Math.abs(parent_right-child_left)/2;
		//child
		childLineLeft = parent_right+Math.abs(parent_right-child_left)/2;
		childLineTop = child_topCenter;
		childLineWidth = Math.abs(parent_right-child_left)/2;
		//arrow "right"
		arrowLineTop = child_topCenter-arrowAdjustment;
		arrowLineLeft = child_left-arrowAdjustment;
		arrowLineBorder = "border-top: "+ arrow_size +" solid transparent;border-bottom: "+ arrow_size +" solid transparent;border-left: "+ arrow_size +" solid "+ arrowLineAddedCSS +"";
		arrowLineType = "arrow-right";
		if(lineAdj['jsonH']['pos']=="top"){
			arrowLineTop = Math.abs(child_topCenter)-arrowAdjustment-lineAdj['jsonH']['ctrH_1'];
			childLineTop = child_topCenter-lineAdj['jsonH']['ctrH_1'];
			middleLineHeight = Math.abs(parent_topCenter-child_topCenter)-lineAdj['jsonH']['ctrH_1'];
			if(String(middleLineHeight).substr(0,1)=="-"){
				var json = getNegative(middleLineHeight,middleLineTop);
				middleLineTop = json['x_y'];
				middleLineHeight = json['size'];
			}
		}else if(lineAdj['jsonH']['pos']=="bottom"){
			arrowLineTop = Math.abs(child_topCenter)-arrowAdjustment+lineAdj['jsonH']['ctrH_2'];
			childLineTop = child_topCenter+lineAdj['jsonH']['ctrH_2'];
			middleLineTop = validatePosition(parent_topCenter,child_topCenter,"vertical")+lineAdj['jsonH']['ctrH_2'];
			middleLineHeight = Math.abs(parent_topCenter-child_topCenter)-lineAdj['jsonH']['ctrH_2'];
			if(String(middleLineHeight).substr(0,1)=="-"){
				var json = getNegative(middleLineHeight,middleLineTop);
				middleLineTop = json['x_y'];
				middleLineHeight = json['size'];
			}
		}
		if(lineAdj['jsonH_parent']['pos']=="top"){
			parentLineTop = parent_topCenter+lineAdj['jsonH_parent']['ctrH_parent_1'];
			middleLineTop = validatePosition(parent_topCenter,child_topCenter,"vertical")+lineAdj['jsonH_parent']['ctrH_parent_1'];
			middleLineHeight = Math.abs(parent_topCenter-child_topCenter)-lineAdj['jsonH_parent']['ctrH_parent_1'];
			if(String(middleLineHeight).substr(0,1)=="-"){
				var json = getNegative(middleLineHeight,middleLineTop);
				middleLineTop = json['x_y'];
				middleLineHeight = json['size'];
			}
		}else if(lineAdj['jsonH_parent']['pos']=="bottom"){
			parentLineTop = parent_topCenter-lineAdj['jsonH_parent']['ctrH_parent_2'];
			middleLineHeight = Math.abs(parent_topCenter-child_topCenter)-lineAdj['jsonH_parent']['ctrH_parent_2'];
			if(String(middleLineHeight).substr(0,1)=="-"){
				var json = getNegative(middleLineHeight,middleLineTop);
				middleLineTop = json['x_y'];
				middleLineHeight = json['size'];
			}
		}
    }else{
    	parent_child_type = "0";
		arrowLineBorder = "border-top: "+ arrow_size +" solid transparent;border-bottom: "+ arrow_size +" solid transparent;border-left: "+ arrow_size +" solid transparent";
		parentLineWidth = 0;
		parentLineHeight = 0;
		childLineWidth = 0;
		childLineHeight = 0;
    }
    
    
    //json to return
    var json = {};
    //parent line
    json.parentLineLeft = parentLineLeft;
    json.parentLineTop = parentLineTop;
    json.parentLineWidth = parentLineWidth;
    json.parentLineHeight = parentLineHeight;
    line_class = parentLineClass;
    //middle line
    json.middleLineTop = middleLineTop;
    json.middleLineLeft = middleLineLeft;
    json.middleLineHeight = middleLineHeight;
    json.middleLineWidth = middleLineWidth;
    //child line
    json.childLineTop = childLineTop;
    json.childLineLeft = childLineLeft;
    json.childLineHeight = childLineHeight;
    json.childLineWidth = childLineWidth;
    //arrow line
    json.arrowLineTop = arrowLineTop;
    json.arrowLineLeft = arrowLineLeft;
    json.arrowLineBorder = arrowLineBorder;
    //other attributes
    json.parent_id = parent_id;
    json.child_id = child_id;
    json['parent_child_type'] = parent_child_type;
    return json;
}

function moveLines(id,lineColor){
    var json_body = $("body").data();
    var json_linedata = json_body['lines'];
    $(".lines").each(function(){
	    if($(this).attr("parent")==id || $(this).attr("child")==id){
			var json = setLineAttribute($(this).attr("parent"),$(this).attr("child"),lineColor);
			var line_class_key = $(this).attr("parent")+"_"+$(this).attr("child");
			if($(this).hasClass("parentLine")){
			    $(this).attr("line-class",line_class_key);
			    $(this).css({
				"top":json.parentLineTop+"px",
				"left":json.parentLineLeft+"px",
				"width":json.parentLineWidth+"px",
				"height":json.parentLineHeight+"px"
			    });
			    json_linedata[''+ line_class_key +'_parentLine']['top'] = json.parentLineTop;
			    json_linedata[''+ line_class_key +'_parentLine']['left'] = json.parentLineLeft;
			    json_linedata[''+ line_class_key +'_parentLine']['width'] = json.parentLineWidth;
			    json_linedata[''+ line_class_key +'_parentLine']['height'] = json.parentLineHeight;
			    if(json.parentLineTop==0){
			    	$(this).find("span").addClass("display");
			    }else{
			    	$(this).find("span").removeClass("display");
			    }
			}else if($(this).hasClass("childLine")){
			    $(this).css({
				"top":json.childLineTop+"px",
				"left":json.childLineLeft+"px",
				"width":json.childLineWidth+"px",
				"height":json.childLineHeight+"px"
			    });
			    json_linedata[''+ line_class_key +'_childLine']['top'] = json.childLineTop;
			    json_linedata[''+ line_class_key +'_childLine']['left'] = json.childLineLeft;
			    json_linedata[''+ line_class_key +'_childLine']['width'] = json.childLineWidth;
			    json_linedata[''+ line_class_key +'_childLine']['height'] = json.childLineHeight;
			}else if($(this).hasClass("middleLine")){
			    $(this).css({
				"top":json.middleLineTop+"px",
				"left":json.middleLineLeft+"px",
				"width":json.middleLineWidth+"px",
				"height":json.middleLineHeight+"px"
			    });
			    json_linedata[''+ line_class_key +'_middleLine']['top'] = json.middleLineTop;
			    json_linedata[''+ line_class_key +'_middleLine']['left'] = json.middleLineLeft;
			    json_linedata[''+ line_class_key +'_middleLine']['width'] = json.middleLineWidth;
			    json_linedata[''+ line_class_key +'_middleLine']['height'] = json.middleLineHeight;
			}else if($(this).hasClass("arrow")){
			    var arrowPosition = 'top:'+json.arrowLineTop +'px;'+'left:'+json.arrowLineLeft +'px';
			    $(this).attr("style",json.arrowLineBorder+arrowPosition)
			    json_linedata[''+ line_class_key +'_arrow']['top'] = json.arrowLineTop;
			    json_linedata[''+ line_class_key +'_arrow']['left'] = json.arrowLineLeft;
			    json_linedata[''+ line_class_key +'_arrow']['border'] = json.arrowLineBorder;
			}
			var text = json_linedata[line_class_key+'_arrow']['json_title'];
			if(text){
				updateLineLabel(line_class_key,text)
			}
	    }
    })
    json_body['lines'] = json_linedata;
    $("body").data(json_body);
}

function getLineAttached(parent_id,child_id,lineclass){
    var json = {};
    var ctr = 0;
    $("span[line-class='"+ lineclass +"']").each(function(){
	if($(this).attr("parent")==parent_id){
	    ctr++;
	}
    })
    json.countParent = ctr;
    return json;
}

//Switch position of lesser value
function validatePosition(value1,value2,type){
    var ret = "";
    var linepixel = 2;
    if(value1<value2){
	ret = value1;
	if(type=="vertical"){
	    ret = value1+linepixel;
	}
    }else{
	ret = value2;
	if(type=="horizontal"){
	    ret = value2+linepixel;
	}
    }
    
    return ret;
}
function shrinkSize(frameId, smallerWidth, smallerHeight) {
    var frameOffset = $(frameId).offset(),
        frameHeight = $(frameId).height()-smallerWidth,
        frameWidth = $(frameId).width()-smallerHeight;
    
    return ([frameOffset.left, frameOffset.top, frameOffset.left+frameWidth, frameOffset.top+frameHeight]);
}
function removePX(str){
	return parseInt(str.replace("px",""));

}

function getNodeAttribute(parent_id,child_id){
	var json = {};
	// container of node
    var containerTop = $("#"+parent_id).closest(".workspace").offset().top;
    var containerLeft = $("#"+parent_id).closest(".workspace").offset().left;
    var padding = 5;
    // Parent Attributes
    json['parent_left'] = $("#"+parent_id).find(".node").offset().left - containerLeft;
    json['parent_top'] = $("#"+parent_id).find(".node").offset().top - containerTop;
    json['parent_width'] = $("#"+parent_id).find(".node").width();
    json['parent_height'] = $("#"+parent_id).find(".node").height();
    json['parent_right'] = $("#"+parent_id).find(".node").width()+json['parent_left'];
    json['parent_bottom'] = $("#"+parent_id).find(".node").height()+json['parent_top'];
    json['parent_halfwidth'] = json['parent_width']/2;
    json['parent_halfheight'] = json['parent_height']/2;
    json['parent_leftCenter'] = parseInt(json['parent_left'])+parseInt(json['parent_halfwidth'])+padding;
    json['parent_topCenter'] = parseInt(json['parent_top'])+parseInt(json['parent_halfheight'])+padding;
    
    json['parent'] = parent_id;

    // Child Attributes
    json['child_left'] = $("#"+child_id+"").find(".node").offset().left - containerLeft;
    json['child_top'] = $("#"+child_id+"").find(".node").offset().top - containerTop;
    json['child_width'] = $("#"+child_id+"").find(".node").width();
    json['child_height'] = $("#"+child_id+"").find(".node").height();
    json['child_right'] = $("#"+child_id+"").find(".node").width()+json['child_left'];
    json['child_bottom'] = $("#"+child_id+"").find(".node").height()+json['child_top'];
    json['child_halfwidth'] = json['child_width']/2;
    json['child_halfheight'] = json['child_height']/2;
    json['child_leftCenter'] = parseInt(json['child_left'])+parseInt(json['child_halfwidth'])+padding;
    json['child_topCenter'] = parseInt(json['child_top'])+parseInt(json['child_halfheight'])+padding;

    json['child'] = child_id;

    return json;
}


function checkAttachedLine(parent,child){
	var childInLoop = "";
	var parentInLoop = "";
	var ctrA = 0;var ctrB = 0;var ctrC = 0;var ctrD = 0;
	var ctrE_1 = 0;var ctrE_2 = 0;
	var ctrF_1 = 0;var ctrF_2 = 0;
	var ctrG_1 = 0;var ctrG_2 = 0;
	var ctrH_1 = 0;var ctrH_2 = 0;
	var ctrA_parent = 0;var ctrB_parent = 0;var ctrC_parent = 0;var ctrD_parent = 0;
	var ctrE_parent_1 = 0;var ctrE_parent_2 = 0;
	var ctrF_parent_1 = 0;var ctrF_parent_2 = 0;
	var ctrG_parent_1 = 0;var ctrG_parent_2 = 0;
	var ctrH_parent_1 = 0;var ctrH_parent_2 = 0;
	var jsonE = {};jsonF = {};jsonG = {};jsonH = {};
	var jsonE_parent = {};jsonF_parent = {};jsonG_parent = {};jsonH_parent = {};
	var jsonAppendAttr = getNodeAttribute(parent,child);
	var jsonDataAttr = "";
	var json = {};
	var ctrD_parent_flag = 0;ctrH_parent_1_flag = 0;
	var ctrE_parent_1_flag = 0;ctrG_1_flag = 0;ctrG_parent_1_flag = 0;
	var ctrE_2_flag = 0;ctrB_flag = 0;ctrF_parent_2_flag = 0;ctrF_parent_1_flag = 0;
	ctrH_parent_2_flag = 0;var ctrH_2_flag = 0;ctrH_1_flag =0;
	ctrF_2_flag = 0;
	// child child
	$(".childLine[child='"+ child +"']").each(function(){
		childInLoop = $(this).attr("child");
		parentInLoop = $(this).attr("parent");

		if(parentInLoop!=parent){

			// console.log(" child "+childInLoop+" || parent "+parentInLoop);
			// console.log($(this))
			var jsonDataAttr = getNodeAttribute(parentInLoop,childInLoop);
			if(
				//console.log("child bottom right");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left
			){
				if(
					jsonDataAttr.parent_top<jsonAppendAttr.parent_top
				){
					ctrA++;
				}else if(
					jsonDataAttr.parent_top==jsonAppendAttr.parent_top &&
					jsonDataAttr.parent_left>jsonAppendAttr.parent_left
				){
					ctrA++;
				}
				if(
					//console.log("child bottom left");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
				){
					// if(ctrB==0){
					// 	ctrB++;
					// 	ctrB_flag++;
					// }
					ctrB_flag++;
					if(ctrB_flag==1){
						ctrB++;
					}
				}
				if(
					//console.log("child bottom bottom");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_right >= jsonAppendAttr.child_left &&
					jsonAppendAttr.parent_left <= jsonAppendAttr.child_right
				){
					var ctrE_1_bool = false;
					if(jsonAppendAttr.parent_left<jsonAppendAttr.child_left){

						if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
							jsonE['pos'] = "left";
							if(jsonDataAttr.parent_top<jsonAppendAttr.parent_top){
								ctrE_1++;
							}
						}
					}else{
						if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
							jsonE['pos'] = "right";
							if(jsonDataAttr.parent_top<jsonAppendAttr.parent_top){
								ctrE_2++;
							}
						}else{
							ctrE_1_bool  = true;
						}
						if(ctrE_2==0 && ctrE_1_bool==true){
							jsonE['pos'] = "right";
							ctrE_2++;
							ctrE_2_flag++;
						}
					}
				}

			}else if(
				//console.log("child bottom left");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right
			){
				if(jsonDataAttr.parent_top<jsonAppendAttr.parent_top){
					ctrB++;
				}else if(
					jsonDataAttr.parent_top==jsonAppendAttr.parent_top &&
					jsonDataAttr.parent_left>jsonAppendAttr.parent_left
				){
					ctrB++;
				}
				if(
					//console.log("child bottom bottom");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_right >= jsonAppendAttr.child_left &&
					jsonAppendAttr.parent_left <= jsonAppendAttr.child_right
				){
					var ctrE_1_bool = false;
					if(jsonAppendAttr.parent_left<jsonAppendAttr.child_left){

						if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
							jsonE['pos'] = "left";
							if(jsonDataAttr.parent_top<jsonAppendAttr.parent_top){
								ctrE_1++;
							}
						}
					}else{
						if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
							jsonE['pos'] = "right";
							if(jsonDataAttr.parent_top<jsonAppendAttr.parent_top){
								ctrE_2++;
							}
						}else{
							ctrE_1_bool  = true;
						}
						if(ctrE_2==0 && ctrE_1_bool==true){
							jsonE['pos'] = "right";
							ctrE_2++;
							ctrE_2_flag++;
						}
					}
				}
			}else if(
				//console.log("child top left");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right
			){

				if(jsonDataAttr.parent_left>jsonAppendAttr.parent_left){
					ctrC++;
				}else if(
					jsonDataAttr.parent_left==jsonAppendAttr.parent_left &&
					jsonDataAttr.parent_top<jsonAppendAttr.parent_top
				){
					ctrC++;
				}
				if(
					//console.log("child left left");
					jsonAppendAttr.parent_bottom >= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right &&
					jsonAppendAttr.parent_top <= jsonAppendAttr.child_bottom 
				){
					var ctrF_1_bool = false;
					if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
						if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
							jsonF['pos'] = "top";
							if(jsonDataAttr.parent_left>jsonAppendAttr.parent_left){
								ctrF_1 ++;
							}
						}
					}else{
						if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
							jsonF['pos'] = "bottom";
							if(jsonDataAttr.parent_left>jsonAppendAttr.parent_left){
								ctrF_2 ++;
							}
						}else{
							ctrF_1_bool  = true;
							ctrF_2_flag++;
							if(ctrF_2_flag==1){
								jsonF['pos'] = "bottom";
								ctrF_2++;
								ctrH_parent_2_flag++;
							}
						}
						// if(ctrF_2==0 && ctrF_1_bool==true){
						// 	jsonF['pos'] = "bottom";
						// 	ctrF_2++;
						// 	ctrH_parent_2_flag++;
						// }
					}
				}
			}else if(
				//console.log("child top right");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left
			){

				if(jsonDataAttr.parent_left<jsonAppendAttr.parent_left){
					ctrD++;
				}else if(
					jsonDataAttr.parent_left==jsonAppendAttr.parent_left &&
					jsonDataAttr.parent_top<jsonAppendAttr.parent_top
				){
					ctrD++;
				}
				if(
					//console.log("child right right");
					jsonAppendAttr.parent_top <= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left &&
					jsonAppendAttr.parent_bottom >= jsonAppendAttr.child_top
				){
					var ctrH_1_bool = false;
					if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
						if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
							jsonH['pos'] = "top";
							if(jsonDataAttr.parent_left<jsonAppendAttr.parent_left){
								ctrH_1 ++;
							}
						}
					}else{
						if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
							jsonH['pos'] = "bottom";
							if(jsonDataAttr.parent_left<jsonAppendAttr.parent_left){
								ctrH_2 ++;
							}
						}else{
							ctrH_1_bool  = true;
						}
						if(ctrH_2==0 && ctrH_1_bool==true){
							jsonH['pos'] = "bottom";
							ctrH_2++;
							ctrH_2_flag++;
						}
					}
				}
			}else if(
				//console.log("child bottom bottom");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_right >= jsonDataAttr.child_left &&
				jsonDataAttr.parent_left <= jsonDataAttr.child_right
			){
				var ctrE_1_bool = false;
				if(jsonAppendAttr.parent_left<jsonAppendAttr.child_left){

					if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
						jsonE['pos'] = "left";
						if(jsonDataAttr.parent_top<jsonAppendAttr.parent_top){
							ctrE_1++;
						}
						if(
							//console.log("child bottom right");
							jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
							jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
						){
							if(jsonDataAttr.parent_top<jsonAppendAttr.parent_top){
								ctrA++;
							}else if(
								jsonDataAttr.parent_top==jsonAppendAttr.parent_top &&
								jsonDataAttr.parent_left>jsonAppendAttr.parent_left
							){
								ctrA++;
							}
						}
					}
				}else{
					if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
						jsonE['pos'] = "right";
						if(jsonDataAttr.parent_top<jsonAppendAttr.parent_top){
							ctrE_2++;
						}
						if(
							//console.log("child bottom left");
							jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
							jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
						){
							if(jsonDataAttr.parent_top<jsonAppendAttr.parent_top){
								ctrB++;
							}else if(
								jsonDataAttr.parent_top==jsonAppendAttr.parent_top &&
								jsonDataAttr.parent_left<jsonAppendAttr.parent_left
							){
								ctrB++;
							}
						}
					}else{
						ctrE_1_bool  = true;
					}
					if(ctrE_2==0 && ctrE_1_bool==true){
						jsonE['pos'] = "right";
						ctrE_2++;
						ctrE_2_flag++;
					}
					if(ctrB==0 && ctrE_1_bool==true){
						ctrB++;
						ctrB_flag++;
					}
				}
			}else if(
				//console.log("child left left");
				jsonDataAttr.parent_bottom >= jsonDataAttr.child_top &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right &&
				jsonDataAttr.parent_top <= jsonDataAttr.child_bottom 
			){
				var ctrF_1_bool = false;
				if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
					if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
						jsonF['pos'] = "top";
						if(jsonDataAttr.parent_left>jsonAppendAttr.parent_left){
							ctrF_1 ++;
						}
					}
				}else{
					if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
						jsonF['pos'] = "bottom";
						if(jsonDataAttr.parent_left>jsonAppendAttr.parent_left){
							ctrF_2 ++;
						}
						if(
							//console.log("child top left");
							jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
							jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
						){
							if(jsonDataAttr.parent_left>jsonAppendAttr.parent_left){
								ctrC++;
							}else if(
								jsonDataAttr.parent_left==jsonAppendAttr.parent_left &&
								jsonDataAttr.parent_top<jsonAppendAttr.parent_top
							){
								ctrC++;
							}
						}
					}else{
						ctrF_1_bool  = true;
						ctrH_parent_1++;
						ctrF_2_flag++;
						if(ctrF_2_flag==1){
							jsonF['pos'] = "bottom";
							ctrF_2++;
							ctrC++;
						}
					}
					// if(ctrF_2==0 && ctrF_1_bool==true){
						

					// }
					if(ctrC == 0 && ctrF_1_bool == true){
						
					}
				}
			}else if(
				//console.log("child top top");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_left <= jsonDataAttr.child_right &&
				jsonDataAttr.parent_right >= jsonDataAttr.child_left
			){
				
				var ctrG_1_bool = false;
				if(jsonAppendAttr.parent_left<jsonAppendAttr.child_left){
					if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
						jsonG['pos'] = "left";
						if(jsonDataAttr.parent_top>jsonAppendAttr.parent_top){
							ctrG_1++;
						}
					}

				}else{
					if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
						jsonG['pos'] = "right";
						if(jsonDataAttr.parent_top>jsonAppendAttr.parent_top){
							ctrG_2++;
						}
					}else{
						ctrG_1_bool  = true;
					}
					if(ctrG_2==0 && ctrG_1_bool==true){
						jsonG['pos'] = "right";
						ctrG_2++;
						ctrG_1_flag ++;
					}
				}
			}else if(
				//console.log("child right right");
				jsonDataAttr.parent_top <= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left &&
				jsonDataAttr.parent_bottom >= jsonDataAttr.child_top
			){

				var ctrH_1_bool = false;
				ctrH_1_flag++;
				ctrF_parent_1_flag++;
				if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
					if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
						jsonH['pos'] = "top";
						if(jsonDataAttr.parent_left<jsonAppendAttr.parent_left){
							ctrH_1 ++;
						}
					}
				}else{
					if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
						jsonH['pos'] = "bottom";
						if(jsonDataAttr.parent_left<jsonAppendAttr.parent_left){
							ctrH_2 ++;
						}
						if(
							//console.log("child top right");
							jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
							jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
						){
							if(jsonDataAttr.parent_left<jsonAppendAttr.parent_left){
								ctrD++;
							}else if(
								jsonDataAttr.parent_left==jsonAppendAttr.parent_left &&
								jsonDataAttr.parent_top<jsonAppendAttr.parent_top
							){
								ctrD++;
							}
						}
					}else{
						ctrH_1_bool  = true;
						ctrH_2_flag++;
						if(ctrH_2_flag==1){
							jsonH['pos'] = "bottom";
							ctrH_2++;
							ctrD++;
						}
					}
				}
				if(ctrD == 0 && ctrH_1_bool == true){
					
				}
			}
		}
	})
	// parent_parent
	$(".childLine[parent='"+ parent +"']").each(function(){
		childInLoop = $(this).attr("child");
		parentInLoop = $(this).attr("parent");
		if(childInLoop!=child){
			var jsonDataAttr = getNodeAttribute(parentInLoop,childInLoop);
			if(
				//console.log("child bottom right");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left
			){

				if(jsonDataAttr.child_left>jsonAppendAttr.child_left){
					ctrA_parent++;
				}else if(
					jsonDataAttr.child_left==jsonAppendAttr.child_left && 
					jsonDataAttr.child_top>jsonAppendAttr.child_top
				){
					ctrA_parent++;
				}
				if(
					//console.log("child right right");
					jsonAppendAttr.parent_top <= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left &&
					jsonAppendAttr.parent_bottom >= jsonAppendAttr.child_top
				){
					ctrH_parent_1_flag++;
					if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
						if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
							jsonH_parent['pos'] = "top";
							if(jsonDataAttr.child_left>jsonAppendAttr.child_left){
								ctrH_parent_1 ++;
							}
						}else{
							if(ctrH_parent_1_flag==1){
								jsonH_parent['pos'] = "top";
								ctrH_parent_1 ++;
							}
						}
					}else{
						if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
							jsonH_parent['pos'] = "bottom";
							if(jsonDataAttr.child_left>jsonAppendAttr.child_left){
								ctrH_parent_2 ++;
							}
							
						}
					}
				}
			}else if(
				//console.log("child bottom left");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right
			){
				if(jsonDataAttr.child_left<jsonAppendAttr.child_left){
					ctrB_parent++;
				}else if(
					jsonDataAttr.child_left==jsonAppendAttr.child_left &&
					jsonDataAttr.child_top<jsonAppendAttr.child_top
				){
					ctrB_parent++;
				}
				if(
					//console.log("child left left");
					jsonAppendAttr.parent_bottom >= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right &&
					jsonAppendAttr.parent_top <= jsonAppendAttr.child_bottom 
				){
					
					if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
						if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
							jsonF_parent['pos'] = "top";
							if(jsonDataAttr.child_left<jsonAppendAttr.child_left){
								ctrF_parent_1 ++;
							}
						}else{
							ctrF_parent_1_flag++;
							if(ctrF_parent_1_flag==1){
								jsonF_parent['pos'] = "top";
								ctrF_parent_1 ++;
							}
						}
					}else{
						if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
							jsonF_parent['pos'] = "bottom";
							if(jsonDataAttr.child_left<jsonAppendAttr.child_left){
								ctrF_parent_2 ++;
							}
						}
					}
				}
			}else if(
				//console.log("child top left");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right
				
			){
				if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
					ctrC_parent++;
				}
				ctrD_parent_flag++;
				if(
					//console.log("child top right");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
				){
					// ctrD_parent++;
					if(ctrD_parent_flag==1){
						ctrD_parent++;
					}
				}
				if(
					//console.log("child top top");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_left <= jsonAppendAttr.child_right &&
					jsonAppendAttr.parent_right >= jsonAppendAttr.child_left
				){
					var ctrG_parent_2_bool = false;
					if(jsonAppendAttr.parent_left>jsonAppendAttr.child_left){
						if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
							jsonG_parent['pos'] = "right";
							if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
								ctrG_parent_2++;
							}
						}
					}else{
						if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
							jsonG_parent['pos'] = "left";
							if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
								ctrG_parent_1++;
							}
						}else{
							ctrG_parent_2_bool  = true;
							ctrG_parent_1_flag++;
							if(ctrG_parent_1_flag==1){
									jsonG_parent['pos'] = "left";
									ctrG_parent_1++;
							}
						}
						// if(ctrG_parent_1==0 && ctrG_parent_2_bool==true){
						// 	jsonG_parent['pos'] = "left";
						// 	ctrG_parent_1++;
						// }
					}
				}
			}else if(
				//console.log("child top right");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left
				
			){
				if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
					ctrD_parent++;
				}
				if(
					//console.log("child top top");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_left <= jsonAppendAttr.child_right &&
					jsonAppendAttr.parent_right >= jsonAppendAttr.child_left
				){
					var ctrG_parent_2_bool = false;
					if(jsonAppendAttr.parent_left>jsonAppendAttr.child_left){
						if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
							jsonG_parent['pos'] = "right";
							if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
								ctrG_parent_2++;
							}
						}
					}else{
						if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
							jsonG_parent['pos'] = "left";
							if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
								ctrG_parent_1++;
							}
						}else{
							ctrG_parent_2_bool  = true;
						}
						if(ctrG_parent_1==0 && ctrG_parent_2_bool==true){
							jsonG_parent['pos'] = "left";
							ctrG_parent_1++;
						}
					}
				}
			}else if(
				//console.log("child bottom bottom");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_right >= jsonDataAttr.child_left &&
				jsonDataAttr.parent_left <= jsonDataAttr.child_right
			){
				var ctrE_parent_1_bool = false;
				if(jsonAppendAttr.parent_left<jsonAppendAttr.child_left){
					if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
						jsonE_parent['pos'] = "left";
						if(jsonDataAttr.child_top>jsonAppendAttr.child_top){
							ctrE_parent_1++;
						}
					}else{
						ctrE_parent_1_bool = true
						ctrE_parent_1_flag++;
						if(ctrE_parent_1_flag==1){
							jsonE_parent['pos'] = "left";
							ctrE_parent_1++;
						}
					}
					
				}else{
					if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
						jsonE_parent['pos'] = "right";
						if(jsonDataAttr.child_top>jsonAppendAttr.child_top){
							ctrE_parent_2++;
						}
					}
				}
				
			}else if(
				//console.log("child left left");
				jsonDataAttr.parent_bottom >= jsonDataAttr.child_top &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right &&
				jsonDataAttr.parent_top <= jsonDataAttr.child_bottom 
			){
				var ctrF_parent_1_bool = false;
				ctrH_1_flag++;
				
				ctrH_2_flag++;
				if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
					if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
						jsonF_parent['pos'] = "top";
						if(jsonDataAttr.child_left<jsonAppendAttr.child_left){
							ctrF_parent_1 ++;
						}
						if(
							//console.log("child bottom left");
							jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
							jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
						){
							if(jsonDataAttr.child_left<jsonAppendAttr.child_left){
								ctrB_parent++;
							}else if(
								jsonDataAttr.child_left==jsonAppendAttr.child_left &&
								jsonDataAttr.child_top<jsonAppendAttr.child_top
							){
								ctrB_parent++;
							}
							
						}
					}else{
						ctrF_parent_1_flag++;
						if(ctrF_parent_1_flag==1){
							jsonF_parent['pos'] = "top";
							ctrF_parent_1 ++;
							ctrB_parent++;
						}
						ctrF_parent_1_bool = true;
					}
				}else{
					if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
						jsonF_parent['pos'] = "bottom";
						if(jsonDataAttr.child_left<jsonAppendAttr.child_left){
							ctrF_parent_2 ++;
						}
					}
				}
				// if(ctrB_parent==0 && ctrF_parent_1_bool==true){
				// 	ctrB_parent++;
				// }
			}else if(
				//console.log("child top top");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_left <= jsonDataAttr.child_right &&
				jsonDataAttr.parent_right >= jsonDataAttr.child_left
			){
				var ctrG_parent_2_bool = false;
				ctrD_parent_flag++;
				if(jsonAppendAttr.parent_left>jsonAppendAttr.child_left){
					if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
						jsonG_parent['pos'] = "right";
						if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
							ctrG_parent_2++;
						}
						if(
							//console.log("child top left");
							jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
							jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
						){
							if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
								ctrC_parent++;
							}
						}
					}
				}else{
					if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
						jsonG_parent['pos'] = "left";
						if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
							ctrG_parent_1++;
						}
					}else{
						ctrG_parent_2_bool  = true;
					}
					if(
						//console.log("child top right");
						jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
						jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
					){
						// if(jsonDataAttr.child_top<jsonAppendAttr.child_top){
						// 	ctrD_parent++;
						// }
					}
					if(ctrG_parent_1==0 && ctrG_parent_2_bool==true){
						jsonG_parent['pos'] = "left";
						ctrG_parent_1++;
						ctrG_parent_1_flag++;
					}
					
					if(ctrD_parent_flag==1){
						ctrD_parent++;
					}
				}
			}else if(
				//console.log("child right right");
				jsonDataAttr.parent_top <= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left &&
				jsonDataAttr.parent_bottom >= jsonDataAttr.child_top
			){
				var ctrH_parent_1_bool = false;
				
				if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
					if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
						jsonH_parent['pos'] = "top";
						if(jsonDataAttr.child_left>jsonAppendAttr.child_left){
							ctrH_parent_1 ++;
						}
						if(
							//console.log("child bottom right");
							jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
							jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
						){
							if(jsonDataAttr.child_left>jsonAppendAttr.child_left){
								ctrA_parent++;
							}else if(
								jsonDataAttr.child_left==jsonAppendAttr.child_left && 
								jsonDataAttr.child_top<jsonAppendAttr.child_top
							){
								ctrA_parent++;
							}
						}
					}else{
						ctrH_parent_1_bool  = true;
						ctrH_parent_1_flag++;
						if(ctrH_parent_1_flag==1){
							jsonH_parent['pos'] = "top";
							ctrH_parent_1 ++;
						}
					}
				}else{
					if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
						jsonH_parent['pos'] = "bottom";
						if(jsonDataAttr.child_left>jsonAppendAttr.child_left){
							ctrH_parent_2 ++;
						}
						
					}else{
						// ctrH_parent_1_bool  = true;
						// ctrH_parent_2_flag++;
						// if(ctrH_parent_2_flag==1){
						// 	jsonH_parent['pos'] = "bottom";
						// 	ctrH_parent_2++;
						// }
					}
				}
				if(ctrA_parent==0 && ctrH_parent_1_bool==true){
					ctrA_parent++
				}
			}
		}
	})
	// child parent
	$(".childLine[child='"+ parent +"']").each(function(){
		childInLoop = $(this).attr("child");
		parentInLoop = $(this).attr("parent");
		// if(parentInLoop!=child){
			var jsonDataAttr = getNodeAttribute(parentInLoop,childInLoop);
			if(
				//console.log("child bottom right");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left
			){
				if(
					//console.log("child top left");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
				){
					if(jsonDataAttr.parent_top<=jsonAppendAttr.child_top){
						ctrC_parent++;
					}
				}
				ctrD_parent_flag++;
				if(
					//console.log("child top right");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
				){
					if(ctrD_parent_flag==1){
						ctrD_parent++;
					}

				}
				if(
					//console.log("child top top");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_left <= jsonAppendAttr.child_right &&
					jsonAppendAttr.parent_right >= jsonAppendAttr.child_left
				){
					if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
						if(jsonAppendAttr.child_left<jsonAppendAttr.parent_left){
							if(jsonAppendAttr.child_top>jsonDataAttr.parent_top){
								jsonG_parent['pos'] = "right";
								ctrG_parent_2++;
							}
						}else{
							ctrG_parent_1_flag++;
							if(ctrG_parent_1_flag==1){
								jsonG_parent['pos'] = "left";
								ctrG_parent_1++;
							}
						}
					}

				}

			}else if(
				//console.log("child top left");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right
			){

				if(
					//console.log("child bottom right");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
				){
					if(jsonAppendAttr.child_left<jsonDataAttr.parent_left){
						ctrA_parent++;
					}
				}
				if(
					//console.log("child right right");
					jsonAppendAttr.parent_top <= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left &&
					jsonAppendAttr.parent_bottom >= jsonAppendAttr.child_top
				){
					if(jsonAppendAttr.child_top<jsonAppendAttr.parent_top){
						if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
							if(jsonAppendAttr.child_left<=jsonDataAttr.parent_left){
								jsonH_parent['pos'] = "bottom";
								ctrH_parent_2 ++;
							}
						}
					}else{
						if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
							if(jsonAppendAttr.child_left<=jsonDataAttr.parent_left){
								jsonH_parent['pos'] = "top";
								ctrH_parent_1 ++;
							}
							if(
								//console.log("child bottom right");
								jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
								jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
							){
								if(jsonAppendAttr.child_left<jsonDataAttr.parent_left){
									ctrA_parent++;
								}
							}
						}else{
							ctrH_parent_1_flag++;
							if(ctrH_parent_1_flag==1){
								jsonH_parent['pos'] = "top";
								ctrH_parent_1 ++;
								ctrA_parent++;
							}
						}
					}
				}
			}else if(
				//console.log("child bottom left");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right
			){
				if(
					//console.log("child top right");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
				){
					if(jsonDataAttr.parent_top<=jsonAppendAttr.child_top){
						ctrD_parent++;
					}
				}
				if(
					//console.log("child top top");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_left <= jsonAppendAttr.child_right &&
					jsonAppendAttr.parent_right >= jsonAppendAttr.child_left
				){
					if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
						if(jsonAppendAttr.child_left>jsonAppendAttr.parent_left){
							if(jsonAppendAttr.child_top>jsonDataAttr.parent_top){
								jsonG_parent['pos'] = "left";
								ctrG_parent_1++;
							}
						}
					}
				}
			}else if(
				//console.log("child top right");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left
			){
				if(
					//console.log("child bottom left");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
				){
					if(jsonDataAttr.parent_left<jsonAppendAttr.child_left){
						ctrB_parent++;
					}
				}
				if(
					//console.log("child left left");
					jsonAppendAttr.parent_bottom >= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right &&
					jsonAppendAttr.parent_top <= jsonAppendAttr.child_bottom 
				){
					ctrH_parent_1_flag++;
					if(jsonAppendAttr.child_top<jsonAppendAttr.parent_top){

						if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
							if(jsonAppendAttr.child_left<=jsonDataAttr.parent_left){
								jsonF_parent['pos'] = "bottom";
								ctrF_parent_2 ++;
							}
						}
					}else{
						if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
							console.log(jsonDataAttr.parent_left+" "+jsonDataAttr.child_left+" "+jsonAppendAttr.parent_left+" "+jsonAppendAttr.child_left);
							if(jsonAppendAttr.child_left>jsonDataAttr.parent_left){
								jsonF_parent['pos'] = "top";
								ctrF_parent_1 ++;
							}
						}else{
							if(ctrH_parent_1_flag==1){
								jsonF_parent['pos'] = "top";
								ctrF_parent_1 ++;
							}
						}
					}
				}
			}else if(
				//console.log("child top top");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_left <= jsonDataAttr.child_right &&
				jsonDataAttr.parent_right >= jsonDataAttr.child_left
			){
				
				if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
					if(jsonAppendAttr.child_left>jsonAppendAttr.parent_left){
						if(jsonDataAttr.parent_top>=jsonAppendAttr.child_top){
							jsonE_parent['pos'] = "left";
							ctrE_parent_1++;
						}
					}
				}else{
					if(jsonAppendAttr.child_left<jsonAppendAttr.parent_left){
						if(jsonDataAttr.parent_top>=jsonAppendAttr.child_top){
							jsonE_parent['pos'] = "right";
							ctrE_parent_2++;
						}
					}else{
						ctrE_parent_1_flag++;

						if(ctrE_parent_1_flag==1){
							jsonE_parent['pos'] = "left";
							ctrE_parent_1++;
						}
					}
				}
			}else if(
				//console.log("child bottom bottom");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_right >= jsonDataAttr.child_left &&
				jsonDataAttr.parent_left <= jsonDataAttr.child_right
			){
				
				if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
					if(jsonAppendAttr.child_left>jsonAppendAttr.parent_left){
						if(jsonAppendAttr.child_top>=jsonDataAttr.parent_top){
							jsonG_parent['pos'] = "left";
							ctrG_parent_1++;
						}
					}
				}else{
					if(jsonAppendAttr.child_left<jsonAppendAttr.parent_left){
						if(jsonAppendAttr.child_top>=jsonDataAttr.parent_top){
							jsonG_parent['pos'] = "right";
							ctrG_parent_2++;
						}
					}else{
						ctrG_parent_1_flag++;
						if(ctrG_parent_1_flag==1){
							jsonG_parent['pos'] = "left";
							ctrG_parent_1++;
						}
					}
				}
				if(
					//console.log("child top left");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
				){
					if(jsonDataAttr.parent_left<jsonDataAttr.child_left){
						if(jsonAppendAttr.child_left<jsonAppendAttr.parent_left){
							if(jsonAppendAttr.child_top>jsonDataAttr.parent_top){
								ctrC_parent++;
							}
						}
					}
					
				}
				if(
					//console.log("child top right");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
				){
					ctrD_parent_flag++;
					if(jsonDataAttr.parent_left>jsonDataAttr.child_left){
						if(jsonAppendAttr.child_left>jsonAppendAttr.parent_left){
							if(jsonAppendAttr.child_top>jsonDataAttr.parent_top){
								ctrD_parent++;
							}
						}
					}else{
						if(ctrD_parent_flag==1){
							ctrD_parent++;
						}
					}
				}
			}else if(
				//console.log("child right right");
				jsonDataAttr.parent_top <= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left &&
				jsonDataAttr.parent_bottom >= jsonDataAttr.child_top
			){
				var ctrB_parent_bool = false;
				if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
					if(jsonAppendAttr.child_top<jsonAppendAttr.parent_top){
						if(jsonAppendAttr.child_left>=jsonDataAttr.parent_left){
							jsonF_parent['pos'] = "bottom";
							ctrF_parent_2 ++;
						}
					}else{
						ctrF_parent_1_flag++;
						if(ctrF_parent_1_flag==1){
							jsonF_parent['pos'] = "top";
							ctrF_parent_1 ++;
							ctrB_parent++;
						}
						ctrB_parent_bool = true;
					}
				}else{
					if(jsonAppendAttr.child_top>jsonAppendAttr.parent_top){
						if(jsonAppendAttr.child_left>=jsonDataAttr.parent_left){
							jsonF_parent['pos'] = "top";
							ctrF_parent_1 ++;
						}
						if(
							//console.log("child bottom left");
							jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
							jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
						){
							if(jsonDataAttr.parent_left<jsonAppendAttr.child_left){
								ctrB_parent++;
							}
						}
					}
				}
				if(ctrB_parent==0 && ctrB_parent_bool==true){
					// ctrB_parent++;
				}
			}else if(
				//console.log("child left left");
				jsonDataAttr.parent_bottom >= jsonDataAttr.child_top &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right &&
				jsonDataAttr.parent_top <= jsonDataAttr.child_bottom 
			){
				
				if(jsonAppendAttr.child_top<jsonAppendAttr.parent_top){
					if(jsonDataAttr.parent_top<jsonDataAttr.child_top){
						if(jsonAppendAttr.child_left<=jsonDataAttr.parent_left){
							jsonH_parent['pos'] = "bottom";
							ctrH_parent_2 ++;
						}
					}
				}else{
					if(jsonDataAttr.parent_top>jsonDataAttr.child_top){
						if(jsonAppendAttr.child_left<=jsonDataAttr.parent_left){
							jsonH_parent['pos'] = "top";
							ctrH_parent_1 ++;
						}
						if(
							//console.log("child bottom right");
							jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
							jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
						){
							if(jsonAppendAttr.child_left<jsonDataAttr.parent_left){
								ctrA_parent++;
							}
						}
					}else{
						ctrH_parent_1_flag++;
						if(ctrH_parent_1_flag==1){
							jsonH_parent['pos'] = "top";
							ctrH_parent_1 ++;
							ctrA_parent++;
						}
					}
				}
			}
		// }
	})
	// parent child
	$(".childLine[parent='"+ child +"']").each(function(){
		childInLoop = $(this).attr("child");
		parentInLoop = $(this).attr("parent");
		// if(childInLoop!=parent){
			var jsonDataAttr = getNodeAttribute(parentInLoop,childInLoop);
			if(
				//console.log("child top left");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right
			){
				if(
					//console.log("child bottom right");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
				){
					if(jsonAppendAttr.parent_top>jsonDataAttr.child_top){
						ctrA++;
					}
				}
				if(
					//console.log("child bottom bottom");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_right >= jsonAppendAttr.child_left &&
					jsonAppendAttr.parent_left <= jsonAppendAttr.child_right
				){
					
					if(jsonDataAttr.child_left<jsonDataAttr.parent_left){
						if(jsonAppendAttr.parent_left<jsonAppendAttr.child_left){
							if(jsonAppendAttr.parent_top>jsonDataAttr.child_top){
								jsonE['pos'] = "left";
								ctrE_1++;
							}
						}else{
							ctrE_2_flag++;
							if(ctrE_2_flag==1){
								jsonE['pos'] = "right";
								ctrE_2++;
							}
						}
					}

				}
				if(
					//console.log("child bottom left");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
				){
					ctrB_flag++;
					if(ctrB_flag==1){
						ctrB++;
					}
				}
			}else if(
				//console.log("child bottom right");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left
			){

				if(
					//console.log("child top left");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
				){
					if(jsonAppendAttr.parent_left<=jsonDataAttr.child_left){
						ctrC++;
					}
				}

				if(
					//console.log("child left left");
					jsonAppendAttr.parent_bottom >= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right &&
					jsonAppendAttr.parent_top <= jsonAppendAttr.child_bottom 
				){
					if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
						if(jsonDataAttr.child_top<jsonDataAttr.parent_top){
							if(jsonAppendAttr.parent_left<jsonDataAttr.child_left){
								jsonF['pos'] = "top";
								ctrF_1 ++;
							}	
						}	
					}else{
						if(jsonDataAttr.child_top>jsonDataAttr.parent_top){
							if(jsonAppendAttr.parent_left<jsonDataAttr.child_left){
								jsonF['pos'] = "bottom";
								ctrF_2 ++;
							}	
						}else{
							ctrF_2_flag++;
							if(ctrF_2_flag==1){
								jsonF['pos'] = "bottom";
								ctrF_2 ++;
							}
						}
					}
				}

			}else if(
				//console.log("child top right");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left
			){
				if(
					//console.log("child bottom left");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
				){
					if(jsonAppendAttr.parent_top>jsonDataAttr.child_top){
						ctrB++;
					}
				}
				if(
					//console.log("child bottom bottom");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_right >= jsonAppendAttr.child_left &&
					jsonAppendAttr.parent_left <= jsonAppendAttr.child_right
				){
					if(jsonDataAttr.child_left>jsonDataAttr.parent_left){
						if(jsonAppendAttr.parent_left>jsonAppendAttr.child_left){
							if(jsonAppendAttr.parent_top>jsonDataAttr.child_top){
								jsonE['pos'] = "right";
								ctrE_2++;
							}
						}
					}
				}
				
			}else if(
				//console.log("child bottom left");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right
			){
				if(
					//console.log("child top right");
					jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
				){
					if(jsonAppendAttr.parent_left>=jsonDataAttr.child_left){
						ctrD++;
					}
				}
				if(
					//console.log("child right right");
					jsonAppendAttr.parent_top <= jsonAppendAttr.child_bottom &&
					jsonAppendAttr.parent_right <= jsonAppendAttr.child_left &&
					jsonAppendAttr.parent_bottom >= jsonAppendAttr.child_top
				){
					
					if(jsonDataAttr.child_top<jsonDataAttr.parent_top){
						if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
							if(jsonAppendAttr.parent_left>jsonDataAttr.child_left){
								jsonH['pos'] = "top";
								ctrH_1 ++;
							}
						}else{
							ctrH_2_flag++;
							if(ctrH_2_flag==1){
								jsonH['pos'] = "bottom";
								ctrH_2 ++;
							}
						}
					}else{
						if(jsonAppendAttr.parent_top>jsonAppendAttr.child_top){
							if(jsonAppendAttr.parent_left>jsonDataAttr.child_left){
								jsonH['pos'] = "bottom";
								ctrH_2 ++;
							}
						}
					}
				}
			}else if(
				//console.log("child bottom bottom");
				jsonDataAttr.parent_bottom <= jsonDataAttr.child_top &&
				jsonDataAttr.parent_right >= jsonDataAttr.child_left &&
				jsonDataAttr.parent_left <= jsonDataAttr.child_right
			){
				
				if(jsonAppendAttr.parent_left>jsonAppendAttr.child_left){
					if(jsonDataAttr.child_left>jsonDataAttr.parent_left){
						if(jsonDataAttr.child_top>jsonAppendAttr.parent_top){
							jsonG['pos'] = "right";
							ctrG_2++;
						}
					}else{
						ctrG_1_flag++;
						if(ctrG_1_flag==1){
							jsonG['pos'] = "right";
							ctrG_2++;
						}
					}	
				}else{
					if(jsonDataAttr.child_left<jsonDataAttr.parent_left){
						if(jsonDataAttr.child_top>jsonAppendAttr.parent_top){
							jsonG['pos'] = "left";
							ctrG_1++;
						}
					}
				}
			}else if(
				//console.log("child top top");
				jsonDataAttr.parent_top >= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_left <= jsonDataAttr.child_right &&
				jsonDataAttr.parent_right >= jsonDataAttr.child_left
			){
				
				if(jsonDataAttr.child_left>jsonDataAttr.parent_left){
					if(jsonAppendAttr.parent_left>jsonAppendAttr.child_left){
						if(jsonAppendAttr.parent_top>jsonDataAttr.child_top){
							jsonE['pos'] = "right";
							ctrE_2++;
						}
					}
				}else{
					if(jsonAppendAttr.parent_left<jsonAppendAttr.child_left){
						if(jsonAppendAttr.parent_top>jsonDataAttr.child_top){
							jsonE['pos'] = "left";
							ctrE_1++;
						}
						if(
							//console.log("child bottom right");
							jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
							jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
						){
							if(jsonAppendAttr.parent_top>=jsonDataAttr.child_top){
								ctrA++;
							}
						}
					}else{
						ctrE_2_flag++;
						if(ctrE_2_flag==1){
							jsonE['pos'] = "right";
							ctrE_2++;
						}
					}
				}
				if(
					//console.log("child bottom left");
					jsonAppendAttr.parent_bottom <= jsonAppendAttr.child_top &&
					jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
				){
					ctrB_flag++;
					if(jsonDataAttr.child_left>jsonDataAttr.parent_left){
						if(jsonAppendAttr.parent_left>jsonAppendAttr.child_left){
							if(jsonAppendAttr.parent_top>jsonDataAttr.child_top){
								ctrB++;
							}
						}
					}else{
						if(ctrB_flag==1){
							ctrB++;
						}
					}
				}
			}else if(
				//console.log("child left left");
				jsonDataAttr.parent_bottom >= jsonDataAttr.child_top &&
				jsonDataAttr.parent_left >= jsonDataAttr.child_right &&
				jsonDataAttr.parent_top <= jsonDataAttr.child_bottom 
			){
				if(jsonDataAttr.child_top<jsonDataAttr.parent_top){
					if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
						if(jsonAppendAttr.parent_left>jsonDataAttr.child_left){
							jsonH['pos'] = "top";
							ctrH_1 ++;
						}
					}else{
						ctrH_2_flag++;
						if(ctrH_2_flag==1){
							jsonH['pos'] = "bottom";
							ctrH_2 ++;
							ctrD++;
						}
					}
				}else{
					if(jsonAppendAttr.parent_top>jsonAppendAttr.child_top){
						if(jsonAppendAttr.parent_left>jsonDataAttr.child_left){
							jsonH['pos'] = "bottom";
							ctrH_2 ++;
						}
						if(
							//console.log("child top right");
							jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
							jsonAppendAttr.parent_right <= jsonAppendAttr.child_left
						){

							if(jsonDataAttr.child_left<jsonAppendAttr.parent_left){
								ctrD++;
							}
						}
					}
				}
			}else if(
				//console.log("child right right");
				jsonDataAttr.parent_top <= jsonDataAttr.child_bottom &&
				jsonDataAttr.parent_right <= jsonDataAttr.child_left &&
				jsonDataAttr.parent_bottom >= jsonDataAttr.child_top
			){
				
				if(jsonAppendAttr.parent_top<jsonAppendAttr.child_top){
					if(jsonDataAttr.child_top<jsonDataAttr.parent_top){
						if(jsonAppendAttr.parent_left<jsonDataAttr.child_left){
							jsonF['pos'] = "top";
							ctrF_1 ++;
						}	
					}	
				}else{
					if(jsonDataAttr.child_top>jsonDataAttr.parent_top){
						if(jsonAppendAttr.parent_left<jsonDataAttr.child_left){
							jsonF['pos'] = "bottom";
							ctrF_2 ++;
						}	
						if(
							//console.log("child top left");
							jsonAppendAttr.parent_top >= jsonAppendAttr.child_bottom &&
							jsonAppendAttr.parent_left >= jsonAppendAttr.child_right
						){
							if(jsonAppendAttr.parent_left<=jsonDataAttr.child_left){
								ctrC++;
							}
						}
					}else{
						ctrF_2_flag++;
						if(ctrF_2_flag==1){
							jsonF['pos'] = "bottom";
							ctrF_2 ++;
							ctrC++;
						}
					}
				}
			}
		// }
	})
	ctrA = parseInt(ctrA)*15;ctrA_parent = parseInt(ctrA_parent)*15;
	ctrB = parseInt(ctrB)*15;ctrB_parent = parseInt(ctrB_parent)*15;
	ctrC = parseInt(ctrC)*15;ctrC_parent = parseInt(ctrC_parent)*15;
	ctrD = parseInt(ctrD)*15;ctrD_parent = parseInt(ctrD_parent)*15;
	ctrE_1 = parseInt(ctrE_1)*15;ctrE_2 = parseInt(ctrE_2)*15;
	ctrF_1 = parseInt(ctrF_1)*15;ctrF_2 = parseInt(ctrF_2)*15;
	ctrG_1 = parseInt(ctrG_1)*15;ctrG_2 = parseInt(ctrG_2)*15;
	ctrH_1 = parseInt(ctrH_1)*15;ctrH_2 = parseInt(ctrH_2)*15;
	ctrE_parent_1 = parseInt(ctrE_parent_1)*15;ctrE_parent_2 = parseInt(ctrE_parent_2)*15;
	ctrF_parent_1 = parseInt(ctrF_parent_1)*15;ctrF_parent_2 = parseInt(ctrF_parent_2)*15;
	ctrG_parent_1 = parseInt(ctrG_parent_1)*15;ctrG_parent_2 = parseInt(ctrG_parent_2)*15;
	ctrH_parent_1 = parseInt(ctrH_parent_1)*15;ctrH_parent_2 = parseInt(ctrH_parent_2)*15;
	jsonE['ctrE_1_middle'] = ctrE_1 + ctrE_parent_1;
	jsonE['ctrE_1'] = ctrE_1;jsonE['ctrE_2'] = ctrE_2;
	jsonF['ctrF_1'] = ctrF_1;jsonF['ctrF_2'] = ctrF_2;
	jsonG['ctrG_1'] = ctrG_1;jsonG['ctrG_2'] = ctrG_2;
	jsonH['ctrH_1'] = ctrH_1;jsonH['ctrH_2'] = ctrH_2;
	jsonE_parent['ctrE_parent_1'] = ctrE_parent_1;jsonE_parent['ctrE_parent_2'] = ctrE_parent_2;
	jsonF_parent['ctrF_parent_1'] = ctrF_parent_1;jsonF_parent['ctrF_parent_2'] = ctrF_parent_2;
	jsonG_parent['ctrG_parent_1'] = ctrG_parent_1;jsonG_parent['ctrG_parent_2'] = ctrG_parent_2;
	jsonH_parent['ctrH_parent_1'] = ctrH_parent_1;jsonH_parent['ctrH_parent_2'] = ctrH_parent_2;
	json['ctrA'] = ctrA;json['ctrA_parent'] = ctrA_parent;
	json['ctrB'] = ctrB;json['ctrB_parent'] = ctrB_parent;
	json['ctrC'] = ctrC;json['ctrC_parent'] = ctrC_parent;	
	json['ctrD'] = ctrD;json['ctrD_parent'] = ctrD_parent;
	json['jsonE'] = jsonE; json['jsonE_parent'] = jsonE_parent;	
	json['jsonF'] = jsonF; json['jsonF_parent'] = jsonF_parent;	
	json['jsonG'] = jsonG; json['jsonG_parent'] = jsonG_parent;
	json['jsonH'] = jsonH; json['jsonH_parent'] = jsonH_parent;
	return json;
}
/*
negativeInt = number that has negative,
size = width or height,
x_y = top or left
*/
function getNegative(negativeInt, x_y){
	var json = {};
	var positiveInt = parseInt(String(negativeInt).replace("-",""));
	json['size'] = positiveInt;
	json['x_y'] = x_y-positiveInt;
	return json;
}

function deleteLine(dis){
	if($(dis).length==0){
		return;
	}
	var json_lines = $("body").data();
    var json_lines_var = json_lines['lines'];
    var json_nodes_var = json_lines['nodes'];
    var line_class = $(dis).attr("line-class");
    var parent_id = $(dis).attr("parent");
    var type_rel = json_nodes_var[''+ parent_id +'']['type_rel'];
    var json_title = $(dis).attr("json_title");
    if (type_rel==1 || type_rel==2) {
	//remove button
	delete json_nodes_var[''+ parent_id +'']['buttonStatus'][''+ json_title +''];
    }else if (type_rel==3) {
	//remove Condition
	delete json_nodes_var[''+ parent_id +'']['condition_return'][''+ json_title +''];
    }else if(type_rel==5){
    	delete json_nodes_var[''+ parent_id +'']['buttonStatus'];
    }
    //remove lines on json
    for(var i in json_nodes_var){
		if(json_nodes_var[i]['workflow-default-action']==parent_id){
			json_nodes_var[i]['workflow-default-action'] = "0";
		}
	}
    delete json_lines_var[''+ line_class +'_arrow'];
    delete json_lines_var[''+ line_class +'_childLine'];
    delete json_lines_var[''+ line_class +'_parentLine'];
    delete json_lines_var[''+ line_class +'_middleLine'];
    //remove lines
    $(".lines[line-class='"+line_class+"']").remove();
    moveLines($(dis).attr("parent"),"");
	moveLines($(dis).attr("child"),"");
}