var msgModaldefault = "Are you sure you want to proceed?";
var message_success = "Request has been submitted.";
var landing_page = "application";
function object_properties_node(object_type,parent_id,child_id) {
    
    var json_nodes = $("body").data();
    var json_nodes_var = json_nodes['nodes'];
    var json_lines_var = if_undefinded(json_nodes['lines'],"");
    var ret = "";
    var output = "";
        ret += '<div style="float:left;width:50%;">';
            ret += '<h3 class="pull-left">';
            if(object_type=="workflow_chart-email"){
                ret += '<i class="icon-asterisk fa fa-envelope"></i> ' + "Message Notification";
            }else if(object_type=="workflow_chart-fields-trigger"){
                ret += '<i class="icon-asterisk fa fa-bolt"></i> ' + "Trigger";
            }else if(object_type=="workflow_chart-user_setup"){
                ret += '<i class="icon-user fa fa-user"></i> ' + "Users";
            }else if(object_type=="workflow_chart-processor"){
                ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-processnode"></use></svg> <span>Process Node Properties</span>';
            }else if(object_type=="workflow_chart-start"){
                ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-startnode"></use></svg> <span>Start Node Properties</span>';
            }else if(object_type=="workflow_chart-end"){
                ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-endnode"></use></svg> <span>End Node Properties</span>';
            }else if(object_type=="workflow_chart_buttons"){
                ret += '<i class="icon-gear fa fa-gear"></i> Line Properties';
            }else if(object_type=="workflow_chart-condition"){
                ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-conditionalnode"></use></svg> <span>Conditional Node Properties</span>';
            }else if(object_type=="organizational_chart"){
                ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-deptorgchart"></use></svg> <span>Department Properties</span>';
            }else if(object_type=="workflow_chart-database"){
                ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-databasenode"></use></svg> <span>Database Properties</span>';
            }else{
                ret += '<i class="icon-asterisk fa fa-gear"></i> Properties';
            }
                
            ret += '</h3>';
        ret += '</div>';
        ret += '<div style="float:right;width:50%;text-align:right;margin-top:-5px;">';
            ret += '<input type="button" class="btn-blueBtn settings-to-json fl-margin-right fl-margin-bottom" object_type="'+ object_type +'" id="" value="OK" node-data-id="'+ parent_id +'" style=""> &nbsp;';
            ret += '<input type="button" class="btn-basicBtn fl-default-btn clear-node-settings fl-margin-right fl-margin-bottom" object_type="'+ object_type +'" id="" value="Clear All" node-data-id="'+ parent_id +'" style=""> &nbsp;';
            ret += '<input type="button" class="btn-basicBtn cancel_dialog fl-margin-bottom" object_type="'+ object_type +'" id="popup_cancel" node-data-id="'+ parent_id +'" value="Cancel" onclick="removeDialog(this)">';
        ret += '</div>';
        ret += '<div class="hr"></div>';
        // var display_deletion = "";
        // if($("#enable_deletion").val()=="0" || $("#enable_config_deletion").text()=="0"){
        //     display_deletion = "display:none;";
        // }
    switch (object_type) {
        //ORGANIZATIONAL CHART
        case "organizational_chart":
        //ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; height: 300px;overflow: hidden;" class="ps-container">';
        ret += '<input type="text" value="'+ parent_id +'" class="display node_data_id" >';
        ret +='<div class="content-dialog" style="height: auto;">';
        var orgchart_dept_name = if_undefinded(json_nodes_var[''+ parent_id +'']['node_text'],"");
        var orgchart_dept_code = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_dept_code'],"");
        var orgchart_dept_code_existing = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_dept_code_existing'],"");
        var orgchart_dept_status = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_dept_status'],"");
        var orgchart_dept_code_type = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_dept_code_type'],"");
        var orgchart_node_color = if_undefinded(json_nodes_var[''+ parent_id +'']['node_color'],"#FFF");
        var orgchart_user_head = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_user_head'],"");
        var orgchart_dept_assistant = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_dept_assistant'],"");
        var orgchart_dept_members = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_dept_members'],"");
        var active_departments = $("#active_department").text();

            ret += '<div class="fields_below section clearing fl-field-style">';
                ret += '<div class="input_position_below column div_1_of_1">';
                    ret += '<span class="font-bold" style="font-size:12px !important;">Department Name:ss <font color="red">*</font></span>';
                    ret += '<input type="text" style="" name="" class="form-text obj_prop" id="orgchart-dept-name" value="';
                        if (orgchart_dept_name=="") {
                            ret += "Node";
                        }else{
                            ret += orgchart_dept_name;
                        }
                    ret += '">';
                ret += '</div>';
            ret += '</div>';
            if(orgchart_dept_status==1){
                ret += '<div class="fields_below section clearing fl-field-style">';
                    ret += '<div class="input_position_below column div_1_of_1">';
                        ret += '<span class="font-bold" style="font-size:12px !important;">Department Code:</span>';
                        ret += '<input type="text" name="" class="form-text obj_prop" id="orgchart-dept-code" value="'+ htmlEntities(orgchart_dept_code) +'" disabled="disabled">';
                    ret += '</div>';
                ret += '</div>';
            }else{
                var auto_generated = "";
                if(active_departments!=""){
                    active_departments = JSON.parse(active_departments);
                    if(active_departments.length>0){
                        ret += '<div class="fields_below section clearing fl-field-style">';
                            ret += '<div class="input_position_below column div_1_of_1">';
                                ret += '<span class="font-bold" style="font-size:12px;">Department Code Type:</span>';
                                ret += '<select class="form-select" id="orgchart_dept_code_type" style="margin-top:0px">';
                                    ret += '<option value="0" '+ setSelected("0", orgchart_dept_code_type) +'>Auto Generated</option>';
                                    ret += '<option value="1" '+ setSelected("1", orgchart_dept_code_type) +'>Use Active Department Code</option>';
                                ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                        var display_active = "";
                        if(orgchart_dept_code_type==0){
                            display_active = "display";
                        }
                        ret += '<div class="section clearing fl-field-style fields_below '+ display_active +'">';
                            ret += '<div class="input_position_below column div_1_of_1">';
                                ret += '<span class="font-bold" style="font-size:12px;">Department Code:</span>';
                                ret += '<select class="form-select" id="orgchart-dept-code" style="margin-top:0px">';
                                    for(var i in active_departments){
                                        ret += '<option value="'+ active_departments[i]['department_code'] +'" '+ setSelected(active_departments[i]['department_code'], orgchart_dept_code) +'>'+ active_departments[i]['department'] +' - '+ active_departments[i]['department_code'] +'</option>';
                                    }
                                ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                    }
                }
            }
            ret += '<div class="clearfix"></div>';
            ret += '<div class="fields_below section clearing fl-field-style">';
                ret += '<div class="input_position_below column div_1_of_1 orgchart-node-color">';
                    ret += '<span class="font-bold" style="font-size:12px;">Node Color:</span><Br/>';
                    ret += '<input type="text" class="chngColor" id="orgchart-node-color" data-properties-type="nodeColor" value="'+ htmlEntities(orgchart_node_color) +'">';
                ret += '</div>';
            ret += '</div>';
            ret += '<div style="float:left;width:32.5%;margin-right:10px">';
                ret += '<div class="fields_below section clearing fl-field-style">';
                    ret += '<div class="input_position_below prop_option_list_column_1 column div_1_of_1">';
                        ret += '<span class="font-bold">Head: <font color="red">*</font></span>';
                        ret += '<input type="text" name="" class="form-text orgchart-user-search" id="orgchart-head-search" style="border-bottom-left-radius:0px;border-bottom-right-radius:0px;" placeholder="Search..">';
                        ret += '<div class="loadHeadUser" style="border-top:none !important;background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;">';
                        //ret += '<input type="text" data-tag-id="0" name="" id="orgchart-user-head" class="form-text obj_prop" id="" value="'+ orgchart_user_head +'">';
                        // Get Head User 
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div style="float:left;width:32.5%;margin-right:10px">';
                ret += '<div class="fields_below section clearing fl-field-style">';
                    ret += '<div class="input_position_below column div_1_of_1 prop_option_list_column_2">';
                    ret += '<span class="font-bold">Assistant Head: </span>';
                    ret += '<input type="text" name="" class="form-text orgchart-user-search" id="orgchart-assistant-head-search" style="border-bottom-left-radius:0px;border-bottom-right-radius:0px;" placeholder="Search..">';
                        ret += '<div class="loadAssistantHeadUser" style="border-top:none !important;background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;">';
                        //ret += '<input type="text" data-tag-id="0" name="" id="orgchart-user-head" class="form-text obj_prop" id="" value="'+ orgchart_user_head +'">';
                        
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div style="float:left;width:32.40%;">';
                ret += '<div class="fields_below section clearing fl-field-style">';
                    ret += '<div class="input_position_below column div_1_of_1 prop_option_list_column_3">';
                    ret += '<span class="font-bold">Member: </span>';
                    ret += '<input type="text" name="" class="form-text orgchart-user-search" id="orgchart-member-search" style="border-bottom-left-radius:0px;border-bottom-right-radius:0px;" placeholder="Search..">';
                        ret += '<div class="loadMemberUser" style="border-top:none !important;background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;">';
                        
                        ret += '</div>'
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            ret += '</div>';
            ret +='</div>';
            return ret;
            $('.content-dialog-scroll').perfectScrollbar('update');
            break;
        //WORKFLOW - PROCESSOR NODE
        case "workflow_chart-processor":
            var workflow_processor_type = if_undefinded(json_nodes_var[''+ parent_id +'']['processorType'],"");
            var workflow_processor = if_undefinded(json_nodes_var[''+ parent_id +'']['processor'],"");
            var workflow_status = if_undefinded(json_nodes_var[''+ parent_id +'']['status'],"");
            var workflow_field_enabled = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldEnabled'],"");
            var workflow_field_required = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldRequired'],"");
            var workflow_field_hidden = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldHiddenValue'],"");
            var workflow_field_read_only = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldReadOnly'],"");
            var workflow_processor_button = if_undefinded(json_nodes_var[''+ parent_id +'']['buttonStatus'],"");
            var workflow_default_action = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow-default-action'],"");
            var workflow_escalation_period = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow-escalation-period'],"");
            var workflow_hw_save = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_hw_save'],"");
            var workflow_hw_cancel = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_hw_cancel'],"");
            var node_del_control = if_undefinded(json_nodes_var[''+ parent_id +'']['node_del_control'],"0");
            var fieldEnabled_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldEnabled_access_type'],"2");
            var fieldRequired_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldRequired_access_type'],"2");
            var fieldHiddenValue_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldHiddenValue_access_type'],"2");


            var workflow_notification_message_type = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_notification_message_type'],"");
            var workflow_notification_message = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_notification_message'],"");
            var workflow_notification_enabled = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_notification_enabled'],"1");
            var workflow_delegate_enabled = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_delegate_enabled'],"1");
            var workflow_guest_enabled = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_guest_enabled'],"0");
            
            var form_fields = $(".form_fields").val();
            form_fields = JSON.parse(form_fields);
            ret += '<div id="content-dialog-scroll">';
            ret +='<div class="content-dialog" style="width: 95%;height: auto; padding:0px;">';
                    ret += '<div id="accordion-container" class="flas" style="width:100%">';
                    ret += '    <ul>';
                    ret += '        <li><a href="#tabs-1">Primary Settings</a></li>';
                    ret += '        <li><a href="#tabs-2">Field Settings</a></li>';
                    ret += '    </ul>';
                    ret += '   <div id="tabs-1" style="padding:10px;">';
                        ret += '<div class="section clearing fl-field-style">';
                            ret += '<div class="column div_1_of_3">';
                                ret += '<div class="fields_below">';
                                    ret += '<span class="font-bold">Processor Type: <font color="red">*</font></span>';
                                    ret += '<div class="input_position_below">';
                                        ret += '<select class="form-select" id="workflow-processor-type" style="margin-top:0px">';
                                            ret += '<option value="0" '+ setSelected("0", workflow_processor_type) +'>--Select--</option>';
                                            ret += '<option value="1" '+ setSelected("1", workflow_processor_type) +'>Department Position</option>';
                                            ret += '<option value="2" '+ setSelected("2", workflow_processor_type) +'>Company Position</option>';
                                            ret += '<option value="3" '+ setSelected("3", workflow_processor_type) +'>Employee</option>';
                                            ret += '<option value="4" '+ setSelected("4", workflow_processor_type) +'>Requestor</option>';
                                            ret += '<option value="5" '+ setSelected("5", workflow_processor_type) +'>Field</option>';
                                            ret += '<option value="6" '+ setSelected("6", workflow_processor_type) +'>Group</option>';
                                        ret += '</select>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div class="column div_1_of_3">';
                                ret += '<div class="fields_below">';
                                    ret += '<span class="font-bold">Processor: <font color="red">*</font></span>';
                                    ret += '<div class="input_position_below">';
                                        ret += '<select class="form-select" id="workflow-processor" style="margin-top:0px">';
                                        ret += '<option value="0">--Select--</option>';
                                        ret += '</select>';
                                        ret += '<img src="/images/loader/loading.gif" class="pull-left display processorLoad"/>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div class="column div_1_of_3">';
                                ret += '<div class="fields_below">';
                                    ret += '<span class="font-bold">Status: <font color="red">*</font></span>';
                                    ret += '<div class="input_position_below">';
                                        ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-status" data-properties-type="lblFldName" data-object-id="" placeholder="Status" value="'+ htmlEntities(workflow_status) +'" style="height:32px">';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                        
                        ret += '<div class="section clearing fl-field-style">';
                                ret += '<div class="column div_1_of_1">';
                                ret += '<div class="fields_below">';
                                    ret += '<span class="font-bold">Default Action:</span>';
                                    ret += '<div class="input_position_below">';
                                        ret += '<select class="form-select" id="workflow-default-action" style="margin-top:0px">';
                                        ret += '<option value="0">--Select--</option>';
                                        if(workflow_processor_button){
                                            for(var i in workflow_processor_button){
                                                ret += '<option value="'+ workflow_processor_button[i]['child_id'] +'" '+ setSelected(workflow_processor_button[i]['child_id'], workflow_default_action) +'>'+ workflow_processor_button[i]['button_name'] +'</option>';
                                            }
                                        }
                                        ret += '<option value="cancel" '+ setSelected('cancel', workflow_default_action) +'>Cancel</option>';
                                        ret += '</select>';
                                        ret += '<img src="/images/loader/loading.gif" class="pull-left display processorLoad"/>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>'
                        ret += '<div class="section clearing fl-field-style">';
                            ret += '<div class="column div_1_of_1">';
                                ret += '<div class="fields_below">';
                                    ret += '<span class="font-bold">Save Button hiding formula:</span>';
                                    ret += '<div class="input_position_below">';
                                        ret += '<textarea name="" class="form-textarea tip wfSettings wf-button-formula" title="" id="hw-save" data-ide-properties-type="save-button-hiding-formula" placeholder="">'+ workflow_hw_save +'</textarea>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                        ret += '<div class="section clearing fl-field-style">';
                            ret += '<div class="column div_1_of_1">';
                                ret += '<div class="fields_below">';
                                    ret += '<span class="font-bold">Cancel Button hiding formula:</span>';
                                    ret += '<div class="input_position_below">';
                                        ret += '<textarea name="" class="form-textarea tip wfSettings wf-button-formula" title="" id="hw-cancel" data-ide-properties-type="cancel-button-hiding-formula" placeholder="">'+ workflow_hw_cancel +'</textarea>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                        
                        // ret += '<div style="float:left;width:30%;height: 65px;'+ display_deletion +'">';
                        //     ret += '<div class="fields_below">';
                        //         ret += '<div class="label_below2"></div>';
                        //         ret += '<div class="input_position_below">';
                        //             ret += '<label><input type="checkbox" name="node_del_control" '+ setChecked("1",node_del_control) +' class="css-checkbox" value="1" id="node_del_control"><label for="node_del_control" class="css-label"></label> <span style="letter-spacing: 1px;font-size: 12px;color: #4E4E4E;">Enable Deletion</span></label>';
                        //         ret += '</div>';
                        //     ret += '</div>';
                        // ret += '</div>';
                        ret += '<div class="display" style="float:left;width:30%;margin-right:10px">';
                            ret += '<div class="fields_below">';
                                ret += '<div class="label_below2">Escalation Period(Day/s): </div>';
                                ret += '<div class="input_position_below">';
                                    ret += '<select class="form-select" id="workflow-escalation-period" style="margin-top:0px">';
                                    for (var i = 1; i <= 10; i++) {
                                        ret += '<option value="'+ i +'" '+ setSelected(i, workflow_escalation_period) +'>'+ i +'</option>';
                                    };
                                    ret += '</select>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                        // ret += '<br /><br /><br /><br />';
                       

                        ret += '<div class="formulaContainer" style="width:100%;">';
                            ret += '<div class="fields_below section clearing fl-field-style">';
                                ret += '<div class="input_position_below column div_1_of_1">';
                                    ret += '<div class="" style="float:right;font-size: 11px;font-weight: bold;">';
                                        ret += '    <label>Static: <input type="radio" name="fm-type" class="fm-type formulaType css-checkbox" value="0" id="fm-type0"><label for="fm-type0" class="css-label"></label></label>';
                                        ret += '    <label>Dynamic: <input type="radio" name="fm-type" class="fm-type formulaType css-checkbox" value="1" id="fm-type1"><label for="fm-type1" class="css-label"></label><label>';
                                        ret += '</div>';
                                     ret += '<div>';
                                            ret += '<label class="font-bold"><input type="checkbox" '+ setChecked("1",workflow_notification_enabled) +' name="enabled-notification" class="enabled-notification css-checkbox" id="enabled-notification"><label for="enabled-notification" class="css-label"></label> Enable Notification</label>';    
                                        ret += '</div>';
                                        
                                     ret += '<span>Notification Message: </span>';
                                    ret += '<textarea name="" class="form-textarea tip wfSettings" title="" id="fm-message" data-ide-properties-type="notification-message" placeholder="">'+ workflow_notification_message +'</textarea>';
                                    ret += '<div class="wf-note-style" style="float:left;font-size:11px;color:red">Note: If no notification message is defined, the system will send the default message: (Request for process: Tracking No. XXX)</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                        ret += '<div class="section clearing fl-field-style">';
                            ret += '<div class="column div_1_of_1">';
                                ret += '<span class="font-bold"><label><input type="checkbox" '+ setChecked("1",workflow_delegate_enabled) +' name="workflow_delegate_enabled" class="workflow_delegate_enabled css-checkbox" id="workflow_delegate_enabled"><label for="workflow_delegate_enabled" class="css-label"></label> Enable Delegation</label></span>';    
                            ret += '</div>';
                        ret += '</div>';
                        if($("#go_lite").val() == 1){
                            ret += '<div class="section clearing fl-field-style">';
                                ret += '<div class="column div_1_of_1">';
                                    ret += '<span class="font-bold"><label><input type="checkbox" '+ setChecked("1",workflow_guest_enabled) +' name="workflow_guest_enabled" class="workflow_guest_enabled css-checkbox" id="workflow_guest_enabled"><label for="workflow_guest_enabled" class="css-label"></label> Enable Guest</label></span>';    
                                ret += '</div>';
                            ret += '</div>';
                        }else{
                            ret += '<div class="section clearing fl-field-style">';
                                ret += '<div class="column div_1_of_1">';
                                    ret += '<span class="font-bold"><label><input type="checkbox"  name="" class="c_key css-checkbox" data-original-title="Available in Full Version" disabled="disabled" id=""><label for="workflow_guest_enabled" class="css-label"></label> Enable Guest</label></span>';    
                                ret += '</div>';
                            ret += '</div>';
                        }
                    ret += '    </div>';
                    ret += '    <div id="tabs-2">';
                        ret += '    <div class="pull-left display" style="width: 32%;margin-right:10px">';
                        ret += '        <div class="fields_below">';
                        ret += '            <div class="label_below2">Access Type:</div>';
                        ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                        ret += '                <select class="fl-input-select" id="access_type-field_enabled" rel="field_enabled" style="width:100%">';
                        ret += '                    <option value="1" '+ setSelected('1',fieldEnabled_access_type) +'>All Fields</option>'; //setSelected('1',json['user_access_type'])
                        ret += '                    <option value="2" '+ setSelected('2',fieldEnabled_access_type) +'>Custom</option>';
                        ret += '                </select>';
                        ret += '            </div>';
                        ret += '        </div>';
                        ret += '    </div>';
                        ret += '    <div class="pull-left display" style="width: 32%;margin-right:10px">';
                        ret += '        <div class="fields_below">';
                        ret += '            <div class="label_below2">Access Type:</div>';
                        ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                        ret += '                <select class="fl-input-select" id="access_type-field_hidden" rel="field_hidden" style="width:100%">';
                        ret += '                    <option value="1" '+ setSelected('1',fieldHiddenValue_access_type) +'>All Fields</option>'; //setSelected('1',json['user_access_type'])
                        ret += '                    <option value="2" '+ setSelected('2',fieldHiddenValue_access_type) +'>Custom</option>';
                        ret += '                </select>';
                        ret += '            </div>';
                        ret += '        </div>';
                        ret += '    </div>';
                        ret += '    <div class="pull-left display" style="width: 32%;">';
                        ret += '        <div class="fields_below">';
                        ret += '            <div class="label_below2">Access Type:</div>';
                        ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                        ret += '                <select class="fl-input-select" id="access_type-field_required" rel="field_required" style="width:100%">';
                        ret += '                    <option value="1" '+ setSelected('1',fieldRequired_access_type) +'>All Enabled Fields</option>'; //setSelected('1',json['user_access_type'])
                        ret += '                    <option value="2" '+ setSelected('2',fieldRequired_access_type) +'>Custom</option>';
                        ret += '                </select>';
                        ret += '            </div>';
                        ret += '        </div>';
                        ret += '    </div>';

                        ret += '<div class="clearfix"></div>';
                        ret += '<div style="float: left;width:32%;margin-right:10px" class="container-fields f_enabled">';
                            ret += '<div class="fields_below section clearing fl-field-style">';
                                
                                ret += '<div class="input_position_below column div_1_of_1">';
                                    ret += '<span class="font-bold"><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_enabled_2"><label for="f_enabled_2" class="css-label"></label> Field Enabled: <font color="red"></font></label></span>';
                                    ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                        for (i in form_fields) {
                                            //console.log(form_fields)
                                            if($.trim(form_fields[i])!=""){
                                                ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldEnabled css-checkbox" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_enabled,form_fields[i]) +' id="ID_field_' + form_fields[i] + '"/><label for="ID_field_' + form_fields[i] + '" class="css-label"></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                            }
                                        }
                                    ret += '</div>';
                                    //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-enabled" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_enabled +'">';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                        
                        ret += '<div style="float: left;width:32%;margin-right:10px" class="container-fields f_hidden">';
                            ret += '<div class="fields_below section clearing fl-field-style">';
                               
                                ret += '<div class="input_position_below column div_1_of_1">';
                                     ret += '<span class="font-bold"><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_hidden_2"><label for="f_hidden_2" class="css-label"></label> Field Hidden: <font color="red"></font></label></span>';
                                    ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                        for (i in form_fields) {
                                            //console.log(form_fields)
                                            if($.trim(form_fields[i])!=""){
                                                ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldHiddenValue css-checkbox" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_hidden,form_fields[i]) +' id="Hidden2_' + form_fields[i] + '"/><label for="Hidden2_' + form_fields[i] + '" class="css-label"></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                            }
                                        }
                                    ret += '</div>';
                                    //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-hidden" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_hidden +'">';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';

                        ret += '<div style="float:left;width:32%;" class="container-fields f_required">';
                            ret += '<div class="fields_below section clearing fl-field-style">';
                                
                                ret += '<div class="input_position_below column div_1_of_1">';
                                    ret += '<span><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_required_2"><label for="f_required_2" class="css-label"></label> Field Required: <font color="red"></font></label></span>';
                                    //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-required" data-properties-type="lblFldName" data-object-id="" placeholder="Status" value="'+ workflow_field_required +'">';
                                    ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                    var requireFieldStatus = "";
                                    var requireFieldCss = "";
                                        for (i in form_fields) {
                                            requireFieldStatus = "";
                                            requireFieldCss = "";
                                            //console.log(form_fields)
                                            // console.log(workflow_field_enabled)
                                            if(workflow_field_enabled){
                                                    if(workflow_field_enabled.indexOf(form_fields[i])==-1 || workflow_field_hidden.indexOf(form_fields[i])>=0){
                                                        requireFieldStatus = "disabled='disabled'";
                                                        requireFieldCss = "opacity: 0.4;cursor:not-allowed !important;";
                                                    }
                                            }
                                            if($.trim(form_fields[i])!=""){
                                                ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left;'+ requireFieldCss +'"><input class="fieldRequired css-checkbox" type="checkbox" '+ requireFieldStatus +' name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_required,form_fields[i]) +' id="Required2_' + form_fields[i] + '"/><label for="Required2_' + form_fields[i] + '" class="css-label" style=""></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                            }
                                        }
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';


                        ret += '<div style="float: left;width:23%;display:none" class="container-fields f_readOnly">';
                            ret += '<div class="fields_below">';
                                ret += '<div class="label_below2"><label><input type="checkbox" class="checkAllFields" style="margin-right:5px">Field Read Only: <font color="red"></font></label></div>';
                                ret += '<div class="input_position_below">';
                                    ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                        for (i in form_fields) {
                                            //console.log(form_fields)
                                            if($.trim(form_fields[i])!=""){
                                                ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldReadOnly" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_read_only,form_fields[i]) +'/> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                            }
                                        }
                                    ret += '</div>';
                                    //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-hidden" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_hidden +'">';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                        ret += '<div class="clearfix"></div>';
                    ret += '    </div>';
                ret += '</div>';
            ret += '</div>';
            return ret;
            break;
        //WORKFLOW - START NODE
        case "workflow_chart-start":
            var workflow_status = if_undefinded(json_nodes_var[''+ parent_id +'']['status'],"");
            var workflow_field_enabled = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldEnabled'],"");
            var workflow_field_required = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldRequired'],"");
            var workflow_field_hidden = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldHiddenValue'],"");
            var workflow_field_read_only = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldReadOnly'],"");
            var workflow_processor_button = if_undefinded(json_nodes_var[''+ parent_id +'']['buttonStatus'],"");
            var workflow_default_action = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow-default-action'],"");
            var workflow_escalation_period = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow-escalation-period'],"");
            var workflow_draft_button = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_draft_button'],"0");
            var fieldEnabled_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldEnabled_access_type'],"2");
            var fieldRequired_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldRequired_access_type'],"2");
            var fieldHiddenValue_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldHiddenValue_access_type'],"2");


            var form_fields = $(".form_fields").val();
            form_fields = JSON.parse(form_fields);
            //ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; overflow: hidden;" class="ps-container">';
                ret +='<div class="content-dialog" style="height: auto;">';
                    ret += '<div style="float:left;width:32%;margin-right:10px">';
                        ret += '<div class="fields_below section clearing fl-field-style">';
                            ret += '<div class="input_position_below column div_1_of_1">';
                                ret += '<span>Default Action: <font color="red">*</font> <i class="fa fa-mobile" title="Available on mobile"></i> </span>';
                                ret += '<select class="form-select" id="workflow-default-action" style="margin-top:0px;">';
                                ret += '<option value="0">--Select--</option>';
                                if(workflow_processor_button){
                                    for(var i in workflow_processor_button){
                                        ret += '<option value="'+ workflow_processor_button[i]['child_id'] +'" '+ setSelected(workflow_processor_button[i]['child_id'], workflow_default_action) +'>'+ workflow_processor_button[i]['button_name'] +'</option>';
                                    }
                                }
                                ret += '<option value="cancel" '+ setSelected('cancel', workflow_default_action) +'>Cancel</option>';
                                ret += '</select>';
                                ret += '<img src="/images/loader/loading.gif" class="pull-left display processorLoad"/>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float:left;width:30%;margin-right:10px;display:none">';
                        ret += '<div class="fields_below">';
                            ret += '<div class="label_below2">Escalation Period(Day/s): </div>';
                            ret += '<div class="input_position_below">';
                                ret += '<select class="form-select" id="workflow-escalation-period" style="margin-top:0px">';
                                // ret += '<option value="0">--Select--</option>';
                                for (var i = 1; i <= 10; i++) {
                                    ret += '<option value="'+ i +'" '+ setSelected(i, workflow_escalation_period) +'>'+ i +'</option>';
                                };
                                ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float:left;width:65%;margin-right:10px;">';
                        ret += '<div class="fields_below section clearing fl-field-style" style="width:49%">';
                           
                            ret += '<div class="input_position_below column div_1_of_1">';
                                 ret += '<span>Save & Review Later Button: </span>';
                                ret += '<select class="form-select" id="workflow_draft_button" style="margin-top:0px">';
                                 ret += '<option value="0" '+ setSelected("0", workflow_draft_button) +'>No</option>';
                                 ret += '<option value="1" '+ setSelected("1", workflow_draft_button) +'>Yes</option>';
                                ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';

                    ret += '    <div class="pull-left display" style="width: 32%;margin-right:10px">';
                    ret += '        <div class="fields_below">';
                    ret += '            <div class="label_below2">Access Type:</div>';
                    ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                    ret += '                <select class="fl-input-select" id="access_type-field_enabled" rel="field_enabled" style="width:100%">';
                    ret += '                    <option value="1" '+ setSelected('1',fieldEnabled_access_type) +'>All Fields</option>'; //setSelected('1',json['user_access_type'])
                    ret += '                    <option value="2" '+ setSelected('2',fieldEnabled_access_type) +'>Custom</option>';
                    ret += '                </select>';
                    ret += '            </div>';
                    ret += '        </div>';
                    ret += '    </div>';
                    ret += '    <div class="pull-left display" style="width: 32%;margin-right:10px">';
                    ret += '        <div class="fields_below">';
                    ret += '            <div class="label_below2">Access Type:</div>';
                    ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                    ret += '                <select class="fl-input-select" id="access_type-field_hidden" rel="field_hidden" style="width:100%">';
                    ret += '                    <option value="1" '+ setSelected('1',fieldHiddenValue_access_type) +'>All Fields</option>'; //setSelected('1',json['user_access_type'])
                    ret += '                    <option value="2" '+ setSelected('2',fieldHiddenValue_access_type) +'>Custom</option>';
                    ret += '                </select>';
                    ret += '            </div>';
                    ret += '        </div>';
                    ret += '    </div>';
                    ret += '    <div class="pull-left display" style="width: 32%;">';
                    ret += '        <div class="fields_below">';
                    ret += '            <div class="label_below2">Access Type:</div>';
                    ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                    ret += '                <select class="fl-input-select" id="access_type-field_required" rel="field_required" style="width:100%">';
                    ret += '                    <option value="1" '+ setSelected('1',fieldRequired_access_type) +'>All Enabled Fields</option>'; //setSelected('1',json['user_access_type'])
                    ret += '                    <option value="2" '+ setSelected('2',fieldRequired_access_type) +'>Custom</option>';
                    ret += '                </select>';
                    ret += '            </div>';
                    ret += '        </div>';
                    ret += '    </div>';
                    ret += '<div class="clearfix"></div>';
                    ret += '<div style="float: left;width:32%;margin-right:10px" class="container-fields f_enabled">';
                        ret += '<div class="fields_below section clearing fl-field-style">';
                            ret += '<div class="input_position_below column div_1_of_1">';
                                ret += '<span><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_enabled"><label for="f_enabled" class="css-label"></label> Field Enabled: <font color="red"></font> <i class="fa fa-mobile" title="Available on mobile"></i> </label></span>';
                                ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                    for (i in form_fields) {
                                        //console.log(form_fields)
                                        if($.trim(form_fields[i])!=""){
                                            ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldEnabled css-checkbox" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_enabled,form_fields[i]) +' id="Enabled_' + form_fields[i] + '"/><label for="Enabled_' + form_fields[i] + '" class="css-label"></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                        }
                                    }
                                   
                                ret += '</div>'; 
                                //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-hidden" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_hidden +'">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    
                    ret += '<div style="float: left;width:32%;margin-right:10px"  class="container-fields f_hidden">';
                        ret += '<div class="fields_below section clearing fl-field-style">';
                            
                            ret += '<div class="input_position_below column div_1_of_1">';
                                ret += '<span><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_hidden"><label for="f_hidden" class="css-label"></label> Field Hidden: <font color="red"></font> <i class="fa fa-mobile" title="Available on mobile"></i> </label></span>';
                                ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                    for (i in form_fields) {
                                        if($.trim(form_fields[i])!=""){
                                            //console.log(form_fields)
                                            ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldHiddenValue css-checkbox" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_hidden,form_fields[i]) +' id="Hidden_' + form_fields[i] + '"/><label for="Hidden_' + form_fields[i] + '" class="css-label"></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                        }
                                    }
                                ret += '</div>';
                                //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-hidden" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_hidden +'">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';

                    ret += '<div style="float: left;width:32%;"  class="container-fields f_required">';
                        ret += '<div class="fields_below section clearing fl-field-style">';
                            
                            ret += '<div class="input_position_below column div_1_of_1">';
                                ret += '<span><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_required"><label for="f_required" class="css-label"></label> Field Required: <font color="red"></font> <i class="fa fa-mobile" title="Available on mobile"></i> </label></span>';
                                ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                    var requireFieldStatus = "";
                                    var requireFieldCss = "";
                                        for (i in form_fields) {
                                            requireFieldStatus = "";
                                            requireFieldCss = "";
                                            //console.log(form_fields)
                                            // console.log(workflow_field_enabled)
                                            if(workflow_field_enabled){
                                                    if(workflow_field_enabled.indexOf(form_fields[i])==-1 || workflow_field_hidden.indexOf(form_fields[i])>=0){
                                                        requireFieldStatus = "disabled='disabled'";
                                                        requireFieldCss = "opacity: 0.4;cursor:not-allowed !important;";
                                                    }
                                            }
                                            if($.trim(form_fields[i])!=""){
                                                ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left;'+ requireFieldCss +'"><input class="fieldRequired css-checkbox" type="checkbox" '+ requireFieldStatus +' name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_required,form_fields[i]) +' id="Required2_' + form_fields[i] + '"/><label for="Required2_' + form_fields[i] + '" class="css-label"></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                            }
                                        }
                                ret += '</div>';
                                //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-hidden" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_hidden +'">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';

                    ret += '<div style="float: left;width:23%;display:none" class="container-fields f_readOnly">';
                        ret += '<div class="fields_below">';
                            ret += '<div class="label_below2"><label><input type="checkbox" class="checkAllFields" style="margin-right:5px">Field Read Only: <font color="red"></font></label></div>';
                            ret += '<div class="input_position_below">';
                                ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                    for (i in form_fields) {
                                        //console.log(form_fields)
                                        if($.trim(form_fields[i])!=""){
                                            ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldReadOnly" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_read_only,form_fields[i]) +'/> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                        }

                                    }
                                ret += '</div>';
                                //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-hidden" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_hidden +'">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';


                ret += '</div>';
            ret += '</div>';
            return ret;
            break
        //WORKFLOW - FOR ROUTE SETTINGS BEFORE CREATING LINE(BUTTONS)
        case "workflow_chart_buttons":
            var form_id = $(".form_chosen").val();
            var buttons_json = json_nodes_var[''+ parent_id +'']['buttonStatus'];
            var wokflow_button_action = $("#"+child_id).find(".node-title").text();
            var button_selected = "";
            if($("#"+child_id).attr("data-node-type")=="circle"){
                wokflow_button_action = $("#"+child_id).find(".node-start-title-label").text();
            }
            if(json_lines_var!=""){
                button_selected = if_undefinded(json_lines_var[''+ parent_id +'_'+ child_id +'_arrow'],"");

            }
            var button_title = "";
            if(button_selected!=""){
                button_title = button_selected['json_title'];
            }
            console.log(button_selected)
            var disabledButton = "";
            var fieldEnabled_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldEnabled_access_type'],"2");
            var fieldRequired_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldRequired_access_type'],"2");
            var fieldHiddenValue_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldHiddenValue_access_type'],"2");
            
            
            var workflow_field_required = "";
            
            var workflow_field_enabled = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldEnabled'],"");
            var workflow_field_hidden = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldHiddenValue'],"");

            var workflow_field_read_only = "";
            
            if(buttons_json){
                if(buttons_json[''+ button_title +'']){
                    workflow_field_required= if_undefinded(buttons_json[''+ button_title +'']['fieldRequired'],"");

                    // workflow_field_read_only= if_undefinded(buttons_json[''+ button_title +'']['fieldReadOnly'],"");
                }
            }

            var form_fields = $(".form_fields").val();
            form_fields = JSON.parse(form_fields);

            ret += '<div class="object_properties_content">';
                ret += '<div class="scrollbarM object_properties_wrapper" style="height:auto">';
                    ret += '<div id="accordion-container" class="flas" style="width:95%">';
                        ret += '<ul>';
                            ret += '<li><a href="#tabs-1">Primary Settings</a></li>';
                            ret += '<li><a href="#tabs-2">Field Settings</a></li>';
                        ret += '</ul>';
                        ret += '<div id="tabs-1" style="padding:10px;">';
                            ret += '<div>';
                                ret += '<div class="fields_below">';
                                    ret += '<div class="input_position_below section clearing fl-field-style">';
                                        ret += '<div class=" column div_1_of_1">';
                                             ret += '<span class="">Button ss: <font color="red">*</font></span>';
                                            ret += '<select class="form-select workflow-buttons" style="margin-top:0px">';
                                                ret += '<option value="0">--Select--</option> ';
                                            var htmlstring_array = $(".form_buttons").val().split(",");
                                            for (var i=0;i<htmlstring_array.length;i++) {
                                                if(button_title!=htmlstring_array[i]){
                                                    disabledButton = hasButton(htmlstring_array[i],buttons_json); 
                                                }
                                                ret += '<option value="'+ htmlstring_array[i] +'" '+ disabledButton +' '+ setSelected(htmlstring_array[i],button_title) +'>'+ htmlstring_array[i] +'</option> ';
                                            }
                                            ret += '</select>';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div>';
                                ret += '<div class="fields_below" style="display:none">';
                                    
                                    ret += '<div class="input_position_below section clearing fl-field-style">';
                                        ret += '<div class="div_1_of_1 column">';
                                        ret += '<span>Action <font color="red">*</font></span>';
                                        ret += '<input type="text" name="" class="form-textarea obj_prop" readonly="readonly" id="wokflow_button-action" data-properties-type="lblFldName" data-object-id="" placeholder="Processor" value="'+ htmlEntities(wokflow_button_action) +'" child_id = "'+ child_id +'" parent_id = "'+ parent_id +'">';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div>';
                                ret += '<div class="fields_below">';
                                    ret += '<div class="input_position_below section clearing fl-field-style">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<span style="display:block;">Line Color: <font color="red">*</font></span>';
                                            ret += '<input type="text" class="chngColor" id="wokflow_button-line-color" data-properties-type="nodeColor" value="#424242" style="width:inherit"/>';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div style="float:left;width:100%;">';
                                ret += '<div class="fields_below">';
                                    ret += '<div class="input_position_below section clearing fl-field-style" style="">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<span>Warning Message: <font color="red">*</font></span>';
                                                ret += '<textarea class="msgModalProcess form-textarea" style="resize:none;width:100%;height:100%;margin:0px;" id="msgModalProcess" data-properties-type="dataModalMsg">'+ msgModaldefault +'</textarea>';
                                            ret += '</div>';
                                        ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div style="float:left;width:100%;">';
                                ret += '<div class="fields_below">';
                                    ret += '<div class="input_position_below section clearing fl-field-style" style="">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<span>Success Message: <font color="red">*</font></span>';
                                            ret += '<textarea class="message_success form-textarea" style="resize:none;width:100%;height:100%;margin:0px;" id="message_success" data-properties-type="message_success">'+ message_success +'</textarea>';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div style="">';
                                ret += '<div class="fields_below">';
                                    
                                    ret += '<div class="input_position_below section clearing fl-field-style">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<span style="display:block;">Comment Settings:</span>';
                                            ret += '<label>';
                                                ret += '<input type="checkbox" name="" class="obj_prop css-checkbox" id="wokflow_button-require">';
                                                ret += '<label for="wokflow_button-require" class="css-label"></label>';
                                                ret += '<span style="margin-left: 5px;font-size: 11px;">Require comment</span>';
                                            ret += '</label>';
                                        ret += '</div>';
                                        ret += '<div class="input_position_below" style="float:left;display:none">';
                                            ret += '<label>';
                                                ret += '<input type="checkbox" name="" class="obj_prop css-checkbox" id="wokflow_button-display-commentbox">';
                                                ret += '<label for="wokflow_button-display-commentbox" class="css-label"></label>';
                                                ret += '<span style="margin-left: 5px;font-size: 11px;">Display comment box</span>';
                                            ret += '</label>';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div style="float:left;width:100%;">';
                                   
                                    ret += '<div class="input_position_below section clearing fl-field-style" style="">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<span>Landing Page: <font color="red">*</font></span>';
                                            ret += '<select class="form-select" id="node-landing-page">';
                                                var wf_landing_page = $.trim($("#wf-landing_page").text());
                                                // ret += '<option value = "application_view">Request View</option>';
                                                // ret += '<option value = "new-request">New Request</option>';
                                                try{
                                                    wf_landing_page = JSON.parse(wf_landing_page);
                                                    wf_landing_page['New Request'] = "workspace";
                                                    wf_landing_page['Request List'] = "application";
                                                    var sortedLandingPage = Object.keys(wf_landing_page).sort();
                                                    // var newSelectionPage = ['application','workspace'];
                                                    // wf_landing_page.concat(newSelectionPage);
                                                    for(var option_ctr in sortedLandingPage){
                                                
                                                        ret += '<option value = "'+wf_landing_page[sortedLandingPage[option_ctr]]+'" '+ setSelected(wf_landing_page[sortedLandingPage[option_ctr]],landing_page) +'>'+sortedLandingPage[option_ctr]+'</option>';

                                                   }
                                                }catch(err){
                                                    console.log(err)
                                                }
                                            ret += '</select>';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div class="clearfix"></div>';
                        ret += '</div>';
                        ret += '    <div id="tabs-2">';
                            ret += '    <div class="pull-left display" style="width: 32%;margin-right:10px">';
                            ret += '        <div class="fields_below">';
                            ret += '            <div class="label_below2">Access Type:</div>';
                            ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                            ret += '                <select class="fl-input-select" id="access_type-field_enabled" rel="field_enabled" style="width:100%">';
                            ret += '                    <option value="1" '+ setSelected('1',fieldEnabled_access_type) +'>All Fields</option>'; //setSelected('1',json['user_access_type'])
                            ret += '                    <option value="2" '+ setSelected('2',fieldEnabled_access_type) +'>Custom</option>';
                            ret += '                </select>';
                            ret += '            </div>';
                            ret += '        </div>';
                            ret += '    </div>';
                            ret += '    <div class="pull-left display" style="width: 32%;margin-right:10px">';
                            ret += '        <div class="fields_below">';
                            ret += '            <div class="label_below2">Access Type:</div>';
                            ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                            ret += '                <select class="fl-input-select" id="access_type-field_hidden" rel="field_hidden" style="width:100%">';
                            ret += '                    <option value="1" '+ setSelected('1',fieldHiddenValue_access_type) +'>All Fields</option>'; //setSelected('1',json['user_access_type'])
                            ret += '                    <option value="2" '+ setSelected('2',fieldHiddenValue_access_type) +'>Custom</option>';
                            ret += '                </select>';
                            ret += '            </div>';
                            ret += '        </div>';
                            ret += '    </div>';
                            ret += '    <div class="pull-left display" style="width: 32%;">';
                            ret += '        <div class="fields_below">';
                            ret += '            <div class="label_below2">Access Type:</div>';
                            ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                            ret += '                <select class="fl-input-select" id="access_type-field_required" rel="field_required" style="width:100%">';
                            ret += '                    <option value="1" '+ setSelected('1',fieldRequired_access_type) +'>All Enabled Fields</option>'; //setSelected('1',json['user_access_type'])
                            ret += '                    <option value="2" '+ setSelected('2',fieldRequired_access_type) +'>Custom</option>';
                            ret += '                </select>';
                            ret += '            </div>';
                            ret += '        </div>';
                            ret += '    </div>';

                            ret += '<div class="clearfix"></div>';
                            // ret += '<div style="float: left;width:32%;margin-right:10px" class="container-fields f_enabled">';
                            //     ret += '<div class="fields_below section clearing fl-field-style">';
                                    
                            //         ret += '<div class="input_position_below column div_1_of_1">';
                            //             ret += '<span class="font-bold"><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_enabled_2"><label for="f_enabled_2" class="css-label"></label> Field Enabled: <font color="red"></font></label></span>';
                            //             ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                            //                 for (i in form_fields) {
                            //                     //console.log(form_fields)
                            //                     if($.trim(form_fields[i])!=""){
                            //                         ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldEnabled css-checkbox" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_enabled,form_fields[i]) +' id="ID_field_' + form_fields[i] + '"/><label for="ID_field_' + form_fields[i] + '" class="css-label"></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                            //                     }
                            //                 }
                            //             ret += '</div>';
                            //             //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-enabled" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_enabled +'">';
                            //         ret += '</div>';
                            //     ret += '</div>';
                            // ret += '</div>';
                            
                            // ret += '<div style="float: left;width:32%;margin-right:10px" class="container-fields f_hidden">';
                            //     ret += '<div class="fields_below section clearing fl-field-style">';
                                   
                            //         ret += '<div class="input_position_below column div_1_of_1">';
                            //              ret += '<span class="font-bold"><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_hidden_2"><label for="f_hidden_2" class="css-label"></label> Field Hidden: <font color="red"></font></label></span>';
                            //             ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                            //                 for (i in form_fields) {
                            //                     //console.log(form_fields)
                            //                     if($.trim(form_fields[i])!=""){
                            //                         ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldHiddenValue css-checkbox" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_hidden,form_fields[i]) +' id="Hidden2_' + form_fields[i] + '"/><label for="Hidden2_' + form_fields[i] + '" class="css-label"></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                            //                     }
                            //                 }
                            //             ret += '</div>';
                            //             //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-hidden" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_hidden +'">';
                            //         ret += '</div>';
                            //     ret += '</div>';
                            // ret += '</div>';

                            ret += '<div style="float:left;width:32%;" class="container-fields f_required">';
                                ret += '<div class="fields_below section clearing fl-field-style">';
                                    
                                    ret += '<div class="input_position_below column div_1_of_1">';
                                        ret += '<span><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_required_2"><label for="f_required_2" class="css-label"></label> Field Required: <font color="red"></font></label></span>';
                                        //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-required" data-properties-type="lblFldName" data-object-id="" placeholder="Status" value="'+ workflow_field_required +'">';
                                        ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                        var requireFieldStatus = "";
                                        var requireFieldCss = "";
                                            for (i in form_fields) {
                                                requireFieldStatus = "";
                                                requireFieldCss = "";
                                                //console.log(form_fields)
                                                // console.log(workflow_field_enabled)
                                                if(workflow_field_enabled){
                                                        if(workflow_field_enabled.indexOf(form_fields[i])==-1 || workflow_field_hidden.indexOf(form_fields[i])>=0){
                                                            requireFieldStatus = "disabled='disabled'";
                                                            requireFieldCss = "opacity: 0.4;cursor:not-allowed !important;";
                                                        }
                                                }
                                                if($.trim(form_fields[i])!=""){
                                                    ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left;'+ requireFieldCss +'"><input class="fieldRequired css-checkbox" type="checkbox" '+ requireFieldStatus +' name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_required,form_fields[i]) +' id="Required2_' + form_fields[i] + '"/><label for="Required2_' + form_fields[i] + '" class="css-label" style=""></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                                }
                                            }
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';


                            // ret += '<div style="float: left;width:23%;display:none" class="container-fields f_readOnly">';
                            //     ret += '<div class="fields_below">';
                            //         ret += '<div class="label_below2"><label><input type="checkbox" class="checkAllFields" style="margin-right:5px">Field Read Only: <font color="red"></font></label></div>';
                            //         ret += '<div class="input_position_below">';
                            //             ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                            //                 for (i in form_fields) {
                            //                     //console.log(form_fields)
                            //                     if($.trim(form_fields[i])!=""){
                            //                         ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldReadOnly" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_read_only,form_fields[i]) +'/> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                            //                     }
                            //                 }
                            //             ret += '</div>';
                            //             //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-hidden" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_hidden +'">';
                            //         ret += '</div>';
                            //     ret += '</div>';
                            // ret += '</div>';
                            ret += '<div class="clearfix"></div>';
                        ret += '    </div>';
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            return ret;
            break;
        //WORKFLOW - CONDITION NODE
        case "workflow_chart-condition":
            var wokflow_condition_fields = if_undefinded(json_nodes_var[''+ parent_id +'']['field'],"");
            var wokflow_condition_operator = if_undefinded(json_nodes_var[''+ parent_id +'']['operator'],"");
            var wokflow_condition_value = if_undefinded(json_nodes_var[''+ parent_id +'']['field_value'],"");
            // var form_fields = $(".form_fields").val();
            var form_fields = $(".form_fieldsV2").text();
            form_fields = JSON.parse(form_fields);
            ret += '<div class="object_properties_content">';
                ret += '<div class="scrollbarM object_properties_wrapper" style="height:auto;">';
                        ret += '<div class="fields_below section clearing fl-field-style">';
                            ret += '<div class="input_position_below column div_1_of_1">';
                                ret += '<span class="font-bold">Fields: <font color="red">*</font></span>';
                                ret += '<select class="form-select wokflow_condition-fields" style="margin-top:0px">';
                                    for (i in form_fields) {
                                        if($.trim(form_fields[i])!=""){
                                            //console.log(form_fields)
                                            ret += '<option value="'+ form_fields[i]['name'] +'" '+ setSelected(form_fields[i]['name'], wokflow_condition_fields) +' field_input_type="'+ form_fields[i]['field_input_type'] +'" data-type="'+ form_fields[i]['data-type'] +'">'+ form_fields[i]['name'] +'</option> ';
                                        }
                                    }

                            ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                        ret += '<div class="fields_below section clearing fl-field-style">';
                            ret += '<div class="input_position_below column div_1_of_1">';
                                ret += '<span class="font-bold">Operator: <font color="red">*</font></span>';
                                ret += '<select class="form-select wokflow_condition-operator" style="margin-top:0px">';
                                        ret += '<option value="==" '+ setSelected('==', wokflow_condition_operator) +'>Equal (==)</option>';
                                        ret += '<option class="number_operator" value="<=" '+ setSelected('<=', wokflow_condition_operator) +'>Less than equal (<=)</option>';
                                        ret += '<option class="number_operator" value=">=" '+ setSelected('>=', wokflow_condition_operator) +'>Greater than equal (>=)</option>';
                                        ret += '<option class="number_operator" value=">" '+ setSelected('>', wokflow_condition_operator) +'>Greater than (>)</option> ';
                                        ret += '<option class="number_operator" value="<" '+ setSelected('<', wokflow_condition_operator) +'>Less than (<)</option> ';
                                        ret += '<option value="!=" '+ setSelected('!=', wokflow_condition_operator) +'>Not equal (!=)</option>';
                                ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                        ret += '<div class="fields_below section clearing fl-field-style">';
                            ret += '<div class="input_position_below column div_1_of_1">';
                                ret += '<span class="font-bold">Value: </span>';
                                ret += '<input type="text" name="" class="form-text"  id="wokflow_condition-value" data-properties-type="lblFldName" data-object-id="" placeholder="Value" value="'+htmlEntities(wokflow_condition_value) +'">';
                            ret += '</div>';
                        ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            return ret;
            break;
        //WORKFLOW - FOR ROUTE SETTINGS BEFORE CREATING LINE(CONDITION)
        case "workflow_chart_condition-line":
            var wokflow_condition_action = $("#"+child_id).find(".node-title").text();
            var workflow_condition_return = if_undefinded(json_nodes_var[''+ parent_id +'']['condition_return'],"");
            var hastrue = "";var hasfalse = "";
            var hastrueStyle = "";var hasfalseStyle = "";
            if($("#"+child_id).attr("data-node-type")=="circle"){
                wokflow_condition_action = $("#"+child_id).find(".node-start-title-label").text();
            }
            if(workflow_condition_return){
                if (json_nodes_var[''+ parent_id +'']['condition_return']['True']) {
                    hastrue = "disabled='disabled'";
                    hastrueStyle = "opacity:0.5";
                }
                if (json_nodes_var[''+ parent_id +'']['condition_return']['False']) {
                    hasfalse = "disabled='disabled'";
                    hasfalseStyle = "opacity:0.5";
                }
            }
            ret += '<div class="object_properties_content">';
                ret += '<div class="scrollbarM object_properties_wrapper">';
                    ret += '<div style="float: left;width:45%;">';
                        ret += '<div class="fields_below">';
                            ret += '<div class="label_below2">Return Boolean:</div>';
                            ret += '<div class="input_position_below">';
                                ret += '<label style="margin-right:10px;'+ hastrueStyle +'">';
                                    ret += '<input type="radio" '+ hastrue +' name="wokflow_condition-return-boolean" class="obj_prop wokflow_condition-return-boolean css-checkbox" id="propRdoTrue" value="True"><label for="propRdoTrue" class="css-label"></label>';
                                    ret += '<span style="margin-left: 5px;font-size: 11px;">True</span>';
                                ret += '</label>';
                                 ret += '<label style="'+ hasfalseStyle +'">';
                                 
                                    ret += '<input type="radio" '+ hasfalse +' name="wokflow_condition-return-boolean" class="obj_prop wokflow_condition-return-boolean css-checkbox" id="propRdoFalse" value="False"><label for="propRdoFalse" class="css-label"></label>';
                                    ret += '<span style="margin-left: 5px;font-size: 11px;">False</span>';
                                ret += '</label>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float:right;width:45%;display:none">';
                        ret += '<div class="fields_below">';
                            ret += '<div class="label_below2">Action <font color="red">*</font></div>';
                            ret += '<div class="input_position_below" style="text-align: right;">';
                                ret += '<input type="text" name="" class="form-text obj_prop" readonly="readonly" id="wokflow_condition-action" data-properties-type="lblFldName" data-object-id="" placeholder="Processor" value="'+ htmlEntities(wokflow_condition_action) +'" child_id = "'+ child_id +'" parent_id = "'+ parent_id +'">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float:left;width:45%;">';
                        ret += '<div class="fields_below">';
                            ret += '<div class="label_below2">Line Color: <font color="red">*</font></div>';
                            ret += '<div class="input_position_below" style="">';
                                ret += '<input type="text" class="chngColor" id="wokflow_condition-line-color" data-properties-type="lineColor" value="#424242"/>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            return ret;
            break;
        case "workflow_chart-end":
            var form_fields = $(".form_fields").val();
            form_fields = JSON.parse(form_fields);
            var workflow_field_hidden = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldHiddenValue'],"");
            var workflow_end_status  = if_undefinded(json_nodes_var[''+ parent_id +'']['status'],"");
            var workflow_notification_message_type = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_notification_message_type'],"");
            var workflow_notification_message = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_notification_message'],"");
            var workflow_notification_enabled = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_notification_enabled'],"1");
            var node_del_control = if_undefinded(json_nodes_var[''+ parent_id +'']['node_del_control'],"0");
            var fieldHiddenValue_access_type = if_undefinded(json_nodes_var[''+ parent_id +'']['fieldHiddenValue_access_type'],"2");
            ret += '<div class="object_properties_content">';
                // ret += '<div class="scrollbarM object_properties_wrapper">';
                    ret += '<div id="accordion-container" class="flas" style="margin:0px !important;">';
                        ret += '    <ul>';
                        ret += '        <li><a href="#tabs-1">Primary Settings</a></li>';
                        ret += '        <li><a href="#tabs-2">Field Settings</a></li>';
                        ret += '    </ul>';
                        ret += '   <div id="tabs-1" style="padding:10px;">';
                            ret += '<div class="fl-end-workflow">';
                                ret += '<div class="fields_below section clearing fl-field-style">';
                                    ret += '<div class="input_position_below column div_1_of_1">';
                                    ret += '<span>Status: <font color="red">*</font></span>';
                                        ret += '<input type="text" name="" class="form-text obj_prop" id="workflow_end-status" data-properties-type="lblFldName" data-object-id="" placeholder="Status" value="'+ htmlEntities(workflow_end_status) +'">';
                                    ret += '</div>';
                                ret += '</div>';
                            // ret += '</div>';
                            // ret += '<div style="float:left;width:30%;height: 65px;">';
                            //     ret += '<div class="fields_below" style="'+ display_deletion +'">';
                            //         ret += '<div class="label_below2"></div>';
                            //         ret += '<div class="input_position_below">';
                            //             ret += '<label><input type="checkbox" name="node_deletion_control" class="css-checkbox" value="1" '+ setChecked("1",node_del_control) +' id="node_del_control"><label for="node_del_control" class="css-label"></label> <span style="letter-spacing: 1px;font-size: 12px;color: #4E4E4E;">Enable Deletion</span></label>';
                            //         ret += '</div>';
                            //     ret += '</div>';
                            // ret += '</div>';

                            ret += '<div class="formulaContainer" style="width:100%;">';
                                ret += '<div class="fields_below section clearing fl-field-style">';
                                    ret += '<div class="clearfix"></div>';
                                    ret += '<div class="input_position_below column div_1_of_1">';
                                         ret += '    <label><input type="checkbox" '+ setChecked("1",workflow_notification_enabled) +' name="enabled-notification" class="enabled-notification css-checkbox" id="enabled-notification"><label for="enabled-notification" class="css-label"></label> Enable Notification</label>';    
                                        ret += '<div class="" style="float:right;">'; //style="float:right;width:30%;margin: 5px 0px;font-size: 10px;font-weight: bold;"
                                        ret += '    <label>Static: <input type="radio" name="fm-type" class="fm-type formulaType css-checkbox" value="0" id="fm-type0"><label for="fm-type0" class="css-label"></label></label>';
                                        ret += '    <label>Dynamic: <input type="radio" name="fm-type" class="fm-type formulaType css-checkbox" value="1" id="fm-type1"><label for="fm-type1" class="css-label"></label><label>';
                                        ret += '</div>';
                                        ret += '<div style="margin:3px 0px 3px 0px;">Notification Message: </div>';
                                        ret += '<textarea name="" class="form-textarea tip wfSettings" title="" id="fm-message" data-ide-properties-type="notification-message" placeholder="" style="width: 99%;">'+ workflow_notification_message +'</textarea>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                        ret += '</div>';
                        ret += '<div id="tabs-2" style="padding:10px;">'
                            ret += '    <div class="pull-left display" style="width: 32%;">';
                            ret += '        <div class="fields_below">';
                            ret += '            <div class="label_below2">Access Type:</div>';
                            ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                            ret += '                <select class="fl-input-select" id="access_type-field_hidden" rel="field_hidden" style="width:100%">';
                            ret += '                    <option value="1" '+ setSelected('1',fieldHiddenValue_access_type) +'>All Fields</option>'; //setSelected('1',json['user_access_type'])
                            ret += '                    <option value="2" '+ setSelected('2',fieldHiddenValue_access_type) +'>Custom</option>';
                            ret += '                </select>';
                            ret += '            </div>';
                            ret += '        </div>';
                            ret += '    </div>';
                            ret += '<div style="float: left;width:100%" class="container-fields f_hidden">';
                                ret += '<div class="fields_below section clearing fl-field-style">';
                                    ret += '<div class="input_position_below column div_1_of_1">';
                                        ret += '<span><label><input type="checkbox" class="checkAllFields css-checkbox" style="margin-right:5px" id="f_hidden_2"><label for="f_hidden_2" class="css-label"></label> Field Hidden: <font color="red"></font></label></span>';
                                        ret += '<div  style="background-color:#fff;padding:5px;border:1px solid #ddd;overflow:auto;min-height:100px;">';
                                            for (var i in form_fields) {
                                                //console.log(form_fields)
                                                if($.trim(form_fields[i])!=""){
                                                    ret += '<label class="tip" data-original-title="' + form_fields[i] + '" style="float:left"><input class="fieldHiddenValue css-checkbox" type="checkbox" name="field_' + form_fields[i] + '" value="' + form_fields[i] + '" '+ setCheckedFromArray(workflow_field_hidden,form_fields[i]) +' id="Hidden2_' + form_fields[i] + '"/><label for="Hidden2_' + form_fields[i] + '" class="css-label"></label> <span class="limit-text-wf">' + form_fields[i] + '</span></label><br /><br />';
                                                }
                                            }
                                        ret += '</div>';
                                        //ret += '<input type="text" name="" class="form-text obj_prop" id="workflow-field-hidden" data-properties-type="lblName" data-object-id="" placeholder="Label Name" value="'+ workflow_field_hidden +'">';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                            ret += '<div class="clearfix"></div>';
                        ret += '</div>'
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            return ret;
            break;
        case "workflow_chart-email":
            //workflow email processor
            var workflow_email  = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_email'],"");
            var workflow_sms  = if_undefinded(json_nodes_var[''+ parent_id +'']['workflow_sms'],"");
            var email_title_type = "0";
            var email_message_type = "0";
            var incRequesLog = "0";
            var email_title = "";
            var email_message = "";
            var departments_to = [];var positions_to = [];var users_to = [];var groups_to = [];var otherRecepient_to = "";
            var departments_cc = [];var positions_cc = [];var users_cc = [];var groups_cc = [];var otherRecepient_cc = "";
            var departments_bcc = [];var positions_bcc = [];var users_bcc = [];var groups_bcc = [];var otherRecepient_bcc = "";
            //
            if(workflow_email){
                //to
                var to = workflow_email['email_recpient'];
                departments_to = to['departments'];
                positions_to = to['positions'];
                users_to = to['users'];
                groups_to = to['groups'];
                otherRecepient_to = if_undefinded(to['otherRecepient'],"");
                //cc
                var cc = workflow_email['email_cc'];
                departments_cc = cc['departments'];
                positions_cc = cc['positions'];
                users_cc = cc['users'];
                groups_cc = cc['groups'];
                otherRecepient_cc = if_undefinded(cc['otherRecepient'],"");
                //bcc
                var bcc = workflow_email['email_bcc'];
                departments_bcc = bcc['departments'];
                positions_bcc = bcc['positions'];
                otherRecepient_bcc = if_undefinded(bcc['otherRecepient'],"");
                users_bcc = bcc['users'];
                groups_bcc = bcc['groups'];

                //message and title
                email_title =   workflow_email['title'];
                email_message = workflow_email['message'];
                email_title_type = workflow_email['email-title-type'];
                email_message_type = workflow_email['email-message-type'];
                incRequesLog = workflow_email['requestLog'];
            }

            //CANCEL Email Notification
            {
                var cl_workflow_email  = if_undefinded(json_nodes_var[''+ parent_id +'']['cl_workflow_email'],"");
                var cl_email_title_type = "0";
                var cl_email_message_type = "0";
                var cl_incRequesLog = "0";
                var cl_email_title = "";
                var cl_email_message = "";
                var cl_departments_to = [];var cl_positions_to = [];var cl_users_to = [];var cl_groups_to = [];var cl_otherRecepient_to = "";
                var cl_departments_cc = [];var cl_positions_cc = [];var cl_users_cc = [];var cl_groups_cc = [];var cl_otherRecepient_cc = "";
                var cl_departments_bcc = [];var cl_positions_bcc = [];var cl_users_bcc = [];var cl_groups_bcc = [];var cl_otherRecepient_bcc = "";
                //
                if(cl_workflow_email){
                    //to
                    var cl_to = cl_workflow_email['email_recpient'];
                    cl_departments_to = cl_to['departments'];
                    cl_positions_to = cl_to['positions'];
                    cl_users_to = cl_to['users'];
                    cl_groups_to = cl_to['groups'];
                    cl_otherRecepient_to = if_undefinded(cl_to['otherRecepient'],"");
                    //cc
                    var cl_cc = cl_workflow_email['email_cc'];
                    cl_departments_cc = cl_cc['departments'];
                    cl_positions_cc = cl_cc['positions'];
                    cl_users_cc =cl_cc['users'];
                    cl_groups_cc = cl_cc['groups'];
                    cl_otherRecepient_cc = if_undefinded(cl_cc['otherRecepient'],"");
                    //bcc
                    var cl_bcc = cl_workflow_email['email_bcc'];
                    cl_departments_bcc = cl_bcc['departments'];
                    cl_positions_bcc = cl_bcc['positions'];
                    cl_otherRecepient_bcc = if_undefinded(cl_bcc['otherRecepient'],"");
                    cl_users_bcc = cl_bcc['users'];
                    cl_groups_bcc = cl_bcc['groups'];

                    //message and title
                    cl_email_title =   cl_workflow_email['title'];
                    cl_email_message = cl_workflow_email['message'];
                    cl_email_title_type = cl_workflow_email['email-title-type'];
                    cl_email_message_type = cl_workflow_email['email-message-type'];
                    cl_incRequesLog = cl_workflow_email['requestLog'];
                }
            }

            var sms_departments = [];var sms_positions = [];var sms_users = [];var sms_groups = [];
            var sms_contact = "";var sms_message = "";var sms_message_type = "0"; var sms_recipient_type = "0";
            if(workflow_sms){
                sms_departments = workflow_sms['departments'];
                sms_positions = workflow_sms['positions'];
                sms_users = workflow_sms['users'];
                sms_groups = workflow_sms['groups'];
                sms_contact =   workflow_sms['contact'];
                sms_message = workflow_sms['message'];
                sms_message_type = workflow_sms['message-type'];
                sms_recipient_type = if_undefinded(workflow_sms['recipient-type'],"0");
            }
            //console.log(departments_to);
            ret += '<div class="object_properties_content">';
                ret += '<div class="content-dialog-scroll-container" style="position: relative; padding: 0px;  max-height: 500px;overflow: hidden;" class="ps-container-a">';
                    ret +='<div class="content-dialog-container" style="width: 95%;height: auto;">';
                        ret += '<div id="accordion-container" class="flas">';
                        ret += '    <ul>';
                        ret += '        <li><a href="#tabs-1">SMS</a></li>';
                        ret += '        <li><a href="#tabs-2">E-Mail</a></li>';
                        ret += '        <li class="cl-email"><a href="#tabs-3">E-Mail for cancellation</a></li>';
                        ret += '    </ul>';
                        ret += '   <div id="tabs-1" style="padding:10px;">';
                            //ret += '<label>SMS</label>';
                                ret += '<div>';
                                var c_lite = $("#go_lite").val();
                                if(c_lite == 0){
                                    ret += '<div style="opacity: 0.4;background-color: #B1B1B1;width: 96%;height: 620px;position: absolute;z-index: 1111;"></div>';
                                }
                                ret += '<div class="fp-container sms-users CSID-search-container">';
                                    ret += '<div class="section clearing fl-field-style">';
                                        ret += '<div class="column div_1_of_1"><label class="font-bold"><input type="checkbox" class="sms-requestor css-checkbox" value="All Users" id="sms-requestor"/><label for="sms-requestor" class="css-label"></label> Requestor</label>';
                                        ret += ' <label class="font-bold"><input type="checkbox" class="sms-processor css-checkbox wf-next-processor" value="All Users" id="sms-processor"/><label for="sms-processor" class="css-label"></label> Processor</label>';
                                    ret +='</div>';
                                  
                                    ret += '</div>';
                                    ret += '<div class="section clearing fl-field-style">';
                                        ret += '<div class="column div_1_of_1" style="display:table;">';
                                        ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allDepartments css-checkbox" value="All Users" id="allDept4"/><label for="allDept4" class="css-label"></label> All Departments</label>';
                                        ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allGroups css-checkbox" value="All Users" id="allGroup4"/><label for="allGroup4" class="css-label"></label> All Groups</label>';
                                        ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allPositions css-checkbox" value="All Users" id="allPosition4"/><label for="allPosition4" class="css-label"></label> All Positions</label>';
                                        ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="fp-allUsers4"/><label for="fp-allUsers4" class="css-label"></label> All Users</label>';
                                        ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';    
                                        ret += '</div>';
                                    ret += '</div>';
                                    
                                    ret += '<div class="clearfix"></div>';
                                    ret += '    <div class="pull-left display" style="width: 100%%;">';
                                    ret += '        <div class="fields_below">';
                                    ret += '            <div class="label_below2">Access Type:</div>';
                                    ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                                    ret += '                <select class="fl-input-select" id="access_type" rel="sms" style="width:100%">';
                                    ret += '                    <option value="1">All Fields</option>'; //setSelected('1',json['user_access_type'])
                                    ret += '                    <option value="2">Custom</option>';
                                    ret += '                </select>';
                                    ret += '            </div>';
                                    ret += '        </div>';
                                    ret += '    </div>';
                                     ret += '<div class="content-dialog-scroll" rel="sms">'; // style="position: relative; margin-left: 20px; padding: 0px; width: 100%; height: 240px;overflow: hidden;" class="ps-container"

                                         ret +='<div class="content-dialog" style="padding:1px; font-size:12px !important;">'; //style="width: 90%;height: auto;"

                                    //ret += '<div style="margin-left: 20px;max-height:240px;overflow-y:auto;overflow-x:hidden">';
                                    // for department
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                                            ret += '<div class="getDepartmentFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                            var departments = $(".departments").text();
                                            var ctr_dept = 0;
                                            if(departments){
                                                departments = JSON.parse(departments);
                                                $.each(departments,function(key, val){
                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                    //setCheckedArray(value, arr)
                                                        ret += '<label class="tip" data-original-title="' + htmlEntities(val.department) + '"><input type="checkbox" class="fp-user fp-user-sms departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, sms_departments) +' value="'+ val.id +'" id="fp-user-sms_dept_'+ val.id +'"/><label for="fp-user-sms_dept_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
                                                    ret += '</div>';
                                                    ctr_dept++
                                                })
                                            }
                                            var displayEmptyDept = "display";
                                            if(ctr_dept==0){
                                                displayEmptyDept = ""
                                            }
                                            ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';

                                    // for groups
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                                            ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                var groups = $(".all_groups").text();
                                                var ctr_groups = 0;
                                                if(groups){
                                                    groups = JSON.parse(groups);

                                                    $.each(groups,function(key, val){
                                                        if(htmlEntities(val.group_name) !=""){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                ret += '<label class="tip" data-original-title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user fp-user-sms groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, sms_groups) +' value="'+ val.id +'" id="groups_sms' + val.id + '"/><label for="groups_sms' + val.id + '" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
                                                            ret += '</div>';
                                                            ctr_groups++;
                                                        }
                                                    })
                                                    
                                                }
                                                var displayEmptyGroups = "display";
                                                if(ctr_groups==0){
                                                    displayEmptyGroups = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyGroups +'" style="color:red">No Groups</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                     ret += '</div>';
                                    // for position
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="positions">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                                            ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                var positions = $(".positions").text();
                                                var ctr_pos = 0;
                                                if(positions){
                                                    positions = JSON.parse(positions);

                                                    $.each(positions,function(key, val){
                                                        if(htmlEntities(val.position) !=""){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                ret += '<label class="tip" data-original-title="' + htmlEntities(val.position) + '"><input type="checkbox" class="fp-user fp-user-sms positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, sms_positions) +' value="'+ val.id +'" id="Position_sms_' + val.id + '"/><label for="Position_sms_' + val.id + '" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
                                                            ret += '</div>';
                                                            ctr_pos++;
                                                        }
                                                    })
                                                    
                                                }
                                                var displayEmptyPos = "display";
                                                if(ctr_pos==0){
                                                    displayEmptyPos = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                     ret += '</div>';
                                        //for users
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
                                        ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
                                            ret += '<div class="getUsersFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                var users = $(".users").text();
                                                var ctr_user = 0;
                                                var user_name = "";
                                                if(users){
                                                    users = JSON.parse(users);
                                                    $.each(users,function(key, val){
                                                        ret += '<div style="width:30%;float:left; margin-bottom:2px;" class="fl-table-ellip CSID-search-data">';
                                                            // user_name = val.first_name +' '+ val.last_name;
                                                            user_name = htmlEntities(val.display_name);
                                                            ret += '<label class="tip" data-original-title="'+ user_name +'"><input type="checkbox" class="fp-user fp-user-sms users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, sms_users) +' value="'+ val.id +'" id="IDuser_'+ val.id +'"/><label for="IDuser_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ user_name + '</span></label>';
                                                        ret += '</div>';
                                                    })
                                                    ctr_user++;
                                                }
                                                var displayEmptyUsers = "display";
                                                if(ctr_user==0){
                                                    displayEmptyUsers = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
                                                // ret += '</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>'; 
                                ret += '<div class="formulaContainer section clearing fl-field-style">';
                                        ret += '<div class="fields_below column div_1_of_1">';
                                            ret += '<span style="float:right;font-size: 10px;font-weight: bold;">';
                                            ret += '    <label>Static: <input type="radio" '+ RequestPivacy.setCheckedArray("0", sms_recipient_type) +'  name="workflow-sms-recipient-type" class="workflow-sms-recipient-type formulaType css-checkbox" value="0" id="workflow-sms-recipient-type0"><label for="workflow-sms-recipient-type0" class="css-label"></label></label>';
                                            ret += '    <label>Dynamic: <input type="radio" '+ RequestPivacy.setCheckedArray("1", sms_recipient_type) +' name="workflow-sms-recipient-type" class="workflow-sms-recipient-type formulaType css-checkbox" value="1" id="workflow-sms-recipient-type1"><label for="workflow-sms-recipient-type1" class="css-label"></label><label>';
                                            ret += '</span>';
                                            ret += '<span class="font-bold">Other Recipient: </span>';
                                            ret += '<div class="clearfix"></div>';
                                            ret += '<div class="input_position_below">';
                                                ret += '<textarea name="" class="form-textarea tip wfSettings" title="Numbers should be separated by semicolon. Eg 09XXXXXXXX1;09XXXXXXXX2" id="workflow-sms-contact" data-ide-properties-type="ide-workflow-sms-contact" placeholder="Contact Number">'+ sms_contact +'</textarea>';
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                    ret += '<div class="formulaContainer section clearing fl-field-style">';
                                        ret += '<div class="fields_below column div_1_of_1">';
                                            ret += '<span class="" style="float:right;font-size: 10px;font-weight: bold;">';
                                            ret += '    <label>Static: <input type="radio" '+ RequestPivacy.setCheckedArray("0", sms_message_type) +' name="workflow-sms-message-type" class="workflow-sms-message-type formulaType css-checkbox" value="0" id="workflow-sms-message-type0"><label for="workflow-sms-message-type0" class="css-label"></label></label>';
                                            ret += '    <label>Dynamic: <input type="radio" '+ RequestPivacy.setCheckedArray("1", sms_message_type) +' name="workflow-sms-message-type" class="workflow-sms-message-type formulaType css-checkbox" value="1" id="workflow-sms-message-type1"><label for="workflow-sms-message-type1" class="css-label"></label><label>';
                                            ret += '</span>';
                                            ret += '<span class="font-bold">Message: </span>';
                                            ret += '<div class="clearfix"></div>';
                                            ret += '<div class="input_position_below">';
                                                ret += '<textarea class="form-textarea wfSettings formulaSMS" placeholder="Message" id="workflow-sms-message" data-ide-properties-type="ide-workflow-sms-message" style="height:70px">'+ sms_message +'</textarea>';
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>';
                        ret += '    </div>';
                        //Email
                        ret += '    <div id="tabs-2" style="padding:10px;">';
                                ret += '<div class="scrollbarM">';
                                    //start accordion
                                    ret += '<div id="accordion">';
                                        //start to
                                        ret += '<label>To:</label>';
                                        ret += '<div class="fp-container email-to CSID-search-container">';
                                            ret += '<div class="section clearing fl-field-style">';
                                                ret += '<div class="column div_1_of_1">';
                                                ret +=  '<label class="font-bold" style="font-size:12px !important;"><input type="checkbox" class="email-requestor css-checkbox" value="All Users" id="email-requestor"/><label for="email-requestor" class="css-label"></label> Requestor</label>';
                                                ret += ' <label class="font-bold" style="font-size:12px !important;"><input type="checkbox" class="email-processor css-checkbox wf-next-processor" value="All Users" id="email-processor"/><label for="email-processor" class="css-label"></label> Processor</label>'; 
                                                ret += '</div>';
                                                
                                            ret += '</div>';
                                            ret += '<div class="fields_below formulaContainer section clearing fl-field-style">';
                                                ret += '<div class="input_position_below column div_1_of_1">';
                                                    ret += '<span class="" style="float:right;font-size: 11px;font-weight: bold;">';
                                                ret += '    <label>Static: <input type="radio"  name="workflow-mail-otherRecepient-type_to" checked="checked" class="workflow-mail-otherRecepient-type formulaType css-checkbox" value="0" id="workflow-mail-otherRecepient-type0_to"><label for="workflow-mail-otherRecepient-type0_to" class="css-label"></label></label>';
                                                ret += '    <label>Dynamic: <input type="radio" name="workflow-mail-otherRecepient-type_to" class="workflow-mail-otherRecepient-type formulaType css-checkbox" value="1" id="workflow-mail-otherRecepient-type1_to"><label for="workflow-mail-otherRecepient-type1_to" class="css-label"></label><label>';
                                                ret += '</span>';
                                                ret += '<span class="font-bold" style="font-size:12px !important;"><label>Other Recipient:</label> </span>';
                                                    // ret += '<input type="text" name="" class="form-text tip wfSettings" title="Email Address should be separated by semicolon" id="workflow-mail-otherRecepient_to" placeholder="sample@domain.com" value="'+ otherRecepient_to +'">';
                                                    ret += '<textarea name="" class="form-textarea tip wfSettings ide-hover" readonly="readonly" data-ide-properties-type="ide-workflow-mail-otherRecepient_to" title="Email Address should be separated by pipe(|)" id="workflow-mail-otherRecepient_to" placeholder="sample@domain.com">'+ otherRecepient_to +'</textarea>';
                                                ret += '</div>';
                                            ret += '</div>';
                                            ret += '<div class="section clearing fl-field-style"">';
                                                ret += '<div class="column div_1_of_1" style="display:table;">';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allDepartments css-checkbox" value="All Users" id="allDept5"/><label for="allDept5" class="css-label"></label> All Departments</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allGroups css-checkbox" value="All Users" id="allGroup5"/><label for="allGroup5" class="css-label"></label> All Groups</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allPositions css-checkbox" value="All Users" id="allPosition5"/><label for="allPosition5" class="css-label"></label> All Positions</label>';
                                                    ret += '<span class="" style="font-size:12px !important; display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="fp-allUsers5"/><label for="fp-allUsers5"class="css-label"></label> All Users</label></span>'; 
                                                    ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
                                                ret  +=  '</div>';
                                            ret  +=  '</div>';
                                    
                                            ret += '    <div class="pull-left display" style="width: 100%%;">';
                                            ret += '        <div class="fields_below">';
                                            ret += '            <div class="label_below2">Access Type:</div>';
                                            ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                                            ret += '                <select class="fl-input-select" id="access_type" rel="email-to" style="width:100%">';
                                            ret += '                    <option value="1">All Fields</option>'; //setSelected('1',json['user_access_type'])
                                            ret += '                    <option value="2">Custom</option>';
                                            ret += '                </select>';
                                            ret += '            </div>';
                                            ret += '        </div>';
                                            ret += '    </div>';

                                             ret += '<div class="content-dialog-scroll" rel="email-to" style="position: relative;  padding: 0px; width: 100%; height: 240px;overflow: hidden;" class="ps-container">';

                                                 ret +='<div class="content-dialog" style="padding:1px; font-size:12px !important; ">'; // style="width: 90%;height: auto;"

                                            //ret += '<div style="margin-left: 20px;max-height:240px;overflow-y:auto;overflow-x:hidden">';
                                            // for department
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                                                ret += '<div class="column div_1_of_1">';
                                                    ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                                                    ret += '<div class="getDepartmentFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    var departments = $(".departments").text();
                                                    var ctr_dept = 0;
                                                    if(departments){
                                                        departments = JSON.parse(departments);
                                                        $.each(departments,function(key, val){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                            //setCheckedArray(value, arr)
                                                                ret += '<label class="tip" data-original-title="' + htmlEntities(val.department) + '"><input type="checkbox" class="fp-user fp-user-to departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, departments_to) +' value="'+ val.id +'" id="fp-user-to_dept_'+ val.id +'"/><label for="fp-user-to_dept_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
                                                            ret += '</div>';
                                                            ctr_dept++
                                                        })
                                                    }
                                                    var displayEmptyDept = "display";
                                                    if(ctr_dept==0){
                                                        displayEmptyDept = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                            // for groups
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                                                ret += '<div class="column div_1_of_1">';
                                                    ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                                                    ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        var groups = $(".all_groups").text();
                                                        var ctr_groups = 0;
                                                        if(groups){
                                                            groups = JSON.parse(groups);

                                                            $.each(groups,function(key, val){
                                                                if(val.group_name !=""){
                                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                        ret += '<label class="tip" data-original-title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user fp-user-sms groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, groups_to) +' value="'+ val.id +'" id="groups_to_' + val.id + '"/><label for="groups_to_' + val.id + '" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
                                                                    ret += '</div>';
                                                                    ctr_groups++;
                                                                }
                                                            })
                                                            
                                                        }
                                                        var displayEmptyGroups = "display";
                                                        if(ctr_groups==0){
                                                            displayEmptyGroups = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyGroups +'" style="color:red">No Groups</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                             ret += '</div>';
                                            // for position
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="positions">';
                                                ret += '<div class="column div_1_of_1">';
                                                    ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                                                    ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        var positions = $(".positions").text();
                                                        var ctr_pos = 0;
                                                        if(positions){
                                                            positions = JSON.parse(positions);

                                                            $.each(positions,function(key, val){
                                                                if(val.position !=""){
                                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                        ret += '<label class="tip" data-original-title="' + htmlEntities(val.position) + '"><input type="checkbox" class="fp-user fp-user-to positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, positions_to) +' value="'+ val.id +'" id="Positions_TO'+ val.id +'"/><label for="Positions_TO'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
                                                                    ret += '</div>';
                                                                    ctr_pos++;
                                                                }
                                                            })
                                                            
                                                        }
                                                        var displayEmptyPos = "display";
                                                        if(ctr_pos==0){
                                                            displayEmptyPos = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                             ret += '</div>';
                                                //for users
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
                                                ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
                                                    ret += '<div class="getUsersFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        var users = $(".users").text();
                                                        var ctr_users = 0;
                                                        var user_name = "";
                                                        if(users){
                                                            users = JSON.parse(users);
                                                            $.each(users,function(key, val){
                                                                ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                    // user_name = val.first_name +' '+ val.last_name;
                                                                    user_name = htmlEntities(val.display_name);
                                                                    ret += '<label class="tip" data-original-title="'+ user_name +'"><input type="checkbox" class="fp-user fp-user-to users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, users_to) +' value="'+ val.id +'" id="2UsersTO_'+ val.id +'"/><label for="2UsersTO_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ user_name +'</span></label>';
                                                                ret += '</div>';
                                                                ctr_users++;
                                                            })
                                                        } 
                                                        var displayEmptyUsers = "display";
                                                        if(ctr_users==0){
                                                            displayEmptyUsers = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
                                                        ret += '</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                            ret += '</div>';
                                        ret += '</div>'; 
                                        //end to
                                        //start cc
                                        ret += '<label>CC:</label>';
                                        ret += '<div  class="fp-container email-cc CSID-search-container" style="font-size:12px !important;">';
                                            ret += '<div class="section clearing fl-field-style">';
                                                ret += '<div class="column div_1_of_1"><label class="font-bold" style="font-size:12px !important;"><input type="checkbox" class="email-requestor css-checkbox" value="All Users" id="email-requestor2"/><label for="email-requestor2" class="css-label"></label> Requestor</label> ';
                                                ret += '<label class="font-bold" style="font-size:12px !important;"><input type="checkbox" class="email-processor css-checkbox wf-next-processor" value="All Users" id="email-processor2"/><label for="email-processor2" class="css-label"></label> Processor</label> </div>';
                                            ret += '</div>';
                                            ret += '<div class="fields_below formulaContainer section clearing fl-field-style">';
                                                
                                                ret += '<div class="input_position_below column div_1_of_1">';
                                                    ret += '<div class="" style="float:right;font-size: 11x;font-weight: bold;">';
                                                    ret += '    <label>Static: <input type="radio"  name="workflow-mail-otherRecepient-type_cc" checked="checked" class="workflow-mail-otherRecepient-type formulaType css-checkbox" value="0" id="workflow-mail-otherRecepient-type0_cc"><label for="workflow-mail-otherRecepient-type0_cc" class="css-label"></label></label>';
                                                    ret += '    <label>Dynamic: <input type="radio" name="workflow-mail-otherRecepient-type_cc" class="workflow-mail-otherRecepient-type formulaType css-checkbox" value="1" id="workflow-mail-otherRecepient-type1_cc"><label for="workflow-mail-otherRecepient-type1_cc" class="css-label"></label><label>';
                                                    ret += '</div>';
                                                    ret += '<span class="font-bold" style="font-size:12px !important;"><label>Other Recipient:</label> </span>';
                                                
                                                    // ret += '<input type="text" name="" class="form-text tip wfSettings" title="Email Address should be separated by semicolon" id="workflow-mail-otherRecepient_cc" placeholder="sample@domain.com" value="'+ otherRecepient_cc +'">';
                                                    ret += '<textarea name="" class="form-textarea tip wfSettings ide-hover" readonly="readonly" data-ide-properties-type="ide-workflow-mail-otherRecepient_cc" title="Email Address should be separated by pipe(|)" id="workflow-mail-otherRecepient_cc" placeholder="sample@domain.com">'+ otherRecepient_cc +'</textarea>';
                                                ret += '</div>';
                                            ret += '</div>';
                                            ret += '<div class="section clearing fl-field-style">';
                                                ret += '<div class="column div_1_of_1" style="display:table;">';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allDepartments css-checkbox" value="All Users" id="allDept6"/><label for="allDept6" class="css-label"></label> All Departments</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allGroups css-checkbox" value="All Users" id="allGroup6"/><label for="allGroup6" class="css-label"></label> All Groups</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allPositions css-checkbox" value="All Users" id="allPosition6"/><label for="allPosition6" class="css-label"></label> All Positions</label>';
                                                    ret += '<span class="" style="display:table-cell;vertical-align:middle; font-size:12px !important;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="fp-allUsers6"/><label for="fp-allUsers6" class="css-label"></label> All Users</label></span>';
                                                    ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
                                                ret += '</div>';
                                            ret += '</div>';   

                                            ret += '    <div class="pull-left display" style="width: 100%%;">';
                                            ret += '        <div class="fields_below">';
                                            ret += '            <div class="label_below2">Access Type:</div>';
                                            ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                                            ret += '                <select class="fl-input-select" id="access_type" rel="email-cc" style="width:100%">';
                                            ret += '                    <option value="1">All Fields</option>'; //setSelected('1',json['user_access_type'])
                                            ret += '                    <option value="2">Custom</option>';
                                            ret += '                </select>';
                                            ret += '            </div>';
                                            ret += '        </div>';
                                            ret += '    </div>';

                                            ret += '<div class="content-dialog-scroll" rel="email-cc" style="position: relative;  padding: 0px; width: 100%; height: 240px;overflow: hidden;" class="ps-container">';

                                                 ret +='<div class="content-dialog" style="padding:1px; font-size:12px !important; ">'; //
                                            //ret += '<div style="margin-left: 20px;max-height:240px;overflow-y:auto;overflow-x:hidden">';
                                            // for department
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                                                ret += '<div class="column div_1_of_1">';
                                                    ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                                                    ret += '<div class="getDepartmentFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    if(departments){
                                                        $.each(departments,function(key, val){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                            //setCheckedArray(value, arr)
                                                                ret += '<label class="tip" data-original-title="'+ htmlEntities(val.department) +'"><input type="checkbox" class="fp-user fp-user-cc departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, departments_cc) +' value="'+ val.id +'" id="fp-user-cc_dept_'+ val.id +'"/><label for="fp-user-cc_dept_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
                                                            ret += '</div>';
                                                        })
                                                        
                                                    }
                                                    var displayEmptyDept = "display";
                                                    if(ctr_dept==0){
                                                        displayEmptyDept = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                            // for groups
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                                                ret += '<div class="column div_1_of_1">';
                                                    ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                                                    ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        var groups = $(".all_groups").text();
                                                        var ctr_groups = 0;
                                                        if(groups){
                                                            groups = JSON.parse(groups);

                                                            $.each(groups,function(key, val){
                                                                if(val.group_name !=""){
                                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                        ret += '<label class="tip" data-original-title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user fp-user-sms groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, groups_cc) +' value="'+ val.id +'" id="groups_cc_' + val.id + '"/><label for="groups_cc_' + val.id + '" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
                                                                    ret += '</div>';
                                                                    ctr_groups++;
                                                                }
                                                            })
                                                            
                                                        }
                                                        var displayEmptyGroups = "display";
                                                        if(ctr_groups==0){
                                                            displayEmptyGroups = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyGroups +'" style="color:red">No Groups</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                             ret += '</div>';
                                            // for position
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="positions">';
                                                ret += '<div class="column div_1_of_1">';    
                                                    ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                                                    ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        
                                                        if(positions){
                                                            $.each(positions,function(key, val){
                                                                if(htmlEntities(val.position) !=""){
                                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                        ret += '<label class="tip" data-original-title="'+ htmlEntities(val.position) +'"><input type="checkbox" class="fp-user fp-user-cc positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, positions_cc) +' value="'+ val.id +'" id="CCPosition_'+ val.id +'"/><label for="CCPosition_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
                                                                    ret += '</div>';
                                                                }
                                                            })
                                                        }
                                                        var displayEmptyPos = "display";
                                                        if(ctr_pos==0){
                                                            displayEmptyUsers = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                                //for users
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
                                                ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
                                                    ret += '<div class="getUsersFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        
                                                        if(users){
                                                            $.each(users,function(key, val){
                                                                ret += '<div style="width:30%;float:left; margin-bottom:2px;" class="fl-table-ellip CSID-search-data"> ';
                                                                // user_name = val.first_name +' '+ val.last_name;
                                                                user_name = htmlEntities(val.display_name);
                                                                    ret += '<label class="tip" data-original-title="'+ user_name +'"><input type="checkbox" class="fp-user fp-user-cc users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, users_cc) +' value="'+ val.id +'" id="CCuser_'+ val.id +'"/><label for="CCuser_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text fl-table-elip">'+ user_name +'</span></label>';
                                                                ret += '</div>';
                                                            })
                                                        }
                                                        var displayEmptyUsers = "display";
                                                        if(ctr_users==0){
                                                            displayEmptyUsers = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
                                                        ret += '</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                        ret += '</div>'; 
                                         ret += '</div>'; 
                                        //end cc
                                        //start bcc
                                        ret += '<label>BCC:</label>';
                                        ret += '<div class="fp-container email-bcc CSID-search-container">';
                                            ret += '<div class="section clearing fl-field-style">';
                                                ret += '<div class="column div_1_of_1"><label class="font-bold" style="font-size:12px !important;"><input type="checkbox" class="email-requestor css-checkbox" value="All Users" id="email-requestor3"/><label for="email-requestor3" class="css-label"></label> Requestor</label> ';
                                                ret += '<label class="font-bold" style="font-size:12px !important;"><input type="checkbox" class="email-processor css-checkbox wf-next-processor" value="All Users" id="email-processor3"/><label for="email-processor3" class="css-label"></label> Processor</label> </div>';
                                            ret += '</div>';
                                            ret += '<div class="fields_below formulaContainer section clearing fl-field-style">';
                                              
                                                ret += '<div class="input_position_below column div_1_of_1">';
                                                      ret += '<div class="" style="float:right;font-size: 11px;font-weight: bold;">';
                                                    ret += '    <label>Static: <input type="radio"  name="workflow-mail-otherRecepient-type_bcc" checked="checked" class="workflow-mail-otherRecepient-type formulaType css-checkbox" value="0" id="workflow-mail-otherRecepient-type0_bcc"><label for="workflow-mail-otherRecepient-type0_bcc" class="css-label"></label></label>';
                                                    ret += '    <label>Dynamic: <input type="radio" name="workflow-mail-otherRecepient-type_bcc" class="workflow-mail-otherRecepient-type formulaType css-checkbox" value="1" id="workflow-mail-otherRecepient-type1_bcc"><label for="workflow-mail-otherRecepient-type1_bcc" class="css-label"></label><label>';
                                                    ret += '</div>';
                                                    ret += '<span class="font-bold" style="font-size:12px !important;"><label>Other Recipient:</label> </span>';
                                                    // ret += '<input type="text" name="" class="form-text tip wfSettings" title="Email Address should be separated by semicolon" id="workflow-mail-otherRecepient_bcc" placeholder="sample@domain.com" value="'+ otherRecepient_bcc +'">';
                                                    ret += '<textarea name="" class="form-textarea tip wfSettings  ide-hover" readonly="readonly" data-ide-properties-type="ide-workflow-mail-otherRecepient_bcc" title="Email Address should be separated by semicolon" id="workflow-mail-otherRecepient_bcc" placeholder="sample@domain.com">'+ otherRecepient_bcc +'</textarea>';
                                                ret += '</div>';
                                            ret += '</div>';
                                            ret += '<div class="section clearing fl-field-style">';
                                                ret += '<div class="column div_1_of_1" style="display:table;">';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allDepartments css-checkbox" value="All Users" id="allDept1"/><label for="allDept1" class="css-label"></label> All Departments</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allGroups css-checkbox" value="All Users" id="allGroup1"/><label for="allGroup1" class="css-label"></label> All Groups</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allPositions css-checkbox" value="All Users" id="allPosition1"/><label for="allPosition1" class="css-label"></label> All Positions</label>';        
                                                    ret += '<span class="" style="font-size:12px !important; display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="fp-allUsers1"/><label for="fp-allUsers1" class="css-label"></label> All Users</label></span>';
                                                     ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                           
                                            ret += '<div class="clearfix"></div>';

                                            ret += '    <div class="pull-left display" style="width: 100%%;">';
                                            ret += '        <div class="fields_below">';
                                            ret += '            <div class="label_below2">Access Type:</div>';
                                            ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                                            ret += '                <select class="fl-input-select" id="access_type" rel="email-bcc" style="width:100%">';
                                            ret += '                    <option value="1">All Fields</option>'; //setSelected('1',json['user_access_type'])
                                            ret += '                    <option value="2">Custom</option>';
                                            ret += '                </select>';
                                            ret += '            </div>';
                                            ret += '        </div>';
                                            ret += '    </div>';

                                            ret += '<div class="content-dialog-scroll" rel="email-bcc" style="position: relative; padding: 0px; width: 100%; height: 240px;overflow: hidden;" class="ps-container">';

                                                 ret +='<div class="content-dialog" style="padding:1px; font-size:12px !important;">';
                                            // for department
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                                                ret += '<div class="column div_1_of_1">';
                                                    ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                                                    ret += '<div class="getDepartmentFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    
                                                    if(departments){
                                                        $.each(departments,function(key, val){
                                                            ret += '<div style="width:30%;float:left; margin-bottom:2px;" class= "fl-table-ellip CSID-search-data"> ';
                                                            //setCheckedArray(value, arr)
                                                                ret += '<label  class="tip" data-original-title="'+ htmlEntities(val.department) +'"><input type="checkbox" class="fp-user fp-user-bcc departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, departments_bcc) +' value="'+ val.id +'" id="bCCUsers_'+ val.id +'"/><label for="bCCUsers_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
                                                            ret += '</div>';
                                                        })
                                                    }
                                                    var displayEmptyDept = "display";
                                                    if(ctr_dept==0){
                                                        displayEmptyDept = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                            // for groups
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                                                ret += '<div class="column div_1_of_1">';
                                                    ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                                                    ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        var groups = $(".all_groups").text();
                                                        var ctr_groups = 0;
                                                        if(groups){
                                                            groups = JSON.parse(groups);

                                                            $.each(groups,function(key, val){
                                                                if(htmlEntities(val.group_name) !=""){
                                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                        ret += '<label class="tip" data-original-title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user fp-user-sms groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, groups_bcc) +' value="'+ val.id +'" id="groups_bcc_' + val.id + '"/><label for="groups_bcc_' + val.id + '" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
                                                                    ret += '</div>';
                                                                    ctr_groups++;
                                                                }
                                                            })
                                                            
                                                        }
                                                        var displayEmptyGroups = "display";
                                                        if(ctr_groups==0){
                                                            displayEmptyGroups = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyGroups +'" style="color:red">No Groups</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                             ret += '</div>';
                                            // for position
                                            ret += '<div class="container-formUser section clearing fl-field-style"  CSID-search-type="positions">';
                                                ret += '<div class="column div_1_of_1">';
                                                    ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                                                    ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        
                                                        if(positions){
                                                            $.each(positions,function(key, val){
                                                                if(htmlEntities(val.position) !=""){
                                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                        ret += '<label class="tip" data-original-title="'+ htmlEntities(val.position) +'"><input type="checkbox" class="fp-user fp-user-bcc positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, positions_bcc) +' value="'+ val.id +'" id="BccPosition_'+ val.id +'"/><label for="BccPosition_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
                                                                    ret += '</div>';
                                                                }
                                                            })
                                                          
                                                        }
                                                        var displayEmptyPos = "display";
                                                        if(ctr_pos==0){
                                                            displayEmptyPos = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                                //for users
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
                                                ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
                                                    ret += '<div class="getUsersFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        
                                                        if(users){
                                                            $.each(users,function(key, val){
                                                                // user_name = val.first_name +' '+ val.last_name;
                                                                user_name = htmlEntities(val.display_name);
                                                                ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                    ret += '<label class="tip" data-original-title="'+ user_name +'"><input type="checkbox" class="fp-user fp-user-bcc users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, users_bcc) +' value="'+ val.id +'" id="Users_BCC_'+ val.id +'"/><label for="Users_BCC_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ user_name +'</span></label>';
                                                                ret += '</div>';
                                                            })
                                                            
                                                        }
                                                        var displayEmptyUsers = "display";
                                                        if(ctr_users==0){
                                                            displayEmptyUsers = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
                                                        ret += '</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                        ret += '</div>'; 
                                        ret += '</div>'; 
                                        //end bcc

                                    ret += '</div>';
                                    //end accordion
                                    ret += '<div  class="formulaContainer section clearing fl-field-style" style="margin-top:5px;">';

                                        ret += '<div class="input_position_below column div_1_of_1">';
                                            ret += '<div class="" style="float:right;font-size: 11px;font-weight: bold;">';
                                            ret += '    <label>Static: <input type="radio" '+ RequestPivacy.setCheckedArray("0", email_title_type) +' name="workflow-email-title-type" class="workflow-email-title-type formulaType css-checkbox" value="0" id="workflow-email-title-type0"><label for="workflow-email-title-type0" class="css-label"></label></label>';
                                            ret += '    <label>Dynamic: <input type="radio" '+ RequestPivacy.setCheckedArray("1", email_title_type) +' name="workflow-email-title-type" class="workflow-email-title-type formulaType css-checkbox" value="1" id="workflow-email-title-type1"><label for="workflow-email-title-type1" class="css-label"></label></label>';
                                            ret += '</div>';
                                             ret += '<span class="font-bold" style="font-size:12px !important;">Subject:</span>';
                                            ret += '<div class="wf-note-style" style="font-size:11px;color:red">Note: If this field is empty the default value is "Form Name"</div>';
                                            ret += '<textarea name="" class="form-textarea wfSettings formulaEmail" id="workflow-email-title" data-ide-properties-type="ide-workflow-email-title" placeholder="Subject">'+ email_title +'</textarea>';
                                        ret += '</div>';
                                       
                                    ret += '</div>';
                                    ret += '<div class="formulaContainer section clearing fl-field-style">';
                                        
                                        //ret += '<div class="label_below2" style=""><label>If you want to include the link of request please use "@link" only. </label></div>';
                                       
                                        ret += '<div class="input_position_below column div_1_of_1" >';
                                             ret += '<div class="" style="float:right;font-size: 11px;font-weight: bold;">';
                                            ret += '    <label>Static: <input type="radio" '+ RequestPivacy.setCheckedArray("0", email_message_type) +' name="workflow-email-message-type" class="workflow-email-message-type formulaType css-checkbox" value="0" id="workflow-email-message-type0"><label for="workflow-email-message-type0" class="css-label"></label></label>';
                                            ret += '    <label>Dynamic: <input type="radio" '+ RequestPivacy.setCheckedArray("1", email_message_type) +' name="workflow-email-message-type" class="workflow-email-message-type formulaType css-checkbox" value="1" id="workflow-email-message-type1"><label for="workflow-email-message-type1" class="css-label"></label></label>';
                                            ret += '</div>';
                                            ret += '<span class="font-bold" style="font-size:12px !important;">Body:</span>';
                                            ret += '<div class="wf-note-style" style="font-size:11px;color:red">Note: If this field is empty the default value is "Requestor has submitted a request for processing."</div>';
                                            ret += '<textarea class="form-textarea wfSettings formulaEmail" placeholder="Body" id="workflow-email-message" data-ide-properties-type="ide-workflow-email-message" style="height:70px">'+ email_message +'</textarea>';
                                        ret += '</div>';
                                   
                                    ret += '</div>';
                                    ret += '<div style="width:100%;display:none">';
                                        ret += '<div class="fields_below">';
                                            ret += '<div class="label_below2" style=""><label><input type="checkbox" class="css-checkbox" value="1" id="incRequesLog" '+ RequestPivacy.setCheckedArray("1", incRequesLog) +'><label for="incRequesLog" class="css-label"></label> Do you want to include Request logs?<label></div>';
                                            ret += '<div class="input_position_below" style="text-align: right;">';
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>';
                        ret += '    </div>';
                        // Email For Cancellation
                        ret += '    <div class="cl-email" id="tabs-3" style="padding:10px;">';
                            ret += '<div class="scrollbarM">';
                                //start accordion
                                ret += '<div id="cl-accordion">';
                                    //start to
                                    ret += '<label>To:</label>';
                                    ret += '<div class="fp-container cl-email-to CSID-search-container">';
                                        ret += '<div class="section clearing fl-field-style">';
                                            // ret += '<div style="width:30%;float:left"><label class="font-bold"><input type="checkbox" class="cl-email-requestor css-checkbox" value="All Users" id="cl-email-requestor"/><label for="cl-email-requestor" class="css-label"></label> Requestor</label></div>';
                                            ret += '<div class="column div_1_of_1">';
                                                ret += '<span class="font-bold" style="font-size:12px !important;"><label><input type="checkbox" class="cl-email-processor css-checkbox" value="All Users" id="cl-email-processor"/><label for="cl-email-processor" class="css-label"></label> Processor</label></span>'; 
                                            ret += '</div>';
                                        ret += '</div>';
                                        ret += '<div class="fields_below formulaContainer section clearing fl-field-style">';
                                            ret += '<div class="input_position_below column div_1_of_1">';
                                                 ret += '<span  style="float:right;font-size: 11px;font-weight: bold;">';
                                                    ret += '    <label>Static: <input type="radio"  name="cl-workflow-mail-otherRecepient-type_to" checked="checked" class="cl-workflow-mail-otherRecepient-type formulaType css-checkbox" value="0" id="cl-workflow-mail-otherRecepient-type0_to"><label for="cl-workflow-mail-otherRecepient-type0_to" class="css-label"></label></label>';
                                                    ret += '    <label>Dynamic: <input type="radio" name="cl-workflow-mail-otherRecepient-type_to" class="cl-workflow-mail-otherRecepient-type formulaType css-checkbox" value="1" id="cl-workflow-mail-otherRecepient-type1_to"><label for="cl-workflow-mail-otherRecepient-type1_to" class="css-label"></label><label>';
                                                ret += '</span>';
                                                ret += '<span class="font-bold" style="font-size:12px !important;"><label>Other Recipient:</label></span>';
                                                // ret += '<input type="text" name="" class="form-text tip wfSettings" title="Email Address should be separated by semicolon" id="cl-workflow-mail-otherRecepient_to" placeholder="sample@domain.com" value="'+ cl_otherRecepient_to +'">';
                                                ret += '<textarea name="" class="form-textarea tip wfSettings ide-hover" readonly="readonly" data-ide-properties-type="ide-cl-workflow-mail-otherRecepient_to" title="Email Address should be separated by pipe(|)" id="cl-workflow-mail-otherRecepient_to" placeholder="sample@domain.com">'+ cl_otherRecepient_to +'</textarea>';
                                            ret += '</div>';
                                        ret += '</div>';
                                        ret += '<div class="section clearing fl-field-style">';
                                            ret += '<div class="column div_1_of_1" style="display:table;">';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allDepartments css-checkbox" value="All Users" id="cl-fp-allDept5"/><label for="cl-fp-allDept5" class="css-label"></label> All Departments</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allGroups css-checkbox" value="All Users" id="cl-fp-allGroup5"/><label for="cl-fp-allGroup5" class="css-label"></label> All Groups</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allPositions css-checkbox" value="All Users" id="cl-fp-allPosition5"/><label for="cl-fp-allPosition5" class="css-label"></label> All Positions</label>';                                            
                                                 ret += '<span class="" style="font-size:12px; display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="cl-fp-allUsers5"/><label for="cl-fp-allUsers5"class="css-label"></label> All Users</label></span>';
                                                ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                        
                                        ret += '    <div class="pull-left display" style="width: 100%%;">';
                                        ret += '        <div class="fields_below">';
                                        ret += '            <div class="label_below2">Access Type:</div>';
                                        ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                                        ret += '                <select class="fl-input-select" id="access_type" rel="cl-email-to" style="width:100%">';
                                        ret += '                    <option value="1">All Fields</option>'; //setSelected('1',json['user_access_type'])
                                        ret += '                    <option value="2">Custom</option>';
                                        ret += '                </select>';
                                        ret += '            </div>';
                                        ret += '        </div>';
                                        ret += '    </div>';

                                         ret += '<div class="content-dialog-scroll" rel="cl-email-to" style="position: relative; padding: 0px; width: 100%; height: 240px;overflow: hidden;" class="ps-container">';

                                             ret +='<div class="content-dialog" style="padding:1px; font-size:12px !important;">';

                                        //ret += '<div style="margin-left: 20px;max-height:240px;overflow-y:auto;overflow-x:hidden">';
                                        // for department
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                                            ret += '<div class="column div_1_of_1">';
                                                ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                                                ret += '<div class="getDepartmentFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                var departments = $(".departments").text();
                                                var ctr_dept = 0;
                                                if(departments){
                                                    departments = JSON.parse(departments);
                                                    $.each(departments,function(key, val){
                                                        ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                        //setCheckedArray(value, arr)
                                                            ret += '<label class="tip" data-original-title="' + htmlEntities(val.department) + '"><input type="checkbox" class="fp-user cl-fp-user-to departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_departments_to) +' value="'+ val.id +'" id="cl_fp-user-to_dept_'+ val.id +'"/><label for="cl_fp-user-to_dept_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
                                                        ret += '</div>';
                                                        ctr_dept++
                                                    })
                                                }
                                                var displayEmptyDept = "display";
                                                if(ctr_dept==0){
                                                    displayEmptyDept = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                        // for groups
                                            ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                                                ret += '<div class="column div_1_of_1">';
                                                    ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                                                    ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                        var groups = $(".all_groups").text();
                                                        var ctr_groups = 0;
                                                        if(groups){
                                                            groups = JSON.parse(groups);

                                                            $.each(groups,function(key, val){
                                                                if(htmlEntities(val.group_name) !=""){
                                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                        ret += '<label class="tip" data-original-title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user fp-user-sms groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_groups_to) +' value="'+ val.id +'" id="cl_groups_to_' + val.id + '"/><label for="cl_groups_to_' + val.id + '" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
                                                                    ret += '</div>';
                                                                    ctr_groups++;
                                                                }
                                                            })
                                                            
                                                        }
                                                        var displayEmptyGroups = "display";
                                                        if(ctr_groups==0){
                                                            displayEmptyGroups = ""
                                                        }
                                                        ret += '<div class="empty-users '+ displayEmptyGroups +'" style="color:red">No Groups</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                             ret += '</div>';
                                        // for position
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="positions">';
                                            ret += '<div class="column div_1_of_1">';
                                                ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                                                ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    var positions = $(".positions").text();
                                                    var ctr_pos = 0;
                                                    if(positions){
                                                        positions = JSON.parse(positions);

                                                        $.each(positions,function(key, val){
                                                            if(htmlEntities(val.position) !=""){
                                                                ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                    ret += '<label class="tip" data-original-title="' + htmlEntities(val.position) + '"><input type="checkbox" class="fp-user cl-fp-user-to positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_positions_to) +' value="'+ val.id +'" id="cl_Positions_TO'+ val.id +'"/><label for="cl_Positions_TO'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
                                                                ret += '</div>';
                                                                ctr_pos++;
                                                            }
                                                        })
                                                        
                                                    }
                                                    var displayEmptyPos = "display";
                                                    if(ctr_pos==0){
                                                        displayEmptyPos = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                         ret += '</div>';
                                            //for users
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
                                            ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
                                                ret += '<div class="getUsersFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    var users = $(".users").text();
                                                    var user_name = "";
                                                    var ctr_users = 0;
                                                    if(users){
                                                        users = JSON.parse(users);
                                                        $.each(users,function(key, val){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                // user_name = val.first_name +' '+ val.last_name;
                                                                user_name = htmlEntities(val.display_name);
                                                                ret += '<label class="tip" data-original-title="'+ user_name +'"><input type="checkbox" class="fp-user cl-fp-user-to users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_users_to) +' value="'+ val.id +'" id="cl_2UsersTO_'+ val.id +'"/><label for="cl_2UsersTO_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ user_name +'</span></label>';
                                                            ret += '</div>';
                                                            ctr_users++;
                                                        })
                                                    } 
                                                    var displayEmptyUsers = "display";
                                                    if(ctr_users==0){
                                                        displayEmptyUsers = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
                                                    ret += '</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>'; 
                                    //end to
                                    //start cc
                                    ret += '<label>CC:</label>';
                                    ret += '<div  class="fp-container cl-email-cc CSID-search-container">';
                                        ret += '<div class="section clearing fl-field-style">';
                                            // ret += '<div style="width:30%;float:left"><label class="font-bold"><input type="checkbox" class="cl-email-requestor css-checkbox" value="All Users" id="cl-email-requestor2"/><label for="cl-email-requestor2" class="css-label"></label> Requestor</label></div>';
                                            ret += '<div class="column div_1_of_1">';
                                                ret += '<span class="font-bold" style="font-size:12px !important;"><label><input type="checkbox" class="cl-email-processor css-checkbox" value="All Users" id="cl-email-processor2"/><label for="cl-email-processor2" class="css-label"></label> Processor</label></span>'; 
                                            ret += '</div>';
                                        ret += '</div>';
                                        ret += '<div class="fields_below formulaContainer section clearing fl-field-style">';
                                            ret += '<div class="input_position_below column div_1_of_1">';
                                                ret += '<span class="" style="float:right;font-size: 11px;font-weight: bold;">';
                                                ret += '    <label>Static: <input type="radio"  name="cl-workflow-mail-otherRecepient-type_cc" checked="checked" class="cl-workflow-mail-otherRecepient-type formulaType css-checkbox" value="0" id="cl-workflow-mail-otherRecepient-type0_cc"><label for="cl-workflow-mail-otherRecepient-type0_cc" class="css-label"></label></label>';
                                                ret += '    <label>Dynamic: <input type="radio" name="cl-workflow-mail-otherRecepient-type_cc" class="cl-workflow-mail-otherRecepient-type formulaType css-checkbox" value="1" id="cl-workflow-mail-otherRecepient-type1_cc"><label for="cl-workflow-mail-otherRecepient-type1_cc" class="css-label"></label><label>';
                                                ret += '</span>';
                                                 ret += '<span class="font-bold" style="font-size:12px !important;"><label>Other Recipient:</label> </span>';
                                                // ret += '<input type="text" name="" class="form-text tip wfSettings" title="Email Address should be separated by semicolon" id="cl-workflow-mail-otherRecepient_cc" placeholder="sample@domain.com" value="'+ cl_otherRecepient_cc +'">';
                                                ret += '<textarea name="" class="form-textarea tip wfSettings ide-hover" readonly="readonly" data-ide-properties-type="ide-cl-workflow-mail-otherRecepient_cc" title="Email Address should be separated by pipe(|)" id="cl-workflow-mail-otherRecepient_cc" placeholder="sample@domain.com">'+ cl_otherRecepient_cc +'</textarea>';
                                            ret += '</div>';
                                        ret += '</div>';
                                        ret += '<div class="section clearing fl-field-style">';
                                            ret += '<div class="column div_1_of_1" style="display:table">';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allDepartments css-checkbox" value="All Users" id="cl-fp-allDept6"/><label for="cl-fp-allDept6" class="css-label"></label> All Departments</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allGroups css-checkbox" value="All Users" id="cl-fp-allGroup6"/><label for="cl-fp-allGroup6" class="css-label"></label> All Groups</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allPositions css-checkbox" value="All Users" id="cl-fp-allPosition6"/><label for="cl-fp-allPosition6" class="css-label"></label> All Positions</label>';                                                                                        
                                                ret += '<span class="" style="font-size:12px !important; display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="cl-fp-allUsers6"/><label for="cl-fp-allUsers6" class="css-label"></label> All Users</label></span>';
                                                 ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
                                            ret +='</div>';
                                        ret +='</div>';

                                        ret += '    <div class="pull-left display" style="width: 100%%;">';
                                        ret += '        <div class="fields_below">';
                                        ret += '            <div class="label_below2">Access Type:</div>';
                                        ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                                        ret += '                <select class="fl-input-select" id="access_type" rel="cl-email-cc" style="width:100%">';
                                        ret += '                    <option value="1">All Fields</option>'; //setSelected('1',json['user_access_type'])
                                        ret += '                    <option value="2">Custom</option>';
                                        ret += '                </select>';
                                        ret += '            </div>';
                                        ret += '        </div>';
                                        ret += '    </div>';

                                        ret += '<div class="content-dialog-scroll" rel="cl-email-cc" style="position: relative;  padding: 0px; width: 100%; height: 240px;overflow: hidden;" class="ps-container">';

                                             ret +='<div class="content-dialog" style="padding:1px; font-size:12px !important;">';
                                        //ret += '<div style="margin-left: 20px;max-height:240px;overflow-y:auto;overflow-x:hidden">';
                                        // for department
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                                            ret += '<div class="column div_1_of_1">';
                                                ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                                                ret += '<div class="getDepartmentFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                if(departments){
                                                    $.each(departments,function(key, val){
                                                        ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                        //setCheckedArray(value, arr)
                                                            ret += '<label class="tip" data-original-title="'+ htmlEntities(val.department) +'"><input type="checkbox" class="fp-user cl-fp-user-cc departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_departments_cc) +' value="'+ val.id +'" id="cl_fp-user-cc_dept_'+ val.id +'"/><label for="cl_fp-user-cc_dept_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
                                                        ret += '</div>';
                                                    })
                                                    
                                                }
                                                var displayEmptyDept = "display";
                                                if(ctr_dept==0){
                                                    displayEmptyDept = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                                                ret += '</div>';
                                            ret += '</div>';    
                                        ret += '</div>';
                                        // for groups
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                                            ret += '<div class="column div_1_of_1">';
                                                ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                                                ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    var groups = $(".all_groups").text();
                                                    var ctr_groups = 0;
                                                    if(groups){
                                                        groups = JSON.parse(groups);

                                                        $.each(groups,function(key, val){
                                                            if(htmlEntities(val.group_name) !=""){
                                                                ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                    ret += '<label class="tip" data-original-title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user fp-user-sms groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_groups_cc) +' value="'+ val.id +'" id="cl_groups_cc_' + val.id + '"/><label for="cl_groups_cc_' + val.id + '" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
                                                                ret += '</div>';
                                                                ctr_groups++;
                                                            }
                                                        })
                                                        
                                                    }
                                                    var displayEmptyGroups = "display";
                                                    if(ctr_groups==0){
                                                        displayEmptyGroups = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyGroups +'" style="color:red">No Groups</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                         ret += '</div>';
                                        // for position
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="positions">';
                                            ret += '<div  class="column div_1_of_1">';
                                                ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                                                ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    
                                                    if(positions){
                                                        $.each(positions,function(key, val){
                                                            if(htmlEntities(val.position) !=""){
                                                                ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                    ret += '<label class="tip" data-original-title="'+ htmlEntities(val.position) +'"><input type="checkbox" class="fp-user cl-fp-user-cc positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_positions_cc) +' value="'+ val.id +'" id="cl_CCPosition_'+ val.id +'"/><label for="cl_CCPosition_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
                                                                ret += '</div>';
                                                            }
                                                        })
                                                    }
                                                    var displayEmptyPos = "display";
                                                    if(ctr_pos==0){
                                                        displayEmptyUsers = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                                                ret += '</div>';
                                            ret += '</div>';    
                                        ret += '</div>';
                                            //for users
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
                                            ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
                                                ret += '<div class="getUsersFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    
                                                    if(users){
                                                        $.each(users,function(key, val){
                                                            ret += '<div style="width:30%;float:left; margin-bottom:2px;" class= fl-table-ellip "CSID-search-data"> ';
                                                                // user_name = val.first_name +' '+ val.last_name;
                                                                user_name = htmlEntities(val.display_name);
                                                                ret += '<label class="tip" data-original-title="'+ user_name +'"><input type="checkbox" class="fp-user cl-fp-user-cc users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_users_cc) +' value="'+ val.id +'" id="cl_CCuser_'+ val.id +'"/><label for="cl_CCuser_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ user_name +'</span></label>';
                                                            ret += '</div>';
                                                        })
                                                    }
                                                    var displayEmptyUsers = "display";
                                                    if(ctr_users==0){
                                                        displayEmptyUsers = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
                                                    ret += '</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>'; 
                                     ret += '</div>'; 
                                    //end cc
                                    //start bcc
                                    ret += '<label>BCC:</label>';
                                    ret += '<div class="fp-container cl-email-bcc CSID-search-container">';
                                        ret += '<div class="section clearing fl-field-style">';
                                             // ret += '<div style="width:30%;float:left"><label class="font-bold"><input type="checkbox" class="cl-email-requestor css-checkbox" value="All Users" id="cl-email-requestor3"/><label for="cl-email-requestor3" class="css-label"></label> Requestor</label></div>';
                                            ret += '<div class="column div_1_of_1">';
                                                ret +='<span class="font-bold" style="font-size:12px !important;"><label ><input type="checkbox" class="cl-email-processor css-checkbox" value="All Users" id="cl-email-processor3"/><label for="cl-email-processor3" class="css-label"></label> Processor</label></span>';
                                            ret += '</div>';
                                        ret += '</div>';
                                        ret += '<div class="fields_below formulaContainer section clearing fl-field-style">';
                                            
                                    
                                            ret += '<div class="input_position_below column div_1_of_1">';
                                                ret += '<span class="" style="float:right;font-size: 11px;font-weight: bold;">';
                                                ret += '    <label>Static: <input type="radio"  name="cl-workflow-mail-otherRecepient-type_bcc" checked="checked" class="cl-workflow-mail-otherRecepient-type formulaType css-checkbox" value="0" id="cl-workflow-mail-otherRecepient-type0_bcc"><label for="cl-workflow-mail-otherRecepient-type0_bcc" class="css-label"></label></label>';
                                                ret += '    <label>Dynamic: <input type="radio" name="cl-workflow-mail-otherRecepient-type_bcc" class="cl-workflow-mail-otherRecepient-type formulaType css-checkbox" value="1" id="cl-workflow-mail-otherRecepient-type1_bcc"><label for="cl-workflow-mail-otherRecepient-type1_bcc" class="css-label"></label><label>';
                                                ret += '</span>';
                                                ret += '<span class="font-bold" style="font-size:12px !important;"><label>Other Recipient:</label> </span>';
                                                // ret += '<input type="text" name="" class="form-text tip wfSettings" title="Email Address should be separated by semicolon" id="cl-workflow-mail-otherRecepient_bcc" placeholder="sample@domain.com" value="'+ cl_otherRecepient_bcc +'">';
                                                ret += '<textarea name="" class="form-textarea tip wfSettings ide-hover" readonly="readonly" data-ide-properties-type="ide-cl-workflow-mail-otherRecepient_bcc" title="Email Address should be separated by pipe(|)" id="cl-workflow-mail-otherRecepient_bcc" placeholder="sample@domain.com">'+ cl_otherRecepient_bcc +'</textarea>';
                                            ret += '</div>';
                                        ret += '</div>';
                                        ret += '<div class="section clearing fl-field-style">';
                                            ret += '<div class="column div_1_of_1" style="display:table;">';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allDepartments css-checkbox" value="All Users" id="cl-fp-allDept1"/><label for="cl-fp-allDept1" class="css-label"></label> All Departments</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allGroups css-checkbox" value="All Users" id="cl-fp-allGroup1"/><label for="cl-fp-allGroup1" class="css-label"></label> All Groups</label>';
                                                    ret +=      '<label class="" style="display:table-cell;vertical-align:middle;"><input type="checkbox" class="fp-allPositions css-checkbox" value="All Users" id="cl-fp-allPosition1"/><label for="cl-fp-allPosition1" class="css-label"></label> All Positions</label>';                                                                                        
                                                ret +='<span class="" style="font-size:12px !important; display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="cl-fp-allUsers1"/><label for="cl-fp-allUsers1" class="css-label"></label> All Users</label></span>';
                                                ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                        ret += '    <div class="pull-left display" style="width: 100%%;">';
                                        ret += '        <div class="fields_below">';
                                        ret += '            <div class="label_below2">Access Type:</div>';
                                        ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                                        ret += '                <select class="fl-input-select" id="access_type" rel="cl-email-bcc" style="width:100%">';
                                        ret += '                    <option value="1">All Fields</option>'; //setSelected('1',json['user_access_type'])
                                        ret += '                    <option value="2">Custom</option>';
                                        ret += '                </select>';
                                        ret += '            </div>';
                                        ret += '        </div>';
                                        ret += '    </div>';

                                        ret += '<div class="content-dialog-scroll" rel="cl-email-bcc" style="position: relative;  padding: 0px; width: 100%; height: 240px;overflow: hidden;" class="ps-container">';

                                             ret +='<div class="content-dialog" style="padding:1px; font-size:12px !important;">';
                                        // for department
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                                            ret += '<div class="column div_1_of_1">';
                                                ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                                                ret += '<div class="getDepartmentFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                
                                                if(departments){
                                                    $.each(departments,function(key, val){
                                                        ret += '<div style="width:30%;float:left; margin-bottom:2px;" class= fl-table-ellip "CSID-search-data"> ';
                                                        //setCheckedArray(value, arr)
                                                            ret += '<label  class="tip" data-original-title="'+ htmlEntities(val.department) +'"><input type="checkbox" class="fp-user cl-fp-user-bcc departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_departments_bcc) +' value="'+ val.id +'" id="cl_bCCDept_'+ val.id +'"/><label for="cl_bCCDept_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
                                                        ret += '</div>';
                                                    })
                                                }
                                                var displayEmptyDept = "display";
                                                if(ctr_dept==0){
                                                    displayEmptyDept = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                                                
                                                ret += '</div>';
                                             ret += '</div>';    
                                        ret += '</div>';
                                        // for groups
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                                            ret += '<div class="column div_1_of_1">';
                                                ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                                                ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    var groups = $(".all_groups").text();
                                                    var ctr_groups = 0;
                                                    if(groups){
                                                        groups = JSON.parse(groups);

                                                        $.each(groups,function(key, val){
                                                            if(htmlEntities(val.group_name) !=""){
                                                                ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                    ret += '<label class="tip" data-original-title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user fp-user-sms groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_groups_bcc) +' value="'+ val.id +'" id="cl_groups_bcc_' + val.id + '"/><label for="cl_groups_bcc_' + val.id + '" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
                                                                ret += '</div>';
                                                                ctr_groups++;
                                                            }
                                                        })
                                                        
                                                    }
                                                    var displayEmptyGroups = "display";
                                                    if(ctr_groups==0){
                                                        displayEmptyGroups = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyGroups +'" style="color:red">No Groups</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                         ret += '</div>';
                                        // for position
                                        ret += '<div class="container-formUser section clearing fl-field-style"  CSID-search-type="positions">';
                                            ret += '<div class="column div_1_of_1">';
                                                ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                                                ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    
                                                    if(positions){
                                                        $.each(positions,function(key, val){
                                                            if(htmlEntities(val.position) !=""){
                                                                ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                    ret += '<label class="tip" data-original-title="'+ htmlEntities(val.position) +'"><input type="checkbox" class="fp-user cl-fp-user-bcc positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_positions_bcc) +' value="'+ val.id +'" id="cl_BccPosition_'+ val.id +'"/><label for="cl_BccPosition_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
                                                                ret += '</div>';
                                                            }
                                                        })
                                                      
                                                    }
                                                    var displayEmptyPos = "display";
                                                    if(ctr_pos==0){
                                                        displayEmptyPos = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                                                ret += '</div>';
                                            ret += '</div>';    
                                        ret += '</div>';
                                            //for users
                                        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
                                            ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
                                                ret += '<div class="getUsersFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                    
                                                    if(users){
                                                        $.each(users,function(key, val){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                // user_name = val.first_name +' '+ val.last_name;
                                                                user_name = htmlEntities(val.display_name);
                                                                ret += '<label class="tip" data-original-title="'+ user_name +'"><input type="checkbox" class="fp-user cl-fp-user-bcc users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, cl_users_bcc) +' value="'+ val.id +'" id="cl_Users_BCC_'+ val.id +'"/><label for="cl_Users_BCC_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ user_name +'</span></label>';
                                                            ret += '</div>';
                                                        })
                                                        
                                                    }
                                                    var displayEmptyUsers = "display";
                                                    if(ctr_users==0){
                                                        displayEmptyUsers = ""
                                                    }
                                                    ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
                                                    ret += '</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>'; 
                                    ret += '</div>'; 
                                    //end bcc

                                ret += '</div>';
                                //end accordion
                                ret += '<div class="formulaContainer section clearing fl-field-style" style="margin-top:5px;">';
                                    ret += '<div class="input_position_below column div_1_of_1">';
                                        ret += '<div class="" style="float:right;font-size: 11px;font-weight: bold;">';
                                        ret += '    <label>Static: <input type="radio" '+ RequestPivacy.setCheckedArray("0", cl_email_title_type) +' name="cl-workflow-email-title-type" class="cl-workflow-email-title-type formulaType css-checkbox" value="0" id="cl-workflow-email-title-type0"><label for="cl-workflow-email-title-type0" class="css-label"></label></label>';
                                        ret += '    <label>Dynamic: <input type="radio" '+ RequestPivacy.setCheckedArray("1", cl_email_title_type) +' name="cl-workflow-email-title-type" class="cl-workflow-email-title-type formulaType css-checkbox" value="1" id="cl-workflow-email-title-type1"><label for="cl-workflow-email-title-type1" class="css-label"></label></label>';
                                        ret += '</div>';
                                        ret += '<span class="font-bold">Subject: </span>';
                                        ret += '<div class="wf-note-style" style="font-size:11px;color:red">Note: If this field is empty the default value is "Form Name"</div>';
                                        ret += '<textarea name="" class="form-textarea wfSettings formulaEmail" id="cl-workflow-email-title" data-ide-properties-type="ide-cl-workflow-email-title" placeholder="Subject">'+ cl_email_title +'</textarea>';
                                    ret += '</div>';
                                   
                                ret += '</div>';
                                ret += '<div class="formulaContainer section clearing fl-field-style">';
                                    
                                    //ret += '<div class="label_below2" style=""><label>If you want to include the link of request please use "@link" only. </label></div>';
      
                                    ret += '<div class="input_position_below column div_1_of_1">';
                                        ret += '<div class="" style="float:right;font-size: 11px;font-weight: bold;">';
                                        ret += '    <label>Static: <input type="radio" '+ RequestPivacy.setCheckedArray("0", cl_email_message_type) +' name="cl-workflow-email-message-type" class="cl-workflow-email-message-type formulaType css-checkbox" value="0" id="cl-workflow-email-message-type0"><label for="cl-workflow-email-message-type0" class="css-label"></label></label>';
                                        ret += '    <label>Dynamic: <input type="radio" '+ RequestPivacy.setCheckedArray("1", cl_email_message_type) +' name="cl-workflow-email-message-type" class="cl-workflow-email-message-type formulaType css-checkbox" value="1" id="cl-workflow-email-message-type1"><label for="cl-workflow-email-message-type1" class="css-label"></label></label>';
                                        ret += '</div>';
                                        ret += '<span class="font-bold" style="">Body: </span>';
                                        ret += '<div class="wf-note-style" style="font-size:11px;color:red">Note: If this field is empty the default value is "Requestor has cancelled the request."</div>';
                                        ret += '<textarea class="form-textarea wfSettings formulaEmail" placeholder="Body" id="cl-workflow-email-message" data-ide-properties-type="ide-cl-workflow-email-message" style="height:70px">'+ cl_email_message +'</textarea>';
                                    ret += '</div>';
                                   
                                ret += '</div>';
                                ret += '<div style="width:100%;display:none">';
                                    ret += '<div class="fields_below">';
                                        ret += '<div class="label_below2" style=""><label><input type="checkbox" class="css-checkbox" value="1" id="cl-incRequesLog" '+ RequestPivacy.setCheckedArray("1", cl_incRequesLog) +'><label for="incRequesLog" class="css-label"></label> Do you want to include Request logs?<label></div>';
                                        ret += '<div class="input_position_below" style="text-align: right;">';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '    </div>';
                            ret += '</div>';
                        ret += '</div>';
                     ret += '</div>';
                ret += '</div>';
            return ret;
            break;
        // case "workflow_chart-fields-trigger":
        //     var form_fields = $(".form_fields").val();
        //     var forms = $(".company_forms").text();
        //     form_fields = JSON.parse(form_fields);
        //     forms = JSON.parse(forms);
        //     ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">';
        //     ret +='<div class="content-dialog" style="width: 95%;height: auto;">';
        //         ret += '<div class="trigger-container-fields" style="">';
        //             ret += '<div style="float: left;width:23%;margin-right:10px">';
        //                 ret += '<div class="fields_below">';
        //                     ret += '<div class="label_below2">Action: <font color="red">*</font></div>';
        //                     ret += '<div class="input_position_below">';
        //                     ret += '<div style="font-size:11px"><label><input type="radio" name="workflow-trigger-action" value="Update" checked="checked" class="workflow-trigger-action"><span> Update Field</span></label></div>';
        //                     ret += '<div style="font-size:11px"><label><input type="radio" name="workflow-trigger-action" value="Create"  class="workflow-trigger-action"><span> Create Request</span></label></div>';
        //                     ret += '</div>';
        //                 ret += '</div>';
        //             ret += '</div>';
        //             //FOR UPDATE TRIGGER
        //             ret += '<div class="forUpdateTrgger">';
        //                 ret += '<div style="float:left;width:23%;margin-right:10px">';
        //                     ret += '<div class="fields_below" id="forFieldFilter">';
        //                         ret += '<div class="label_below2">Field Filter: <font color="red">*</font></div>';
        //                         ret += '<div class="input_position_below">';
        //                             ret += '<select class="form-select" id="workflow-trigger-fields-filter" style="margin-top:0px">';
        //                             for (i in form_fields) {
        //                                 // console.log(form_fields)
        //                                 ret += '<option>'+ form_fields[i] +'</option>';
        //                             }
        //                             ret += '</select>';
        //                             ret += '<img src="/images/loader/loading.gif" class="pull-left display processorLoad"/>';
        //                         ret += '</div>';
        //                     ret += '</div>';
        //                 ret += '</div>';
        //                 ret += '<div style="float:left;width:23%;margin-right:10px">';
        //                     ret += '<div class="fields_below" id="forFieldUpdate">';
        //                         ret += '<div class="label_below2">Field Update: <font color="red">*</font></div>';
        //                         ret += '<div class="input_position_below">';
        //                             ret += '<select class="form-select" id="workflow-trigger-fields-update" style="margin-top:0px">';
        //                             for (i in form_fields) {
        //                                 // console.log(form_fields)
        //                                 ret += '<option>'+ form_fields[i] +'</option>';
        //                             }
        //                             ret += '</select>';
        //                             ret += '<img src="/images/loader/loading.gif" class="pull-left display processorLoad"/>';
        //                         ret += '</div>';
        //                     ret += '</div>';
        //                 ret += '</div>';
        //                 ret += '<div id="forOperator" style="float:left;width:23%;margin-right:10px">';
        //                     ret += '<div class="fields_below">';
        //                         ret += '<div class="label_below2">Operator: <font color="red">*</font></div>';
        //                         ret += '<div class="input_position_below">';
        //                             ret += '<select class="form-select" id="workflow-trigger-operator" style="margin-top:0px">';
        //                                 ret += '<option value="Addition">Addition</option>'
        //                                 ret += '<option value="Subtraction">Subtraction</option>'
        //                                 ret += '<option value="Multiplication">Multiplication</option>'
        //                                 ret += '<option value="Division">Division</option>'
        //                             ret += '</select>';
        //                         ret += '</div>';
        //                     ret += '</div>';
        //                 ret += '</div>';
        //             ret += '</div>';
        //             //FOR CREATE TRIGGER
        //             ret += '<div class="forCreateTrgger display">';
        //                 ret += '<div style="float:left;width:35%;margin-right:10px">';
        //                      ret += '<div class="fields_below" id="forFormTrigger">';
        //                         ret += '<div class="label_below2">Form Trigger: <font color="red">*</font></div>';
        //                         ret += '<div class="input_position_below">';
        //                             ret += '<select class="form-select" id="workflow-form-trigger" style="margin-top:0px">';
        //                                 ret += '<option value="0">--Select Form--</option>';
        //                             for (i in forms) {
        //                                 // console.log(form_fields)
        //                                 ret += '<option value="'+ forms[i]['id'] +'">'+ forms[i]['form_name'] +'</option>';
        //                             }
        //                             ret += '</select>';
        //                             ret += '<img src="/images/loader/loading.gif" class="pull-left display processorLoad"/>';
        //                         ret += '</div>';
        //                     ret += '</div>';
        //                 ret += '</div>';
        //                 ret += '<div style="float:left;width:35%;margin-right:10px">';
        //                     ret += '<div class="fields_below" id="forReferenceField">';
        //                         ret += '<div class="label_below2">Refence Field: <font color="red">*</font></div>';
        //                         ret += '<div class="input_position_below">';
        //                             ret += '<select class="form-select" id="workflow-reference-fields-update" style="margin-top:0px">';
        //                             ret += '<option value="0">--Select Field--</option>';
        //                             ret += '</select>';
        //                             ret += '<img src="/images/loader/loading.gif" class="pull-left display processorLoad"/>';
        //                         ret += '</div>';
        //                     ret += '</div>';
        //                 ret += '</div>';
        //             ret += '</div>';


        //         ret += '</div>';
        //         ret += '</div>';
        //     ret += '</div>';
        //     return ret;


        // break;
        //multi
        
        case "workflow_chart-fields-trigger":
            var form_fields = $(".form_fields").val();
            var forms = $(".company_forms").text();
            forms = JSON.parse(forms);
            form_fields = JSON.parse(form_fields);
            ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; min-height: 300px;max-height: 350px;overflow: hidden;" class="ps-container">';
            ret +='<div class="content-dialog trigger-container" >'; //style="width: 95%;height: auto;"
                ret += WorkflowTrigger.trigger_html(0,"");
            ret += '</div>';
        ret += '</div>';

        return ret;
        break;
        case "workflow_chart-user_setup" :
            var users = if_undefinded(json_nodes_var[''+ parent_id +'']['users'],"");
            var node_type = json_nodes_var[''+ parent_id +'']['type_rel'];
            var displayEditor = "";

            if(node_type=="4"){
                displayEditor = "isDisplayNone";
            }
            var departments_editor = "";
            var positions_editor = "";
            var users_editor = "";
            var groups_editor = "";
            var departments_viewer = "";
            var positions_viewer = "";
            var users_viewer = "";
            var groups_viewer = "";
            if(users){
                //user_editor
                var user_editor = users['user_editor'];
                if(user_editor){
                    departments_editor = user_editor['departments'];
                    positions_editor = user_editor['positions'];
                    users_editor = user_editor['users'];
                    groups_editor = user_editor['groups'];
                }
                
                var user_viewer = users['user_viewer'];
                if(user_viewer){
                    departments_viewer = user_viewer['departments'];
                    positions_viewer = user_viewer['positions'];
                    users_viewer = user_viewer['users'];
                    groups_viewer = user_viewer['groups'];
                }
            }
            var category_id = $("#category_id").val();
            ret += '<div class="object_properties_content">';
                ret += '<div class="content-dialog-scroll-container" style="position: relative; padding: 0px; max-height: 530px;overflow: hidden;" class="ps-container-a">';
                    ret +='<div class="content-dialog-container" style="width: 95%;height: auto;">';
                         ret += '<div id="accordion-container">';
                            ret += '    <ul>';
                            if(category_id!="0"){ //hide if others category
                                ret += '        <li><a href="#tabs-1">Viewer</a></li>';
                            }
                            ret += '        <li class="'+ displayEditor +'"><a href="#tabs-2">Editor</a></li>';
                            ret += '    </ul>';
                            if(category_id!="0"){

                                ret += '   <div id="tabs-1" style="padding:10px;">';            
                            //start viewer_editor
                                // ret += '<label>User Editor:</label>';
                                ret += '    <div class="pull-left display" style="width: 100%">';
                                ret += '        <div class="fields_below">';
                                ret += '            <div class="label_below2">Access Type:</div>';
                                ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                                ret += '                <select class="fl-input-select" id="access_type" rel="users-viewer">';
                                ret += '                    <option value="1">All Users</option>'; //setSelected('1',json['user_access_type'])
                                ret += '                    <option value="2">Custom</option>';
                                ret += '                </select>';
                                ret += '            </div>';
                                ret += '        </div>';
                                ret += '    </div>';
                                ret += '<div class="fp-container users-viewer CSID-search-container" rel="users-viewer">';
                                    // ret += '<div style="width:100%;display:inline-block">';
                                    //     ret += '<div style="width:30%;float:left"><label class="font-bold"><input type="checkbox" class="user-requestor" value="All Users"/> Requestor</label></div>';
                                    //     ret += '<div style="width:50%;float:left"><label class="font-bold"><input type="checkbox" class="user-processor" value="All Users"/> Processor</label> </div>';
                                    // ret += '</div>';
                                    ret += '<div class="section clearing fl-field-style">';
                                        ret += '<div class="column div_1_of_1" style="display:table;">';
                                            ret += '<span class="font-bold" style="font-size:12px !important; display:table-cell; vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="fp-allUsers3"/><label for="fp-allUsers3" class="css-label"></label> All Users</label></span>';
                                             ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                    ret += '<div class="content-dialog-scroll" style="position: relative; padding: 0px; width: 100%; height: 240px;overflow: hidden;" class="ps-container">';

                                         ret +='<div class="content-dialog" style="padding:1px; font-size:12px !important;">';

                                    //ret += '<div style="margin-left: 20px;max-height:240px;overflow-y:auto;overflow-x:hidden">';
                                    // for department
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                                            ret += '<div class="getDepartmentFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                            var departments = $(".form_departments").text();
                                            var ctr_dept = 0;
                                            if(departments){
                                                departments = JSON.parse(departments);
                                                $.each(departments,function(key, val){
                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                    //setCheckedArray(value, arr)
                                                        ret += '<label class="tip" data-original-title="' + htmlEntities(val.department) + '"><input type="checkbox" class="fp-user fp-user-to departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, departments_viewer) +' value="'+ val.id +'" id="fp-user-to_dept_3_'+ val.id +'"/><label for="fp-user-to_dept_3_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
                                                    ret += '</div>';
                                                    ctr_dept++
                                                })
                                            }
                                            var displayEmptyDept = "display";
                                            if(ctr_dept==0){
                                                displayEmptyDept = ""
                                            }
                                            ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                    // for groups
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                                            ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                var groups = $(".form_group").text();
                                                var ctr_groups = 0;
                                                if(groups){
                                                    groups = JSON.parse(groups);

                                                    $.each(groups,function(key, val){
                                                        if(val.groups !=""){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                ret += '<label class="tip" data-original-title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user fp-user-to groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, groups_viewer) +' value="'+ val.id +'" id="Viewer_group_'+ val.id +'"/><label for="Viewer_group_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
                                                            ret += '</div>';
                                                            ctr_groups++;
                                                        }
                                                    })
                                                    
                                                }
                                                var displayEmptyGroup = "display";
                                                if(ctr_groups==0){
                                                    displayEmptyGroup = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyGroup +'" style="color:red">No Groups</div>';
                                                
                                            ret += '</div>';
                                        ret += '</div>';    
                                    ret += '</div>';
                                    // for position
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="positions">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                                            ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                var positions = $(".form_positions").text();
                                                var ctr_pos = 0;
                                                if(positions){
                                                    positions = JSON.parse(positions);

                                                    $.each(positions,function(key, val){
                                                        if(htmlEntities(val.position) !=""){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                ret += '<label class="tip" data-original-title="' + htmlEntities(val.position) + '"><input type="checkbox" class="fp-user fp-user-to positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, positions_viewer) +' value="'+ val.id +'" id="Viewer_position_'+ val.id +'"/><label for="Viewer_position_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
                                                            ret += '</div>';
                                                            ctr_pos++;
                                                        }
                                                    })
                                                    
                                                }
                                                var displayEmptyPos = "display";
                                                if(ctr_pos==0){
                                                    displayEmptyPos = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                                            ret += '</div>';        
                                        ret += '</div>';
                                    ret += '</div>';
                                        //for users
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
                                        ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
                                            ret += '<div class="getUsersFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                var users = $(".form_users").text();
                                                var ctr_users = 0;
                                                var user_name = "";
                                                if(users){
                                                    users = JSON.parse(users);
                                                    $.each(users,function(key, val){
                                                        // user_name = val.first_name +' '+ val.last_name;
                                                        user_name = htmlEntities(val.display_name);
                                                        ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                            ret += '<label class="tip" data-original-title="'+ user_name +'"><input type="checkbox" class="fp-user fp-user-to users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, users_viewer) +' value="'+ val.id +'" id="Viewer_Users_'+ val.id +'"/><label for="Viewer_Users_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ user_name +'</span></label>';
                                                        ret += '</div>';
                                                        ctr_users++
                                                    })
                                                }
                                                var displayEmptyUsers = "display";
                                                if(ctr_users==0){
                                                    displayEmptyUsers = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
                                                ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>'; 
                                ret += '</div>'; 
                                ret += '</div>'; 
                                //end user_viewer
                            }
                                ret += '   <div class="'+ displayEditor +'" id="tabs-2">';            
                            //start user_editor
                                // ret += '<label>User Editor:</label>';
                                ret += '    <div class="pull-left display" style="width: 100%">';
                                ret += '        <div class="fields_below">';
                                ret += '            <div class="label_below2">Access Type:</div>';
                                ret += '            <div class="input_position_below" style="margin-bottom:10px;">';
                                ret += '                <select class="fl-input-select" id="access_type" rel="users-editor">';
                                ret += '                    <option value="1">All Users</option>'; //setSelected('1',json['user_access_type'])
                                ret += '                    <option value="2">Custom</option>';
                                ret += '                </select>';
                                ret += '            </div>';
                                ret += '        </div>';
                                ret += '    </div>';
                                ret += '<div class="fp-container users-editor CSID-search-container" rel="users-editor">';
                                    ret += '<div class="section clearing fl-field-style">';
                                        ret += '<div class="column div_1_of_1" >';
                                            ret += '<span class="font-bold"><label><input type="checkbox" class="user-requestor css-checkbox" value="All Users" id="user-requestor"/><label for="user-requestor" class="css-label"></label> Requestor</label></span>';
                                        ret += '</div>';
                                        // ret += '<div style="width:50%;float:left"><label class="font-bold"><input type="checkbox" class="user-processor" value="All Users"/> Processor</label> </div>';
                                    ret += '</div>';
                                    ret += '<div class="section clearing fl-field-style">';
                                        ret += '<div class="column div_1_of_1" style="display:table;">';
                                            ret += '<span class="font-bold" style="display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="fp-allUsers2"/><label for="fp-allUsers2" class="css-label"></label> All Users</label></span>';
                                            ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
                                        ret += '</div>';
                                   ret += '</div>';
                                
                                     ret += '<div class="content-dialog-scroll" style="position: relative;  padding: 0px; width: 100%; height: 240px;overflow: hidden;" class="ps-container">';

                                         ret +='<div class="content-dialog" style="padding:1px; font-size:12px !important;">';

                                    //ret += '<div style="margin-left: 20px;max-height:240px;overflow-y:auto;overflow-x:hidden">';
                                    // for department
                                    
                                    if(category_id==0){
                                        var departments = $(".departments").text();
                                        var groups = $(".all_groups").text();
                                        var positions = $(".positions").text();
                                        var users = $(".users").text();
                                    }else{
                                        var departments = $(".form_departments").text();
                                        var groups = $(".form_group").text();
                                        var positions = $(".form_positions").text();
                                        var users = $(".form_users").text();
                                    }

                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                                            ret += '<div class="getDepartmentFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                            
                                            var ctr_dept = 0;
                                            if(departments){
                                                departments = JSON.parse(departments);
                                                $.each(departments,function(key, val){
                                                    ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                    //setCheckedArray(value, arr)
                                                        ret += '<label class="tip" data-original-title="' + htmlEntities(val.department) + '"><input type="checkbox" class="fp-user fp-user-to departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, departments_editor) +' value="'+ val.id +'" id="fp-user-to_dept_2_'+ val.id +'"/><label for="fp-user-to_dept_2_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
                                                    ret += '</div>';
                                                    ctr_dept++
                                                })
                                            }
                                            var displayEmptyDept = "display";
                                            if(ctr_dept==0){
                                                displayEmptyDept = ""
                                            }
                                            ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                    // for groups
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                                            ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                
                                                var ctr_groups = 0;
                                                if(groups){
                                                    groups = JSON.parse(groups);

                                                    $.each(groups,function(key, val){
                                                        if(val.groups !=""){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                ret += '<label class="tip" data-original-title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user fp-user-to groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, groups_editor) +' value="'+ val.id +'" id="Editor_group_'+ val.id +'"/><label for="Editor_group_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
                                                            ret += '</div>';
                                                            ctr_groups++;
                                                        }
                                                    })
                                                    
                                                }
                                                var displayEmptyGroup = "display";
                                                if(ctr_groups==0){
                                                    displayEmptyGroup = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyGroup +'" style="color:red">No Groups</div>';
                                                
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                    // for position
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="positions">';
                                        ret += '<div class="column div_1_of_1">';
                                            ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                                            ret += '<div class="getPositionFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                
                                                var ctr_pos = 0;
                                                if(positions){
                                                    positions = JSON.parse(positions);

                                                    $.each(positions,function(key, val){
                                                        if(htmlEntities(val.position) !=""){
                                                            ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                                ret += '<label class="tip" data-original-title="' + htmlEntities(val.position) + '"><input type="checkbox" class="fp-user fp-user-to positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, positions_editor) +' value="'+ val.id +'" id="Editor_position_'+ val.id +'"/><label for="Editor_position_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
                                                            ret += '</div>';
                                                            ctr_pos++;
                                                        }
                                                    })
                                                    
                                                }
                                                var displayEmptyPos = "display";
                                                if(ctr_pos==0){
                                                    displayEmptyPos = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                                                
                                            ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                        //for users
                                    ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
                                        ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
                                            ret += '<div class="getUsersFF" style="display:inline-block;width:120%;margin-left:10px;margin-top:5px;">';
                                                
                                                var ctr_users = 0;
                                                var user_name = "";
                                                if(users){
                                                    users = JSON.parse(users);
                                                    $.each(users,function(key, val){
                                                        // user_name = val.first_name +' '+ val.last_name;
                                                        user_name = htmlEntities(val.display_name);
                                                        ret += '<div style="width:30%;float:left;margin-bottom:2px;" class=" fl-table-ellip CSID-search-data"> ';
                                                            ret += '<label class="tip" data-original-title="'+ user_name +'"><input type="checkbox" class="fp-user fp-user-to users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, users_editor) +' value="'+ val.id +'" id="UsersTO_'+ val.id +'"/><label for="UsersTO_'+ val.id +'" class="css-label"></label> <span class="limit-text-wf CSID-search-data-text">'+ user_name +'</span></label>';
                                                        ret += '</div>';
                                                        ctr_users++;
                                                    })
                                                }
                                                var displayEmptyUsers = "display";
                                                if(ctr_users==0){
                                                    displayEmptyUsers = ""
                                                }
                                                ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
                                                ret += '</div>';
                                        ret += '</div>';
                                    ret += '</div>';
                                ret += '</div>'; 
                                //end user_editor
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                     ret += '</div>';
                ret += '</div>';
            return ret;
            break;
        case "workflow_chart-database":
            ret += '<div class="object_properties_content">';
                // ret += '<div class="scrollbarM object_properties_wrapper">';
                    ret += '<div id="accordion-container" class="flas" style="margin:0px!important;">';
                        ret += '    <ul>';
                        ret += '        <li><a href="#dbtabs-2">Upload to External DB</a></li>';
                        ret += '        <li class="display"><a href="#dbtabs-3">Import to Formalistics DB</a></li>';
                        ret += '    </ul>';
                        ret += '<div id="dbtabs-2" class="clearfix external_db_triggers" trigger_type="upload-ext" style="padding:10px;">';
                            ret += WorkflowTrigger.trigger_html(0,"","upload-external","workflow_chart-database");
                        ret += '</div>';
                        ret += '<div id="dbtabs-3" class="clearfix" trigger_type="import-ext" style="padding:10px;">';
                            ret += WorkflowTrigger.trigger_html(0,"","import-forma","workflow_chart-database");
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';
            return ret;
            break;
        
    }
}
/**OTHER FUNCTIONS***/


//deleted function setSelected(value, selectedValue){  ... move to global.js by michael espano 10:21 AM 10/8/2015


function hasButton(button_value,button_json) {
    var ret = "";
    if (typeof button_json!="undefined") {
        for (var i in button_json) {
            if (button_value == button_json[i]['button_name']) {
                ret = "disabled = 'disabled'";
            }
        }
    }
    return ret;
}
//Dialog for get form for workflow
function getFormForWorkflow(callback) {
    var form_id = $(".form_chosen").val();
    $.post("/ajax/getFormProperties",{action:"getForm"},function(forms){
        var forms_json = JSON.parse(forms);
        var ret = "";
        ret += '<div style="float:left;width:50%;">';
            ret += '<h3 class="pull-left fl-margin-bottom">';
                ret += '<i class="icon-asterisk"></i> ' + "Get Workspace";
            ret += '</h3>';
        ret += '</div>';
        //ret += '<div style="float:right;width:50%;text-align:right;margin-top:-5px;">';
        //    ret += '<input type="button" class="btn-basicBtn getFormForWorkflow" id="" value="Ok" node-data-id="" style="">';
        //ret += '</div>';
        ret += '<div class="hr"></div>';
                ret += '<div class="fields">';
                    ret += '<div class="label_below2 fl-get-workspace-popup"> Please select a form: </div>';
                    ret += '<div class="input_position">';
                        ret += '<select class="form-select forms_selection fl-margin-bottom" style="margin-top:5px; margin-bottom:5px;">';
                            ret += '<option value="0">----------------------------Select----------------------------</option>';
                            for (var i in forms_json){
                                
                                 ret += '<option value="'+ forms_json[i]['form_id'] +'">'+ forms_json[i]['form_name'] +'</option> ';
                            }
                        ret += '</select>';
                    ret += '</div>';
                ret += '</div>';
                ret += '<div class="fields">';
                    ret += '<div class="label_below2"></div>';
                    ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                        ret += '<input type="button" class=" fl-positive-btn btn-blueBtn getFormForWorkflow fl-margin-right" value="OK"> ';
                        //ret += '<a href="/"><input type="button" class="btn-basicBtn" id="workflow-cancel" value="Cancel"></a>';
                    ret += '</div>';
                ret += '</div>';
        callback(ret);
    })
}

function getUser(parent_id,search,type,callback) {
    
    $.post("/ajax/regUser",{action:"getUser",search:search,type:"orgchart"},function(result){
        var ret = "";
        if(result){
            var user_json = JSON.parse(result);
            var json_nodes = $("body").data();
            var json_nodes_var = json_nodes['nodes'];
            var orgchart_user_head = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_user_head'],"");
            var orgchart_dept_assistant = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_dept_assistant'],0);
            var orgchart_dept_members = if_undefinded(json_nodes_var[''+ parent_id +'']['orgchart_dept_members'],"");
            var userID = "";
            var name = "";
            var avatar = "";
            var headUser = "";
            var assHeadUser = '';
            var memberUser = "";
            var chk_head = "";
            var chk_assistant = '<label class="orgchart_user_wrap"><img src="/images/avatar/small.png" width="25" height="25" class="avatar userAvatar" onerror="this.src='+ $('#broken_image').text() +'"><span style="position: absolute;margin-top: 7px;margin-left:11px"><input class="asst_head_user css-checkbox" type="radio" name="assheadUser" '+ setChecked("0", orgchart_dept_assistant) +' value="0" id="assheadUser_' + setChecked("0", orgchart_dept_assistant) + '" /><label for="assheadUser_' + setChecked("0", orgchart_dept_assistant) + '" class="css-label"></label> None</span></label><br />';
            var chk_member = "";
            // orgchart_dept_members = $(".orgchart-user-search").data();
            var orgchartPartialChecked = $(".orgchart-user-search").data();
            orgchartPartialChecked = if_undefinded(orgchartPartialChecked['members_id'],"");
            orgchart_dept_members = orgchart_dept_members.concat(orgchartPartialChecked);

            for (var i in user_json){
                userID = user_json[i].userID;
                name = user_json[i]['display_name'];
                avatar = "<span class='avatar-oc'>"+user_json[i]['avatar']+"</span>";
                if(setChecked(userID, orgchart_user_head)){
                    chk_head += '<label class="orgchart_user_wrap">'+ avatar +'<span style="position: absolute;margin-top: 7px;margin-left:11px"><input class="head_user css-checkbox" type="radio" name="headUser" checked="checked" value="' + userID + '" id="headUser_' + userID + '"/><label for="headUser_' + userID + '" class="css-label"></label> <span class="limit-text-wf">' + name + '</span></span></label><br />';
                }else{
                    headUser += '<label class="orgchart_user_wrap">'+ avatar +'<span style="position: absolute;margin-top: 7px;margin-left:11px"><input class="head_user css-checkbox" type="radio" name="headUser"  value="' + userID + '" id="headUser_' + userID + '"/><label for="headUser_' + userID + '" class="css-label"></label> <span class="limit-text-wf">' + name + '</span></span></label><br />';
                }
                if(setChecked(userID, orgchart_dept_assistant)){
                    chk_assistant += '<label class="orgchart_user_wrap">'+ avatar +'<span style="position: absolute;margin-top: 7px;margin-left:11px"><input class="asst_head_user css-checkbox" type="radio" name="assheadUser" checked="checked" value="' + userID + '" id="assheadUser_' + userID + '"/><label for="assheadUser_' + userID + '" class="css-label"></label> <span class="limit-text-wf">' + name + '</span></span></label><br />';
                }else{
                    assHeadUser += '<label class="orgchart_user_wrap">'+ avatar +'<span style="position: absolute;margin-top: 7px;margin-left:11px"><input class="asst_head_user css-checkbox" type="radio" name="assheadUser" value="' + userID + '" id="assheadUser_' + userID + '"/><label for="assheadUser_' + userID + '" class="css-label"></label> <span class="limit-text-wf">' + name + '</span></span></label><br />';
                }
                if(setCheckedFromArray(orgchart_dept_members,userID)){
                    chk_member += '<label class="orgchart_user_wrap">'+ avatar +'<span style="position: absolute;margin-top: 7px;margin-left:11px"><input class="member_user css-checkbox" type="checkbox" name="memberUser" checked="checked" value="' + userID + '" id="' + userID + '" /><label for="'+ userID +'" class="css-label"></label> <span class="limit-text-wf">' + name + '</span></span></label><br />';
                }else{
                    memberUser += '<label class="orgchart_user_wrap">'+ avatar +'<span style="position: absolute;margin-top: 7px;margin-left:11px"><input class="member_user css-checkbox" type="checkbox" name="memberUser"   value="' + userID + '" id="' + userID + '" /><label for="' + userID + '" class="css-label"></label> <span class="limit-text-wf">' + name + '</span></span></label><br />';
                }
                    
            }
            headUser += "<br />";
            memberUser+= "<br />";
            if(type==0){
                $(".loadHeadUser").html(chk_head+headUser);
                $(".loadAssistantHeadUser").html(chk_assistant+assHeadUser);
                $(".loadMemberUser").html(chk_member+memberUser);
            }else if(type=="orgchart-head-search"){
                $(".loadHeadUser").html(chk_head+headUser);
            }else if(type=="orgchart-assistant-head-search"){
                $(".loadAssistantHeadUser").html(chk_assistant+assHeadUser);
            }else if(type=="orgchart-member-search"){
                $(".loadMemberUser").html(chk_member+memberUser);
            }
            
            if($(".userLevel").val()!=2){
                $(".member_user, .asst_head_user, .head_user").attr("disabled","disabled");
            }

            callback(1)
        }
    });
}


function disabledCheckedOptions(){
    
}
function removeDialog(obj){
    var type = $(obj).attr("object_type");
    var node_data_id = $(obj).attr("node-data-id");
    var json_nodes = $("body").data();
    var json_nodes_var = json_nodes['nodes'];
    if(type=="organizational_chart"){
        // json_nodes_var[''+ node_data_id +'']['node_text'] = "";
        // json_nodes_var[''+ node_data_id +'']['node_color'] = "";
        // json_nodes_var[''+ node_data_id +'']['orgchart_user_head'] = "";
        // json_nodes_var[''+ node_data_id +'']['orgchart_dept_assistant'] = "";
        // json_nodes_var[''+ node_data_id +'']['orgchart_dept_members'] = "";
    }   
}