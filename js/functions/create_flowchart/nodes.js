//initialize variables 
var counter = 1;
var wpc =  18; //workspace padding container
//node plugin
var pathname = window.location.pathname;

if(pathname=="/workflow" || pathname == "/organizational_chart"){
	var checkCollisionDroppableBoundaries = function(this_element){
		if($(this_element).closest(".workspace").length == 0){
			alert("the element is not in the droppable containment");
			return false;
		}

		// var droppable_top = 0;
		// var droppable_left = 0;
		// var droppable_right = $("#ws-container #droppable").width();
		// var droppable_bottom = $("#ws-container #droppable").height();
		var droppable_ele = $(this_element).parent();
		var droppable_top = 0 + Number( (droppable_ele.css("padding-top")||"").replace("px","")||0 );
	    var droppable_left = 0 + Number( (droppable_ele.css("padding-left")||"").replace("px","")||0 );
		var droppable_right = droppable_ele.width() + Number( (droppable_ele.css("padding-left")||"").replace("px","")||0 );
		var droppable_bottom = droppable_ele.height() + Number( (droppable_ele.css("padding-top")||"").replace("px","")||0 );

		var element_right = $(this_element).position().left + $(this_element).outerWidth();
		var element_bottom = $(this_element).position().top + $(this_element).outerHeight();

		var element_top = $(this_element).position().top;
		var element_left = $(this_element).position().left;
		// $(".ws-status-bar").html(
		//  element_right+" <= "+droppable_right+" && "+
		//  "<br/>"+element_bottom+" <= "+droppable_bottom+" && "+
		//  "<br/>"+element_top+" >= "+droppable_top+" && "+
		//  "<br/>"+element_left+" >= "+droppable_left
		// );

		if(element_right >= droppable_right){
			$(this_element).css({
				left:droppable_right - $(this_element).outerWidth()
			});
		}
		if(element_bottom >= droppable_bottom){
			$(this_element).css({
				top:droppable_bottom - $(this_element).outerHeight()
			});
		}
		if(element_top <= droppable_top){
			$(this_element).css({
				top:droppable_top
			});
		}
		if(element_left <= droppable_left){
			$(this_element).css({
				left:droppable_left
			});
		}

		//OLD VERSION

		// if($(this_element).closest(".workspace").length == 0){
		// 	alert("the element is not in the droppable containment");
		// 	return false;
		// }

		// // var droppable_top = 0;
		// // var droppable_left = 0;
		// // var droppable_right = $("#ws-container #droppable").width();
		// // var droppable_bottom = $("#ws-container #droppable").height();

		// var droppable_top = 0;
		// var droppable_left = 0;
		// var droppable_right = $(this_element).parent().width();
		// var droppable_bottom = $(this_element).parent().height();

		// var element_right = $(this_element).position().left + $(this_element).outerWidth();
		// var element_bottom = $(this_element).position().top + $(this_element).outerHeight();

		// var element_top = $(this_element).position().top;
		// var element_left = $(this_element).position().left;
		// // $(".ws-status-bar").html(
		// // 	element_right+" <= "+droppable_right+" && "+
		// // 	"<br/>"+element_bottom+" <= "+droppable_bottom+" && "+
		// // 	"<br/>"+element_top+" >= "+droppable_top+" && "+
		// // 	"<br/>"+element_left+" >= "+droppable_left
		// // );

		// if(element_right >= droppable_right){
		// 	$(this_element).css({
		// 		left:droppable_right - $(this_element).outerWidth()
		// 	});
		// }
		// if(element_bottom >= droppable_bottom){
		// 	$(this_element).css({
		// 		top:droppable_bottom - $(this_element).outerHeight()
		// 	});
		// }
		// if(element_top <= droppable_top){
		// 	$(this_element).css({
		// 		top:droppable_top
		// 	});
		// }
		// if(element_left <= droppable_left){
		// 	$(this_element).css({
		// 		left:droppable_left
		// 	});
		// }
	}
}


(function($) {
    $.fn.node = function(options) {
	// key value pair on the custom plugin.
	var settings = $.extend({
	    event	: "",
	    container	: "",
	    startCount 	: 0,
	    defaultText	: "Node",
	    nodeColor	: "#000",
	    lineColor	: "#000",
	    click	: null,
	    state	: true,
	    height	: "50px",
	    width	: "130px",
	    onDrop	: true,
	    settings_property	: "",
	    drop	: true,
	    hasDrag	: true,
	    type_rel 	: 0,
	    maxCount	: 0
        }, options);
	
	//counter = settings.startCount; //start counter
	var ctr_arr = [];
	$(".outer-node").each(function(){
	    ctr_arr.push(this.id.split("_")[1]);
	})
	ctr_arr.sort(function(a,b){return a-b});
	if(ctr_arr.length>0){
		counter = ctr_arr[ctr_arr.length-1];
		counter = parseInt(counter)+1;
	}
	//Event to add node
	var addnode_bool = true;
	this.bind(settings.event, function () {
	    //start and end only 1 node
	    var count_node = 0;
	    var json_nodes = $("body").data();
            var json_nodes_var = json_nodes['nodes'];
	    for (var i in json_nodes_var) {
			if (json_nodes_var[i]['type_rel']==settings.type_rel) {
			    count_node++;
			}
	    }
	    if (count_node<settings.maxCount || settings.maxCount==0) {
		//Node
		var element = node.addNode({
		    container 	: settings.container,
		    type 		: settings.type,
		    defaultText	: settings.defaultText,
		    nodeColor	: settings.nodeColor,
		    lineColor	: settings.lineColor,
		    click		: settings.click,
		    enableRuler	: null,
		    state		: settings.state,
		    height		: settings.height,
		    width		: settings.width,
		    onDrop		: settings.onDrop,
		    drop		: settings.drop,
		    settings_property:settings.settings_property,
		    hasDrag		:settings.hasDrag,
		    type_rel	:settings.type_rel
		});
		node.mouseoverNode();
		node.mouseleaveNode();
		node.mousedown();
		node.activateNode();
		node.dropToNode(".outer-node",settings.container,settings.lineColor);
		node.dragAlso(element);
		node.dragNode(".outer-node",settings.container,settings.enableRuler,settings.lineColor);
		node.resizeNode(settings.container,settings.lineColor);
		node.resizeStartNode(settings.container,settings.lineColor);
		//Node Settings
		nodeSettings.minimizeNode(settings.lineColor);
		nodeSettings.maximizeNode(settings.lineColor);
		    //Line Trigger Drag
	    }else{
			showNotification({
                    message: "You already reached the maximum limit for this node.",
                    type: "error",
                    autoClose: true,
                    duration: 3
            });
	    }


	    //
	    bind_onBeforeOnload(1);
	});
	
	//container click remove all active nodes
	$(settings.container).click(function(e){
		evtarget = (window.event) ? window.event.srcElement /* for IE */ : e.target;
		if($(evtarget).closest(".outer-node").length===0){
			node.removeAllActiveNode();
		}
		
	    

	})
	var pathname = window.location.pathname;
	if(pathname=="/workflow" || pathname == "/organizational_chart"){
		//initialization for line
		$("html, .content_wrapper").droppable({
			"drop":function(){
			}
		});
	    }
	}
    //live functions/node settings
    $(document).ready(function(){
	nodeSettings.removeNode(); //remove node

	// Hover on movable node
		$("body").on("mouseenter",".node-drag-wrapper",function(){
	    	var node_id = $(this).attr("id");
	    	$("#tip_" + node_id).tooltip();
		});
		$("body").on("mouseleave",".node-drag-wrapper",function(){
			var node_id = $(this).attr("id");
	    	$("#tip_" + node_id).tooltip('destroy');
		});
		
		
    })
    
    
    /**********  Other Node functions  *********/
    //json key value pair
    node = {
	/*
	Add node Design
	@json = collection of data inside the node
       */
	addNode : function(nodeSettings){
	    json = {
		"node_left" 	: "0px",
		"node_top" 	: "0px",
		"node_data_id" 	: "node_"+counter,
		"node_text" 	: nodeSettings.defaultText,
		"node_type" 	: nodeSettings.type,
		"node_container": nodeSettings.container,
		"node_color" 	: nodeSettings.nodeColor,
		"node_state"	: nodeSettings.state,
		"node_height"	: nodeSettings.height,
		"node_width"	: nodeSettings.width,
		"node_onDrop"	: nodeSettings.onDrop,
		"node_drop"	: nodeSettings.drop,
		"node_settings_property":nodeSettings.settings_property,
		"node_hasDrag"	:nodeSettings.hasDrag,
		"type_rel"	:nodeSettings.type_rel,
	    }
	    counter++;
	    var element = nodeDesign(json); //
	    drop_top = $(document).scrollTop();
		drop_left = $(document).scrollLeft();
	    $(element).css({
	    	"top":drop_top + wpc,
	    	"left":drop_left + wpc,
	    })
	    $(element).snapToGrid({
        	"topSetBack":5,
        	"leftSetBack":5
        });
	    //Event on appended nodes
	    $(element).on({
			click : function(){
			    if(nodeSettings.click){
				nodeSettings.click();
			    }
			}
	    })
	    return element;
	},
	dragNode : function(element,container,enableRuler,lineColor){

		if($(".userLevel").val()!=2){
			$(element).find(".node-action-content").css({
				"cursor":"auto"
			})
			return;
		}
		var option_focused_component_datas = [];
		$(element).draggable({
			containment:container,
			handle:".node-drag-text-wrapper, .handle-node-drag",
			helper: "clone",
			start : function(event,ui){
                               // $(this).css("box-shadow","rgba(0,0,0,0.2) 5px 5px 7px");
				// if(event.ctrlKey){
				// 	$(this).addClass("dragAlso");
				// }else{
				// 	$(".outer-node").removeClass("dragAlso");
				// }
				
			},
			drag : function(event,ui){
				
				
			    /* Hide object properties */
			    // $(".node-setting").popover('hide')
			},
			stop : function(event,ui){
				// $(this).snapToGrid({
				// 	"topSetBack":5,
				// 	"leftSetBack":5
				// });
                                //$(this).css("box-shadow","");
				var drag_ele_origin_pos_left = ui.originalPosition.left;
				var drag_ele_origin_pos_top = ui.originalPosition.top;
				drag_ele_origin_distance_to_current_left =  parseInt($(ui.helper).css("left").substring(0,$(ui.helper).css("left").length - 2)) - drag_ele_origin_pos_left;
				drag_ele_origin_distance_to_current_top =   parseInt($(ui.helper).css("top").substring(0,$(ui.helper).css("top").length - 2)) - drag_ele_origin_pos_top;
				console.log('new pos',$(this).position().left,ui['helper'].position().left)
				if(option_focused_component_datas.length == 0){
					if($(this).hasClass("dragAlso")){
						$(".dragAlso").each(function(i){
							option_focused_component_datas.push({
								"data-node-id":$(this).attr("data-node-id"),
								"parent-node" : $(".lines[child='"+ $(this).attr("data-node-id") +"']").attr("parent"),
								"child-node" : $(".lines[parent='"+ $(this).attr("data-node-id") +"']").attr("child"),
								"object":$(this),
								"originLeft":parseInt($(this).css("left").substring(0,$(this).css("left").length - 2)),
								"originTop":parseInt($(this).css("top").substring(0,$(this).css("top").length - 2))
							});
						})
					}else{
						option_focused_component_datas.push({
							"data-node-id":$(this).attr("data-node-id"),
							"parent-node" : $(".lines[child='"+ $(this).attr("data-node-id") +"']").attr("parent"),
							"child-node" : $(".lines[parent='"+ $(this).attr("data-node-id") +"']").attr("child"),
							"object":$(this),
							"originLeft":parseInt($(this).css("left").substring(0,$(this).css("left").length - 2)),
							"originTop":parseInt($(this).css("top").substring(0,$(this).css("top").length - 2))
						});
					}
				}
				dragManyNodes(option_focused_component_datas);
				if(enableRuler){
				// perfect position of moving object
				var drag_ele_current_pos_left = ui.offset.left;
				var drag_ele_current_pos_top = ui.offset.top;
				 // ruler
				
				dragShowVerticalRuler($(ui.helper).find(".node"), drag_ele_current_pos_left, drag_ele_current_pos_top,container);
				dragShowHorizontalRuler($(ui.helper).find(".node"), drag_ele_current_pos_left, drag_ele_current_pos_top,container);
				//dragShowVerticalRuler($(ui.helper).find(".node"), drag_ele_current_pos_left+paddingLeft, event.pageY,container);
				//dragShowHorizontalRuler($(ui.helper).find(".node"), event.pageX, drag_ele_current_pos_top+containerTop,container);
			    }
				removeRuler();
			    
			    /*orgchart*/
			    if (checkFormMinWidth("orgchart") != null) {
                                $(".workspace.orgchart_ws").resizable("option", "minWidth", checkFormMinWidth("orgchart"));
                }
                if (checkFormMinHeight("orgchart") != null) {
                                $(".workspace.orgchart_ws").resizable("option", "minHeight", checkFormMinHeight("orgchart"));
                }
                /*workflow*/
                if (checkFormMinWidth("workflow") != null) {
                              $(".workspace.workflow_ws").resizable("option", "minWidth", checkFormMinWidth("workflow"));

                }
                if (checkFormMinHeight("workflow") != null) {
                             	$(".workspace.workflow_ws").resizable("option", "minHeight", checkFormMinHeight("workflow"));

                }
					
					option_focused_component_datas = [];
				

                            /*added Carlo Medina 4/21/2015*/
                            console.log("draggedele",$(this))
                            var draggedele = $(this);
                            var draggedelePosLeft = draggedele.position().left;
                            var draggedelePosTop = draggedele.position().top;
                            var this_position_left_remainder = draggedelePosLeft % 50;
                            var this_position_top_remainder = draggedelePosTop % 50;

                            // draggedele.snapToGrid({
                            // 	"topSetBack":5,
                            // 	"leftSetBack":5
                            // });
                            moveLines($(this).attr('id'));
			},
			opacity: 0.5	
		})
	 //    $(element).draggable({
		// containment:container,
		// handle:".handle-node-drag",
		// drag:function(event,ui){
			
		//     // padding of parent div
		//     var paddingLeft = 6;
		//     var paddingTop = 18;
		//     // container offset on the screen
		//     var containerTop = $(container).offset().top+paddingTop;
		//     var containerLeft = $(container).offset().top+paddingLeft;
		//     if(enableRuler){
		// 	// perfect position of moving object
		// 	var drag_ele_current_pos_left = ui.offset.left;
		// 	var drag_ele_current_pos_top = ui.offset.top;
		// 	 // ruler
			
		// 	dragShowVerticalRuler($(ui.helper).find(".node"), drag_ele_current_pos_left, drag_ele_current_pos_top,container);
		// 	dragShowHorizontalRuler($(ui.helper).find(".node"), drag_ele_current_pos_left, drag_ele_current_pos_top,container);
		// 	//dragShowVerticalRuler($(ui.helper).find(".node"), drag_ele_current_pos_left+paddingLeft, event.pageY,container);
		// 	//dragShowHorizontalRuler($(ui.helper).find(".node"), event.pageX, drag_ele_current_pos_top+containerTop,container);
		//     }
		//     /* Hide object properties */
		//     $(".node-setting").popover('hide')
		//     // lines interaction
		//     var moveIDs = $(this).attr("data-node-id");
		//     moveLines(moveIDs,lineColor);
		// },
		// stop:function(event,ui){
		//     removeRuler();
		//     var object_id = $(this).attr("data-node-id");
		//     moveLines(object_id,lineColor);
		//     // perfect position of moving object
		//     var pos_left = ui.position.left;
		//     var pos_top = ui.position.top;
		//     var json = $("body").data();
		//     json['nodes'][''+object_id+'']['node_top'] = pos_top;
		//     json['nodes'][''+object_id+'']['node_left'] = pos_left;
		//     $("body").data(json)
		//     /*orgchart*/
		//     if (checkFormMinWidth("orgchart") != null) {
		// 		$(".workspace.orgchart_ws").resizable("option", "minWidth", checkFormMinWidth("orgchart"));
	 //        }
	 //        if (checkFormMinHeight("orgchart") != null) {
		// 		$(".workspace.orgchart_ws").resizable("option", "minHeight", checkFormMinHeight("orgchart"));
	 //        }
	 //        /*workflow*/
	 //        if (checkFormMinWidth("workflow") != null) {
		// 		$(".workspace.workflow_ws").resizable("option", "minWidth", checkFormMinWidth("workflow"));
	 //        }
	 //        if (checkFormMinHeight("workflow") != null) {
		// 		$(".workspace.workflow_ws").resizable("option", "minHeight", checkFormMinHeight("workflow"));
	 //        }
            
		// },
		// //opacity: 0.5
	 //    })
	},
	dragAlso : function(self){

		$('body').on("click",'.node-drag-wrapper',function(ev){
			if(ev.ctrlKey){
				
				if($(this).hasClass("dragAlso")){

					$(this).removeClass("dragAlso");
				}else{
					$(this).addClass("dragAlso");
				}
			}else{
				$(".outer-node").removeClass("dragAlso");
			}
		})
	},
	activateNode:function(){
		if($(".userLevel").val()!=2){
			return;
		}
	    $("body").on("click",".outer-node",function(){
		// $(this).css("border-color","#CCC");
		$(this).find(".node-action-content").removeClass("display");
		//zindex
		$(".outer-node").css("z-index","1"); //
		$(this).css("z-index","2");
		$(".outer-node").removeClass("activateNode");
		$(this).addClass("activateNode");
	    })
	},
	resizeNode : function(container,lineColor){
	    //process node
	    $(".node-body-process").resizable({
	    	handles: "s",
		containment:container,
		minHeight:"50",
		minWidth:"130",
		resize:function(event,ui){
		    var width = $(this).width();
		    //$(this).closest(".outer-node").find(".node-title").css("width",width);
		    //recompute position
		    var object_id = $(this).closest(".outer-node").attr("id");
		    moveLines(object_id,lineColor);
		    /* Hide object properties */
			// $(".node-setting").popover('hide')
		},
		stop: function(event,ui){
		    var width = $(this).width();
		    var height = $(this).height();
		    var object_id = $(this).closest(".outer-node").attr("id");
		    var json = $("body").data();
		    json['nodes'][''+object_id+'']['node_height'] = height;
		    json['nodes'][''+object_id+'']['node_width'] = width;
		    $("body").data(json)
		}
	    });
	},
	resizeStartNode : function(container,lineColor){
	    //process node
	    $(".node-body-start").resizable({
		containment:container,
		minHeight:"80",
		minWidth:"90",
		resize:function(event,ui){
		    //recompute position
		    var moveIDs = $(this).closest(".outer-node").attr("id");
		    moveLines(moveIDs,lineColor);
		    /* Hide object properties */
			// $(".node-setting").popover('hide')
		    
		},
		stop: function(event,ui){
		    var width = $(this).width();
		    var height = $(this).height();
		    var object_id = $(this).closest(".outer-node").attr("id");
		    var json = $("body").data();
		    json['nodes'][''+object_id+'']['node_height'] = height;
		    json['nodes'][''+object_id+'']['node_width'] = width;
		    $("body").data(json)
		}
	    });
	},
	mouseoverNode : function(){
	    $("body").on("mouseenter",".outer-node",function(){
	    	if(!$(this).hasClass("node-drag-wrapper")){
	    		var pathname = window.location.pathname;
		    	if($(".userLevel").val()!=2){
					$(".node-minimize").remove();
					$(".node-maximize").remove();
					$(".node-remove").remove();
				}
		    	if(!$(this).hasClass('ui-draggable') && pathname=="/workspace"){
		    		return;
		    	}
		    	$(this).css("border-color","#CCC");
				$(this).find(".node-action-content").removeClass("display");
	    	}
			
	    })
	    var self = this;
		$('body').on('mouseenter',".node-drag-wrapper", function(){
			self.mouseEnterFN(this);
		});
	},
	mouseEnterFN : function(self){
		var node_data_type = $(self).attr("data-node-type");
		var userLevel = $(".userLevel").val();
		var pathname = window.location.pathname;
		if(pathname=="/user_view/workspace"){
			return;
		}
		if($("body").find(".ele-dragging").length>0){
			return ;
		}
		if(userLevel==2){
			$(self).find(".node-drag-holder, .node-drag-holder-circle").css({
			'border':'#cccccc dotted 2px'
			});
			$(self).find('.node-drag-item-wrapper').css({
				'border':'#cccccc dotted 2px'
			});
			$(self).find('span.node-drag-text-wrapper').css({
			'display':'block'
			});
			$(self).find('.node-circle-settings-wrapper').stop(false,false).animate({
				bottom:-15,
				opacity:1
			});
				$(self).find('.node-dragtopoint-wrapper').stop(false,false).animate({
					top:0,
					opacity:1,
				});

			$(self).find('.node-circle-dragtopoint-wrapper').stop(false,false).animate({
				top:0,
				opacity:1
			});
			$(".outer-node").css("z-index","1");
			var node_id = $(this).attr("data-node-id");
			
			$(self).css({
				"z-index":"2",
				
		  });
		  var this_id = $(self).attr("id");
		  $(".lines[parent='"+ this_id +"']").not(".arrow").css({
				"box-shadow": "1px 1px 10px #33b5e5",
				"-webkit-transition-duration": "0.5s",
				"-moz-transition-duration": "0.5s",
				"-o-transition-duration": "0.5s",
				"transition-duration": "0.5s",
				"z-index":"2"
		  })
		  var pathname = window.location.pathname;
		  if (pathname!="/user_view/organizational_chart") {
				$(".lines[parent='"+ this_id +"']").find(".customLineText").addClass("customLineTextActive")	
		  }
			
		}
		$(self).find('.node-settings-wrapper').stop(false,false).animate({
			bottom:0,
			opacity:1
		});
	},
	mouseleaveNode : function(){
	    $("body").on("mouseleave",".outer-node",function(){
	    	if(!$(this).hasClass("node-drag-wrapper")){
				removeActiveNode(this)
			}
	    })
	    var self = this;
	    $('body').on('mouseleave','.node-drag-wrapper', function(){
			self.mouseLeaveFN(this,"hover");
		});
	},
	mouseLeaveFN : function(self,type){
		if($("body").find(".ele-dragging").length>0){
			return ;
		}
		$(self).find(".node-drag-holder, .node-drag-holder-circle").css({
			'border-color':'transparent', 'width':'100px'
		});
		$(self).find('.node-drag-item-wrapper').css({
			'border-color':'transparent'
		});
		$(self).find('span.node-drag-text-wrapper').css({
			'display':'none'
		});
		$(self).find('.node-settings-wrapper').stop(false,false).animate({
			bottom:-21,
			opacity:0
		});

		$(self).find('.node-circle-settings-wrapper').stop(false,false).animate({
			bottom:-21,
			opacity:0
		});
		if(type=="hover"){
			$(self).find('.node-dragtopoint-wrapper').stop(false,false).animate({
				top:-28,
				opacity:0
			});
			$(self).find('.node-circle-dragtopoint-wrapper').stop(false,false).animate({
				top:-28,
				opacity:0
			});
			$(self).css({
				"z-index":"1"
		  });
		  var this_id = $(self).attr("id");
		  $(".lines[parent='"+ this_id +"']").css({
				"box-shadow": "",
				"z-index":"",
				"-webkit-transition-duration": "",
				"-moz-transition-duration": "",
				"-o-transition-duration": "",
				"transition-duration": "",
		  })
		  $(".lines[parent='"+ this_id +"']").find(".customLineText").removeClass("customLineTextActive");
		}
	 
		
	},
	removeAllActiveNode:function(){
	    $(".outer-node").removeClass("activateNode");
	    $(".outer-node").removeClass("dragAlso");
	    removeActiveNode(".outer-node");
	},
	mousedown:function(){
	    $("body").on("mousedown",".outer-node",function(){
		$(".outer-node").css("z-index","1"); //
		$(this).css("z-index","2");
	    })
	},
	dropToNode: function(element,container,lineColor){
	    $(element).droppable({
		drop:function(event,ui){
			//create line
		    var drop_node_this = this;
		    var json_body = $("body").data();
		    var json_node = json_body['nodes'];
		    var json_lines = json_body['lines'];
			//for reassigning
			if($(ui.helper).hasClass("line-reassign")){

				//initialization
				//drop node
				var drop_node_id_child = $(this).closest(".outer-node").attr("id");
				var parentNode = $(ui.helper).attr("parent");
				var childNode = $(ui.helper).attr("child");
				var uiHelper = $("#"+parentNode);
				var json_title = json_lines[parentNode+"_"+childNode+"_parentLine"]['json_title'];
				var custom_line_color = json_lines[parentNode+"_"+childNode+"_parentLine"]['background-color'];
				var line_class = parentNode+"_"+childNode;
				

				//parent node json
			    var node_json_parent = json_node[''+ parentNode +''];
			    var node_json_type = node_json_parent['type_rel'];
				//return if the same node
				if(parentNode==drop_node_id_child || childNode==drop_node_id_child){
					return;
				}

				//return if there are already connected
				if($(".lines[line-class='"+ parentNode +"_"+ drop_node_id_child +"']").length>0){
					showNotification({
	                    message: "A return line already exists for the selected node. Please remove the existing line to continue your action.",
	                    type: "error",
	                    autoClose: true,
	                    duration: 3
		            });
					return;
				}
				if(node_json_type=="3"){
					var buttons_json = {};
                    buttons_json['child_id'] = drop_node_id_child;
                    buttons_json['parent_id'] = parentNode;
                    buttons_json['wokflow_condition_return_boolean'] = "sssssss";

                    json_node[''+ parentNode +'']['condition_return'][''+ json_title +''] = buttons_json;
                }else if(node_json_type=="5"){
                    json_node[''+ parentNode +'']['buttonStatus'] = drop_node_id_child;
                	//database node
				}else{
					json_node[''+ parentNode +'']['buttonStatus'][''+ json_title +'']['child_id'] = drop_node_id_child;
				}


				//delete existing line
				$(".lines[line-class='"+ line_class +"']").remove();

				delete json_lines[''+ line_class +'_arrow'];
			    delete json_lines[''+ line_class +'_childLine'];
			    delete json_lines[''+ line_class +'_parentLine'];
			    delete json_lines[''+ line_class +'_middleLine'];
			 	
			 	//create new line re assignment
				line.createLine(uiHelper,drop_node_this,container,custom_line_color,json_title);
                
                //to initialize save node event
				workflow_actions.save_node_settings(null);

				//to stop other events
				return;
			}
		    
		    //parent( for onDrop)
		    
		    //parent id
		    var drop_node_id_parent = $(ui.helper).closest(".outer-node").attr("id");

		    //parent node json
		    var node_json_parent = json_node[''+ drop_node_id_parent +''];
		    var node_json_type = node_json_parent['type_rel'];
		    //drop function
		    var drop_node_fn = node_json_parent['node_onDrop'];
		    
		    //child( for drop boolean)
		    //child id
		    var drop_node_id_child = $(this).closest(".outer-node").attr("id");
		    //child node json
		    var node_json_child = json_node[''+ drop_node_id_child +''];
		    //drop bool (if allowed)
		    var drop_bool = node_json_child['node_drop'];
		    if(node_json_type==0){
		    	if(json_lines){
		    		for(var i in json_lines){
		    			if(drop_node_id_parent==json_lines[i]['parent'] && drop_node_id_child==json_lines[i]['child']){
		    				showNotification({
			                    message: "Already connected",
			                    type: "error",
			                    autoClose: true,
			                    duration: 3
				            });
		    				return;
		    			}
		    		}
		    	}
		    }

		    if(node_json_type==3){
		    	if($(".lines[line-class='"+ drop_node_id_parent +"_"+ drop_node_id_child +"']").length>0){
					showNotification({
	                    message: "A return line already exists for the selected node. Please remove the existing line to continue your action.",
	                    type: "error",
	                    autoClose: true,
	                    duration: 3
		            });
					return;
				}
		    }

		    //for database limit 1
		    if(node_json_type==5){
		    	if($(".lines[parent='"+ drop_node_id_parent +"']").length>0){
					showNotification({
	                    message: "A return line already exists for the selected node. Please remove the existing line to continue your action.",
	                    type: "error",
	                    autoClose: true,
	                    duration: 3
		            });
					return;
				}
		    }

		    if(drop_node_id_parent!=drop_node_id_child){
		    	if (drop_bool==true || drop_bool=="true") {
		    		var uiHelper = $(ui.helper).closest(".outer-node");
					if(typeof drop_node_fn == "function"){
					    //function
					    //for has a function

					    drop_node_fn(ui.helper,drop_node_this,function(custom_line_color,json_title){
							line.createLine(uiHelper,drop_node_this,container,custom_line_color,json_title);
					    });
					}else if (typeof drop_node_fn == "boolean") {
						if(node_json_type==5){
		                    json_node[''+ drop_node_id_parent +'']['buttonStatus'] = drop_node_id_child;
						}

					    //for no function in drop
					    line.createLine(uiHelper,drop_node_this,container,lineColor,null);
					}else{
					    //for has a string function
					    //conver string to function
					    var node_linecolor = "424242";
					    var regExpJDialog = new RegExp('jDialog\\(object_properties_node\\(object_type,node_data_id,child_id\\), \\"\\",\\"500\", \\"\\", \\"\\", function\\(\\){}\\);');

					    var object_type = "";

					    if(node_json_parent['type_rel']=="1" || node_json_parent['type_rel']=="2"){
					    	//start
					    	object_type = 'workflow_chart_buttons';
					    }else if(node_json_parent['type_rel']=="3"){
					    	//condition
					    	object_type = 'workflow_chart_condition-line';
					    }
					    // replace new dialog
					    if(drop_node_fn.indexOf('newDialog')==-1){
					    	// alert(123)
					    	var newDialog = 'var newDialog = new jDialog(object_properties_node("'+ object_type +'",node_data_id,child_id), "","500", "", "", function(){});newDialog.themeDialog("modal2");';
					    	drop_node_fn = drop_node_fn.replace(regExpJDialog,newDialog);
					    }
					    

					    drop_node_fn = eval("("+drop_node_fn+")");
					    

					    drop_node_fn(ui.helper,drop_node_this,function(custom_line_color,json_title){

							line.createLine(uiHelper,drop_node_this,container,custom_line_color,json_title);
							//convert string to function
							node_json_parent['node_onDrop'] = drop_node_fn;
							//return back the string function to function
							$("body").data(json_body);
					    })
					    
					}
			    }else{
					//alert(drop_bool+"=asds");
			    }
		    }
		    $(this).closest(".outer-node").removeClass("node-hover");
		},
		over: function(event,ui){
			var json_body = $("body").data();
		    var json_node = json_body['nodes'];
			var thisID = $(ui.helper).closest(".outer-node").attr("id");
			var targetID = $(this).closest(".outer-node").attr("id");
			var targetType = json_node[targetID]['type_rel'];
			/*console.log*/(targetID+"!="+thisID +"&&"+ targetType +"=="+ 2)
			if(targetID!=thisID && targetType != 1){
				$(this).closest(".outer-node").addClass("node-hover");
			}
		},
		out : function(event,ui){
			$(this).closest(".outer-node").removeClass("node-hover");
		},
		accept: ".node-drag, .lines",
	    })
	}
    }
    
    
    //Node Settings
    nodeSettings = {
	settingNode : function(){
	},
	removeNode : function(){
	    $("body").on("click",".node-remove",function(){
			var conf = "Are you sure you want to remove this node?";
			var dis = this;
			var json_nodes = $("body").data();
	        var json_nodes_var = json_nodes['nodes'];
			var json_lines_var = json_nodes['lines'];
			var newConfirm = new jConfirm(conf, 'Confirmation Dialog','', '', '', function(r){
			    if(r==true){
					var nodeID = $(dis).closest(".outer-node").attr("id");
					$(dis).closest(".outer-node").remove();
					$(".lines[parent='"+ nodeID +"'], .lines[child='"+ nodeID +"'] ").each(function(){
					    var line_class = $(this).attr("parent")+"_"+$(this).attr("child");
					    delete json_lines_var[''+ line_class +'_arrow'];
					    delete json_lines_var[''+ line_class +'_childLine'];
					    delete json_lines_var[''+ line_class +'_parentLine'];
					    delete json_lines_var[''+ line_class +'_middleLine'];
					    $(".lines[line-class='"+line_class+"']").remove();
					    // moveLines($(this).attr("parent"),"");
					    // moveLines($(this).attr("child"),"");
					});
					$(".lines[parent='"+ nodeID +"'], .lines[child='"+ nodeID +"'] ").each(function(){
					    moveLines($(this).attr("parent"),"");
					    moveLines($(this).attr("child"),"");
					});
					var btn_stat = "";
					var condition_return = "";
					for(var i in json_nodes_var){
						if(json_nodes_var[i]['workflow-default-action']==nodeID){
							json_nodes_var[i]['workflow-default-action'] = "0";
						}

						btn_stat = if_undefinded(json_nodes_var[i]['buttonStatus'],"");
						condition_return = if_undefinded(json_nodes_var[i]['condition_return'],"");
						
						if(json_nodes_var[i]['type_rel']=="5"){
							//walang gagawin
						}else{
							if(btn_stat){
								$.each(btn_stat,function(key,value){
									if(btn_stat[key]['child_id']==nodeID){
										delete btn_stat[key];
									}
								})
							}
						}
						if(condition_return){
							if(condition_return['True']){
								if(condition_return['True']['child_id']==nodeID){
									delete condition_return['True'];
								}	
							}
							if(condition_return['False']){
								if(condition_return['False']['child_id']==nodeID){
									delete condition_return['False'];
								}
							}
						}
					}

					//$(".lines[child='"+ nodeID +"']").remove();
					//remove node in json

					delete json_nodes_var[''+ nodeID +''];
					if($(".outer-node").length==0){
						bind_onBeforeOnload(0);
					}else{
						bind_onBeforeOnload(1);
					}
					
			    }
			});
			newConfirm.themeConfirm("confirm2", {
				'icon':'<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>' 
			});
	    });
	},
	minimizeNode : function(lineColor){
	    $("body").on("click",".node-minimize",function(){
		$(this).closest(".outer-node").find(".node-body").slideUp(function(){
		    //recompute position
		    var moveIDs = $(this).closest(".outer-node").attr("id");
		    moveLines(moveIDs,lineColor);
		});
		$(this).closest(".outer-node").find(".node-maximize").css("display","inline-block");
		$(this).css("display","none");
		/* Hide object properties */
		    // $(".node-setting").popover('hide');
		var node_id = $(this).closest(".outer-node").attr("id");
		var json_body = $("body").data();
		var json_node = json_body['nodes'];
		var node_json = json_node[''+ node_id +''];
		node_json['node_state'] = false;
		$("body").data(json_body);
	    })
	},
	maximizeNode : function(lineColor){
	    $("body").on("click",".node-maximize",function(){
		$(this).closest(".outer-node").find(".node-body").slideDown(function(){
		    //recompute position
		    var moveIDs = $(this).closest(".outer-node").attr("id");
		    moveLines(moveIDs,lineColor);
		});
		$(this).closest(".outer-node").find(".node-minimize").css("display","inline-block");
		$(this).css("display","none")
		/* Hide object properties */
		    // $(".node-setting").popover('hide');
		    
		var node_id = $(this).closest(".outer-node").attr("id");
		var json_body = $("body").data();
		var json_node = json_body['nodes'];
		var node_json = json_node[''+ node_id +''];
		node_json['node_state'] = true;
		$("body").data(json_body);
	    })
	}
    }
}(jQuery));

/* ADDITIONAL FUNCTIONALITY */

//Design of Node
function nodeDesign(json){
    //node properties
    var node_left = json.node_left;
    var node_top = json.node_top;
    var node_data_id = json.node_data_id;
    var node_text = json.node_text;
    var status = if_undefinded(json.status,"");
    var node_type = json.node_type;
    var node_container = json.node_container;
    var node_color = json.node_color;
    var node_height = has_px(json.node_height);
    var node_width = has_px(json.node_width);
    var node_settings_property = json.node_settings_property;
    var maximize = json.node_state;
    var type_rel = json.type_rel;
    var mail_icon = "";
    var nodeObj = "";
    var json_nodes = $("body").data();

    json_nodes['nodes'] = (json_nodes['nodes'] == null)?{}:json_nodes['nodes'];
    json_nodes['nodes'][''+ node_data_id +'']= json;

    // additional for check all enable fields
    // for start and process node only in workflow
    if(typeof json_nodes['nodes'][''+ node_data_id +'']['fieldEnabled'] == "undefined" && (type_rel=="1" || type_rel=="2")){
    	var form_fields = $(".form_fields").val();
	    var enableFields = [];
	    form_fields = JSON.parse(form_fields);
    	for (i in form_fields) {
    		enableFields.push(form_fields[i])
    	}
    	json_nodes['nodes'][''+ node_data_id +'']['fieldEnabled'] = enableFields;
    }
    //
    var nodeIdReplace = node_data_id;
    var generateNodeId = nodeIdReplace.replace('node_', 'ID:');
    //console.log(generateNodeId);

    var widthEndSettings = "";
	    
    
    $("body").data(json_nodes);
    var maximize_body = "display:none;";
    var maximize_icon = "display:none;";
    
    var minimize_icon = "display:none;";
    
    if (maximize==true) {
		maximize_body = "display:block;";
		minimize_icon = "display:inline-block;";
    }else{
		maximize_icon = "display:inline-block;";
    }
    if(type_rel=="2" || type_rel=="4"){
    	//for processor
    	mail_icon = '<i class="icon-envelope fa fa-envelope node-email tip" title="Message Settings" style="margin-right:5px;cursor: pointer;"></i>'+
					 '<i class="icon-bolt fa fa-bolt node-field-trigger tip" title="Trigger Settings" style="margin-right:5px;cursor: pointer;"></i>';
    	// if(type_rel == "4"){
    		//for end
    		mail_icon += '</i><i class="icon-user fa fa-user node-user-setup tip" title="User Settings" style="margin-right:5px;cursor: pointer;"></i>';
    		widthEndSettings = "width: 92px;";
    	// }
    }

    switch(node_type){
    case "department":
   //  	nodeObj = $('<div class="outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;">'+
			// '<div class="node-action-container handle-node-drag" style="height:18px">'+
			//     '<div class="node-action-content display">'+
			// 	'<i class="icon-minus node-minimize" style="'+ minimize_icon +'"></i>'+
			// 	'<i class="icon-plus node-maximize" style="'+ maximize_icon +'"></i>'+
			// 	'<i class="icon-cogs node-setting" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'" data-toggle="popover" data-placement="right"></i>'+
			// 	mail_icon+
			// 	'<i class="icon-remove node-remove"></i>'+
			//     '</div>'+
			// '</div>'+
			// '<div class="node node-process" id="node_process_'+ node_data_id +'" style="background-color:'+ node_color +'">'+
			//     '<div class="node-title" id="node_title_'+ node_data_id +'" style="">'+ node_text +'</div>'+
			//     '<div class="node-body node-body-process" style="'+ maximize_body +'height:'+ node_height +';width:'+ node_width +';">'+
			//     '<div class="node-other-text" style="height:50%">'+
			// 	'<div class="node-other-text" id="node_status_'+ node_data_id +'" style="height:100%;text-align:center;font-size:10px;font-family:Tahoma;"> '+
			// 	status +

			// 	'</div>'+
			// 	'<div class="node-drag node-process-drag">'+
				    
			// 	'</div>'+
			//     '</div>'+
			//     '<div style="clear:both"></div>'+
			// '</div>'+
			// '<div class="line_container"></div>'+
		 //    '</div>');
		var head_name = if_undefinded(json.head_name,"");
		if(status==""){
			status = '<img src="/js/functions/create_flowchart/adminnodes/avatardefault.jpg" width="36" height="36">';
		}
		nodeObj = $('<div class="node-drag-wrapper outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;position:absoulte">'+
					'<div class="node-drag-item-wrapper" id="tip_' + node_data_id + '" data-original-title="Hold drag to move" style="text-align: center;height: 20px;padding-top: 0px;left:-3px;width:20px">'+
						'<span class="node-drag-text-wrapper"></span>'+
					'</div>'+
						'<div class="node" style="">'+
							'<div class="node-drag-holder" style="">'+

								'<div class="node-firstoption-wrapper">'+
									'<div class="node-holder-avatar-wrapper node-bg-color" style="background-color:'+ node_color +';">'+
										'<div class="node-avatar-wrapper">'+ status +'</div>'+
									'</div>	'+		
									'<div class="node-settings-wrapper" style="text-align: center;">'+
									'<i class="icon-cogs node-setting fa fa-gear tip" title="Primary Settings" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'"  style="margin-right:5px;cursor: pointer;"></i>'+
									mail_icon+
									'<i class="icon-remove node-remove fa fa-times tip" title="Delete" style="cursor: pointer;"></i>'+
									'</div>'+
								'</div>	'+	
								'<div class="node-wrapper node-bg-color" style="background-color:'+ node_color +';">'+
									'<div class="node-content-wrapper">'+
										'<div class="node-dept-wrapper node-title node-bg-color" style="background-color:'+ node_color +';" id="node_title_'+ node_data_id +'" style="">'+
											node_text +
										'</div>'+
										'<div class="node-name-wrapper node-other-text" id="node_status_'+ node_data_id +'">'+
											head_name +
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="clearfix" style=""></div>'+
								'<div class="node-firstoption-wrapper">'+
									'<div class="node-dragtopoint-wrapper node-drag" style="cursor:move"><span class="dragtext"> Drag this to connect</span></div>'+
								'</div>'+
							'</div>'+
						'</div>'+
				'</div>')
	    break;
	case "box":
	    nodeObj = $('<div class="outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;">'+
			'<div class="node-action-container handle-node-drag" style="height:18px">'+
			    '<div class="node-action-content display">'+
				'<i class="icon-minus node-minimize" style="'+ minimize_icon +'"></i>'+
				'<i class="icon-plus node-maximize" style="'+ maximize_icon +'"></i>'+
				'<i class="icon-cogs fa fa-gear node-setting" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'" data-toggle="popover" data-placement=""></i>'+
				mail_icon+
				'<i class="icon-remove fa fa-times node-remove"></i>'+
			    '</div>'+
			'</div>'+
			'<div class="node node-process" id="node_process_'+ node_data_id +'" style="background-color:'+ node_color +'">'+
			    '<div class="node-title" id="node_title_'+ node_data_id +'" style="">'+ node_text +'</div>'+
			    '<div class="node-body node-body-process" style="'+ maximize_body +'height:'+ node_height +';width:'+ node_width +';">'+
			    '<div class="node-other-text" style="height:50%">'+
				'<div class="node-other-text" id="node_status_'+ node_data_id +'" style="height:100%;text-align:center;font-size:10px;font-family:Tahoma;"> '+
				status +

				'</div>'+
				'<div class="node-drag node-process-drag">'+
				    
				'</div>'+
			    '</div>'+
			    '<div style="clear:both"></div>'+
			'</div>'+
			'<div class="line_container"></div>'+
		    '</div>');
	    break;
	case "circle":
	    //START NODE
	    var end = "";
	    if(status==""){
	    	status = "Status here";
	    }
	    if (node_text=="End") {
	    	end +='<div class="node-other-text" id="node_status_'+ node_data_id +'" style="margin-top:-20px;text-align:center;font-size:10px;font-family:Tahoma;"> '+
				    status+		    
				'</div>';
	    }
	    nodeObj = $('<div class="outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;">'+
			'<div class="node-action-container handle-node-drag" style="height:18px">'+
			    '<div class="node-action-content display">'+
				'<i class="icon-cogs fa fa-gear node-setting" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'"></i>'+
				mail_icon +
				'<i class="icon-remove node-remove"></i>'+
			    '</div>'+
			'</div>'+
			'<div class="node node-start" id="node_process_'+ node_data_id +'" style="background-color:'+ node_color +'">'+
			    '<div class="node-body node-body-start" style="height:'+ node_height +';width:'+ node_width +';">'+
				'<div class="node-start-title" style="">'+
				    '<span class="node-start-title-label" id="node_title_'+ node_data_id +'" style="">'+ node_text +'</span>'+
				'</div>'+
				end +
				'<div class="node-drag node-start-drag" style="">'+
				'</div>'+
			    '</div>'+
			'</div>'+
			'<div class="line_container"></div>'+
		    '</div>');
		// nodeObj = $('<div class="node-drag-wrapper add-padding-node-workflow outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;">'+
		// 				'<div class="node-drag-item-wrapper">'+
		// 					'<span class="node-drag-text-wrapper"> Drag here to move item</span>'+
		// 				'</div>'+
		// 				'<div class="node-drag-holder">'+
		// 					'<div class="node-firstoption-wrapper">	'+	
		// 						'<div class="node-circle-settings-wrapper border-circle-node" style="text-align:center">'+
		// 							'<i class="icon-cogs node-setting" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'" style="margin-right:5px;cursor: pointer;"></i>'+
		// 							mail_icon +
		// 							'<i class="icon-remove node-remove"></i>'+
		// 						'</div>'+
		// 					'</div>	'+	
		// 					'<div class="node-circle-wrapper">'+
		// 						'<div class="node-circle-title">'+ node_text +'</div>'+

		// 						'<div class="node-workflow-icon-wrapper node-circle"><img src="/js/functions/create_flowchart/adminnodes/node-circle-start.PNG"></div>'+
		// 					'</div>'+
		// 					'<div class="node-firstoption-wrapper">'+
		// 						'<div class="node-circle-dragtopoint-wrapper border-circle-node-top node-drag" style="cursor:move"><span class="dragtext"> Drag this to connect</span></div>'+
		// 					'</div>'+
		// 				'</div>'+
		// 			'</div>');
	    break;
	    case "circle2":
	    if(status==""){
	    	status = "<span style='color:transparent;border: 1px solid transparent;'></span>";
	    }
	    var connect_display = "";
	    if(type_rel=="4"){
	    	connect_display = "display";
	    }
	    nodeObj = $('<div class="node-drag-wrapper add-padding-node-workflow outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;width: auto;">'+
						'<div class="node-drag-item-wrapper" id="tip_' + node_data_id + '" style="left: -3px;" data-original-title="Hold drag to move">'+
							'<span class="node-drag-text-wrapper"></span>'+
						'</div>'+
						'<div class="node">' +
						'<div class="node-drag-holder-circle"  style="padding: 5px 5px 5px 15px;">'+
							'<div class="node-firstoption-wrapper">	'+	
								'<div class="node-circle-settings-wrapper border-circle-node" style="text-align:center;'+widthEndSettings+'">'+
									'<i class="icon-cogs fa fa-gear node-setting tip" title="Primary Settings" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'" style="margin-right:5px;cursor: pointer;"></i>'+
									mail_icon +
									'<i class="icon-remove fa fa-times node-remove tip" title="Delete"></i>'+
								'</div>'+
							'</div>	'+	
							//'<div  style="padding:5px">'+
								// '<div class="node" style="float: right;width: 125px;height: 115px;position:absolute;">'+
								'<div class="" style="">'+
									'<div class="node-circle-wrapper">'+
										'<div class="node-circle-status"  style="">' + status  + '</div>'+ //text-align: center; position: relative; top: 81px;text-align: center;font-weight: bold;font-family: arial;font-size: 13px;color: #222222;
										'<div class="node-circle-title">'+ node_text +'</div>'+
										((node_text == "Start")?'<div class="node-workflow-icon-wrapper-circle node-circle"><svg class="icon-svg icon-svg-workspace icon-svg-node" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-startnode"></use></svg></div>':'<div class="node-workflow-icon-wrapper-circle node-circle"><svg class="icon-svg icon-svg-workspace icon-svg-node" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-endnode"></use></svg></div>')+
										
									'</div>'+
									
									'<div class="clearfix" style=""></div>'+
								'</div>'+
								'<div class="node-firstoption-wrapper '+ connect_display +'">'+
										'<div class="node-circle-dragtopoint-wrapper border-circle-node-top node-drag" style="float:left"><span class="dragtext" title="Drag this to connect"> Drag this to connect</span></div>'+
									'</div>'+
							'</div>'+
						'</div>'+
					'</div>');
		break;
		case "processor":
		nodeObj = $('<div class="node-drag-wrapper add-padding-node-workflow outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;padding-bottom:0px;width:auto">'+
						'<div class="node-drag-item-wrapper" id="tip_' + node_data_id + '" style="left: -3px;" data-original-title="Hold drag to move">'+
							'<span class="node-drag-text-wrapper"></span>'+
						'</div>'+
						'<div class="node" style="">'+
						'<div class="node-drag-holder " style="">'+
						
							'<div class="node-firstoption-wrapper" style="text-align:center">	'+
								'<span style="position: absolute; left: 31px; top: -16px; background-color: rgb(52, 52, 52); color: #fff; padding: 2px; border-top-left-radius: 3px; border-top-right-radius: 4px;">' + generateNodeId + '</span>' + 	
								'<div class="node-settings-wrapper" style="'+ widthEndSettings +'">'+
								'<i class="icon-cogs fa fa-gear node-setting tip" title="Primary Settings" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'" style="margin-right:5px;cursor: pointer;"></i>'+
								mail_icon+
								'<i class="icon-remove fa fa-times node-remove tip" title="Delete"></i>'+
								'</div>'+
							'</div>	'+	
							// '<div class="node" style="float: right;width:180px;position:absolute;">'+
							'<div class="" style="">'+
								
								'<div class="node-wrapper">'+
									'<div class="node-content-wrapper">'+
										'<div class="node-content-wrapper">'+
											'<div class="node-dept-wrapper">'+
												node_text + 
											'</div>'+
											'<div class="node-name-wrapper">'+
												status+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="node-workflow-icon-wrapper"><svg class="icon-svg icon-svg-workspace icon-svg-node-processor" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-processnode"></use></svg></div>'+
								'<div class="clearfix" style=""></div>'+
							'</div>'+
							'<div class="node-firstoption-wrapper">'+
								'<div class="node-dragtopoint-wrapper node-drag"><span class="dragtext"> Drag this to connect</span></div>'+
							'</div>'+
						'</div>'+
						'</div>'+
					'</div>');
		break;
		case "condition":
		nodeObj = $('<div class="node-drag-wrapper add-padding-node-workflow outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;;padding-bottom:0px;width:auto">'+
						'<div class="node-drag-item-wrapper" id="tip_' + node_data_id + '" style="left: -3px;" data-original-title="Hold drag to move">'+
							'<span class="node-drag-text-wrapper"></span>'+
						'</div>'+
						'<div class="node" style="">'+
						'<div class="node-drag-holder " style="width: 100px;">'+
							'<div class="node-firstoption-wrapper" style="text-align:center">	'+	
								'<div class="node-settings-wrapper">'+
								'<i class="icon-cogs fa fa-gear node-setting tip" title="Primary Settings" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'" style="margin-right:5px;cursor: pointer;"></i>'+
								mail_icon+
								'<i class="icon-remove fa fa-times node-remove tip" title="Delete"></i>'+
								'</div>'+
							'</div>	'+	
							// '<div class="node" style="float: right;width:180px;position:absolute;">'+
							'<div class="" style="">'+
								'<div class="node-wrapper">'+
									'<div class="node-content-wrapper">'+
										'<div class="node-content-wrapper">'+
											'<div class="node-dept-wrapper">'+
												node_text +
											'</div>'+
											'<div class="node-name-wrapper node-status-container">'+
												status+
											'</div>'+
										'</div>'+
									'</div>'+	
								'</div>'+
								'<div class="node-workflow-icon-wrapper"><svg class="icon-svg icon-svg-workspace icon-svg-node-processor" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-processnode"></use></svg></div>'+
								'<div class="clearfix" style=""></div>'+
							'</div>'+
							'<div class="node-firstoption-wrapper">'+
								'<div class="node-dragtopoint-wrapper node-drag"><span class="dragtext"> Drag this to connect</span></div>'+
							'</div>'+
							'</div>'+
						'</div>'+
					'</div>');
		break;
		case "condition_diamond":
		nodeObj = $('<div class="node-drag-wrapper add-padding-node-workflow outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;;padding-bottom:0px;width:auto">'+
						'<div class="node-drag-item-wrapper" id="tip_' + node_data_id + '" style="left: -3px;" data-original-title="Hold drag to move">'+
							'<span class="node-drag-text-wrapper"></span>'+
						'</div>'+
						'<div class="node" style="height:125px">'+
						'<div class="node-drag-holder" style="width: 100px;min-width: 133px;">'+
							'<div class="node-firstoption-wrapper" style="text-align:center">	'+	
								'<div class="node-settings-wrapper">'+
								'<i class="icon-cogs fa fa-gear node-setting tip" title="Primary Settings" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'" style="margin-right:5px;cursor: pointer;"></i>'+
								mail_icon+
								'<i class="icon-remove fa fa-times node-remove tip" title="Delete"></i>'+
								'</div>'+
							'</div>	'+	
							// '<div class="node" style="float: right;width:180px;position:absolute;">'+
							'<div class="fl-condition-wrapper" style="width:73px">'+
								'<div class="node-workflow-icon-wrapper"><svg class="icon-svg icon-svg-workspace icon-svg-node-processor" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-conditionalnode"></use></svg></div>'+
								'<div class="fl-condition-content">'+
									'<div class="diamond-L"></div>'+
									'<div class="diamond-R"></div>'+
								    '<div class="fl-condition-inner">'+
								    	'<span style="width:30px; position: absolute; left: 30%;  top: -3px; background-color: rgb(52, 52, 52); color: #fff; padding: 2px; border-top-left-radius: 3px; border-top-right-radius: 4px;">' + generateNodeId + '</span>' + 	
								        '<div class="fl-value-cond">'+ if_undefinded(json['field'],"Field") +'</div> <div class="fl-operators-cond">'+ if_undefinded(json['operator'],"Operator") +'</div><div class="fl-fieldsCondition-cond">'+ if_undefinded(json['field_value'],"Value") +'</div>'+//<div class="fl-label-cond">Condition</div>
								    '</div>'+
								'</div>'+
							'</div>'+
							'<div class="node-firstoption-wrapper">'+
								'<div class="node-dragtopoint-wrapper node-drag" style="width:118px"><span class="dragtext"> Drag this to connect</span></div>'+
							'</div>'+
							'</div>'+
						'</div>'+
					'</div>');
		break;
		case "database" :
			nodeObj = $('<div class="node-drag-wrapper add-padding-node-workflow outer-node" id="'+ node_data_id +'" data-node-id="'+ node_data_id +'" data-node-type="'+ node_type +'" style="left:'+node_left+'px;top:'+node_top+'px;padding-bottom:0px;width:auto">'+
						'<div class="node-drag-item-wrapper" id="tip_' + node_data_id + '" style="left: -3px;" data-original-title="Hold drag to move">'+
							'<span class="node-drag-text-wrapper"></span>'+
						'</div>'+
						'<div class="node" style="">'+
						'<div class="node-drag-holder node-database-node-drag-holder" style="width: 100px;">'+
						
							'<div class="node-firstoption-wrapper" style="text-align:center">	'+
								'<span class="isDisplayNone" style="position: absolute; left: 31px; top: -16px; background-color: rgb(52, 52, 52); color: #fff; padding: 2px; border-top-left-radius: 3px; border-top-right-radius: 4px;">' + generateNodeId + '</span>' + 	
								'<div class="node-settings-wrapper" style="">'+
								'<i class="icon-cogs fa fa-gear node-setting tip" title="Primary Settings" id="node_properties_'+ node_data_id +'" data-object-type="'+ node_settings_property +'" style="margin-right:5px;cursor: pointer;"></i>'+
								// mail_icon+
								'<i class="icon-remove fa fa-times node-remove tip" title="Delete"></i>'+
								'</div>'+
							'</div>	'+	
							// '<div class="node" style="float: right;width:180px;position:absolute;">'+
							'<div class="" style="">'+
								
								'<div class="node-wrapper node-database-wrapper">'+
									'<div class="node-content-wrapper node-database-content-wrapper">'+
										'<div class="node-content-wrapper node-database-content-wrapper">'+
											'<svg class="icon-svg svg-icon-wf-database" viewBox="0 0 80 90">' +
					                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-wf-database"></use>'+
					                        '</svg>'+
											'<div class="node-dept-wrapper isDisplayNone">'+
												node_text + 
											'</div>'+
											'<div class="node-name-wrapper isDisplayNone">'+
												status+
											'</div>'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="node-workflow-icon-wrapper isDisplayNone"><img src="/js/functions/create_flowchart/adminnodes/workflow-processor-icon.jpg"></div>'+
								'<div class="clearfix" style=""></div>'+
							'</div>'+
							'<div class="node-firstoption-wrapper">'+
								'<div class="node-dragtopoint-wrapper node-database-node-dragtopoint-wrapper node-drag "><span class="dragtext"> &nbsp;&nbsp;Connect</span></div>'+
							'</div>'+
						'</div>'+
						'</div>'+
					'</div>');
		break;
    }
    $(node_container).append(nodeObj);
	 $(".outer-node").disableSelection();
	 $(".tip").tooltip();
    if(type_rel=="0"){
    	$(".node-other-text").find("img").css({
    		"z-index": "2",
			"position": "relative"
    	})
    	$(".node-other-text").find("img").attr({
    		"width":"30",
    		"height":"30"
    	})
    }
    
    /*
     * Node Object Properties
     *
     */
    
	// $('body').on('click',"#node_properties_" + node_data_id, function (e) {
	// 	ui.block();
	//     var json_nodes = $("body").data();
 //            var json_nodes_var = json_nodes['nodes'];
	//     // Remove other open popover
	// 	//$('.object_properties').not(this).popover('hide');
	// 	var object_type = $(this).attr("data-object-type");
	// 	var width = "500";
	// 	if(object_type=="workflow_chart-processor" || object_type=="workflow_chart-start"){
	// 		width = "650";
	// 	}else if(object_type=="organizational_chart"){
	// 		width = "650";
	// 	}
	// 	jDialog(object_properties_node(object_type,node_data_id,""), "",width, "", "", function(){});

	// 	//field enabled
	// 	CheckProperties.setCheckAllChecked(".fieldEnabled",".f_enabled",".checkAllFields");
	// 	CheckProperties.bindOnClickCheckBox(".fieldEnabled",".f_enabled",".checkAllFields");
	// 	//field required
	// 	CheckProperties.setCheckAllChecked(".fieldRequired",".f_required",".checkAllFields");
	// 	CheckProperties.bindOnClickCheckBox(".fieldRequired",".f_required",".checkAllFields");
	// 	//field hidden
	// 	CheckProperties.setCheckAllChecked(".fieldHiddenValue",".f_hidden",".checkAllFields");
	// 	CheckProperties.bindOnClickCheckBox(".fieldHiddenValue",".f_hidden",".checkAllFields");


	// 	limitText(".limit-text-wf",25); // set limit of text in the fields  
	//     if (object_type=="organizational_chart") {
	//     	getUser(node_data_id,"",0,function(){
	//     		$('#content-dialog-scroll').perfectScrollbar();
	//     		limitText(".limit-text-wf",19); // set limit of text in the name  
	//     		$(".tip").tooltip(); 
	//     		ui.unblock();
	//     	});
	//     }else if (object_type=="workflow_chart-processor" || object_type=="workflow_chart-start") {
	// 		//getFlds();
	//     		$('#content-dialog-scroll').perfectScrollbar();
	//     }
	//     ui.unblock();
	    
	//     $(".tip").tooltip(); 
	// 	//for processor
	// 	var workflow_processor_type = if_undefinded(json_nodes_var[''+ node_data_id +'']['processorType'],"");
	// 	var workflow_processor = if_undefinded(json_nodes_var[''+ node_data_id +'']['processor'],"");
	// 	getprocessor(workflow_processor_type,workflow_processor);


	// 	//hide scroll x
		
	//     $(".ps-scrollbar-x").hide();
	    
	//     var node_color = if_undefinded(json_nodes_var[''+ node_data_id +'']['node_color'],"#424242");
	//     $(".chngColor[data-properties-type='nodeColor']").spectrum({
	// 		color: node_color
	//     });
	// 	//enable save json properties
	// 	workflow_actions.save_node_settings(null);

	//     });
    line.startLine($(nodeObj));

    return $(nodeObj);
}

//Ruler
//function dragShowHorizontalRuler(passedElement, mouseX, mouseY,container){
//	container_of_passed_ele = $(passedElement).closest(container);
//	if($(".ws-ruler-horizontal-row-guide").length >= 1 ){
//		
//		$(".ws-ruler-horizontal-row-guide").css({
//			top:/*passedElement.offset().top+1*/mouseY - container_of_passed_ele.offset().top
//		});
//	}else{
//
//		tempAppendShowRulerHorizontalRowGuide = $('<span class="draggable ui-draggable ws-ruler-horizontal-row-guide"></span>');
//
//		container_of_passed_ele.append(tempAppendShowRulerHorizontalRowGuide);
//
//		tempAppendShowRulerHorizontalRowGuide.css({
//			top:/*passedElement.offset().top+1*/mouseY - container_of_passed_ele.offset().top,
//			left:0
//		});
//	}
//	ruler_h_top = $(".ws-ruler-horizontal-row-guide").offset().top - container_of_passed_ele.offset().top;
//	if(ruler_h_top != (passedElement.offset().top - container_of_passed_ele.offset().top) ){
//		$(".ws-ruler-horizontal-row-guide").css({
//			top:/*passedElement.offset().top*/mouseY - container_of_passed_ele.offset().top
//		});
//	}
//}
//
//function dragShowVerticalRuler(passedElement, mouseX, mouseY,container){
//	container_of_passed_ele = $(passedElement).closest(container);
//	if($(".ws-ruler-vertical-column-guide").length >= 1 ){
//		
//		$(".ws-ruler-vertical-column-guide").css({
//			left:/*passedElement.offset().left*/mouseX - container_of_passed_ele.offset().left
//		});
//	}else{
//
//		tempAppendShowRulerVerticalColumnGuide = $('<span class="draggable ui-draggable ws-ruler-vertical-column-guide"></span>');
//
//		container_of_passed_ele.append(tempAppendShowRulerVerticalColumnGuide);
//
//		tempAppendShowRulerVerticalColumnGuide.css({
//			left:mouseX - container_of_passed_ele.offset().left,
//			top:0
//		});
//	}
//	ruler_v_left = $(".ws-ruler-vertical-column-guide").offset().left - container_of_passed_ele.offset().left;
//	if(ruler_v_left != (passedElement.offset().left - container_of_passed_ele.offset().left) ){
//		$(".ws-ruler-vertical-column-guide").css({
//			left:/*passedElement.offset().left*/mouseX - container_of_passed_ele.offset().left
//		});
//	}
//}
function removeRuler(){
    $(".ws-ruler-horizontal-row-guide").remove();
    $(".ws-ruler-vertical-column-guide").remove();
    
}
//Remove Active Node
function removeActiveNode(obj){
    if(!$(obj).hasClass("activateNode")){
	// $(obj).css("border-color","transparent");
	$(obj).find(".node-action-content").addClass("display");
    }
}
function has_px(value) {
    value = String(value);
    var ret = "";
    var suffix = value.substring(value.length-2,value.length);
    if (suffix!="px") {
	value = value+"px";
    }
    return(value);
}



function customTextLine(line_class,text){

	$("[line-class="+ line_class +"]").find(".customLineText").remove();
	var display = "";
	if($(".parentLine[line-class="+ line_class +"]").css("top")=="0px"){
		display="display";
	}
	$(".parentLine[line-class="+ line_class +"]").html('<span class="customLineText '+ display +'" line-class='+ line_class +' style="color:rgb(145, 145, 140);margin-left: 20px;position: absolute;bottom: 0;font-weight: bold;font-size: 10px;margin-bottom:5px">'+ text +'</span>');
	// updateLineLabel(line_class,text)
}
function updateLineLabel(line_class,text){
	//remove before lines
	$("[line-class="+ line_class +"]").find(".customLineText").remove();
	if($(".parentLine[line-class="+ line_class +"]").css("top")=="0px" && $(".parentLine[line-class="+ line_class +"]").css("left")=="0px"){
		return;
	}
	var json = $("body").data();
	var arrow = json['lines'][line_class+"_arrow"];
	var type = setLineAttribute(arrow['parent'],arrow['child'],"");
	type = type['parent_child_type'];
	if(type=="child top right" || type=="child top left"){
		$(".childLine[line-class="+ line_class +"]").html('<span class="customLineText" line-class='+ line_class +' style="color:rgb(145, 145, 140);margin-left: 20px;position: absolute;bottom: 0;font-weight: bold;font-size: 10px;margin-bottom:5px">'+ text +'</span>');
	}else if(type=="child top top" || type=="child bottom bottom"){
		$(".middleLine[line-class="+ line_class +"]").html('<span class="customLineText" line-class='+ line_class +' style="color:rgb(145, 145, 140);margin-left: 20px;position: absolute;bottom: 0;font-weight: bold;font-size: 10px;margin-bottom:5px">'+ text +'</span>');
	}else{
		$(".parentLine[line-class="+ line_class +"]").html('<span class="customLineText" line-class='+ line_class +' style="color:rgb(145, 145, 140);margin-left: 20px;position: absolute;bottom: 0;font-weight: bold;font-size: 10px;margin-bottom:5px">'+ text +'</span>');
	}

	//for middle line
	if($(".middleLine[line-class="+ line_class +"]").css("top")=="0px" && $(".middleLine[line-class="+ line_class +"]").css("left")=="0px"){
		$(".middleLine[line-class="+ line_class +"]").css("display","none");
	}else{
		$(".middleLine[line-class="+ line_class +"]").css("display","");
	}
}
function dragManyNodes(option_focused_component_datas){
	var pos_left = "";
    var pos_top = "";
    var json = $("body").data();
	for(var i in option_focused_component_datas){
		pos_left = option_focused_component_datas[i]['originLeft'] + drag_ele_origin_distance_to_current_left;
	    pos_top = option_focused_component_datas[i]['originTop'] + drag_ele_origin_distance_to_current_top;
		option_focused_component_datas[i]['object'].css({
			left: pos_left,
			top: pos_top
		})
		//added by Carlo Medina for snapping
		$(option_focused_component_datas[i]['object']).snapToGrid({
			topSetBack:5,
			leftSetBack:5
		});
		pos_left = $(option_focused_component_datas[i]['object']).position().left;
		pos_top = $(option_focused_component_datas[i]['object']).position().top;

		//end snapping
		var moveIDs = option_focused_component_datas[i]["data-node-id"];
		var parentNode = option_focused_component_datas[i]["parent-node"];
		var childNode = option_focused_component_datas[i]["child-node"];
		checkCollisionDroppableBoundaries(option_focused_component_datas[i]['object']);
		moveLines(moveIDs,"");
		moveLines(childNode,"");
		moveLines(parentNode,"");
	    json['nodes'][''+moveIDs+'']['node_top'] = pos_top;
	    json['nodes'][''+moveIDs+'']['node_left'] = pos_left;
	    $("body").data(json)
	}
	
	// console.log(json)
}


