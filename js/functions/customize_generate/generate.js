    

    customize_reports = {
	save_form_workspace : function(elements){
	    
	    $("body").on("click",elements,function(){
		var form_type = $(this).attr("data-form-type");
                    // Settings
                    var workspace_title = $(".workspace_title").val();
                    var workspace_displayName = $(".workspace_description").val();
                    var workspace_width = $(".form_size_width").val();
                    var workspace_height = $(".form_size_height").val();
		   
		    ui.block();
                    switch (form_type) {
        
                        case "generate":
        
                            if ($(".form-builder-ruler").length >= 1) {
                                $(".form-builder-ruler").remove()
                            }
        
                            // Get Object Length
        
                            var object_length = $(".setObject").length;
                            // Get Button Length
                            var btn_length = $(".setObject_btn").length;
        
        
                            // Condition if workspace is null
                            if (object_length < 1) {
                                // Notification
                                jAlert("Workspace is empty.", "", "", "", "", function() {
                                });
				ui.unblock();
                            } else if (workspace_title == "") {
                                showNotification({
                                    message: "Title is empty.",
                                    type: "error",
                                    autoClose: true,
                                    duration: 3
                                });
				ui.unblock();
                            } else {
                                $(".saveFormLoad").show();
                                var form_Fields_Arr = new Array();
                                // Get Fields Name
                                //    var form_fields = $(".getFields").map(function(){
                                //	return $(this).attr("name"));
                                //    }).get().join();
                                //
                                //
                                $(".getFields").each(function() {
                                    //return $(this).attr("name"));
                                    form_Fields_Arr.push({
                                        fieldName: $(this).attr('name'),
                                        fieldType: $(this).attr('data-type')
                                    });
                                })
                                //console.log(form_fields)
        
                                // Get Button
                                var btnName = $(".setObject_btn").map(function() {
                                    return $(this).children("[data-type='button']").val();
                                }).get().join();
                                // var btnName = $(".setObject_btn").map(function(){
                                //     return $(this).children().attr("name");
                                // }).get().join();
        
                                // Get Workspace Content
                                var workspace_content = $(".workspace").html();
				var pathname = window.location;
				    if (getParametersName("view_type",pathname)=="update"){
					var formId = $('#FormIdPrint').val();
                                        
				    }else{
					var formId = $('#FormId').val();
				    }
                                var btnContent = $(".btn_content").html();
    
                                //console.log(btnName)
                                // Set JSON
                                var workspace = {};
                                workspace["Title"] = workspace_title;
                                workspace["DisplayName"] = workspace_displayName;
                                workspace["Prefix"] = "";
                                workspace["Type"] = "";
                                workspace["WorkspaceHeight"] = workspace_height;
                                workspace["WorkspaceWidth"] = workspace_width;
                                workspace['BtnLength'] = btn_length;
                                workspace['BtnName'] = btnName;
                                workspace['button_content'] = btnContent;
                                workspace['ObjectLength'] = object_length;
                                workspace['WorkspaceContent'] = workspace_content;
                                workspace['form_json'] = $("body").data();
                                workspace['form_fields'] = JSON.stringify(form_Fields_Arr);
                                workspace['categoryName'] = "";
                                workspace['formrequestID'] = $("#requestID").val();
                                workspace['formtrackNo'] = $("#trackNo").val();
				workspace['formID'] = formId;
                                workspace['generate_object'] = $(".appendObjectsHere").html();
                                workspace['tab_object'] =  $(".tab_objects").html();
				
                                var json_a = $("body").data();
                                
                                // workspace["WorkspaceHeight"] = workspace_height;
                                // workspace["WorkspaceWidth"] = workspace_width;
                                if(workspace["WorkspaceHeight"] == "" || workspace["WorkspaceHeight"] == null || typeof workspace["WorkspaceHeight"] === "undefined"){
                                    workspace["WorkspaceHeight"] = $('.workspace').height();
                                }
                                if(workspace["WorkspaceWidth"] == "" || workspace["WorkspaceWidth"] == null || typeof workspace["WorkspaceWidth"] === "undefined"){
                                    workspace["WorkspaceHeight"] = $('.workspace').width();
                                }
                                if (typeof json_a.saving_request_format != "undefined") {
                                    workspace['MobileJsonData'] = JSON.stringify(json_a.saving_request_format);
                                } else {
                                    workspace['MobileJsonData'] = "";
                                }
        
                                if (typeof json_a.saved_mobile_content != "undefined") {
                                    workspace['MobileContent'] = json_a.saved_mobile_content;
                                } else {
                                    workspace['MobileContent'] = "";
                                }
                                /*collecting existing fields on the form*/
                                workspace['existing_fields'] = [];
                                $(".setObject").each(function(eqi_setObj) {
                                    if (
                                            $(this).attr("data-type") == "labelOnly" ||
                                            $(this).attr("data-type") == "createLine" ||
                                            $(this).attr("data-type") == "table" ||
                                            $(this).attr("data-type") == "tab-panel" ||
                                            $(this).attr("data-type") == "listNames"
                                            ) {
                                        return true; //preventing to insert it to the database
                                    }
                                    counted_id = $(this).attr("data-object-id");
                                    if ($(this).find(".getFields_" + counted_id).attr("name")) {
                                        workspace['existing_fields'].push($(this).find(".getFields_" + counted_id).attr("name").replace("[]", ""));
                                    }
                                });
                                workspace['existing_fields'] = workspace['existing_fields'].join(",");
                                
                                var json_encode = JSON.stringify(workspace);
                                //console.log(json_encode)
                                $.post("/ajax/generate", {
                                    action: "saveWorkspace",
                                    json_encode: json_encode,
                                    formId: formId,
				    type : $("#saveType").val()
                                }, function(data) {
                                    if (data) {
                                        data = JSON.parse(data)
                                        if (data['status'] == "success") {
                                            $("#popup_overlay,#popup_container").remove();
                                            bind_onBeforeOnload(0)
                                            showNotification({
                                                message: "Customize Report was successfully saved.",
                                                type: "success",
                                                autoClose: true,
                                                duration: 3
                                            });
					    window.location.replace(data['redirectURL']);
                                        }
                                        $(".saveFormLoad").hide();
					ui.unblock();
                                    }
                                })
                            }
                            break;
        
                    }
	    });
	},
        
        
        load_form_generate : function(){
            
            var pathname = window.location.pathname;
	    var user_view = $("#user_url_view").val();
	    //$('.appendObjectsHere').perfectScrollbar('destroy');
            if (pathname == "/generate" || pathname == "/user_view/generate") {
		
                var path = window.location;
		
                if (getParametersName("view_type", path) == "update"){
		   
                    $.ajax({
                        type    :   "POST",
                        url     :   "/ajax/generate",
                        dataType:   'json',
                        data    :   {action:"loadWorkspace",generateID:getParametersName("formID", path)},
                        cache   :   false,
                        success :   function(e){
                            
                			//console.log(e[0].generate_object)
			    $("body").data(e[0]);
			    var json_parser = JSON.parse(e[0]['form_json']);
			    //console.log(json_parser['WorkspaceHeight']);
			    $(".form_size_width").val(json_parser['WorkspaceWidth']);
			    $(".form_size_height").val(json_parser['WorkspaceHeight']);
			    $(".workspace").css({"width":json_parser['WorkspaceWidth'],
						 "height":json_parser['WorkspaceHeight']});
                            $(".appendObjectsHere").html(e[0].generate_object);
                            $(".tab_objects").html(e[0].tab_object);
                            $('.appendObjectsHere').perfectScrollbar('destroy');
                            $('.appendObjectsHere').perfectScrollbar();
                                     
            			    var sLength = $('.ps-scrollbar-y').length;
            			    var xLength = $('.ps-scrollbar-x').length;
                                    
            			    var l = parseInt(sLength)-2;
            			    //$('.ps-scrollbar-y:eq('+ l +')').remove();
            			    //$('.ps-scrollbar-x').remove();
				    //$('.workspace.formbuilder_ws').html(e[0].form_content);
            			   
            			    var collect_counter_id = [];
            			    $('.workspace.formbuilder_ws').find('.setObject').each(function(){
            				 var dis_ele = $(this);
            				 var dis_ele_id = dis_ele.attr('data-object-id');
            				 collect_counter_id.push(Number(dis_ele_id));
            			    });
            			    collect_counter_id.sort(function(a,b){
            				 return b-a;
            			    });
            			    count = collect_counter_id[0] + 1;
				    // alert(count + "meeee")
                    // alert()
                        }
                    });
                   
                }
                 
		
                $(".object_properties").each(function(){
                    $(this).remove();
                });
                $(".object_remove").each(function(){
                    $(this).addClass("hideObjectFields");
		     
                    $(this).removeClass("object_remove");
                    $(this).show();
                    $(this).removeClass("display");
                });
		
		$(".hideObjectFields").each(function(){
		    $(this).addClass("fa fa-times");
        });
		
                
                // Table Properties
                $(".table-actions-ra").each(function(){
                    $(this).remove();    
                });
                
                // Remove Fields
                $("body").on("click",".hideObjectFields",function(){

                    
                    var dataID = $(this).attr("data-object-id");
                    var object_name = $("#lbl_" + dataID).html();

		    var data_type = $("#setObject_" + dataID).attr("data-type");
                    //$("#setObject_" + dataID).hide();
                    //$(this).parent().parent().parent().parent().hide();
		    //$(this).parent().parent().parent().parent().attr("style",$(this).parent().parent().parent().parent().attr("style")+";display:none !important");
                    
                    $(".appendObjectsHere").perfectScrollbar('update');
                   
                    
                    if (data_type == "tab-panel") {
        			$("#setObject_" + dataID).hide();
        			$("#setObject_" + dataID).attr("style",$(this).parent().parent().parent().parent().attr("style")+";display:none !important");
        		    }else{
        			$("#setObject_" + dataID).hide();
        		    }
		    $("#setObject_" + dataID).hide();
		    //$("#setObject_" + dataID).attr("style",$(this).parent().parent().parent().parent().attr("style")+";display:none !important");
		    //$("#setObject_" + dataID).css("display","none !important");
		   
             
            var ret = '<button class="fl-buttonEffect btn-basicBtn padding_5 cursor showObjectFields dataTip" data-original-title="' + object_name + '" id="showObjectFields_' + dataID + '" data-object-id="' + dataID + '" style="border:none;"><i class="icon-cogs"></i> ' + object_name +  '</button>'; //'...'  object_name.substr(0, 10) +  use css ellipsis instead
                    
                    $(".appendObjectsHere").append(ret);
                    $(".dataTip").tooltip();
                    
                    $(".leftPointerRuler-tip").css("top",mostBottomShowTab($(".workspace").children(".setObject"))[0] + "px");
                });
              
                // Show / Add Fields
                $("body").on("click",".showObjectFields",function(){
                    var dataID = $(this).attr("data-object-id");
                    $(".dataTip").tooltip('destroy');
                    $("#showObjectFields_" + dataID).remove();
                    $("#setObject_" + dataID).show();
		    $('html, body').animate({
			scrollTop: $("#setObject_" + dataID).offset().top
		    }, 'fast');
                    $("#setObject_" + dataID).css({ "border":"1px solid",
						    "border-color":"red",
						    "z-index":"2",
							"display":""});
		    
                });

                addRemoveTable(".formbuilder_ws");
                // Check if there's a tab in the form
                 /*accordion customize print/ accordion search*/
                 
                if($('.form-accordion').length !=0){

                    var  ret = "";
                   //var tab_html="";
                    var count1 = 1;
                    var accordion_content = "";
                    var header_name = "";
                    $('.form-accordion-action-icon').empty();
                    $(".formbuilder_ws").children("[data-type='accordion']").each(function(){

                        makeThisButton($(this));
                        $(this).remove();
                        
                    });
                }

                $("body").on("click",".getContainerValue",function(){
                    var container_id = Number($(this).attr("id"))-1;
                    var element_to_append = container_array_storage[container_id];
                    console.log("arraraararaaraay", $(this).attr('data-width'));
                    var append_me = "";
                    var dot = element_to_append.attr("id");
                    var ret = "";
                        ret +=  '<div class="setObject container-parent-accordion dataTip" data-original-title="'+dot+'"></div>';
                    ret = $(ret);
                    $('.formbuilder_ws').append(ret);
                    var rulerLeft = $(".leftPointerRuler-tip").css("top");
                    var rulerTop = $(".topPointerRuler-tip").css("left");
                    ret.css({
                        "width":Number($(this).attr('data-width'))+10+"px",
                        "height":Number($(this).attr('data-height'))+10+"px",
                        "top": rulerLeft,
                        "left":rulerTop
                    });
                    element_to_append.css({
                        "top":"0px",
                        "left":"0px"
                    });
                    ret.append(element_to_append);

                    if(element_to_append.is("[data-type='accordion']")){
                        extractThisContent(element_to_append);
                    }
                    var height_array=[];
                    var content_container_height = $('.formbuilder_ws').height();

                    $('.formbuilder_ws').children('.setObject').each(function(){
                        var array_top = $(this).css('top')
                        array_top = array_top.replace('px',"");
                        var array_height = $(this).height();

                        height_array.push(Number(array_top)+array_height);
                        height_array.sort(function(a, b){return b-a});
                    });
                    if(height_array[0]>content_container_height){
                        content_container_height = height_array[0];
                    }
                    console.log("container",content_container_height);
                    $('.formbuilder_ws').css({
                        "height":content_container_height,
                    });
                    $(window).trigger("resize");
                    $('.dataTip').tooltip();
                    $(this).hide();
                    $(".leftPointerRuler-tip").css("top",mostBottomShowTab($(".workspace").children(".setObject"))[0] + "px");


                });

                if ($(".form-tabbable-pane").length!=0) {
                    
                    
                   var  ret = "";
                   //var tab_html="";
                   var count1 = 1;

                   $(".form-tabbable-pane").children("ul.ui-tabs-nav").children("li").each(function(id,val){
                        var tab = $(this).attr("aria-controls");
                        var tab_name = $(this).children().html();
                       // console.log($(this));
                        var label_id = $(this).attr("aria-labelledby");
                        
                        var tab_width = $("div[data-type='tab-panel']").width();
                        var tab_height = $("div[data-type='tab-panel']").height();
			var a = parseInt(count1);
			
			var count_setObject = $(".setObject").length;
			var new_count = parseInt(count_setObject) + parseInt(count1++);
                        
                        //<div style="position:absolute;right:0;z-index: 111111;"><i class="icon-remove"></i></div>
                        var tab_html = '<div class="showTabObjectGet setObject cursor_move ui-draggable ui-resizable"  data-tab-name="' + tab + '" data-type="tab-panel" data-object-id="' + new_count + '" id="setObject_tab_' + new_count + '" data-toggle="popover" data-placement="right" style=" border-color: rgb(221, 221, 221); float: none; position: absolute; width: '+ tab_width + 'px; height: '+ tab_height + 'px;">';
			    tab_html += '<div class="setObjects_actions"><div class="obj_actions display" id="obj_actions_1_' + a + '" style="display: none;"><i class="icon-remove cursor removeTabObjects" data-tab-name="' + tab + '"  data-object-id="' + a + '" data-original-title="Remove" ></i></div></div>';
			    tab_html +=  $("#" + tab).html();
			    tab_html += '</div>';
			//tab_html.replace(/"/g, "///\/")
                       if (tab!="blank-new-tab-panel") {
                          ret += '<button class="fl-buttonEffect btn-basicBtn padding_5 cursor getTabValue dataTip" data-original-title="' + tab_name + '" data-height="'+tab_height+'" data-objects-tab="'+ htmlEntities(tab_html) +'" data-tab-name="' + tab + '" id="' + label_id + '" data-object-id="' + label_id + '" style="border:none;"><i class="icon-cogs"></i> ' + tab_name + '</button>'; //tab_name.substr(0, 10) ellipsis not working cutting the string value. changed by: Roni Pinili aug 10, 2016
                       }
                       
		       count1++
                   });

                   $(".tab_objects").append(ret);
                  
                   $("div[data-type='tab-panel']").remove(); // remove original tab panel

                   $(".tab_objects").perfectScrollbar();
                   $(".dataTip").tooltip();
                   
                }
               
                // Get Tab Value HTMl
                var count = 1;
                $("body").on("click",".getTabValue",function(){
                    var getTab = $(this).attr("data-tab-name");
        		    $(this).hide();
                    var tabdoid = $(this).attr("data-object-id");
					
        		    var objectID = $("div[data-type='tab-panel']").attr("data-object-id");
                            var tab_width = $("div[data-type='tab-panel']").width();
        		   
                            var tab_height = $("div[data-type='tab-panel']").height();
        		    var dataHeight = $(this).attr("data-height");
        		    var top_ruler = $(".leftPointerRuler-tip").css("top");
        		    var left_ruler = $(".topPointerRuler-tip").css("left");
                    //var tab_html = '<div class="setObject cursor_move ui-draggable ui-resizable" data-type="tab-panel" data-object-id="' + count++ + '" id="setObject_tab_' + count++ + '" data-toggle="popover" data-placement="right" style=" border-color: rgb(221, 221, 221); float: none; position: absolute; width: '+ tab_width + 'px; height: '+ tab_height + 'px;">';
                    //    tab_html += $("#" + getTab).html();
                    //tab_html += '</div>';
                    //$(".formbuilder_ws").append(tab_html);
		    
                    // Row
                    
                    
                    var tabhtml = $(this).attr("data-objects-tab");
		   
                    //console.log(tabhtml.replace(/\/\/\/\//g, '"'))
                    //var ele_tabhtml = $(tabhtml.replace(/\/\/\/\//g, '"').replace(/\/\/\/>/g, "'"));

                    // Remove tab inside tab


		    var ele_tabhtml = $(tabhtml);

            // console.log($(ele_tabhtml).children("[data-type='tab-panel']")[0].remove(),"samuel pulta")
                    $(".formbuilder_ws").append(ele_tabhtml);
                    //console.log(mostBottomShowTab($(ele_tabhtml).children(".setObject"))[0]);
                    ele_tabhtml.css({
                        "height":mostBottomShowTab($(ele_tabhtml).children(".setObject"))[0] + "px"
                    })

                    var ob_id = $(ele_tabhtml).attr("data-object-id");
                    var tabhtml = $(ele_tabhtml).children("[data-type='tab-panel']");
                    var tabOffset = tabhtml[0];
					if(tabOffset){
                    tabOffset.remove();
}
		            var action_ret = "";
                    action_ret += '<div class="setObjects_actions">';
                        action_ret += '<div class="pull-left">';
                            action_ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_'+ob_id+'">';
                        action_ret += '</div>';
                        action_ret += '<div class="obj_actions fl-options-data-type" id="obj_actions_'+ob_id+'" style="">';
                            action_ret += '<i class="fa fa-times icon-remove cursor object_setup hidegenerateTab" data-tab-id="'+tabdoid+'" data-object-id="'+ob_id+'" data-original-title="Remove"></i>';
                        action_ret += '</div>';
                    action_ret += '</div>';


                    $(ele_tabhtml).children(".setObjects_actions").replaceWith(action_ret);

                    addRemoveTable(".showTabObjectGet");
                    //console.log($(ele_tabhtml).children(".thisDynamicTable > tr").length);
        		    $("div[data-tab-name='"+getTab+"']").css({"top":top_ruler,
        						    "left":0});
        		    $(".leftPointerRuler-tip").css("top",mostBottomShowTab($(".workspace").children(".setObject"))[0] + "px");
                    $(".setObject").draggable({containment:"parent",
            			// drag: function(event, ui) {
            			//     // Show Ruler guide                   
            			//     var drag_ele_current_pos_left = ui.offset.left
            			//     var drag_ele_current_pos_top = ui.offset.top
            		
            			//     dragShowVerticalRuler($(ui.helper), drag_ele_current_pos_left, drag_ele_current_pos_top, ".workspace");
            			//     dragShowHorizontalRuler($(ui.helper), drag_ele_current_pos_left, drag_ele_current_pos_top, ".workspace");
            			// },
            			// stop: function(){
            			//     removeRuler(); // Remove Rule after drag stop
            			// }
                    });
                    //console.log("HOYYSSs")
                    //console.log(ele_tabhtml)
                    //>??? DITO NA AKO
                    // $(".setObject").resizable({
                    //     "handles":"s,e"
                    // });

                    ele_tabhtml.children(".setObject").removeClass("ui-resizable");
                    ele_tabhtml.children(".setObject").find(".ui-resizable-handle").remove();
                    ele_tabhtml.children(".setObject").resizable({
                        "handles":"s"
                    });
                    // Tab Droppable
                    $(".showTabObjectGet").droppable({
                        greedy : true
                        });
                    $("div[data-type='tab-panel']").resizable();
                    //dragObects(".setObject", ".workspace", $("." + "workspace").height, count++)
                    
                    //var ret = $("div[data-type='tab-panel']").html();
                    //$(".formbuilder_ws").append(ret);
                    $(".showTabObjectGet").parents(".ui-resizable").eq(0).resizable("option","minHeight",checkParentResizableMinHeight($(".showTabObjectGet")))
                    $(".showTabObjectGet").parents(".ui-resizable").eq(0).resizable("option","minWidth",checkParentResizableMinWidth($(".showTabObjectGet")))
                    $(".showTabObjectGet").parents(".ui-resizable").eq(0).css("min-height",(checkParentResizableMinHeight($(".showTabObjectGet")))+"px")
                    $(".showTabObjectGet").parents(".ui-resizable").eq(0).css("min-width",(checkParentResizableMinWidth($(".showTabObjectGet")))+"px")
                
		    $(".form_size_height").val($(".workspace").height());
			
                });


        // recover tab hide 
        $("body").on("click",".hidegenerateTab",function(){
            var tabid = $(this).attr("data-tab-id");
            var objID = $(this).attr("data-object-id");
            $("[data-object-id='"+tabid+"']").show();
            $("#setObject_tab_" + objID).remove();
        })

                // Remove tab objects
		

        $("body").on("click",".removeTabObjects",function(){
		    var getTab = $(this).attr("data-tab-name");
		    $(this).parent().parent().parent().remove();
		    $("button[data-tab-name='"+getTab+"']").show();
                    
                    $(".leftPointerRuler-tip").css("top",mostBottomShowTab($(".workspace").children(".setObject"))[0] + "px");
		    // WOrkspace
		    $(".workspace").css("min-height",$(".leftPointerRuler-tip").position().top);
		});
		
                $("body").on("mouseenter",".setObject",function(){
                    var dataID = $(this).attr("data-object-id");
                    $(this).css("border-color","rgb(221, 221, 221)");
                    $("#obj_actions_1_" + dataID).show();
		    $("#obj_actions_" + dataID).show();
                });
                $("body").on("mouseleave",".setObject",function(){
                    var dataID = $(this).attr("data-object-id");
                    $(this).css("border-color","transparent");
                    $("#obj_actions_1_" + dataID).hide();
		    $("#obj_actions_" + dataID).hide();
                });
                
                
                
                
                // Show Dynamic Table
                $("body").on("click",".showDynamicTable",function(){
                    var dataID = $(this).attr("data-object-id");
                    var type = $(this).attr("data-type-view");
                    var title = $(this).attr("data-original-title");
                    $(".dataTip").tooltip('destroy');
                    
                    if (type=="row") {
                        $("#" + title + "_" + dataID).show();   
                    }else{
                        $("." + title + "_" + dataID).show();   
                    }
                    
                    $(this).remove();
                    
                });
		// Detect objects
		$('html, body').animate({
		    scrollTop: $(".workspace").children().first().offset().top
	    }, 'fast');

                $(".setObject.showTabObjectGet").filter(':data("uiDraggable")').removeData('uiDraggable');//eto dinagdag ko rons
                $(".setObject").draggable({containment:"parent",
        			// drag: function(event, ui) {
        			//     // Show Ruler guide                   
        			//     var drag_ele_current_pos_left = ui.offset.left
        			//     var drag_ele_current_pos_top = ui.offset.top
        		
        			//     dragShowVerticalRuler($(ui.helper), drag_ele_current_pos_left, drag_ele_current_pos_top, ".workspace");
        			//     dragShowHorizontalRuler($(ui.helper), drag_ele_current_pos_left, drag_ele_current_pos_top, ".workspace");
        			// },
        			// stop: function(){
        			//     removeRuler(); // Remove Rule after drag stop
        			// }
                });
                // Tab Droppable
                $(".showTabObjectGet").droppable({
                    greedy : true
                });
		    
		    
            }
            
        }
    }
    
    $(document).ready(function(){
	   var pathname = window.location.pathname;
        if (pathname == "/user_view/generate") {
            
            
    		$(".workspace").resizable();
            workspace_functions.view_save(".save_workspace");
            customize_reports.save_form_workspace(".save_form_workspace");
            customize_reports.load_form_generate();

            var collect_counter_id = [];
                            $('.workspace.formbuilder_ws').find('.setObject').each(function(){
                             var dis_ele = $(this);
                             var dis_ele_id = dis_ele.attr('data-object-id');
                             collect_counter_id.push(Number(dis_ele_id));
                            });
                            console.log('collect_counter_id', collect_counter_id)
                            collect_counter_id.sort(function(a,b){
                             return b-a;
                            });
                            count = collect_counter_id[0] + 1;
            //         alert(count + "meeee000807708")

            // alert(count+"PAGKATAPOS")
            // alert(1)
    	}
        
        //resizeForm(".formbuilder_ws", $(".form_size_height").val());
        var pathname = window.location.pathname;
      
        if (pathname == "/workspace" || pathname == "/user_view/workspace" ) {
            $(".removeRow, .removeCol").hide();
        }
            
        $(".leftPointerRuler-tip").css("top",mostBottomShowTab($(".workspace").children(".setObject"))[0] + "px");
    });
    
    // Functions
    function mostBottomShowTab(elements) {
        var collect = [];
        if ($(elements).length >= 1) {
            $(elements).each(function(eqi){
                collect.push($(this).position().top + $(this).outerHeight());
            })
            collect.sort(function(a,b){
                return b - a;
            })
            
        }else{
            collect.push(0);
        }
        return collect; 
    }
   var container_button_count = "1";
   var container_array_storage = [];
    function makeThisButton(ele){
    

        if(ele.is("[data-type='accordion']")){
            var doid = ele.attr("data-object-id");
            var ele_type = ele.attr("data-type");
            var label_id = ele.attr("id");
            var ele_clone = ele.clone();
            var this_width = ele.width();
            var this_height = ele.height(); 

        }
        var ret = "";
            ret += '<button class="fl-buttonEffect btn-basicBtn padding_5 cursor getContainerValue dataTip" data-original-title="' + ele_type + '" data-objects-container="'+ htmlEntities(ele) +'" data-accordion-name="'+ele_type+'"  id="' + container_button_count + '" data-object-id="' + doid + '" style="border:none;" data-height="'+this_height+'" data-width="'+this_width+'"><i class="icon-cogs"></i> ' + ele_type.substr(0, 10) + '</button>';

        $(".tab_objects").append(ret);
        $(".tab_objects").perfectScrollbar();
        $(".dataTip").tooltip();
        container_array_storage.push(ele_clone);
        container_button_count++;
         
    }
    function extractThisContent(element){
        // console.log("elel",element.find('.form-accordion:eq(0)').find('.form-accordion-section-group:eq(0)'),element.find('.form-accordion:eq(0)').find('.form-accordion-section-group:eq(0)').outerHeight())
        var option_focused_component_datas= [];
        var extract_me = element;
        var container_settings_resize = {
            "handles":"s,e",
            "containment":"parent",
            "stop":function(e,ui){
                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                checkParentResizableMinWidth($(this));
                checkParentResizableMinHeight($(this));
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)));
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)));
            }
        }
        var container_settings_draggable = {
            "containment":"parent",
        }
        
        var content_container_height = extract_me.find('.form-accordion:eq(0)').find('.form-accordion-section-group:eq(0)').outerHeight();
        var content_container_width = extract_me.find('.form-accordion:eq(0)').find('.form-accordion-section-group:eq(0)').outerWidth();
        var section_group = extract_me.find('.form-accordion:eq(0)').children('.form-accordion-section-group').clone();
        var element_parent = extract_me.parent();
        
        extract_me.remove();

        element_parent.append(section_group);

        element_parent.resizable(container_settings_resize);
        element_parent.draggable(container_settings_draggable);
        element_parent.droppable({
            "greedy":true
        });
        section_group.each(function(){

            var ret = "";
            var section_contents = $(this).find('.accordion-div-content:eq(0)').children('.setObject').clone();
            
            var height_array=[];
            $(this).find('.accordion-div-content:eq(0)').children('.setObject').each(function(){
                var array_top = $(this).css('top')
                array_top = array_top.replace('px',"");
                var array_height = $(this).height();

                height_array.push(Number(array_top)+array_height);
                height_array.sort(function(a, b){return b-a});
            });
            if(height_array[0]>content_container_height){
                content_container_height = height_array[0];
            }
            
            var header_name = $(this).find('.accordion-header-name:eq(0)').text();
           
            ret +=  '<div class="setObject accordion-position dataTip" data-original-title="'+header_name+'"/>';
            $(this).wrap(ret);
            var ret_element =  $(this).parent();
            var element_parent = extract_me.parent();

            ret_element.css({
                "top":"5px",
                "left":"5px",
                "height":content_container_height +5+ "px",
                "width":content_container_width+"px",
            });
            $(this).remove();
            
            element_parent.append(ret_element);
            ret_element.append(section_contents);
            ret_element.resizable(container_settings_resize);
            ret_element.draggable(container_settings_draggable);
            ret_element.droppable({
                "greedy":true
            });
           
            section_contents.resizable(container_settings_resize);
            section_contents.draggable(container_settings_draggable);

            if(ret_element.find('.setObject[data-type="accordion"]')){
                var accordion_position_holder = $('.accordion-position').find('.setObject[data-type="accordion"]');
                console.log("holder",accordion_position_holder)
                accordion_position_holder.each(function(){
                    
                    
                    var inner_accordion_height = $(this).outerHeight();
                    var inner_accordion_width = $(this).outerWidth();
                    dot = $(this).attr('id');
                    var ret2 = "";
                        ret2 +=  '<div class="setObject container-parent-accordion dataTip" data-original-title="'+dot+'"></div>';
                    ret2 = $(ret2);
                    $(this).parent().append(ret2);
                    ret2.css({
                        "width":inner_accordion_width,
                        "height":inner_accordion_height,
                        "top":"0px",
                        "left":"0px"
                    });
                    $(this).css({
                        "top":"0px",
                        "left":"0px"
                    });
                    ret2.append($(this));
                    extractThisContent($(this));
                });

            }


        });
       var positioning_accordion =  $('.accordion-position').css({
            "position":"relative",
             "top":"0px",
             "left":"0px",
        }).map(function(){
           
                return {
                    "top":$(this).position().top,
                    "left":$(this).position().left,
                    "ele":$(this)
                };   
        }).get();
        var display_before = $('.container-parent-accordion').css('display');
        $('.container-parent-accordion').css({
            "display":"inline-table"
        });
        $('.container-parent-accordion').each(function(){
            $(this).css({
                "width":$(this).width()+"px",
                "height":$(this).height()+"px",
                "display":display_before,
            });
        })
        
        for(var ctr in positioning_accordion){
            positioning_accordion[ctr]['ele'].css({
                "top":positioning_accordion[ctr]['top']+"px",
                "left":positioning_accordion[ctr]['left']+"px",
                "position":"absolute"
            })
            // alert("alerta"+positioning_accordion[ctr]['top']+"px")
        }
  
    }
    function addRemoveTable(elementAppend) {
        count = 0;
        count_1 = 0;
        $(elementAppend).children(".setObject").each(function(){
            if($(this).find(".form-table.thisDynamicTable").length == 1){
                var ele_table = $(this).find(".form-table.thisDynamicTable");
                
                ele_table.children("tbody").children("tr").each(function(){
                    
                    var append_button = $('<div class="removeRow " style="position:absolute;left:0;z-index: 111111;cursor: pointer;margin-left: -10px;"><i class="icon-remove"></i></div>');
                    
                    $(this).children("td").first().children(".td-relative").prepend(append_button);
                    var nametbl = $(this).parents(".setObject").eq(0).attr("data-object-id");
                    var object_name = $("#lbl_" + nametbl).html().replace(":","");
                    object_name = object_name.replace(new RegExp(" ","g"), "_");
                    
                    $(this).attr({"id":object_name+ "_" + count,
                                 "data-id":count});
                    // Table Tab Name
                    
                    append_button.on({
                        "click.HideMyRow":function(e){
                            $(this).parents("tr").hide();
                            var nametbl = $(this).parents(".setObject").eq(0).attr("data-object-id");
                            var object_name = $("#lbl_" + nametbl).html().replace(":","");
                            object_name = object_name.replace(new RegExp(" ","g"), "_");
                            
                            var dataID = $(this).parents("tr").attr("data-id");
                            var ret = '<button class="fl-buttonEffect btn-basicBtn padding_5 cursor showDynamicTable dataTip" data-type-view="row" data-original-title="' + object_name + '" id="dynamicTable_' + dataID + '" data-object-id="' + dataID + '" style="border:none;"><i class="icon-cogs"></i> ' + object_name.substr(0, 5) + '...' + '</button>';
                    
                            $(".appendObjectsHere").append(ret);
                            $(".appendObjectsHere").perfectScrollbar();
                            $(".dataTip").tooltip();
                            
                            
                        }
                    });
                    count++;
                });
                ele_table.children("tbody").children("tr").first().children("td").first().find(".removeRow").remove();
            }
        })
        
        $(elementAppend).children(".setObject").each(function(){
            if($(this).find(".form-table.thisDynamicTable").length == 1){
                var ele_table = $(this).find(".form-table.thisDynamicTable");
                
                ele_table.children("tbody").children("tr").each(function(eqi){
                    if (eqi == 0) {
                        var append_button = $('<div class="removeCol " style="position:absolute;right:0;z-index: 111111;cursor: pointer;margin-top: -15px;"><i class="icon-remove"></i></div>');
                        append_button.on({
                            "click.HideMyCol":function(e){
                                var hide_colindex = $(this).parents("td").eq(0).index();
                                
                                $(this).parents("tbody").eq(0).children("tr").each(function(){
                                    $(this).children("td").eq(hide_colindex).hide();
                                });
                                
                                var nametbl = $(this).parents(".setObject").eq(0).attr("data-object-id");
                                var object_name = $("#lbl_" + nametbl).html().replace(":","");
                                object_name = object_name.replace(new RegExp(" ","g"), "_");
                                
                                var dataID = $(this).parents("td").eq(0).attr("data-id");
                                var ret = '<button class="fl-buttonEffect btn-basicBtn padding_5 cursor showDynamicTable dataTip" data-type-view="col" data-original-title="col_' + object_name + '" id="dynamicTable_' + dataID + '" data-object-id="' + dataID + '" style="border:none;"><i class="icon-cogs"></i> ' + object_name.substr(0, 5) + '...' + '</button>';
                        
                                $(".appendObjectsHere").append(ret);
                                $(".appendObjectsHere").perfectScrollbar();
                                $(".dataTip").tooltip();
                                
                                
                            }
                        });
                        $(this).children("td").children(".td-relative").prepend(append_button);
                        
                        
                    }
                    var nametbl = $(this).parents(".setObject").eq(0).attr("data-object-id");
                    var object_name = $("#lbl_" + nametbl).html().replace(":","");
                    object_name = object_name.replace(new RegExp(" ","g"), "_");
                    $(this).children("td").each(function(eqi_tdindex){
                        $(this).attr({"id":"col_" + object_name+ "_" + eqi_tdindex,
                                    "data-id":eqi_tdindex});
                        $(this).addClass("col_" + object_name+ "_" + eqi_tdindex);
                       // Table Tab Name
                    })
                    count_1++;
                });
                ele_table.children("tbody").children("tr").first().children("td").first().find(".removeCol").remove();
            }
        })
        
        $(elementAppend).children(".setObject").find(".thisDynamicTable").on({
            "mouseenter.dTableEnter":function(){
                $(this).find(".removeCol, .removeRow").show();
            },
            "mouseleave.dTableLeave":function(){
                $(this).find(".removeCol, .removeRow").hide();
            }
        })
    }
