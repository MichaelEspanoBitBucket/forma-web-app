/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var pin = {
    init: function() {
        this.addEventListeners();
    },
    addEventListeners: function() {
        var self = this;

        $('.dashboard_wrapper').sortable({
            tolerance: "pointer",
            items: "> div",
            helper: "clone",
            placeholder: "dashboard-placeholder",
            containment: "parent",
            handle: ".handle_dashboard",
            stop: function(event, ui) {
                console.log(event);
                console.log(ui);

                dashboard.save();
            },
            start: function(e, ui) {
                $(this).sortable("refreshPositions");
            }
        });

        $('body').on('click', '.dashboard_pin', function() {
            var pin = this;

            ui.block();
            var content = [];
            var newWidget = [];
            content.push({
                object_type: $(this).attr('fl-data-type'),
                object_id: $(this).attr('fl-data-object-id')
            });

            newWidget.push({
                    width:12, 
                    height:4, 
                    id:Date.now(),
                    type:"widgetView",
                    object_form_name:$('.form-name').val(),
                    object_type: $(this).attr('fl-data-type'),
                    object_id: $(this).attr('fl-data-object-id')
                });

             self.saveToDashboard({data:newWidget}, function(result) {

                ui.unblock();
                console.log(result);
                //$(pin).css('opacity', '.4');
                showNotification({
                    message: "Content has been pinned.",
                    type: "success",
                    autoClose: true,
                    duration: 3
                });
            });

            self.save({
                application_id: $(this).attr('fl-data-application-id'),
                content: JSON.stringify(content)
            }, function(result) {

                ui.unblock();
                console.log(result);
                //$(pin).css('opacity', '.4');
                showNotification({
                    message: "Content has been pinned.",
                    type: "success",
                    autoClose: true,
                    duration: 3
                });
            });
        });

        //request pin
        $('body').on('click', '.request_dashboard_pin', function() {

            ui.block();
           
            
            var pin = this;
            var content = [];
            content.push({
                object_type: "request_form",
                object_id: $('#FormName').val(),
                formId: $('#FormID').val(),
                requestId: $('#ID').val(),
                url: String(window.location)
            });
             
            self.save({
                application_id: $(this).attr('fl-data-application-id'),
                content: JSON.stringify(content)
            }, function(result) {
                console.log(result);

                ui.unblock();
                showNotification({
                    message: "Content has been pinned.",
                    type: "success",
                    autoClose: true,
                    duration: 3
                });
                //$(pin).css('opacity', '.4');
//                showNotification({
//                    message: "Panel s.",
//                    type: "success",
//                    autoClose: true,
//                    duration: 3
//                });
            });
            
            
        });

        $('body').on('click', '.unpin', function() {
            //unpin
            $(this).tooltip('hide');
            $(this).closest('.fl-table-wrapper').remove();

            dashboard.save()
        });
    },
    save: function(data, callback) {
        $.get('/ajax/dashboard', data, function() {
            callback(data);
        });
    },

    saveToDashboard:function(data, callback){
        $.post('/modules/dashboard_data/save-dashboard-pinned-view.php', data, function() {
            callback(data);
        });
    }
}

var dashboard = {
    save: function() {
        ui.block();
        var content = [];
        var applicationID = '';
        var dashboard = [];
        $('[fl-data="dashboard"]').each(function() {
            applicationID = $(this).attr('fl-data-application-id');

            if ($(this).attr('fl-data-type') == 'report') {
                content.push({
                    object_type: $(this).attr('fl-data-type'),
                    object_id: $(this).attr('fl-data-object-id'),
                    formId: $(this).attr('fl-data-form-id'),
                    reportId: $(this).attr('fl-data-report-id'),
                    url: $(this).attr('fl-data-url')
                });
            } else if ($(this).attr('fl-data-type') == 'request_form') {
                content.push({
                    object_type: $(this).attr('fl-data-type'),
                    object_id: $(this).attr('fl-data-object-id'),
                    formId: $(this).attr('fl-data-form-id'),
                    requestId: $(this).attr('fl-data-request-id'),
                    url: $(this).attr('fl-data-url')
                });
                
            } else {
                content.push({
                    object_type: $(this).attr('fl-data-type'),
                    object_id: $(this).attr('fl-data-object-id'),
                });

                newWidget.push({
                    width:12, 
                    height:3, 
                    id:Date.now(),
                    type:"widgetView",
                    object_type: $(this).attr('fl-data-type'),
                    object_id: $(this).attr('fl-data-object-id')
                });
            }

        });

        // console.log(content);
        // console.log(newWidget)
        pin.saveToDashboard({data:newWidget}, function(result) {
            ui.unblock();
            console.log(result);
        });
        // pin.save({
        //     application_id: applicationID,
        //     content: JSON.stringify(content),
        //     action: 'sorting'
        // }, function(result) {
        //     ui.unblock();
        //     console.log(result);
        // });
    }
};

$(document).ready(function() {
    pin.init();
});