$(document).ready(function(){
    //starred
    var objStarred_user_widget = {"type":"2"};
    $(".starredUserWidgetWrapper").closest(".fl-widget").find(".fl-widget-wrapper-center-content").show();
    $(".starredUserWidgetWrapper").closest(".fl-widget").find(".fl-loading-content").fadeIn();
    Starred.load(0,"start",objStarred_user_widget,function(result){
        $(".starredUserWidget").html(result)
        widgetUserSeeMore(".starredUserWidget","/user_view/starred")
        Starred.count();
        //$(".starredUserWidgetWrapper").perfectScrollbar("destroy"); remove by roni pinili unwanted destroy
        
        $(".starredUserWidgetWrapper").closest(".fl-widget").find(".fl-loading-content").fadeOut();
        // $(".starredUserWidgetWrapper").closest(".fl-widget").find(".fl-widget-wrapper-center-content").hide();
        if(result==""){
            $(".starredUserWidgetWrapper").closest(".fl-widget").find(".fl-no-data-found").fadeIn();
        }
    // posting.loadEvent();
    
    })

    $(".starredUserWidgetWrapper").mouseenter(function(){
        $(".starredUserWidgetWrapper").perfectScrollbar("update");
    })

    $(".starredUserWidgetWrapper").scroll(function() {
        var scroll_diff = $(this).scrollTop() - ($(this).find(".starredUserWidget").outerHeight() - $(this).outerHeight());
        // console.log((scroll_diff+" >= "+(-2))+" && "+(scroll_diff+" <= "+2))
        if ((scroll_diff >= -20) && (scroll_diff <= 22)) {
            if(continueToLoad_starred==true){
                Starred.load(ctr_load_starred,"load",objStarred_user_widget,function(result){
                    if(result.length>0){
                        ctr_load_starred+=10;
                        continueToLoad_starred = true;
                        $(".starredUserWidget").append(result);
                        $(".starredUserWidgetWrapper").perfectScrollbar("destroy");
                        $(".starredUserWidgetWrapper").perfectScrollbar();
                    }
                    widgetUserSeeMore(".starredUserWidget","/user_view/starred")
                }); 
            }
        }
    });


    var objMyRequestUser = {"type":"1"};
    //my request
    $(".myRequestUserWidgetWrapper").closest(".fl-widget").find(".fl-widget-wrapper-center-content").show();
    $(".myRequestUserWidgetWrapper").closest(".fl-widget").find(".fl-loading-content").fadeIn();
    MyRequest.load(0,"start",objMyRequestUser,function(result){
        $(".myRequestUserWidget").html(result);
        widgetUserSeeMore(".myRequestUserWidget","/user_view/my-requests")
        MyRequest.count();
        $(".myRequestUserWidget").closest(".fl-widget").find(".fl-loading-content").fadeOut();
        if(result==""){
            $(".myRequestUserWidget").closest(".fl-widget").find(".fl-no-data-found").fadeIn();
        }
    });

    $(".myRequestUserWidgetWrapper").mouseenter(function(){
        $(".myRequestUserWidgetWrapper").perfectScrollbar("update");
    })
    
})

function widgetUserSeeMore(container,link){
    var length = $(container).find(".fl-widget-content").length;
    if(length>=10 && $(container).closest(".fl-widget-wrapper").find(".fl-see-all").length==0){
        $(container).closest(".fl-widget-wrapper").append('<div class="fl-see-all">' +
                    '<a href="'+ link +'"><span>See all <i class="fa fa-hand-o-right"></i></span></a>' +
                '</div> ');
    }
}