

var app = angular.module("myApp", ['gridstack-angular','DashboardServices','WidgetAppsModule','WidgetFeedsModule','WidgetViewModule','WidgetListsModule','ngAnimate', 'WidgetChartModule','WidgetFormaGanttChart','dashboard.header','ui.router','CollaborationServices']);


app.config(function($stateProvider,$urlRouterProvider){
	console.log('config')
	$urlRouterProvider.otherwise('/');

	$stateProvider
    .state('home', {
      url: "/",
      controller : 'Gridcontent',
      templateUrl: "/modules/dashboard/templates/home.html"
    })
    .state('createdesign', {
      url: "/designer",
      controller : 'Gridcontent',
      templateUrl:'/modules/dashboard/templates/home.html' 
    })
    .state('designer', {
      url: "/designer/:id",
      controller : 'Gridcontent',
      templateUrl:'/modules/dashboard/templates/home.html' 
    })
    
    
})



app.controller('Gridcontent', function($scope,$log, UserPreferencesService, NavigationService,$state,$uibModal,DashboardTemplateService) {
///$scope.widgets = [{ x:0, y:0, width:3, height:4 }, { x:0, y:0, width:9, height:4 }];
	//console.log($state)
	$scope.ShowFeed = false;
	$scope.ShowStarred = false;
	$scope.isNavLoaded = false;

	UserPreferencesService
		.getPreferences
		.then(
			function(data){

				console.log(data)
				$scope.company_info = data.CompanyInfo
				$scope.current_user = data.Current_User
				$scope.widgets = data.Layout;
				$scope.config = data.CompanyMod;
				$scope.configProp = data.Properties;
				$scope.isChanged = false;
				$scope.isDesignMode = false; 

		
				if ($state.current.name == 'createdesign')
				{
					$scope.widgets = []
					$scope.isDesignMode = true;
				}
				else{
					if($scope.widgets==null){
					//Default setup...	for prod
						// $scope.widgets = [{ x:0, y:0, width:3, height:6,id:Date.now() + Math.random(), type:"widgetFeeds"},
						// { x:9, y:0, width:3, height:6,id:Date.now() + Math.random(), type:"widgetStarred"},
						// { x:3, y:0, width:6, height:6,id:Date.now() + Math.random(), type:"widgetView", object_form_name:"Leave Requests", object_id:"2", object_type:"request"}];
						 $scope.isChanged = true;
						console.log($scope.current_user)
						DashboardTemplateService.getTemplates($scope.current_user.id).then(function(data){
							$scope.widgets = []
							if (data["userTemplates"]){
								if (data["userTemplates"].length > 0){
									$scope.widgets = angular.fromJson(data["userTemplates"][0]['layout'])
								}
							}
							if (data["groupTemplates"]){
								if (data["groupTemplates"].length > 0){
									$scope.widgets = angular.fromJson(data["groupTemplates"][0]['layout'])
								}
							}

							if(getWidgetIndex("widgetFeeds")==null){
								$scope.ShowFeed = false;
							}else{
								$scope.ShowFeed = true;
							}
							if(getWidgetIndex("widgetStarred")==null){
								$scope.ShowStarred = false;
							}else{
								$scope.ShowStarred = true;
								
							}

						})
					}
					else{
						if(getWidgetIndex("widgetFeeds")==null){
							$scope.ShowFeed = false;
						}else{
							$scope.ShowFeed = true;
						}
						if(getWidgetIndex("widgetStarred")==null){
							$scope.ShowStarred = false;
						}else{
							$scope.ShowStarred = true;
							
						}
					}
					
				}

			},
			function(){

			}
		)

	$scope.isChanged = false;

	$scope.options = {
	    cellHeight: 80 ,
	    verticalMargin: 10,
	    float:false,
	    resizable:{
	    	handles: "n, e, s, w, ne, se, sw, nw"
	    },
	    draggable:{

	    	cancel:".feed-body, .widget-feed-tools, .btn, .survey_response_item, .feed-element, input, .ibox-content"
	    }
	};

	$scope.navigation = function(){
		if(!$scope.isNavLoaded){
			NavigationService.getNavigation().then(function(data){
				$scope.isNavLoaded = true;
				angular.element('.nav-item-navigation').append(data.data); 
			})
		}
	}

	$scope.addWidget = function() {
	    var newWidget = { x:0, y:0, width:3, height:2, id:Date.now(),type:"widgetApps"};
	    $scope.widgets.push(newWidget);
	    $scope.isChanged=true;
	};

	$scope.addWidgetGanttChart = function() {
	    var newWidget = { x:0, y:0, width:5, height:5, id:Date.now(),type:"widgetGanttChart"};
	    $scope.widgets.push(newWidget);
	    $scope.isChanged=true;
	};

	$scope.removeWidget = function(w) {
	    var index = $scope.widgets.indexOf(w);
	    $scope.widgets.splice(index, 1);    
	    $scope.isChanged=true;
	};

	function getWidgetIndex(widgetType){
		var index = null;
		if ($scope.widgets.length==0) {
			return null
		};
		for (var i = $scope.widgets.length - 1; i >= 0; i--) {
			if($scope.widgets[i].type==widgetType){
				index = i;
			}
			
		};
		return index;
	};

	$scope.addRemoveFeedWidget = function(){
		var index = getWidgetIndex("widgetFeeds");
	
		// console.log($scope.widgets)
		// console.log(index)
		if(index==null){
			var newWidget = { x:0, y:0, width:3, height:6,id:Date.now(), type:"widgetFeeds"};
	    	$scope.widgets.push(newWidget);
	    	$scope.ShowFeed = true;
		}else{
			$scope.widgets.splice(index,1);
			$scope.ShowFeed = false;
		}

		
	   $scope.isChanged=true;
	}

	$scope.addRemoveStarredWidget = function(){
		var index = getWidgetIndex("widgetStarred");
	
		console.log($scope.widgets)
		console.log(index)
		if(index==null){
			var newWidget = { x:0, y:0, width:3, height:6,id:Date.now(), type:"widgetStarred"};
	    	$scope.widgets.push(newWidget);
	    	$scope.ShowStarred = true;
		}else{
			$scope.widgets.splice(index,1);
			$scope.ShowStarred = false;
		}
		$scope.isChanged=true;
	}


	$scope.removeFeedWidget = function(){
		
	}

	$scope.editLayout = function(){
		$scope.isChanged = false;
		
	}

	$scope.cancelEditLayout = function(){
		$scope.isChanged = false;

	}

	$scope.saveLayout = function(){
		$scope.isChanged = false;

		//to save the chronological position of every widget.
		var res = _.map($('.grid-stack .grid-stack-item:visible'), function (el) {
		    el = $(el);
		    var node = el.data('_gridstack_node');
		    return el.data("$scope").w;
		 
		});
		
		var sortedWidget = resolve.sort(function(a, b){
		    var keyA = a.y;//new Date(a.updated_at),
		        keyB = b.y;//new Date(b.updated_at);
		    // Compare the 2 dates
		    if(keyA < keyB) return -1;
		    if(keyA > keyB) return 1;
		    return 0;
		});

		
			UserPreferencesService.saveLayout(sortedWidget);
		}
	


	$scope.saveTemplate = function(){
		// write script 

		$scope.isChanged = false;
		var res = _.map($('.grid-stack .grid-stack-item:visible'), function (el) {
		    el = $(el);
		    var node = el.data('_gridstack_node');
		    return el.data("$scope").w;
		 
		});
		
		var sortedWidget = res.sort(function(a, b){
		    var keyA = a.y;//new Date(a.updated_at),
		        keyB = b.y;//new Date(b.updated_at);
		    // Compare the 2 dates
		    if(keyA < keyB) return -1;
		    if(keyA > keyB) return 1;
		    return 0;
		});

		$scope.layout = sortedWidget

		 var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: '/modules/dashboard/templates/dashboard-create-template-dialog.html',
	      controller : 'SaveDashboardController',
	      scope : $scope,
	      //size: size,
	      resolve: {
	        items: function () {
	          return $scope.resources;
	        }
	      }
	    });
	      
	}




	$scope.onResizeStop = function(event, items){
		$scope.isChanged = true;
	}

	$scope.onDragStop = function(event, items){
		$scope.isChanged = true;
	}


	$scope.showDashboardTemplateCreator = function(){
		$scope.isInDesignMode = true
	}

	$scope.showDashboardTemplateCreator = function(){
		$scope.isInDesignMode = true
	}

	$scope.openSelectViews = function () {
	///console.log("sd");
	$scope.isConfig = false;
    var modalInstance = $uibModal.open({
      animation: false,
      templateUrl: '/modules/dashboard/templates/modal-add-widget-view.html',
      controller: 'SelectViewController',
    });

      modalInstance.result.then(function (items) {
      	console.log(items);

      	for (var i = items.length - 1; i >= 0; i--) {
      		
      		$scope.widgets.push({x:3, y:0, width:12, height:6,id:Date.now() + Math.random(), type:"widgetView", object_form_name:items[i].display_name, object_id:items[i].id, object_type:"request"});

      	};
      	
		// $scope.schedTitle = item.schedTitle;
		// $scope.schedDescription = item.schedDescription;
		// $scope.schedColor = item.schedColor.charAt(0) == '#'?item.schedColor:'#' + item.schedColor;

  //     	$scope.sched.addLegend();
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
}



});

app.directive('onErrorSrc', function() {
  return {
    	link: function(scope, element, attrs) {
      	element.bind('error', function() {
      		//console.log(attrs.onErrorSrc);
        		if (attrs.src != attrs.onErrorSrc) {
        			attrs.$set('src', attrs.onErrorSrc);
         		}
    		});
		}
	}
});



app.controller('SelectViewController', function ($scope, $uibModalInstance, DashboardTemplateService, $filter) {

  $scope.views = [];
  $scope.collaborators;


  DashboardTemplateService.getAllFormaForms().then(function(data){
  	$scope.views = data;
  })


  $scope.ok = function () {
  	var $selected = $filter('filter')($scope.views, {selected: true});
    $uibModalInstance.close($selected);
  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

