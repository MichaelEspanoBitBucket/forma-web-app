var dbModels = angular.module('DashboardServices',[]);

var VisibilityType = {
        PUBLIC: '1',
        COMPANY: '2',
        CUSTOM: '3'
    };

    var PostType = {
        ANNOUNCEMENT: 1,
        SURVEY: 2
    };

    var FollowerType = {
    	USER:1,
    	GROUP:2,
    	DEPARTMENT:3,
    	POSITION:4,
    	PERSONAL_GROUP:null
    }

dbModels.service('getAllFeed', function($http){

	//filter_post_type=1&
	this.get = function(param){
		return $http.post('/portal/post_list',
				"from_index=0&fetch_count=5&response_type=JSON",
				{
					headers: {
		   				'Content-Type': "application/x-www-form-urlencoded"
					}
				}
			).then(
				function(data){
					return data.data;
				},
				function(data){
					return data;
				}
			)
	}
});

dbModels.factory('Posts', function($http) {
  var Posts = function() {
    this.posts = [];
    this.busy = false;
    this.after = 0;
  };

 	Posts.prototype.reload = function() {
  	var context = this;
  	this.posts = [];
    this.after = 0;
	///console.log(context.busy)
  	return this.loadMore();


	 
	}

	Posts.prototype.loadMore = function() {
	  	var context = this;
		///console.log(context.busy)
	    if (context.busy) return;
	    context.busy = true

		   // console.log(context.busy)
		   //&filter_post_type=1
			return $http.post('/modules/dashboard_data/post_list_json.php',
				"from_index=" + context.after + "&fetch_count=4&response_type=JSON",
				{
					headers: {
		   				'Content-Type': "application/x-www-form-urlencoded"
					}
				}
			).then(
				function(data){
					var posts = data.data;
					for(var i = 0; i<posts.length;i++){
						
						context.posts.push(posts[i]);
					}
				
					context.after = context.posts.length;
					context.busy = false;
				},
				function(data){
					//context.busy = false;
					//return data;
				}
			)
	}

	Posts.prototype.fetchPostComment = function(post, count) {
	  	var context = this;
	  	if (count==undefined) {count=3};
		if(post.comments == undefined){
			post.comments = [];
		}
		post.isCommenting = true;
		
		   // console.log(context.busy)
		   //&filter_post_type=1
			return $http.post('/modules/dashboard_data/post_comment_list_json.php?' + "post_id=" + post.id + "&from_index=" + post.comments.length + "&fetch_count=" + count,
				
				{
					headers: {
		   				'Content-Type': "application/x-www-form-urlencoded"
					}
				}
			).then(
				function(comm){
					var comm = comm.data;
					for(var i = 0; i<comm.length;i++){
						
						post.comments.push(comm[i]);
					}
				
					// context.after = context.posts.length;
					// context.busy = false;
				},
				function(data){
					//context.busy = false;
					//return data;
				}
			)
	}





	return Posts;
});



dbModels.service('UserPreferencesService', function($http,$window){

	context = this
	this.getPreferences = $http.get("/modules/dashboard_data/get-dashboard-user-preferences.php").then(
			function(data){
				console.log(data)
				if (data.status == 401){
					
					return data.status
				}
				return data.data;
				
			},
			function(error){
				//context.busy = false;
				return [];
		}
	)

	

	this.saveApps = function(apps){
		d = {Apps:apps}
		return $http.post('/modules/dashboard_data/save-dashboard-user-apps.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.saveLayout = function(layout){
		
		d = {
				Layout:layout
		}

		

		return $http.post('/modules/dashboard_data/save-dashboard-user-layout.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.saveLayoutAsTemplate = function(layout,user_id,templateName){
		d = {
				Layout:layout,
				User : user_id,
				templateName : templateName
		}

		return $http.post('/modules/dashboard_data/save-dashboard-user-layout.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)

	}



	
})

dbModels.service('NavigationService', function($http){

	context = this;

	this.getNavigation = function(){
		d = {action:"getLeftBar"};
		return $http.post('/ajax/navigation',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}


	
})


function serialize(obj, prefix) {
    var str = [];
    for (var p in obj) {
      if (obj.hasOwnProperty(p)) {
        var k = prefix ? prefix + "[" + p + "]" : p,
              v = obj[p];
          str.push(typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
      }
    }
    return str.join("&");
}

function formatFollwers(flwr){
	var formatedFlwrs = [];
	if (flwr!=undefined) {
		for (var i = flwr.length - 1; i >= 0; i--) {
			
			formatedFlwrs.push({
				type:FollowerType[flwr[i].type],
				display_text:flwr[i].text,
				value:flwr[i].id,
			})
		};
	};
	return formatedFlwrs;
	
}

dbModels.service('FeedsService', function($http){

	this.saveAnnouncement = function(postTextContents, postVisibilityType, postId, followers){
		//d = {Layout:layout}
		var params = {
            post_visibility_type: postVisibilityType,
            post_text_contents: postTextContents,
            post_type: 1,
            post_id: postId
        };
         if(postVisibilityType == VisibilityType.CUSTOM){
        	params.post_followers = formatFollwers(followers);
        }
		console.log(params)

		return $http.post('/portal_api/save_announcement',
			serialize(params),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.removePost = function(postId){

		return $http.get('/portal_api/delete_post?post_id='+ postId)
	}

	this.saveComment = function(post_id, text_content){
		//d = {Layout:layout}
		var params = {
            text_content: text_content,
            post_id: post_id
        };
		console.log(params)
		///portal_api/save_post_comment
		return $http.post('/modules/dashboard_data/save_post_comment.php',
			serialize(params),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.saveSurvey = function(postTextContents, postVisibilityType, postId,  responses, enable_multiple_responses, publish_results, followers){
		//d = {Layout:layout}
		var params = {
            post_visibility_type: postVisibilityType,
            post_text_contents: postTextContents,
            post_type: 2,
            post_id: postId,
            survey_responses: responses,
            enable_multiple_responses: enable_multiple_responses,
            publish_results: publish_results
        };

        if(postVisibilityType == VisibilityType.CUSTOM){
        	params.post_followers = formatFollwers(followers);
        }
        console.log(params)

        
		return $http.post('/portal_api/save_survey',
			serialize(params),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.saveSurveyResponses = function(params){
	
        console.log(params)

        
		return $http.post('/portal_api/save_survey_response',
			serialize(params),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	

	this.setMood = function(params){
		
        console.log(params)

        
		return $http.post('/portal_api/set_mood',
			serialize(params),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}


	this.getSurveyResults = function(id){        
		return $http.get('/modules/dashboard_data/get_dashboard_survey_result.php?id=' + id)
	}

	this.getUserMoods = function(params){
		
        console.log(params)

        
		return $http.post('/portal_api/user_moods',
			serialize(params),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}


	this.loadNames = function($query) {
		params = {
			search_string:$query
		}
	    return $http.post('/modules/dashboard_data/name_selection.php',serialize(params), { 
			headers: {
				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
			}
		}).then(
				function(data){
					return data.data;
				},
				function(data){
					return data;
				}
			)
  	};


})


dbModels.service('ViewService', function($http){
	this.getViewDetails = function(form_id){
		return $http.get('/modules/dashboard_data/widget-view-get-view-details.php?form_id='+ form_id)
	}

	this.setStarred = function(form_id, data_id, table_name, data_starred){
		d = {
				action:"updateData",
				data_starred:data_starred, //Current Stared
				tablename:table_name,	//
	            form_id:form_id,
				data_id:data_id
			}

		return $http.post('/modules/dashboard_data/starred.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}

	this.setCustomView = function(form_id, customView){
		var customV = []
		if(customView.length>0){
			angular.forEach(customView, function(value, key){
				if(key!=0){
					var header = {};
					header.field_name = value.mData;
					header.field_label = value.sTitle;
					header.width = value.nTh.style.width;
					customV.push(header);
				}
			});
		}

		d = {
				action:"saveCustomView",
	            form_id:form_id,
				customView:JSON.stringify(customV)
			}

		return $http.post('/modules/dashboard_data/search.php',
			serialize(d),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		)
	}
})

// dbModels.service('DashboardTemplateService', function($http, $q){
// 	var $forms = [];
// 	this.getAllFormaForms = function(){
// 		//return $http.get('/modules/dashboard_data/get-all-forma-forms.php');
// 		if($forms.length==0){
		
// 			console.log('not cached')
// 			return $http.get('/modules/dashboard_data/get-all-forma-forms.php').then(function(data){
// 				$forms = data.data;	
// 				return data.data
// 			});

// 		}else{
// 			console.log("cached");
// 			var dfd = $q.defer()
// 			dfd.resolve($forms);
// 			return dfd.promise;
			

// 		}
		
// 	}
// })


	

