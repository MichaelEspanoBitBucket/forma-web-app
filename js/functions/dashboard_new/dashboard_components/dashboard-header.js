function DashboardHeaderController (UserPreferencesService,$scope,$location){
	UserPreferencesService.getPreferences.then(function(data){
		$scope.company_info = data.CompanyInfo
		$scope.current_user = data.Current_User
	},function(error){
		console.log(error)
		//$location.path('/login')
	})
}


angular.module("dashboard.header",[])
.controller('DashboardHeaderController',['UserPreferencesService','$scope','$location',DashboardHeaderController])