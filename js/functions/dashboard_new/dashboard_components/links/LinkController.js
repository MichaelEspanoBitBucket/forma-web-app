
function LinkController(LinksService,$q){
	console.log("LinkController")
	var vm = this
	
	vm.loading = true;
	vm.linkCount = 0

	$q.when(LinksService.getLinks(),function(data){
		vm.links = data
	},function(rejection){
		vm.links = []
	}).finally(function(){
		console.log(vm.links)
		vm.linkCount = vm.links.length
		vm.loading = false;
	})

}


angular.module("dashboard.header")
.controller("LinkController",['LinksService','$q',LinkController])