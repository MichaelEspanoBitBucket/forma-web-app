

function LinksService($http,$q){

	var url = "/modules/dashboard_data/link_maintenance.php"
	var links = []

	this.clearLinks = function(){
		links = []
	}
	this.getLinks = function(){


		if (links.length > 0)
		{
			return links
		}

		return $http.get(url).then(function(result){
			links = []
			for (var i in result.data){
				links.push(result.data[i])
			}
			return links
		},function(error){
			return $q.reject(links)
		})
	}

}


angular.module("dashboard.header")
.service("LinksService",['$http','$q',LinksService])