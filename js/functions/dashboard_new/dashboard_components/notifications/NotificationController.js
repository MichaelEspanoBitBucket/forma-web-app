
function NotificationController(NotificationService,$q){
	
	var vm = this
	
	vm.loading = true;
	vm.notifCount = 0

	$q.when(NotificationService.getNotifications(),function(data){
		vm.notifs = data
		console.log(vm.notifs)
	},function(rejection){
		vm.notifs = []
	}).finally(function(){
		vm.notifCount = vm.notifs.length
		vm.loading = false;
		NotificationService.tagNotificationsAsRead()
	})

}


angular.module("dashboard.header")
.controller("NotificationController",['NotificationService','$q',NotificationController])