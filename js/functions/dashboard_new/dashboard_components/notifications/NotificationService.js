function NotificationService($http,$q){

	var url = "/modules/dashboard_data/notification.php"
	var notifs = []

	this.getNotifications = function(){

		var data = {
			action : 'notificationPost',
			count_notif : 0,
			limit : 10
		}

		var config = {
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded"
				}
		}

		return $http.post(url,serialize(data),config).then(function(result){
			notifs = []
			var notifications = result.data[0].notification

			for (var i in notifications){
				if (i != "totalCount"){
					if (notifications[i]){
						for (var j in notifications[i]){
							notifications[i][j].notifType = i
							notifs.push(notifications[i][j])
						}
					}
				}
			}
			console.log(notifs)
			return notifs
			
		},function(error){
			return $q.reject(notifs)
		})
	}

	this.tagNotificationsAsRead = function(){
		var data = {
			action : 'updateisRead'
		}

		var config = {
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded"
				}
		}

		return $http.post(url,serialize(data),config).then(function(result){
			return true
		},function(error){
			return $q.reject(false)
		})
	}

}

angular.module("dashboard.header")
.service("NotificationService",['$http','$q',NotificationService])