
function DashboardTemplateService ($http,$q){

	this.save = function (templateName,users,groups,layout){

		var data = {
			templateName : templateName,
			users : users,
			groups : groups,
			layout : layout
		}

		console.log(data)

		var url = '/modules/dashboard_data/create-dashboard-user-preferences-default.php'

		 return $http.post(url,serialize(data), { 
			headers: {
				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
			}
		})
	}

	this.getTemplates= function(user_id,group_id){

		var params = {
				user_id : user_id,
				group_id : group_id
		}


		var url = '/modules/dashboard_data/get-dashboard-user-preferences-default.php'

		 return $http.get(url,{params : params}, { 
			headers: {
				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
			}
		}).then(function(response){
			console.log(response.data)
			return response.data
		})
	}

	var $forms = [];
	this.getAllFormaForms = function(){
		//return $http.get('/modules/dashboard_data/get-all-forma-forms.php');
		if($forms.length==0){
		
			console.log('not cached')
			return $http.get('/modules/dashboard_data/get-all-forma-forms.php').then(function(data){
				$forms = data.data;	
				return data.data
			});

		}else{
			console.log("cached");
			var dfd = $q.defer()
			dfd.resolve($forms);
			return dfd.promise;
			

		}
		
	}

}

angular.module("myApp")
.service("DashboardTemplateService",['$http','$q',DashboardTemplateService])