
function SaveDashboardController ($scope,$uibModalInstance,CollabService,$filter,DashboardTemplateService){

	console.log($scope)

	var groupsPromise = CollabService.loadGroups().then(function(data){
		
		console.log(data)
		return data;
	})

	$scope.getUsersAndGroups = function($query){
		return CollabService.loadNames($query).then(function(names){
			return names
		}).then(function(names){
			return groupsPromise.then(function(groups){
				var groupTemp = $filter('filter')(groups, $query, false)
				return names.concat(groupTemp)
			})
		})
	}

	$scope.getUsers = function($query){
		return CollabService.loadNames($query).then(function(names){
			return names
		})
	}

	$scope.getGroups = function($query){
		return groupsPromise.then(function(groups){
				var groupTemp = $filter('filter')(groups, $query, false)
				console.log(groupTemp)
				return groupTemp
		})
	}

	$scope.saveTemplate = function(){

		return DashboardTemplateService.save($scope.templateName,$scope.users,$scope.groups,$scope.layout)

	}

	$scope.getAllTemplates = function(){
		return DashboardTemplateService.getTemplates()
	}

	$scope.getTemplates = function($user_id,$group_id){
		return DashboardTemplateService.getTemplates($user_id,$group_id).then(function(data){
			return data
		})
	}


	$scope.cancel = function () {
    	$uibModalInstance.dismiss();
  };

}




angular.module("myApp")
.controller("SaveDashboardController",['$scope','$uibModalInstance','CollabService','$filter','DashboardTemplateService',SaveDashboardController])