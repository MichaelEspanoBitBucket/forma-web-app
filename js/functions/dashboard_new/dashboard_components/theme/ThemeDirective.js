

function ThemeController(ThemeService,$scope){
	ThemeService.getTheme().then(function(data){
		$scope.theme = data
	})	
}

function ThemeDirective (){
	return {
		restrict : 'E',
		scope : {},
		controller : 'ThemeController',
		template : '<style> .navbar-inverse{ ' + 
					'background-color : {{theme.light_color}} ;' +
					'}</style>'
	};
}


angular.module('dashboard.header')
.controller('ThemeController',['ThemeService','$scope',ThemeController])
.directive('themeStyle',[ThemeDirective])