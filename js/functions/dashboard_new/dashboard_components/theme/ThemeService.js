
function ThemeService($http,$q){
	this.getTheme = function (){
		return $http.get('/modules/dashboard_data/get-dashboard-user-theme.php').then(function(result){
			console.log(result.data)
			if (result.data.length != 0)
			{

				return angular.fromJson(result.data[0]['theme'])
			}
			else
			{
				return  {
							light_color: '#7f8c8d',
							dark_color: '#7f8c8d'
						}   
			}

		},function(error){
			return $q.reject(error)
		})
	}
}

angular.module('dashboard.header')
.service("ThemeService",['$http','$q',ThemeService])