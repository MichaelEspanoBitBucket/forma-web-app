
var utilityModule = angular.module("UtilityModule",[])

utilityModule.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $rootScope) {
        if ($rootScope.activeCalls == undefined) {
            $rootScope.activeCalls = 0;
        }

        return {
            request: function (config) {
                // console.log("halo")
                // console.log(config)
                $rootScope.activeCalls += 1;
                return config;
            },
            requestError: function (rejection) {
                $rootScope.activeCalls -= 1;
                return rejection;
            },
            response: function (response) {
                $rootScope.activeCalls -= 1;
                return response;
            },
            responseError: function (rejection) {
                $rootScope.activeCalls -= 1;
                return rejection;
            }
        };
    });



}]);

utilityModule.directive('loading', function ($http,$rootScope) {
    return {
        restrict: 'A',
        replace: true,
        template: '<h5 class="text-muted"> &nbsp Loading...</h5>',
        link: function (scope, element, attrs) {

            scope.activeCalls = $rootScope.activeCalls
            scope.$watch('activeCalls', function (newVal, oldVal) {
                console.log(newVal)
                console.log(oldVal)
                if (newVal == 0) {
                    $(element).hide();
                }
                else {
                    $(element).show();
                }
            });
        }
    };
});

utilityModule.service("EncryptionService",function(){
	 this.encrypt = function(string){
            return btoa(btoa(btoa(btoa(string))))
      }

     this.decrypt = function(encryptedString){
            return btoa(btoa(btoa(btoa(encryptedString))))
      }
});

utilityModule.service("SerializationService",function(){
     this.serialize = function serialize(obj, prefix) {
        var str = [];
        for (var p in obj) {
          if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + "[" + p + "]" : p,
                  v = obj[p];
              str.push(typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
          }
        }
        return str.join("&");
    }
});








