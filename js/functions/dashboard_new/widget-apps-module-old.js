 
 /*
    author : Edgar Palmon
    date : March 9, 2016
    module description : module for the application widget
    module dependencies : UtilityModule -> found in utilities-module.js. 
                                        -> contains the loading directive and EncryptionService
                         DashboardServices -> found in dashboard-models.js. 
                                        -> contains UserPreferencesService
 */



 var module = angular.module("WidgetAppsModule",["UtilityModule","as.sortable","DashboardServices"]) 

    module.service("WidgetAppsService", function($http,EncryptionService,UserPreferencesService){
        
        var url = "/modules/dashboard_data/form_selection_view.php"
        
        // sets the response data

        this.getFormSelectionViewResponse = null

        this.setResponsePromise = function(){

            if (!this.getFormSelectionViewResponse)
            {
                this.getFormSelectionViewResponse = $http.get(url)
            }
        }

        // gives the list of all categories together with child forms
         this.getCategoricalData =  function () {

            this.setResponsePromise()
             return this.getFormSelectionViewResponse.then(function(response){
                return response.data
            }, function(error){
                return error
            })
        }

        /*
            gives the list of all forms 
            parameters : withEncryptedId -> a boolean value that indicates whether to add encrypted id's in each forms.
        */
        this.getFlatData =  function (withEncryptedId) {

            this.setResponsePromise()
             return this.getFormSelectionViewResponse.then(function(response){
                
                var flatData = []
                for(var category in response.data)
                {
                    for (var i=0 ; i < response.data[category].length ; i++ )
                    {
                        
                        if (withEncryptedId)
                        {
                            response.data[category][i].form_id_encrypted = EncryptionService.encrypt(response.data[category][i].form_id)
                            flatData.push(response.data[category][i])
                        }
                        else
                        {
                            flatData.push(response.data[category][i])
                        }
                    }
                }
                return flatData

            }, function(error){
                return error
            })
        }

         /*
            gives the list of all forms given the categoryName
            parameters : categoryName -> a string value to specify the categoryName   
                        withEncryptedId -> a boolean value that indicates whether to add encrypted id's in each forms.
        */
        this.getFormsByCategory =  function (categoryName,withEncryptedId) {
            this.setResponsePromise()
             return this.getFormSelectionViewResponse.then(function(response){
                
                var forms = []

                if (responseData.hasOwnProperty(categoryName))
                {
                    for (var i=0 ; i < response.data[categoryName].length ; i++ )
                    {
                         
                        if (withEncryptedId)
                        {
                            response.data[categoryName][i].form_id_encrypted = EncryptionService.encrypt(response.data[categoryName][i].form_id)
                            flatData.push(response.data[categoryName][i])
                        }
                        else
                        {
                            flatData.push(response.data[categoryName][i])
                        }

                    }
                }
                return forms

            }, function(error){
                return error
            })
        }
         /*
            gets all forms currently shown to the user merged with all the existing forms in the Forma Tables. If a previously present form is now not accessible, the form shall not be shown anymore.
            parameters : isUserPreferencePresent -> a boolean value that indicates whether to add another property to each forms signifying its existence in the user's preference data.
                        currentApps -> an array value containing the currently presented forms
        */  
        this.getAllForms = function (isUserPreferencePresent,currentApps){
            return this.getFlatData(true).then(function(flatData){

                    var allData = []
                    var preferenceFound = false

                    if (currentApps)
                    {
    
                        //loop first through all preferenceData
                         for (var j = 0 ; j < currentApps.length ; j ++)
                         {
                            for (var i = 0 ; i < flatData.length ; i++)
                            {
                                //add if found in flatData add
                                if (flatData[i].form_id == currentApps[j].form_id)
                                {
                                     //set visibility as true
                                    currentApps[j].user_preference_visible = true;
                                    allData.push(currentApps[j]);
                                    break;
                                }
                            }
                           
                        }    
                    }

                    if (flatData)
                    {
                         //loop through the flatData
                        for (var i = 0 ; i < flatData.length ; i++)
                        {
                           
                            preferenceFound = false
                            //check if already in preferences
                            if (currentApps)
                            {
                                for (var j = 0 ; j < currentApps.length ; j ++)
                                {
                                    if (flatData[i].form_id == currentApps[j].form_id)
                                    {
                                       
                                        preferenceFound = true
                                        break;
                                    }
                                   
                                }
                            }
                             //add if not found in preference
                            if(!preferenceFound)
                            {
                                 //set visibility as false
                                flatData[i].user_preference_visible = false;
                                allData.push(flatData[i]);
                            }
                                      
                        } 
                    }
                    console.log(allData)
                    return allData
              

            },function(error){

            })
        }

        /*
            Initially returns a promise that if fulfilled, shall return the user preferences data
            preferenceData =  [
                {
                    id : "appId" ,
                    name : "appName" ,
                    apps : ["appObjects"]
                }, .... , lastObject
            ]             
        */  
        this.getUserPreferences = function(){

            return UserPreferencesService.getPreferences.then(function(preferenceData){

                if (preferenceData.Apps)
                {

                     return preferenceData.Apps 
                }
                else
                {
                    return null
                }
            },function(error)
            {

            })
        } 


        /*
            saves the added forms in the user preference table.
            parameters : appData -> an array value containing the forms tagged as visible
                        id -> the id of the app set needed.
        */  

        this.saveAppsPreferences = function(appData,name,id)
        {
            var appDataToSave = []
            for (var i = 0 ; i < appData.apps.length ; i++)
            {
                if (appData[i].user_preference_visible == true)
                {
                    appDataToSave.push(appData[i])
                }
               
            }

            // return  this.getUserPreferences().then(function(preferenceData){

            //     if (preferenceData)
            //         {
            //             for (var i = 0 ; i < preferenceData.length ; i++)
            //             {
            //                 if (preferenceData[i].appId == scope.appId)
            //                 {
            //                      preferenceData[i].apps = appDataToSave
            //                      preferenceData[i].name = 
            //                     break;
            //                 }
            //             }  
            //         }

                return UserPreferencesService.saveApps(appDataToSave).then(function(response){

                    return appDataToSave
                },function(error)
                {
                    
                })

            // },function(error){

            // })

             
        }
    
    })

    module.directive('widgetApps', function(WidgetAppsService) {
        return {
            restrict: 'E',
            templateUrl: '/modules/dashboard/templates/widget-apps.html',
            scope : {
                appId : "@",
            },
            link:function(scope, element, attrs){
                 
                 // create widgetApps Object
                scope.widgetApps = {}
                var previousApps = []
                var previousName = ""
                var widgetApps = scope.widgetApps
                
                console.log(scope.appId)

                //set SortableOptions
                widgetApps.isSortable = false;
                widgetApps.sortableOptions = {
                    containment: '#sortable-container',
                    containerPositioning : "relative"
                }

                /*
                    initially load all Forms found in User Preference Table
                */
                WidgetAppsService.getUserPreferences().then(function(preferenceData){

                    if (preferenceData)
                    {
                        for (var i = 0 ; i < preferenceData.length ; i++)
                        {
                            if (preferenceData[i].appId == scope.appId)
                            {
                                widgetApps.data = preferenceData[i].apps
                                widgetApps.name = preferenceData[i].name

                                break;
                            }
                        }  
                    }

                 },function(error){

                 })

                /* 
                    toggles apps widget mode
                    parameters : save -> boolean whether to save the preference to DB
                */
                widgetApps.toggle = function(save)
                {
                    widgetApps.isSortable = !widgetApps.isSortable
                    if (widgetApps.isSortable)
                    {
                        previousApps = widgetApps.data
                        previousName = widgetApps.name
                        WidgetAppsService.getAllForms(true,widgetApps.data).then(function(forms){
                            widgetApps.data = forms
                        },function(error){

                        })
                    }
                    else
                    {
                        if (save)
                        {
                            console.log(widgetApps)
                            WidgetAppsService.saveAppsPreferences(widgetApps,scope.appId).then(function(data){
                                widgetApps.data = data
                            },function(error){

                            })
                        }
                        else
                        {
                            widgetApps.data = previousApps
                            widgetApps.data = previousName
                        }

                    }
                }

                widgetApps.addToPreferences = function(index)
                {
                    widgetApps.data[index].user_preference_visible = true
                }

                widgetApps.removeFromPreferences = function(index)
                {
                    widgetApps.data[index].user_preference_visible = false
                }
            }
        };
    });



    //  module.controller("WidgetAppsController",function($scope,WidgetAppsService){
    //     // create widgetApps Object
    //     $scope.widgetApps = {}
    //     var previousApps = []
    //     var widgetApps = $scope.widgetApps
        

    //     //set SortableOptions
    //     widgetApps.isSortable = false;
    //      widgetApps.sortableOptions = {
    //         containment: '#sortable-container',
    //         containerPositioning : "relative"

    //     }

    //     /*
    //         initially load all Forms found in User Preference Table
    //     */

    //     WidgetAppsService.UserPreferencesForms().then(function(preferenceData){
    //         widgetApps.data = preferenceData
    //      },function(error){

    //      })

    //     /* 
    //         toggles apps widget mode
    //         parameters : save -> boolean whether to save the preference to DB
    //     */
    //     widgetApps.toggle = function(save)
    //     {
    //         widgetApps.isSortable = !widgetApps.isSortable
    //         if (widgetApps.isSortable)
    //         {
    //             previousApps = widgetApps.data
    //             WidgetAppsService.getAllForms(true,widgetApps.data).then(function(forms){
    //                 widgetApps.data = forms
    //             },function(error){

    //             })
    //         }
    //         else
    //         {
    //             if (save)
    //             {
    //                 WidgetAppsService.saveAppsPreferences(widgetApps.data).then(function(data){
    //                     widgetApps.data = data
    //                 },function(error){

    //                 })
    //             }
    //             else
    //             {
    //                 widgetApps.data = previousApps
    //             }

    //         }
    //     }

       

    //     widgetApps.addToPreferences = function(index)
    //     {
    //         widgetApps.data[index].user_preference_visible = true
    //     }

    //     widgetApps.removeFromPreferences = function(index)
    //      {
    //         widgetApps.data[index].user_preference_visible = false
    //      }
       
    // })


// module.directive('widgetApps', function() {
//   return {
//     restrict: 'E',
//     templateUrl: '/modules/dashboard/templates/widget-apps.html',
//     controller:"WidgetAppsController",
//     link:function(scope, element, attrs){
//        $(element).find(".scrollable").perfectScrollbar();
//     }
//   };
// });
    
