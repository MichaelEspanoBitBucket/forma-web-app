 /*
    author : Edgar Palmon
    date : March 9, 2016
    module description : module for the application widget
    module dependencies : UtilityModule -> found in utilities-module.js. 
                                        -> contains the loading directive and EncryptionService
                         DashboardServices -> found in dashboard-models.js. 
                                        -> contains UserPreferencesService
 */

var module = angular.module("WidgetAppsModule",["UtilityModule","as.sortable","DashboardServices"]) 

module.service("WidgetAppsService",['$http','EncryptionService','UserPreferencesService',function($http,EncryptionService,UserPreferencesService){

	var url = "/modules/dashboard_data/form_selection_view.php"
	var promise;

	this.getHierarchialForms = function (){
        if(!promise){
             promise = $http.get(url)
        }
       
		return promise.then(function(response){
			return response.data
		},function(error){
			return error
		})
	}

	this.getFlattenedForms = function (){
        if(!promise){
             promise = $http.get(url)
        }
		return promise.then(function(response){
            var flattenedForms = []
            for(var category in response.data)
            {
                for (var i=0 ; i < response.data[category].length ; i++ )
                {
                	 response.data[category][i].form_id_encrypted = EncryptionService.encrypt(response.data[category][i].form_id)
                     flattenedForms.push(response.data[category][i])
                }
            }
            return flattenedForms
		},function(error){
			return error
		})
	}

	this.getAllFlattenedFormsWithUserPreferencesTag = function (currentForms){
		return this.getFlattenedForms().then(function(flattenedForms){
			var allForms = []
            var preferenceFound = false

            if (currentForms)
            {
                //loop first through all preferenceData
                 for (var j = 0 ; j < currentForms.length ; j ++)
                 {
                    for (var i = 0 ; i < flattenedForms.length ; i++)
                    {
                        //add if found in flatData add
                        if (flattenedForms[i].form_id == currentForms[j].form_id)
                        {
                             //set visibility as true
                            var form = {}
                            form.user_preference_visible = true;
                            form.form_name = flattenedForms[i].form_name
                            form.form_id = flattenedForms[i].form_id
                            form.form_id_encrypted = flattenedForms[i].form_id_encrypted
                            form.portal_icon = flattenedForms[i].portal_icon
                            allForms.push(form);
                            break;
                        }
                    }
                   
                }    
            }

            if (flattenedForms)
            {
                 //loop through the flatData
                for (var i = 0 ; i < flattenedForms.length ; i++)
                {
                   
                    preferenceFound = false
                    //check if already in preferences
                    if (currentForms)
                    {
                        for (var j = 0 ; j < currentForms.length ; j ++)
                        {
                            if (flattenedForms[i].form_id == currentForms[j].form_id)
                            {
                               
                                preferenceFound = true
                                break;
                            }
                           
                        }
                    }
                     //add if not found in preference
                    if(!preferenceFound)
                    {
                         //set visibility as false
                        var form = {}
	                    form.user_preference_visible = false;
	                    form.form_name = flattenedForms[i].form_name
	                    form.form_id = flattenedForms[i].form_id
	                    form.form_id_encrypted = flattenedForms[i].form_id_encrypted
	                    form.portal_icon = flattenedForms[i].portal_icon
	                    allForms.push(form);
                    }
                              
                } 
            }
            return allForms

		},function(error){

		})
	}


	this.getUserPreferences = function(){
		return UserPreferencesService.getPreferences.then(function(preferenceData){

                if (preferenceData.Apps)
                {

                     return preferenceData.Apps 
                }
                else
                {
                    return null
                }
        },function(error)
        {

        })
	}

	this.getUserPreferencesById = function(id){
		return this.getUserPreferences().then(function(preferences){
            //console.log(preferences)
            
			for (var i = 0 ; i < preferences.length ; i++)
			{
				if (preferences[i].id == id)
				{
					return preferences[i]
				}
			}
			return null
		},function(error){

		})
	}

	this.saveUserPreferencesById = function( preference ,id){

		
		return this.getUserPreferences().then(function(preferences){

			//filter preference
			var visible_apps = []
			var preferenceExisting = false

			for (var i = 0 ; i < preference.data.length ; i++)
			{
				if (preference.data[i].user_preference_visible == true)
				{
					visible_apps.push(preference.data[i])
				}
			}

			for (var i = 0 ; i < preferences.length ; i++)
			{
				if (preferences[i].id == id)
				{
					preferences[i].apps = visible_apps
					preferences[i].name = preference.name
					preferenceExisting = true
					break;
				}
			}

			if (!preferenceExisting){
				preferences.push({apps:visible_apps,name:preference.name,id:id})
			}
			
			console.log(preferences)

			return UserPreferencesService.saveApps(preferences).then(function(response){
				return visible_apps
			},function(error)
			{

			})

		},function(error){

		})	
	}
}])

module.directive('widgetApps', function(WidgetAppsService) {
    return {
        restrict: 'E',
        templateUrl: '/modules/dashboard/templates/widget-apps.html',
        scope : {
            appId : "@",
            removeWidget:"&"
        },
        link:function(scope, element, attrs){

        	$(element).find(".scrollable").perfectScrollbar();
        	scope.widgetApps = {}
            var previousApps = []
            var previousName = ""
            var widgetApps = scope.widgetApps
            
          

            //set SortableOptions
            widgetApps.isSortable = false;
            widgetApps.sortableOptions = {
                containment: '#sortable-container',
                containerPositioning : "relative"
            }


             /*
               preference contains apps, name, id
            */
            WidgetAppsService.getUserPreferencesById(scope.appId).then(function(preference){

            	if (preference){
            		widgetApps.data = preference.apps
            		widgetApps.name = preference.name
            	}
            	else{
            		widgetApps.data = []
            		widgetApps.name = ""

            	}


            	
            },function(error){

            })

            /* 
                toggles apps widget mode
                parameters : save -> boolean whether to save the preference to DB
            */

            widgetApps.toggle = function(save)
            {
                widgetApps.isSortable = !widgetApps.isSortable
                if (widgetApps.isSortable)
                {
                    previousApps = widgetApps.data
                    previousName = widgetApps.name
                    WidgetAppsService.getAllFlattenedFormsWithUserPreferencesTag(widgetApps.data).then(function(forms){
                        widgetApps.data = forms
                    },function(error){

                    })
                   
                }
                else
                {
                    if (save)
                    {
                        WidgetAppsService.saveUserPreferencesById(widgetApps,scope.appId).then(function(data){
                            widgetApps.data = data
                        },function(error){

                        })
                    }
                    else
                    {
                        widgetApps.data = previousApps
                        widgetApps.name = previousName
                    }

                }
            }

            widgetApps.addToPreferences = function(index)
            {
                widgetApps.data[index].user_preference_visible = true
            }

            widgetApps.removeFromPreferences = function(index)
            {
                widgetApps.data[index].user_preference_visible = false
            }
        }
         
    }
    
})