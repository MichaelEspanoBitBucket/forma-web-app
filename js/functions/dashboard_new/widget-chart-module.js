/*
  WidgetView Directive
  Created by Mark Rowi T. Dizon
  Dependencies : chart.js
    
*/
var dApp = angular.module("WidgetChartModule", ['chart.js'])
//Loading of view
dApp.config(['ChartJsProvider', function (ChartJsProvider) {
  ChartJsProvider.setOptions({ colors : [ '#ffdc7a', '#cccccc', '#95d9f0', '#7a98bf', '#f17979', '#4D5360', '#949FB1'] });
}])

dApp.controller('WidgetChartController', WidgetChartController);

function WidgetChartController($scope){
 $scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales", "Tele Sales", "Corporate Sales"];
  $scope.data = [300, 500, 100, 40, 120];
    $scope.options = {
      responsive: true
    }
    $scope.colors = [ '#ffdc7a', '#cccccc', '#95d9f0', '#7a98bf', '#f17979', '#4D5360', '#949FB1'];
    $scope.title = "Chartssss";


}



//Directive of widget view
dApp.directive("widgetChart", function(){
	return {
    restrict:'E',
    scope:{
      parent:"=",
      removeWidget:"&"
    },
    templateUrl: '/modules/dashboard/templates/widget-chart.html',
    controller:"WidgetChartController",
    controllerAs:"chart",
    bindToController:true,
    link:function(scope, element, attrs){
     
    
    }
  };
})

