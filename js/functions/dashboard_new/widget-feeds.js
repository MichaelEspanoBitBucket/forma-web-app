
var dApp = angular.module("WidgetFeedsModule", ['ngtimeago', 'infinite-scroll', 'DashboardServices','ngSanitize','ngAnimate','ui.bootstrap','ngTagsInput']);

dApp.controller('WidgetFeedsController', function($scope, Posts,FeedsService){
var VisibilityType = {
        PUBLIC: '1',
        COMPANY: '2',
        CUSTOM: '3'
    };

    var PostType = {
        ANNOUNCEMENT: 1,
        SURVEY: 2
    };

	$scope.isCreatingPost = false;
  $scope.isCreatingPostBusy = false;
  $scope.posts = new Posts();
  $scope.postType = 'Announcement';
  $scope.postVisibilityType = VisibilityType.COMPANY;
  $scope.viewType = 'Feeds';
  $scope.PostCommentViewer = [];
 $scope.tempMoods=[];

 


//   $scope.tempMoods = [{mood: "Inspired", user_id: "422", display_name: "Liza Soberano",image_url
// :
// "/images/formalistics/tbuser/296aa6a1bc72e3c85f51ea2e0f05f03d./small_296aa6a1bc72e3c85f51ea2e0f05f03d.png"},
// {mood: "Happy", user_id: "422", display_name: "Liza Soberano",image_url
// :
// "/images/formalistics/tbuser/296aa6a1bc72e3c85f51ea2e0f05f03d./small_296aa6a1bc72e3c85f51ea2e0f05f03d.png"},
// {mood: "Inspired", user_id: "422", display_name: "Liza Soberano",image_url
// :
// "/images/formalistics/tbuser/296aa6a1bc72e3c85f51ea2e0f05f03d./small_296aa6a1bc72e3c85f51ea2e0f05f03d.png"}]

  $scope.postSurveyResponses = [{id:null, text:""},{id:null, text:""}]


  $scope.refresh = function(){
    $scope.posts.reload().then(function(){
      angular.element('#widget-feed-container').animate({ scrollTop: 0 }, 'fast');
      console.log($scope.posts.posts)
    });
    
  }


  $scope.editPost = function(post){
    $scope.feedBodyText = post.announcement;
    console.log(post)
  }


  $scope.addNewResponse = function(isLast){
    if(isLast && $scope.postSurveyResponses.length < 10){
      $scope.postSurveyResponses.push({id:null, text:""})
    }
  }
  $scope.isCreatingPostF = function(val){
    $scope.isCreatingPost = val
    if(val){
      angular.element(".feed-body-text").focus();
    }else{
      angular.element(".feed-body-text").empty();
      angular.element(".feed-body-images").empty();
      $scope.postSurveyResponses = [{id:null, text:""},{id:null, text:""}];
      $scope.isCreatingPostBusy = false;
      $scope.followers = [];
      $scope.postVisibilityType = '2'
    }
  };


  //View Controller to Comment or View to Moods :)

  $scope.commentsPreview = function(post, count){
    if(count==undefined)(count=0)
    //$scope.PostCommentViewer = [];
    $scope.PostCommentViewer = [post];
    $scope.posts.fetchPostComment($scope.PostCommentViewer[0],count).then(function(){
       // angular.element('.feed-comment-create-text').focus();

    })
    $scope.viewType = 'Comments';
    angular.element('#widget-feed-container-comment-viewer').animate({ scrollTop: 0 }, 'fast');

  
  }



  $scope.moodsPreview = function(node){
    $scope.viewType = 'Moods';
    $scope.tempMoods=[];
    params = {
      post_id:(node.post_id==undefined?node.id:node.post_id),
      comment_id:(node.post_id==undefined?0:node.id)
    }
    FeedsService.getUserMoods(params).then(function(res){

      $scope.tempMoods = res.data.results; 
    });
  }

  $scope.imagesPreview = function(post){

  }
  


  $scope.getMoodIcon = function(mood){
    switch(mood){
      case "Happy" : return 'svg-icon-mood-happy-v2';
        break;
      case "Inspired" : return 'svg-icon-mood-inspired-v2';
        break;
      case "Sad" : return 'svg-icon-mood-sad-v2';
        break;
      case "Angry" : return 'svg-icon-mood-angry-v2';
        break;
      case "Annoyed" : return 'svg-icon-mood-annoyed-v2';
        break;
      case "Don't Care" : return 'svg-icon-mood-dont-care-v2';
        break;
      default : return 'svg-icon-mood-happy-v2';
        break;
    }
  }

   $scope.getMoodCount = function(node){
    moodCount = 0;
    moods = ['happy_users_count', 'inspired_users_count', 'sad_users_count', 'angry_users_count', 'annoyed_users_count', 'dont_care_users_count'];
    for (var i = moods.length - 1; i >= 0; i--) {
      if(node[moods[i]] != undefined && node[moods[i]] != null){
        moodCount+=parseInt(node[moods[i]]);
      }
    };
    ///console.log(node.user_mood)
    if (node.user_mood!=null) {
      moodCount++;
    };

      // console.log(node)
      // console.log(moodCount)
    return moodCount;
  }

  $scope.removePost = function(post){
    post.busy = true;
    FeedsService.removePost(post.id).then(function(){
      $scope.refresh()
    })
  }

  $scope.Post = function(){
  //  alert(angular.element('#feedBodytext').html())feed-body-images
    var $bodyText = angular.element('#feedBodytext'),
        $bodyImages = angular.element('#feed-body-images img')
        $scope.isCreatingPostBusy = true;
    
    if($bodyText.html() != ""){
      var FeedContent = $bodyText.clone();
      for (var i = $bodyImages.length - 1; i >= 0; i--) {
        FeedContent.append($bodyImages[i]);
      };
      //FeedContent.append($bodyImages);
      if ($scope.postType=="Announcement") {
        FeedsService.saveAnnouncement(FeedContent.html(), $scope.postVisibilityType, 0, $scope.followers).then(function(){
            $scope.refresh();
            $scope.isCreatingPostF(false)
            
        })
      }else{
        var responses = $($scope.postSurveyResponses).map(function(){
          return $.trim(this.text) != ""? this : null
        }).get()

        FeedsService.saveSurvey(FeedContent.html(), $scope.postVisibilityType, 0, responses, ($scope.multipleResponse?1:0), 1, $scope.followers).then(function(){
           $scope.refresh();
           
          $scope.isCreatingPostF(false)
        })
      }
     
    }else{
      alert("Do not leave empty fields")
    }
   
  }
//Multiple
//   survey_id:76
// survey_responses[]:40
// survey_responses[]:41

//Single
// survey_id:69
// survey_responses[]:39
  $scope.submitSurvey = function(post){
    params = {
      survey_id:post.id,
      survey_responses:[]
    }
    post.survey_results = [];
    post.busy = true;

    if(post.enable_multiple_responses==0){
      if(post.Response!=undefined){
        params.survey_responses.push(post.Response)
      }
      
    }else{

      angular.forEach(post.survey_selectable_responses, function(val, index){
        if(val.ischecked){
          params.survey_responses.push(val.id)
        }
      })
    }

    if(params.survey_responses.length>0){
      
      FeedsService.saveSurveyResponses(params).then(function(){
        post.survey_my_responses = params.survey_responses;
        post.busy = false;
        post.is_survey_edit = false
      })
    }else{
       return;
     
    }



  }

  $scope.loadNames = function($query){
    //  console.log($query)
   return FeedsService.loadNames($query);
  }

  //console.log($scope.loadNames("Edgar").then(function(res){result res.data}));

  $scope.editSurvey = function(post){
    
    post.is_survey_edit = true
    // if(count==undefined)(count=0)
    // $scope.PostCommentViewer = [];
    // $scope.PostCommentViewer.push(post)
    // $scope.posts.fetchPostComment($scope.PostCommentViewer[0],count).then(function(){
    //    // angular.element('.feed-comment-create-text').focus();

    // })
    // $scope.viewType = 'Comments';
    // angular.element('#widget-feed-container-comment-viewer').animate({ scrollTop: 0 }, 'fast');

  
  }




  $scope.moods = [ 
      {'icon':'svg-icon-mood-happy-v2', 'text':'Happy', 'property_count':'happy_users_count', 'about':'You feel happy about this.'}, //svg-icon-mood-happy
      {'icon':'svg-icon-mood-inspired-v2', 'text':'Inspired', 'property_count':'inspired_users_count','about':'You feel inspired about this.'}, //svg-icon-mood-inspired
      {'icon':'svg-icon-mood-sad-v2', 'text':'Sad', 'property_count':'sad_users_count','about':'You feel happy sad this.'}, //svg-icon-mood-sad
      {'icon':'svg-icon-mood-angry-v2', 'text':'Angry', 'property_count':'angry_users_count','about':'You feel angry about this.'}, //svg-icon-mood-angry
      {'icon':'svg-icon-mood-annoyed-v2', 'text':'Annoyed', 'property_count':'annoyed_users_count','about':'You feel annoyed about this.'}, // svg-icon-mood-annoyed
      {'icon':'svg-icon-mood-dont-care-v2', 'text':"Don't Care", 'property_count':'dont_care_users_count','about':"You don't care about this."} //svg-icon-mood-dont-care
    ]

    $scope.setMood =function(mood, post, comment){
      // console.log(post)
      // console.log(comment)
      // console.log(mood)
      
      params = {
        mood:mood.text,
        post_id:post.id,
        comment_id:(comment == undefined?0:comment.id)
      }
      if(comment==undefined){
        post.user_mood = mood.text
        post.busy = true
      }else{
        comment.user_mood = mood.text
        comment.busy = true
      }
      // console.log(params)
      FeedsService.setMood(params).then(
        function(){
          if(comment==undefined){
       
        post.busy = false
      }else{
        comment.busy = false
      }
      }, function(){

      })

    }

})






dApp.controller('widgetFeedsPostController',function($scope,$sce, FeedsService){
  $scope.moodsTemplate = $sce.trustAsHtml('<li data-mood="Happy" data-mood-icon="svg-icon-mood-happy-v2" class="fl-action-your-mood"><svg class="icon-svg icon-svg-announcement svg-icon-mood-happy-v2 tip" viewBox="0 0 100.264 100.597"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-mood-happy-v2"></use></svg><a class="fl-action-mood" data-post-id="69" data-post-icon="svg-icon-mood-happy-v2">Happy</a></li>')
  

  // .checked="{{(post.survey_my_responses.indexOf(resp.id)>=0?resp.ischecked=true:resp.ischecked=false)}}"
  if($scope.post.type == 2 && $scope.post.survey_my_responses!=null){
    
    angular.forEach($scope.post.survey_selectable_responses, function(value, key){
      if($scope.post.survey_my_responses.indexOf(value.id)>=0){
        value.ischecked = true;
      }else{
        value.ischecked = false;
      }
    });
  }
  $scope.createComment = function(comment_text){
    if(comment_text.trim()==""){
      return
    }
   var temp = {"id":null,"author_id":$scope.current_user.id,"date_posted":Date.now(),
          "post_id":$scope.post.id,"text_content":comment_text,
          "author_display_name":$scope.current_user.display_name,
            "author_image_url":$scope.current_user.image_url}
    

    $scope.post.comment_count++;

    $scope.post.comments.unshift(temp)
    $scope.commentText = ""


    FeedsService.saveComment($scope.post.id, comment_text).then(
      function(result){
        //success
       temp.id = result.data.result;
       // console.log(result.data.results)
       // console.log(temp)
       // console.log($scope.post.comments)
        
      },
      function(){
        //fail

    });
  
  }




})

dApp.controller('widgetFeedsMoodsController', function($scope){
  // $scope.getMoodCount = function(){
  //   moodCount = 0;
  //   moods = ['happy_users_count', 'inspired_users_count', 'sad_users_count', 'angry_users_count', 'annoyed_users_count', 'dont_care_users_count'];
  //   for (var i = moods.length - 1; i >= 0; i--) {
  //     if($scope.node[moods[i]] != undefined){
  //       moodCount+=$scope.node[moods[i]];
  //     }
  //   };
  //   if ($scope.node.user_mood!=undefined) {
  //     moodCount++;
  //   };
  //   return moodCount;
  // }
})

dApp.run(function($animate) {
  $animate.enabled(true);
});

// dApp.directive("widgetFeedsMoods", function(){
//   return {
//     restrict:"E",
//     scope:{
//       node:'=',
//       moods:'='
//     },
//     templateUrl:'/modules/dashboard/templates/widget-feeds-moods.html',
//     controller:'widgetFeedsMoodsController',
//     link:function(){
    
//     }
//   }
// })
dApp.controller('widgetFeedsSurveyResultsController', function($scope,FeedsService){
  
  $scope.post.busy = true;
  
  FeedsService.getSurveyResults($scope.post.id).then(function(res){
    totalRes = 0;
    for (var i = res.data.length - 1; i >= 0; i--) {
      totalRes = totalRes + res.data[i].Count;
    };
   
    $scope.post.survey_results_total = totalRes;
    $scope.post.survey_results = res.data;
    $scope.post.busy = false;
  })

  

})
dApp.directive("widgetFeedsSurveyResults", function(){
  return {
    restrict:'A',
    scope:false,
    templateUrl:'/modules/dashboard/templates/widget-feeds-survey-result.html',
    controller:'widgetFeedsSurveyResultsController',
    link:function(){

    }
  }
})

dApp.directive("widgetFeeds", ['$animate', function($animate, $compile){
	return {
    restrict: 'E',
    scope: false,
    templateUrl: '/modules/dashboard/templates/widget-feeds.html',
    controller:"WidgetFeedsController",
    link:function(scope, element, attrs){
       
        element.find(".slideable, .feed-body").perfectScrollbar();
        
        element.on('click','#feed-body-images',function(){
          angular.element('#feedBodytext').focus();
        })
   

    }
  };
}])

dApp.directive('uploadImage', function () {
    return {
      restrict: 'A',
      link: function(scope, element) {
        //console.log("uploadImage")
        window.URL    = window.URL || window.webkitURL;
        var elBrowse  = document.getElementById("image-browse"),
            elPreview = document.getElementById("feed-body-images"),
            useBlob   = false && window.URL; // `true` to use Blob instead of Data-URL

        function readImage (file) {
          var reader = new FileReader();
          reader.addEventListener("load", function () {
            var image  = new Image();
            image.addEventListener("load", function () {
              var imageInfo = file.name    +' '+
                              image.width  +'×'+
                              image.height +' '+
                              file.type    +' '+
                              Math.round(file.size/1024) +'KB';

             // $(this).attr('width', '100%')
              $(this).attr('data-filename', file.name);

              var imgCon = document.createElement('div');
              var closeButton = document.createElement('i');
              closeButton.addEventListener('click', function(){
                imgCon.remove();
              })
              closeButton.className = "fa fa-times-circle"
              if(image.width > image.height){
                imgCon.className = "feed-image-item-container landscape"
              }else{
                imgCon.className = "feed-image-item-container portrait"
              }
              
              imgCon.appendChild(this)
              imgCon.appendChild(closeButton)

              elPreview.appendChild(imgCon);
             
            // var iCount = elPreview.childNodes.length
            
            // if(iCount==1){
            //   this.style = 'width:100%'
            // }else if(iCount == 2){
            //   elPreview.childNodes[0].style = 'width:45%;margin:5px;float:left'
            //   elPreview.childNodes[1].style = 'width:45%;margin:5px;float:left'
            // }else if(iCount== 3){
            //   elPreview.childNodes[0].style = 'width:100%;margin:5px;float:left'
            //   elPreview.childNodes[1].style = 'width:45%;margin:5px;float:left'
            //   elPreview.childNodes[2].style = 'width:45%;margin:5px;float:left'
            // }else if(iCount > 3){
            //   for(i=0; i<=iCount;i++){
            //     elPreview.childNodes[i].style = 'width:45%;margin:5px;float:left'
            //   }
            // }
              //elPreview.insertAdjacentHTML("beforeend", imageInfo);
            });
            image.src = useBlob ? window.URL.createObjectURL(file) : reader.result;
            
            if (useBlob) {
              window.URL.revokeObjectURL(file); // Free memory
            }
          });
          reader.readAsDataURL(file);  
        }

        elBrowse.addEventListener("change", function() {
          var files  = this.files;
          var errors = "";
          if (!files) {
            errors += "File upload not supported by your browser.";
          }
          if (files && files[0]) {
            for(var i=0; i<files.length; i++) {
              var file = files[i];
              if ( (/\.(png|jpeg|jpg|gif)$/i).test(file.name) ) {
                readImage( file ); 
              } else {
                errors += file.name +" Unsupported Image extension\n";  
              }
            }
            
          }
          if (errors) {
            alert(errors); 
          }
        });

        element.bind('click', function(e) {
           // console.log(angular.element(e.target).siblings('#image-browse'))
            element.siblings('#image-browse').trigger('click');
        });
      }
    };
});


dApp.directive('widgetFeedPost', function(){
  return {
    restrict:'A',
    templateUrl:'/modules/dashboard/templates/widget-feeds-post.html',
    controller:'widgetFeedsPostController',
    scope:true,
    // {
    //   post:'=',
    //   posts:'='
    // }
    link:function(){
      
    }
  }
})


dApp.directive("contenteditable", [function() {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ngModel) {

      function read() {
        var html = element.html();
        html = html.replace(/&nbsp;/g, "\u00a0");
        ngModel.$setViewValue(html);
      }

      ngModel.$render = function() {
        element.html(ngModel.$viewValue || "");
      };

      element.bind("blur keyup change", function() {
        scope.$apply(read);
      });
    }
  };
}]);


dApp.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
 
                event.preventDefault();
            }
        });
    };
});


// dApp.directive('widgetFeedComments', function(){
//   return {
//     restrict:'A',
//     templateUrl:'',
//     controller:'',
//     link:function(){

//     }
//   }
// })




