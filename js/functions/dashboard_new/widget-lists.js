 
 /*
    author : Edgar Palmon
    date : March 20, 2016
    module description : module for the application widget
    module dependencies : UtilityModule -> found in utilities-module.js. 
                                        -> contains the loading directive and EncryptionService
                         DashboardServices -> found in dashboard-models.js. 
                                        -> contains UserPreferencesService
 */

var ListService = function ($http,SerializationService){

    this.getList =  function (url){

            var promise = $http.post(url,
                            SerializationService.serialize({
                                action:'loadData',
                                start : 0
                            }),
                            {
                                headers: {
                                    'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
                                }
                            }
                        )

            return promise.then(function(response){
                return response.data
            },function(error){
                return error
            })
        }
 }

var RequestService = function (WidgetListService){
   
   this.promisedRequestCollection = WidgetListService.getList("/ajax/my_request")

}

var StarredService = function ($http, WidgetListService, SerializationService){
   
   this.promisedStarredCollection = function(start){
        return $http.post("/modules/dashboard_data/starred.php",
                            SerializationService.serialize({
                                action:'loadData',
                                start : start
                            }),
                            {
                                headers: {
                                    'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
                                }
                            }
                        )
   }


   //WidgetListService.getList("/ajax/starred")

    this.unmark = function(object)
   {
            var unmarkUrl = "/api/unstar-document"
            return $http.post(unmarkUrl,
                            SerializationService.serialize({
                                form_id: object.ws_id,
                                request_id: object.ID
                            }),
                            {
                                headers: {
                                    'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
                                }
                            }
                        )
   }

}


var PinnedService = function ($http,$filter){
   
   var url = ("/modules/dashboard_data/get-dashboard-user-pinned-objects.php")

   
   this.reportList =  $http.get(url).then(function(response){
            
            var reports = $filter('filter')(response.data, {object_type: "report"}, true)
            console.log(reports)
            reports = $filter('orderBy')(reports,true)
            return reports
        },function(error){

        })
   this.unpin = function(object)
   {
            var unPinUrl = "/ajax/dashboard"
            var params = {
                application_id : "false",
                content : {object_id : 'widget', object_type : 'widget'},
                action : "sorting"
            }

            return $http.get(unPinUrl,{params:params}).then(function(response){
                return response.data
            },function(error){

            })
   }

   

 }




 var listModule = angular.module("WidgetListsModule",["UtilityModule", 'infinite-scroll'])

    listModule.service("WidgetListService",['$http','SerializationService',ListService])
    listModule.service("WidgetRequestService",['WidgetListService',RequestService])
    listModule.service("WidgetStarredService",['$http','WidgetListService','SerializationService',StarredService])
    listModule.service("PinnedService",['$http','$filter',PinnedService])

    listModule.controller("widgetStarredController", ['$scope','WidgetStarredService',function($scope, WidgetStarredService){
                $scope.requests = [];

                $scope.unmark = function(object){
                    WidgetStarredService.unmark(object).then(function(data){
                        console.log(object)
                        var index = $scope.requests.indexOf(object)
                        $scope.requests.splice(index,1)
                    },function(error){

                    })   
                }
                $scope.refresh = function(start){
                    // console.log(start)
                     //$scope.requests = [];
                     if(start == 0){
                        $scope.requests = [];
                     }
                     $scope.busy = true;
                    WidgetStarredService.promisedStarredCollection(start).then(function(data){
                            for (var i = data.data.length - 1; i >= 0; i--) {
                                $scope.requests.push(data.data[i])
                            };
                            // = data.data;
                            $scope.busy = false
                        },function(error){

                        })
                }

                //$scope.refresh(0);

    }])



    listModule.directive("widgetRequests",['WidgetRequestService',function(WidgetRequestService){
        return {
            restrict: 'E',
            templateUrl: '/modules/dashboard/templates/widget-request.html',
            link:function(scope, element, attrs){
                
                
                
                WidgetRequestService.promisedRequestCollection.then (function(data){
                    scope.requests = data
                },function(error){

                })
            }
        }

    }])

    listModule.directive("widgetStarred",['WidgetStarredService',function(WidgetStarredService){
        return {
            restrict: 'E',
            controller:"widgetStarredController",
            controllerAs:"starred",
            templateUrl: '/modules/dashboard/templates/widget-starred.html',
            link:function(scope, element, attrs){
                element.find(".scrollable").perfectScrollbar();
                


            }
        }

    }])


    listModule.directive("widgetPinnedReports",['PinnedService',function(PinnedService){
        return {
            restrict: 'E',
            templateUrl: '/modules/dashboard/templates/widget-pinned-reports.html',
            link:function(scope, element, attrs){
                
                scope.isEditable = false;

                PinnedService.reportList.then (function(reports){
                    scope.reports = reports
                },function(error){

                })

                scope.unpinReport = function(object){
                    PinnedService.unpin(object).then(function(data){
                        var index = scope.reports.indexOf(object)
                        scope.reports.splice(index,1)
                    },function(error){

                    })
                    
                }
            }
        }

    }])
    
   
        
        
      