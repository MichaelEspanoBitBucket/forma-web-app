/*
  WidgetView Directive
  Created by Mark Rowi T. Dizon
  Dependencies : 
    jquery, jquery-datatables
    angular, angular-datatables, angular-datatables-bootstrap
    
*/
var dApp = angular.module("WidgetViewModule", ['ngAnimate','ui.bootstrap', 'datatables','datatables.bootstrap', 'DashboardServices','datatables.buttons','datatables.colreorder']);

//Loading of view
dApp.run(initDT);
function initDT(DTDefaultOptions) {
    DTDefaultOptions.setLoadingTemplate('<center><h5><i class="fa fa-spinner fa-spin"></i> Preparing Widget...</h5></center>');
}

dApp.controller('WidgetViewController', WidgetViewController)

function WidgetViewController($scope,$compile, DTOptionsBuilder, DTColumnBuilder, ViewService){
  var vm = this;
  vm.rowData = {};
  vm.selected = {};
  vm.rowData = {};
  vm.selectAll = false;
  vm.toggleAll = toggleAll;
  vm.toggleOne = toggleOne;
  vm.busy = false;
  vm.showActionButton = false;
  //vm.hasOrganized = false;
  vm.isLoadingWidget = true;
  vm.actionButtons;
  vm.selectedAction = null;
  //remove non processor and non requestor after

var columns = []; 
var titleHtml = '<input type="checkbox" ng-model="view.selectAll" ng-click="view.toggleAll(view.selectAll, view.selected)">';

 vm.dtColumns = ViewService.getViewDetails(vm.parent.object_id).then(function(result){
    vm.vDetails = result.data
    var index = 1
     columns.push(DTColumnBuilder.newColumn(null)
      .withTitle(titleHtml)
      .withOption('width', '85px')
      .renderWith(actionsHtml)
      .notSortable());

      angular.forEach(result.data.custom_view, function(col, key){
         columns.push(DTColumnBuilder.newColumn(col.field_name)
         .withTitle(col.field_label)
         .withOption('width',col.width)
         .withClass('widget-view-tbl-ellipse'))//.renderWith(actionsHtml));//col.field_name
         index++;
      });
      console.log(columns);
      return columns;
  });

 $.fn.dataTable.ext.pager.numbers_length =4;
  $.extend( $.fn.dataTableExt.oPagination, {
      "bootstrap": {
          "fnInit": function( oSettings, nPaging, fnDraw ) {
              var oLang = oSettings.oLanguage.oPaginate;
              var fnClickHandler = function ( e ) {
                  e.preventDefault();
                  if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                      fnDraw( oSettings );
                  }
              };
   
              $(nPaging).addClass('pagination').append(
                  '<ul class="pagination">'+
                      '<li class="prev disabled"><a href="#"><i class="fa fa-chevron-left"></i></a></li>'+
                      '<li class="next disabled"><a href="#"><i class="fa fa-chevron-right"></i></a></li>'+
                  '</ul>'
              );
              var els = $('a', nPaging);
              $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
              $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
             //$(nPaging).parent().addClass('adasdas');
          },
   
          "fnUpdate": function ( oSettings, fnDraw ) {
              var iListLength = 3;
              var oPaging = oSettings.oInstance.fnPagingInfo();
              var an = oSettings.aanFeatures.p;
              var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
   
              if ( oPaging.iTotalPages < iListLength) {
                  iStart = 1;
                  iEnd = oPaging.iTotalPages;
              }
              else if ( oPaging.iPage <= iHalf ) {
                  iStart = 1;
                  iEnd = iListLength;
              } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                  iStart = oPaging.iTotalPages - iListLength + 1;
                  iEnd = oPaging.iTotalPages;
              } else {
                  iStart = oPaging.iPage - iHalf + 1;
                  iEnd = iStart + iListLength - 1;
              }
   
              for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                  // Remove the middle elements
                  $('li:gt(0)', an[i]).filter(':not(:last)').remove();
   
                  // Add the new list items and their event handlers
                  for ( j=iStart ; j<=iEnd ; j++ ) {
                      sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                      $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                          .insertBefore( $('li:last', an[i])[0] )
                          .bind('click', function (e) {
                              e.preventDefault();
                              oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                              fnDraw( oSettings );
                          } );
                  }
   
                  // Add / remove disabled classes from the static elements
                  if ( oPaging.iPage === 0 ) {
                      $('li:first', an[i]).addClass('disabled');
                  } else {
                      $('li:first', an[i]).removeClass('disabled');
                  }
   
                  if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                      $('li:last', an[i]).addClass('disabled');
                  } else {
                      $('li:last', an[i]).removeClass('disabled');
                  }
              }
          }
      }
  } );
 
  vm.dtOptions = DTOptionsBuilder.newOptions()
        .withOption('createdRow', createdRow)
        .withOption('rowCallback', rowCallback)
        .withOption('ajax', {
         // Either you specify the AjaxDataProp here
          data: function(data){
            data.action = "getRequestDataTableAngular"
            data.id = vm.parent.object_id;
            data.search_value = "";
            data.multi_search = [];
            data.field = 0;
            
            if(data.order[0].column!=0){
              data["column-sort"]=data.columns[data.order[0].column].data
              data["column-sort-type"]= data.order[0].dir
            }else{
              data["column-sort"]=data.columns[1].data
              data["column-sort-type"]= 'ASC'
            }
            
      
            return data;
          },
         url: '../../dashboard_data/search.php',
         type: 'POST'
     })
     // or here
     .withDataProp('data')
        .withOption('processing', true)
        .withOption('serverSide', true)
        .withPaginationType('full_numbers')
        .withOption('createdRow', function(row, data, dataIndex) {
            // Recompiling so we can bind Angular directive to the DT
            $compile(angular.element(row).contents())($scope);
        })
        .withOption('headerCallback', function(header) {
            if (!vm.headerCompiled) {
                // Use this headerCompiled field to only compile header once
                vm.headerCompiled = true;
                $compile(angular.element(header).contents())($scope);
            }
        })
        // .withOption('responsive', true)
        .withOption('order', [1, 'asc'])
        .withOption('scrollY', '100px')
        .withOption('scrollX', '100%')
      
        // .withFixedColumns({
        //    leftColumns: 1
        // })
        .withOption('fnInitComplete',function(){
          this.parent().perfectScrollbar();
          this.parent().trigger('resize');

          // if(!vm.hasOrganized){
          //    vm.reorganizeView();
          //    vm.hasOrganized = true;
          // }
         vm.isLoadingWidget = false;
          vm.renderView();

        })
        // .withOption('colReorder', true)
        .withColReorder()
        .withColReorderOption('fixed', true)
        .withColReorderOption('allowResize', true)
        .withColReorderOption('iFixedColumns', 1)
        .withColReorderOption('fnResizeCallback', function(){
          //reloadData()
          vm.busy = true;
          //vm.renderView();
          //console.log(this);
          //console.log(this.s.dt.aoColumns);
          ViewService.setCustomView(vm.parent.object_id,this.s.dt.aoColumns).then(function(){
            vm.busy = false;
           });
        })
        .withColReorderCallback(function() {
          //console.log(this);
          vm.busy = true;
           //console.log(vm.dtColumns.$$state.value);
           ViewService.setCustomView(vm.parent.object_id,this.s.dt.aoColumns).then(function(){
            vm.busy = false;
           });
        })
        .withOption('oLanguage', {
          "sProcessing": '<h5 class="dt-loading"><i class="fa fa-spinner fa-spin "></i> Loading...</h5>'
        })
        
        .withBootstrap()
        .withDOM("'<'row'<'pull-left'l><'pull-right'f>r>t<'row'<'pull-left'i><'pull-right'p>>'")



  vm.reloadData = reloadData;
  vm.dtInstance = {};
  vm.toggleStarred = toggleStarred;

  function createdRow(row, data, dataIndex) {
      // Recompiling so we can bind Angular directive to the DT
      $compile(angular.element(row).contents())($scope);
  }
  function actionsHtml(data, type, full, meta) {
     vm.selected[full.ID] = false;
     //vm.rowData[full.ID] = data
     vm.rowData[full.ID] = data;

     var checkbox = '<input type="checkbox" ng-model="view.selected[' + data.ID + ']" ng-click="view.toggleOne(view.selected)">';
     var starred = (vm.vDetails.show_starred==0?"":'<a class="table-item-action starred_button" ng-click="view.toggleStarred(view.rowData['+data.ID+'])"><i class="fa {{(view.rowData['+data.ID+'][0].starred>0?\'fa-star\':\'fa-star-o\')}}"></i></a>');
     var print = (vm.vDetails.print_form_id==null?'':'<a class="table-item-action" target="_blank" href="/user_view/workspace?view_type=update&ID=' + 
                  vm.vDetails.print_form_id + '&formID=' + vm.parent.object_id + '&requestID=' + data.request_id + '&trackNo=' + 
                  data.TrackNo + '&embed_type=viewEmbedOnly&print_form=true"><i class="fa fa-print"></i></a>');


     return checkbox + starred + print;
      

  }

  function setButtonActions(){
    var uniqueStats = [];
    vm.selectedAction = null;
   // vm.rowData
    var status = [];
    // console.log(vm.selected)
    // for (var i = vm.selected.length - 1; i >= 0; i--) {
    //   console.log(vm.selected[i])
    //   if(vm.selected[i]){
    //     console.log(vm.rowData[i].Status)
    //     if(status.indexOf(vm.rowData[i].Status)==-1){
    //       uniqueStats.push(vm.rowData[i]);
    //       status.push(vm.rowData[i].Status);
    //     }
    //   }
    // };
    // console.log(vm.selected)
    // console.log(status)
    angular.forEach(vm.selected, function(value, key){
      if(value){
        if(status.indexOf(vm.rowData[key].Status)==-1){
          uniqueStats.push(vm.rowData[key]);
          status.push(vm.rowData[key].Status);
        }
      }
      
    });
    
    vm.actionButtons = [];
    var tempButtons = [];
    var isFirstStat = true;
    angular.forEach(uniqueStats, function(value, key){
      var buttons = $.map(JSON.parse(value[2].buttons), function(el) { return el; })
      
      if(!isFirstStat){
        var resButton = [];
        angular.forEach(buttons, function(butt, key){
          angular.forEach(vm.actionButtons, function(tbutt, tkey){
            if(butt.button_name==tbutt.button_name){
              resButton.push(tbutt);
            }
          })
        });
        vm.actionButtons = resButton;
      }else{
        vm.actionButtons = buttons;
        isFirstStat  = false;
      }

      
      
      
    });
    console.log(vm.actionButtons);
    //qconsole.log(JSON.parse(uniqueStats[0][2].buttons));
    // console.log(status)


  }

  function toggleAll (selectAll, selectedItems) {
    
      for (var id in selectedItems) {
          if (selectedItems.hasOwnProperty(id)) {
              selectedItems[id] = selectAll;
          }
      }
      setButtonActions()
  }




  function toggleOne (selectedItems) {

      for (var id in selectedItems) {
          if (selectedItems.hasOwnProperty(id)) {
              if(!selectedItems[id]) {
                  vm.selectAll = false;
                  setButtonActions()
                  return;
              }
          }
      }
      vm.selectAll = true;
      setButtonActions()
  }

    function rowClick(data) {
        var href = "/user_view/workspace?view_type=request&formID="+vm.parent.object_id+"&requestID="+data.request_id+"&trackNo="+data.TrackNo+"&printID=0";
        window.open(href);
      
    }

    function rowCallback(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        $(nRow).find('td:not(td:first-child)').unbind('click');
        $(nRow).find('td:not(td:first-child)').bind('click', function() {
            $scope.$apply(function() {
                rowClick(aData);
            });
        });
        return nRow;
    }

    function reloadData() {
        var resetPaging = false;
        vm.dtInstance.reloadData(function(data){}, resetPaging);
    }

    function toggleStarred(data){
      vm.busy = true;
      ViewService.setStarred(vm.object_id, data.ID, vm.vDetails.form_table_name, data[0].starred).then(function(res){
        vm.busy = false;

        if(data[0].starred==0){
          data[0].starred = res.data.id;
        }else{
          data[0].starred = 0;
        }
      })
    }

  
    

}



//Directive of widget view
dApp.directive("widgetView", function($compile){
	return {
    restrict:'E',
    scope:{
      parent:"=",
      removeWidget:"&"
    },
    templateUrl: '/modules/dashboard/templates/widget-view.html',
    controller:"WidgetViewController",
    controllerAs:"view",
    bindToController:true,
    link:function(scope, element, attrs){
     
     scope.$watch(function(){ return scope.view.parent.height;}, function(newValue){
        scope.view.renderView();
     })

     scope.$watch(function(){ return scope.view.parent.width;}, function(newValue){
        scope.view.renderView();
     })

     //Min Width
     angular.element(element).parent().attr("data-gs-min-width",5);
     angular.element(element).parent().data('_gridstack_node').minWidth = "5";
     //Min Width
     angular.element(element).parent().attr("data-gs-min-height",5);
     angular.element(element).parent().data('_gridstack_node').minHeight = "5";

    // scope.view.reorganizeView = function(){
    //   var topFilter = angular.element('<div class="row"></div>');
    //   var bottomFilter = angular.element('<div class="row"></div>');
      

    //   // element.find('.dataTables_wrapper').prepend(topFilter);
    //   // element.find('.dataTables_length, .dataTables_filter, .dataTables_processing').wrapAll(topFilter);
    //   // element.find('.dataTables_wrapper').append(bottomFilter);
    //   // element.find('.dataTables_info, .dataTables_paginate').wrapAll(bottomFilter);


    //   // element.find('.dataTables_filter, .dataTables_paginate').wrap('<div class="pull-right"></div>');
    //   // element.find('.dataTables_length, .dataTables_info').wrap('<div class="pull-left"></div>');
    //   //element.find('.dataTables_length, .dataTables_filter, .dataTables_info, .dataTables_paginate').wrap('<div class="col-xs-6"></div>');


    //  }

     scope.view.renderView = function(){
     // console.log(angular.element(element.find('.ibox-content')[0]).height());
        var containerH = angular.element(element.find('.ibox-content')[0]).height();
        //var formActions = element.find('.datatable-form-actions').height();
        var rowH = 0;
        angular.forEach(element.find('.row'),function(val, key){
          rowH = rowH + angular.element(val).height();
         // console.log(rowH)
        })

        rowH+=45;

        // rowH+=60;
        element.find('.dataTables_scrollBody').height((containerH - rowH) + "px");    
      }
    }
  };
})

