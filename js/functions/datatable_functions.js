/* 
 *  All functions that are related in datatable
 *  No events allowed here
 *
*/


function addIndexClassOnSort(self){
    var index = $(self).index();
    $("td").removeClass("td-sorted");
    $(self).closest("table").find("tr").each(function(){
        $(this).find("td").eq(index).addClass("td-sorted");
    })
}
function setDatatableTooltip(container){
    
    if(typeof container == "object"){
        container.find(".tooltip").remove();
        $(".tip").tooltip({
            "container" : container
        });
    }else{
        $(container+" .tooltip").remove();
        $(".tip").tooltip({
            "container" : container
        });
    }
}

function dataTable_widget_hide(obj, listOfappContainer){ 
    $(obj).closest(listOfappContainer).find(".dataTables_paginate").hide();
}


function dataTable_widget(obj, listOfappContainer){

    var dataTableWid = $(obj).closest(listOfappContainer).find(".dataTable_widget");
    if($(obj).closest(listOfappContainer).find(".dataTables_info").html()==""){
        return false;
    }
    dataTableWid.html("");
    $(obj).closest(listOfappContainer).find(".dataTables_info").appendTo(dataTableWid);
    $(obj).closest(listOfappContainer).find(".dataTables_paginate").appendTo(dataTableWid);
    $(obj).closest(listOfappContainer).find(".dataTables_paginate").show();
}

function set_dataTable_th_width(thArray, minus_width) {
    return false;
    var data_table_th = $('.fl-list-of-app-record-wrapper .display_data th');
    var th_ele = '';
    var th_width;
    minus_width = minus_width||15;
    
    for(var i in thArray){
        //width manipulation
        th_ele = data_table_th.eq(thArray);
        th_width = th_ele.width();
        console.log(th_width);
        th_ele.css('width', th_width - minus_width);
    }   
}
function datatableHoverColor(target){
    // var oldColor = "";
    $("body").on("mouseenter",target, function(){
        // alert("enter")
        var backgroundColor = $(this).css("background-color");
        var trDarkenColor = darkenColor(backgroundColor);
        $(this).css("background-color",trDarkenColor)
        $(this).attr("original-bg-color",backgroundColor)
    })

    $("body").on("mouseleave",target, function(){
        var backgroundColor = $(this).attr("original-bg-color");
        $(this).css("background-color",backgroundColor)
    })    
}