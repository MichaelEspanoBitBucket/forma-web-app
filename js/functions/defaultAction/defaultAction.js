
var selected_action = [];
$(document).ready(function(){
	var request_id = $("#ID").val();
	$("#horizontal-sub-menu").prepend($(".defaultActionButton").html());
	if(location.pathname.indexOf('workspace') > -1){
		console.log('may workspace');
		if($('.defaultActionButton').children().length < 1){
			$(".checkDefaultActions").hide();
		}
		posting.showReply('#Notes', ".comment_");
		//check the availability of the next and previous
		//
		//next
		// var params = {
		// 	"request_id":$('#Next').attr('request-id'),
		// 	"form_id":$('#Next').attr('form-id'),
		// 	"less_greater":"<",
		// 	"check":"true"
		// }
		// $.post('/ajax/default_action_previous_next', params, function(echo_result){
		// 	echo_result = parseInt(echo_result);
		// 	if(echo_result < 1){
		// 		$('[id="Next"]').remove();
		// 	}
		// });
		// //previous
		// params = {
		// 	"request_id":$('#Previous').attr('request-id'),
		// 	"form_id":$('#Previous').attr('form-id'),
		// 	"less_greater":">",
		// 	"check":"true"
		// }
		// $.post('/ajax/default_action_previous_next', params, function(echo_result){
		// 	echo_result = parseInt(echo_result);
		// 	if(echo_result < 1){
		// 		$('[id="Previous"]').remove();
		// 	}		
		// });
	}
	// $.post("/ajax/default_action_previous_next",{
	// 		"request_id":$('#ID').val(),
	// 		"form_id":$('#FormID').val()
	// 	},function(data){
	// 		console.log("data from prevnext", data);
	// 	});
});

$(document).on('click', '.default-action', function(e){
	if($("#config_default_actions").val() == 1){
		DefaultAction.init();
	}
});

$(document).on('click', '.set-default-actions', function(e){
	defaultAction.saveDefaultActions();
});

$(document).on('click', '.d_action', function(e){
	var link_name = $(this).val();
	if($(this).prop("checked") == true){
		defaultAction.selectDefaultAction(link_name);
	}
	else{
		defaultAction.removeDefaultAction(link_name);
	}
	if($('.d_action:checked').length == $('.d_action').length){
		$('.default_action_select_all').prop('checked', true);
	}
	else{
		$('.default_action_select_all').prop('checked', false);
	}
});
$(document).on('click','.add-action', function(e) {
	if($(e.target)[0] == $(this)[0]){
		$(this).find('.d_action').trigger('click');
	}	
});
$(document).on('mouseover','.add-action', function(e) {
	$(this).css('cursor', 'pointer');
});

$(document).on('mouseover','.selectall', function(e) {
	$(this).css('cursor', 'pointer');
});

$(document).on('click','.selectall', function(e){
	console.log("select all");
	if($(e.target)[0] == $(this)[0]){
		$(this).find('.default_action_select_all').trigger('click');
	}
	if($(this).find('.default_action_select_all').prop('checked') == true){
		$('.form-list').find('.add-action').each(function(e){
			if($(this).find('.d_action').prop("checked") == false){
				var action = $(this).find('.d_action').val();
				defaultAction.selectDefaultAction(action);
				$(this).find('.d_action').prop('checked', true);
			}
		});
	}
	else{
		$('.form-list').find('.add-action').each(function(e){
			if($(this).find('.d_action').prop("checked") == true){
				var action = $(this).find('.d_action').val();
				defaultAction.removeDefaultAction(action);
				$(this).find('.d_action').prop('checked', false);
			}
		});
	}
})

//default action commands.........
$(document).on('click', '.default-action-btn', function(e){
	if($('#config_default_actions').val() == "1"){
		if($.inArray($(this).attr('id'), defaultAction.loadActionNames()) > -1){
			if($(this).attr('id') == "Notes"){

			}
			if($(this).attr('id') == "Refresh"){
				location.reload();
			}
			if($(this).attr('id') == "Print"){
				$('.printForm').trigger('click');
			}
			if($(this).attr('id') == "Next"){
				var params = {
					"request_id":Number($(this).attr('request-id')),
					"form_id":$(this).attr('form-id'),
					"less_greater":"<"
				}
				// window.open("http://www.google.com.ph");
				ui.block();
				$.post('/ajax/default_action_previous_next', params, function(echo_result){
					ui.unblock();
					if(echo_result.length > 0){
						location.replace(echo_result);
						// window.open(echo_result);
					}
					else{
						showNotification({
                             message: "You have reached the last document in the list.",
                             type: "information",
                             autoClose: true,
                             duration: 3
                        });
					}
				});
			}
			if($(this).attr('id') == "Previous"){
				var params = {
					"request_id":Number($(this).attr('request-id')),
					"form_id":$(this).attr('form-id'),
					"less_greater":">"
				}
				ui.block();
				$.post('/ajax/default_action_previous_next', params, function(echo_result){
					ui.unblock();
					if(echo_result.length > 0){
						location.replace(echo_result);
						// window.open(echo_result);
					}
					else{
						showNotification({
                             message: "You have reached the last document in the list..",
                             type: "information",
                             autoClose: true,
                             duration: 3
                        });
					}
				});
			}
			// console.log("in array boy.");
		}
	}
});

//default key commands
$(document).on('keydown', 'body', function(e){
	// e.preventDefault();
	if(window.location.pathname.indexOf('workspace') > -1){
		if($('#config_default_actions').val() == "1"){
			if(e.altKey == true && e.ctrlKey == true){
				if(e.keyCode == "67"){
					//ctrl+alt+c comments/notes
					e.preventDefault();
					    // var newNotesDialog = new jDialog(DefaultAction.loadNotesDialog('Notes', 'default-action', '', 'fa fa-link', 'faiconColor', 'Japhet Morada', 435), "", "600", "", "40", function() {
					    // });
					    // newNotesDialog.themeDialog("modal2");
					    $('#Notes').trigger('click');
				}
				else if(e.keyCode == "78"){
					//ctrl+alt+n = New request
					//trigger a new request
					e.preventDefault();
					var url = $('#New a').attr('href');
					if(url.length > 0){
						window.open(url);
					}
				}
			}
			//ctrl+shift+v View
			if(e.shiftKey == true && e.ctrlKey == true){
				if(e.keyCode == "86"){
					e.preventDefault();
					//trigger a view
					var url = $('#View a').attr('href');
					if(url.length > 0){
						window.open(url);
					}
				}
			}
			//shift+tab Next
			if(e.shiftKey == true && e.keyCode == "9"){
				// var url = $(this).attr('href');
				// window.open(url);
				e.preventDefault();
				var url = $('#Next a').attr('href');
				// if(url.length > 0){
				// 	window.open(url);
				// }
				if($('#Next').length > 0){
					var params = {
						"request_id":$('#Next').attr('request-id'),
						"form_id":$('#Next').attr('form-id'),
						"less_greater":"<"
					}
					$.post('/ajax/default_action_previous_next', params, function(echo_result){
						if(echo_result.length > 0){
							location.replace(echo_result);
							// window.open(echo_result);
						}
						else{
							showNotification({
	                             message: "You have reached the last document in the list..",
	                             type: "information",
	                             autoClose: true,
	                             duration: 3
                            });
						}
					});
				}
			}
			//shift+backspace Previous
			if(e.shiftKey == true && e.keyCode == "8"){
				// var url = $(this).attr('href');
				// window.open(url);
				e.preventDefault();
				// var url = $('#Previous a').attr('href');
				// if(url.length > 0){
				// 	window.open(url);
				// }
				if($('#Previous').length > 0){
					var params = {
						"request_id":$('#Previous').attr('request-id'),
						"form_id":$('#Previous').attr('form-id'),
						"less_greater":">"
					}
					$.post('/ajax/default_action_previous_next', params, function(echo_result){
						if(echo_result.length > 0){
							location.replace(echo_result);
							// window.open(echo_result);
						}
						else{
							showNotification({
	                             message: "You have reached the last document in the list.",
	                             type: "information",
	                             autoClose: true,
	                             duration: 3
                            });
						}
					});
				}
			}

			if(e.ctrlKey == true && e.altKey == false){
				if(e.keyCode == "80"){
				//ctrl+P Print
					e.preventDefault();
					$('.printForm').trigger('click');
				}
			}
		}
	}
});

//--------------------------------

function loadActionListDialog(){
	var newDialog = new jDialog(DefaultAction.setDialog('Default Actions', 'default-action', '', '', 'faiconColor', '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-default-action"></use></svg>'), "", "300", "", "40", function() {

    });

	newDialog.themeDialog("modal2");
}


DefaultAction = {
	"init" : function(callback){
		if(callback){
			callback();
		}
		selected_action = [];
		loadActionListDialog();
		defaultAction.loadSelectedActions();
	},

	"setDialog": function(title, type, note, icon, faiconColor, svgIcon){
		var layout = '<div><h3 class="fl-margin-bottom">';
		layout += svgIcon + '<span>'+title+'</span>';//'<i class=" '+ icon +' '+ faiconColor +' icon-save"></i> ' + ;
	    layout += '</h3></div>';
	    layout += '<div class="hr"></div>';
	    //layout +='<div style="margin:2px 0px 0px 2px;font-size:11px;color:red">' + note + '</div>';
	    layout += '<div class="content-dialog">';
	    //left panel
	    layout += '<div class="fl-floatLeft section clearing fl-field-style" style="width: 99%;">';
	    layout += '<div class="properties width container column div_1_of_1">';
		layout += '<span class="font-bold">Default Actions List</span>';
	    layout += '<ul class="drag-handle fl-margin-top" style="margin-bottom: 5px;">';
	    layout += '<div class="field below" >';
			layout += '<div class="label below2"></div>';
			layout += '<div class="input position below selectall">';
					layout += '<input type="checkbox" id="default_action_select_all" class="default_action_select_all css-checkbox" value="Select All" />';
					layout += '<label for="default_action_select_all" class="css-label"></label>';
					layout += 'Select All';
			layout += '</div>';
		layout += '</div>';
		layout += '</ul>';
	    layout += '<ul class="fl-margin-top form-list">';
	    //iterate for links
    	for(i in default_action_list){
    		layout += '<li style="margin-bottom: 2px;padding-left:20px;">';
	    		layout += '<div class="fields below">';
	    			layout += '<div class="label below2"></div>';
	    			layout += '<div class="input position below add-action fl-table-ellip">';
	    				layout += '<input type="checkbox" id="check' + default_action_list[i].id + '" class="d_action css-checkbox" value="' + default_action_list[i].action + '"/>';
	    				layout += '<label for="check' + default_action_list[i].id + '" class="css-label"></label>';
	    				layout += default_action_list[i].action;
	    			layout += '</div>';
	    		layout += '</div>';
	    	layout += '</li>';
    	}
	    //-----------------
	    layout += '</ul>';
	    layout += '</div>';
	    layout += '</div>';
	    layout += '<input type="button" class="btn-blueBtn fl-margin-right set-default-actions fl-positive-btn" value="OK"/>';
	    layout += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel" style="display:none;"/>';
	    layout += '</div>';
	    return layout;
	},

	"loadNotesDialog": function(title, type, note, icon, faiconColor, recepient, req_id){
		
		var layout = '<h3 class="fl-margin-bottom">';
		layout += '<i class=" '+ icon +' '+ faiconColor +' icon-save"></i> ' + title;
	    layout += '</h3>';
	    layout += '<div class="hr"></div>';
	    //layout +='<div style="margin:2px 0px 0px 2px;font-size:11px;color:red">' + note + '</div>';
	    layout += '<div class="content-dialog" style="height: 300px;">';
	    //left panel
	    layout += '<div class="fl-floatLeft" style="width: 100%;border:1px solid #cfdbe2;">';
	    layout += '<div class="properties width container" style="height:225px;">';
		// layout += '<h3 class="fl-margin-top  fl-margin-bottom" style="width:55%;padding-bottom:5px;">To:</h3>';
	    // layout += '<div class="notification-notes">';
    	layout += '<div style="float:left;width:99%;border-bottom:1px solid #cfdbe2;padding-bottom:1%;padding-left:1%;">';
    		layout += '<div style="margin:10px 3px 0px 0px;float:left">To: </div>';
    		layout += '<div class="recipient"><span class="getRecepient" style="margin-right:5px;"> ' + recepient + '</span></div>';
	    layout += '</div>';
	    layout += '<div style="padding-top:5px;float:left;width:100%;height:100%;">';
			    	layout +='<form style="height:100%;">';
			    		layout += '<textarea style="width:98%;height:95%;resize:none;border:none;padding:1%;" placeholder="Enter your message...">';
			    		layout += '</textarea>';
			    	layout +='</form>';
		    	layout += '</div>';
    	layout += '</div>';
	    // layout += '</div>';
	    //-----------------
	    layout += '</div>';
	    layout += '</div>';
	    layout += '<input type="button" class="btn-blueBtn fl-margin-right set-default-actions fl-positive-btn" value="OK"/>';
	    layout += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel" style="display:none;"/>';
	    layout += '</div>';
	    return layout;
	}
}

defaultAction = {
	"loadSelectedActions": function(){
		var body_data = $("body").data();
		var d_actions = body_data['default-action-button'];
		for(i in d_actions){
			var id = "#check" + d_actions[i].id;
			$(id).prop("checked", true);
			defaultAction.selectDefaultAction(d_actions[i].action);
		}
		if($('.d_action:checked').length == $('.d_action').length){
			$('.default_action_select_all').prop('checked', true);
		}
		else{
			$('.default_action_select_all').prop('checked', false);
		}
	},
	"loadActionNames": function(){
		var actionNames = [];
		for(i in default_action_list){
			actionNames.push(default_action_list[i].action);
			//console.log("action name:", default_action_list[i].action);
		}
		return actionNames;
	},
	"saveDefaultActions": function(){
		var body_data = $('body').data();
		console.log(selected_action);
		selected_action = defaultAction.sortActions();
		console.log("selected action (sorted)", selected_action);
		body_data['default-action-button'] = selected_action;
		$('#popup_cancel').trigger('click');
	},
	"selectDefaultAction": function(link_name){
		for(i in default_action_list){
			if(default_action_list[i].action == link_name){
				var act = {}
				act.id = default_action_list[i].id;
				act.action = default_action_list[i].action;
				act.key = default_action_list[i].key;
				selected_action.push(act);
			}
		}
	},
	"removeDefaultAction": function(link_name){
		//var index = form_clicked_id.indexOf(link_name);
		//form_clicked_id.splice(index, 1);
		for(i in selected_action){
			//body_data['default_action'].push(selected_action);
			if(selected_action[i].action == link_name){
				delete selected_action[i];
			}
		}
		selected_action = defaultAction.formatSelectedActions(selected_action);
	},
	"formatSelectedActions": function(obj){
		var formatted_selected_actions = [];
		for(i in obj){
			if(typeof(obj[i].action) != "undefined"){
				formatted_selected_actions.push(obj[i]);
			}
		}
		 return formatted_selected_actions;
	},
	"sortActions": function(){
		var sortedActions = [];
		selected_action = selected_action.sort(function(a, b){
		    return a.id - b.id;
		});
		return selected_action;
	}
}