var jsonDelegateData = {
    search_value: "",   
    "column-sort": "",
    "column-sort-type": "", 
    limit: 10,
}
var columnSort = [];

$(document).ready(function(){
    DelateSettings.init();
})


DelateSettings = {
	"init" : function(){
		var pathname = window.location.pathname;
		if(pathname!="/user_view/delegate"){
			return false;
		}
		//for load
		this.load();
		
        //for sort
		this.sort();
		
        //for search
		this.search();

		//for limit
		this.limit();

        //create
        this.create();

        //update
        this.update();

        //submit
        this.submit();

        //validateUser
        this.validateUser();

        //delete
        this.deleteDelegate();
	},
	load : function(callback){

		//code here
		$("#loadDelagate").dataTable().fnDestroy();
		var oColReorder = {
            "iFixedColumns": 0
        };
		var oTable = $('#loadDelagate').dataTable( {
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/delegate",
            "fnServerData": function(sSource, aoData, fnCallback) {
            	aoData.push({"name": "limit", "value": jsonDelegateData['limit']});
                aoData.push({"name": "search_value", "value": $.trim(jsonDelegateData['search_value'])});
                aoData.push({"name": "action", "value": "loadDelegateDatatable"});
                aoData.push({"name": "column-sort", "value": jsonDelegateData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonDelegateData['column-sort-type']});

                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                $.ajax({
                    // "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        try{
                            // console.log(data)
                            data = JSON.parse(data);
                            // return;
                            fnCallback(data);
                        }catch(err){
                            console.log(err)
                        }
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
            	
            	var obj = '#loadDelagate';
            	var listOfappContainer = '.fl-list-of-app-record-wrapper';
            	dataTable_widget(obj, listOfappContainer)	
            
            },
            fnDrawCallback : function(){
            	$("#loadDelagate tr .rule-data").each(function(){
            		var json_data = $(this).text()
            		json_data = JSON.parse(json_data);
            		$(this).closest("tr").data("row_data",json_data);
            	})
                //set_dataTable_th_width([0]);
                setDatatableTooltip(".dataTable_widget");
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))

            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonDelegateData['limit']),
        });
        return oTable;
	},
	"sort" : function(){
		var self = this;
		$("body").on("click","#loadDelagate th",function(){
	        var cursor = $(this).css("cursor");
	        jsonDelegateData['column-sort'] = $(this).attr("field_name");
	        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
	            return;
	        }
	        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

	        $(this).closest("#loadDelagate").find(".sortable-image").html("");
	        if(indexcolumnSort==-1){
	            columnSort.push($(this).attr("field_name"))
	            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
	            jsonDelegateData['column-sort-type'] = "ASC";
	        }else{
	            columnSort.splice(indexcolumnSort,1);
	            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
	            jsonDelegateData['column-sort-type'] = "DESC";
	        }
	        var thisTh = this;
	        self.load(function(){
	        	addIndexClassOnSort(thisTh);
	        });
	    })
	},
	"search" : function(){
		var self = this;
		$("body").on("keyup","#txtSearchRuleDatatable",function(e){
            if (e.keyCode == "13") {
    	        $("#loadDelagate").dataTable().fnDestroy();
    	        jsonDelegateData['search_value'] = $(this).val();
    	        self.load();
            }
	    })
        $("body").on("click","#btnSearchDelagateDatatable",function(){
            $("#loadDelagate").dataTable().fnDestroy();
            jsonDelegateData['search_value'] = $("#txtSearchRuleDatatable").val();
            self.load();
        })
	},
	"limit" : function(){
		var self = this;
		$("#searchDelagateLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonDelegateData['limit'] = val;
            $("#loadDelagate").dataTable().fnDestroy();
            // var self = this;
            self.load();
        // }
        })
	},
    "create" : function(){
        var self = this;
        $("body").on("click","#addDelagateModal",function(){
            var json = {
                "title" : "Create Delegation",
                "user" : "",
                "userDelegate" : "",
                "remarks" : "",
                "start_date" : "",
                "end_date" : "",
                "action" : "addDelegate",
                "data_id" : "0",
                "message" : "Delegate has been successfully added."
            };
            self.modal(json,1);
        })
    },
    update : function(){
        var self = this;
        $("body").on("click",".updateDelegate",function(){
            var json_data = $(this).attr("json_data");
            if(json_data){
                
                json_data = JSON.parse(json_data);
                var json = {
                    "title" : "Update Delegation",
                    "user" : json_data['user_id'],
                    "user_delegate_id" : json_data['user_delegate_id'],
                    "remarks" : json_data['remarks'],
                    "start_date" : json_data['start_date'],
                    "end_date" : json_data['end_date'],
                    "action" : "editDelegate",
                    "data_id" : json_data['id'],
                    "message" : "Delegate has been successfully updated.",

                    "created_by_user" : json_data['created_by_user'],
                    "date_created" : json_data['date_created'],
                    "updated_by_user" : json_data['updated_by_user'],
                    "date_updated" : json_data['date_updated'],
                };

                self.modal(json,2);
                $("#delegate-user_id").prop("disabled",true);
            }
        })
    },
    "modal" : function(json,type){
        var users = $("#users-list").text();
        var ret = '<div><h3 class="fl-margin-bottom">';
            ret += '<svg class="icon-svg icon-svg-modal" viewBox="0 0 100.264 100.597"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-delegation"></use></svg> <span>'+json['title']+ '</span>';
            ret += '</h3></div>';
            ret += '<div class="hr"></div>';
            ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
            ret += '<div class="content-dialog" style="height: auto;">';
                // your content here
                ret += '<div class="fields_below section clearing fl-field-style">';
                    ret += '<div class="input_position_below column div_1_of_1">';
                        ret += '<span class="">User: <font color="red">*</font></span>';
                        ret += '<select class="form-select" id="delegate-user_id" style="margin-top:0px">'; //setSelected("0", orgchart_dept_code_type)
                            ret += '<option value="0">-- Select User--</option>';
                            if(users!=""){
                                users = JSON.parse(users);
                                for(var i in users){
                                    ret += '<option value="'+ users[i]['id'] +'" '+ setSelected(users[i]['id'],json['user']) +'>'+ users[i]['display_name'] +'</option>';
                                }
                            }
                        ret += '</select>';
                    ret += '</div>';
                ret += '</div>';
                ret += '<div class="fields_below section clearing fl-field-style">';
                    
                    ret += '<div class="input_position_below column div_1_of_1">';
                        ret += '<span class="">Delegate: <font color="red">*</font></span>';
                        ret += '<select class="form-select" id="delegate-user_delegate_id" style="margin-top:0px">'; //setSelected("0", orgchart_dept_code_type)
                            ret += '<option value="0">-- Select User--</option>';
                            if(users!=""){
                                var disableUser = "";
                                for(var i in users){
                                    disableUser = "";
                                    if(users[i]['id']==json['user']){
                                        disableUser = "disabled='disabled'";
                                    }
                                    ret += '<option value="'+ users[i]['id'] +'" '+ setSelected(users[i]['id'],json['user_delegate_id']) +' '+ disableUser +'>'+ users[i]['display_name'] +'</option>';
                                }
                            }
                        ret += '</select>';
                    ret += '</div>';
                ret += '</div>';
                ret += '<div class="fields_below section clearing fl-field-style">';
                   
                    ret += '<div class="input_position_below column div_1_of_1">';
                        ret += '<span>Reason for delegation: <font color="red">*</font></span>';
                        ret += '<input type="text" name="" data-type="textbox" class="form-text" id="delegate-remarks" value="'+ json['remarks'] +'">';
                    ret += '</div>';
                ret += '</div>';
                ret += '<div class="fields_below section clearing fl-field-style">';
                    ret += '<div class="input_position_below column div_1_of_1"';
                    ret += '<span class="">Start Date: <font color="red">*</font></span>';
                        ret += '<input type="text" name="" data-type="textbox" class="form-text" id="delegate-start_date" value="'+ json['start_date'] +'">';
                    ret += '</div>';
                ret += '</div>';
                ret += '<div class="fields_below section clearing fl-field-style">';
                    ret += '<div class="input_position_below column div_1_of_1">';
                    ret += '<span class="">End Date: <font color="red">*</font></span>';
                        ret += '<input type="text" name="" data-type="textbox" class="form-text" id="delegate-end_date" value="'+ json['end_date'] +'">';
                    ret += '</div>';
                ret += '</div>';
                if(type==2){
                    ret += '<div class="fields_below">';
                        ret += '<div class="label_below2">Logs:</div>';
                        ret += '<div class="fields_below" style="margin-bottom: 10px">';
                            ret += '<div class="label_below2" style="display:inline;font-size:11px">Created By: </div>';
                            ret += '<div class="input_position_below" style="margin-bottom:10px;display:inline">';
                                ret += json['created_by_user'];
                            ret += '</div>';
                        ret += '</div>';
                        ret += '<div class="fields_below" style="margin-bottom: 10px">';
                            ret += '<div class="label_below2" style="display:inline;font-size:11px">Date Created: </div>';
                            ret += '<div class="input_position_below" style="margin-bottom:10px;display:inline">';
                                ret += json['date_created'];
                            ret += '</div>';
                        ret += '</div>';
                        ret += '<div class="fields_below" style="margin-bottom: 10px">';
                            ret += '<div class="label_below2" style="display:inline;font-size:11px">Updated By: </div>';
                            ret += '<div class="input_position_below" style="margin-bottom:10px;display:inline">';
                                ret += (json['updated_by_user']==null?"":json['updated_by_user']);
                            ret += '</div>';
                        ret += '</div>';
                        ret += '<div class="fields_below">';
                            ret += '<div class="label_below2" style="display:inline;font-size:11px">Date Updated: </div>';
                            ret += '<div class="input_position_below" style="margin-bottom:10px;display:inline">';
                                ret += (json['date_updated']=="0000-00-00 00:00:00"?"":json['date_updated']);
                            ret += '</div>';
                        ret += '</div>';

                    ret += '</div>';
                }
                ret += '<div class="fields" style="">';
                    ret += '<div class="label_basic"></div>';
                    ret += '<div class="input_position" style="margin-top:5px;">';
                    ret += '<input type="button" class="btn-blueBtn fl-margin-right" id="delegate-submit" action="'+ json['action'] +'" data_id="'+ json['data_id'] +'" message="'+ json['message'] +'" value="Save">';
                    ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
                ret += '</div>';
            ret+= '</div>';
        var newDialog = new jDialog(ret, "", "", "400", "", function() {   
        });
        newDialog.themeDialog("modal2"); 
        $(".content-dialog-scroll").css({
            "max-height":"365px"
        })
        $(".content-dialog-scroll").perfectScrollbar('destroy');
        $(".content-dialog-scroll").perfectScrollbar();
        $("#delegate-end_date").datepicker({dateFormat: 'yy-mm-dd'});
        $("#delegate-start_date").datepicker({dateFormat: 'yy-mm-dd'});

        //date trigger
    },
    "submit" : function(){
        var self = this;
        $("body").on("click","#delegate-submit",function(){
            var action = $(this).attr("action");
            var data_id = $(this).attr("data_id");
            var user_id = parseInt($.trim($("#delegate-user_id").val()));
            var user_delegate_id = parseInt($.trim($("#delegate-user_delegate_id").val()))
            var remarks = $.trim($("#delegate-remarks").val());
            var start_date = $.trim($("#delegate-start_date").val());
            var end_date = $.trim($("#delegate-end_date").val());
            var message = $.trim($(this).attr("message"));

            var valid_startDate = new Date(start_date);
            var valid_endDate = new Date(end_date);

            if(valid_startDate>valid_endDate){
                showNotification({
                    message: "End Date must be later than Start Date.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return false;
            }

            //validation here
            if(user_id==0 || user_id=="" || remarks=="" || start_date=="" || end_date==""){
                showNotification({
                    message: "Please fill out the required fields.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return false;
            }
            //var array assignment here
            var json_data = {
                "user_id": user_id,
                "user_delegate_id":user_delegate_id,
                "remarks": remarks,
                "start_date":start_date,
                "end_date":end_date,
                "action":action,
                "data_id":data_id
            }
//            alert(user_delegate_id)
            //loading
            ui.block();
            $.post("/ajax/delegate",json_data,function(data){
                //error message here
                if(data=="0"){
                    showNotification({
                        message: "User already exists in your delegation list.",
                        type: "error",
                        autoClose: true,
                        duration: 3
                    });
                }else{
                    //success message here
                    showNotification({
                        message: message,
                        type: "success",
                        autoClose: true,
                        duration: 3
                    });
                    //reload table
                    self.load();
                    //unload
                    //close dialog
                    $(".fl-closeDialog").trigger("click");
                }
                ui.unblock();
            })
        })
    },

    "validateUser" : function(){
        $("body").on("change","#delegate-user_id",function(){
            var value = $(this).val();
            $("#delegate-user_delegate_id option").prop("disabled",false);
            if(value!="0"){
                if($("#delegate-user_delegate_id").val()==value){
                    $("#delegate-user_delegate_id").val("0");
                }
                $("#delegate-user_delegate_id option[value='"+ value +"']").prop("disabled",true);
            }
        })
    },

    "deleteDelegate" : function(){
        var self = this;
        $("body").on("click",".deleteDelegate",function(){
            var thisEvt = this;
            var newConfirm = new jConfirm("Are you sure you want to delete this delegate?", 'Confirmation Dialog','', '', '', function(r){
                if(r==true){
                    ui.block();
                    var data_id = $(thisEvt).attr("data-id");
                    var json_data = {
                        data_id:data_id,
                        "action":"deleteDelegate"
                    }
                    $.post("/ajax/delegate",json_data,function(data){
//                        alert(data)
                        //success message here
                        showNotification({
                            message: "Delegate has been successfully deleted.",
                            type: "success",
                            autoClose: true,
                            duration: 3
                        });
                        //reload table
                        self.load();
                        ui.unblock();
                    })
                }
            });
            newConfirm.themeConfirm("confirm2", {
                'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>' 
            });
        })
    }
}