(function(){

	$(document).ready(function(){
		erpUi.init('.fl-erp-main-wrapper');
	});

	var windowHeight = $(window).height();
	var docHeight = $(document).height();
	var erpTabNavHeight = $('.fl-tab-wrapper-nav-erp').children('#tabs').height();
	var appHeader = $('.fl-user-header-wrapper').height();
	var totalSub = erpTabNavHeight + appHeader + 45
	console.log(totalSub, appHeader, erpTabNavHeight)
	erpUi = {
		'init':function(element){
			var self = this;
			console.log(windowHeight);
			self.tabContainer('.fl-tab-container-erp');
			self.flErpMainContentWrapper('.fl-erp-main-content-wrapper');
			self.flUiAnnouncementOnlineUsersContent('#fl-ui-announcement-online-users-content');
		},

		'tabContainer':function(self){

			$(self).css({
				'height':windowHeight - totalSub 
			});
		
		},
		'flErpMainContentWrapper': function(self){
			
			$(self).css({
				'height':windowHeight - totalSub
			});
		
		},

		'flUiAnnouncementOnlineUsersContent': function(self){

			$(self).css({
				'height':windowHeight - totalSub
			});

			$(self).perfectScrollbar().mouseenter(function(){
				$(this).perfectScrollbar('update');
			});
			
		}


	}

})();