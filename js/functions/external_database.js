var jsonExternalDBData = {
    search_value: "",
    start: 0,
    limit: 10,
}
var columnSort = [];
$(document).ready(function () {

    //create modal
    $("#create-new-DBConnection").click(function () {
        var json = {};
        json['title'] = "Create External Connection";
        json['type'] = 1;
        ExternalDBDataTable.create_update_modalodal(json);

    })


    //update modal
    $("body").on("click", ".update-DBConnection", function () {
        var json = {};
        var json_data = $(this).attr("json_data");
        try {
            json_data = JSON.parse(json_data);
            json['title'] = "Update External Connection";
            json['type'] = 2;
            json = $.extend(json_data, json);
            ExternalDBDataTable.create_update_modalodal(json);
        } catch (err) {
            console.log(err)
        }
    })

    //save
    $("body").on("click", "#saveExternalConnection", function () {
        ExternalDBDataTable.saveExternalConnection(this);
    })

    //delete
    $("body").on("click", ".deleteExternalDB", function () {
        ExternalDBDataTable.deleteExternalConnection(this);
    })


    $("body").on("keyup", "#txtSearchDBConnectionDatatable", function (e) {
        if (e.keyCode == "13") {
            $(".loadDBConnection").dataTable().fnDestroy();
            jsonExternalDBData['search_value'] = $(this).val();
            ExternalDBDataTable.defaultData();

        }
    })
    $("body").on("click", "#btnSearchDBConnectionDatatable", function (e) {
        $(".loadDBConnection").dataTable().fnDestroy();
        jsonExternalDBData['search_value'] = $("#txtSearchWorkflowDatatable").val();
        ExternalDBDataTable.defaultData();
    })
    //load
    ExternalDBDataTable.defaultData();
    //sort
    $("body").on("click", ".loadDBConnection th", function () {
        var cursor = $(this).css("cursor");
        jsonExternalDBData['column-sort'] = $(this).attr("field_name");
        if ($(this).attr("field_name") == "" || $(this).attr("field_name") == undefined || cursor == "col-resize") {
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest(".loadDBConnection").find(".sortable-image").html("");
        if (indexcolumnSort == -1) {
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonExternalDBData['column-sort-type'] = "ASC";
        } else {
            columnSort.splice(indexcolumnSort, 1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonExternalDBData['column-sort-type'] = "DESC";
        }

        $(".loadDBConnection").dataTable().fnDestroy();
        var self = this;
        ExternalDBDataTable.defaultData(function () {
            addIndexClassOnSort(self);
        });
        //show entries
    })
    //show entries
    $(".searchDBConnectionLimitPerPage").change(function (e) {
        // if (e.keyCode == "13") {
        var val = parseInt($(this).val());
        jsonExternalDBData['limit'] = val;
        $(".loadDBConnection").dataTable().fnDestroy();
        // var self = this;
        var oTable = ExternalDBDataTable.defaultData();


        // }
    })

    //test connection

    $("body").on("click", "#test_connection", function () {
        ExternalDBDataTable.testConnection();
    })
})

ExternalDBDataTable = {
    "defaultData": function (callback) {
        var oColReorder = {
            allowReorder: false,
            allowResize: true
        };
        var oTable = $(".loadDBConnection").dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/external_database",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({"name": "action", "value": "loadExternalDBDataTable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "limit", "value": jsonExternalDBData['limit']});
                aoData.push({"name": "search_value", "value": $.trim(jsonExternalDBData['search_value'])});
                aoData.push({"name": "column-sort", "value": jsonExternalDBData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonExternalDBData['column-sort-type']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                $.ajax({
                    // "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        try {
                            data = JSON.parse(data);
                            console.log(data)
                            fnCallback(data);
                        } catch (err) {
                            console.log(data)
                            return false;
                        }
                    }
                });
            },
            "fnInitComplete": function (oSettings, json) {

                var obj = '.loadDBConnection';
                var listOfappContainer = $('.fl-list-of-app-record-wrapper');
                dataTable_widget(obj, listOfappContainer);

                // $(".tip").tooltip();
            },
            fnDrawCallback: function () {
                // $(".tip").tooltip();
                //set_dataTable_th_width([0]);
                setDatatableTooltip(".dataTable_widget");
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart: 0,
            iDisplayLength: parseInt(jsonExternalDBData['limit']),
        });
        return oTable;
    },
    create_update_modalodal: function (json) {
        var self = this;
        // $("body").on("click",".addCategoryModal",function(){
        var ret = '<h3><i class="icon-print"></i> ' + json.title + '</h3>';
        ret += '<div class="hr"></div>';
        ret += '</div>';
        ret += '<div class="content-dialog" id="external_database_form_container" style="width: 100%;">';
        ret += '    <div class="pull-left container-position" style="width: 100%">';
        ret += '    <div class="pull-left" style="width: 100%">';

        ret += '<div class="fl-end-workflow">';
        ret += '<div class="fields_below section clearing fl-field-style">'; // style="width:45%;float:left"

        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Connection Name: <font color="red">*</font></span>';
        ret += '<input type="text" name="" req="true" class="form-text obj_prop" id="connection_name" placeholder="Conn1" value="' + if_undefinded(json['connection_name'], "") + '">';
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields_below section clearing fl-field-style">';
        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Database Server: <font color="red">*</font></span>';
        // ret += '<input type="text" name="" class="form-text obj_prop" id="workflow_end-status" data-properties-type="lblFldName" data-object-id="" placeholder="Status" value="">';
        ret += '<select class="fl-input-select" id="db_type" style="width:100%">';
        ret += '<option value="1" ' + setSelected('1', json['db_type']) + '>MySQL</option>'; //setSelected('1',json['user_access_type'])
        ret += '<option value="2" ' + setSelected('2', json['db_type']) + '>MS SQL</option>';
        ret += '<option value="3" ' + setSelected('3', json['db_type']) + '>Oracle</option>';
        ret += '</select>';
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields_below section clearing fl-field-style">'; // style="width:45%;float:left"

        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Username: <font color="red">*</font></span>';
        ret += '<input type="text" name="" req="true" class="form-text obj_prop" id="db_username" placeholder="root" value="' + if_undefinded(json['db_username'], "") + '">';
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields_below section clearing fl-field-style">'; // style="width:45%;float:rigth"
        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Password: </span>';
        ret += '<input type="password"  name="" class="form-text obj_prop" style="width:100%" id="db_password" value="' + if_undefinded(json['db_password'], "") + '">';
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields_below section clearing fl-field-style">'; // style="width:45%;float:left"

        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Port: </span>';
        ret += '<input type="text" name="" class="form-text obj_prop" id="db_port" placeholder="3306" value="' + if_undefinded(json['db_port'], "") + '">';
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields_below section clearing fl-field-style">'; // style="width:45%;float:rigth"

        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Host Name: <font color="red">*</font></span>';
        ret += '<input type="text" name="" req="true" class="form-text obj_prop" id="db_hostname" value="' + if_undefinded(json['db_hostname'], "") + '" placeholder="localhost">';
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields_below section clearing fl-field-style">'; // style="width:45%;float:rigth"

        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Database: <font color="red">*</font></span>';
        ret += '<input type="text" name="" req="true" class="form-text obj_prop" id="db_name" value="' + if_undefinded(json['db_name'], "") + '" placeholder="dbSample">';
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields_below section clearing fl-field-style">';
        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Status: <font color="red">*</font></span>';
        // ret += '<input type="text" name="" class="form-text obj_prop" id="workflow_end-status" data-properties-type="lblFldName" data-object-id="" placeholder="Status" value="">';
        ret += '<select class="fl-input-select" id="is_active" style="width:100%">';
        ret += '<option value="1" ' + setSelected('1', json['is_active']) + '>Active</option>'; //setSelected('1',json['user_access_type'])
        ret += '<option value="0" ' + setSelected('0', json['is_active']) + '>Inactive</option>';
        ret += '</select>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        //ret += '<div class="fields" style="border-top:1px solid #ddd;">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += '<input type="button" class="btn-blueBtn fl-margin-right" value="Save" id="saveExternalConnection" save-type="' + json.type + '" data-id="' + if_undefinded(json['id'], "") + '">';
        ret += '<input type="button" class="btn-blueBtn fl-margin-right fl-default-btn" value="Test Connection" id="test_connection" style="width:110px">';
        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
        ret += '</div>';
        ret += '</div>';
        var newDialog = new jDialog(ret, "", "500", "", "25", function () {

        });
        newDialog.themeDialog("modal2");
        $(".content-dialog-scroll").css({
            "max-height": "400px"
        })
        $(".content-dialog-scroll").perfectScrollbar("update");
        // })
    },
    saveExternalConnection: function (obj) {
        var save_type = $(obj).attr("save-type");
        var field = {};
        var error = 0;

        //for validation
        $("#external_database_form_container input[req='true']").each(function () {
            var value = $.trim($(this).val());
            if (value == "") {
                error++;
                return true;
            }
        })

        if (error > 0) {
            showNotification({
                message: "Please fill out required field.",
                type: "error",
                autoClose: true,
                duration: 3
            })
            return false;
        }

        /*
         Collect textbox and combo box
         */
        $("#external_database_form_container input, #external_database_form_container select").each(function () {
            var value = $.trim($(this).val());
            console.log($(this))
            field['' + $(this).attr("id") + ''] = value;
        })
        if (save_type == "1") {
            field['action'] = "addExternalDB";
        } else if (save_type == "2") {
            field['id'] = $(obj).attr("data-id");
            field['action'] = "updateExternalDB";
        }
        ui.block();
        $.post("/ajax/external_database", field, function (data) {
            if (data == "duplicate") {
                showNotification({
                    message: "Your connection name has already in use, Please change to avoid any conflict.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                })
            } else if (data > 0) {
                var message = "Connection successfully created!";
                if (save_type == "2") {
                    message = "Connection successfully updated!";
                }
                showNotification({
                    message: message,
                    type: "success",
                    autoClose: true,
                    duration: 3
                })
                $(".loadDBConnection").dataTable().fnDestroy();
                ExternalDBDataTable.defaultData();
                $("#popup_cancel").trigger("click");
            } else {
                showNotification({
                    message: "Oooops, There's something wrong. Please contact your administrator for assistance",
                    type: "error",
                    autoClose: true,
                    duration: 3
                })
                console.log(data)
                console.log("error")
            }
            ui.unblock();
        })


        //validation empty

    },
    deleteExternalConnection: function (obj) {
        var id = $(obj).attr("data-id");
        var newConfirm = new jConfirm("Are you sure you want to delete this connection?", 'Confirmation Dialog', '', '', '', function (r) {
            if (r == true) {
                //get forms
                // ui.block();
                $.post("/ajax/external_database",
                        {
                            action: "deleteExternalDB",
                            id: id
                        }, function (data) {
                    $(".loadDBConnection").dataTable().fnDestroy();
                    ExternalDBDataTable.defaultData();
                    // ui.unblock();
                    showNotification({
                        message: "The select connection has been successfully deleted.",
                        type: "success",
                        autoClose: true,
                        duration: 3
                    });
                })
            }
        })
        newConfirm.themeConfirm("confirm2", {'icon': '<i class="fa fa-question-circle fl-margin-right" font-size:15px;"></i>'});
    },
    testConnection: function () {
        var field = {};
        var error = 0;

        //for validation
        $("#external_database_form_container input[req='true']").each(function () {
            var value = $.trim($(this).val());
            if (value == "") {
                error++;
                return true;
            }
        })

        if (error > 0) {
            showNotification({
                message: "Please fill out required field.",
                type: "error",
                autoClose: true,
                duration: 3
            })
            return false;
        }

        /*
         Collect textbox and combo box
         */
        $("#external_database_form_container input, #external_database_form_container select").each(function () {
            var value = $.trim($(this).val());
            // console.log($(this))
            field['' + $(this).attr("id") + ''] = value;
        })

        field['action'] = "test_connection";
        console.log("Connecting...")
        ui.block();
        $.post("/ajax/external_database", field, function (data) {
            ui.unblock();
            if (data == "200") {
                showNotification({
                    message: "Connection is up and running.",
                    type: "success",
                    autoClose: true,
                    duration: 3
                })
            } else {
                showNotification({
                    message: "Connection failed.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                })
                console.log(data)
            }

        })
    }
}