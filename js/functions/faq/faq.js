(function(){

	$(document).ready(function(){
		faqs.init('.fl-faq-container');
		faqs.searchFaq('.fl-faq-container');
	});
	
	var fl_faq_field = $('#fl-faqField-wrapper');
	
	faqs = {

		init: function(container){
			
			var self = this;
			self.createFaq(container);
			self.editFaq(container);
			
		},

		createFaq: function(container){

			var self = $(this);
			var faqMessage = "Your Frequently asked question has been saved";
			var conf = "Are you sure you want to save this FAQ?";
		
			// var input_faq_ctr = $(container).find('input[type="text"]').last().attr('name').match(/[0-9]/)[0];
			// var textarea_faq_ctr = $(container).find('textarea.faq_response').last().attr('name').match(/[0-9]/)[0];
			
			// var input_faq = Number(input_faq_ctr);
			// var textarea_faq = Number(textarea_faq_ctr);

			var add_faq_btn = $(container).find('.add_faq');
			var remove_faq_btn = $(container).find('.remove_faq');
			var save_faq_btn = $(container).find('.save_faq');
			
			$(add_faq_btn).on('click', function(){
				
				var faq_question = $('<div class="fl-faqField"><div class="section clearing fl-field-style"><div class="column div_1_of_1"><input type="text" class="form-text faq_question new-faq" name="faq_question" placeholder="Question" faq-new="true"></div></div><div class="section clearing fl-field-style"><div class="column div_1_of_1"><textarea class="new-faq form-textarea faq_response" name="faq_response" placeholder="Response" faq-new="true"></textarea></div></div></div>');
				
				$(faq_question).prependTo(fl_faq_field);
				
			});

			$('body').on('focus', '.new-faq[faq-new="true"]', function(){
				console.log("new instance");
				faqMessage = "Your Frequently asked question has been saved";
				conf = "Are you sure you want to save this FAQ?";
			});

			$('body').on('focus', '.old-faq', function(){
				console.log("update instance");
				faqMessage = "Your Frequently asked question has been successfully updated.";
				conf = "Are you sure you want to update this FAQ?";
			});
			


			$(remove_faq_btn).on('click', function(){

				$(fl_faq_field).children().not(':first').last().remove();

				var newAlert = new jAlert("To resave your list of frequently asked questions please click save.", "", "", "", "", function () {
                });
                newAlert.themeAlert("jAlert2", {
                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                });
                
			
			});

			$(save_faq_btn).on('click', function(){

			

				//ui.block();
				var container_faq = $(container).find('[id="fl-faqField-wrapper"]');
				var faq_question = $(container).find('input[type="text"].faq_question');
				var faq_response =  $(container).find('textarea.faq_response');



				
				var collector = [];
				container_faq.children(".fl-faqField").each(function(){
					collector.push({
						"question":$(this).find(faq_question).val(),
						"response":$(this).find(faq_response).val()
					});
				
					//console.log($(faq_question).val(), $(faq_response).val())
			
				});
				//console.log("before ajax post ", collector);
				

				if ($.trim($(faq_question)).length == 0  || $.trim($(faq_response).val()).length == 0) {
					showNotification({
		                message: "Please no empty fields",
		                type: "error",
		                autoClose: true,
		                duration: 3
		            });
		            ui.unblock();
				}else {
					ui.unblock();
					
	                var newConfirm = new jConfirm(conf, 'Confirmation Dialog', '', '', '', function (r) {
	                	if (r) {
	                		$.post("/ajax/faqdetails", {"faq_data":collector, action:"setFaq"}, function(data){
								console.log("save ", collector);
								
								showNotification({
					                //message: "Your Frequently asked question has been saved.",
					                message:faqMessage,
					                type: "",
					                autoClose: true,
					                duration: 3
					            });
							});
	                	};
	                })
	                newConfirm.themeConfirm("confirm2", {
	                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
	                });
				}

			});
		
		},
		
		editFaq: function(container){
			
			$.post("/ajax/faqdetails", {action:"editFaq"},function(data) {
				//console.log(data)
				if(!data){
					return '';
				}
				var parse_data = JSON.parse(data);
				var edit_faq_data = JSON.parse(parse_data[0].faq_data);
				console.log(edit_faq_data)

				edit_faq_data.forEach( function(item) { 
				    
				    var input_q = $('<div class="fl-faqField"><div class="section clearing fl-field-style"><div class="column div_1_of_1" style="position:relative;"><input type="text" class="form-text faq_question old-faq" name="faq_question[]" value="'+item.question+'"><i class="fa fa-minus fl-remove-this-faq isCursorPointer tip" data-original-title="Delete This" data-placement="top" style="position: absolute; right: 3px; top: -5px;"></i></div></div><div class="section clearing fl-field-style"><div class="column div_1_of_1"><textarea class="form-textarea faq_response old-faq" name="faq_response[]">'+item.response+'</textarea></div></div></div>');
				    $(input_q).appendTo('#fl-faqField-wrapper');
				    $('.fl-return-view-faq').append('<div class="fl-faq-content"> <h1><i class="fa fa-question-circle"></i> '+item.question+'</h1>' + '<p><i class="fa fa-quote-left"></i> '+item.response+' <i class="fa fa-quote-right"></i></p></div>');
				    $('.fl-remove-this-faq').tooltip();
				    //console.log(item.question);

				});


			    $('.fl-remove-this-faq').on('click', function(){
			    	$(this).parents('.fl-faqField').remove();
			    	var newAlert = new jAlert("To resave your list of frequently asked questions please click save.", "", "", "", "", function () {
	                });
	                newAlert.themeAlert("jAlert2", {
	                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
	                });
			    })


			});


		},

		updateFaq: function(){

		},

		deleteFaq: function(){

		},

		searchFaq: function(container){

			var searchBtn = $(container).find($('.fl-ui-search-button'));
			var searchFieldEmpty = $(container).find($('.fl-search-field'));

			searchBtn.each(function(){
				
				$(this).on('click', function(){
					
					var searchField = $(this).parent().children($('.fl-search-field')).val();
					var searchValue = $(this).parents($(container)).find('.faq_question');
					var searValueView = $(this).parents($(container)).find('.fl-faq-content').children('h1');

					var item =  "";
					var searthItem = "";
					var ctr = 0;

					searchValue.each(function(){
						 
						searthItem = $(this).val();
						item = searthItem.indexOf(searchField);

						if (item >= 0) {
							$(this).parents('.fl-faqField').show();
							ctr++;
						}else {
							$(this).parents('.fl-faqField').hide();
							
						}

					});

					searValueView.each(function(){

						searchItem = $(this).text();
						item = searchItem.indexOf(searchField);

						if (item >= 0) {
							$(this).parents('.fl-faq-content').show();
							ctr++;
						}else {
							$(this).parents('.fl-faq-content').hide();
						}

					});

					if (ctr == 0) {
						$('.fl-no-match').text('No match found.');
					}else {
						$('.fl-no-match').text(' ');
					}

					searchFieldEmpty.each(function(){
						$(this).on('keyup', function(){
							if ($(this).val() == "") {
								$(this).parents('.fl-faq-container').find('.fl-faq-content').show();
								$(this).parents('.fl-faq-container').find('.fl-faqField').show();
								$('.fl-no-match').text(' ');
							}

						});
					})


				});
			});

			searchFieldEmpty.each(function(){
				$(this).on('keydown', function(){
					
					if (event.which == 13) {

						var searchField = $(this).val();
						var searchValue = $(this).parents($(container)).find('.faq_question');
						var searValueView = $(this).parents($(container)).find('.fl-faq-content').children('h1');

						var item =  "";
						var searthItem = "";
						var ctr = 0;

						searchValue.each(function(){
							 
							searthItem = $(this).val();
							item = searthItem.indexOf(searchField);

							if (item >= 0) {
								$(this).parents('.fl-faqField').show();
								ctr++;
							}else {
								$(this).parents('.fl-faqField').hide();
								
							}

						});

						searValueView.each(function(){

							searchItem = $(this).text();
							item = searchItem.indexOf(searchField);

							if (item >= 0) {
								$(this).parents('.fl-faq-content').show();
								ctr++;
							}else {
								$(this).parents('.fl-faq-content').hide();
							}

						});

						if (ctr == 0) {
							$('.fl-no-match').text('No match found.');
						}else {
							$('.fl-no-match').text(' ');
						}

						searchFieldEmpty.each(function(){
							$(this).on('keyup', function(){
								if ($(this).val() == "") {
									$(this).parents('.fl-faq-container').find('.fl-faq-content').show();
									$(this).parents('.fl-faq-container').find('.fl-faqField').show();
									$('.fl-no-match').text(' ');
								}

							});
						})

					};
				});
				
			});

		}
	}


})();

