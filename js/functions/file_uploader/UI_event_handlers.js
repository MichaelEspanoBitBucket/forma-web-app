
var file_uploader_ui_event_handlers = {
    uploadWidgetId: 0
};

var file_uploader = {
    uploadedFileMap: {},
    alreadyUploadedFiles: [],
    onRemoveCallback: null,
    onAllAttachmentsRemovedCallback: null,
    onAttachmentAddedCallback: null
};

(function () {

    file_uploader_ui_event_handlers.onUploadWidgetReady = function (uploadWidgetId) {
        file_uploader_ui_event_handlers.uploadWidgetId = uploadWidgetId;
    };

    file_uploader_ui_event_handlers.onFileUploadingDone = function (uploadResults) {

        if (uploadResults.error) {
            alert(uploadResults.error_message);
            return;
        }

        var filesUploaded = uploadResults.results;
        var uploadedFilenames = [];

        //  Remove the loading UI for each uploading file               
        for (var index in filesUploaded) {
            var fileName = filesUploaded[index].original_filename;
            var fileId = generateElementIdFromText(fileName);

            uploadedFilenames.push(filesUploaded[index].new_filename);
            file_uploader.uploadedFileMap[fileId] = filesUploaded[index].new_filename;

            $("#" + fileId).find(".fl-image-loading").css("display", "none");
        }

        file_uploader.addFiles(file_uploader_ui_event_handlers.uploadWidgetId, uploadedFilenames);

    };

    file_uploader_ui_event_handlers.onFileSelectedHandler = function (uploadedData) {

        var continueUpload = false;

        if (uploadedData.files) {

            continueUpload = true;

            var totalFileSize = file_uploader.getCurrentUploadedFilesize();
            for (var i = 0; i < uploadedData.files.length; i++) {
                totalFileSize += uploadedData.files[i].size;
            }

            if (totalFileSize > 2000000) {
                file_uploader.clearAttachments(file_uploader_ui_event_handlers.uploadWidgetId);
                alert("Total attachment filesize must not exceed 2MB");
                return;
            }

            var validFiles = [];

            for (var i = 0; i < uploadedData.files.length; i++) {

                var fileType = getFileType(uploadedData.files[i]);

                console.log(file_uploader.alreadyUploadedFiles);

                var fileToken = uploadedData.files[i].lastModified + "" + uploadedData.files[i].name;
                if (file_uploader.alreadyUploadedFiles.indexOf(fileToken) > -1) {
//                    alert(uploadedData.files[i].name + " is already uploaded");

                    $(file_uploader_ui_event_handlers.uploadWidgetId + " > .generic-file-upload").val("");

                    if (uploadedData.files.length === 1) {
                        alert("The selected file is already uploaded.");
                    } else {
                        alert("Some or all of the selected files are already uploaded.");
                    }

                    continueUpload = false;
                    break;
                }

                file_uploader.alreadyUploadedFiles.push(fileToken);

                var reader = new FileReader();
                reader.onload = (function (fileData) {
                    return function (e) {

                        //var fileData = uploadedData.files[i];
                        var fileTextData = e.target.result;
                        var generatedPreview;

                        var fileToken = fileData.lastModified + "" + fileData.name;

                        if (fileType === "image") {
                            fileData.fileTextData = fileTextData;
                            generatedPreview = createImagePreview(fileData, fileToken);
                            appendPreviewPane(generatedPreview, "IMAGE");
                        } else {
                            generatedPreview = createFilePreview(fileData, fileToken);
                            appendPreviewPane(generatedPreview, "OTHER");
                        }

                        var currentFileSize = fileData.size;

                        //  Add the filesize
                        file_uploader.setCurrentUploadedFilesize(totalFileSize + currentFileSize);

                        //  Register events to the newly added UI elements
                        var previewId = $(generatedPreview).attr("id");
                        $("#" + previewId).find(".fl-file-delete").click(function () {

                            var tokenString = $("#" + previewId).attr("file-token");
                            var index = file_uploader.alreadyUploadedFiles.indexOf(tokenString);

                            if (index > -1) {
                                file_uploader.alreadyUploadedFiles.splice(index, 1);
                            }

                            file_uploader.removeAttachment(
                                    file_uploader_ui_event_handlers.uploadWidgetId,
                                    previewId, currentFileSize
                                    );
                        });

                        if (file_uploader.onAttachmentAddedCallback) {
                            file_uploader.onAttachmentAddedCallback();
                        }

                    };
                })(uploadedData.files[i]);  //  Continuous Async

                reader.readAsDataURL(uploadedData.files[i]);
            }

        }

        return continueUpload;
    };

    file_uploader.addFiles = function (uploadWidgetId, files) {

        var uploadedFilenames = $("#" + uploadWidgetId).attr("uploaded-filenames");
        var newFilenames = files.join(",");

        if (newFilenames) {
            if (uploadedFilenames) {
                uploadedFilenames = uploadedFilenames + "," + newFilenames;
            } else {
                uploadedFilenames = newFilenames;
            }
        }

        $("#" + uploadWidgetId).attr("uploaded-filenames", uploadedFilenames);

    };

    file_uploader.registerOnAttachmentRemovedCallback = function (onRemoveCallback) {
        file_uploader.onRemoveCallback = onRemoveCallback;
    };

    file_uploader.clearAttachments = function (uploadWidgetId) {

        $(".fl-file-resource-wrapper").html("");
        $(".fl-file-images-wrapper").html("");

        $(".fl-file-resource-wrapper").css("display", "none");
        $(".fl-file-images-wrapper").css("display", "none");

        $("#" + uploadWidgetId).attr("uploaded-filenames", "");

        file_uploader.setCurrentUploadedFilesize(0);
        file_uploader.alreadyUploadedFiles = [];
    };

    file_uploader.removeAttachment = function (uploadWidgetId, attachmentId, attachmentFilesize) {

        var attachmentClass = $("#" + attachmentId).attr("class");
        var attachmentCount;

        if (attachmentClass === "fl-file-image") {
            attachmentCount = $("#" + uploadWidgetId).find('.fl-file-images-wrapper').children("." + attachmentClass).length;
        } else {
            attachmentCount = $("#" + uploadWidgetId).find('.fl-file-resource-wrapper').children("." + attachmentClass).length;
        }

        if (attachmentCount <= 1) {

            if (attachmentClass === "fl-file-image") {
                $("#" + uploadWidgetId).find('.fl-file-images-wrapper').html("");
                $("#" + uploadWidgetId).find('.fl-file-images-wrapper').css("display", "none");
            } else {
                $("#" + uploadWidgetId).find('.fl-file-resource-wrapper').html("");
                $("#" + uploadWidgetId).find('.fl-file-resource-wrapper').css("display", "none");
            }

            if (file_uploader.onAllAttachmentsRemovedCallback) {
                file_uploader.onAllAttachmentsRemovedCallback();
            }

            file_uploader.setCurrentUploadedFilesize(0);
        } else {
            //  subtract the filesize
            var currentFilesize = file_uploader.getCurrentUploadedFilesize();
            currentFilesize -= attachmentFilesize;
            file_uploader.setCurrentUploadedFilesize(currentFilesize);

            $("#" + attachmentId).remove();
        }

        var uploadedFileNamesString = $("#" + uploadWidgetId).attr("uploaded-filenames");
        var uploadedFilenames = uploadedFileNamesString.split(",");

        var uploadedFileName = file_uploader.uploadedFileMap[attachmentId];
        var fileToRemoveIndex = uploadedFilenames.indexOf(uploadedFileName);

        if (fileToRemoveIndex > -1) {
            uploadedFilenames.splice(fileToRemoveIndex, 1);
        }
        var newUploadedFilenamesString = uploadedFilenames.join(",");

        $("#" + uploadWidgetId).attr("uploaded-filenames", newUploadedFilenamesString);

        if (file_uploader.onRemoveCallback) {
            file_uploader.onRemoveCallback(uploadWidgetId);
        }


    };

    file_uploader.getCurrentUploadedFilesize = function () {
        return parseInt($("#" + file_uploader_ui_event_handlers.uploadWidgetId).attr("uploaded-filesize")) || 0;
    };

    file_uploader.setCurrentUploadedFilesize = function (filesize) {
        $("#" + file_uploader_ui_event_handlers.uploadWidgetId).attr("uploaded-filesize", filesize);
    };

    // <editor-fold desc="Private Functions" defaultstate="collapsed">

    function getFileType(fileData) {
        var mimeType = fileData.type;
        return mimeType.split("/")[0];
    }

    function appendPreviewPane(html, type) {

        if (type === "IMAGE") {
            $(".fl-file-images-wrapper").css("display", "block");
            $(".fl-file-images-wrapper").append(html);
        } else {
            $(".fl-file-resource-wrapper").css("display", "block");
            $(".fl-file-resource-wrapper").append(html);
        }

    }

    function createImagePreview(fileData, fileToken) {

        var fileTextData = fileData.fileTextData;
        var imagePreview = $(".fl-file-uploader-widget-templates").find(".fl-file-image-template").clone();
        $(imagePreview).find(".fl-file-image").attr("id", generateElementIdFromText(fileData.name));
        $(imagePreview).find(".fl-file-image").attr("file-token", fileToken);
        $(imagePreview).find("img").attr("src", fileTextData);

        $(imagePreview).attr("file-token", fileToken);

        return imagePreview.html();

    }

    function createFilePreview(fileData, fileToken) {

        var filePreview = $(".fl-file-uploader-widget-templates").find(".fl-file-template");
        filePreview.find(".fl-file").attr("id", generateElementIdFromText(fileData.name));
        filePreview.find(".fl-file").attr("file-token", fileToken);
        filePreview.find("span").html(fileData.name);

        return filePreview.html();

    }

    function generateElementIdFromText(plainText) {
        var decodedText = htmlEnDeCode.htmlDecode(plainText);
        var filteredSpaceText = decodedText.split(" ").join("_");
        var splittedText = filteredSpaceText.split(".");
        var extensionlessText = filteredSpaceText.replace("." + splittedText[splittedText.length - 1], "");
        var specialCharacterlessText = extensionlessText.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-').replace(/^(-)+|(-)+$/g, '_');

        return specialCharacterlessText;
    }

    // </editor-fold>

}());

var htmlEnDeCode = (function () {
    var charToEntityRegex,
            entityToCharRegex,
            charToEntity,
            entityToChar;

    function resetCharacterEntities() {
        charToEntity = {};
        entityToChar = {};
        // add the default set
        addCharacterEntities({
            '&amp;': '&',
            '&gt;': '>',
            '&lt;': '<',
            '&quot;': '"',
            '&#39;': "'"
        });
    }

    function addCharacterEntities(newEntities) {
        var charKeys = [],
                entityKeys = [],
                key, echar;
        for (key in newEntities) {
            echar = newEntities[key];
            entityToChar[key] = echar;
            charToEntity[echar] = key;
            charKeys.push(echar);
            entityKeys.push(key);
        }
        charToEntityRegex = new RegExp('(' + charKeys.join('|') + ')', 'g');
        entityToCharRegex = new RegExp('(' + entityKeys.join('|') + '|&#[0-9]{1,5};' + ')', 'g');
    }

    function htmlEncode(value) {
        var htmlEncodeReplaceFn = function (match, capture) {
            return charToEntity[capture];
        };

        return (!value) ? value : String(value).replace(charToEntityRegex, htmlEncodeReplaceFn);
    }

    function htmlDecode(value) {
        var htmlDecodeReplaceFn = function (match, capture) {
            return (capture in entityToChar) ? entityToChar[capture] : String.fromCharCode(parseInt(capture.substr(2), 10));
        };

        return (!value) ? value : String(value).replace(entityToCharRegex, htmlDecodeReplaceFn);
    }

    resetCharacterEntities();

    return {
        htmlEncode: htmlEncode,
        htmlDecode: htmlDecode
    };
})();
