
//  globals
var fileUploaderEventRegistry = {};

(function () {

    $(document).ready(function () {

        var uploadTrigger = $(".fl-file-uploader-widget").attr("click-trigger-class");
        var uploadWidgetId = $(".fl-file-uploader-widget").attr("id");

        var uploadWidgetUploadInput = $("#" + uploadWidgetId).find(".generic-file-upload");
        var uploadWidgetUploadForm = uploadWidgetUploadInput.parent(".uploader");

        //  register variable events 
        $("." + uploadTrigger).click(function () {
            uploadWidgetUploadInput.click();
        });

        uploadWidgetUploadInput.on("change", function () {
            var uploadedData = this;

            var continueUpload = file_uploader_ui_event_handlers.onFileSelectedHandler(uploadedData);

            if (continueUpload) {
                file_uploader_backend_event_handlers.uploadFiles(
                        uploadWidgetUploadForm,
                        file_uploader_ui_event_handlers.onFileUploadingDone
                        );
            }
        });

        file_uploader_ui_event_handlers.onUploadWidgetReady(uploadWidgetId);

    });

}());
