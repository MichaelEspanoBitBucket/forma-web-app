var formbuilder_context_menu = {
	delegate: ".formbuilder_ws",
	menu: [
		{title:  "<i class='fa fa-files-o'></i> Copy <kbd> | Ctrl+C</kbd>", action: function(event, ui){
					              	
					             	if($(".component-ancillary-focus").length>=1){
					             		$(document).trigger('copy');
					             		$(document).contextmenu("enableEntry", "paste", true);
					             	}
					              	
					             },cmd: "copy"},

		{title: "<i class='fa fa-clipboard'></i> Paste <kbd> | Ctrl+V</kbd>", action:function(event, ui){
					              	$(document).trigger('paste')
					             },cmd: "paste", disabled:true}, 
    {title: "<i class='fa fa-scissors'></i> Cut <kbd> | Ctrl+X</kbd>", action:function(event, ui){
                          $(document).trigger('cut');
                          $(document).contextmenu("enableEntry", "paste", true);
                       },cmd: "cut"}, 
	],
	beforeOpen: function(event,ui){
		
    
    var pathname = window.location.pathname; 
    if($(".component-ancillary-focus").length>=1 && pathname =="/user_view/formbuilder"){
			$(document).contextmenu("enableEntry", "copy", true);
      $(document).contextmenu("enableEntry", "cut", true);

		}
		else{
			$(document).contextmenu("enableEntry", "copy", false);
      $(document).contextmenu("enableEntry", "cut", false);					
		}
    if(localStorage['variableToStore'] !== "" && pathname =="/user_view/formbuilder"){

      $(document).contextmenu("enableEntry", "paste", true);
    }
	}
}
var paste_storage = $();
var embed_context_menu = {
		delegate: ".embed-context-menu",
		menu: [
        /*COPY context-menu*/
			  { 
          title:  "<i class='fa fa-files-o'></i> Copy ",
          action: function(event, ui){
      				var target = $(ui.target);
      					
      				
      				if(target.is('.embed-context-menu')){
                paste_storage = target.find('td').last();
      					paste_storage.find('#copyTDEmbedded').trigger('click');
      				}	
              else{
                paste_storage = target.closest('.embed-context-menu').find('td').last();
                paste_storage.find('#copyTDEmbedded').trigger('click');
              }              	
             	$(document).contextmenu("enableEntry", "paste", true);
                	
          },
          cmd: "copy"
        },
        /*PASTE context-menu*/
			  { 
          title: "<i class='fa fa-clipboard'></i> Paste ",
          action:function(event, ui){
              paste_storage.find('#pasteTDEmbedded').trigger('click');
	          	$(document).contextmenu("enableEntry", "paste", false);
	        },
          cmd: "paste",
          disabled:true
        },
        /*EDIT context-menu*/
        { 
          title: "<i class='fa fa-pencil'></i> Edit ",
          action:function(event, ui){
              
              
            var target = $(ui.target);
            if(target.is('.embed-context-menu')){
               target.find('td').last().find('#editDEmbedded').trigger('click');
            }
            else{
               target.closest('.embed-context-menu').find('td').last().find('#editDEmbedded').trigger('click');
            }
          },
          cmd: "edit"
        }, 
        /*ADD ROW context-menu*/ 
  		  // { 
      //     title: "<i class='fa fa-plus'></i> Add Row ",
      //     action:function(event, ui){
      //       	var target = $(ui.target),
      //       	append_before_ele = target.closest('tbody').find('tr.appendNewRecord:last');
      //       	var tr_clone = embed_row_fixer(target);
      //         $(append_before_ele).before(tr_clone);
      //       	$(document).contextmenu("enableEntry", "add", false);
      //         $(document).contextmenu("enableEntry", "copy", false);
      //         getEmbedData(tr_clone);
      //         if($(tr_clone).closest('table').is('.enabled_embed_action_click_number_class')){
      //           var last_number_row = $(tr_clone).siblings('.embed-context-menu').last().find('td:eq(0)').html();
      //           $(tr_clone).find('td:eq(0)').html(Number(last_number_row)+1);
      //           $(tr_clone).find('input:eq(0)').focus();
      //         }
              
            	
			   //  },
      //     cmd: "add"
      //   }, 
        /*DELETE context-menu*/
        { 
          title:  "<i class='fa fa-trash-o'></i> Delete ",
          action: function(event, ui){
               var target = $(ui.target);
               if(target.is('.embed-context-menu')){
                  target.find('td').last().find('#deleteTDEmbedded').trigger('click');
               }
               else{
                  target.closest('.embed-context-menu').find('td').last().find('#deleteTDEmbedded').trigger('click');
               }
          },
          cmd: "delete"
        },    
		],
		beforeOpen: function(event,ui){
      
			 
       if($(ui.target).closest('tbody').find('.activeActionClass').length > 0){

           // $(document).contextmenu("enableEntry", "add", false);
            $(document).contextmenu("enableEntry", "edit", false);

       }
       else{

          //$(document).contextmenu("enableEntry", "add", true);
          $(document).contextmenu("enableEntry", "edit", true);
       }

       if($(ui.target).closest('.setOBJ').find('.embed_newRequest').length < 1){
           // $(document).contextmenu("enableEntry", "add", false);
        }
       if($(ui.target).closest('.setOBJ').find('#copyTDEmbedded').length < 1){
         $(document).contextmenu("enableEntry", "copy", false);
       }
        if($(ui.target).closest('.setOBJ').find('.editEmbedClass').length < 1){

         $(document).contextmenu("enableEntry", "edit", false);
       }
       if($(ui.target).closest('.setOBJ').find('#deleteTDEmbedded').length < 1){

         $(document).contextmenu("enableEntry", "delete", false);
       }
		},

}
		