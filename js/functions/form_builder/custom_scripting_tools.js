var scripting_tools = {
    "getFieldsData":function (){
        var data_field_name = "";
        var field_type = "";
        var data_field_input_type = "";
        var data_field_input_type = "";
        var data_field_default_type = "";
        var data_field_formula = "";
        var data_field_label = "";
        var visibility_formula = "";
        var json = [];
        var readonly = "";
        var data_field_formula_computed = "";
        var data_field_formula_static = "";
        var data_field_formula_middleware = "";
        var refresh_fields_onchange_attr = "";
        
       
        $(".setObject").each(function(eqi_setObj) {
            readonly = "";
             var multiple_values = [];
            if (
                    $(this).attr("data-type") == "labelOnly" ||
                    $(this).attr("data-type") == "createLine" ||
                    $(this).attr("data-type") == "table" ||
                    $(this).attr("data-type") == "tab-panel" ||
                    // $(this).attr("data-type") == "listNames" || inalis ko to aaron para masama yung names sa tbfields -- JEWEL 
                    $(this).attr("data-type") == "imageOnly" ||
                    $(this).attr("data-type") == "button" ||
                    $(this).attr("data-type") == "embeded-view" ||
                    $(this).attr("data-type") == "accordion" ||
                    $(this).attr("data-type") == "details-panel" ||
                    $(this).attr("data-type") == "formDataTable"
                    ) {
                return true; //preventing to insert it to the database
            }
            counted_id = $(this).attr("data-object-id");
            data_field_name = $(this).find(".getFields_" + counted_id).attr("name").replace("[]", "");
            field_type = $(this).attr("data-type");
            data_field_input_type = $(this).find(".getFields_" + counted_id).attr("data-input-type");
            data_field_default_type = $(this).find(".getFields_" + counted_id).attr("default-type");
            visibility_formula = $(this).find(".getFields_" + counted_id).attr("visible-formula");
            // if(data_field_default_type=="computed"){
                data_field_formula_computed = $(this).find(".getFields_" + counted_id).attr("default-formula-value");    
            // }else if(data_field_default_type=="static"){
                data_field_formula_static = $(this).find(".getFields_" + counted_id).attr("default-static-value");    
            // }else if(data_field_default_type=="middleware"){
                data_field_formula_middleware = $(this).find(".getFields_" + counted_id).attr("default-middleware-value");
            // }else{
                // data_field_formula="";
            // }
            
            refresh_fields_onchange_attr = $(this).find(".getFields_" + counted_id).is('[lookup-event-trigger="true"]');

            // console.log("dataFieldFOrmula",data_field_formula)
            data_field_label = $(this).find("#lbl_" + counted_id).text();
            //readonly = $(this).find(".getFields_" + counted_id+"[readonly]").attr('readonly');
            
            if($(this).find(".getFields_" + counted_id+"[readonly]").is('[readonly]')){
                // console.log("pipipiipcklist",$(this))
                readonly = 'readonly';
            }
            // console.log("length",counted_id,$(this),$(this).find(".getFields_" + counted_id+"[type='radio']"));
            if($(this).is('[data-type="checkbox"]')){
                multiple_values.push({});
                $(this).find(".getFields_" + counted_id+"[type='checkbox']").each(function(){
                    multiple_values[multiple_values.length-1][""+$(this).val()] = $(this).siblings('.checkbox-input-label').html();
                });
            }
            if($(this).is('[data-type="radioButton"]')){
                multiple_values.push({});
                $(this).find(".getFields_" + counted_id+"[type='radio']").each(function(){

                    
                    multiple_values[multiple_values.length-1][""+$(this).val()] = $(this).siblings('.radio-input-label').html();
                });
            }
            if($(this).is('[data-type="dropdown"]')||$(this).is('[data-type="selectMany"]')){
                multiple_values.push({});
                // console.log("option",$(this).find("select.getFields_" + counted_id).children('option'))
                $(this).find("select.getFields_" + counted_id).children('option').each(function(){
                    multiple_values[multiple_values.length-1][""+$(this).val()] = $(this).html();
                    // console.log("multiple",multiple_values);
                });
            }
            
            if($(this).is('[data-type="pickList"]')){
                var pl_button = $(this).find('a.pickListButton');
                var pl_class = pl_button.attr("class");
                var pl_display_columns = pl_button.attr("display_columns")||"";
                var pl_display_columns_sequence = pl_button.attr("display_columns_sequence")||"";
                var pl_formname = pl_button.attr("formname")||"";
                var pl_return_field_name = pl_button.attr('return-field-name')||"";
                var pl_type = pl_button.attr("picklist-type")||"";
                var pl_button_id = pl_button.attr("picklist-button-id")||"";
                var pl_return_field = pl_button.attr("return-field")||"";
                var pl_condition_selection_type = pl_button.attr("selection-type")||"";
                var pl_condition = pl_button.attr('condition')||"";
                var pl_form_id = pl_button.attr("form-id")||"";
                var pl_name = pl_button.attr("name")||"";
                var pl_enable_entry = pl_button.attr('enable-add-entry')||"";

                var picklist_json_attr = {
                    "class":pl_class,
                    "display_columns":pl_display_columns,
                    "display_columns_sequence":pl_display_columns_sequence,
                    "formname":pl_formname,
                    "return-field-name":pl_return_field_name,
                    "picklist-type":pl_type,
                    "picklist-button-id":pl_button_id,
                    "return-field":pl_return_field,
                    "selection-type":pl_condition_selection_type,
                    "form-id":pl_form_id,
                    "name":pl_name,
                    "condition":pl_condition,
                    "enable_entry":pl_enable_entry
                }

            }
            if($(this).is('[data-type="listNames"]')){
                var pl_button = $(this).find('a.viewNames');
                var pl_class = pl_button.attr("class");
                var pl_return_field = pl_button.attr("return-field")||"";
                var pl_list_type_selection = pl_button.attr("list-type-selection")||"";
                var picklist_json_attr = {
                    "class":pl_class,
                    "return-field":pl_return_field,
                    "list-type-selection":pl_list_type_selection
                    
                }

            }
           
            
            
            if ($(this).find(".getFields_" + counted_id).attr("name")) {
                json.push({
                    "data_object_id":if_undefinded(counted_id,""),
                    "data_field_name":if_undefinded(data_field_name,""),
                    "data_field_input_type":if_undefinded(data_field_input_type,""),
                    "data_field_default_type":if_undefinded(data_field_default_type,""),
                    "data_field_formula":if_undefinded(data_field_formula,""),
                    "field_type":if_undefinded(field_type,""),
                    "data_field_visibility_formula":if_undefinded(visibility_formula,""),
                    "data_field_label":if_undefinded(data_field_label,""),
                    "readonly":if_undefinded(readonly, ""),
                    "data_multiple_values": JSON.stringify(multiple_values)||"",
                    "other_attributes": JSON.stringify(picklist_json_attr)||"",
                    "data_field_formula_static":if_undefinded(data_field_formula_static, ""),
                    "data_field_formula_computed": JSON.stringify(data_field_formula_computed)||"",
                    "data_field_formula_middleware": JSON.stringify(data_field_formula_middleware)||"",
                    "refresh_fields_onchange_attr": refresh_fields_onchange_attr||""
                });
            }
        });
        // console.log("json",json)
        json = json.map(function(a,b){
            return $.extend(a,{"index":b});
        });
      return json;
    },

    "PrintKeyValuePairFormulaList":function (print_type){
        var temp2 = getFieldsData();
        var field_computed = "";
        var replace_fields = [
            '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now'
        ];
        var replace_functions = ['@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
            '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
            '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
            '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
            '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
            '@GetAuth', "@FormatDate","@NumToWord",
            "@Head","@AssistantHead","@GetData",
            "@LookupAuthInfo",
            "@GetAVG","@GetMin","@SDev","@GetMax",
            "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
            "@StrFind","@StrReplace", "@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
            "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource","@GetLocation",
            "@GetMonth","@GetFileName","@CustomScript", "@ArraySearchBy"];
        var replace_fields2 = [];
        var data_field_name = "";

        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            replace_fields.push("@"+data_field_name+"(?![a-zA-Z0-9_])");
        }
        var regexz = new RegExp(replace_fields.join("|"),"g");
        var regexz_func = new RegExp(replace_functions.join("|"),"g");
        for(var i in temp2){
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            if(field_computed!=""){
                console.log("$formatted_data[\"computed_value\"][\""+temp2[i]['data_field_name']+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );

            }
        }
    },
    "PrintKeyValuePairVisibilityFormulaList":function (){
        var temp2 = getFieldsData();
        var field_computed = "";
        var replace_fields = [
            '@Requestor(?![a-zA-Z0-9_])', '@RequestID(?![a-zA-Z0-9_])', '@Status(?![a-zA-Z0-9_])', '@CurrentUser(?![a-zA-Z0-9_])', '@Department(?![a-zA-Z0-9_])', '@TrackNo(?![a-zA-Z0-9_])', '@Today(?![a-zA-Z0-9_])', '@Now(?![a-zA-Z0-9_])'
        ];
        var replace_functions = ['@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
            '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
            '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
            '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
            '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
            '@GetAuth', "@FormatDate","@NumToWord",
            "@Head","@AssistantHead","@GetData",
            "@LookupAuthInfo",
            "@GetAVG","@GetMin","@SDev","@GetMax",
            "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
            "@StrFind","@StrReplace", "@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
            "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource","@GetLocation",
            "@GetMonth","@GetFileName","@CustomScript", "@ArraySearchBy"];
        var replace_fields2 = [];
        var data_field_name = "";

        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            replace_fields.push("@"+data_field_name+"(?![a-zA-Z0-9_])");
        }
        var regexz = new RegExp(replace_fields.join("|"),"g");
        var regexz_func = new RegExp(replace_functions.join("|"),"g");
        for(var i in temp2){
            field_computed = $.trim(temp2[i]['data_field_visibility_formula']);
            if(field_computed!=""){
                console.log("$formatted_data[\"visibility\"][\""+temp2[i]['data_field_name']+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
            }
        }
        $('[aria-controls][field-visible="computed"][visible-formula]:not([visible-formula=""])').each(function(){
            var field_computed = $.trim($(this).attr("visible-formula"));
            console.log("$formatted_data[\"visibility\"][\"form-tab-panel\"][\""+$(this).attr("aria-controls")+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
        });
        $('.embed-view-container[field-visible="computed"][visible-formula]:not([visible-formula=""])').each(function(){
            var doi = $(this).parents('.setObject[data-object-id]').attr("data-object-id");
            var field_computed = $.trim($(this).attr("visible-formula"));
            console.log("$formatted_data[\"visibility\"][\"embed\"][\""+doi+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
        });
    },
    "PrintAllRelatedSourceFieldNames":function (){
        var temp2 = getFieldsData();
        var field_computed = "";
        var replace_fields = [
            '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now'
        ];
        var replace_functions = ['@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
            '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
            '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
            '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
            '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
            '@GetAuth', "@FormatDate","@NumToWord",
            "@Head","@AssistantHead","@GetData",
            "@LookupAuthInfo",
            "@GetAVG","@GetMin","@SDev","@GetMax",
            "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
            "@StrFind","@StrReplace", "@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
            "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource","@GetLocation",
            "@GetMonth","@GetFileName","@CustomScript", "@ArraySearchBy"];
        var replace_fields2 = [];
        var data_field_name = "";

        var regex_get_fieldnames = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*","g");
        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            // if(temp2[i]['refresh_fields_onchange_attr'] == true||temp2[i]['refresh_fields_onchange_attr'] == "true"){
                console.log(" \""+data_field_name+"\" => array( \""+ $.unique(( field_computed.match(regex_get_fieldnames)||[] ).map(function(vals){
                    if( vals && replace_functions.indexOf(vals) <= -1 && replace_fields.indexOf(vals) <= -1 ){
                        return vals.replace("@","");
                    }else{
                        return null;
                    }
                }).filter(Boolean)).join("\",\"") +"\" ) , " );
            // }
        }
    },
    "GetAllRelatedFieldNamesAt":function (selected_fieldname){
        var temp2 = getFieldsData();
        var temp3 = temp2;
        var field_computed = "";
        var replace_fields = [
            '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now'
        ];
        var replace_functions = ['@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
            '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
            '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
            '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
            '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
            '@GetAuth', "@FormatDate","@NumToWord",
            "@Head","@AssistantHead","@GetData",
            "@LookupAuthInfo",
            "@GetAVG","@GetMin","@SDev","@GetMax",
            "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
            "@StrFind","@StrReplace", "@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
            "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource","@GetLocation",
            "@GetMonth","@GetFileName","@CustomScript", "@ArraySearchBy"];
        var replace_fields2 = [];
        var data_field_name = "";

        var regex_get_fieldnames = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*","g");
        var temp_get_array_fieldnames = [];
        var collected_related_fieldnames = [];
        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            temp_get_array_fieldnames = $.unique(( field_computed.match(regex_get_fieldnames)||[] ).map(function(vals){
                if( vals && replace_functions.indexOf(vals) <= -1 && replace_fields.indexOf(vals) <= -1 ){
                    return vals.replace("@","");
                }else{
                    return null;
                }
            }).filter(Boolean));
            if(temp_get_array_fieldnames.indexOf(selected_fieldname) >= 0){
                collected_related_fieldnames.push(data_field_name);
            }
            // if(temp2[i]['refresh_fields_onchange_attr'] == true||temp2[i]['refresh_fields_onchange_attr'] == "true"){
                // console.log(" \""+data_field_name+"\" => array( \""+ .join("\",\"") +"\" ) , " );
            // }
        }
        return $.unique(collected_related_fieldnames)||[];
    },
    "PrintGetAllRelatedFieldNames":function (){
        var temp2 = getFieldsData();
        var field_computed = "";
        var replace_fields = [
            '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now'
        ];
        var replace_functions = ['@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
            '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
            '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
            '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
            '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
            '@GetAuth', "@FormatDate","@NumToWord",
            "@Head","@AssistantHead","@GetData",
            "@LookupAuthInfo",
            "@GetAVG","@GetMin","@SDev","@GetMax",
            "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
            "@StrFind","@StrReplace", "@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
            "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource","@GetLocation",
            "@GetMonth","@GetFileName","@CustomScript", "@ArraySearchBy"];
        var replace_fields2 = [];
        var data_field_name = "";

        var regex_get_fieldnames = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*","g");
        var tempral_array = [];
        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            temporal_array = GetAllRelatedFieldNamesAt(data_field_name).filter(Boolean);

            for(var ii in temporal_array){
                temporal_array = $.unique(temporal_array.concat(GetAllRelatedFieldNamesAt(temporal_array[ii]).filter(Boolean)));
            }

            if(temporal_array.indexOf(data_field_name) >= 0){
                delete temporal_array[ temporal_array.indexOf(data_field_name) ];
                temporal_array = temporal_array.filter(Boolean);
            }
            
            if(temporal_array.length >= 1){
                // if(temp2[i]['refresh_fields_onchange_attr'] == true||temp2[i]['refresh_fields_onchange_attr'] == "true"){
                    console.log(" \""+data_field_name+"\" => array( \""+ temporal_array.join("\",\"") +"\" ) , " );
                // }
            }
        }
    },
    //getting field name,s
    "PrintInitializationOfFieldNames":function (){
        var temp2 = getFieldsData();
        var field_computed = "";
        var replace_fields = [];
        var replace_fields2 = [];
        var data_field_name = "";

        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            console.log("$myvar['"+data_field_name+"'] = $_POST['"+data_field_name+"']?:\"\";");
        }
    },
    "ResetRemoveFormFormulaSettings":function (){
        ComputedToStatic();
        $('[lookup-event-trigger="true"]').attr("lookup-event-trigger","qwerty");
        // $('[field-visible="computed"][visible-formula]:not([visible-formula=""])').attr("visible-formula","");
    },
    "HTMLOfficialPrintCustomScript":function (){
        var temp2 = getFieldsData();
        var field_computed = "";
        var replace_fields = [
            '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now'
        ];
        var collect = {
            print:"",
            printHtml:"",
            log:function(param){
                this.print += (param||"")+"\n";
            }
        };
        var replace_functions = ['@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
            '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
            '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
            '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
            '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
            '@GetAuth', "@FormatDate","@NumToWord",
            "@Head","@AssistantHead","@GetData",
            "@LookupAuthInfo",
            "@GetAVG","@GetMin","@SDev","@GetMax",
            "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
            "@StrFind","@StrReplace", "@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
            "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource","@GetLocation",
            "@GetMonth","@GetFileName","@CustomScript", "@ArraySearchBy"];
        var replace_fields2 = [];
        var data_field_name = "";


        for(var i in replace_fields){
            collect.log("//"+replace_fields[i].replace("@",""));
            collect.log("$myvar['"+replace_fields[i].replace("@","")+"'] = $_POST['"+replace_fields[i].replace("@","")+"']?:\"\";");
            collect.log("\n");
        }

        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            replace_fields.push("@"+data_field_name+"(?![a-zA-Z0-9_])");
        }
        var regexz = new RegExp(replace_fields.join("|"),"g");
        var regexz_func = new RegExp(replace_functions.join("|"),"g");
        var temporary2 = "";

        for(var i in temp2){
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            data_field_name = $.trim(temp2[i]['data_field_name']);
            
                collect.log("//"+data_field_name);
                collect.log("$myvar['"+data_field_name+"'] = $_POST['"+data_field_name+"']?:\"\";");
                if(field_computed!=""){
                    if(temp2[i]["data_field_default_type"] == "computed"){
                        temporary2 = field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); });
                        if(temporary2[0] == "\"" && temporary2[temporary2.length-1] == "\""){
                            temporary2 = temporary2.substr(1, temporary2.length -2 );
                        }
                        collect.log("$myvar['"+data_field_name+"'] = "+temporary2+" ;" );
                    }
                }
                collect.log("\n");

        }
        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            if(temp2[i]["data_field_default_type"] == "computed"){
                if(field_computed!=""){
                    if(temp2[i]['field_type'] == "dropdown" || temp2[i]['field_type'] == "selectMany"){
                        //collect.log("$formatted_data[\"change_list_computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }else{
                        collect.log("$formatted_data[\"computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }
                    
                }
            }
            
        }
        collect.log("\n");
        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            if(field_computed!=""){
                if(temp2[i]["data_field_default_type"] == "computed"){
                    if(temp2[i]['field_type'] == "dropdown" || temp2[i]['field_type'] == "selectMany"){
                        collect.log("$formatted_data[\"change_list_computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }else{
                        //collect.log("$formatted_data[\"computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }
                }
            }
        }
        collect.log("\n");
        for(var i in temp2){
            field_computed = $.trim(temp2[i]['data_field_visibility_formula']);
            if(field_computed!=""){
                collect.log("$formatted_data[\"visibility\"][\""+temp2[i]['data_field_name']+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
            }
        }
        $('[aria-controls][field-visible="computed"][visible-formula]:not([visible-formula=""])').each(function(){
            var field_computed = $.trim($(this).attr("visible-formula"));
            collect.log("$formatted_data[\"visibility\"][\"form-tab-panel\"][\""+$(this).attr("aria-controls")+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
        });
        $('.embed-view-container[field-visible="computed"][visible-formula]:not([visible-formula=""])').each(function(){
            var doi = $(this).parents('.setObject[data-object-id]').attr("data-object-id");
            var field_computed = $.trim($(this).attr("visible-formula"));
            collect.log("$formatted_data[\"visibility\"][\"embed\"][\""+doi+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
        });

        collect.log('    //return process');
        collect.log('    $print_debugger .= "\\$myvar = ".json_encode($myvar);');
        collect.log('    $print_debugger .= "\\n\\n";');
        collect.log('    $print_debugger .= "\\$personnel_info_SCGEmpID = ".json_encode($personnel_info_SCGEmpID);');
        collect.log('    ');
        collect.log('    if( ( $selected_fields && count($selected_fields)>0 )&& ( $excluded_fields && count($excluded_fields)>0 ) ){');
        collect.log('        foreach ($selected_fields as $key => $value) {');
        collect.log('            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];');
        collect.log('        }');
        collect.log('        foreach ($excluded_fields as $key => $value) {');
        collect.log('            unset($new_formatted_data["computed_value"]["". $value .""]);');
        collect.log('        }');
        collect.log('        $new_formatted_data["__DEBUG"] = $print_debugger;');
        collect.log('        return $new_formatted_data;');
        collect.log('    }else if($selected_fields && count($selected_fields)>0){');
        collect.log('        foreach ($selected_fields as $key => $value) {');
        collect.log('            // echo $formatted_data["". $value .""];');
        collect.log('            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];');
        collect.log('        }');
        collect.log('        $new_formatted_data["__DEBUG"] = $print_debugger;');
        collect.log('        return $new_formatted_data;');
        collect.log('    }else if($excluded_fields && count($excluded_fields)>0){');
        collect.log('        foreach ($excluded_fields as $key => $value) {');
        collect.log('            unset($formatted_data["computed_value"]["". $value .""]);');
        collect.log('        }');
        collect.log('        $formatted_data["__DEBUG"] = $print_debugger;');
        collect.log('        //print_r($formatted_data);');
        collect.log('        return $formatted_data;');
        collect.log('    }else{');
        collect.log('        return $formatted_data;');
        collect.log('    }');

        collect.printHtml = collect.print;
        collect.printHtml = collect.printHtml.replace(/\n/g,"<br/>");
        collect.printHtml = collect.printHtml.replace(/(\s)/g,"&nbsp;");
        // collect.printHtml = collect.printHtml.replace(/(\s\s\s\s|\t)/g,"&#9;");

        return collect;
    },
    "HTMLOfficialPrintCustomScript2":function (){
        var temp2 = this.getFieldsData();
        if($('[fields-data-name]').length >= 1){
            var temp1 = $('[fields-data-name]').map(function(){ return ($(this).attr('fields-data-name')||"").replace("[]","");}).get();
            temp1 = temp1.map(function(a,b){
                for( var idx = 0 ; idx < temp2.length ; idx ++ ) {
                    if( temp2[idx]["data_field_name"] == a ) return temp2[idx];
                }
                return false;
            });
            temp2 = temp1;
        }
        
        var field_computed = "";
        var replace_fields = [
            '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now'
        ];
        var collect = {
            print:"",
            printHtml:"",
            log:function(param){
                this.print += (param||"")+"\n";//\ufeff //&#xfeff;
            },
            data:{
                "visibility":{},
                "computed_value":{},
                "change_list_computed_value":{},
                "readonly":{},
                "validation":{}
            }
        };
        var replace_functions = ['@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
            '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
            '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
            '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
            '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
            '@GetAuth', "@FormatDate","@NumToWord",
            "@Head","@AssistantHead","@GetData",
            "@LookupAuthInfo",
            "@GetAVG","@GetMin","@SDev","@GetMax",
            "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
            "@StrFind","@StrReplace", "@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
            "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource","@GetLocation",
            "@GetMonth","@GetFileName","@CustomScript", "@ArraySearchBy"];
        var replace_fields2 = [];
        var data_field_name = "";


        for(var i in replace_fields){
            collect.log("    //"+replace_fields[i].replace("@",""));
            collect.log("    $myvar['"+replace_fields[i].replace("@","")+"'] = $_POST['"+replace_fields[i].replace("@","")+"']?:\"\";");
            collect.log("\n");
        }

        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            replace_fields.push("@"+data_field_name+"(?![a-zA-Z0-9_])");
        }
        var regexz = new RegExp(replace_fields.join("|"),"g");
        var regexz_func = new RegExp(replace_functions.join("|"),"g");
        var temporary2 = "";

        for(var i in temp2){
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            data_field_name = $.trim(temp2[i]['data_field_name']);

                collect.log("    //"+data_field_name);
                collect.log("    $myvar['"+data_field_name+"'] = $_POST['"+data_field_name+"']?:\"\";");
                if(field_computed!=""){
                    if(temp2[i]["data_field_default_type"] == "computed"){
                        temporary2 = field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); });
                        if(temporary2[0] == "\"" && temporary2[temporary2.length-1] == "\""){
                            temporary2 = temporary2.substr(1, temporary2.length -2 );
                        }
                        collect.log("    $myvar['"+data_field_name+"'] = "+temporary2+" ;" );
                        
                        if(temp2[i]['field_type'] == "dropdown" || temp2[i]['field_type'] == "selectMany"){
                            collect.data["change_list_computed_value"][""+data_field_name] = field_computed;
                        }else{
                            collect.data["computed_value"][""+data_field_name] = field_computed;
                        }
                    }
                }
                collect.log("\n");

        }
        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            if(temp2[i]["data_field_default_type"] == "computed"){
                if(field_computed!=""){
                    if(temp2[i]['field_type'] == "dropdown" || temp2[i]['field_type'] == "selectMany"){
                        //collect.log("$formatted_data[\"change_list_computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }else{
                        collect.log("    $formatted_data[\"computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }
                    
                }
            }
            
        }
        collect.log("\n");
        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            if(field_computed!=""){
                if(temp2[i]["data_field_default_type"] == "computed"){
                    if(temp2[i]['field_type'] == "dropdown" || temp2[i]['field_type'] == "selectMany"){
                        collect.log("    $formatted_data[\"change_list_computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }else{
                        //collect.log("$formatted_data[\"computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }
                }
            }
        }
        collect.log("\n");
        for(var i in temp2){
            field_computed = $.trim(temp2[i]['data_field_visibility_formula']);
            if(field_computed!=""){
                collect.data["visibility"][""+temp2[i]['data_field_name'] ] = field_computed;
                collect.log("    $formatted_data[\"visibility\"][\""+temp2[i]['data_field_name']+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
            }
        }
        $('[aria-controls][field-visible="computed"][visible-formula]:not([visible-formula=""])').each(function(){
            var field_computed = $.trim($(this).attr("visible-formula"));
            collect.data["visibility"]["form-tab-panel"] = (collect.data["visibility"]["form-tab-panel"]||{});
            collect.data["visibility"]["form-tab-panel"][""+$(this).attr("aria-controls") ] = field_computed;
            collect.log("    $formatted_data[\"visibility\"][\"form-tab-panel\"][\""+$(this).attr("aria-controls")+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
        });
        $('.embed-view-container[field-visible="computed"][visible-formula]:not([visible-formula=""])').each(function(){
            var doi = $(this).parents('.setObject[data-object-id]').attr("data-object-id");
            var field_computed = $.trim($(this).attr("visible-formula"));
            collect.data["visibility"]["form-tab-panel"] = (collect.data["visibility"]["form-tab-panel"]||{});
            collect.data["visibility"]["embed"] = (collect.data["visibility"]["embed"]||{});
            collect.data["visibility"]["embed"][""+doi ] = field_computed;
            collect.log("    $formatted_data[\"visibility\"][\"embed\"][\""+doi+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
        });

        collect.log('    //return process');
        collect.log('    $print_debugger .= "\\$myvar = ".json_encode($myvar);');
        collect.log('    $print_debugger .= "\\n\\n";');
        collect.log('    $print_debugger .= "\\$personnel_info_SCGEmpID = ".json_encode($personnel_info_SCGEmpID);');
        collect.log('    ');
        collect.log('    if( ( $selected_fields && count($selected_fields)>0 )&& ( $excluded_fields && count($excluded_fields)>0 ) ){');
        collect.log('        foreach ($selected_fields as $key => $value) {');
        collect.log('            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];');
        collect.log('        }');
        collect.log('        foreach ($excluded_fields as $key => $value) {');
        collect.log('            unset($new_formatted_data["computed_value"]["". $value .""]);');
        collect.log('        }');
        collect.log('        $new_formatted_data["__DEBUG"] = $print_debugger;');
        collect.log('        return $new_formatted_data;');
        collect.log('    }else if($selected_fields && count($selected_fields)>0){');
        collect.log('        foreach ($selected_fields as $key => $value) {');
        collect.log('            // echo $formatted_data["". $value .""];');
        collect.log('            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];');
        collect.log('        }');
        collect.log('        $new_formatted_data["__DEBUG"] = $print_debugger;');
        collect.log('        return $new_formatted_data;');
        collect.log('    }else if($excluded_fields && count($excluded_fields)>0){');
        collect.log('        foreach ($excluded_fields as $key => $value) {');
        collect.log('            unset($formatted_data["computed_value"]["". $value .""]);');
        collect.log('        }');
        collect.log('        $formatted_data["__DEBUG"] = $print_debugger;');
        collect.log('        //print_r($formatted_data);');
        collect.log('        return $formatted_data;');
        collect.log('    }else{');
        collect.log('        return $formatted_data;');
        collect.log('    }');

        collect.printHtml = collect.print;
        collect.printHtml = collect.printHtml.replace(/\n/g,"<br/>");
        collect.printHtml = collect.printHtml.replace(/(\s)/g,"&nbsp;");
        // collect.printHtml = collect.printHtml.replace(/(\s\s\s\s|\t)/g,"&#9;");

        return collect;
    },
    "CopyHtml":function(){
        return $('.print-formatted-script').text().replace(/\ufeff/g,"\n");
    },
    "selectText":function(container) {
        if($(container).is('textarea')){
            $(container).select();
        }else if (document.selection) {
            var range = document.body.createTextRange();
            range.moveToElementText(container);
            range.select();
        } else if (window.getSelection) {
            var range = document.createRange();
            range.selectNode(container);
            window.getSelection().addRange(range);
        }
    },
    "GetFieldsDetails":function (){
        var temp2 = this.getFieldsData();
        if($('[fields-data-name]').length >= 1){
            var temp1 = $('[fields-data-name]').map(function(){ return ($(this).attr('fields-data-name')||"").replace("[]","");}).get();
            temp1 = temp1.map(function(a,b){
                for( var idx = 0 ; idx < temp2.length ; idx ++ ) {
                    if( temp2[idx]["data_field_name"] == a ) return temp2[idx];
                }
                return false;
            });
            temp2 = temp1;
        }
        
        var field_computed = "";
        var replace_fields = [
            '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now'
        ];
        var collect = {
            print:"",
            printHtml:"",
            log:function(param){
                this.print += (param||"")+"\n";//\ufeff //&#xfeff;
            },
            data:{
                "visibility":{},
                "computed_value":{},
                "change_list_computed_value":{},
                "readonly":{},
                "validation":{}
            }
        };
        var replace_functions = ['@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
            '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
            '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
            '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
            '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
            '@GetAuth', "@FormatDate","@NumToWord",
            "@Head","@AssistantHead","@GetData",
            "@LookupAuthInfo",
            "@GetAVG","@GetMin","@SDev","@GetMax",
            "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
            "@StrFind","@StrReplace", "@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
            "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource","@GetLocation",
            "@GetMonth","@GetFileName","@CustomScript", "@ArraySearchBy"];
        var replace_fields2 = [];
        var data_field_name = "";


        for(var i in replace_fields){
            collect.log("    //"+replace_fields[i].replace("@",""));
            collect.log("    $myvar['"+replace_fields[i].replace("@","")+"'] = $_POST['"+replace_fields[i].replace("@","")+"']?:\"\";");
            collect.log("\n");
        }

        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            replace_fields.push("@"+data_field_name+"(?![a-zA-Z0-9_])");
        }
        var regexz = new RegExp(replace_fields.join("|"),"g");
        var regexz_func = new RegExp(replace_functions.join("|"),"g");
        var temporary2 = "";

        for(var i in temp2){
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            data_field_name = $.trim(temp2[i]['data_field_name']);

                collect.log("    //"+data_field_name);
                collect.log("    $myvar['"+data_field_name+"'] = $_POST['"+data_field_name+"']?:\"\";");
                if(field_computed!=""){
                    if(temp2[i]["data_field_default_type"] == "computed"){
                        temporary2 = field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); });
                        if(temporary2[0] == "\"" && temporary2[temporary2.length-1] == "\""){
                            temporary2 = temporary2.substr(1, temporary2.length -2 );
                        }
                        collect.log("    $myvar['"+data_field_name+"'] = "+temporary2+" ;" );
                        
                        if(temp2[i]['field_type'] == "dropdown" || temp2[i]['field_type'] == "selectMany"){
                            collect.data["change_list_computed_value"][""+data_field_name] = field_computed;
                        }else{
                            collect.data["computed_value"][""+data_field_name] = field_computed;
                        }
                    }
                }
                collect.log("\n");

        }
        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            if(temp2[i]["data_field_default_type"] == "computed"){
                if(field_computed!=""){
                    if(temp2[i]['field_type'] == "dropdown" || temp2[i]['field_type'] == "selectMany"){
                        //collect.log("$formatted_data[\"change_list_computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }else{
                        collect.log("    $formatted_data[\"computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }
                    
                }
            }
            
        }
        collect.log("\n");
        for(var i in temp2){
            data_field_name = $.trim(temp2[i]['data_field_name']);
            field_computed = $.trim(temp2[i]['data_field_formula_computed']);
            if(field_computed!=""){
                if(temp2[i]["data_field_default_type"] == "computed"){
                    if(temp2[i]['field_type'] == "dropdown" || temp2[i]['field_type'] == "selectMany"){
                        collect.log("    $formatted_data[\"change_list_computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }else{
                        //collect.log("$formatted_data[\"computed_value\"][\""+temp2[i]['data_field_name']+"\"] = $myvar[\""+data_field_name+"\"] ;" );
                    }
                }
            }
        }
        collect.log("\n");
        for(var i in temp2){
            field_computed = $.trim(temp2[i]['data_field_visibility_formula']);
            if(field_computed!=""){
                collect.data["visibility"][""+temp2[i]['data_field_name'] ] = field_computed;
                collect.log("    $formatted_data[\"visibility\"][\""+temp2[i]['data_field_name']+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
            }
        }
        $('[aria-controls][field-visible="computed"][visible-formula]:not([visible-formula=""])').each(function(){
            var field_computed = $.trim($(this).attr("visible-formula"));
            collect.data["visibility"]["form-tab-panel"] = (collect.data["visibility"]["form-tab-panel"]||{});
            collect.data["visibility"]["form-tab-panel"][""+$(this).attr("aria-controls") ] = field_computed;
            collect.log("    $formatted_data[\"visibility\"][\"form-tab-panel\"][\""+$(this).attr("aria-controls")+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
        });
        $('.embed-view-container[field-visible="computed"][visible-formula]:not([visible-formula=""])').each(function(){
            var doi = $(this).parents('.setObject[data-object-id]').attr("data-object-id");
            var field_computed = $.trim($(this).attr("visible-formula"));
            collect.data["visibility"]["form-tab-panel"] = (collect.data["visibility"]["form-tab-panel"]||{});
            collect.data["visibility"]["embed"] = (collect.data["visibility"]["embed"]||{});
            collect.data["visibility"]["embed"][""+doi ] = field_computed;
            collect.log("    $formatted_data[\"visibility\"][\"embed\"][\""+doi+"\"] = "+field_computed.replace(regexz,function(dt){ return "$myvar['"+dt.replace("@","")+"']"; }).replace(/\\n|\\t|\\/g,"").replace(regexz_func,function(dt){ return dt.replace("@",""); })+" ;" );
        });

        collect.log('    //return process');
        collect.log('    $print_debugger .= "\\$myvar = ".json_encode($myvar);');
        collect.log('    $print_debugger .= "\\n\\n";');
        collect.log('    $print_debugger .= "\\$personnel_info_SCGEmpID = ".json_encode($personnel_info_SCGEmpID);');
        collect.log('    ');
        collect.log('    if( ( $selected_fields && count($selected_fields)>0 )&& ( $excluded_fields && count($excluded_fields)>0 ) ){');
        collect.log('        foreach ($selected_fields as $key => $value) {');
        collect.log('            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];');
        collect.log('        }');
        collect.log('        foreach ($excluded_fields as $key => $value) {');
        collect.log('            unset($new_formatted_data["computed_value"]["". $value .""]);');
        collect.log('        }');
        collect.log('        $new_formatted_data["__DEBUG"] = $print_debugger;');
        collect.log('        return $new_formatted_data;');
        collect.log('    }else if($selected_fields && count($selected_fields)>0){');
        collect.log('        foreach ($selected_fields as $key => $value) {');
        collect.log('            // echo $formatted_data["". $value .""];');
        collect.log('            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];');
        collect.log('        }');
        collect.log('        $new_formatted_data["__DEBUG"] = $print_debugger;');
        collect.log('        return $new_formatted_data;');
        collect.log('    }else if($excluded_fields && count($excluded_fields)>0){');
        collect.log('        foreach ($excluded_fields as $key => $value) {');
        collect.log('            unset($formatted_data["computed_value"]["". $value .""]);');
        collect.log('        }');
        collect.log('        $formatted_data["__DEBUG"] = $print_debugger;');
        collect.log('        //print_r($formatted_data);');
        collect.log('        return $formatted_data;');
        collect.log('    }else{');
        collect.log('        return $formatted_data;');
        collect.log('    }');

        collect.printHtml = collect.print;
        collect.printHtml = collect.printHtml.replace(/\n/g,"<br/>");
        collect.printHtml = collect.printHtml.replace(/(\s)/g,"&nbsp;");
        // collect.printHtml = collect.printHtml.replace(/(\s\s\s\s|\t)/g,"&#9;");

        return collect;
    }
};