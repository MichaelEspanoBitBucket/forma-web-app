var fontFamily = {

	"Droid Sans":"droid_sansregular", 
	"Open Sans" :"open_sansregular",
	"Protestant":"protestant_regular",
	"Dustismo":"dustismo_regular",
	"Perspective":"perspective_sansregular",
	"Sansumi":"sansumiregular",
	"Tradition Sans":"traditionsans_xlightregular",
	"Vera Humana":"vera_humana_95regular",
	"Webly Sleek":"weblysleeklight",
						   			
}

var newFontFamily = {

	"Droid Sans":

		{
			"regular": "droid_sansregular",
			"bold": "droid_sansregular_bold",
		},



	"Dustismo" :
		{
			"regular": "dustismo_regular",
			"bold": "dustismo_regular_bold"
		},


	"Open Sans" :
		{
			"regular": "open_sanslight",
			"bold": "open_sanslight_bold",
		},
	

	"Perspective": 

		{
			"regular": "perspective_sansregular",
			"bold": "perspective_sansregular_bold",
		},


	"Sansumi": 
		{
			"regular": "sansumiregular",
			"bold": "sansumiregular_bold"
		}
	,

	"Vera Humana" : 
		{
			"regular": "vera_humana_95regular",
			"bold": "vera_humana_95regular_bold"
		},
	

	"Webly Sleek": 
		{
			"regular":"weblysleeklight",
			"bold":"weblysleeklight_bold"
		}
	

}

function mobileIcon(data_type){
	var allowed_data_type = ['button','dropdown','checkbox','textArea','datepicker','pickList','textbox','dateTime','radioButton','time','imageOnly','textbox_editor_support','textbox_reader_support','qr-code','barCodeScanner'];
	if( allowed_data_type.indexOf(data_type) >= 0 ){
		return '<i class="fa fa-mobile" title="Available on mobile"></i>';
	}
	return '';
}
//console.log(newFontFamily);
function object_properties(properties_type,data_type,field_type, object_ele_type) {
	 
	var this_setObject_eleDOM = $(this);
	var dialog_data_prop = {};
	dialog_data_prop["this_object_ele_type"] = "";
	dialog_data_prop["dialog_icon"] = '<i class="icon-asterisk fa fa-cogs"></i>';
	var this_setObject_eleDOM_data_type = "";


	if(this_setObject_eleDOM.is("[data-type]")){
		this_setObject_eleDOM_data_type = this_setObject_eleDOM.attr("data-type");
		dialog_data_prop["this_object_ele_type"] = this_setObject_eleDOM.attr("data-type");

		if(dialog_data_prop["this_object_ele_type"]){
			dialog_data_prop["this_object_ele_type"] = dialog_data_prop["this_object_ele_type"].trim();
		}
		if(dialog_data_prop["this_object_ele_type"] == "selectMany"){
			dialog_data_prop["this_object_ele_type"] = "Select Multiple";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-select-many'></use></svg>" || dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "colorpicker"){
			dialog_data_prop["this_object_ele_type"] = "Color Picker";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-requestview-bucket'></use></svg>" || dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "labelOnly"){
			dialog_data_prop["this_object_ele_type"] = "Label";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-label'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "tab-panel"){
			dialog_data_prop["this_object_ele_type"] = "Tabbed Panel";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-tab-panel'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "textbox"){
			dialog_data_prop["this_object_ele_type"] = "Text Box";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-textbox'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "textArea"){
			dialog_data_prop["this_object_ele_type"] = "Text Area";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-textarea'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "checkbox"){
			dialog_data_prop["this_object_ele_type"] = "Checkbox";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-checkbox'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "radioButton"){
			dialog_data_prop["this_object_ele_type"] = "Radio Button";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-radio-button'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "dropdown"){
			dialog_data_prop["this_object_ele_type"] = "Dropdown";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-dropdown-list'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "dateTime"){
			dialog_data_prop["this_object_ele_type"] = "Date Time Picker";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-date-time-picker'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "datepicker"){
			dialog_data_prop["this_object_ele_type"] = "Date Picker";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-date-picker'></use></svg>" || dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "table"){
			dialog_data_prop["this_object_ele_type"] = "Table Object";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-table'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "createLine"){
			dialog_data_prop["this_object_ele_type"] = "Line Object";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-line'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "button"){
			dialog_data_prop["this_object_ele_type"] = "Action Button";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-button'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "listNames"){
			dialog_data_prop["this_object_ele_type"] = "List Names";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-names'></use></svg>" || dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "computed"){
			dialog_data_prop["this_object_ele_type"] = "Computed Processor";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-computed'></use></svg>"||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "embeded-view"){
			dialog_data_prop["this_object_ele_type"] = "Embedded Object";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-embedded-view'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "pickList"){
			dialog_data_prop["this_object_ele_type"] = "Data Pick List";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-pick-list'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		// else if(dialog_data_prop["this_object_ele_type"] == "imageOnly"){
		// 	dialog_data_prop["this_object_ele_type"] = "Request Image";
		// 	dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-request-image'></use></svg>" ||dialog_data_prop["dialog_icon"];
		// }
		else if(dialog_data_prop["this_object_ele_type"] == "barCodeScanner"){
			dialog_data_prop["this_object_ele_type"] = "Bar Code Data Scanner";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-barcode'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}else if(dialog_data_prop["this_object_ele_type"] == "smart_barcode_field"){
			dialog_data_prop["this_object_ele_type"] = "Smart Bar Code Data Scanner";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-smart-barcode'></use></svg>" ||dialog_data_prop["dialog_icon"];

		}
		else if(dialog_data_prop["this_object_ele_type"] == "time"){
			dialog_data_prop["this_object_ele_type"] = "Time Picker";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-time-picker'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "attachment_on_request"){
			dialog_data_prop["this_object_ele_type"] = "Single File Attachment";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-single-attachment'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "multiple_attachment_on_request"){
			dialog_data_prop["this_object_ele_type"] = "Multiple File Attachment";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-multiple-attachment'></use></svg>"||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "noteStyleTextarea"){
			dialog_data_prop["this_object_ele_type"] = "Notes Style";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-note-style'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "textbox_reader_support"){
			dialog_data_prop["this_object_ele_type"] = "Reader Support";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-reader-support'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "textbox_editor_support"){
			dialog_data_prop["this_object_ele_type"] = "Editor Support";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-editor-support'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "autoTimer"){
			dialog_data_prop["this_object_ele_type"] = "Timer";
			dialog_data_prop["dialog_icon"] = ""||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "accordion"){
			dialog_data_prop["this_object_ele_type"] = "Accordion";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-accordion'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "details-panel"){
			dialog_data_prop["this_object_ele_type"] = "Details Panel";
			dialog_data_prop["dialog_icon"] = ""||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "qr-code"){
			dialog_data_prop["this_object_ele_type"] = "QR Code";
			dialog_data_prop["dialog_icon"] = "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-qrcode'></use></svg>" ||dialog_data_prop["dialog_icon"];
		}
		else if(dialog_data_prop["this_object_ele_type"] == "imageOnly"){
			dialog_data_prop["this_object_ele_type"] = "Image";
			dialog_data_prop["dialog_icon"] = ""||dialog_data_prop["dialog_icon"];
		}

	}
	if(typeof field_type == "undefined"){
		//catch
	}
	console.log(this_setObject_eleDOM_data_type)
    var ret = "";
    ret += '<div style="float:left;width:50%;">';
	ret += '<h3 class="pull-left" style="line-height: 22px;">';
	    ret += dialog_data_prop["dialog_icon"] + ' ' + '<span style="margin-left:5px;">'+dialog_data_prop["this_object_ele_type"] + '</span>' + " Properties";
	ret += '</h3>';
    ret += '</div>';
    ret += '<div style="float:right;width:50%;text-align:right;margin-top:-5px;">';
	ret += '<input type="button" class="btn-basicBtn fl-margin-bottom" id="save_prop" value="OK" onclick="setFieldProperties()">';
	ret += '<input type="button" class="btn-basicBtn fl-margin-bottom" id="popup_cancel" value="Cancel">';
    ret += '</div>';
    ret += '<div class="hr"></div>';
    //ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 317px;overflow: hidden;" class="ps-container">';
    ret +='<div class="content-dialog">';
 //    ret += '<div class="object_properties_content">';
	// ret += '<div class="scrollbarM object_properties_wrapper">';
	

	switch(properties_type){
		// case "picklist":
		// ret += obj_lblName("pull-left",data_type); 
		// ret += obj_lblInputName("pull-right",data_type); 
		// ret += obj_PlaceHolder("pull-left",data_type); 
		// ret += obj_Tooltip("pull-right",data_type); 
		// ret += obj_lblFontWeight("pull-left",data_type); 
		// ret += obj_lblFontSize("pull-right",data_type); 
		// ret += obj_fldMaxLength("pull-left",data_type);
		// ret += obj_fldHeight("pull-right",data_type);
		// ret += obj_fldWidth("pull-left",data_type);
		// ret += obj_fldType("pull-right",data_type);
		// ret += obj_lblAllow("pull-left",data_type);
		// ret += obj_lblAlignment("pull-right",data_type);
		// ret += obj_lblFontColor("pull-left",data_type); 
		// ret += obj_fldColor("pull-right",data_type);
		// ret += obj_chkETrig("pull-left",data_type);
		// ret += obj_tabIndex("pull-right",data_type);
		
		
		// ret += obj_inputValidation("pull-left",data_type);
		// ret += obj_chkVisibility("pull-right",data_type);
		// ret += obj_inputValidationMessage("pull-left",data_type);
		// ret += obj_defaultValue("pull-right",data_type)
		// ret += picklistSelectionType("pull-left",data_type);
		// break;

	    case "1":
	    ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
		    ret += obj_lblName("",data_type); 
			ret += obj_PlaceHolder("", data_type); 
			//ret += obj_lblFontWeight("", data_type); 
			ret += obj_lblwsd("",data_type);
			ret += obj_fldwsd("",data_type);
			ret += fieldFontSize("", data_type);

				if(this_setObject_eleDOM.attr("data-type") == "textbox"){
					ret += obj_fldHeight("", data_type);
				}
				if(field_type != "pickList" && this_setObject_eleDOM_data_type != "datepicker" && this_setObject_eleDOM_data_type != "dateTime" && this_setObject_eleDOM_data_type != "time" && this_setObject_eleDOM_data_type != "listNames"){
					ret += obj_fldType("", data_type);
				}
			
			ret += obj_fldColor("", data_type);
			ret += obj_fldBGColor("", data_type);
			ret += obj_lblAlignment("", data_type);
			ret += obj_tabIndex("", data_type);
			if(field_type == "pickList"){ 
				ret += obj_hideModalCloseBtn("",data_type);
			}
			ret += obj_inputValidation("", data_type);
			ret += obj_inputValidationMessage("", data_type);
			
			if(field_type == "listNames"){
			ret += obj_shortlist("", data_type);
			}
			ret += obj_defaultValue("", data_type);
			ret += obj_onchangeFormulaEvent("", data_type);
			ret += obj_objectCss("", data_type);
		ret += '</div>';

		ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
			ret += obj_lblInputName("", data_type); 
			ret += obj_fontType("", data_type);
			ret += obj_fieldFontFamily("",data_type);
			ret += obj_alignment("",data_type);
			
			ret += obj_fieldTextAlignment("",data_type);
			
			ret += obj_Tooltip("", data_type); 
			ret += obj_lblFontSize("", data_type);
			ret += obj_fldMaxLength("", data_type);
			ret += obj_fldWidth("", data_type);
			ret += obj_lblAllow("", data_type);
			ret += obj_lblFontColor("", data_type);
			ret += obj_fldfc("", data_type); 
			ret += obj_chkETrig("", data_type);
			ret += obj_propEcrypt("", data_type);
			ret += obj_chkVisibility("", data_type);
			//ret += obj_chkDisableFormula("", data_type);
			ret += obj_chkReadOnlyFormula("", data_type);
			ret += obj_borderVisibility("", data_type);

			ret += obj_setReadonlyField("",data_type);
			if(field_type == "textbox" || field_type == "textarea"){
				ret += ecryptionSwitch("",data_type);
			}
			//ret += obj_allowDuplicate("", "pull-right",data_type);
			if(field_type == "pickList"){		
				
				ret += obj_picklistActionVisibility("",data_type);
				ret += picklistSelectionType("", data_type);
				
				//ret += picklist_image("", data_type);
				//picklist_image(obj_float,data_type)
				// ret += obj_picklistButton("", data_type);
			}
			if (field_type == "listNames") {
				ret += listNameChoiceType("", data_type);
				
			}
			
			ret += obj_middleWareChoice("", data_type);
		ret += '</div>';

		
		break;
	    case "2":
	    ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
	    	ret += obj_lblName("", data_type);
			ret += obj_Tooltip("", data_type);
			ret += obj_lblwsd("",data_type);
			ret += obj_fldwsd("",data_type);
			ret += obj_lblFontSize("", data_type);
			if(field_type != "radioButton" && field_type != "checkbox"){
				ret += obj_fldHeight("", data_type);
			}
			
			ret += obj_lblAllow("", data_type);
			ret += obj_alignRCField("", data_type);
			ret += obj_tabIndex("", data_type);
			ret += obj_setReadonlyField("",data_type);
			
			ret += obj_defaultValueChoices("", data_type);
			ret += obj_inputValidation("", data_type);
			ret += obj_inputValidationMessage("", data_type);
			ret += obj_onchangeFormulaEvent("", data_type);

			if (field_type == "dropdown") {
				
				// ret += obj_inputValidation("", data_type);
				// ret += obj_inputValidationMessage("", data_type);
				ret += dynamicListing("", data_type);
							}
			if (field_type == "selectMany") {
				ret += dynamicListing("", data_type);
			}
			ret += obj_objectCss("", data_type);
	    ret += '</div>';

	    ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
	    	ret += obj_lblInputName("", data_type);	
			ret += obj_addChoices("", data_type);
			ret += fieldFontSize("", data_type);
			if(field_type != "radioButton" && field_type != "checkbox"){
				ret += obj_fldWidth("", data_type);
			}

			ret += obj_lblAlignment("", data_type);			
			//ret += obj_allowDuplicate("pull-right",data_type);
			ret += obj_fontType("", data_type);
			ret += obj_fieldFontFamily("",data_type);
			// if(field_type != 'dropdown' ){
				
			// 	ret += obj_fieldTextAlignment("",data_type);
			// }
			// ret += obj_alignment("",data_type);
			
			ret += obj_lblFontColor("", data_type);
			ret += obj_fldfc("", data_type);
			ret += obj_propEcrypt("", data_type);
			ret += obj_chkETrig("", data_type);
			ret += obj_chkVisibility("", data_type);
			//ret += obj_chkDisableFormula("", data_type);
			ret += obj_chkReadOnlyFormula("", data_type);
			
			if(field_type == "radioButton" || field_type == "checkbox"){
				// ret += picklistSelectionType("pull-right",data_type);
				ret += radioAndCheckboxLabelProp("", data_type,field_type);
				ret += obj_radioCheckboxSpacing("", data_type,field_type);
			}

			
			
	    ret += '</div>';
		break;
	    case "3":
	    ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
	    	ret += obj_lblName("", data_type);
			ret += obj_lblFontColor("", data_type);
			ret += obj_lblFontSize("", data_type);
			ret += obj_fldHeight.call(this_setObject_eleDOM_data_type,"", data_type);
			ret += obj_fldWidth.call(this_setObject_eleDOM_data_type,"", data_type);
			ret += obj_objectCss("", data_type);
	    ret += '</div>';

	    ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
	    	// ret += obj_fldHeight("pull-right",data_type);
			// ret += obj_fldWidth("pull-left",data_type);
			ret += obj_fontType("", data_type);
			
			// ret += obj_alignment("",data_type);
			
			ret += obj_lblAllow("", data_type);
			ret += obj_fldBGColor.call(this_setObject_eleDOM_data_type, "", data_type);
	    ret += '</div>';
		break;
	    case "4":
	    	//modified by joshua reyes 03/07/2016
	    	ret += obj_lblInputName.call(this_setObject_eleDOM_data_type, this_setObject_eleDOM.attr("data-object-id"), data_type);
			//ret += obj_lblInputName.call(this_setObject_eleDOM_data_type, "", data_type); This is the OLD code
		break;
		case "formtable":

		ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
			ret += obj_lblName("", data_type);
			ret += obj_lblwsd("",data_type);

			ret += obj_lblFontColor("", data_type);
			ret += obj_repeaterTable("", data_type);
			ret += obj_objectCss("", data_type);
		ret += '</div>';

		ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
			// ret += obj_TableName("", data_type);
			ret += obj_fontType("", data_type);
			// ret += obj_fieldFontFamily("",data_type);
			ret += obj_alignment("",data_type);
			// ret += obj_fieldTextAlignment("",data_type);
			ret += obj_lblFontSize("", data_type);
			ret += obj_lblAllow("", data_type);
			ret += obj_tableBorderVisibility("", data_type);
			if (this_setObject_eleDOM_data_type != "table") {
				ret += obj_tableResponsiveContainer("", data_type);
			}
			
			
			
			// ret += obj_tableRelationalAction("", data_type);
			//ret += obj_cellColor("pull-left",data_type);
		ret += '</div>';
		break; 
		case "tabpanel":

		ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
			// ret += obj_lblInputName("", data_type);
			ret += obj_lblwsd("",data_type);
			ret += obj_lblFontColor("", data_type);
			ret += obj_objectCss("", data_type);
		ret += '</div>';

		ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
			ret += obj_lblName("", data_type);
			ret += obj_fontType("", data_type);
			//ret += obj_fieldFontFamily("",data_type);
			ret += obj_alignment("",data_type);
			// ret += obj_fieldTextAlignment("",data_type);
			ret += obj_lblFontSize("", data_type);
			ret += obj_lblAllow("", data_type);
		ret += '</div>';
		
		// ret += obj_propTabName("pull-right",data_type);
		// ret += obj_chkETrig("pull-right",data_type);
		// ret += obj_lblInputName("pull-right",data_type);
		break;

		case "label":
			
			if(field_type == "imageForm"){

			}else{
				ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
					ret += obj_lblIdentifier("",data_type);
					ret += obj_lblFontSize("", data_type);
					ret += obj_lblName("", data_type);
					ret += hyperlink_lbl("", data_type);
					ret += obj_Tooltip("", data_type);
					ret += obj_chkVisibility("", data_type);
					ret += obj_objectCss("", data_type);
				ret += '</div>';

				ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
					ret += obj_fontType("", data_type);
					
					ret += obj_alignment("",data_type);
					
					ret += obj_lblwsd("",data_type);
					ret += obj_lblFontColor("", data_type);
				ret += '</div>';
			}
		break;
		
		case "on_attachment":
			ret += obj_lblName("",data_type);
			ret += obj_lblInputName("", data_type);
			ret += obj_fontType("", data_type);
			ret += obj_fieldFontFamily("",data_type);
			// ret += obj_alignment("",data_type);
			// ret += obj_fieldTextAlignment("",data_type);
			ret += obj_lblwsd("",data_type);
			ret += obj_lblFontSize("",data_type);
			ret += obj_lblFontColor("",data_type);
			ret += obj_lblAlignment("", data_type);
			ret += obj_inputValidation("", data_type);
			ret += obj_inputValidationMessage("", data_type);
			ret += obj_chkVisibility("", data_type);
			ret += obj_objectCss("", data_type);
			//ret += obj_chkDisableFormula("", data_type);
			//ret += obj_chkReadOnlyFormula("", data_type);
		break;
		
		case "photos":
		// ret += obj_lblName("pull-left",data_type);
		//ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
		ret += obj_lblInputName("", data_type);
		ret += obj_objectCss("", data_type);
		ret += obj_onchangeFormulaEvent("", data_type);
		//ret += '</div>';
		// ret += obj_lblFontWeight("pull-right",data_type);
		// ret += obj_lblFontSize("pull-left",data_type);
		// ret += obj_lblFontColor("pull-right",data_type);
		break;
		
		case "computed":

			ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
				ret += obj_lblName("", data_type); 
				ret += obj_PlaceHolder("", data_type); 
				ret += obj_lblwsd("",data_type);
				ret += obj_fldwsd("",data_type);
				ret += fieldFontSize("", data_type); /*ret += obj_lblInputName.call(this_setObject_eleDOM_data_type, "", data_type);*/
				ret += obj_fldHeight("", data_type);
				//ret += obj_fldType("", data_type);
				ret += obj_lblAlignment("", data_type);
				ret += obj_fldColor("", data_type);
				ret += obj_fldBGColor("", data_type);
				ret += processorType_fb("", data_type);
				ret += processor_fb("", data_type);
				ret += obj_objectCss("", data_type);
			ret += '</div>';

			ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
				ret += obj_lblInputName("", data_type);
				ret += obj_fontType("", data_type);	
				ret += obj_fieldFontFamily("",data_type);
				ret += obj_alignment("",data_type);	
				ret += obj_fieldTextAlignment("",data_type);	
				ret += obj_Tooltip("", data_type); 				
				ret += obj_lblFontSize("", data_type);
				ret += obj_fldMaxLength("", data_type);
				ret += obj_fldWidth("", data_type);
				ret += obj_lblAllow("", data_type);
				ret += obj_lblFontColor("", data_type);
				ret += obj_fldfc("", data_type);
				ret += obj_tabIndex("", data_type);
				ret += obj_setReadonlyField("",data_type);
			ret += '</div>';
		break;
		
		case "embedview":
		//value="static"

			ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
				ret += obj_lblInputName.call(this_setObject_eleDOM_data_type, "", data_type);
				ret += obj_lblFontSize("", data_type);
				ret += obj_lblwsd("",data_type);
				ret += obj_lblAllow("", data_type);
				ret += obj_onchangeFormulaEvent("", data_type);
				/*ret += embedRowClick("", data_type);// embed talga
				ret += embedPopupDataSending("", data_type);// embed talga
				ret += highlightedRowProp("", data_type);// embed talga*/
				// ret += propEmbedActionEventVisibility("", data_type);//luma na to
				/*ret += propAdvanceEmbedActionEventVisibility("", data_type);// embed talga*/
				// ret += propEmbedViewVisibility("", data_type);//luma na to
			ret += '</div>';

			//ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
			//	ret += obj_lblName("", data_type);
			//	ret += obj_lblFontColor("", data_type);
			//	ret += obj_embedProperty("", data_type);
			//	ret += embedRoutingField("", data_type);
			//	ret += embedDisplayColumn("", data_type);
			//	ret += obj_setReadonlyField("",data_type);
			//	ret += propEmbedSetColumnResult("",data_type);
			//ret += '</div>';
			
			ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
				ret += obj_lblName("", data_type);
				ret += obj_fontType("", data_type);
				ret += obj_fieldFontFamily("",data_type);
				ret += obj_alignment("",data_type);
				//ret += obj_fieldTextAlignment("",data_type);
				ret += obj_lblFontColor("", data_type);
				ret += obj_objectCss("", data_type);
				//ret += obj_fldfc("", data_type);
				/*ret += obj_embedProperty("", data_type);// embed talga
				ret += embedRoutingField("", data_type);// embed talga
				ret += embedDisplayColumn("", data_type);// embed talga
				ret += propEmbedSetColumnResult("",data_type);// embed talga*/
			ret += '</div>';
		break;
		
		case "barcode_property":
			ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
				ret += obj_lblName("", data_type); 
				ret += obj_PlaceHolder("", data_type); 
				ret += obj_lblwsd("",data_type);
				ret += obj_fldMaxLength("", data_type);
				ret += obj_fldWidth("", data_type);
				ret += obj_lblAllow("", data_type);
				ret += obj_lblFontColor("", data_type); 
				ret += obj_fldfc("", data_type);
				ret += obj_chkETrig("", data_type);
				ret += obj_inputValidation("", data_type);
				ret += obj_inputValidationMessage("", data_type);
				ret += obj_objectCss("", data_type);
			ret += '</div>';

			ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
				ret += obj_lblInputName("", data_type); 
				ret += obj_fontType("", data_type);
				ret += obj_fieldFontFamily("",data_type);
				ret += obj_alignment("",data_type);
				ret += obj_fieldTextAlignment("",data_type);
				ret += obj_Tooltip("", data_type); 
				ret += obj_lblFontSize("", data_type); 
				ret += obj_fldHeight("", data_type);
				ret += obj_fldType("", data_type);
				ret += obj_lblAlignment("", data_type);
				ret += obj_fldColor("", data_type);

				ret += obj_tabIndex("", data_type);
				ret += obj_chkVisibility("", data_type);
				//ret += obj_chkDisableFormula("", data_type);
				ret += obj_chkReadOnlyFormula("", data_type);
				ret += obj_defaultValue("", data_type);
				
			ret += '</div>';

			
		break;
		
		case "smart-barcode-field":
			ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
				ret += obj_lblName("", data_type); 
				ret += obj_PlaceHolder("", data_type); 
				ret += obj_lblwsd("",data_type);
				ret += obj_fldMaxLength("", data_type);
				ret += obj_fldWidth("", data_type);
				ret += obj_lblAllow("", data_type);
				ret += obj_lblFontColor("", data_type); 
				ret += obj_fldfc("", data_type);
				ret += obj_chkETrig("", data_type);
				ret += obj_inputValidation("", data_type);
				ret += obj_inputValidationMessage("", data_type);
				ret += obj_objectCss("", data_type);
			ret += '</div>';

			ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
				ret += obj_lblInputName("", data_type); 
				ret += obj_fontType("", data_type);
				ret += obj_fieldFontFamily("",data_type);
				ret += obj_alignment("",data_type);
				ret += obj_fieldTextAlignment("",data_type);
				ret += obj_Tooltip("", data_type); 
				ret += obj_lblFontSize("", data_type); 
				ret += obj_fldHeight("", data_type);
				ret += obj_fldType("", data_type);
				ret += obj_lblAlignment("", data_type);
				ret += obj_fldColor("", data_type);

				ret += obj_tabIndex("", data_type);
				ret += obj_chkVisibility("", data_type);
				//ret += obj_chkDisableFormula("", data_type);
				ret += obj_chkReadOnlyFormula("", data_type);
				ret += obj_defaultValue("", data_type)
			ret += '</div>';
			console.log("call smart-barcode-field properties");

			
		break;
		
		case "notes-style-property":

			ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
				ret += obj_lblName("", data_type); 
				ret += obj_PlaceHolder("", data_type); 
				ret += obj_lblwsd("",data_type);
				ret += obj_fldwsd("",data_type);
				ret += fieldFontSize("", data_type);
				ret += obj_fldMinHeight("", data_type);//BAGO
				ret += obj_fldMaxHeight("", data_type);//BAGO
				//ret += obj_fldType("", data_type);
				ret += obj_fldColor("", data_type);
				ret += obj_lblAlignment("", data_type);
				ret += obj_tabIndex("", data_type);
				ret += obj_inputValidation("", data_type);
				ret += obj_inputValidationMessage("", data_type);
				ret += obj_middleWareChoice("", data_type);
				ret += obj_objectCss("", data_type);
				if(this_setObject_eleDOM.parent().hasClass("td-relative")){
					ret += obj_noteStyleFluidContainerCell("", data_type);
				}
			ret += '</div>';

			ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
				ret += obj_lblInputName("", data_type);
				ret += obj_fontType("", data_type);
				ret += obj_fieldFontFamily("",data_type);
				ret += obj_alignment("",data_type);
				ret += obj_fieldTextAlignment("",data_type);
				ret += obj_Tooltip("", data_type);
				ret += obj_lblFontSize("", data_type);
				ret += obj_fldMaxLength("", data_type);
				// ret += obj_fldHeight("pull-left",data_type);
				// ret += obj_fldWidth("pull-right",data_type);
				ret += obj_fldMinWidth("", data_type);//BAGO
				ret += obj_fldMaxWidth("", data_type);//BAGO
				ret += obj_lblAllow("", data_type);
				ret += obj_fldBGColor("",data_type);
				ret += obj_lblFontColor("", data_type);
				ret += obj_fldfc("", data_type);
				ret += obj_chkETrig("", data_type);
				ret += obj_propEcrypt("", data_type);
				ret += obj_chkVisibility("", data_type);
				//ret += obj_chkDisableFormula("", data_type);
				ret += obj_chkReadOnlyFormula("", data_type);
				//ret += obj_defaultValue("", data_type);
			ret += '</div>';
			break;

			case "Embedded View Properties":
				ret += '<div class="properties_width_container section clearing fl-field-style sourFormType">';
					ret += '<div class="column div_1_of_1">';
						ret += '<label style="padding:5px;"><input type="radio" class="css-checkbox" name="sourceformtype" id="singleSourceForm" value="single" /><label for="singleSourceForm" class="css-label"></label>Single Form: </label>';
						ret += '<label style="padding:5px;"><input type="radio" class="css-checkbox" name="sourceformtype" id="multipleSourceForm" value="multiple" /><label for="multipleSourceForm" class="css-label"></label>Multiple Form: </label>';
					ret += '</div>';
				ret += '</div>';
				ret += '<div class="embed-default-prop">';
					ret  += '<div class="fl-floatLeft embedLeftPanel fl-form-prop-cont-wid">';
						ret += embedRowClick("", data_type);
						ret += embedPopupDataSending("", data_type);
						// ret += embed_allowImport("",data_type);
						ret += highlightedRowProp("", data_type);
						ret += embedRoutingField("", data_type);
						ret += obj_hideModalCloseBtn("",data_type);
						ret += propAdvanceEmbedActionEventVisibility("", data_type);
						if($('#enable_inline_add_embed').text() != "0"){
							ret += relatedFormDetails("", data_type);
						}
						ret += obj_default_embed_sorting("",data_type);
						ret += obj_embed_row_selection("", data_type);
						ret += obj_embedCustomActions("", data_type);
					ret += '</div>'; // HIDE FOR NOW

					// OLD EMBEDED SOURCE FORM
					ret += '<div class="fl-floatRight embedRightPanel fl-form-prop-cont-wid">';	
						ret += obj_embedPropertyOLD("", data_type);
						ret += embedDisplayColumn("", data_type);
						ret += propEmbedSetColumnResult("",data_type);
						ret += embedDialogType("",data_type);
					ret += '</div>';
				ret += "</div>"
				ret += '<div class="embed-multiple-prop display">';
					ret += loadEmbedMultipleFormPropertiesLeft("",data_type);
				ret += '</div>';
			
			// ret += '<div>';
			// ret += '</div>';
			// ret += obj_embededSourceForm("", data_type); //NEW EMBEDED SOUR FORM
				
			break;
			
			case "accordion-property":
			ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';

				ret += obj_lblName("", data_type); 
				ret += obj_lblwsd("",data_type);
				// ret += obj_sectionwsd("",data_type);
				ret += obj_lblFontColor("", data_type);
				ret += obj_lblAlignment("", data_type);
				ret += obj_objectCss("", data_type);
			ret += '</div>';
			ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';	
				
				ret += obj_fontType("", data_type);
				// ret += obj_SectionfontType("",data_type);
				ret += obj_alignment("",data_type);
				ret += obj_lblFontSize("", data_type);
				ret += obj_lblAllow("", data_type)
			ret += '</div>';
			break;
			
			case "detailsPanel":
				ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
					ret += obj_lblInputName.call(this_setObject_eleDOM_data_type, "", data_type);
					ret += obj_lblFontSize("", data_type);
					ret += obj_lblwsd("",data_type);
					ret += obj_lblAllow("", data_type);
					
				ret += '</div>';

				
				ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
					ret += obj_lblName("", data_type);
					ret += obj_fontType("", data_type);
					ret += obj_fieldFontFamily("",data_type);
					ret += obj_alignment("",data_type);
					
					ret += obj_lblFontColor("", data_type);
					
				ret += '</div>';
			break;
			
			case "detailsPanelContentSettings":
				ret += obj_detailsPanel("",data_type);
			break;

			case "QRCode":
				ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';

					ret += obj_lblName("", data_type); 
					ret += obj_lblwsd("",data_type);
					ret += obj_lblFontColor("", data_type);
					ret += obj_lblAlignment("", data_type);
					ret += obj_defaultValue("", data_type);
					ret += obj_objectCss("", data_type);
				ret += '</div>'; 
				ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';	
					ret += obj_lblInputName("", data_type); 
					ret += obj_fontType("", data_type);
					//ret += obj_fieldFontFamily("",data_type);
					ret += obj_alignment("",data_type);
					ret += obj_lblFontSize("", data_type);
					ret += obj_lblAllow("", data_type);

				ret += '</div>';

			break;

			case "justImage":
				ret += obj_imageBackground("",data_type);
				ret += obj_chkVisibility("",data_type);
			break;
			case "formDataTable":
				ret  += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
				    ret += obj_lblName("",data_type); 
					//ret += obj_lblFontWeight("", data_type); 
					ret += obj_lblwsd("",data_type);
					ret += obj_tabIndex("", data_type);
				ret += '</div>';
			break;

			case "Form DataTable Properties":
				//left
				ret  += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
					ret += obj_formDataTableActions("", data_type);
				ret += '</div>';
				//right
				ret  += '<div class="fl-floatRight fl-form-prop-cont-wid">';
					// ret += obj_formDataTableSearchField("", data_type);
					// ret += obj_alignment("",data_type);
					ret += obj_formDataTableLookupForm("", data_type);
					ret += obj_formDataTableLookupField("", data_type);
					ret += obj_formDataTableLookupOperator("", data_type);
					ret += obj_formDataTableSearchField("", data_type);
					ret += obj_formDataTableDisplayColumn("", data_type);
				ret += '</div>';
			break;
			case "text-tagging":
			    ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
				    ret += obj_lblName("",data_type); 
					ret += obj_PlaceHolder("", data_type); 
					//ret += obj_lblFontWeight("", data_type); 
					ret += obj_lblwsd("",data_type);
					ret += obj_fldwsd("",data_type);
					ret += fieldFontSize("", data_type);

					ret += obj_fldColor("", data_type);
					ret += obj_fldBGColor("", data_type);
					ret += obj_lblAlignment("", data_type);
					ret += obj_tabIndex("", data_type);

					ret += obj_inputValidation("", data_type);
					ret += obj_inputValidationMessage("", data_type);

					ret += obj_defaultValue("", data_type);
					ret += obj_onchangeFormulaEvent("", data_type);
					ret += obj_objectCss("", data_type);
				ret += '</div>';

				ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
					ret += obj_lblInputName("", data_type); 
					ret += obj_fontType("", data_type);
					ret += obj_fieldFontFamily("",data_type);
					ret += obj_alignment("",data_type);
					
					ret += obj_fieldTextAlignment("",data_type);
					
					ret += obj_Tooltip("", data_type); 
					ret += obj_lblFontSize("", data_type);
					ret += obj_fldMaxLength("", data_type);
					ret += obj_fldWidth("", data_type);
					ret += obj_lblAllow("", data_type);
					ret += obj_lblFontColor("", data_type);
					ret += obj_fldfc("", data_type); 
					ret += obj_chkETrig("", data_type);
					ret += obj_propEcrypt("", data_type);
					ret += obj_chkVisibility("", data_type);
					//ret += obj_chkDisableFormula("", data_type);
					ret += obj_chkReadOnlyFormula("", data_type);
					ret += obj_borderVisibility("", data_type);

					ret += obj_setReadonlyField("",data_type);
					
					ret += obj_middleWareChoice("", data_type);
				ret += '</div>';
			break;
	}

	ret += '</div>';
    ret += '</div>';
	
    return ret;
    
}

function applyPropertyValues(){
	//console.log("ISANG MALAKING THIS",this)
	if(typeof this["popupcontainer"] === "undefined" || typeof this["OID"] === "undefined"){
		return null;
	}else{
		var OID = this["OID"];
		var popupcontainer = this["popupcontainer"];
	}

	{
					// {//first property must be executed
					// 	if(popupcontainer.find('[data-properties-type="lblChoices"]').length >= 1 ){
					//     	var prop_ele = popupcontainer.find('[data-properties-type="lblChoices"]');
					//     	temp_prop_value = prop_ele.val();
				    //         var data_type = prop_ele.attr("data-type");
				    //         var select = "";
				    //         var type = "";
				    //         var eachLine = temp_prop_value.split('\n');
				    //         switch (data_type) {
				    //             case "checkbox":
				    //                 type = "checkbox";
				    //                 var checkbox_name = 'checkbox_' + OID + '[]';
				    //                 var field_name = $("[data-properties-type='lblFldName']").val();
				    //                 if (field_name) {
				    //                     checkbox_name = field_name + "[]";
				    //                 }

				    //                 var rc_align_type = $("#setObject_" + OID).attr("rc-align-type");
				    //                 var rc_elem_alignment = "";
				    //                 if (rc_align_type == "vertical") {
				    //                     rc_elem_alignment = '<span class="rc-prop-alignment rc-align-vertical"></span>';
				    //                 } else if (rc_align_type == "horizontal") {
				    //                     rc_elem_alignment = '<span class="rc-prop-alignment rc-align-horizontal"></span>';
				    //                 }

				    //                 var text = "";
				    //                 var optionValue = "";
				    //                 for (var i = 0, l = eachLine.length; i < l; i++) {
				    //                     text = eachLine[i];
				    //                     optionValue = eachLine[i];
				    //                     if(eachLine[i].indexOf("|")>=0){
				    //                         text = $.trim(eachLine[i].split("|")[0]);
				    //                         optionValue = $.trim(eachLine[i].split("|")[1]);
				    //                     }
				    //                     select += '<label><input type="' + type + '" data-type="longtext" disabled="disabled" name="' + checkbox_name + '" class="getFields getFields_' + OID + '" id="getFields_' + OID + '" value="' + optionValue + '"/> ' + text + ' </label>' + rc_elem_alignment;
				    //                 }

				    //                 this_select = $(select)
				    //                 var allow_label_rc_ele = $('[data-properties-type="lblAllowLabel-RC"]');
				    //                 var element_c_temporary = "";
				    //                 //wrap
				    //                 //<span class="checkbox-input-label">
				    //                 this_select.contents().each(function() {
				    //                     if (prop_ele.prop("tagName") != "INPUT" || prop_ele.prop("tagName") == "SPAN") {
				    //                         prop_ele.wrap('<span class="checkbox-input-label"/>');
				    //                     }
				    //                 })
				    //                 $("#obj_fields_" + OID).html(element_c_temporary = this_select.clone());
				    //                 if (allow_label_rc_ele.val() == "Yes") {
				    //                     $("#obj_fields_" + OID).find(".checkbox-input-label").show();
				    //                     //  element_c_temporary.each(function(){
				    //                     //      if(prop_ele.prop("tagName") == "LABEL" ){
				    //                     //          prop_ele.find(".checkbox-input-label").show();
				    //                     //      }
				    //                     // })
				    //                 } else {
				    //                     $("#obj_fields_" + OID).find(".checkbox-input-label").hide();
				    //                     // element_c_temporary.each(function(){
				    //                     //      if(prop_ele.prop("tagName") == "LABEL" ){
				    //                     //          prop_ele.find(".checkbox-input-label").hide();
				    //                     //      }
				    //                     // })
				    //                 }

				    //                 // if ($(".obj_defaultValueChoices").length >= 1) {
				    //                 //     var temp_elem_clone = "";
				    //                 //     $(".obj_defaultValueChoices").html(temp_elem_clone = this_select.clone());

				    //                 //     $(".obj_defaultValueChoices").find(".checkbox-input-label").show();
				    //                 //     // temp_elem_clone.each(function(){
				    //                 //     //     if(prop_ele.prop("tagName") == "LABEL" ){
				    //                 //     //         prop_ele.find(".checkbox-input-label").show();
				    //                 //     //     }
				    //                 //     // })

				    //                 //     temp_elem_clone.closest(".obj_defaultValueChoices").find(".getFields_" + OID).attr("name", "for_default_use");
				    //                 //     temp_elem_clone.closest(".obj_defaultValueChoices").find(".getFields_" + OID).removeAttr("disabled");

				    //                 // }
				    //                 break;
				    //             case "radioButton":
				    //                 type = "radio";
				    //                 var radio_name = "radio_" + OID;
				    //                 var field_name = $("[data-properties-type='lblFldName']").val();
				    //                 if (field_name) {
				    //                     radio_name = field_name;
				    //                 }
				    //                 var rc_align_type = $("#setObject_" + OID).attr("rc-align-type");
				    //                 var rc_elem_alignment = "";
				    //                 if (rc_align_type == "vertical") {
				    //                     rc_elem_alignment = '<span class="rc-prop-alignment rc-align-vertical"></span>';
				    //                 } else if (rc_align_type == "horizontal") {
				    //                     rc_elem_alignment = '<span class="rc-prop-alignment rc-align-horizontal"></span>';
				    //                 }


				    //                 // for (var i = 0, l = eachLine.length; i < l; i++) {
				    //                 //     select += '<label><input type="' + type + '" data-type="longtext" disabled="disabled" name="' + radio_name + '" class="getFields getFields_' + OID + '" id="getFields_' + OID + '" value="' + eachLine[i] + '"/>' + eachLine[i] + '</label>' + rc_elem_alignment;
				    //                 // }
				    //                 var text = "";
				    //                 var optionValue = "";
				    //                 for (var i = 0, l = eachLine.length; i < l; i++) {
				    //                     text = eachLine[i];
				    //                     optionValue = eachLine[i];

				    //                     if(eachLine[i].indexOf("|")>=0){
				    //                         text = $.trim(eachLine[i].split("|")[0]);
				    //                         optionValue = $.trim(eachLine[i].split("|")[1]);
				    //                     }
				    //                     select += '<label><input type="' + type + '" data-type="longtext" disabled="disabled" name="' + radio_name + '" class="getFields getFields_' + OID + '" id="getFields_' + OID + '" value="' + optionValue + '"/> ' + text + ' </label>' + rc_elem_alignment;
				    //                 }

				    //                 this_select = $(select)
				    //                 var allow_label_rc_ele = $('[data-properties-type="lblAllowLabel-RC"]');
				    //                 var element_c_temporary = "";
				    //                 //wrap
				    //                 //<span class="checkbox-input-label">
				    //                 this_select.contents().each(function() {
				    //                     if (prop_ele.prop("tagName") != "INPUT" || prop_ele.prop("tagName") == "SPAN") {
				    //                         prop_ele.wrap('<span class="radio-input-label"/>');
				    //                     }
				    //                 })
				    //                 $("#obj_fields_" + OID).html(element_c_temporary = this_select.clone());
				    //                 if (allow_label_rc_ele.val() == "Yes") {
				    //                     $("#obj_fields_" + OID).find(".radio-input-label").show();
				    //                 } else {
				    //                     $("#obj_fields_" + OID).find(".radio-input-label").hide();
				    //                 }
				    //                 // if ($(".obj_defaultValueChoices").length >= 1) {
				    //                 //     var temp_elem_clone = "";
				    //                 //     $(".obj_defaultValueChoices").html(temp_elem_clone = this_select.clone());

				    //                 //     temp_elem_clone.closest(".obj_defaultValueChoices").find(".getFields_" + OID).attr("name", "for_default_use");
				    //                 //     temp_elem_clone.closest(".obj_defaultValueChoices").find(".getFields_" + OID).removeAttr("disabled");
				    //                 // }
				    //                 break;
				    //             case "selectMany":
				    //                 var text = "";
				    //                 var optionValue = "";
				    //                 for (var i = 0, l = eachLine.length; i < l; i++) {
				    //                     text = eachLine[i];
				    //                     optionValue = eachLine[i];
				    //                     if(eachLine[i].indexOf("|")>=0){
				    //                         text = $.trim(eachLine[i].split("|")[0]);
				    //                         optionValue = $.trim(eachLine[i].split("|")[1]);
				    //                     }
				    //                     select += '<option value="' + optionValue + '"> ' + text + ' </option>';
				    //                     //select += '<input type="' + type + '" disabled="disabled" name="radio_' + OID + '" class="" id="getFields_' + OID + '"/> ' + eachLine[i] + ' <br />';
				    //                 }
				    //                 $("#getFields_" + OID).html(select);
				    //                 // if ($(".obj_defaultValueChoices").length >= 1) {
				    //                 //     this_select = $(select);
				    //                 //     $(".obj_defaultValueChoices").find(".getFields_" + OID).html(this_select);
				    //                 //     this_select.closest(".obj_defaultValueChoices").find(".getFields_" + OID).attr("name", "for_default_use");
				    //                 //     this_select.closest(".obj_defaultValueChoices").find(".getFields_" + OID).removeAttr("disabled");
				    //                 // }
				    //                 break;
				    //             case "dropdown":
				    //                 var text = "";var optionValue = "";
				    //                 for (var i = 0, l = eachLine.length; i < l; i++) {
				    //                     text = eachLine[i];
				    //                     optionValue = eachLine[i];
				    //                     if(eachLine[i].indexOf("|")>=0){
				    //                         text = $.trim(eachLine[i].split("|")[0]);
				    //                         optionValue = $.trim(eachLine[i].split("|")[1]);
				    //                     }
				    //                     select += '<option value="' + optionValue + '">' + text + '</option>';
				    //                     //select += '<input type="' + type + '" disabled="disabled" name="radio_' + OID + '" class="" id="getFields_' + OID + '"/> ' + eachLine[i] + ' <br />';
				    //                 }
				    //                 $("#getFields_" + OID).html(select);
				    //                 // if ($(".obj_defaultValueChoices").length >= 1) {
				    //                 //     this_select = $(select)
				    //                 //     $(".obj_defaultValueChoices").find(".getFields_" + OID).html(this_select);
				    //                 //     this_select.closest(".obj_defaultValueChoices").find(".getFields_" + OID).attr("name", "for_default_use");
				    //                 //     this_select.closest(".obj_defaultValueChoices").find(".getFields_" + OID).removeAttr("disabled");
				    //                 // }
				    //                 break;
				    //         }
				    //         if($('[data-properties-type="dynamicListing"]').length >= 1 ){
				    //         	temp_prop_value = popupcontainer.find('[data-properties-type="dynamicListing"]').val();
								// if(temp_prop_value){
				    //                 if(temp_prop_value != ""){
				    //                     $(".getFields_" + OID).attr("default-type", "computed");
				    //                     $(".getFields_" + OID).attr("default-formula-value", temp_prop_value);
				    //                 }else{
				    //                     $(".getFields_" + OID).attr("default-type", "");
				    //                     $(".getFields_" + OID).attr("default-formula-value", "");
				    //                 }
				    //             }else{
				    //                 $(".getFields_" + OID).attr("default-type", "");
				    //                 $(".getFields_" + OID).attr("default-formula-value", "");
				    //             }
				    //         }
				    //         defaultValuesFN(OID);
					//     }

					// }
	}

	//applying all the field properties
	var form_ele = $('.workspace.formbuilder_ws');
	var field_obj_ele = form_ele.find(".getFields_"+OID);
	//console.log("ISANG MALAKING KAL",field_obj_ele)
	// console.log(field_obj_ele)
	var field_obj_setobj_ele = field_obj_ele.parents('#setObject_'+OID).eq(0);
	// console.log("field_obj_setobj_ele",field_obj_setobj_ele)
	var temp_prop_value = null;
	if(field_obj_ele.length >= 1 ){
		
		{//applying embed actions events
			if(popupcontainer.find('[data-properties-type="enable_embed_row_click"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[data-properties-type="enable_embed_row_click"]').is(":checked");
				field_obj_ele.attr('enable-embed-row-click',temp_prop_value);
			}
			if(popupcontainer.find('[name="link_caption"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[name="link_caption"]').val();
				field_obj_ele.attr('link-ele-caption',$.trim(temp_prop_value));
				field_obj_ele.prev().filter('.embed_newRequest:not(.custom-actions-add-button)').text($.trim(temp_prop_value));
			}
			if(popupcontainer.find('[data-properties-type="EmbedCopyRowForm"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[data-properties-type="EmbedCopyRowForm"]').is(":checked");
				field_obj_ele.attr('embed-action-click-copy',temp_prop_value);
			}
			//added by japhet morada 10-06-2015
			if(popupcontainer.find('[data-properties-type="EmbedCreateRowForm"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[data-properties-type="EmbedCreateRowForm"]').is(":checked");
				field_obj_ele.attr('embed-action-inline-create',temp_prop_value);
			}
			//=======================================================================
			if(popupcontainer.find('[data-properties-type="EmbedDeleteRowForm"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[data-properties-type="EmbedDeleteRowForm"]').is(":checked");
				field_obj_ele.attr('embed-action-click-delete',temp_prop_value);
			}
			if(popupcontainer.find('[data-properties-type="EmbedEditRowForm"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[data-properties-type="EmbedEditRowForm"]').is(":checked");
				field_obj_ele.attr('embed-action-click-edit',temp_prop_value);
			}
			if(popupcontainer.find('[data-properties-type="EmbedViewRowForm"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[data-properties-type="EmbedViewRowForm"]').is(":checked");
				field_obj_ele.attr('embed-action-click-view',temp_prop_value);
			}
			if(popupcontainer.find('[data-properties-type="EmbedNumberRowForm"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[data-properties-type="EmbedNumberRowForm"]').is(":checked");
				field_obj_ele.attr('embed-action-click-number',temp_prop_value);
			}
			if(popupcontainer.find('[data-properties-type="EmbedEditPopUpRowForm"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[data-properties-type="EmbedEditPopUpRowForm"]').is(":checked");
				field_obj_ele.attr('embed-action-click-edit-popup',temp_prop_value);
			}

			if(popupcontainer.find('[data-properties-type="EmbedViewCustomAction"]').length >= 1) {
				temp_prop_value = popupcontainer.find('[data-properties-type="EmbedViewCustomAction"]').is(":checked");
				field_obj_ele.attr('embed-action-custom-action',temp_prop_value);
			}
		}
		

		function waitBeforeCommit(){
							//applying label
							if(popupcontainer.find('[data-properties-type="lblName"]').length >= 1){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblName"]').val();
								if(temp_prop_value !== ''){
									field_obj_setobj_ele.find('#label_'+OID+'.obj_label').eq(0).children('#lbl_'+OID).html(temp_prop_value);
								}
							}
							//applying fieldname
							if(popupcontainer.find('[data-properties-type="lblFldName"]').length >= 1){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblFldName"]').val();
								if(field_obj_ele.is('input[type="checkbox"]') || field_obj_ele.is('select[multiple]') ){
									field_obj_ele.attr('name',temp_prop_value+'[]');
								}else{
									field_obj_ele.attr('name',temp_prop_value);
								}
							}
							//applying placeholder
							if( popupcontainer.find('[data-properties-type="lblPlaceHolder"]').length >= 1 ){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblPlaceHolder"]').val();
								if(field_obj_setobj_ele.is('[data-type="noteStyleTextarea"]')){
									field_obj_ele.attr('placeholder',temp_prop_value);
									field_obj_ele.parents('.note-style-wrap-contents').eq(0).find('.content-editable-field_'+OID).attr('placeholder',temp_prop_value)
								}else{
									field_obj_ele.attr('placeholder',temp_prop_value);
								}
							}
							//applying tooltip hovers
							if( popupcontainer.find('[data-properties-type="lblTooltip"]').length >= 1 ){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblTooltip"]').val();
								if(true){
									$("#getFields_" + OID).attr("data-original-title", temp_prop_value);
									$("#getFields_" + OID).attr("title", temp_prop_value);
									$(".content-editable-field_" + OID).attr("data-original-title", temp_prop_value);
									$(".content-editable-field_" + OID).attr("title", temp_prop_value);
									// field_obj_ele
								}
							}
							if( popupcontainer.find('[data-properties-type="lblFontWg"]').length >= 1  ){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblFontWg"]').val()
								if(temp_prop_value === '------Select------'){temp_prop_value = ''};
								field_obj_setobj_ele.find('#label_'+OID+'.obj_label').eq(0).children('#lbl_'+OID).css({
									"font-weight":temp_prop_value
								});
							}
							if( popupcontainer.find('[data-properties-type="lblFontSize"]').length >= 1 ){;
								temp_prop_value = popupcontainer.find('[data-properties-type="lblFontSize"]').val();
								if(temp_prop_value === '------Select------'){temp_prop_value = '';};
								field_obj_setobj_ele.find('#label_'+OID+'.obj_label').eq(0).children('#lbl_'+OID).css({
									"font-weight":temp_prop_value
								});
							}

							if( popupcontainer.find('[data-properties-type="fieldFontSize"]').length >= 1 ){
								temp_prop_value = popupcontainer.find('[data-properties-type="fieldFontSize"]').val();
								if(temp_prop_value === '------Select------'){temp_prop_value = '';};
								field_obj_setobj_ele.find('#label_'+OID+'.obj_label').eq(0).children('#lbl_'+OID).css({
									"font-weight":temp_prop_value
								});
							}

							if( popupcontainer.find('[data-properties-type="lblMaxLength"]').length >= 1 ){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblMaxLength"]').val()
								field_obj_ele.attr("maxlength", temp_prop_value);
							}

							if( popupcontainer.find('[data-properties-type="lblFieldHeight"]').length >= 1 ){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblFieldHeight"]').val()
								field_obj_ele.css("height", Number(temp_prop_value)+"px");
								if(field_obj_ele.is('textarea')){
									temp_prop_value = field_obj_ele.closest(".setObject").outerHeight();
									field_obj_ele.closest(".setObject").css({"height": (temp_prop_value) + "px"});
								}
							}
							if( popupcontainer.find('[data-properties-type="lblFieldWidth"]').length >= 1 ){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblFieldWidth"]').val();
								field_obj_ele.css("width", Number(temp_prop_value)+"px");
								field_obj_setobj_ele.css({
									"width":"auto"
								});
								field_obj_setobj_ele.css({
									"width":field_obj_setobj_ele.outerWidth()
								});
								field_obj_ele.css("width", (100)+"%");
							}
							if( popupcontainer.find('[data-properties-type="lblFieldType"]').length >= 1 ){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblFieldType"]').val();
								field_obj_ele.attr("data-type", temp_prop_value);
							}
							if( popupcontainer.find('[data-properties-type="lblAllowLabel"]').length >= 1 ){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblAllowLabel"]').val();
								if (temp_prop_value == "Yes") {
								    field_obj_setobj_ele.find("#label_" + OID).show();
								} else {
								    field_obj_setobj_ele.find("#label_" + OID).hide();
								}
							}

							if(popupcontainer.find(".chngColor[data-properties-type='lblFieldColor']").length >= 1 ){
								temp_prop_value = popupcontainer.find(".chngColor[data-properties-type='lblFieldColor']").val();
								field_obj_ele.css("border-color", temp_prop_value);
								$(".content-editable-field_" + OID).css("border-color", temp_prop_value);
							}

							if(popupcontainer.find(".chngColor[data-properties-type='lblFontolor']").length >= 1 ){
								temp_prop_value = popupcontainer.find(".chngColor[data-properties-type='lblFontolor']").val();
								field_obj_setobj_ele.find('#lbl_'+OID).css("color", temp_prop_value);
							}

							if(popupcontainer.find('[data-properties-type="lblAlignment"]').length >= 1 ){
								temp_prop_value = popupcontainer.find('[data-properties-type="lblAlignment"]').val();
								// field_obj_setobj_ele.find('#lbl_'+OID).css("color", temp_prop_value);
								if (temp_prop_value == "alignLeft") {
									field_obj_setobj_ele.find("#label_" + OID).addClass("lbl-aligned-left");
					                field_obj_setobj_ele.find("#label_" + OID).children("*").wrap("<div class='lbl-aligned-left-wrap'/>");
					                field_obj_setobj_ele.find("#label_" + OID).resizable({
					                    handles: "e"
					                });

					                field_obj_setobj_ele.find("#label_" + OID).children(".ui-resizable-handle").appendTo(field_obj_setobj_ele.find("#label_" + OID).children(".lbl-aligned-left-wrap").eq(0));

					                field_obj_setobj_ele.find("#obj_fields_" + OID).addClass("obj_f-aligned-left");

					                field_obj_setobj_ele.find("#obj_fields_" + OID).children("*").wrapAll("<div class='obj_f-aligned-left-wrap'/>");

					                field_obj_setobj_ele.find("#label_" + OID).add(field_obj_setobj_ele.find("#obj_fields_" + OID)).wrapAll("<div class='align-left-wrapper'/>").wrapAll("<div class='align-left-wrapper-table'/>").wrapAll("<div class='align-left-wrapper-row'/>");
								}else{
									if (field_obj_setobj_ele.find("#label_" + OID).hasClass("ui-resizable")) {
					                    field_obj_setobj_ele.find("#label_" + OID).resizable("destroy");
					                }
					                var ele_label_field = field_obj_setobj_ele.find("#label_" + OID).add(field_obj_setobj_ele.find("#obj_fields_" + OID));
					                var ele_left_wrapper = ele_label_field.parents(".align-left-wrapper").eq(0);
					                if (ele_left_wrapper.length >= 1) {

					                    ele_left_wrapper.after(ele_label_field);
					                    ele_left_wrapper.remove();
					                    ele_label_field.each(function() {
					                        $(this).children(".ui-resizable-handle").remove();
					                    })
					                }
					                field_obj_setobj_ele.find("#label_" + OID).find(".lbl-aligned-left-wrap").eq(0).children("*").unwrap();
					                field_obj_setobj_ele.find("#obj_fields_" + OID).find(".obj_f-aligned-left-wrap").eq(0).children("*").unwrap();
					                field_obj_setobj_ele.find("#label_" + OID).removeClass("lbl-aligned-left");
					                field_obj_setobj_ele.find("#label_" + OID).css("width", "");
					                field_obj_setobj_ele.find("#obj_fields_" + OID).removeClass("obj_f-aligned-left");
								}
							}

							if(popupcontainer.find('#refreshtrigger').length >= 1 ){
								temp_prop_value = popupcontainer.find('#refreshtrigger').is(":checked");
								field_obj_ele.attr("lookup-event-trigger", temp_prop_value);
							}

							if( popupcontainer.find('[data-properties-type="tabIndex"]').length >= 1 && popupcontainer.find('[data-properties-type="tabIndex"]').val() !== '' ){
								temp_prop_value = popupcontainer.find('[data-properties-type="tabIndex"]').val();
					            field_obj_ele.attr("tabindex", value);
							}
							if(popupcontainer.find('.visibility-formula').length >= 1 && $('.chk_Visibility').length >= 1 ){
						    	temp_prop_value = popupcontainer.find('.visibility-formula').val();
						    	if(temp_prop_value != ''){
									field_obj_ele.attr('field-visible','computed');
									field_obj_ele.attr('visible-formula',temp_prop_value);
						    	}
						    }
						    if(popupcontainer.find(".obj_inputValidation").length >= 1 && popupcontainer.find(".obj_inputValidation").val() !== ''){
						    	temp_prop_value = popupcontainer.find(".obj_inputValidation").children(".field-formula-validation").eq(0).val();
					    		field_obj_ele.attr("field-formula-validation", temp_prop_value);
						    }
						    if(popupcontainer.find('[data-properties-type="DisableFormula"]').length >= 1 && popupcontainer.find('[data-properties-type="DisableFormula"]').val() !== '' ){
						    	temp_prop_value = popupcontainer.find('[data-properties-type="DisableFormula"]').val();
						    	field_obj_ele.attr("disable-formula",temp_prop_value);
						    }
						    if(popupcontainer.find('.field-formula-validation-message').length >= 1 && popupcontainer.find('.field-formula-validation-message').val() !== ''){
						    	temp_prop_value = popupcontainer.find('[data-properties-type="DisableFormula"]').val();
						    	field_obj_ele.attr("field-formula-validation-message", temp_prop_value);
						    }
						    if(popupcontainer.find('[data-properties-type="borderVisibility"]').length >= 1 ){
						    	temp_prop_value = popupcontainer.find('[data-properties-type="borderVisibility"]').val();
						    	if(temp_prop_value == "No"){
						    		field_obj_ele.css("border", "0px");
						    	}else{
						    		field_obj_ele.css("border", "");
						    	}
						    }
						    if(popupcontainer.find('.chngColor[data-properties-type="lblFieldBGColor"]').length >= 1 && popupcontainer.find('.chngColor[data-properties-type="lblFieldBGColor"]').val() !== ''){
						    	temp_prop_value = popupcontainer.find('.chngColor[data-properties-type="lblFieldBGColor"]').val();
						    	$(".content-editable-field_" + OID).css("background-color", temp_prop_value);
						    	field_obj_ele.attr("style", field_obj_ele.attr('style') + ";background-color:" + temp_prop_value + '!important');
						    	$(".content-editable-field_" + OID).attr("style", $(".content-editable-field_" + object_id).attr('style') + ";background-color:" + temp_prop_value + '!important');
						    }
						    if(popupcontainer.find('[data-properties-type="setReadOnlyField"]').length >= 1 ){
						    	temp_prop_value = popupcontainer.find('[data-properties-type="setReadOnlyField"]').val();
						    	if(temp_prop_value == "Yes"){
			                        field_obj_ele.attr("readonly","readonly");
			                    }else{
			                        field_obj_ele.removeAttr("readonly");
			                    }
						    }
						    if(popupcontainer.find('[data-properties-type="defaultValue"]').length >= 1 && popupcontainer.find('[data-properties-type="defaultValue"]').val() !== '' && popupcontainer.find('[name="default_val"][data-properties-type="defaultValueType"]').length >= 1 ){
						    	var default_value_type = popupcontainer.find('[name="default_val"][data-properties-type="defaultValueType"]').val();
						    	temp_prop_value = popupcontainer.find('[data-properties-type="defaultValue"]').val();
						    	field_obj_ele.attr('default-type',default_value_type);
						    	if(default_value_type == "static"){
						    		field_obj_ele.attr('default-static-value',temp_prop_value);
						    	}else if(default_value_type == "computed"){
						    		field_obj_ele.attr('default-formula-value',temp_prop_value);
						    	}else if(default_value_type == "middleware"){
						    		field_obj_ele.attr('default-middleware-value',temp_prop_value);
						    		if(popupcontainer.find('[data-properties-type="middlewareTagging"]:visible').length >= 1 && popupcontainer.find('[data-properties-type="middlewareTagging"]:visible').val() !== ''){
						    			temp_prop_value = popupcontainer.find('[data-properties-type="middlewareTagging"]:visible').val();
						    			field_obj_ele.attr('middlewaretagging',temp_prop_value);
						    		}
						    	}
						    }
						    if(popupcontainer.find('.multiple_field_value .obj_defaultValueChoices').length >= 1 ){
						    	var cloned_default_choices = popupcontainer.find('.multiple_field_value .obj_defaultValueChoices').find(".getFields");
						    	var element_on_form = $(".getFields_" + OID);
						    	if(cloned_default_choices.is("select[multiple]")){
						    		console.log("element_on_form",field_obj_ele);
					    			element_on_form.val(cloned_default_choices.children('option:selected').map(function(){
					    				return ($(this).attr("value"))?$(this).attr("value"):null;
					    			}).get());
						    	}else if(cloned_default_choices.is("input[type='checkbox']")){
						    		console.log("element_on_form",field_obj_ele);
						    		element_on_form.val(cloned_default_choices.filter(":checked").map(function(){
					    				return ($(this).attr("value"))?$(this).attr("value"):null;
					    			}).get());
						    	}
						    }

						    


						    

		}


							

	}
}


// Hide / Show Properties of objects
function prop_hovering() {
    $("body").on("mouseenter",".workspace.formbuilder_ws .setObject,.workspace.formbuilder_ws .setObject .obj_actions",function(){
		var object_id = $(this).attr("data-object-id");
		$("#obj_actions_"+object_id).removeClass("display")
		/*$("#setObject_"+object_id).css("border-color","#ddd");*/
	    if($("#setObject_" + object_id).hasClass("workspace_bckgrnd"))
	    {

	    }
	    else{
	    $("#setObject_" + object_id).addClass("hovering_object");
    	}
    });
    $("body").on("mouseleave",".setObject",function(){
	var object_id = $(this).attr("data-object-id");
	$("#obj_actions_"+object_id).addClass("display")
	/*$("#setObject_"+object_id).css("border-color","transparent");*/
	    $(".hovering_object").removeClass("hovering_object");
    });
}

/***************************** Object Properties ***********************************/
	
    /*
    *
    *
    */
    function ecryptionSwitch(obj_float,data_type){
    	var ret = "";
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container set_encryption">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below set_encryption_prop">'+
				    	//'<div class="label_below2">Set as Readonly: <font color="red"></font></div>'+
						'<div class="input_position_below section clearing fl-field-style">'+ //
							'<div class="column div_1_of_1">'+	
								'<span>Set Encryption: <font color="red"></font></span>'+
								'<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="set_encryption" data-object-id="">'+
									'<option value="No">No</option>'+
									'<option value="Yes">Yes</option>'+
							    '</select>'+
						    '</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_show_view(obj_float,data_type){
    	var ret
     	=	'<div class="properties_width_container ">'+
 			    '<div class="fields_below">'+
 					'<div class="input_position_below">'+
 				    	//'<div class="label_below2">Allow View: <font color="red"></font></div>'+
 						'<div class="input_position_below section clearing fl-field-style">'+
 							'<div class="column div_1_of_1">'+
 								'<span>Allow View: <font color="red"></font></span>'+
	 							'<select name="" class="show-picklist-view form-select" id=""  data-object-id="">'+
	 								'<option value="No">No</option>'+
	 								'<option value="Yes">Yes</option>'+
	 						    '</select>'+
 							'</div>'+
 						'</div>'+
 					'</div>'+
 			    '</div>'+
 			'</div>';
 		return ret;
    }
    function picklist_other_properties(obj_float,data_type){
		var ret
     	=	'<div class="properties_width_container ">'+
 			    '<div class="fields_below">'+
 					'<div class="input_position_below">'+
 				    	//'<div class="label_below2">Allow View: <font color="red"></font></div>'+
 						'<div class="input_position_below section clearing fl-field-style">'+
 							'<div class="column div_1_of_1">'+
 								'<label><input type="checkbox" class="hide-picklist-pagination-property"/> Hide Pagination<font color="red"></font></label>'+
 							'</div>'+
 						'</div>'+
 					'</div>'+
 			    '</div>'+
 			'</div>';
 		return ret;
    }
    function obj_allow_values_not_in_list(obj_float,data_type){
 		var ret
     	=	'<div class="properties_width_container ">'+
 			    '<div class="fields_below">'+
 					'<div class="input_position_below">'+
 				    	/*'<div class="label_below2">Allow Values Not in List: <font color="red"></font></div>'+*/
 						'<div class="input_position_below section clearing fl-field-style">'+
 							'<div class="column div_1_of_1">'+
 								'<span>Allow Values Not in List: <font color="red"></font></span>'+
	 							'<select name="" class=" allow-values-not-in-list form-select" id=""  data-object-id="">'+
	 								'<option value="No">No</option>'+
	 								'<option value="Yes">Yes</option>'+
	 						    '</select>'+
 							'</div>'+
 						'</div>'+
 					'</div>'+
 			    '</div>'+
 			'</div>';
 		return ret;
    }
    function obj_default_embed_sorting(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container embedDefaultSorting">'+
    		    '<div class="fields_below">'+
    				//'<div class="label_below2">Default Column Sorting: <font color="red"></font></div>'+
    				'<div class="input_position_below section clearing fl-field-style">'+
    					'<div class="choice-content column div_1_of_1">'+
    						'<span>Default Column Sorting: <font color="red"></font></span>'+
    						//embed-source-lookup-field
    						'<select class="form-select edsc-prop edsc-field-name" data-type="'+data_type+'" style="width:78.7%; margin-right:4px;" ></select>'+
    						'<select class="form-select edsc-prop edsc-sort-type" data-type="'+data_type+'" style="width:20%;" >'+
    							'<option value="ASC">Asc</option>'+
    							'<option value="DESC">Desc</option>'+
    						'</select>'+
    						
    					'</div>'+
    				'</div>'+
    		    '</div>'+
    		'</div>';
    	return ret;
    }

    function obj_embed_row_selection(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container embedRowSelection">'+
    		    '<div class="fields_below">'+
    				//'<div class="label_below2">Default Column Sorting: <font color="red"></font></div>'+
    				'<div class="input_position_below section clearing fl-field-style">'+
    					'<div class="choice-content column div_1_of_1">'+
    					'<span>Row Properties</span>'+
    						//embed-source-lookup-field
    						// '<select class="form-select edsc-prop edsc-field-name" data-type="'+data_type+'" style="width:78.7%; margin-right:4px;" ></select>'+
    						// '<select class="form-select edsc-prop edsc-sort-type" data-type="'+data_type+'" style="width:20%;" >'+
    						// 	'<option value="ASC">Asc</option>'+
    						// 	'<option value="DESC">Desc</option>'+
    						// '</select>'+
    						'<label style="display:block;margin-bottom:5px;"><input type="checkbox" data-type="'+data_type+'" class="css-checkbox embed_row_selection_prop" name="allow_record_selection" id="allow_record_selection" /><label for="allow_record_selection" class="css-label"></label> Allow Row Selection</label>'+
    						'<label style="display:block;margin-bottom:5px;"><input type="checkbox" data-type="'+data_type+'" class="css-checkbox embed_row_category_prop" name="allow_row_category" data-properties-type="embedAllowRowCategory" id="allow_row_category" /><label for="allow_row_category" class="css-label"></label> Allow Row Category</label>'+
    						'<div class="column div_1_of_1 column_category_selection">'+
    							'<span>Select Column to be Categorized</span><span style="float:right; cursor: pointer;" class="single-form-row-category-refresh"><i class="fa fa-refresh"></i></span>'+
    							'<select class="form-select embed_row_category_column_dropdown" name="embed_row_category_column_dropdown"></select>'+
    						'</div>'+
    					'</div>'+
    				'</div>'+
    		    '</div>'+
    		'</div>';
    	return ret;
    }


    function obj_enablePicklistAdd(obj_float,data_type){
 		var ret
     	=	'<div class="properties_width_container ">'+
 			    '<div class="fields_below">'+
 					'<div class="input_position_below enablePicklistAdd_prop">'+
 				    	//'<div class="label_below2">Enable Add Entry: <font color="red"></font></div>'+
 						'<div class="input_position_below section clearing fl-field-style">'+
 							'<div class="column div_1_of_1">'+
 								'<span>Enable Add Entry: <font color="red"></font></span>'+
	 							'<select name="" class=" enablePicklistAdd form-select" id=""  data-object-id="">'+
	 								'<option value="No">No</option>'+
	 								'<option value="Yes">Yes</option>'+
	 						    '</select>'+
 							'</div>'+
 						'</div>'+
 					'</div>'+
 			    '</div>'+
 			'</div>';
 		return ret;
    }
    function obj_detailsPanel(obj_float,data_type){
    	var ret = "";
    	//new DP
    	ret += '<div class="hr"></div>';
    	ret += '</div>';
    	//ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">';
    	    ret += '<div class="content-dialog" style="height: auto;margin-bottom:0x">';
    	        ret +='</br>';
    	        // ret += '<label><input type="checkbox" value="yes" class="css-checkbox detail-default-view" id="defaultApplicationView" /><label for="defaultApplicationView" class="css-label"></label> Show details on load</label>'
    	        // ret += '</br>';
    	        // ret += '</br>';
    	        ret += '<div class= "detP-master-container">';
    	            ret += '<div class = "detP-slave-container"><div><span class="detP-add-minus-row"><i class="fa fa-plus detP-add-row"></i> <i class="fa fa-minus detP-minus-row"></i></span></div>';
    	                ret += '<input type="text" class="form-text" data-properties-type = "details-panel-name" />';
    	                ret += '</br>';
    	                ret += '</br>';

    	                    ret += '<table class="detP-table-master">';
    	                        ret += '<thead>';
    	                            ret += '<tr>';
    	                                ret += '<th>Title</th>';
    	                                ret += '<th>Formula</th>';
    	                            ret += '</tr>';
    	                        ret += '</thead>';
    	                        ret += '<table class="detP-table-slave">';
    	                            ret += '<tbody class="detP-tr-group">';
    	                                ret += '<tr>'; 
    	                                    ret += '<td colspan="2"><div><span class="detP-add-minus-row"><i class="fa fa-plus detP-add-row-inner"></i> <i class="fa fa-minus detP-minus-row-inner"></i></span></div></td>';
    	                                ret += '</tr>';
    	                                ret += '<tr>';
    	                                    ret += '<td class="details-panel-title-td"><input type="text" data-properties-type="details-panel-title" class="form-text"/></td>';
    	                                    ret += '<td colspan="2"><textarea class="detP-formula form-textarea" data-properties-type="details-panel-formula" data-ide-properties-type="detP-formula"></textarea><label><input type="checkbox" value="yes" class="css-checkbox linkmakerClass" id="linkmaker0" /><label for="linkmaker0" class="linkmakerLabel css-label"></label> Create Link</label>';
    	                                        
    	                                    ret += '</td>';
    	                                    
    	                                ret += '</tr>';
    	                                ret += '<tr class="link-form-select">';
    	                                    ret += '<td colspan="2">';
    	                                        ret += '<select class="form-select" data-properties-type="linkInputAdder-field" data-object-id="" id="">';
    	                                        ret += '</select>';
    	                                    ret += '</td>';
    	                                ret += '</tr>';
    	                                
    	                                ret += '<tr class="linkInputAdder">';
    	                                    ret += '<td colspan="2">';
    	                                        ret += '<table class="linkInputAdder-table">';
    	                                            ret += '<tbody class="linkInputAdder-table-tbody">';

    	                                                ret += '<tr>'; 
    	                                                    ret += '<td colspan="3"><div><span class="detP-add-minus-row"><i class="fa fa-plus linkInputAdder-add-row"></i> <i class="fa fa-minus linkInputAdder-minus-row"></i></span></div></td>';
    	                                                ret += '</tr>';
    	                                                ret += '<tr>';
    	                                                    ret += '<td><select class="form-select" data-properties-type="linkInputAdder-field" data-object-id="" id="">';
    	                                                    ret += '</select></td>';
    	                                                    ret += '<td><select class="form-select" data-properties-type="linkInputAdder-operator" data-object-id="" id="">';
    	                                                    ret += '</select></td>';
    	                                                    ret += '<td><input type="text" data-properties-type="linkInputAdder-value" class="form-text"/></td>';
    	                                                ret += '</tr>';
    	                                            ret += '</tbody>';
    	                                        ret += '</table>';
    	                                    ret += '</td>';
    	                                    
    	                                ret += '</tr>';
    	                            ret += '</tbody>';
    	                        ret += '</table>';
    	                    ret += '</table>';
    	            ret +='</div>';
    	        ret += '</div>';
    	    ret += '</div>';
    	//ret += '</div>';
    	ret += '<div class="fields">';
    	ret += '<div class="label_basic"></div>';
    	ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
    	
    	ret += '</div>';
    	ret += '</div>';


    	return ret;
    }
    function obj_picklistActionVisibility(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container picklistActionVisibility">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below picklistActionVisibility_prop">'+
				    	//'<div class="label_below2">Picklist Action Visibility: <font color="red"></font></div>'+
						'<div class="input_position_below section clearing fl-field-style">'+
							'<div class="column div_1_of_1">'+
								'<span>Picklist Action Visibility: <font color="red"></font></span>'+
								'<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="picklistActionVisibility" data-object-id="">'+
									'<option value="Yes">Yes</option>'+
									'<option value="No">No</option>'+
							    '</select>'+
						    '</div>'+
						    /*balik ka dito*/
						 //    '</br>'+
						 //    '</br>'+
						 //    '<div class="isDisplayNone type-ahead-option">'+
							//     '<label class="label_below2">Action option:</label>'+
							//     '</br>'+
							//     '</br>'+
							//     '<label><input type="radio"  value="Yes" name="type_ahead_option" data-type="' + data_type + '" class=" css-checkbox" data-properties-type="typeAheadOption" data-object-id="" id="allowTypeAhead" /><label for="allowTypeAhead" class="css-label"></label> Typeahead </label>'+
							//     '<label><input type="radio" checked="true" value="No" name="type_ahead_option" data-type="' + data_type + '" class=" css-checkbox" data-properties-type="typeAheadOption" data-object-id="" id="disAllowTypeAhead" /><label for="disAllowTypeAhead" class="css-label"></label> Pop-up</label>'+
							// '</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }

    function obj_hideModalCloseBtn(obj_float, data_type){

    	var ret = '<div class="' + obj_float + ' properties_width_container  obj_hideModalCloseBtn">'+
    				'<div class="fields_below">'+
    					'<div class="input_position_below obj_hideModalCloseBtn_prop">'+
    						//'<div class="label_below2">Hide Modal Close Button:</div>'+
    						'<div class="input_position_below section clearing fl-field-style">'+
    							'<div class="column div_1_of_1">'+
    								'<span>Hide Modal Close Button:</span>'+
		    						'<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="obj_hideModalCloseBtn" data-object-id="">'+
										'<option value="No">No</option>'+
										'<option value="Yes">Yes</option>'+
								    '</select>'+	
								'</div>'+
							'</div>'+    	
						'</div>'+		
					'</div>'+		
				'</div>';
			return ret;		
    }

    function obj_lblIdentifier(obj_float,data_type){
    	var ret = "";
    	ret += '<div class="' + obj_float + ' properties_width_container">';
    	    ret += '<div class="fields_below">';
    		//ret += '<div class="label_below2">Label Identifier:(Readonly)</div>';
    		ret += '<div class="input_position_below section clearing fl-field-style">';
    			ret += '<div class="column div_1_of_1">';
    				ret += '<span>Label Identifier:(Readonly)</span>';
    		    	ret += '<input type="text" disabled="disabled" name="" data-type="' + data_type + '" class="form-text obj_prop label-identifier" id="" data-properties-type="lblIdentifier" data-object-id="" placeholder="Label Name" readonly>';
    			ret += '</div>';
    		ret += '</div>';
    	    ret += '</div>';
    	ret += '</div>';
    	    return ret;
    }

    function obj_fldfc(obj_float,data_type) {
		var ret
		=	'<div class="' + obj_float + ' properties_width_container propfieldFontColor">'+
				'<div class="fields_below">'+
					//'<div class="label_below2">Field Font Color:</div>'+
					'<div class="input_position_below section clearing fl-field-style">'+ //
						'<div class="column div_1_of_1">'+
							'<span>Field Font Color:</span><Br/>'+
							'<input type="text" data-type="' + data_type + '" class="chngColor" data-properties-type="fld_fc" data-type="button"/>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
	    return ret;
    }

    function obj_fldwsd(obj_float,data_type){
    	var ret = '<div class="' + obj_float + ' properties_width_container">'+
	    			'<div class="fields_below ">'+ //fl-controls-genProperties
	    				//'<div class="label_below2">Field Font Format:</div>'+
	    				'<div class="input_position_below section clearing fl-field-style fl-textfield-type-opt">'+ //
	    					'<div class="column div_1_of_1">'+
	    						'<span>Field Font Format:</span>'+	
	    					 	'<span class="spanbutton" style="margin-left:3px"><input class="textdecor fl-input-form-settings prop_field_font_weight" type="checkbox" value="bold" id="pb1" data-properties-type = "fld_fw"><label for="pb1"><i class="fa fa-bold textpos"></i></label></span>'+
								'<span class="spanbutton" style="margin-left:3px"><input class="textdecor fl-input-form-settings prop_field_font_style" type="checkbox" value="italic" id="pb2" data-properties-type = "fld_fs"><label for="pb2"><i class="fa fa-italic textpos"></i></label></span>'+
							 	'<span class="spanbutton" style="margin-left:3px"><input class="textdecor fl-input-form-settings prop_field_text_decoration" type="checkbox" value="underline" id="pb3" data-properties-type = "fld_td"><label for="pb3"><i class="fa fa-underline textpos"></i></label></span>'+
	    					'</div>'+
	    				'</div>'+
	    			'</div>'+
	    		'</div>';
	    return ret;
    }
    function obj_lblwsd(obj_float,data_type){
	    var ret	= '<div class="' + obj_float + ' properties_width_container">'+
	    			'<div class="fields_below ">'+ //fl-controls-genProperties
	    				//'<div class="label_below2">Label Font Format:</div>'+
	    				'<div class="input_position_below section clearing fl-field-style fl-textfield-type-opt">'+
	    					 '<div class="column div_1_of_1">'+
	    					 	 '<span>Label Font Format:</span>'+
		    					 '<span class="spanbutton" style="margin-left:3px"><input class="textdecor fl-input-form-settings prop_label_font_weight" type="checkbox" value="bold" id="pa1" data-properties-type = "lblFontWg"><label for="pa1"><i class="fa fa-bold textpos"></i></label></span>'+
								 '<span class="spanbutton" style="margin-left:3px"><input class="textdecor fl-input-form-settings prop_label_font_style" type="checkbox" value="italic" id="pa2" data-properties-type = "lbl_fs"><label for="pa2"><i class="fa fa-italic textpos"></i></label></span>'+
								 '<span class="spanbutton" style="margin-left:3px"><input class="textdecor fl-input-form-settings prop_label_text_decoration" type="checkbox" value="underline" id="pa3" data-properties-type = "lbl_td"><label for="pa3"><i class="fa fa-underline textpos"></i></label></span>'+
	    					'</div>'+
	    				'</div>'+
	    			'</div>'+
	    		'</div>';
	    return ret;
    }

    function obj_embededSourceForm(obj_float,data_type){

    	var ret = '<div class=" ' + obj_float + ' new-embed-source-form-data-filtering">'+
    			   		'<div class="fields_below">'+
    			   				
    			   				'<div class=" properties_width_container">'+
	    			   				'<div class="fields_below">'+
	    			   					'<div class="label_below2">Embedded Source Form:</div>'+
	    			   					'<div class="input_position_below">'+
		    			   					'<select class="form-select fl-embed-select-source-form">'+
												'<option value="">--Select--</option>'+
											'</select>'+
										'</div>'+
	    			   				'</div>'+
    			   				'</div>'+
    			   				'<div class="fl-tbl-embed-source-form-content">'+
    			   					'<div class=" properties_width_container">'+
		    			   				'<div class="fields_below">'+
		    			   					'<div class="label_below2">Advance Filtering:</div>'+
		    			   					'<div class="input_position_below">'+
											'</div>'+
		    			   				'</div>'+
	    			   				'</div>'+
				   					'<table class="fl-tbl-embed-source-form" style="width:100%;">'+
				   						'<thead>'+

				   						'</thead>'+
				   						'<tbody>'+
				   							'<tr>'+ //style="line-height:0px;"

				   								/*'<td colspan="5">'+
				   									'<div class=" properties_width_container">'+
				   										'<div class="fields_below">'+
		    			   									'<div class="label_below2">Group name:</div>'+
		    			   									'<div class="input_position_below">'+
				   												'<input type="text" class="form-text fl-embed-filter-group-name" placeholder="Type group your name">'+
				   											'</div>'+
				   										'</div>'+
				   									'</div>'+
				   								'</td>'+*/
				   								'<td>'+
				   									'<div class="properties_width_container" style="max-height:0px;">'+//style="max-width:100px;"
				   										'<div class="fields_below forTblAddRemoveItem">'+
		    			   									'<div class="label_below2"></div>'+
		    			   									'<div class="input_position_below" style="position:absolute;right:0px;margin: -33px 80px;">'+
							   									'<select class="form-select fl-embed-filter-group-and-or" style="display:none;">'+
							   										'<option>AND</option>'+
							   										'<option>OR</option>'+
							   									'</select>'+
							   									'</div>'+
				   											'</div>'+
				   									'</div>'+		
				   								'</td>'+
				   								'<td>'+
				   									'<div class=" properties_width_container" style="max-height:0px; margin:0 auto; text-align:Center;">'+
				   										'<div class="fields_below forTblAddRemoveItem">'+
		    			   									'<div class="label_below2"></div>'+
		    			   									'<div class="input_position_below" style="position:absolute;right:0px;margin:-30px 30px;">'+
				   												'<i class="fa fa-plus fl-addRemoveItem"></i> ' + '<i class="fa fa-minus fl-addRemoveItem" style="display:none;"></i>'+
				   											'</div>'+
				   										'</div>'+
				   									'</div>'+
				   								'</td>'+
				   								 //end of group name
				   							'</tr>'+ 
				   							'<tr>'+
				   								'<td>'+
				   									'<div class="properties_width_container">'+
			   											'<div class="fields_below forTblAddRemoveItem">'+
			    			   									'<div class="label_below2">Filter Condition:</div>'+
			    			   									'<div class="input_position_below">'+
				    			   									'<select class="form-select fl-embed-filter-condition-and-or" style="display:none;">'+
								   										'<option value="and">AND</option>'+
								   										'<option value="or">OR</option>'+
								   									'</select>'+
			    			   									'</div>'+
			    			   							'</div>'+
			    			   						'</div>'+
				   								'</td>'+
				   								'<td>'+
				   									'<div class="properties_width_container">'+
			   											'<div class="fields_below">'+
			    			   									'<div class="label_below2"></div>'+
			    			   									'<div class="input_position_below">'+
			    			   									// '<input type="text" class="form-text " placeholder="Column">'+
			    			   									'<select class="form-select fl-embed-filter-condition-sform-field">'+
			    			   										'<option value="">--Select--</option>'+
			    			   									'</select>'+
			    			   									'</div>'+
			    			   							'</div>'+
			    			   						'</div>'+
				   								'</td>'+
				   								'<td>'+
				   									'<div class="properties_width_container">'+
			   											'<div class="fields_below forTblAddRemoveItem">'+
			    			   									'<div class="label_below2"></div>'+
			    			   									'<div class="input_position_below">'+
				    			   									'<select class="form-select fl-embed-filter-condition-operator">'+
								   										'<option value="=">==</option>'+
								   										'<option value="<>">!=</option>'+
								   									'</select>'+
			    			   									'</div>'+
			    			   							'</div>'+
			    			   						'</div>'+
				   								'</td>'+
				   								'<td>'+
				   									'<div class="properties_width_container">'+
			   											'<div class="fields_below forTblAddRemoveItem">'+
			    			   									'<div class="label_below2"></div>'+
			    			   									'<div class="input_position_below">'+
				    			   									'<select class="form-select fl-embed-filter-condition-value-switcher">'+
								   										'<option value="static">Static</option>'+
								   										'<option value="dynamic">Computed</option>'+
								   									'</select>'+
			    			   									'</div>'+
			    			   							'</div>'+
			    			   						'</div>'+
				   								'</td>'+
				   								'<td>'+
				   									'<div class="properties_width_container isDisplayNone">'+
			   											'<div class="fields_below forTblAddRemoveItem">'+
			    			   									'<div class="label_below2"></div>'+
			    			   									'<div class="input_position_below">'+
				    			   									'<textarea class="form-textarea fl-embed-filter-condition-dynamic-value"></textarea>'+
			    			   									'</div>'+
			    			   							'</div>'+
			    			   						'</div>'+
			    			   						'<div class="properties_width_container ">'+
			   											'<div class="fields_below forTblAddRemoveItem">'+
			    			   									'<div class="label_below2"></div>'+
			    			   									'<div class="input_position_below">'+
				    			   									'<select class="form-select fl-embed-filter-condition-static-value">'+
								   										'<option value="">--Select--</option>'+
								   									'</select>'+
			    			   									'</div>'+
			    			   							'</div>'+
			    			   						'</div>'+
				   								'</td>'+
				   								
				   								'<td style="width:0px;">'+
				   									'<div class=" properties_width_container" style="margin:0 auto; text-align:left;">'+
				   										'<div class="fields_below forTblAddRemoveItem">'+
		    			   									'<div class="label_below2"></div>'+
		    			   									'<div class="input_position_below">'+
				   												'<i class="fa fa-plus fl-addRemoveItem"></i> ' + '<i class="fa fa-minus fl-addRemoveItem" style="display:none;"></i>'+
				   											'</div>'+
				   										'</div>'+
				   									'</div>'+
				   								'</td>'+
				   							'</tr>'+
				   						'</tbody>'+ // END OF Group name TBL
				   					'</table>'+
			   					'</div>'+
    			   		'</div>'+
    			   '</div>';
    	return ret;

    }

    function obj_fieldTextAlignment(obj_float,data_type){
	    var ret
		= '<div class="' + obj_float + ' properties_width_container">'+
			'<div class="fields_below">'+
				//'<div class="label_below2">Field Text Alignment:</div>'+
				'<div class="input_position_below section clearing fl-field-style">'+
					'<div class="column div_1_of_1">'+
						'<span>Field Text Alignment:</span>'+
						'<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="fieldTxtAlignment">'+
						    '<option value="Left">Left</option>'+
						    '<option value="Right">Right</option>'+
						    '<option value="Center">Center</option>'+
						    '<option value="Justify">Justify</option>'+    
					    '</select>'+
					'</div>'+
				'</div>'+
				
			'</div>'+
		'</div>';
	    return ret;
    

    }
    function obj_fieldFontFamily(obj_float, data_type){
		var ret
			= '<div class="' + obj_float + ' properties_width_container">'+
				'<div class="fields_below">'+
					//'<div class="label_below2">Field Font Family:<span class="font-preview">Sample Text</span></div>'+
					'<div class="input_position_below section clearing fl-field-style">'+ //
						'<div class="column div_1_of_1">'+
							'<span>Field Font Family: <span class="font-preview">Sample Text</span></span>'+
							'<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="fieldFontType">';
							  	
							  	 for (var key in newFontFamily) {
						        	newFontFamily[key]["bold"]
						        	newFontFamily[key]["regular"]
						        	ret+='<option value="'+ newFontFamily[key]["regular"] +'"  attrfontNormal="'+newFontFamily[key]["regular"]+'" attrfontbold="'+newFontFamily[key]["bold"]+'">'+ key +'</option>';
							    }
						    ret += '</select>'+

						   //   	var keys = Object.keys(fontFamily),
							  //  i, len = keys.length;

							  //  keys.sort();
							  
							  // for (i = 0; i < len; i++)
							  // {
							  //     k = keys[i];
							    
							  //     ret+='<option value="'+ fontFamily[k]+'">'+ k +'</option>';
							  // }
							  
					    //     ret += '</select>'+
				     '</div>'+   
					'</div>'+
					
				'</div>'+
			'</div>';
		 return ret;
    }
    
    function obj_alignment(obj_float, data_type) {
	    var ret
		= '<div class="' + obj_float + ' properties_width_container">'+
			'<div class="fields_below">'+
				//'<div class="label_below2">Label Text Alignment:</div>'+
				'<div class="input_position_below section clearing fl-field-style">'+ //
					'<div class="column div_1_of_1">'+
						'<span>Label Text Alignment:</span>'+
						'<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="lblTxtAlignment">'+
						    
						    '<option value="Left">Left</option>'+
						    '<option value="Right">Right</option>'+
						    '<option value="Center">Center</option>'+
						    '<option value="Justify">Justify</option>'+    
					    '</select>'+
				    '</div>'+
				'</div>'+
				
			'</div>'+
		'</div>';
	    return ret;
    }
    function obj_shortlist(obj_float,data_type) {
	//para sa list names
	var ret
		= '<div class="' + obj_float + ' properties_width_container">'+
			'<div class="fields_below">'+
				//'<div class="label_below2">Short list:</div>'+
				'<div class="input_position_below section clearing fl-field-style">'+
					'<div class="column div_1_of_1">'+
						'<span>Short list:</span>'+
						'<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="fieldShortlist">'+
						    '<option value="">------Select------</option>'+
						    '<option value="All Users">All Users</option>'+
						    '<option value="All Registered Users">All Registered Users</option>'+
						    '<option value="All Guest Users">All Guest Users</option>'+
						    
					        '</select>'+
						'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	    return ret;
	
    }
    
    function obj_fontType(obj_float,data_type) {

	var ret
		= '<div class="' + obj_float + ' properties_width_container">'+
			'<div class="fields_below">'+
				//'<div class="label_below2">Label Font Family:<span class="font-preview">Sample Text</span></div>'+
				'<div class="input_position_below section clearing fl-field-style">'+ //
					'<div class="column div_1_of_1">'+
						'<span>Label Font Family: <span class="font-preview">Sample Text</span></span>'+
						'<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="lblFontType">';
				    		//var keys = Object.keys(fontFamily);
				    	
					        for (var key in newFontFamily) {
					        	newFontFamily[key]["bold"]
					        	newFontFamily[key]["regular"]
					        	ret+='<option value="'+ newFontFamily[key]["regular"] +'"  attrfontNormal="'+newFontFamily[key]["regular"]+'" attrfontbold="'+newFontFamily[key]["bold"]+'">'+ key +'</option>';
					        }
					    ret += '</select>'+
					  	 	//i, len = keys.length;
					  
						   	//keys.sort();
							  
						  	//for (i = 0; i < len; i++)
						  	//{
						      //k = keys[i];
						    
						      //ret+='<option value="'+ fontFamily[k]+'" attrfontbold="" attrfontNormal="" >'+ k +'</option>';
						  	//}
					        //ret += '</select>'+
				     '</div>'+   
				        
				'</div>'+
				
			'</div>'+
		'</div>';
	    return ret;
	
    }
	function listNameChoiceType(obj_float, data_type){
		var ret
	    	=	'<div class="' + obj_float + ' properties_width_container ListNameChoiceType">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below ListNameChoiceType_prop">'+
					//'<div class="label_below2">List Type: <font color="red"></font></div>'+
						'<div class="input_position_below section clearing fl-field-style listname-choice-type" data-type="'+data_type+'">'+
							'<div class="column div_1_of_1">'+
								'<span>List Type: <font color="red"></font></span><br/>'+
								'<label><input type="radio" checked="true" value="Single" name="listname_selection_type" data-type="' + data_type + '" class=" css-checkbox" data-properties-type="listTypeName" data-object-id="" id="list_name_selection_single" /><label for="list_name_selection_single" class="css-label"></label> Single</label>'+
								'<label> <input type="radio" value="Multiple" name="listname_selection_type" data-type="' + data_type + '" class="css-checkbox" data-properties-type="listTypeName" data-object-id="" id="list_name_selection_multiple" /><label for="list_name_selection_multiple" class="css-label"></label> Multiple</label>'+
							'</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
	}
    function propEmbedViewVisibility(obj_float, data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container EmbedViewVisibility">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below EmbedViewVisibility_prop">'+
				    	'<div class="label_below2">Embed View Visibility Formula: <font color="red"></font></div>'+
						'<div class="input_position_below embed-view-visibility" data-type="'+data_type+'">'+
							'<textarea data-type="' + data_type + '" class="form-textarea obj_prop" data-properties-type="embedViewVisibility" placeholder="Formula" style="resize:vertical;"></textarea>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function propEmbedActionEventVisibility(obj_float, data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container EmbedActionEventVisibility">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below EmbedActionEventVisibility_prop">'+
				    	'<div class="label_below2">Event Action Visibility Formula: <font color="red"></font></div>'+
						'<div class="input_position_below embed-action-event-visibility" data-type="'+data_type+'">'+
							'<textarea data-type="' + data_type + '" class="form-textarea obj_prop" data-properties-type="actionEventVisibilityFormula" placeholder="Formula" style="resize:vertical;"></textarea>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function propAdvanceEmbedActionEventVisibility(obj_float, data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container AdvanceEmbedActionEventVisibility">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below AdvanceEmbedActionEventVisibility_prop">'+
				    	//'<div class="label_below2">Advance Event Action Visibility Formula: <font color="red"></font></div>'+
						// '<div class="input_position_below section clearing fl-field-style" data-type="'+data_type+'" >'+
						// 	'<div class="column div_1_of_1">'+
						// 		'<span>Advance Event Action Visibility Formula: <font color="red"></font></span>'+
						// 		'<table style="width:100%;">'+
						// 			'<thead>'+
						// 				'<tr>'+
						// 					'<th style="width: 205px;">Formula</th>'+
						// 					'<th style="">Target Visibility</th>'+
						// 				'</tr>'+
						// 			'</thead>'+
						// 		'</table>'+
						// 	'</div>'+	
						// '</div>'+
						'<div class="input_position_below advance-embed-action-event-visibility section clearing fl-field-style" data-type="'+data_type+'" style="overflow-y: auto;min-height: 51px;max-height: 170px;overflow-x: hidden;padding-bottom:2px;">'+
							'<div class="column div_1_of_1">'+
								'<span>Advance Event Action Visibility Formula: <font color="red"></font></span>'+
								'<table style="width:100%;">'+
									'<tbody class="aeaev-tr-group">'+
										'<tr>'+
											'<td colspan="2"><div><span class="aeaev-add-minus-row" style="float:right;"><i class="fa fa-plus" style="cursor:pointer;"></i> <i class="fa fa-minus" style="cursor:pointer;display:none;"></i></span></div></td>'+
										'</tr>'+
										'<tr>'+
											//'<td></td>'+
											'<td>'+
											'<span>Target Visibility:</span>'+
											'<select class="form-select aeaev-data-target-visibility">'+
												'<option value="-1">--Select--</option>'+
												'<option value="1">Whole Embed</option>'+
												'<option value="2">Embed Action</option>'+
												'<option value="3">Add Link</option>'+
												//ADDED BY JOSHUA CLIFFORD REYES 09/22/2015
												'<option value="4">Copy Action</option>'+
												'<option value="5">Delete Action</option>'+
												'<option value="6">In-line Action</option>'+
												'<option value="7">Pop-up Action</option>'+
												'<option value="8">View Action</option>'+
												'<option value="9">Import</option>'+
											'</select>'+
											'<span style="margin-top: 4px; margin-bottom:0px !important;">Formula:</span>'+
											'<textarea class="aeaev-formula-based-visibility form-textarea" data-ide-properties-type="aeaev-formula-based-visibility" style="margin-top:0px !important;"></textarea>'+//style="resize:both;max-width: 235px;min-width: 200px;height:30px;"
											'</td>'+
										'</tr>'+
									'</tbody>'+
								'</table>'+
							'</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function embedDialogType(obj_float, data_type){
    	var ret
    	=	'<div class="properties_width_container embedProperty embedPropertyDialogType">'+
    		    '<div class="fields_below">'+
    				//'<div class="label_below2">Conditional operator: <font color="red"></font></div>'+
    				'<div class="input_position_below section clearing fl-field-style embed_dialog_type_container">'+
    					'<div style="margin-top:2px;" class="embed-dialog-type-c">'+
    						'<div class="column div_1_of_1">'+
    							'<span>Action Dialog Type: <font color="red"></font></span>'+
    							'<select  class="form-select embed-dialog-type obj_prop" data-type="'+data_type+'" data-properties-type="embedDialogType">'+
    								'<option value="1" selected="selected">Dialog Popup</option>'+
    								'<option value="2">New Window Popup</option>'+
    							'</select>'+
    						'</div>'+
    						'<table class="embed-popup-window-properties">'+
    							'<tr>'+
    								'<td style="padding:5px;">'+
    									'<span>Window Width: <font color="red"></font></span>'+
    									'<input type="text" class="form-text obj_prop" style="width:90%;text-align:right;" data-properties-type="embed-popup-window-width"/>'+
    									'<span>&nbsp;px<font color="red"></font></span>'+
    								'</td>'+
    								'<td style="padding:5px;">'+
    									'<span>Window Height: <font color="red"></font></span>'+
    									'<input type="text" class="form-text obj_prop" style="width:90%;text-align:right;" data-properties-type="embed-popup-window-height"/>'+
    									'<span>&nbsp;px<font color="red"></font></span>'+
    								'</td>'+
    							'</tr>'+
    						'</table>'+
    					'</div>'+
    				'</div>'+
    		    '</div>'+
    		'</div>';
		return ret;
    }
    function propEmbedSetColumnResult(obj_float, data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container EmbedSetColumnResult_container">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below EmbedSetColumnResult_prop">'+
				    	//'<div class="label_below2">Set Column Computation: <font color="red"></font></div>'+
						'<div class="input_position_below section clearing fl-field-style" style="">'+
							// '<table style="width:100%;">'+
							// 	'<thead>'+
							// 		'<tr>'+
							// 			'<th style="text-align:center; width:33.33%;">Embed data column<th>'+
							// 			'<th style="text-align:center;width:33.33%;">Computation<th>'+
							// 			'<th style="text-align:center;width:33.33%;">Field Result<th>'+
							// 		'</tr>'+
							// 	'</thead>'+
							// '</table>'+	
							'<div class="column div_1_of_1">'+
								'<table style="width:100%;">'+
									'<tbody class="escr-tr-group">'+
										'<tr>'+
											'<td colspan="3"><div><span class="escr-add-minus-row" style="float:right; margin-top:5px;"><i class="fa fa-plus" style="cursor:pointer;"></i> <i class="fa fa-minus" style="cursor:pointer;display:none;"></i></span></div></td>'+
										'</tr>'+
										'<tr>'+
											'<td style="width:33.33%; padding-right:2px;">Embed data column<select class="form-select escr-data-embed-column-field"><option value="-1">--Select--</option></select></td>'+
											'<td style="width:33.33%; padding-right:2px;">Computation<select class="form-select escr-data-embed-computation-field"><option value="-1">--Select--</option><option value="Average">Average</option><option value="Count">Count</option><option value="Total">Total</option></select></td>'+
											'<td style="width:33.33%;">Field Result<select class="form-select escr-data-result-output-field"><option value="-1">--Select--</option></select></td>'+
										'</tr>'+
									'</tbody>'+
								'</table>'+
							'</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
//	    	var ret
//    	=	'<div class="' + obj_float + ' properties_width_container EmbedSetColumnResult_container">'+
//			    '<div class="fields_below">'+
//					'<div class="input_position_below EmbedSetColumnResult_prop">'+
//				    	'<div class="label_below2">Set Column Total Result: <font color="red"></font></div>'+
//						'<div class="input_position_below" style="">'+
//							'<table style="width:100%;">'+
//								'<thead>'+
//									'<tr>'+
//										'<th>Embed data column<th>'+
//										'<th>Field Result<th>'+
//									'</tr>'+
//								'</thead>'+
//							'</table>'+	
//							'<table style="width:100%;">'+
//								'<tbody class="escr-tr-group">'+
//									'<tr>'+
//										'<td colspan="2"><div><span class="escr-add-minus-row" style="float:right;"><i class="fa fa-plus" style="cursor:pointer;"></i> <i class="fa fa-minus" style="cursor:pointer;display:none;"></i></span></div></td>'+
//									'</tr>'+
//									'<tr>'+
//										'<td><textarea class="escr-formula-based-col-comp" style="resize:both;max-width: 235px;min-width: 200px;height:30px;"placeholder="@GetData(&#34;embed_name&#34;, &#34;column_fld_name&#34;)[&#34;Total&#34;]"></textarea></td>'+
//										'<td><select class="form-select escr-data-result-output-field"><option value="-1">--Select--</option></select></td>'+
//									'</tr>'+
//								'</tbody>'+
//							'</table>'+
//						'</div>'+
//					'</div>'+
//			    '</div>'+
//			'</div>';
//		return ret;


	
    }
	    // function computedContainmentSearch(obj_float, data_type){
	    // 	var ret
	    // 	=	'<div class="' + obj_float + ' properties_width_container embedRoutingField_container">'+
			// 	    '<div class="fields_below">'+
			// 			'<div class="input_position_below embedRoutingField_prop">'+
			// 		    	'<div class="label_below2">Form route flow: <font color="red"></font></div>'+
			// 				'<div class="input_position_below" style="">'+
			// 					'<table style="width:100%;">'+
			// 						'<thead>'+
			// 							'<tr>'+
			// 								'<th>My form fields<th>'+
			// 								'<th>Popup form fields<th>'+
			// 							'</tr>'+
			// 						'</thead>'+
			// 						'<tbody>'+
			// 							'<tr>'+
			// 								'<td><select class="form-select erf-route-my-form-fields"><option value="-1">--Select--</option></select><td>'+
			// 								'<td><select class="form-select erf-route-popup-form-fields"><option value="-1">--Select--</option></select><td>'+
			// 							'</tr>'+
			// 						'</tbody>'+
			// 					'</table>'+
			// 				'</div>'+
			// 			'</div>'+
			// 	    '</div>'+
			// 	'</div>';
			// return ret;
	    // }

    function embedRoutingField(obj_float, data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container embedRoutingField_container">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below embedRoutingField_prop">'+
				    	//'<div class="label_below2">Sub Request Identifier: <font color="red">*</font></div>'+
						'<div class="input_position_below section clearing fl-field-style" style="">'+
							'<div class="column div_1_of_1">'+
								'<span>Sub Request Identifier: <font color="red">*</font></span>'+
								'<table style="width:100%;">'+
									'<thead>'+
										'<tr>'+
											'<th>My form fields:<th>'+
											'<th>Popup form fields:<th>'+
										'</tr>'+
									'</thead>'+
									'<tbody>'+
										'<tr>'+
											'<td><select class="form-select erf-route-my-form-fields" style="margin-right:4px;"><option value="-1">--Select--</option></select><td>'+
											'<td><select class="form-select erf-route-popup-form-fields" style="margin-left:4px;"><option value="-1">--Select--</option></select><td>'+
										'</tr>'+
									'</tbody>'+
								'</table>'+
							'</div>'+	
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function embedPopupDataSending(obj_float, data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container embedPopupDataSending_container">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below section clearing fl-field-style embedPopupDataSending_prop">'+
				    	'<div class="label_below2" style="margin:0px !important; padding:5px;"><label>'+'<input type="checkbox" value="rowclick_enable" class="enable_embedPopupDataSending css-checkbox" data-properties-type="enable_embedPopupDataSending" id="enable_embedPopupDataSending"/>'+'<label for="enable_embedPopupDataSending" class="css-label"></label> Data sending on add request: '+'</label><font color="red"></font></div>'+
						'<div class="input_position_below display2" style="position:relative;padding:5px;">'+ //max-height: 100%; min-height: 100px;
							'<table style="width:100%;position:relative;">'+
								'<thead>'+
									'<tr>'+
										'<th colspan="2"><label>Fields from my form</label></th><th colspan="2"><label>To popup form fields</label></th>'+
									'</tr>'+
								'</thead>'+
							'</table>'+
							'<div style="width:100%;overflow-x:hidden;overflow-y:auto;max-height:180px;">'+
								'<table class="epds-data-send-collection" style="width:100%">'+
									'<colgroup>'+
										'<col width="25%">'+
										'<col width="25%">'+
										'<col width="25%">'+
										'<col width="25%">'+
									'</colgroup>'+
									'<tbody class="epds-tr-group">'+
										
										'<tr>'+
								            '<td><label  style="display:none;"><input  type="radio" name="epds-my-form-side-chk" data-name="epds-my-form-side-chk" value="static" checked="checked"/> static</label></td>'+
								            '<td><label  style="display:none;"><input type="radio" name="epds-my-form-side-chk" data-name="epds-my-form-side-chk" value="computed"/> computed</label></td>'+
								            '<td><label  style="display:none;"><input type="radio" name="epds-popup-form-side-chk" data-name="epds-popup-form-side-chk" value="static" checked="checked"/> static</label></td>'+
								            '<td><div><label  style="display:none;"><input type="radio" name="epds-popup-form-side-chk" data-name="epds-popup-form-side-chk" value="computed"/> computed</label><span class="epds-add-minus-row" style="float:right; margin-top:3px;"><i class="fa fa-minus" style="cursor:pointer;display:none; float:right; "></i> <i class="fa fa-plus" style="cursor:pointer;float:right; margin-right:5px;"></i> </span></div></td>'+
								        '</tr>'+
								        '<tr>'+
								            '<td class="epds-my-form-field-side" colspan="2">'+
								            	'<div class="epds-my-form-side-chk-static-selected" style="padding-right:5px;"><select name="epds-select-my-form-field-side" data-name="epds-select-my-form-field-side" class="form-select"><option>--Select--</option></select></div>'+
								            	'<div class="epds-my-form-side-chk-computed-selected" style="display:none;"><textarea name="epds-formula-my-form-field-side" data-name="epds-formula-my-form-field-side" class="form-textarea" placeholder="Formula here"></textarea></div>'+
							            	'</td>'+

								            '<td class="epds-popup-form-field-side" colspan="2">'+
								            	'<div class="epds-popup-form-side-chk-static-selected" padding-left:2px;><select name="epds-select-popup-form-field-side" data-name="epds-select-popup-form-field-side" class="form-select"><option>--Select--</option></select></div>'+
								            	'<div class="epds-popup-form-side-chk-computed-selected" style="display:none;"><textarea name="epds-formula-popup-form-field-side" data-name="epds-formula-popup-form-field-side" class="form-textarea" placeholder="Formula here"></textarea></div>'+
							            	'</td>'+
								        '</tr>'+
									'</tbody>'+
								'</table>'+
							'</div>'+
							
							// '<label>'+'<input type="checkbox" value="creation" class="embed_row_click_field_prop" data-properties-type="embed_row_click_creation_form"/>'+' Allow create new</label></br>'+
							// '<label>'+'<input type="checkbox" value="edit" class="embed_row_click_field_prop" data-properties-type="embed_row_click_update_form"/>'+' Update record</label>'+
							// // '<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="setReadOnlyField" data-object-id="">'+
							// // 	'<option value="No">No</option>'+
							// // 	'<option value="Yes">Yes</option>'+
						    // '</select>'+
						    // '<textarea data-type="' + data_type + '" data-properties-type="dynamicListing" class="form-select obj_prop">'+
						    // '</textarea>'+
						    // '<input type="checkbox" value="rowclick" class="embed_row_click_field_prop" data-properties-type="embed_row_click"/>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function embedRowClick(obj_float, data_type) {
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container embed_row_click">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below section clearing fl-field-style embed_row_click_prop">'+
				    	'<div class="column div_1_of_1">'+
							'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="embed_number_row" class="embed_row_click_field_prop embed_row_number_prop css-checkbox" data-properties-type="EmbedNumberRowForm" id="EmbedNumberRowForm"/>'+'<label for="EmbedNumberRowForm" class="css-label"></label> Display Column Number</label>'+
							'<label>'+'<input type="checkbox" value="rowclick_enable" class="embed_row_click_enable_prop css-checkbox" data-properties-type="enable_embed_row_click" id="enable_embed_row_click"/>'+'<label for="enable_embed_row_click" class="css-label"></label> Enable event actions'+'<font color="red"></font></label></div>'+
						
						'<div class="input_position_below" style="padding:5px;">'+
							'<div class="display2 link_caption_container">'+
							'<input type="text" name="link_caption" data-type="'+data_type+'" class="form-text obj_prop link_caption" id="link_caption" data-properties-type="link_caption" data-object-id="1" placeholder="Link Caption" value="Add Request" style="margin-bottom:5px;" />';
							if($('#enable_inline_add_embed').text() != "0"){
								ret += '<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="embed_creation_row" class="embed_row_click_field_prop css-checkbox" data-properties-type="EmbedCreateRowForm" id="embed_row_click_inline_creation_form"/>'+'<label for="embed_row_click_inline_creation_form" class="css-label"></label> Enable In-line Create</label></div>';
							}
							//=================================
							// '<label>'+'<input type="checkbox" value="edit" class="embed_row_click_field_prop" data-properties-type="embed_row_click_update_form"/>'+' Update record</label>'+
							
							ret += '<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="embed_copy_row" class="embed_row_click_field_prop embed_row_copy_prop css-checkbox" data-properties-type="EmbedCopyRowForm" id="EmbedCopyRowForm"/>'+'<label for="EmbedCopyRowForm" class="css-label"></label> Allow Copy</label>'+
							'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="creation" class="embed_row_click_field_prop css-checkbox" data-properties-type="embed_row_click_creation_form" id="embed_row_click_creation_form"/>'+'<label for="embed_row_click_creation_form" class="css-label"></label> Allow Create</label>';
							//added by japhet morada 10-06-2015
							
							ret += '<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="embed_delete_row" class="embed_row_click_field_prop embed_row_delete_prop css-checkbox" data-properties-type="EmbedDeleteRowForm" id="EmbedDeleteRowForm"/>'+'<label for="EmbedDeleteRowForm" class="css-label"></label> Allow Delete</label>'+
							'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="embedded-view-allow-import" class="embedded_view_click_field_prop embedded_view_allow_import css-checkbox" data-properties-type="EmbeddedViewAllowImport" id="EmbeddedViewAllowImport"/>'+'<label for="EmbeddedViewAllowImport" class="css-label"></label> Allow Import Records</label>' + 
							'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="embed_edit_row" class="embed_row_click_field_prop embed_row_edit_prop css-checkbox" data-properties-type="EmbedEditRowForm" id="EmbedEditRowForm"/>'+'<label for="EmbedEditRowForm" class="css-label"></label> Allow In-line Edit</label>'+
							'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="embed_edit_popup_row" class="embed_row_click_field_prop embed_row_edit_popup_prop css-checkbox" data-properties-type="EmbedEditPopUpRowForm" id="EmbedEditPopUpRowForm"/>'+'<label for="EmbedEditPopUpRowForm" class="css-label"></label> Allow Pop-up Edit</label>'+
							'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="embed_view_row" class="embed_row_click_field_prop embed_row_view_prop css-checkbox" data-properties-type="EmbedViewRowForm" id="EmbedViewRowForm"/>'+'<label for="EmbedViewRowForm" class="css-label"></label> Allow View</label>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_embedCustomActions(obj_float, data_type) {
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container embedCustomActions">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below section clearing fl-field-style embed_custom_actions_prop" style="position:relative;">'+
				    	'<div class="column div_1_of_1">'+
				    		'<span>Custom Actions</span>' +
							'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="embed_custom_actions" class="embed_row_click_field_prop embed_row_custom_actions css-checkbox" data-properties-type="EmbedViewCustomAction" id="EmbedViewCustomAction"/>'+'<label for="EmbedViewCustomAction" class="css-label"></label> Allow Custom Actions</label>'+
						'</div>'+
						'<div class="column div_1_of_1 embed-custom-actions-container" style="overflow-y: auto;height: 194px;overflow-x: hidden;">'+
							'<table class="embed-custom-actions-table" style="width:100%;">'+
								'<tbody class="embed-repeated-actions">'+
									'<tr>'+
										'<td>'+
											'<span class="embed_custom_actions_plus_minus" style="float: right;">'+
												'<i class="fa fa-plus" style="margin-right: 3px;cursor: pointer;"></i>'+
												'<i class="fa fa-minus" style="margin-right: 3px;cursor: pointer; display: none;"></i>'+
											'</span>'+
										'</td>'+
									'</tr>'+
									'<tr>'+
										'<td>'+
											'<span>Select Action</span>' +
											'<select class="form-select select_custom_action">'+
												'<option value="0">--Select--</option>' + 
												//'<option value="1">New</option>' + 
												'<option value="2">Edit</option>' + 
												'<option value="3">Delete</option>' + 
												'<option value="4">Lock</option>' +
												'<option value="5">Custom...</option>' + 
											'</select>' +
											'<div style="display: table; width: 100%;">' + 
												'<div style="display: table-row;">' + 
													'<div style="display: table-cell;width: 81%;">' + 
														'<span style="margin-top: 4px; margin-bottom:0px !important;">Action Label: </span>'+
														'<input type="text" class="form-text" name="embed_custom_actions_label" />'+
													'</div>' +
													'<div style="display: table-cell;height: 48px;padding-left: 20px;">' + 
														'<div class="embedded-view-custom-actions-icon-pick" icon_type="custom_actions"></div>' + 
													'</div>' +
												'</div>' + 
											'</div>' + 
											'<div class="custom-action-custom-formula">'+
												'<span style="margin-top: 4px; margin-bottom:0px !important;">Custom Action Formula (Custom Script)</span>'+
												'<textarea class="form-textarea embed_custom_action_formula ide-hover" data-ide-properties-type="embed-custom-action-formula"></textarea>'+
											'</div>'+
										'</td>'+
									'</tr>'+
								'</tbody>'+
							'</table>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }

  //   function multipleEmbedRowClick(obj_float, data_type) {
  //   	var ret
  //   	=	'<div class="' + obj_float + ' properties_width_container m_embed_row_click">'+
		// 	    '<div class="fields_below">'+
		// 			'<div class="input_position_below section clearing fl-field-style m_embed_row_click_prop">'+
		// 		    	'<div class="column div_1_of_1">'+
		// 					'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="multiple_embed_number_row" class="multiple_embed_row_click_field_prop multiple_embed_row_number_prop css-checkbox" data-properties-type="MultipleEmbedNumberRowForm" id="MultipleEmbedNumberRowForm"/>'+'<label for="MultipleEmbedNumberRowForm" class="css-label"></label> Display Column Number</label>'+
		// 					'<label>'+'<input type="checkbox" value="multiple_rowclick_enable" class="multiple_embed_row_click_enable_prop css-checkbox" data-properties-type="multiple_enable_embed_row_click" id="multiple_enable_embed_row_click"/>'+'<label for="multiple_enable_embed_row_click" class="css-label"></label> Enable event actions'+'<font color="red"></font></label></div>'+
						
		// 				'<div class="input_position_below" style="padding:5px;">'+
		// 					'<div class="display2 link_caption_container">'+
		// 					'<input type="text" name="link_caption" data-type="'+data_type+'" class="form-text obj_prop link_caption" id="link_caption" data-properties-type="link_caption" data-object-id="1" placeholder="Link Caption" value="Add Request" style="margin-bottom:5px;" />';
		// 					if($('#enable_inline_add_embed').text() != "0"){
		// 						ret += '<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="multiple_embed_creation_row" class="multiple_embed_row_click_field_prop css-checkbox" data-properties-type="MultipleEmbedCreateRowForm" id="multiple_embed_row_click_inline_creation_form"/>'+'<label for="multiple_embed_row_click_inline_creation_form" class="css-label"></label> Enable In-line Create</label></div>';
		// 					}
		// 					//=================================
		// 					// '<label>'+'<input type="checkbox" value="edit" class="embed_row_click_field_prop" data-properties-type="embed_row_click_update_form"/>'+' Update record</label>'+
							
		// 					ret += '<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="multiple_embed_copy_row" class="multiple_embed_row_click_field_prop multiple_embed_row_copy_prop css-checkbox" data-properties-type="MultipleEmbedCopyRowForm" id="MultipleEmbedCopyRowForm"/>'+'<label for="MultipleEmbedCopyRowForm" class="css-label"></label> Allow Copy</label>'+
		// 					'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="multiple_creation" class="multiple_embed_row_click_field_prop css-checkbox" data-properties-type="multiple_embed_row_click_creation_form" id="multiple_embed_row_click_creation_form"/>'+'<label for="multiple_embed_row_click_creation_form" class="css-label"></label> Allow Create</label>';
		// 					//added by japhet morada 10-06-2015
							
		// 					ret += '<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="multiple_embed_delete_row" class="multiple_embed_row_click_field_prop multiple_embed_row_delete_prop css-checkbox" data-properties-type="MultipleEmbedDeleteRowForm" id="MultipleEmbedDeleteRowForm"/>'+'<label for="MultipleEmbedDeleteRowForm" class="css-label"></label> Allow Delete</label>'+
		// 					// '<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="multiple_embedded-view-allow-import" class="embedded_view_click_field_prop embedded_view_allow_import css-checkbox" data-properties-type="EmbeddedViewAllowImport" id="EmbeddedViewAllowImport"/>'+'<label for="EmbeddedViewAllowImport" class="css-label"></label> Allow Import Records</label>' + 
		// 					'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="multiple_embed_edit_row" class="multiple_embed_row_click_field_prop multiple_embed_row_edit_prop css-checkbox" data-properties-type="MultipleEmbedEditRowForm" id="MultipleEmbedEditRowForm"/>'+'<label for="MultipleEmbedEditRowForm" class="css-label"></label> Allow In-line Edit</label>'+
		// 					'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="multiple_embed_edit_popup_row" class="multiple_embed_row_click_field_prop multiple_embed_row_edit_popup_prop css-checkbox" data-properties-type="MultipleEmbedEditPopUpRowForm" id="MultipleEmbedEditPopUpRowForm"/>'+'<label for="MultipleEmbedEditPopUpRowForm" class="css-label"></label> Allow Pop-up Edit</label>'+
		// 					'<label style="display:block; margin-bottom:5px;">'+'<input type="checkbox" value="multiple_embed_view_row" class="multiple_embed_row_click_field_prop multiple_embed_row_view_prop css-checkbox" data-properties-type="MultipleEmbedViewRowForm" id="MultipleEmbedViewRowForm"/>'+'<label for="MultipleEmbedViewRowForm" class="css-label"></label> Allow View</label>'+
		// 				'</div>'+
		// 			'</div>'+
		// 	    '</div>'+
		// 	'</div>';
		// return ret;
  //   }

    function dynamicListing(obj_float, data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container dynamicListing_container">'+
			    '<div class="fields_below">'+
					
			    	//'<div class="label_below2">Change list computed: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style dynamicListing_prop">'+ //
							'<div class="column div_1_of_1">'+
								'<span>Change list computed: <font color="red"></font></span>'+
							// '<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="setReadOnlyField" data-object-id="">'+
							// 	'<option value="No">No</option>'+
							// 	'<option value="Yes">Yes</option>'+
						    // '</select>'+
						    //'<textarea data-type="' + data_type + '" data-properties-type="dynamicListing" data-ide-properties-type="ide-dynamicListing" class="form-select obj_prop">'+
						    '<textarea data-type="' + data_type + '" data-properties-type="dynamicListing" data-ide-properties-type="ide-dynamicListing" class="form-select obj_prop">'+ //obj_prop ibalik pag may problem 
						    '</textarea>'+
						'</div>'+
					'</div>'+
					
			    '</div>'+
			'</div>';
		return ret;
    }

    function obj_picklistButton(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container picklistButton">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below picklistButton_prop_cont">'+
				    	'<div class="label_below2">Options Spacing: <font color="red"></font></div>'+
						'<div class="input_position_below " data-type="'+data_type+'">'+
							'<select name="picklistButton_prop" data-type="' + data_type + '" class="form-text obj_prop picklistButton_prop_field" data-properties-type="picklistButton_prop" placeholder="Picklist Button Property">'+
								'<option value="0">--Select--</option>'+
							'</select>'+
							'<div class="">'+
								'<input type="text"/>'+
							'</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_radioCheckboxSpacing(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container radioCheckboxSpacing">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below radioCheckboxSpacing_prop">'+
				    	//'<div class="label_below2">Options Spacing: <font color="red"></font></div>'+
						'<div class="input_position_below section clearing fl-field-style rc-field-spacing" data-type="'+data_type+'">'+
							'<div class="column div_1_of_1">'+
								'<span>Options Spacing: <font color="red"></font></span>'+
								'<input type="text" name="option-spacing" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" data-properties-type="optionSpacing" placeholder="Options Spacing">'+
							'</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';	
		return ret;
    }
    function obj_tableRelationalAction(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container tableRelationalAction">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below tableRelationalAction_prop">'+
				    	'<div class="label_below2">Add Remove Row Response To TBL: <font color="red"></font></div>'+
						'<div class="input_position_below select_names_here" data-type="'+data_type+'">'+
							//'<label><input type="checkbox" name="prop-select-tbl-names" value="'+"name"+'" /><span class="name-text">'+'NAME'+'</span></label>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_noteStyleFluidContainerCell(obj_float,data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container noteStyleFluidContainerCell">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below noteStyleFluidContainerCell_prop">'+
						'<div class="input_position_below section clearing fl-field-style">'+
							'<div class="column div_1_of_1">'+
								'<span>Set Cell Container Responsive: <font color="red"></font></span>'+
								'<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="noteStyleFluidContainerCell" data-object-id="">'+
									'<option value="Yes">Yes</option>'+
									'<option value="No">No</option>'+
							    '</select>'+
						    '</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_setReadonlyField(obj_float,data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container setReadOnlyField">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below setReadOnlyField_prop">'+
				    	//'<div class="label_below2">Set as Readonly: <font color="red"></font></div>'+
						'<div class="input_position_below section clearing fl-field-style">'+ //
							'<div class="column div_1_of_1">'+	
								'<span>Set as Readonly: <font color="red"></font></span>'+
								'<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="setReadOnlyField" data-object-id="">'+
									'<option value="No">No</option>'+
									'<option value="Yes">Yes</option>'+
							    '</select>'+
						    '</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_fldBGColor(obj_float,data_type) {
    	var object_type = "Field";
    	if(typeof this !== window){
    		if(this == "createLine"){
	    		object_type = "Line";
	    	}
	    
    	}
		var ret
		=	'<div class="' + obj_float + ' properties_width_container propFieldBGcolor">'+
				'<div class="fields_below">'+
					//'<div class="label_below2">'+object_type+' BGColor:</div>'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span>'+object_type+' background color:</span><br/>'+
							'<input type="text" data-type="' + data_type + '" class="chngColor" data-properties-type="lblFieldBGColor" data-type="button"/>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
	    return ret;
    }

    function obj_borderVisibility(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container borderVisibility">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below borderVisibility_prop">'+
				    	//'<div class="label_below2">Border Visibility: <font color="red"></font></div>'+
						'<div class="input_position_below section clearing fl-field-style">'+
							'<div class="column div_1_of_1">'+
								'<span>Border Visibility: <font color="red"></font></span>'+
								'<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="borderVisibility" data-object-id="">'+
									'<option value="Yes">Yes</option>'+
									'<option value="No">No</option>'+
							    '</select>'+
							'</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }

    function obj_propEcrypt(obj_float,data_type){
    	return "";
    	// var return
    	// =	'<div class="' + obj_float + ' properties_width_container encryptDataProperty">'+
		// 	    '<div class="fields_below">'+
		// 			'<div class="input_position_below encrypt_data_prop">'+
		// 		    	'<div class="label_below2">Data Encryption: <font color="red"></font></div>'+
		// 				'<div class="input_position_below">'+
		// 					'<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="ecryptDataPropField" data-object-id="">'+
		// 						'<option value="No">No</option>'+
		// 						'<option value="Yes">Yes</option>'+
		// 				    '</select>'+
		// 				'</div>'+
		// 			'</div>'+
		// 	    '</div>'+
		// 	'</div>';
		// return ret;
    }
    function obj_tableBorderVisibility(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container tableBorderProperty">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below form_table_border_visibility">'+
				    	//'<div class="label_below2">Border visible: <font color="red"></font></div>'+
						'<div class="input_position_below section clearing fl-field-style">'+
							'<div class="column div_1_of_1">'+
								'<span>Border visible: <font color="red"></font></span>'+
								'<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="tableBorderVisible" data-object-id="">'+
									'<option value="Yes">Yes</option>'+
									'<option value="No">No</option>'+
							    '</select>'+
							'</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_tableResponsiveContainer(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container tableResponsiveContainerProperty">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below form_table_responsive_container">'+
				    	'<div class="label_below2">Table Container Responsive: <font color="red"></font></div>'+
						'<div class="input_position_below">'+
							'<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="tableResponsiveContainer" data-object-id="">'+
								'<option value="false">False</option>'+
								'<option value="true">True</option>'+
						    '</select>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    //FIELD FONT SIZE
    function fieldFontSize(obj_float,data_type){
    	var ret
		= '<div class="' + obj_float + ' properties_width_container">'+
			'<div class="fields_below">'+
				//'<div class="label_below2">Field Font Size:</div>'+
				'<div class="input_position_below section clearing fl-field-style">'+ //
					'<div class="column div_1_of_1">'+
						'<span>Field Font Size:</span>'+
						'<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="fieldFontSize">'+
						    
						    '<option value="6px">6px</option>'+
						    '<option value="8px">8px</option>'+
						    '<option value="9px">9px</option>'+
						    '<option value="10px">10px</option>'+
						    '<option value="11px">11px</option>'+
						    '<option value="12px">12px</option>'+
						    '<option selected="selected" value="14px">14px</option>'+
						    '<option value="18px">18px</option>'+
						    '<option value="24px">24px</option>'+
						    '<option value="30px">30px</option>'+
						    '<option value="36px">36px</option>'+
						    '<option value="48px">48px</option>'+
						    '<option value="60px">60px</option>'+
						    '<option value="72px">72px</option>'+
					    '</select>'+
				    '</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	    return ret;
    }
    //EMBED CHOOSE DISPLAY COLUMN
	function embedDisplayColumn(obj_float, data_type) {
		var ret
		= '<div class="' + obj_float + ' properties_width_container embedPropertyDisplayColumn">' +
		'<div class="fields_below">' +
			'<div class="label_below2">Select column to display:</br><span style="font-size:9px;">*Fill-up the field label of the fields you would like to appear in the column header.</br>*Drag the field name up or down to rearrange.</span></div>' +
				'<div class="input_position_below section clearing fl-field-style">' +
		//					'<select multiple data-type="' + data_type + '" name="" class="form-select embed-display-column" id="" data-properties-type="lblAllowLabel" data-object-id="">'+
		//						//magkakalaman
			//					'</select>'+
					//added by japhet morada, add a button for inserting columns in 
					//'<div class="embed_newRequest embed_single_form_add_column_icon" style="display: inline-block; border-radius: 5px; padding: 5px; margin-right: 5px; margin-top: 5px; float: right; color: white; cursor: pointer;">Add Icon</div>' +
					'<div multiple data-type="' + data_type + '" name="" class=" column div_1_of_1 form-select embed-display-column" id="" data-properties-type="lblAllowLabel" data-object-id="" style="background:transparent; border:none;">' +
						//magkakalaman
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';
		return ret;
	}

    //EMBED HIGHLIGHTED ROW
    function highlightedRowProp(obj_float,data_type){
    	var ret =
			'<div class="' + obj_float + ' properties_width_container embedHighlightProp">'+
			    '<div class="fields_below">'+
					'<div class="section clearing fl-field-style">'+ //label_below2
						'<div class="column div_1_of_1">'+
							'<label>Highlight: <input value="true" style="vertical-align: middle;" type="checkbox" name="allow_highlights" class="css-checkbox" id="allow_highlights"/><label for="allow_highlights" class="css-label"></label> </label>'+
							'&nbsp;<label>cell: <input value="cell" style="vertical-align: middle;" type="radio" name="highlight_type" class="css-checkbox" id="highlight_type_cell"/><label for="highlight_type_cell" class="css-label"></label> </label>'+
							'&nbsp;<label>row: <input checked="checked" value="row" style="vertical-align: middle;" type="radio" name="highlight_type" class="css-checkbox" id="highlight_type_row"/><label for="highlight_type_row" class="css-label"></label> </label><font color="red"></font>'+
						'</div>'+
					'</div>'+
					'<div class="input_position_below section clearing fl-field-style" style="overflow:hidden; overflow-y:auto; height:100%; max-height:290px;">'+ //
					    '<div class="highlight-row-prop column div_1_of_1" data-type="' + data_type + '" >'+//style="position:relative;min-height:160px;max-height:200px;overflow:auto; padding:10px;"
					    	'<table class="design-embed-highlights-prop" border="1" style="width:100%;">'+
					    		'<tbody>'+
					    			'<tr class="HL_RULE_PROP1" style="border-top: 1px solid #D5D5D5;">'+
						    			'<td style="width:55px;">'+
						    				'<div class="relative-wrap" style="position: relative;width: 100%;height: 100%; margin-top:5px;">'+
							    				'<label style="text-align:center;display:inline-block;widt8100%; margin:7px 0px;">Color:</label><br/>'+
							    				'<input type="text" class="chooseColor hl-color" data-properties-type="embRowColor" data-type="emb-highlight-color"/>'+
						    				'</div>'+
						    			'</td>'+
						    			'<td>'+
						    				'<div class="relative-wrap" style="position: relative;width: 100%;height: 100%; margin-top:5px;">'+
							    				'<label style="text-align:center;display:inline-block;width:108; margin:7px 0px;">Fieldname:</label><br/>'+
							    				'<select class="form-select hl-fn fieldname-basis-highlight">'+
							    					'<option>--Select--</option>'+
							    					'<option>2</option>'+
							    					'<option>3</option>'+
							    				'</select>'+
							    			'</div>'+
						    			'</td>'+
						    		'</tr>'+
						    		'<tr class="HL_RULE_PROP2" style="border-bottom: 1px solid #D5D5D5;">'+
						    			'<td colspan="2" style="width:55px;text-align:center;">'+
						    				'<div class="relative-wrap" style="position: relative;width: 100%;height: 100%; margin-top:5px;">'+
							    				'<label style="display:inline-block;width:50%; position:absolute; left:0px; text-align:left;top: 16px;">Condition Type:</label>'+
							    				'<label style="display:inline-block;wid8:50%; position:absolute; text-align:left;top: 16px;">Value:</label><br/>'+
							    				'<select type="text" class="form-select choose-condition-type" style="margin: 0px 4px 0px 0px; display:inline-block;width:49%;">'+
							    					'<option value="equals">Equals</option>'+
							    					'<option value="greater_than_equals">Greater Than Equals</option>'+
							    					'<option value="greater_than">Greater Than</option>'+
							    					'<option value="less_than_equals">Less Than Equals</option>'+
							    					'<option value="less_than">Less Than</option>'+
							    					'<option value="range">Range</option>'+
							    					'<option value="in_between">In Between</option>'+
							    				'</select>'+
							    				'<div class="form-text value-container" style="display:inline-block;width:49%; background-color:#efefef;height:49%;">'+
							    					'<div style="position:relative; top:-10px; right:-13px;">'+
							    						'<label style="margin-bottom:7px; display: inline-block;">static: <input type="radio" checked="checked" value="static" data-properties-type="embed-value-type" class="value-type-HL css-checkbox" data-name="val-type" name="val-type_r0" id="HLradioStatic"/><label for="HLradioStatic" class="css-label"></label></label>'+
							    						'<label style="display: inline-block;margin-bottom:7px;">computed: <input type="radio" value="computed" data-properties-type="embed-value-type" class="value-type-HL css-checkbox" data-name="val-type" name="val-type_r0" id="HLradioComputed"/><label for="HLradioComputed" class="css-label"></label></label>'+
							    					'</div>'+	
							    					'<input class="form-text hl-value" data-ide-properties-type = "ide-embed-value-type-value" data-properties-type="embed-value-type-value" style="min-width:20px;margin:0;display:inline-block;"/>'+
							    					'<input class="form-text hl-value2" data-ide-properties-type = "ide-embed-value-type-value" style="min-width:20px;width:49%;margin:0;display:none;"/>'+
							    					
							    				'</div>'+
							    				'<div style="clear:both;"></div>'+
						    				'</div>'+
						    			'</td>'+
						    		'</tr>'+
					    		'</tbody>'+
					    	'</table>'+
					    '</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
	    return ret;
    }

   //EMBED HIGHLIGHTED ROW VERSION 2
   //  function highlightedRowPropV2(obj_float,data_type){
   //  	var ret =
			// '<div class="' + obj_float + ' properties_width_container">'+
			//     '<div class="fields_below">'+
			// 	'<div class="label_below2">'+
			// 		'<label>Highlight: <input value="true" style="vertical-align: middle;" type="checkbox" name="allow_highlights"/></label>'+
			// 		'<label>cell: <input value="cell" style="vertical-align: middle;" type="radio" name="highlight_type"/></label>'+
			// 		'<label>row: <input checked="checked" value="row" style="vertical-align: middle;" type="radio" name="highlight_type"/></label><font color="red"></font>'+
			// 	'</div>'+
			// 	'<div class="input_position_below">'+
			// 	    '<div class="highlight-row-prop" data-type="' + data_type + '" style="position:relative;height:200px;overflow:auto;">'+
			// 	    	'<table class="design-embed-highlights-prop" border="1" style="width:100%;">'+
			// 	    		'<tbody>'+
			// 	    			'<tr class="HL_RULEV2_PROP">'+
			// 		    			'<td style="width:55px;height: 110px;">'+
			// 		    				'<div class="relative-wrap" style="position: relative;width: 100%;height: 100%;">'+
			// 			    				'<label style="text-align:center;display:i8ine-block;width:100%;">Color:</label><br/>'+
			// 			    				'<input type="text" class="chooseColor hl-color" data-properties-type="embRowColor" data-type="emb-highlight-color"/>'+
			// 		    				'</div>'+
			// 		    			'</td>'+
			// 		    			'<td>'+
			// 		    				'<div class="relative-wrap" style="position: relative;width: 100%;height: 100%;">'+
			// 			    				'<label style="text-align:center;display:inl8e-block;width:100%;">Formula:</label><br/>'+
			// 			    				'<textarea class="HL-formula" style="width: 99%; height: 80px; margin: 0px; resize: vertical;"></textarea>'+
			// 			    			'</div>'+
			// 		    			'</td>'+
			// 		    		'</tr>'+
			// 	    		'</tbody>'+
			// 	    	'</table>'+
			// 	    '</div>'+
			// 	'</div>'+
			//     '</div>'+
			// '</div>';
	  //   return ret;
   //  }
    function obj_TableName(obj_float,data_type) {
		var ret =
			'<div class="' + obj_float + ' properties_width_container">'+
			    '<div class="fields_below">'+
				//'<div class="label_below2">Table Name: <font color="red"></font></div>'+
				'<div class="input_position_below section clearing fl-field-style">'+
					'<div class="column div_1_of_1">'+
						'<span>Table Name: <font color="red"></font></span>'+
				    	'<input type="text" name="set-prop-table-name" data-type="' + data_type + '" class="form-text obj_prop" data-properties-type="TableName" placeholder="Table Name">'+
					'</div>'+
				'</div>'+
			    '</div>'+
			'</div>';
	    return ret;
    }
    function radioAndCheckboxLabelProp(obj_float,data_type,field_type){
    	var caption = "";
    	if(field_type == "radioButton"){
    		caption = "Allow label in radio button:";
    	}else{
    		caption = "Allow label in checkbox:";
    	}
    	var ret
    	= '<div class="' + obj_float + ' properties_width_container">'+
    		'<div class="fields_below">'+
			//'<div class="label_below2">'+caption+'</div>'+
			'<div class="input_position_below section clearing fl-field-style">'+
				'<div class="column div_1_of_1">'+
					'<span>'+caption+'</span>'+
				    '<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="lblAllowLabel-RC" data-object-id="">'+
						'<option value="Yes">Yes</option>'+
						'<option value="No">No</option>'+
				    '</select>'+
			    '</div>'+
			'</div>'+
		    '</div>'+
		'</div>';
	    return ret;
    }

    function skipSpace(obj_float,data_type){
    	var ret = ""+
			'<div class="' + obj_float + ' properties_width_container">'+
				'<div class="fields_below">'+
					'<div class="label_below2"><font color="red"></font></div>'+
					'<div class="input_position_below">'+
						'<div></div>'+
					'</div>'+
				'</div>'+
			'</div>';
	    return ret;
    }
    function picklistSelectionType(obj_float,data_type){
    	var ret = ""+
			'<div class="' + obj_float + ' properties_width_container picklistSelectionType">'+
				'<div class="fields_below">'+
					//'<div class="label_below2">Picklist Selection Type: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span>Picklist Selection Type: <font color="red"></font></span><Br/>'+
							'<label><input type="radio" checked="true" value="Single" name="selection_type" data-type="' + data_type + '" class=" css-checkbox" data-properties-type="" data-object-id="" id="picklistSingle" /><label for="picklistSingle" class="css-label"></label> Single</label>'+
							'<label> <input type="radio" value="Multiple" name="selection_type" data-type="' + data_type + '" class="css-checkbox" data-properties-type="" data-object-id="" id="picklistMultiple" /><label for="picklistMultiple" class="css-label"></label> Multiple</label>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
	    return ret;
    }
    
    function picklist_image(obj_float,data_type){
    	var ret = ""+
			'<div class="' + obj_float + ' properties_width_container picklis_img_button">'+
				'<div class="fields_below">'+
					'<div class="label_below2">Choose what you want to for the icon clickable in picklist: <font color="red"></font></div>'+
					'<div class="input_position_below">'+
						'<label><input type="radio" checked="true" value="Default" name="selection_type_img" data-type="' + data_type + '" class="choose_set_picklist" data-properties-type="" data-object-id="">Default</label> '+
						'<label><input type="radio" value="Image" name="selection_type_img" data-type="' + data_type + '" class="choose_set_picklist" data-properties-type="" data-object-id="">Image</label> '+
						'<label><input type="radio" value="Word" name="selection_type_img" data-type="' + data_type + '" class="choose_set_picklist" data-properties-type="" data-object-id="">Word</label> '+
						
						'<div class="set_b_i set_Default" style="margin-top:10px;">'+
						'<i class="icon-list-alt fa fa-list-alt"></i> '+
						'</div>'+
						'<div class="set_b_i set_Word display" style="margin-top:10px;">'+
						'<input type="text" data-type="' + data_type + '" class=" form-text word_btn" id="word_btn" data-properties-type="' + data_type + '" data-object-id="" placeholder="Set your word."> '+
						'</div>'+
						'<div class="set_b_i set_Image display" style="margin-top:10px;">'+
						'<form id="postFile" method="post" enctype="multipart/form-data" action="/ajax/uploadFile" style="margin-top: 10px;">'+
							'<div class="postActions">'+
								'<img data-placement="bottom"  data-type="' + data_type + '" data-original-title="" src="/images/error/broken.png" width="44" height="44" class="avatar userAvatar imagePrev" onerror="this.src='+ $('#broken_image').text() +'" style="float: left;margin-right: 10px;">'+
							   //'<input type="submit" class="btn-blueBtn fl-btn-right ceatepost " data-get-file="imagePost" data-location="postUpload" data-file="imagePreviewUpload" value="POST" style="">'+
							   '<div id="uniform-fileInput" class="uploader">'+
							      '<input type="file" data-action-id="2" value="upload" name="postFile" id="uploadForm" size="24" data-action-id="2" data-action-type="upload_form_icon_image" style="opacity: 0;" class="upfile">'+
							      '<span id="uploadFilename_2" class="filename">No file selected</span>'+
							      '<span class="action">Choose File</span>'+
							      '<input type="hidden" value="post_forms_object" name="uploadType">'+
							      '<input type="hidden" value="forms_object" name="uploadFolder">'+
							   '</div>'+
							      '<div class="spinner  display postFileLoad">'+
								 '<div class="bar1"></div>'+
								 '<div class="bar2"></div>'+
								 '<div class="bar3"></div>'+
								 '<div class="bar4"></div>'+
								 '<div class="bar5"></div>'+
								 '<div class="bar6"></div>'+
								'<div class="bar7"></div>'+
								'<div class="bar8"></div>'+
								'<div class="bar9"></div>'+
								'<div class="bar10"></div>'+
							      '</div>'+
							    '<!--<img src="/images/loader/loader.gif" class="postFileLoad pull-left " style="margin-left: 5px;margin-top:6px;"/> -->'+
							'</div>'+
						     '</form>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>';
	    return ret;
    }
  //   function obj_propTabName(obj_float,data_type) {
		// var ret = ""+
		// 	'<div class="' + obj_float + ' properties_width_container">'+
		// 		'<div class="fields_below">'+
		// 			'<div class="label_below2">Tab Name: <font color="red"></font></div>'+
		// 			'<div class="input_position_below">'+
		// 				'<input type="radio" name="pick_slelection_type" data-type="' + data_type + '" class="form-text obj_prop" data-properties-type="" data-object-id="">'+
		// 				'<input type="radio" name="pick_slelection_type" data-type="' + data_type + '" class="form-text obj_prop" data-properties-type="" data-object-id="">'+
		// 			'</div>'+
		// 		'</div>'+
		// 	'</div>';
	 //    return ret;
  //   }

    function obj_propTabPanelName(obj_float,data_type) {
		var ret = ""+
			'<div class="' + obj_float + ' properties_width_container">'+
				'<div class="fields_below">'+
					'<div class="label_below2">Tabbed Panel Name: <font color="red"></font></div>'+
					'<div class="input_position_below">'+
						'<input type="text" name="" data-type="' + data_type + '" class="form-text obj_prop" id="prop-tabPanelName" data-properties-type="tabPanelName" data-object-id="" placeholder="Tab Name">'+
					'</div>'+
				'</div>'+
			'</div>';
	    return ret;
    }

    function obj_chkVisibility(obj_float,data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container chk_Visibility">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">'+"Visibility by formula: "+'  <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style chk_Visibility_below">'+ //
						'<div class="column div_1_of_1">'+
							'<span>'+"Visibility by formula: "+'  <font color="red"></font></span>'+
					    	'<label style="display:none;"><input type="radio" value="yes" class="visibility-choices css-checkbox" name="visibility-choices" id="visibility-choices" /><label for="visibility-choices" class="css-label"></label> Yes </label>'+
					    	'<label style="display:none;"><input type="radio" value="no" class="visibility-choices css-checkbox" name="visibility-choices" id="visibility-choices2" /><label for="visibility-choices2" class="css-label"></label> No </label>'+
					    	'<label style="display:none;"><input type="radio" checked="checked" value="computed" class="visibility-choices css-checkbox" name="visibility-choices" id="visibility-choices3" /><label for="visibility-choices3" class="css-label"></label> Computed </label>'+
					    	'<textarea class="form-textarea visibility-formula" data-properties-type="visibility-formula" data-ide-properties-type="ide-visibility-formula"></textarea>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_chkDisableFormula(obj_float,data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container obj_chkDisableFormula">'+
			    '<div class="fields_below">'+
					'<div class="label_below2">'+"Disable by formula: "+'  <font color="red"></font></div>'+
					'<div class="input_position_below obj_chkDisableFormula_below">'+
				    	'<textarea data-properties-type="DisableFormula" class="form-textarea disable-formula-prop obj_prop"></textarea>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_chkReadOnlyFormula(obj_float,data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container obj_chkReadOnlyFormula">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">'+"Readonly by formula: "+'  <font color="red"></font></div>'+
					'<div class=" input_position_below section clearing fl-field-style obj_chkReadOnlyFormula_below">'+ //
						'<div class="column div_1_of_1">'+
							'<span>'+"Readonly by formula: "+'  <font color="red"></font></span>'+
				    		'<textarea data-properties-type="ReadOnlyFormula" data-ide-properties-type="ide-read-only-formula" class="form-textarea read-only-formula-prop obj_prop"></textarea>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_chkETrig(obj_float,data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container chk_ETrig" style="height: 60px;">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">'+" Refresh trigger"+'  <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style chk_ETrig_below">'+ //
						'<div class="column div_1_of_1">'+
							'<span>'+" Refresh trigger:"+'  <font color="red"></font></span><Br/>'+
				    		'<label><input type="checkbox" value="yes" class="css-checkbox" id="refreshtrigger" /><label for="refreshtrigger" class="css-label"></label> Refresh Fields On Change</label>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_alignRCField(obj_float,data_type){
    	var labelRC_type = "";
    	if(data_type == "radioButton"){
    		labelRC_type = "Radio";
    	}else if(data_type == "checkbox"){
    		labelRC_type = "Checkbox";
    	}
    	if(labelRC_type.length){
    		var ret
	    	=	'<div class="' + obj_float + ' properties_width_container rcAlignmentProperty">'+
				    '<div class="fields_below">'+
						//'<div class="label_below2">'+labelRC_type+' alignment type: <font color="red"></font></div>'+
						'<div class="input_position_below section clearing fl-field-style rc_alignment">'+
							'<div class="column div_1_of_1">'+
								'<span>'+labelRC_type+' alignment type: <font color="red"></font></span>'+
						    	'<select class="form-select rc-alignment">'+
						    		'<option value="vertical">Vertical</option>'+
						    		'<option value="horizontal">Horizontal</option>'+
						    	'</select>'+
					    	'</div>'+
						'</div>'+
				    '</div>'+
				'</div>';
			return ret;
    	}else{
    		return "";
    	}
    }
    function obj_repeaterTable(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container repeaterTableOption">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Table Type: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style form_table_type">'+
						'<div class="column div_1_of_1">'+
							'<span>Table Type: <font color="red"></font></span>'+
					    	'<select class="form-select form-table-type">'+
					    		'<option value="normal">Default</option>'+
					    		'<option value="dynamic">Dynamic Row</option>'+
					    	'</select>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }

    function obj_embedPropertyOLD(obj_float1,obj_float2,obj_float3,data_type){
    	var ret
    	=	'<div class="' + obj_float1 + ' properties_width_container embedProperty">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Select Embed Source Form: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style ">'+ //
						'<div class="column div_1_of_1 embed_source_form">'+
							'<span>Select Embed Source Form:</span>'+
				    		'<select class="form-select embed-source-form" data-type="'+data_type+'" ></select>'+
						'</div>'+
					'</div>'+
			    '</div>'+ 
			'</div>';
		ret
		+=	'<div class="' + obj_float2 + ' properties_width_container embedProperty">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Source lookup field: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style embed_source_lookup_field">'+
						'<div class="choice-content">'+
							//embed-source-lookup-field
							'<div class="column div_1_of_1">'+
								'<span>Source lookup field: <font color="red"></font></span>'+
								'<select class="form-select embed-source-lookup-field" data-type="'+data_type+'" ></select>'+
							'</div>'+	
								
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		ret
		+=	'<div class="properties_width_container embedProperty">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Conditional operator: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style embed_conditional_operator">'+
						'<div style="margin-top:2px;" class="embed-conditional-operator-c">'+
							'<div class="column div_1_of_1">'+
								'<span>Conditional operator: <font color="red"></font></span>'+
								'<select  class="form-select embed-conditional-operator" data-type="'+data_type+'">'+
									'<option value="=">==</option>'+
									'<option value="<>">!=</option>'+
								'</select>'+
							'</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		ret
		+=	'<div class="' + obj_float3 + ' properties_width_container embedProperty">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Search Field: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style embed_result_field">'+
						'<div class="column div_1_of_1">'+
							'<span>Search Field: <font color="red"></font></span><Br/>'+
							'<label><input type="radio" value="static" checked="true" name="embed_rfc_choice" class=""/>Static</label>'+ //DITO NA KO
							'<label> <input type="radio" value="computed" name="embed_rfc_choice" class=""/>Computed</label>'+
							'<div class="embed-result-field-c" style="margin-top:7px;">'+
								'<select class="form-select embed-result-field" data-type="'+data_type+'" ></select>'+
							'</div>'+
							'<div style="display:none; margin-top:2px;" class="embed-result-field-computed-c">'+
								'<textarea  class="form-textarea embed-result-field-formula" data-ide-properties-type="embed-result-field-formula" data-type="'+data_type+'"></textarea>'+
							'</div>'+
				    	'</div>'+	
					'</div>'+
			    '</div>'+
			'</div>';
		ret
		+=	'<div class="properties_width_container embedProperty">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Additional filter formula: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style embed_additional_filter_formula">'+
						'<div class="column div_1_of_1">'+
							'<span>Additional filter formula: <font color="red"></font></span>'+
							'<div style="margin-top:2px;" class="embed-additional-filter-formula-c">'+
								'<textarea  class="form-textarea embed-additional-filter-formula" data-ide-properties-type="embed-additional-filter-formula" data-type="'+data_type+'"></textarea>'+
							'</div>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }

    function obj_embedProperty(obj_float1,obj_float2,obj_float3,data_type){
    	var ret
    	=	'<div class="' + obj_float1 + ' properties_width_container embedProperty">'+
			    '<div class="fields_below">'+
					'<div class="label_below2">Select Embed Source Form: <font color="red"></font></div>'+
					'<div class="input_position_below embed_source_form">'+
				    	'<select class="form-select embed-source-form" data-type="'+data_type+'" ></select>'+
					'</div>'+
			    '</div>'+ 
			'</div>';
		ret
		+=	'<div class="' + obj_float2 + ' properties_width_container embedProperty">'+
			    '<div class="fields_below">'+
					'<div class="input_position_below embedProperty_advance_search_filtering">'+
				    	'<div class="label_below2">Search filter of records: <font color="red"></font></div>'+
						'<div class="input_position_below" style="">'+
							'<table style="width:100%;">'+
								'<thead>'+
									'<tr>'+
										'<th style="text-align:center; width:33.33%;">Lookup field<th>'+
										'<th style="text-align:center;width:33.33%;">Filter condition<th>'+
										'<th style="text-align:center;width:33.33%;">Search Value<th>'+
									'</tr>'+
								'</thead>'+
							'</table>'+	
							'<table style="width:100%;">'+
								'<tbody class="easfp-tr-group">'+
									'<tr>'+
										'<td colspan="2"></td>'+
										'<td class="form-text"><table><tbody><tr>'+
											'<td><label>field:<input type="radio" name="easfp-embed-search-field-type" checked="checked"/></labe></td>'+
											'<td>'+
												'<label>computed:<input type="radio" name="easfp-embed-search-field-type"/></label>'+
												'<span class="easfp-add-minus-row" style="float:right;">'+
													'<i class="fa fa-plus" style="cursor:pointer;"></i> <i class="fa fa-minus" style="cursor:pointer;display:none;"></i>'+
												'</span>'+
											'</td>'+
										'</tr></tbody></table></td>'+
									'</tr>'+
									'<tr>'+
										'<td style="width:33.33%;"><select class="form-select easfp-data-lookup-field"><option value="">--Select--</option></select></td>'+
										'<td style="width:33.33%;">'+
											'<select class="form-select easfp-data-filter-condition">'+
												'<option value="=">==</option>'+
												'<option value="<="><=</option>'+
												'<option value=">=">>=</option>'+
												'<option value="<"><</option>'+
												'<option value=">">></option>'+
												'<option value="<>">!=</option>'+
												'<option value="%text%">contains</option>'+
											'</select>'+
										'</td>'+
										'<td style="width:33.33%;">'+
											'<div class="easfp-data-static-field-selection-val" ><select class="form-select easfp-data-search-field"><option value="">--Select--</option></select></div>'+
											'<div class="easfp-data-computed-formula-selection-val" style="display:none;">'+
												'<textarea class="form-textarea easfp-embed-result-field-formula"></textarea>'+
											'</div>'+
										'</td>'+
									'</tr>'+
								'</tbody>'+
							'</table>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
   
    function obj_inputValidation(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Input validation formula: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style obj_inputValidation">'+ //
						'<div class="column div_1_of_1">'+
							'<span>Input validation formula: <font color="red"></font></span>'+
				    		'<textarea class="form-textarea field-formula-validation obj_prop" data-ide-properties-type="ide-field-formula-validation" data-type="'+data_type+'" style="resize:vertical;"></textarea>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_inputValidationMessage(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Input validation message: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style obj_inputValidationMessage">'+ //
						'<div class="column div_1_of_1">'+
							'<span>Input validation message: <font color="red"></font></span>'+
				    		'<textarea class="form-textarea field-formula-validation-message" data-type="'+data_type+'" style="resize:vertical;"></textarea>'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_defaultValueChoices(obj_float,data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container multiple_field_value">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Default value: <button class="clear-default-value">Clear Value</button><font color="red"></font></div>'+
					'<div data-type="'+data_type+'" class="input_position_below section clearing fl-field-style input_position_below ">'+
						'<div class="column div_1_of_1">'+
							'<span ">Default value: <button class="clear-default-value">Clear Value</button><font color="red"></font></span>'+
							'<div id="obj_defaultValueChoices" class="obj_defaultValueChoices">'+
							'</div>'+
						'</div>'+	
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
	}
	function obj_defaultValue(obj_float,data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Default value: <font color="red"></font><span class="formula-help" help-type="DefaultComputed"><span class="icon-question-circle"></span></span></div>'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span>Default value: <font color="red"></font><span class="formula-help" help-type="DefaultComputed"><span class="icon-question-circle"></span></span></span>'+
				    		'<label><input data-properties-type="defaultValueType" class="css-checkbox" value="static" type="radio" checked="checked" name="default_val" id="Static" /><label for="Static" class="css-label"></label>Static </label>'+
				    		'<label><input data-properties-type="defaultValueType" class="css-checkbox" value="computed" type="radio"  name="default_val" id="Computed"/><label for="Computed" class="css-label"></label>Computed </label>'+
				    		'<label><input data-properties-type="defaultValueType" class="css-checkbox" value="middleware" type="radio"  name="default_val" id="Middleware"/><label for="Middleware" class="css-label"></label>Middleware </label>'+
							//'<label><input data-properties-type="defaultValueType" class="css-checkbox" value="OnSubmit" type="radio"  name="default_val" id="OnSubmit"/><label for="OnSubmit" class="css-label"></label>OnSubmit </label>'+
							'<textarea data-type="' + data_type + '" class="form-textarea obj_prop fl-remove-margin-top" id="" data-properties-type="defaultValue" data-ide-properties-type="ide-default-value" data-object-id="" style="resize:vertical;"></textarea>'+					
			    		'</div>'+
			    	'</div>'+
			    '</div>'+
			'</div>';
		return ret;
	}
	function obj_onchangeFormulaEvent(obj_float,data_type){
		if($('#app_config_formbuilder_form_events').val() == "0"){ 
			//this was disabled on configuration settings
            return "";
        }
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container">'+
			    '<div class="fields_below">'+
					// '<div class="label_below2">Default value: <font color="red"></font><span class="formula-help" help-type="DefaultComputed"><span class="icon-question-circle"></span></span></div>'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span>Onchange Formula: <font color="red"></font><span class="formula-help" help-type="OnchangeFormulaEvent"><span class="icon-question-circle"></span></span></span>'+
				    		// '<label><input data-properties-type="defaultValueType" class="css-checkbox" value="static" type="radio" checked="checked" name="default_val" id="Static" /><label for="Static" class="css-label"></label>Static </label>'+
				    		// '<label><input data-properties-type="defaultValueType" class="css-checkbox" value="computed" type="radio"  name="default_val" id="Computed"/><label for="Computed" class="css-label"></label>Computed </label>'+
				    		// '<label><input data-properties-type="defaultValueType" class="css-checkbox" value="middleware" type="radio"  name="default_val" id="Middleware"/><label for="Middleware" class="css-label"></label>Middleware </label>'+
							//'<label><input data-properties-type="defaultValueType" class="css-checkbox" value="OnSubmit" type="radio"  name="default_val" id="OnSubmit"/><label for="OnSubmit" class="css-label"></label>OnSubmit </label>'+
							'<textarea data-type="' + data_type + '" class="form-textarea obj_prop fl-remove-margin-top" id="" data-properties-type="onchange-formula-event" data-ide-properties-type="ide-onchange-formula-event" data-object-id="" style="resize:vertical;"></textarea>'+
						'</div>'+
		    		'</div>'+
			    '</div>'+
			'</div>';
		return ret;
	}
	function obj_middleWareChoice(obj_float,data_type){
		var ret
    	=	'<div class="' + obj_float + ' properties_width_container middlewareTaggingContainer" style="display:none;">'+
			    '<div class="fields_below">'+
					'<div class="label_below2">Middleware Formula Execution Tag: <font color="red"></font><span class="formula-help" help-type="DefaultComputed"><span class="icon-question-circle"></span></span></div>'+
					'<div class="input_position_below">'+
				    	'<textarea data-type="'+ data_type +'" class="form-textarea obj_prop" data-ide-properties-type="ide-middlewareTagging" data-properties-type="middlewareTagging">'+
				    	'</textarea>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
	}
    function obj_tabIndex(obj_float,data_type){
    	var ret
    	=	'<div class="' + obj_float + ' properties_width_container">'+
			    '<div class="fields_below">'+
					//'<div class="label_below2">Field Tab Index: <font color="red"></font></div>'+
					'<div class="input_position_below section clearing fl-field-style">'+ //
						'<div class="column div_1_of_1">'+
				    		'<span>Field Tab Index: <font color="red"></font></span>'+
				    		'<input type="number" min="1" name="" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" id="" data-properties-type="tabIndex" data-object-id="" placeholder="Tabindex">'+
						'</div>'+
					'</div>'+
			    '</div>'+
			'</div>';
		return ret;
    }
    function obj_lblName(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    //ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Label Caption: <font color="red">*</font></div>';
		ret += '<div class="input_position_below section clearing fl-field-style">'; //input_position_below 
			ret += '<div class=" column div_1_of_1">';
				ret += '<span>Label Caption: <font color="red">*</font> '+mobileIcon(data_type)+'</span>';
		    	ret += '<input type="text"  name="" data-type="' + data_type + '" class="form-text obj_prop" id="" data-properties-type="lblName" data-object-id="" placeholder="Label Name" required>';
			ret += '</div>';
		ret += '</div>';
	    //ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_lblInputName(obj_float,data_type) {
    	var object_type = "Field";
    	if(typeof this !== window){
    		if(this == "embeded-view"){
	    		object_type = "Object";
	    	}else if(this == "button"){
	    		object_type = "Button";
			
	    	}
    	}
		var ret = "";
		ret += '<div class="' + obj_float + ' properties_width_container">';
		    ret += '<div class="fields_below">';
				//ret += '<div class="label_below2">'+object_type+' Name: <font color="red">*</font></div>';
				ret += '<div class="section clearing fl-field-style">'; //input_position_below
					ret += '<div class="column div_1_of_1">';
						ret += '<span>'+object_type+' Name: <font color="red">*</font> '+mobileIcon(data_type)+'</span>';
				    	ret += '<input type="text"  name="" data-type="' + data_type + '" class="form-text obj_prop fl-lblFldName" id="" data-properties-type="lblFldName" data-object-id="" placeholder="'+object_type+' Name">';
					ret += '</div>';
				ret += '</div>';
			ret += '</div>';
			//UPDATED UI FOR FIELDS : TO DO 
			// ret += '<div class="section clearing fl-field-style">';
			// 	ret += '<div class="column div_1_of_1">';
			// 		ret += '<span> '+object_type +' Name: <font color="red">*</font></span>';
			// 		ret += '<input type="text"  name="" data-type="' + data_type + '" class="form-text obj_prop fl-lblFldName" id="" data-properties-type="lblFldName" data-object-id="" placeholder="'+object_type+' Name">';
			// 	ret += '</div>';
			// ret += '</div>';
		 //    ret += '</div>';

		//added by joshua reyes 03/07/2016
		if(typeof this !== window) {
    		if(this == "button") {
				 var get_visibility_formula_object_btn = $('[data-object-id="' + obj_float + '"]').attr("data-visibility-button");
				 var visibility_formula_obj_btn = "";
				 //Checked Condition for Old Buttons to filter out the Undefined value	
				 if (typeof get_visibility_formula_object_btn === "undefined" || get_visibility_formula_object_btn == "") {
				 	visibility_formula_obj_btn = "";
				 } else {
				 	visibility_formula_obj_btn = get_visibility_formula_object_btn;
				 }
					
				ret += '<div class="fields_below">';
					ret += '<div class="section clearing fl-field-style">'; //input_position_below
						ret += '<div class="column div_1_of_1">';
							ret += '<span>Visibility by formula:   <font color="red"></font></span>';
							ret += '<textarea id="btn_formula_textarea" class="form-textarea visibility-formula-btn ide-hover" data-properties-type-btn="visibility-formula-btn" data-ide-properties-type="ide-visibility-formula-btn" placeholder="">';
							ret += ''+ visibility_formula_obj_btn +'</textarea>';
						ret += '</div>';
					ret += '</div>';
				ret += '</div>';
			}
		}

		ret += '</div>';
	    return ret;
    }
    
    function obj_PlaceHolder(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Text Placeholder: <font color="red"></font></div>';
		ret += '<div class="input_position_below section clearing fl-field-style">';
			ret += '<div class="column div_1_of_1">';
				ret += '<span>Text Placeholder: <font color="red"></font> '+mobileIcon(data_type)+'</span>';
		    	ret += '<input type="text" name="" data-type="' + data_type + '" class="form-text obj_prop" id="" data-properties-type="lblPlaceHolder" data-object-id="" placeholder="Text Placeholder">';
			ret += '</div>';
		ret += '</div>';
	    ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_Tooltip(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		ret += '<div class="input_position_below section clearing fl-field-style">';
			ret += '<div class="column div_1_of_1">';
			ret += '<span>Tooltip (Help): <font color="red"></font></span>';
		    	ret += '<input type="text" name="" data-type="' + data_type + '" class="form-text obj_prop" id="" data-properties-type="lblTooltip" data-object-id="" placeholder="Tooltip">';
			ret += '</div>';
		ret += '</div>';
	    ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_lblFontWeight(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		ret += '<div class="label_below2">Label Font Weight: <font color="red"></font></div>';
		ret += '<div class="input_position_below">';
		    ret += '<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="lblFontWg">';
			
			ret += '<option value="normal">Normal</option>';
			ret += '<option value="bold">Bold</option>';
		    ret += '</select>';
		ret += '</div>';
	    ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_lblFontSize(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Label Font Size: <font color="red"></font></div>';
		ret += '<div class="input_position_below section clearing fl-field-style">'; //
			ret += '<div class="column div_1_of_1">';
				ret += '<span>Label Font Size: <font color="red"></font></span>';
			    ret += '<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="lblFontSize">';
			    
			    ret += '<option value="6px">6px</option>';
			    ret += '<option value="8px">8px</option>';
			    ret += '<option value="9px">9px</option>';
			    ret += '<option value="10px">10px</option>';
			    ret += '<option value="11px">11px</option>';
			    ret += '<option selected="selected" value="12px">12px</option>';
			    ret += '<option value="14px">14px</option>';
			    ret += '<option value="18px">18px</option>';
			    ret += '<option value="24px">24px</option>';
			    ret += '<option value="30px">30px</option>';
			    ret += '<option value="36px">36px</option>';
			    ret += '<option value="48px">48px</option>';
			    ret += '<option value="60px">60px</option>';
			    ret += '<option value="72px">72px</option>';
			    ret += '</select>';
			ret += '</div>';    
		ret += '</div>';
	    ret += '</div>';
	ret += '</div>';
	    return ret;
    }
	
    function obj_lblAllow(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Allow Label:</div>';
		ret += '<div class="input_position_below section clearing fl-field-style">'; //
			ret += '<div class="column div_1_of_1">';
				ret += '<span>Allow Label:</span>';
		    	ret += '<select data-type="' + data_type + '" name="" class="form-select obj_prop" id="" data-properties-type="lblAllowLabel" data-object-id="">';
				ret += '<option value="Yes">Yes</option>';
				ret += '<option value="No">No</option>';
		    	ret += '</select>';
		    ret += '</div>';		
		ret += '</div>';
	    ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_addChoices(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	 //    ret += '<div class="fields_below">';
	 //   	ret += '<div class="label_below2">List: <button class="sortlist-value">Sort</button></br>'
	 //    if(data_type == "dropdown"){
	 //    	ret += '<span style="font-size:9px;">*Note : Place a pipeline symbol (|) at the end of the text to indicate a blank value. eg : --select--|</span> ';
	 //    }
		// ret+='</div>';
		ret += '<div class="input_position_below section clearing fl-field-style">'; //
			ret += '<div class="column div_1_of_1">';
				ret += '<span>List: <button class="sortlist-value">Sort</button></br></span>';
				if(data_type == "dropdown"){
			    	ret += '<span style="font-size:9px;">*Note : Place a pipeline symbol (|) at the end of the text to indicate a blank value. eg : --select--|</span> ';
			    }
		    	ret += '<textarea name="" data-type="' + data_type + '" class="form-textarea obj_prop" id="" data-properties-type="lblChoices" data-object-id="" style="height:90px;">';
		    	ret += '</textarea>';
		    ret += '</div>';
		ret += '</div>';
	    ret += '</div>';
	//ret += '</div>';
	    return ret;
    }
    
    function obj_fldMaxLength(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Field Max Length: <font color="red"></font></div>';
		ret += '<div class="input_position_below section clearing fl-field-style">';
			ret +='<div class="column div_1_of_1">';
				ret += '<span>Field Max Length: <font color="red"></font></span>';
		    	ret += '<input type="text" name="" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" id="" data-properties-type="lblMaxLength" data-object-id="" placeholder="Max Length">';
			ret += '</div>';
		ret += '</div>';
	    ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_fldHeight(obj_float,data_type) {
       var object_type = "";
    var eleLabel = "Field Height:"
   	if(this !== window){
		if(this == "createLine"){
    		object_type = "Line";
    		eleLabel = "Line Height:";
    	}
	    
    }
    
    
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
	   
		//ret += '<div class="label_below2">'+eleLabel+'</div>';
		ret += '<div class=" input_position_below section clearing fl-field-style">';
			ret += '<div class="column div_1_of_1">';
				ret += '<span>'+eleLabel+'</span>';
		    	ret += '<input type="text" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" id="" data-properties-type="lblFieldHeight" data-object-id="" placeholder="Field Height"/>';
			ret += '</div>';
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    function obj_fldMinHeight(obj_float,data_type) {//BAGO
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		ret += '<div class="label_below2">Field Min Height:</div>';
		ret += '<div class="input_position_below">';
		    ret += '<input type="text" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" id="" data-properties-type="lblFieldMinHeight" data-object-id="" placeholder="Field Min Height"/>';
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    function obj_fldMaxHeight(obj_float,data_type) {//BAGO
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		ret += '<div class="label_below2">Field Max Height:</div>';
		ret += '<div class="input_position_below">';
		    // ret += '<input type="text" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" id="" data-properties-type="lblFieldMaxHeight" data-object-id="" placeholder="Field Max Height"/>';
		    // ret += '<input type="text" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" id="" data-properties-type="lblFieldMaxHeight" data-object-id="" placeholder="Field Max Height"/>';
		    ret += '<select data-type="' + data_type + '" class="form-select obj_prop " id="" data-properties-type="lblFieldMaxHeight" data-object-id="" >';
		    	//ret += '<option value="20">--Select--</option>';
		    	for(var ii = 20 ; ii <= 500 ; ii++){
		    			ret += '<option value="'+ii+'">'+ii+'</option>';
		    	}
		    	ret += '<option value="none">Infinite</option>';
		    ret += '</select>';
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    function obj_fldWidth(obj_float,data_type) {
    var object_type = "";
    var eleLabel = "Field Width:"
   	if(this !== window){
		if(this == "createLine"){
    		object_type = "Line";
    		eleLabel = "Line Width:";
    	}
	    
    }
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';

		//ret += '<div class="label_below2">'+eleLabel+'</div>';
		ret += '<div class=" input_position_below section clearing fl-field-style">'; //
			ret += '<div class="column div_1_of_1">';
			ret += '<span>'+eleLabel+'</span>';
		    	ret += '<input type="text" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" id="" data-properties-type="lblFieldWidth" data-object-id="" placeholder="Field Width"/>';
			ret += '</div>';
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    function obj_fldMinWidth(obj_float,data_type) {//BAGO
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		ret += '<div class="label_below2">Field Min Width:</div>';
		ret += '<div class="input_position_below">';
		    ret += '<input type="text" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" id="" data-properties-type="lblFieldMinWidth" data-object-id="" placeholder="Field Min Width"/>';
		    
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    function obj_fldMaxWidth(obj_float,data_type) {//BAGO
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		ret += '<div class="label_below2">Field Max Width:</div>';
		ret += '<div class="input_position_below">';
		    // ret += '<input type="text" data-type="' + data_type + '" class="form-text obj_prop prop_numeric_val" id="" data-properties-type="lblFieldMaxWidth" data-object-id="" placeholder="Field Max Width"/>';
		    ret += '<select data-type="' + data_type + '" class="form-select obj_prop " id="" data-properties-type="lblFieldMaxWidth" data-object-id="" >';
		    	//ret += '<option value="20">--Select--</option>';
		    	for(var ii = 100 ; ii <= 500 ; ii++){
		    			ret += '<option value="'+ii+'">'+ii+'</option>';
		    	}
		    	ret += '<option value="none">Infinite</option>';
		    ret += '</select>';
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    function obj_fldType(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Field Type:</div>';
		ret += '<div class="input_position_below section clearing fl-field-style">';
			ret += '<div class="column div_1_of_1">';
				ret += '<span>Field Type:</span>';
			    ret += '<select name="" data-type="' + data_type + '" class="form-select obj_prop" id="" data-properties-type="lblFieldType" data-object-id="">';
				ret += '<option data-input-type="Text" value="longtext">Text</option>';
				ret += '<option data-input-type="Currency" value="double">Currency</option>';
				ret += '<option data-input-type="Number" value="double">Number</option>';
			    ret += '</select>';
		    ret += '</div>';
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_allowDuplicate(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		ret += '<div class="label_below2">Allow Duplicate:</div>';
		ret += '<div class="input_position_below">';
		    ret += '<input type="button" name="" data-type="' + data_type + '" class="btn-basicBtn obj_prop" id="" data-properties-type="lblAllowDuplicate" data-object-id="" value="Yes">';
		ret += '</div>';
	    ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_lblFontColor(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Label Font Color:</div>';
		ret += '<div class="input_position_below section clearing fl-field-style">'; //
		    ret += '<div class="column div_1_of_1">';
		    	ret += '<span>Label Font Color:</span><Br/>';	
		    	ret += '<input type="text" data-type="' + data_type + '" class="chngColor" data-properties-type="lblFontolor" data-type="label"/>';
			ret += '</div>';
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_fldColor(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Border Field Color:</div>';
		ret += '<div class="input_position_below section clearing fl-field-style">';
			ret += '<div class="column div_1_of_1">';
				ret +='<span>Border Field Color:</span><Br/>';
		    	ret += '<input type="text" data-type="' + data_type + '" class="chngColor" data-properties-type="lblFieldColor" data-type="button"/>';
			ret += '</div>';
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    
    function obj_lblAlignment(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Label Alignment:</div>';
		ret += '<div class="input_position_below section clearing fl-field-style">'; //
			ret += '<div class="column div_1_of_1">';
				ret += '<span>Label Alignment:</span>';
			    ret += '<select name="" data-type="' + data_type + '" class="form-select obj_prop" id="" data-properties-type="lblAlignment" data-object-id="">';
				ret += '<option value="alignTop">Align Top</option>';
				ret += '<option value="alignLeft">Align Left</option>';
		    	ret += '</select>';
			ret += '</div>';
		ret += '</div>';
		ret += '</div>';
	ret += '</div>';
	    return ret;
    }
    function processorType_fb(obj_float,data_type) {
		var ret = "";
		ret += '<div class="' + obj_float + ' properties_width_container">';
		    ret += '<div class="fields_below">';
			
				ret += '<div class="input_position_below section clearing fl-field-style">';
				ret += '<div class="column div_1_of_1">';
					ret += '<span>Processor Type:</span>';
					ret += '<select class="form-select workflow-processor-type obj_prop" data-properties-type="workflow_processor_type" id="" style="margin-top:0px">'
					ret +=   '<option value="0">--Select--</option>'
					ret +=   '<option value="1">Department Position</option>'
					ret +=   '<option value="2">Company Position</option>'
					ret +=   '<option value="3">Employee</option>'
					ret +=   '<option value="4">Requestor</option>'
					// ret +=   '<option value="5">Field</option>'
					ret +=  '</select>';
				ret += '</div>';
			
			ret += '</div>';
			ret += '</div>';
		ret += '</div>';
		return ret;
    } 
    function processor_fb(obj_float,data_type) {
		var ret = "";
		ret += '<div class="' + obj_float + ' properties_width_container">';
		    ret += '<div class="fields_below">';
				ret += '<div class="input_position_below section clearing fl-field-style">';
						ret += '<div class="column div_1_of_1">';
							ret += '<span>Processor:</span>';
							ret += '<select class="form-select workflow-processor obj_prop" id="" style="margin-top:0px">';
				            ret += '<option value="0">--Select--</option>';
				            ret += '</select>';
				            ret += '<img src="/images/loader/loading.gif" class="pull-left display processorLoad"/>';
						ret += '</div>';
				ret += '</div>';
			ret += '</div>';
		ret += '</div>';
		return ret;
    }
    function hyperlink_lbl(obj_float,data_type) {
		var ret = "";
		ret += '<div class="' + obj_float + ' properties_width_container">';
		    ret += '<div class="fields_below">';
				//ret += '<div class="label_below2">Link Address:</div>';
				ret += '<div class="input_position_below section clearing fl-field-style">';
					ret += '<div class="column div_1_of_1">';
						ret += '<span>Link Address:</span><Br/>';
						ret += '<label><input data-properties-type="defaultHyperLinkValue" class="css-checkbox" value="static" type="radio" checked="checked" name="default_hyperlink" id="Static" /><label for="Static" class="css-label"></label>Static </label>';
						ret += '<label><input data-properties-type="defaultHyperLinkValue" class="css-checkbox" value="computed" type="radio"  name="default_hyperlink" id="Computed"/><label for="Computed" class="css-label"></label>Computed </label>';
						ret += '<input type="text" data-type="' + data_type + '" class="form-text obj_prop" data-properties-type="lblHyperLink" data-type="button" placeholder="Link Address"/>';
					ret += '</div>';
				ret += '</div>';
			ret += '</div>';
		ret += '</div>';
		return ret;
    }
    function relatedFormDetails(obj_float,data_type){
    	var ret = "";
    	ret += '<div class="' + obj_float + ' properties_width_container embedCommitDataRowProperty">';
    	    ret += '<div class="fields_below">';
    			//ret += '<div class="label_below2">Link Address:</div>';
    			ret += '<div class="input_position_below section clearing fl-field-style">';
    				ret += '<div class="column div_1_of_1">';
    				//dropdown
    				ret += '<span>Commit Data Row Event:</span>';
					ret += '<select name="commit_data_row_event" data-properties-type="commit_data_row_event" class="form-select obj_prop" id=""  data-object-id="">';
						ret += '<option value="default">On Child Submission</option>';
						ret += '<option value="parent">On Parent Submission</option>';
					ret += '</select>';
    				ret += '</div>';
    			ret += '</div>';
    		ret += '</div>';
    	ret += '</div>';
    	return ret;
    }
    function obj_image(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		ret += '<div class="label_below2">Allow Duplicate:</div>';
		ret += '<div class="input_position_below">';
		    ret += '<input type="button" name="" data-type="' + data_type + '" class="btn-basicBtn obj_prop" id="" data-properties-type="lblAllowDuplicate" data-object-id="" value="Yes">';
		ret += '</div>';
	    ret += '</div>';
	ret += '</div>';
	    return ret;
    }

function obj_objectCss(obj_float, data_type){
var ret
=	'<div class="' + obj_float + ' properties_width_container obj_objectCss_container ">'+
	    '<div class="fields_below">'+
			'<div class="input_position_below section clearing fl-field-style obj_objectCss_prop">'+ //
					'<div class="column div_1_of_1">'+
						'<span>Object custom styles</span>'+
					
				    	'<textarea  data-type="' + data_type + '" data-properties-type="obj_objectCss" data-ide-properties-type="ide-object-css-styles"  class="form-select obj_prop" id="object-css">'+ //obj_prop ibalik pag may problem  
				    	'</textarea>'+
				'</div>'+
			'</div>'+
			
	    '</div>'+
	'</div>';
return ret;
}

function obj_imageBackground(obj_float,data_type) {
	var ret = "";
	ret += '<div class="' + obj_float + ' properties_width_container">';
	    ret += '<div class="fields_below">';
		//ret += '<div class="label_below2">Label Font Size: <font color="red"></font></div>';
		ret += '<div class="input_position_below section clearing fl-field-style">'; //
			ret += '<div class="column div_1_of_1">';
				ret += '<span>Set As Background: <font color="red"></font></span>';
			    ret += '<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="imgBckground">';
			    ret += '<option value="1">Default</option>';
			    ret += '<option value="2">Fit To Screen</option>';
			    ret += '<option value="3">Fill</option>';
			    ret += '<option value="4">Tiles</option>'
			    ret += '</select>';
			ret += '</div>';    
		ret += '</div>';
	    ret += '</div>';
	ret += '</div>';
	    return ret;
    }
function obj_SectionfontType(obj_float,data_type) {

	var ret
		= '<div class="' + obj_float + ' properties_width_container">'+
			'<div class="fields_below">'+
				//'<div class="label_below2">Label Font Family:<span class="font-preview">Sample Text</span></div>'+
				'<div class="input_position_below section clearing fl-field-style">'+ //
					'<div class="column div_1_of_1">'+
						'<span>Section Font Family: <span class="font-preview">Sample Text</span></span>'+
						'<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="secFontType">';
				    		//var keys = Object.keys(fontFamily);
				    	
					        for (var key in newFontFamily) {
					        	newFontFamily[key]["bold"]
					        	newFontFamily[key]["regular"]
					        	ret+='<option value="'+ newFontFamily[key]["regular"] +'"  attrfontNormal="'+newFontFamily[key]["regular"]+'" attrfontbold="'+newFontFamily[key]["bold"]+'">'+ key +'</option>';
					        }
					    ret += '</select>'+
					  	 	//i, len = keys.length;
					  
						   	//keys.sort();
							  
						  	//for (i = 0; i < len; i++)
						  	//{
						      //k = keys[i];
						    
						      //ret+='<option value="'+ fontFamily[k]+'" attrfontbold="" attrfontNormal="" >'+ k +'</option>';
						  	//}
					        //ret += '</select>'+
				     '</div>'+   
				        
				'</div>'+
				
			'</div>'+
		'</div>';
	    return ret;
    }
    function obj_sectionwsd(obj_float,data_type){
	    var ret	= '<div class="' + obj_float + ' properties_width_container">'+
	    			'<div class="fields_below ">'+ //fl-controls-genProperties
	    				//'<div class="label_below2">Label Font Format:</div>'+
	    				'<div class="input_position_below section clearing fl-field-style fl-textfield-type-opt">'+
	    					 '<div class="column div_1_of_1">'+
	    					 	 '<span>Section Font Format:</span>'+
		    					 '<span class="spanbutton" style="margin-left:3px"><input class="textdecor fl-input-form-settings prop_label_font_weight" type="checkbox" value="bold" id="pa1" data-properties-type = "secFontWg"><label for="pa1"><i class="fa fa-bold textpos"></i></label></span>'+
								 '<span class="spanbutton" style="margin-left:3px"><input class="textdecor fl-input-form-settings prop_label_font_style" type="checkbox" value="italic" id="pa2" data-properties-type = "lbl_fs"><label for="pa2"><i class="fa fa-italic textpos"></i></label></span>'+
								 '<span class="spanbutton" style="margin-left:3px"><input class="textdecor fl-input-form-settings prop_label_text_decoration" type="checkbox" value="underline" id="pa3" data-properties-type = "lbl_td"><label for="pa3"><i class="fa fa-underline textpos"></i></label></span>'+
	    					'</div>'+
	    				'</div>'+
	    			'</div>'+
	    		'</div>';
	    return ret;
    }

    //copy this.....
    function embed_allowImport(obj_float, data_type){
    	var ret	= '<div class="' + obj_float + ' properties_width_container embedImport">'+
    				'<div class="fields_below ">'+ //fl-controls-genProperties
    					//'<div class="label_below2">Label Font Format:</div>'+
    					'<div class="input_position_below section clearing fl-field-style fl-textfield-type-opt">'+
    						 '<div class="column div_1_of_1">'+
    						 	 '<label style="display:block; margin-bottom:5px;"><input type="checkbox" value="embedded-view-allow-import" class="embedded_view_click_field_prop embedded_view_allow_import css-checkbox" data-properties-type="EmbeddedViewAllowImport" id="EmbeddedViewAllowImport"><label for="EmbeddedViewAllowImport" class="css-label"></label> Allow Import Records</label>' + 
    						'</div>'+
    					'</div>'+
    				'</div>'+
    			'</div>';
    	return ret;
    }

    //form data table functions
    function obj_formDataTableActions(obj_float,data_type){
	    var ret	= '<div class="' + obj_float + ' properties_width_container">'+
	    			'<div class="fields_below ">'+ //fl-controls-genProperties
	    				//'<div class="label_below2">Label Font Format:</div>'+
	    				'<div class="input_position_below section clearing fl-field-style fl-textfield-type-opt">'+
	    					 '<div class="column div_1_of_1">'+
	    					 	 '<label style="display:block; margin-bottom:5px;"><input type="checkbox" value="form_data_table_numbering" class="form_data_table_click_field_prop form_data_table_row_number_prop css-checkbox" data-properties-type="FormDataTableNumbering" id="FormDataTableNumbering"><label for="FormDataTableNumbering" class="css-label"></label> Display Column Number</label>' + 
	    					'</div>'+
	    				'</div>'+
	    			'</div>'+
	    		'</div>';
	    return ret;
    }
    function obj_formDataTableSearchField(obj_float,data_type){
	    var ret	= '<div class="' + obj_float + ' properties_width_container">'+
	    			'<div class="fields_below ">'+ //fl-controls-genProperties
	    				//'<div class="label_below2">Label Font Format:</div>'+
	    				'<div class="input_position_below section clearing fl-field-style fl-textfield-type-opt">'+
	    					 '<div class="column div_1_of_1">'+ 
	    					 	'<span>Select Form:</span>';
	    					 	ret += '<select data-type="' + data_type + '" class="form-select obj_prop" data-object-id="" id="" data-properties-type="FormDataTableLookupForm">';
	 					    		//var keys = Object.keys(fontFamily);
	 						        for (var key in newFontFamily) {
	 						        	newFontFamily[key]["bold"]
	 						        	newFontFamily[key]["regular"]
	 						        	ret+='<option value="'+ newFontFamily[key]["regular"] +'"  attrfontNormal="'+newFontFamily[key]["regular"]+'" attrfontbold="'+newFontFamily[key]["bold"]+'">'+ key +'</option>';
	 						        }
	 						    ret += '</select>'+
	    					'</div>'+
	    				'</div>'+
	    			'</div>'+
	    		'</div>';
	    return ret;
    }
    function obj_formDataTableLookupForm(obj_float, data_type){
	    var ret = '<div class="' + obj_float + ' properties_width_container form-datatable-lookup-form">'+
			'<div class="fields_below">'+
				//'<div class="label_below2">Label Text Alignment:</div>'+
				'<div class="input_position_below section clearing fl-field-style">'+ //
					'<div class="column div_1_of_1">'+
						'<span>Select Form:</span>'+
						'<select data-type="' + data_type + '" class="form-select form-data-table-source-form" data-object-id="" id="" data-properties-type="FormDataTableLookupForm">'+
							'<option value="">--select form--</option>'+
					    '</select>'+
				    '</div>'+
				'</div>'+
				
			'</div>'+
		'</div>';
    	return ret;
    }

    function obj_formDataTableLookupField(obj_float, data_type){
    	var ret = "";
    	ret += '<div class="' + obj_float + ' properties_width_container form-datatable-lookup-field">';
    	    ret += '<div class="fields_below">';
    		//ret += '<div class="label_below2">Label Alignment:</div>';
    		ret += '<div class="input_position_below section clearing fl-field-style">'; //
    			ret += '<div class="column div_1_of_1">';
    				ret += '<span>Form Lookup Field:</span>';
    			    ret += '<select name="" data-type="' + data_type + '" class="form-select form-data-table-source-field" id="" data-properties-type="FormDataTableLookupField" data-object-id="">';
    			    ret += '<option value="">--select field--</option>'
    		    	ret += '</select>';
    			ret += '</div>';
    		ret += '</div>';
    		ret += '</div>';
    	ret += '</div>';
    	return ret;
    }

    function obj_formDataTableLookupOperator(obj_float, data_type){
    	var ret = "";
    	ret += '<div class="' + obj_float + ' properties_width_container form-datatable-lookup-operator">';
    	    ret += '<div class="fields_below">';
    		//ret += '<div class="label_below2">Label Alignment:</div>';
    		ret += '<div class="input_position_below section clearing fl-field-style">'; //
    			ret += '<div class="column div_1_of_1">';
    				ret += '<span>Conditional Operator:</span>';
    			    ret += '<select name="" data-type="' + data_type + '" class="form-select form-data-table-source-operator" id="" data-properties-type="FormDataTableLookupOperator" data-object-id="">';
    			    ret += '<option value="">--select operator--</option>'
    			    ret += '<option value="=">equal</option>'
    			    ret += '<option value="<>">NOT equal</option>'
    		    	ret += '</select>';
    			ret += '</div>';
    		ret += '</div>';
    		ret += '</div>';
    	ret += '</div>';
    	return ret;
    }

    function obj_formDataTableSearchField(obj_float, data_type){
    	var ret = "";
    	ret += '<div class="' + obj_float + ' properties_width_container form-datatable-search-field">';
    	    ret += '<div class="fields_below">';
    		//ret += '<div class="label_below2">Label Alignment:</div>';
    		ret += '<div class="input_position_below section clearing fl-field-style">'; //
    			ret += '<div class="column div_1_of_1">';
    				ret += '<span>Form Search Field:</span>';
    			    ret += '<select name="" data-type="' + data_type + '" class="form-select form-data-table-search-field" id="" data-properties-type="FormDataTableLookupField" data-object-id="">';
    			    ret += '<option value="">--select field--</option>'
    		    	ret += '</select>';
    			ret += '</div>';
    		ret += '</div>';
    		ret += '</div>';
    	ret += '</div>';
    	return ret;
    }

    function obj_formDataTableDisplayColumn(obj_float, data_type) {
    	var ret
    	= '<div class="' + obj_float + ' properties_width_container form-datatable-display-column">' +
    	'<div class="fields_below">' +
    		'<div class="label_below2">Select column to display:</br><span style="font-size:9px;">*Fill-up the field label of the fields you would like to appear in the column header.</br>*Drag the field name up or down to rearrange.</span></div>' +
    			'<div class="input_position_below section clearing fl-field-style">' +
    	//					'<select multiple data-type="' + data_type + '" name="" class="form-select embed-display-column" id="" data-properties-type="lblAllowLabel" data-object-id="">'+
    	//						//magkakalaman
    		//					'</select>'+
    				'<div multiple data-type="' + data_type + '" name="" class=" column div_1_of_1 form-select form-data-table-display-column" id="" data-properties-type="lblAllowLabel" data-object-id="" style="background:transparent; border:none;">' +
    					//magkakalaman
    				'</div>' +
    			'</div>' +
    		'</div>' +
    	'</div>';
    	return ret;
    }


    //end of form data t able functions

    // PLEASE USE THIS TEMPLATE FOR OBJECT PROPERTIES
    // Object template
    // Roni Pinili 
	// var ret = "";
	// ret += '<div class="' + obj_float + ' properties_width_container">';
	// 	ret += '<div class="fields_below">';
	// 		ret += '<div class="input_position_below section clearing fl-field-style">';
	// 			ret += '<div class="column div_1_of_1">';
	// 				ret += '<span>Title of object</span><Br/>';
	// 				ret += 'textfield radio checkbox etc'
	// 			ret += '</div>';
	// 		ret += '</div>';
	// 	ret += '</div>';
	// ret += '</div>';
	// return ret;


function setFieldProperties(){
console.time("Save")
	var object_id = if_undefinded($(".workflow-processor").attr("data-object-id"),"");
	var json = $("body").data();
	var property_errors = 0;

	if(object_id){
		var ptype = $(".workflow-processor-type").val();
		var proc = $(".workflow-processor").val();
		
		if(json[''+ object_id +'']){
			json[''+ object_id +'']['processor-type'] = ptype;
			json[''+ object_id +'']['processor'] = proc;
			$("#getFields_"+object_id).attr("processor-type",ptype);
			$("#getFields_"+object_id).attr("processor",proc);

		}
	}

	var popupcontainer = $("#popup_container")
	var OID = popupcontainer.attr("data-object-id");
	
	
	applyPropertyValues.call({
		"popupcontainer":popupcontainer,
		"OID":OID
	});

	//added by joshua reyes 03/07/2016 [Link Button]
	var visibility_formula_object_btn = $('[data-properties-type-btn="visibility-formula-btn"]').val();
	$('[data-object-id="'+OID+'"]').attr("data-visibility-button", visibility_formula_object_btn);

	{
		//saving property of image picklist
		// if ($('body').data(OID)) {//kapag existing na ung prop data
		// 	var body_data_val_iod = $('body').data(OID);
		// 	body_data_val_iod['imagePrev'] = $(".imagePrev").attr("src");
		// 	$('body').data(OID, body_data_val_iod );
		// 	var img = '<img data-placement="bottom" data-type="pickList" data-original-title="" src="' + $(".imagePrev").attr("src") + '" width="10" height="10" class="avatar userAvatar imagePrev" style="float: left;margin-top: -21px;margin-left: 5px;cursor: pointer;">';
		// 	$('a[return-field="getFields_' + OID + '"]').html(img);

		// }else{ //pag di pa existing ung prop data
		// 	$('body').data(OID, {'imagePrev':$(".imagePrev").attr("src")} );
		// 	var img = '<i class="icon-list-alt fa fa-list-alt"></i>';
		// 	$('a[return-field="getFields_' + OID + '"]').html(img);
		// 	alert("adsdsa")
		// }
	}

	if(popupcontainer.find(".embed-result-field").length >= 1){
		popupcontainer.find(".embed-result-field").trigger("change");
	}

	if (typeof json['' + OID] != "undefined") {
        if ($(".design-embed-highlights-prop").length >= 1) {
            json['' + OID + '']['embed-hl-data'] = [];
            $(".design-embed-highlights-prop").find("tr").each(function(eqi) {
                if ($(this).hasClass('HL_RULE_PROP1')) {
                    var hl_rule_prop1 = $(this);
                    var hl_rule_prop2 = $(this).next();
                    json['' + OID + '']['embed-hl-data'].push({
                        "hl_color": "" + hl_rule_prop1.find(".sp-preview-inner").css("background-color"),
                        "hl_fn": "" + hl_rule_prop1.find(".hl-fn").val(),
                        "condition_type": "" + hl_rule_prop2.find(".choose-condition-type").val(),
                        "hl_value": "" + hl_rule_prop2.find(".hl-value").val(),
                        "hl_value2": "" + hl_rule_prop2.find(".hl-value2").val(),
                        "value_type": hl_rule_prop2.find(".value-type-HL:checked").val()
                    });
                } else {
                    return true;
                }
            });
            json['' + OID + '']['embed-hl-data'] = JSON.stringify(json['' + OID + '']['embed-hl-data']);
            $("#setObject_" + OID).find(".getFields_" + OID).attr("embed-hl-data", json['' + OID + '']['embed-hl-data']);
        }
        if ($(".embedded-view-columns").length >= 1) {
            json['' + OID + '']['embed-column-data'] = [];
			// $(".embed-display-column").find("option:selected").each(function(eqi){
				// json[''+ OID +'']['embed-column-data'].push($(this).val());
			// })
			// json[''+ OID +'']['embed-column-data'] = JSON.stringify(json[''+ OID +'']['embed-column-data']);
            var embeddedViewColumns = [];
            var sortedembeddedViewColumns = [];
            $('.embedded-view-columns tr').each(function(index) {
                var fieldLabel = $(this).children('td').eq(0).children('input').val();
                if(fieldLabel){
                	fieldLabel = fieldLabel.trim();
                }
                var fieldName = $(this).children('td').eq(1).children('span').text();
                if (index > 0 && fieldLabel != '') {
                    embeddedViewColumns.push({
                        FieldLabel: fieldLabel,
                        FieldName: fieldName
                    });
                }
                if (index > 0) {
                    sortedembeddedViewColumns.push({
                        FieldLabel: fieldLabel,
                        FieldName: fieldName
                    });
                }
            });
            var selected_form = $('.embed-source-form').val();
            json['' + OID + '']['embed-column-data'] = JSON.stringify(embeddedViewColumns);
            json['' + OID + '']['data-embed-sortedcolumn'] = JSON.stringify({"selected_form":selected_form,"sortedcols":sortedembeddedViewColumns});
            
            $("#setObject_" + OID).find(".getFields_" + OID).attr("data-embed-sortedcolumn",json['' + OID + '']['data-embed-sortedcolumn']);
            $("#setObject_" + OID).find(".getFields_" + OID).attr("embed-column-data", json['' + OID + '']['embed-column-data']);
        }
    } else {
        json['' + OID + ''] = {};

        if ($(".design-embed-highlights-prop").length >= 1) {
            json['' + OID + '']['embed-hl-data'] = [];
            $(".design-embed-highlights-prop").find("tr").each(function(eqi) {
                if ($(this).hasClass('HL_RULE_PROP1')) {
                    var hl_rule_prop1 = $(this);
                    var hl_rule_prop2 = $(this).next();
                    json['' + OID + '']['embed-hl-data'].push({"hl_color": "" + hl_rule_prop1.find(".sp-preview-inner").css("background-color"), "hl_fn": "" + hl_rule_prop1.find(".hl-fn").val(),
                        "condition_type": "" + hl_rule_prop2.find(".choose-condition-type").val(), "hl_value": "" + hl_rule_prop2.find(".hl-value").val(),
                        "hl_value2": "" + hl_rule_prop2.find(".hl-value2").val(),
                        "value_type": hl_rule_prop2.find(".value-type-HL:checked").val()
                    });
                } else {
                    return true;
                }
            })
            json['' + OID + '']['embed-hl-data'] = JSON.stringify(json['' + OID + '']['embed-hl-data']);
            $("#setObject_" + OID).find(".getFields_" + OID).attr("embed-hl-data", json['' + OID + '']['embed-hl-data']);
        }
        if ($(".embedded-view-columns").length >= 1) {
            json['' + OID + '']['embed-column-data'] = [];
           // $(".embed-display-column").find("option:selected").each(function(eqi) {
               // json['' + OID + '']['embed-column-data'].push($(this).val());
           // })
           // json['' + OID + '']['embed-column-data'] = JSON.stringify(json['' + OID + '']['embed-column-data']);
            var embeddedViewColumns = [];
            var sortedembeddedViewColumns = [];
            $('.embedded-view-columns tr').each(function(index) {
                var fieldLabel = $(this).children('td').eq(0).children('input').val();
                var fieldName = $(this).children('td').eq(1).children('span').text();
                if (index > 0 && fieldLabel != '') {
                    embeddedViewColumns.push({
                        FieldLabel: fieldLabel,
                        FieldName: fieldName
                    });
                }
                  if (index > 0) {
                    sortedembeddedViewColumns.push({
                        FieldLabel: fieldLabel,
                        FieldName: fieldName
                    });
                }
            });
            var formsaver = $('.embed-source-form').val();

            json['' + OID + '']['embed-column-data'] = JSON.stringify(embeddedViewColumns);
            json['' + OID + '']['data-embed-sortedcolumn'] = JSON.stringify({formsaverZ:formsaver,"sortedcols":sortedembeddedViewColumns});
            
            $("#setObject_" + OID).find(".getFields_" + OID).attr("data-embed-sortedcolumn",json['' + OID + '']['data-embed-sortedcolumn']);
            $("#setObject_" + OID).find(".getFields_" + OID).attr("embed-column-data", json['' + OID + '']['embed-column-data']);
        }
    }

    if($('.embedDefaultSorting').length >= 1){
    	var obj_on_form_sel = $(".getFields_"+OID);
    	var sort_prop = $('.embedDefaultSorting').find('.edsc-prop');
    	var sort_column = sort_prop.filter('.edsc-field-name').val();
    	var sort_type = sort_prop.filter('.edsc-sort-type').val();
    	if(sort_column != '--Select--'){
    		obj_on_form_sel.attr('data-default-sort-col',sort_column);
    		obj_on_form_sel.attr('data-default-sort-type',sort_type);
    	}
    }
    console.log("visibility length", $('.advance-embed-action-event-visibility').length);
    if($('.advance-embed-action-event-visibility').length >= 1 ){
		var table_advance_visibility = $('.advance-embed-action-event-visibility');
		var obj_on_form_sel = $(".getFields_"+OID);
		var collect_advance_visibility_data = [];
		// console.log("find table", table_advance_visibility.children('table'));
		collect_advance_visibility_data = table_advance_visibility.find('.aeaev-tr-group').map(function(){
			if($(this).find('.aeaev-formula-based-visibility').val() != "" && $(this).find('.aeaev-data-target-visibility').val() != "-1"){
				return [
					$(this).find('.aeaev-formula-based-visibility').val(),
					$(this).find('.aeaev-data-target-visibility').val()
				];
			}else{
				return null;
			}
		}).get().filter(Boolean);
		// console.log("collect_advance_visibility_data", collect_advance_visibility_data);
		// console.log(collect_advance_visibility_data)
		var stringified_cavd = JSON.stringify(collect_advance_visibility_data);
		var data_prop = $('body').data(""+OID+"");
		data_prop["visible-formula"] = stringified_cavd;
		$('body').data(""+OID+"", data_prop);
		if(collect_advance_visibility_data.length >= 1){
			// obj_on_form_sel.attr("visible-formula","["+collect_advance_visibility_data.join(",")+"]");
			var temp = [];
			for(var ctr = 0 ; ctr < collect_advance_visibility_data.length ; ctr+=2){
				// console.log("BWHAT", ctr, collect_advance_visibility_data[ctr] , collect_advance_visibility_data[ctr+1] );
				temp.push([collect_advance_visibility_data[ctr],collect_advance_visibility_data[ctr+1]]);
			}
			// console.log("VAKITT",temp,collect_advance_visibility_data)
			obj_on_form_sel.attr("visible-formula","[\n"+temp.map(function(va){ return va.join(","); }).join(",\n")+"\n]");
			obj_on_form_sel.attr("field-visible","computed");
		}else{
			obj_on_form_sel.attr("visible-formula","[]");
			obj_on_form_sel.attr("field-visible","");
		}
    }
    if($('[data-properties-type="actionEventVisibilityFormula"]').length >= 1 ){
    	if($('[data-properties-type="actionEventVisibilityFormula"]').val().trim() == ""){
    		$(".getFields_"+OID).attr("data-action-event-visibility","false");
    		$(".getFields_"+OID).attr("data-action-event-visibility-formula",$('[data-properties-type="actionEventVisibilityFormula"]').val());
    		$(".getFields_"+OID).attr("field-visible","computed");
    		// $(".getFields_"+OID).data("visible_formula",{
    		// 	"data_action_event_visibility_formula":$('[data-properties-type="actionEventVisibilityFormula"]').val(),
    		// 	"data-embed-view-visibility_formula":$('[data-properties-type="embedViewVisibility"]').val()
    		// });
    		$(".getFields_"+OID).attr("visible-formula","["+$('[data-properties-type="actionEventVisibilityFormula"]').val()+","+$('[data-properties-type="embedViewVisibility"]').val()+"]");
    	}else{
    		$(".getFields_"+OID).attr("data-action-event-visibility","true");
    		$(".getFields_"+OID).attr("field-visible","computed");
    		// $(".getFields_"+OID).data("visible_formula",{
    		// 	"data_action_event_visibility_formula":$('[data-properties-type="actionEventVisibilityFormula"]').val(),
    		// 	"data-embed-view-visibility_formula":$('[data-properties-type="embedViewVisibility"]').val()
    		// });
    		$(".getFields_"+OID).attr("visible-formula","["+$('[data-properties-type="actionEventVisibilityFormula"]').val()+","+$('[data-properties-type="embedViewVisibility"]').val()+"]");
    	}
    }

    if($('[data-properties-type="embedViewVisibility"]').length >= 1 ){
    	if($('[data-properties-type="embedViewVisibility"]').val().trim() == ""){
    		$(".getFields_"+OID).attr("data-embed-view-visibility","false");
    		$(".getFields_"+OID).attr("data-embed-view-visibility-formula",$('[data-properties-type="embedViewVisibility"]').val());
    		$(".getFields_"+OID).attr("field-visible","computed");
    		// $(".getFields_"+OID).data("visible_formula",{
    		// 	"data_action_event_visibility_formula":$('[data-properties-type="actionEventVisibilityFormula"]').val(),
    		// 	"data-embed-view-visibility_formula":$('[data-properties-type="embedViewVisibility"]').val()
    		// });
    		$(".getFields_"+OID).attr("visible-formula","["+$('[data-properties-type="actionEventVisibilityFormula"]').val()+","+$('[data-properties-type="embedViewVisibility"]').val()+"]");
    	}else{
    		$(".getFields_"+OID).attr("data-embed-view-visibility","true");
    		$(".getFields_"+OID).attr("data-embed-view-visibility-formula",$('[data-properties-type="embedViewVisibility"]').val());
    		$(".getFields_"+OID).attr("field-visible","computed");
    		// $(".getFields_"+OID).data("visible_formula",{
    		// 	"data_action_event_visibility_formula":$('[data-properties-type="actionEventVisibilityFormula"]').val(),
    		// 	"data-embed-view-visibility_formula":$('[data-properties-type="embedViewVisibility"]').val()
    		// });
    		$(".getFields_"+OID).attr("visible-formula","["+$('[data-properties-type="actionEventVisibilityFormula"]').val()+","+$('[data-properties-type="embedViewVisibility"]').val()+"]");
    	}
    }

    if($('.visibility-formula').length >= 1 && $('.chk_Visibility').length >= 1 ){
    	if($(".getFields_"+OID).length >= 1){
    		if($('.visibility-formula').val() != ''){
				$(".getFields_"+OID).attr('field-visible','computed');
				$(".getFields_"+OID).attr('visible-formula',$('.visibility-formula').val());
	    	}
    	}else{
    		if($('.setObject_'+OID+'[data-type="labelOnly"]').length >= 1){
    			if($('.visibility-formula').val() != ''){
					$(".setObject_"+OID).attr('field-visible','computed');
					$(".setObject_"+OID).attr('visible-formula',$('.visibility-formula').val());
		    	}else if($('.visibility-formula').val() == ''){
		    		$(".setObject_"+OID).attr('field-visible','not-computed');
					$(".setObject_"+OID).attr('visible-formula',$('.visibility-formula').val());
		    	}
    		}
    	}
    }

	if($('.embedRoutingField_container').length >= 1 ){
		var embedRoutingField_container = $('.embedRoutingField_container');
		$(".getFields_"+OID).attr("erf-route-popup-form-fields",embedRoutingField_container.find('.erf-route-popup-form-fields').val());
		$(".getFields_"+OID).attr("erf-route-my-form-fields",embedRoutingField_container.find('.erf-route-my-form-fields').val());
	}

	if($('[data-properties-type="defaultValue"]').length >= 1 ){
		var radio_check = $('[data-properties-type="defaultValueType"]:checked').val();
		if(radio_check == "computed"){
			$(".getFields_"+OID).attr("default-formula-value",$('[data-properties-type="defaultValue"]').val());
			$(".getFields_"+OID).attr("default-type", "computed");
		}else if(radio_check == "middleware"){
			$(".getFields_"+OID).attr("default-middleware-value",$('[data-properties-type="defaultValue"]').val());
			$(".getFields_"+OID).attr("default-type", "middleware");
			var middlewareTag = $('[data-properties-type="middlewareTagging"]').val();
			$(".getFields_"+OID).attr("middlewareTagging", middlewareTag);
		}else if(radio_check == "OnSubmit"){
			$(".getFields_"+OID).attr("default-onsubmit-value",$('[data-properties-type="defaultValue"]').val());
			$(".getFields_"+OID).attr("default-type", "OnSubmit");
		}else {
			$(".getFields_"+OID).attr("default-static-value",$('[data-properties-type="defaultValue"]').val());
			$(".getFields_"+OID).attr("default-type", "static");
		}
		
		if(typeof json[''+OID] != "undefined"){
			json[''+OID]["defaultValue"] = $('[data-properties-type="defaultValue"]').val();
			json['' + OID + '']["defaultValueType"] = radio_check;
		}else{
			json[''+OID] = {};
			json[''+OID]["defaultValue"] = $('[data-properties-type="defaultValue"]').val();
			json['' + OID + '']["defaultValueType"] = radio_check;
		}
	}

	if($('.encryptDataProperty').length >= 1 ){
		var encrypt_type_ele_field = $('[data-properties-type="ecryptDataPropField"]');
		$(".getFields_"+OID).attr("encryption-type",encrypt_type_ele_field.val());
	}

	if($('.numberToWords').length >= 1 ){
		var num_to_words_ele_field = $('[data-properties-type="numberToWordsPropField"]');
		$(".getFields_"+OID).attr("num-to-words",num_to_words_ele_field.val());
	}

console.timeEnd("Save")

	//CLEAR PROPERTY UI'S
	if($(".mentionSearchChoice").length >= 1){
		$(".mentionSearchChoice").html("");
	}

	{
		// 	if($('[data-properties-type="defaultValue"]').length == 1 && $('[data-properties-type="defaultValueType"][value="computed"]:checked').length == 1){
		// 		var collect_message_error = "";
		// 		var default_val_ele = $('[data-properties-type="defaultValue"]');
		// 		var default_val_formula = default_val_ele.val();
		// 		var process_val_formula = default_val_formula;
		// 		var atfnmatches = default_val_formula.match(/@[A-Za-z][0-9a-zA-Z_]*/g);
		// 		if(atfnmatches != null){
		// 			atfnmatches = atfnmatches.filter(Boolean);
		// 			atfnmatches = atfnmatches.filter(function(value, index){
		// 				if(function_keyword_names.indexOf(value) >= 0){
		// 					return false;
		// 				}else{
		// 					return true;
		// 				}
		// 			});
		// 			var reg_eks = "";
		// 			atfnmatches = $.unique(atfnmatches);

		// 			$.each(atfnmatches,function(i, v){
		// 				reg_eks = new RegExp("\""+v+"\"|"+v+"(?![A-Za-z][0-9a-zA-Z_]*)","g");
		// 				process_val_formula = process_val_formula.replace(reg_eks, "\""+v+"\"")
		// 				if($('[name="'+(v.replace(/@/g,""))+'"]').length <= 0 ){
		// 					collect_message_error += "\n"+v+" does not exist!";
		// 					property_errors++;
		// 				}else if($('[name="'+(v.replace(/@/g,""))+'"]').length >= 2){
		// 					collect_message_error += "\n"+v+" has an identical name!";
		// 					property_errors++;
		// 				}
		// 			});
		// 			if(property_errors >=1){
		// 				showNotification({
		//                     message: collect_message_error+" on default value property",
		//                     type: "error",
		//                     autoClose: true,
		//                     duration: 3
		//                 });
		//                 return;
		// 			}

		// 			$.each(function_keyword_names,function(i, v){
		// 				reg_eks = new RegExp(v+"(?![A-Za-z][0-9a-zA-Z_]*)","g");
		// 				process_val_formula = process_val_formula.replace(reg_eks, v.replace(/@/g,""))
		// 			});

		// 			try{
		// 				eval(process_val_formula);
		// 			}catch(error){
		// 				collect_message_error += "\nSyntax error: "+error.message;
		// 				property_errors++;
		// 			}
		// 			if(property_errors >=1){
		// 				showNotification({
		//                     message: collect_message_error+" on default value property",
		//                     type: "error",
		//                     autoClose: true,
		//                     duration: 3
		//                 });
		//                 return;
		// 			}
		// 		}
		// 	}
	}
	
	//LABEL HYPERLINK

	{
		if($("#setObject_"+OID).attr("data-type")=="labelOnly"){
			var encrypt_type_ele_field = $('[data-properties-type="lblHyperLink"]');
			var lblText = $("#setObject_"+OID).find(".getFields_"+OID).text();
			var hyperlinkAddress = encrypt_type_ele_field.val().trim();
			

			if(hyperlinkAddress.length==0){
				//valid without hyperlink
				$("#setObject_"+OID).find(".getFields_"+OID).text(lblText);
			}else if(hyperlinkAddress.length>=1 && hyperlinkAddress.length<=5){
				//hyperlink to short
				showNotification({
                    message: "Link Address is too short",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                property_errors++;
			}
			else if (CheckUrl(hyperlinkAddress)) {
				
			
				var hyperlink = '<a href="'+ hyperlinkAddress +'" style="text-decoration: underline;">'+ lblText +'</a>';
				$("#setObject_"+OID).find(".getFields_"+OID).html(hyperlink);
		
			}
			
			else{

				if($('[data-properties-type="defaultHyperLinkValue"]:checked').val()!="computed"){
					showNotification({
						message: "Not a valid link address",
						type: "error",
						autoClose: true,
						duration: 3
					});
					 property_errors++;
				}
				
				
			}
		}
	}

	{ //embed data sending
		if($('.embedPopupDataSending_container').length >= 1){
			//
			var collect_json_data_send = [];
			var embedPopupDataSending_container = $('.embedPopupDataSending_container')
			
			var enable_embedPopupDataSending = $('.enable_embedPopupDataSending').eq(0);
			embedPopupDataSending_container.find(".epds-tr-group").each(function(){
				collect_json_data_send.push({});
				var epds_tr_group = $(this);
				
				var epds_my_form_side_chk = epds_tr_group.find('[data-name="epds-my-form-side-chk"]');
				var epds_popup_form_side_chk = epds_tr_group.find('[data-name="epds-popup-form-side-chk"]');

				var epds_select_my_form_field_side = epds_tr_group.find('[data-name="epds-select-my-form-field-side"]');
				var epds_formula_my_form_field_side = epds_tr_group.find('[data-name="epds-formula-my-form-field-side"]');

				var epds_select_popup_form_field_side = epds_tr_group.find('[data-name="epds-select-popup-form-field-side"]');
				var epds_formula_popup_form_field_side = epds_tr_group.find('[data-name="epds-formula-popup-form-field-side"]');

				collect_json_data_send[collect_json_data_send.length-1]["enable_embedPopupDataSending"] = enable_embedPopupDataSending.prop("checked");
				collect_json_data_send[collect_json_data_send.length-1]["epds_my_form_side_chk"] = epds_my_form_side_chk.filter(":checked").val();
				collect_json_data_send[collect_json_data_send.length-1]["epds_popup_form_side_chk"] = epds_popup_form_side_chk.filter(":checked").val();

				collect_json_data_send[collect_json_data_send.length-1]["epds_select_my_form_field_side"] = epds_select_my_form_field_side.val();
				collect_json_data_send[collect_json_data_send.length-1]["epds_formula_my_form_field_side"] = epds_formula_my_form_field_side.val();

				collect_json_data_send[collect_json_data_send.length-1]["epds_select_popup_form_field_side"] = epds_select_popup_form_field_side.val();
				collect_json_data_send[collect_json_data_send.length-1]["epds_formula_popup_form_field_side"] = epds_formula_popup_form_field_side.val();
			})
			var stringified_data_send = JSON.stringify(collect_json_data_send);
			$(".getFields_"+OID).attr("stringified-json-data-send",stringified_data_send);
		}
	}
	if($('.EmbedSetColumnResult_container').length >= 1 ){//embed column total result
		if($('[name="sourceformtype"]:checked').val() != "multiple"){
			var collect_set_col_total_res = [];
			var embedded_view_display_columns = $('.embedded-view-columns');
			var embedded_view_display_columns_fields = embedded_view_display_columns.find('tbody').eq(0).children('tr').map(function(){
				var dis_self = $(this);
				return {
					"column_name_eq":dis_self.children('td').eq(0).children('input.form-text').val(),
					"field_name_eq":dis_self.children('td').eq(1).text(),
					"ele_field":dis_self.children('td').eq(0).children('input.form-text')
				};
			});
			// console.log("CRAAAAZY MAN DUDE xD", embedded_view_display_columns_fields)
			$('.EmbedSetColumnResult_container').find('.escr-tr-group').each(function(){
				var dis_self = $(this);
				var escr_data_embed_column_field_val = dis_self.find('.escr-data-embed-column-field').val();
				var escr_data_embed_computation_field_val = dis_self.find('.escr-data-embed-computation-field').val();
				//var escr_data_embed_column_field_val = dis_self.find('.escr-formula-based-col-comp').val();
				var escr_data_result_output_field_val = dis_self.find('.escr-data-result-output-field').val();
				if(escr_data_embed_column_field_val){
					if(escr_data_embed_column_field_val.length >= 1){
						var match_to_empty_column_data = embedded_view_display_columns_fields.filter(function(index_ctr, value_data){
							// console.log("BROOM",escr_data_embed_column_field_val," == ",value_data["field_name_eq"]," ~~ ", $.type(value_data["column_name_eq"]), " ::: ", value_data["ele_field"])
							return (escr_data_embed_column_field_val == value_data["field_name_eq"] && $.trim(value_data["column_name_eq"]).length <= 0);
						});
						if( match_to_empty_column_data.length >= 1 ){
							// console.log("KULANGOT",match_to_empty_column_data[0]["ele_field"]);
							NotifyMe(match_to_empty_column_data[0]["ele_field"], "This field is required for the column computation. Please fill this up.").css({
								"margin-top": "-30px",
								"top":""
							});
							property_errors ++;
							// .css("top","").css("margin-top",-(embedded_view_display_columns_ele_input.outerHeight())+"px")
						}else{
							collect_set_col_total_res.push({
								"escr_embed_fs_col_data":escr_data_embed_column_field_val,
								"escr_embed_computation_field":escr_data_embed_computation_field_val,
								"escr_embed_output_field":escr_data_result_output_field_val
							});
						}
					}
				}
			});
			$(".getFields_"+OID).attr("data-output-col-total",JSON.stringify(collect_set_col_total_res));
		}
	}

	if($('[data-properties-type="defaultValueType"]:checked').val() == "computed" ||  $('[data-properties-type="defaultValueType"]:checked').val() == "middleware"){
		if($(".getFields_"+OID).length >= 1){
			var my_formula_stat = client_formula.checkFormulaErrors($('[data-properties-type="defaultValue"]').val());//$(".getFields_"+OID)
			if(my_formula_stat["status"] == false){
				NotifyMe($('[data-properties-type="defaultValue"]'), my_formula_stat["message"]).css("min-width","100px");
				property_errors ++;
			}
		}
	}
	if ($('[data-properties-type="setReadOnlyField"]').length >= 1) {
		var selector_ng_readonly = $('[data-properties-type="setReadOnlyField"]');
		 var the_element = $("#setObject_" + OID);
		 if (selector_ng_readonly.val() == "Yes") {
                        the_element.find(".getFields_" + OID).attr("readonly", "readonly");
                        //if (the_element.find(".getFields_" + OID).is("input[type='checkbox']")) {
                        //    the_element.find(".getFields_" + OID).attr("onclick","return false");
                        //}
                        //if (the_element.find(".getFields_" + OID).is("input[type='radio']")) {
                        //    
                        //    //the_element.find(".getFields_"+OID).attr("onclick","(this.checked)?true:false;");
                        //    //the_element.children('.fields_below').eq(0).prepend("<div class='readonly-overlay' style='width:100%;height:100%;background-color:rgba(0,0,0,0)'></div>");
                        //    the_element.children('.fields_below').children(".input_position_below").find(".getFields_" + OID).filter('input[type="radio"]:not(:checked)').attr('disabled',true);
                        //}
                       if (the_element.find(".getFields_" + OID).is("select")) {
                             //the_element.children('.fields_below').eq(0).prepend("<div class='readonly-overlay' style='width:100%;height:100%;background-color:rgba(0,0,0,0)'></div>");
                            the_element.children('.fields_below').children(".input_position_below").find(".getFields_" + OID).children('option:not(:selected)').prop('disabled', true);
                          
                            //the_element.find(".obj_fields_"+OID).prepend("<div class='readonly-overlay' style='width:100%;height:100%;background-color:rgba(0,0,0,0)'></div>");
                          
                        }
                        
                            
                    } else {
                        the_element.children('.fields_below').children(".input_position_below").find(".getFields_" + OID).children('option').prop('disabled', false);
                        the_element.children('.fields_below').children(".input_position_below").find(".getFields_" + OID).filter('input[type="radio"]:not(:checked)').prop('disabled',false);
			
			//the_element.find(".getFields_" + OID).removeAttr("readonly");
                        //the_element.find(".getFields_" + OID).removeAttr("onclick");
                    }
	}
	
	
	if ($('[data-properties-type="lblName"]').length>=1) {
		var lblname_checker = $.trim($('[data-properties-type="lblName"]').val());
		var lblname_ele = $('[data-properties-type="lblName"]');
		var tooltip_position = lblname_ele.parents(".input_position_below").find('font:eq(0)');
		if (lblname_checker=="") {
			
			var tooltip_top = NotifyMe(lblname_ele, "Field Required").css("min-width","100px");
			var scroll_content_dialog = tooltip_top.parents('.content-dialog-scroll').eq(0);
			console.log("tooltip_top" , tooltip_top)
			// tooltip_top.css("top").replace("px","");
			var top_tooltip = tooltip_position.position().top - 10;
			
			var left_tooltip = tooltip_position.position().left + 10;
			
			tooltip_top.css("top",(top_tooltip+scroll_content_dialog.scrollTop())+"px");
			tooltip_top.css("left",left_tooltip+"px");
			// console.log("error to oh",OID)
			$('[data-properties-type="lblName"]').focus()
			property_errors++;

			
		}
	}
	if($('[data-properties-type="lblFldName"]').length>=1){
		var lblfieldname_checker = $.trim($('[data-properties-type="lblFldName"]').val());
		var lblfldname_ele = $('[data-properties-type="lblFldName"]');
		var tooltip_position = lblfldname_ele.parents('.fields_below').find("font"); //children('.label_below2')
		var special_character_checker = /^[a-zA-Z0-9_\s]*$/;
		
		if (lblfieldname_checker=="") {
			
			var tooltip_top = NotifyMe(lblfldname_ele, "Field Required").css("min-width","100px");
			
			var top_tooltip = tooltip_position.position().top;
			
			var left_tooltip = tooltip_position.position().left;
			
			tooltip_top.css("top",(top_tooltip-7)+"px");
			tooltip_top.css("left",left_tooltip+"px");
			$('[data-properties-type="lblFldName"]').focus()
			property_errors++;

		}
		else if(!special_character_checker.test(lblfieldname_checker)){
			var tooltip_top = NotifyMe(lblfldname_ele, "Special characters not allowed").css("min-width","100px");
			
			var top_tooltip = tooltip_position.position().top;
			
			var left_tooltip = tooltip_position.position().left;
			
			tooltip_top.css("top",(top_tooltip-7)+"px");
			tooltip_top.css("left",left_tooltip+"px");
			$('[data-properties-type="lblFldName"]').focus()
			property_errors++;
		}

	}
	
	if($('.new-embed-source-form-data-filtering').length >= 1){
		
		var save_json_new_embed_source = SaveEmbedSourceAndFiltering();
		var stringify_embed_source = JSON.stringify(SaveEmbedSourceAndFiltering());
		$(".getFields_"+OID).attr('data-new-embed-source',stringify_embed_source);
	}


	if($('.embed_conditional_operator').length >= 1){
		$(".getFields_"+OID).attr('embed-conditional-operator',$('.embed-conditional-operator').val());
	}

	if($('.embed_additional_filter_formula').length >= 1){
		$(".getFields_"+OID).attr('embed-additional-filter-formula',$('.embed-additional-filter-formula').val());
	}


	if($('[data-properties-type="tabIndex"]').length >= 1){
		var tabindex_ele = $('[data-properties-type="tabIndex"]');
		$('.formbuilder_ws').find('[tabindex]:not([id="getFields_'+OID+'"])').each(function(){

			var tabindex = $(this).attr('tabindex');
			

			if(tabindex == tabindex_ele.val() && tabindex_ele.val() !=""){

				var tooltip_position = tabindex_ele.parents('.fields_below').children('.label_below2').children("font");
 				var tooltip_top = NotifyMe(tabindex_ele, "Duplicate Index Number","tabindex").css("min-width","100px");
				tooltip_top.parent().css('position','relative');
				tooltip_top.css({
					'top':"-30px",
					'left':"110px"
				});
				$('[data-properties-type="tabIndex"]').focus()
				property_errors++;
				
			}

			// else if(tabindex_ele.val().length>=1 && !$.isNumeric(tabindex_ele.val())){

			// 	var tooltip_position = tabindex_ele.parents('.fields_below').children('.label_below2').children("font");
 		// 		var tooltip_top = NotifyMe(tabindex_ele, "Invalid Input","tabindex").css("min-width","100px");
			// 	tooltip_top.parent().css('position','relative');
			// 	tooltip_top.css({
			// 		'top':"-30px",
			// 		'left':"110px"
			// 	});

			// 	property_errors++;
			// }
		});
		
	}
	//added carlo 03/03/2015
	if($('[data-type="radioButton"].obj_defaultValueChoices').length >=1){
		var radio_values = [];
		$('[data-type="radioButton"].obj_defaultValueChoices').find('.radio-input-label').each(function(){
			radio_values.push($(this).html());
		});
		json[''+OID]["radioValues"] = radio_values;
	}
	if($('[data-properties-type="defaultHyperLinkValue"]').length>=1){

		var radio_check = $('[data-properties-type="defaultHyperLinkValue"]:checked').val();
		if(radio_check == "computed"){
			$(".getFields_"+OID).attr("default-hyperlink-formula-value",$('[data-properties-type="lblHyperLink"]').val());
			$(".getFields_"+OID).attr("default-hyperlink-type", "computed");
		}else {
			$(".getFields_"+OID).attr("default-hyperlink-static-value",$('[data-properties-type="lblHyperLink"]').val());
			$(".getFields_"+OID).attr("default-hyperlink-type", "static");
		}
		
		if(typeof json[''+OID] != "undefined"){
			json[''+OID]["lblHyperLink"] = $('[data-properties-type="lblHyperLink"]').val();
			json['' + OID + '']["defaultHyperLinkValue"] = radio_check;
		}else{
			json[''+OID] = {};
			json[''+OID]["lblHyperLink"] = $('[data-properties-type="lblHyperLink"]').val();
			json['' + OID + '']["defaultHyperLinkValue"] = radio_check;
		}
		
	}
	//added by japhet morada add properties for embedded view if the child form is related to parent form
	if($('[data-properties-type="commit_data_row_event"]')){
		var embed_commit_type = $('[data-properties-type="commit_data_row_event"]').val();
		$(".getFields_"+OID).attr("commit-data-row-event", embed_commit_type);
	}
	if($('.linkmakerClass').length >= 1){
		
		saveDetailsPanelData(OID);
	}

	if($('.erf-route-my-form-fields,.erf-route-popup-form-fields').length >= 2  ){
		if($('[name="sourceformtype"]:checked').val() != "multiple"){
			var sriA = $('.erf-route-my-form-fields').val();
			var sriB = $('.erf-route-popup-form-fields').val();
			if(sriA == "--Select--" || sriB == "--Select--"){
				var tooltip_top = NotifyMe($('.erf-route-my-form-fields,.erf-route-popup-form-fields').eq(0), "Invalid sub request identifier","sub-request-identifier").css("min-width","100px");
				tooltip_top.parent().css('position','relative');
				tooltip_top.css({
					'top':"-30px",
					'left':"110px"
				});

				property_errors++;
			}
		}
	}
	if($('.embed-dialog-type').length >= 1){
		$(".getFields_"+OID).attr('embed-dialog-type', $('.embed-dialog-type').val() );
		if(typeof json[''+OID] != "undefined"){
			json[''+OID]["embedDialogType"] = $('.embed-dialog-type').val();
			var embed_prop = {};
			embed_prop.width = $('[data-properties-type="embed-popup-window-height]').val()||"300";
			embed_prop.height = $('[data-properties-type="embed-popup-window-height]').val()||"300";
			json[''+OID]['embed_popup_window_properties'] = embed_prop;
		}else{
			json[''+OID] = {};
			json[''+OID]["embedDialogType"] = $('.embed-dialog-type').val();
			var embed_prop = {};
			embed_prop.width = $('[data-properties-type="embed-popup-window-height]').val()||"300";
			embed_prop.height = $('[data-properties-type="embed-popup-window-height]').val()||"300";
			json[''+OID]['embed_popup_window_properties'] = embed_prop;
		}
	}
	if(property_errors>0){
		(function DetectPropErrors(){

			// console.log("errors",$('#popup_container').find('.content-dialog-scroll.ps-container').eq(0).find('.NM-notification'))
			var nm_ele_top = $('#popup_container').find('.content-dialog-scroll.ps-container').eq(0).find('.NM-notification').sort(function(ele_a, ele_b){
				console.log("errors",ele_a,ele_b)
				$(ele_a).show();
				$(ele_b).show();
				var av = $(ele_a).offset().top;
				var bv = $(ele_b).offset().top;
				$(ele_a).hide();
				$(ele_b).hide();
				return av - bv;
			});
			var nm_ele_top_dom = nm_ele_top;
			console.log(prop_containment_ele)
			nm_ele_top_dom.show();
			nm_ele_top = nm_ele_top_dom.eq(0).offset().top;
			nm_ele_top_dom.hide();
			var prop_containment_ele = $('#popup_container').find('.content-dialog-scroll.ps-container').eq(0);
			var minus_scope_container_off_top = prop_containment_ele.offset().top;
			nm_ele_top = nm_ele_top - minus_scope_container_off_top;
			var scrolled_top = prop_containment_ele.scrollTop();
			// prop_containment_ele.scrollTop(scrolled_top + nm_ele_top);
			prop_containment_ele.animate({ scrollTop: (scrolled_top + nm_ele_top) }, 800);
		})();
		return;
	}

	// var elements = $(".getFields_"+OID).closest(".setObject");
	var elements = $('.setObject');
	// console.log("ELE FROM OK BUTTON PROP", elements);
	var objID = elements.attr('data-object-id');
    var setObjIdTag = $('#setObject_'+elements.attr('data-object-id'));
    var labelTag = $('#label_'+elements.attr('data-object-id'));
    var fieldTag = $('#getFields_'+elements.attr('data-object-id'));

    //added by japhet morada 05-03-2016
    //get the sorted columns for form datatable

    if($('.form-data-table-display-column').length > 0){

    	var self = $('.form-data-table-display-column');
    	var current_element = $('#setObject_' + OID).find('.getFields');
    	var sorted_column = [];
    	var all_columns = [];
    	self.find('tr').each(function(index){
    		if(index > 0){
	    		var self = $(this);
	    		var json_values = {};
	    		var json_values_all_columns = {};
	    		if(self.find('td input[type="text"]').eq(0).val() != ""){
	    			json_values['FieldLabel'] = self.find('td input[type="text"]').eq(0).val();
	    			json_values['FieldName'] = self.find('td div').eq(0).text();
	    			sorted_column.push(json_values);
	    		}
	    		json_values_all_columns['FieldLabel'] = self.find('td div').eq(0).text();
	    		json_values_all_columns['FieldName'] = self.find('td div').eq(0).text();
	    		all_columns.push(json_values_all_columns);
	    	}
    	})
		current_element.attr('display-column', JSON.stringify(sorted_column));
    }

	objectCSSProperties(objID, setObjIdTag, labelTag, fieldTag, elements);
	
	{
		//added by Pao, Change Span of Object's Layer list..
		if($('#setObject_'+OID).is('[data-type="imageOnly"],[data-type="accordion"],[data-type="labelOnly"],[data-type="table"],[data-type="createLine"],[data-type="tab-panel"],[data-type="button"]')== true){
		// console.log("EEE")
			if($('#setObject_'+OID).is('[data-upload="photo_upload"]')==true){
				$('#list_'+OID).text($('#setObject_'+OID).find('input[type="text"]').attr('name').replace(/[^a-zA-Z_0-9-]/g, ""));
			}
			if($('#setObject_'+OID).is('[data-type="accordion"],[data-type="createLine"],[data-type="table"],[data-type="tab-panel"],[data-type="labelOnly"]') ==true){
			$('#list_'+OID).text($('#setObject_'+OID).find('label').eq(0).html().replace(/[^a-zA-Z_0-9-]/g, ""));
			}
		}
		else if($('#setObject_'+OID).is('[data-type="embeded-view"]') == true){
		var obj_getfield_name=$('.getFields_'+OID).attr('embed-name').replace(/[^a-zA-Z_0-9-]/g, "");
		var obj_layer_list='#list_'+OID;
		$(obj_layer_list).text(obj_getfield_name);
		}
		else if($('#setObject_'+OID).is('[data-type="formDataTable"') == true){//added by japhet for new embed object 05-02-2016
			var obj_getfield_name=$('.getFields_'+OID).attr('form-data-table-name').replace(/[^a-zA-Z_0-9-]/g, "");
			var obj_layer_list='#list_'+OID;
			$(obj_layer_list).text(obj_getfield_name);
		}
		else{
		// var obj_getfield_name=$('#getFields_'+OID).attr('name').replace(/[^a-zA-Z_0-9-]/g, "");
		// var obj_layer_list='#list_'+OID;
		// console.log(obj_getfield_name)
		// console.log(obj_layer_list)
		$('#list_'+OID).text($('#getFields_'+OID).attr('name').replace(/[^a-zA-Z_0-9-]/g, ""));
		}
		// console.log(OID)
		// console.log(samid)
		// console.log(objID)
		// console.log(labelTag)
		// console.log(fieldTag)
		// console.log(setObjIdTag)
		// console.log(samname+' '+samid);
		
	}
	{
		//image property backbground
		if( $('[data-properties-type="imgBckground"]').length >= 1){
			var img_height = $('.formbuilder_ws').height();
			var img_width = $('.formbuilder_ws').width();
			var img_src_bckground =$('#getFields_'+OID).attr("src")
			function image_reset_prop(){
						$('#getFields_'+OID).removeAttr("max-height").removeAttr("maxlength");
						$('#setObject_'+OID).find('.uiform-image-resizable').removeAttr("style");
						$('#setObject_'+OID).css({
							"height":"auto",
							"width":"auto",
							"float":"none",
						})
						$('#setObject_'+OID).find('.uiform-image-resizable').css({
							"height":"100px",
							"width":"100px"
						}).children(".ui-resizable-handle").show()
						$('#getFields_'+OID).css({
							"width":"100%",
							"height":"100%",
						})
			}
			if($('[data-properties-type="imgBckground"]').val() == '1' ){
						image_reset_prop();				
						$('#setObject_'+OID).removeClass("workspace_bckgrnd")
			}
			else if(($('[data-properties-type="imgBckground"]').val() == '2' )){
						image_reset_prop();
						$($('#setObject_'+OID)).appendTo(".workspace");
						if($('#setObject_'+OID).hasClass("workspace_bckgrnd")){

						}
						else{
							$('#setObject_'+OID).addClass("workspace_bckgrnd")							
						}
						$('#setObject_'+OID).css({
							"top":"0px",
							"left":"0px",
							"z-index":"0"
						})
						$('#setObject_'+OID).find('.uiform-image-resizable').css({
							"height":img_height,
							"width":img_width
						}).children(".ui-resizable-handle").hide()
						$('#getFields_'+OID).css({
							"width":"auto",
							"height":"auto",
							"max-height":"100%",
							"max-width":"100%"
						})
			}
			else if(($('[data-properties-type="imgBckground"]').val() == '3' )){
						image_reset_prop();
						$($('#setObject_'+OID)).appendTo(".workspace");
						if($('#setObject_'+OID).hasClass("workspace_bckgrnd")){

						}
						else{
							$('#setObject_'+OID).addClass("workspace_bckgrnd")							
						}
						$('#setObject_'+OID).css({
							"top":"0px",
							"left":"0px",
							"z-index":"0"
						})
						$('#setObject_'+OID).find('.uiform-image-resizable').css({
							"height":img_height,
							"width":img_width
						}).children(".ui-resizable-handle").hide()
						
						
			}
			else if(($('[data-properties-type="imgBckground"]').val() == '4' )){
						image_reset_prop();
						$($('#setObject_'+OID)).appendTo(".workspace");
						if($('#setObject_'+OID).hasClass("workspace_bckgrnd")){

						}
						else{
							$('#setObject_'+OID).addClass("workspace_bckgrnd")							
						}						
						$('#setObject_'+OID).css({
							"top":"0px",
							"left":"0px",
							"z-index":"0"
						})
						$('#setObject_'+OID).find('.uiform-image-resizable').css({
							"height":img_height,
							"width":img_width,
							"background": 'url('+ img_src_bckground +')'
						}).children(".ui-resizable-handle").hide()
						$('#getFields_'+OID).css({
							"height":"auto",
							"width":"auto"
						})

			}

		}
	}
	if($('[name="sourceformtype"]').length > 0){
		if($('[name="sourceformtype"]:checked').val() == "multiple"){
			saveMultipleFormProperties(OID);
		}
	}
	if($('.embedCustomActions').length > 0){
		if($('[data-properties-type="EmbedViewCustomAction"]').prop("checked") == true){
			saveCustomActions(OID);
		}
	}
	if($('.embed_row_category_prop').length > 0){
		if($('.embed_row_category_prop').prop("checked") == true){
			$('body').data(''+OID)['allow_row_category_column'] = $('.embed_row_category_column_dropdown').val();
		}
	}
	// console.log($("body").data());
	if($(".fl-closeDialog").length >= 1){
		//$(".fl-closeDialog").trigger("click");
		$('#popup_container').fadeOut(function(){
			$(this).remove();
		});
		$('#popup_overlay').fadeOut(function(){
			$(this).remove();
		});
		
	}else{
		$("#popup_cancel").trigger("click");
	}
	dropdown_reset();

}


var function_keyword_names = [
    '@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
    '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
    '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
    '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
    '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
    '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now',
    '@GetAuth', "@FormatDate","@NumToWord",
    "@Head","@AssistantHead","@GetData",
    "@LookupAuthInfo",
    "@GetAVG","@GetMin","@SDev","@GetMax",
    "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
    "@StrFind","@StrReplace", "@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
    "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource","@GetLocation",
    "@GetMonth","@GetFileName","@CustomScript", "@ArraySearchBy","@LookupExternalWhere"
];








function MakePlusPlusRow(param_option_settings){

	var option_settings = {
		"tableElement":"",
		"selectorElePlus":"",
		"selectorEleMinus":"",
		"selectorToDuplicate":"",
	}
	$.extend(option_settings, param_option_settings)
	
	var mppr_event = {
		"eventRowAdd":function(){
			var opt_set = option_settings;

			var parent_to_duplicate = opt_set["selectorToDuplicate"];
			var element_minus = opt_set["selectorEleMinus"];
			var dis_ele_click = $(this);//the plus
			var cloned_to_duplicate = dis_ele_click.parents(parent_to_duplicate).eq(0).clone(true);
			dis_ele_click.parents(parent_to_duplicate).after(cloned_to_duplicate);
			cloned_to_duplicate.find(element_minus).show();
		},
		"eventRowSubtract":function(){
			var opt_set = option_settings;

			var parent_to_duplicate = opt_set["selectorToDuplicate"];
			var dis_ele_click = $(this);//the minus
			dis_ele_click.parents(parent_to_duplicate).remove();
		}
	}

	option_settings["init"] = function(){
		var self = this;
		var table_container = $(self["tableElement"]);
		var table_container_ele_plus = table_container.find(self["selectorElePlus"]);
		var table_container_ele_minus = table_container.find(self["selectorEleMinus"]);
		table_container_ele_plus.on("click",mppr_event["eventRowAdd"]);
		table_container_ele_minus.on("click",mppr_event["eventRowSubtract"]);
	}
	
	option_settings.init();
}








function FindContentNode(container_ele){
	return $(container_ele).contents().map(function(){
		if(this.childNodes.length >= 1){
			return FindContentNode($(this));
		}else {
			return this;
		}
	}).get().filter(Boolean);
}
// var node_texts = FindContentNode($('[contenteditable="true"]'));
// node_texts;