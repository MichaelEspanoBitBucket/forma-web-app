// var serverside_formulas = [
//     '@Lookup(?![a-zA-Z0-9])',
//     '@LookupCountIf(?![a-zA-Z0-9])',
//     '@LookupWhere(?![a-zA-Z0-9])',
//     '@LookupWhereArray(?![a-zA-Z0-9])',
//     '@DataSource(?![a-zA-Z0-9])',
//     '@UserInfo(?![a-zA-Z0-9])',
//     '@Head(?![a-zA-Z0-9])',
//     '@AssistantHead(?![a-zA-Z0-9])',
//     '@LookupGetMax(?![a-zA-Z0-9])',
//     '@LookupGetMin(?![a-zA-Z0-9])',
//     '@LookupSDev(?![a-zA-Z0-9])',
//     '@LookupSDev2(?![a-zA-Z0-9])',
//     '@LookupAVG(?![a-zA-Z0-9])',
//     '@LookupCountIf(?![a-zA-Z0-9])',
//     '@LookupAuthInfo(?![a-zA-Z0-9])',
//     '@GetAuth(?![a-zA-Z0-9])',
//     '@Total(?![a-zA-Z0-9])',
//     '@RecordCount(?![a-zA-Z0-9])'
// ]

// //DataSource
// var rE = new RegExp(serverside_formulas.join("|"),"g");


// $('[default-type="computed"]:not([default-formula-value=""])').filter(function(){
//     if( (($(this).attr("default-formula-value")||"").match(rE)||[]).length >= 1 ){
//         return true;
//     }
//     return false;
// });

var old_object_dom;
var wpcfb = 38;
var existing_fnames = {};
// Modified 11:58 AM 10/19/2015 formbuilder.js, accordion.js
// --remove accordion settings here ... moved to accordion.js

var default_spectrum_settings = {
    allowEmpty: true,
    showAlpha: true,
    showInput: true,
    className: "full-spectrum",
    showInitial: true,
    showPalette: true,
    showSelectionPalette: true,
    maxPaletteSize: 10,
    preferredFormat: "rgb",
    localStorageKey: "spectrum-text-color",
    // move: function (color) {

    // },
    hide: function(color) {
        var self = $(this);
        var object_id = self.parents('[data-object-id]:eq(0)').attr('data-object-id')||'';
        var revert_color = self.next().find('.sp-preview-inner').css("background-color");
        $("#lbl_" + object_id).css("color", revert_color);
        self.val(revert_color); //FS#8106 - para pag nagrevert dapat pati ung mismong value ng input ganun din
        var data_properties_type = self.attr("data-properties-type");
        var json = $("body").data();
        if (json['' + object_id + '']) {
            var prop_json = json['' + object_id + ''];
        } else {
            var prop_json = {};
        }
        prop_json['' + data_properties_type + ''] = revert_color;
        json['' + object_id + ''] = prop_json;
        $("body").data(json);
    },
   show: function () {
       var self = $(this);
       var selfNextTop = $('.sp-container:visible').position().top;
       var selfNextLeft = $('.sp-container:visible').position().left;
       var docScollTop = $(document).scrollTop();
       var docScollLeft = $(document).scrollLeft();
       $('.sp-container:visible').css({'position':'fixed', 'top':(selfNextTop-docScollTop)+'px', 'left':(selfNextLeft-docScollLeft)+'px' });
     
    },
    // beforeShow: function () {

    // },
    // hide: function () {

    // },
    // change: function() {
    //     alert(423)
    // },
    palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
            "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
            "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
            "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
            "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
            "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
            "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
            "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
            "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
            "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
            "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
            "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    ]
};

function setFormbuilderMinSize(){
    var setObjCompilation = $('.formbuilder_ws').children('.setObject');
    var leftArray = [];
    var topArray = [];
    setObjCompilation.each(function(){
        var eleLeft = $(this).position().left;
        var eleWidth = $(this).outerWidth();    
        var eleTop = $(this).position().top;
        var eleHeight = $(this).outerHeight();
        var leftTotal = eleLeft + eleWidth;
        var topTotal = eleTop + eleHeight;
        leftArray.push(leftTotal);
        topArray.push(topTotal);
    });
    leftArray.sort(function(a, b){return b-a});
    topArray.sort(function(a, b){return b-a});

    $('.formbuilder_ws').css({
        "min-height":topArray[0],
        "min-width":leftArray[0]
    });
    
}

function addPickListEntry(sourceFormId) {
    var form_id = sourceFormId;

    var dis_ele = $(this);
    var str_link = "/user_view/workspace?view_type=request&formID=" + form_id + "&requestID=0";
    console.log('frame', str_link)
    var my_dialog = $(
            '<div class="picklist-iframe" style="height: 100%;">' +
            '<iframe style="height:100%;width:100%;" src="' + str_link + '">' +
            '</iframe>' +
            '</div>'
            );
    var newHeight = $(window).outerHeight() - 50;
    var newWidth = $(window).outerWidth() - 50;

    var ret = "";
    ret += '<div class="picklistoverlay">';
    ret += '<div class="picklistmiddler">';
    ret += '<div id="' + form_id + '" class="picklistpos">';
    ret += '<div class="idehead">';//'+ideID+'

    ret += '<i class="picklistclose fa fa-times"></i>';
    ret += '</div>';
    ret += '<div class="clearfix"></div>';
    ret += '<div class="picklistcontent" style="width:' + newWidth + 'px; height:' + newHeight + 'px;">';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    var ret_elem = $(ret);

    dis_ele.closest('body').prepend(ret_elem);

    $(ret_elem).find('.picklistcontent:eq(0)').html(my_dialog);
    $(ret_elem).on('click', '.picklistclose', function (e) {
        e.stopPropagation();
        $(this).closest('.picklistoverlay').remove();
        var e = $.Event("keyup");
        e.which = 13;
        var auto_search_picklist = $('body').find('.picklist-request-id-storage').text();

        $('.searchPicklist').val(auto_search_picklist);

        $('.searchPicklist').trigger(e);

    });
}

function viewPickListSource(sourceFormId) {
    var form_id = sourceFormId;

    //e.preventDefault();
    var dis_ele = $(this);
    var requestId = dis_ele.closest('td').find('[data-form-source-id]').attr('data-form-source-id');
    var trackNo = dis_ele.closest('td').find('[data-trackno]').attr('data-trackno');
    var src = '/user_view/workspace?view_type=request&formID=' + form_id + '&requestID=' + requestId + '&trackNo=' + trackNo + '&embed_type=viewEmbedOnly';
    //var str_link = "/user_view/workspace?view_type=request&formID=" + form_id + "&requestID=0";
    var newHeight = $(window).outerHeight() - 50;
    var newWidth = $(window).outerWidth() - 50;
    var ret = "";
    ret += '<div class="picklistoverlay">';
    ret += '<div class="picklistmiddler">';
    ret += '<div id="' + form_id + '" class="picklistpos">';
    ret += '<div class="idehead">';//'+ideID+'

    ret += '<i class="picklistclose fa fa-times"></i>';
    ret += '</div>';
    ret += '<div class="clearfix"></div>';
    ret += '<div class="picklistcontent" style="width:' + newWidth + 'px; height:' + newHeight + 'px;">';
    ret += '</div>';

    //ret += '<div class="idefoot">';
    //ret += '<button class="idesave">Save</button>';
    //ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    var ret_elem = $(ret);
    dis_ele.closest('body').prepend(ret_elem);
    var my_dialog = $(
            '<div class="picklist-iframe" style="height: 100%;">' +
            '<iframe style="height:100%;width:100%;" src="' + src + '">' +
            '</iframe>' + '</div>'
            )
    $(ret_elem).find('.picklistcontent:eq(0)').html(my_dialog);
    $(ret_elem).on('click', '.picklistclose', function (e) {
        e.stopPropagation();
        $(this).closest('.picklistoverlay').remove();
        var e = $.Event("keyup");
        e.which = 13;

        $('.searchPicklist').trigger(e);
    });
}
// function CollisionTester(this_elements){

// 	var checked_ele = this_elements.map(function(a,b){
// 		var this_element = $(b);
// 		var ele_parent = this_element.parent();
// 		var this_ele_top = this_element.position().top;
// 		var this_ele_left = this_element.position().left;
// 		var this_ele_width = this_element.outerWidth();
// 		var this_ele_height = this_element.outerHeight();
// 		var formbuilder_ws_height = ele_parent.height();
// 		var formbuilder_ws_width = ele_parent.width();
// 		var this_ele_top_collide = this_ele_top + this_ele_height;
// 		var this_ele_left_collide = this_ele_left + this_ele_width;
// 		var c_bot = 0;
// 		var o_bot = 0;
// 		var c_top = 0;
// 		var o_top = 0;
// 		var c_left = 0;
// 		var o_left = 0;
// 		var c_right = 0;
// 		var o_right = 0;

// 		if(this_ele_top_collide >= formbuilder_ws_height){

// 			c_bot = true;
// 			o_bot = Math.abs(this_ele_top_collide - formbuilder_ws_height);
// 		}
// 		else{

// 			c_bot= false;
// 		}
// 		if(this_ele_left_collide >= formbuilder_ws_width){

// 			c_right= true;
// 			o_right = Math.abs(this_ele_left_collide -formbuilder_ws_width);

// 		}
// 		else{

// 			c_right= false;
// 		}
// 		if(this_ele_top<=0){

// 			c_top = true;
// 			o_top = Math.abs(0 - this_ele_top);
// 		}
// 		else{
// 			c_top= false;
// 		}
// 		if(this_ele_left<=0){
// 			c_left= true;
// 			o_left = Math.abs(0 - this_ele_left);
// 		}
// 		else{
// 			c_left = false;
// 		}
// 		return {
// 			"collide_top":c_top,
// 			"collide_bot":c_bot,
// 			"collide_left":c_left,
// 			"collide_right":c_right,
// 			"collide_overflow_top": o_top,
// 			"collide_overflow_bot": o_bot,
// 			"collide_overflow_left": o_left,
// 			"collide_overflow_right": o_right,
// 		}
// 	}).get();
// 	return {
// 		"final_collision_top":checked_ele.map(function(a){ return a["collide_top"]; }).filter(Boolean).length >= 1,
// 		"final_collision_bot":checked_ele.map(function(a){ return a["collide_bot"]; }).filter(Boolean).length >= 1,
// 		"final_collision_left":checked_ele.map(function(a){ return a["collide_left"]; }).filter(Boolean).length >= 1,
// 		"final_collision_right":checked_ele.map(function(a){ return a["collide_right"]; }).filter(Boolean).length >= 1,
// 		"final_collide_overflow_top": checked_ele.map(function(a){ return a["collide_overflow_top"]}).sort(function(a,b){ return b-a })[0],
// 		"final_collide_overflow_bot": checked_ele.map(function(a){ return a["collide_overflow_bot"]}).sort(function(a,b){ return b-a })[0],
// 		"final_collide_overflow_left": checked_ele.map(function(a){ return a["collide_overflow_left"]}).sort(function(a,b){ return b-a })[0],
// 		"final_collide_overflow_right": checked_ele.map(function(a){ return a["collide_overflow_right"]}).sort(function(a,b){ return b-a })[0],
// 	}

// }




function GetValidFieldsSOBJC(sel_setObject_container) {
    var each_container = $(sel_setObject_container);
    var collect_valid_fields = $();
    each_container.each(function () {
        var self_dis_ele = $(this);
        if (
                self_dis_ele.attr("data-upload") != "photo_upload" && (
                self_dis_ele.attr("data-type") == "labelOnly" ||
                self_dis_ele.attr("data-type") == "createLine" ||
                self_dis_ele.attr("data-type") == "table" ||
                self_dis_ele.attr("data-type") == "tab-panel" ||
                // self_dis_ele.attr("data-type") == "listNames" ||
                self_dis_ele.attr("data-type") == "button" ||
                self_dis_ele.attr("data-type") == "embeded-view" ||
                self_dis_ele.attr("data-type") == "autoTimer" ||
                self_dis_ele.attr("data-type") == "accordion" ||
                self_dis_ele.is('.fl-requestImg')
                )
                ) {
            return true; //preventing to insert it to the database
            // continue
        } else {
            collect_valid_fields = collect_valid_fields.add(self_dis_ele);
        }
    });
    return collect_valid_fields;
}
/*dito
 * FormBuilder Methods
 */

// Array.prototype.equalsArray = function (array) {
//     // if the other array is a falsy value, return
//     if (!array)
//         return false;picklist

//     // compare lengths - can save a lot of time 
//     if (this.length != array.length)
//         return false;

//     for (var i = 0, l=this.length; i < l; i++) {
//         // Check if we have nested arrays
//         if (this[i] instanceof Array && array[i] instanceof Array) {
//             // recurse into the nested arrays
//             if (!this[i].equals(array[i]))
//                 return false;       
//         }           
//         else if (this[i] != array[i]) { 
//             // Warning - two different object instances will never be equal: {x:20} != {x:20}
//             return false;   
//         }           
//     }       
//     return true;
// }


(function maintainRulerOnScroll() {
    return false;
    $(window).scroll(function () {
        var self = $(this);
        var window_scroll_h = self.scrollLeft();
        var window_scroll_v = self.scrollTop();
        // console.log(window_scroll_v)
        {//for ruler
            if (window_scroll_h >= 11) {
                $('.vRule').css({
                    "margin-left": (window_scroll_h - 11) + "px"
                });
                $('.leftPointerRuler-container').css({
                    "left": (window_scroll_h - 11) + "px"
                });
            } else {
                $('.vRule,.leftPointerRuler-container').css({
                    "margin-left": (0) + "px"
                });
                $('.leftPointerRuler-container').css({
                    "left": ""
                });
            }
            if (window_scroll_v >= 52) {
                $('.hRule').css({
                    "margin-top": (window_scroll_v - 52) + "px"
                });
                $('.topPointerRuler-container').css({
                    "top": (window_scroll_v - 52) + "px"
                });
            } else {
                $('.hRule,.topPointerRuler-container').css({
                    "margin-top": (0) + "px"
                });
                $('.topPointerRuler-container').css({
                    "top": ""
                });
            }
        }
    });
})()
function save_history(save) {

    if (save == "save") {
        var formbuilder_saver = $('.formbuilder_ws').find('.setObject').filter(function () {
            return $(this).parent().is('.formbuilder_ws');
        }).clone();
        var bdata = RenewObject($.extend({}, $('body').data()));
        undo_data_array.push(bdata);
        undo_array.push(formbuilder_saver);
    }
    else {
        redo_array = [];
        redo_data_array = [];
        if (save == "btn") {

            var formbuilder_saver = $('.btn_content').find('.setObject').clone();
        }
        else {

            var formbuilder_saver = $('.formbuilder_ws').find('.setObject').filter(function () {
                return $(this).parent().is('.formbuilder_ws');
            }).clone();
        }
        var bdata = RenewObject($.extend({}, $('body').data()));
        undo_data_array.push(bdata);
        undo_array.push(formbuilder_saver);
    }
    if (redo_array.length == 0) {
        $('.redo-me').addClass('redo-disable');
        $('.redo-me').removeClass('fl-buttonEffect');
    }
    $('.undo-me').removeClass('undo-disable');
    $('.undo-me').addClass('fl-buttonEffect');

}

function save_redo() {

    var formbuilder_saver = $('.formbuilder_ws').find('.setObject').filter(function () {
        return $(this).parent().is('.formbuilder_ws');
    }).clone();
    var bdata = RenewObject($.extend({}, $('body').data()));
    redo_data_array.push(bdata);
    redo_array.push(formbuilder_saver);

    $('.redo-me').removeClass('redo-disable');
    $('.redo-me').addClass('fl-buttonEffect');
}

function name_checker(ele_name) {
    var copy_ctr = 0;
    var intReg = /copy[0-9]+/g;
    var new_ele_name = ele_name;

    if (existing_fnames[ele_name]) {

        if (new_ele_name.match(/_copy/g) != null) {
            do {
                copy_ctr++;
                new_ele_name = new_ele_name.replace(intReg, "copy" + copy_ctr);
            } while (existing_fnames[new_ele_name]);

        }
        else {
            do {
                copy_ctr++;
                new_ele_name = ele_name.replace(ele_name, ele_name + '_copy' + copy_ctr + '');
            } while (existing_fnames[new_ele_name]);
        }

    }

    if ($.type(new_ele_name) == "undefined") {
        new_ele_name = "";
    }

    return new_ele_name;
}
var new_id = ""
function property_changer(copy) {

    /*declaration*/
    var tabRegex = /pane\-[0-9]+/g;
    var intRegex = /[0-9]+/g;
    var intReg = /copy[0-9]+/g;
    var lbl_below = copy.find('.label_below.obj_label');
    var pos_below = copy.find('.input_position_below');

	var elements_with_name = copy.find('[name]').filter(function (index) {
        return !$(this).parent().is('form');
    });
    var ele_name = elements_with_name.attr("name");

    var data_original_title = copy.find('[data-original-title]:not(.object_setup.object_properties):not(.object_setup.object_remove):not(.getFields)');

    /*end*/

    /*change the class and ID of the element*/

    var data_object_id = copy.attr('data-object-id');
    var new_data_oid = data_object_id.replace(intRegex, count);
    var ele_id = copy.attr('id');
    var new_ele_id = ele_id.replace(intRegex, count);
    // console.log("una ba to?",new_data_oid)
    new_id = new_data_oid;
    copy.attr('data-object-id', new_data_oid);
    copy.attr('id', new_ele_id);
    if (copy.is('[data-type="tab-panel"]')) {

        var match_string = copy.find('li').children('[href]');
        match_string.each(function () {
            var new_href = $(this).attr('href').replace(tabRegex, 'pane-' + count);

            $(this).attr('href', new_href);
        });
    }

    /*end*/


    /*change the ID of the contents of the element*/

    var old_id = copy.find('[id]');

    old_id.each(function () {
        var new_id = $(this).attr('id');
        if ($(this).is('[role="tabpanel"]')) {

            new_id = $(this).attr('id').replace(tabRegex, 'pane-' + count);
        }
        else {
            new_id = $(this).attr('id').replace(intRegex, count);
        }
        $(this).attr('id', new_id);
    });



    var old_doid = copy.find('[data-object-id]');
    old_doid.each(function () {
        var new_doid = $(this).attr('data-object-id').replace(intRegex, count);
        $(this).attr('data-object-id', new_doid);
    });

    var old_daid = copy.find('[data-action-id]');
    old_daid.each(function () {
        var new_daid = $(this).attr('data-action-id').replace(intRegex, count);
        $(this).attr('data-action-id', new_daid);
    });

    var old_da = copy.find('[data-attachment]');
    old_da.each(function () {
        var new_da = $(this).attr('data-attachment').replace(intRegex, count);
        $(this).attr('data-attachment', new_da);
    });

    /*end*/
    /*change the class of the contents of the element*/

    var old_class = copy.find('[class]');
    old_class.each(function () {
        var new_class = $(this).attr('class').replace(intRegex, count);
        $(this).attr('class', new_class);
    });
    var old_picklist_btn = copy.find('[picklist-button-id]');
    old_picklist_btn.each(function () {
        var new_picklist_btn = $(this).attr('picklist-button-id').replace(intRegex, count);
        $(this).attr('picklist-button-id', new_picklist_btn);
    });
    var old_rfield = copy.find('[return-field]');
    old_rfield.each(function () {
        var new_rfield = $(this).attr('return-field').replace(intRegex, count);
        $(this).attr('return-field', new_rfield);
    });

    /*end*/

    /*change the name of the element*/
    //console.log("sino ako",JSON.parse($(copy).attr('data-temp-doid')))
    var parseOldObj = JSON.parse($(copy).attr('data-temp-doid'));
    var parsecssOldObj = parseOldObj['obj_objectCss'];

    var repObjId =  new RegExp("_"+data_object_id,"g");
    var parsecssOldObjRep = (parsecssOldObj||"").replace(repObjId, "_"+new_id);
    
    //console.log("OLD OBJ CSS", parsecssOldObj);
    //console.log("NEW OBJ CSS", parsecssOldObjRep);

    if (!$(copy).parents('.setObject').length > 0) {
        elements_with_name.each(function(){
            var ele_name = $(this).attr("name");
            var new_ele_name = name_checker($(this).attr("name"));
            var copy = $(this).parents(".setObject[data-object-id]:eq(0)");
            if (copy.is('[data-type="checkbox"]') || copy.is('[data-type="selectMany"]')) {

                new_ele_name = new_ele_name.replace("[]", "") + "[]";
            }
            collectNames(new_ele_name);

            copy.find('[name="' + ele_name + '"]').filter(function (index) {
                return !$(this).parent().is('form');
            }).each(function () {
                $(this).attr('name', new_ele_name);
            });
            
            if(copy.attr('data-type') == 'listNames'){
                data_original_title.attr('data-original-title', 'Select Name');
            }
            else if(copy.attr('data-type') == 'pickList'){
                data_original_title.attr('data-original-title', 'Pick Keyword');
            }
            else{
                data_original_title.attr('data-original-title', new_ele_name);
            }
            var newData = $(copy).attr('data-temp-doid') || "";
            var dbd = $('body').data(new_data_oid);
            
            if (newData !== "") {
                newData = JSON.parse(newData);
                for (var ctr in newData) {

                    if ($.type(dbd) == "undefined") {
                        $('body').data(new_data_oid, {});
                        dbd = $('body').data(new_data_oid);
                        dbd[ctr] = newData[ctr];
                    }
                    else {
                        //Roni Pinili
                        var elements = $(copy);
                        var setObjIdTag = $('#setObject_'+elements.attr('data-object-id'));
                        var labelTag = $('#label_'+elements.attr('data-object-id'));
                        var fieldTag = $('#getFields_'+elements.attr('data-object-id'));
                        objectCSSProperties(new_data_oid, setObjIdTag, labelTag, fieldTag, elements);
                        
                        new_ele_name = new_ele_name.replace("[]", "");
                        
                        dbd['lblFldName'] = new_ele_name;
                        dbd[ctr] = newData[ctr];
                        dbd['obj_objectCss'] = parsecssOldObjRep;
                        
                    }

                }
            }
            else {

                if ($.type(dbd) == "undefined") {
                    $('body').data(new_data_oid, {});
                    dbd = $('body').data(new_data_oid);
                }
                else {
                    new_ele_name = new_ele_name.replace("[]", "");
                    dbd['lblFldName'] = new_ele_name;
                    dbd['obj_objectCss'] = parsecssOldObjRep;
                    //Roni Pinili
                    var elements = $(copy);
                    var setObjIdTag = $('#setObject_'+elements.attr('data-object-id'));
                    var labelTag = $('#label_'+elements.attr('data-object-id'));
                    var fieldTag = $('#getFields_'+elements.attr('data-object-id'));
                    objectCSSProperties(new_data_oid, setObjIdTag, labelTag, fieldTag, elements);
                }
            }
        });
        {//BACKUP 
                            // var new_ele_name = name_checker(ele_name);
                            

                            // if (copy.is('[data-type="checkbox"]') || copy.is('[data-type="selectMany"]')) {

                            //     new_ele_name = new_ele_name.replace("[]", "") + "[]";
                            // }
                            // collectNames(new_ele_name);

                            // copy.find('[name="' + ele_name + '"]').filter(function (index) {
                            //     return !$(this).parent().is('form');
                            // }).each(function () {
                            //     $(this).attr('name', new_ele_name);
                            // });
                            
                            // if(copy.attr('data-type') == 'listNames'){
                            //     data_original_title.attr('data-original-title', 'Select Name');
                            // }
                            // else{
                            //     data_original_title.attr('data-original-title', new_ele_name);
                            // }
                            // var newData = $(copy).attr('data-temp-doid') || "";
                            // var dbd = $('body').data(new_data_oid);
                            
                            // if (newData !== "") {
                            //     newData = JSON.parse(newData);
                            //     for (var ctr in newData) {

                            //         if ($.type(dbd) == "undefined") {
                            //             $('body').data(new_data_oid, {});
                            //             dbd = $('body').data(new_data_oid);
                            //             dbd[ctr] = newData[ctr];
                            //         }
                            //         else {
                            //             //Roni Pinili
                            //             var elements = $(copy);
                            //             var setObjIdTag = $('#setObject_'+elements.attr('data-object-id'));
                            //             var labelTag = $('#label_'+elements.attr('data-object-id'));
                            //             var fieldTag = $('#getFields_'+elements.attr('data-object-id'));
                            //             objectCSSProperties(new_data_oid, setObjIdTag, labelTag, fieldTag, elements);
                                        
                            //             new_ele_name = new_ele_name.replace("[]", "");
                                        
                            //             dbd['lblFldName'] = new_ele_name;
                            //             dbd[ctr] = newData[ctr];
                            //             dbd['obj_objectCss'] = parsecssOldObjRep;
                                        
                            //         }

                            //     }
                            // }
                            // else {

                            //     if ($.type(dbd) == "undefined") {
                            //         $('body').data(new_data_oid, {});
                            //         dbd = $('body').data(new_data_oid);
                            //     }
                            //     else {
                            //         new_ele_name = new_ele_name.replace("[]", "");
                            //         dbd['lblFldName'] = new_ele_name;
                            //         dbd['obj_objectCss'] = parsecssOldObjRep;
                            //         //Roni Pinili
                            //         var elements = $(copy);
                            //         var setObjIdTag = $('#setObject_'+elements.attr('data-object-id'));
                            //         var labelTag = $('#label_'+elements.attr('data-object-id'));
                            //         var fieldTag = $('#getFields_'+elements.attr('data-object-id'));
                            //         objectCSSProperties(new_data_oid, setObjIdTag, labelTag, fieldTag, elements);
                            //     }
                            // }
        }



    }
    else {
        var newData = $(copy).attr('data-temp-doid') || "";

        var dbd = $('body').data(new_data_oid);
        var new_ele_name = ele_name;
        if (newData !== "") {
            newData = JSON.parse(newData);
            $(copy).removeAttr('data-temp-doid');
            for (var ctr in newData) {


                if ($.type(dbd) == "undefined") {
                    $('body').data(new_data_oid, {});
                    dbd = $('body').data(new_data_oid);
                    dbd[ctr] = newData[ctr];
                }
                else {
                    new_ele_name = new_ele_name.replace("[]", "");
                    dbd['lblFldName'] = new_ele_name;
                    dbd[ctr] = newData[ctr];
                    dbd['obj_objectCss'] = parsecssOldObjRep;
                    //Roni Pinili
                    var elements = $(copy);
                    var setObjIdTag = $('#setObject_'+elements.attr('data-object-id'));
                    var labelTag = $('#label_'+elements.attr('data-object-id'));
                    var fieldTag = $('#getFields_'+elements.attr('data-object-id'));
                    objectCSSProperties(new_data_oid, setObjIdTag, labelTag, fieldTag, elements);
                    console.log("ELSE", elements);
                }

            }
        }
        else {

            if ($.type(dbd) == "undefined") {
                $('body').data(new_data_oid, {});
                dbd = $('body').data(new_data_oid);
            }
            else {


                new_ele_name = new_ele_name.replace("[]", "");
                dbd['lblFldName'] = new_ele_name;
                dbd['obj_objectCss'] = parsecssOldObjRep;

                //Roni Pinili
                var elements = $(copy);
                var setObjIdTag = $('#setObject_'+elements.attr('data-object-id'));
                var labelTag = $('#label_'+elements.attr('data-object-id'));
                var fieldTag = $('#getFields_'+elements.attr('data-object-id'));
                objectCSSProperties(new_data_oid, setObjIdTag, labelTag, fieldTag, elements);

            }
        }
    }
 





}
function cut_fixer(elements) {
    var fix_me = $(elements);
    return fix_me.each(function () {
        var change_me = $(this);
        clone_position_fixer.call(this);
        if (change_me.is('[data-type="accordion"]')) {
            change_me.find('.ui-state-active,.ui-state-hover').removeClass('ui-state-active').removeClass('ui-state-hover');
        }

        property_changer(change_me);
        count++;
        if (change_me.find('.setObject').length > 0) {

            change_me.find('.setObject').each(function () {

                var change_mes = $(this);

                property_changer(change_mes);
                count++;
            });
        }
        $(this).appendTo(".formbuilder_ws");
    });
}
function clone_position_fixer() {
    var change_me = $(this);
    console.log("copied ele", change_me);
    var original_scroll_top = change_me.attr("data-original-scroll-top");
    var original_top = change_me.attr("data-original-top");
    original_top = Number(original_top.replace("px", ""));
    var fld_position_top = original_top - original_scroll_top;
    var original_pos_left = change_me.css('left');
    original_pos_left = Number(original_pos_left.replace('px', ""));
    var original_scroll_left = change_me.attr("data-original-scroll-left");
    var fld_position_left = original_pos_left - original_scroll_left;
    var scroll_top = $(document).scrollTop();
    var scroll_left = $(document).scrollLeft();
    change_me.addClass('component-ancillary-focus');
    change_me.css('left', fld_position_left + scroll_left + 20 + "px");
    change_me.css('top', 20 + scroll_top + fld_position_top + "px");
    var fb_height = Number($('.formbuilder_ws').height());
    var fb_width = Number($('.formbuilder_ws').width());
    var this_top = Number(change_me.css("top").replace("px", "")) + 100;
    // var this_left = Number(change_me.css("left").replace("px",""))+ change_me.outerWidth();
    //console.log("excess",change_me.css("top").replace("px",""),Number(change_me.outerHeight()))
    console.log("diff", fb_width, Number(change_me.css("left").replace("px", "")), change_me.outerWidth())

    if (fb_height < this_top) {

        var difference_height = this_top - fb_height;
        //console.log("diffh",fb_height,this_top,difference_height)
        var new_fb_height = fb_height + difference_height;
        $('.formbuilder_ws').css("height", new_fb_height + "px");
        $(".formbuilder_ws").trigger("resize");

    }
    // if(fb_width < this_left){
    // 	alert()
    // 	var difference_width = this_left-fb_width;
    // 	console.log("diffh",fb_width,difference_width)
    // 	var new_fb_width = fb_width+difference_width;
    // 	$('.formbuilder_ws').css("width",new_fb_width+"px");
    // 	$(".formbuilder_ws").trigger("resize");
    // }



}
function clone_fixers(new_copy) {
    return new_copy.each(function () {
        var change_me = $(this);
        console.log("change", change_me)
        /*position the clone*/
        clone_position_fixer.call(this);


        /*end*/
        if (change_me.is('[data-type="accordion"]')) {
            change_me.find('.ui-state-active,.ui-state-hover').removeClass('ui-state-active').removeClass('ui-state-hover');
        }
        property_changer(change_me);
        count++;
        if (change_me.find('.setObject').length > 0) {

            change_me.find('.setObject').each(function () {

                var change_mes = $(this);

                property_changer(change_mes);
                count++;
            });
        }

        new_copy.appendTo(".formbuilder_ws");


    });
}
function dropdown_reset() {

    var lff_array = [];
    var fff_array = [];
    var lta_array = [];
    var fta_array = [];
    var ffs_array = [];
    var lfs_array = [];
    var fh_array = [];
    var fw_array = [];
    var lfstyle_array = [];
    var ltd_array = [];
    var lblc_array = [];
    var fldfw_array = [];
    var fldfs_array = [];
    var fldtd_array = [];
    var fldc_array = [];
    var ff = "";
    var ta = "";

    var dropdown_ctr = $('.formbuilder_ws')
            .find('.component-ancillary-focus')
            .filter(function (a, b) {

                return $(b).is('[data-type="imageOnly"],[data-type="createLine"],[data-type="labelOnly"]')
            })//PEDE GAWING FUNCT
            ;

    // console.log("drop", $.inArray($('[data-type="imageOnly"]'), dropdown_ctr))
    $('.multiple-field-property').find('.fl-input-form-settings').each(function () {
        if (dropdown_ctr.exists() || $('.formbuilder_ws').find('.component-ancillary-focus').length < 1) {
            // if($.inArray($('[data-type="imageOnly"]'),dropdown_ctr))

           //$(this).prop('disabled', true);
            $(this).not('[type="checkbox"]').val('');
            if ($(this).is('.change_label_color')) { // spectrumclass
                $(this).spectrum('disable');
                $(this).spectrum("set", "");
            }
            if ($(this).is('[type="checkbox"]')) {
                $(this).prop('checked', false);
                $(this).closest('.spanbutton').removeClass('bn-cd');
            }

            if ((!dropdown_ctr.filter('[data-type="imageOnly"]').exists()) && $(this).is('.label-gen-prop') && $('.formbuilder_ws').find('.component-ancillary-focus').exists()) {
                $(this).prop('disabled', false);
                if ($(this).is('.change_label_color')) { // spectrumclass
                    $(this).spectrum('enable');

                }

            }

        }
        else if (!dropdown_ctr.exists() && $('.formbuilder_ws').find('.component-ancillary-focus').length >= 1) {
            //$(this).prop('disabled', false);
            if ($(this).is('.change_label_color')) { // spectrumclass
                $(this).spectrum('enable');
            }
        }
        if ($(this).is('[disabled]')) {
            //$(this).css('opacity', '0.5').addClass('disable-clickable');
            if ($(this).is('[type="checkbox"]')) {
                $(this).parent().css('opacity', '0.5').find('label').addClass('disable-clickable');
               
            }
        }
        if (!$(this).is('[disabled]')) {
            //$(this).css('opacity', '').removeClass('disable-clickable').addClass('enable-clickable');
            if ($(this).is('[type="checkbox"]')) {
                $(this).parent().css('opacity', '').find('label').removeClass('disable-clickable').addClass('enable-clickable');
            }
        }
    });

    $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

        var doid = $(this).attr('data-object-id');
        var aligned_left = $(this).children("div.fields_below").find("div.input_position_below.obj_f-aligned-left");
        if (aligned_left.length >= 1) {
            var field_font = aligned_left.find(".obj_f-aligned-left-wrap").children();
            var field_input = aligned_left.find(".obj_f-aligned-left-wrap").find("input");
            var field_align = aligned_left.find(".obj_f-aligned-left-wrap").children();
        }
        else {
            var field_align = $(this).children("div.fields_below").find("div.input_position_below").children();
            var field_font = $("#setObject_" + doid).children("div.fields_below").find("div.input_position_below").children();
            var field_input = $("#setObject_" + doid).children("div.fields_below").find("div.input_position_below").find("input");
        }

        if (field_font.length >= 1) {
            ff = field_font.css("font-family");


        }
        if (field_input.length >= 1) {
            ff = field_input.css("font-family");
        }
        if (field_align.length >= 1) {
            ta = field_align.css("text-align");

        }


        if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "radio" || $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("type") == "checkbox") {
            var lbl_size = $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("font-size");
            //$("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
            var fldfw = $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("font-weight");
            var fldfs = $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("font-style");
            var fldtd = $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("text-decoration");
            var fldc = $("#obj_fields_" + doid).find(".getFields_" + doid).parent().css("color");
        }
        else {
            var lbl_size = $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("font-size");
            var fldfw = $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("font-weight");
            var fldfs = $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("font-style");
            var fldtd = $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("text-decoration");
            if ($("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).attr("data-type") == "createLine") {
                var fldc = $(".getFields_" + doid).css('background-color');
            }
            else {
                var fldc = $("#obj_fields_" + doid).find(".getFields_" + doid).eq(0).css("color");
            }


        }
        //var fld_height = $("#getFields_" + doid).css("height");
        var f_w = $("#lbl_" + doid).css("font-weight");
        var lfstyle = $("#lbl_" + doid).css("font-style");
        var ltd = $("#lbl_" + doid).css("text-decoration");
        var lta = $(this).find('#lbl_' + doid).css('text-align');
        var lblc = $(this).find('#lbl_' + doid).css('color');


        if (ta == "start") {
            ta = "Left";
        }
        if (f_w == 400) {
            f_w = "normal";
        }
        if (lta == "start") {
            lta = "Left";
        }

        var doid = $(this).attr('data-object-id');
        var dbd = $('body').data(doid);

        if(dbd){
            if(dbd["lblFontType"]){
                //console.log("TEST54321",dbd["lblFontType"])
                lff_array.push(dbd["lblFontType"]);
                fff_array.push(dbd["fieldFontType"]);
            }
        }else{
            lff_array.push($(this).find('#lbl_' + doid).css('font-family'))
            fff_array.push(ff);
            // console.log("TEST54321",dbd["lblFontType"])
        }


        // lff_array.push($(this).find('#lbl_' + doid).css('font-family'));
        // fff_array.push(ff);
        lta_array.push(lta);
        fta_array.push(ta);
        ffs_array.push(lbl_size);
        lfs_array.push($("#lbl_" + doid).css("font-size"));

        //fh_array.push(fld_height);
        fw_array.push(f_w);
        lfstyle_array.push(lfstyle);
        ltd_array.push(ltd);
        lblc_array.push(lblc);
        fldfw_array.push(fldfw);
        fldfs_array.push(fldfs);
        fldtd_array.push(fldtd);
        fldc_array.push(fldc);


        

    });

    $.unique(lff_array);
    $.unique(fff_array);
    $.unique(lta_array);
    $.unique(fta_array);
    $.unique(ffs_array);
    $.unique(lfs_array);
    //$.unique(fh_array);
    $.unique(fw_array);
    $.unique(lfstyle_array);
    $.unique(ltd_array);
    $.unique(lblc_array);
    $.unique(fldfw_array);
    $.unique(fldfs_array);
    $.unique(fldtd_array);
    $.unique(fldc_array);


    if (fldc_array.length == 1 && fldtd_array.length == 1 && fldfs_array.length == 1 && lff_array.length == 1 && fff_array.length == 1 && lta_array.length == 1 && fta_array.length == 1 && ffs_array.length == 1 && lfs_array.length == 1 && fw_array.length == 1 && lfstyle_array.length == 1 && ltd_array.length == 1 && lblc_array.length == 1 && fldfw_array.length == 1) {
        $('.multiple-field-property').find('.change_label_font_family').val(lff_array[0]);
        // console.log("PLEASE LABEL", $('.multiple-field-property').find('.change_label_font_family').val());
        $('.multiple-field-property').find('.change_field_font_family').val(fff_array[0]);
        /*console.log("tae FIELD", $('.multiple-field-property').find('.change_field_font_family').val());*/
        $('.multiple-field-property').find('.change_label_text_alignment').val(capitaliseFirstLetter(lta_array[0]||""));
        $('.multiple-field-property').find('.change_field_text_alignment').val(capitaliseFirstLetter(fta_array[0]||""));
        $('.multiple-field-property').find('.change_field_font_size').val(ffs_array[0]);
        $('.multiple-field-property').find('.change_label_font_size').val(lfs_array[0]);
        //$('.multiple-field-property').find('.change_field_height').val(fh_array[0]);
        $('.multiple-field-property').find('.change_label_color').spectrum('set', lblc_array[0]);
        $('.multiple-field-property').find('.change_field_color').spectrum('set', fldc_array[0]);
        if (fw_array[0] == "bold") { //  normal
            // console.log("bold", fw_array[0])
            $('.multiple-field-property').find('.change_label_font_weight').prop('checked', true);
            $('.multiple-field-property').find('.change_label_font_weight').parents('.spanbutton').addClass('bn-cd');
            /*$('.multiple-field-property').find('.change_label_font_family').val($('.change_label_font_weight').attr('has-normal'));*/
           
        }
        if (fw_array[0] != "bold") { //  normal
             /*console.log(fw_array[0])*/
            $('.multiple-field-property').find('.change_label_font_weight').prop('checked', false);
            $('.multiple-field-property').find('.change_label_font_weight').parents('.spanbutton').removeClass('bn-cd');
           
        }
        if (lfstyle_array[0] == "italic") {
            $('.multiple-field-property').find('.change_label_font_style').prop('checked', true);
            $('.multiple-field-property').find('.change_label_font_style').parents('.spanbutton').addClass('bn-cd');
        }
        if (lfstyle_array[0] != "italic") {
            $('.multiple-field-property').find('.change_label_font_style').prop('checked', false);
            $('.multiple-field-property').find('.change_label_font_style').parents('.spanbutton').removeClass('bn-cd');
        }
        if (ltd_array[0] == "underline") {
            $('.multiple-field-property').find('.change_label_text_decoration').prop('checked', true);
            $('.multiple-field-property').find('.change_label_text_decoration').parents('.spanbutton').addClass('bn-cd');
        }
        if (ltd_array[0] != "underline") {
            $('.multiple-field-property').find('.change_label_text_decoration').prop('checked', false);
            $('.multiple-field-property').find('.change_label_text_decoration').parents('.spanbutton').removeClass('bn-cd');
        }
        if (fldfw_array[0] == "bold") {//  normal
            $('.multiple-field-property').find('.change_field_font_weight').prop('checked', true);
            $('.multiple-field-property').find('.change_field_font_weight').parents('.spanbutton').addClass('bn-cd');
             
        }
        if (fldfw_array[0] != "bold") { //  normal
            $('.multiple-field-property').find('.change_field_font_weight').prop('checked', false);
            $('.multiple-field-property').find('.change_field_font_weight').parents('.spanbutton').removeClass('bn-cd');
        }
        if (fldfs_array[0] == "italic") {
            $('.multiple-field-property').find('.change_field_font_style').prop('checked', true);
            $('.multiple-field-property').find('.change_field_font_style').parents('.spanbutton').addClass('bn-cd');
        }
        if (fldfs_array[0] != "italic") {
            $('.multiple-field-property').find('.change_field_font_style').prop('checked', false);
            $('.multiple-field-property').find('.change_field_font_style').parents('.spanbutton').removeClass('bn-cd');
        }
        if (fldtd_array[0] == "underline") {
            $('.multiple-field-property').find('.change_field_text_decoration').prop('checked', true);
            $('.multiple-field-property').find('.change_field_text_decoration').parents('.spanbutton').addClass('bn-cd');
        }
        if (fldtd_array[0] != "underline") {
            $('.multiple-field-property').find('.change_field_text_decoration').prop('checked', false);
            $('.multiple-field-property').find('.change_field_text_decoration').parents('.spanbutton').removeClass('bn-cd');
        }

    }
    else {

        $('.multiple-field-property').find('.change_label_font_family').val("");
        $('.multiple-field-property').find('.change_field_font_family').val("");
        $('.multiple-field-property').find('.change_label_text_alignment').val("");
        $('.multiple-field-property').find('.change_field_text_alignment').val("");
        $('.multiple-field-property').find('.change_field_font_size').val("");
        $('.multiple-field-property').find('.change_label_font_size').val("");
        //$('.multiple-field-property').find('.change_field_height').val("");
        $('.multiple-field-property').find('.change_label_font_weight').prop('checked', false);
        $('.multiple-field-property').find('.change_label_font_weight').parents('.spanbutton').removeClass('bn-cd');
        $('.multiple-field-property').find('.change_label_font_style').prop('checked', false);
        $('.multiple-field-property').find('.change_label_font_style').parents('.spanbutton').removeClass('bn-cd');
        $('.multiple-field-property').find('.change_label_text_decoration').prop('checked', false);
        $('.multiple-field-property').find('.change_label_text_decoration').parents('.spanbutton').removeClass('bn-cd');
        $('.multiple-field-property').find('.change_label_color').spectrum('set', "");
        $('.multiple-field-property').find('.change_field_font_weight').prop('checked', false);
        $('.multiple-field-property').find('.change_field_font_weight').parents('.spanbutton').removeClass('bn-cd');
        $('.multiple-field-property').find('.change_field_font_style').prop('checked', false);
        $('.multiple-field-property').find('.change_field_font_style').parents('.spanbutton').removeClass('bn-cd');
        $('.multiple-field-property').find('.change_field_text_decoration').prop('checked', false);
        $('.multiple-field-property').find('.change_field_text_decoration').parents('.spanbutton').removeClass('bn-cd');
        $('.multiple-field-property').find('.change_field_color').spectrum('set', "");
    }

}


//Set your default object properties here
//defaultObjectProperties(objID)
//Roni Pinili
function defaultObjectProperties(objID){

    var dbd = $('body').data(objID);
    if ($.type(dbd) == "undefined") {
        
        $('body').data(objID, {});
        dbd = $('body').data(objID);
        dbd['lblFontType'] = 'droid_sansregular';
        dbd['fieldFontType'] = 'droid_sansregular';
        //console.log($('body').data());
    }
 
}

// I was here

function deleteMultipleObjects(elements) {
    var body_data = $("body").data();
    var obj_num = elements.length;
    if (obj_num > 1) {
        obj_num = "Are you sure you want to delete the selected objects (" + obj_num + ")?";
    }
    else {
        obj_num = "Are you sure you want to delete this object?";
    }

    var newConfirm = new jConfirm(obj_num, 'Confirmation Dialog', '', '', '', function (r) {
        if (r == true) {
            save_history();
            elements.each(function () {
                var object_id = $(this).attr('data-object-id');
                var parent_container = $(this).parent();

                $(this).remove();
                // slide menu layer remove
                $('#layer_'+object_id).remove();
                $('.ps-container').perfectScrollbar("update");
                // 
                if (parent_container.is('.formbuilder_ws')) {
                    var fmw = checkParentResizableMinWidth(parent_container.children('.setObject').eq(0));
                    var fmh = checkParentResizableMinHeight(parent_container.children('.setObject').eq(0));

                    $('.formbuilder_ws').resizable('option', 'minWidth', fmw);
                    $('.formbuilder_ws').resizable('option', 'minHeight', fmh);
                    $('.formbuilder_ws').css('min-width', fmw);
                    $('.formbuilder_ws').css('min-height', fmh);
                }
                if (typeof body_data[object_id] != "undefined") {
                    delete body_data[object_id];
                    console.log("NAGDELETE")
                    console.log(body_data)
                }
            });
            $("body").data(body_data);
        } else {
            //WAG IMERGE
        }
    });
    newConfirm.themeConfirm("confirm2", {
        'icon': '<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>'
    });
}

function ctrlArrowKeyEvent(map, ctr) {

    var arrow_ctr = ctr;
    if (arrow_ctr == 0) {
        save_history();
    }
    //var collision = CollisionTester($('.formbuilder_ws').find('.component-ancillary-focus'));
    var collision = $('.formbuilder_ws').find('.component-ancillary-focus').testCollision();
    if ($('.formbuilder_ws').find('.setObject').length > 0) {
        $('.formbuilder_ws').find('.component-ancillary-focus').each(function () {

            var pos_top = $(this).position().top;
            var pos_left = $(this).position().left;
            var movement_top = pos_top;
            var movement_left = pos_left;
            if (map[40]) {
                if (!collision["final_collision_bot"]) {
                    movement_top = pos_top + arrow_ctr;
                }
                else {
                    movement_top = pos_top - collision["final_collide_overflow_bot"];
                }
            }

            if (map[38]) {
                if (!collision["final_collision_top"]) {
                    movement_top = pos_top - arrow_ctr;
                }
                else {
                    movement_top = (pos_top + collision["final_collide_overflow_top"]);
                }
            }
            if (map[37]) {
                if (!collision["final_collision_left"]) {
                    movement_left = pos_left - arrow_ctr;
                }
                else {
                    movement_left = pos_left + collision["final_collide_overflow_left"];
                }
            }
            if (map[39]) {
                if (!collision["final_collision_right"]) {
                    movement_left = pos_left + arrow_ctr;
                }
                else {
                    movement_left = pos_left - collision["final_collide_overflow_right"];
                }
            }
            $(this).css({
                "top": movement_top,
                "left": movement_left,
            });
        });
    }


}
function shortCutNavPosition(ele, operation) {
    var li_ele = $(ele).closest('li.nav-menu-li');
    var li_ele_class = li_ele.attr('class')
    var li_ele_address = li_ele.attr('address');
    var classRegex = /placeholder-\d/;
    var new_li_class = "";
    var switcher = true;
    var descendant_li_ele = li_ele.nextAll().not(':has(.active-secondary-element-nav)').filter(function () {
        console.log("address", $(this).attr('address'), li_ele_address)
        if (switcher && (Number($(this).attr('address')) > Number(li_ele_address))) {
            return true;
        }
        else {
            switcher = false;
            return false;
        }
    });
    if (operation == "add") {

        new_li_class = li_ele_class.replace(classRegex, 'placeholder-' + (Number(li_ele_address) + 1))


        

        if (li_ele.nextAll().exists()) {

            console.log("descendant_li_ele", descendant_li_ele)
            descendant_li_ele.each(function () {
                var descendant_li_ele_class = $(this).attr('class');
                var descendant_li_ele_address = $(this).attr('address');
                var new_descendant_li_class = descendant_li_ele_class.replace(classRegex, 'placeholder-' + (Number(descendant_li_ele_address) + 1));
                $(this).attr('class', new_descendant_li_class);
                $(this).attr('address', Number(descendant_li_ele_address) + 1);
            });

        }

        li_ele.removeClass(li_ele_class).addClass(new_li_class);
        li_ele.attr('address', Number(li_ele_address) + 1);

    }
    if (operation == "minus") {
        new_li_class = li_ele_class.replace(classRegex, 'placeholder-' + (Number(li_ele_address) - 1));
        if (li_ele.nextAll().exists()) {
            descendant_li_ele.each(function () {
                var descendant_li_ele_class = $(this).attr('class');
                var descendant_li_ele_address = $(this).attr('address');
                var new_descendant_li_class = descendant_li_ele_class.replace(classRegex, 'placeholder-' + (Number(descendant_li_ele_address) - 1));
                $(this).attr('class', new_descendant_li_class);
                $(this).attr('address', Number(descendant_li_ele_address) - 1);
            });
        }
        li_ele.removeClass(li_ele_class).addClass(new_li_class);
        li_ele.attr('address', Number(li_ele_address) - 1);
    }
    unsavedEditConfirmation(true);


}

function keyBoardShortCut() {
    var last_action = localStorage["lastActionToStore"] || "";
    var history = [];
    var arrow_ctr = 0;
    var ex = 0;
    var way = 0;
    var pathname = window.location.pathname;

    // console.log('pathname', pathname)

    if (pathname == '/user_view/workspace') {
        $('.fl-toggle-nav-view.switch-nav-view').not('.fl-toggle-nav-view.switch-nav-view').css('display', 'none');
    };

    var map = {40: false, 39: false, 38: false, 37: false};
    $(document).on({
        "keyup": function (e) {
            if (e.keyCode in map) {
                map[e.keyCode] = false;

            }
            if (e.keyCode == 27) { // esc
                $(".dynamic-td-droppable-selected").removeClass("dynamic-td-droppable-selected");
                $(".component-ancillary-focus").removeClass("component-ancillary-focus");
                $(".component-primary-focus").removeClass("component-primary-focus");
                $(".layerhighlight").removeClass("layerhighlight");
                if (pathname == "/user_view/nav_settings") {
                    $('.active-primary-element-nav, .active-secondary-element-nav').removeClass('active-primary-element-nav').removeClass('active-secondary-element-nav');
                }
                $('.content-dialog-formbuilder-description').parents('#popup_container').find('.fl-closeDialog').trigger('click');
            }

            else if (e.ctrlKey && e.keyCode == 37 || e.ctrlKey && e.keyCode == 38 || e.ctrlKey && e.keyCode == 39 || e.ctrlKey && e.keyCode == 40) {
                arrow_ctr = 0;
            }
            else if (e.keyCode == 46 && pathname == "/user_view/formbuilder") {
                if ($('.formbuilder_ws').find('.component-ancillary-focus').length > 0 && !$('#popup_container').exists()) {
                    deleteMultipleObjects($('.formbuilder_ws').find('.component-ancillary-focus'));
                }

            }



        },
        "keydown": function (e) {
            var keycode = e.which || e.keyCode;
            if ($('.formbuilder-ws-active').length >= 1 && ( $('#popup_container').length <= 0 && $('.ideoverlay').length <= 0 ) ) {

                if (e.keyCode in map) {
                    if (e.ctrlKey) {
                        e.preventDefault();
                        map[e.keyCode] = true;
                        arrow_ctr++;
                        ctrlArrowKeyEvent(map, arrow_ctr);
                        console.log("ee Control");
                    }


                }
                if (e.ctrlKey && e.keyCode == 65) {
                    if ($('.formbuilder_ws').find('.setObject').length > 0) {
                        e.preventDefault();
                        $('.formbuilder_ws').find('.setObject').addClass('component-ancillary-focus');
                        dropdown_reset();
                    }
                }



                /*undo redo*/
                else if (e.ctrlKey && e.keyCode == 90) { //undo
                    if ($('.formbuilder_ws').exists() && !(pathname == "/user_view/formbuilder")) {
                        jAlert("Undo/Redo is not allowed.", "", "", "", "", function () {
                        });
                        return false;
                    }
                    if (last_action == "cut") {
                        history = [];
                    }
                    var u_count = undo_array.length - 1;
                    var u_d_count = undo_data_array.length - 1;
                    if (u_count >= 0) {
                        $("body").find('.tooltip.fade.top.in').each(function () {
                            $(this).remove();
                        });
                        save_redo();
                        $('.formbuilder_ws').find('.setObject').each(function () {
                            $(this).remove();
                        });
                        undo_array[u_count].each(function () {
                            $(this).appendTo('.formbuilder_ws');
                        });


                        $('body').removeData();
                        $('body').data($.extend({}, undo_data_array[u_d_count]));

                        delete undo_data_array[u_d_count];
                        undo_data_array = undo_data_array.filter(Boolean);
                        delete undo_array[u_count];
                        undo_array = undo_array.filter(Boolean);
                        if ($('.formbuilder_ws').find('.setObject').length > 0) {

                            $('.formbuilder_ws').find('.setObject').each(function () {
                                $(this).rebindDragObects();
                            });
                        }
                        dropdown_reset();


                    }
                    if (undo_array.length == 0) {
                        $('.undo-me').addClass('undo-disable');
                        $('.undo-me').removeClass('fl-buttonEffect');
                    }
                }
                else if (e.ctrlKey && e.keyCode == 89) //redo
                {
                    if ($('.formbuilder_ws').exists() && !(pathname == "/user_view/formbuilder")) {
                        jAlert("Undo/Redo is not allowed.", "", "", "", "", function () {
                        });
                        return false;
                    }
                    if (last_action == "cut") {
                        history = [];
                    }
                    /*redo*/
                    var r_count = redo_array.length - 1;
                    var r_d_count = redo_data_array.length - 1;
                    if (r_count >= 0) {
                        $('body').find('.tooltip').each(function () {
                            $(this).remove();
                        });

                        save_history("save");
                        $('.formbuilder_ws').find('.setObject').each(function () {
                            $(this).remove();
                        });
                        redo_array[r_count].each(function () {
                            $(this).appendTo('.formbuilder_ws');
                        });
                        $('body').removeData();

                        $('body').data(redo_data_array[r_d_count]);

                        delete redo_data_array[r_d_count];
                        delete redo_array[r_count];
                        redo_data_array = redo_data_array.filter(Boolean);
                        redo_array = redo_array.filter(Boolean);
                        if ($('.formbuilder_ws').find('.setObject').length > 0) {

                            $('.formbuilder_ws').find('.setObject').each(function () {
                                $(this).rebindDragObects();
                            });
                        }
                        dropdown_reset();

                    }
                    if (redo_array.length == 0) {
                        $('.redo-me').addClass('redo-disable');
                        $('.redo-me').removeClass('fl-buttonEffect');
                    }
                }
            }

            if (e.keyCode == 112) {
                formbuilderHelpModal();
                e.preventDefault();
                e.stopPropagation();
            };
        },
    });


    $(document).on({

        copy: function () {


            if ($('#popup_container').length <= 0 && $('.ideoverlay').length <= 0 && $('.formbuilder_ws').exists()) {     
                if (!(pathname == "/user_view/formbuilder")) {
                    var newAlert = new jAlert("Copy/Cut/Paste is not allowed.", "", "", "", "", function () {
                    });
                    newAlert.themeAlert("jAlert2", {
                        'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                    });
                    return false;
                }
                if ($(this).find('[data-type="button"]').hasClass('component-ancillary-focus')) {
                    $(this).find('[data-type="button"]').removeClass('component-ancillary-focus');
                    $(this).find('[data-type="button"]').removeClass('component-primary-focus');
                }
                if ($(this).find('[data-type="tab-panel"]').hasClass('component-ancillary-focus')) {
                    $(this).find('[data-type="tab-panel"]').children('.fields_below').find('.setObject').removeClass('component-ancillary-focus');
                    $(this).find('[data-type="tab-panel"]').children('.fields_below').find('.setObject').removeClass('component-primary-focus');
                }
                if ($(this).find('[data-type="table"],[data-type="accordion"]').hasClass('component-ancillary-focus')) {
                    $(this).find('[data-type="table"],[data-type="accordion"]').children('.fields_below').find('.setObject').removeClass('component-ancillary-focus');
                }
                $(this).find('[data-type="table"],[data-type="accordion"]').children('.fields_below').find('.setObject').removeClass('component-primary-focus');
                var test = $(this).find('.component-ancillary-focus');

                test.each(function () {
                    var doid = "";
                    var data_val = "";
                    if ($(this).find('.setObject').length > 0) {
                        $(this).find('.setObject').add(this).each(function () {

                            doid = $(this).attr('data-object-id');
                            if ($.type($('body').data(doid)) != "undefined") {
                                data_val = JSON.stringify($('body').data(doid));
                                $(this).attr("data-temp-doid", data_val);

                            }
                        });
                    }
                    else {

                        doid = $(this).attr('data-object-id');
                        if ($.type($('body').data(doid)) != "undefined") {
                            data_val = JSON.stringify($('body').data(doid));
                            $(this).attr("data-temp-doid", data_val);
                        }
                    }




                    var scrollTop = $(document).scrollTop();
                    var scrollLeft = $(document).scrollLeft();
                    var originalTop = $(this).offset().top - $('.formbuilder_ws').offset().top;
                    //var originalTop = $(this).css('top');
                    var originalLeft = $(this).offset().left - $('.formbuilder_ws').offset().left;


                    $(this).attr("data-original-scroll-top", scrollTop);
                    $(this).attr("data-original-top", originalTop);
                    $(this).attr("data-original-left", originalLeft);
                    $(this).attr("data-original-scroll-left", scrollLeft);



                });

                var new_copy = test.clone();
                $('[data-temp-doid]').removeAttr('data-temp-doid');
                var divStorage = $('<div class="clone-storage"></div>');
                new_copy.appendTo(divStorage);
                localStorage["variableToStore"] = escape(divStorage.html());
                //history.push({"element":new_copy});
                // localStorage["dataToStore"] = JSON.stringify(bodyData);
                localStorage["lastActionToStore"] = "copy";
                localStorage["countToStore"] = count;
                console.log(divStorage);
                console.log("WHO THIS ID", new_copy.attr('id'));
                console.log("WHO THIS", new_copy);
                console.log($('#'+new_copy.attr('id')).find("input:checked").val());
                localStorage["copy_default_value"]="";
                $('#'+new_copy.attr('id')).find("input:checked").each(function(){
                    localStorage["copy_default_value"] += $(this).val()+',';
                })
                console.log(localStorage["copy_default_value"]);

                // localStorage["copy_default_value"]=$('#'+new_copy.attr('id')).find("input:checked").val();
            }
        },
        
        paste: function () {
            var pasted_elements = $();


            if ($('#popup_container').length <= 0 && $('.ideoverlay').length <= 0 && localStorage.getItem("variableToStore") !== null && $('.formbuilder_ws').exists()) {
                if (!(pathname == "/user_view/formbuilder")) {
                    var newAlert = new jAlert("Copy/Cut/Paste is not allowed.", "", "", "", "", function () {
                    });
                    newAlert.themeAlert("jAlert2", {
                        'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                    });
                    return false;
                }
                var divStorage = $('<div class="clone-storage"></div>');
                var variableToStore = unescape(localStorage["variableToStore"]);
                $(variableToStore).appendTo(divStorage);
                last_action = localStorage["lastActionToStore"];

                //var bodyDataStored = JSON.parse(localStorage["dataToStore"]);
                history.push({"element": $(divStorage.html()).clone()})
                // for(var ctr in bodyDataStored){
                // 	console.log("asdasdasdsad",ctr)
                // 	$('body').data(ctr,bodyDataStored[ctr]);
                // }
                if (count <= localStorage["countToStore"]) {
                    count = Number(localStorage["countToStore"]);

                }
                if (last_action == "copy") {
                    $(".component-ancillary-focus").removeClass("component-ancillary-focus");
                    $(".component-primary-focus").removeClass("component-primary-focus");
                    var l_array = history.length;

                    var copy = history[l_array - 1]["element"];
                    console.log("eeee",copy);


                    var new_copy = copy.clone();
                    console.log("what id from paste", copy.attr("id"));
                    console.log("body data from paste", $('body').data());
                    save_history();

                    pasted_elements = clone_fixers(new_copy).each(function () {
                        $(this).rebindDragObects();

                        if ($(this).find('.setObject').length > 0) {
                            $(this).find('.setObject').each(function () {
                                $(this).rebindDragObects();
                            });
                        }
                    });
                    $(".component-primary-focus").removeClass("component-primary-focus");
                ////---slide menu copy////
                 layerAppend();                
                    ///
                    // console.log("oh eto una?")
                    // var new_default_value= localStorage["copy_default_value"];
                    // $('#setObject_'+new_id).find('input[value="'+ new_default_value +'"]').prop("checked", true);
                    // console.log(localStorage["copy_default_value"].split(","))
                    var def_value = localStorage["copy_default_value"].split(",")
                    jQuery.each(def_value, function(i,d_value){
                        $('#setObject_'+new_id).find('input[value="'+ d_value +'"]').prop("checked", true);
                        // console.log(d_value)
                    })
                    delete $("body").data(new_id).tabIndex;
                    $(".getFields_"+new_id).removeAttr("tabindex");
                    //Pao Remove PickList Duplicate Drag Handler When Copied--//
                    var drag_elem = $('#setObject_'+new_id).find(".setObject-drag-handle");
                    if($('#setObject_'+new_id).attr("data-type")== "pickList"){
                        if(drag_elem.length >= 2){
                            // console.log($('#setObject_'+new_id).attr("data-type"))
                            $('#setObject_'+new_id).find(".setObject-drag-handle").eq(0).remove()
                        }
                    }
                    //--fix for Copied Object With Left Aligned Label can't be resized
                    if($('#setObject_'+new_id).find('.align-left-wrapper').length == 1){
                    $('#setObject_'+new_id).find('.align-left-wrapper').find('.ui-resizable-handle').eq(0).remove();

                    $('#setObject_'+new_id).find("#label_"+new_id).resizable({
                                        handles: "e"
                                    })
                    }

                    //------//
                }
                else if (last_action == "cut") {
                    console.log("paste");
                    $(".component-ancillary-focus").removeClass("component-ancillary-focus");
                    $(".component-primary-focus").removeClass("component-primary-focus");
                    var l_array = history.length;
                    var copy = history[l_array - 1]["element"];
                    var new_copy = copy.clone();
                    //save_history();
                    if (context_menu_config == 1) {
                        $(document).contextmenu("enableEntry", "paste", false);
                    }

                    pasted_elements = cut_fixer(new_copy).each(function () {

                        $(this).rebindDragObects();

                        if ($(this).find('.setObject').length > 0) {
                            $(this).find('.setObject').each(function () {
                                $(this).rebindDragObects();
                            });
                        }

                    });
                    // var new_default_value= localStorage["copy_default_value"];
                    // $('#setObject_'+new_id).find('input[value="'+ new_default_value +'"]').prop("checked", true);
                    
                    
                    history = [];
                    localStorage["variableToStore"] = "";
                     layerAppend();   
                }

            } 
            pasted_elements.each(function(){
                var self = $(this);
                if(self.is('[data-temp-doid]')){
                    self.removeAttr('data-temp-doid');
                }
            });
        },
        cut: function () {

            if ($('#popup_container').length <= 0 && $('.ideoverlay').length <= 0 && $('.formbuilder_ws').exists()) {
                if (!(pathname == "/user_view/formbuilder")) {
                     var newAlert = new jAlert("Copy/Cut/Paste is not allowed.", "", "", "", "", function () {
                    });
                    newAlert.themeAlert("jAlert2", {
                        'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                    });
                    return false;
                }
                var test = $(this).find('.component-ancillary-focus');
                save_history();
                if ($(this).find('[data-type="button"]').hasClass('component-ancillary-focus')) {
                    $(this).find('[data-type="button"]').removeClass('component-ancillary-focus');
                    $(this).find('[data-type="button"]').removeClass('component-primary-focus');
                }
                if ($(this).find('[data-type="tab-panel"]').hasClass('component-ancillary-focus')) {
                    $(this).find('[data-type="tab-panel"]').children('.fields_below').find('.setObject').removeClass('component-ancillary-focus');
                    $(this).find('[data-type="tab-panel"]').children('.fields_below').find('.setObject').removeClass('component-primary-focus');
                }
                if ($(this).find('[data-type="table"],[data-type="accordion"]').hasClass('component-ancillary-focus')) {
                    $(this).find('[data-type="table"],[data-type="accordion"]').children('.fields_below').find('.setObject').removeClass('component-ancillary-focus');
                }
                $(this).find('[data-type="table"],[data-type="accordion"]').children('.fields_below').find('.setObject').removeClass('component-primary-focus');


                test.each(function () {

                    var ele_name = $(this).find('[name]').eq(0).filter(function (index) {
                        return !$(this).parent().is('form');
                    }).attr("name");

                    existing_fnames[ele_name] = false;
                    var doid = "";
                    var data_val = "";
                    if ($(this).find('.setObject').length > 0) {
                        $(this).find('.setObject').each(function () {
                            var ele_name = $(this).find('[name]').eq(0).filter(function (index) {
                                return !$(this).parent().is('form');
                            }).attr("name");

                            existing_fnames[ele_name] = false;
                            doid = $(this).attr('data-object-id');
                            if ($.type($('body').data(doid)) != "undefined") {
                                data_val = JSON.stringify($('body').data(doid));
                                $(this).attr("data-temp-doid", data_val);

                            }
                        });
                    }
                    else {

                        doid = $(this).attr('data-object-id');
                        if ($.type($('body').data(doid)) != "undefined") {
                            data_val = JSON.stringify($('body').data(doid));
                            $(this).attr("data-temp-doid", data_val);
                        }
                    }
                    console.log("bodyid", data_val)



                    var scrollTop = $(document).scrollTop();
                    var scrollLeft = $(document).scrollLeft();
                    var originalTop = $(this).offset().top - $('.formbuilder_ws').offset().top;
                    //var originalTop = $(this).css('top');
                    var originalLeft = $(this).offset().left - $('.formbuilder_ws').offset().left;

                    $(this).attr("data-original-scroll-top", scrollTop);
                    $(this).attr("data-original-top", originalTop);
                    $(this).attr("data-original-left", originalLeft);
                    $(this).attr("data-original-scroll-left", scrollLeft);
                    $('.tooltip').remove();


                });
                var new_copy = test.clone();
                // console.log("new_copy", new_copy.attr("id").replace(/[^0-9]/g, ""));
                var cut_obj_id= $(new_copy).attr("id").replace(/[^0-9]/g, "");
                $("#layer_"+cut_obj_id).remove();
                var divStorage = $('<div class="clone-storage"></div>');
                new_copy.appendTo(divStorage);
                localStorage["variableToStore"] = escape(divStorage.html());
                //history.push({"element":new_copy});
                // localStorage["dataToStore"] = JSON.stringify(bodyData);
                localStorage["lastActionToStore"] = "cut";
                localStorage["countToStore"] = count;
                localStorage["copy_default_value"]=$('#'+new_copy.attr('id')).find("input:checked").val();

                //history.push({"element":new_copy});

                test.remove();




            }




            
        },
    });
}
function WorkspaceFocus(){
    $(document).on("click",function(e){
        var element_clicked = $(e.target);
        if(element_clicked.is(".formbuilder_ws") || element_clicked.parents(".formbuilder_ws").length >= 1){
            $('.formbuilder_ws').addClass("formbuilder-ws-active");
          
        }else{
            $('.formbuilder_ws').removeClass("formbuilder-ws-active");

        }
    });
}

function bindMergeUnmerge() {
    $('.relative-grid-merge').on({
        "click.bindMergeUnmerge.mergeCell": function () {
            var self_dis_ele = $(this);
            console.log('bindMergeUnmerge', self_dis_ele);
            var highlighted_data = self_dis_ele.data('highlight_data');
            console.log("highlighted_data", highlighted_data)
            var newConfirm = new jConfirm("WARNING: Merging cell only keeps the upper left cell content, and discards the other content.<br/>Are you sure you want to continue?", 'Confirmation Dialog', '', '', '', function (r) {
                if (r == true) {
                    console.log(highlighted_data['highlightedCells'].eq(0), highlighted_data['highlightedCells'].not(':eq(0)'));
                    if (highlighted_data['colspanData'] >= 1) {
                        highlighted_data['highlightedCells'].eq(0).attr('colspan', highlighted_data['colspanData']);
                    }
                    if (highlighted_data['rowspanData'] >= 1) {
                        highlighted_data['highlightedCells'].eq(0).attr('rowspan', highlighted_data['rowspanData']);
                    }
                    // highlighted_data['highlightedCells'].not(':eq(0)').data('source_merge_cell');
                    highlighted_data['highlightedCells'].not(':eq(0)').hide();
                    highlighted_data['highlightedCells'].not(':eq(0)').children('.td-relative').children('.setObject').remove();
                    highlighted_data['highlightedCells'].eq(0).data('affected_cells', highlighted_data['highlightedCells'].not(':eq(0)'));
                    $('.relative-grid-unmerge').show();
                    $('.relative-grid-merge').hide();
                } else {
                    //WAG IMERGE
                }
            });
            newConfirm.themeConfirm("confirm2", {
                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
            });
        }
    });
    $('.relative-grid-unmerge').on({
        "click.bindMergeUnmerge.unMergeCell": function () {

            var selected_cell = $('.dynamic-td-droppable-selected').filter('[colspan],[rowspan]');
            var collect_affected_cells = $();
            selected_cell.each(function () {
                var this_selected_cell = $(this);
                var affected_cells = this_selected_cell.data('affected_cells');
                if (affected_cells) {
                    console.log("this_selected_cell", this_selected_cell, this_selected_cell.data('affected_cells'));

                    this_selected_cell.removeAttr('colspan');
                    this_selected_cell.removeAttr('rowspan');

                    affected_cells.show();

                    collect_affected_cells = collect_affected_cells.add(affected_cells);

                    this_selected_cell.removeData('affected_cells');
                }
            });
            // $('.dynamic-td-droppable-selected').removeClass('dynamic-td-droppable-selected');
            collect_affected_cells.addClass('dynamic-td-droppable-selected');

            var highlight_data = mergeHighlightIsValidV2();
            if (highlight_data == true) {
                if (highlight_data['mergeType'] == "merge") {
                    $('.relative-grid-merge').show();
                    $('.relative-grid-unmerge').hide();
                    $('.relative-grid-merge').data("highlight_data", highlight_data);
                } else if (highlight_data['mergeType'] == "unmerge") {
                    $('.relative-grid-unmerge').show();
                    $('.relative-grid-merge').hide();
                }
            } else {
                $('.relative-grid-merge').hide();
                $('.relative-grid-unmerge').hide();
                $('.relative-grid-merge').removeData('highlight_data');
            }
        }
    });
}
function mergeHighlightIsValidV2() {
    var status_validation = false;
    var merge_type = "merge";
    var collect_row_cell_indexes = {};
    var colspan_ctr = [];
    var rowspan_ctr = [];
    var collect_doi_table = [];
    var highlighted_eles = $('.dynamic-td-droppable-selected');

    if (highlighted_eles.filter('[colspan]').length >= 1 || highlighted_eles.filter('[rowspan]').length >= 1) { // if there is an 
        merge_type = "unmerge";
        status_validation = true;
    } else if (highlighted_eles.length >= 2) {
        highlighted_eles.each(function () {
            var dis_self = $(this);
            var row_ele = dis_self.parents('tr').eq(0);
            var td_ele = dis_self;
            if (collect_row_cell_indexes[row_ele.index()]) {
                collect_row_cell_indexes[row_ele.index()].push(td_ele.index());
            } else {
                collect_row_cell_indexes[row_ele.index()] = [td_ele.index()];
            }
            collect_doi_table.push(dis_self.parents('.setObject[data-type="table"][data-object-id]').eq(0).attr('data-object-id'));
        });
        {//checking if the highlight cell was desame table

            collect_doi_table = $.unique(collect_doi_table);
            // console.log("collect_doi_table",collect_doi_table);
            if (collect_doi_table.length == 1) {
                status_validation = true;
            } else {
                status_validation = false;
            }
        }
        if (status_validation == true) {//checking equality of each cell index in a row if highlighted cells are in the desame table
            var temporary_array = null;
            var temp_first_cell_in_row_ctr = null;
            var temp_first_row_index_ctr = null;
            for (var ctr in collect_row_cell_indexes) {
                rowspan_ctr.push(ctr);
                console.log(temp_first_row_index_ctr, Number(ctr));
                if (temp_first_row_index_ctr == null) {
                    temp_first_row_index_ctr = Number(ctr); // getting the first index of row 
                    temp_first_row_index_ctr++; //getting previuos index 
                    status_validation = true;
                } else if (temp_first_row_index_ctr == Number(ctr)) { //must be consecutive
                    temp_first_row_index_ctr++; //getting previuos index 
                    status_validation = true;
                } else {
                    status_validation = false;
                    break;
                }

                if (temporary_array == null) {//checking the first row if highlighted indexes are consecutives

                    temporary_array = collect_row_cell_indexes[ctr];
                    if (temporary_array.length >= 1) {
                        temp_first_cell_in_row_ctr = temporary_array[0];
                        // console.log("temp_first_cell_in_row_ctr",temporary_array)
                    }


                    for (var ctr2 in temporary_array) {
                        colspan_ctr.push(ctr2);
                        // console.log(temp_first_cell_in_row_ctr, " == ", temporary_array[ctr2])
                        if (temp_first_cell_in_row_ctr == temporary_array[ctr2]) { // if ung unang row ay walang laktaw na highlight //must be consecutive
                            temp_first_cell_in_row_ctr++;
                            status_validation = true;
                        } else {
                            status_validation = false;
                            break;
                        }
                    }
                    // alert(status_validation )
                    if (status_validation == false) {
                        break;
                    } else {
                        status_validation = true;
                    }

                } else if ($(temporary_array).not(collect_row_cell_indexes[ctr]).length == 0 && $(collect_row_cell_indexes[ctr]).not(temporary_array).length == 0) { //condition if the two arrays have the same value whether it isn't ordered bec values can be sorted
                    temporary_array = collect_row_cell_indexes[ctr];
                    status_validation = true;
                } else if ($(temporary_array).not(collect_row_cell_indexes[ctr]).length >= 1 || $(collect_row_cell_indexes[ctr]).not(temporary_array).length >= 1) {
                    status_validation = false;
                    break;
                }
            }
        }

        // console.log(collect_row_cell_indexes)
    }


    return {
        "collectedHighlightData": collect_row_cell_indexes,
        "highlightedCells": highlighted_eles,
        "objectTableIDS": collect_doi_table,
        "mergeType": merge_type,
        "rowspanData": rowspan_ctr.length,
        "colspanData": colspan_ctr.length,
        "valueOf": function () {
            return status_validation;
        }
    };
}
function bindCellArrayLength() {
    if ($(".fcp_cell_length").length >= 1) {
        if ($(".fcp-cell-pane-columns").length >= 1/* && $(".fcp-cell-pane-columns").is(":visible")*/) {

            $(".fcp-cell-pane-columns").on({
                "change.evtCellArrayLength": function (e) {
                    if ($(".table-form-design-relative").length == 1) {
                        var value_set = Number($(this).val());
                        var table_fdr = $(".table-form-design-relative");
                        var column_button = table_fdr.find(".action-column-add").eq(0).children(".table-add-column");
                        var fdr_form_table = table_fdr.find(".form-table").eq(0);
                        var fdr_tbody = fdr_form_table.children("tbody").eq(0);

                        var cols_existing = fdr_tbody.children("tr").eq(0).children("td:visible").length;


                        {//if there is any kind of merges/colspan
                            var colspan_existing = 0;
                            // colspan_existing = fdr_tbody.children("tr").eq(0).children("td[colspan]:visible").length;
                            // var colspan_values_arr = fdr_tbody.children("tr").eq(0).children("td[colspan]:visible").get().map(function(ele,index){
                            //     return Number($(ele).attr("colspan"));
                            // });
                            var colspan_values_total = 0;
                            // for (var cva_index in colspan_values_arr) {
                            //     colspan_values_total = colspan_values_total + colspan_values_arr[cva_index];
                            // };

                        }


                        cols_existing = cols_existing + colspan_values_total - colspan_existing; //process all columns existing including intersection of merges

                        if (value_set == cols_existing) {//nothing
                            //same as the existing
                        } else if (value_set > cols_existing) {//add col
                            var cols_to_add = value_set - cols_existing;
                            for (var ctr = 1; ctr <= cols_to_add; ctr++) {
                                column_button.trigger("click");
                            }
                        } else if (value_set < cols_existing) {//remove col
                            console.log("cols_existing - value_set", cols_existing, value_set);
                            var cols_to_remove = cols_existing - value_set;
                            var reverse_order_td_col_btn_container = fdr_tbody.children("tr").eq(0).children("td:visible").get().reverse();
                            var total_columns = reverse_order_td_col_btn_container.length;
                            // var collect_remove_buttons = $();
                            // console.log("reverse_order_td_col_btn_container",reverse_order_td_col_btn_container);
                            var obj_length_in_cols_to_delete = $(fdr_tbody.children("tr").children("td").get().reverse()).filter(function (eqi, ele) {
                                if ($(ele).index() >= (total_columns - cols_to_remove)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }).children(".td-relative").children(".setObject").length

                            if (obj_length_in_cols_to_delete >= 1) {
                                var newConfirm = new jConfirm("WARNING: All objects on the last " + cols_to_remove + " columns will be discarded.<br/>Are you sure you want to continue?", 'Confirmation Dialog', '', '', '', function (r) {
                                    if (r == true) {
                                        var ctr = 0;
                                        for (var rotdcbc_index in reverse_order_td_col_btn_container) {
                                            // console.log("TEST",reverse_order_td_col_btn_container[rotdcbc_index])
                                            if (ctr == cols_to_remove) {
                                                break;
                                            }
                                            ctr = ctr + 1;
                                            $(reverse_order_td_col_btn_container[rotdcbc_index]).children(".td-relative").children(".action-column-remove").children(".table-remove-column").trigger("click", [{"auto_remove": true}]);
                                            // $("#popup_ok").trigger("click");
                                        }
                                    } else {
                                        //WAG IDELETE
                                    }
                                });
                                newConfirm.themeConfirm("confirm2", {
                                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                                });
                            } else {
                                var ctr = 0;
                                for (var rotdcbc_index in reverse_order_td_col_btn_container) {
                                    // console.log("TEST",reverse_order_td_col_btn_container[rotdcbc_index])
                                    if (ctr == cols_to_remove) {
                                        break;
                                    }
                                    ctr = ctr + 1;
                                    $(reverse_order_td_col_btn_container[rotdcbc_index]).children(".td-relative").children(".action-column-remove").children(".table-remove-column").trigger("click", [{"auto_remove": true}]);
                                    // $("#popup_ok").trigger("click");
                                }
                            }
                        }
                    }
                }
            })
        }
        if ($(".fcp-cell-pane-rows").length >= 1/*&& $(".fcp-cell-pane-rows").is(":visible")*/) {
            $(".fcp-cell-pane-rows").on({
                "change.evtCellArrayLength": function (e) {
                    if ($(".table-form-design-relative").length == 1) {
                        var value_set = Number($(this).val());
                        var table_fdr = $(".table-form-design-relative");
                        var row_button = table_fdr.find(".table-wrapper").eq(0).children(".action-row-add").eq(0).children(".table-add-row");
                        var fdr_form_table = table_fdr.find(".form-table").eq(0);
                        var fdr_tbody = fdr_form_table.children("tbody").eq(0);

                        var rows_existing = fdr_tbody.children("tr:visible").length;

                        {//if there is an existing merges/rowspan
                            // var rowspan_existing = fdr_tbody.children("tr").eq(0).children("td[colspan]:visible").length;    
                        }

                        if (value_set == rows_existing) {
                            //do nothing if the same as the existing rows
                        } else if (value_set > rows_existing) { //add rows
                            //
                            var rows_to_add = value_set - rows_existing;
                            for (var ctr = 1; ctr <= rows_to_add; ctr++) {
                                row_button.trigger("click");
                            }
                        } else if (value_set < rows_existing) { //remove rows
                            //
                            var rows_to_remove = rows_existing - value_set;
                            var reverse_order_tr_row_btn_container = fdr_tbody.children("tr:visible").children("td").filter(function (eq_index, ele) {
                                return ($(ele).index() == 0);
                            }).get().reverse();

                            var total_rows = reverse_order_tr_row_btn_container.length;
                            // var collect_remove_buttons = $();
                            // console.log("reverse_order_td_col_btn_container",reverse_order_td_col_btn_container);
                            var obj_length_in_rows_to_delete = $(fdr_tbody.children("tr").get().reverse()).filter(function (eqi, ele) {
                                if ($(ele).index() >= (total_rows - rows_to_remove)) {
                                    return true;
                                } else {
                                    return false;
                                }
                            }).children(".td-relative").children(".setObject").length

                            if (obj_length_in_rows_to_delete >= 1) {
                                var newConfirm = new jConfirm("WARNING: All objects on the last " + rows_to_remove + " columns will be discarded.<br/>Are you sure you want to continue?", 'Confirmation Dialog', '', '', '', function (r) {
                                    $('#popup_container').find('.fa.fa-exclamation-triangle').eq(0).remove();
                                    $('#popup_container').find('.fl-footDialog').eq(0).remove();
                                    if (r == true) {
                                        var ctr = 0;
                                        for (var rotrrbc_index in reverse_order_tr_row_btn_container) {
                                            if (ctr == rows_to_remove) {
                                                break;
                                            }
                                            ctr = ctr + 1;
                                            $(reverse_order_tr_row_btn_container[rotrrbc_index]).children(".td-relative").children(".action-row-remove").children(".table-remove-row").trigger("click", [{"auto_remove": true}]);
                                        }
                                        ;
                                    } else {
                                        //WAG IDELETE
                                    }
                                });
                                newConfirm.themeConfirm("confirm2", {
                                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                                });
                            } else {
                                var ctr = 0;
                                for (var rotrrbc_index in reverse_order_tr_row_btn_container) {
                                    if (ctr == rows_to_remove) {
                                        break;
                                    }
                                    ctr = ctr + 1;
                                    $(reverse_order_tr_row_btn_container[rotrrbc_index]).children(".td-relative").children(".action-row-remove").children(".table-remove-row").trigger("click", [{"auto_remove": true}]);
                                }
                                ;
                            }


                        }
                    }
                }
            });
        }
    }
}
function bindCellGridSize() {
    if ($(".fcp-cell-width-size").length >= 1 /*&& $(".fcp-cell-width-size").is(":visible")*/) {
        $(".fcp-cell-width-size").on({
            "change": function (e) {
                if ($(".table-form-design-relative").length >= 1) {
                    var value_set = $(this).val();
                    var fcp_td_rel = $(".fcp-td-relative").get();
                    for (var ctr_i in fcp_td_rel) {
                        $(fcp_td_rel[ctr_i]).css("min-width", (value_set) + "px");
                        $(fcp_td_rel[ctr_i]).parent().css("width", (value_set) + "px");
                    }
                }
            }
        })
    }
    if ($(".fcp-cell-height-size").length >= 1 /*&& $(".fcp-cell-height-size").is(":visible")*/) {
        $(".fcp-cell-height-size").on({
            "change": function () {
                if ($(".table-form-design-relative").length >= 1) {
                    var value_set = $(this).val();
                    var fcp_td_rel = $(".fcp-td-relative").get();
                    for (var ctr_i in fcp_td_rel) {
                        $(fcp_td_rel[ctr_i]).css("min-height", (value_set) + "px");
                        $(fcp_td_rel[ctr_i]).parent().css("height", (value_set) + "px");
                    }
                }
            }
        })
    }
}
function saveAbsoluteBehavior(elements_to_save) {
    elements_to_save.not(".table-form-design-relative").each(function () {
        var self = $(this);

        $(this).data("absolute_behavior", {
            "position": self.css("position"),
            "top": self.css("top"),
            "left": self.css("left")
        });
    })
}
function initializeTable() {
    var form_objects = $(".workspace.formbuilder_ws").children(".setObject");
    var flag_if_table_exist = false;
    saveAbsoluteBehavior(form_objects);
    var last_doi = (typeof count == "undefined") ? 0 : count;

    if ($('.table-form-design-relative').length >= 1) {
        flag_if_table_exist = true;
        var form_design_relative_table_ele = $('.table-form-design-relative');
    } else {
        $('.getObjects[data-object-type="table"]').trigger("click")
        var form_design_relative_table_ele = $('.setObject[data-object-id="' + last_doi + '"]');
    }
    if (!form_design_relative_table_ele.parent().hasClass("formbuilder_ws")) {
        form_design_relative_table_ele.appendTo($(".formbuilder_ws"));
    }
    form_design_relative_table_ele.css({
        "top": (0) + "px",
        "left": (0) + "px",
        "height": "auto"
    });
    form_design_relative_table_ele.attr("style", form_design_relative_table_ele.attr("style") + ";position:relative !important");
    form_design_relative_table_ele.find('.form-table').eq(0).attr("fld-table-container-responsive", "true").attr("border-visible", "No");
    form_design_relative_table_ele.addClass("table-form-design-relative")

    form_design_relative_table_ele.off("mousedown.fieldObjectFocus");
    // .css('box-shadow','0px 0px 10px black');
    form_design_relative_table_ele.off("dblclick.doubleClickProp");
    if (form_design_relative_table_ele.data()["uiDraggable"]) {
        form_design_relative_table_ele.draggable("destroy");
    }
    form_design_relative_table_ele.children(".fields_below").children(".setObjects_actions").remove();
    form_design_relative_table_ele.children(".fields_below").children(".obj_label").remove();


    if ($('.fcp_cell_length').length >= 2 && $('.fcp_cell_length').is(":visible")) {
        var cell_length_ele = $('.fcp_cell_length');

        var cols_existing_length = form_design_relative_table_ele.find('.form-table').children('tbody').children('tr').eq(0).children("td").length;
        var rows_existing_length = form_design_relative_table_ele.find('.form-table').children('tbody').children('tr').length;

        // alert("Columns "+cell_length_ele.filter(".fcp-cell-pane-columns").val())
        // alert("cols_existing_length "+cols_existing_length)
        var cols_length = Number(cell_length_ele.filter(".fcp-cell-pane-columns").val()) - cols_existing_length;
        var rows_length = Number(cell_length_ele.filter(".fcp-cell-pane-rows").val()) - rows_existing_length;

        var button_row_add = form_design_relative_table_ele.find(".action-row-add").eq(0).children(".table-add-row");
        var button_col_add = form_design_relative_table_ele.find(".action-column-add").eq(0).children(".table-add-column");

        button_row_add.addClass("fcp-table-add-row");
        button_col_add.addClass("fcp-table-add-column");

        button_col_add.on({
            "click.formCellPaneBehavior": function () {
                var fcp_cell_size_ele = $(".fcp-cell-size");
                if (fcp_cell_size_ele.length >= 1 /*&& fcp_cell_size_ele.is(":visible")*/) {
                    var fcp_cell_width_size = Number(fcp_cell_size_ele.filter(".fcp-cell-width-size").val());
                    var fcp_cell_height_size = Number(fcp_cell_size_ele.filter(".fcp-cell-height-size").val());
                    $(this).parents(".table-wrapper").eq(0).find(".form-table").eq(0)
                            .children("tbody").children("tr").each(function () {
                        $(this).children("td").last().css({
                            "width": (fcp_cell_width_size) + "px",
                            "height": (fcp_cell_height_size) + "px"
                        }).children(".td-relative").eq(0).css({
                            "min-width": (fcp_cell_width_size) + "px",
                            "min-height": (fcp_cell_height_size) + "px"
                        }).addClass("fcp-td-relative")
                    })
                }
            }
        });
        button_row_add.on({
            "click.formCellPaneBehavior": function () {
                var fcp_cell_size_ele = $(".fcp-cell-size");
                if (fcp_cell_size_ele.length >= 1 /*&& fcp_cell_size_ele.is(":visible")*/) {
                    var fcp_cell_width_size = Number(fcp_cell_size_ele.filter(".fcp-cell-width-size").val());
                    var fcp_cell_height_size = Number(fcp_cell_size_ele.filter(".fcp-cell-height-size").val());
                    $(this).parents(".table-wrapper").eq(0).find(".form-table").eq(0)
                            .children("tbody").children("tr").last().children("td").each(function () {
                        $(this).css({
                            "width": (fcp_cell_width_size) + "px",
                            "height": (fcp_cell_height_size) + "px"
                        }).children(".td-relative").eq(0).css({
                            "min-width": (fcp_cell_width_size) + "px",
                            "min-height": (fcp_cell_height_size) + "px"
                        }).addClass("fcp-td-relative");
                    });
                }
            }
        });
        if (flag_if_table_exist == false) {
            for (var c_cols = 1; c_cols <= cols_length; c_cols++) {
                button_col_add.trigger("click");
            }
            for (var c_rows = 1; c_rows <= rows_length; c_rows++) {
                button_row_add.trigger("click");
            }
        }
    }
    if ($(".fcp-cell-size").length >= 2 /*&& $(".fcp-cell-size").is(":visible")*/ && flag_if_table_exist == false) {
        var fcp_cell_size_ele = $(".fcp-cell-size");
        var fcp_cell_width_size = Number(fcp_cell_size_ele.filter(".fcp-cell-width-size").val());
        var fcp_cell_height_size = Number(fcp_cell_size_ele.filter(".fcp-cell-height-size").val());
        var fcp_tr = form_design_relative_table_ele.find(".form-table").eq(0).children("tbody");
        var fcp_selected_tds = fcp_tr.children("tr").children("td").get();
        for (var ctr in fcp_selected_tds) {
            $(fcp_selected_tds[ctr]).css("width", (fcp_cell_width_size) + "px");
            $(fcp_selected_tds[ctr]).css("height", (fcp_cell_height_size) + "px");
            $(fcp_selected_tds[ctr]).children(".td-relative").eq(0).css("min-width", (fcp_cell_width_size) + "px");
            $(fcp_selected_tds[ctr]).children(".td-relative").eq(0).css("min-height", (fcp_cell_height_size) + "px");
            $(fcp_selected_tds[ctr]).children(".td-relative").eq(0).addClass("fcp-td-relative");
        }
        /*.css({
         "width":(fcp_cell_width_size)+"px",
         "height":(fcp_cell_height_size)+"px"
         });*/
    }


    $('.formbuilder_ws').data("affected_element_for_initializeTable", $('.formbuilder_ws').parents().filter(function (eqi, ele) {
        if (eqi >= 2 && eqi <= 5) {
            return true;
        } else {
            return false;
        }
    }).add($('.formbuilder_ws')))

    $('.formbuilder_ws').data("affected_element_for_initializeTable").each(function () {

        // if($(this).hasClass("formbuilder_ws")){
        //     $(this).data("origin_save_for_initializeTable",{
        //         "width":$(this).css("width"),
        //         "height":$(this).css("height"),
        //         "display":$(this).css("display")
        //     });
        // }else{
        $(this).data("origin_save_for_initializeTable", {
            "width": $(this).css("width"),
            "height": $(this).css("height"),
            "display": $(this).css("display")
        });
        // }


        $(this).css({
            "width": "auto",
            "height": "auto"
        });
    });
    // console.log("CRAP",$('.formbuilder_ws').data("affected_element_for_initializeTable"))
    $('.formbuilder_ws').data("affected_element_for_initializeTable").filter('.formbuilder_ws').css("display", "inline-block").children('.ui-resizable-handle').hide();
    // alert(123)
    return {
        "firstCell": form_design_relative_table_ele.find(".form-table").eq(0).find(".td-relative").eq(0)
    }
}
function tabFunctionality() { //DITO NA KO tabFunctionality
    $('body').on('click', '.add-new-tab-panel', function () {
        // console.log("DITO NA SA KABILA",$(this).data("added_tab_element"));
        // alert(count)
        $('.getObjects[data-object-type="table"]').trigger('click', [{"auto": true, "autoAddTab": $(this).data("added_tab_element")}]);
        $(this).parents('.setObject[data-type="tab-panel"]').eq(0);
        $('.fl-closeDialog').click();
    });
}
function removeRelativeTable() {
    //table-form-design-relative
    var relative_table = $('.table-form-design-relative');
    if (relative_table.length >= 1) {
        if ($('.fcp-td-relative').length >= 1) {
            $('.fcp-td-relative').children(".setObject").appendTo($('.formbuilder_ws'));
            // $(".workspace.formbuilder_ws").data("affected_element_for_initializeTable").filter(function(eqi,ele) {
            //     if($(ele).hasClass("formbuilder_ws")){
            //         $(ele).css($(ele).data("origin_save_for_initializeTable"));
            //     }
            // });
            $(".workspace.formbuilder_ws").css({
                "width": ($(".workspace.formbuilder_ws").width()) + "px",
                "height": ($(".workspace.formbuilder_ws").height()) + "px",
            });
            $(".workspace.formbuilder_ws").children(".ui-resizable-handle").css("display", "");
            relative_table.remove();
        }
    }
}
function displaceNotesStyle() {
    var elements_existing_in_form = this;
    var existing_notestyle = elements_existing_in_form.filter('[data-type="noteStyleTextarea"]');
    var available_tds = $('.fcp-td-relative:visible').filter(function (eqi, elem) {
        if ($(elem).children('.setObject').length >= 1) {
            return false;
        } else if ($(elem).children('.setObject').length <= 0) {
            return true;
        }
    });
    // console.log("WHOAAHAHHA",existing_notestyle.length , available_tds.length)
    if (existing_notestyle.length < available_tds.length) {
        existing_notestyle.each(function (eqi) {
            $(this).appendTo(available_tds.eq(eqi));
        });
        var form_grid_table_tbody = $('.table-form-design-relative').find(".form-table").eq(0).children("tbody");
        var fgt_rows = form_grid_table_tbody.children("tr").length;
        var fgt_cols = form_grid_table_tbody.children("tr").eq(0).children("td").length;
        $('.fcp-cell-pane-columns').val(fgt_cols);
        $('.fcp-cell-pane-rows').val(fgt_rows);
    } else {
        $(".fcp-table-add-column").trigger("click");
        $(".fcp-table-add-row").trigger("click");
        displaceNotesStyle.call(elements_existing_in_form);
    }
}



function selectFormBehavior() {
    // function formGetElements(){
    //     $(".workspace.formbuilder_ws").children(".setObject");
    // }


    if ($('.show-form-design-select').length >= 1) {
        var form_des_radio_ele = $('.show-form-design-select');
        form_des_radio_ele.on({
            "change": function (event, data_trigger) {
                if (typeof data_trigger == "undefined") {
                    data_trigger = {};
                }
                console.log("data_trigger", data_trigger);
                var self = $(this);
                if (self.val() == "relative") {//absolute to relative

                    $('.form-select.form_size').parents(".fl-option-form-wrapper").eq(0).add($('.form-select.form_size').parents(".fl-option-form-wrapper").eq(0).prev().filter(":contains('Form Size')")).css("display", "none");
                    $('.form-text.form_set_size').parents(".fl-option-form-wrapper").eq(0).add($('.form-text.form_set_size').parents(".fl-option-form-wrapper").eq(0).prev().filter(":contains('Custom Form Size')")).css("display", "none");
                    $('.fcp_cell_length').parents('.fl-option-form-wrapper').eq(0).add($('.fcp_cell_length').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('Form Cell Grid Pane')")).css("display", "");

                    // console.log("ADAS",$('.fcp-cell-size').add($('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('All Cell Grid Size')")))
                    $('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).add($('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('All Cell Grid Size')")).css("display", "");
                    // alert("TSEK"+$('.fcp-cell-size').is(":visible"));
                    // $('.fcp-cell-size').parents(".ps-container").eq(0).scrollTop($('.fcp-cell-size').add($('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('All Cell Grid Size')")).scrollTop());
                    $('.fcp-cell-size').parents(".ps-container").eq(0).perfectScrollbar("update");
                    var elements_existing_in_form = $(".workspace.formbuilder_ws").children(".setObject").not('.show-form-design-select').not('.table-form-design-relative');
                    if (elements_existing_in_form.length >= 1) {
                        var added_message_for_notestyle = "";
                        if (elements_existing_in_form.filter('[data-type="noteStyleTextarea"]').length >= 1) {
                            added_message_for_notestyle = "<br/>Except for notes styled fields, it will served as only field per cell.";
                        }
                        var conf = "NOTE: All field objects will be placed onto the first cell pane." + added_message_for_notestyle + "<br/>Do you still want to continue?";



                        if (data_trigger["auto"]) {
                            var collect_most_right = [];
                            var collect_most_bottom = [];
                            elements_existing_in_form.css({
                                "top": "0px",
                                "left": "0px"
                            });
                            elements_existing_in_form.each(function () {
                                collect_most_right.push($(this).outerWidth());
                                collect_most_bottom.push($(this).outerHeight());
                            });
                            collect_most_right.sort(function (a, b) {
                                return b - a;
                            });
                            collect_most_bottom.sort(function (a, b) {
                                return b - a;
                            });
                            // alert("OHHMY"+$('.fcp-cell-size').is(":visible"))
                            var first_cell = initializeTable()["firstCell"];
                            elements_existing_in_form.filter(':not([data-type="noteStyleTextarea"])').appendTo(first_cell);

                            {//notestyle
                                displaceNotesStyle.call(elements_existing_in_form);
                            }

                            first_cell.parent().css({
                                "width": (collect_most_right[0]) + "px",
                                "height": (collect_most_bottom[0]) + "px"
                            });
                            first_cell.css({
                                "min-width": (collect_most_right[0]) + "px",
                                "min-height": (collect_most_bottom[0]) + "px"
                            })
                        } else {
                            var newConfirm = new jConfirm(conf, 'Confirmation Dialog', '', '', '', function (r) {
                                // alert("TSEK2 "+$('.fcp-cell-size').is(":visible"));
                                if (r == true) {
                                    var collect_most_right = [];
                                    var collect_most_bottom = [];
                                    elements_existing_in_form.css({
                                        "top": "0px",
                                        "left": "0px"
                                    });
                                    elements_existing_in_form.each(function () {
                                        collect_most_right.push($(this).outerWidth());
                                        collect_most_bottom.push($(this).outerHeight());
                                    });
                                    collect_most_right.sort(function (a, b) {
                                        return b - a;
                                    });
                                    collect_most_bottom.sort(function (a, b) {
                                        return b - a;
                                    });
                                    // alert("OHHMY"+$('.fcp-cell-size').is(":visible"))
                                    var first_cell = initializeTable()["firstCell"];
                                    elements_existing_in_form.filter(':not([data-type="noteStyleTextarea"])').appendTo(first_cell);

                                    {//notestyle
                                        displaceNotesStyle.call(elements_existing_in_form);
                                    }

                                    first_cell.parent().css({
                                        "width": (collect_most_right[0]) + "px",
                                        "height": (collect_most_bottom[0]) + "px"
                                    });
                                    first_cell.css({
                                        "min-width": (collect_most_right[0]) + "px",
                                        "min-height": (collect_most_bottom[0]) + "px"
                                    });
                                    $('.ui-droppable.ui-tabs-panel.ui-widget-content[role="tabpanel"]').each(function () {
                                        var self_dis = $(this);
                                        var dis_id = self_dis.attr('id');
                                        // var dis_li_ele = $('a[href="#'+dis_id+'"]');
                                        var dis_li_ele = $('li[aria-controls="' + dis_id + '"][role="tab"]');
                                        $('.getObjects[data-object-type="table"]').trigger('click', [{"auto": true, "autoAddTab": self_dis.add(dis_li_ele)}]);
                                    });
                                } else {
                                    self.parents('.fl-option-form-wrapper').eq(0).find('.show-form-design-select[value="absolute"]').prop("checked", true);
                                    $('.getObjects[data-object-type="noteStyleField"]').hide();
                                    $('.form-select.form_size').parents(".fl-option-form-wrapper").eq(0).add($('.form-select.form_size').parents(".fl-option-form-wrapper").eq(0).prev().filter(":contains('Form Size')")).css("display", "");
                                    $('.form-text.form_set_size').parents(".fl-option-form-wrapper").eq(0).add($('.form-text.form_set_size').parents(".fl-option-form-wrapper").eq(0).prev().filter(":contains('Custom Form Size')")).css("display", "");
                                    $('.fcp_cell_length').parents('.fl-option-form-wrapper').eq(0).add($('.fcp_cell_length').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('Form Cell Grid Pane')")).css("display", "none");
                                    $('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).add($('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('All Cell Grid Size')")).css("display", "none");
                                }
                            });
                            newConfirm.themeConfirm("confirm2", {
                                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                            });
                        }
                    } else {
                        initializeTable()["firstCell"];
                    }


                    $('.getObjects[data-object-type="noteStyleField"]').show();
                } else if (self.val() == "absolute") {//relative to absolute 
                    if ($('.formbuilder_ws').find('[data-type="noteStyleTextarea"]').length >= 1) {
                        var newConfirm = new jConfirm('WARNING: All notes style field will be automatically deleted.', 'Confirmation Dialog', '', '', '', function (r) {
                            // alert("TSEK2 "+$('.fcp-cell-size').is(":visible"));
                            if (r == true) {
                                $('.formbuilder_ws').find('.setObject[data-type="noteStyleTextarea"]').remove();
                                removeRelativeTable();
                                $('.form-select.form_size').parents(".fl-option-form-wrapper").eq(0).add($('.form-select.form_size').parents(".fl-option-form-wrapper").eq(0).prev().filter(":contains('Form Size')")).css("display", "");
                                $('.form-text.form_set_size').parents(".fl-option-form-wrapper").eq(0).add($('.form-text.form_set_size').parents(".fl-option-form-wrapper").eq(0).prev().filter(":contains('Custom Form Size')")).css("display", "");
                                $('.fcp_cell_length').parents('.fl-option-form-wrapper').eq(0).add($('.fcp_cell_length').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('Form Cell Grid Pane')")).css("display", "none");
                                $('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).add($('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('All Cell Grid Size')")).css("display", "none");

                                $('.fdr-table-for-tab-panel').each(function () {
                                    var self_dis = $(this);

                                    var parent_containment_tab = self_dis.parent();

                                    $(this).find('.form-table').eq(0)
                                            .children('tbody')
                                            .children('tr')
                                            .children('td')
                                            .children('.td-relative')
                                            .children('.setObject').appendTo(parent_containment_tab);


                                    if (parent_containment_tab.is(':visible')) {
                                        var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                        var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                        parent_containment_tab.css({
                                            "height": (parent_containment_tab_oh) + "px"
                                        });
                                        parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                            "width": (parent_containment_tab_ow) + "px"
                                        }).children('.ui-resizable-handle').show();
                                    } else {
                                        parent_containment_tab.show();

                                        var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                        var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                        parent_containment_tab.css({
                                            "height": (parent_containment_tab_oh) + "px"
                                        });
                                        parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                            "width": (parent_containment_tab_ow) + "px"
                                        }).children('.ui-resizable-handle').show();
                                        parent_containment_tab.hide();
                                    }
                                    self_dis.remove()
                                });
                            } else {
                                $('.show-form-design-select').filter('[value="relative"]').prop('checked', true)
                            }
                        });
                        newConfirm.themeConfirm("confirm2", {
                            'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                        });
                    } else {

                        removeRelativeTable();
                        $('.form-select.form_size').parents(".fl-option-form-wrapper").eq(0).add($('.form-select.form_size').parents(".fl-option-form-wrapper").eq(0).prev().filter(":contains('Form Size')")).css("display", "");
                        $('.form-text.form_set_size').parents(".fl-option-form-wrapper").eq(0).add($('.form-text.form_set_size').parents(".fl-option-form-wrapper").eq(0).prev().filter(":contains('Custom Form Size')")).css("display", "");
                        $('.fcp_cell_length').parents('.fl-option-form-wrapper').eq(0).add($('.fcp_cell_length').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('Form Cell Grid Pane')")).css("display", "none");
                        $('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).add($('.fcp-cell-size').parents('.fl-option-form-wrapper').eq(0).prev().filter(":contains('All Cell Grid Size')")).css("display", "none");

                        $('.fdr-table-for-tab-panel').each(function () {
                            var self_dis = $(this);

                            var parent_containment_tab = self_dis.parent();

                            $(this).find('.form-table').eq(0)
                                    .children('tbody')
                                    .children('tr')
                                    .children('td')
                                    .children('.td-relative')
                                    .children('.setObject').appendTo(parent_containment_tab);


                            if (parent_containment_tab.is(':visible')) {
                                var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                parent_containment_tab.css({
                                    "height": (parent_containment_tab_oh) + "px"
                                });
                                parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                    "width": (parent_containment_tab_ow) + "px"
                                }).children('.ui-resizable-handle').show();
                            } else {
                                parent_containment_tab.show();

                                var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                parent_containment_tab.css({
                                    "height": (parent_containment_tab_oh) + "px"
                                });
                                parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                    "width": (parent_containment_tab_ow) + "px"
                                }).children('.ui-resizable-handle').show();
                                parent_containment_tab.hide();
                            }
                            self_dis.remove()
                        });
                    }
                    $('.getObjects[data-object-type="noteStyleField"]').hide();
                    $('.getObjects[data-object-type="noteStyleField"]').addClass("isDisplayNone");
                }
            }
        })
    }
}


function selectFormBehaviorV2() {
    var radio_form_design_selection = $('.show-form-design-select:not([name="allow-portal"])');
    
    if (radio_form_design_selection.length >= 1) {
        
        radio_form_design_selection.on({
            
            "change.selectFormBehaviorV2": function () {

                var self = $(this);

                var save_current_interface = [];
                var form_workspace_content_ele = $('.formbuilder_ws').eq(0);
                var table_form_design_relative = $('.table-form-design-relative');
                var container_grid_settings = $('#fl-grid-setting');
                var container_default_form_settings = $('#fl-default-set-size');

                if (self.val() == "relative") {
                    if (table_form_design_relative.length == 0) {
                        //mag aadd ng table_form_design_relative and set 
                        var elements_existing_in_form = $('.formbuilder_ws').children('.setObject');
                        var notestyled_fields = elements_existing_in_form.filter('[data-type="noteStyleTextarea"]');
                        var not_notestyled_table_tab_fields = elements_existing_in_form.filter(':not([data-type="noteStyleTextarea"]):not([data-type="tab-panel"]):not([data-type="table"])');
                        if (elements_existing_in_form.length >= 1) {
                            var added_message_for_notestyle = "";
                            if (notestyled_fields.length >= 1) {
                                added_message_for_notestyle = "<br/>Except for notes styled fields, it will served as only field per cell.";
                            }
                            var conf = "NOTE: All field objects will be placed onto the first cell pane." + added_message_for_notestyle + "<br/>Do you still want to continue?";
                            var newConfirm = new jConfirm(conf, 'Confirmation Dialog', '', '', '', function (r) {
                                if (r == true) {
                                    $('.getObjects[data-object-type="table"]').trigger('click', [{"auto": true, "form_behavior_type": "relative"}]);
                                    var relative_tbl = $('.table-form-design-relative');
                                    var relative_tbl_obj_id = relative_tbl.attr('data-object-id');
                                    relative_tbl.find("[id='obj_actions_"+relative_tbl_obj_id+"']").addClass("isDisplayNone");
                                    if (relative_tbl.length >= 1) {
                                        var rel_tbl_tds = relative_tbl.find('.form-table').eq(0)
                                                .children('tbody').eq(0)
                                                .children('tr').eq(0)
                                                .children('td').eq(0);
                                        rel_tbl_tds.children('.td-relative').eq(0)
                                                .prepend(not_notestyled_table_tab_fields);
                                        not_notestyled_table_tab_fields.css({
                                            "top": "0px",
                                            "left": "0px"
                                        });
                                        var most_right = not_notestyled_table_tab_fields.map(function () {
                                            return $(this).outerWidth();
                                        }).get().sort(function (a, b) {
                                            return b - a;
                                        });
                                        var most_bottom = not_notestyled_table_tab_fields.map(function () {
                                            return $(this).outerHeight();
                                        }).get().sort(function (a, b) {
                                            return b - a;
                                        });
                                        not_notestyled_table_tab_fields.parent().css({
                                            "min-width": (most_right[0]) + "px",
                                            "min-height": (most_bottom[0]) + "px"
                                        }).parent().css({
                                            "width": (most_right[0]) + "px",
                                            "height": (most_bottom[0]) + "px"
                                        }).resizable('option', 'minHeight', most_bottom[0]).resizable('option', 'minWidth', most_right[0]);
                                        console.log("BROOMSKI", most_right, " ", most_bottom)
                                        var available_cell = rel_tbl_tds.filter(function () {
                                            return $(this).children('.td-relative').children('.setObject').length <= 0;
                                        });
                                        $('.formbuilder_ws').css({
                                            'width': 'auto',
                                            'height': 'auto'
                                        });
                                    }
                                    container_grid_settings.removeClass('isDisplayNone');
                                    container_default_form_settings.addClass('isDisplayNone');
                                    $('.getObjects[data-object-type="noteStyleField"]').removeClass('isDisplayNone');
                                } else {
                                    $('.show-form-design-select').filter('[value="absolute"]').prop('checked', true)
                                }
                            });
                            newConfirm.themeConfirm("confirm2", {
                                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                            });
                        } else {
                            $('.getObjects[data-object-type="table"]').trigger('click', [{"auto": true, "form_behavior_type": "relative"}]);
                            $('.formbuilder_ws').css({
                                'width': 'auto',
                                'height': 'auto'
                            });
                            container_grid_settings.removeClass('isDisplayNone');
                            container_default_form_settings.addClass('isDisplayNone');
                            $('.getObjects[data-object-type="noteStyleField"]').removeClass('isDisplayNone');
                            $('.table-form-design-relative').find('.setObjects_actions').remove();
                        }

                    }
                } else if (self.val() == "absolute") {
                    if (table_form_design_relative.length >= 1) {
                        if ($('.formbuilder_ws').find('[data-type="noteStyleTextarea"]').length >= 1) {
                            var newConfirm = new jConfirm('WARNING: All notes style field will be automatically deleted.', 'Confirmation Dialog', '', '', '', function (r) {
                                // alert("TSEK2 "+$('.fcp-cell-size').is(":visible"));
                                if (r == true) {
                                    $('.formbuilder_ws').find('.setObject[data-type="noteStyleTextarea"]').remove();
                                    $('.fcp-td-relative').each(function () {
                                        var self = $(this);
                                        var each_ele = self.children('.setObject');
                                        each_ele.appendTo('.formbuilder_ws');
                                        each_ele.css({
                                            "top": (0) + "px",
                                            "left": (0) + "px"
                                        });

                                    });
                                    $('.fdr-table-for-tab-panel').each(function () {
                                        var self_dis = $(this);

                                        var parent_containment_tab = self_dis.parent();

                                        $(this).find('.form-table').eq(0)
                                                .children('tbody')
                                                .children('tr')
                                                .children('td')
                                                .children('.td-relative')
                                                .children('.setObject').appendTo(parent_containment_tab);


                                        if (parent_containment_tab.is(':visible')) {
                                            var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                            var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                            parent_containment_tab.css({
                                                "height": (parent_containment_tab_oh) + "px"
                                            });
                                            parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                                "width": (parent_containment_tab_ow) + "px"
                                            }).children('.ui-resizable-handle').show();
                                        } else {
                                            parent_containment_tab.show();

                                            var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                            var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                            parent_containment_tab.css({
                                                "height": (parent_containment_tab_oh) + "px"
                                            });
                                            parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                                "width": (parent_containment_tab_ow) + "px"
                                            }).children('.ui-resizable-handle').show();
                                            parent_containment_tab.hide();
                                        }
                                        self_dis.remove();
                                    });
                                    var set_object_ele = $('.table-form-design-relative').eq(0);
                                    var o_width = set_object_ele.outerWidth();
                                    var o_height = set_object_ele.outerHeight();
                                    $('.formbuilder_ws').css({
                                        "width": (o_width) + "px",
                                        "height": (o_height) + "px"
                                    }).children('.ui-resizable-handle').show();
                                    set_object_ele.remove();
                                    container_grid_settings.addClass('isDisplayNone');
                                    container_default_form_settings.removeClass('isDisplayNone');
                                    $('.getObjects[data-object-type="noteStyleField"]').addClass('isDisplayNone');
                                } else {
                                    $('.show-form-design-select').filter('[value="relative"]').prop('checked', true)
                                }
                            });
                            newConfirm.themeConfirm("confirm2", {
                                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                            });
                        } else {
                            var td_cell_of_grid_form_tbl = $('.fcp-td-relative');
                            if (td_cell_of_grid_form_tbl.children('.setObject').length >= 1) {
                                var newConfirm = new jConfirm('TAKE NOTE: All objects you created will be placed on upper left corner of the formbuilder.', 'Confirmation Dialog', '', '', '', function (r) {
                                    if (r == true) {
                                        $('[data-object-type="noteStyleField"]').addClass('isDisplayNone');
                                        $('.fcp-td-relative').each(function () {
                                            var self = $(this);
                                            var each_ele = self.children('.setObject');
                                            each_ele.appendTo('.formbuilder_ws');
                                            each_ele.css({
                                                "top": (0) + "px",
                                                "left": (0) + "px"
                                            });

                                        });
                                        $('.fdr-table-for-tab-panel').each(function () {
                                            var self_dis = $(this);

                                            var parent_containment_tab = self_dis.parent();

                                            $(this).find('.form-table').eq(0)
                                                    .children('tbody')
                                                    .children('tr')
                                                    .children('td')
                                                    .children('.td-relative')
                                                    .children('.setObject').appendTo(parent_containment_tab);


                                            if (parent_containment_tab.is(':visible')) {
                                                var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                                var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                                parent_containment_tab.css({
                                                    "height": (parent_containment_tab_oh) + "px"
                                                });
                                                parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                                    "width": (parent_containment_tab_ow) + "px"
                                                }).children('.ui-resizable-handle').show();
                                            } else {
                                                parent_containment_tab.show();

                                                var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                                var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                                parent_containment_tab.css({
                                                    "height": (parent_containment_tab_oh) + "px"
                                                });
                                                parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                                    "width": (parent_containment_tab_ow) + "px"
                                                }).children('.ui-resizable-handle').show();
                                                parent_containment_tab.hide();
                                            }
                                            self_dis.remove()
                                        });
                                        var set_object_ele = $('.table-form-design-relative').eq(0);
                                        var o_width = set_object_ele.outerWidth();
                                        var o_height = set_object_ele.outerHeight();
                                        $('.formbuilder_ws').css({
                                            "width": (o_width) + "px",
                                            "height": (o_height) + "px"
                                        }).children('.ui-resizable-handle').show();
                                        set_object_ele.remove();

                                        container_grid_settings.addClass('isDisplayNone');
                                        container_default_form_settings.removeClass('isDisplayNone');
                                        $('.getObjects[data-object-type="noteStyleField"]').addClass('isDisplayNone');
                                    } else {
                                        $('.show-form-design-select').filter('[value="relative"]').prop('checked', true)
                                    }
                                });
                                newConfirm.themeConfirm("confirm2", {
                                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                                });
                            } else {
                                $('.fcp-td-relative').each(function () {
                                    var self = $(this);
                                    var each_ele = self.children('.setObject');
                                    each_ele.appendTo('.formbuilder_ws');
                                    each_ele.css({
                                        "top": (0) + "px",
                                        "left": (0) + "px"
                                    });

                                });
                                $('.fdr-table-for-tab-panel').each(function () {
                                    var self_dis = $(this);

                                    var parent_containment_tab = self_dis.parent();

                                    $(this).find('.form-table').eq(0)
                                            .children('tbody')
                                            .children('tr')
                                            .children('td')
                                            .children('.td-relative')
                                            .children('.setObject').appendTo(parent_containment_tab);


                                    if (parent_containment_tab.is(':visible')) {
                                        var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                        var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                        parent_containment_tab.css({
                                            "height": (parent_containment_tab_oh) + "px"
                                        });
                                        parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                            "width": (parent_containment_tab_ow) + "px"
                                        }).children('.ui-resizable-handle').show();
                                    } else {
                                        parent_containment_tab.show();

                                        var parent_containment_tab_oh = parent_containment_tab.outerHeight();
                                        var parent_containment_tab_ow = parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).outerWidth();
                                        parent_containment_tab.css({
                                            "height": (parent_containment_tab_oh) + "px"
                                        });
                                        parent_containment_tab.parents('.setObject[data-type="tab-panel"]').eq(0).css({
                                            "width": (parent_containment_tab_ow) + "px"
                                        }).children('.ui-resizable-handle').show();
                                        parent_containment_tab.hide();
                                    }
                                    self_dis.remove()
                                });
                                var set_object_ele = $('.table-form-design-relative').eq(0);
                                var o_width = set_object_ele.outerWidth();
                                var o_height = set_object_ele.outerHeight();
                                $('.formbuilder_ws').css({
                                    "width": (o_width) + "px",
                                    "height": (o_height) + "px"
                                }).children('.ui-resizable-handle').show();
                                set_object_ele.remove();

                                container_grid_settings.addClass('isDisplayNone');
                                container_default_form_settings.removeClass('isDisplayNone');
                                $('.getObjects[data-object-type="noteStyleField"]').addClass('isDisplayNone');
                            }
                        }
                    }
                }
            }
        })
    }
}


function NotifyMe(element_affected, message, uid_name) {
    if (typeof message == "undefined") {
        //catch errr
        message = '';
    }
    var this_element = $(element_affected);
    var this_id_object_data = $(element_affected).attr("data-object-id");
    var this_element_name = $(element_affected).attr("name");
    if (!this_element_name) {
        this_element_name = uid_name;
    }
    var element_container = this_element.parent();
    var element_ptop = this_element.position().top;
    var element_pleft = this_element.position().left;
    var element_pright = this_element.position().left + this_element.outerWidth();
    var element_pbottom = this_element.position().top + this_element.outerHeight();
    var noti_ele = $(
            '<div class="NM-notification" nm-unique-attr="' + this_element_name + this_id_object_data + '" style="z-index:500;padding:5px;"><div class="arrow-left"></div>' + message + '</div>'
            )
    if(this_element.is('[type="radio"]')||this_element.is('[type="checkbox"]')){
        this_element.parent().parent().addClass("NM-redborder");
        element_container = element_container.parent();
    }
    this_element.addClass("NM-redborder");
    this_element.attr("autofocus","autofocus");
    if (element_container.children(".NM-notification[nm-unique-attr='" + this_element_name + this_id_object_data + "']").length == 0) {
        element_container.append(noti_ele);
        $(noti_ele).css({
            "top": (element_ptop) + "px",
            "left": (element_pright + 5) + "px",
            "padding": "8px",
            "color": "#ffffff",
            "text-align": "center",
            "text-decoration": "none",
            "position": "absolute",
            "-webkit-border-radius": "4px",
            "-moz-border-radius": "4px",
            "border-radius": "4px",
            "background-color": "#000000",
            "z-index": (500)
        });
        $(noti_ele).children(".arrow-left").eq(0).css({
            "width": "0px",
            "height": "0px",
            "border-top": "10px solid transparent",
            "border-right": "10px solid #000000",
            "position": "absolute",
            "top": "5px",
            "left": "0px",
            "margin-left": "-5px"
        });
        $(noti_ele).hide();
        var mouse_entered = "not yet";
        var event_name_mouseenter = "mouseenter.EventNotifyMe" + this_id_object_data;
        // var event_name_focusin = "focusin.EventNotifyMe";

        noti_ele.on(event_name_mouseenter, function () {
            if (mouse_entered == "not yet") {
                noti_ele.fadeOut(2500, function () {
                    $(this).remove();
                });
                mouse_entered = "yes";
                $(this).off(event_name_mouseenter);
            }
        });
        // this_element.on(event_name_focusin, function() {
        //         $(this).removeClass("frequired-red");
        //         if (noti_ele.length >= 1) {
        //             noti_ele.fadeOut(1000, function() {
        //                 $(this).remove();
        //             });
        //         }
        // });
        this_element.on(event_name_mouseenter, function () {
            $(this).removeClass("NM-redborder");
            $(this).parent().parent().removeClass("NM-redborder");
            if (noti_ele.length >= 1) {
                noti_ele.fadeIn(500, function () {
                    noti_ele.fadeOut(2500, function (){
                        $(this).remove();
                    });
                    
                });
            }
            $(this).off(event_name_mouseenter);
        });
    } else if (element_container.children(".NM-notification[nm-unique-attr='" + this_element_name + this_id_object_data + "']").length == 1) {
        noti_ele = element_container.children(".NM-notification[nm-unique-attr='" + this_element_name + this_id_object_data + "']");
        if (noti_ele.text().indexOf(message) >= 0) {

        } else {
            noti_ele.append("<br/>" + message);
        }
    }
    return noti_ele;
}
var count = '1';
var undo_array = [];
var redo_array = [];
var undo_data_array = [];
var redo_data_array = [];
var count_btn = "1";
//form_settings_get_objects moved to formbuilder_common.js Feb, 02 2016



$(document).ready(function () {

    setValidCategoryUsers($("#category_id").val());
    //added aaron 10202014
    ButtonDraggableFormObj.init();
    //added jewel 10142014 
    //moved names.init(); to list_names_function.js 0721PM 10202015
    //added aaron 12012014
    CalendarView.init();

    $("body").on("click", ".fields-reset", function (e) {
        var ual = undo_array.length - 1;
        var dal = undo_data_array.length - 1;
        delete undo_array[ual];
        delete undo_data_array[dal];
        undo_data_array = undo_data_array.filter(Boolean);
        undo_array = undo_array.filter(Boolean);
        if (undo_array.length == 0) {
            $('.undo-me').addClass('undo-disable');
            $('.undo-me').removeClass('fl-buttonEffect');
        }
        if (e.target.id == "save_prop") {
            fr_removeDialog()
            return;
        }

        //return to prop
        var object_id = $(this).attr("object_id");
        var body_data = $("body").data();
        if ($("#setObject_" + object_id).is('[data-type="tab-panel"]') || $("#setObject_" + object_id).is('[data-type="table"]') || $("#setObject_" + object_id).is('[data-type="accordion"]') ) {
            var old_label = $("#setObject_" + object_id).attr('data-temp-label-holder');

            $("#setObject_" + object_id).find('label#lbl_' + object_id).text(old_label);
            $("#setObject_" + object_id).removeAttr('data-temp-label-holder');
            var body_data = $('body').data();
            if ($.type(body_data[object_id]) != 'undefined') {
                body_data[object_id]['lblName'] = old_label;
            }
            fr_removeDialog();


            return true;
        }
        if ($("#setObject_" + object_id).is('[data-upload="photo_upload"]')) {
            $("#setObject_" + object_id).html(old_object_dom);
        } else if ($("#setObject_" + object_id).find(".fields_below").length >= 1) {
            $("#setObject_" + object_id).children(".fields_below").replaceWith(old_object_dom);
            console.log(old_object_dom)
        } else if ($('.setObject[data-object-id="' + object_id + '"]').length >= 1 && $('.setObject[data-object-id="' + object_id + '"]').hasClass('setObject_btn')) {
            console.log("old_object_dom  ", old_object_dom)
            $('.setObject[data-object-id="' + object_id + '"]').replaceWith(old_object_dom);
        } else if ($("#setObject_" + object_id).length >= 1) {
            $("#setObject_" + object_id).replaceWith(old_object_dom);
        }
        if (if_undefinded($(this).attr("old_json"), "") != "") {
            body_data['' + object_id + ''] = JSON.parse($(this).attr("old_json"));
        } else {
            delete body_data['' + object_id + ''];
        }
        if (old_object_dom.hasClass('setObject')) {
            var ele_obj = old_object_dom;
        } else {
            var ele_obj = old_object_dom.closest(".setObject");
        }
        ele_obj_to_bind = ele_obj;
        ele_obj_to_bind = ele_obj_to_bind.add(ele_obj.find('.setObject[data-object-id][data-type]'));
        console.log("BINDING TEST", ele_obj_to_bind);
        ele_obj_to_bind.each(function () {
            var self = $(this);
            var ele_this = $(this).children("div.fields_below").find('div.setObject-drag-handle');
            var line_ele = $(this).children("div.fields_below").children('div.input_position_below').find('div[data-type="createLine"]');
            var object_id = $(this).attr('data-object-id');
            if (line_ele.length >= 1) {
                line_ele.resizable('destroy');
                line_ele.removeData('uiResizable');
                dragObects(self, '.formbuilder_ws', line_ele);

            }
            dragElement($(this), ".workspace", "", object_id);
            //rebind tooltip
            console.log("sadasdas", ele_this);
            if ($(this).find('[data-original-title]').length >= 1) {
                ele_this.tooltip("destroy");
                ele_this.removeData("tooltip");
                ele_this.tooltip();
            }

            console.log(self.find("#label_" + object_id));

            if (self.find("#label_" + object_id).is(".lbl-aligned-left")) {
                self.find("#label_" + object_id).removeData("uiResizable");
                self.find("#label_" + object_id).resizable({
                    handles: "e"
                });
            };

        });
            //added by Pao... Remove PO
            var drag_elem = $('#'+ele_obj_to_bind.attr('id')).find(".setObject-drag-handle");
            if(ele_obj_to_bind.attr("data-type")== "pickList"){
                if(drag_elem.length >= 2){
                console.log(ele_obj_to_bind.attr("data-type"))
                $('#'+ele_obj_to_bind.attr('id')).find(".setObject-drag-handle").eq(0).remove()
            }
            }
        fr_removeDialog();
    });

    {
        var log_id = $(".login-expire-security-id").text();
        $("html").data("login_expire_security_id", log_id);
        $(".login-expire-security-id").remove()
    }

    $("body").on("change", ".choose_set_picklist", function () {
        var data_val = $(this).val();
        $(".set_b_i").each(function () {
            if (!$(this).hasClass("display") == true) {
                $(this).addClass("display");
                $(this).addClass("set_b_i");
            } else {
                $(".set_" + data_val).removeClass("display");
            }
        });
    });

    var patname = window.location.pathname;
    var user_view = $("#user_url_view").val();
    if (patname == "/formbuilder" || pathname == "/user_view/formbuilder" || pathname == user_view + "create_forms") {
        $(".formbuilder_objects").perfectScrollbar();
        $(".ps-scrollbar-x").remove();

    }

    // OOP JS Functions Call

    /* Main Workspace */
    workspace_functions.preview_form(".preview_form");
    var pathname = window.location.pathname;
    if (pathname == "/user_view/formbuilder") {
        workspace_functions.view_save(".save_workspace");
        workspace_functions.view_save(".saveas_workspace");
       
        //  $('.formbuilder_page_btn.btn_content').sortable({
        //     items:".setObject_btn",
        //     axis:"x",
        //     handle:'.setObject-drag-handle',
        //     tolerance:'pointer',
        //     start: function(e, ui){

        //         //ui.placeholder.attr("style",ui.item.attr("style")+"; outline:none;");
        //         ui.placeholder.height(ui.item.height());
        //         ui.placeholder.width(ui.item.width()-10);

        //         //$(this).sortable("destroy")
        //     },
        //     stop:function(e, ui){
        //         $(this).sortable("refresh");
        //     }
        // });
    }


    /* Main Formbuilder */

    form_settings_get_objects.get_objects(".getObjects");

    form_settings_get_objects.delete_objects(".object_remove");
    form_settings_get_objects.clear_all(".clear_all_formbuilder_workspace");
    form_settings_get_objects.form_size(".form_size");
    form_settings_get_objects.form_set_size(".form_set_size");
    form_settings_get_objects.save_form_workspace(".save_form_workspace");
    form_settings_get_objects.label_font_family(".change_label_font_family");
    form_settings_get_objects.field_font_family(".change_field_font_family");
    form_settings_get_objects.label_text_alignment(".change_label_text_alignment");
    form_settings_get_objects.field_text_alignment(".change_field_text_alignment");
    form_settings_get_objects.field_font_size('.change_field_font_size');
    form_settings_get_objects.label_font_size('.change_label_font_size');
    form_settings_get_objects.font_weight('.change_label_font_weight');
    form_settings_get_objects.field_font_weight('.change_field_font_weight');
    form_settings_get_objects.label_font_style('.change_label_font_style');
    form_settings_get_objects.field_font_style('.change_field_font_style');
    form_settings_get_objects.label_text_decoration('.change_label_text_decoration');
    form_settings_get_objects.field_text_decoration('.change_field_text_decoration');
    form_settings_get_objects.label_color('.change_label_color');
    form_settings_get_objects.field_color('.change_field_color');
    form_settings_get_objects.undo_me('.undo-me');
    form_settings_get_objects.redo_me('.redo-me');
    form_settings_get_objects.allow_portal_size('#allow-portal');
    

    /*initialization startup load of the workspace*/
    {
        //selectFormBehavior();

        selectFormBehaviorV2();
        bindCellGridSize();
        bindCellArrayLength();
        WorkspaceFocus();
        keyBoardShortCut();
        tabFunctionality();
        bindMergeUnmerge();

    }

    form_settings_get_objects.load_workspace_state();

    if ($(".workspace.formbuilder_ws").find(".setObject").length >= 1) {
        if (checkFormMinWidth() != null) {

            $(".workspacev2.formbuilder_ws").css({
                "min-width": checkFormMinWidth()
            });
        }
        if (checkFormMinHeight() != null) {
            $(".workspacev2.formbuilder_ws").css({
                "min-height": checkFormMinHeight()
            });
        }
    }

    //resizeForm(".formbuilder_page"); // Resize Form
    // Control Save Workspace
    // var pathname = window.location.pathname;
    //         if (pathname == '/user_view/formbuilder') {

    //         }
    //remove control_save_workspace(".save_workspace"); 10132015 move to builder common
    // Hide / Show Properties of objects
    prop_hovering();

    //set form properties on json
    setFormProperties();

    var pathname = window.location.pathname;
    if (pathname == "/formbuilder" || pathname == "/user_view/formbuilder") {
        picklist.init();
        PicklistSelectionType.init();
    }

    // if(document.URL.indexOf("formbuilder") >= 0){
  
    function checkStaticRulerTop(rulertip_ele) {// Top (target left)
        mostleft = $(rulertip_ele).position().left + $(rulertip_ele).width();
        try {
            $(".workspace.formbuilder_ws.ui-resizable,.workspace.workflow_ws.ui-resizable,.workspace.orgchart_ws.ui-resizable").resizable({
                "minWidth": mostleft
            });
        } catch (error) {
            console.log("error on function checkStaticRulerTop( maybe due to uninitialized resizable");
        }
    }
    function checkStaticRulerLeft(rulertip_ele) {// left (target top)
        mostbottom = $(rulertip_ele).position().top + $(rulertip_ele).height();
        try {
            $(".workspace.formbuilder_ws.ui-resizable,.workspace.workflow_ws.ui-resizable,.workspace.orgchart_ws.ui-resizable").resizable({
                "minHeight": mostbottom
            });
        } catch (error) {
            console.log("error on function checkStaticRulerLeft( maybe due to uninitialized resizable");
        }
    }
    // Top (target left)
    // Ruler drag and click move to builder_common - Roni Pinili 2:27 PM 10/12/2015
    // }else{
    // 	$(".f-show-ruler[name='show-form-ruler-select']").hide()
    // 	$(".topPointerRuler-container").hide();
    // 	$(".leftPointerRuler-container").hide()
    // 	$(".form-main-ruler-nav-control").hide();
    // }

    {//DITO
        // Set Value of the Object Properties On Text Field
        $("body").on('change.CustomSpectrumChange','[data-properties-type="fld_fc"]',function(e){//FS#8106 - dito ung mga spectrum trigger change ng property
            var self = $(this);
            var self_spectrum_option = self.spectrum("option");
            self_spectrum_option.change.call( self, tinycolor(self.val(),self_spectrum_option) );
        });
        $("body").on("change", "input[type='text'].obj_prop,textarea.obj_prop", function () {
            //get property type

            var data_properties_type = $(this).attr("data-properties-type");
            var value = $(this).val();
            var json = $("body").data();
            var objID = $(this).attr("data-object-id");
            if (!objID) {
                objID = $("#popup_container").attr("data-object-id");
            }

            if (json['' + objID + '']) {
                var prop_json = json['' + objID + ''];
            } else {
                var prop_json = {};
            }

            // Change Properties of the selected objects
            switch (data_properties_type) {

                case "lblName":
                    var lblName = $(this).val();
                    $("#lbl_" + objID).html(lblName);

                    break;
                case "lblFldName":
                    var lblFldName = $(this).val();
                    var data_type = $(this).attr("data-type");

                    switch (data_type) {
                        case "button":
                            $("#btnName_" + objID).attr("name", lblFldName.replace(/ /g, ''));
                            $("#btnName_" + objID).val(lblFldName);
                            $("#btnName_" + objID).parents('.setObject').eq(0).attr("data-original-title", lblFldName);
                            break;
                        case "checkbox":
                            lblFldName = lblFldName + "[]";
                            $(".getFields_" + objID).attr("name", lblFldName.replace(/ /g, ''));
                            $(".getFields_" + objID).parents('.setObject').eq(0).find('.setObject-drag-handle').attr("data-original-title", lblFldName);
                            break;
                        case "selectMany":
                            lblFldName = lblFldName + "[]";
                            $(".getFields_" + objID).attr("name", lblFldName.replace(/ /g, ''));
                            $(".getFields_" + objID).parents('.setObject').eq(0).find('.setObject-drag-handle').attr("data-original-title", lblFldName);
                            break;
                        case "text-tagging":
                            lblFldName = lblFldName + "[]";
                            $(".getFields_" + objID).attr("name", lblFldName.replace(/ /g, ''));
                            $(".getFields_" + objID).parents('.setObject').eq(0).find('.setObject-drag-handle').attr("data-original-title", lblFldName);
                            break;
                        case "embeded-view":
                            lblFldName = lblFldName.replace(/ /g, '');
                            $(".getFields_" + objID).attr("embed-name", lblFldName.replace(/ /g, ''));
                            $(".getFields_" + objID).parents('.setObject').eq(0).find('.setObject-drag-handle').attr("data-original-title", lblFldName);
                            break;
                            // case "smart_barcode_field":
                            // 	lblFldName = lblFldName.replace(/ /g, '');
                            // 	//$(".getFields_" + objID).attr("name", lblFldName.replace(/(\s|\[|\])/g, '')+'[]');
                            // 	$(".getFields_" + objID).attr("name", lblFldName.replace(/(\s|\[|\])/g, ''));
                            // 	$(".getFields_" + objID).parents('.setObject').eq(0).find('.setObject-drag-handle').attr("data-original-title", lblFldName);
                            // 	break;
                        default:
                            $(".getFields_" + objID).attr("name", lblFldName.replace(/ /g, ''));
                            $(".getFields_" + objID).parents('.setObject').eq(0).find('.setObject-drag-handle').attr("data-original-title", lblFldName);
                            break;
                    }
                    var updatedFormula = UpdateMentionData.init();
                    if ($("[data-properties-type='defaultValue']").length >= 1) { //added by aaron
                        // $("[data-properties-type='defaultValue']").mention_post({
                        //     json: updatedFormula, // from local file fomula.json
                        //     highlightClass: "",
                        //     prefix: "",
                        //     countDisplayChoice: "4"
                        // });
                    }
                    collectNames();
                    break;
                case "lblPlaceHolder":
                    var lblPlaceHolder = $(this).val();
                    $("#getFields_" + objID).attr("placeholder", lblPlaceHolder);
                    $(".content-editable-field_" + objID).attr("placeholder", lblPlaceHolder);
                    if($('.setObject[data-object-id="' + objID + '"]').attr('data-type') == "smart_barcode_field"){
                        $('.setObject[data-object-id="' + objID + '"]').find('.smart-barcode-field-input').attr("placeholder", lblPlaceHolder);
                    }
                    break;
                case "lblTooltip":
                    var lblTooltip = $(this).val();
                    $("#getFields_" + objID).attr("data-original-title", lblTooltip);
                    if ($('.setObject[data-object-id="' + objID + '"]').attr('data-type') == "labelOnly") {
                        $('.setObject[data-object-id="' + objID + '"]').find('label').attr('title', lblTooltip);
                        $('.setObject[data-object-id="' + objID + '"]').find('label').attr('data-original-title', lblTooltip);
                    }
                    if($('.setObject[data-object-id="' + objID + '"]').attr('data-type') == "smart_barcode_field"){
                         $("#getFields_" + objID).parents('.input_position_below').find('table').attr('title', lblTooltip);
                         $("#getFields_" + objID).parents('.input_position_below').find('table').attr('data-original-title', lblTooltip);
                         $("#getFields_" + objID).parents('.input_position_below').find('table').addClass('tip');
                    }
                    $("#getFields_" + objID).attr("title", lblTooltip);
                    $(".content-editable-field_" + objID).attr("data-original-title", lblTooltip);
                    $(".content-editable-field_" + objID).attr("title", lblTooltip);
                    break;
                case "lblMaxLength":
                    var lblMaxLength = $(this).val();
                    $("#getFields_" + objID).attr("maxlength", lblMaxLength);
                    $(".content-editable-field_" + objID).attr("onkeypress", "return (window.getSelection()['type']=='Range')?true:(this.innerText.length < " + (Number(lblMaxLength)) + ")");
                    $(".content-editable-field_" + objID).attr("onkeydown", "return (window.getSelection()['type']=='Range'||event.keyCode==8|| (event.keyCode>=36&&event.keyCode<=40) || (event.ctrlKey && event.keyCode==65 ) || event.keyCode==35 || event.keyCode==36  )?true:(this.innerText.length < " + (Number(lblMaxLength)) + ")");
                    if($('.setObject[data-object-id="' + objID + '"]').attr('data-type') == "smart_barcode_field"){
                        $('.setObject[data-object-id="' + objID + '"]').find('.smart-barcode-field-input').attr("maxlength", lblMaxLength);
                    }
                    break;
                case "lblFieldHeight":
                    var lblFieldHeight = $(this).val();
                    if($("#getFields_" + objID).parents('.td-relative').length > 0){
                        var td_relative = $("#getFields_" + objID).parents('.td-relative');
                        if(lblFieldHeight > td_relative.height()){
                            var td_relative_height = ((td_relative.height() - 5) - $("#getFields_" + objID).parents('.setObject').position()['top']);
                            // $("#getFields_" + objID).parents('.setObject').css("height", td_relative_height + "px");
                            var top = $('#getFields_' + objID).parent().position()['top'];
                            var field_height = td_relative_height - top;
                            $("#getFields_" + objID).css("height", field_height);
                            $(this).val(td_relative_height);
                            value = $(this).val();
                        }
                        else{
                            // $("#getFields_" + objID).parents('.setObject').css("height", lblFieldHeight + "px");
                            var top = $('#getFields_' + objID).parent().position()['top'];
                            var field_height = td_relative_height - top;
                            $("#getFields_" + objID).css("height", field_height);
                            value = $(this).val();
                        }
                    }
                    else{
                        if(Number(lblFieldHeight) + ($("#getFields_" + objID).parents('.setObject').position()['top']) > Number($('.form_size_height').val())){
                            var form_height = ((Number($('.form_size_height').val())) - ($("#getFields_" + objID).parents('.setObject').position()['top'] + $("#getFields_" + objID).parent().prev().height()));
                            if($('[data-properties-type="fieldFontSize"]').length > 0){
                                if(form_height > Number($('[data-properties-type="fieldFontSize"]').val().replace(/px/g, '')) + 11){
                                    console.log("form_height",form_height);
                                    var field = $('#getFields_' + objID);
                                    
                                    var setOBJHeight = field.parents('.setObject').height();
                                    var inputPositionBelow = field.parent();
                                    console.log("inputPositionBelow", inputPositionBelow);
                                    var label = inputPositionBelow.prev();
                                    console.log("label", label);
                                    field.css('height', (form_height - label.height()) + "px");
                                    // field.parents('.setObject').css('height', setOBJHeight + "px");
                                    inputPositionBelow.css('height', '100%');
                                    // field.css('height', '100%');
                                    $(this).val(field.height());
                                    value = $(this).val();
                                }
                                else{
                                    $(this).val($('#getFields_' + objID).height());
                                    value = $(this).val();
                                }
                            }
                            else{
                                var obj = $('#getFields_' + objID);
                                var parent = obj.parents('#setObject_' + objID).eq(0);
                                var label = obj.parent().prev();
                                // parent.css('height', form_height + 'px');
                                obj.parent().css('height', '100%');
                                obj.css('height', (form_height - label.height()) + 'px');
                                $(this).val();
                                value = $(this).val();
                            }
                        }
                        else{
                            // $("#getFields_" + objID).css("width", lblFieldWidth + "px");
                            // $("#getFields_" + objID).parents('.setObject').css("height", lblFieldHeight + "px");
                            // var top = $('#getFields_' + objID).parent().position()['top'];
                            // var field_height = td_relative_height - top;
                            // $("#getFields_" + objID).css
                            if($('[data-properties-type="fieldFontSize"]').length > 0){
                                if(lblFieldHeight > Number($('[data-properties-type="fieldFontSize"]').val().replace(/px/g, '')) + 11){// 11 adjustment of fontsize
                                    var field = $('#getFields_' + objID);
                                    
                                    var setOBJHeight = field.parents('.setObject').height();
                                    var inputPositionBelow = field.parent();
                                    console.log("inputPositionBelow", inputPositionBelow,"field_height", lblFieldHeight);
                                    var label = inputPositionBelow.prev();
                                    console.log("label", label, "setOBJHeight", setOBJHeight);
                                    field.css('height', (lblFieldHeight) + "px");
                                    // field.parents('.setObject').css('height', label.height() + field.height() + "px");
                                    inputPositionBelow.css('height', '100%');
                                    // field.css('height', '100%');
                                    // $(this).val(field.height());
                                    value = $(this).val();
                                }
                                else{
                                    
                                    $(this).val();
                                    value = $(this).val();
                                }
                            }
                            else{
                                console.log($('#getFields_' + objID).height());
                                var obj = $('#getFields_' + objID);
                                var parent = obj.parents('#setObject_' + objID).eq(0);
                                var label = obj.parent().prev();
                                // parent.css('height', lblFieldHeight + 'px');
                                obj.parent().css('height', '100%');
                                obj.css('height', (Number(lblFieldHeight)) + 'px');
                                $(this).val();
                                value = $(this).val();
                            }
                            
                        }
                    }
                    
                    if ($("#getFields_" + objID).prop("tagName") == "TEXTAREA") {
                        $("#getFields_" + objID).closest(".setObject").css({"height": ($("#getFields_" + objID).closest(".setObject").outerHeight()) + "px"})
                        $("#getFields_" + objID).css({"height": "100%"})
                    }
                    break;
                case "lblFieldMinHeight":
                    var lblFieldHeight = $(this).val();
                    var max_h = Number($("#getFields_" + objID).css("max-height").replace("px", ""));
                    max_h = (isNaN(max_h) ? 200 : max_h);
                    var min_h = /*Number($("#getFields_" + objID).css("min-width").replace("px",""))*/Number(lblFieldHeight);
                    if (min_h <= max_h) {
                        $("#getFields_" + objID).css("min-height", lblFieldHeight + "px");
                        $(".content-editable-field_" + objID).css("min-height", lblFieldHeight + "px");
                    } else {
                        $("#getFields_" + objID).css("min-height", max_h + "px");
                        $(".content-editable-field_" + objID).css("min-height", max_h + "px");
                        $(this).val(max_h);
                        value = max_h;
                    }
                    break;
                case "lblFieldMaxHeight":
                    var lblFieldHeight = $(this).val();
                    var max_h = /*Number($("#getFields_" + objID).css("max-width").replace("px",""))*/Number(lblFieldHeight);
                    var min_h = Number($("#getFields_" + objID).css("min-height").replace("px", ""));
                    min_h = (isNaN(min_h) ? 20 : min_h);
                    if (min_h <= max_h) {
                        $("#getFields_" + objID).css("max-height", lblFieldHeight + "px");
                        $(".content-editable-field_" + objID).css("max-height", lblFieldHeight + "px");
                    } else {
                        $("#getFields_" + objID).css("max-height", min_h + "px");
                        $(".content-editable-field_" + objID).css("max-height", min_h + "px");
                        $(this).val(min_h);
                        value = min_h;
                    }

                    break;
                case "lblFieldWidth":
                    var lblFieldWidth = Number($(this).val())||80;
                    if($("#getFields_" + objID).parents('.td-relative').length > 0){
                        var td_relative = $("#getFields_" + objID).parents('.td-relative');
                        if(lblFieldWidth > td_relative.width()){
                            var td_relative_width = ((td_relative.width() - 5) - $("#getFields_" + objID).parents('.setObject').position()['left']);
                            // $("#getFields_" + objID).css("width", td_relative_width + "px");
                            $("#getFields_" + objID).parents('.setObject').css("width", td_relative_width + "px");
                            $(this).val(td_relative_width);
                            value = $(this).val();
                        }
                        else{
                            // $("#getFields_" + objID).css("width", lblFieldWidth + "px");
                            $("#getFields_" + objID).parents('.setObject').css("width", lblFieldWidth + "px");
                            value = $(this).val();
                        }
                    }
                    else{

                        if((lblFieldWidth + Number($("#getFields_" + objID).parents('.setObject').position()['left']))  > Number($('.form_size_width').val())){
                            var form_width = ((Number($('.form_size_width').val()) - 5) - $("#getFields_" + objID).parents('.setObject').position()['left']);
                            // $("#getFields_" + objID).css("width", form_width + "px");
                            $("#getFields_" + objID).css("width", form_width + "px");
                            $(this).val(form_width);
                            value = $(this).val();
                        }
                        else{
                            // $("#getFields_" + objID).css("width", lblFieldWidth + "px");
                            $("#getFields_" + objID).css("width", lblFieldWidth + "px");
                            value = $(this).val();
                        }
                        
                    }
                    break;
                case "lblFieldMinWidth":
                    var lblFieldWidth = $(this).val();
                    var max_w = Number($("#getFields_" + objID).css("max-width").replace("px", ""));
                    max_w = (isNaN(max_w) ? 300 : max_w);
                    var min_w = /*Number($("#getFields_" + objID).css("min-width").replace("px",""))*/Number(lblFieldWidth);
                    if (min_w <= max_w) {
                        $("#getFields_" + objID).css("min-width", lblFieldWidth + "px");
                        $(".content-editable-field_" + objID).css("min-width", lblFieldWidth + "px");
                    } else {
                        $("#getFields_" + objID).css("min-width", max_w + "px");
                        $(".content-editable-field_" + objID).css("min-width", max_w + "px");
                        $(this).val(max_w);
                        value = max_w;
                    }
                    break;
                case "lblFieldMaxWidth":
                    var lblFieldWidth = $(this).val();
                    var max_w = /*Number($("#getFields_" + objID).css("max-width").replace("px",""))*/Number(lblFieldWidth);
                    var min_w = Number($("#getFields_" + objID).css("min-width").replace("px", ""));
                    min_w = (isNaN(min_w) ? 20 : min_w);
                    if (min_w <= max_w) {
                        $("#getFields_" + objID).css("max-width", lblFieldWidth + "px");
                        $(".content-editable-field_" + objID).css("max-width", lblFieldWidth + "px");
                    } else {
                        $("#getFields_" + objID).css("max-width", min_w + "px");
                        $(".content-editable-field_" + objID).css("max-width", min_w + "px");
                        $(this).val(min_w);
                        value = min_w;
                    }
                    break;
                case "lblChoices":

                    var lblChoices = $(this).val();
                    var data_type = $(this).attr("data-type");
                    var select = "";
                    var type = "";
                    var eachLine = lblChoices.split('\n');
                    switch (data_type) {
                        case "checkbox":
                            type = "checkbox";
                            var checkbox_name = 'checkbox_' + objID + '[]';
                            var field_name = $("[data-properties-type='lblFldName']").val();
                            if (field_name) {
                                checkbox_name = field_name + "[]";
                            }

                            var rc_align_type = $("#setObject_" + objID).attr("rc-align-type");
                            var rc_elem_alignment = "";
                            if (rc_align_type == "vertical") {
                                rc_elem_alignment = '<span class="rc-prop-alignment rc-align-vertical"></span>';
                            } else if (rc_align_type == "horizontal") {
                                rc_elem_alignment = '<span class="rc-prop-alignment rc-align-horizontal"></span>';
                            }

                            var text = "";
                            var optionValue = "";
                            for (var i = 0, l = eachLine.length; i < l; i++) {
                                text = eachLine[i];
                                optionValue = eachLine[i];
                                if(text.length > 0){//modified by japhet morada fix FS#8065
                                    if (eachLine[i].indexOf("|") >= 0) {
                                        text = $.trim(eachLine[i].split("|")[0]);
                                        optionValue = $.trim(eachLine[i].split("|")[1]);
                                    }
                                    select += '<label><input type="' + type + '" data-type="longtext" disabled="disabled" name="' + checkbox_name + '" class="getFields getFields_' + objID + '" id="getFields_' + objID + '" value="' + optionValue + '"/> ' + text + ' </label>' + rc_elem_alignment;
                                }//
                            }
                            //select += '<label><input type="' + type + '" data-type="longtext" disabled="disabled" name="' + checkbox_name + '" class="getFields getFields_' + objID + '" id="getFields_' + objID + '" value="' + "default" + '"/> ' + text + ' </label>' + rc_elem_alignment;
                            this_select = $(select)
                            var allow_label_rc_ele = $('[data-properties-type="lblAllowLabel-RC"]');
                            var element_c_temporary = "";
                            //wrap
                            //<span class="checkbox-input-label">
                            this_select.contents().each(function () {
                                if ($(this).prop("tagName") != "INPUT" || $(this).prop("tagName") == "SPAN") {
                                    $(this).wrap('<span class="checkbox-input-label"/>');
                                }
                            })
                            $("#obj_fields_" + objID).html(element_c_temporary = this_select.clone());
                            if (allow_label_rc_ele.val() == "Yes") {
                                $("#obj_fields_" + objID).find(".checkbox-input-label").show();
                                //  element_c_temporary.each(function(){
                                //      if($(this).prop("tagName") == "LABEL" ){
                                //          $(this).find(".checkbox-input-label").show();
                                //      }
                                // })
                            } else {
                                $("#obj_fields_" + objID).find(".checkbox-input-label").hide();
                                // element_c_temporary.each(function(){
                                //      if($(this).prop("tagName") == "LABEL" ){
                                //          $(this).find(".checkbox-input-label").hide();
                                //      }
                                // })
                            }

                            if ($(".obj_defaultValueChoices").length >= 1) {
                                var temp_elem_clone = "";
                                $(".obj_defaultValueChoices").html(temp_elem_clone = this_select.clone());

                                $(".obj_defaultValueChoices").find(".checkbox-input-label").show();
                                // temp_elem_clone.each(function(){
                                //     if($(this).prop("tagName") == "LABEL" ){
                                //         $(this).find(".checkbox-input-label").show();
                                //     }
                                // })

                                temp_elem_clone.closest(".obj_defaultValueChoices").find(".getFields_" + objID).attr("name", "for_default_use");
                                temp_elem_clone.closest(".obj_defaultValueChoices").find(".getFields_" + objID).removeAttr("disabled");

                            }
                            if(json){
                                if(json[objID+'']){
                                    if(json[objID+'']['fld_fs'] == "italic"){
                                        $('[data-properties-type="fld_fs"]').trigger("change");
                                    }
                                    if(json[objID+'']['fld_fw'] == "bold"){
                                        $('[data-properties-type="fld_fw"]').trigger("change");
                                    }
                                    if(json[objID+'']['fld_td'] == "underline"){
                                        $('[data-properties-type="fld_td"]').trigger("change");
                                    }
                                }
                            }
                            $('[data-properties-type="lblFontSize"],[data-properties-type="fieldFontSize"],[data-properties-type="fld_fc"]').trigger("change"); //FS#8106 - added trigger change for color
                            break;
                        case "radioButton":
                            type = "radio";
                            var radio_name = "radio_" + objID;
                            var field_name = $("[data-properties-type='lblFldName']").val();
                            if (field_name) {
                                radio_name = field_name;
                            }
                            var rc_align_type = $("#setObject_" + objID).attr("rc-align-type");
                            var rc_elem_alignment = "";
                            if (rc_align_type == "vertical") {
                                rc_elem_alignment = '<span class="rc-prop-alignment rc-align-vertical"></span>';
                            } else if (rc_align_type == "horizontal") {
                                rc_elem_alignment = '<span class="rc-prop-alignment rc-align-horizontal"></span>';
                            }


                            // for (var i = 0, l = eachLine.length; i < l; i++) {
                            //     select += '<label><input type="' + type + '" data-type="longtext" disabled="disabled" name="' + radio_name + '" class="getFields getFields_' + objID + '" id="getFields_' + objID + '" value="' + eachLine[i] + '"/>' + eachLine[i] + '</label>' + rc_elem_alignment;
                            // }
                            var text = "";
                            var optionValue = "";
                            for (var i = 0, l = eachLine.length; i < l; i++) {
                                text = eachLine[i];
                                optionValue = eachLine[i];
                                if(text.length > 0){//modified by japhet morada fix FS#8065
                                    if (eachLine[i].indexOf("|") >= 0) {
                                        text = $.trim(eachLine[i].split("|")[0]);
                                        optionValue = $.trim(eachLine[i].split("|")[1]);
                                    }
                                    select += '<label><input type="' + type + '" data-type="longtext" disabled="disabled" name="' + radio_name + '" class="getFields getFields_' + objID + '" id="getFields_' + objID + '" value="' + optionValue + '"/> ' + text + ' </label>' + rc_elem_alignment;
                                }
                            }

                            this_select = $(select)
                            var allow_label_rc_ele = $('[data-properties-type="lblAllowLabel-RC"]');
                            var element_c_temporary = "";
                            //wrap
                            //<span class="checkbox-input-label">
                            this_select.find("input").eq(0).attr("checked", "checked");
                            this_select.contents().each(function () {
                                if ($(this).prop("tagName") != "INPUT" || $(this).prop("tagName") == "SPAN") {
                                    $(this).wrap('<span class="radio-input-label"/>');
                                }
                            })
                            $("#obj_fields_" + objID).html(element_c_temporary = this_select.clone());
                            if (allow_label_rc_ele.val() == "Yes") {
                                $("#obj_fields_" + objID).find(".radio-input-label").show();
                            } else {
                                $("#obj_fields_" + objID).find(".radio-input-label").hide();
                            }
                            if ($(".obj_defaultValueChoices").length >= 1) {
                                var temp_elem_clone = "";
                                $(".obj_defaultValueChoices").html(temp_elem_clone = this_select.clone());

                                temp_elem_clone.find(".getFields_" + objID).attr("name", "for_default_use");
                                temp_elem_clone.find(".getFields_" + objID).removeAttr("disabled");

                            }
                            if(json){
                                if(json[objID+'']){
                                    if(json[objID+'']['fld_fs'] == "italic"){
                                        $('[data-properties-type="fld_fs"]').trigger("change");
                                    }
                                    if(json[objID+'']['fld_fw'] == "bold"){
                                        $('[data-properties-type="fld_fw"]').trigger("change");
                                    }
                                    if(json[objID+'']['fld_td'] == "underline"){
                                        $('[data-properties-type="fld_td"]').trigger("change");
                                    }
                                }
                            }
                            $('[data-properties-type="lblFontSize"],[data-properties-type="fieldFontSize"],[data-properties-type="fld_fc"]').trigger("change"); //FS#8106 - added trigger change for color
                            break;
                        case "selectMany":
                            var text = "";
                            var optionValue = "";
                            for (var i = 0, l = eachLine.length; i < l; i++) {
                                text = eachLine[i];
                                optionValue = eachLine[i];
                                if(text.length > 0){
                                    if (eachLine[i].indexOf("|") >= 0) {
                                        text = $.trim(eachLine[i].split("|")[0]);
                                        optionValue = $.trim(eachLine[i].split("|")[1]);
                                    }
                                    select += '<option value="' + optionValue + '"> ' + text + ' </option>';
                                }//select += '<input type="' + type + '" disabled="disabled" name="radio_' + objID + '" class="" id="getFields_' + objID + '"/> ' + eachLine[i] + ' <br />';
                            }
                            $("#getFields_" + objID).html(select);
                            if ($(".obj_defaultValueChoices").length >= 1) {
                                this_select = $(select);
                                $(".obj_defaultValueChoices").find(".getFields_" + objID).html(this_select);
                                this_select.closest(".obj_defaultValueChoices").find(".getFields_" + objID).attr("name", "for_default_use");
                                this_select.closest(".obj_defaultValueChoices").find(".getFields_" + objID).removeAttr("disabled");
                            }
                            break;
                        case "dropdown":
                            var text = "";
                            var optionValue = "";
                            for (var i = 0, l = eachLine.length; i < l; i++) {
                                text = eachLine[i];
                                optionValue = eachLine[i];
                                if(text.length > 0){
                                    if (eachLine[i].indexOf("|") >= 0) {
                                        text = $.trim(eachLine[i].split("|")[0]);
                                        optionValue = $.trim(eachLine[i].split("|")[1]);
                                    }
                                    select += '<option value="' + optionValue + '">' + text + '</option>';
                                }//select += '<input type="' + type + '" disabled="disabled" name="radio_' + objID + '" class="" id="getFields_' + objID + '"/> ' + eachLine[i] + ' <br />';
                            }
                            $("#getFields_" + objID).html(select);
                            if ($(".obj_defaultValueChoices").length >= 1) {
                                this_select = $(select)
                                this_select.eq(0).prop("selected", "selected");
                                $(".obj_defaultValueChoices").find(".getFields_" + objID).html(this_select);

                                this_select.closest(".obj_defaultValueChoices").find(".getFields_" + objID).attr("name", "for_default_use");
                                this_select.closest(".obj_defaultValueChoices").find(".getFields_" + objID).removeAttr("disabled");
                            }
                            break;
                    }
                    defaultValuesFN(objID);
                    break;
                case "defaultValue":
                    if ($(this).parent().find('[name="default_val"]').length >= 1) {
                        default_type = $(this).parent().find('[name="default_val"]:checked').val();
                        if (default_type == "static") {
                            $(".getFields_" + objID).attr("default-type", "static");
                            $(".getFields_" + objID).attr("default-static-value", value);
                            if (typeof json['' + objID + ''] != "undefined") {
                                json['' + objID + ''].defaultValueType = "static";
                            }
                        } else if (default_type == "computed") {
                            $(".getFields_" + objID).attr("default-type", "computed");
                            $(".getFields_" + objID).attr("default-formula-value", value);
                            if (typeof json['' + objID + ''] != "undefined") {
                                json['' + objID + ''].defaultValueType = "computed";
                            }
                        } else if (default_type == "OnSubmit") {
                            $(".getFields_" + objID).attr("default-type", "OnSubmit");
                            $(".getFields_" + objID).attr("default-formula-value", value);
                            if (typeof json['' + objID + ''] != "undefined") {
                                json['' + objID + ''].defaultValueType = "OnSubmit";
                            }
                        }
                    }
                    break;
                case "dynamicListing":
                    if (value) {
                        if (value != "") {
                            $(".getFields_" + objID).attr("default-type", "computed");
                            $(".getFields_" + objID).attr("default-formula-value", value);
                        } else {
                            $(".getFields_" + objID).attr("default-type", "");
                            $(".getFields_" + objID).attr("default-formula-value", "");
                        }
                    } else {
                        $(".getFields_" + objID).attr("default-type", "");
                        $(".getFields_" + objID).attr("default-formula-value", "");
                    }
                    break;
                case "optionSpacing":
                    if ($(".getFields_" + objID).parents("#obj_fields_" + objID).find('.rc-align-vertical').length >= 1) {
                        $(".getFields_" + objID).parents("#obj_fields_" + objID).find('.rc-align-vertical').css({
                            "height": (value) + "px",
                            "width": (0) + "px"
                        });
                    } else {
                        $(".getFields_" + objID).parents("#obj_fields_" + objID).find('.rc-align-horizontal').css({
                            "width": (value) + "px",
                            "height": (0) + "px"
                        });
                    }
                    break;
                case "DisableFormula":
                    $(".getFields_" + objID).attr("disable-formula", value);
                    break;
                case "ReadOnlyFormula":
                    $(".getFields_" + objID).attr("read-only-formula", value);
                    break;
            }
            prop_json['' + data_properties_type + ''] = value
            json['' + objID + ''] = prop_json;
            $("body").data(json);
        });
        // Set Value of the Object Properties On Combo Box
        $("body").on("change", "select.obj_prop, input[data-properties-type][type='radio'],input[data-properties-type][type='number'],input[data-properties-type][type='checkbox']", function () {
            //get property type

            var data_properties_type = $(this).attr("data-properties-type");
            var value = $(this).val();

            var json = $("body").data();
            if (typeof $("#popup_container").attr("data-object-id") != "undefined") {
                var objID = $("#popup_container").attr("data-object-id");
            } else {
                var objID = $(this).attr("data-object-id");
            }
            if (json['' + objID + '']) {
                var prop_json = json['' + objID + ''];
            } else {
                var prop_json = {};
            }

            // Change Properties of the selected objects
            
            switch (data_properties_type) {

                case "defaultHyperLinkValue":
                    var ide_or_not = $(this).val();
                    var input_HL = $(this).parents('.input_position_below').find('input[data-properties-type="lblHyperLink"]:eq(0)');

                    if (ide_or_not === "computed") {

                        input_HL.attr("data-ide-properties-type", "ide-lblHyperLink");
                        input_HL.val($("#setObject_" + objID).find('[default-hyperlink-static-value]:eq(0)').attr("default-hyperlink-formula-value"));
                    }
                    else {
                        input_HL.removeAttr("data-ide-properties-type").removeAttr("readonly");
                        input_HL.val($("#setObject_" + objID).find('[default-hyperlink-static-value]:eq(0)').attr("default-hyperlink-static-value"));
                    }
                    break;
                case "picklistActionVisibility":
                    var picklist_visibility = $(this).val();
                    if (picklist_visibility === "No") {
                        $("#setObject_" + objID).find('[picklist-button-id]:eq(0)').addClass("hidePicklistAction");
                        //$(this).closest('.input_position_below').find('.type-ahead-option').removeClass('isDisplayNone');	
                    }
                    else {
                        $("#setObject_" + objID).find('[picklist-button-id]:eq(0)').removeClass("hidePicklistAction");
                        //$(this).closest('.input_position_below').find('.type-ahead-option').addClass('isDisplayNone');
                    }
                    break;
                case "typeAheadOption":
                    var picklistaction = $(this).val();
                    if (picklistaction === "No") {
                        $("#setObject_" + objID).find('[picklist-button-id]:eq(0)').removeClass("typeAheadPickList");
                    }
                    else {
                        $("#setObject_" + objID).find('[picklist-button-id]:eq(0)').addClass("typeAheadPickList");
                    }
                    break;
                case "fld_td":
                    var fldtd = $(this).val();
                    var aligned_left = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below.obj_f-aligned-left");
                    var is_embed = $('div[data-type="embeded-view"]');
                    if (aligned_left.length >= 1) {
                        var field_font = aligned_left.find(".obj_f-aligned-left-wrap").children();
                        var field_input = aligned_left.find(".obj_f-aligned-left-wrap").find("input");
                    }

                    else {
                        var field_font = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below").children();
                        var field_input = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below").find("input");
                    }

                    if ($(this).is(":checked")) {
                        $(this).closest('.spanbutton').addClass('bn-cd');
                    }
                    else {
                        fldtd = "normal";
                        value = "normal";
                        $(this).closest('.spanbutton').removeClass('bn-cd');
                    }
                    if (field_font.length >= 1) {

                        field_font.css("text-decoration", fldtd);
                        if (is_embed.length >= 1) {
                            is_embed.attr("data-type-textdecoration", fldtd);

                        }

                    }
                    if (field_input.length >= 1) {
                        field_input.css("text-decoration", fldtd);
                    }
                    break;
                case "fld_fs":
                    var fldfs = $(this).val();
                    var aligned_left = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below.obj_f-aligned-left");
                    var is_embed = $('div[data-type="embeded-view"]');
                    if (aligned_left.length >= 1) {
                        var field_font = aligned_left.find(".obj_f-aligned-left-wrap").children();
                        var field_input = aligned_left.find(".obj_f-aligned-left-wrap").find("input");
                    }

                    else {
                        var field_font = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below").children();
                        var field_input = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below").find("input");
                    }

                    if ($(this).is(":checked")) {
                        $(this).closest('.spanbutton').addClass('bn-cd');
                    }
                    else {
                        fldfs = "normal";
                        value = "normal";
                        $(this).closest('.spanbutton').removeClass('bn-cd');
                    }
                    if (field_font.length >= 1) {

                        field_font.css("font-style", fldfs);
                        if (is_embed.length >= 1) {
                            is_embed.attr("data-type-fontstyle", fldfs);

                        }

                    }
                    if (field_input.length >= 1) {
                        field_input.css("font-style", fldfs);
                    }
                    break;
                case "fld_fw":
                    

                    var fontBold = $('[data-properties-type="fieldFontType"] option:selected').attr('attrfontBold');
                    var prop_field_font_weight_bold = $('.prop_field_font_weight').attr('has-bold', fontBold);
                    
                    var aligned_left = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below.obj_f-aligned-left");
                    var is_embed = $('div[data-type="embeded-view"]');
                    if (aligned_left.length >= 1) {
                        var field_font = aligned_left.find(".obj_f-aligned-left-wrap").children();
                        var field_input = aligned_left.find(".obj_f-aligned-left-wrap").find("input");
                    }

                    else {
                        var field_font = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below").children();
                        var field_input = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below").find("input");
                    }

                    
                    var fldfw = $(this).val();
                    var fldFonBold = fontBold;

                    if ($(this).is(":checked")) {
                        
                        var fldfw = $(this).val();
                        var fldFonBold = fontBold;
                        $(this).closest('.spanbutton').addClass('bn-cd');

                        // var objID = $('#getFields_'+$('.setObject').attr('data-object-id')).parents('.setObject').attr('data-object-id');
                        // var dbd = $('body').data(objID);
                        
                        // if ($.type(dbd) == "undefined") {
                        //     $('body').data(objID, {});
                        //     dbd = $('body').data(objID);
                        // }
                        // dbd['fld_fw'] = "bold";
                        // console.log("checked", dbd['fld_fw'], dbd);

                        if (field_font.length >= 1) {

                        field_font.css({
                            "font-weight": fldfw,
                            "font-family":fldFonBold
                        });
                            
                        if (is_embed.length >= 1) {
                            is_embed.attr("data-type-fontweight", fldfw);
                        }

                        }
                        if (field_input.length >= 1) {
                            field_input.css({
                                "font-weight": fldfw,
                                "font-family":fldFonBold
                            });
                        }   
                    }
                    else {
                           
                        var fontNormal = $('[data-properties-type="fieldFontType"] option:selected').attr('attrfontnormal');
                        fldFontNormal = fontNormal;
                        fldfw = "normal";
                        value = "normal";
                        /*var objID = $('#getFields_'+$('.setObject').attr('data-object-id')).parents('.setObject').attr('data-object-id');
                        var dbd = $('body').data(objID);
                        
                        if ($.type(dbd) == "undefined") {
                            $('body').data(objID, {});
                            dbd = $('body').data(objID);
                        }

                        dbd['fld_fw'] = "normal";
                        fldfw =  dbd['fld_fw'];
                        console.log("not checked", $('body').data(objID));*/


                        $(this).closest('.spanbutton').removeClass('bn-cd');

                        if (field_font.length >= 1) {

                        field_font.css({
                            "font-weight": fldfw,
                            "font-family":fldFontNormal
                        });
                            
                            if (is_embed.length >= 1) {
                                is_embed.attr("data-type-fontweight", fldfw);

                            }

                        }
                        if (field_input.length >= 1) {
                            field_input.css({
                                "font-weight": fldfw,
                                "font-family":fldFontNormal
                            });
                        }

                    }
                   
                    break;
                case "lbl_td":
                    var lbltd = $(this).val();
                    if ($(this).is(":checked")) {
                        lbltd = $(this).val();
                        $(this).closest('.spanbutton').addClass('bn-cd');
                    }
                    else {
                        lbltd = "none";
                        value = "none";
                        $(this).closest('.spanbutton').removeClass('bn-cd');
                    }
                    $("#lbl_" + objID).css("text-decoration", lbltd);
                    break;
                case "lbl_fs":
                    var lblfs = $(this).val();
                    if ($(this).is(":checked")) {
                        lblfs = $(this).val();
                        $(this).closest('.spanbutton').addClass('bn-cd');
                    }
                    else {
                        lblfs = "normal";
                        value = "normal";
                        $(this).closest('.spanbutton').removeClass('bn-cd');
                    }
                    $("#lbl_" + objID).css("font-style", lblfs);
                    break;
                case "lblFontWg":

                    var lblFontWg = $(this).val();
                    var fontBold = $('[data-properties-type="lblFontType"] option:selected').attr('attrfontBold');
                    var prop_label_font_weight_bold = $('.prop_label_font_weight').attr('has-bold', fontBold);                    
                    
                    
                    var lblFonBold = fontBold;
                    if ($(this).is(":checked")) {
                        lblFontWg = $(this).val();
                        lblFonBold = fontBold;
                        $(this).closest('.spanbutton').addClass('bn-cd');

                        $("#lbl_" + objID).css({
                            "font-weight": lblFontWg,
                            "font-family": lblFonBold
                        });

                        $('.formbuilder_ws').find('.component-ancillary-focus').each(function(){
                            //console.log($(this).attr('data-type'));
                            var dot = $(this).attr('data-type');
                                
                            $("#lbl_" + objID).css({
                                "font-weight": lblFontWg,
                                "font-family": lblFonBold
                            });

                            if (dot == 'tab-panel') {

                                $(this).children().find('ul li').css({
                                    "font-weight": lblFontWg,
                                    "font-family": lblFonBold
                                });
                            };

                            if (dot == 'accordion') {
                                $(this).children().find('.accordion-header-name').css({
                                    "font-weight": lblFontWg,
                                    "font-family": lblFonBold
                                });
                            };

                           
                        });

                    }
                    else {

                        fontNormal = $('[data-properties-type="lblFontType"] option:selected').attr('attrfontnormal'); 
                        lblFontWg =  "normal";
                        value = "normal";
                        lblFontNormal = fontNormal;
                        $(this).closest('.spanbutton').removeClass('bn-cd');

                        $("#lbl_" + objID).css({
                            "font-weight": lblFontWg,
                            "font-family": lblFontNormal
                        });

                        $('.formbuilder_ws').find('.component-ancillary-focus').each(function(){
                            //console.log($(this).attr('data-type'));
                            var dot = $(this).attr('data-type');
                                
                             $("#lbl_" + objID).css({
                                "font-weight": lblFontWg,
                                "font-family": lblFontNormal
                            });

                            if (dot == 'tab-panel') {
                                
                                $(this).children().find('ul li').css({
                                    "font-weight": lblFontWg,
                                    "font-family": lblFontNormal
                                });
                            };

                            if (dot == 'accordion') {
                                $(this).children().find('.accordion-header-name').css({
                                    "font-weight": lblFontWg,
                                    "font-family": lblFontNormal
                                });
                            };

                           
                        });

                    }
                    
                    

                    break;
                case "fieldTxtAlignment":

                    var field_alignment = $(this).val();
                    var aligned_left = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below.obj_f-aligned-left");

                    if (aligned_left.length >= 1) {
                        var field_align = aligned_left.find(".obj_f-aligned-left-wrap").children();
                    }
                    else {
                        var field_align = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below").children();
                    }
                    if (field_align.length >= 1) {
                        field_align.css("text-align", field_alignment);
                        if(field_align.parents('.setObject').attr('data-type') == 'smart_barcode_field'){
                            field_align.find('.smart-barcode-field-input').css("text-align", field_alignment);
                        }
                        if(field_align.parents('.setObject').attr('data-type') == 'pickList' || field_align.parents('.setObject').attr('data-type') == 'listNames'){
                            field_align.find('input').css("text-align", field_alignment);
                        }
                    }
                    break;
                case "fieldFontType":

                    var font_style = $(this).val();
                    var aligned_left = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below.obj_f-aligned-left");
                    var is_embed = $('div[data-type="embeded-view"]');
               
                    var fontBold = $('[data-properties-type="fieldFontType"] option:selected').attr('attrfontBold');
                    var prop_field_font_weight_attrbold = $(this).parents('.content-dialog-scroll').find('.prop_field_font_weight').attr('has-bold', fontBold);
                    var prop_field_font_weight_bold = prop_field_font_weight_attrbold.attr('has-bold');
                    var prop_field_font_weight_hasClass = $(this).parents('.content-dialog-scroll').find('.prop_field_font_weight').parent().hasClass('bn-cd');

                    if (aligned_left.length >= 1) {
                        var field_font = aligned_left.find(".obj_f-aligned-left-wrap").children();
                        var field_input = aligned_left.find(".obj_f-aligned-left-wrap").find("input");
                    }

                    else {
                        var field_font = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below").children();
                        var field_input = $("#setObject_" + objID).children("div.fields_below").find("div.input_position_below").find("input");
                    }


                    if (field_font.length >= 1) {
                        field_font.css("font-family", font_style);
                        if (is_embed.length >= 1) {
                            is_embed.attr("data-type-fontfamily", font_style);

                        }

                    }
                    if (field_input.length >= 1) {
                        field_input.css("font-family", font_style);
                    }

                    if (prop_field_font_weight_hasClass) {
                        field_font.css("font-family", prop_field_font_weight_bold);
                        field_input.css("font-family", prop_field_font_weight_bold);
                    };

                    break;
                case "fieldShortlist":

                    var fld_names_obj_button_cont_ele = $(".getFields_" + objID).next();
                    var dis_val = $(this).val();
                    if (fld_names_obj_button_cont_ele.is('span')) {
                        fld_names_obj_button_cont_ele.children('.viewNames').attr('short-list-type', dis_val);
                    }
                    break;
                case "lblFontType":

                    var font_style = $(this).val();
                    var fontBold = $('[data-properties-type="lblFontType"] option:selected').attr('attrfontBold');
                    var prop_label_font_weight_attrbold = $(this).parents('.content-dialog-scroll').find('.prop_label_font_weight').attr('has-bold', fontBold);
                    var prop_label_font_weight_bold = prop_label_font_weight_attrbold.attr('has-bold');
                    var prop_label_font_weight_hasClass = $(this).parents('.content-dialog-scroll').find('.prop_label_font_weight').parent().hasClass('bn-cd');
                    var data_type_this = $(this).attr("data-type");
                    
                    // $('.formbuilder_ws').find('.component-ancillary-focus').each(function(){
                        //console.log($(this).attr('data-type'));
                        var dot = $(this).attr('data-type');

                        if (prop_label_font_weight_hasClass) {
                        //console.log("mine", prop_label_font_weight_bold.attr('has-bold'), "theirs",font_style);
                            $("#lbl_" + objID).css("font-family", prop_label_font_weight_bold);
                            if (dot == 'tab-panel') {
                                $(this).children().find('ul li').css('font-family', prop_label_font_weight_bold);
                            };

                            if (dot == 'accordion') {
                                $(this).children().find('.accordion-header-name').css('font-family', prop_label_font_weight_bold);
                            };

                                
                        }else {
                            $("#lbl_" + objID).css("font-family", font_style);
                            if (dot == 'tab-panel') {
                                $(this).children().find('ul li').css('font-family', font_style);
                            };
                            if (dot == 'accordion') {
                                $(this).children().find('.accordion-header-name').css('font-family', prop_label_font_weight_bold);
                            };
                           
                        };
                    // });

                    break;
                case "lblTxtAlignment":

                    var font_alignment = $(this).val();

                    $("#lbl_" + objID).css("text-align", font_alignment);
                    break;
                    // case "lblFontWg":
                    // 	var lblFontWg = $(this).val();
                    // 	$("#lbl_" + objID).css("font-weight", lblFontWg);
                    // 	break;
                case "lblFontSize":
                    var lblFontSize = $(this).val();
                    var lbl_object_ele = $("#lbl_" + objID);
                    lbl_object_ele.css("font-size", lblFontSize);
                    lbl_object_ele.css("line-height", (Number(lblFontSize.replace("px", "")) - 1) + "px");
                    var dot = $(this).attr('data-type');
                    if(dot == 'accordion'){
                        lbl_object_ele.closest('.setObject').find('.input_position_below_accordion').css("height","calc( 100% - "+(lbl_object_ele.outerHeight())+"px)")
                    }

                    lbl_object_ele.parent().next().css('height', 'calc(100% - ' + lbl_object_ele.height() + 'px)');
                    $('[data-properties-type="lblFieldHeight"]').val(lbl_object_ele.parent().next().height());
                    break;
                case "lblAllowLabel":
                    var lblAllowLabel = $(this).val();
                    if (lblAllowLabel == "Yes") {
                        $("#label_" + objID).show();
                    } else {
                        $("#label_" + objID).hide();
                    }
                    break;
                case "lblFieldType":
                    var lblFieldType = $(this).val();
                    var lblFieldTypeActual = $(this).children("option:selected").attr("data-input-type");
                    $("#getFields_" + objID).attr("data-type", lblFieldType);
                    $("#getFields_" + objID).attr("data-input-type", $(this).children("option:selected").attr("data-input-type"));
                    if($("#getFields_" + objID).parents('.setObject').attr('data-type') == 'smart_barcode_field'){
                        $("#getFields_" + objID).parents('.setObject').find('input[type="text"]').attr("data-type", lblFieldType);
                        $("#getFields_" + objID).parents('.setObject').find('input[type="text"]').attr("data-input-type", $(this).children("option:selected").attr("data-input-type"));
                    }
                    break;
                case "lblAlignment":
                    var lblAlignment = $(this).val();
                    if (lblAlignment == "alignLeft") {
                        // alert("left")
                        // $("#label_" + objID).toggleClass('label_below label_basic');
                        // $("#obj_fields_" + objID).toggleClass('input_position_below input_position');
                        // $("#label_" + objID).removeClass("obj_label");
                        // $("#label_" + objID).css("margin-top", "5px");
                        // $("#label_" + objID).resizable({"handles":"e"})

                        //=========================
                        $("#label_" + objID).addClass("lbl-aligned-left");
                        $("#label_" + objID).children("*").wrap("<div class='lbl-aligned-left-wrap'/>");
                        $("#label_" + objID).resizable({
                            handles: "e"
                        });
                        $("#label_" + objID).children(".ui-resizable-handle").appendTo($("#label_" + objID).children(".lbl-aligned-left-wrap").eq(0));


                        $("#obj_fields_" + objID).addClass("obj_f-aligned-left");

                        $("#obj_fields_" + objID).children("*").wrapAll("<div class='obj_f-aligned-left-wrap'/>");

                        $("#label_" + objID).add($("#obj_fields_" + objID)).wrapAll("<div class='align-left-wrapper'/>").wrapAll("<div class='align-left-wrapper-table'/>").wrapAll("<div class='align-left-wrapper-row'/>");

                    } else {
                        //alert("normal")
                        // $("#label_" + objID).toggleClass('label_basic label_below');
                        // $("#obj_fields_" + objID).toggleClass('input_position input_position_below');
                        // $("#label_" + objID).addClass("obj_label");
                        // $("#label_" + objID).css("margin-top", "0px");

                        //========================
                       // if ($("#label_" + objID).hasClass("ui-resizable")) {
                            console.log($("#label_" + objID))
                            //.resizable("destroy");
                        //}
                        var ele_label_field = $("#label_" + objID).add($("#obj_fields_" + objID));
                        var ele_left_wrapper = ele_label_field.parents(".align-left-wrapper").eq(0);
                        if (ele_left_wrapper.length >= 1) {

                            ele_left_wrapper.after(ele_label_field);
                            ele_left_wrapper.remove();
                            ele_label_field.each(function () {
                                $(this).children(".ui-resizable-handle").remove();
                            })
                        }

                        $("#label_" + objID).find(".lbl-aligned-left-wrap").eq(0).children("*").unwrap();
                        $("#obj_fields_" + objID).find(".obj_f-aligned-left-wrap").eq(0).children("*").unwrap();
                        $("#label_" + objID).removeClass("lbl-aligned-left");
                        $("#label_" + objID).css("width", "");
                        $("#obj_fields_" + objID).removeClass("obj_f-aligned-left");
                    }
                    break;
                case "lblFieldMaxHeight":
                    var lblFieldHeight = $(this).val();
                    if (lblFieldHeight == "none") {
                        $("#getFields_" + objID).css("max-height", lblFieldHeight);
                        $(".content-editable-field_" + objID).css("max-height", lblFieldHeight);
                    } else {
                        var max_h = /*Number($("#getFields_" + objID).css("max-width").replace("px",""))*/Number(lblFieldHeight);
                        var min_h = Number($("#getFields_" + objID).css("min-height").replace("px", ""));
                        min_h = (isNaN(min_h) ? 20 : min_h);
                        if (min_h <= max_h) {
                            $("#getFields_" + objID).css("max-height", lblFieldHeight + "px");
                            $(".content-editable-field_" + objID).css("max-height", lblFieldHeight + "px");
                        } else {
                            $("#getFields_" + objID).css("max-height", min_h + "px");
                            $(".content-editable-field_" + objID).css("max-height", min_h + "px");
                            $(this).val(min_h);
                            value = min_h;
                        }
                    }

                    break;
                case "lblFieldMaxWidth":
                    var lblFieldWidth = $(this).val();
                    if (lblFieldWidth == "none") {
                        $("#getFields_" + objID).css("max-width", lblFieldWidth);
                        $(".content-editable-field_" + objID).css("max-width", lblFieldWidth);
                    } else {
                        var max_w = /*Number($("#getFields_" + objID).css("max-width").replace("px",""))*/Number(lblFieldWidth);
                        var min_w = Number($("#getFields_" + objID).css("min-width").replace("px", ""));
                        min_w = (isNaN(min_w) ? 20 : min_w);
                        if (min_w <= max_w) {
                            $("#getFields_" + objID).css("max-width", lblFieldWidth + "px");
                            $(".content-editable-field_" + objID).css("max-width", lblFieldWidth + "px");
                        } else {
                            $("#getFields_" + objID).css("max-width", min_w + "px");
                            $(".content-editable-field_" + objID).css("max-width", min_w + "px");
                            $(this).val(min_w);
                            value = min_w;
                        }
                    }
                    break;
                case "tableResponsiveContainer":
                    $("#obj_fields_" + objID).find(".form-table").eq(0).attr("fld-table-container-responsive", value);
                    break;
                    //medzz

                case "defaultValueType"://change event
                    var radio_val = $('[name="default_val"]:checked').val();
                    $(".getFields_" + objID).data();
                    {//SAVE LAST DATA
                        if ($(".getFields_" + objID).data("previousData")["defaultType"] == "static") {
                            $(".getFields_" + objID).attr("default-static-value",
                                    $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']").val()
                                    )
                        } else if ($(".getFields_" + objID).data("previousData")["defaultType"] == "computed") {

                            $(".getFields_" + objID).attr("default-formula-value",
                                    $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']").val()
                                    )
                        } else if ($(".getFields_" + objID).data("previousData")["defaultType"] == "middleware") {
                            $(".getFields_" + objID).attr("default-middleware-value",
                                    $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']").val()
                                    )
                        } else if ($(".getFields_" + objID).data("previousData")["defaultType"] == "OnSubmit") {
                            $(".getFields_" + objID).attr("default-onsubmit-value",
                                    $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']").val()
                                    )
                        }
                    }
                    // var updatedFormula = UpdateMentionData.init();
                    if (radio_val == "static") {
                        if ($(".getFields_" + objID).attr("default-static-value")) {
                            $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']")
                                    .val($(".getFields_" + objID).attr("default-static-value"));
                            prop_json["defaultValue"] = $(".getFields_" + objID).attr("default-static-value");
                            json['' + objID + ''] = prop_json;

                        } else {
                            if ($(".getFields_" + objID).data("default-static-value")) {
                                $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']")
                                        .val($(".getFields_" + objID).data("default-static-value"));
                                prop_json["defaultValue"] = $(".getFields_" + objID).data("default-static-value");
                                json['' + objID + ''] = prop_json;
                            } else {
                                $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']")
                                        .val("");
                                prop_json["defaultValue"] = "";
                            }
                            json['' + objID + ''] = prop_json;
                        }
                        json['' + objID + ''].defaultValueType = "static";
                        $(".getFields_" + objID).attr("default-type", "static");

                        $(".getFields_" + objID).data("previousData", {
                            "defaultType": "static"
                        })
                        // $("[data-properties-type='defaultValue']").mention_post("destroy");
                    } else if (radio_val == "computed") {
                        if ($(".getFields_" + objID).attr("default-formula-value")) {
                            $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']")
                                    .val($(".getFields_" + objID).attr("default-formula-value"));
                            prop_json["defaultValue"] = $(".getFields_" + objID).attr("default-formula-value");
                            json['' + objID + ''] = prop_json;
                        } else {
                            $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']")
                                    .val("");
                            prop_json["defaultValue"] = "";
                            json['' + objID + ''] = prop_json;
                        }
                        json['' + objID + ''].defaultValueType = "computed";
                        $(".getFields_" + objID).attr("default-type", "computed");

                        $(".getFields_" + objID).data("previousData", {
                            "defaultType": "computed"
                        })

                        if ($("[data-properties-type='defaultValue']").length >= 1) { //added by aaron
                            // $("[data-properties-type='defaultValue']").mention_post("destroy")
                            // $("[data-properties-type='defaultValue']").mention_post({
                            //     json: updatedFormula, // from local file fomula.json
                            //     highlightClass: "",
                            //     prefix: "",
                            //     countDisplayChoice: "4"
                            // });
                        }
                    } else if (radio_val == "middleware") {
                        if ($(".getFields_" + objID).attr("default-middleware-value")) {
                            $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']").val($(".getFields_" + objID).attr("default-middleware-value"));
                            prop_json["defaultValue"] = $(".getFields_" + objID).attr("default-middleware-value");
                            json['' + objID + ''] = prop_json;
                        } else {
                            $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']")
                                    .val("");
                            prop_json["defaultValue"] = "";
                            json['' + objID + ''] = prop_json;
                        }
                        json['' + objID + ''].defaultValueType = "middleware";
                        $(".getFields_" + objID).attr("default-type", "middleware");

                        $(".getFields_" + objID).data("previousData", {
                            "defaultType": "middleware"
                        })
                        if ($("[data-properties-type='defaultValue']").length >= 1) { //added by aaron
                            // $("[data-properties-type='defaultValue']").mention_post("destroy")
                            // $("[data-properties-type='defaultValue']").mention_post({
                            //     json: updatedFormula, // from local file fomula.json
                            //     highlightClass: "",
                            //     prefix: "",
                            //     countDisplayChoice: "4"
                            // });
                        }
                    } else if (radio_val == "OnSubmit") {
                        if ($(".getFields_" + objID).attr("default-onsubmit-value")) {
                            $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']").val($(".getFields_" + objID).attr("default-onsubmit-value"));
                            prop_json["defaultValue"] = $(".getFields_" + objID).attr("default-onsubmit-value");
                            json['' + objID + ''] = prop_json;
                        } else {
                            $(this).closest(".input_position_below").find("textarea[data-properties-type='defaultValue']")
                                    .val("");
                            prop_json["defaultValue"] = "";
                            json['' + objID + ''] = prop_json;
                        }
                        json['' + objID + ''].defaultValueType = "OnSubmit";
                        $(".getFields_" + objID).attr("default-type", "OnSubmit");

                        $(".getFields_" + objID).data("previousData", {
                            "defaultType": "OnSubmit"
                        })
                        if ($("[data-properties-type='defaultValue']").length >= 1) { //added by aaron
                            // $("[data-properties-type='defaultValue']").mention_post("destroy")
                            // $("[data-properties-type='defaultValue']").mention_post({
                            //     json: updatedFormula, // from local file fomula.json
                            //     highlightClass: "",
                            //     prefix: "",
                            //     countDisplayChoice: "4"
                            // });
                        }
                    }
                    MiddlewareTagging.selectMiddleware(this);
                    $("#content-dialog-scroll").perfectScrollbar("update");
                    break;
                case "tabIndex":
                    $(".getFields_" + objID).attr("tabindex", value);
                    if($('.setObject[data-object-id="'+objID+'"]').is('[data-type="pickList"]')){
                        if($('.setObject[data-object-id="'+objID+'"]').children().is('[allow-values-not-in-list="No"]')){
                             $(".getFields_" + objID).children().attr("tab-index",value);
                        }
                        $(".getFields_" + objID).next().children().attr("tab-index",value);
                    }
                    break;
                case "tableBorderVisible":
                    $("#obj_fields_" + objID).find(".form-table").eq(0).attr("border-visible", value);
                    //if(value == "Yes"){
                    //    $("#obj_fields_" + objID).find(".form-table").eq(0).css("border","1px solid")
                    //}else{
                    //    $("#obj_fields_" + objID).find(".form-table").eq(0).css("border","none");
                    //}
                    break;
                case "middlewareTagging":
                    // $("#obj_fields_" + objID).find(".form-table").eq(0).attr("border-visible", value);
                    break;
                case "fieldFontSize":
                    $("#obj_fields_" + objID).find(".getFields_" + objID).css('height','auto');
                    if ($("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).attr("type") == "radio" || $("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).attr("type") == "checkbox") {
                        $("#obj_fields_" + objID).find(".getFields_" + objID).parent().css("font-size", value);
                        // $("#obj_fields_" + objID).find(".getFields_" + objID).parent().css("line-height", (Number(value.replace("px", "")) + 11) + "px");
                    } else {
                        $("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).css("font-size", value);
                        $("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).css("line-height", (Number(value.replace("px", "")) + 11) + "px");

                        $("#obj_fields_" + objID).find(".content-editable-field_" + objID).eq(0).css("font-size", value);
                        $("#obj_fields_" + objID).find(".content-editable-field_" + objID).eq(0).css("line-height", (Number(value.replace("px", "")) + 11) + "px");
                    }
                    $("#obj_fields_" + objID).find(".getFields_" + objID).filter(':not(input[type="checkbox"],input[type="radio"])').css('height', '100%');
                    $('[data-properties-type="lblFieldHeight"]').val($("#obj_fields_" + objID).find(".getFields_" + objID).height());
                    // $("#obj_fields_" + objID).find(".getFields_" + objID).css('height', '100%');
                    // alert($("#obj_fields_" + objID).find(".getFields_" + objID).outerHeight());
                    $('[data-properties-type="lblFieldHeight"]').val($("#obj_fields_" + objID).find(".getFields_" + objID).outerHeight() - 11);
                    $('[data-properties-type="lblFieldHeight"]').trigger('change');
                    if($('.setObject[data-object-id="'+objID+'"]').is('[data-type="pickList"]')){
                        $("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).css("height","auto");
                        $("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).next().css('vertical-align','middle');
                    }
                    if($('.setObject[data-object-id="'+objID+'"]').is('[data-type="datepicker"]') || $('.setObject[data-object-id="'+objID+'"]').is('[data-type="dateTime"]') || $('.setObject[data-object-id="'+objID+'"]').is('[data-type="time"]')){
                        $("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).css("height","auto");
                    }
                    break;
                case "numberToWordsPropField":
                    $(".getFields_" + objID).each(function () {
                        $(this).attr("num-to-words", value);
                    });
                    break;
                case "borderVisibility":

                    var temp_color = $("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).css("border-left-color");
                    console.log("tempcolor", temp_color);


                    if (value == "No") {


                        $("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).css("border", "0px " + temp_color + "");
                    } else {

                        $("#obj_fields_" + objID).find(".getFields_" + objID).eq(0).css("border", "1px solid " + temp_color + "");

                    }
                    break;
                case "noteStyleFluidContainerCell":
                    $("#setObject_" + objID).attr("fluid-container-cell", value);
                    break;
                case "fieldBackground":

                    break;

            }

            prop_json['' + data_properties_type + ''] = value;
            json['' + objID + ''] = prop_json;

            if (data_properties_type === "lblFieldType") {
                json['' + objID + '']['lblFieldTypeActual'] = lblFieldTypeActual;
            }
            $("body").data(json);
        });
        
        //embedded view row selection
        $("body").on("change", ".embed_row_selection_prop", function(e){
            var object_id = $(this).parents('#popup_container').attr('data-object-id');
            var object_data = $('body').data('' + object_id);
            if(typeof object_data['allow_row_selection'] != "undefined"){
                if(object_data['allow_row_selection'] == false){
                    object_data['allow_row_selection'] = true;
                }
                else{
                    object_data['allow_row_selection'] = false;
                }
            }
            else{
                object_data['allow_row_selection'] = true;
            }
        });
        //embedded view row category
        $("body").on("change", ".embed_row_category_prop", function(e){
            var object_id = $(this).parents('#popup_container').attr('data-object-id');
            var object_data = $('body').data('' + object_id);
            if(typeof object_data['allow_row_category'] != "undefined"){
                if(object_data['allow_row_category'] == false){
                    object_data['allow_row_category'] = true;
                    $(this).closest('.input_position_below').find('.column_category_selection').show();
                }
                else{
                    object_data['allow_row_category'] = false;
                    $(this).closest('.input_position_below').find('.column_category_selection').hide();
                }
            }
            else{
                object_data['allow_row_category'] = true;
                $(this).closest('.input_position_below').find('.column_category_selection').show();
            }
        });
        $("body").on("click", ".single-form-row-category-refresh", function(e){
            var object_id = $(this).parents('#popup_container').attr('data-object-id');
            var object_data = $('body').data('' + object_id);
            // var single_columns = JSON.parse($('.getFields_' + object_id).attr('embed-column-data')||"[]");
            var single_columns  = $('.embedded-view-columns > tbody > tr:not(:eq(0))').map(function(){
                if(typeof $(this).find('input[type="text"]').val() != "undefined" && $(this).find('input[type="text"]').val() != ""){
                    var json = {};
                    json['FieldName'] = $(this).find('div').text();
                    json['FieldLabel'] = $(this).find('input[type="text"]').val();
                    return json;
                }
            }).get().filter(Boolean);
            var columns = "";
            if(single_columns.length > 0){
                for(i in single_columns){
                    var column_data  = single_columns[i];
                    var is_selected = "";
                    if(column_data['FieldName'] == object_data['allow_row_category_column']){
                       is_selected = " selected";
                    }   
                    columns += "<option value='" + column_data['FieldName'] + "'" + is_selected + ">" + column_data['FieldLabel'] + "</option>";
                }
            }
            $('.embedRowSelection > .fields_below .input_position_below > .div_1_of_1 > .column_category_selection > .embed_row_category_column_dropdown').html(columns);
        });
    }



    $("[default-type]").each(function () {

        $(this).data("previousData", {
            "defaultType": $(this).attr("default-type")
        });

        if (typeof $(this).attr("default-formula-value") != "undefined") {
            $(this).data("defaultFormulaValue", $(this).attr("default-formula-value"));
        }
        if ($(this).attr("default-static-value") != "undefined") {
            $(this).data("defaultStaticValue", $(this).attr("default-static-value"));
        }
        if (typeof $(this).attr("default-middleware-value-value") != "undefined") {
            $(this).data("defaultMiddlewareValue", $(this).attr("default-middleware-value"));
        }
    })


    resizeForm($(".formbuilder_ws"), $(".formbuilder_ws").outerHeight());
    // alert("MAY HANDLE? "+$('.formbuilder_ws').children('.ui-resizable-handle').filter(':visible').length)
    if ($('.table-form-design-relative:visible').length >= 1) {
        $(".formbuilder_ws").children('.ui-resizable-handle').hide();
    }

    ui.unblock();
    CustomScriptingToolsUI.init();
    FormulaListUI.init();

    //added by japhet morada 01-27-2016 field themes

    if($('#field_themes').text() != "0"){
        $('.field-theme-selection').val($('body').data('current_theme')||"none");
        $('.field-theme-selection').on('change', function(e){
            applyFieldTheme($(this).val(),  $('body').data('current_theme')||"");
            $('body').data('current_theme', $(this).val());
        });
    }

    //added by japhet morada
    //add ajax for reducing the on load speed of the formbuilder
    $.post('/ajax/sharelink', {}, function(echo){
        var user_forms = $('<div class="user-forms display"></div>');
        user_forms.text(echo);
        $('.fl-main-content-wrapper').prepend(user_forms);
    });

    
});

/*
 * Function to select what type of object 
 *  you want to add on the workspace
 *
 */
function getDefaultValues() {

}


function objType(object_type, object_drop, count, container_height) {

    var dis_event_data = this;
    var drop_location = $("." + object_drop);
    // if (
    //         $(".dynamic-td-droppable-selected").length == 1 &&
    //         object_type != "tabPanel" &&
    //         object_type != "table"
    //    ) {
    //     drop_location = $(".dynamic-td-droppable-selected").children(".td-relative").eq(0);
    // } else {

    // } 

    if (
            $(".f-show-ruler[name='show-form-ruler-select']:checked").val() == "yes" &&
            $(".form-builder-ruler.vertical").length >= 1 &&
            $(".form-builder-ruler.horizontal").length >= 1
            ) {
        drop_left = $(".topPointerRuler-tip").position().left;
        drop_top = $(".leftPointerRuler-tip").position().top;
    } else {
        drop_top = $(document).scrollTop() + wpcfb;
        drop_left = $(document).scrollLeft() + wpcfb;
    }

    var appended_ele = "";
    switch (object_type) {

            case "textTag":
            // alert("dsadas");
            // save_history();

            console.log("counter", count);
            save_history();

            appended_ele = $(tagging(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;

            break;
        case "contactNumberField":
            save_history();

            appended_ele = $(contactNumber(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            contact_number_functionality.events($(appended_ele));
            count++;

            break;
        case "colorpicker":
            save_history();
            appended_ele = $(ColorPickerObject(count, object_type));
            drop_location.append(appended_ele);
            appended_ele.find('.forma-colorpicker').spectrum(default_spectrum_settings);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;
            break;
        case "detailsPanel":
            save_history();
            appended_ele = $(detailsPanelField(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;
            break;
        case "qrCodeScanner":
            save_history();
            appended_ele = $(QRCodeField(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);

            count++;
            break;
        case "smartBarCode":
            save_history();
            appended_ele = $(smartBarcodeField(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);

            count++;
            break;
        case "accordion":
            save_history();
            appended_ele = $(createAccordion(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);

            $(appended_ele).find(".form-accordion").accordion(default_accordion_settings).sortable({
                axis: "y",
                handle: "h3",
                stop: function (event, ui) {
                    // IE doesn't register the blur when sorting
                    // so trigger focusout handlers to remove .ui-state-focus
                    ui.item.children("h3").triggerHandler("focusout");

                    // Refresh accordion to handle new order
                    $(this).accordion("refresh");
                }
            });
            var accordion_group = appended_ele.find('.form-accordion-section-group:eq(0)');
            accordionAction(accordion_group);
            count++;

            break;
        case "textbox_reader_support":
            save_history();
            appended_ele = $(textbox_reader_support(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;
            /*redo undo*/

            break;
        case "textbox_editor_support":
            save_history();
            appended_ele = $(textbox_editor_support(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
            // case "textbox_support":
            //     appended_ele = $(textbox_ersupport(count, object_type));
            //     drop_location.append(appended_ele);
            //     $(appended_ele).css({
            //         "top": (drop_top) + "px",
            //         "left": (drop_left) + "px"
            //     });
            //     dragObects($(appended_ele), drop_location, container_height, count);
            //     count++;
            //     break;
        case "labelOnly":
            save_history();
            appended_ele = $(labelOnly(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "timer":
            save_history();
            appended_ele = $(autoTimer(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "textbox":
            console.log("counter", count);
            save_history();

            appended_ele = $(textbox(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "checkbox":
            save_history();
            appended_ele = $(checkbox(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "dropdown":
            save_history();
            appended_ele = $(dropdown(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "radioButton":
            save_history();
            appended_ele = $(radioButton(count, object_type))
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "textarea":
            save_history();
            appended_ele = $(textArea(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px",
                "width": "200px",
                "height": "100px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "selectMany":
            save_history();
            appended_ele = $(selectMany(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "fileUpload":
            save_history();
            appended_ele = $(fileUpload(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "datepicker":
            save_history();
            appended_ele = $(datepicker(count, object_type))
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "fullname":
            save_history();
            appended_ele = $(fullname(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "dateTime":
            save_history();
            appended_ele = $(dateTime(count, object_type))
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "website":
            save_history();
            appended_ele = $(website(count, object_type))
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "fullAddress":
            save_history();
            appended_ele = $(fullAddress(count, object_type))
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "button":
            // save_history('btn');
            appended_ele = $(btn(count, object_type))

            drop_location.append(appended_ele);

            $(".ws_btn_label").hide();
            dragObects($(appended_ele), drop_location, "", count);
            //$(appended_ele).parents('.formbuilder_page_btn.btn_content').eq(0).sortable('refresh')
            count++;

            break;
        case "line":
            save_history();
            appended_ele = $(createLine(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "listNames":
            save_history();
            appended_ele = $(fieldListOfNames(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "table":
            save_history();
            appended_ele = $(createTable(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);

            $(appended_ele).find(".form-table").dynamicTable({
                "tableBorder": "1",
                "tHeadTag": false,
                droppableCell: true,
                cellDroppable: {
                    accept: ".setObject",
                    revert: true,
                    greedy: true,
                    drop: function (e, ui) {
                        //comment for reference use
                        //evtarget = (window.event) ? window.event.srcElement /* for IE */ : e.target;
                        //console.log(e.target);
                        //console.log(ui.draggable[0].parentElement);
                        var target = e.target;
                        var source = ui.draggable[0].parentElement;
                        if (
                                $(ui.helper).find(".form-table").length == 0 && $(ui.helper).find(".form-tabbable-pane").length == 0 ||
                                ($(".table-form-design-relative").length >= 1 && $(target).hasClass("fcp-td-relative"))
                                ) {
                            if (target === source) { //conditional purpose to prevent dropping off the same place
                                //alert('Same droppable container');
                            } else {

                                // console.log("TRALALA",$(target), $(source), $(this), $(ui.helper));
                                // console.log("SABOG",$(ui.helper).is('[data-type="noteStyleTextarea"]') , $(target).hasClass('td-relative') , $(target).children('.setObject').length >= 1);
                                if ($(ui.helper).is('[data-type="noteStyleTextarea"]') && $(target).hasClass('td-relative') && $(target).children('.setObject').length >= 1) {
                                    // console.log("APPENDTO", source)
                                    jAlert("Notes Style requires empty cell pane to drop.", "", "", "", "", function () {
                                    });
                                    $(ui.helper).appendTo(source);
                                } else if ($(target).hasClass('td-relative') && $(target).children('.setObject[data-type="noteStyleTextarea"]').length >= 1) {
                                    jAlert("You are not allowed to drop other fields in a cell pane which a Notes Style is present.", "", "", "", "", function () {
                                    });
                                    $(ui.helper).appendTo($(source));
                                } else if ($(target).hasClass('td-relative') && $(target).children('.setObject[data-type="table"]').length >= 1) {
                                    jAlert("You are not allow to drop any other fields in that cell pane if a table is present.", "", "", "", "", function () {
                                    });
                                    $(ui.helper).appendTo($(source));
                                } else {

                                    if ($(this).children(".td-relative").eq(0).length == 0) {
                                        $(ui.helper).appendTo(this);
                                        $(ui.helper).css({
                                            "top": "0px",
                                            "left": "0px"
                                        });
                                    } else {
                                        $(ui.helper).appendTo($(this).children(".td-relative").eq(0));
                                        $(ui.helper).css({
                                            "top": "0px",
                                            "left": "0px"
                                        });
                                    }
                                    console.log("IMPOSSIBLE", $(ui.helper).outerWidth(), $(this).outerWidth());
                                    console.log("GRAAA", $(ui.helper), $(this));
                                    if ($(ui.helper).outerWidth() > $(this).outerWidth()) {
                                        ung_padding_left = $(ui.helper).css("padding-left").split("px").join("");
                                        ung_padding_right = $(ui.helper).css("padding-right").split("px").join("");
                                        draggable_ele_width = $(ui.helper).outerWidth();
                                        var collect_most_right = [draggable_ele_width];

                                        if ($(this).hasClass("ui-droppable")) {
                                            dis_tr_index = $(this).closest("tr").index();
                                            if ($(this).prop("tagName") == "TD") {
                                                dis_td_index = $(this).index();
                                            } else {
                                                dis_td_index = $(this).closest("td.ui-resizable").index();
                                            }

                                            var dropped_column = $(this).closest("tbody").children("tr").children("td.ui-resizable").filter(function (eqi, ele) {
                                                if ($(ele).index() == dis_td_index) {
                                                    return true;
                                                } else {
                                                    return false;
                                                }
                                            });
                                            dropped_column.children(".td-relative").children(".setObject").each(function (eqi, elem) {
                                                collect_most_right.push($(this).outerWidth() + $(this).position().left);
                                            });
                                            collect_most_right.sort(function (a, b) {
                                                return b - a;
                                            });
                                            dropped_column.css({
                                                "width": (collect_most_right[0]) + "px"
                                            });
                                            dropped_column.children(".td-relative").css({
                                                "min-width": (collect_most_right[0]) + "px"
                                            });
                                            console.log("AMP", dropped_column.children(".td-relative").children(".setObject"))

                                            // $(this).closest("tbody").children("tr").each(function() {
                                            //     $(this).children("td.ui-resizable").eq(dis_td_index).css({
                                            //         "width": draggable_ele_width
                                            //     });
                                            //     console.log("TEKA HERE DROPPABLE",$(this).children("td.ui-resizable").eq(dis_td_index))
                                            //     // $(this).children("td.ui-resizable").eq(dis_td_index).resizable({
                                            //     //     "minWidth": draggable_ele_width
                                            //     // });
                                            // });
                                        }
                                    }
                                    if ($(ui.helper).outerHeight() > $(this).outerHeight()) {
                                        ung_padding_top = $(ui.helper).css("padding-top").split("px").join("");
                                        ung_padding_bottom = $(ui.helper).css("padding-bottom").split("px").join("");
                                        draggable_ele_height = $(ui.helper).outerHeight();
                                        if ($(this).hasClass("ui-droppable")) {
                                            $(this).closest("tr").children("td.ui-resizable").css({
                                                "height": draggable_ele_height
                                            });
                                            // $(this).closest("tr").children("td.ui-resizable").resizable({
                                            //     "minHeight": draggable_ele_height
                                            // });
                                        }
                                    }
                                    if ($(source).hasClass("td-relative")) {
                                        var collect_objs_bottom = [];
                                        var collect_objs_right = [];
                                        $(source).children(".setObject:visible").each(function () {
                                            collect_objs_bottom.push($(this).position().top + $(this).outerHeight());
                                            collect_objs_right.push($(this).position().left + $(this).outerWidth());
                                        });
                                        var dis_tr_index = $(source).closest("tr").index();
                                        if ($(source).prop("tagName") == "TD") {
                                            var dis_td_index = $(source).index();
                                        } else {
                                            var dis_td_index = $(source).closest("td.ui-resizable").index();
                                        }
                                        collect_objs_bottom.concat(
                                                $(source).closest("tr").eq(dis_tr_index).children("td").children(".td-relative").children(".setObject:visible").get().map(function (ele, eqi) {
                                            return ($(ele).position().top + $(ele).outerHeight());
                                        })
                                                );

                                        collect_objs_right.concat(
                                                $(source).closest("tbody").children("tr").children("td").filter(function (eqi, ele) {
                                            if ($(ele).index() == dis_td_index) {
                                                return true;
                                            } else {
                                                return false;
                                            }
                                        }).children(".td-relative").children(".setObject:visible").get().map(function (ele, eqi) {
                                            return ($(ele).position().left + $(ele).outerWidth());
                                        })
                                                );

                                        collect_objs_bottom.sort(function (a, b) {
                                            return b - a;
                                        })
                                        collect_objs_right.sort(function (a, b) {
                                            return b - a;
                                        });

                                        if (collect_objs_bottom.length >= 1 && collect_objs_right.length >= 1) {










                                            $(source).closest("tbody").children("tr").each(function () {
                                                $(this).children("td.ui-resizable").eq(dis_td_index).css({
                                                    "width": collect_objs_right[0]
                                                });
                                                $(this).children("td.ui-resizable").eq(dis_td_index).resizable({
                                                    "minWidth": collect_objs_right[0]
                                                });
                                                if ($(this).index() == dis_tr_index) { // buong row dapat magkakapareha ng height
                                                    $(this).children("td.ui-resizable").each(function () {
                                                        $(this).css({
                                                            "height": collect_objs_bottom[0]
                                                        });
                                                        $(this).children("td.ui-resizable").resizable({
                                                            "minHeight": collect_objs_bottom[0]
                                                        });
                                                    })
                                                }
                                            })
                                        } else {
                                            console.log("HEYO! DELO", $(source).closest("td.ui-resizable"))
                                            $(source).closest("td.ui-resizable").resizable({
                                                "minHeight": 0,
                                                "minWidth": 0
                                            });
                                        }
                                    }
                                }

                            }


                            $(ui.helper).draggable("option", "containment", "parent");
                            if ($(ui.helper).data()) {
                                if ($(ui.helper).data().uiResizable) {
                                    $(ui.helper).resizable("option", "containment", "parent");
                                }
                            }

                            // alert("test");
                            // console.log($(source));
                            //set a min height and min width of the cell when object leaves a cell

                        }
                        var tableSetObject = $(this).closest('.setObject')
                        var collision = tableSetObject.testCollision();
                        console.log("collision", collision, $(this));
                        if (collision["final_collision_bot"] || collision["final_collision_right"]) {
                            var newObjLeft = tableSetObject.position().left - collision["final_collide_overflow_right"];
                            var newObjBot = tableSetObject.position().top - collision["final_collide_overflow_bot"];
                            tableSetObject.css({
                                left: newObjLeft,
                                top: newObjBot
                            });

                        }

                    }
                }
            }).css({
                "border": "none"
            });
            count++;

            break;
        case "tabPanel":
            save_history();
            appended_ele = $(createTabPanel(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);

            $(appended_ele).find(".form-tabbable-pane").tabPanel({
                resizableContent: true,
                resizableContentContainment: ".workspace.formbuilder_ws"
            });
            count++;

            break;
        case "computed":
            save_history();
            appended_ele = $(computedField(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "embedView":
            save_history();
            appended_ele = $(embedView(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "pickList":
            save_history();
            appended_ele = $(pickList(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "requestImage":

            save_history();
            appended_ele = $(photos(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "barCodeScanner":
            save_history();
            appended_ele = $(barCodeScanner(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "time":
            save_history();
            appended_ele = $(TimePick(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "noteStyleField":
            save_history();
            appended_ele = $(noteStyleTextArea(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "requestImport":
            save_history();
            appended_ele = $(requestImport(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "multiplerequestImport":
            save_history();
            appended_ele = $(multiple_requestImport(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
        case "formDataTable":
            save_history();
            appended_ele = $(formDataTable(count, object_type));
            drop_location.append(appended_ele);
            $(appended_ele).css({
                "top": (drop_top) + "px",
                "left": (drop_left) + "px"
            });
            dragObects($(appended_ele), drop_location, container_height, count);
            count++;

            break;
    }




    // PAG MAY MGA NAKA HAYLIGHT CONTAINERS

    if ($(".table-form-design-relative").length <= 0) {
        var grid_opts = false;
        if (dis_event_data) {
            if (dis_event_data['auto'] && dis_event_data['form_behavior_type']) {
                if (dis_event_data['form_behavior_type'] == 'relative') {
                    $('.formbuilder_ws').children('.ui-resizable-handle').hide()

                    $(appended_ele).css({
                        "position": "relative",
                        "top": "",
                        "left": "",
                        "padding": "0px",
                    }).attr('style', $(appended_ele).attr('style') + ";outline:none");

                    $(appended_ele).find('tbody').eq(0)
                            .children('tr')
                            .children('td')
                            .children('.td-relative').addClass('fcp-td-relative');
                    // .each(function(){
                    // 	$(this).addClass('fcp-td-relative');
                    // });

                    $(appended_ele).find('.obj_label').eq(0).hide();

                    if ($(appended_ele).is(':data("uiDraggable")')) {
                        $(appended_ele).draggable('destroy');
                    }

                    $(appended_ele).addClass('table-form-design-relative');
                    $(appended_ele).off('.doubleClickProp');
                    $(appended_ele).off('.fieldObjectFocus');

                    var button_row_add = $(appended_ele).find(".action-row-add").eq(0).children(".table-add-row");
                    var button_col_add = $(appended_ele).find(".action-column-add").eq(0).children(".table-add-column");

                    button_row_add.addClass("fcp-table-add-row");
                    button_col_add.addClass("fcp-table-add-column");

                    button_col_add.on({
                        "click.formCellPaneBehavior": function () {
                            var fcp_cell_size_ele = $(".fcp-cell-size");
                            if (fcp_cell_size_ele.length >= 1 /*&& fcp_cell_size_ele.is(":visible")*/) {
                                var fcp_cell_width_size = Number(fcp_cell_size_ele.filter(".fcp-cell-width-size").val());
                                var fcp_cell_height_size = Number(fcp_cell_size_ele.filter(".fcp-cell-height-size").val());
                                $(this).parents(".table-wrapper").eq(0).find(".form-table").eq(0)
                                        .children("tbody").children("tr").each(function () {
                                    $(this).children("td").last().css({
                                        "width": (fcp_cell_width_size) + "px",
                                        "height": (fcp_cell_height_size) + "px"
                                    }).children(".td-relative").eq(0).css({
                                        "min-width": (fcp_cell_width_size) + "px",
                                        "min-height": (fcp_cell_height_size) + "px"
                                    }).addClass("fcp-td-relative")
                                })
                            }
                        }
                    });
                    button_row_add.on({
                        "click.formCellPaneBehavior": function () {
                            var fcp_cell_size_ele = $(".fcp-cell-size");
                            if (fcp_cell_size_ele.length >= 1 /*&& fcp_cell_size_ele.is(":visible")*/) {
                                var fcp_cell_width_size = Number(fcp_cell_size_ele.filter(".fcp-cell-width-size").val());
                                var fcp_cell_height_size = Number(fcp_cell_size_ele.filter(".fcp-cell-height-size").val());
                                $(this).parents(".table-wrapper").eq(0).find(".form-table").eq(0)
                                        .children("tbody").children("tr").last().children("td").each(function () {
                                    $(this).css({
                                        "width": (fcp_cell_width_size) + "px",
                                        "height": (fcp_cell_height_size) + "px"
                                    }).children(".td-relative").eq(0).css({
                                        "min-width": (fcp_cell_width_size) + "px",
                                        "min-height": (fcp_cell_height_size) + "px"
                                    }).addClass("fcp-td-relative");
                                });
                            }
                        }
                    });
                    var dimension_col = 4;
                    var dimension_row = 4;
                    var width_cell = ($('.formbuilder_ws').width() / dimension_col) - 2;
                    var height_cell = ($('.formbuilder_ws').height() / dimension_row) - 1;
                    var collector = $();
                    collector = collector
                            .add($('.fcp-cell-width-size').val(width_cell))
                            .add($('.fcp-cell-height-size').val(height_cell))
                            .add($('.fcp-cell-pane-columns').val(dimension_col))
                            .add($('.fcp-cell-pane-rows').val(dimension_row))
                    collector.trigger('change');
                    console.log("DATA", $(appended_ele).data(), "\nEVENT", $(appended_ele).data('events'));
                }
            }
        }
        if (
            $(".dynamic-td-droppable-selected").length == 1 &&
            object_type != "tabPanel" &&
            object_type != "table" &&
            object_type != "button"
        ) {
            drop_location = $(".dynamic-td-droppable-selected").children(".td-relative").eq(0);
            $(appended_ele).appendTo(drop_location);
            // console.log("YYY")
            // console.log(appended_ele)
            appended_ele_width = appended_ele.outerWidth();
            appended_ele_height = appended_ele.outerHeight();
            $(".dynamic-td-droppable-selected").css({
                "width": (appended_ele_width) + "px",
                "height": (appended_ele_height) + "px"
            });
            appended_ele.css({
                "top": (0) + "px",
                "left": (0) + "px"
            });
        }else if(
            $('.component-primary-focus').length == 1 &&
            $('.component-primary-focus').is('[data-type="accordion"]') &&
            appended_ele.is(':not(.table-form-design-relative)')
        ){ //IF PRIMARY FOCUS ON A CONTAINER
            drop_location = $('.component-primary-focus').find('.ui-accordion-content-active:visible').eq(0);
            appended_ele.appendTo(drop_location);
            appended_ele.css({
                "top": "",
                "left": ""
            });
        }


    } else if ($(".table-form-design-relative").length >= 1) {
        if (object_type != "button") {
            if ($(".dynamic-td-droppable-selected").length >= 1 /*&& $(".dynamic-td-droppable-selected").children(".td-relative").hasClass("fcp-td-relative")*/) { //kapag ung drop location merong fcp-td-relative
                drop_location = $(".dynamic-td-droppable-selected").children(".td-relative").eq(0);
                // console.log("drop_location",drop_location)
                if (drop_location.children('.setObject[data-type="noteStyleTextarea"]').length >= 1) {
                    jAlert("You are not allowed to drop other fields in a cell pane which a Notes Style is present.", "", "", "", "", function () {
                    });
                    $(appended_ele).remove();
                    // }else if(drop_location.children('.setObject[data-type="table"]').length >= 1){
                    //     jAlert("You are not allow to drop any other fields in that cell pane if a table is present.", "", "", "", "", function() {
                    //     });
                    //     $(appended_ele).remove();
                } else if (drop_location.hasClass("fcp-td-relative") && $(appended_ele).is('[data-type="table"]') && drop_location.children('.setObject').length >= 1) {
                    jAlert("You cannot drop your table to at the selected cell, requires empty cell.", "", "", "", "", function () {
                    });
                    $(appended_ele).remove();
                } else if (drop_location.children(".setObject").length >= 1 && $(appended_ele).is('[data-type="noteStyleTextarea"]')) {
                    jAlert("Notes Style requires empty cell pane to drop.", "", "", "", "", function () {
                    });
                    $(appended_ele).remove();
                } else if (drop_location.hasClass("fcp-td-relative") && $(appended_ele).is('[data-type="table"]')) {
                    $(appended_ele).appendTo(drop_location);
                    appended_ele_width = appended_ele.outerWidth();
                    appended_ele_height = appended_ele.outerHeight();
                    $(".dynamic-td-droppable-selected").css({
                        "width": (appended_ele_width) + "px",
                        "height": (appended_ele_height) + "px"
                    });
                    appended_ele.css({
                        "top": (0) + "px",
                        "left": (0) + "px"
                    });
                } else if (!drop_location.hasClass("fcp-td-relative") && $(appended_ele).is('[data-type="table"]')) {
                    jAlert("You cannot drop your table to an object table.<br/>You can drop a table to the workspace given", "", "", "", "", function () {
                    });
                    $(appended_ele).remove();
                    // drop_location = $(".fcp-td-relative").eq(0);
                    // $(appended_ele).appendTo(drop_location);
                    // appended_ele_width = appended_ele.outerWidth();
                    // appended_ele_height = appended_ele.outerHeight();
                    // $(drop_location).css({
                    //     "width": (appended_ele_width) + "px",
                    //     "height": (appended_ele_height) + "px"
                    // });
                    // appended_ele.css({
                    //     "top": (0) + "px",
                    //     "left": (0) + "px"
                    // });
                } else {
                    $(appended_ele).appendTo(drop_location);
                    appended_ele_width = appended_ele.outerWidth();
                    appended_ele_height = appended_ele.outerHeight();
                    $(".dynamic-td-droppable-selected").css({
                        "width": (appended_ele_width) + "px",
                        "height": (appended_ele_height) + "px"
                    });
                    appended_ele.css({
                        "top": (0) + "px",
                        "left": (0) + "px"
                    });
                }
            
            }else if(
                $('.component-primary-focus').length == 1 &&
                $('.component-primary-focus').is('[data-type="accordion"]') &&
                appended_ele.is(':not(.table-form-design-relative)')
            ){ //IF PRIMARY FOCUS ON A CONTAINER

                drop_location = $('.component-primary-focus').find('.ui-accordion-content-active:visible');
                appended_ele.appendTo(drop_location);
                appended_ele.css({
                    "top": "",
                    "left": ""
                });
            } else if ($(".dynamic-td-droppable-selected").length == 0) {
                drop_location = $(".fcp-td-relative").eq(0);
                if ($(appended_ele).is('[data-type="noteStyleTextarea"]')) {
                   
                    var newAlert = new jAlert("Please select a specific cell for notes style.", '', '', '', '', function (data) {
                        
                    });

                    newAlert.themeAlert("jAlert2", {
                        'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                    });
                    $(appended_ele).remove();
                } else if (dis_event_data) {
                    if (dis_event_data['auto'] && dis_event_data['autoAddTab']) {
                        // console.log("dis_event_data",dis_event_data['autoAddTab'])
                        var flag_visibility_false_switch = false;
                        if (dis_event_data['autoAddTab'].filter(".ui-droppable").eq(0).is(':visible') == false) {
                            flag_visibility_false_switch = true;
                            dis_event_data['autoAddTab'].filter(".ui-droppable").eq(0).show();
                        }
                        dis_event_data['autoAddTab'].filter(".ui-droppable").eq(0).prepend(appended_ele);
                        $(appended_ele).css({
                            "top": "0",
                            "left": "0"
                        }).attr('style', $(appended_ele).attr('style') + ";position:relative !important");

                        $(appended_ele).find('button.table-add-column').eq(0).trigger('click'/*,[{'auto':true, "cellSize":[200,100]}]*/);
                        $(appended_ele).find('button.table-add-row').eq(0).trigger('click'/*,[{'auto':true, "cellSize":[200,100]}]*/);
                        $(appended_ele).find('.obj_label').eq(0).remove();
                        $(appended_ele).off('mousedown.fieldObjectFocus');
                        var tab_containment_width = dis_event_data['autoAddTab'].filter(".ui-droppable").eq(0).outerWidth();
                        var tab_containment_height = dis_event_data['autoAddTab'].filter(".ui-droppable").eq(0).outerHeight();
                        var counted_td_col = $(appended_ele).find('.form-table').eq(0).children('tbody').children('tr').eq(0).children('td').length;
                        $(appended_ele).find('.form-table').eq(0).children('tbody').children('tr').children('td').css({
                            "width": (tab_containment_width / counted_td_col) + "px",
                            "height": (100) + "px"
                        });
                        if ($(appended_ele).parent().is('.ui-tabs-panel.ui-droppable')) {
                            var height = $(appended_ele).parent()
                            $(appended_ele).parent().css({
                                "overflow": "visible",
                                "width": "",
                                "height": "",
                                "display": "inline-block",
                                "min-width": "100%",
                                "min-height": "100%"
                            });
                            $(appended_ele).draggable('destroy');
                        }
                        $(appended_ele).addClass('fdr-table-for-tab-panel');
                        $(appended_ele).find('.form-table').eq(0).children('tbody').children('tr').children('td').children('.td-relative').addClass('fdr-cell-for-tab-panel');

                        var setobj_tabpanel_ele = dis_event_data['autoAddTab'].filter(".ui-droppable").eq(0).parents('.setObject[data-type="tab-panel"]').eq(0);
                        var setobj_parent_ele = setobj_tabpanel_ele.parent();
                        if (setobj_parent_ele.is('.td-relative') && (setobj_tabpanel_ele.outerWidth() > setobj_parent_ele.outerWidth())) {
                            var overrun_width = setobj_tabpanel_ele.outerWidth() - setobj_parent_ele.outerWidth();
                            setobj_parent_ele.parents('td.ui-resizable').resizable('resizeBy', {
                                "width": overrun_width
                            });
                        }
                        if (setobj_parent_ele.is('.td-relative') && (setobj_tabpanel_ele.outerHeight() > setobj_parent_ele.outerHeight())) {
                            // alert(2345)
                            var overrun_height = setobj_tabpanel_ele.outerHeight() - setobj_parent_ele.outerHeight();
                            setTimeout(function () {
                                setobj_parent_ele.parents('td.ui-resizable').resizable('resizeBy', {
                                    "height": overrun_height
                                });
                            }, 500);
                        }

                        if (dis_event_data['autoAddTab'].filter(".ui-droppable").eq(0).children('.setObject:not(.fdr-table-for-tab-panel)').length >= 1) {
                            dis_event_data['autoAddTab'].filter(".ui-droppable").eq(0).children('.setObject:not(.fdr-table-for-tab-panel)').appendTo(
                                    $(appended_ele).find('.form-table').eq(0).children('tbody').children('tr').eq(0).children('td').eq(0)
                                    );
                        }

                        if (flag_visibility_false_switch) {
                            dis_event_data['autoAddTab'].filter(".ui-droppable").eq(0).hide();
                        }
                    }
                } else if ($(appended_ele).is('[data-type="table"]')) {//alert((dis_event_data)?dis_event_data['auto']:"WALA")  //autoAddTab
                    jAlert("Please select a specific cell for table.", "", "", "", "", function () {
                    });
                    $(appended_ele).remove();
                } else {
                    $(appended_ele).appendTo(drop_location);
                    appended_ele_width = appended_ele.outerWidth();
                    appended_ele_height = appended_ele.outerHeight();
                    drop_location.css({
                        "width": (appended_ele_width) + "px",
                        "height": (appended_ele_height) + "px"
                    });
                    appended_ele.css({
                        "top": (0) + "px",
                        "left": (0) + "px"
                    });
                }
            } else {
                //teka loading_content
                // drop_location = $(".dynamic-td-droppable-selected").children(".td-relative").eq(0);
            }

            if ($(appended_ele).is('[data-type="tab-panel"]')) {
                $(appended_ele).children('.ui-resizable-handle').hide();
            }
        }
    }




    collectNames();
    if ($(appended_ele).not('[data-type="button"]').exists()) {
        $(appended_ele).not('[data-type="button"]').snapToGrid();
    }

    $(appended_ele).trigger('mousedown.fieldObjectFocus');
    
    var elements = $(appended_ele);
    var objID = elements.attr('data-object-id');
    var setObjIdTag = $('#setObject_'+elements.attr('data-object-id'));
    var labelTag = $('#label_'+elements.attr('data-object-id'));
    var fieldTag = $('#getFields_'+elements.attr('data-object-id'));
    objectCSSProperties(objID, setObjIdTag, labelTag, fieldTag, elements);

}

function fire_onBeforeOnload(status) {
    if (status == 0) {
        window.onbeforeunload = function () {
            // blank function do nothing
        }
    } else {
        window.onbeforeunload = function (e) {
            return 'You will lose any unsaved edits';
        };
    }
}





function checkParentResizableMinHeight(passedObjEle) {
    var the_parent_resizable = $(passedObjEle).parents(".ui-resizable").eq(0);


    var add_padding = 0;
    var dragObjBottomPosition = [];
    try {
        if (the_parent_resizable.children(".td-relative").length >= 1) {
            the_parent_resizable.children(".td-relative").children(".ui-draggable").each(function () {
                dragObjBottomPosition.push(($(this).position().top) + ($(this).outerHeight()));
            });
        } else {
            the_parent_resizable.children(".ui-draggable").each(function () {
                if ($(this).is('[data-type="createLine"]')) {

                    var the_line = $(this).find('.input_position_below').find('div[data-type="createLine"]').eq(0);

                    dragObjBottomPosition.push(($(this).position().top) + ($(the_line).outerHeight() + $(the_line).parents('.setObject').find('.label_below').outerHeight()));
                } else {
                    dragObjBottomPosition.push(($(this).position().top) + ($(this).outerHeight()));
                }

            });
        }
        rets = dragObjBottomPosition.sort(function (a, b) {
            return b - a;
        });
        /*console.log("minHeight", rets);*/
        return rets[0] + add_padding;
    } catch (error) {
        console.log("function checkParentResizableMinHeight: the resizable is not yet initialized ")
    }
}

function checkParentResizableMinWidth(passedObjEle) {

    var the_parent_resizable = $(passedObjEle).parents(".ui-resizable").eq(0);
    /*console.log("widddd", the_parent_resizable)*/
    var add_padding = 0;
    var dragObjRightPosition = [];
    try {
        if (the_parent_resizable.children(".td-relative").length >= 1) {
            the_parent_resizable.children(".td-relative").children(".ui-draggable").each(function () {
                dragObjRightPosition.push(($(this).position().left) + ($(this).outerWidth()));
            });
        } else {
            the_parent_resizable.children(".ui-draggable").each(function () {
                if ($(this).is('[data-type="createLine"]')) {
                    var the_line = $(this).find('.input_position_below').find('div[data-type="createLine"]').eq(0);
                    dragObjRightPosition.push(($(this).position().left) + 2 + ($(the_line).outerWidth()));
                } else {
                    dragObjRightPosition.push(($(this).position().left) + ($(this).outerWidth()));
                }

            });
        }
        rets = dragObjRightPosition.sort(function (a, b) {
            return b - a;
        });
        return (rets[0]||0) + add_padding;
    } catch (error) {
        console.log("function checkParentResizableMinWidth: the resizable is not yet initialized ")
    }
    return 0;
}
function defaultValuesFN(object_id) {

    //this default value is for multiple selection options and choices
    if ($(".obj_defaultValueChoices").length >= 1) {

        json = $("body").data();

        // console.log("SABOGG",$(".getFields_" + object_id).closest("#obj_fields_" + object_id + ".input_position_below"))
        if($($(".getFields_" + object_id)).parent().is('.obj_f-aligned-left-wrap') ){
            var ele_choices_default = $($(".getFields_" + object_id).parent().html());
        }else{
            var ele_choices_default = $($(".getFields_" + object_id).closest("#obj_fields_" + object_id + ".input_position_below").html());
        }
        
        $("#obj_defaultValueChoices").html(ele_choices_default);
        console.log($("#obj_defaultValueChoices").html(ele_choices_default))
        console.log(ele_choices_default)
        ele_choices_default.children('option').attr('disabled', false);
        if (ele_choices_default.hasClass("getFields")) {
            ele_choices_default.removeAttr("disabled");
            ele_choices_default.attr("name", "for_default_use");

        } else {
            ele_choices_default.find(".getFields_" + object_id).removeAttr("disabled"); //.closest(".input_position_below.obj_defaultValueChoices")
            ele_choices_default.find(".getFields_" + object_id).attr("name", "for_default_use"); //.closest(".input_position_below.obj_defaultValueChoices")
        }

        //restoring values saved settings for default value
        if (typeof json[object_id] != "undefined") {

            if (typeof json[object_id].defaultValues != "undefined") {
                if (ele_choices_default.hasClass("getFields")) {
                    if (ele_choices_default.is('select[multiple]')) {
                        //console.log("SHOOOPIEEE",json[object_id]["defaultValues"]);
                        $.each(json[object_id].defaultValues, function (index, item) {
                            ele_choices_default.children("option[value='" + json[object_id].defaultValues[index] + "']").eq(0).prop("selected", true);
                        });
                    }

                    if (typeof json[object_id].defaultValueType != "undefined") {
                        if ($('[data-properties-type="defaultValueType"][value="' + json[object_id].defaultValueType + '"]').length >= 1) {
                            $('[data-properties-type="defaultValueType"][value="' + json[object_id].defaultValueType + '"]').prop("checked", true);
                        }
                    }
                } else {
                    var container_radio_check = ele_choices_default.closest(".obj_defaultValueChoices");
                    var radio_check_buttons = container_radio_check.find(".getFields_" + object_id);
                    $.each(json[object_id].defaultValues, function (index, item) {
                        var radio_check_strvalue = json[object_id].defaultValues[index];
                        if (
                                $(radio_check_buttons).eq(index).length >= 1 &&
                                $(radio_check_buttons).eq(index).val() == radio_check_strvalue
                                ) {
                            $(radio_check_buttons).eq(index).prop("checked", true);
                        }
                    });
                    if (typeof json[object_id].defaultValueType != "undefined") {
                        if ($('[data-properties-type="defaultValueType"][value="' + json[object_id].defaultValueType + '"]').length >= 1) {
                            $('[data-properties-type="defaultValueType"][value="' + json[object_id].defaultValueType + '"]').prop("checked", true);
                        }
                    }
                }
            }

        }




        //MODIFY DISPLAY OF CHOICES checkbox HERE
        ele_choices_default.each(function (eq) {
            $(this).find('[type="checkbox"]').attr('id', 'chk_getfield' + eq);
            $(this).find('[type="checkbox"]').addClass('css-checkbox');
            var labelCheck = $('<label for="chk_getfield' + eq + '" class="css-label" style="margin-bottom:2px;"></label>');
            labelCheck.insertAfter($(this).find('[type="checkbox"]'));
        });

        ele_choices_default.each(function (eq) {
            $(this).find('[type="radio"]').attr('id', 'rdo_getfield' + eq);
            $(this).find('[type="radio"]').addClass('css-checkbox');
            var labelCheck = $('<label for="rdo_getfield' + eq + '" class="css-label" style="margin-bottom:2px;"></label>');
            labelCheck.insertAfter($(this).find('[type="radio"]'));
        });
        //making the functionality for default values
        // console.log("WGAGESGFDSFSA")
        // console.log(ele_choices_default)
        if (ele_choices_default.hasClass("getFields")) {

            ele_choices_default.on({
                "change": function () {
                    var json = $("body").data();
                    var object_id = $("#popup_container").attr("data-object-id");
                    if (ele_choices_default.closest('div[data-type="dropdown"]').length >= 1) {

                        var selected_inHTML = $(".workspace.formbuilder_ws").find(".getFields_" + object_id).children("option:selected");

                        $('div.ps-container').find('div.input_position_below').children(".getFields_" + object_id).val($(this).val());
                        var new_selected_value = $(".workspace.formbuilder_ws").find(".getFields_" + object_id).find("[value='" + $(this).val() + "']");
                        console.log("New Depolt",new_selected_value.attr("value"))
                        if (selected_inHTML.length >= 1) {

                            selected_inHTML.removeAttr("selected");

                        }

                        new_selected_value.attr("selected", true);



                    }
                    // if(ele_choices_default.closest('div[data-type="listNames"]').length>=1){
                    //     alert("sadasdsad");

                    // }


                    else {
                        //apply changes field on the form
                        $(".workspace.formbuilder_ws").find(".getFields_" + object_id).val($(this).val());
                        
                        //saving on json

                        json[object_id] = (typeof json[object_id] == "undefined") ? {} : json[object_id];
                        json[object_id].defaultValues = (typeof json[object_id].defaultValues == "undefined") ? {} : json[object_id].defaultValues;
                        ele_choices_default.children("option").each(function (eqi_option) {
                            if ($(this).is(":selected")) {
                                json[object_id].defaultValues[eqi_option] = $(this).val();
                                json[object_id].defaultValueType = $('[data-properties-type="defaultValueType"]:checked').val();
                            } else {
                                if (typeof json[object_id].defaultValues[eqi_option] != "undefined") {
                                    delete json[object_id].defaultValues[eqi_option]
                                }
                            }
                        });
                        // alert($(ele_choices_default,":checked").val())
                        // alert($(this)[0].selectedIndex)
                        // alert(ele_choices_default.children("option").eq(0).prop("selected",true));
                        // console.log($(ele_choices_default,":checked"));

                        $("body").data(json);
                        // console.log("select");
                        // console.log($("body").data());
                    }
                }
            })
            //medz code
            //var selected_inHTML = $(".workspace.formbuilder_ws").find(".getFields_" + object_id).children("option:selected");
            //
            //if (selected_inHTML.length>=1) {
            //    
            //    
            //    
            //}

            //dito seset mamaya
            //
            // var default_dropdown = $("body").data('1')['defaultValues'];
            // console.log("bakit nawawala?", default_dropdown);
            //
            // var option_inHTML = $(".workspace.formbuilder_ws").find(".getFields_" + object_id);
            // var selected_inHTML = $(".workspace.formbuilder_ws").find(".getFields_" + object_id).children("option:selected");
            // selected_inHTML.removeAttr("selected");
            // //ele_choices_default.val(default_dropdown);
            // for (var ctr in default_dropdown) {
            //    
            //     option_inHTML.children("option[value='"+default_dropdown[ctr]+"']").prop("selected",true);
            //     option_inHTML.children("option[value='"+default_dropdown[ctr]+"']").attr("selected",true);
            // }
            //console.log("hahahahaha",ele_choices_default.children("option:selected"));              
            ele_choices_default.trigger("change");

        } else {
            //console.log(ele_choices_default.closest(".input_position_below.obj_defaultValueChoices")) 
            var container_radio_check = ele_choices_default; //.closest(".input_position_below.obj_defaultValueChoices") remove for new ui prop
            var radio_check_buttons = container_radio_check.find(".getFields_" + object_id);

            radio_check_buttons.on({
                "change": function () {
                    var json = $("body").data();
                    var object_id = $("#popup_container").attr("data-object-id");
                    if ($(this).is(":checked")) {

                        //apply changes to the fields on form
                        var checked_index = radio_check_buttons.index($(this));
                        var checked_value = $(this).val();
                        $(".workspace.formbuilder_ws").find(".getFields_" + object_id).eq(checked_index).prop("checked", true);
                        //saving on json
                        json[object_id] = (typeof json[object_id] == "undefined") ? {} : json[object_id];
                        var parallel_field_on_form = $(".workspace.formbuilder_ws").find(".getFields_" + object_id)

                        parallel_field_on_form.each(function (eqi_parallel) {
                            if ($(this).is(":checked")) {
                                json[object_id].defaultValues = (typeof json[object_id].defaultValues == "undefined") ? {} : json[object_id].defaultValues;
                                json[object_id].defaultValues[checked_index] = checked_value;
                                json[object_id].defaultValueType = $('[data-properties-type="defaultValueType"]:checked').val();
                            } else {
                                //this is for radio because the radio is single value not a multiple values
                                if (radio_check_buttons.attr("type") == "radio") {
                                    if (typeof json[object_id].defaultValues != "undefined") {
                                        if (typeof json[object_id].defaultValues[eqi_parallel] != "undefined") {
                                            delete json[object_id].defaultValues[eqi_parallel]
                                        }
                                    }
                                }
                            }
                        });
                        // json[object_id].defaultValuesIndex = checked_index;
                        // json[object_id].defaultValues = checked_value;
                    } else {
                        //apply changes to the fields on the form
                        var checked_index = radio_check_buttons.index($(this));
                        var checked_value = $(this).val();
                        $(".workspace.formbuilder_ws").find(".getFields_" + object_id).eq(checked_index).prop("checked", false);
                        //saving changes onto json
                        if (typeof json[object_id] != "undefined") {
                            if (typeof json[object_id].defaultValues != "undefined") {
                                if (typeof json[object_id].defaultValues[checked_index] != "undefined") {
                                    delete json[object_id].defaultValues[checked_index]
                                }
                            }
                        }
                    }
                    $("body").data(json);
                    // console.log("radio check")
                    // console.log($("body").data())
                }
            });
            radio_check_buttons.trigger("change");
        }


        if ($('.multiple_field_value').length >= 1) {
            var multiple_val_container = $('.multiple_field_value');
            var g_fields = multiple_val_container.find(".getFields");
            if (g_fields.prop("tagName") == "SELECT") {

                g_fields.css({
                    "width": "100%"
                });
            }
            // if(g_fields.prop("tagName") == "INPUT" && g_fields.attr("type") == "checkbox"){
            //     g_fields.css({
            //         "width":"100%"
            //     });
            // }
        }
            // console.log($('#setObject_'+object_id).find('[selected="selected"]').attr("value"));
    }

}

function dragElement(elements, container, container_height, properties_id) {
    // Drag  / Resize Objects
    var fields_below = $(elements).children(".fields_below");
    var input_position_below = fields_below.children(".input_position_below");

    // $(elements).css({
    // 	// "float":"left",
    // 	"position":"relative",
    // 	"display":"table-cell"

    // });
    var field_name = elements.find("[name]");
    var setobj_drag_handle = $('<div class="setObject-drag-handle" style="position:absolute;width: calc(100% + 10px);margin: -5px;height: calc(100% + 10px);"></div>');
    if (field_name.length >= 1) {
        var tooltip_fieldname = field_name.attr("name");
        setobj_drag_handle.attr('data-original-title', tooltip_fieldname);
        setobj_drag_handle.tooltip();
    }




    if ($(elements).children(".fields_below").children(".setObject-drag-handle").length == 0 && ($(elements).attr("data-type") != "pickList" && $(elements).find(".form-table").length == 0 && $(elements).find(".form-tabbable-pane").length == 0 && $(elements).find(".form-accordion").length == 0)) {
        if ($(elements).hasClass('setObject_btn')) {
            //$(elements).prepend(setobj_drag_handle)
        } else {
            $(elements).children(".fields_below").prepend(setobj_drag_handle)
        }
    } else if ($(elements).children(".fields_below").children(".setObject-drag-handle").length >= 2) {
        $(elements).children(".fields_below").children(".setObject-drag-handle").each(function (eqi) {
            if (eqi == 1) {
                return true;
            }
            $(this).remove()
        })
    } else if ($(elements).is('[data-type="pickList"]')) {
        // var get_fields = $(elements).find('.getFields');
        // get_fields.parent().prepend(setobj_drag_handle);
        var get_fields = $(elements).children('.fields_below').prepend(setobj_drag_handle);

        $(elements).find('.icon-list-alt').parent().css({'position':'relative', 'z-index':3, 'top':0});

        setobj_drag_handle.css({
            // "width": "calc(100% - 20px)",
            // "margin": "0px",
            // "height": "calc(100% + -20px)"
            "height":"100%",
            "width": "100%",
            "margin":"0px;"

        });
        setobj_drag_handle = false;
    } else {
        setobj_drag_handle = false;
    }
    option_focused_component_datas = [];
    $(elements).not('.setObject_btn').draggable({
        containment: "parent",
        // cancel:'.form-tabbable-pane',
        start: function (event, ui) {
            save_history();
            var drag_affecting_elements = $('.component-ancillary-focus').filter(function(){ //FS#8040
                var dis_ele = $(this);
                var parent_ele = dis_ele.parent();
                var parent_container_ele = parent_ele.parents('.setObject:eq(0)');
                if(parent_ele.is('.accordion-div-content,.ui-tabs-panel,.td-relative') && parent_container_ele.is('.component-ancillary-focus')){
                    return true;
                }
                return false;
            });
            if(drag_affecting_elements.length >= 1){ //FS#8040
                var newAlert = new jAlert("Moving the objects and its container at the same time is not allowed. This action will un-select the objects within the container.", "", "", "", "", function () {
                    setTimeout(function(){
                        drag_affecting_elements.removeClass('component-ancillary-focus').removeClass('component-primary-focus');
                        $('.formbuilder_ws').addClass('formbuilder-ws-active');
                    },0);
                });
                newAlert.themeAlert("jAlert2", {
                    'icon': '<i class="fa fa-info-circle fl-margin-right" style="color:blue; font-size:15px;"></i>'
                });
                $(document).trigger("mouseup");
                $(this).draggable('option','stop').call(this,event,ui);
                return false;
                { //I need this reference commented below
                        // showNotification({
                        //     message: "Moving the container and objects at the same time will also move the object's position in proportion to the container.",
                        //     type: "information",
                        //     autoClose: true,
                        //     duration: 3
                        // });

                        // var conf = "Moving the container and objects at the same time will also move the object's position in proportion to the container.";
                        // var newConfirm = jConfirm(conf, 'Confirmation Dialog', '', '', '', function (answer) {
                        //     if (answer == true) {
                        //         // $("#setObject_" + DOI).find(".form-table").eq(0).children("tbody").children("tr").each(function (eqi) {
                        //         //     if (eqi >= 1) {
                        //         //         $(this).remove();
                        //         //     }
                        //         // });
                        //         // $("#setObject_" + DOI).find(".form-table").eq(0).closest(".table-wrapper").find(".table-actions-ra.action-row-add").hide();
                        //         // $("#setObject_" + DOI).find(".form-table").eq(0).attr("repeater-table", true);
                        //     } else {
                        //         // self.val("normal");
                        //     }
                        // });
                        // newConfirm.themeConfirm("confirm2", {
                        //     'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
                        // });

                        // var answer = confirm("Moving the container and objects at the same time will also move the object's position in proportion to the container. Would you like to proceed?");
                        // if( !(answer == true) ){
                            // $(document).trigger("mouseup");
                            // $(this).draggable('option','drag_affecting_elements', drag_affecting_elements);
                            // $(this).draggable('option','stop').call(this,event,ui);
                            // return false;
                        // }
                }
            }
            $(ui.helper).css("box-shadow", "rgba(0,0,0,0.2) 5px 5px 7px");
            $(ui.helper).parent().addClass("ui-draggable-dragging-some-content");
            // alert("START")
            // if ($(ui.helper).css("float") == "left") {
            //     offset_left = ui.offset.left - ($(".formbuilder_ws").offset().left);
            //     offset_top = ui.offset.top - ($(".formbuilder_ws").position().top);
            //     if (parseInt($(ui.helper).css("top").split("px").join("")) >= 1) {
            //         offset_top = offset_top - parseInt($(ui.helper).css("top").split("px").join(""));
            //     }
            //     if (parseInt($(ui.helper).css("left").split("px").join("")) >= 1) {
            //         offset_left = offset_left - parseInt($(ui.helper).css("left").split("px").join(""));
            //     }
            //     //alert(ui.offset.top+" - "+($(".formbuilder_ws").position().top))
            //     //ETO UNG BEST SOLUTION KO SA NAG KAKAROON NG ADDITIONAL OFFSET
            //     //alert(ui.offset.top+"  -"+($(".formbuilder_ws").offset().top)+" -"+$(window).scrollTop()) // pangdebug ko sa additional offset
            //     //nagkakaroon ng additional offset kasi naka position relative ung mother ng parent ng drag ele
            //     // if( (ui.offset.left - ($(".formbuilder_ws").offset().left)) >= 2 ){
            //     // 	offset_left = offset_left - $(window).scrollLeft();
            //     // }
            //     // if( (ui.offset.top - ($(".formbuilder_ws").offset().top)) >=2 ){
            //     // 	offset_top = offset_top - $(window).scrollTop();
            //     // }

            //     $(ui.helper).draggable("option", "firstFloatLeftOffs", {
            //         "left": offset_left,
            //         "top": offset_top
            //     });

            //     $(ui.helper).css({
            //         "float": "none",
            //         "top": (offset_top) + "px",
            //         "left": (offset_left) + "px",
            //         "position": "absolute"

            //     });
            // }

            if ($(ui.helper).css("float") == "left") {
            }
            $(".component-primary-focus").removeClass("component-primary-focus");
            $(this).addClass("component-primary-focus");
            $(this).addClass("component-ancillary-focus");

            if (option_focused_component_datas.length == 0) {
                if ($(this).hasClass("component-ancillary-focus")) {
                    $(".component-ancillary-focus").not('.setObject_btn').each(function (i) {
                        option_focused_component_datas.push({
                            "object": $(this),
                            "originLeft": parseInt($(this).css("left").substring(0, $(this).css("left").length - 2)),
                            "originTop": parseInt($(this).css("top").substring(0, $(this).css("top").length - 2))
                        });
                    })
                } else {
                    option_focused_component_datas.push({
                        "object": $(this),
                        "originLeft": parseInt($(this).css("left").substring(0, $(this).css("left").length - 2)),
                        "originTop": parseInt($(this).css("top").substring(0, $(this).css("top").length - 2))
                    });
                }
            }


        },
        stop: function (event, ui) {
            // alert("STOP")
            $('.ui-draggable-dragging-some-content').removeClass('ui-draggable-dragging-some-content');
            var drag_ele_origin_pos_left = ui.originalPosition.left;
            var drag_ele_origin_pos_top = ui.originalPosition.top;
            var drag_ele_container = $(ui.helper).parent();
            var drag_ancillary_ele_container = $();
            drag_ele_origin_distance_to_current_left = parseInt($(ui.helper).css("left").substring(0, $(ui.helper).css("left").length - 2)) - drag_ele_origin_pos_left;
            drag_ele_origin_distance_to_current_top = parseInt($(ui.helper).css("top").substring(0, $(ui.helper).css("top").length - 2)) - drag_ele_origin_pos_top;
            
            for (var i in option_focused_component_datas) {
                drag_ancillary_ele_container = option_focused_component_datas[i]['object'].parent();
                pos_left = Number(option_focused_component_datas[i]['originLeft']) + Number(drag_ele_origin_distance_to_current_left);
                pos_top = option_focused_component_datas[i]['originTop'] + drag_ele_origin_distance_to_current_top;
                if(drag_ele_container[0] == drag_ancillary_ele_container[0]){
                    option_focused_component_datas[i]['object'].css({
                        left: pos_left,
                        top: pos_top
                    });
                }
                option_focused_component_datas[i]['object'].parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth(option_focused_component_datas[i]['object']));
                option_focused_component_datas[i]['object'].parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight(option_focused_component_datas[i]['object']));
                checkCollisionDroppableBoundaries(option_focused_component_datas[i]['object']);
            }
            option_focused_component_datas = [];




            if ($(ui.helper).draggable("option", "firstFloatLeftOffs")) {
                this_ele_data = $(ui.helper).data();
                delete this_ele_data.uiDraggable.options.firstFloatLeftOffs
                $(ui.helper).data(this_ele_data);
            }
            removeRuler(); // Remove Rule after drag stop

            $(container).resizable("option", "minHeight", checkFormMinHeight());
			// checkParentResizableMinWidth($(this));
			// checkParentResizableMinHeight($(this));
            $(ui.helper).css("box-shadow", "");
            if (!($(this).attr('data-type') == "createLine")) {

                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)));
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)));

                // $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px");
                // $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px");
            }
            else {
                var line_ele = $(this).find('[data-type="createLine"]');
                var line_ele_width = line_ele.width();
                var line_left_width = line_ele_width + $(this).position().left;
                var formbuilder_width = $('.formbuilder_ws').width();
                var formbuilder_height = $('.formbuilder_ws').height();
                var line_ele_height = line_ele.height() + $(this).find('.obj_label').height();
                var line_left_height = line_ele_height + $(this).position().top;
                var overlapped_left = 0;
                var overlapped_top = 0;

                if (line_left_width > formbuilder_width) {
                    overlapped_left = line_left_width - formbuilder_width + 5;
                }
                if (line_left_height > formbuilder_height) {
                    overlapped_top = line_left_height - formbuilder_height + 5;
                }

                $(this).css({'left': $(this).position().left - overlapped_left,
                    'top': $(this).position().top - overlapped_top
                });

            }





            var objectDraggedWidth = checkParentResizableMinWidth($(this));
            var objectDraggedHeight = checkParentResizableMinHeight($(this));
            //console.log($(objectDragged));
            var topRuler = $(this).parents('.fl-workspacev2').find('.topPointerRuler-tip');
            var topRulerPosLeft = $(topRuler).position().left;
            //console.log($(topRulerPosLeft));
            var leftRuler = $(this).parents('.fl-workspacev2').find('.leftPointerRuler-tip');
            var leftRulerPosTop = $(leftRuler).position().top;
            //console.log($(leftRulerPosTop));



            if (topRulerPosLeft > objectDraggedWidth) {
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", topRulerPosLeft + $(topRuler).width());
                //$(this).parents(".ui-resizable").eq(0).css("min-width", topRulerPosLeft + $(topRuler).width() + "px");
            }

            if (leftRulerPosTop > objectDraggedHeight) {
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", leftRulerPosTop + $(leftRuler).width());
                //$(this).parents(".ui-resizable").eq(0).css("min-height", leftRulerPosTop + $(leftRuler).width() + "px");
            }

            $(".formbuilder_ws").trigger("resize");
            /*search createLine*/

            // if($(this).attr('data-type')=="createLine"){
            // 	var lineObject =  $(this).find('.input_position_below').find('div[data-type="createLine"]');
            // 	var lineWidth = lineObject.width();
            // 	var lineLeft =Number( lineObject.closest('.setObject').css('left').replace('px',""));
            // 	var lineTop =Number( lineObject.closest('.setObject').css('top').replace('px',"")) + lineObject.closest('.setObject').find('.label_below.obj_label').height();
            // 	var lineHeight = lineObject.height();
            // 	var fbHeight = $('.formbuilder_ws').height();
            // 	var fbWidth = $('.formbuilder_ws').width();
            // 	var lineSetBack = "";

            // 	if(lineWidth+lineLeft >fbWidth){
            // 		lineLeftSetBack =  fbWidth - lineLeft;

            // 		lineObject.css('width',lineLeftSetBack + "px");
            // 	}
            // 	if(lineHeight + lineTop >fbHeight){
            // 		lineTopSetBack =   fbHeight - lineTop ;

            // 		lineObject.css('height',lineTopSetBack);
            // 	}

            // }

        },
        drag: function (event, ui) {

            $(ui['helper']).customToolTip({
                "event": event.type
            });
            var drag_ele_origin_pos_left = ui.originalPosition.left;
            var drag_ele_origin_pos_top = ui.originalPosition.top;

            drag_ele_origin_distance_to_current_left = parseInt($(ui.helper).css("left").substring(0, $(ui.helper).css("left").length - 2)) - drag_ele_origin_pos_left; //old version of multi dragging
            drag_ele_origin_distance_to_current_top = parseInt($(ui.helper).css("top").substring(0, $(ui.helper).css("top").length - 2)) - drag_ele_origin_pos_top; //old version of multi dragging


            var drag_ele_current_pos_left2 = ui.position.left - drag_ele_origin_pos_left
            var drag_ele_current_pos_top2 = ui.position.top - drag_ele_origin_pos_top
            // console.log(option_focused_component_datas[0]['originLeft'])
            for (var i in option_focused_component_datas) {
                pos_left = option_focused_component_datas[i]['originLeft'] + drag_ele_current_pos_left2;
                pos_top = option_focused_component_datas[i]['originTop'] + drag_ele_current_pos_top2;
                option_focused_component_datas[i]['object'].css({
                    left: pos_left,
                    top: pos_top
                });

                // if(!event.altKey){ // THIS IS FOR MULTIPLE DROPPABLE
                    checkCollisionDroppableBoundaries(option_focused_component_datas[i]['object']);
                // }
            }




            if ($(ui.helper).draggable("option", "firstFloatLeftOffs")) {
                ui.position.left = ui.position.left + $(ui.helper).draggable("option", "firstFloatLeftOffs").left;
                ui.position.top = ui.position.top + $(ui.helper).draggable("option", "firstFloatLeftOffs").top;
            }
            //$(".object_properties").popover('hide')

            $(this).css({
                "float": "none",
                "top": (ui.position.top) + "px",
                "left": (ui.position.left) + "px",
                "position": "absolute"

            });
            // Show Ruler guide                   
            var drag_ele_current_pos_left = ui.offset.left
            var drag_ele_current_pos_top = ui.offset.top

            dragShowVerticalRuler($(ui.helper), drag_ele_current_pos_left, drag_ele_current_pos_top, container);
            dragShowHorizontalRuler($(ui.helper), drag_ele_current_pos_left, drag_ele_current_pos_top, container);
            $('.tooltip').remove();
        },
        snapMode: "outer",
        snap: ".setObject, .form-builder-ruler",
        snapTolerance: "5",
        handle: setobj_drag_handle
    });
    // console.log("TESTLOGY",$(elements))
    // if($(elements).is('[data-type="pickList"]')){ //para madrag ung mismong field na text field kaso lang di gumagana to kapag may attribute na disabled="disabled"
    // console.log("PASOK ETO NAA",$(elements).find('.getFields:input'))
    // $(elements).find('.getFields:input').on("mousedown",function(e){
    // var mdown = document.createEvent("MouseEvents");
    // mdown.initMouseEvent("mousedown", false, true, window, 0, e.screenX, e.screenY, e.clientX, e.clientY, true, false, false, true, 0, null);
    // $(this).closest('.setObject').eq(0)[0].dispatchEvent(mdown);
    // });
    // }
}


function dragObects(elements, container, container_height, properties_id) {
    var test = "";

    dragElement(elements, container, container_height, properties_id);

    $(elements).on({
        "mousedown.fieldObjectFocus": function (event) {
            event.stopPropagation();

            if (!$(this).hasClass('setObject_btn')) {
                if ($(this).is(':data("uiDraggable")')) {
                    if (event.altKey || (event.shiftKey && event.altKey)) {
                        $(this).draggable("option", "containment", $(".workspace.formbuilder_ws"));
                    } else {
                        $(this).draggable("option", "containment", "parent");
                    }
                }
            }
       
            var eto_event = event;
            var eto_dis = $(this);
            var eto_dis_id =$(this).attr('data-object-id');
            var eto_dis_data_type = $(eto_dis).attr('data-type')
            
            if (eto_dis_data_type == "tab-panel" || eto_dis_data_type == "accordion" || eto_dis_data_type == "attachment_on_request" || eto_dis_data_type == "multiple_attachment_on_request") {
                disableEnableFieldGeneralProperties("disabled", eto_dis_data_type);
            }else {
                disableEnableFieldGeneralProperties("enabled");
            }
        
            if (eto_event.ctrlKey) {
                if (eto_dis.hasClass("component-ancillary-focus")) {
                    if (eto_dis.hasClass("component-primary-focus")) {
                        eto_dis.removeClass("component-primary-focus");
                        eto_dis.removeClass("component-ancillary-focus");
                        $(".component-ancillary-focus").last().addClass("component-primary-focus");
                        $('#list_'+eto_dis_id).removeClass("layerhighlight")
                        console.log("remove!")

                    } else {
                        // $(".component-primary-focus").removeClass("component-primary-focus");
                        // eto_dis.addClass("component-primary-focus");
                        eto_dis.removeClass("component-ancillary-focus");
                        $('#list_'+eto_dis_id).removeClass("layerhighlight")
                    }
                } else {
                    eto_dis.addClass("component-ancillary-focus");

                    $(".component-primary-focus").removeClass("component-primary-focus");
                    eto_dis.addClass("component-primary-focus");
                    $('#list_'+eto_dis_id).addClass("layerhighlight")
                }

            } else if (!eto_dis.hasClass("component-ancillary-focus") && !eto_dis.hasClass("component-primary-focus")) {
                $(".component-ancillary-focus").removeClass("component-ancillary-focus");
                eto_dis.addClass("component-ancillary-focus");

                $(".component-primary-focus").removeClass("component-primary-focus");
                eto_dis.addClass("component-primary-focus");
                //

                $('.layerhighlight').removeClass("layerhighlight")
                $('#list_'+eto_dis_id).addClass("layerhighlight")
                //
            } else {
                if (eto_dis.hasClass("component-ancillary-focus")) {
                    if (eto_dis.hasClass("component-primary-focus")) {
                        // eto_dis.removeClass("component-primary-focus");
                        // eto_dis.removeClass("component-ancillary-focus");
                        // $(".component-ancillary-focus").last().addClass("component-primary-focus");
                    } else {
                        $(".component-primary-focus").removeClass("component-primary-focus");
                        eto_dis.addClass("component-primary-focus");
                    }
                } else {
                    eto_dis.addClass("component-ancillary-focus");

                    $(".component-primary-focus").removeClass("component-primary-focus");
                    eto_dis.addClass("component-primary-focus");
                }
            }

            dropdown_reset();


        },
        "dblclick.doubleClickProp": function (e) {
            e.stopPropagation();
            $(this).find(".object_setup.object_properties").eq(0).click();
        }
    })
    //parseInt($(elements).find(".getFields")) + parseInt($(elements).find(".getFields").css("padding-left").split("px").join(""))
    //RESIZABLE KINDS OF FIELDS
    var fieldCounterID = $(elements).attr("data-object-id");
    var gettingFields = $(elements).find(".getFields_" + fieldCounterID).eq(0);
    if (gettingFields.is('.forma-colorpicker')) {
        //no resize
    } else if (gettingFields.hasClass("noteStyleField")) {

        $(elements).find(".content-editable-field").eq(0).resizable({
            "handles": "s,e",
            start: function () {
                save_history();
                $(this).resizable("option", "minHeight", "0px");
                $(this).resizable("option", "minWidth", "0px");
                $(this).css({
                    "min-width": 0 + "px",
                    "min-height": 0 + "px",
                });
            },
            resize: function () {
                var dis_seobj = $(this).parents(".setObject").eq(0);
                var dis_doid = dis_seobj.attr("data-object-id");
                dis_seobj.find("#lbl_" + dis_doid).css({
                    "width": ($(this).outerWidth()) + "px"
                });
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
            },
            stop: function () {
                var dis_seobj = $(this).parents(".setObject").eq(0);
                var dis_doid = dis_seobj.attr("data-object-id");
                $(this).resizable("option", "minHeight", $(this).height());
                $(this).resizable("option", "minWidth", $(this).width());
                $(this).css({
                    "min-width": $(this).width() + "px",
                    "min-height": $(this).height() + "px",
                });
                if (dis_seobj.parent().hasClass("td-relative")) {
                    var td_ele = dis_seobj.parents("td").eq(0)
                    var td_oheight = td_ele.outerHeight();
                    var td_owidth = td_ele.outerWidth();

                    if (td_owidth < ($(dis_seobj).outerWidth() + $(dis_seobj).position().left)) {
                        td_ele.css("width", ($(dis_seobj).outerWidth() + $(dis_seobj).position().left) + "px");
                    }
                    if (td_oheight < ($(dis_seobj).outerHeight() + $(dis_seobj).position().top)) {
                        td_ele.css("height", ($(dis_seobj).outerHeight() + $(dis_seobj).position().top) + "px");
                    }
                    if (td_ele.hasClass("ui-resizable")) {
                        td_ele.resizable("option", "minWidth", checkParentResizableMinWidth($(this)));
                        td_ele.resizable("option", "minHeight", checkParentResizableMinHeight($(this)));
                        td_ele.css("min-height", (checkParentResizableMinHeight($(this))) + "px");
                        td_ele.css("min-width", (checkParentResizableMinWidth($(this))) + "px");
                    }
                }
                // console.log(dis_seobj,"TEST PARENTS",)
                dis_seobj.parent().parents('.td-relative').each(function (eqi) {
                    //
                    var self = $(this);
                    var td_elem = self.parent();
                    var td_oheight = td_elem.outerHeight();
                    var td_owidth = td_elem.outerWidth();
                    var dis_seobjs = self.children('.setObject');
                    var most_right = dis_seobjs.get().map(function (ele, m_index) {
                        return $(ele).outerWidth() + $(ele).position().left;
                    }).sort(function (a, b) {
                        return b - a;
                    });

                    var most_bottom = dis_seobjs.get().map(function (ele, m_index) {
                        return $(ele).outerHeight() + $(ele).position().top;
                    }).sort(function (a, b) {
                        return b - a;
                    });

                    // console.log(td_owidth,"<",most_right[0])
                    if (td_owidth < most_right[0]) {
                        // console.log(td_elem)
                        td_elem.css("width", most_right[0] + "px");
                        td_elem.children('.td-relative').eq(0).css("min-width", most_right[0] + "px");
                    }

                    if (td_oheight < most_bottom[0]) {
                        td_elem.css("height", most_bottom[0] + "px");
                        td_elem.children('.td-relative').eq(0).css("min-height", most_bottom[0] + "px");
                    }

                })
            }
        }).find(".ui-resizable-s").css({
            "bottom": "0px"
        }).parent().find(".ui-resizable-e").css({
            "right": "0px"
        });


        // $(elements).resizable({
        //     "handles":"s,e",
        //     start:function(){
        //         // $(this).resizable("option","minHeight","0px");
        //         // $(this).resizable("option","minWidth","0px");
        //     },
        //     stop:function(){
        //         // $(this).resizable("option","minHeight",$(this).outerHeight());
        //         // $(this).resizable("option","minWidth",$(this).outerWidth());
        //     }
        // });

    } else if (gettingFields.closest('.setObject[data-type="smart_barcode_field"]').length >= 1) {
        gettingFields.closest('.setObject[data-type="smart_barcode_field"]').resizable({
            // containment: "parent",
            handles: 's,e',
            start: function (event, ui) {
                save_history();
                // console.log($(this))
            },
            resize: function (event, ui) {
                var self = $(this);
                var dis_data_object_id = self.attr('data-object-id');

                //$(".object_properties").popover('hide');
                ui.size.width = ui.size.width - 10;
                ui.size.height = ui.size.height - 10;
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
            },
            stop: function (ee, ui) {
                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")

            }
        });
    } else if (gettingFields.prop("tagName") == "TEXTAREA") {
        $(elements).resizable({
            containment: "parent",
            handles: 's,e,se',
            autoHide: true,
            start: function (event, ui) {
                save_history();
            },
            resize: function (event, ui) {

                //$(".object_properties").popover('hide');
                // ui.size.width = ui.size.width - 10;
                // ui.size.height = ui.size.height - 10;

                var dis_id = $(this).attr("data-object-id");
                var obj_fld_container = $(this).find("#obj_fields_" + dis_id);
                var obj_fld_container_pos_top = obj_fld_container.position().top;
                obj_fld_container.css({
                    "height": "calc(100% - " + (obj_fld_container_pos_top) + "px)"
                });
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                if (resizableAxis === "se") {
                    resizableAxis = "s";
                }
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
            },
            stop: function (ee, ui) {
                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")

            }
        });
    } else if (gettingFields.prop("tagName") == "IMG") {
        $(elements).find(".uiform-image-resizable").resizable({
            // containment: "parent",
            handles: 'n,s,e,w,se',
            start: function (event, ui) {
                save_history();
                // console.log($(this))
            },
            resize: function (event, ui) {
                //$(".object_properties").popover('hide');
                ui.size.width = ui.size.width - 10;
                ui.size.height = ui.size.height - 10;
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
            },
            stop: function (ee, ui) {
                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                
                // $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                // $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                // $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                // $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")
            }
        });
    } else if (gettingFields.attr("type") == "radio" || gettingFields.attr("type") == "checkbox") {
        //walang resizable itong si checkbox at si radio
    } else if (gettingFields.parents(".uiform-image-resizable").find(".ImagegetFields_" + fieldCounterID).length >= 1) {


        $(elements).find(".uiform-image-resizable").resizable({
            // containment: "parent",
            handles: 'n,s,e,w,se',
            start: function (event, ui) {
                save_history();
                // console.log($(this))
            },
            resize: function (event, ui) {
                //$(".object_properties").popover('hide');
                ui.size.width = ui.size.width - 10;
                ui.size.height = ui.size.height - 10;
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
            },
            stop: function (ee, ui) {
                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")
            }
        });

    } else if (elements.find('#obj_fields_' + fieldCounterID).children(".table-wrapper,.form-table").length >= 1) {//palatandaan
        $(elements).resizable({
            containment: "parent",
            handles: 's',
            start: function (event, ui) {
                save_history();
            },
            resize: function (event, ui) {
                //$(".object_properties").popover('hide');
                // ui.size.width = ui.size.width - 10;
                // ui.size.height = ui.size.height - 10;
                // console.log(ui.size.width)
                var dis_id = $(this).attr("data-object-id");
                var obj_fld_container = $(this).find("#obj_fields_" + dis_id);
                var obj_fld_container_pos_top = obj_fld_container.position().top;
                obj_fld_container.css({
                    "height": "calc(100% - " + (obj_fld_container_pos_top) + "px)"
                });
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
            },
            stop: function (ee, ui) {
                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")
            }
        }).children(".ui-resizable-s").hide();
        if ($(elements).find('[repeater-table="true"]').length >= 1) {
            $(elements).children(".ui-resizable-s").show();
        }
    } else if (elements.find('#obj_fields_' + fieldCounterID).children(".tab-panel-parent-container,.form-tabbable-pane").length >= 1) {
        $(elements).resizable({
            containment: "parent",
            handles: 'e',
            start: function (e, ui) {
                save_history();
                $(this).find(".form-tabbable-pane").eq(0).children(".ui-tabs-panel,.ui-widget-content").each(function () {
                    if ($(this).css('width') != "100%") {
                        $(this).css('width', (100) + "%");
                    }
                })

                var ui_tabs_nav_ele = $(this).find('.ui-tabs-nav').eq(0);
                if (hasScrollLeft(ui_tabs_nav_ele) == true) {
                    ui_tabs_nav_ele.attr("has-scroll", "true");
                    ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).attr("has-scroll", "true");
                } else {
                    ui_tabs_nav_ele.removeAttr("has-scroll", "true");
                    ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).removeAttr("has-scroll", "true");
                }
            },
            resize: function (event, ui) {
                //$(".object_properties").popover('hide');
                ui.size.width = ui.size.width - 10;
                ui.size.height = ui.size.height - 10;

                var ui_tabs_nav_ele = $(this).find('.ui-tabs-nav').eq(0);
                if (hasScrollLeft(ui_tabs_nav_ele) == true) {
                    ui_tabs_nav_ele.attr("has-scroll", "true");
                    ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).attr("has-scroll", "true");
                } else {
                    ui_tabs_nav_ele.removeAttr("has-scroll", "true");
                    ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).removeAttr("has-scroll", "true");
                }
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
            },
            stop: function (ee, ui) {
                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")

                var ui_tabs_nav_ele = $(this).find('.ui-tabs-nav').eq(0);
                if (hasScrollLeft(ui_tabs_nav_ele) == true) {
                    ui_tabs_nav_ele.attr("has-scroll", "true");
                    ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).attr("has-scroll", "true");
                } else {
                    ui_tabs_nav_ele.removeAttr("has-scroll", "true");
                    ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).removeAttr("has-scroll", "true");
                }
            }
        });
    } else if (gettingFields.attr("type") == "text") {
        $(elements).resizable({
            containment: "parent",
            handles: 'e',
            start: function (event, ui) {
                save_history();
                //not new line
                // var dis_ele = $(this);
                // var doi = dis_ele.attr("data-object-id");
                // var object_field_cont = dis_ele.find('[id="obj_fields_'+doi+'"]');
                // if(object_field_cont.length >= 1){
                //     var object_field_cont_top = object_field_cont.position().top;

                // }
            },
            resize: function (event, ui) {
                //$(".object_properties").popover('hide');
                ui.size.width = ui.size.width - 10;
                ui.size.height = ui.size.height - 10;
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });

            },
            stop: function (ee, ui) {
                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")
            }
        });
    } else if (elements.attr("data-type") == "createLine") {
        var eto_yung_linya = elements.children(".fields_below").children("div.input_position_below").find("div[data-type='createLine']");
        $(eto_yung_linya).resizable({
            "minHeight": 1,
            "minWidth": 1,
            containment: $(".formbuilder_ws"),
            handles: 's,e',
            start: function (event, ui) {
                save_history();
                var maxWidth = $('.formbuilder_ws').outerWidth();
                var width = Number(ui['helper'].width()) + Number($(this).closest('.setObject').position().left);
                var height = Number(ui['helper'].height()) + Number($(this).closest('.setObject').position().top) + Number($(this).closest('.setObject').find('.label_below').height());
                var maxHeight = $('.formbuilder_ws').outerHeight();

                $(this).resizable("option", "maxWidth", maxWidth - width + Number(ui['helper'].width()));
                $(this).resizable("option", "maxHeight", maxHeight - height + Number(ui['helper'].height()));



            },
            resize: function (event, ui) {
                // console.log("target",$(this).data('ui-resizable').axis)
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
                //$(".object_properties").popover('hide');
                //ui.size.width = ui.size.width - 10;
                //ui.size.height = ui.size.height - 10;
                //resizeableTooltip($(this));


            },
            stop: function (ee, ui) {

                var properties_data = $('body').data();
                var ele_dis = $(ui['helper']);
                var ele_dis_parent_container = ele_dis.parents('.setObject[data-type="createLine"]');
                var ele_dis_doi = ele_dis_parent_container.attr('data-object-id');
                $(container).resizable("option", "minWidth", checkFormMinWidth());
                $(container).resizable("option", "minHeight", checkFormMinHeight());
                ele_dis.parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight(ele_dis));
                ele_dis.parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth(ele_dis));
                ele_dis.parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight(ele_dis)) + "px");
                ele_dis.parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth(ele_dis)) + "px");
                // console.log("STOP LOOK AND LISTEN",ee,ui);
                if (properties_data[ele_dis_doi]) {
                    properties_data[ele_dis_doi]['lblFieldHeight'] = (ui['size']['height'] <= 0) ? ele_dis.height() : ui['size']['height'];
                    properties_data[ele_dis_doi]['lblFieldWidth'] = (ui['size']['width'] <= 0) ? ele_dis.width() : ui['size']['width'];
                } else {
                    properties_data[ele_dis_doi] = {};
                    properties_data[ele_dis_doi]['lblFieldHeight'] = (ui['size']['height'] <= 0) ? ele_dis.height() : ui['size']['height'];
                    properties_data[ele_dis_doi]['lblFieldWidth'] = (ui['size']['width'] <= 0) ? ele_dis.width() : ui['size']['width'];
                }
            }
        });
    }
    else if (elements.attr("data-type") == "accordion") {
        
        accordion_resizable(elements);
    }
    else if(elements.is('[data-type="embeded-view"]')){
        $(elements).resizable({
            containment: "parent",
            handles: 'n,s,e,w',
            start: function (event, ui) {
                save_history();
                console.log(this,ui)
                var self = $(this);
                var doi = self.attr("data-object-id");
                var label_height = (self.find('.obj_label[id="label_'+doi+'"]').eq(0).outerHeight()||0) + self.find('.getFields_'+doi+':eq(0)').parent().children('.embed_newRequest').outerHeight() + 5;
                self.find('.getFields_'+doi+':eq(0)').parent().css('height','calc(100% - '+(label_height)+'px)');

            },
            resize: function (event, ui) {
                //$(".object_properties").popover('hide');
                ui.size.width = ui.size.width - 10;
                ui.size.height = ui.size.height - 10;
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
            },
            stop: function (ee, ui) {

                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")
            }
        });
    }
    else {
        $(elements).not(".setObject_btn").resizable({
            containment: "parent",
            handles: 'n,s,e,w',
            start: function (event, ui) {
                save_history();
            },
            resize: function (event, ui) {
                //$(".object_properties").popover('hide');
                ui.size.width = ui.size.width - 10;
                ui.size.height = ui.size.height - 10;
                var resizableAxis = $(this).data('ui-resizable').axis;
                //resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
                $(this).children('.ui-resizable-' + resizableAxis).customToolTip({
                    "event": event.type
                });
            },
            stop: function (ee, ui) {

                // $(container).resizable("option", "minWidth", checkFormMinWidth());
                // $(container).resizable("option", "minHeight", checkFormMinHeight());
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")
            }
        });
    }
    if ($(elements).data()) {
        if ($(elements).data().uiResizable) {
            $(elements).resizable("option", "minWidth", 20);
        }
    }
    // Hide / Show Properties of objects
    //prop_hovering();

    // Click Object Properties
    //$(".object_properties").popover({
    //    html : true, 
    //    content: function() {
    //	var properties_type = $(this).attr("data-properties-type");
    //	return  object_properties(properties_type);
    //    },
    //    container: "body"
    //});


    $('.setObject #object_properties_' + properties_id).on({
        'click': function (e) { //ETO UNG CLICK NG PROPERTIES

            var body_data = $("body").data();
            // console.log("TEST", body_data);
            //OLD SHIT
            if ($(this).closest(".setObject").is('[data-upload="photo_upload"]')) {
                old_object_dom = $(this).closest(".setObject").children().clone(true);
            } else if ($(this).closest(".setObject").find(".fields_below").length >= 1) {
                old_object_dom = $(this).closest(".setObject").children(".fields_below").clone(true);
            } else if ($(this).closest(".setObject").length >= 1) {
                old_object_dom = $(this).closest(".setObject").clone(true);
            }

            // old_object_data = if_undefinded(body_data[''+ properties_id +''],"");
            // console.log("old_object_data",old_object_data);

            // Remove other open popover
            //$('.object_properties').not(this).popover('hide');
            var properties_type = $(this).attr("data-properties-type");
            var object_id = $(this).attr("data-object-id");

            var data_type = $("#setObject_" + object_id).attr("data-type");
            var data_object_type = $(this).attr("data-object-type");
            //added by roni for custom width modal property 04-21-2015
            var objWidth = "";
            if ($('.setObject #object_properties_' + properties_id).attr('data-object-type') == "button" || $('.setObject #object_properties_' + properties_id).attr('data-object-type') == "requestImage") {
                objWidth = 500;
            } else {
                objWidth = 800;
            }

            //data-object-type
            // Show properties on the modal box

            var newDialog = new jDialog(object_properties.call($("#setObject_" + object_id), properties_type, data_type, data_object_type), "", objWidth, "", "95", function () {

            });



            var updatedFormula = UpdateMentionData.init();
            newDialog.themeDialog("modal2");
            save_history('save');

            $('.fl-closeDialog').addClass("fields-reset").attr({
                "object_id": object_id,
                "old_json": JSON.stringify(if_undefinded(body_data['' + properties_id + '']), "")
            }).off("click");



            $("#popup_container").attr("data-object-id", object_id);
            if ($('#setObject_' + properties_id).is('[data-type="tab-panel"]') || $('#setObject_' + properties_id).is('[data-type="table"]') || $('#setObject_' + properties_id).is('[data-type="accordion"]')) {
                var label_value = $('#setObject_' + properties_id).find('label#lbl_' + properties_id).text();
                $('#setObject_' + properties_id).attr('data-temp-label-holder', label_value);
            }
            if ($('[data-properties-type="picklistActionVisibility"]').length >= 1) {
                var evt_val = $('body').data(object_id);

                if ($.type(evt_val) != "undefined") {
                    if ($.type(evt_val['picklistActionVisibility']) != "undefined") {
                        $('[data-properties-type="picklistActionVisibility"]').val(evt_val['picklistActionVisibility']).trigger("change");
                    }
                    if ($.type(evt_val['picklistActionVisibility']) != "undefined" && evt_val['typeAheadOption'] != "undefined") {
                        console.log('typeahead', evt_val['typeAheadOption'])
                        $('[data-properties-type="typeAheadOption"][value="' + evt_val['typeAheadOption'] + '"]').trigger('click');

                    }
                    $('[data-properties-type="picklistActionVisibility"]').trigger("change");
                }


            }
            if ($('.linkmakerClass').length >= 1) {

                detailPanelPlusMinus();
                restoreData($('body').data(object_id));
            }

            if($('[data-properties-type="object-css-styles"]').length >= 1) {
                $(this).val("testing");
                
            } 

            if ($('[data-properties-type="embed-value-type"]').length >= 1) {

                var evt_val = $('body').data(object_id);
                var fld_evt_val = "static";
                if ($.type(evt_val) != "undefined") {
                    if ($.type(evt_val['embed-value-type']) != "undefined") {
                        fld_evt_val = evt_val['embed-value-type'];
                    }
                }
                $('[data-properties-type="embed-value-type"][value="' + fld_evt_val + '"]').trigger("click");
            }
            if ($('[data-properties-type="defaultHyperLinkValue"]').length >= 1) {
                var hl_val = $('body').data(object_id);
                var fld_hl_val = "static";
                if ($.type(hl_val) != "undefined") {
                    if ($.type(hl_val['defaultHyperLinkValue']) != "undefined") {
                        fld_hl_val = hl_val['defaultHyperLinkValue'];
                    }

                }
                $('[data-properties-type="defaultHyperLinkValue"][value="' + fld_hl_val + '"]').trigger("click");
                console.log("HL val", fld_hl_val)
            }
            if ($('[data-properties-type="lblFontSize"]').length >= 1) {
                var lblf_size = $('.setObject').find("label#lbl_" + object_id).css('font-size');
                $('[data-properties-type="lblFontSize"]').val(lblf_size);

            }
            if ($('[data-properties-type="lblIdentifier"]').length >= 1) {
                var lbl_id = $('.setObject').find("label.getFields_" + object_id).attr("name");
                $('[data-properties-type="lblIdentifier"]').val(lbl_id);
            }
            if ($('[data-properties-type="fld_fc"]').length >= 1) {
                var lblfc = $('body').data(object_id);
                var fldfc = "";
                if ($.type(lblfc) != "undefined") {
                    if ($.type(lblfc['fld_fc']) != "undefined") {
                        fldfc = lblfc['fld_fc'];
                    }

                }
                $('[data-properties-type="fld_fc"]').val(fldfc);
            }

            if ($("[data-properties-type='fld_td']").length >= 1) {
                var lbltd = $('body').data(object_id);
                var fldtd = "";
                if ($.type(lbltd) != "undefined") {
                    if ($.type(lbltd['fld_td']) != "undefined") {
                        fldtd = lbltd['fld_td'];
                    }

                }

                //console.log("fldtd", fldtd);
                if (fldtd == "underline") {
                    $("[data-properties-type='fld_td']").closest('.spanbutton').addClass('bn-cd');
                    $("[data-properties-type='fld_td']").prop('checked', true);
                }
                else {
                    $("[data-properties-type='fld_td']").closest('.spanbutton').removeClass('bn-cd');
                    $("[data-properties-type='fld_td']").prop('checked', false);
                }

                if ($('#getFields_'+object_id).css('text-decoration') != "underline") {
                   
                    $("[data-properties-type='fld_td']").closest('.spanbutton').removeClass('bn-cd');
                    $("[data-properties-type='fld_td']").prop('checked', false);
                };

            }
            if ($("[data-properties-type='fld_fs']").length >= 1) {
                var lbltd = $('body').data(object_id);
                var fldfs = "";
                if ($.type(lbltd) != "undefined") {
                    if ($.type(lbltd['fld_fs']) != "undefined") {
                        fldfs = lbltd['fld_fs'];
                    }

                }

                //var fldfs = $('#getFields_'+object_id).css('font-style');

                if (fldfs == "italic") {
                    $("[data-properties-type='fld_fs']").closest('.spanbutton').addClass('bn-cd');
                    $("[data-properties-type='fld_fs']").prop('checked', true);
                }
                else {
                    $("[data-properties-type='fld_fs']").closest('.spanbutton').removeClass('bn-cd');
                    $("[data-properties-type='fld_fs']").prop('checked', false);
                }

                if ($('#getFields_'+object_id).css('font-style') != "italic") {

                    $("[data-properties-type='fld_fs']").closest('.spanbutton').removeClass('bn-cd');
                    $("[data-properties-type='fld_fs']").prop('checked', false);
                };

            }
            if ($("[data-properties-type='fld_fw']").length >= 1) {

                var lbltd = $('body').data(object_id);
                var fldfw = "";
                if ($.type(lbltd) != "undefined") {
                    if ($.type(lbltd['fld_fw']) != "undefined") {
                        fldfw = lbltd['fld_fw'];
                    }

                }

                if ($('#getFields_'+object_id).css('font-weight') == "bold") {
                    $("[data-properties-type='fld_fw']").closest('.spanbutton').addClass('bn-cd');
                    $("[data-properties-type='fld_fw']").prop('checked', true);
                } else {
                    $("[data-properties-type='fld_fw']").closest('.spanbutton').removeClass('bn-cd');
                    $("[data-properties-type='fld_fw']").prop('checked', false);
                }
                
                if (fldfw == "bold") {//   normal
                    $("[data-properties-type='fld_fw']").closest('.spanbutton').addClass('bn-cd');
                    $("[data-properties-type='fld_fw']").prop('checked', true);
                }
                else {
                    fldfw = "normal";
                    $("[data-properties-type='fld_fw']").closest('.spanbutton').removeClass('bn-cd');
                    $("[data-properties-type='fld_fw']").prop('checked', false);
                };

            }
            if ($("[data-properties-type='lbl_td']").length >= 1) {
                var lbltd = $("#lbl_" + object_id).css('text-decoration');
                if (lbltd == "underline") {
                    $("[data-properties-type='lbl_td']").closest('.spanbutton').addClass('bn-cd');
                    $("[data-properties-type='lbl_td']").prop('checked', true);
                }
                else {
                    $("[data-properties-type='lbl_td']").closest('.spanbutton').removeClass('bn-cd');
                    $("[data-properties-type='lbl_td']").prop('checked', false);
                }
            }
            if ($("[data-properties-type='lbl_fs']").length >= 1) {
                var lblfs = $("#lbl_" + object_id).css('font-style');
                if (lblfs == "italic") {
                    $("[data-properties-type='lbl_fs']").closest('.spanbutton').addClass('bn-cd');
                    $("[data-properties-type='lbl_fs']").prop('checked', true);
                }
                else {
                    $("[data-properties-type='lbl_fs']").closest('.spanbutton').removeClass('bn-cd');
                    $("[data-properties-type='lbl_fs']").prop('checked', false);
                }
            }
            if ($("[data-properties-type='lblFontWg']").length >= 1) {
                var lblfw = $('#lbl_' + object_id).css('font-weight');

                //check if mozilla
                if(typeof InstallTrigger !== 'undefined'){
                    lblfw = $('#lbl_' + object_id)[0].style['font-weight'];
                }
                
                if (lblfw == "bold") { //  normal

                    $("[data-properties-type='lblFontWg']").closest('.spanbutton').addClass('bn-cd');
                    $("[data-properties-type='lblFontWg']").prop('checked', true);

                }
                else {
                    $("[data-properties-type='lblFontWg']").closest('.spanbutton').removeClass('bn-cd');
                    $("[data-properties-type='lblFontWg']").prop('checked', false);
                }
            }
            if ($("[data-properties-type='lblFontType']").length >= 1) {
                var fonttype = $("[data-properties-type='lblFontType']");
                fonttype.on("change", function () {

                    var pamilya_font = $(this).val();
                    $(this).parents('.fields_below').eq(0).find('.font-preview').css({"font-family": pamilya_font});
                });
            }
            if ($("[data-properties-type='fieldFontType']").length >= 1) {
                var fonttype = $("[data-properties-type='fieldFontType']");
                fonttype.on("change", function () {

                    var pamilya_font = $(this).val();
                    $(this).parents('.fields_below').eq(0).find('.font-preview').css({"font-family": pamilya_font});
                });
            }

            if ($("[data-properties-type='lblChoices']").length >= 1) {
                $(".sortlist-value").on('click', function () {
                    var arrange = $("[data-properties-type='lblChoices']");
                    var arrange_array = arrange.val().split("\n");
                    arrange.val(arrange_array.sort().join("\n"));
                    $("[data-properties-type='lblChoices']").trigger('change');
                });



            }
            if ($("#popup_container").find('.ListNameChoiceType').length >= 1) {

                var listNames_type = $('.properties_width_container.ListNameChoiceType').find('div[data-type="listNames"]').eq(0).find('[name="listname_selection_type"]');
                var checked_LN = $('.getFields_' + object_id + '[name]').parent().find('.viewNames').attr("list-type-selection")|| "Single";
                
                $("#popup_container").find('.ListNameChoiceType').find('[name="listname_selection_type"]').on("change", function (evs, data) {
                    var doid = $("#popup_container").attr("data-object-id");
                    var dis_val = $(this).val();
                    $('.getFields_' + object_id + '[name]').parent().find('.viewNames').attr("list-type-selection", dis_val);
                    var new_checked = listNames_type.filter('[value="' + data + '"]');
                    if(new_checked.exists()){
                        new_checked.attr('checked', true);
                        new_checked.prop('checked', true);
                    }
                });
                console.log('checkedLN',checked_LN);
                $("#popup_container").find('.ListNameChoiceType').find('[name="listname_selection_type"][value="'+checked_LN+'"]').trigger("change", [checked_LN]);
            }



            if ($('[data-properties-type="lblFldName"]').length >= 1) {//PANGKUHA NG NAME KAPAG WALA PANG SINESET
                var lbl_name_fields = $('.getFields_' + object_id + '[name]').eq(0).attr('name');
                //console.log(lbl_name_fields);
                if (!lbl_name_fields) {
                    if ($('.getFields_' + object_id + '').hasClass("embed-view-container")) {
                        lbl_name_fields = $('.getFields_' + object_id + '[embed-name]').eq(0).attr('embed-name');
                        if (lbl_name_fields) {
                            $('[data-properties-type="lblFldName"]').val(lbl_name_fields.replace('[]', ""));
                        }
                    }
                } else {
                    $('[data-properties-type="lblFldName"]').val(lbl_name_fields.replace('[]', ""));
                }
            }
            if ($("#popup_container").find(".clear-default-value").length >= 1) {
                $("#popup_container").find(".clear-default-value").on({
                    "click": function () {

                        var doi_fld = $("#popup_container").attr("data-object-id");
                        if ($(this).parents(".fields_below").eq(0).find('[name="for_default_use"]').is("select[multiple]")) {
                            $(this).parents(".fields_below").eq(0).find('[name="for_default_use"]').children("option").prop("selected", false);
                            $("#setObject_" + doi_fld).find('.getFields_' + doi_fld + '').children("option").prop("selected", false);
                        } else if ($(this).parents(".fields_below").eq(0).find('[name="for_default_use"]').is("select:not([multiple])")) {
                            $(this).parents(".fields_below").eq(0).find('[name="for_default_use"]').children("option").eq(0).prop("selected", true);
                            $("#setObject_" + doi_fld).find('.getFields_' + doi_fld + '').children("option").eq(0).prop("selected", true);
                        } else {
                            $(this).parents(".fields_below").eq(0).find('[name="for_default_use"]').prop("checked", false);
                            $(this).parents(".fields_below").eq(0).find('[name="for_default_use"]').attr("checked", false);
                            $("#setObject_" + doi_fld).find('.getFields_' + doi_fld + '').prop("checked", false);
                            $("#setObject_" + doi_fld).find('.getFields_' + doi_fld + '').attr("checked", false);
                        }

                        var temp_for_ele_data = $("body").data("" + doi_fld + "");
                        temp_for_ele_data["defaultValues"] = [];
                        $("body").data("" + doi_fld + "", temp_for_ele_data);
                    }
                })
            }
            //result embed for collectdata
            //if ($("#popup_container").find("div.EmbedSetColumnResult_container").length >= 1) {
            //        var nilagay_na_formula = $('.getFields_' + object_id + '[data-output-col-total]').eq(0).attr("data-output-col-total");
            //        console.log("memdemdmedmemdemd",nilagay_na_formula);
            //      
            //}
            if ($('[data-properties-type="lblName"]').length >= 1) { // FS#3911 Form Builder : Remove the default value in the Label Name
                if ($.trim($("#setObject_" + object_id).find('#lbl_' + object_id).text()) != "" || $.type($("#setObject_" + object_id).find('#lbl_' + object_id).text()) != "undefined") {
                    $('[data-properties-type="lblName"]').val();
                }
            }
            if ($('[data-properties-type="embedViewVisibility"]').length >= 1) {
                if ($(".getFields_" + object_id).is('[data-embed-view-visibility-formula]') || $(".getFields_" + object_id).is('[field-visible="computed"][visible-formula]')) {
                    $('[data-properties-type="embedViewVisibility"]').val($(".getFields_" + object_id).attr("data-embed-view-visibility-formula"));
                }
            }


            if ($('[data-properties-type="actionEventVisibilityFormula"]').length >= 1) {
                if ($(".getFields_" + object_id).is('[data-action-event-visibility-formula]') || $(".getFields_" + object_id).is('[field-visible="computed"][visible-formula]')) {
                    $('[data-properties-type="actionEventVisibilityFormula"]').val($(".getFields_" + object_id).attr("data-action-event-visibility-formula"));
                }
            }

            if ($('[data-properties-type="lblPlaceHolder"]').length >= 1) {
                $('[data-properties-type="lblPlaceHolder"]').val($('.getFields_' + object_id + '[placeholder]').attr('placeholder'));
            }
            if ($('[data-properties-type="lblChoices"]').length >= 1) {//PANGKUHA NG NAME KAPAG WALA PANG SINESET
                if ($('.getFields_' + object_id + '[name]').is('select')) {
                    var data_list_value = $('.getFields_' + object_id + '[name]').children('option').map(function () {
                        if ($.trim($(this).val()) === $.trim($(this).text())) {
                            return $.trim($(this).text());
                        } else {
                            return $.trim($(this).text()) + "|" + $.trim($(this).val());
                        }
                    }).get().join('\n');
                    $('[data-properties-type="lblChoices"]').val(data_list_value);
                } else {
                    var data_list_value = $('.getFields_' + object_id + '[name]').map(function () {
                        if ($.trim($(this).val()) === $.trim($(this).parent().text())) {
                            return $.trim($(this).parent().text());
                        } else {
                            return $.trim($(this).parent().text()) + "|" + $.trim($(this).val());
                        }
                    }).get().join('\n');
                    $('[data-properties-type="lblChoices"]').val(data_list_value);
                }
            }
            {//restore embed properties
                if ($('[data-properties-type="EmbedEditRowForm"]').length >= 1) {
                    if ($(".getFields_" + object_id).is('[embed-action-click-edit="true"]')) {
                        $('[data-properties-type="EmbedEditRowForm"]').prop("checked", true);
                    }
                }
                if ($('[data-properties-type="EmbedCopyRowForm"]').length >= 1) {
                    if ($(".getFields_" + object_id).is('[embed-action-click-copy="true"]')) {
                        $('[data-properties-type="EmbedCopyRowForm"]').prop("checked", true);
                    }
                }
                if ($('[data-properties-type="EmbedDeleteRowForm"]').length >= 1) {
                    if ($(".getFields_" + object_id).is('[embed-action-click-delete="true"]')) {
                        $('[data-properties-type="EmbedDeleteRowForm"]').prop("checked", true);
                    }
                }
                if ($('[data-properties-type="EmbedCreateRowForm"]').length >= 1) {
                    if ($(".getFields_" + object_id).is('[embed-action-inline-create="true"]')) {
                        $('[data-properties-type="EmbedCreateRowForm"]').prop("checked", true);
                    }
                }
                if ($('[data-properties-type="EmbedViewRowForm"]').length >= 1) {
                    if ($(".getFields_" + object_id).is('[embed-action-click-view="true"]')) {
                        $('[data-properties-type="EmbedViewRowForm"]').prop("checked", true);
                    }
                }

                if ($('[data-properties-type="EmbedNumberRowForm"]').length >= 1) {
                    if ($(".getFields_" + object_id).is('[embed-action-click-number="true"]')) {
                        $('[data-properties-type="EmbedNumberRowForm"]').prop("checked", true);
                    }
                }

                if ($('[data-properties-type="EmbedEditPopUpRowForm"]').length >= 1) {
                    if ($(".getFields_" + object_id).is('[embed-action-click-edit-popup="true"]')) {
                        $('[data-properties-type="EmbedEditPopUpRowForm"]').prop("checked", true);
                    }
                }
                if ($('[data-properties-type="EmbedViewCustomAction"]').length >= 1) {
                    $(".embedded-view-custom-actions-icon-pick").iconPicker();
                    if ($(".getFields_" + object_id).is('[embed-action-custom-action="true"]')) {
                        $('[data-properties-type="EmbedViewCustomAction"]').prop("checked", true);
                        $('.embed-custom-actions-container').show();
                    }
                    else{
                        $('.embed-custom-actions-container').hide();
                    }
                    $('.select_custom_action').on('change', function(e){
                        if($(this).val() == "5"){
                            $(this).parent().children('.custom-action-custom-formula').show();
                        }
                        else{
                            $(this).parent().children('.custom-action-custom-formula').hide();
                        }

                    });
                    $('.embed_row_custom_actions').on("change", function(e){
                        var DOI = $("#popup_container").attr("data-object-id");
                        $(".getFields_" + DOI).attr("embed-action-custom-action", $(this).prop("checked"));
                        if($(this).prop('checked') == true){
                            $('.embed-custom-actions-container').show();
                        }
                        else{
                            $('.embed-custom-actions-container').hide();
                            // $(".getFields_" + DOI).find('.embed_customActionButton').css('display','none');
                            // $(".getFields_" + DOI).find('.embed_customActionList').css('display', 'none');   
                        }
                    });
                    // $('.embed-custom-actions-icon').on("click", function(e){
                    //     var relative_parent = $(this).closest('.input_position_below');
                    //     var customActions = $(this).parents('.embedCustomActions').data('svg_list');
                    //     var top = $(this).offset()['top'] + $(this).outerHeight();
                    //     var left = $(this).offset()['left'];
                    //     var icon_container = $('<div id="embed_actions_icon_list" class="properties_width_container fl-field-style embed_actions_icon_list embed_newRequest" data-object-id="' + $(this).attr('data-object-id') + '" style="width:270px;height:150px;overflow:auto;position:absolute;background-color:black;top: ' + top + 'px;left: ' + left + 'px;z-index: 99999999999;">' + customActions + '</div>');
                    //     if($('body').find('.embed_actions_icon_list').length > 0){
                    //         $('body').find('.embed_actions_icon_list').remove();
                    //     }
                    //     $('body').append(icon_container);
                    //     $('#embed_actions_icon_list').perfectScrollbar();
                    //     $('.embed_action_icon_click').on('click', function(e){
                    //         var self = $(this);
                    //         // console.log("parent element", $('.embedCustomActions'));
                    //         var parent_element = $('.embedCustomActions');
                    //         var get_index = parseInt($(this).parents('.embed_actions_icon_list').attr('data-object-id'));
                    //         var target_element = parent_element.find('.embed-custom-actions-icon').eq(get_index);
                    //         var selected_icon_data = self.find('svg > use').attr('xlink:href');
                    //         console.log("target_element", target_element, get_index);
                    //         target_element.find('svg').html('<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + selected_icon_data + '"></use>');
                    //         target_element.attr('data-selected-icon', selected_icon_data.replace("#", ""));
                    //         $('body').find('.embed_actions_icon_list').remove();
                    //         // var 
                    //     });
                    // })
                    // $('body').on('click', function(e){
                    //     if(e.target.id != "embed-custom-actions-icon" && e.target.parentNode.id != "embed-custom-actions-icon" && $(e.target).parents('.embed_actions_icon_list').attr('id') != "embed_actions_icon_list"){
                    //         if($('body').find('.embed_actions_icon_list').length > 0){
                    //             $('body').find('.embed_actions_icon_list').remove();
                    //         }
                    //     }
                    // });
                }
                if ($('.embedMergedColumns').length >= 1) {
                    // if ($(".getFields_" + object_id).is('[embed-action-custom-action="true"]')) {
                    //     $('[data-properties-type="EmbedViewCustomAction"]').prop("checked", true);
                    //     $('.embed-custom-actions-container').show();
                    // }
                    // else{
                    //     $('.embed-custom-actions-container').hide();
                    // }
                    // $('.embed_row_custom_actions').on("change", function(e){
                    //     var DOI = $("#popup_container").attr("data-object-id");
                    //     $(".getFields_" + DOI).attr("embed-action-custom-action", $(this).prop("checked"));
                    //     if($(this).prop('checked') == true){
                    //         $('.embed-custom-actions-container').show();
                    //     }
                    //     else{
                    //         $('.embed-custom-actions-container').hide();
                    //         // $(".getFields_" + DOI).find('.embed_customActionButton').css('display','none');
                    //         // $(".getFields_" + DOI).find('.embed_customActionList').css('display', 'none');   
                    //     }
                    // });
                    $('.embed-column-values-icon').on("click", function(e){
                        var relative_parent = $(this).closest('.input_position_below');
                        var customActions = $(this).parents('.embedMergedColumns').data('svg_list');
                        var top = $(this).offset()['top'] + $(this).outerHeight();
                        var left = $(this).offset()['left'];
                        var icon_container = $('<div id="embed_column_icon_list" class="properties_width_container fl-field-style embed_column_icon_list embed_newRequest" data-object-id="' + $(this).attr('data-object-id') + '" style="width:287px;height:150px;overflow:auto;position:absolute;background-color:black;top: ' + top + 'px;left: ' + left + 'px;z-index: 99999999999;">' + customActions + '</div>');
                        if($('body').find('.embed_column_icon_list').length > 0){
                            $('body').find('.embed_column_icon_list').remove();
                        }
                        $('body').append(icon_container);
                        $('#embed_column_icon_list').perfectScrollbar();
                        $('.embed_column_icon_click').on('click', function(e){
                            var self = $(this);
                            // console.log("parent element", $('.embedCustomActions'));
                            var parent_element = $('.embedMergedColumns');
                            var get_index = parseInt($(this).parents('.embed_column_icon_list').attr('data-object-id'));
                            var target_element = parent_element.find('.embed-column-values-icon').eq(get_index);
                            var selected_icon_data = self.find('svg > use').attr('xlink:href');
                            console.log("target_element", target_element, get_index);
                            target_element.find('svg').html('<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + selected_icon_data + '"></use>');
                            target_element.attr('data-selected-icon', selected_icon_data.replace("#", ""));
                            $('body').find('.embed_column_icon_list').remove();
                            // var 
                        });
                    })
                    // $('body').on('click', function(e){
                    //     if(e.target.id != "embed-column-values-icon" && e.target.parentNode.id != "embed-column-values-icon" && $(e.target).parents('.embed_column_icon_list').attr('id') != "embed_column_icon_list"){
                    //         if($('body').find('.embed_column_icon_list').length > 0){
                    //             $('body').find('.embed_column_icon_list').remove();
                    //         }
                    //     }
                    // });
                }
            }


            if ($('[data-properties-type="tableResponsiveContainer"]').length >= 1) {//fld-table-container-responsive="true"
                var fld_table_container_responsive = $('[data-type="table"][data-object-id="' + object_id + '"]').find('#obj_fields_' + object_id).children('.table-wrapper').children('table.form-table').attr("fld-table-container-responsive")
                $('[data-properties-type="tableResponsiveContainer"]').val(fld_table_container_responsive);
            }
            if ($('[data-properties-type="enable_embed_row_click"]').length >= 1) {
                if ($(".getFields_" + object_id).attr("enable-embed-row-click")) {
                    if ($(".getFields_" + object_id).attr("enable-embed-row-click") == "true") {
                        $('[data-properties-type="enable_embed_row_click"]').prop("checked", true);
                    } else {
                        $('[data-properties-type="enable_embed_row_click"]').prop("checked", false);
                    }
                }

                $('[data-properties-type="enable_embed_row_click"]').on({
                    "change": function () {
                        var DOI = $("#popup_container").attr("data-object-id");
                        $(".getFields_" + DOI).attr("enable-embed-row-click", $(this).prop("checked"));
                    }
                })
            }
            if ($('[data-properties-type="embed_row_click_creation_form"]').length >= 1) {
                if ($(".getFields_" + object_id).attr("embed-row-click-creation-form")) {
                    if ($(".getFields_" + object_id).attr("embed-row-click-creation-form") == "true") {
                        $('[data-properties-type="embed_row_click_creation_form"]').prop("checked", true);
                        $('[data-properties-type="embed_row_click_creation_form"]').parents(".input_position_below").find(".display2").removeClass("display2");
                    } else {
                        $('[data-properties-type="embed_row_click_creation_form"]').prop("checked", false);
                    }
                }

                $('[data-properties-type="embed_row_click_creation_form"]').on({
                    "change": function () {
                        var DOI = $("#popup_container").attr("data-object-id");
                        $(".getFields_" + DOI).attr("embed-row-click-creation-form", $(this).prop("checked"));
                    }
                })
            }
            if ($('[data-properties-type="embed_row_click_update_form"]').length >= 1) {
                if ($(".getFields_" + object_id).attr("embed-row-click-update-form")) {
                    if ($(".getFields_" + object_id).attr("embed-row-click-update-form") == "true") {
                        $('[data-properties-type="embed_row_click_update_form"]').prop("checked", true);
                    } else {
                        $('[data-properties-type="embed_row_click_update_form"]').prop("checked", false);
                    }
                }
                $('[data-properties-type="embed_row_click_update_form"]').on({
                    "change": function () {
                        var DOI = $("#popup_container").attr("data-object-id");
                        $(".getFields_" + DOI).attr("embed-row-click-update-form", $(this).prop("checked"));
                    }
                })
            }

            // if ($("[data-properties-type='defaultValue']").length >= 1) { //added by aaron
            // 	// $("[data-properties-type='defaultValue']").mention_post({
            // 	// 	// json: updatedFormula, // from local file fomula.json
            // 	// 	// highlightClass: "",
            // 	// 	// prefix: "",
            // 	// 	// countDisplayChoice: "4"
            // 	// });

            // }
            if ($(".dynamicListing_container").length >= 1) {
                // alert(2343432)
                // $(".dynamicListing_container").find('[data-properties-type="dynamicListing"]').mention_post({
                //     json: updatedFormula, // from local file fomula.json
                //     highlightClass: "",
                //     prefix: "",
                //     countDisplayChoice: "4"
                // });
            }

            var DOI = $("#popup_container").attr("data-object-id");
            if ($(".picklis_img_button").length >= 1) {
                if ($("body").data(DOI)) {
                    if ($("body").data(DOI)['imagePrev']) {
                        var body_data = $("body").data(DOI)['imagePrev'];
                        $(".imagePrev").attr("src", body_data);
                    }
                }
            }
            if ($('[data-properties-type="setReadOnlyField"]').length >= 1) {
                $('[data-properties-type="setReadOnlyField"]').on("change", function () {
                    var DOI = $("#popup_container").attr("data-object-id");
                    var the_element = $("#setObject_" + DOI);
                    if ($(this).val() == "Yes") {
                        the_element.find(".getFields_" + DOI).attr("readonly", "readonly");
                        //if (the_element.find(".getFields_" + DOI).is("input[type='checkbox']")) {
                        //    the_element.find(".getFields_" + DOI).attr("onclick", "return false");
                        //}
                        //else if (the_element.find(".getFields_" + DOI).is("input[type='radio']")) {
                        //
                        //    the_element.find(".getFields_"+DOI).attr("onclick","return this.checked=false");
                        //    //the_element.children('.fields_below').eq(0).prepend("<div class='readonly-overlay' style='width:100%;height:100%;background-color:rgba(0,0,0,0)'></div>");
                        //
                        //}
                        //else if (the_element.find(".getFields_" + DOI).is("select")) {
                        //     //the_element.children('.fields_below').eq(0).prepend("<div class='readonly-overlay' style='width:100%;height:100%;background-color:rgba(0,0,0,0)'></div>");
                        //    the_element.children('.fields_below').children(".input_position_below").find(".getFields_" + DOI).children('option:not(:selected)').prop('disabled', true);
                        //  
                        //    //the_element.find(".obj_fields_"+DOI).prepend("<div class='readonly-overlay' style='width:100%;height:100%;background-color:rgba(0,0,0,0)'></div>");
                        //  
                        //}


                    } else {
                        //the_element.children('.fields_below').children(".input_position_below").find(".getFields_" + DOI).children('option:not(:selected)').prop('disabled', false);
                        the_element.find(".getFields_" + DOI).removeAttr("readonly");
                        the_element.find(".getFields_" + DOI).removeAttr("onclick");
                    }
                });
            }

            //for embedded view multiple source form
            var embed_source_type = $("#setObject_" + DOI).find(".getFields_" + DOI).attr('source-form-type')||"single"; 
            embedProperties();
            // var form_data_table = new FormDataTable($('[data-type="formDataTable"][data-object-id="' + DOI + '"]'));
            // console.log(form_data_table.form_datatable);
            // form_data_table.loadFormDataTableProperties();
            //WHEN tableRelationalAction is existing
            if ($(".tableRelationalAction").length >= 1) {
                // '<label><input type="checkbox" name="prop-select-tbl-names" value="'+"name"+'" /><span class="name-text">'+'NAME'+'</span></label>'+
                // $('.select_names_here')
                var collect_choices_tblnames = "";
                $('[table-name]').each(function (eq) {
                    var dis_name = $(this).attr("table-name");
                    collect_choices_tblnames += '<label><input type="checkbox" name="prop-select-tbl-names" value="' + dis_name + '" /><span class="name-text">' + dis_name + '</span></label><br/>';
                })
                var collect_choices_tblnames_jq = $(collect_choices_tblnames);
                $('.select_names_here').html(collect_choices_tblnames_jq);
                //on click selecting table names
                $("[name='prop-select-tbl-names']").on("click", function () {
                    var dis_ele = $(this);
                    var dis_value = dis_ele.val();
                    var doid = $("#popup_container").attr('data-object-id');
                    var ele_form_tbl = $('#setObject_' + doid).find(".form-table").eq(0);
                    if (ele_form_tbl.attr("table-response-to")) {
                        var existing_vals = ele_form_tbl.attr("table-response-to");
                        var spl_existing_vals = existing_vals.split(",");

                        if ($(this).prop("checked") == true) {
                            if (spl_existing_vals.indexOf(dis_value) < 0) {
                                ele_form_tbl.attr("table-response-to", existing_vals + "," + dis_value);
                            }
                        } else {
                            if (spl_existing_vals.indexOf(dis_value) >= 0) {
                                ele_form_tbl.attr("table-response-to", spl_existing_vals.filter(function (i, e) {
                                    if (spl_existing_vals.indexOf(dis_value) == e) {
                                        return false;
                                    }
                                    ;
                                    return true;
                                }).join(","));
                            }
                        }
                    } else {
                        ele_form_tbl.attr("table-response-to", dis_value);
                    }
                });
                //restore selected table names for action response
                var doid = $("#popup_container").attr('data-object-id');
                var ele_form_tbl = $('#setObject_' + doid).find(".form-table").eq(0);
                var existing_vals = ele_form_tbl.attr("table-response-to");
                if (existing_vals) {
                    var spl_existing_vals = existing_vals.split(",");
                    $("[name='prop-select-tbl-names']").filter(function (i, e) {
                        if (spl_existing_vals.indexOf($(this).val()) >= 0) {
                            return true;
                        } else {
                            return false;
                        }
                    }).prop("checked", true);
                }

            }

            //WHEN table-name is existing
            if ($("#popup_content").find('[name="set-prop-table-name"]').length >= 1) {
                var DOI = $("#popup_container").attr("data-object-id");
                var the_element = $("#setObject_" + DOI);
                if (the_element.find(".form-table").length >= 1) {
                    $("#popup_content").find('[name="set-prop-table-name"]').on({
                        "keyup": function (e) {
                            var dis_value = $(this).val();
                            var DOI = $("#popup_container").attr("data-object-id");
                            var the_element = $("#setObject_" + DOI);

                            the_element.find(".form-table").eq(0).attr("table-name", dis_value);
                        }
                    })
                    if (the_element.find(".form-table").eq(0).attr("table-name")) {
                        $("#popup_content").find('[name="set-prop-table-name"]').val(the_element.find(".form-table").eq(0).attr("table-name"))
                    }
                }
            }

            //WHEN data-properties-type="lblAllowLabel-RC"
            if ($('[data-properties-type="lblAllowLabel-RC"]').length >= 1) {
                $('[data-properties-type="lblAllowLabel-RC"]').on("change", function () {
                    var choose_value = $(this).val();
                    var DOI = $("#popup_container").attr("data-object-id");
                    var the_element = $("#setObject_" + DOI);

                    if (choose_value == "Yes") {
                        if (the_element.attr("data-type") == "checkbox") {
                            $("#setObject_" + DOI).find(".checkbox-input-label").show();
                        } else {
                            $("#setObject_" + DOI).find(".radio-input-label").show();
                        }
                    } else {
                        if (the_element.attr("data-type") == "checkbox") {
                            $("#setObject_" + DOI).find(".checkbox-input-label").hide();
                        } else {
                            $("#setObject_" + DOI).find(".radio-input-label").hide();
                        }
                    }
                });

            }

            //WHEN PICKLIST SELECTION IS EXISTING
            if ($("[name='selection_type']").length >= 2) {

                var DOI = $("#popup_container").attr("data-object-id");
                console.log("DOI", DOI);
                if ($("#setObject_" + DOI).find("[picklist-button-id='picklist_button_" + DOI + "']").attr("selection-type")) {
                    $("[name='selection_type'][value='" + $("#setObject_" + DOI).find("[picklist-button-id='picklist_button_" + DOI + "']").attr("selection-type") + "']").prop("checked", true);
                }
                $("[name='selection_type']").on("change", function () {
                    var dis_value = $(this).val();
                    var DOI = $("#popup_container").attr("data-object-id");
                    $("#setObject_" + DOI).find("[picklist-button-id='picklist_button_" + DOI + "']").attr("selection-type", dis_value);
                });
            }


            //WHEN VISIBILITY IS EXISTING
            if ($(".chk_Visibility").length >= 1) {
                $(".chk_Visibility").find("[name='visibility-choices']").on("change.field_visibility", function (e) {
                    var DOI = $("#popup_container").attr("data-object-id");
                    if ($(this).val() == "computed") {
                        // alert( $(".chk_Visibility").find(".visibility-computed").val() )
                        $(".getFields_" + DOI).attr("visible-formula", $(".chk_Visibility").find(".visibility-computed").val());
                        $(".getFields_" + DOI).attr("field-visible", "computed");
                    } else if ($(this).val() == "no") {
                        $(".getFields_" + DOI).attr("field-visible", false);
                    } else {
                        $(".getFields_" + DOI).attr("field-visible", true);
                    }
                });
                //visibility formula
                $(".chk_Visibility").find(".visibility-formula").on("keyup", function () {
                    var DOI = $("#popup_container").attr("data-object-id");
                    $(".getFields_" + DOI).attr("visible-formula", $(this).val());
                });

                //RESTORING VALUES
                if (typeof $(".getFields_" + object_id).attr("field-visible") != "undefined") {
                    $(".chk_Visibility").find("[name='visibility-choices'][value='" + $(".getFields_" + object_id).attr("field-visible") + "']").prop("checked", true);
                    if ($(".getFields_" + object_id).attr("field-visible") == "computed") {
                        $(".chk_Visibility").find(".visibility-formula").val($(".getFields_" + object_id).attr("visible-formula"));
                    }
                }
            }

            //WHEN CHECKBOX EVENT TRIGGER
            if ($("#refreshtrigger").length >= 1) {
                //bind onchange of refresh trigger properties
                $("#refreshtrigger").on({
                    "change": function () {
                        var DOI = $("#popup_container").attr("data-object-id");
                        $(".getFields_" + DOI).attr("lookup-event-trigger", $(this).prop("checked"));
                    }
                });
                //restore value of properties
                if (typeof $(".getFields_" + object_id).attr("lookup-event-trigger") != "undefined") {
                    if ($(".getFields_" + object_id).attr("lookup-event-trigger") == "false") {
                        $("#refreshtrigger").prop("checked", false);
                    } else {
                        $("#refreshtrigger").prop("checked", true);
                    }
                }
            }

            //WHEN FORMTABLE PROPERTY IS EXISTING FOR DYNAMIC TABLE
            if ($(".form_table_type").length >= 1) {
                var form_table_type_ele = $(".form_table_type");
                //RESTORING THE SELECTED VALUE
                if ($("#setObject_" + object_id).find(".form-table").eq(0).attr("repeater-table") == "true") {
                    form_table_type_ele.find(".form-table-type").children("option[value='dynamic']").prop("selected", true);
                }
                //SETTINGS OF TABLE TYPE WHEN ON CHANGE
                form_table_type_ele.find(".form-table-type").on({
                    "change": function () {
                        var DOI = $("#popup_container").attr("data-object-id");
                        if ($(this).val() == "normal") {
                            $("#setObject_" + DOI).eq(0).children(".ui-resizable-s").hide();
                            $("#setObject_" + DOI).eq(0).css("height", "auto");
                            $("#setObject_" + DOI).find(".form-table").eq(0).closest(".table-wrapper").find(".table-actions-ra.action-row-add").show();
                            $("#setObject_" + DOI).find(".form-table").eq(0).removeAttr("repeater-table");

                        } else if ($(this).val() == "dynamic") {
                            var dataRes = $("#setObject_" + DOI).data();
                            if (dataRes["uiResizable"]) {
                                $("#setObject_" + DOI).children(".ui-resizable-s").show();
                            } else {
                                $("#setObject_" + DOI).children(".ui-resizable-s").show();

                                // $("#setObject_" + DOI).resizable("enable");
                                // $("#setObject_" + DOI).resizable("option","handles","s");


                                // $("#setObject_" + DOI).resizable({
                                //     containment: "parent",
                                //     handles: 's',
                                //     resize: function(event, ui) {
                                //         //$(".object_properties").popover('hide');
                                //         // ui.size.width = ui.size.width - 10;
                                //         // ui.size.height = ui.size.height - 10;
                                //         // console.log(ui.size.width)
                                //         var dis_id = $(this).attr("data-object-id");
                                //         var obj_fld_container = $(this).find("#obj_fields_" + dis_id);
                                //         var obj_fld_container_pos_top = obj_fld_container.position().top;
                                //         obj_fld_container.css({
                                //             "height": "calc(100% - " + (obj_fld_container_pos_top) + "px)"
                                //         });
                                //     },
                                //     stop: function(ee, ui) {
                                //         // $(container).resizable("option", "minWidth", checkFormMinWidth());
                                //         // $(container).resizable("option", "minHeight", checkFormMinHeight());
                                //         $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
                                //         $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
                                //         $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
                                //         $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")
                                //     }
                                // })
                            }

                            var rows_length = $("#setObject_" + DOI).find(".form-table").eq(0).children("tbody").children("tr").length;
                            if (rows_length == 1) {
                                $("#setObject_" + DOI).find(".form-table").eq(0).closest(".table-wrapper").find(".table-actions-ra.action-row-add").hide();
                                $("#setObject_" + DOI).find(".form-table").eq(0).attr("repeater-table", true);
                            } else {
                                var self = $(this);
                                var conf = "Selecting dynamic row, will delete the existing rows below\nAnd only the first row will be left.\nWould you still like to continue?\nNOTE: you cannot undone deleted rows.";
                                jConfirm(conf, 'Confirmation Dialog', '', '', '', function (answer) {
                                    if (answer == true) {
                                        $("#setObject_" + DOI).find(".form-table").eq(0).children("tbody").children("tr").each(function (eqi) {
                                            if (eqi >= 1) {
                                                $(this).remove();
                                            }
                                        });
                                        $("#setObject_" + DOI).find(".form-table").eq(0).closest(".table-wrapper").find(".table-actions-ra.action-row-add").hide();
                                        $("#setObject_" + DOI).find(".form-table").eq(0).attr("repeater-table", true);
                                    } else {
                                        self.val("normal");
                                    }
                                });
                            }

                        }
                    }
                });
            }



            //WHEN ALIGNMENT OF RADIO AND CHECKBOX IS EXISTING
            if ($(".rcAlignmentProperty").length >= 1) {
                var property_container = $(".rcAlignmentProperty");
                var DOI = $("#popup_container").attr("data-object-id");

                //RESTORE VALUES
                if ($("#setObject_" + DOI).find(".rc-prop-alignment").hasClass("rc-align-vertical")) {
                    property_container.find(".rc-alignment").val("vertical");
                } else if ($("#setObject_" + DOI).find(".rc-prop-alignment").hasClass("rc-align-horizontal")) {
                    property_container.find(".rc-alignment").val("horizontal");
                }

                //SET ONCHANGE OF THE PROPERTY
                property_container.find(".rc-alignment").on({
                    "change": function () {
                        var this_value = $(this).val();
                        var DOI = $("#popup_container").attr("data-object-id");
                        if (this_value == "vertical") {
                            $("#setObject_" + DOI).attr("rc-align-type", "vertical");
                            $("#setObject_" + DOI).find(".rc-prop-alignment").removeClass("rc-align-horizontal").addClass("rc-align-vertical");
                            if ($('[name="option-spacing"]').length >= 1) {
                                $("#setObject_" + DOI).find(".rc-prop-alignment").not(':last-child').css({
                                    "height": (Number($('[name="option-spacing"]').val())) + "px",
                                    "width": (0) + "px"
                                })
                            }

                        } else if (this_value == "horizontal") {
                            $("#setObject_" + DOI).attr("rc-align-type", "horizontal");
                            $("#setObject_" + DOI).find(".rc-prop-alignment").removeClass("rc-align-vertical").addClass("rc-align-horizontal");

                            if ($('[name="option-spacing"]').length >= 1) {
                                $("#setObject_" + DOI).find(".rc-prop-alignment").not(':last-child').css({
                                    "width": (Number($('[name="option-spacing"]').val())) + "px",
                                    "height": (0) + "px"
                                })
                            }
                        }
                    }
                });
            }

            //WHEN obj_inputValidation IS EXISTING
            if ($(".obj_inputValidation").length >= 1) {
                var obj_prop_data_validation = $(".obj_inputValidation").find(".field-formula-validation").eq(0);
                //restoring value of the specific field for validation
                if ($('.getFields_' + object_id + '[field-formula-validation]').length >= 1) {
                    obj_prop_data_validation.val($('.getFields_' + object_id + '[field-formula-validation]').attr("field-formula-validation"));
                }

                obj_prop_data_validation.on({
                    "keyup": function (e) {
                        //saving value to attribute of a specific field
                        this_value = $(this).val();
                        $(".getFields_" + object_id).attr("field-formula-validation", this_value);

                        var json_data_valid = $("body").data();
                        //saving to json
                        if (typeof json_data_valid[object_id] != "undefined") {
                            json_data_valid[object_id].field_formula_validation = "" + this_value;
                        } else {
                            json_data_valid[object_id] = {
                                field_formula_validation: this_value
                            };
                        }
                        $("body").data(json_data_valid);
                    }
                });
            }
            //WHEN obj_inputValidationMessage IS EXISTING
            if ($(".obj_inputValidationMessage").length >= 1) {
                var obj_prop_data_validation = $(".obj_inputValidationMessage").find(".field-formula-validation-message").eq(0);
                //restoring value of the specific field for validation
                if ($('.getFields_' + object_id + '[field-formula-validation-message]').length >= 1) {
                    obj_prop_data_validation.val($('.getFields_' + object_id + '[field-formula-validation-message]').attr("field-formula-validation-message").replace(/<[\s]*?br[\s]*?[\/]?[\s]*?>/g, "\n"));
                }

                obj_prop_data_validation.on("keyup", function () {
                    this_value = $(this).val().replace(/\n/g, "<br/>");
                    $(".getFields_" + object_id).attr("field-formula-validation-message", this_value);

                    var json_data_valid_message = $("body").data();
                    //saving to json
                    if (typeof json_data_valid_message[object_id] != "undefined") {
                        json_data_valid_message[object_id].field_formula_validation = "" + this_value;
                    } else {
                        json_data_valid_message[object_id] = {
                            field_formula_validation_message: this_value
                        };
                    }
                    $("body").data(json_data_valid_message);
                });
            }

            defaultValuesFN(object_id);
            // Show Scroll bar of popover
            // $('.scrollbarM').perfectScrollbar({
            // 				wheelSpeed: 20,
            // 				wheelPropagation: false
            // 			      });

            // alert(324);

            $(".ps-scrollbar-x").hide();

            // Get Object Attribute
            var object_type = $(this).attr("data-object-type");
            // var object_id = $(this).attr("data-object-id");
            var object_id = $("#popup_container").attr("data-object-id")
            //setObject_1
            $(".obj_prop").attr("data-object-id", object_id);
            $(".obj_prop").attr("id", "obj_prop" + object_id);

            // Get Field Height

            // console.log("WAHA HEIGHT", $("#getFields_" + object_id))
            var field_height = $("#getFields_" + object_id).outerHeight();
            var field_width = $("#getFields_" + object_id).outerWidth();
            $("#obj_prop" + object_id + "[data-properties-type='lblFieldHeight']").val(field_height);
            $("#obj_prop" + object_id + "[data-properties-type='lblFieldWidth']").val(field_width);

            // Get Value of the Object Properties On Text Field
            $(".obj_prop").each(function () {
                var json = $("body").data();
                var objID = $(this).attr("data-object-id");
                if (typeof objID === "undefined") {
                    objID = $('#popup_container').attr("data-object-id");
                }
                var data_properties_type = $(this).attr("data-properties-type");
                if (json['' + objID + '']) {
                    var valueOnJSON = json['' + objID + '']['' + data_properties_type + ''];
                    if ($('[data-properties-type="defaultValueType"]').length >= 1) {
                        if (json['' + objID + '']["defaultValueType"]) {
                            $('[data-properties-type="defaultValueType"][value="' + json['' + objID + '']["defaultValueType"] + '"]').prop('checked', true);
                            if (json['' + objID + '']["defaultValueType"] != "static") {
                                var updatedFormula = UpdateMentionData.init();
                                if ($("[data-properties-type='defaultValue']").length >= 1) { //added by aaron
                                   /* $("[data-properties-type='defaultValue']").mention_post({
                                        json: updatedFormula, // from local file fomula.json
                                        highlightClass: "",
                                        prefix: "",
                                        countDisplayChoice: "4"
                                    });*/

                                }
                            }
                        }
                    }
                    //     if ($('[data-properties-type="defaultValue"]').length >= 1) {
                    //         if (json['' + objID + '']["defaultValue"]) {
                    //             $('[data-properties-type="defaultValue"][value="' + json['' + objID + '']["defaultValue"] + '"]').prop('checked', true);
                    //         }
                    //     }
                    if (data_properties_type == "lblFieldType") {
                        $(this).children("option[data-input-type='" + $("#getFields_" + objID).attr("data-input-type") + "']").prop("selected", true);
                    } else if (valueOnJSON) {
                        $(this).val(valueOnJSON);
                    }

                    {

                        if (data_properties_type == "defaultValue") {
                            if ($('[data-properties-type="defaultValue"]').length >= 1) {
                                $('[data-properties-type="defaultValue"]').trigger("keyup");
                            }
                        }
                        if (data_properties_type == "dynamicListing") {
                            if ($(".dynamicListing_container").length >= 1) {
                                $(".dynamicListing_container").find('[data-properties-type="dynamicListing"]').trigger("keyup");
                            }
                        }
                        if (data_properties_type == "lblFieldMinHeight") {
                            if (json['' + objID + ''][data_properties_type]) {
                                $(this).val(json['' + objID + ''][data_properties_type]);
                            } else {

                                if ($("#setObject_" + objID).attr("data-type") == "noteStyleTextarea") {
                                    $(this).val($("#setObject_" + objID).find(".content-editable-containter").eq(0).children('[contenteditable="true"]').eq(0).css("min-height").replace("px", ""));
                                } else {
                                    $(this).val($("#setObject_" + objID).find(".getFields_" + objID).css("min-height").replace("px", ""));
                                }
                            }
                        }

                        if (data_properties_type == "lblFieldMaxHeight") {
                            if (json['' + objID + ''][data_properties_type]) {
                                $(this).val(json['' + objID + ''][data_properties_type]);
                            } else {
                                if ($("#setObject_" + objID).attr("data-type") == "noteStyleTextarea") {
                                    $(this).val($("#setObject_" + objID).find(".content-editable-containter").eq(0).children('[contenteditable="true"]').eq(0).css("max-height").replace("px", ""));
                                } else {
                                    $(this).val($("#setObject_" + objID).find(".getFields_" + objID).css("max-height").replace("px", ""));
                                }
                            }
                        }

                        if (data_properties_type == "lblFieldMinWidth") {
                            if (json['' + objID + ''][data_properties_type]) {
                                $(this).val(json['' + objID + ''][data_properties_type]);
                            } else {
                                if ($("#setObject_" + objID).attr("data-type") == "noteStyleTextarea") {
                                    $(this).val($("#setObject_" + objID).find(".content-editable-containter").eq(0).children('[contenteditable="true"]').eq(0).css("min-width").replace("px", ""));
                                } else {
                                    $(this).val($("#setObject_" + objID).find(".getFields_" + objID).css("min-width").replace("px", ""));
                                }
                            }
                        }

                        if (data_properties_type == "lblFieldMaxWidth") {
                            if (json['' + objID + ''][data_properties_type]) {
                                $(this).val(json['' + objID + ''][data_properties_type]);
                            } else {
                                if ($("#setObject_" + objID).attr("data-type") == "noteStyleTextarea") {
                                    $(this).val($("#setObject_" + objID).find(".content-editable-containter").eq(0).children('[contenteditable="true"]').eq(0).css("max-width").replace("px", ""));
                                } else {
                                    $(this).val($("#setObject_" + objID).find(".getFields_" + objID).css("max-width").replace("px", ""));
                                }
                            }
                        }
                    }
                } else {
                    {
                        if (data_properties_type == "lblFieldMinHeight") {
                            if ($("#setObject_" + objID).attr("data-type") == "noteStyleTextarea") {
                                $(this).val($("#setObject_" + objID).find(".content-editable-containter").eq(0).children('[contenteditable="true"]').eq(0).css("min-height").replace("px", ""));
                            } else {
                                $(this).val($("#setObject_" + objID).find(".getFields_" + objID).css("min-height").replace("px", ""));
                            }
                        }

                        if (data_properties_type == "lblFieldMaxHeight") {
                            if ($("#setObject_" + objID).attr("data-type") == "noteStyleTextarea") {
                                $(this).val($("#setObject_" + objID).find(".content-editable-containter").eq(0).children('[contenteditable="true"]').eq(0).css("max-height").replace("px", ""));
                            } else {
                                $(this).val($("#setObject_" + objID).find(".getFields_" + objID).css("max-height").replace("px", ""));
                            }
                        }

                        if (data_properties_type == "lblFieldMinWidth") {
                            if ($("#setObject_" + objID).attr("data-type") == "noteStyleTextarea") {
                                $(this).val($("#setObject_" + objID).find(".content-editable-containter").eq(0).children('[contenteditable="true"]').eq(0).css("min-width").replace("px", ""));
                            } else {
                                $(this).val($("#setObject_" + objID).find(".getFields_" + objID).css("min-width").replace("px", ""));
                            }
                        }

                        if (data_properties_type == "lblFieldMaxWidth") {
                            if ($("#setObject_" + objID).attr("data-type") == "noteStyleTextarea") {
                                $(this).val($("#setObject_" + objID).find(".content-editable-containter").eq(0).children('[contenteditable="true"]').eq(0).css("max-width").replace("px", ""));
                            } else {
                                $(this).val($("#setObject_" + objID).find(".getFields_" + objID).css("max-width").replace("px", ""));
                            }
                        }
                    }
                }

            });
            var json = $("body").data();
            var object_id = if_undefinded($(".workflow-processor").attr("data-object-id"), $("#popup_container").attr("data-object-id"));
            if (json['' + object_id + '']) {
                getprocessor(json['' + object_id + '']['processor-type'], json['' + object_id + '']['processor']);
            }

            // Color Picker
            var lblFontColor = $("#lbl_" + object_id).css("color");
            var fld_fcolor_settings = $.extend({}, default_spectrum_settings);
            fld_fcolor_settings['change'] = function (a, b) {
                var color_value = $(this).val();
                var type = $(this).attr("data-type");
                var object_id = $("#popup_container").attr("data-object-id");
                //

                if ($("#obj_fields_" + object_id).find(".getFields_" + object_id).eq(0).attr("type") == "radio" || $("#obj_fields_" + object_id).find(".getFields_" + object_id).eq(0).attr("type") == "checkbox") {
                    $("#obj_fields_" + object_id).find(".getFields_" + object_id).parent().css("color", color_value);
                    //$("#obj_fields_" + object_id).find(".getFields_" + object_id).parent().css("line-height", (Number(lbl_size.replace("px", "")) + 11) + "px");
                } else {
                    $("#obj_fields_" + object_id).find(".getFields_" + object_id).eq(0).css("color", color_value);


                    $("#obj_fields_" + object_id).find(".content-editable-field_" + object_id).eq(0).css("color", color_value);

                }
                //

                var data_properties_type = $(this).attr("data-properties-type");
                var json = $("body").data();
                if (json['' + object_id + '']) {
                    var prop_json = json['' + object_id + ''];
                } else {
                    var prop_json = {};
                }
                prop_json['' + data_properties_type + ''] = color_value;
                json['' + object_id + ''] = prop_json;
                $("body").data(json);


            };
            fld_fcolor_settings['cancelFN'] =  function(color){
               var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color")||"rgb(0, 0, 0)";
               var object_id = $("#popup_container").attr("data-object-id");
               if ($("#obj_fields_" + object_id).find(".getFields_" + object_id).eq(0).attr("type") == "radio" || $("#obj_fields_" + object_id).find(".getFields_" + object_id).eq(0).attr("type") == "checkbox") {
                   $("#obj_fields_" + object_id).find(".getFields_" + object_id).parent().css("color", revert_color);
               } else {
                   $("#obj_fields_" + object_id).find(".getFields_" + object_id).eq(0).css("color", revert_color);
                   $("#obj_fields_" + object_id).find(".content-editable-field_" + object_id).eq(0).css("color", revert_color);
               }
               var data_properties_type = $(this).attr("data-properties-type");
               var json = $("body").data();
               if (json['' + object_id + '']) {
                   var prop_json = json['' + object_id + ''];
               } else {
                   var prop_json = {};
               }

               prop_json['' + data_properties_type + ''] = revert_color;
               json['' + object_id + ''] = prop_json;
               $("body").data(json);
            };
            fld_fcolor_settings['allowEmpty'] = true;
            

            $(".chngColor[data-properties-type='fld_fc']").spectrum(fld_fcolor_settings);

            $(".chngColor[data-properties-type='lblFontolor']").spectrum({
                color: lblFontColor,
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxPaletteSize: 10,
                preferredFormat: "hex",
                localStorageKey: "spectrum.demo",
                hide: function(color) {
                   var revert_color = $(this).next().find('.sp-preview-inner').css("background-color");
                   $("#lbl_" + object_id).css("color", revert_color);
                   var data_properties_type = $(this).attr("data-properties-type");
                   var json = $("body").data();
                   if (json['' + object_id + '']) {
                       var prop_json = json['' + object_id + ''];
                   } else {
                       var prop_json = {};
                   }

                   prop_json['' + data_properties_type + ''] = revert_color;
                   json['' + object_id + ''] = prop_json;
                   $("body").data(json);
                },
                cancelFN: function(color){
                   var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
                   $("#lbl_" + object_id).css("color", revert_color);
                   var data_properties_type = $(this).attr("data-properties-type");
                   var json = $("body").data();
                   if (json['' + object_id + '']) {
                       var prop_json = json['' + object_id + ''];
                   } else {
                       var prop_json = {};
                   }

                   prop_json['' + data_properties_type + ''] = revert_color;
                   json['' + object_id + ''] = prop_json;
                   $("body").data(json);
                },
                change: function (color) {
                    var type = $(this).attr("data-type");
                    $("#lbl_" + object_id).css("color", color.toHexString());
                    var data_properties_type = $(this).attr("data-properties-type");
                    var json = $("body").data();
                    if (json['' + object_id + '']) {
                        var prop_json = json['' + object_id + ''];
                    } else {
                        var prop_json = {};
                    }

                    prop_json['' + data_properties_type + ''] = color.toHexString()
                    json['' + object_id + ''] = prop_json;
                    $("body").data(json);
                },
                show: function () {
                    var self = $(this);
                    var selfNextTop = $('.sp-container:visible').position().top;
                    var selfNextLeft = $('.sp-container:visible').position().left;
                    var docScollTop = $(document).scrollTop();
                    var docScollLeft = $(document).scrollLeft();
                    $('.sp-container:visible').css({'position':'fixed', 'top':(selfNextTop-docScollTop)+'px', 'left':(selfNextLeft-docScollLeft)+'px' });
                  
                 },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                        "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });

            // Color Picker
            var lblFieldColor = "";
            if ($(".getFields_" + object_id).css("display") == "none") {
                lblFieldColor = $(".content-editable-field_" + object_id).css("border-color");
            } else {
                lblFieldColor = $(".getFields_" + object_id).css("border-color");
            }
            $(".chngColor[data-properties-type='lblFieldColor']").spectrum({
                color: lblFieldColor,
                showInput: true,
                //color: field_restore_color,
                className: "full-spectrum",
                showInitial: true,
                showAlpha: true,
                showPalette: true,
                showSelectionPalette: true,
                maxPaletteSize: 10,
                preferredFormat: "rgb",
                localStorageKey: "spectrum.demo",
                hide: function(color) {
                    var object_id = $('[id="popup_container"]:eq(0)').attr("data-object-id");
                    var revert_color = $(this).next().find('.sp-preview-inner').css("background-color");
                    $(".getFields_" + object_id).css("border-color", revert_color);
                    $(".content-editable-field_" + object_id).css("border-color", revert_color);
                    var data_properties_type = $(this).attr("data-properties-type");
                    var json = $("body").data();
                    if (json['' + object_id + '']) {
                        var prop_json = json['' + object_id + ''];
                    } else {
                        var prop_json = {};
                    }
                    prop_json['' + data_properties_type + ''] = revert_color;
                    json['' + object_id + ''] = prop_json;
                    $("body").data(json);
                },
                cancelFN: function(color){
                    var object_id = $('[id="popup_container"]:eq(0)').attr("data-object-id");
                    var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
                    $(".getFields_" + object_id).css("border-color", revert_color);
                    $(".content-editable-field_" + object_id).css("border-color", revert_color);
                    var data_properties_type = $(this).attr("data-properties-type");
                    var json = $("body").data();
                    if (json['' + object_id + '']) {
                        var prop_json = json['' + object_id + ''];
                    } else {
                        var prop_json = {};
                    }
                    prop_json['' + data_properties_type + ''] = revert_color;
                    json['' + object_id + ''] = prop_json;
                    $("body").data(json);
                },
                show: function () {
                    var self = $(this);
                    var selfNextTop = $('.sp-container:visible').position().top;
                    var selfNextLeft = $('.sp-container:visible').position().left;
                    var docScollTop = $(document).scrollTop();
                    var docScollLeft = $(document).scrollLeft();
                    $('.sp-container:visible').css({'position':'fixed', 'top':(selfNextTop-docScollTop)+'px', 'left':(selfNextLeft-docScollLeft)+'px' });
                  
                },
                change: function (color) {
                    var color_value = $(this).val();
                    var type = $(this).attr("data-type");
                    var object_id = $("#popup_container").attr("data-object-id");
                    $(".getFields_" + object_id).css("border-color", color_value);
                    $(".content-editable-field_" + object_id).css("border-color", color_value);
                    var data_properties_type = $(this).attr("data-properties-type");
                    var json = $("body").data();
                    if (json['' + object_id + '']) {
                        var prop_json = json['' + object_id + ''];
                    } else {
                        var prop_json = {};
                    }
                    prop_json['' + data_properties_type + ''] = color_value;
                    json['' + object_id + ''] = prop_json;
                    $("body").data(json);
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                        "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });
            /*$(".chngColor[data-properties-type='lblFieldColor']").spectrum({
             color: lblFieldColor,
             showInput: true,
             className: "full-spectrum",
             showInitial: true,
             showPalette: true,
             showSelectionPalette: true,
             maxPaletteSize: 10,
             preferredFormat: "hex",
             localStorageKey: "spectrum.demo",
             change: function(color) {
             var color_value = $(this).val();
             var type = $(this).attr("data-type");
             var  object_id = $("#popup_container").attr("data-object-id");
             //$(".getFields_" + object_id).css("border-color", color.toHexString());
             // $(".content-editable-field_" + object_id).css("border-color", color.toHexString());
             // $(".getFields_" + object_id).css("border-color", color.toHexString());
             
             $(".content-editable-field_" + object_id).attr("style", $(".content-editable-field_" + object_id).attr('style') + ';border-color:' + color_value + '!important');
             $(".getFields_" + object_id).attr("style", $(".getFields_" + object_id).attr('style') + ';border-color:' + color_value + '!important');
             
             var data_properties_type = $(this).attr("data-properties-type");
             var json = $("body").data();
             if (json['' + object_id + '']) {
             var prop_json = json['' + object_id + ''];
             } else {
             var prop_json = {};
             }
             prop_json['' + data_properties_type + ''] = color.toHexString()
             json['' + object_id + ''] = prop_json;
             $("body").data(json);
             },
             palette: [
             ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
             "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
             ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
             "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
             ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
             "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
             "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
             "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
             "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
             "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
             "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
             "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
             "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
             "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
             ]
             });*/

            var field_restore_color = "";
            if ($(".getFields_" + object_id).css("display") == 'none') {
                field_restore_color = $(".content-editable-field_" + object_id).css("background-color");
            } else {
                field_restore_color = $(".getFields_" + object_id).css("background-color");
            }

            var lblFieldBGColor = $(".chngColor[data-properties-type='lblFieldBGColor']").spectrum({
                //color: lblFieldColor,
                showInput: true,
                color: field_restore_color,
                className: "full-spectrum",
                showInitial: true,
                showAlpha: true,
                showPalette: true,
                showSelectionPalette: true,
                maxPaletteSize: 10,
                preferredFormat: "rgb",
                localStorageKey: "spectrum.demo",
                hide: function(color) {
                    var object_id = $('[id="popup_container"]:eq(0)').attr("data-object-id");
                    var revert_color = $(this).next().find('.sp-preview-inner').css("background-color");
                    $(".content-editable-field_" + object_id).css("background-color", revert_color);
                    $(".getFields_" + object_id).css('background-color', revert_color);
                    var data_properties_type = $(this).attr("data-properties-type");
                    var json = $("body").data();
                    if (json['' + object_id + '']) {
                        var prop_json = json['' + object_id + ''];
                    } else {
                        var prop_json = {};
                    }
                    prop_json['' + data_properties_type + ''] = revert_color;
                    json['' + object_id + ''] = prop_json;
                    $("body").data(json);
                },
                cancelFN: function(color){
                    var object_id = $('[id="popup_container"]:eq(0)').attr("data-object-id");
                    var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
                    $(".content-editable-field_" + object_id).css("background-color", revert_color);
                    $(".getFields_" + object_id).css('background-color', revert_color);
                    var data_properties_type = $(this).attr("data-properties-type");
                    var json = $("body").data();
                    if (json['' + object_id + '']) {
                        var prop_json = json['' + object_id + ''];
                    } else {
                        var prop_json = {};
                    }
                    prop_json['' + data_properties_type + ''] = revert_color;
                    json['' + object_id + ''] = prop_json;
                    $("body").data(json);
                },
                show: function () {
                    var self = $(this);
                    var selfNextTop = $('.sp-container:visible').position().top;
                    var selfNextLeft = $('.sp-container:visible').position().left;
                    var docScollTop = $(document).scrollTop();
                    var docScollLeft = $(document).scrollLeft();
                    $('.sp-container:visible').css({'position':'fixed', 'top':(selfNextTop-docScollTop)+'px', 'left':(selfNextLeft-docScollLeft)+'px' });
                  
                },
                change: function (color) {
                    var color_value = $(this).val();
                    var object_id = $("#popup_container").attr("data-object-id");
                    console.log("BACKGROUND COLOR", color, $(this).val(), $(".content-editable-field_" + object_id))
                    var type = $(this).attr("data-type");
                    /*$(".getFields_" + object_id).css("background-color", color_value + '!important');
                     $(".content-editable-field_" + object_id).css("background-color", color_value + '!important');*/
                    $(".content-editable-field_" + object_id).css("background-color", color_value);
                    $(".getFields_" + object_id).css('background-color', color_value);
                    //$(".getFields_" + object_id).attr("style", $(".getFields_" + object_id).attr('style') + ";background-color:" + color_value + '!important');
                    $(".content-editable-field_" + object_id).attr("style", $(".content-editable-field_" + object_id).attr('style') + ";background-color:" + color_value + '!important');

                    var data_properties_type = $(this).attr("data-properties-type");
                    var json = $("body").data();
                    if (json['' + object_id + '']) {
                        var prop_json = json['' + object_id + ''];
                    } else {
                        var prop_json = {};
                    }


                    prop_json['' + data_properties_type + ''] = color_value;
                    json['' + object_id + ''] = prop_json;
                    $("body").data(json);
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                        "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });
            //scroll
            $('#content-dialog-scroll').perfectScrollbar();

            if ($("[data-properties-type='defaultValueType']:checked").val() == "middleware") {
                $(".middlewareTaggingContainer").css("display", "");
            }
        }

       

    });
    numericFunction(".prop_numeric_val");


    $(".object_setup").tooltip({
        html: true,
        placement: "top"
    });

    //console.log("DRAGOBECTS", gettingFields);
    gettingFields.data("previousData", {
        "defaultType": "static"
    })
    //console.log("DRAGOBECTS", gettingFields.data());
    //console.log("OBJ id", gettingFields.parents('.setObject').attr('data-object-id'), "getfields", gettingFields);    
    defaultObjectProperties(gettingFields.parents('.setObject').attr('data-object-id'));
    defaultObjectProperties(elements.attr('data-object-id'));

    
}


//Roni pinili
//Disabling Field in general properties
// enabled or disabled param
function disableEnableFieldGeneralProperties(turn, obj_type) {

    var change_field_font_family = $('.change_field_font_family');
    var change_field_font_size = $('.change_field_font_size');
    var change_field_text_alignment = $('.change_field_text_alignment');
    var change_field_font_weight = $('.change_field_font_weight');
    var change_field_font_style = $('.change_field_font_style');
    var change_field_text_decoration = $('.change_field_text_decoration');    
    var change_field_color = $('.change_field_color'); 
    switch(turn){

        case 'enabled':
        
        change_field_font_size
        .add(change_field_font_family)
        .add(change_field_text_alignment)
        .add(change_field_font_weight)
        .add(change_field_font_style)
        .add(change_field_text_decoration)
        .css('opacity','').prop('disabled', false);
  


        change_field_color.spectrum('enable');

        break;

        case 'disabled':

        change_field_font_size
        .add(change_field_font_family)
        .add(change_field_text_alignment)
        .add(change_field_font_weight)
        .add(change_field_font_style)
        .add(change_field_text_decoration)
        .css('opacity','0.5').prop('disabled', true);

        change_field_color.spectrum('disable');
        change_field_color.spectrum('set', '');
        
        if (obj_type == "attachment_on_request" || obj_type == "multiple_attachment_on_request") {
           $(change_field_font_family).css('opacity','').prop('disabled', false);
        }
        break;
    }

}   


function setFormProperties() {
    var json = $("body").data();
    $("body").on("keyup", "#workspace_title", function () {
        json.workspace_title = $(this).val().trim();
    });
    $("body").on("keyup", "#workspace_aliasName", function () {
        json.workspace_aliasName = $(this).val().trim();
    });
    $("body").on("keyup", "#button_name", function () {
        json.workspace_buttonName = $(this).val().trim();
    });
    $("body").on("keyup", "#workspace_displayName", function () {
        json.workspace_displayName = $(this).val().trim();
    });
    $("body").on("keyup", "#workspace_prefix", function () {
        json.workspace_prefix = $(this).val().trim();
    });
    $("body").on("change", "#workspace_type", function () {
        json.workspace_type = $(this).val();
    });
    $("body").on("change", "#form_category", function () {
        json.form_category = $(this).val();
    });
}

/*search picklist*/
//remove picklist functionality move to workspace_formbuilder_common 10122015 0830AM

MiddlewareTagging = {
    "selectMiddleware": function (obj) {
        // $("body").on("click",'[data-properties-type="defaultValueType"]',function(){
        var value = $(obj).val();
        if (value == "middleware") {
            $(".middlewareTaggingContainer").css("display", "");
        } else {
            $(".middlewareTaggingContainer").css("display", "none");
        }
        // })
    }
}

var SessionExpire = {
    "run":function(){
        var MeClass = this;
        jDialog(MeClass.sessionExpLogin(), "", "800", "", "", function () {
        });
        MeClass.sessionDialogFunctionality();
    },
    "sessionExpLogin":function() {
        var ret =
                '<div style="float:left;width:50%;">' +
                '<h3 style="width: 415px;" class="pull-left">' +
                '<i class="icon-asterisk"></i> ' + 'Your session has expired, please enter your email and password.<br/>NOTE: After relogging in, proceed to save the changes on your form.' +
                '</h3>' +
                '</div>' +
                '<div style="float:right;width:50%;text-align:right;margin-top:-5px;">' + '<input type="button" class="btn-basicBtn fl-margin-right fl-margin-bottom" id="sessionLoginButton" value="Cancel" >' +
                '</div>' +
                '<div class="hr"></div>' +
                '<div style="height: 285px;overflow:hidden;">' +
                    '<iframe id="session-login-expire" src="/login?session_expire=true" style="overflow:hidden;border:none;outline:none;margin:0px;padding:0px;width: 100%;height: 100%;"></iframe>' +
                    '<div class="session-processing-display" style="height:100%;width:100%;display:table;text-align: center;">'+
                        '<h1 style="font-size: 50px;vertical-align: middle;display: table-cell;">Processing...</h1>'+
                    '</div>'+
                '</div>';
        return ret;
    },
    "sessionDialogFunctionality":function(){
        var expire_login_iframe = $("#session-login-expire");
        var popup_overlay_var = $("#popup_overlay");
        var submit_overlay_var = $(".submit-overlay");
        var popup_container_var = $("#popup_container");


        popup_container_var.find("#sessionLoginButton").click(function () {
            popup_container_var.remove();
            popup_overlay_var.remove();
            submit_overlay_var.remove();
        });

        //on iframe document on ready
        expire_login_iframe.on("load", function () {//parang $(document).ready(function(){})
            var iframe_window = $(this)[0].contentWindow;
            var iframe_$ = $(this)[0].contentWindow.$;
            expire_login_iframe.show();
            iframe_$('form[action="/login"]').find('input[type="submit"]').on("click",function(){
                expire_login_iframe.hide();
            });
            iframe_$('a[href="/company_registration"]').remove();
            var path_url = iframe_window.location.pathname; //KUNIN UNG URL
            if (path_url.indexOf("/login") < 0) {
                setTimeout(function(){
                    popup_overlay_var.remove();
                    popup_container_var.remove();
                },0);
            }
        });
    }
}

UpdateMentionData = {
    init: function () {
        var formulaJsonUpdated = [];
        var returnFormula = [];
        var count = formulaJson.length;
        $(".setObject").each(function () {
            var fields = $(this).find(".getFields");
            if (typeof fields.attr("name") === "undefined") {
                return true;
            }
            // console.log(fields.attr("name"))
            formulaJsonUpdated.push({
                "id": count,
                "name": "@" + fields.attr("name"),
                "first_name": "",
                "last_name": "", "ws": "1"
            })
            count++;
        })
        // $.extend(returnFormula, formulaJson);
        // console.log(formulaJson)
        // $.extend(returnFormula, formulaJsonUpdated);
        // $.extend(true, returnFormula, formulaJson);
        returnFormula = formulaJsonUpdated.concat(formulaJson);
        return returnFormula;
    }
}



// var fieldname_keywords = [
//                 "@FormID", "@WorkflowId", "@ID",
//                 "@Requestor", "@CurrentUser", "@Status",
//                 "@Processor", "@LastAction", "@DateCreated",
//                 "@DateUpdated", "@CreatedBy", "@UpdatedBy",
//                 "@Unread", "@Node_ID", "@Workflow_ID",
//                 "@Mode", "@TrackNo", "@imported",
//                 "@computedFields", "fieldEnabled", "KeywordsField",
//                 "Repeater_Data", "Department_Name", "From",
//                 "To", "Range", "requestorname","Auth"
//             ];


function fr_removeDialog() {
    removeFadeout.call($('#popup_container'));
    removeFadeout.call($('#popup_overlay'));
}



function remove(arr, item) {
    for (var i = arr.length; i--; ) {
        if (arr[i] === item) {
            arr.splice(i, 1);
        }
    }
}

//names will be move to workspace formbuilder commons 10202015 0658PM

function removeEntry(arr, item) {
    for (var i = arr.length; i--; ) {
        if (arr[i] === item) {
            arr.splice(i, 1);
        }
    }
}
function collectNames(collect_this) {
    var selectors = $('.formbuilder_ws [name]');
    if ($.type(collect_this) != "undefined") {
        if ($.type(collect_this) == "string") {
            var str_fname = collect_this;
            existing_fnames[str_fname] = true;
        } else if ($.type(collect_this) == "array") {
            var str_fname = "";
            for (var ctr = 0 in collect_this) {
                if ($.type(collect_this[ctr]) == "string") {
                    str_fname = collect_this[ctr];
                    existing_fnames[str_fname] = true;
                }
            }
        }
        AsyncLoop(existing_fnames, function (key_name) {
            if ($('[name="' + key_name + '"]').length <= 0) {
                existing_fnames[key_name] = false;
            }
        });
        // selectors = $(collect_this);
        // selectors.each(function(){
        // 	var obj_ele_dis = $(this)
        // 	var str_fname = obj_ele_dis.attr('name');
        // 	if( str_fname.length >= 1 && !existing_fnames[str_fname] ){

        // 	}
        // },function(){
        // 	AsyncLoop(existing_fnames,function(key_name){
        // 		if($('[name="'+key_name+'"]').length <= 0 ){
        // 			existing_fnames[key_name] = false;
        // 		}
        // 	});
        // });
    } else {
        AsyncLoop(selectors, function () {
            var obj_ele_dis = $(this)
            var str_fname = obj_ele_dis.attr('name');
            if (str_fname.length >= 1 && !existing_fnames[str_fname]) {
                existing_fnames[str_fname] = true;
            }
        }, function () {
            AsyncLoop(existing_fnames, function (key_name) {
                if ($('[name="' + key_name + '"]').length <= 0) {
                    existing_fnames[key_name] = false;
                }
            });
        });
    }

}
;




function specificSizeForm(){
            $('.form_size_width').css('opacity', '0.5');
            $('.form_size_width').attr('readonly', 'readonly');
            $('.form_size_height').css('opacity', '0.5');
            $('.form_size_height').attr('readonly', 'readonly');
            $(".form_size_width").val("");
            $(".form_size_height").val("");
            $('.fl-custom-size').css('opacity', '0.5');

}

function disableFormSize(){
    $('.form_size').css('opacity', '0.5');
    $('.form_size').attr('disabled', 'disabled');
}

function enableFormSize(){
    $('.form_size').css('opacity', '');
    $('.form_size').removeAttr('disabled');
}


function disableFormsetsizeWidthConfirm(){

    var conf = "You cannot set width for portal. only height can be set.";
    var newConfirm = new jConfirm(conf, 'Confirmation Dialog', '', '', '', function (r) {
        //$('.form_size_width').attr('readonly', 'readonly');
       // $('.form_size_width').css({'opacity': '0.5'})
    });

    newConfirm.themeConfirm("confirm2", {
        'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
    });
    $('#popup_cancel').remove();
    $('#popup_ok').val('OK');

}

//moved to global.js suggestionTableModal function 0728 10202015




function embedViewPicklistItemUi() {

    var iframe_ele_dialog = $("iframe").eq(0);
    iframe_ele_dialog.load(function () {
        var iframe_document = $(this)[0].contentWindow.$('html');
        iframe_document.find('body').addClass('add-picklist-iframe');
    });

}

FormUserAccess = {
    removeTempCheckedUncheckedUsers: function () {
        var json = $("body").data();
        var usersKey = ['form-authors', 'form-readers', 'form-admin', 'form-users', 'customized-print', 'delete-access']
        var jsonChecked = "";
        var jsonUnchecked = "";

        for (var i in usersKey) {
            jsonChecked = usersKey[i] + "_checked";
            jsonUnchecked = usersKey[i] + "_unChecked";

            json['' + jsonChecked + ''] = {};
            json['' + jsonUnchecked + ''] = {};
        }
    },
    udpdateOriginalUsersState: function () {
        var json = $("body").data();
        var usersArray = [
            {
                "container": ".form-user-accessor_original",
                "json_key": "form-users"
            },
            {
                "container": ".form-user-authors_original",
                "json_key": "form-authors"
            },
            {
                "container": ".form-user-viewers_original",
                "json_key": "form-readers"
            },
            {
                "container": ".form-custom-print-users_original",
                "json_key": "customized-print"
            },
            {
                "container": ".form-delete-access-json_original",
                "json_key": "delete-access"
            },
            {
                "container": ".form-user-admin_original",
                "json_key": "form-admin"
            }
        ];
        var container = "";
        var json_key = "";
        var json_value;
        for (var i in usersArray) {
            container = usersArray[i]['container'];
            json_key = usersArray[i]['json_key'];
            json_value = json['' + json_key + ''];
            if (typeof json_value == "object") {
                json_value = JSON.stringify(json_value);
            }
            // console.log(json_value)
            $(container).text(json_value);
        }

    }
}
var CustomScriptingToolsUI = {
    "init":function(){
        this.BindOnClearExistingFormulas();
        this.BindOnPrintExistingTools();
    },
    "BindOnClearExistingFormulas":function(){
        $('[data-object-type="clear-existing-formula"]').on('click',function(){
            var newConfirm = new jConfirm("TEST", 'Confirmation Dialog', '', '', '', function (r) {
                if (r == true) {
                    if($.type(ResetRemoveFormFormulaSettings) == "function"){
                        scripting_tools.ResetRemoveFormFormulaSettings();
                        alert("successfully remove");
                    }else{
                        alert("failed to remove");
                    }
                }
            });
            newConfirm.themeConfirm("confirm2", {
                'icon': '<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>'
            });
        });
    },

    "BindOnPrintExistingTools":function(){
        $('[data-object-type="generate-custom-script"]').on("click",function(){

            var formbuilder_workspace = $('.workspacev2');
            var formbuilder_workspace_objects = $('.workspacev2').find(".setObject");

            var ret = '<h3 class="fl-margin-bottom">';
            ret += '<i class="icon-save"></i> Print Custom Script';
            ret += '</h3>';
            ret += '<div class="hr">vcvxc</div>';
            ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
            ret += '<div class="content-dialog" style="height: auto;">';
                // your content here
                ret += '<div class="sort-field-formulas-for-computed-container fl-form-prop-cont-wid fl-floatLeft" style="height: 300px; overflow:hidden;position: relative;padding-right: 15px;">';
                    ret += '<div class="sort-field-formulas-for-computed">';
                    ret += '</div>';
                ret += '</div>';


                ret += '<div class="print-formatted-script fl-form-prop-cont-wid fl-floatRight">';
                    ret += '<textarea class="custom-print-value-script" style="height: 300px;width: 470px;resize:none;"></textarea>';
                ret += '</div>';

                ret += '<div class="fields" style="">';
                    ret += '<div class="label_basic"></div>';
                    ret += '<div class="input_position" style="margin-top:5px;text-align:center;">';
                        ret += ' <input type="button" class="btn-blueBtn fl-margin-right" id="buttonCommitSort" data-commit-sort-button="buttonCommitSort" value="Ok" style="display:table-cell;width: 47%;" />';
                        ret += ' <input type="button" class="btn-blueBtn fl-margin-right isDisplayNone" id="buttonCopy" data-copy-button="buttonCopy" value="Copy" style="display:table-cell;width: 47%;" />';
                        // ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
                    ret += '</div>';
                ret += '</div>';
            ret+= '</div>';
            var newDialog = new jDialog(ret, "", 1000, "", "", function() {   
            });
            newDialog.themeDialog("modal2");

            var append_target_for_fields = $('.sort-field-formulas-for-computed');
            var collecting_append = "";
            if(formbuilder_workspace_objects.length >= 1){
                append_target_for_fields.html("");
                AsyncLoop( formbuilder_workspace_objects , function(){
                    var self = $(this);
                    var data_type = self.attr("data-type");

                    if (
                            data_type == "labelOnly" ||
                            data_type == "createLine" ||
                            data_type == "table" ||
                            data_type == "tab-panel" ||
                            // data_type == "listNames" || inalis ko to aaron para masama yung names sa tbfields -- JEWEL 
                            data_type == "imageOnly" ||
                            data_type == "button" ||
                            data_type == "embeded-view" ||
                            data_type == "accordion" ||
                            data_type == "details-panel"
                            ) {
                        return true; //preventing to insert it to the database
                    }

                    
                    
                    var doi = self.attr("data-object-id");
                    var field_name = self.find(".getFields_"+doi).attr("name");

                    collecting_append = "";
                    collecting_append += '<div class="input_position_below section clearing fl-field-style">';
                        collecting_append += '<div class="column div_1_of_1">';
                            collecting_append += '<span fields-data-name="'+field_name+'" >'+field_name+'</span><br/>';
                        collecting_append += '</div>';
                    collecting_append += '</div>';

                    append_target_for_fields.append(collecting_append);
                },function(){
                    append_target_for_fields.sortable({
                        axis: "y",
                        sort:function(){
                        }
                    });
                });
            }
            $('[data-commit-sort-button="buttonCommitSort"]').on("click",function(){
                // var html_print = scripting_tools.HTMLOfficialPrintCustomScript2().printHtml;
                var str_print = scripting_tools.HTMLOfficialPrintCustomScript2().print;
                $('.print-formatted-script').children('textarea.custom-print-value-script').html(str_print);
            });
            $('[data-copy-button="buttonCopy"]').on("click",function(){
                scripting_tools.selectText($('.print-formatted-script').children('textarea.custom-print-value-script'));
                $('.print-formatted-script').children('textarea.custom-print-value-script').focus();
                if($.type(document.execCommand) == "function"){
                    var successfully_copied = document.execCommand('copy');
                }else{
                    var successfully_copied = false;
                }
                
                
                if(successfully_copied){
                    alert("Successfully copied to clipboard");
                }
            });
            $('.sort-field-formulas-for-computed-container').perfectScrollbar(/*{
                "scrollHeight":"100"
            }*/);
            $('.sort-field-formulas-for-computed-container').children('.ps-scrollbar-y').css('min-height','50px'); //di ko ginamit ung perfectscrollbar scrolly minheight dahil wala naman syang  settings na ganun
        });
    }

}

var FormulaListUI = {
    "init":function(){
        this.bindEvents();
    },

    "UIDialogDesign":   '<div><h3 class="fl-margin-bottom">'+
                        '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-form-events-formulalist"></use></svg> <span>Computed Formula Event List</span> '+
                        '</h3></div>'+
                        
                        '<div class="hr"></div>'+
                        '<div style="margin:2px 0px 0px 2px+font-size:11px+color:red"></div>'+
                        '<div class="content-dialog" style="height: auto+">'+
                            // your content here
                            '<div class="flist-inner-header" style="z-index: 1;background-color: #FEFEFE;position: fixed;width: 100%;box-sizing: border-box;left: 0px;top: 32px;padding: 0px;padding-bottom: 0px;">'+
                                '<div class="fl-search-wrapper fl-ui-search" style="float: right; margin-right: 15px; margin-top: 15px;">'+
                                    '<input class="flist-search-selected-field" type="text" placeholder="Search Selected Field" style="padding-right: 90px;">'+
                                    '<ul class="fl-ui-search-button">'+
                                        '<li class="flist-search-previous-selected-field"><i class="fa fa-caret-up"></i></li>'+
                                        '<li class="flist-search-next-selected-field"><i class="fa fa-caret-down"></i></li>'+
                                        '<li class="flist-cancel-search" style="float: left;"><i class="fa fa-times"></i></li>'+
                                    '</ul>'+
                                '</div>'+
                            '</div>'+

                            '<div class="flist-container" style="">'+
                            '</div>'+

                            '<div class="fields" style="">'+
                                '<div class="label_basic"></div>'+
                                '<div class="input_position" style="margin-top:5px+">'+
                                    // ' <input type="button" class="btn-blueBtn fl-margin-right" id="flist-clear" value="Clear">'+
                                    ' <input type="button" class="btn-blueBtn fl-margin-right loading-data-please-wait" id="flist-commit" data-temp-disable value="OK" />'+
                                    ' <input type="button" class="btn-blueBtn fl-margin-right" id="flist-footer-clear" data-temp-disable value="Clear" />'+
                                    ' <input type="button" class="btn-blueBtn fl-margin-right" id="flist-footer-get-exist" data-temp-disable value="Get existing formula" style="width: 150px;"/>'+
                                    ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close"/>'+
                                '</div>'+
                            '</div>'+
                        '</div>',

    "FieldTemplate1AppendTo":function(container_here, call_back){
        var class_self = this;
        var design_template = $(
            '<div class="flist-entry-container fields_below">'+
                '<div style="float:left;margin-left:5px">'+
                    '<span class="flist-field-entry-handle"><i class="fa fa-arrows" style="cursor:move;font-size: 15px;"></i></span>'+
                '</div>'+
                '<div style="float:right;margin-left:5px">'+
                    '<span class="flist-add-field-entry"><i class="icon-plus-sign fa fa-plus cursor"></i></span>'+
                    '<span class="flist-minus-field-entry isDisplayNone"><i class="icon-minus-sign fa fa-minus cursor" style="margin-left:5px"></i></span>'+
                '</div>'+
                '<div class="input_position_below section clearing fl-field-style">'+
                    '<div class="column div_1_of_3">'+
                        '<select class="flist-field-selection form-select"><option value="-1">--Select--</option></select>'+
                        '<textarea placeholder="Description (optional)" class="flist-item-description form-textarea" style="resize: vertical;"></textarea>'+
                        // '<input type="text" class="flist-item-description form-text">'+
                        '<input class="flist-get-exist-formula fl-positive-btn btn-blueBtn" type="button" value="Get computed formula" style="width:145px;margin-top:7px;margin-right: 0px;"/>'+
                        '<label class="tip isDisplayNone" data-original-title="Commit" style="float:right;margin-top:10px;">'+
                            '<input class="flist-use-exist-formula css-checkbox" type="checkbox" name="flist-use-exist-formula" value="Use existing formula" id="flist_use_exist_formula" data-original-id="flist_use_exist_formula">'+
                            '<label for="flist_use_exist_formula" class="css-label"></label>'+
                            '<span>Use existing formula</span>'+
                        '</label>'+
                        '<label class="tip isDisplayNone" data-original-title="Choose Formula Method Type" style="float:right;margin-top:10px;">'+
                            '<select style="width:130px;">'+
                                '<option value="computed_value">Computed Value</option>'+
                                '<option value="change_list">Change List</option>'+
                            '</select>'+
                        '</label>'+
                        // '<label><input class="flist-use-exist-formula" type="checkbox" value="1"/> Use existing formula</label>'+
                    '</div>'+
                    '<div class="column div_2_of_3">'+
                        // '<textarea class="flist-field-equivalent-formula form-textarea" style="margin-top:0px;resize:vertical;height:70px;-webkit-transition-duration: 0s;-webkit-transition-duration: 0.5s;-moz-transition-duration: 0.5s;-o-transition-duration: 0.5s;transition-duration: 0.5s;"></textarea>'+
                        '<div class="flist-tab-control-formulas" style="background-color:rgba(0,0,0,0); padding: 0px;">'+
                            '<ul style=" height: 24px; overflow: hidden;">'+
                                '<li><a nav-selector-id="#flist-tabs-1" href="#flist-tabs-1">Computed Value</a></li>'+
                                '<li><a nav-selector-id="#flist-tabs-2" href="#flist-tabs-2">Change List</a></li>'+
                                '<li><a nav-selector-id="#flist-tabs-3" href="#flist-tabs-3">Placeholder</a></li>'+
                                '<li><a nav-selector-id="#flist-tabs-4" href="#flist-tabs-4">Label</a></li>'+
                                // '<li><a nav-selector-id="#flist-tabs-3" href="#flist-tabs-3">Readonly</a></li>'+
                                // '<li><a nav-selector-id="#flist-tabs-4" href="#flist-tabs-4">Input Validation</a></li>'+
                                // '<li><a nav-selector-id="#flist-tabs-2" href="#flist-tabs-2">Visibility</a></li>'+
                            '</ul>'+
                            '<div content-selector-id="flist-tabs-1" id="flist-tabs-1" style="background-color:rgba(0,0,0,0); padding: 0px;">'+
                                '<textarea data-ide-properties-type="flist-field-equivalent-formula" class="flist-field-equivalent-formula form-textarea" style="border: none;margin-top:0px;resize:vertical;height:70px;-webkit-transition-duration: 0s;-webkit-transition-duration: 0.5s;-moz-transition-duration: 0.5s;-o-transition-duration: 0.5s;transition-duration: 0.5s;"></textarea>'+
                            '</div>'+
                            '<div content-selector-id="flist-tabs-2" id="flist-tabs-2" style="background-color:rgba(0,0,0,0); padding: 0px;">'+
                                '<textarea data-ide-properties-type="flist-field-equivalent-formula-changelist" class="flist-field-equivalent-formula-changelist form-textarea" style="border: none;margin-top:0px;resize:vertical;height:70px;-webkit-transition-duration: 0s;-webkit-transition-duration: 0.5s;-moz-transition-duration: 0.5s;-o-transition-duration: 0.5s;transition-duration: 0.5s;"></textarea>'+
                            '</div>'+
                            '<div content-selector-id="flist-tabs-3" id="flist-tabs-3" style="background-color:rgba(0,0,0,0); padding: 0px;">'+
                                '<textarea data-ide-properties-type="flist-field-equivalent-formula-placeholder" class="flist-field-equivalent-formula-placeholder form-textarea" style="border: none;margin-top:0px;resize:vertical;height:70px;-webkit-transition-duration: 0s;-webkit-transition-duration: 0.5s;-moz-transition-duration: 0.5s;-o-transition-duration: 0.5s;transition-duration: 0.5s;"></textarea>'+
                            '</div>'+
                            '<div content-selector-id="flist-tabs-4" id="flist-tabs-4" style="background-color:rgba(0,0,0,0); padding: 0px;">'+
                                '<textarea data-ide-properties-type="flist-field-equivalent-formula-label" class="flist-field-equivalent-formula-label form-textarea" style="border: none;margin-top:0px;resize:vertical;height:70px;-webkit-transition-duration: 0s;-webkit-transition-duration: 0.5s;-moz-transition-duration: 0.5s;-o-transition-duration: 0.5s;transition-duration: 0.5s;"></textarea>'+
                            '</div>'+
                            // '<div content-selector-id="flist-tabs-3" id="flist-tabs-3">'+
                            //     'TESTC'+
                            // '</div>'+
                            // '<div content-selector-id="flist-tabs-4" id="flist-tabs-4">'+
                            //     'TESTD'+
                            // '</div>'+    
                        '</div>'+
                        
                    '</div>'+
                '</div>'+
            '</div>'
        );

        container_here.append(design_template);
        var temp_collector = {};
        class_self.dialog_selector['flist-field-selection'] = design_template.find('.flist-field-selection');
        if(class_self.existing_fields.length >= 1){
            var cs_ef = [];
            cs_ef = cs_ef.concat(class_self.existing_fields);
            $('.setObject[data-type="labelOnly"],.setObject[data-type="tab-panel"],.setObject[data-type="accordion"],.setObject[data-type="table"],.setObject[data-type="embeded-view"],.setObject[data-type="createLine"]').each(function(){
                var inner2self = $(this);
                var doi = inner2self.attr('data-object-id');
                var label_element = inner2self.find('[id="lbl_'+doi+'"]:eq(0)');
                cs_ef.push({
                    "data_field_default_type": "",
                    "data_field_formula": "",
                    "data_field_formula_computed": "",
                    "data_field_formula_middleware": "",
                    "data_field_formula_static": "",
                    "data_field_input_type": "",
                    "data_field_label": label_element.text(),
                    "data_field_name": label_element.attr("name")||"",
                    "data_field_visibility_formula": "",
                    "data_multiple_values": "[]",
                    "data_object_id": inner2self.attr("data-object-id"),
                    "field_type": inner2self.attr("data-type"),
                    "index": cs_ef.length,
                    "other_attributes": "",
                    "readonly": "",
                    "refresh_fields_onchange_attr": ""
                });
            });
            AsyncLoop(cs_ef,function(a,b){
                temp_collector['option_for_field'] = (temp_collector['option_for_field']||'') + '<option data-obj-id="'+b['data_object_id']+'" value="'+(b['data_field_name']||""/*b['data_object_id']*/)+'">'+(b['data_field_name']/*||b["field_type"]+" id: "+b['data_object_id']*/)+' ( ' + $.trim(b['data_field_label']) + ' ) </option>';
            },function(){
                class_self.dialog_selector['flist-field-selection'].append(temp_collector["option_for_field"]);
                if($.type(call_back) == "function"){
                    call_back();
                }
            });
        }else{
            if($.type(call_back) == "function"){
                call_back();
            }
        }

        
        return "";
    },
    "getExisitngFields":function(){
        var fields_data = scripting_tools.getFieldsData()||[];
        fields_data.sort(function(a,b){
            return (a["data_field_name"]||"").localeCompare(b["data_field_name"]||"");
        });
        return fields_data;
    },
    "existing_fields":[],
    "dialog_selector":{},

    "dialog_counter":1,
    "bindEvents":function(){
        var class_self = this;
        $('[btn-formula-event="formula-list-dialog"]').on('click',function(){
            var ret = class_self.UIDialogDesign;
            class_self.existing_fields = class_self.getExisitngFields()||[];



            var newDialog = new jDialog(ret, '', '900', '10000', '', function() {   
            });
            newDialog.themeDialog('modal2');
            //onload of the dialog

            $('.content-dialog-scroll.ps-container').css({
                "max-height": "365px",
                "min-height":"175px",
                "margin-top": "90px"
            }).children(".ps-scrollbar-y,.ps-scrollbar-x").css("z-index",1);

            class_self.dialog_selector = {
                'flist-commit':$('[id="flist-commit"]'),
                'flist-get-exist-formula':$('.flist-get-exist-formula'),
                'flist-container':$('.flist-container'),
                'flist-inner-header':$('.flist-inner-header'),
                'flist-execute-action':$('[id="flist-execute-action"]'),
                'flist-tab-control-formulas':$('.flist-tab-control-formulas'),
                'flist-footer-clear':$('[id="flist-footer-clear"]'),
                'flist-footer-get-exist':$('[id="flist-footer-get-exist"]'),
                //search field
                'flist-search-previous-selected-field':$('.flist-search-previous-selected-field:eq(0)'),
                'flist-search-next-selected-field':$('.flist-search-next-selected-field:eq(0)'),
                'flist-search-selected-field':$('.flist-search-selected-field:eq(0)'),
                'flist-cancel-search':$('.flist-cancel-search:eq(0)'),
            };

            class_self.dialog_selector['flist-inner-header'].append($(
                '<div class="input_position_below section clearing fl-field-style" style="border: 0px; padding-top: 5px; padding-left: 10px; display: none;">'+
                    '<div class="column div_1_of_3">'+
                        '<select class="form-select flist-select-action" id="flist-select-action" style="width: auto;">'+
                            '<option value="-1">Select action:</option>'+
                            '<option value="1">Clear</option>'+
                            '<option value="2">Get all existing</option>'+
                        '</select>'+
                        '<input type="button" class="btn-blueBtn fl-margin-right" id="flist-execute-action" value="Go"/>'+
                    '</div>'+
                '</div>'+

                '<div class="section clearing" style="padding-left: 20px;padding-right: 20px;margin-top: 10px;">'+
                    '<div class="column div_1_of_3" style=" border-bottom: 2px solid black; font-size: 18px; width: 32%;"><label>Select Field</label></div>'+
                    '<div class="column div_2_of_3" style=" border-bottom: 2px solid black; font-size: 18px; margin-left: 20px; width: 65.5%;"><label>Formula</label></div>'+
                '</div>'
            ));
            class_self.dialog_selector['flist-select-action'] = $('.flist-select-action');
            class_self.dialog_selector['flist-execute-action'] = $('[id="flist-execute-action"]');
            class_self.dialog_selector['flist-commit'] = $('[id="flist-commit"]');


            class_self.dialog_selector['flist-container'].sortable({
                handle: ".flist-field-entry-handle",
                axis:'y'
            });

            {//DIALOG BUTTON EVENT ACTIONS   content-dialog
                
                // class_self.dialog_selector['flist-tab-control-formulas'].tabs();
                //DITO NA AKO
                {//search field
                    var json_params = {
                        "searchbox":class_self.dialog_selector['flist-search-selected-field'],
                        "previousbtn":class_self.dialog_selector['flist-search-previous-selected-field'],
                        "nextbtn":class_self.dialog_selector['flist-search-next-selected-field'],
                        "cancelbtn":class_self.dialog_selector['flist-cancel-search'],
                        "parentContainer":class_self.dialog_selector['flist-container'].parents('.content-dialog-scroll:eq(0)'),
                        "targetEleContainer":class_self.dialog_selector['flist-container'].find(".flist-entry-container"), //$(".flist-entry-container"),
                        "defaultState":function(){
                            // $(".flist-entry-container").css("border",false)
                            $(".target-searched-np").removeClass('formulaListSearchActive');
                            // $(".target-searched-np").find(".nav-title").css("color","#6e6e6e");
                            // $(".target-searched-np").removeClass("target-searched-np");
                        },
                        "target_ele":[{
                            "parent":".flist-entry-container",
                            "target_text":$(".flist-field-selection"),
                            "target":".fl-field-style",
                            "type":"value",
                            "selectedState":function(target_ele_parent_obj){
                                target_ele_parent_obj.find(".fl-field-style").addClass('formulaListSearchActive')/*.css("background-color","#CCC")*/
                                // var bg_color = $(".fl-user-header-wrapper").css("background-color");
                                // target_ele_parent_obj.find(".original_data").css("background-color",bg_color);
                                // target_ele_parent_obj.find(".nav-title").css("color","#FFF");
                                
                            },
                            "unselectedState":function(target_ele_parent_obj){
                                // target_ele_parent_obj.css("border",false)
                                target_ele_parent_obj.find(".fl-field-style").removeClass('formulaListSearchActive')/*.css("background-color","#EFEFEF")*/
                                // target_ele_parent_obj.find(".nav-title").css("color","#6e6e6e");
                            },
                        }]
                    }
                    SearchNextPrevious.init(json_params);
                }


                // class_self.dialog_selector['flist-search-previous-selected-field'].on('click',function(){
                //     // var value_search = class_self.dialog_selector['flist-search-selected-field'].val();
                //     // var scroll_container = class_self.dialog_selector['flist-container'].parents(".ps-container:eq(0)");
                //     // var flist_container_entry_data = class_self.dialog_selector['flist-container'].children('.flist-entry-container').map(function(){
                //     //     var self = $(this);
                //     //     var collector = {
                //     //         'selected-field':self.children('.input_position_below:eq(0)').children(".column div_1_of_3:eq(0)").children('.flist-field-selection:eq(0)').val(),
                //     //         'flist-container':self,
                //     //         'top':self.position().top
                //     //     };
                //     //     return collector;
                //     // });
                //     // scroll_container.scrollTop( $('.flist-entry-container').eq(2).position().top )
                // });

                // class_self.dialog_selector['flist-search-next-selected-field'].on('click',function(){
                //     var value_search = class_self.dialog_selector['flist-search-selected-field'].val();
                //     var flist_container_entry_data = class_self.dialog_selector['flist-container'].children('.flist-entry-container').map(function(){
                //         var self = $(this);
                //         var collector = {
                //             '':''
                //         };
                //         return collector;
                //     });
                // });

                class_self.dialog_selector['flist-footer-clear'].on('click',function(){
                    class_self.dialog_selector['flist-select-action'].val("1");
                    class_self.dialog_selector['flist-execute-action'].trigger("click");
                });
                class_self.dialog_selector['flist-footer-get-exist'].on('click',function(){
                    class_self.dialog_selector['flist-select-action'].val("2");
                    class_self.dialog_selector['flist-execute-action'].trigger("click");
                });
                //fixing the dropdown
                class_self.dialog_selector['flist-commit'].on('click',function(){
                    var self = $(this);
                    if(self.is('.loading-data-please-wait')){
                        showNotification({
                            message: "Please wait to load all your settings.",
                            type: "information",
                            autoClose: true,
                            duration: 3
                        });
                        return;
                    }
                    
                    
                    // var newConfirm = new jConfirm("All fields with computed value will be switched to static.<br/>Proceed anyway?...", 'Confirmation Dialog', '', '', '', function (r) {
                    //     if (r == true) {
                            
                            var all_computed_fields = $('[default-type="computed"][default-formula-value]:not([default-formula-value=""])').length;
                            var r = false;
                            if(all_computed_fields >= 1){
                                r = confirm("All fields with computed value will be switched to static.\nProceed anyway?...");
                            }else{
                                r = true;
                            }
                            
                            if(r == true){
                                var collect_data = [];
                                var flist_entry_container = class_self.dialog_selector['flist-container'].children('.flist-entry-container');
                                
                                AsyncLoop(flist_entry_container,function(a,b){
                                    var selfz = $(this);
                                    var div_1_of_3 = selfz.find('>div>.div_1_of_3');
                                    var user_input = {
                                        "data_object_id":div_1_of_3.children(".flist-field-selection").children(":selected").attr("data-obj-id"),
                                        "field_name":div_1_of_3.children(".flist-field-selection").val(),
                                        "formula_item_list_description":div_1_of_3.children(".flist-item-description").val(),
                                        "use_exist":div_1_of_3.children("label").children(".flist-use-exist-formula").is(":checked"),
                                        "eq_formula":selfz.find('>div>.div_2_of_3>.flist-tab-control-formulas>[content-selector-id="flist-tabs-1"]>.flist-field-equivalent-formula').val()||"",
                                        "existing_formula":"",
                                        "change_list_formula": $.trim(selfz.find('>div>.div_2_of_3>.flist-tab-control-formulas>[content-selector-id="flist-tabs-2"]>.flist-field-equivalent-formula-changelist').val())||"",
                                        "placeholder_formula": $.trim(selfz.find('>div>.div_2_of_3>.flist-tab-control-formulas>[content-selector-id="flist-tabs-3"]>.flist-field-equivalent-formula-placeholder').val())||"",
                                        "label_formula": $.trim(selfz.find('>div>.div_2_of_3>.flist-tab-control-formulas>[content-selector-id="flist-tabs-4"]>.flist-field-equivalent-formula-label').val())||""
                                    };

                                    if(user_input["use_exist"]){
                                        user_input["existing_formula"] = $('[name="'+user_input['field_name']+'"]').attr("default-formula-value")||"";
                                    }else{
                                        user_input["existing_formula"] = "";
                                    }
                                    if(user_input['field_name'] != '-1' && ( user_input['use_exist'] || (user_input['eq_formula'] || user_input['change_list_formula'] || user_input['placeholder_formula'] || user_input['label_formula']) ) ){
                                        collect_data.push(user_input);
                                    }
                                },function(){
                                    $('body').data("ComputedFormulaEventList",collect_data);
                                    scripting_tools.ResetRemoveFormFormulaSettings();
                                    // showNotification({
                                    //     message: "Formula list Successfully saved.",
                                    //     type: "success",
                                    //     autoClose: true,
                                    //     duration: 3
                                    // });
                                    $('.fl-closeDialog').trigger("click");
                                });
                            }
                            
                    //     }
                    // });
                    // newConfirm.themeConfirm("confirm2", {
                    //     'icon': '<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>'
                    // });
                    
                });
                
                //added by japhet
                //add event for field selection
                // console.log(class_self.dialog_selector);
                {//comment ko muuna to japs
                    // $('body').on('change', '.flist-field-selection', function(e){
                    //     var data_obj_id = $(this).children('option[value="' + $(this).val() + '"]').attr('data-obj-id');
                    //     console.log("#setObject_" + data_obj_id);
                    //     var getElement = document.getElementById("setObject_" + data_obj_id);
                    //     console.log("data_obj_id", data_obj_id);
                    //     if(getElement.getAttribute('data-type') == 'dropdown' || getElement.getAttribute('data-type') == 'selectMany'){
                    //         console.log("class_self", this.parentNode.parentNode.getElementsByClassName('ui-tabs-nav')[0].style);
                    //         var tab_3 = this.parentNode.parentNode.querySelectorAll("[nav-selector-id='#flist-tabs-3']")[0].parentNode;
                    //         console.log("elements #3", tab_3);
                    //         tab_3.style.display = 'none';
                    //         var id = tab_3.childNodes[0].getAttribute('nav-selector-id');
                    //         id = id.replace(/\#/g, '');
                    //         console.log("parentNode", this.parentNode.parentNode);
                    //         var tab_3_textarea = this.parentNode.parentNode.querySelectorAll('[id="' + id + '"]')[0];
                    //         tab_3_textarea.value = '';
                    //         var tab_2 = this.parentNode.parentNode.querySelectorAll("[nav-selector-id='#flist-tabs-2']")[0].parentNode;
                    //         console.log("elements #3", tab_2);
                    //         tab_2.style.display = 'block';
                    //         tab_2.childNodes[0].click();
                    //     }
                    //     else{
                    //         var tab_2 = this.parentNode.parentNode.querySelectorAll("[nav-selector-id='#flist-tabs-2']")[0].parentNode;
                    //         tab_2.style.display = 'none';
                    //         var id = tab_2.childNodes[0].getAttribute('nav-selector-id');
                    //         id = id.replace(/\#/g, '');
                    //         var tab_2_textarea = this.parentNode.parentNode.querySelectorAll('[id="' + id + '"]')[0];
                    //         tab_2_textarea.value = '';
                    //         var tab_3 = this.parentNode.parentNode.querySelectorAll("[nav-selector-id='#flist-tabs-3']")[0].parentNode;
                    //         tab_3.style.display = 'block';
                    //         // tab_3.childNodes[0].click();
                    //         var tab_1 = this.parentNode.parentNode.querySelectorAll('[nav-selector-id="#flist-tabs-1"]')[0].parentNode;
                    //         tab_1.style.display = 'block';
                    //         tab_1.childNodes[0].click();
                    //     }
                    // });
                }

                class_self.dialog_selector['flist-execute-action'].on('click',function(){
                    var self = $(this);
                    var selected_action = self.prev().val();
                    var all_field_entry_container_selector = class_self.dialog_selector['flist-container'].children('.flist-entry-container');
                    if(selected_action == "1"){
                        all_field_entry_container_selector.not(':eq(0)').remove();
                        all_field_entry_container_selector.eq(0).find('>div>.flist-minus-field-entry').hide();
                        all_field_entry_container_selector.eq(0).find('>div>.div_1_of_3>.flist-field-selection').val("-1").next().val("");
                        all_field_entry_container_selector.eq(0).find('>div>.div_1_of_3>label>.flist-use-exist-formula').prop("checked",false);
                        all_field_entry_container_selector.eq(0).find('.flist-field-equivalent-formula').val("");
                        $('.content-dialog-scroll.ps-container').perfectScrollbar("update"); //FS#8058
                    }else if(selected_action == "2"){
                        if(class_self.existing_fields.length >= 1){
                            var ii_temp = class_self.existing_fields;
                            var ii_temp2 = "";
                            var existing_setup = class_self.dialog_selector['flist-container'].find('>.flist-entry-container').map(function(){
                                var tdata = {};
                                var inself = $(this);
                                tdata["flist-field-selection-ele"] = inself.find(".flist-field-selection:eq(0)");
                                tdata["flist-field-selection"] = tdata["flist-field-selection-ele"].val();
                                tdata["flist-field-equivalent-formula-ele"] = inself.find(".flist-field-equivalent-formula:eq(0)");
                                tdata["flist-field-equivalent-formula"] = tdata["flist-field-equivalent-formula-ele"].val();
                                return tdata;
                            }).get();
                            AsyncLoop(ii_temp,function(a,b){
                                if(b['data_field_default_type'] == "computed"){
                                    ii_temp2 = $.trim(JSON.parse(b['data_field_formula_computed']));
                                    for(var ctr in existing_setup){
                                        if( ii_temp2 && (existing_setup[ctr]["flist-field-selection"] == b['data_field_name'] || existing_setup[ctr]["flist-field-selection"] == "-1") ){
                                            existing_setup[ctr]["flist-field-selection"] = b['data_field_name'];
                                            existing_setup[ctr]["flist-field-selection-ele"].val(b['data_field_name']);
                                            existing_setup[ctr]["flist-field-equivalent-formula-ele"].val(ii_temp2);
                                            existing_setup[ctr]["flist-field-equivalent-formula"] = ii_temp2;
                                            return true;
                                        }
                                    }
                                    if(ii_temp2){
                                        class_self.dialog_selector['flist-container'].find('>.flist-entry-container:last>div>.flist-add-field-entry:eq(0)').trigger("click",[{
                                            "field_name":b['data_field_name'],
                                            "equivalent_formula":ii_temp2
                                        }]);
                                    }
                                }
                            });
                        }
                    }
                });
            }
            class_self.FieldTemplate1AppendTo(class_self.dialog_selector['flist-container'],function(){
                class_self.dialog_selector['flist-tab-control-formulas'] = $('.flist-tab-control-formulas');
                class_self.dialog_selector['flist-tab-control-formulas'].tabs(); //onload

                class_self.dialog_selector['flist-get-exist-formula'] = $('.flist-get-exist-formula');
                class_self.dialog_selector['flist-get-exist-formula'].on('click',function(){
                    var self = $(this);
                    var select_field_element = self.prev();
                    var textarea_field_element = self.parent().next().children().children().children('.flist-field-equivalent-formula');
                    var sfe_value = select_field_element.val();
                    console.log(textarea_field_element)
                    if(sfe_value != "-1"){
                        textarea_field_element.val($('[name="'+sfe_value+'"]').attr('default-formula-value')||"");
                    }
                });

                //added by japhet
                //add event for field selection
                // console.log(class_self.dialog_selector);
                $('.flist-field-selection').on('change', function(e){
                    var self = $(this);
                    var data_obj_id = $(this).children('option[value="' + $(this).val() + '"]').attr('data-obj-id');
                    // console.log("#setObject_" + data_obj_id);
                    var getElement = document.getElementById("setObject_" + data_obj_id);
                    var jq_getElement = $(getElement);
                    var tab_container = self.parent().next();
                    var all_tab_nav = tab_container.find('[role="tablist"]:eq(0)').children('li');
                    var selector = { "nav":"" , "content":""};
                    if($(this).val() == "-1"){
                        selector = {
                            "nav":'a[href][nav-selector-id="#flist-tabs-1"],a[href][nav-selector-id="#flist-tabs-2"],a[href][nav-selector-id="#flist-tabs-3"],a[href][nav-selector-id="#flist-tabs-4"]',
                            "content":'[content-selector-id="flist-tabs-1"],[content-selector-id="flist-tabs-2"],[content-selector-id="flist-tabs-3"],[content-selector-id="flist-tabs-4"]'
                        };
                    }
                    else if(jq_getElement.is('[data-type="dropdown"],[data-type="selectMany"]') ){ //japs old: getElement.getAttribute('data-type') == 'dropdown' || getElement.getAttribute('data-type') == 'selectMany'
                        selector = {
                            "nav":'a[href][nav-selector-id="#flist-tabs-1"],a[href][nav-selector-id="#flist-tabs-2"],a[href][nav-selector-id="#flist-tabs-4"]',
                            "content":'[content-selector-id="flist-tabs-1"],[content-selector-id="flist-tabs-2"],[content-selector-id="flist-tabs-4"]'
                        };
                        {//japs old
                            // // console.log("class_self", this.parentNode.parentNode.getElementsByClassName('ui-tabs-nav')[0].style);
                            // var tab_3 = this.parentNode.parentNode.querySelectorAll("[nav-selector-id='#flist-tabs-3']")[0].parentNode;
                            // console.log("elements #3", tab_3);
                            // tab_3.style.display = 'none';
                            // var id = tab_3.childNodes[0].getAttribute('nav-selector-id');
                            // id = id.replace(/\#/g, '');
                            // console.log("parentNode", this.parentNode.parentNode);
                            // var tab_3_textarea = this.parentNode.parentNode.querySelectorAll('[id="' + id + '"]')[0];
                            // tab_3_textarea.value = '';
                            // var tab_2 = this.parentNode.parentNode.querySelectorAll("[nav-selector-id='#flist-tabs-2']")[0].parentNode;
                            // console.log("elements #3", tab_2);
                            // tab_2.style.display = 'block';
                            // tab_2.childNodes[0].click();
                        }
                    }
                    else if(jq_getElement.is('[data-type="labelOnly"],[data-type="accordion"],[data-type="table"],[data-type="embeded-view"],[data-type="createLine"]')){
                        selector = {
                            "nav":'a[href][nav-selector-id="#flist-tabs-4"]',
                            "content":'[content-selector-id="flist-tabs-4"]'
                        };
                    }
                    else{
                        selector = {
                            "nav":'a[href][nav-selector-id="#flist-tabs-1"],a[href][nav-selector-id="#flist-tabs-3"],a[href][nav-selector-id="#flist-tabs-4"]',
                            "content":'[content-selector-id="flist-tabs-1"],[content-selector-id="flist-tabs-3"],[content-selector-id="flist-tabs-4"]'
                        };
                        {//japs old
                            // var tab_2 = this.parentNode.parentNode.querySelectorAll("[nav-selector-id='#flist-tabs-2']")[0].parentNode;
                            // tab_2.style.display = 'none';
                            // var id = tab_2.childNodes[0].getAttribute('nav-selector-id');
                            // id = id.replace(/\#/g, '');
                            // var tab_2_textarea = this.parentNode.parentNode.querySelectorAll('[id="' + id + '"]')[0];
                            // tab_2_textarea.value = '';
                            // var tab_3 = this.parentNode.parentNode.querySelectorAll("[nav-selector-id='#flist-tabs-3']")[0].parentNode;
                            // tab_3.style.display = 'block';
                            // tab_3.childNodes[0].click();
                        }
                    }

                    var tab_show_nav = all_tab_nav.children(selector['nav']);
                    var tab_hide_nav = all_tab_nav.children(':not('+selector['nav']+')');
                    var tab_content = tab_container.children().children(selector['content']);
                    var tab_content_textarea = tab_content.children('textarea');
                    // tab_content_textarea.val('');
                    tab_show_nav.eq(0).trigger('click');
                    tab_show_nav.show();
                    tab_hide_nav.hide();

                });
                MakePlusPlusRowV2({
                    'tableElement':'.flist-container',
                    'rules':[
                        {
                            'elePlus':'>.flist-entry-container>div>.flist-add-field-entry',
                            'elePlusTarget':'.flist-entry-container',
                            'elePlusTargetFn':function(e,data){
                                var self_ele = $(this);
                                self_ele.removeClass('isDisplayNone');
                                var all_field_entry_container = self_ele.parent().children('.flist-entry-container');
                                all_field_entry_container.find('>div>.flist-minus-field-entry').removeClass('isDisplayNone');
                                
                                var selector_fields = {//clear fields first
                                    "dropdown":self_ele.find('>div>.div_1_of_3>.flist-field-selection'),
                                    "checkbox":self_ele.find('>div>.div_1_of_3>label>.flist-use-exist-formula'),
                                    "textarea":self_ele.find('>div>.div_2_of_3>div>div>.flist-field-equivalent-formula'),
                                    "textarea_changelist":self_ele.find('>div>.div_2_of_3>div>div>.flist-field-equivalent-formula-changelist'),
                                    "textarea_placeholder":self_ele.find('>div>.div_2_of_3>div>div>.flist-field-equivalent-formula-placeholder'),
                                    "textarea_label":self_ele.find('>div>.div_2_of_3>div>div>.flist-field-equivalent-formula-label'),
                                    "flist-tab-control-formulas":self_ele.find('>div>.div_2_of_3>.flist-tab-control-formulas')
                                };
                                selector_fields['textarea_description'] = selector_fields['dropdown'].next();
                                var is_selected_field_name_exist = false;
                                if(data){
                                    if(data.parameter){
                                        if(data.parameter.length >= 1){
                                            var is_selected_field_name_exist = $('[name="'+data.parameter[0].field_name+'"]').length >= 1 || $('[name="'+data.parameter[0].field_name+'[]"]').length >= 1;
                                        }
                                    }
                                }
                                
                                {//tab functionality
                                    selector_fields["flist-tab-control-formulas"].removeData("uiTabs");
                                    selector_fields["flist-tab-control-formulas"].find('*').off();
                                    //changing the idss
                                    // selector_fields["flist-tab-control-formulas"].find('[nav-selector-id],[content-selector-id]').each(function(){
                                    //     var self = $(this);
                                    //     if(self.is('a[href]')){
                                    //         self.attr('href', self.attr('nav-selector-id')+"_"+class_self.dialog_counter);
                                    //     }else{
                                    //         self.attr('id', self.attr('content-selector-id')+"_"+class_self.dialog_counter);
                                    //     }
                                    // });
                                    selector_fields["flist-tab-control-formulas"].tabs();

                                }

                                selector_fields['checkbox'].attr('id', selector_fields['checkbox'].attr('data-original-id')+ class_self.dialog_counter );
                                selector_fields['checkbox'].next().attr('for', selector_fields['checkbox'].attr('data-original-id')+ class_self.dialog_counter );

                                selector_fields['dropdown'].val("-1");
                                selector_fields['textarea_description'].val("");
                                selector_fields['checkbox'].prop("checked", false);
                                selector_fields['textarea'].val("");
                                selector_fields['textarea_changelist'].val("");
                                selector_fields['textarea_placeholder'].val("");
                                selector_fields['textarea_label'].val("");
                                self_ele.find('>div>.div_2_of_3>.flist-tab-control-formulas>.ui-tabs-nav').css('height', '24px');
                                if(data.parameter.length >= 1){
                                    // self_ele.find('>div>.div_2_of_3>.flist-tab-control-formulas>.ui-tabs-nav').css('height', '24px');
                                    if(data.parameter[0].field_name){
                                        if(is_selected_field_name_exist){
                                            selector_fields['dropdown'].val(data.parameter[0].field_name);
                                            selector_fields['dropdown'].trigger('change');    
                                        }else{
                                            selector_fields['dropdown'].val("-1");
                                        }
                                    }else if($('.setObject[data-object-id="'+data.parameter[0].data_object_id+'"]').length >= 1){ //FS#8336 - for container objects
                                        selector_fields['dropdown'].children('[data-obj-id="'+data.parameter[0].data_object_id+'"]').prop('selected',true);
                                    }
                                    if(data.parameter[0].formula_item_list_description){
                                        selector_fields['textarea_description'].val( data.parameter[0].formula_item_list_description );
                                    }
                                    if(data.parameter[0].use_exist){
                                        selector_fields['checkbox'].prop("checked", data.parameter[0].use_exist);
                                    }
                                    if(data.parameter[0].equivalent_formula){
                                        selector_fields['textarea'].val(data.parameter[0].equivalent_formula);
                                    }
                                    if(data.parameter[0].change_list_formula){
                                        selector_fields['textarea_changelist'].val(data.parameter[0].change_list_formula);
                                    }
                                    if(data.parameter[0].placeholder_formula){
                                        selector_fields['textarea_placeholder'].val(data.parameter[0].placeholder_formula);
                                    }
                                    if(data.parameter[0].label_formula){
                                        selector_fields['textarea_label'].val(data.parameter[0].label_formula);
                                    }
                                }
                                class_self.dialog_counter = class_self.dialog_counter + 1;
                                $('.content-dialog-scroll.ps-container').perfectScrollbar("update");
                            },
                            'eleMinus':'>.flist-entry-container>div>.flist-minus-field-entry',
                            'eleMinusTarget':'.flist-entry-container',
                            'eleMinusTargetFnAfter':function(){
                                var self_ele = $(this);
                                var all_field_entry_container = self_ele.children('.flist-entry-container');
                                if(all_field_entry_container.length == 1){
                                    all_field_entry_container.find('>div>.flist-minus-field-entry').addClass('isDisplayNone')
                                }
                                $('.content-dialog-scroll.ps-container').perfectScrollbar("update");
                            }
                        }
                    ]
                });
                
                

                var commited_settings = $('body').data("ComputedFormulaEventList");
                var restore_settings = false;
                if($.type(commited_settings) == "array")
                    { if(commited_settings.length >= 1) restore_settings = true; }

                if(restore_settings == true){
                    AsyncLoop(commited_settings,function(a,b){
                        var is_selected_field_name_exist = $('[name="'+b['field_name']+'"]').length >= 1 || $('[name="'+b['field_name']+'[]"]').length;
                        if(!is_selected_field_name_exist){
                            return true; //FS#8117
                        }
                        if(a == 0){
                            if(b['change_list_formula']){
                                var temp_variable = {
                                    "element":class_self.dialog_selector['flist-container'].children('.flist-entry-container:last'),
                                    "field_name":b['field_name'],
                                    "formula_item_list_description":b['formula_item_list_description'],
                                    "equivalent_formula":b['eq_formula'],
                                    "change_list_formula": b['change_list_formula'],
                                    "placeholder_formula": b['placeholder_formula'],
                                    "use_exist":b['use_exist'],
                                    "label_formula":b['label_formula'],
                                    "data_object_id":b['data_object_id']
                                };
                                temp_variable['element'].children('div').children('.div_1_of_3')
                                    .children('.flist-field-selection').val(  temp_variable['field_name'] )
                                    .next().val( temp_variable['formula_item_list_description'] ).parent()
                                    .children('label').children('.flist-use-exist-formula').prop('checked', temp_variable['use_exist'] ).parent().parent()
                                    .next().children().children().children('.flist-field-equivalent-formula').val( temp_variable['equivalent_formula'] )
                                    .parent().next().children('.flist-field-equivalent-formula-changelist').val(temp_variable['change_list_formula'] )
                                    .parent().next().next().children('.flist-field-equivalent-formula-label').val(temp_variable['label_formula']);
                                    
                                // temp_variable['element'].find('>div>.div_2_of_3>.flist-tab-control-formulas>.ui-tabs-nav').css('height', '24px').find('[aria-controls="flist-tabs-3"]').css('display', 'none');
                            }
                            else{
                                var temp_variable = {
                                    "element":class_self.dialog_selector['flist-container'].children('.flist-entry-container:last'),
                                    "field_name":b['field_name'],
                                    "formula_item_list_description":b['formula_item_list_description'],
                                    "equivalent_formula":b['eq_formula'],
                                    "placeholder_formula":b['placeholder_formula'],
                                    "use_exist":b['use_exist'],
                                    "label_formula":b['label_formula'],
                                    "data_object_id":b['data_object_id']
                                };
                                temp_variable['element'].children('div').children('.div_1_of_3')
                                    .children('.flist-field-selection').val(  temp_variable['field_name'] )
                                    .next().val( temp_variable['formula_item_list_description'] ).parent()
                                    .children('label').children('.flist-use-exist-formula').prop('checked', temp_variable['use_exist'] ).parent().parent()
                                    .next().children().children().children('.flist-field-equivalent-formula').val( temp_variable['equivalent_formula'] )
                                    .parent().next().next().children('.flist-field-equivalent-formula-placeholder').val(temp_variable['placeholder_formula'])
                                    .parent().next().children('.flist-field-equivalent-formula-label').val(temp_variable['label_formula']);
                                // temp_variable['element'].find('>div>.div_2_of_3>.flist-tab-control-formulas>.ui-tabs-nav').css('height', '24px').find('[aria-controls="flist-tabs-2"]').css('display', 'none');

                            }
                            temp_variable['element'].children('div').children('.div_1_of_3').children('.flist-field-selection').trigger("change");
                        }else{
                            if(b['change_list_formula']){
                                class_self.dialog_selector['flist-container'].find('>.flist-entry-container:last>div>.flist-add-field-entry:eq(0)').trigger("click",[{
                                    "field_name":b['field_name'],
                                    "formula_item_list_description":b['formula_item_list_description'],
                                    "equivalent_formula":b['eq_formula'],
                                    "use_exist":b['use_exist'],
                                    "change_list_formula":b['change_list_formula'],
                                    "label_formula":b['label_formula'],
                                    "data_object_id":b['data_object_id']
                                }]);
                                // class_self.dialog_selector['flist-container'].children('.flist-entry-container:last').find('>div>.div_2_of_3>.flist-tab-control-formulas>.ui-tabs-nav').css('height', '24px').find('[aria-controls="flist-tabs-3"]').css('display', 'none');
                            }
                            else{
                                class_self.dialog_selector['flist-container'].find('>.flist-entry-container:last>div>.flist-add-field-entry:eq(0)').trigger("click",[{
                                    "field_name":b['field_name'],
                                    "formula_item_list_description":b['formula_item_list_description'],
                                    "equivalent_formula":b['eq_formula'],
                                    "use_exist":b['use_exist'],
                                    "placeholder_formula":b['placeholder_formula'],
                                    "label_formula":b['label_formula'],
                                    "data_object_id":b['data_object_id']
                                }]);
                                // class_self.dialog_selector['flist-container'].children('.flist-entry-container:last').find('>div>.div_2_of_3>.flist-tab-control-formulas>.ui-tabs-nav').css('height', '24px').find('[aria-controls="flist-tabs-2"]').css('display', 'none');
                            }
                            
                        }
                    },function(){
                        var temp = class_self.dialog_selector['flist-container'].find('.flist-field-selection');
                        temp.filter(function(){ return $(this).children('[value="-1"]:selected').length > 1; }).closest('.flist-entry-container').remove(); //FS#8117
                        class_self.dialog_selector['flist-commit'].removeAttr('data-temp-disable');
                        temp.trigger("change"); // FS#8394
                        class_self.dialog_selector['flist-commit'].removeClass('loading-data-please-wait');
                    });
                }else{
                    class_self.dialog_selector['flist-commit'].removeClass('loading-data-please-wait');
                }
            });
        });
    }
}

function formbuilderHelpModal(){

    $.get('/json/formbuilder-help.json', function(data){

        var ele = '';
        $.each(data, function(key,value){

            $.each(value, function(key, val){

                var obj_type = val.object_type;
                var obj_title = val.title;
                var obj_desc = val.description;
                //console.log(obj_title, obj_desc)

                ele += '<div class="input_position_below section clearing fl-field-style  fl-modal-content-text-wrapper" data-formbuilder-desc-search="'+obj_title.toLowerCase()+'"><div class="column div_1_of_1  fl-modal-content-text"><h3>'+obj_title+'</h3><p>'+obj_desc+'</p></div></div>';

            });
        });
            
        var ret = '<h3 class="fl-margin-bottom">';
        ret += '<i class="icon-asterisk fa fa-cogs"></i> Formbuilder Help';
        ret += '</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
        ret += '<div class="content-dialog content-dialog-formbuilder-description" style="height: auto;">';
            // your content here
            ret +='<div class="input_position_below section clearing fl-field-style search-formbuilder-desc"><div class=" column div_1_of_1"><span class="font-bold">Search tool: </span><input type="text" name="search-formbuilder-desc" id="search-formbuilder-desc"  class="form-text"></div></div>';
            ret +='<div class="fl-formbuilder-desc-list-wrapper">';
                ret += ele;
            ret += '</div>';    
            ret += '<span id="no-match" class="font-bold"></span>'
        ret+= '</div>';
        var newDialog = new jDialog(ret, "", "700", "500", "", function() {   

        });
        newDialog.themeDialog("modal2"); 
        $('.getObjects').tooltip('hide');

        $('#search-formbuilder-desc').on('keyup', function(){
            
            var searchTerm = $(this).val().toLowerCase();
            var test = "";
            var a  = "";
            var ctr = 0;

            $('.content-dialog-scroll').perfectScrollbar('update');

            $('.fl-modal-content-text-wrapper').each(function(){

                a = $(this).attr('data-formbuilder-desc-search');
                test = a.indexOf(searchTerm);

                if (test >= 0) {
                    $(this).show();
                    ctr++;
                }else {
                    $(this).hide();
                }

            });

            if (ctr == 0) {
                 $('#no-match').text('No tool found.');
             }else {
                 $('#no-match').text(' ');
             }

        });

    });    

}

function applyFieldTheme(theme_name, previousTheme){
    if(theme_name != "none"){
        $('.setObject').removeClass(previousTheme);
        $('.setObject').addClass(theme_name);
    }
    else{
        $('.setObject').removeClass(previousTheme);
    }
}