
(function(){

	var formbuilderDes = angular.module('formbuilderDes', []);
	
	formbuilderDes.controller('formbuilderDesCtrl',['$scope', '$http', function($scope, $http){

		var ele  = angular.element("#fl-workspace-controls").find('button');
		ele.attr('data-placement','bottom');
		
		ele.tooltip({
			template: '<div class="tooltip formbuilder-description-tooltip custom-data-tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner custom-data-tooltip-inner formbuilder-description-tooltip-inner"></div><div class="formbuilder-description-tooltip-help">Press f1 for more help</div></div>',
			delay: { "show": 1000, "hide": 100 },
			html:true
		});

		angular.element('.formbuilder-description-tooltip');

		var onDataComplete = function(response){
			
			$scope.description = response.data;
			var values = $scope.description;
			var description = {};
			var obj_type = {};

			angular.forEach(values, function(value, key){
		
				angular.forEach(value, function(val, key){

					description = val.title;
					obj_type = val.object_type;
					angular.element("[data-object-type='"+ obj_type +"']").attr('data-original-title', val.description);
					angular.element("[data-disabled='"+ obj_type +"']").attr('data-original-title', val.description);
					
					
				});

			});


		}

		var onError = function(reason){
			$scope.error = "Fetching data problem.";
		}	

		$http.get('/json/formbuilder-help.json')
			.then(onDataComplete);

	}]);

})();

