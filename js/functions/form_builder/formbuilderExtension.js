/*
choosing of form category
*/
var formID = getURLParameter("formID");
var category_id = getURLParameter("category_id");
if(formID==0 && category_id == null){
	setFormCategory();
}

function calendarViewDesign(self){
    var json_body = $("body").data();
    var include_calendar = if_undefinded(json_body['include_calendar'],"");
    var calendar_view = if_undefinded(json_body['calendar_view'],"");
    var calendar_view_type = "1";
    var calendar_field = "";
    var calendar_field_title = "";
    var calendar_default_view
    var calendar_enable_tooltip = "1";
    if(calendar_view!=""){
        calendar_view_type = calendar_view['calendar_view_type'];
        calendar_field = calendar_view;
        calendar_field_title = calendar_view['title'];
        calendar_default_view = calendar_view['default_view'];
        calendar_enable_tooltip = if_undefinded(calendar_view['calendar_enable_tooltip'],"1");
    }
    var ret = '<div id = "calendar-tab" >';
          ret += '<ul>';
            ret += '<li><a href="#calendar-display"><span>Display</span></a>';
            ret += '<li><a href="#calendar-options"><span>Filter</span></a>';
          ret += '</ul>';
          ret += '<div id="calendar-display" style="padding:10px;">';
            ret += '<div class="fields_below section clearing fl-field-style">';
                ret += '<div class="input_position_below column div_1_of_1"><label><input type="checkbox" ' + setChecked("1",include_calendar) + ' class="css-checkbox" id="include_calendar" value="1"><label for="include_calendar" class="css-label"></label>Enable calendar view</label></div>';
            ret += '</div>';
            ret += '<div class="fields_below section clearing fl-field-style">';

                ret += '<div class="input_position_below column div_1_of_1"><label><input type="checkbox" ' + setChecked("1",calendar_default_view) + ' class="css-checkbox caldr-field" id="calendar_default_view" value="1"><label for="calendar_default_view" class="css-label"></label>Set calendar as default view</label></div>';
            ret += '</div>';
            ret += '<div class="fields_below section clearing fl-field-style">';
                ret +=  '<div class="input_position_below column div_1_of_1"><label>Display Record Reference As:<br/>Note: If the value was not a valid formatted date then DateCreated will be default</label>';
                    ret += '<select class="form-select caldr-field calendar_view_display_record_as">';
                        ret += '<option value="DateCreated" '+ setSelected("DateCreated",calendar_field_title) +'>Date Created(Default)</option>';
                        
                        GetValidFieldsSOBJC(".formbuilder_ws .setObject").sort(function(a,b){
                            var this_obj_a = $(a);
                            var this_obj_b = $(b);
                            var data_obj_id_a = this_obj_a.attr("data-object-id");
                            var data_obj_id_b = this_obj_b.attr("data-object-id");
                            var label_a = $(a).find("#lbl_"+data_obj_id_a).text().replace(":","");
                            var label_b = $(b).find("#lbl_"+data_obj_id_b).text().replace(":","");
                            var field_name_a = this_obj_a.find(".getFields_"+data_obj_id_a).attr("name");
                            var field_name_b = this_obj_b.find(".getFields_"+data_obj_id_b).attr("name");
                            if(field_name_a!=undefined && field_name_b!= undefined){
                                return field_name_a.toLowerCase().localeCompare(field_name_b.toLowerCase());
                            }
                        }).each(function(){
                            var this_obj = $(this);
                            var data_obj_id = this_obj.attr("data-object-id");
                            var this_obj_label = this_obj.find("#lbl_"+data_obj_id).text().replace(":","");
                            var field_name = this_obj.find(".getFields_"+data_obj_id).attr("name");
                            // if(field_name && this_obj.is('[data-type="datepicker"],[data-type="time"],[data-type="dateTime"]')){
                                field_name.replace("[]","");
                                ret += '<option value="'+ field_name +'" '+ setSelected(field_name,calendar_field_title) +'>'+ field_name +'</option>';
                            // }
                        });
                    ret += '</select>';
                ret += '</div>';
            ret += '</div>';
            
            ret += '<div class="fields_below section clearing fl-field-style">';
                ret += '<div class="input_position_below column div_1_of_1">';
                    ret += '<span class="font-bold">Field to Display</span>';
                    ret += '<select class="form-select caldr-field calendar_view_display_title">';
                        ret += '<option value="TrackNo" '+ setSelected("TrackNo",calendar_field_title) +'>Tracking Number(Default)</option>';
                        ret += '<option value="requestorName" '+ setSelected("requestorName",calendar_field_title) +'>Requestor(Default)</option>';
                        // ret += '<option value="Processor" '+ setSelected("Processor",calendar_field_title) +'>Processor(Default)</option>';
                        ret += '<option value="Status" '+ setSelected("Status",calendar_field_title) +'>Status(Default)</option>';
                        ret += '<option value="DateCreated" '+ setSelected("DateCreated",calendar_field_title) +'>Date Created(Default)</option>';
                       
                        GetValidFieldsSOBJC(".formbuilder_ws .setObject").sort(function(a,b){
                            var this_obj_a = $(a);
                            var this_obj_b = $(b);
                            var data_obj_id_a = this_obj_a.attr("data-object-id");
                            var data_obj_id_b = this_obj_b.attr("data-object-id");
                            var label_a = $(a).find("#lbl_"+data_obj_id_a).text().replace(":","");
                            var label_b = $(b).find("#lbl_"+data_obj_id_b).text().replace(":","");
                            var field_name_a = this_obj_a.find(".getFields_"+data_obj_id_a).attr("name");
                            var field_name_b = this_obj_b.find(".getFields_"+data_obj_id_b).attr("name");
                            if(field_name_a!=undefined && field_name_b!= undefined){
                                return field_name_a.toLowerCase().localeCompare(field_name_b.toLowerCase());
                            }
                        }).each(function(){
                            var this_obj = $(this);
                            var data_obj_id = this_obj.attr("data-object-id");
                            var this_obj_label = this_obj.find("#lbl_"+data_obj_id).text().replace(":","");
                            var field_name = this_obj.find(".getFields_"+data_obj_id).attr("name");
                            if(field_name){
                                field_name.replace("[]","");
                                ret += '<option value="'+ field_name +'" '+ setSelected(field_name,calendar_field_title) +'>'+ field_name +'</option>';
                            }
                        })
                    ret += '</select>';
                ret += '</div>';
                ret += '<div class="fields_below">';
                    ret += '<div class="label_below2"></div>';
                    ret += '<div class="input_position_below column div_1_of_1"><label><input type="checkbox" ' + setChecked("1",calendar_enable_tooltip) + ' class="css-checkbox calendar_enable_tooltip" id="calendar_enable_tooltip" value="1"><label for="calendar_enable_tooltip" class="css-label"></label>Enable Tooltip</label></div>';
                ret += '</div>';
                var show_calendar_tooltip_values = "";
                if(calendar_enable_tooltip=="0"){
                    show_calendar_tooltip_values = "isDisplayNone";
                }
                ret += '<div class="column div_1_of_1 fields_below '+ show_calendar_tooltip_values +'" id="calendar_tooltip_values">';
                    ret += '<div class="input_position_below">';
                         
                        ret += '<div class="input-tooltip-container" style="margin-right:5px;">';
                            ret += '<div class = "label-button-div">';
                              ret += '<div class="fL">';
                                ret += '<span class="font-bold">Tooltip Value</span>';
                              ret += '</div>';
                              ret += '<div class="fR">';
                                ret += '<i class="add-calendar-tooltip-row fa fa-plus cursor"></i>';
                                  ret += '<i class="minus-calendar-tooltip-row fa fa-minus cursor" style="display:none;margin-left:5px;"></i>';
                                  
                              ret += '</div>'; 
                              ret += '<div class="section clearing fl-field-style">';
                                ret += '<div class="column div_1_of_2">';
                                    ret += '<input type="text" class="calendar-tooltip-input form-text"/>';
                                ret += '</div>';
                                ret += '<div class="column div_1_of_2">';
                                    ret += '<select class="form-select calendar-tooltip-combo">';
                                        ret += self.getFieldsCalendar(calendar_field["single_date"]);
                                    ret += '</select>';
                                ret += '</div>';
                              ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
               ret += '</div>';

            ret += '</div>';

          ret += '</div>';
          ret += '<div id="calendar-options" style="padding:10px;">';
            ret += '<div class="fields_below section clearing fl-field-style">';
                ret += '<div class="input_position_below column div_1_of_1">';
                    ret += '<span class="font-bold">Date Type:</span><Br/>';
                     ret += '<span class="font-bold"><label><input type="radio" class="calendar_view_type caldr-field css-checkbox" ' + setChecked("1",calendar_view_type) + ' name="calendar_type" id="calendar_type_1" value="1"><label for="calendar_type_1" class="css-label"></label> Single Date</label></span>';
                ret += '<div>';
                    ret += '<div class="fields">';
                    ret += '<span class="font-bold">Date Field:</span>';
                    ret += '<select class="form-select caldr-single-date caldr-field">';
                        ret += self.dateField(calendar_field["single_date"]);
                    ret += '</select>';
                    ret += '</div>';
                ret += '</div>';
                ret += '<div >';
                    ret += '<div class="range_date section clearing">';
                        ret += '<div class="column div_1_of_2">';
                            ret += '<div class="properties_width_container">';
                                ret += '<div class="fields_below">';
                                    ret += '<span class="font-bold"><label><input type="radio" class="calendar_view_type caldr-field css-checkbox" ' + setChecked("2",calendar_view_type) + ' name="calendar_type" id="calendar_type_2" value="2"><label for="calendar_type_2" class="css-label"></label> Date Range</label></span>';
                                    ret += '<div class="input_position_below fields">';
                                         ret += '<span class="font-bold">Start Date Field:</span>';
                                            ret += '<select class="form-select caldr-start-date caldr-field">';
                                                ret += self.dateField(calendar_field["start_date"]);
                                            ret += '</select>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';

                        ret += '<div class="column div_1_of_2">';
                            ret += '<div class="properties_width_container">';
                                ret += '<div class="fields_below" style="margin-top:18px;">';
                             
                                    ret += '<div class="input_position_below fields">';
                                         ret += '<span class="font-bold">End Date Field:</span>';
                                            ret += '<select class="form-select caldr-end-date caldr-field">';
                                                 ret += self.dateField(calendar_field["end_date"]);
                                            ret += '</select>';
                                    ret += '</div>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';   
            ret +='</div>';
          ret += '</div>';
        ret += '</div>';
    return ret;
}
function setFormCategory(){
	setModalCategory(function(data){
		var newDialog = new jDialog(data, "","", "", "", function(){});
		newDialog.themeDialog("modal2");
		$(".setFormCategory").click(function(){
			var value = $(".formcategory_selection").val();
			var user_view = $("#user_url_view").val();
			window.location= user_view + "formbuilder?formID=0&category_id="+value;
			
			$("#popup_overlay,#popup_container").remove();
			
		})
	})
}
function setModalCategory(callback) {
    $.post("/ajax/form_category",{action:"getFormCategories"},function(data){
        var forms_json = JSON.parse(data);
        var json = $("body").data();
        var ret = "";
        ret += '<div style="float:left;width:50%;">';
            ret += '<h3 class="pull-left fl-margin-bottom">';
                ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-form-category"></use></svg>' + "<span>Form Category</span>";
            ret += '</h3>';
        ret += '</div>';
        //ret += '<div style="float:right;width:50%;text-align:right;margin-top:-5px;">';
        //    ret += '<input type="button" class="btn-basicBtn getFormForWorkflow" id="" value="Ok" node-data-id="" style="">';
        //ret += '</div>';
        ret += '<div class="hr"></div>';
                ret += '<div class="fields">';
                    //ret += '<div class="label_below2 fl-get-workspace-popup"> Please select a form category: </div>';
                    ret += '<div class="input_position section clearing fl-field-style">';
                        ret += '<div class="column div_1_of_1">';
                            ret += '<span class="fl-get-workspace-popup">Please select a form category: </span>';
                            ret += '<select class="form-select formcategory_selection fl-margin-bottom" style="margin-top:5px; margin-bottom:5px;">';
                                ret += '<option value="0">Others</option>';
                                for (var i in forms_json){
                                     ret += '<option '+ setSelected(json['form_category'], forms_json[i]['id']) +' value="'+ forms_json[i]['id'] +'">'+ forms_json[i]['category_name'] +'</option> ';
                                }
                            ret += '</select>';
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';
                ret += '<div class="fields">';
                    ret += '<div class="label_below2"></div>';
                    ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                        ret += '<input type="button" class="btn-blueBtn setFormCategory fl-margin-right fl-positive-btn" value="OK"> ';
                        //ret += '<a href="/"><input type="button" class="btn-basicBtn" id="workflow-cancel" value="Cancel"></a>';
                    ret += '</div>';
                ret += '</div>';
        callback(ret);
    })
}



function setSavedFormUsers(){
    //migrated form users(author,viewer and admin)

    var authors = $(".form-user-authors").text();
    var viewers = $(".form-user-viewers").text();
    var admin = $(".form-user-admin").text();
    var users = $(".form-user-accessor").text();
    var customPrintUsers = $(".form-custom-print-users").text();
    var deleteAccess = $(".form-delete-access-json").text();
    // var navAccess = $(".form-nav-access").text();
    

    var json = $("body").data();
    if(authors){
        json['form-authors'] = JSON.parse(authors);
    }
    if(viewers){
        json['form-readers'] = JSON.parse(viewers);
    }
    if(admin){
        json['form-admin'] = JSON.parse(admin);
    }
    if(users){
        json['form-users'] = JSON.parse(users);
    }
    if(customPrintUsers){
        json['customized-print'] = JSON.parse(customPrintUsers);
    }
    if(deleteAccess){
        json['delete-access'] = JSON.parse(deleteAccess);
    }
    // if(navAccess){
    //     json['nav-viewers'] = JSON.parse(navAccess);
    // }
}

function changeCategory(){
    $("body").on("click",".form-category",function(){
        ui.block();
        var value = "";
        setModalCategory(function(data){
            var newDialog = new jDialog(data, "","", "", "", function(){});
            newDialog.themeDialog("modal2");
            ui.unblock();
            $(".setFormCategory").click(function(){
                value = $(".formcategory_selection").val();
                var json = $("body").data();
                var exisitingCategory = json['form_category'];

                if(exisitingCategory!=value){
                    var message_concat_1 = "";var message_concat_2 = "";
                    if($("#enable_config_deletion").text()=="1"){
                        message_concat_1 = ", ";
                        message_concat_2 = "and Delete Access Users";
                    }else{
                        message_concat_1 = " and ";
                    }
                    var message = "Warning : Changing the form category will reset the Users, Authors, Admin, Viewers"+ message_concat_1 +"Custom Print Users "+ message_concat_2 +" of this form. Do you want to proceed?";
                    // if(value==0){
                    //     message = "Warning, All users will automatically become form users,authors,admin and viewers";
                    // }
                    var newConfirm = new jConfirm(message, 'Confirmation Dialog','', '', '', function(r){
                        if(r==true){
                            if(value=="0"){
                                setAllFormusers();
                               // $('.controlsAccess_c').css({'top': '-31px'})
                            }else{
                                //show form users modification
                                modifyformUsers();
                                //$('.controlsAccess_c').css({'top': '-67px'})
                              
                            }
                            //set ajax here
                            $.post("/ajax/form_category",{action:"getCategoryUsers",category_id:value},function(data){
                                if(data){
                                    data = JSON.parse(data);
                                    //set data
                                    $(".departments").text(data['departments']);
                                    $(".positions").text(data['positions']);
                                    $(".users").text(data['users']);
                                    $(".users-admin").text(data['users_admin']);
                                    $(".form_group").text(data['form_groups']);
                                    RequestPivacy.resetUsers();

                                    //set value of category id in form json
                                    json['form_category'] = value;
                                    $("#category_id").val(value);
                                    //remove dialog
                                    $("#popup_overlay,#popup_container").remove();


                                }
                            })
                        }
                    });
                    newConfirm.themeConfirm("confirm2", {
                        'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>' 
                    });
                }else{
                    //if others
                    if(value==0){
                        setAllFormusers();
                    }else{
                        modifyformUsers();
                    }
                    $("#popup_overlay,#popup_container").remove();
                }



              

            })
        })
        
    })
}


function setValidCategoryUsers(value){
    return false;
    ui.block();
    //set ajax here

    $.ajax({
        type: 'POST',
        url: '/ajax/form_category',
        data: {action:"getCategoryUsers",category_id:value},
        async : false,
        success: function (data) {
            if(data){
                data = JSON.parse(data);
                //set data
                $(".departments").text(data['departments']);
                $(".positions").text(data['positions']);
                $(".users").text(data['users']);
                $(".users-admin").text(data['users_admin']);
                $(".form_group").text(data['form_groups']);
                ui.unblock();
            }
        }
    });
}
function setAllFormusers(){
    $(".form-authors, .form-readers, .form-users, .form-admin").hide();
}
function modifyformUsers(){
    $(".form-authors, .form-readers, .form-users, .form-admin").show();
}
$(document).ready(function(){
    var formID = getURLParameter("formID");
    var category_id_get = getURLParameter("category_id");
    var category_id = $("#category_id").val();
    if(category_id==0){
        setAllFormusers();
    }
    if(formID==0 && category_id_get != null){
        var json = $("body").data();
        json['form_category'] = category_id;
    }   
    //change form category
    changeCategory();
})


//for junction in tables
function getFieldsData(){
    var data_field_name = "";
    var field_type = "";
    var data_field_input_type = "";
    var data_field_input_type = "";
    var data_field_default_type = "";
    var data_field_formula = "";
    var data_field_label = "";
    var visibility_formula = "";
    var object_id = undefined; //object id
    var input_validation = {//input validation
        "formula":"",
        "message":""
    };
    var json = [];
    var readonly = "";
   
    $(".setObject").each(function(eqi_setObj) {
        readonly = "";
        data_field_formula="";
        var multiple_values = [];
        if (
                $(this).attr("data-type") == "labelOnly" ||
                $(this).attr("data-type") == "createLine" ||
                $(this).attr("data-type") == "table" ||
                $(this).attr("data-type") == "tab-panel" ||
                // $(this).attr("data-type") == "listNames" || inalis ko to aaron para masama yung names sa tbfields -- JEWEL 
                // $(this).attr("data-img") == "formImgDesign" ||
                $(this).is('[data-img="formImgDesign"]') ||
                $(this).attr("data-type") == "button" ||
                $(this).attr("data-type") == "embeded-view" ||
                $(this).attr("data-type") == "accordion" ||
                $(this).attr("data-type") == "details-panel"
                ) {
            return true; //preventing to insert it to the database
        }
        counted_id = $(this).attr("data-object-id");
        //added by japhet morada
        //add object id
        object_id = counted_id;

        data_field_name = if_undefinded($(this).find(".getFields_" + counted_id).attr("name"),"").replace("[]", "");

        field_type = $(this).attr("data-type");
        data_field_input_type = $(this).find(".getFields_" + counted_id).attr("data-input-type");
        data_field_default_type = $(this).find(".getFields_" + counted_id).attr("default-type");
        visibility_formula = $(this).find(".getFields_" + counted_id).attr("visible-formula");
        if(data_field_default_type=="computed"){
            data_field_formula = $(this).find(".getFields_" + counted_id).attr("default-formula-value");    
        }else if(data_field_default_type=="static"){
            data_field_formula = $(this).find(".getFields_" + counted_id).attr("default-static-value");    
        }else if(data_field_default_type=="middleware"){
            data_field_formula = $(this).find(".getFields_" + counted_id).attr("default-middleware-value");    
        }else{
            data_field_formula="";
        }
        //added by japhet
        //add input validation in tbfields
        input_validation['formula'] = $(this).find(".getFields_" + counted_id).attr("field-formula-validation");
        input_validation['message'] = $(this).find(".getFields_" + counted_id).attr("field-formula-validation-message");
        
        // console.log("dataFieldFOrmula",data_field_formula)
        data_field_label = $(this).find("#lbl_" + counted_id).text();
        //readonly = $(this).find(".getFields_" + counted_id+"[readonly]").attr('readonly');
        
        if($(this).find(".getFields_" + counted_id+"[readonly]").is('[readonly]')){
            // console.log("pipipiipcklist",$(this))
            readonly = 'readonly';
        }
        // console.log("length",counted_id,$(this),$(this).find(".getFields_" + counted_id+"[type='radio']"));
        if($(this).is('[data-type="checkbox"]')){
            multiple_values.push({});
            $(this).find(".getFields_" + counted_id+"[type='checkbox']").each(function(){
                multiple_values[multiple_values.length-1][""+$(this).val()] = $(this).siblings('.checkbox-input-label').html();
            });
        }
        if($(this).is('[data-type="radioButton"]')){
            multiple_values.push({});
            $(this).find(".getFields_" + counted_id+"[type='radio']").each(function(){

                
                multiple_values[multiple_values.length-1][""+$(this).val()] = $(this).siblings('.radio-input-label').html();
            });
        }
        if($(this).is('[data-type="dropdown"]')||$(this).is('[data-type="selectMany"]')){
            multiple_values.push({});
            // console.log("option",$(this).find("select.getFields_" + counted_id).children('option'))
            $(this).find("select.getFields_" + counted_id).children('option').each(function(){
                multiple_values[multiple_values.length-1][""+$(this).html()] = $(this).val();
                // console.log("multiple",multiple_values);
            });
        }
        
        if($(this).is('[data-type="pickList"]')){
            var pl_button = $(this).find('a.pickListButton');
            var pl_class = pl_button.attr("class");
            var pl_display_columns = pl_button.attr("display_columns")||"";
            var pl_display_columns_sequence = pl_button.attr("display_columns_sequence")||"";
            var pl_formname = pl_button.attr("formname")||"";
            var pl_return_field_name = pl_button.attr('return-field-name')||"";
            var pl_type = pl_button.attr("picklist-type")||"";
            var pl_button_id = pl_button.attr("picklist-button-id")||"";
            var pl_return_field = pl_button.attr("return-field")||"";
            var pl_condition_selection_type = pl_button.attr("selection-type")||"";
            var pl_condition = pl_button.attr('condition')||"";
            var pl_form_id = pl_button.attr("form-id")||"";
            var pl_name = pl_button.attr("name")||"";
            var pl_enable_entry = pl_button.attr('enable-add-entry')||"";
            var pl_allow_values_not_in_list = pl_button.attr('allow-values-not-in-list')||"";

            var picklist_json_attr = {
                "class":pl_class,
                "display_columns":pl_display_columns,
                "display_columns_sequence":pl_display_columns_sequence,
                "formname":pl_formname,
                "return-field-name":pl_return_field_name,
                "picklist-type":pl_type,
                "picklist-button-id":pl_button_id,
                "return-field":pl_return_field,
                "selection-type":pl_condition_selection_type,
                "form-id":pl_form_id,
                "name":pl_name,
                "condition":pl_condition,
                "enable_entry":pl_enable_entry,
                "allow-values-not-in-list":pl_allow_values_not_in_list
            }

        }
        if($(this).is('[data-type="listNames"]')){
            var pl_button = $(this).find('a.viewNames');
            var pl_class = pl_button.attr("class");
            var pl_return_field = pl_button.attr("return-field")||"";
            var pl_list_type_selection = pl_button.attr("list-type-selection")||"";
            var picklist_json_attr = {
                "class":pl_class,
                "return-field":pl_return_field,
                "list-type-selection":pl_list_type_selection
                
            }

        }
       
        
        
        if ($(this).find(".getFields_" + counted_id).attr("name")) {
            json.push({
                "data_field_name":if_undefinded(data_field_name,""),
                "data_field_input_type":data_field_input_type||"Text",//if_undefinded(data_field_input_type,"Text"),
                "data_field_default_type":if_undefinded(data_field_default_type,""),
                "data_field_formula":if_undefinded(data_field_formula,""),
                "field_type":if_undefinded(field_type,""),
                "data_field_visibility_formula":if_undefinded(visibility_formula,""),
                "data_field_label":if_undefinded(data_field_label,""),
                "readonly":if_undefinded(readonly, ""),
                "data_multiple_values": JSON.stringify(multiple_values)||"",
                "other_attributes": JSON.stringify(picklist_json_attr)||"",
                "object_id": object_id||"",
                "input_validation": JSON.stringify(input_validation)||"{}"
            });
        }
    })
    // console.log("json",json)
  return json;
}

ButtonDraggableFormObj = {
    "init" : function(){
        return;
        var self = this;
        $('.getObjects').draggable( {
            cursor: 'move',
            containment: 'document',
            helper: function(e){
                var object_type = $(this).attr("data-object-type");
                return self.helperObjectDesign("a", object_type);
            },
            start : function(e,ui){
                $(ui.helper).find("label").css({
                    "font-size":"inherit"
                })
            },
            cancel:false,
            opacity:".5"
        });
        // self.makeWorkspaceDroppable();
    },
    "helperObjectDesign" : function(count, object_type){
        var ret = "<div>Dragging</div>";

        switch (object_type) {//textbox_editor_support
            case "textbox_reader_support":
                ret = textbox_reader_support(count, object_type);
                break;
            case "textbox_editor_support":
                ret = textbox_editor_support(count, object_type);
                break;
            case "labelOnly":
                ret = labelOnly(count, object_type);
                break;
            case "textbox":
                ret = textbox(count, object_type);
                break;
            case "checkbox":
                ret = checkbox(count, object_type);
                break
            case "dropdown":
                ret = dropdown(count, object_type);
                break;
            case "radioButton":
                ret = radioButton(count, object_type);
                break;
            case "textarea":
                ret = textArea(count, object_type);
                break;
            case "selectMany":
                ret = selectMany(count, object_type);
                break;
            case "fileUpload":
                ret = fileUpload(count, object_type);
                break;
            case "datepicker":
                ret = datepicker(count, object_type);
                break;
            case "fullname":
                ret = fullname(count, object_type);
                break;
            case "dateTime":
                ret = dateTime(count, object_type);
                break;
            case "website":
                ret = website(count, object_type);
                break;
            case "fullAddress":
                ret = fullAddress(count, object_type);
                break;
            case "button":
                ret = btn(count, object_type);
                break;
            case "line":
                ret = createLine(count, object_type);
                break;
            case "listNames":
                ret = fieldListOfNames(count, object_type);
                break;
            case "table":
                ret = createTable(count, object_type);
                break;
            case "tabPanel":
                ret = createTabPanel(count, object_type);
                break;
            case "computed":
                ret = computedField(count, object_type);
                break;
            case "embedView":
                ret = embedView(count, object_type);
                break;
            case "pickList":
                ret = pickList(count, object_type);
                break;
            case "requestImage":
                ret = photos(count, object_type);
                break;
            case "barCodeScanner":
                ret = barCodeScanner(count, object_type);
                break;
            case "time":
                ret = TimePick(count, object_type);
                break;
            case "noteStyleField":
                ret = noteStyleTextArea(count, object_type);
                break;
            case "requestImport":
                ret = requestImport(count, object_type);
                break;
            case "multiplerequestImport":
                ret = multiple_requestImport(count, object_type);
                break;
        }
        return ret;
    },
    "workspaceButtonDrop" : function(event,ui,self){
        return;
        //tagname of drag element
        var tagname = $(ui.draggable).prop("tagName");
        //for button draggble drop only
        if(tagname=="BUTTON"){
            //declaring variable for needed adjustment
            var helper = $(ui.helper);
            var parent = $(self);
            var docs = $(document);

            // top and left adjustment
            var leftAdjust = (helper.position().left - parent.offset().left+8)+docs.scrollLeft();
            var topAdjust = (helper.position().top - parent.offset().top+48)+docs.scrollTop();

            //counter in creating of node
            //var count is global variable
            var oldCount = count;
            console.log(topAdjust+" "+leftAdjust)

            $(ui.draggable).trigger("click");
            // alert(oldCount)
            var setObj = $("#setObject_"+oldCount);

            setObj.css({
                top:topAdjust+"px",
                left:leftAdjust+"px"
            });
        }
    },

}

CalendarView = {
    "init" : function(){
        this.setDialog();
    },
    "setDialog" : function(){
        var self = this;
        $("body").on("click",".calendar-view",function(){
            
           

            var ret = '<div><h3 class="fl-margin-bottom">';
            ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-calendar-view"></use></svg> <span>Calendar View</span>';
            ret += '</h3></div>';
            ret += '<div class="hr"></div>';
            ret += '<div class="content-dialog" style="height: auto;">';
                ret += calendarViewDesign.call($(this),self);
            ret += '</div>';
            ret += '<div class="fields">';
                ret += '<div class="label_basic"></div>';
                ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                    ret += '<input type="button" class="fl-positive-btn btn-blueBtn fl-margin-right setCalendarView" data-type="" value="OK">     ';
                    ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
                ret += '</div>';
            ret += '</div>';
            var newDialog = new jDialog(ret, "","600", "", "40", function(){});
            newDialog.themeDialog("modal2");
            //on saving
            $('#calendar-tab').tabs();
            self.setCalendarView();

            self.include_calendar();
            self.calendar_type();

            var tooltipCombo = $(".calendar-tooltip-combo");
            tooltipCombo.each(function(){
                var opts_list = $(this).find('option');
                opts_list.sort(function(a, b) { return $(a).text().localeCompare($(b).text())});
                $(this).html(opts_list);
            })
            
            

            MakePlusPlusRowV2({
                "tableElement": '.input-tooltip-container',
                "rules":[{
                    "eleMinus":".minus-calendar-tooltip-row",
                    "eleMinusTarget":".label-button-div",
                    "elePlus":".add-calendar-tooltip-row",
                    "elePlusTarget":".label-button-div",
                    "elePlusTargetFn":function(){
                        
                        var self = $(this);


                        // self.each(function(){
                            self.find("input.calendar-tooltip-input").val("");
                            self.find("select.calendar-tooltip-combo").val("TrackNo");
                        // });

                        // if(data['field']){
                        //     self.find('.calendar-tooltip-combo').val(data['field']);
                        // }


                        // if(data['label']){
                        //     self.find('.calendar-tooltip-input').val(data['label']);
                        // }
                        
                        if(self.closest('.input-tooltip-container').find('.label-button-div').length > 1){
                            self.closest('.input-tooltip-container').find('.minus-calendar-tooltip-row').show();
                        }
                        if(self.closest(".input-tooltip-container").find(".label-button-div").length>=2){
                            self.closest(".input-tooltip-container").find(".fa-minus").show();
                        }
                        self.closest('.content-dialog-scroll').perfectScrollbar("update");
                    },
                    "eleMinusTargetFnBefore":function(){
                        var self = $(this);
                        
                        if(self.closest(".input-tooltip-container").find(".label-button-div").length<=2){
                            self.closest(".input-tooltip-container").find(".fa-minus").hide();
                        }
                    },
                    "elePlusTargetFnBefore" : function(){
                        if($(".input-tooltip-container").find(".label-button-div").length >= 5){
                            showNotification({
                                message: "You've reach the maximum allowed rows in tooltip.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                            })
                            return false;
                        }
                    }
                }]
            });
            var body_data = $('body').data();
            var calendar_view = body_data['calendar_view'] ||"";
            var calendar_tooltip_values = calendar_view["calendar_tooltip_values"] ||"";
            if(calendar_tooltip_values.length > 0){
              // console.log(calendar_tooltip_values)
              for(var ctr in calendar_tooltip_values){
                var label = calendar_tooltip_values[ctr]['label'];
                var field = calendar_tooltip_values[ctr]['field'];
                // console.log("values",input_val)
                if(ctr != 0){
                    
                    $('.input-tooltip-container').find('.label-button-div:eq(0)').find('.add-calendar-tooltip-row').trigger('click',[{"label":label,"field":field}]);
                }
                // console.log(ctr,$('.input-tooltip-container').find('.label-button-div:eq('+ctr+')'))


                // if(label){
                    // $('.input-tooltip-container').find('.label-button-div:eq('+ctr+')').find('input.calendar-tooltip-input').val(label);
                // }
                // if(field){
                    // $('.input-tooltip-container').find('.label-button-div:eq('+ctr+')').find('select.calendar-tooltip-combo').val(field);
                // }
              }
              for(var ctr in calendar_tooltip_values){
                var label = calendar_tooltip_values[ctr]['label'];
                var field = calendar_tooltip_values[ctr]['field'];
                if(label){
                    $('.input-tooltip-container').find('.label-button-div:eq('+ctr+')').find('input.calendar-tooltip-input').val(label);
                }
                if(field){
                    $('.input-tooltip-container').find('.label-button-div:eq('+ctr+')').find('select.calendar-tooltip-combo').val(field);
                }
              }
            }

            $(".calendar_view_type").click(function(){
                self.calendar_type();
            })

            $("#include_calendar").click(function(){
                self.include_calendar()
                self.calendar_type();
            })
        })
    },
    "setCalendarView" : function(){
        var json_body = $("body").data();
        $(".setCalendarView").click(function(){
            var include_calendar = if_undefinded($("#include_calendar:checked").val(),"0");
            var calendar_view_type = if_undefinded($(".calendar_view_type:checked").val(),"0");
            var calendar_default_view = if_undefinded($("#calendar_default_view:checked").val(),"0");
            var calendar_enable_tooltip = if_undefinded($("#calendar_enable_tooltip:checked").val(),"0");
            var calendar_tooltip_values = [];
            var err = 0;
            if(include_calendar=="1"){
                json_body['include_calendar'] = 1;
            }else{
                json_body['include_calendar'] = 0;
                $("#popup_cancel").trigger("click");
                return false;
            }
            var single_date = $(".caldr-single-date").val();
            var start_date = $(".caldr-start-date").val();
            var end_date = $(".caldr-end-date").val();
            var calendar_view_display_title = $(".calendar_view_display_title").val();
            $('.label-button-div').each(function(){
                var label = $.trim($(this).find("input.calendar-tooltip-input").val());
                var field = $(this).find("select.calendar-tooltip-combo").val();
                if(label!=""){
                    calendar_tooltip_values.push({
                        "label":label,
                        "field":field
                    });
                }else{
                    err++;
                }
            });
            if(calendar_enable_tooltip=="1"){
                if(err>0){
                    showNotification({
                        message: "Please fill out tooltip label.",
                        type: "error",
                        autoClose: true,
                        duration: 3
                    })
                    return false;
                }
            }
            json_body['calendar_view'] = {
                "calendar_view_type":calendar_view_type,
                "single_date":single_date,
                "start_date":start_date,
                "end_date":end_date,
                "title":calendar_view_display_title,
                "default_view":calendar_default_view,
                "calendar_enable_tooltip":calendar_enable_tooltip,
                "calendar_tooltip_values": calendar_tooltip_values
            }

            $("#popup_cancel").trigger("click");
        })
        
        $("#calendar_enable_tooltip").click(function(){
            var val = $(this).filter(":checked").val();
            if(val=="1"){
                $("#calendar_tooltip_values").removeClass("isDisplayNone");
            }else{
                $("#calendar_tooltip_values").addClass("isDisplayNone");
            }
            $(this).closest(".content-dialog-scroll").perfectScrollbar("update");
        })
    },
    "include_calendar" : function(){
        // $("#include_calendar").click(function(){
            if($("#include_calendar").prop("checked")==true){
                $(".caldr-field").prop("disabled",false);
                $(".caldr-field").closest("label").css({
                    "opacity":1
                })
            }else{
                $(".caldr-field").prop("disabled",true);
                $(".caldr-field").closest("label").css({
                    "opacity":0.6
                })
            }
        // })
    },
    "calendar_type" : function(){
        // $(".calendar_view_type").click(function(){
            if($(".calendar_view_type:checked").val()==1){
                $(".calendar_view_type[value='2']").closest(".range_date").find("select").prop("disabled",true);

                $(".calendar_view_type[value='1']").closest(".fields_below").find("select").prop("disabled",false);
            }else{
                $(".calendar_view_type[value='1']").closest(".fields_below").find("select").prop("disabled",true);

                $(".calendar_view_type[value='2']").closest(".range_date").find("select").prop("disabled",false);
            }


            if($("#include_calendar").prop("checked")==false){
                $(".caldr-field").prop("disabled",true);
            }
        // })
    },
    "dateField" : function(calendar_field){
        var ret = '<option value="DateCreated" '+ setSelected('DateCreated',calendar_field) +'>Date Created</option>';
            ret += '<option value="DateUpdated" '+ setSelected('DateUpdated',calendar_field) +'>Date Updated</option>';
            $(".getFields[data-type='date'], .getFields[data-type='dateTime']").each(function(){
                var label = $(this).closest(".setObject").find(".obj_label").text().replace(":","");
                var value = $(this).attr("name").replace(":","");
                ret += '<option value="'+ value +'" '+ setSelected(value,calendar_field) +'>'+ label +'</option>';
            })
        return ret;
    },
    "getFieldsCalendar" : function(selected_vale){
        var ret = '<option value="TrackNo" '+ setSelected('TrackNo',selected_vale) +'>TrackNo</option>';
            ret += '<option value="requestorName" '+ setSelected('requestorName',selected_vale) +'>Requestor</option>';
            ret += '<option value="Status" '+ setSelected('Status',selected_vale) +'>Status</option>';
            ret += '<option value="DateCreated" '+ setSelected('DateCreated',selected_vale) +'>Date Created</option>';
            ret += '<option value="DateUpdated" '+ setSelected('DateUpdated',selected_vale) +'>Date Updated</option>';
            $(".setObject").each(function(){
                if (
                        $(this).attr("data-type") == "labelOnly" ||
                        $(this).attr("data-type") == "createLine" ||
                        $(this).attr("data-type") == "table" ||
                        $(this).attr("data-type") == "tab-panel" ||
                        // $(this).attr("data-type") == "listNames" || inalis ko to aaron para masama yung names sa tbfields -- JEWEL 
                        // $(this).attr("data-type") == "imageOnly" ||
                        $(this).is('[data-img="formImgDesign"]') ||
                        $(this).attr("data-type") == "button" ||
                        $(this).attr("data-type") == "embeded-view" ||
                        $(this).attr("data-type") == "accordion" ||
                        $(this).attr("data-type") == "details-panel"
                        ) {
                    return true; //preventing to insert it to the database
                }
                try{
                    var label = $(this).find(".obj_label").text().replace(":","");
                    var value = $(this).find(".getFields").attr("name").replace(":","");
                    ret += '<option value="'+ value +'" '+ setSelected(value,selected_vale) +'>'+ value.replace('[]','') +'</option>';
                }catch(e){
                    console.log(e)
                }
            })
        return ret;
    }
}