$(document).ready(function () {
    $("body").on("click", ".form-header-info", function () {
        if ($(".jdiag-header-info-container").length == 0) {
            var form_json = $("body").data();
            var defaultFieldsOpt = [
                {
                    "fieldLabel": "Date Created",
                    "fieldName": "DateCreated"
                },
                {
                    "fieldLabel": "Status",
                    "fieldName": "Status"
                },
                {
                    "fieldLabel": "Requestor",
                    "fieldName": "Requestor"
                },
                {
                    "fieldLabel": "Tracking Number",
                    "fieldName": "TrackNo"
                }
            ];
            defaultFieldsOpt = defaultFieldsOpt.reverse();
            var dialog_header_info
                    = '<div class="jdiag-header-info-container">' +
                    '<div class="header-info-header">' +
                    '<h3 class="fl-margin-bottom"><i class="icon-save"></i> Header Information</h3>' +
                    '<div class="hr"></div>' +
                    '</div>';
            dialog_header_info += '<div id="tab-container">';
            dialog_header_info += '    <ul>';
            dialog_header_info += '        <li><a href="#tabs-1">Header Information</a></li>';
            dialog_header_info += '        <li><a href="#tabs-2">Default Sorting</a></li>';
            dialog_header_info += '        <li><a href="#tabs-3">Highlight</a></li>';
            dialog_header_info += '    </ul>';
            dialog_header_info += ' <div id="tabs-1" style="padding:10px;">';
            dialog_header_info += ' 	<div class="header-info-body section clearing fl-field-style" style="margin:0px !important;">' +
                    //'<label><input type="checkbox" class="enable-header-info"/> Enable header information</label>'+
                    '<div class="fields column div_1_of_1">' +
                    '<span class="font-bold">Default order of columns</span> ' +
                    '<i class="fa fa-mobile" title="Available on mobile"></i>' +
                    '<div class="input_position" style="color:#ff0000;font-style:italic;margin-top:5px;margin-bottom: 5px;">' +
                    'Note: Drag the field up or down to rearrange' +
                    '</div>' +
                    '</div>' +
                    '<div class="column div_1_of_1"><label><input type="checkbox" class="header-info-check-all css-checkbox" id="header-info-check-all"/><label for="header-info-check-all" class="css-label"></label> Check all</label></div>' +
                    '<div class="hr"></div><br/><br/>' +
                    '<div id="form-field-list-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; overflow: hidden;height: 200px;">' +
                    '<ul class="content-dialog form-field-list-names" style="height: auto;">' +
                    '</ul>' +
                    '</div>' +
                    '</div>' +
                    '<div class="header-info-footer">' +
                    '<div class="fields">' +
                    '<div class="label_basic"></div>' +
                    '<div class="input_position" style="margin-top:5px;text-align:right;">' +
                    '<input type="button" class="btn-blueBtn save-header-info fl-positive-btn" value="OK" style="margin-right:8px;">' +
                    '<input type="button" class="btn-basicBtn cancel-header-info" value="Cancel">' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            dialog_header_info += ' <div id="tabs-2">' +
                    '<div class="fields section clearing fl-field-style">' +
                    '<div class="input_position column div_1_of_1">' +
                    '<span class="font-bold">Field: </span>';
            dialog_header_info += '<select class="input-select fieldDefaultSort" style="margin-top:0px;width: 100%;margin-right:0px;padding-top: 5px;">';
            //set up fields for default sorting
            {
                for (var j in defaultFieldsOpt) {
                    dialog_header_info += '<option value="' + defaultFieldsOpt[j]['fieldName'] + '">' + defaultFieldsOpt[j]['fieldLabel'] + '</option>';
                }

                $(".formbuilder_ws .setObject").each(function (eqindex) {
                    var self = $(this);
                    if (//SKIP THE NON INPUT FIELDS
                            self.attr("data-type") == "labelOnly" ||
                            self.attr("data-type") == "createLine" ||
                            self.attr("data-type") == "tab-panel" ||
                            self.attr("data-type") == "table" ||
                            self.attr("data-type") == "imageOnly" ||
                            self.attr("data-type") == "accordion"
                            ) {
                        return true;
                    }
                    var data_obj_id = self.attr("data-object-id");
                    var this_obj_label = self.find("#lbl_" + data_obj_id).text();
                    var field_name = self.find(".getFields_" + data_obj_id).attr("name");
                    dialog_header_info += '<option value="' + field_name + '">' + this_obj_label + '</option>';
                });
            }
            dialog_header_info += '</select>';
            dialog_header_info += '</div>' +
                    '</div>' +
                    '<div class="fields section clearing fl-field-style">' +
                    '<div class="input_position column div_1_of_1">' +
                    '<span class="font-bold">Sort Type: </span>';
            dialog_header_info += '<select class="input-select fieldDefaultSortType" style="margin-top:0px;width: 100%;margin-right:0px;padding-top: 5px;">';
            dialog_header_info += '<option value="DESC">Descending</option>';
            dialog_header_info += '<option value="ASC">Ascending</option>';
            dialog_header_info += '</select>' +
                    '</div>' +
                    '</div>';
            dialog_header_info += '</div>';

            dialog_header_info += ' <div id="tabs-3" class="view-settings-highlight-rule" style="padding:10px;">';

            { //for background
                dialog_header_info += '<div class="fields section clearing fl-field-style">';
                dialog_header_info += '<div class="input_position column div_1_of_1">';
                dialog_header_info += '<label><input value="true" style="vertical-align: middle;" type="checkbox" name="view-settings-allow_highlights" class="css-checkbox" id="allow_highlights"/><label for="allow_highlights" class="css-label"></label> Enable Background Highlight:</label><br/>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';

                dialog_header_info += '<div class="fields section clearing fl-field-style">';
                dialog_header_info += '<div class="input_position column div_1_of_5">';
                dialog_header_info += '<label><input value="cell" style="vertical-align: middle;" type="radio" name="view-settings-highlight_type" class="css-checkbox" id="highlight_type_cell"/><label for="highlight_type_cell" class="css-label"></label>Cell</label><br/>';
                dialog_header_info += '<label><input checked="checked" value="row" style="vertical-align: middle;" type="radio" name="view-settings-highlight_type" class="css-checkbox" id="highlight_type_row"/><label for="highlight_type_row" class="css-label"></label>Row</label><font color="red"></font>';
                dialog_header_info += ' </div>';

                dialog_header_info += '<div class="input_position column div_4_of_5">';
                dialog_header_info += '<div class="section clearing">';
                dialog_header_info += '<div class="input_position column div_1_of_1 view-settings-field-highlight-color-basis-container">';
                dialog_header_info += '<label>Select Field Reference Color:</label>';
                dialog_header_info += '<select class="form-select view-settings-field-highlight-color-basis" style=""></select>';
                dialog_header_info += '</div>';
                dialog_header_info += '<div class="input_position column div_1_of_2 view-settings-cell-highlight-color-basis-container" style="display:none;">';
                dialog_header_info += '<label>Select Target Cell Field:</label>';
                dialog_header_info += '<div class="vs-select-target-cell-psc" style="position:relative; overflow: auto; max-height: 116px; width: 100%; padding-right: 10px;">';
                dialog_header_info += '<div class="view-settings-cell-highlight-color-basis-fields">';
                dialog_header_info += '<div style="position: relative;"><span style="right: 20px; position: absolute; bottom: -29px;"><span class="add-cell-background"><i class="fa fa-plus cursor"></i></span>&nbsp;&nbsp;<span class="minus-cell-background" style="display:none;"><i class="fa fa-minus cursor"></i></span></span></div>';
                dialog_header_info += '<select class="form-select view-settings-cell-highlight-color-basis"></select>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';

            }


            { // for font
                dialog_header_info += '<div class="fields section clearing fl-field-style" style="">';
                dialog_header_info += '<div class="input_position column div_1_of_1">';
                dialog_header_info += '<label><input value="true" style="vertical-align: middle;" type="checkbox" name="view-settings-allow_font_highlights" class="css-checkbox" id="allow_font_highlights"/><label for="allow_font_highlights" class="css-label"></label> Enable Font Color:</label><br/>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';

                dialog_header_info += '<div class="fields section clearing fl-field-style" style="">';
                dialog_header_info += '<div class="input_position column div_1_of_5">';
                dialog_header_info += '<label><input value="cell" style="vertical-align: middle;" type="radio" name="view-settings-font-highlight_type" class="css-checkbox" id="highlight_font_type_cell"/><label for="highlight_font_type_cell" class="css-label"></label>Cell</label><br/>';
                dialog_header_info += '<label><input checked="checked" value="row" style="vertical-align: middle;" type="radio" name="view-settings-font-highlight_type" class="css-checkbox" id="highlight_font_type_row"/><label for="highlight_font_type_row" class="css-label"></label>Row</label><font color="red"></font>';
                dialog_header_info += ' </div>';

                dialog_header_info += '<div class="input_position column div_4_of_5">';
                dialog_header_info += '<div class="section clearing">';
                dialog_header_info += '<div class="input_position column div_1_of_1 view-settings-font-field-highlight-color-basis-container">';
                dialog_header_info += '<label>Select Field Reference Color:</label>';
                dialog_header_info += '<select class="form-select view-settings-font-field-highlight-color-basis" style=""></select>';
                dialog_header_info += '</div>';
                dialog_header_info += '<div class="input_position column div_1_of_2 view-settings-font-cell-highlight-color-basis-container" style="display:none;">';
                dialog_header_info += '<label>Select Target Cell Field:</label>';
                dialog_header_info += '<div class="vs-select-target-cell-psc" style="position:relative; overflow: auto; max-height: 116px; width: 100%; padding-right: 10px;">';
                dialog_header_info += '<div class="view-settings-font-cell-highlight-color-basis-fields">';
                dialog_header_info += '<div style="position: relative;"><span style="right: 20px; position: absolute; bottom: -29px;"><span class="add-cell-background"><i class="fa fa-plus cursor"></i></span>&nbsp;&nbsp;<span class="minus-cell-background" style="display:none;"><i class="fa fa-minus cursor"></i></span></span></div>';
                dialog_header_info += '<select class="form-select view-settings-font-cell-highlight-color-basis"></select>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';
                dialog_header_info += '</div>';
            }


            dialog_header_info += ' </div>';


            dialog_header_info += '</div>'; // ending of tab-container
            dialog_header_info += '</div>'; //ending of jdiag-header-info-container 

            var newDialog = new jDialog(dialog_header_info, '', '600', '', '80', function () {

            });
            newDialog.themeDialog("modal2");

            //HIGHLIGHT UI FUNCTIONALITY VERSION 2
            var highlight_rule_class = {
                "container": $('.view-settings-highlight-rule'),
                "init": function () {
                    var self = this;
                    self.reference_color_element = self.container.find('.view-settings-field-highlight-color-basis-container');
                    self.cell_target_element = self.container.find('.view-settings-cell-highlight-color-basis-container');
                    self.reference_color_element_font = self.container.find('.view-settings-font-field-highlight-color-basis-container');
                    self.cell_target_element_font = self.container.find('.view-settings-font-cell-highlight-color-basis-container');
                    self.add_minus_cell_background_element = self.container.find('.view-settings-cell-highlight-color-basis-fields');
                    self.add_minus_cell_font_color_element = self.container.find('.view-settings-font-cell-highlight-color-basis-fields');
                    self.vs_target_cell_psc = self.container.find('.vs-select-target-cell-psc');
                    self.highlight_settings = {
                        "viewHighlight_background": $('body').data('viewHighlight_background') || null,
                        "viewHighlight_font": $('body').data('viewHighlight_font') || null
                    };

                    var data_drop_down_collection = [
                        {'name': '', 'element_str': '<option value="0">None</option>'}
                    ];
                    var data_drop_down_collection_without_keyword = [];
                    var keywords_ele = [
                        {'name': 'TrackNo', 'element_str': '<option value="TrackNo">Tracking Number</option>'},
                        {'name': 'Requestor', 'element_str': '<option value="Requestor">Requestor</option>'},
                        {'name': 'Status', 'element_str': '<option value="Status">Status</option>'},
                        {'name': 'DateCreated', 'element_str': '<option value="DateCreated">DateCreated</option>'}
                    ];

                    self.BindEvents();

                    data_drop_down_collection = data_drop_down_collection.concat(keywords_ele);

                    GetValidFieldsSOBJC($('.workspacev2').find('.setObject')).each(function () {
                        var so_self = $(this);
                        var doi = so_self.attr("data-object-id");
                        var field_ele = $(this).find('[name].getFields_' + doi + ':eq(0)');
                        var field_name_str = (field_ele.attr('name') || '').replace('[]', '');
                        data_drop_down_collection.push({
                            'element_str': '<option value="' + field_name_str + '">' + field_name_str + '</option>',
                            'name': field_name_str
                        });
                    });

                    data_drop_down_collection = data_drop_down_collection.sort(function (a, b) {
                        return a['name'].localeCompare(b['name']);
                    }).map(function (v) {
                        if (v['name'] != "TrackNo" && v['name'] != "Requestor" && v['name'] != "Status" && v['name'] != "DateCreated") {
                            data_drop_down_collection_without_keyword.push(v['element_str']);
                        }
                        return v['element_str'];
                    });

                    self.cell_target_element.find('.view-settings-cell-highlight-color-basis').html(data_drop_down_collection.join(""));
                    self.reference_color_element.find('.view-settings-field-highlight-color-basis').html(data_drop_down_collection_without_keyword.join(""));
                    self.cell_target_element_font.find(".view-settings-font-cell-highlight-color-basis").html(data_drop_down_collection.join(""));
                    self.reference_color_element_font.find(".view-settings-font-field-highlight-color-basis").html(data_drop_down_collection_without_keyword.join(""));

                    self.RestoreSettings();
                },
                "BindEvents": function () {
                    var self = this;
                    self.container.find('[name="view-settings-highlight_type"]').on("change", function () {
                        var value = $(this).val();
                        if (value == "row") {
                            self.reference_color_element.removeClass("div_1_of_2").addClass("div_1_of_1");
                            self.cell_target_element.hide();
                        } else {
                            self.reference_color_element.removeClass("div_1_of_1").addClass("div_1_of_2");
                            self.cell_target_element.show();
                        }
                    });

                    self.container.find('[name="view-settings-font-highlight_type"]').on("change", function () {
                        var value = $(this).val();
                        if (value == "row") {
                            self.reference_color_element_font.removeClass("div_1_of_2").addClass("div_1_of_1");
                            self.cell_target_element_font.hide();
                        } else {
                            self.reference_color_element_font.removeClass("div_1_of_1").addClass("div_1_of_2");
                            self.cell_target_element_font.show();
                        }
                    });
                    self.vs_target_cell_psc.perfectScrollbar();
                    MakePlusPlusRowV2({
                        "tableElement": self.add_minus_cell_background_element,
                        "rules": [
                            {
                                "eleMinus": ">div>span>span.minus-cell-background",
                                "eleMinusTarget": ".view-settings-cell-highlight-color-basis-fields",
                                "eleMinusTargetFn": function (e, ui) {
                                    if (ui['parent_ele'].children('.view-settings-cell-highlight-color-basis-fields').length == 1) {
                                        ui['next_ele'].add(ui['prev_ele']).find('>div>span>span.minus-cell-background').hide();
                                    }
                                    self.vs_target_cell_psc.perfectScrollbar("update");
                                },
                                "eleMinusTargetFnBefore": "",
                                "eleMinusTargetFnAfter": "",
                                "elePlus": ">div>span>span.add-cell-background",
                                "elePlusTarget": ".view-settings-cell-highlight-color-basis-fields",
                                "elePlusTargetFn": function (e, ui) {
                                    if (ui['parent_ele'].children('.view-settings-cell-highlight-color-basis-fields').length >= 2) {
                                        ui['parent_ele'].children('.view-settings-cell-highlight-color-basis-fields').find('>div>span>span.minus-cell-background').show();
                                    }
                                    if (ui['parameter'].length >= 1) {
                                        $(this).find('.view-settings-cell-highlight-color-basis').val(ui['parameter'][0]);
                                    }
                                    self.vs_target_cell_psc.perfectScrollbar("update");
                                },
                                "elePlusTargetFnBefore": "",
                                "elePlusTargetFnAfter": ""
                            }
                        ]
                    });
                    MakePlusPlusRowV2({
                        "tableElement": self.add_minus_cell_font_color_element,
                        "rules": [
                            {
                                "eleMinus": ">div>span>span.minus-cell-background",
                                "eleMinusTarget": ".view-settings-font-cell-highlight-color-basis-fields",
                                "eleMinusTargetFn": function (e, ui) {
                                    if (ui['parent_ele'].children('.view-settings-font-cell-highlight-color-basis-fields').length == 1) {
                                        ui['next_ele'].add(ui['prev_ele']).find('>div>span>span.minus-cell-background').hide();
                                    }
                                    self.vs_target_cell_psc.perfectScrollbar("update");
                                },
                                "eleMinusTargetFnBefore": "",
                                "eleMinusTargetFnAfter": "",
                                "elePlus": ">div>span>span.add-cell-background",
                                "elePlusTarget": ".view-settings-font-cell-highlight-color-basis-fields",
                                "elePlusTargetFn": function (e, ui) {
                                    if (ui['parent_ele'].children('.view-settings-font-cell-highlight-color-basis-fields').length >= 2) {
                                        ui['parent_ele'].children('.view-settings-font-cell-highlight-color-basis-fields').find('>div>span>span.minus-cell-background').show();
                                    }
                                    if (ui['parameter'].length >= 1) {
                                        $(this).find('.view-settings-font-cell-highlight-color-basis').val(ui['parameter'][0]);
                                    }
                                    self.vs_target_cell_psc.perfectScrollbar("update");
                                },
                                "elePlusTargetFnBefore": "",
                                "elePlusTargetFnAfter": ""
                            }
                        ]
                    });
                },
                "RestoreSettings": function () {
                    var self = this;
                    if (self.highlight_settings['viewHighlight_background']) { //self.highlight_settings["viewHighlight_background"]
                        self.container.find('[name="view-settings-allow_highlights"]').prop('checked', self.highlight_settings['viewHighlight_background']['allow_highlights'] || false);
                        self.container.find('[name="view-settings-highlight_type"][value="' + (self.highlight_settings['viewHighlight_background']['highlight_type'] || 'row') + '"]')
                                .prop('checked', true).trigger("change");
                        self.container.find('.view-settings-field-highlight-color-basis').val(self.highlight_settings['viewHighlight_background']['field_color_basis'] || '0');
                        if (self.highlight_settings['viewHighlight_background']['cell_to_highlight'].length >= 1) {
                            var temp_add_element = self.container.find('.view-settings-cell-highlight-color-basis').parent().find('.add-cell-background:eq(0)');
                            self.highlight_settings['viewHighlight_background']['cell_to_highlight'] = self.highlight_settings['viewHighlight_background']['cell_to_highlight'].filter(function (v) {
                                return v != '0';
                            });
                            for (var key in self.highlight_settings['viewHighlight_background']['cell_to_highlight']) {
                                if (key == 0) {
                                    self.container.find('.view-settings-cell-highlight-color-basis').eq(key).val(self.highlight_settings['viewHighlight_background']['cell_to_highlight'][key] || '0');
                                } else {
                                    temp_add_element.trigger('click', [self.highlight_settings['viewHighlight_background']['cell_to_highlight'][key] || '0']);
                                }
                            }
                        }

                    }
                    if (self.highlight_settings['viewHighlight_font']) {
                        self.container.find('[name="view-settings-allow_font_highlights"]').prop('checked', self.highlight_settings['viewHighlight_font']['allow_highlights_font'] || false);
                        self.container.find('[name="view-settings-font-highlight_type"][value="' + (self.highlight_settings['viewHighlight_font']['highlight_type_font'] || 'row') + '"]')
                                .prop('checked', true).trigger("change");

                        self.container.find('.view-settings-font-field-highlight-color-basis').val(self.highlight_settings['viewHighlight_font']['field_color_basis_font'] || '0');

                        if (self.highlight_settings['viewHighlight_font']['cell_to_highlight_font'].length >= 1) {
                            var temp_add_element = self.container.find('.view-settings-font-cell-highlight-color-basis').parent().find('.add-cell-background:eq(0)');
                            self.highlight_settings['viewHighlight_font']['cell_to_highlight_font'] = self.highlight_settings['viewHighlight_font']['cell_to_highlight_font'].filter(function (v) {
                                return v != '0';
                            });
                            for (var key in self.highlight_settings['viewHighlight_font']['cell_to_highlight_font']) {
                                if (key == 0) {
                                    self.container.find('.view-settings-font-cell-highlight-color-basis').eq(key).val(self.highlight_settings['viewHighlight_font']['cell_to_highlight_font'][key] || '0');
                                } else {
                                    temp_add_element.trigger('click', [self.highlight_settings['viewHighlight_font']['cell_to_highlight_font'][key] || '0']);
                                }
                            }
                        }
                    }
                }
            }
            highlight_rule_class.init();


            //INITIALIZING PERFECT SCROLLBAR ... 
            $(".content-dialog-scroll").perfectScrollbar("update");
            $(".content-dialog-scroll").css("height", "200px");
            $("#tab-container").tabs();
            //ADDING LIST OF HEADER INFO ON THE FORM
            var append_location_list_field = $("#popup_container").find(".form-field-list-names").eq(0);
            var listedSortChoiceFlag = 0;
            var listedSortChoiceArr = [];
            var defaultFields = ["DateCreated", "Status", "Processor", "Requestor", "TrackNo"];

            //if has sorted header info
            if (typeof form_json.collected_data_header_info != "undefined") {
                if (typeof form_json.collected_data_header_info.listedSortChoice != "undefined") {
                    console.log("form_json.collected_data_header_info", form_json.collected_data_header_info);
                    var hasProcessor = false;
                    for (var index in form_json.collected_data_header_info.listedSortChoice) {
//                        console.log("jj",form_json.collected_data_header_info.listedSortChoice[index]);
                        if (form_json.collected_data_header_info.listedSortChoice[index]["field_name"] == "Processor") {
                            hasProcessor = true;
                        }
                    }

                    if (!hasProcessor) {
                        var processor_object = {
                            data_obj_id: 'Processor',
                            field_label: 'Processor',
                            field_name: 'Processor',
                            field_width: ''
                        }

                        form_json.collected_data_header_info.listedSortChoice.push(processor_object);
                    }
                    for (var ctr = 0; ctr < form_json.collected_data_header_info.listedSortChoice.length; ctr++) {
                        var saved_datas = form_json.collected_data_header_info.listedSortChoice[ctr];

                        var data_obj_id = saved_datas['data_obj_id'];

                        //saved header data
                        var this_obj_label = saved_datas['field_label'];
                        var field_name = saved_datas['field_name'];
                        var field_width = saved_datas['field_width'];


                        //updated header data
                        var workspace = $(".formbuilder_ws");
                        var this_obj_label_updated = workspace.find("#lbl_" + data_obj_id).text();
                        var field_name_updated = workspace.find(".getFields_" + data_obj_id).attr("name");

                        if (workspace.find("[id='" + data_obj_id + "']").length == 0) {
                            if (defaultFields.indexOf(data_obj_id) >= 0) {
                                this_obj_label_updated = this_obj_label;
                                field_name_updated = field_name;
                            }
                        }
                        if (field_name_updated == "undefined" || field_name_updated == undefined) {
                            continue;
                        }

                        console.log("field_name_updated", field_name_updated);
                        var append_list_info = $(setLayoutInLi(data_obj_id, field_name_updated, this_obj_label_updated)
                                );

                        //set the field_width
                        append_list_info.find('.header-info-field-width').val(field_width);
                        append_location_list_field.append(append_list_info);
                        listedSortChoiceArr.push(data_obj_id);
                    }

                    //for newly added field / skip if existed in array
                    $(".formbuilder_ws .setObject").each(function (eqindex) {
                        var self = $(this);
                        if (//SKIP THE NON INPUT FIELDS
                                self.attr("data-type") == "labelOnly" ||
                                self.attr("data-type") == "createLine" ||
                                self.attr("data-type") == "tab-panel" ||
                                self.attr("data-type") == "table" ||
                                self.attr("data-type") == "imageOnly" ||
                                self.attr("data-type") == "button" ||
                                self.attr("data-type") == "embeded-view" ||
                                self.attr("data-type") == "accordion" ||
                                self.attr("data-type") == "details-panel"
                                ) {
                            return true;
                        }
                        var data_obj_id = self.attr("data-object-id");
                        var this_obj_label = self.find("#lbl_" + data_obj_id).text();
                        var field_name = self.find(".getFields_" + data_obj_id).attr("name");
                        //if existing skip
                        if (listedSortChoiceArr.indexOf(data_obj_id) >= 0) {
                            return true;
                        }

                        var append_list_info = $(setLayoutInLi(data_obj_id, field_name, this_obj_label)
                                );
                        append_location_list_field.append(append_list_info);
                    });
                    listedSortChoiceFlag++;
                }
            }

            //if do not have any sorting
            if (listedSortChoiceFlag == 0) {
                $(".formbuilder_ws .setObject").each(function (eqindex) {
                    var self = $(this);
                    if (//SKIP THE NON INPUT FIELDS
                            self.attr("data-type") == "labelOnly" ||
                            self.attr("data-type") == "createLine" ||
                            self.attr("data-type") == "tab-panel" ||
                            self.attr("data-type") == "table" ||
                            self.attr("data-type") == "imageOnly" ||
                            self.attr("data-type") == "button" ||
                            self.attr("data-type") == "embeded-view" ||
                            self.attr("data-type") == "accordion" ||
                            self.attr("data-type") == "details-panel"
                            ) {
                        return true;
                    }
                    var data_obj_id = self.attr("data-object-id");
                    var this_obj_label = self.find("#lbl_" + data_obj_id).text();
                    var field_name = self.find(".getFields_" + data_obj_id).attr("name");
                    var append_list_info = $(setLayoutInLi(data_obj_id, field_name, this_obj_label)
                            );
                    append_location_list_field.append(append_list_info);
                });
            }
            $(".content-dialog-scroll").perfectScrollbar("update");
            $(".content-dialog-scroll").css("height", "200px");

            FormsHeaderInfo.init();
            //RESTORING SAVED VALUES
            if (typeof form_json.collected_data_header_info != "undefined") {
                if (typeof form_json.collected_data_header_info.listedchoice != "undefined") { //THIS TYPEOF CONDITION ABLES US TO SECURE THE EXISTENCE OF THE SPECIFIC DATA BASED ON THE CONDITION
                    for (var ctr = 0; ctr < form_json.collected_data_header_info.listedchoice.length; ctr++) {
                        var saved_datas = form_json.collected_data_header_info.listedchoice[ctr];
                        if (//I JUST DID THIS KIND OF CONDITION TO SECURE THE DATA INEGRITY OR SECURE THE ACCURATENESS OF RESTORING THE DATA OF A SAVED STATE
                                // $(".header-info-field").eq(saved_datas.checkbox_index).length >= 1 &&
                                // $(".header-info-field").eq(saved_datas.checkbox_index).closest("[header-info-object-id]").attr("header-info-object-id") == saved_datas.data_obj_id &&
                                // $(".header-info-field").eq(saved_datas.checkbox_index).closest("[header-info-object-id]").children(".header-info-label").text() == saved_datas.field_label
                                // $("label[header-info-object-id='"+saved_datas.data_obj_id+"']").eq(0).length >= 1 &&
                                // $("label[header-info-object-id='"+saved_datas.data_obj_id+"']").eq(0).find(".header-info-label").text() == saved_datas.field_label
                                $("label[header-info-object-id='" + saved_datas.data_obj_id + "']").eq(0).length >= 1
                                ) {
                            //$(".header-info-field").eq(saved_datas.checkbox_index).prop("checked",true); //RESTORED
                            $("label[header-info-object-id='" + saved_datas.data_obj_id + "']").eq(0).find(".header-info-field[type='checkbox']").eq(0).prop("checked", true); //RESTORED
                        }
                    }
                    var total_checkbox = $(".form-field-list-names").find(".header-info-field").length;
                    var total_checked = $(".form-field-list-names").find(".header-info-field:checked").length;
                    if (total_checkbox == total_checked && total_checkbox != 0) {
                        $(".header-info-check-all").prop("checked", true);
                    } else {
                        $(".header-info-check-all").prop("checked", false);
                    }
                }
            }

            //CHECK ALL CHECKBOX
            $(".header-info-check-all").on({
                "click": function () {
                    var is_checked = $(this).is(":checked");
                    var self = $(this);
                    if (is_checked) {
                        self.closest(".header-info-body").find(".header-info-field").prop("checked", true);
                    } else {
                        self.closest(".header-info-body").find(".header-info-field").prop("checked", false);
                    }
                }
            });

            //CANCEL BUTTON
            $(".cancel-header-info").on({
                "click": function () {
                    $(this).closest("#popup_container").remove();
                    $("#popup_overlay").remove();
                }
            });

            //SAVING BUTTON
            $(".save-header-info").on({
                "click": function () {
                    var this_json = $("body").data();
                    var self = $(this);
                    collect_checked_header_info = {
                        listedchoice: [],
                        listedSortChoice: []
                                //,enable_header_info:$(".enable-header-info").is(":checked")
                    };

                    listed_choices_items = self.closest("#popup_container").find("label[header-info-object-id]");
                    console.log(listed_choices_items)
                    listed_choices_items.each(function (eqindex) {
                        var self = $(this);
                        if (self.children(".header-info-field[type='checkbox']").prop("checked") == true) {
                            collect_checked_header_info.listedchoice.push({
                                //"checkbox_index":eqindex,
                                "data_obj_id": self.attr("header-info-object-id"),
                                "field_label": self.find(".header-info-label").text(),
                                "field_name": self.find("[data-name]").val(),
                                "field_width": self.next().find(".header-info-field-width").val()
                            });
                        }
                        collect_checked_header_info.listedSortChoice.push({
                            //"checkbox_index":eqindex,
                            "data_obj_id": self.attr("header-info-object-id"),
                            "field_label": self.find(".header-info-label").text(),
                            "field_name": self.find("[data-name]").val(),
                            "field_width": self.next().find(".header-info-field-width").val()
                        });
                    });
                    var allowReorder = "0";
                    if ($(".allowReorder").prop("checked") == true) {
                        allowReorder = "1";
                    }

                    var allowColumnBorder = "";
                    if ($(".allowColumnBorder").prop("checked") == true) {
                        allowColumnBorder = "app-view-border-right";
                    }

                    var allowGeneralSearch = "0";
                    if ($("#allowGeneralSearch").prop("checked") == true) {
                        allowGeneralSearch = "1";
                    }

                    var fieldDefaultSort = $(".fieldDefaultSort").val();
                    var fieldDefaultSortType = $(".fieldDefaultSortType").val();


                    //for View highlight background
                    var viewHighlight_background = {};
                    var allow_highlights = $("#allow_highlights").prop("checked");
                    var highlight_type = $('[name="view-settings-highlight_type"]:checked').val();
                    var field_color_basis = $(".view-settings-field-highlight-color-basis").val();
                    var cell_to_highlight = [];

                    $(".view-settings-cell-highlight-color-basis").each(function () {
                        if ($(this).val() != "0") {
                            cell_to_highlight.push($(this).val());
                        }
                    });

                    viewHighlight_background['allow_highlights'] = allow_highlights;
                    viewHighlight_background['highlight_type'] = highlight_type;
                    viewHighlight_background['field_color_basis'] = field_color_basis;
                    viewHighlight_background['cell_to_highlight'] = cell_to_highlight;

                    //for view highlight font
                    var viewHighlight_font = {};
                    var allow_highlights_font = $("#allow_font_highlights").prop("checked");
                    var highlight_type_font = $('[name="view-settings-font-highlight_type"]:checked').val();
                    var field_color_basis_font = $(".view-settings-font-field-highlight-color-basis").val();
                    var cell_to_highlight_font = [];

                    $(".view-settings-font-cell-highlight-color-basis").each(function () {
                        if ($(this).val() != "0") {
                            cell_to_highlight_font.push($(this).val());
                        }
                    });


                    viewHighlight_font['allow_highlights_font'] = allow_highlights_font;
                    viewHighlight_font['highlight_type_font'] = highlight_type_font;
                    viewHighlight_font['field_color_basis_font'] = field_color_basis_font;
                    viewHighlight_font['cell_to_highlight_font'] = cell_to_highlight_font;

                    this_json.collected_data_header_info = collect_checked_header_info;
                    this_json.allowReorder = allowReorder;
                    this_json.allowGeneralSearch = allowGeneralSearch;
                    this_json.allowColumnBorder = allowColumnBorder;
                    this_json.fieldDefaultSort = fieldDefaultSort;
                    this_json.fieldDefaultSortType = fieldDefaultSortType;
                    this_json.headerInfoType = $(".headerInfoType:checked").val();
                    this_json.viewHighlight_background = viewHighlight_background;
                    this_json.viewHighlight_font = viewHighlight_font;

                    //				showNotification({
                    //                    message: "Header Saved!",
                    //                    type: "success",
                    //                    autoClose: true,
                    //                    duration: 3
                    //                });
                    $('.fl-closeDialog').click();
                }
            });

            //CHECK INDIVIDUAL CHECKBOX
            $(".header-info-field").on({
                "click": function () {
                    var is_checked = $(this).is(":checked");
                    var total_checkbox = $(".form-field-list-names").find(".header-info-field").length;
                    var total_checked = $(".form-field-list-names").find(".header-info-field:checked").length;
                    if (total_checkbox == total_checked) {
                        $(".header-info-check-all").prop("checked", true);
                    } else {
                        $(".header-info-check-all").prop("checked", false);
                    }
                }
            })
        }
    });
});

//Header info
FormsHeaderInfo = {
    init: function () {
        // return; 
        this.setFormHeaderTypeHtml();
        this.setDefaultHeaderInFormFieldsHeader();
        this.setFormHeaderType();
        this.setFormHeaderTypeFields();

        this.setAllowReorder();
        this.setAllowColumnBorder();
        this.setAllowGeneralSearch();


        this.setFieldDefaultSort();
        /******RULES FOR CUSTOM HEADER INFO*/
        /*	For old and new forms*/
        //	- if the form do not have any header info, it means that the field in request will be the default fields(track no, requestor, status, date create);
        /*	For old forms*/
        //  - if the form do not have any settings for default or specific view, the view shall retain to its original view
    },
    setFormHeaderTypeHtml: function () {
        var htmlPrepend = '<div class="fields section clearing fl-field-style">' +
                '<div class="input_position column div_1_of_2">' +
                '<label class="font-bold">' +
                '<input type="radio" name="headerInfoType" class="headerInfoType css-checkbox" value="0" style="margin-right:8px;" id="headerInfoType0">' +
                '<label for="headerInfoType0" class="css-label"></label>' +
                'Default Header ' +
                '<i class="fa fa-mobile" title="Available on mobile"></i>' +
                '</label>' +
                '</div>' +
                '<div class="input_position column div_1_of_2">' +
                '<label class="font-bold">' +
                '<input type="radio" name="headerInfoType" class="headerInfoType css-checkbox" value="1" style="margin-right:8px;" id="headerInfoType1">' +
                '<label for="headerInfoType1" class="css-label"></label>' +
                'Specific Header ' +
                '<i class="fa fa-mobile" title="Available on mobile"></i>' +
                '</label>' +
                '</div>' +
                '</div>' +
                // '<div class="fields" style="margin-left:10px;">'+
                // 	'<div class="label_basic"></div>'+
                // 	'<div class="input_position" style="margin-top:5px;">'+
                // 		'<label style="font-weight: bold;font-size: 10px;">'+
                // 		'<input type="radio" name="headerInfoType" class="headerInfoType css-checkbox" value="1" style="margin-right:8px;" id="headerInfoType1">'+
                // 			'<label for="headerInfoType1" class="css-label"></label>'+
                // 			'Specific Header'+
                // 		'</label>'+
                // 	'</div>'+
                // '</div>'+
                '<div class="fields section clearing fl-field-style">' +
                '<div class="input_position column div_1_of_2">' +
                '<label class="font-bold">' +
                '<input type="checkbox" name="allowReorder" class="allowReorder css-checkbox" value="1" style="margin-right:8px;" id="allowReorder">' +
                '<label for="allowReorder" class="css-label"></label>' +
                ' Allow user to reorder column' +
                '</label>' +
                '</div>' +
                '<div class="input_position column div_1_of_2">' +
                '<label class="font-bold">' +
                '<input type="checkbox" name="allowColumnBorder" class="allowColumnBorder css-checkbox" value="1" style="margin-right:8px;" id="allowColumnBorder">' +
                '<label for="allowColumnBorder" class="css-label"></label>' +
                ' Allow Column Border' +
                '</label>' +
                '</div>' +
                '</div>' +
                '<div class="fields section clearing fl-field-style">' +
                '<div class="input_position column div_1_of_2">' +
                '<label class="font-bold">' +
                '<input type="checkbox" name="allowGeneralSearch" class="allowGeneralSearch css-checkbox" value="1" style="margin-right:8px;" id="allowGeneralSearch">' +
                '<label for="allowGeneralSearch" class="css-label"></label>' +
                ' General search' +
                '</label>' +
                '</div>' +
                '</div>';


        $(".header-info-body").before(htmlPrepend);
        // $(".header-info-body").css({
        // 	"margin-left": "20px",
        // 	"margin-top": "20px",
        // })
    },
    setDefaultHeaderInFormFieldsHeader: function () {
        var append_location_list_field = $("#popup_container").find(".form-field-list-names");
        // var defaultFields = ['Tracking Number','Requestor','Status','Date Created']
        var form_json = $("body").data();
        var listedSortChoiceFlag = 0;
        var defaultFields = [
            {
                "fieldLabel": "Date Created",
                "fieldName": "DateCreated"
            },
            {
                "fieldLabel": "Status",
                "fieldName": "Status"
            },
            {
                "fieldLabel": "Processor",
                "fieldName": "Processor"
            },
            {
                "fieldLabel": "Requestor",
                "fieldName": "Requestor"
            },
            {
                "fieldLabel": "Tracking Number",
                "fieldName": "TrackNo"
            }
        ];
        //if do not have any sorting
        if (typeof form_json.collected_data_header_info != "undefined") {
            if (typeof form_json.collected_data_header_info.listedSortChoice != "undefined") {
                listedSortChoiceFlag++;
            }
        }

        if (listedSortChoiceFlag == 0) {
            for (var j in defaultFields) {
                //var append_list_info = $(
                //	'<li style="background-color: #e0e0e0;padding: 10px;margin-bottom:3px;cursor:move;"><label header-info-object-id="'+defaultFields[j]['fieldName']+'">'+
                //		'<input type="checkbox" class="header-info-field css-checkbox" data-name="'+defaultFields[j]['fieldName']+'" value="'+defaultFields[j]['fieldName']+'" id="'+defaultFields[j]['fieldName']+'"/><label for="'+defaultFields[j]['fieldName']+'" class="css-label"></label> <span class="header-info-label">'+defaultFields[j]['fieldLabel']+'</span>'+
                //	'</label></li>'
                //);
                var append_list_info = $(
                        setLayoutInLi(defaultFields[j]['fieldName'], defaultFields[j]['fieldName'], defaultFields[j]['fieldLabel'])
                        );
                append_location_list_field.prepend(append_list_info);
            }
        }
        // console.log(append_location_list_field)
    },
    setFormHeaderType: function () {
        var self = this;
        var formId = $("#FormId").val();
        var form_json = $("body").data();
        var headerInfoType = form_json['headerInfoType'];

        //Form is not yet created
        if (typeof headerInfoType === undefined) {
            self.setheaderTypeFieldsPropDisable(0)
            return;
        }


        var form_json = $("body").data();
        var headerInfoType = form_json['headerInfoType'];




        if (typeof headerInfoType == "undefined") {
            if (typeof form_json.collected_data_header_info != "undefined") {
                if (form_json.collected_data_header_info.listedchoice.length == 0) { // no header info has been set
                    self.setheaderTypeFieldsPropDisable(0)
                } else {
                    self.setheaderTypeFieldsPropDisable(1)
                }
            } else {
                self.setheaderTypeFieldsPropDisable(0)
            }
        } else {
            self.setheaderTypeFieldsPropDisable(headerInfoType)
        }
    },
    setFormHeaderTypeFields: function () {
        var self = this;
        $(".headerInfoType").change(function () {
            var headerInfoType = $(this).val();
            self.setheaderTypeFieldsPropDisable(headerInfoType);
        })
    },
    setheaderTypeFieldsPropDisable: function (headerInfoType) {
        if (headerInfoType == 1) {
            $(".header-info-field, .header-info-check-all").attr("disabled", false);
            $(".header-info-field, .header-info-check-all").attr("disabled", false);
            $(".header-info-field, .header-info-check-all").closest("label").css("opacity", false);
            $(".content-dialog").sortable({
                "axis": "y"
            });
        } else {
            $(".header-info-field, .header-info-check-all").attr("disabled", "disabled");
            $(".header-info-field, .header-info-check-all").closest("label").css("opacity", "0.3");
            if ($(".content-dialog").hasClass("ui-sortable")) {
                $(".content-dialog").sortable("destroy");
            }
        }
        $(".headerInfoType[value='" + headerInfoType + "']").attr("checked", "checked");
    },
    setAllowReorder: function () {
        var form_json = $("body").data();
        if (form_json['allowReorder'] != undefined && form_json['allowReorder'] != "undefined") {
            if (form_json['allowReorder'] == "1") {
                $(".allowReorder").prop("checked", true);
            } else {
                $(".allowReorder").prop("checked", false);
            }
        } else {
            $(".allowReorder").prop("checked", true);
        }
    },
    setAllowGeneralSearch: function () {
        var form_json = $("body").data();
        if (form_json['allowGeneralSearch'] != undefined && form_json['allowGeneralSearch'] != "undefined") {
            if (form_json['allowGeneralSearch'] == "1") {
                $("#allowGeneralSearch").prop("checked", true);
            } else {
                $("#allowGeneralSearch").prop("checked", false);
            }
        } else {
            $("#allowGeneralSearch").prop("checked", true);
        }
    },
    setAllowColumnBorder: function () {
        var form_json = $("body").data();
        if (form_json['allowColumnBorder'] != undefined && form_json['allowColumnBorder'] != "undefined") {
            if (form_json['allowColumnBorder'] == "app-view-border-right") {
                $(".allowColumnBorder").prop("checked", true);
            } else {
                $(".allowColumnBorder").prop("checked", false);
            }
        } else {
            $(".allowColumnBorder").prop("checked", false);
        }
    },
    setFieldDefaultSort: function () {
        var form_json = $("body").data();
        if (form_json['fieldDefaultSort'] != undefined && form_json['fieldDefaultSort'] != "undefined") {
            if ($(".fieldDefaultSort").find("option[value='" + form_json['fieldDefaultSort'] + "']").length > 0) {
                $(".fieldDefaultSort").val(form_json['fieldDefaultSort'])
            }
        }
        if (form_json['fieldDefaultSortType'] != undefined && form_json['fieldDefaultSortType'] != "undefined") {
            $(".fieldDefaultSortType").val(form_json['fieldDefaultSortType'])
        }
    }
}

function setLayoutInLi(data_obj_id, field_name, this_obj_label) {
    this_obj_label = this_obj_label.replace(/[^a-zA-Z0-9\s]/g, '');
    var default_val = 100;
    return '<li style="background-color: #e0e0e0;padding: 10px;margin-bottom:3px;cursor:move;"><label header-info-object-id="' + data_obj_id + '">' +
            '<input type="checkbox" class="header-info-field css-checkbox" data-name="' + field_name + '" id="field-' + data_obj_id + '" value="' + field_name + '"/> ' +
            '<label for="field-' + data_obj_id + '" class="css-label"></label>' +
            '<span class="header-info-label">' + this_obj_label + '</span>' +
            '</label>' +
            '<label style="float: right;position: relative;height: 25px;top: -4px;width: 137px;display: inline-block;"><span><pre>Width:&nbsp;</pre></span><input type="text" class="form-text header-info-field-width" style="/* float:right; *//* position: relative; */height: 25px;/* top: -4px; */width: 80px; text-align:right;" value="' + default_val + '"><span>&nbsp;px</span></label>' +
            '</li>';
}