$(document).ready(function(){
	MobilitySetupV2.init();
	return;
});
var MobilitySetupV2 = {};

MobilitySetupV2.MOBILITY_SETTINGS_VER = '3.0.4';

MobilitySetupV2.init = function () {
	var self = this;

	self.castInitialEvents()
	.then(function(createdMobileDialog){
		self.InitializeMobileProperty(createdMobileDialog);
		self.castDialogEvents(createdMobileDialog);
		self.loadFormObjects(createdMobileDialog);
	});
};
MobilitySetupV2.createMobileDialog = function(){
	var self = this;
	var is_enabled = "";
	var tooltip_active_mobility = "Activate mobility on this form.";
	if($('body').data('is_active_mobility')){
		is_enabled = " is-enabled";
		tooltip_active_mobility = "Deactivate mobility on this form."
	}
	var ret
	=	'<div class="mobility-overlay" title="click the overlay background to close the dialog" style="">'+
			'<div class="mobility-content-middle" style="">'+
				
				'<div class="mobility-content-center" style="">'+
					'<div class="mobile-dialog-advance-settings mobile-general-property" style="width:0px;height:0px; margin-left: -480px;">'+
						'<div class="mobility-content-middle mobility-dialog-content mobility-field-dialog-container">'+
							'<div class="mobility-dialog-content2 mobility-field-dialog-header">Mobile Property v'+self.MOBILITY_SETTINGS_VER+'<input class="isDisplayNone" type="button" class="btn-basicBtn borderR3" value="OK" style="right: 10px; position: absolute; width: 39px;"></div>'+
							'<div class="mobility-dialog-content mobility-field-dialog-body" style="overflow:hidden;background-color: rgba(255,255,255,1);">'+
								'<div class="tab-mobile-properties" style="width: 100%; height: 100%; margin: 0px; border: 0px; padding: 0px;">'+
									'<ul>'+
										'<li><a href="#general-tab-property">General Property</a></li>'+
									'</ul>'+
									'<div id="general-tab-property" class="tab-content-scrollable" style="position:relative;overflow:auto; height: calc(100% - 49px);">'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
					'<div class="mobile-dialog-advance-settings mobile-unused-property" style="display: none; width: 0px; height: 0px; margin-left: 340px;">'+
						'<div class="mobility-content-middle mobility-dialog-content mobility-field-dialog-container">'+
							'<div class="mobility-dialog-content2 mobility-field-dialog-header">Unused Fields<input class="isDisplayNone" type="button" class="btn-basicBtn borderR3" value="OK" style="right: 10px; position: absolute; width: 39px;"></div>'+
							'<div class="mobility-dialog-content mobility-field-dialog-body" style="overflow:hidden;background-color: rgba(255,255,255,1);">'+
								'<div class="tab-mobile-properties" style="width: 100%; height: 100%; margin: 0px; border: 0px; padding: 0px;">'+
									'<ul>'+
										'<li class="isDisplayNone"><a href="#unused-fields">Unused Fields</a></li>'+
									'</ul>'+
									'<div style="position:relative; height: 100%;">'+
										'<ul id="unused-fields" class="unusedFields" style="width: calc(100% - 2.8em); height: calc(100% - 2em);">'+

										'</ul>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
					// '<div class="mobile-dialog-advance-settings mobile-unused-field-property isDisplayNone" style="width:0px;height:0px; margin-left: 345px;">'+
					// 	'<div class="mobility-content-middle mobility-dialog-content mobility-field-dialog-container">'+
					// 		'<div class="mobility-dialog-content2 mobility-field-dialog-header">TEST HEADER</div>'+
					// 		'<div class="mobility-dialog-content mobility-field-dialog-body">'+
					// 			'<div class="mobility-dialog-content2 mobility-field-element"></div>'+
					// 			'<div class="mobility-dialog-content2 mobility-field-element"></div>'+
					// 			'<div class="mobility-dialog-content2 mobility-field-element"></div>'+
					// 			'<div class="mobility-dialog-content2 mobility-field-element"></div>'+
					// 			'<div class="mobility-dialog-content2 mobility-field-element"></div>'+
					// 		'</div>'+
					// 	'</div>'+
					// '</div>'+
					// '<span >'+
						// '<input type="checkbox" name="enable-mobility" id="enable-mobility" class="css-yes-no mobility-toggle-on-off">'+
         			// '<label for="enable-mobility" class="css-label-yes-no"></label>'+
     				// '</span>'+
     				'<div class="mobility-close-container tip mobility-general-property isDisplayNone" data-original-title="Set property." style="left: 201px;"><span class="fa fa-cog" style="color:white;"></span></div>'+
  					'<div class="mobility-close-container tip mobility-reset-elements isDisplayNone" data-original-title="Reset to recently loaded fields." style="left: 233px;"><span class="fa fa-refresh" style="color:white;"></span></div>'+
  					'<div class="mobility-close-container tip mobility-unused-property" data-original-title="See unused fields" style="left: 232px;"><span class="fa fa-exchange" style="color:white; vertical-align: middle !important; font-size: 18px; height: 23px;"></span></div>'+
  					'<div class="mobility-close-container tip container-is-active-toggle'+is_enabled+'" data-original-title="'+tooltip_active_mobility+'" style="left: 265px;"><span class="fa fa-power-off" style="color:white;"></span></div>'+

					'<div class="mobility-close-container tip" data-original-title="Close dialog." style=""><span class="fa fa-times" style="color:white;"></span></div>'+
					'<div class="mobility-header" style=""><div class="head-design" style="color:white;text-align:center;"></div></div>'+
					'<div class="mobility-body" style="padding:0px;">'+
						'<div class="mobility-emulator-header" style="height: 35px;position: relative;"><div style="position: fixed;width: 263px;height: 25px;z-index: 101;background-color: #ff9000;color: white;padding: 5px;">'+
							'<span class="mobile-form-header-icon" style="width: 25px;height: 100%;display: inline-block;" data-id="svg-icon-fbuilder-01"><svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title=""><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-01"></use></svg></span>'+
						'</div></div>'+
						'<ul id="sortable-mobile" style=""></ul>'+
					'</div>'+
					'<div class="mobility-footer">'+
						'<div class="fields" style="">'+
							// '<i  class="fa fa-ban mobility-toggle-on-off" disable-mobility-form="false"></i>'+
							'<input type="button" class=" mobility-save" data-form-type="formbuilder" id="mobility-save" value="OK" style="width: calc(100% - 45px);">'+
							// '<i  class="fa fa-check-circle mobility-toggle-on-off" enable-mobility-form="true"></i>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	return ret;
};
MobilitySetupV2.InitializeMobileProperty = function(dialog){
	var componentProperty = {
		temp:null,
		init:function(d){
			this.temp = $('body').data('saving_request_format')||{};
			this.UITemplate(d);
			this.formIcon(d);
			this.UIPlugins(d);
			this.bindSave(d);
			this.recordAppearance(d);
			this.restoreSavedProperty(d);
		},
		UITemplate:function(dialog){
			dialog.find('[id="general-tab-property"]').prepend(
				'<div class="fields_below">'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span><label><input type="checkbox" value="yes" name="mobile-form-hide-navigation-icon"/> Hide Navigation Icon</label></span>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="fields_below">'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span class="choose-form-icon">Choose Form Icon: <a><i class="fa fa-photo"></i></a></span>'+
							'<div class="content-dialog" style="padding: 0px;overflow: hidden;">    <div class="section clearing">                    <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-01" id="svg-icon-fbuilder-01">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-01"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-02" id="svg-icon-fbuilder-02">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-02"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-03" id="svg-icon-fbuilder-03">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-03"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-04" id="svg-icon-fbuilder-04">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-04"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-05" id="svg-icon-fbuilder-05">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-05"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-06" id="svg-icon-fbuilder-06">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-06"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-07" id="svg-icon-fbuilder-07">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-07"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-08" id="svg-icon-fbuilder-08">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-08"></use>                    </svg>                </div>            </div>                                    </div>            <div class="section clearing">                                <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-09" id="svg-icon-fbuilder-09">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-09"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-10" id="svg-icon-fbuilder-10">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-10"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-11" id="svg-icon-fbuilder-11">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-11"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-12" id="svg-icon-fbuilder-12">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-12"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-13" id="svg-icon-fbuilder-13">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-13"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-14" id="svg-icon-fbuilder-14">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-14"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-15" id="svg-icon-fbuilder-15">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-15"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-16" id="svg-icon-fbuilder-16">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-16"></use>                    </svg>                </div>            </div>                                    </div>            <div class="section clearing">                                <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-17" id="svg-icon-fbuilder-17">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-17"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-18" id="svg-icon-fbuilder-18">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-18"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-19" id="svg-icon-fbuilder-19">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-19"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-20" id="svg-icon-fbuilder-20">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-20"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper " id="">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-21"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-22" id="svg-icon-fbuilder-22">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-22"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-23" id="svg-icon-fbuilder-23">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-23"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-24" id="svg-icon-fbuilder-24">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-24"></use>                    </svg>                </div>            </div>                                    </div>            <div class="section clearing">                                <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-25" id="svg-icon-fbuilder-25">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-25"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-26" id="svg-icon-fbuilder-26">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-26"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-27" id="svg-icon-fbuilder-27">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-27"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-28" id="svg-icon-fbuilder-28">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-28"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-29" id="svg-icon-fbuilder-29">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-29"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-30" id="svg-icon-fbuilder-30">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-30"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-31" id="svg-icon-fbuilder-31">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-31"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-32" id="svg-icon-fbuilder-32">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-32"></use>                    </svg>                </div>            </div>                                    </div>            <div class="section clearing">                                <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-33" id="svg-icon-fbuilder-33">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-33"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-34" id="svg-icon-fbuilder-34">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-34"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-35" id="svg-icon-fbuilder-35">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-35"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-36" id="svg-icon-fbuilder-36">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-36"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-37" id="svg-icon-fbuilder-37">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-37"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-38" id="svg-icon-fbuilder-38">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-38"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-39" id="svg-icon-fbuilder-39">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-39"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-40" id="svg-icon-fbuilder-40">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-40"></use>                    </svg>                </div>            </div>                                    </div>            <div class="section clearing">                                <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-41" id="svg-icon-fbuilder-41">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-41"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-42" id="svg-icon-fbuilder-42">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-42"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-43" id="svg-icon-fbuilder-43">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-43"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-44" id="svg-icon-fbuilder-44">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-44"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-45" id="svg-icon-fbuilder-45">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-45"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-46" id="svg-icon-fbuilder-46">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-46"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-47" id="svg-icon-fbuilder-47">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-47"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-48" id="svg-icon-fbuilder-48">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-48"></use>                    </svg>                </div>            </div>                                    </div>            <div class="section clearing">                                <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-49" id="svg-icon-fbuilder-49">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-49"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-50" id="svg-icon-fbuilder-50">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-50"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-51" id="svg-icon-fbuilder-51">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-51"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-52" id="svg-icon-fbuilder-52">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-52"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-53" id="svg-icon-fbuilder-53">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-53"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-54" id="svg-icon-fbuilder-54">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-54"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-55" id="svg-icon-fbuilder-55">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-55"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-56" id="svg-icon-fbuilder-56">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-56"></use>                    </svg>                </div>            </div>                                    </div>            <div class="section clearing">                                <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-57" id="svg-icon-fbuilder-57">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-57"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-58" id="svg-icon-fbuilder-58">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-58"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-59" id="svg-icon-fbuilder-59">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-59"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-60" id="svg-icon-fbuilder-60">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-60"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-61" id="svg-icon-fbuilder-61">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-61"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-62" id="svg-icon-fbuilder-62">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-62"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-63" id="svg-icon-fbuilder-63">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-63"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-64" id="svg-icon-fbuilder-64">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-64"></use>                    </svg>                </div>            </div>                                    </div>            <div class="section clearing">                                <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-65" id="svg-icon-fbuilder-65">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-65"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-66" id="svg-icon-fbuilder-66">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-66"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-67" id="svg-icon-fbuilder-67">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-67"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-68" id="svg-icon-fbuilder-68">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-68"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-69" id="svg-icon-fbuilder-69">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-69"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-70" id="svg-icon-fbuilder-70">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-70"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-71" id="svg-icon-fbuilder-71">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-71"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-72" id="svg-icon-fbuilder-72">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-72"></use>                    </svg>                </div>            </div>                                    </div>            <div class="section clearing">                                <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-73" id="svg-icon-fbuilder-73">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-73"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-74" id="svg-icon-fbuilder-74">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-74"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-75" id="svg-icon-fbuilder-75">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-75"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-76" id="svg-icon-fbuilder-76">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-76"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-77" id="svg-icon-fbuilder-77">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-77"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-78" id="svg-icon-fbuilder-78">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-78"></use>                    </svg>                </div>            </div>                                            <div class="column div_1_of_8 svg-icon-fbuilder-wrapper">                <div class="custom_icon_wrapper svg-icon-fbuilder-79" id="svg-icon-fbuilder-79">                    <svg style="width:auto;height:auto;" class="icon-svg svg-icon-fbuilder tip" viewBox="-1 -1 91.264 90.597" data-original-title="" title="">                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-fbuilder-79"></use>                    </svg>                </div>            </div>                                    </div><div class="clearfix"></div></div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="fields_below">'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span>Form Security: <label><input type="radio" value="yes" name="mobile-form-security"/> Yes</label> <label><input type="radio" value="no" checked="checked" name="mobile-form-security"/> No</label></span>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="fields_below">'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span>Include Formula List: <label><input type="checkbox" name="formula-list-toggle"/></label><br/>'+
								'<label><input type="checkbox" name="onload"/> Onload</label><br/>'+
								'<label><input type="checkbox" name="onchange"/> Onchange</label><br/>'+
								// '<label class=""><input type="checkbox" name="postsubmit"/> Post submission</label><br/>'+
								// '<label class=""><input type="checkbox" name="presubmit"/> Pre submission</label>'+
							'</span>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'<div class="fields_below">'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span>Geolocation: <label><input type="radio" value="yes" name="mobile-form-geolocation"/> Yes</label> <label><input type="radio" value="no" checked="checked" name="mobile-form-geolocation"/> No</label></span>'+
						'</div>'+
					'</div>'+
				'</div>'+

				'<div class="fields_below">'+
					'<div class="input_position_below section clearing fl-field-style">'+
						'<div class="column div_1_of_1">'+
							'<span><label><input type="checkbox" value="yes" name="mobile-form-request-header"/> Record appearance settings</label></span><br/><br/>'+
							'<div><label>Record appearance: <select class="form-select" name="mobile-form-request-header-appearance"><option value="1">Default</option><option value="2">Design 1</option></select></label></div>'+
							'<div><label>Record title: <select class="form-select" name="mobile-form-request-header-title"><option value="default">Default</option></select></label></div>'+
							'<div><label>Record image: <select class="form-select" name="mobile-form-request-header-image"><option value="none">None</option><option value="default">Default</option></select></label></div>'+
							'<div><label>Record field caption: <select class="form-select" name="mobile-form-request-header-field-caption"><option value="1">Show</option><option value="2">Hide</option></select></label></div>'+
						'</div>'+
					'</div>'+
				'</div>'
			);
		},
		formIcon:function(dialog){
			dialog.find('.custom_icon_wrapper').on('click',function(){
				dialog.find('.mobile-form-header-icon').attr('data-id', $(this).attr('id')).html($(this).html());
			});
		},
		UIPlugins:function(dialog){
			dialog.find('.tab-mobile-properties').tabs();
			dialog.find('.tab-content-scrollable').perfectScrollbar();
		},
		recordAppearanceVersion1:function(dialog){
			var fieldNames = $('.formbuilder_ws').find('.getFields[name]').get().map(function(v){
				return $(v).attr('name');
			});
			var optionTemplate = "";
			fieldNames = $.unique(fieldNames);
			fieldNames = fieldNames.sort();

			for(fieldNamesIndex in fieldNames){ optionTemplate = '<option value="'+fieldNames[fieldNamesIndex]+'">'+fieldNames[fieldNamesIndex]+'</option>'; }

			optionTemplate = $(optionTemplate);
			dialog.find('[name="mobile-form-request-header-title"]').append(optionTemplate.clone());
		},
		recordAppearance:function(dialog){
			var optionTemplate = "";
			var optionData = [];
			var fieldNames = [];
			var notFields = ['labelOnly', 'embeded-view', 'tab-panel', 'accordion', 'createLine'];
			$('.formbuilder_ws').find('.setObject[data-object-id][data-type]:not([data-type="'+notFields.join('"]):not([data-type="')+'"])').each(function(){
				var temp = $(this).find('.getFields[name]');
				if($.inArray(temp.attr('name') , fieldNames) === -1){
					fieldNames.push(temp.attr('name'));
					optionData.push({
						name: temp.attr('name').replace('[]',''),
						objectType: $(this).is('[data-type="imageOnly"][data-upload="photo_upload"]')?'requestImage':$(this).attr('data-type'),
					});
				}
			});
			optionData = optionData.sort(function(a,b){ return a.name.localeCompare(b.name); });

			for(optionDataIndex in optionData){ optionTemplate += '<option object-type="'+optionData[optionDataIndex].objectType+'" value="'+optionData[optionDataIndex].name+'">'+optionData[optionDataIndex].name+'</option>'; }

			optionTemplate = $(optionTemplate);

			dialog.find('[name="mobile-form-request-header-title"]').append(optionTemplate.clone());
			dialog.find('[name="mobile-form-request-header-image"]').append(optionTemplate.filter('[object-type="requestImage"]').clone());
		},
		bindSave:function(dialog){
			dialog.find('[id="mobility-save"]').data('mobile-properties-data',function(){
				return {
					"mobile_form_hide_navigation_icon":dialog.find('[name="mobile-form-hide-navigation-icon"]').is(':checked')?"yes":"no",
					"mobile_form_icon_id":dialog.find('.mobile-form-header-icon:eq(0)').attr('data-id'),
					"mobile_form_security":dialog.find('[name="mobile-form-security"]:checked').val(),
					"mobile_form_geolocation":dialog.find('[name="mobile-form-geolocation"]:checked').val(),
					"mobile_form_formula": {
						"allow_mobile_formula": dialog.find('[name="formula-list-toggle"]').is(':checked')?"yes":"no",
						"onchange": {
							"is_allow": dialog.find('[name="onchange"]').is(':checked')?"yes":"no"
						},
						"onload": {
							"is_allow": dialog.find('[name="onload"]').is(':checked')?"yes":"no", 
							"formula": ""
						},
						"postsubmit":{
							"is_allow":dialog.find('[name="postsubmit"]').is(':checked')?"yes":"no",
							"formula": ""
						},
						"presubmit":{
							"is_allow":dialog.find('[name="presubmit"]').is(':checked')?"yes":"no",
							"formula":""
						}
					},
					"mobile_form_request_header":{
						"enable":dialog.find('[name="mobile-form-request-header"]').is(':checked')?'yes':'no',
						"appearance":dialog.find('[name="mobile-form-request-header-appearance"] option:selected').val(),
						"title":dialog.find('[name="mobile-form-request-header-title"] option:selected').val(),
						"image":dialog.find('[name="mobile-form-request-header-image"] option:selected').val(),
						"field_caption":dialog.find('[name="mobile-form-request-header-field-caption"] option:selected').val(),
					}
				};
			});
		},
		restoreSavedProperty:function(dialog){
			var temp = this.temp;
			if(temp.mobile_form_properties){
				if(temp.mobile_form_properties.mobile_form_icon_id){
					dialog.find('.custom_icon_wrapper[id="'+temp.mobile_form_properties.mobile_form_icon_id+'"]').trigger('click');
				}
				if(temp.mobile_form_properties.mobile_form_security){
					dialog.find('[name="mobile-form-security"][value="'+temp.mobile_form_properties.mobile_form_security+'"]').prop('checked',true);
				}
				if(temp.mobile_form_properties.mobile_form_formula){
					if(temp.mobile_form_properties.mobile_form_formula.allow_mobile_formula){
						dialog.find('[name="formula-list-toggle"]').prop('checked', true);
					}
					if(temp .mobile_form_properties .mobile_form_formula .onchange){
						if(temp .mobile_form_properties .mobile_form_formula .onchange .is_allow == "yes"){
							dialog.find('[name="onchange"]').prop('checked', true);
						}
					}
					if(temp .mobile_form_properties .mobile_form_formula .onload){
						if(temp .mobile_form_properties .mobile_form_formula .onload .is_allow == "yes"){
							dialog.find('[name="onload"]').prop('checked', true);
						}
					}
					if(temp .mobile_form_properties .mobile_form_formula .postsubmit){
						if(temp .mobile_form_properties .mobile_form_formula .postsubmit .is_allow == "yes"){
							dialog.find('[name="postsubmit"]').prop('checked', true);
						}
					}
					if(temp .mobile_form_properties .mobile_form_formula .presubmit){
						if(temp .mobile_form_properties .mobile_form_formula .presubmit .is_allow == "yes"){
							dialog.find('[name="presubmit"]').prop('checked', true);
						}
					}
				}
				if(temp.mobile_form_properties.mobile_form_geolocation){
					dialog.find('[name="mobile-form-geolocation"][value="'+temp.mobile_form_properties.mobile_form_geolocation+'"]').prop('checked', true);
				}
				if(temp.mobile_form_properties.mobile_form_hide_navigation_icon){
					dialog.find('[name="mobile-form-hide-navigation-icon"][value="'+temp.mobile_form_properties.mobile_form_hide_navigation_icon+'"]').prop('checked', true);
				}
				if(temp.mobile_form_properties.mobile_form_request_header){
					if(temp.mobile_form_properties.mobile_form_request_header.enable == "yes"){
						dialog.find('[name="mobile-form-request-header"]').prop('checked', true);
					}
					if(temp.mobile_form_properties.mobile_form_request_header.appearance){
						dialog.find('[name="mobile-form-request-header-appearance"] option[value="'+temp.mobile_form_properties.mobile_form_request_header.appearance+'"]').prop('selected',true);
					}
					if(temp.mobile_form_properties.mobile_form_request_header.title){
						dialog.find('[name="mobile-form-request-header-title"] option[value="'+temp.mobile_form_properties.mobile_form_request_header.title+'"]').prop('selected',true);
					}
					if(temp.mobile_form_properties.mobile_form_request_header.image){
						dialog.find('[name="mobile-form-request-header-image"] option[value="'+temp.mobile_form_properties.mobile_form_request_header.image+'"]').prop('selected',true);
					}
					if(temp.mobile_form_properties.mobile_form_request_header.field_caption){
						dialog.find('[name="mobile-form-request-header-field-caption"] option[value="'+temp.mobile_form_properties.mobile_form_request_header.field_caption+'"]').prop('selected',true);
					}
				}
			}
		}
	};
	componentProperty.init(dialog);
	
	

	//FROM QA REQUEST THE ICON SETTINGS MUST BE DISPLAYED RIGHT AWAY
	// dialog.find('[id="general-tab-property"]').find('.choose-form-icon').on('click',function(){
	// 	if($(this)
	// 			// .parent()
	// 			.next().is('.isDisplayNone')){
	// 		$(this)
	// 			// .parent()
	// 			.next().removeClass('isDisplayNone');
	// 	}else{
	// 		$(this)
	// 			// .parent()
	// 			.next().addClass('isDisplayNone');
	// 	}
	// });
	
};
MobilitySetupV2.castInitialEvents = function(){
	var self = this;
	var defer = {};
	$("body").on("click",".mobility-setup",function (a,b) {
		if($(".mobility-overlay").length <= 0){
			var createdMobileDialog = $(self.createMobileDialog());
			$("body").append(createdMobileDialog);
			if(defer.callBack){
				defer.callBack(createdMobileDialog);
			}
		}
	});
	defer.then = function(cb){
		this.callBack = cb;
	};
	return defer;
};
MobilitySetupV2.castDialogEvents = function(dialog){
	var self = this;
	var saveProcess = self.okSave();
	var dialogSector = {};
	dialogSector.closeOverlay = dialog.filter(".mobility-overlay");
	dialogSector.toolTips  = dialog.find('.tip');
	dialogSector.mainButton = dialog.find(".mobility-close-container");
	dialogSector.mobilityBody = dialog.find('.mobility-body');
	dialogSector.sortableContents = dialogSector.mobilityBody.eq(0).children("#sortable-mobile").add($('#unused-fields'));
	dialogSector.save = dialog.find("#mobility-save.mobility-save");
	//PERFECT SCROLL BAR
	dialogSector.sortableContents.perfectScrollbar({
		wheelSpeed: 1,
		wheelPropagation: false,
		suppressScrollX: true,
    });
    dialogSector.sortableContents.perfectScrollbar('update');
    dialogSector.sortableContents.find('.ps-scrollbar-y-rail').css({
    	'z-index':100,
    	'width':'3px'
    }).find('.ps-scrollbar-y').css({
    	'width':'3px'
    });
	//CLOSE DIALOG
	dialogSector.closeOverlay.click(function(e){
		if (e.target === this || e.target === $(this).children(".mobility-content-middle")[0] ){
	        //outer
	        $(this).fadeOut(function(){
	        	$(this).remove();
	        });
	    }else{
	    	// inner
	    }
	});
	//TOOLTIP
	dialogSector.toolTips.tooltip();
	//CLOSE / ENABLE or DISABLE
	dialogSector.mainButton.on({
		"click":function(e){
			var self = $(this);
			if(self.is('.container-is-active-toggle')){
				if(self.is('.is-enabled')){
					self.removeClass('is-enabled');
					self.attr("data-original-title","Activate mobility on this form.");
				}else{
					self.addClass('is-enabled');
					self.attr("data-original-title","Deactivate mobility on this form.");
				}
			}else if(self.is('.mobility-unused-property')){
				if($('.mobile-unused-property').children(':eq(0)').is(':visible')){
					$('.mobile-unused-property').hide();
				}else{
					$('.mobile-unused-property').show();
				}
				
			}else if(self.is('.mobility-reset-elements')){
				ResetLastSaved();
			}else if(self.is('.mobility-general-property')){
				if($('.mobile-dialog-advance-settings.mobile-general-property').is('.isDisplayNone')){
					$('.mobile-dialog-advance-settings.mobile-general-property').removeClass('isDisplayNone');
				}else{
					$('.mobile-dialog-advance-settings.mobile-general-property').addClass('isDisplayNone');
				}
			}else{
				$(this).parents(".mobility-overlay").eq(0).trigger("click");
			}
			$('.tooltip.fade.top.in:visible').remove()
		},
		"mouseenter":function(e){
			var self = $(this);
			$(this).children("span.fa").css({
				"color":"black",
				"text-shadow":"0px 0px 10px white",
				"transition":"ease 0.5s"
			});
			self.addClass('is-hovered');
			// if(self.is('.container-is-active-toggle') || self.is('.mobility-reset-elements')){

			// }else{
			// 	$(this).css({
			// 		"box-shadow":"0px 0px 10px white inset, 0px 0px 10px white",
			// 		"background-color":"rgb(100,100,100)"
			// 	})
			// }
			
			var cloned = $(this).clone();
			$(this).parent().prepend(cloned);
			
			// alert(Number(cloned.css("top").replace("px","")) - 5);
			cloned.css({
				"background-color":"rgba(100,100,100,0)",
				"border":"2px solid black",
				"opacity":"0.8"
			}).animate({
				top: Number(cloned.css("top").replace("px","")) - 5, //- 9,
				left: Number(cloned.css("left").replace("px","")) - 5, // - 292,
				width: Number(cloned.css("width").replace("px","")) + 10, //34,
				height: Number(cloned.css("height").replace("px","")) + 10,
			},function(){
				$(this).fadeOut(function(){
					$(this).remove();
				})
			});
			// var cloned_times = $(this).children("span.fa.fa-times").clone();
			// $(this).prepend(cloned_times)
			// cloned_times.css({
			// 	"position":"absolute"
			// })
			/*.children("span.fa.fa-times").css({
				"color":"black"
			})*/

		},
		"mouseleave":function(){
			var self = $(this);
			$(this).children("span.fa").css({
				"color":"white",
				"text-shadow":""
			});
			if(self.is('.container-is-active-toggle') || self.is('.mobility-reset-elements')){
				self.removeClass('is-hovered');
			}else{
				self.removeClass('is-hovered');
				$(this).css({
					"box-shadow":"",
					"background-color":"black"
				});
			}
		}
	});
	//SORTABLE CONTENTS
	dialogSector.sortableContents.sortable({
		connectWith: ".unusedFields",
		// containment: "parent",
		containment: ".mobility-body",
		items: 'li.element-mobile',
		axis: 'y',
		// cursorAt: { top:0, left: 0 },
		tolerance: "pointer",
		start: function(e, ui){
			$(this).sortable( "refreshPositions" );
			$(this).sortable("option","dragHeight",$(ui.item).outerHeight());
			$(ui.item).css({
				"opacity":"0.8",
				"height":"30px",
			});
		},
		stop: function(e, ui){
			dialogSector.sortableContents.perfectScrollbar('update');
			$(ui.item).css({
				"opacity":"",
				"height":($(this).sortable("option","dragHeight"))+"px"
			});
		},
		over: function(e, ui){
			console.log("MOBLITY SORTABLE",ui,this);
			$(this).sortable( "refreshPositions" );
		},
		// helper: function(e, ui) {
	        // var helper = $(ui.item).clone(); // Untested - I create my helper using other means...
	        // // jquery.ui.sortable will override width of class unless we set the style explicitly.
	        // helper.css({'height': '30px'});
	        // return helper;
	    // },
		// revert:true,
		placeholder:"sortable-placeholder"
	}).disableSelection();

	dialogSector.save.on(saveProcess[0],saveProcess[2]);
};

MobilitySetupV2.fieldUICleanUp = function(ind,fieldsObject){
	fieldsObject = $(fieldsObject);
	var objID = fieldsObject.attr('data-object-id');
	var objectData = $('body').data(objID)||{};

	fieldsObject.css({
		'z-index':''
	});

	if(objectData.lblAlignment == 'alignLeft'){
		var eleLabelField = fieldsObject.find("#label_" + objID).add(fieldsObject.find("#obj_fields_" + objID));
		var ele_left_wrapper = eleLabelField.parents(".align-left-wrapper").eq(0);
		if (ele_left_wrapper.length >= 1) {
		    ele_left_wrapper.after(eleLabelField);
		    ele_left_wrapper.remove();
		    eleLabelField.each(function () {
		        $(this).children(".ui-resizable-handle").remove();
		    });
		}
		fieldsObject.find("#label_" + objID).find(".lbl-aligned-left-wrap").eq(0).children("*").unwrap();
		fieldsObject.find("#obj_fields_" + objID).find(".obj_f-aligned-left-wrap").eq(0).children("*").unwrap();
		fieldsObject.find("#label_" + objID).removeClass("lbl-aligned-left");
		fieldsObject.find("#label_" + objID).css("width", "");
		fieldsObject.find("#obj_fields_" + objID).removeClass("obj_f-aligned-left");
	}

	fieldsObject.children('.ui-resizable-handle').remove();

	return fieldsObject.get(0);
};

MobilitySetupV2.loadFormObjects = function(dialog){
	var self = this;
	var container_elements = dialog.find('[id="sortable-mobile"],[id="unused-fields"].unusedFields:eq(0)');
	var unusedFields = $('body').data('mobility_unused_fields')||[];
	var fieldSequence = $('body').data('saving_request_format')||{};
	var unusedFieldElements = $();
	var supportedFields = ["textbox", "textArea", "radioButton", "checkbox", "dropdown", "datepicker", "dateTime", "time", "attachment_on_request", "contactNumber", "qr-code", "barCodeScanner","pickList","embeded-view","textbox_reader_support","textbox_editor_support"]; //,"pickList","embeded-view","textbox_reader_support","textbox_editor_support"
	var supportedSelectors = ['[data-type="imageOnly"]:not([data-img="formImgDesign"])','[data-type="attachment_on_request"]'];
	var removeEleButton = $('<div class="container-mobility-remove-field-element" style="position:absolute;z-index:5;width:100%;height:0;overflow:visible;text-align:right;"><i class="mobility-remove-field-element fl-delete fa fa-times" style="opacity:1"></i></div>');
	var eleMobileWrapper = '<li class="element-mobile" style="top:0px;left:0px;margin-bottom:5px;position:relative;display:block;width:100%;"></li>';
	
	//version that excludes containers
	// var formObjects = $('.formbuilder_ws').children('.setObject[data-object-id]').filter(function(){
	// 	if( $(this).is( '[data-type="'+supportedFields.join('"],[data-type="')+'"],'+supportedSelectors.join(",") ) ){
	// 		if(unusedFields.indexOf($(this).attr('data-object-id')||'') >= 0){
	// 			unusedFieldElements = unusedFieldElements.add($(this).clone());
	// 			return false;
	// 		}
	// 		return true;
	// 	}
	// 	return false;
	// }).clone();

	var formObjects = $('.formbuilder_ws').find('.setObject[data-object-id]').filter(supportedSelectors.join(',')+',[data-type="'+supportedFields.join('"],[data-type="')+'"]').filter(function(){
		if(unusedFields.indexOf($(this).attr('data-object-id')||'') >= 0){
			unusedFieldElements = unusedFieldElements.add($(this).clone().map(MobilitySetupV2.fieldUICleanUp));
			return false;
		}
		return true;
	}).clone().map(MobilitySetupV2.fieldUICleanUp);

	if( (fieldSequence.fields||[]).length >= 1 ){
		var addedFieldsFromForm = '';
		var tempFieldSequence = $(fieldSequence.fields.map(function(a){
			addedFieldsFromForm += ':not([name="'+a.id+'"]:eq(0)):not([embed-name="'+a.id+'"]:eq(0)):not([name="'+a.id+'[]"]:eq(0))';
			return formObjects.filter(function(){
				return $(this).find('[name="'+a.id+'"]:eq(0),[embed-name="'+a.id+'"]:eq(0),[name="'+a.id+'[]"]:eq(0)').length >= 1;
			}).get(0);
		}).filter(Boolean));
		// console.log("HOW ABOUT", tempFieldSequence);
		formObjects = tempFieldSequence.add(formObjects.filter(function(){
			return $(this).find('.getFields:eq(0)'+addedFieldsFromForm).length >= 1;
		}));

		// formObjects = formObjects.sort(function(a,b){
		// 	var aTemp = $(a).find('.getFields:eq(0)');
		// 	var bTemp = $(b).find('.getFields:eq(0)');
		// 	var aTempIndex = tempFieldSequence.indexOf(aTemp.attr('name')||aTemp.attr('embed-name')||'');
		// 	var bTempIndex = tempFieldSequence.indexOf(bTemp.attr('name')||bTemp.attr('embed-name')||'');
		// 	var aTempFSV = tempFieldSequence[aTempIndex];
		// 	var bTempFSV = tempFieldSequence[bTempIndex];

		// 	if(aTempIndex < 0){
		// 		tempFieldSequence[aTempIndex] = bTempFSV;
		// 		tempFieldSequence[bTempIndex] = aTempFSV;
		// 		return -1;
		// 	}
		// 	if(bTempIndex < 0){
		// 		tempFieldSequence[aTempIndex] = aTempFSV;
		// 		tempFieldSequence[bTempIndex] = bTempFSV;
		// 		return 1;
		// 	}

		// 	if(aTempIndex < bTempIndex){
		// 		tempFieldSequence[aTempIndex] = bTempFSV;
		// 		tempFieldSequence[bTempIndex] = aTempFSV;
		// 	}
		// 	if(aTempIndex > bTempIndex){
		// 		tempFieldSequence[aTempIndex] = aTempFSV;
		// 		tempFieldSequence[bTempIndex] = bTempFSV;
		// 	}
		// 	// var aTemp = formObjects.get().indexOf(a);
		// 	// var bTemp = formObjects.get().indexOf(b);
		// 	// if( aTemp < tempFieldSequence.indexOf(a) ){
		// 	// 	return - 1;
		// 	// }
		// 	// if( bTemp < tempFieldSequence.indexOf(b) ){
		// 	// 	return - 1;
		// 	// }
		// 	return aTempIndex - bTempIndex;
		// });
	}

	formObjects.add(unusedFieldElements).css({
		'top':'',
		'left':'',
		'width':'auto',
		'position':''
	});
	formObjects.add(unusedFieldElements).removeClass("component-primary-focus");
	formObjects.add(unusedFieldElements).removeClass("component-ancillary-focus");
	formObjects.add(unusedFieldElements).filter('[data-type="labelOnly"]').find('.obj_label').children('label').css({
		'font-size':16,
		'line-height':'16px'
	});
	console.log("formObjects", formObjects);
	container_elements.filter('[id="sortable-mobile"]').prepend(formObjects);
	container_elements.filter('[id="unused-fields"].unusedFields:eq(0)').prepend(unusedFieldElements);
	formObjects.add(unusedFieldElements).wrap(eleMobileWrapper);

	//REMOVE TO UNUSED FIELDS
	formObjects.add(unusedFieldElements).each(function(){
		var temp = removeEleButton.clone();
		$(this).before(temp);
		temp.on({
			'click.removeAppend':function(){
				var _this = $(this);
				setTimeout(function(){
					if(_this.parents('[id="unused-fields"]:eq(0)').length <= 0){
						_this.parents('.element-mobile').eq(0).data('parentOrigin',_this.parents('.element-mobile').eq(0).parent());
						// _this.parents('.element-mobile').eq(0).appendTo($('[id="unused-fields"].unusedFields:eq(0)'));
						$('[id="unused-fields"].unusedFields:eq(0)').prepend(_this.parents('.element-mobile').eq(0));
						$('[id="unused-fields"].unusedFields:eq(0)').perfectScrollbar('update');
					}else{
						// _this.parents('.element-mobile').eq(0).appendTo(_this.parents('.element-mobile').eq(0).data('parentOrigin')||$('[id="sortable-mobile"]:eq(0)'));
						(_this.parents('.element-mobile').eq(0).data('parentOrigin')||$('[id="sortable-mobile"]:eq(0)')).prepend(_this.parents('.element-mobile').eq(0));
						(_this.parents('.element-mobile').eq(0).data('parentOrigin')||$('[id="sortable-mobile"]:eq(0)')).perfectScrollbar('update');
					}
					// _this.parents('.element-mobile').eq(0).remove();
				},0);
			}
		});
		// if(unusedFields.indexOf($(this).attr('data-object-id')||'') >= 0){
		// 	temp.hide();
		// 	setTimeout(function(){
		// 		temp.trigger('click.removeAppend');
		// 		temp.show();
		// 	},0);
		// 	// $(this).parent().appendTo($('[id="unused-fields"].unusedFields'));
		// }
	});
	formObjects.add(unusedFieldElements).filter('[data-type="embeded-view"]').each(function(){
		$(this).find('.getFields:eq(0)').css({
			"height":'',
			"width":'',
			"min-width":'',
		});
		$(this).find('.embed_newRequest').remove();
		$(this).find('.getFields.embed-view-container').before('<input type="button" class="" value="Show Embedded Form" style="width: 100%;">');
		$(this).css('height', '');
		// $(this).find('.getFields:eq(0)').parent().prepend($(this).find('.embed_newRequest').clone());
		// // $(this).find('.getFields:eq(0)').parent().children('.embed_newRequest:eq(0)').text('add');
		// $(this).find('.getFields:eq(0)').parent().children('.embed_newRequest:eq(1)').text("View");
		// $(this).find('.getFields:eq(0)').parent().children('.embed_newRequest').css('margin-bottom','5px');
		// $(this).find('.getFields:eq(0)').parent().children('.embed_newRequest').css('margin-top','5px');
		// $(this).find('.getFields:eq(0)').parent().children('.embed_newRequest').css('margin-left','5px');
		// // $(this).find('.getFields:eq(0)').parent().children('.embed_newRequest').removeClass('embed_newRequest');
		// // $(this).find('.getFields:eq(0)').remove();
		$(this).css('height', '');
	});

	formObjects.find('.pickListButton').removeClass('pickListButton');
	//MOVE TO UNUSED FIELDS 
	// formObjects.filter(function(){
	// 	return unusedFields.indexOf($(this).attr('data-object-id')||'') >= 0;
	// }).parent().appendTo($('[id="unused-fields"].unusedFields'));
};

MobilitySetupV2.okSave = function () {
	return ["click","#mobility-save.mobility-save",function(){
		var mobility_workspace = $("body").data();
		mobility_workspace.saving_request_format = {};
		mobility_workspace.saving_request_format.fields = [];
		mobility_workspace.saving_request_format.mobile_settings_version = MobilitySetupV2.MOBILITY_SETTINGS_VER;
		var mobilityDynamicFields = [];
		var mobilityLocationContent = $("#mobility-save.mobility-save").closest(".mobility-content-center").children(".mobility-body").eq(0);
		// getting fieldss
		if(mobilityLocationContent.children("ul#sortable-mobile").children(".element-mobile").length >= 1){
			console.log("MOBILITY SAVING")
			mobilityLocationContent.children("ul#sortable-mobile").children(".element-mobile").each(function(){
				var set_obj_ele = $(this).children(".setObject");
				var object_index_id = set_obj_ele.attr("data-object-id");
				var fields_below = set_obj_ele.children(".fields_below");
				var get_fields_and_containers_ele = $(this).find(".getFields_"+object_index_id).eq(0);
				
					if($(this).hasClass("mobDynamicTable")){ //NASA DYNAMIC TABLE
						
						mobility_workspace.saving_request_format.fields.push({
							"id": $(this).attr("data-mob-dynamicTable-id"),
							"label": $(this).find(".obj_label").eq(0).text(),
							"type": "REPEATER"
						});
						var keyFieldDynamic = 0;
						$(this).find(".setObject").each(function(){
							var set_obj_ele = $(this);
							var object_index_id = set_obj_ele.attr("data-object-id");
							var fields_below = set_obj_ele.children(".fields_below");
							var get_fields_and_containers_ele = set_obj_ele.find(".getFields_"+object_index_id).eq(0);
							if(get_fields_and_containers_ele.length <= 0){
								if(set_obj_ele.is('[data-upload="photo_upload"]')){
									get_fields_and_containers_ele = set_obj_ele.find(".getFields_"+object_index_id+"[id='getFields_"+object_index_id+"'][name]").eq(0);
									mobilityDynamicFields.push({
										"id": set_obj_ele.find('.getFields_'+object_index_id+'[id="getFields_'+object_index_id+'"][name]').attr('name'),
										"label": "",
										"generation": "REQUESTOR",
									});
								}else{
									get_fields_and_containers_ele = $(this).find(".getFields_"+object_index_id).eq(0);
								}
							}else if(get_fields_and_containers_ele.is('[name]')){
								mobilityDynamicFields.push({
									"id": get_fields_and_containers_ele.attr("name").replace("[]",""),
				                    "label": fields_below.find("#label_"+object_index_id).find("#lbl_"+object_index_id).text(),
				                    "generation": "REQUESTOR"
								});
							}else if(get_fields_and_containers_ele.is('[embed-name]')){
								mobilityDynamicFields.push({
									"id": get_fields_and_containers_ele.attr("embed-name"),
				                    "label": fields_below.find("#label_"+object_index_id).find("#lbl_"+object_index_id).text(),
				                    "generation": "REQUESTOR"
								});
							}
							
							field_and_container_ele = fields_below.find("#obj_fields_"+object_index_id).find(".getFields_"+object_index_id+"");
							//ADDITIONAL FOR SPECIAL FIELDS
							if(set_obj_ele.attr("data-type") == "computed"){
								mobilityDynamicFields[keyFieldDynamic]["generation"] = "SYSTEM";
							}
							// =========================================				                                                               
			                                
			                                if (field_and_container_ele.attr("type") !== undefined) {
			                                    //alert(field_and_container_ele.attr('name') + ' ' + field_and_container_ele.attr("type") + ' ' + field_and_container_ele.parents(".thisDynamicTable").eq(0).length);
			                                    
			                                    if (field_and_container_ele.parents(".thisDynamicTable").eq(0).length > 0) {
			                                       // alert('from dynamic table');
			                                    }
			                                }
							if(field_and_container_ele.closest(".setObject").is('[data-upload="photo_upload"]')){ // BY SPECIFICS
	                    		mobilityDynamicFields[keyFieldDynamic]["type"] = "REQUEST_IMAGE";
	                    	}else if(set_obj_ele.is('[data-type="contactNumber"]')){
	                    		mobilityDynamicFields[keyFieldDynamic]["type"] = "CONTACT_NUMBER";
	                    	}else if(set_obj_ele.is('[data-type="labelOnly"]')){ //BY SPECIFICS
	                    		mobilityDynamicFields[keyFieldDynamic]["type"] = "LABEL";
							}else if( field_and_container_ele.length >= 1 && field_and_container_ele.attr("type") ){ //BY INPUT TYPES
								if(field_and_container_ele.attr("type") == "checkbox"){

									mobilityDynamicFields[keyFieldDynamic]["type"] = "CHECK_BOX_GROUP";
									mobilityDynamicFields[keyFieldDynamic]["options"] = [];
									field_and_container_ele.each(function(eqi_ffc){
										mobilityDynamicFields[keyFieldDynamic]["options"].push($(this).val());
									});

								}else if(field_and_container_ele.attr("type") == "radio"){

									mobilityDynamicFields[keyFieldDynamic]["type"] = "RADIO_BUTTON_GROUP";
									mobilityDynamicFields[keyFieldDynamic]["options"] = [];
									field_and_container_ele.each(function(eqi_ffc){
										mobilityDynamicFields[keyFieldDynamic]["options"].push($(this).val());
									});
								}else if(field_and_container_ele.attr("type") == "text"){
									if(field_and_container_ele.attr("data-type") == "dateTime"){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "DATE_TIME_PICKER";
									}else if(field_and_container_ele.attr("data-type") == "time"){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "TIME_PICKER";
									}else if(field_and_container_ele.attr("data-type") == "date"){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "DATE_PICKER";
									}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "computed"){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "TEXT_FIELD";
										if(typeof field_and_container_ele.attr("processor-type") != "undefined")
										{ mobilityDynamicFields[keyFieldDynamic]["processor-type"] = ""+field_and_container_ele.attr("processor-type"); }
										if(typeof field_and_container_ele.attr("processor") != "undefined")
										{ mobilityDynamicFields[keyFieldDynamic]["processor"] = ""+field_and_container_ele.attr("processor"); }
									}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "barCodeScanner"){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "BAR_CODE";
									}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "attachment_on_request"){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "SINGLE_ATTACHMENT";
									}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "multiple_attachment_on_request"){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "MULTIPLE_ATTACHMENT";
									}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "pickList"){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "PICK_LIST";
										var picklist_button = field_and_container_ele.parents("#obj_fields_"+object_index_id).eq(0).find('.pickListButton,a[form-id]').eq(0);
										mobilityDynamicFields[keyFieldDynamic]["picklist_data"] = {};
										if( picklist_button.is("[return-field]") ){
											mobilityDynamicFields[keyFieldDynamic]["picklist_data"]["return_field"] = picklist_button.attr("return-field");
										}
										if( picklist_button.is("[form-id]") ){
											mobilityDynamicFields[keyFieldDynamic]["picklist_data"]["form_id"] = picklist_button.attr("form-id");
										}
										if( picklist_button.is("[return-field-name]") ){
											mobilityDynamicFields[keyFieldDynamic]["picklist_data"]["return_field_name"] = picklist_button.attr("return-field-name");
										}
										if( picklist_button.is("[formname]") ){
											mobilityDynamicFields[keyFieldDynamic]["picklist_data"]["formname"] = picklist_button.attr("formname");
										}
										if( picklist_button.is("[display_columns]") ){
											var display_columns = [];
											try{
												display_columns = JSON.parse(picklist_button.attr("display_columns"));
											}catch(error){

											}
											mobilityDynamicFields[keyFieldDynamic]["picklist_data"]["display_columns"] = display_columns;
										}
										if( picklist_button.is("[display_column_sequence]") ){
											var display_column_sequence = [];
											try{
												display_column_sequence = JSON.parse(picklist_button.attr("display_column_sequence"));
											}catch(error){

											}
											mobilityDynamicFields[keyFieldDynamic]["picklist_data"]["display_column_sequence"] = display_column_sequence;
										}
										if( picklist_button.is("[condition]") ){
											mobilityDynamicFields[keyFieldDynamic]["picklist_data"]["condition"] = picklist_button.attr("condition");
										}
										if( picklist_button.is('[allow-values-not-in-list]') ){
											mobilityDynamicFields[keyFieldDynamic]["picklist_data"]["allow_input_edit"] = picklist_button.is('[allow-values-not-in-list="Yes"]');
										}
									} else if(field_and_container_ele.closest(".setObject").is('[data-type="qr-code"]') ){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "QR_CODE_FIELD";
									} else if(field_and_container_ele.closest(".setObject").is('[data-type="listNames"]') ){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "NAMES_FIELD";
									} else if(field_and_container_ele.closest(".setObject").is('[data-type="textbox_editor_support"]') ){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "EDITOR_FIELD";
									} else if(field_and_container_ele.closest(".setObject").is('[data-type="textbox_reader_support"]') ){
										mobilityDynamicFields[keyFieldDynamic]["type"] = "READER_FIELD";
									} else{
										mobilityDynamicFields[keyFieldDynamic]["type"] = "TEXT_FIELD";
									}
								} else {
	                                //alert(field_and_container_ele.attr("type"));
	                            }
	                        
	                        } else if(field_and_container_ele.is('.embed-view-container')){ //BY CONTAINERS paki add na lang kapag mga containers ang target
	                        	//dito na ako
	                        	mobilityDynamicFields[keyFieldDynamic]["type"] = "CONTAINER_EMBED_VIEW";
	                        	mobility_workspace.saving_request_format.fields[push_length]["primary_data"] = {}; //groupings
	                        	mobility_workspace.saving_request_format.fields[push_length]["events"] = {}; //groupings
	                        	if(field_and_container_ele.is('[embed-source-form-val-id]')){
	                        		mobilityDynamicFields[keyFieldDynamic]["primary_data"]["source_form_id"] = field_and_container_ele.attr('embed-source-form-val-id');
	                        	}
	                        	//basic primary filter
	                        	if(field_and_container_ele.is('[embed-source-lookup-active-field-val]')){
	                        		mobilityDynamicFields[keyFieldDynamic]["primary_data"]["primary_filter_search_source_field"] = field_and_container_ele.attr('embed-source-lookup-active-field-val');
	                        	}
	                        	if(field_and_container_ele.is('[embed-conditional-operator]')){
	                        		mobilityDynamicFields[keyFieldDynamic]["primary_data"]["primary_filter_search_conditional_operator"] = field_and_container_ele.attr('embed-conditional-operator');
	                        	}
	                        	if(field_and_container_ele.is('[embed-result-field-val]')){
	                        		mobilityDynamicFields[keyFieldDynamic]["primary_data"]["primary_filter_search_current_field"] = field_and_container_ele.attr('embed-result-field-val');
	                        	}
	                        	//selected columns
	                        	if(field_and_container_ele.is('[embed-column-data]')){
	                        		var temp_json = field_and_container_ele.attr('embed-column-data');
	                        		try{
	                        			temp_json = JSON.parse(temp_json);
	                        		}catch(err){
	                        			temp_json = [];
	                        		}
	                        		mobilityDynamicFields[keyFieldDynamic]["primary_data"]["json_view_selected_columns"] = temp_json;
	                        	}
	                        	//data sending to child form
	                        	if(field_and_container_ele.is('[stringified-json-data-send]')){
	                        		var temp_json = field_and_container_ele.attr('stringified-json-data-send');
	                        		try{
	                        			temp_json = JSON.parse(temp_json);
	                        		}catch(err){
	                        			temp_json = [];
	                        		}
	                        		
	                        	}
	                        	//data sending to child form
		                    	if(field_and_container_ele.is('[stringified-json-data-send]')){
		                    		var temp_json = field_and_container_ele.attr('stringified-json-data-send');
		                    		try{
		                    			temp_json = JSON.parse(temp_json);
		                    		}catch(err){
		                    			temp_json = [];
		                    		}
		                			if(temp_json.length >= 1){
		                				mobilityDynamicFields[keyFieldDynamic]["enable_embedPopupDataSending"] = temp_json[0]["enable_embedPopupDataSending"];
		                				temp_json = temp_json.map(function(a,b){
		                					delete a["enable_embedPopupDataSending"];
		                					return a;
		                				});
		                			}

		                    		mobilityDynamicFields[keyFieldDynamic]["json_data_sending"] = temp_json;

		                    	}
	        	            	//event action privilages
	        	            	if(field_and_container_ele.is('[enable-embed-row-click]')){
	        	            		var enable_action_privilages = field_and_container_ele.attr('enable-embed-row-click');
	        	            		mobilityDynamicFields[keyFieldDynamic]["events"]["enable_event_action_privilages"] = enable_action_privilages;
	        	            		if(enable_action_privilages == "true" || enable_action_privilages == true){
	        	            			if(field_and_container_ele.is('[embed-row-click-creation-form]')){
	        	        					mobilityDynamicFields[keyFieldDynamic]["events"]["embed_action_click_create_request"] = field_and_container_ele.attr('embed-row-click-creation-form');
	        	        					mobilityDynamicFields[keyFieldDynamic]["events"]["embed_action_click_create_request_caption"] = field_and_container_ele.attr('link-ele-caption');
	        	            			}
	        	            			if(field_and_container_ele.is('[embed-action-click-copy]')){
	        	        					mobilityDynamicFields[keyFieldDynamic]["events"]["embed_action_click_copy"] = field_and_container_ele.attr('embed-action-click-copy');
	        	            			}
	        	            			if(field_and_container_ele.is('[embed-action-click-delete]')){
	        	        					mobilityDynamicFields[keyFieldDynamic]["events"]["embed_action_click_delete"] = field_and_container_ele.attr('embed-action-click-delete');
	        	            			}
	        	            			if(field_and_container_ele.is('[embed-action-click-edit]')){
	        	        					mobilityDynamicFields[keyFieldDynamic]["events"]["embed_action_click_inline_edit"] = field_and_container_ele.attr('embed-action-click-edit');
	        	            			}
	        	            			if(field_and_container_ele.is('[embed-action-click-edit-popup]')){
	        	        					mobilityDynamicFields[keyFieldDynamic]["events"]["embed_action_click_popup_edit"] = field_and_container_ele.attr('embed-action-click-edit-popup');
	        	            			}
	        	            			if(field_and_container_ele.is('[embed-action-click-view]')){
	        	        					mobilityDynamicFields[keyFieldDynamic]["events"]["embed_action_click_view"] = field_and_container_ele.attr('embed-action-click-view');
	        	            			}
	        	            		}
	        	            	}
	        	            	//allow numbering of recs
	        	            	if(field_and_container_ele.is('[embed-action-click-number]')){
	        						mobilityDynamicFields[keyFieldDynamic]["embed_action_click_number"] = field_and_container_ele.attr('embed-action-click-number');
	        	    			}
	        	    			//allow aggregate functions
		            			if(field_and_container_ele.is('[data-output-col-total]')){
		            				mobilityDynamicFields[keyFieldDynamic]['data_output_col_total'] = JSON.parse(field_and_container_ele.attr('data-output-col-total')||'{}');
		    	    			}
							} else{ //BY TAG NAME

	                            if (field_and_container_ele.prop("tagName") == "SELECT") {
	                                
	                                if (field_and_container_ele.attr("multiple")) {
	                                    mobilityDynamicFields[keyFieldDynamic]["type"] = "SELECT_MANY";
	                                    mobilityDynamicFields[keyFieldDynamic]["property"] = "MULTIPLE";                                                                                       
	                                    
	                                    //  include the options of this field in the JSON object
	                                    var options = [];
	                                    $(field_and_container_ele).find('option').each(function() {
	                                        options.push($(this).attr('value'));
	                                    });
	                                    
	                                    mobilityDynamicFields[keyFieldDynamic]["options"] = options;
	                                    
	                                } else if (field_and_container_ele.closest(".setObject").attr("data-type") == "dropdown") {
	                                    mobilityDynamicFields[keyFieldDynamic]["type"] = "DROPDOWN";
	                                    mobilityDynamicFields[keyFieldDynamic]["property"] = "DROPDOWN";
	                                    
	                                    //  include the options of this field in the JSON object
	                                    var options = [];
	                                    $(field_and_container_ele).find('option').each(function() {
	                                        options.push($(this).attr('value'));
	                                    });
	                                    
	                                    mobilityDynamicFields[keyFieldDynamic]["options"] = options;
	                                    
	                                } else {
	                                    mobilityDynamicFields[keyFieldDynamic]["type"] = "SELECT";
	                                    mobilityDynamicFields[keyFieldDynamic]["property"] = "";
	                                }
	                                
	                            } else if (field_and_container_ele.prop("tagName") == "TEXTAREA") {
	                                mobilityDynamicFields[keyFieldDynamic]["type"] = "TEXT_AREA";
	                            }
							}

							//SAVING PROPERTY OF EACH FIELDS
							{	
								var str_default_value_type = field_and_container_ele.attr("default-type");
								var str_default_value = "";
								if(str_default_value_type == "computed"){
									str_default_value = field_and_container_ele.attr("default-formula-value") || "";
								}else if(str_default_value_type == "middleware"){
									str_default_value = field_and_container_ele.attr("default-middleware-value") || "";
								}else{
									str_default_value = field_and_container_ele.attr("default-static-value") || "";
								}
								mobilityDynamicFields[keyFieldDynamic]["formulas"] = {
									"formula_list_onchange":{
										"formula_rule":$('body').data(object_index_id+"")['onchange-formula-event']||""
									},
									"computed":{
										"type":str_default_value_type || "",
										"formula_rule":str_default_value || ""
									},
									"readonly":{
										"formula_rule":field_and_container_ele
									},
									"visibility":{
										"formula_rule":field_and_container_ele.attr("visible-formula") || ""
									},
									"input_validation":{
										"formula_rule":field_and_container_ele.attr("field-formula-validation") || "",
										"message":field_and_container_ele.attr("field-formula-validation-message") || "",
									}
								};

								//added by joshua reyes [05-26-2016] For mobility properties support
								mobilityDynamicFields[keyFieldDynamic]["properties"] = {
									"placeholder": field_and_container_ele.attr('placeholder') || "",
									"readonly": field_and_container_ele.is('[readonly="readonly"]'),
									"allowLabelCaption": ($('body').data(object_index_id+"")['lblAllowLabel']||"Yes").toLowerCase()
								};
							}

							keyFieldDynamic++;
						})
						push_length = mobility_workspace.saving_request_format.fields.length - 1;
						// console.log(mobilityDynamicFields)
						mobility_workspace.saving_request_format.fields[push_length]['repeater_fields'] = mobilityDynamicFields;
						mobilityDynamicFields = [];
					
					
					}else{ //KAPAG WALA SA DYNAMIC TABLE
						
						if(get_fields_and_containers_ele.length <= 0){
							if(set_obj_ele.is('[data-upload="photo_upload"]')){
								get_fields_and_containers_ele = set_obj_ele.find(".getFields_"+object_index_id+"[id='getFields_"+object_index_id+"'][name]").eq(0);
								mobility_workspace.saving_request_format.fields.push({
									"id": set_obj_ele.find('.getFields_'+object_index_id+'[id="getFields_'+object_index_id+'"][name]').attr('name'),
									"label": "",
									"generation": "REQUESTOR",
								});
							}else{
								get_fields_and_containers_ele = $(this).find(".getFields_"+object_index_id).eq(0);
							}
						}else if(get_fields_and_containers_ele.is('[name]')){
							mobility_workspace.saving_request_format.fields.push({
								"id": $(this).find(".getFields_"+object_index_id).eq(0).attr("name").replace("[]",""),
								"label": fields_below.find("#label_"+object_index_id).find("#lbl_"+object_index_id).text(),
								"generation": "REQUESTOR",
							});
						}else if(get_fields_and_containers_ele.is('[embed-name]')){
							mobility_workspace.saving_request_format.fields.push({
								"id": $(this).find(".getFields_"+object_index_id).eq(0).attr("embed-name"),
								"label": fields_below.find("#label_"+object_index_id).find("#lbl_"+object_index_id).text(),
								"generation": "REQUESTOR",
							});
						}
					}


					push_length = mobility_workspace.saving_request_format.fields.length - 1;
					field_and_container_ele = fields_below.find("#obj_fields_"+object_index_id).find(".getFields_"+object_index_id+"");
					if( field_and_container_ele.length <= 0 ){
						/*dito add ng containers tab panel, accordion, request image, table kapag ng field_and_container_ele.length <= 0*/
						if(set_obj_ele.is('[data-upload="photo_upload"]')){
							field_and_container_ele = set_obj_ele.find('.getFields_'+object_index_id+'[id="getFields_'+object_index_id+'"][name]');
						}
					}

					//ADDITIONAL FOR SPECIAL FIELDS
					if(set_obj_ele.attr("data-type") == "computed"){
						mobility_workspace.saving_request_format.fields[push_length]["generation"] = "SYSTEM";
					}
					// =========================================

                    if (field_and_container_ele.attr("type") !== undefined) {
                        //alert(field_and_container_ele.attr('name') + ' ' + field_and_container_ele.attr("type") + ' ' + field_and_container_ele.parents(".thisDynamicTable").eq(0).length);
                        
                        if (field_and_container_ele.parents(".thisDynamicTable").eq(0).length > 0) {
                           // alert('from dynamic table');
                        }
                    }
					
					if(field_and_container_ele.closest(".setObject").is('[data-upload="photo_upload"]')){ //BY SPECIFICS
                    	mobility_workspace.saving_request_format.fields[push_length]["type"] = "REQUEST_IMAGE";
                	}else if(set_obj_ele.is('[data-type="contactNumber"]')){ //BY SPECIFICS
                    	mobility_workspace.saving_request_format.fields[push_length]["type"] = "CONTACT_NUMBER";
                    }else if(set_obj_ele.is('[data-type="labelOnly"]')){ //BY SPECIFICS
                    	mobility_workspace.saving_request_format.fields[push_length]["type"] = "LABEL";
					}else if( field_and_container_ele.length >= 1 && field_and_container_ele.attr("type") ){ //BY INPUT TYPES
						if(field_and_container_ele.attr("type") == "checkbox"){

							mobility_workspace.saving_request_format.fields[push_length]["type"] = "CHECK_BOX_GROUP";
							mobility_workspace.saving_request_format.fields[push_length]["options"] = [];
							field_and_container_ele.each(function(eqi_ffc){
								mobility_workspace.saving_request_format.fields[push_length]["options"].push($(this).val());
							});

						}else if(field_and_container_ele.attr("type") == "radio"){

							mobility_workspace.saving_request_format.fields[push_length]["type"] = "RADIO_BUTTON_GROUP";
							mobility_workspace.saving_request_format.fields[push_length]["options"] = [];
							field_and_container_ele.each(function(eqi_ffc){
								mobility_workspace.saving_request_format.fields[push_length]["options"].push($(this).val());
							});
						}else if(field_and_container_ele.attr("type") == "text"){
							if(field_and_container_ele.attr("data-type") == "dateTime"){
								mobility_workspace.saving_request_format.fields[push_length]["type"] = "DATE_TIME_PICKER";
							}else if(field_and_container_ele.attr("data-type") == "time"){
								mobility_workspace.saving_request_format.fields[push_length]["type"] = "TIME_PICKER";
							}else if(field_and_container_ele.attr("data-type") == "date"){
								mobility_workspace.saving_request_format.fields[push_length]["type"] = "DATE_PICKER";
							}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "computed"){
								mobility_workspace.saving_request_format.fields[push_length]["type"] = "TEXT_FIELD";
								if(typeof field_and_container_ele.attr("processor-type") != "undefined")
								{ mobility_workspace.saving_request_format.fields[push_length]["processor-type"] = ""+field_and_container_ele.attr("processor-type"); }
								if(typeof field_and_container_ele.attr("processor") != "undefined")
								{ mobility_workspace.saving_request_format.fields[push_length]["processor"] = ""+field_and_container_ele.attr("processor"); }
							}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "barCodeScanner"){
								mobility_workspace.saving_request_format.fields[push_length]["type"] = "BAR_CODE";
							}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "attachment_on_request"){
								mobility_workspace.saving_request_format.fields[push_length]["type"] = "SINGLE_ATTACHMENT";
							}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "multiple_attachment_on_request"){
								mobility_workspace.saving_request_format.fields[push_length]["type"] = "MULTIPLE_ATTACHMENT";
							}else if(field_and_container_ele.closest(".setObject").attr("data-type") == "pickList"){
								mobility_workspace.saving_request_format.fields[push_length]["type"] = "PICK_LIST";
								var picklist_button = field_and_container_ele.parents("#obj_fields_"+object_index_id).eq(0).find('.pickListButton,a[form-id]').eq(0);
								mobility_workspace.saving_request_format.fields[push_length]["picklist_data"] = {};
								if( picklist_button.is("[return-field]") ){
									mobility_workspace.saving_request_format.fields[push_length]["picklist_data"]["return_field"] = picklist_button.attr("return-field");
								}
								if( picklist_button.is("[form-id]") ){
									mobility_workspace.saving_request_format.fields[push_length]["picklist_data"]["form_id"] = picklist_button.attr("form-id");
								}
								if( picklist_button.is("[return-field-name]") ){
									mobility_workspace.saving_request_format.fields[push_length]["picklist_data"]["return_field_name"] = picklist_button.attr("return-field-name");
								}
								if( picklist_button.is("[formname]") ){
									mobility_workspace.saving_request_format.fields[push_length]["picklist_data"]["formname"] = picklist_button.attr("formname");
								}
								if( picklist_button.is("[selection-type]") ){
									mobility_workspace.saving_request_format.fields[push_length]["picklist_data"]["picklist_selection_type"] = picklist_button.attr("selection-type");
								}
								if( picklist_button.is("[display_columns]") ){
									var display_columns = [];
									try{
										display_columns = JSON.parse(picklist_button.attr("display_columns"));
									}catch(error){

									}
									mobility_workspace.saving_request_format.fields[push_length]["picklist_data"]["display_columns"] = display_columns;
								}
								if( picklist_button.is("[display_column_sequence]") ){
									var display_column_sequence = [];
									try{
										display_column_sequence = JSON.parse(picklist_button.attr("display_column_sequence"));
									}catch(error){

									}
									mobility_workspace.saving_request_format.fields[push_length]["picklist_data"]["display_column_sequence"] = display_column_sequence;
								}
								if( picklist_button.is("[condition]") ){
									mobility_workspace.saving_request_format.fields[push_length]["picklist_data"]["condition"] = picklist_button.attr("condition");
								}
								if( picklist_button.is('[allow-values-not-in-list]') ){
									mobility_workspace.saving_request_format.fields[push_length]["picklist_data"]["allow_input_edit"] = picklist_button.is('[allow-values-not-in-list="Yes"]');
								}
							} else if(field_and_container_ele.closest(".setObject").is('[data-type="qr-code"]') ){
									mobility_workspace.saving_request_format.fields[push_length]["type"] = "QR_CODE_FIELD";
							} else if(field_and_container_ele.closest(".setObject").is('[data-type="listNames"]') ){
									mobility_workspace.saving_request_format.fields[push_length]["type"] = "NAMES_FIELD";
							} else if(field_and_container_ele.closest(".setObject").is('[data-type="textbox_editor_support"]') ){
									mobility_workspace.saving_request_format.fields[push_length]["type"] = "EDITOR_FIELD";
							} else if(field_and_container_ele.closest(".setObject").is('[data-type="textbox_reader_support"]') ){
									mobility_workspace.saving_request_format.fields[push_length]["type"] = "READER_FIELD";
							} else{
								mobility_workspace.saving_request_format.fields[push_length]["type"] = "TEXT_FIELD";
							}
						} else {
                            //alert(field_and_container_ele.attr("type"));
                        }
                    } else if(field_and_container_ele.is('.embed-view-container')){ //BY CONTAINERS paki or na lang kapag mga containers ang target
                    	//dito na ako
                    	mobility_workspace.saving_request_format.fields[push_length]["type"] = "CONTAINER_EMBED_VIEW";
                    	mobility_workspace.saving_request_format.fields[push_length]["primary_data"] = {}; //groupings
                    	mobility_workspace.saving_request_format.fields[push_length]["events"] = {}; //groupings
                    	if(field_and_container_ele.is('[embed-source-form-val-id]')){
                    		mobility_workspace.saving_request_format.fields[push_length]["primary_data"]["source_form_id"] = field_and_container_ele.attr('embed-source-form-val-id');
                    	}
                    	//basic primary filter
                    	if(field_and_container_ele.is('[embed-source-lookup-active-field-val]')){
                    		mobility_workspace.saving_request_format.fields[push_length]["primary_data"]["primary_filter_search_source_field"] = field_and_container_ele.attr('embed-source-lookup-active-field-val');
                    	}
                    	if(field_and_container_ele.is('[embed-conditional-operator]')){
                    		mobility_workspace.saving_request_format.fields[push_length]["primary_data"]["primary_filter_search_conditional_operator"] = field_and_container_ele.attr('embed-conditional-operator');
                    	}
                    	if(field_and_container_ele.is('[embed-result-field-val]')){
                    		mobility_workspace.saving_request_format.fields[push_length]["primary_data"]["primary_filter_search_current_field"] = field_and_container_ele.attr('embed-result-field-val');
                    	}
                    	//selected columns
                    	if(field_and_container_ele.is('[embed-column-data]')){
                    		var temp_json = field_and_container_ele.attr('embed-column-data');
                    		try{
                    			temp_json = JSON.parse(temp_json);
                    		}catch(err){
                    			temp_json = [];
                    		}
                    		mobility_workspace.saving_request_format.fields[push_length]["primary_data"]["json_view_selected_columns"] = temp_json;
                    	}
                    	//data sending to child form
                    	if(field_and_container_ele.is('[stringified-json-data-send]')){
                    		var temp_json = field_and_container_ele.attr('stringified-json-data-send');
                    		try{
                    			temp_json = JSON.parse(temp_json);
                    		}catch(err){
                    			temp_json = [];
                    		}
                			if(temp_json.length >= 1){
                				mobility_workspace.saving_request_format.fields[push_length]["enable_embedPopupDataSending"] = temp_json[0]["enable_embedPopupDataSending"];
                				temp_json = temp_json.map(function(a,b){
                					delete a["enable_embedPopupDataSending"];
                					return a;
                				});
                			}

                    		mobility_workspace.saving_request_format.fields[push_length]["json_data_sending"] = temp_json;

                    	}
                    	//event action privilages
                    	if(field_and_container_ele.is('[enable-embed-row-click]')){
                    		var enable_action_privilages = field_and_container_ele.attr('enable-embed-row-click');
                    		mobility_workspace.saving_request_format.fields[push_length]["events"]["enable_event_action_privilages"] = enable_action_privilages;
                    		if(enable_action_privilages == "true" || enable_action_privilages == true){
                    			if(field_and_container_ele.is('[embed-row-click-creation-form]')){
                					mobility_workspace.saving_request_format.fields[push_length]["events"]["embed_action_click_create_request"] = field_and_container_ele.attr('embed-row-click-creation-form');
                					mobility_workspace.saving_request_format.fields[push_length]["events"]["embed_action_click_create_request_caption"] = field_and_container_ele.attr('link-ele-caption');
                    			}
                    			if(field_and_container_ele.is('[embed-action-click-copy]')){
                					mobility_workspace.saving_request_format.fields[push_length]["events"]["embed_action_click_copy"] = field_and_container_ele.attr('embed-action-click-copy');
                    			}
                    			if(field_and_container_ele.is('[embed-action-click-delete]')){
                					mobility_workspace.saving_request_format.fields[push_length]["events"]["embed_action_click_delete"] = field_and_container_ele.attr('embed-action-click-delete');
                    			}
                    			if(field_and_container_ele.is('[embed-action-click-edit]')){
                					mobility_workspace.saving_request_format.fields[push_length]["events"]["embed_action_click_inline_edit"] = field_and_container_ele.attr('embed-action-click-edit');
                    			}
                    			if(field_and_container_ele.is('[embed-action-click-edit-popup]')){
                					mobility_workspace.saving_request_format.fields[push_length]["events"]["embed_action_click_popup_edit"] = field_and_container_ele.attr('embed-action-click-edit-popup');
                    			}
                    			if(field_and_container_ele.is('[embed-action-click-view]')){
                					mobility_workspace.saving_request_format.fields[push_length]["events"]["embed_action_click_view"] = field_and_container_ele.attr('embed-action-click-view');
                    			}
                    		}
                    	}
                    	//allow numbering of recs
                    	if(field_and_container_ele.is('[embed-action-click-number]')){
        					mobility_workspace.saving_request_format.fields[push_length]["embed_action_click_number"] = field_and_container_ele.attr('embed-action-click-number');
            			}
            			//allow aggregate functions
            			if(field_and_container_ele.is('[data-output-col-total]')){
            				mobility_workspace.saving_request_format.fields[push_length]["data_output_col_total"] = JSON.parse(field_and_container_ele.attr('data-output-col-total')||'{}');
    	    			}
					} else { //BY TAG NAME
                        
                        if (field_and_container_ele.prop("tagName") == "SELECT") {
                            
                            if (field_and_container_ele.attr("multiple")) {
                                mobility_workspace.saving_request_format.fields[push_length]["type"] = "SELECT_MANY";
                                mobility_workspace.saving_request_format.fields[push_length]["property"] = "MULTIPLE";                                                                                       
                                
                                //  include the options of this field in the JSON object
                                var options = [];
                                $(field_and_container_ele).find('option').each(function() {
                                    options.push($(this).attr('value'));
                                });
                                
                                mobility_workspace.saving_request_format.fields[push_length]["options"] = options;
                                
                            } else if (field_and_container_ele.closest(".setObject").attr("data-type") == "dropdown") {
                                mobility_workspace.saving_request_format.fields[push_length]["type"] = "DROPDOWN";
                                mobility_workspace.saving_request_format.fields[push_length]["property"] = "DROPDOWN";
                                
                                //  include the options of this field in the JSON object
                                var options = [];
                                $(field_and_container_ele).find('option').each(function() {
                                    options.push($(this).attr('value'));
                                });
                                
                                mobility_workspace.saving_request_format.fields[push_length]["options"] = options;
                                
                            } else {
                                mobility_workspace.saving_request_format.fields[push_length]["type"] = "SELECT";
                                mobility_workspace.saving_request_format.fields[push_length]["property"] = "";
                            }
                            
                        } else if (field_and_container_ele.prop("tagName") == "TEXTAREA") {
                            mobility_workspace.saving_request_format.fields[push_length]["type"] = "TEXT_AREA";
                        }
					}

					//SAVING PROPERTY OF EACH FIELDS
					{	
						var str_default_value_type = field_and_container_ele.attr("default-type");
						var str_default_value = "";
						if(str_default_value_type == "computed"){
							str_default_value = field_and_container_ele.attr("default-formula-value") || "";
						}else if(str_default_value_type == "middleware"){
							str_default_value = field_and_container_ele.attr("default-middleware-value") || "";
						}else{
							str_default_value = field_and_container_ele.attr("default-static-value") || "";
						}
						mobility_workspace.saving_request_format.fields[push_length]["formulas"] = {
							"formula_list_onchange":{
								"formula_rule":$('body').data(object_index_id+"")['onchange-formula-event']||""
							},
							"computed":{
								"type":str_default_value_type || "",
								"formula_rule":str_default_value || ""
							},
							"readonly":{
								"formula_rule":field_and_container_ele.attr('read-only-formula')||""
							},
							"visibility":{
								"formula_rule":field_and_container_ele.attr("visible-formula") || ""
							},
							"input_validation":{
								"formula_rule":field_and_container_ele.attr("field-formula-validation") || "",
								"message":field_and_container_ele.attr("field-formula-validation-message") || "",
							}
						};

						//added by joshua reyes [05-26-2016] For mobility properties support
						mobility_workspace.saving_request_format.fields[push_length]["properties"] = {
							"placeholder": field_and_container_ele.attr('placeholder') || "",
							"readonly": field_and_container_ele.is('[readonly="readonly"]'),
							"allowLabelCaption": ($('body').data(object_index_id+"")['lblAllowLabel']||"Yes").toLowerCase()
						};
					}
                                      
				// console.log(JSON.stringify(mobility_workspace.saving_request_format.fields[push_length]));
			});
		}
		
		if(typeof mobility_workspace.manage_field_component_on_form != "undefined"){
			delete mobility_workspace.manage_field_component_on_form;	
		}
		if(typeof mobility_workspace.manage_saved_ui_mobile != "undefined"){
			delete mobility_workspace.manage_saved_ui_mobile;
		}
		// mobility_workspace.enable_mobility = true;
		// if($('[enable-mobility-form]').is('[enable-mobility-form="false"]') ){
		// 	mobility_workspace.enable_mobility = false;
		// 	delete mobility_workspace.saving_request_format;
		// }
		mobility_workspace.saved_mobile_content = $(this).closest(".mobility-content-center").children(".mobility-body").children("#sortable-mobile").html();
		
		//MOBILE PROPERTY
		mobility_workspace.saving_request_format.mobile_form_properties = $(this).data('mobile-properties-data')();
		if($('.mobility-close-container.container-is-active-toggle').is('.is-enabled')){
			mobility_workspace.is_active_mobility = true;
		}else{
			mobility_workspace.is_active_mobility = false;
		}

		//SAVE THE UNUSED FIELDS
		{
			mobility_workspace.mobility_unused_fields = $('[id="unused-fields"].unusedFields').children('.element-mobile').map(function(){
				return $(this).children('[data-object-id]').attr('data-object-id')||false;
			}).get().filter(Boolean);
		}
		$("body").data(mobility_workspace);
		console.log("mobility_workspace",mobility_workspace);
		$(".form-workspace-mobility-json").text(mobilityLocationContent.children("#sortable-mobile").html());
		// showNotification({
		//     message: "Mobility was successfully saved!",
		//     type: "success",
		//     autoClose: true,
		//     duration: 3
	 //    });
		$(".mobility-overlay").click();
		// console.log("TERSTYYING");
		// console.log(mobility_workspace);
		// console.log(mobility_workspace.saving_request_format);
		// console.log(JSON.stringify(mobility_workspace.saving_request_format));
		console.log($("body").data())
	}];
};