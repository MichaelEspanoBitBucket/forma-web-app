
/*---------------------------------------- Functions for Form Builder ---------------------------------*/

// //editors readers support

// function textbox_ersupport(count, object_type) {
    
//     var ret = "";
//     ret += '<div class="setObject cursor_move" data-type="textbox_support" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
//     ret += '<div class="fields_below">'; //  style="min-width:200px;"
//     ret += '<div class="setObjects_actions">';
//     ret += '<div class="pull-left">';
//     ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
//     ret += '</div>';
//     ret += '<div class="obj_actions display fl-options-data-type" id="obj_actions_' + count + '">';
//     ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i>';
//     ret += '<i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i>';
//     ret += '</div>';
//     ret += '</div>';
//     ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Editor\'s & Reader\'s Support:</label></div>';
//     ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
//     ret += '<input data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="textbox_support_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>';
//     ret += '</div>';
//     ret += '</div>';
//     ret += '</div>';
//     return ret;
// }
function QRCodeField(count, object_type) {
    var ret = ''+
    '<div class="setObject cursor_move" data-type="qr-code" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">'+
        '<div class="fields_below">'+ //  style="min-width:200px;+
        '<div class="setObjects_actions ">'+
            '<div class="obj_actions display" id="obj_actions_' + count + '">'+
                '<ul class="fl-options-data-type">'+
                    '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="QRCode" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>'+
                    '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>'+
                '</ul>'+
            '</div>'+
        '</div>'+
        '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">QR Code:</label></div>'+
            '<div class="input_position_below" id="obj_fields_' + count + '">'+
                '<div class="qr-code-field-container">'+
                    '<input data-type="longtext" type="text" disabled="disabled" class="tip form-text qr-code-input getFields getFields_' + count + '" name="qr_code_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>'+
                    '<div id="qr_code_' + count + '" class="qr-code-image"></div>'+

                '</div>'+
                
            '</div>'+
        '</div>'+
    '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-qrcode"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">qr_code_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip"  data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting  fa-cogs icon-cogs object_setup cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}



// FOR SMART SCANNER DESIGN
function detailsPanelField(count, object_type){

        var ret
        =   '<div class="setObject cursor_move" data-type="details-panel" data-object-id="'+ count +'" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="height: 180px;width:308px; z-index=' + count + ' ">'+
                '<div class="setObjects_actions">'+
                    /*'<div class="pull-left">'+
                        '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>'+
                    '</div>'+*/
                    '<div class="obj_actions display" id="obj_actions_' + count + '">'+
                        '<ul class="fl-options-data-type">'+
                            '<li><i class="fa fa-eye pull-left cursor object_setup object_properties" data-properties-type="detailsPanelContentSettings" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Details Panel Properties"></i></li>'+
                            '<li><i class="fa fa-cogs pull-left cursor object_setup object_properties" data-properties-type="detailsPanel" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Basic Properties"></i></li>'+
                            '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="'+ count +'" data-original-title="Remove"></i></li>'+
                        '</ul>'+
                    '</div>'+
                '</div>'+

                '<div class="fields_below" style="width:100%;height:100%;">'+ // style="min-width:200px;"

                    '<div class="label_below obj_label" id="label_'+ count +'">'+
                        '<label id="lbl_' + count + '">Details Panel:</label>'+
                    '</div>'+
                    
                    '<div class="input_position_below" id="obj_fields_' + count + '" style="height:calc(100% - 25px);">'+
                        '<div class="details-panel-container getFields_'+count+'" detail-panel-name="details_panel_'+count+'"></div>'+
                    '</div>'+
                '</div>'+
            '</div>';
            
        return ret;

}
function smartBarcodeField(count, object_type) {
    var ret = "";
    ret +=
    '<div class="setObject cursor_move" data-type="smart_barcode_field" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="height: 100px; z-index=' + count + ' ">'+
        '<div class="fields_below" style="min-height:15px;" >'+
            '<div class="setObjects_actions ">'+
                '<div class="obj_actions display" id="obj_actions_' + count + '">'+
                    '<ul class="fl-options-data-type">'+
                        '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="smart-barcode-field" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>'+
                        '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>'+
                    '</ul>'+
                '</div>'+
            '</div>'+

            '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Smart Barcode:</label></div>'+

            '<div style="font-size:14px;" class="input_position_below" id="obj_fields_' + count + '">'+
                //dito na ung field design
                '<div class="sbcf-container isDisplayNone">'+
                    '<input name="smart_barcode_field_'+count+'" id="getFields_'+count+'" class="form-text getFields getFields_'+count+' smart-barcode-field-input-str-collection" data-type="longtext" type="text" />'+
                '</div>'+
                '<div class="sbcf-container">'+
                    '<table style="width:100%;">'+
                        '<tbody>'+
                            '<tr class="sbcf-smart-barcode-field-group">'+
                                '<td><div style="position:relative;">'+
                                    '<input class="form-text smart-barcode-field-input" type="text" style="width:100%;" />'+
                                '</div></td>'+
                                '<td><div style="position:relative;">'+
                                    '<div class="sbcf-action-icon-master-container">'+
                                        '<ul class="sbcf-action-icon-slave-container">'+
                                            '<li><i class="fa fa-plus sbcf-action-icon-add"></i></li>'+
                                            '<li><i class="fa fa-minus sbcf-action-icon-delete isDisplayNone"></i></li>'+
                                        '</ul>'+
                                    '</div>'+
                                '</div></td>'+
                            '</tr>'+
                        '</tbody>'+
                    '</table>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-smart-barcode"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">smar_barcode_field_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip"  data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup  fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

//editors support design
function createAccordion(count, object_type){

    var ret =
    '<div class="setObject cursor_move" data-type="accordion" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="min-width:140px;z-index=' + count + ';display:block;">'+
        '<div class="fields_below fields_below_accordion">'+ //  style="min-width:200px;"
            '<div class="setObjects_actions">'+
                /*'<div class="pull-left">'+
                    '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>'+
                '</div>'+*/
                '<div class="obj_actions display" style="margin-top: 1px;" id="obj_actions_' + count + '">'+ 
                    '<ul class="fl-options-data-type">'+
                        '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="accordion-property" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>'+
                        '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>'+
                    '</ul>'+
                 '</div>'+
            '</div>'+
            '<div class="label_below obj_label" id="label_' + count + '"><label style="cursor:move;" id="lbl_' + count + '">Accordion Container:</label></div>'+
            '<div class="input_position_below input_position_below_accordion" id="obj_fields_' + count + '">'+
                // '<input data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="textbox_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>'+
                
                '<div class="form-accordion form-accordion-'+count+'">'+
                    '<div class="form-accordion-section-group">'+
                        '<h3 class="form-accordion-section-header">'+
                            '<div class="form-accordion-action-tbl">'+ 
                                '<div class="form-accordion-action-row">'+ 
                                    '<div class="form-accordion-action-cell accordion-header-name">Section 1</div>'+
                                    '<div  class="form-accordion-action-cell">'+
                                        '<ul class="form-accordion-action-icon">'+
                                            '<li><span class="fa fa-pencil form-accordion-action-icon-edit"></span></li>'+
                                            '<li><span class="fa fa-plus form-accordion-action-icon-add"></span></li>'+
                                            '<li><span class="fa fa-times form-accordion-action-icon-delete cursor-disable"></span></li>'+
                                        '</ul>'+
                                    '</div>'+ 
                                '</div>'+
                            '</div>'+
                        '</h3>'+
                        '<div class="accordion-div-content" style="height: 50px;"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>';
if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-accordion"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">setObject'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup  fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
}
function textbox_editor_support(count, object_type) {
    
    var ret = "";
    ret += '<div class="setObject cursor_move" data-type="textbox_editor_support" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below">'; //  style="min-width:200px;"
    ret += '<div class="setObjects_actions">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Editor\'s Support:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input data-type="longtext" type="text" disabled="disabled" style="font-size:14px;" class="tip form-text getFields getFields_' + count + '" name="textbox_editor_support_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-editor-support"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">textbox_editor_support_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup  fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}
//editors support design
function textbox_reader_support(count, object_type) {
    
    var ret = "";
    ret += '<div class="setObject cursor_move" data-type="textbox_reader_support" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below">'; //  style="min-width:200px;"
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Reader\'s Support:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input style="font-size:14px;" data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="textbox_reader_support_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-reader-support"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">textbox_reader_support_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}
function multiple_requestImport(count, object_type) {
    var img = "";
    var ret     
        = '<div class="setObject cursor_move " data-type="multiple_attachment_on_request" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="z-index=' + count + ' ">';
            ret += '<div class="fields_below">'; //  style="min-width:200px;"
            ret += '<div class="setObjects_actions ">';
            /*ret += '<div class="pull-left">';
            ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
            ret += '</div>';*/
            ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
            ret += '<ul class="fl-options-data-type">';    
              ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="on_attachment" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
              ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
            ret += '</ul>';
            ret += '</div>';
            ret += '</div>';
            
            ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Multiple Attachment:</label></div>';
            ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
            ret +='<form id="multiple_on_attach_' + count + '" method="post" enctype="multipart/form-data" action="/ajax/multiple_request_on_attachment">';
                ret += '<input type="file" class="multiple_file_attachement obj-attachment" data-action-id="' + count + '"  name="multiple_file_attachment[]" multiple id="multiple_on_attachment_' + count + '" size="24" data-action-type="multiple_attachmentForm">';
            ret +='</form>';
            ret +='<input style="font-size:14px;" data-type="longtext" type="text" disabled="disabled" data-attachment="attachment_multiple_on_attachment_' + count + '" class="tip form-text getFields getFields_' + count + ' display" name="multiple_on_attachment_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>';
            ret += '</div>';
            
            ret += '</div>';
        ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-multiple-attachment"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">multiple_on_attachment_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
return ret;

}
//single attachment
function requestImport(count, object_type) {
    var img = "";
    var ret     
        = '<div class="setObject cursor_move " data-type="attachment_on_request" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="z-index=' + count + ' ">';
            ret += '<div class="fields_below">'; //  style="min-width:200px;"
            ret += '<div class="setObjects_actions ">';
            /*ret += '<div class="pull-left">';
            ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
            ret += '</div>';*/
            ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
            ret += '<ul class="fl-options-data-type">';
                ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="on_attachment" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
                ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
            ret += '</ul>';  
            ret += '</div>';
            ret += '</div>';
            
            ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Single Attachment:</label></div>';
            ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
            ret +='<form id="on_attach_' + count + '" method="post" enctype="multipart/form-data" action="/ajax/request_on_attachment">';
                ret += '<input type="file" class="file_attachement obj-attachment" data-action-id="' + count + '"  name="file_attachment" id="on_attachment_' + count + '" size="24" data-action-type="attachmentForm">';
            ret +='</form>';
            ret +='<input style="font-size:14px;" data-type="longtext" type="text" data-single-attachment="true" disabled="disabled" class="tip form-text getFields getFields_' + count + ' display" name="on_attachment_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>';
            ret += '</div>';
            
            ret += '</div>';
        ret += '</div>';
if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-single-attachment"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">on_attachment_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
return ret;

}

// NOTESTYLE TEXTAREA Design
function noteStyleTextArea(count, object_type) {
    var maxWH = "max-width:none;max-height:none";
    if($('.table-form-design-relative').length <= 0 ){
        maxWH = "max-width:300px;max-height:200px";
    }
    var ret =
    '<div class="setObject cursor_move" fluid-container-cell="Yes" data-type="noteStyleTextarea" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="min-width:20px;z-index=' + count + ' ">'+
        '<div class="fields_below">'+ //  style="min-width:200px;"
            '<div class="setObjects_actions">'+
                /*'<div class="pull-left">'+
                    '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>'+
                '</div>'+*/
            '<div class="obj_actions display" id="obj_actions_' + count + '">'+
                '<ul class="fl-options-data-type">'+
                '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="notes-style-property" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>'+
                '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>'+
                '</ul>'+
            '</div>'+
        '</div>'+
        '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Notes Style Field:</label></div>'+
            '<div class="input_position_below" id="obj_fields_' + count + '">'+
                // '<input data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="textbox_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>'+
                '<div class="note-style-wrap-contents">'+
                    '<div class="content-editable-containter-value">'+
                        '<textarea style="padding: 0px;width: auto;display:none;" data-type="longtext" disabled="disabled" class="noteStyleField tip form-text getFields getFields_' + count + '" name="note_style_textarea_' + count + '" id="getFields_' + count + '" data-placement="bottom"></textarea>'+
                    '</div>'+
                    '<div class="content-editable-containter" style="position:relative;">'+
                        '<div class="cefb-corner-top-left"></div>'+
                        '<div class="content-editable-field content-editable-field_'+count+' tip" style="background-color:white;height:auto;width:auto;min-width:20px;min-height:20px;'+maxWH+';border: 1px solid #D5D5D5;word-wrap:word-break;overflow:auto;padding:2px;" class="note-style-content-field" contenteditable="true"></div>'+
                        '<div class="cefb-corner-bottom-right"></div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>';
    return ret;
}

//Pick List
function pickList(count, object_type) {
    var ret
    =   '<div class="setObject cursor_move" data-type="pickList" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">' +
            '<div class="fields_below" >' + //style="min-width:200px;"
                '<div class="setObjects_actions ">' +
                    /*'<div class="pull-left">' +
                        '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>' +
                    '</div>' +*/
                    '<div class="obj_actions display" id="obj_actions_' + count + '" style="margin-top: 1px;">' +
                        '<ul class="fl-options-data-type">'+
                        '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>' +
                        '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>' +
                        '</ul>'+
                    '</div>' +
                '</div>' +
                '<div class="label_below obj_label" id="label_' + count + '">' +
                    '<label id="lbl_' + count + '">Pick List:</label>' +
                '</div>' +
                '<div class="input_position_below" id="obj_fields_' + count + '">' +
                    '<div style="display:table;width:100%;" id="field-container-pick-list_' + count + '" class="field-container-pick-list field-container-picklist_' + count + '">' +
                        '<input style="min-width:30px;display:table-cell;font-size:14px;" data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="picklist_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>' +
                        '<span style="width:19px;display:table-cell;white-space:nowrap;"><a class="btn-basicBtn padding_5 cursor tip pickListButton" picklist-button-id="picklist_button_' + count + '" data-original-title="Pick Keyword" picklist-type="Forms" style="margin-left:5px;" return-field="getFields_' + count + '"><i class="icon-list-alt fa fa-list-alt"></i></a></span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>';

 if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-pick-list"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">picklist_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                    '<i class="layer cursor icon-list-alt fa fa-list-alt tip" data-original-title="Picklist Properties" data-object-id="'+ count +'" data-properties-type="pickList_list"></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

//Embedded View
function embedView(count,object_type){
    var ret
    =   '<div class="setObject cursor_move" data-type="embeded-view" data-object-id="'+ count +'" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="height: 180px;width:308px; z-index=' + count + ' ">'+
            '<div class="setObjects_actions">'+
                /*'<div class="pull-left">'+
                    '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>'+
                '</div>'+*/
                '<div class="obj_actions display" id="obj_actions_' + count + '">'+
                    '<ul class="fl-options-data-type">'+
                        '<li><i class="fa fa-eye pull-left icon-cogs cursor object_setup object_properties" data-properties-type="Embedded View Properties" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Embedded View Properties"></i></li>'+
                        '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="embedview" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Basic Properties"></i></li>'+
                        '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="'+ count +'" data-original-title="Remove"></i></li>'+
                    '</ul>'+
                '</div>'+
            '</div>'+

            '<div class="fields_below" style="width:100%;height:100%;">'+ // style="min-width:200px;"

                '<div class="label_below obj_label" id="label_'+ count +'">'+
                    '<label id="lbl_' + count + '">Embedded View:</label>'+
                '</div>'+
                
                '<div class="input_position_below" id="obj_fields_' + count + '" style="height:calc(100% - 25px);">'+
                    '<div class="getFields embed-view-container getFields_'+count+'" embed-name="embed_view_'+count+'" style="height:100%;min-width:300px;width:100%;outline:1px solid #ccc;overflow:auto;"></div>'+
                '</div>'+
            '</div>'+
        '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-embedded-view"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">embed_view_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting  fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-properties-type="embedview" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                    '<i class="fa fa-eye layer layersetting icon-cogs cursor object_setup object_properties tip" data-properties-type="Embedded View Properties" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Embedded View Properties"></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

//Tab panel
function createTabPanel(count, object_type) {
    var ret
            = '<div class="setObject cursor_move" data-type="tab-panel" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">' +
            '<div class="fields_below">' + // style="min-width:200px;"
            '<div class="setObjects_actions ">' +
            /*'<div class="pull-left">' +
            '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>' +
            '</div>' +*/
                '<div class="obj_actions display" style="margin-top: 1px;" id="obj_actions_' + count + '">' +
                    '<ul class="fl-options-data-type">'+
                        '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="tabpanel" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>' +
                        '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>' +
                    '</ul>'+
                '</div>' +
            '</div>' +
            '<div class="label_below obj_label" id="label_' + count + '">' +
                '<label style="cursor:move;" id="lbl_' + count + '">Tabbed Panel:</label>' +
            '</div>' +
            '<div class="input_position_below" id="obj_fields_' + count + '">' +
            '<div style="clear:both;" class="getFields form-tabbable-pane" id="form-tabbable-pane-' + count + '">' +
            '<ul>' +
            '<li tab-name="Tab 1" class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" aria-controls="tabs-1_form-tabbable-pane-'+count+'"><a href="#tabs-1_form-tabbable-pane-'+count+'" class="ui-tabs-anchor" role="presentation">Tab 1</a><span class="ui-icon ui-icon-close" role="presentation" style="cursor:pointer;">Remove Tab</span><span class="edit-title-tab-panel ui-icon ui-icon-edit" style="margin:0px;cursor:pointer;">Edit tab</span></li>'+
            // '<li tab-name="Tab 2" class="ui-state-default ui-corner-top " role="tab" aria-controls="tabs-2_form-tabbable-pane-'+count+'"><a href="#tabs-2_form-tabbable-pane-'+count+'" class="ui-tabs-anchor" role="presentation">Tab 2</a><span class="ui-icon ui-icon-close" role="presentation" style="cursor:pointer;">Remove Tab</span><span class="edit-title-tab-panel ui-icon ui-icon-edit" style="margin:0px;cursor:pointer;">Edit tab</span></li>'+
            // '<li><a href="#tab-1_form-tabbable-pane-' + count + '">Tab1</a></li>'+
            '</ul>' +
            '<div style="padding: 0px; overflow: auto; position: relative; width: 100%; height: 100px; display: block;" id="tabs-1_form-tabbable-pane-'+count+'" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel"<p></p><div class="" style="z-index: 90;"></div></div>'+
            // '<div style="padding: 0px; overflow: auto; position: relative; width: 100%; height: 100px; display: block;" id="tabs-2_form-tabbable-pane-'+count+'" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel"<p></p><div class="" style="z-index: 90;"></div></div>'+
            // '<div style="position:relative;width:100%;" id="tab-1_form-tabbable-pane-' + count + '">TESTING 12</div>'+
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
            
if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-tab-panel"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">setObject_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

/* LABEL DESIGN */
function labelOnly(count, object_type) {
   //  alert(count+"qwerty")
   //  var pathname = window.location.pathname;
   //  //console.log(pathname)
   //  if (pathname == "/user_view/generate") {
   //  // var new_count = $("button[data-object-type='" + object_type + "']").attr("data-count");
   //  // var count_setObject = $(".setObject").length;
   //  // //console.log(count_setObject)
   //  // var count = parseInt(new_count) + parseInt(count) + parseInt(count_setObject);
   //  //console.log(count)
   // count++;
   //  }else{
   //  var count = count;
   //  }
    
    var ret
            = '<div class="setObject cursor_move" data-type="labelOnly" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">' +
            '<div class="fields_below">' +
            '<div class="setObjects_actions ">' +
            /*'<div class="pull-left">' +
            '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>' +
            '</div>' +*/
            '<div class="obj_actions display" id="obj_actions_' + count + '">' +
            '<ul class="fl-options-data-type">'+
            '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="label" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i>' +
            '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i>' +
            '</ul>'+
            '</div>' +
            '</div>' +
            '<div class="label_below obj_label" id="label_' + count + '" style="">' +
            '<label id="lbl_' + count + '" style="font-size:14px;" type="label" class="getFields_' + count + '" name="label_' + count + '">Label Caption:</label>' +
            '</div>' +
            '<div class="input_position_below" id="obj_fields_' + count + '">' +
            '</div>' +
            '</div>' +
            '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-label"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">Label Caption:</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

// //TEXTBOX DESIGN2
// function textbox(count, object_type) {
    // var ret
    // =   '<div class="setObject cursor_move" data-type="textbox" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" vertical-align: top;display: table; z-index=' + count + ' ">'+
    //         '<div class="setObjects_actions">'+
    //          iv class="pull-left">'+
    //                 '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>'+
    //             '</div>'+
    //             '<div class="obj_actions display" id="obj_actions_' + count + '">'+
    //                 '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i>'+
    //                 '<i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i>'+
    //             '</div>'+
    //         '</div>'+
    //         '<div class="fields_below" style="display: table;width: 100%;height: 100%;">'+
    //             '<div style="display: table-row;float: none;clear: both;" class="label_below obj_label" id="label_' + count + '">'+
    //                 '<div style="display:table-cell;height:20px;">'+
    //                     '<div style="position:relative;width: 100%;height: 100%;">'+
    //                         '<label style="height: 20px;" id="lbl_' + count + '">Label Name:</label>'+
    //                     '</div>'+
    //                 '</div>'+
    //             '</div>'+
    //             '<div style="display: table-row;" class="input_position_below" id="obj_fields_' + count + '">'+
    //                 '<div style="display:table-cell;">'+
    //                     '<div style="position:relative;width: 100%;height: 100%;">'+
    //                         '<input style="height: 100%;margin: 0px;" data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_1" name="textbox_1" id="getFields_1" data-placement="bottom">'+
    //                     '</div>'+
    //                 '</div>'+
    //             '</div>'+
    //         '</div>'+
    //     '</div>';
    // return ret;
// }
//


// Contact Number Design
function contactNumber(count, object_type) {
    var ret = "";
    ret += '<div class="setObject cursor_move" data-type="contactNumber" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
        ret += '<div class="fields_below">'; //  style="min-width:200px;"
            ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
                ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
                        ret += '<ul class="fl-options-data-type">';
                            ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
                            ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
                        ret +='</ul>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Contact Number:</label></div>';
            ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
                ret += '<div class="original-contact-field-container isDisplayNone">';
                    ret += '<input style="height:30px; font-size:14px" data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="contactNumber_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>';
                ret += '</div>';
                ret += '<div class="contact-number-temp-field-container" style="overflow:auto;max-height:130px;position:relative;">';
                    ret += '<span style=" position: relative; display: inline-table; width:100%;">';
                        ret += '<div style=" display: table-cell; vertical-align: middle; height: 100%;">';
                            ret += '<span style="right:12px;position: absolute; margin-top: -5px;">';
                                ret += '<span class="add-cell-contact"><i class="fa fa-plus cursor"></i></span>&nbsp;&nbsp;<span class="minus-cell-contact isDisplayNone" style=""><i class="fa fa-minus cursor"></i></span>';
                            ret += '</span>';
                        ret += '</div>';
                        ret += '<input style="" maxlength="11" data-type="longtext" type="text" disabled="disabled" class="tip form-text contact-number-temp-field numeric" data-placement="bottom">';
                    ret += '</span>';
                ret += '</div>';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';

                if($('#slideLayersMenu').text()== 1){   
                    var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-textbox"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">contactNumber_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

// TextBox Design
function textbox(count, object_type) {
    var ret = "";
    ret += '<div class="setObject cursor_move" data-type="textbox" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
        ret += '<div class="fields_below">'; //  style="min-width:200px;"
            ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
                ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
                        ret += '<ul class="fl-options-data-type">';
                            ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
                            ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
                        ret +='</ul>';
                ret += '</div>';
            ret += '</div>';
                ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Text Box:</label></div>';
                ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
                ret += '<input style="height:30px; font-size:14px" data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="textbox_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-textbox"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">textbox_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}
 
// Color picker
function ColorPickerObject(count, object_type) {
    var ret = "";
    ret += '<div class="setObject cursor_move" data-type="colorpicker" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="min-width: initial; width: auto; z-index=' + count + ' ">';
        ret += '<div class="fields_below">';
            ret += '<div class="setObjects_actions ">';
                ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
                        ret += '<ul class="fl-options-data-type">';
                            ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
                            ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
                        ret +='</ul>';
                ret += '</div>';
            ret += '</div>';
                ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Pick Color:</label></div>';
                ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
                ret += '<input style="height:30px; font-size:14px" data-type="longtext" type="text" disabled="disabled" class="tip form-text forma-colorpicker getFields getFields_' + count + '" name="colorpicker_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-bucket"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">colorpicker_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

// Checkbox Design
// function checkbox(count, object_type) {
//     var ret = "";
//     ret += '<div class="setObject cursor_move" data-type="checkbox" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
//     ret += '<div class="fields_below">'; //  style="min-width:200px;"
//     ret += '<div class="setObjects_actions">';
//     ret += '<div class="pull-left">';
//     ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
//     ret += '</div>';
//     ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
//     ret += '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="2" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i>';
//     ret += '<i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i>';
//     ret += '</div>';
//     ret += '</div>';
//     ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Label Name:</label></div>';
//     ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
//     ret += '<label><input data-type="longtext" type="checkbox" disabled="disabled" class="getFields getFields_' + count + '" name="checkbox_' + count + '[]" id="getFields_' + count + '"  value="Sample 1" /> Sample 1</label><br />';
//     ret += '<label><input data-type="longtext" type="checkbox" disabled="disabled" class="getFields getFields_' + count + '" name="checkbox_' + count + '[]" id="getFields_' + count + '" value="Sample 2" /> Sample 2</label><br />';
//     ret += '<label><input data-type="longtext" type="checkbox" disabled="disabled" class="getFields getFields_' + count + '" name="checkbox_' + count + '[]" id="getFields_' + count + '" value="Sample 3" /> Sample 3</label><br />';
//     ret += '</div>';
//     ret += '</div>';
//     ret += '</div>';


//     return ret;
// }

// Checkbox Design version 2-3
function checkbox(count, object_type) {
    var ret = "";
    ret += '<div class="setObject cursor_move" data-type="checkbox" data-object-id="' + count + '" rc-align-type="vertical" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="min-width: 13px;z-index=' + count + ' ">';
    ret += '<div class="fields_below" style="min-height: 15px;">'; //  style="min-width:200px;"
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs icon-cogs pull-left cursor object_setup object_properties" data-properties-type="2" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Checkbox:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<label><input data-type="longtext" type="checkbox" disabled="disabled" class="getFields getFields_' + count + '" name="checkbox_' + count + '[]" id="getFields_' + count + '"  value="Sample 1" /><span class="checkbox-input-label" style="display: inline;"> Sample 1</span></label><span class="rc-prop-alignment rc-align-vertical"></span>';
    ret += '<label><input data-type="longtext" type="checkbox" disabled="disabled" class="getFields getFields_' + count + '" name="checkbox_' + count + '[]" id="getFields_' + count + '" value="Sample 2" /><span class="checkbox-input-label" style="display: inline;"> Sample 2</span></label><span class="rc-prop-alignment rc-align-vertical"></span>';
    ret += '<label><input data-type="longtext" type="checkbox" disabled="disabled" class="getFields getFields_' + count + '" name="checkbox_' + count + '[]" id="getFields_' + count + '" value="Sample 3" /><span class="checkbox-input-label" style="display: inline;"> Sample 3</span></label><span class="rc-prop-alignment rc-align-vertical"></span>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-checkbox"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">checkbox_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip"  data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

// Dropdown Design
function dropdown(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="dropdown" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below" >'; // style="min-width:200px;"
    ret += '<div class="setObjects_actions ">';
   /* ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="2" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Dropdown Option Items:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<select style="font-size:14px;" data-type="longtext" disabled="disabled" class="form-select getFields getFields_' + count + '" name="dropdown_' + count + '" id="getFields_' + count + '" >';
    ret += '<option selected="selected" value="">--Select--</option>';
    ret += '<option value="Sample 1">Sample 1</option>';
    ret += '<option value="Sample 2">Sample 2</option>';
    ret += '<option value="Sample 3">Sample 3</option>';
    ret += '</select>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-dropdown-list"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">dropdown_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

// Radio Button Design
// function radioButton(count, object_type) {
//     var ret = "";
//     ret += '<div class="setObject cursor_move" data-type="radioButton" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
//     ret += '<div class="fields_below" >'; // style="min-width:200px;"
//     ret += '<div class="setObjects_actions">';
//     ret += '<div class="pull-left">';
//     ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
//     ret += '</div>';
//     ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
//     ret += '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="2" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i>';
//     ret += '<i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i>';
//     ret += '</div>';
//     ret += '</div>';
//     ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Label Name:</label></div>';
//     ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
//     ret += '<label><input type="radio" data-type="longtext" disabled="disabled" name="radio_' + count + '" class="getFields getFields_' + count + '" id="getFields_' + count + '" value="Sample 1"/> Sample 1</label><br />';
//     ret += '<label><input type="radio" data-type="longtext" disabled="disabled" name="radio_' + count + '" class="getFields getFields_' + count + '"  id="getFields_' + count + '" value="Sample 2"/> Sample 2</label><br />';
//     ret += '<label><input type="radio" data-type="longtext" disabled="disabled" name="radio_' + count + '" class="getFields getFields_' + count + '"  id="getFields_' + count + '" value="Sample 3"/> Sample 3</label><br />';
//     ret += '</div>';
//     ret += '</div>';
//     ret += '</div>';
//     return ret;
// }

// Radio Button Design version2-3
function radioButton(count, object_type) {
    var ret = "";
    ret += '<div class="setObject cursor_move" data-type="radioButton" data-object-id="' + count + '" rc-align-type="vertical" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below" style="min-height:15px;" >'; // style="min-width:200px;"
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="2" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Opt Radio:</label></div>';
    ret += '<div style="font-size:14px;" class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<label><input type="radio" checked="checked" data-type="longtext" disabled="disabled" name="radio_' + count + '" class="getFields getFields_' + count + '"  id="getFields_' + count + '" value="none"/><span class="radio-input-label"> none</span></label><span class="rc-prop-alignment rc-align-vertical"></span>';
    ret += '<label><input type="radio" data-type="longtext" disabled="disabled" name="radio_' + count + '" class="getFields getFields_' + count + '" id="getFields_' + count + '" value="Sample 1"/><span class="radio-input-label"> Sample 1</span></label><span class="rc-prop-alignment rc-align-vertical"></span>';
    ret += '<label><input type="radio" data-type="longtext" disabled="disabled" name="radio_' + count + '" class="getFields getFields_' + count + '"  id="getFields_' + count + '" value="Sample 2"/><span class="radio-input-label"> Sample 2</span></label><span class="rc-prop-alignment rc-align-vertical"></span>';
    ret += '<label><input type="radio" data-type="longtext" disabled="disabled" name="radio_' + count + '" class="getFields getFields_' + count + '"  id="getFields_' + count + '" value="Sample 3"/><span class="radio-input-label"> Sample 3</span></label><span class="rc-prop-alignment rc-align-vertical"></span>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    
if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-radio-button"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">radio_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

// // TextArea Design
// function textArea(count, object_type) {
//     var ret = "";

//     ret += '<div class="setObject cursor_move" data-type="textArea" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
//     ret += '<div class="fields_below">'; // style="min-width:200px;"
//     ret += '<div class="setObjects_actions">';
//     ret += '<div class="pull-left">';
//     ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
//     ret += '</div>';
//     ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
//     ret += '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i>';
//     ret += '<i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i>';
//     ret += '</div>';
//     ret += '</div>';
//     ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Label Name:</label></div>';
//     ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
//     ret += '<textarea data-type="longtext" class="form-textarea getFields getFields_' + count + '" disabled="disabled" name="textarea_' + count + '" id="getFields_' + count + '" "></textarea>';
//     ret += '</div>';
//     ret += '</div>';
//     ret += '</div>';
//     return ret;
// }

// TextArea Design2
function textArea(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="textArea" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="display:inline-table;z-index=' + count + ' ">';
    ret += '<div class="fields_below" style="height:100%;display:table-cell;">'; // style="min-width:200px;"
    ret += '<div class="setObjects_actions">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret +=  '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Textarea:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '" style="height: calc(100% - 25px);">';
    ret += '<textarea style="resize:none;width:100%;height:100%;margin:0px;font-size:14px;" data-type="longtext" class="form-textarea getFields getFields_' + count + '" disabled="disabled" name="textarea_' + count + '" id="getFields_' + count + '" "></textarea>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-textarea"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">textarea_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}
// Select Many Design
function selectMany(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="selectMany" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below">'; // style="min-width:200px;"
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="2" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret +='</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Select Multiple Items:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<select data-type="longtext" style="font-size:14px;" class="form-selectMany getFields getFields_' + count + '" disabled="disabled" name="select_' + count + '[]" id="getFields_' + count + '"  multiple="multiple">';
    ret += '<option value="Sample 1">Sample 1</option>';
    ret += '<option value="Sample 2">Sample 2</option>';
    ret += '<option value="Sample 3">Sample 3</option>';
    ret += '</select>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-select-many"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">select_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");  
                }
    return ret;
    
}

// File Upload Design
function fileUpload(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="fileUpload" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below">'; // style="min-width:200px;"
    ret += '<div class="setObjects_actions">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Upload File:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input style="height:30px;" data-type="longtext" type="file" class="getFields getFields_' + count + '" name="upload_' + count + '" disabled="disabled" id="getFields_' + count + '" />';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    //return ret;
}

// Date Picker Design
function datepicker(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="datepicker" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below">'; // style="min-width:200px;"
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Date Picker:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input style="height:30px;font-size:14px;" data-type="date" type="text" disabled="disabled" class="form-text getFields getFields_' + count + '" name="datepicker_' + count + '" id="getFields_' + count + '" />';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';


if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-date-picker"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">datepicker_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

// Full name
function fullname(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="fullname" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below" style="min-width:200px;">';
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Label Name:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="Firstname" disabled="disabled" >';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="Middlename" disabled="disabled" >';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="Lastname" disabled="disabled" >';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="Suffix" disabled="disabled" >';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="Nickname" disabled="disabled" >';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    return ret;
}

// Date Time Design
function dateTime(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="dateTime" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below">';
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/ 
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret  += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Date Time Picker:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input style="height:30px;font-size:14px;" data-type="dateTime" type="text" disabled="disabled" class="form-text getFields getFields_' + count + '" name="dateTime_' + count + '" id="getFields_' + count + '" />';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-date-time-picker"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">Time_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

// Time Design
function TimePick(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="time" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below">';
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Time Picker:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input style="height:30px; font-size:14px;" data-type="time" type="text" disabled="disabled" class="form-text getFields getFields_' + count + '" name="Time_' + count + '" id="getFields_' + count + '" />';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

 
if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-time-picker"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">Time_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-original-title="Object Properties" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

// Website Design
function website(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="website" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below" style="min-width:200px;">';
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>'    
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Label Name:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-text getFields getFields_' + count + '" name="website_' + count + '" id="getFields_' + count + '" />';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    return ret;
}

// Full Address Design
function fullAddress(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="fullAddress" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below" style="min-width:200px;">';
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Label Name:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="Street Address" disabled="disabled" >';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="Address 2" disabled="disabled" >';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="City / Province" disabled="disabled" >';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="Postal / Zip Code" disabled="disabled" >';
    ret += '<input data-type="longtext" type="text" disabled="disabled" class="form-field getFields getFields_' + count + '" name="getFields" id="getFields_' + count + '"  placeholder="Nickname" disabled="disabled" >';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    return ret;
}

// Button
function btn(count, object_type) {
    var ret = "";
    //modified by joshua reyes 03/07/2016
    //added this attr to div -> data-visibility-button=""

    //ret += '<input type="submit" class="btn-basicBtn " value="Button '+ count +'" style="margin:5px 0px 5px 5px;">';
    ret += '<div data-object-id="' + count + '" data-type="button" class="setObject setObject_btn" id="setObject_' + count + '" data-toggle="popover" data-placement="right" data-visibility-button="" style="display: inline-block;margin-left:3px;margin-top: 5px;position:relative;">';
    ret += '<input data-type="button" type="button" class="btn-basicBtn borderR3" name="submit_button_'+count+'" id="btnName_' + count + '" value="Button_' + count + '" style="margin:5px 5px 5px 5px;">';
    ret += '<div class="btn-actions" style="width:37px; display:inline-block;">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="4" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times pull-left icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';    
    ret += '</div>';

    return ret;
}

// Line
function createLine(count, object_type) {
    var ret = "";
    // var pathname = window.location.pathname;
    // //console.log(pathname)
    // if (pathname == "/user_view/generate") {
    // var new_count = $("button[data-object-type='" + object_type + "']").attr("data-count");
    // var count_setObject = $(".setObject").length;
    // //console.log(count_setObject)
    // var count = parseInt(new_count) + parseInt(count) + parseInt(count_setObject);
    // //console.log(count)
    // }else{
    // var count = count;
    // }
   
    ret += '<div class="setObject cursor_move" data-type="createLine" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="display:block; z-index=' + count + ' ">';
    ret += '<div class="fields_below" style="min-height: 5px;">';//style="min-width:200px;"
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="3" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Form Line:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<div data-type="createLine" class="getFields getFields_' + count + '" id="getFields_' + count + '" style="background-color:black; height:3px; width:100%;"></div>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-line"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">setObject_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}
//list names
function fieldListOfNames(count, object_type) {
    var ret
            = '<div class="setObject cursor_move" data-type="listNames" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">' +
            '<div class="fields_below" >' + //style="min-width:200px;"
            '<div class="setObjects_actions ">' +
            /*'<div class="pull-left">' +
            '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>' +
            '</div>' +*/
            '<div class="obj_actions display" id="obj_actions_' + count + '">' +
            '<ul class="fl-options-data-type">'+
            '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="1" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>' +
            '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>' +
            '</ul>'+
            '</div>' +
            '</div>' +
            '<div class="label_below obj_label" id="label_' + count + '">' +
            '<label id="lbl_' + count + '">Names:</label>' +
            '</div>' +
            '<div class="input_position_below" id="obj_fields_' + count + '">' +
            '<div style="display:table;width:100%;" id="field-container-listnames_' + count + '" class="field-container-listnames field-container-listnames_' + count + '">' +
            '<input style="min-width:30px;height:30px;display:table-cell; font-size:14px;" data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="listnames_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>' +
            '<span style="width:19px;display:table-cell;white-space:nowrap;"><a class="btn-basicBtn padding_5 cursor tip viewNames" data-original-title="Select Name" style="margin-left:5px;" return-field="getFields_' + count + '"><i class="icon-list-alt fa fa-list-alt"></i></a></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-names"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">listnames_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

//Table Design
function createTable(count, object_type) {
    var responsive_table = false;
    if($('.table-form-design-relative').length >= 1){
        responsive_table = true;
    }
    var ret
            = '<div class="setObject cursor_move" data-type="table" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="display:inline-table;z-index=' + count + ' ">' +
            '<div class="fields_below" style="height:100%;">' +
            '<div class="setObjects_actions ">' +
            /*'<div class="pull-left">' +
            '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>' +
            '</div>' +*/
            '<div class="obj_actions display" id="obj_actions_' + count + '">' +
            '<ul class="fl-options-data-type">'+
            '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="formtable" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>' +
            '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>' +
            '</ul>'+
            '</div>' +
            '</div>' +
            '<div class="label_below obj_label" id="label_' + count + '">' +
            '<label style="cursor:move;" id="lbl_' + count + '">Form Table Object:</label>' +
            '</div>' +
            '<div class="input_position_below" id="obj_fields_' + count + '" style="height:calc(100% - 25px);">' + //overflow:auto; FS#7728
            '<table class="getFields form-table" fld-table-container-responsive="'+responsive_table+'"></table>' +
            '</div>' +
            '</div>' +
            '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-table"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">setObject_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-original-title="Object Properties" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

//Table Design 2
// function createTable(count, object_type) {
//     var ret
//     =   '<div class="setObject cursor_move" data-type="table" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" vertical-align: top;display: table; z-index=' + count + ' ">'+
//             '<div class="setObjects_actions">'+
//                 '<div class="pull-left">'+
//                     '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>'+
//                 '</div>'+
//                 '<div class="obj_actions display" id="obj_actions_' + count + '">'+
//                     '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="formtable" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i>' +
//                     '<i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i>' +
//                 '</div>'+
//             '</div>'+
//             '<div class="fields_below" style="display: table;width: 100%;height: 100%;">'+
//                 '<div style="display: table-row;float: none;clear: both;" class="label_below obj_label" id="label_' + count + '">'+
//                     '<div style="display:table-cell;height:20px;">'+
//                         '<div style="position:relative;width: 100%;height: 100%;">'+
//                             '<label style="height: 20px;" id="lbl_' + count + '">Label Name:</label>'+
//                         '</div>'+
//                     '</div>'+
//                 '</div>'+
//                 '<div style="display: table-row;" class="input_position_below" id="obj_fields_' + count + '">'+
//                     '<div style="display:table-cell;">'+
//                         '<div style="position:relative;width: 100%;height: 100%;">'+
//                             '<table class="form-table"></table>' +
//                         '</div>'+
//                     '</div>'+
//                 '</div>'+
//             '</div>'+
//         '</div>';
//     return ret;
// }

// Computed Field Design
function computedField(count, object_type) {
    var ret = "";
    ret += '<div class="setObject cursor_move" data-type="computed" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
    ret += '<div class="fields_below">'; //  style="min-width:200px;"
    ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
    ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
    ret += '<ul class="fl-options-data-type">';
        ret += '<li><i class="fa pull-left fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="computed" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
    ret += '</ul>';
    ret += '</div>';
    ret += '</div>';
    ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Computed:</label></div>';
    ret += '<div class="input_position_below" id="obj_fields_' + count + '">';
    ret += '<input style="height:30px;font-size:14px;" data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="computed_field_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';


if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-computed"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">computed_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}
//request image
function photos(count, object_type) {
    var page = $("#main_page").val();
    var ret
            =   '<div class="setObject cursor_move resize_image_request" data-type="imageOnly" data-upload="photo_upload" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style="z-index=' + count + ' ">' +
                    '<div class="fields_below" style="">' +
                        '<div class="setObjects_actions ">' +
                            /*'<div class="pull-left">' +
                                '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>' +
                            '</div>' +*/
                            '<div class="obj_actions display" id="obj_actions_' + count + '">' +
                            '<ul class="fl-options-data-type">'+
                                '<li><i class="  fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="photos" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>' +
                                '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>' +
                            '</ul>'+
                            '</div>' +
                        '</div>' +
                        '</div>' +
                        '<div>' +
                        '<div class="uiform-image-resizable" style="position:relative;overflow:hidden;display:table-cell;width:100px;height:100px;">' +
                    
                            '<div style="position:relative;height:100%;width:100%;">'+
                                    '<img id="getFields_' + count + '" class="getFields ImagegetFields_' + count + '" src="' + page + 'images/avatar/large.png" style="cursor:pointer;background-color:#fff;width:100%;height:100%;">' +
                            '</div>'+
                            '<form id="getpostSelectFormPhotos" method="post" enctype="multipart/form-data" action="/ajax/formUpload" style="overflow:visible; height:0px; z-index: 1000000000000;position: relative;">'+
                                //'<input type="file" data-action-id="2" value="upload" name="image" id="formImage" size="24" data-action-type="postImageForm" style="opacity: 0;position: absolute;" class="">'+
                                //'<i class="icon-picture"></i> Image'+
                                '<div id="uniform-fileInput" data-object-id="' + count + '"form_upload_photos class="uploader2 isDisplayNone upload_photos_' + count +'">'+
                                //'<input type="file" data-action-id="' + count + '" value="upload" name="image" id="postSelectFormPhotos_' + count + '" size="24" data-action-type="postImageForm">'+
                                '<span id="uploadFilename_2" class="filename">No file selected</span>'+
                                '<span class="action">Choose File</span>'+
                                
                                '</div>'+
                            '</form>'+
                        '<input data-type="longtext" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + ' display" name="request_image_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>'+
                        '</div>' +
                        
                        
                '</div>';
   
if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-request-image"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">request_image_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");   
                }
    return ret;
    
}

// Image Design
//image
function getimageForm(count, object_type, img) {
    // var pathname = window.location.pathname;
    // //console.log(pathname)
    // if (pathname == "/user_view/generate") {
    // var new_count = $("button[data-object-type='image']").attr("data-count");
    // var count_setObject = $(".setObject").length;
    // //console.log(object_type)
    // //console.log(new_count)
    // var count = parseInt(new_count) + parseInt(count) + parseInt(count_setObject);
    // //console.log(count,"sam")
    // }else{
    // var count = count;
    // }
    var ret
            =   '<div class="fl-requestImg setObject cursor_move" data-img="formImgDesign" data-type="imageOnly" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right"  style=" z-index=' + count + ' ">' +
                    '<div class="fields_below" style="">' +
                        '<div class="setObjects_actions ">' +
                            /*'<div class="pull-left">' +
                                '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>' +
                            '</div>' +*/
                        '<div class="obj_actions display" id="obj_actions_' + count + '">' +
                            // '<li><i class="fa fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="label" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i>' +
                            '<ul class="fl-options-data-type">'+
                                '<li><i class="fa pull-left fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="justImage" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>'+
                                '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>'+
                          '</ul>'+
                        '</div>' +
                    '</div>' +
                    '<div class="uiform-image-resizable" style="position:relative;overflow:hidden;display:table-cell;width:100px;height:100px;">' +
                        '<div style="position:relative;height:100%;width:100%;">'+
                            '<img id="getFields_' + count + '" class="getFields getFields_' + count + '" src="' + img + '" style="width:100%;height:100%;">' +
                        '</div>'+
                    '</div>' +
                '</div>';

if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-image"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">Image_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-properties-type="justImage" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

function barCodeScanner(count, object_type){
    var ret
    =   '<div class="setObject cursor_move" data-type="barCodeScanner" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">'+
            '<div class="fields_below">'+ //  style="min-width:200px;"
                '<div class="setObjects_actions ">'+
                    /*'<div class="pull-left">'+
                        '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>'+
                    '</div>'+*/
                    '<div class="obj_actions display" id="obj_actions_' + count + '">'+
                        '<ul class="fl-options-data-type">'+
                        '<li><i class="fa pull-left fa-cogs icon-cogs cursor object_setup object_properties" data-properties-type="barcode_property" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>'+
                        '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>'+
                        '</ul>'+
                    '</div>'+
                '</div>'+
                '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Bar Code:</label></div>'+
                '<div class="input_position_below" id="obj_fields_' + count + '">'+
                    '<input style="height:30px;font-size:14px;" data-type="longtext" placeholder="Code Here" type="text" disabled="disabled" class="tip form-text getFields getFields_' + count + '" name="barcode_scan_' + count + '" id="getFields_' + count + '" data-placement="bottom"/>'+
                '</div>'+
            '</div>'+
        '</div>';
if($('#slideLayersMenu').text()== 1){   
                 var lay = '<div class="ghost" id="layer_'+ count +'" layer-id="'+ count +'">';
                    lay +=      '<div class="fl-widget-info" id="layer_'+ count +'"">';
                    lay +=                    '<div class="fl-widget-contentv2 fl-widget-contentv3">';
                    lay +=                        '<div class="drag_handler">';
                    lay +=                            '<svg class="layer_img cursor icon-svg icon-svg-workspace" viewBox="0 0 90 90">';
                    lay +=                                            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-barcode"></use>';
                    lay +=                                        '</svg>';         
                    lay +=                        '</div>';
                    lay +=                        '<div class="fl-widget-starred-content-wrapperv3 theme-font-color">';
                    lay +=                            '<div class="fl-wcheader">';
                    lay +=                                '<div class="fl-wchname" style="padding-top: 3px;padding-bottom: 5px;width:200px"><span class="layerSpan" id="list_'+ count +'">barcode_'+ count +'</span>';
                    lay +=                                    '<i class="fa pull-right layersetting fa-times icon-remove cursor object_setup object_remove tip" data-object-id="'+ count +'" data-original-title="Remove Object"></i>';
                    lay +=                                    '<i class="fa layer pull-right layersetting object_setup   fa-cogs icon-cogs cursor tip" data-original-title="Object Properties" data-object-id="'+ count +'" id="object_properties_'+ count +'" ></i>';
                    lay +=                                '</div>';    
                    lay +=                                '<div class="fl-wchdate">';
                    lay +=                                    '<br>';
                    lay +=                                '</div>';                                           
                    lay +=                            '</div>';
                    lay +=                        '</div>';  
                    lay +=                    '</div>';
                    lay +=                '</div>';
                    lay +=     '</div>';
                    $('.layerContainer').append(lay);
                    $('.ps-container').perfectScrollbar("update");
                }
    return ret;
    
}

function formDataTable(count, object_type){
    var ret = "";
    ret += '<div class="setObject cursor_move" data-type="formDataTable" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + '; width: 600px; height: 250px; ">';
        ret += '<div class="fields_below" style="width:100%;height:100%;">'; //  style="min-width:200px;"
            ret += '<div class="setObjects_actions ">';
    /*ret += '<div class="pull-left">';
    ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
    ret += '</div>';*/
                ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
                        ret += '<ul class="fl-options-data-type">';
                            ret += '<li><i class="fa fa-eye pull-left icon-cogs cursor object_setup object_properties" data-properties-type="Form DataTable Properties" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '" data-original-title="Form DataTable Properties"></i></li>'
                            ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="formDataTable" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
                            ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
                        ret +='</ul>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div class="label_below obj_label" id="label_' + count + '"><label id="lbl_' + count + '">Form DataTable:</label></div>';
            ret += '<div class="input_position_below" id="obj_fields_' + count + '" style="height:calc(100% - 25px);">';
                //design
                ret += '<div class="getFields form-data-table getFields_' + count + '" form-data-table-name="form_data_table_' + count + '" style="height:100%;min-width:300px;width:100%;outline:1px solid #ccc;overflow:auto;">';
                    ret += '<div class="form-datatable-wrapper"></div>';
                    ret += '<div class="form-datatable-widget"></div>'
                ret += '</div>';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    return ret;
}

function tagging(count, object_type) {
    var ret = "";

    ret += '<div class="setObject cursor_move" data-type="text-tagging" data-object-id="' + count + '" id="setObject_' + count + '" data-toggle="popover" data-placement="right" style=" z-index=' + count + ' ">';
        ret += '<div class="fields_below" style="width:100%;height:100%;" >'; // style="min-width:200px;"
            ret += '<div class="setObjects_actions ">';
           /* ret += '<div class="pull-left">';
            ret += '<img src="/images/loader/load.gif" class="display" id="obj_load_' + count + '"/>';
            ret += '</div>';*/
                ret += '<div class="obj_actions display" id="obj_actions_' + count + '">';
                    ret += '<ul class="fl-options-data-type">';
                        ret += '<li><i class="fa fa-cogs pull-left icon-cogs cursor object_setup object_properties" data-properties-type="text-tagging" id="object_properties_' + count + '" data-object-id="' + count + '" data-object-type="' + object_type + '"  data-original-title="Properties"></i></li>';
                        ret += '<li><i class="fa fa-times icon-remove cursor object_setup object_remove" data-object-id="' + count + '" data-original-title="Remove"></i></li>';
                    ret += '</ul>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div class="label_below obj_label" id="label_' + count + '">'
                ret += '<label id="lbl_' + count + '">Tagging:</label>';
            ret += '</div>';
            ret += '<div class="input_position_below" id="obj_fields_' + count + '" style="width:100%;height:calc(100% - 11px);">';
                // ret += '<input type="text" data-type="longtext" class="getFields_' + count + ' getFields" name="text_tagging_' + count + '" id="getFields_' + count + '" />';
                ret += '<select data-type="longtext" class="tagging-input getFields_' + count + ' getFields" multiple="multiple" placeholder="Select Recipient/s:"  name="text_tagging_' + count + '[]" id="getFields_' + count + '" style="width:100%;height:100%;"></select>';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';

    return ret;
}