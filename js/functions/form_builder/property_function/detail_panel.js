function detailPanelDesign(){
    var ret = "";
  
    ret += '<div class= "detP-master-container">';
        ret += '<div class = "detP-slave-container section clearing fl-field-style"><div class="column div_1_of_1"><div class="fR"><span class="detP-add-minus-row"><i class="fa fa-plus detP-add-row"></i> <i class="fa fa-minus detP-minus-row"></i></span></div>';
            ret += '<input type="text" class="form-text" data-properties-type = "details-panel-name" />';
                ret += '<table class="detP-table-master">';
                    ret += '<thead>';
                       /* ret += '<tr>';
                            ret += '<th>Title</th>';
                            ret += '<th>Formula</th>';
                        ret += '</tr>';*/
                    ret += '</thead>';
                    ret += '<table class="detP-table-slave">';
                        ret += '<tbody class="detP-tr-group">';
                            ret += '<tr>'; 
                                ret += '<td colspan="2"></td>';//
                            ret += '</tr>';
                            ret += '<tr>';
                                ret += '<td class="details-panel-title-td" style="padding-right:5px;padding-top:10px;">Title:<input type="text" data-properties-type="details-panel-title" class="form-text"/></td>';
                                ret += '<td colspan="2" style="padding-top:10px;">Formula:<div class="fR"><span class="detP-add-minus-row"><i class="fa fa-plus detP-add-row-inner"></i> <i class="fa fa-minus detP-minus-row-inner"></i></span></div><textarea class="detP-formula form-textarea" data-properties-type="details-panel-formula" data-ide-properties-type="detP-formula"></textarea><label style="position:relative;top:5px;"><input type="checkbox" value="yes" class="css-checkbox linkmakerClass" id="linkmaker0" /><label for="linkmaker0" class="linkmakerLabel css-label"></label> Create Link</label>';
                                    
                                ret += '</td>';
                                
                            ret += '</tr>';
                            ret += '<tr class="link-form-select">';
                                ret += '<td>Form: </td>';
                                ret += '<td style="padding-top:10px;">';
                                    ret += '<select class="form-select details-condition-form" data-properties-type="linkInputAdder-form" data-object-id="" id="">';
                                    ret += '<option value="0">--Select Form--</option>';
                                    ret += '</select>';
                                ret += '</td>';
                            ret += '</tr>';
                            
                            ret += '<tr class="linkInputAdder">';
                                ret += '<td colspan="2" style="padding-top: 10px;">';
                                    ret += '<table class="linkInputAdder-table">';
                                        ret += '<tbody class="linkInputAdder-table-tbody">';

                                            ret += '<tr>'; 
                                                ret += '<td colspan="3"><div><span class="detP-add-minus-row"><i class="fa fa-plus linkInputAdder-add-row"></i> <i class="fa fa-minus linkInputAdder-minus-row"></i></span></div></td>';
                                            ret += '</tr>';
                                            ret += '<tr>';
                                                ret += '<td style="padding-right:5px;"><select class="form-select details-condition-fields" data-properties-type="linkInputAdder-field" data-object-id="" id="">';
                                                ret += '<option value="0">--Select Fields--</option>';
                                                ret += '</select></td>';
                                                ret += '<td style="padding-right:5px;"><select class="form-select details-condition-operator" data-properties-type="linkInputAdder-operator" data-object-id="" id="">';
                                                ret += '<option value="0">--Select Condition--</option>';
                                                ret += '<option value="=">Equal (==)</option>';
                                                ret += '<option value="<=" class="number_operator">Less than equal (&lt;=)</option>';
                                                ret += '<option value=">=" class="number_operator">Greater than equal (&gt;=)</option>';
                                                ret += '<option value=">" class="number_operator">Greater than(&gt;)</option>';
                                                ret += '<option value="<" class="number_operator">Less than (&lt;)</option>';
                                                ret += '<option value="!=">Not equal(!=)</option>';
                                                ret += '<option value="%">Contains(%)</option>';

                                                ret += '</select></td>';
                                                ret += '<td><input type="text" data-properties-type="linkInputAdder-value" class="form-text"/></td>';
                                            ret += '</tr>';
                                        ret += '</tbody>';
                                    ret += '</table>';
                                ret += '</td>';
                                
                            ret += '</tr>';
                        ret += '</tbody>';
                    ret += '</table>';
                ret += '</table>';
            ret +='</div>';
        ret +='</div>';
    ret += '</div>';

    return ret;
}
function detailPanelPlusMinus(){
	var linkmaker_ctr = 0;
    $('.linkInputAdder').hide();
    $('.link-form-select').hide();
    $('.linkmakerClass').each(function(){
        $(this).on('click',function(){
            if($(this).attr("checked")){
                $(this).closest('tbody.detP-tr-group').find('.linkInputAdder').show();
                $(this).closest('tbody.detP-tr-group').find('.link-form-select').show()
            }
            else{
                $(this).closest('tbody.detP-tr-group').find('.linkInputAdder').hide();
                $(this).closest('tbody.detP-tr-group').find('.link-form-select').hide();
            }
        });
    });
	MakePlusPlusRowV2({
        "tableElement":".detP-master-container",
        "rules":[
            {
                "eleMinus":".linkInputAdder-minus-row",
                "eleMinusTarget":"tbody.linkInputAdder-table-tbody",
                "elePlus":".linkInputAdder-add-row",
                "elePlusTarget":"tbody.linkInputAdder-table-tbody",
                "elePlusTargetFn":function(){
                    var self_ele = $(this);
                    self_ele.css({
                        //"margin-top":"15px",
                    });
                    if(self_ele.closest('.linkInputAdder-table').find('.linkInputAdder-table-tbody').length>1){
                        self_ele.find('[data-properties-type="linkInputAdder-field"]').val("0");
                        self_ele.find('[data-properties-type="linkInputAdder-operator"]').val("");
                        self_ele.find('[data-properties-type="linkInputAdder-value"]').val("");
                        self_ele.closest('.linkInputAdder-table').find('.linkInputAdder-table-tbody').each(function(){
                            $(this).find('.fa.fa-minus:eq(0)').show();
                        });
                    }
                    self_ele.closest('.content-dialog-scroll').perfectScrollbar("update");
                },
                "eleMinusTargetFnBefore":function(){
                    var self_ele = $(this);
                    
                    if(self_ele.closest('.linkInputAdder-table').find('tbody.linkInputAdder-table-tbody').length<=2){

                        self_ele.closest('.linkInputAdder-table').find('tbody.linkInputAdder-table-tbody').each(function(){
                            $(this).find('.fa.fa-minus:eq(0)').hide();
                        });
                        
                    }
                }
            },
            {
                "eleMinus":".detP-minus-row",
                "eleMinusTarget":".detP-slave-container",
                "elePlus":".detP-add-row",
                "elePlusTarget":".detP-slave-container",
                "elePlusTargetFn":function(){
                    linkmaker_ctr++;
                    
                    var self_ele = $(this);
                    if(self_ele.closest('.detP-master-container').find('.detP-slave-container').length>=1){
                        self_ele.closest('.detP-master-container').find('.detP-slave-container').each(function(){
                            $(this).find('.fa.fa-minus:eq(0)').show();
                            // self_ele.find('.fa.fa-minus:eq(0)').show();
                        });
                    }
                    
                    self_ele.css({
                       //"margin-top":"15px",
                    });
                    self_ele.find('[data-properties-type="linkInputAdder-field"]').val("0");
                    self_ele.find('[data-properties-type="linkInputAdder-form"]').val("0").trigger("change");
                    self_ele.find('[data-properties-type="linkInputAdder-operator"]').val("");
                    self_ele.find('[data-properties-type="linkInputAdder-value"]').val("");
                    self_ele.find('input[data-properties-type="details-panel-name"]:eq(0)').val("");
                    self_ele.find('input[data-properties-type="details-panel-title"]').val("");
                    self_ele.find('tbody.linkInputAdder-table-tbody:not(:eq(0))').each(function(){
                        $(this).find('.fa.fa-minus:eq(0)').trigger('click');
                    });
                    self_ele.find('tbody.detP-tr-group:not(:eq(0))').each(function(){
                        $(this).find('.fa.fa-minus:eq(0)').trigger('click');
                    });

                    self_ele.find('.linkmakerClass').prop("checked",true).trigger("click");   
                    self_ele.find('.linkmakerClass').attr("id","linkmaker"+linkmaker_ctr);
                    self_ele.find('.linkmakerLabel').attr("for","linkmaker"+linkmaker_ctr);
                    self_ele.closest('.content-dialog-scroll').perfectScrollbar("update");
                },
                "eleMinusTargetFnBefore":function(){
                   
                    if($('.detP-slave-container').length<=2){
                        $('.detP-slave-container').each(function(){
                            $(this).find('.fa.fa-minus:eq(0)').hide();
                        });
                    }
                }
            },
            {
                "eleMinus":".detP-minus-row-inner",
                "eleMinusTarget":"tbody.detP-tr-group",
                "elePlus":".detP-add-row-inner",
                "elePlusTarget":"tbody.detP-tr-group",
                "elePlusTargetFn":function(){
                    linkmaker_ctr++;
                    var self_ele = $(this);
                    self_ele.find('.fa.fa-minus:eq(0)').show();
                    self_ele.find('[data-properties-type="linkInputAdder-form"]').val("0").trigger("change");
                    self_ele.find('[data-properties-type="linkInputAdder-field"]').val("0");
                    self_ele.find('[data-properties-type="linkInputAdder-operator"]').val("");
                    self_ele.find('[data-properties-type="linkInputAdder-value"]').val("");
                    self_ele.find('input[data-properties-type="details-panel-title"]').val("");
                    self_ele.closest('.detP-slave-container').find('tbody.detP-tr-group').each(function(){
                        $(this).find('.fa.fa-minus:eq(0)').show();
                    });
                    self_ele.find('tbody.linkInputAdder-table-tbody:not(:eq(0))').each(function(){
                        $(this).find('.fa.fa-minus:eq(0)').trigger('click');
                    });
                    self_ele.find('.linkmakerClass').prop("checked",true).trigger("click");
                    self_ele.find('.linkmakerClass').attr("id","linkmaker"+linkmaker_ctr);
                    self_ele.find('.linkmakerLabel').attr("for","linkmaker"+linkmaker_ctr);
                    self_ele.closest('.content-dialog-scroll').perfectScrollbar("update");
                    

                },
                "eleMinusTargetFnBefore":function(){
                    var self_ele = $(this);
                    if(self_ele.closest('.detP-slave-container').find('tbody.detP-tr-group').length<=2){
                        self_ele.closest('.detP-slave-container').find('tbody.detP-tr-group').each(function(){
                            $(this).find('.fa.fa-minus:eq(0)').hide();
                        });
                    }
                }
            }
        ]
    });
}

function saveDetailsPanelData(obj_id){
    var detP_array = [];
    if($.type(obj_id) == "undefined"){
       var isShow = $('.detail-default-view:eq(0):checked').val();
       if($.type(isShow)=="undefined"){
           isShow = "no"
       } 
    }
   
    $(".detP-slave-container").each(function(){
        var container_name = $(this).find('input[data-properties-type="details-panel-name"]:eq(0)').val();
        if($.type(obj_id) == "undefined"){
            detP_array.push({"container_name":container_name,
                "title_formula_array":[],
                "default_view": isShow,
            });
        }
        else{
            detP_array.push({"container_name":container_name,
                "title_formula_array":[],
            });  
        }
        
        $(this).find(".detP-tr-group").each(function(){
            var dp_title = $(this).find('input[data-properties-type="details-panel-title"]').val();
            var dp_formula = $(this).find('[data-properties-type="details-panel-formula"]').val();
            var islink = $(this).find('.linkmakerClass:checked').val();
            var link_form = $(this).find('tr.link-form-select').find('[data-properties-type="linkInputAdder-form"]').val();
            var link_val_array = [];
            if($.type(islink) == "undefined"){
                islink = "no";
            }
            $(this).find('.linkInputAdder-table').find('.linkInputAdder-table-tbody').each(function(){
                var field_name = $(this).find('[data-properties-type="linkInputAdder-field"]').val();
                var operator = $(this).find('[data-properties-type="linkInputAdder-operator"]').val();
                var link_val = $(this).find('[data-properties-type="linkInputAdder-value"]').val();

                link_val_array.push({
                    "field_name":field_name,
                    "link_operator":operator,
                    "link_value":link_val
                });
            });

            detP_array[detP_array.length-1]["title_formula_array"].push({"title":dp_title,
                "formula":dp_formula,
                "link": islink,
                "link_val":link_val_array,
                "link_form":link_form
                
            });

        });
        
    });
    if($.type(obj_id) != "undefined"){
        var object_data = $("body").data(""+obj_id+"");
         
        if($.type(object_data) === "undefined"){
            
            object_data = {};
            object_data["saved_details_panel"] = detP_array;
        }
        else{
            object_data["saved_details_panel"] = detP_array;
        }
    }else{
        $('body').data("saved_details_panel",detP_array);
        $('body').data("default_detail_view",isShow);
        $(".fl-closeDialog").click();
    }
    
    
    
    
    
}
function restoreData(data_object){

	var saved_details_panel_object = data_object;

    if($.type(saved_details_panel_object) != "undefined"){
        var default_details_view = saved_details_panel_object['default_detail_view'];
        if($.type(default_details_view)!="undefined"){
            if(default_details_view=="yes"){
                $('.detail-default-view').prop('checked',true);
            }
            else{
                $('.detail-default-view').prop('checked',false);
            }
        }
        var saved_details_panel = saved_details_panel_object['saved_details_panel'];
        var sdp_length = saved_details_panel.length;
        var slave_container_ctr = 0;
        if (sdp_length >= 1){
            for(var ctr in saved_details_panel){
                if(ctr != 0){
                    $(".detP-add-row:eq(0)").trigger("click");
                }
            }
            $('.detP-slave-container').each(function(){
                var self = $(this);

                self.find('input[data-properties-type="details-panel-name"]:eq(0)').val(saved_details_panel[slave_container_ctr]['container_name']);
                for(var ctr in saved_details_panel[slave_container_ctr]['title_formula_array']){
                     
                     var title_formula_array_title_val = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['title'];
                     var title_formula_array_formula_val = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['formula'];
                     var form_value_self = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link_form'];
                    if(ctr != 0){
                        
                        self.find('tbody.detP-tr-group:eq('+(Number(ctr)-1)+')').find('.detP-add-row-inner:eq(0)').trigger("click"); 
                    }
                    var self_children_group_link_box = self.find('tbody.detP-tr-group:eq('+ctr+')').find('tr.linkInputAdder:eq(0)');
                    
                    self.find('input[data-properties-type="details-panel-title"]:eq('+ctr+')').val(title_formula_array_title_val);
                    self.find('[data-properties-type="details-panel-formula"]:eq('+ctr+')').val(title_formula_array_formula_val);
                    console.log("formformform",form_value_self)
                    self.find('tbody.detP-tr-group:eq('+ctr+')').find('[data-properties-type="linkInputAdder-form"]:eq(0)').val(form_value_self).trigger("change");
                    if(saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link'] == "yes"){
                        
                        self.find('.linkmakerClass:eq('+ctr+')').prop("checked",false).trigger("click");
                        
                    }
                    else{

                         self.find('.linkmakerClass:eq('+ctr+')').prop("checked",true).trigger("click");
                    }
                    for(var ctr2 in saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link_val']){
                        
                        var link_value = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link_val'][ctr2]["link_value"];
                        var link_field = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link_val'][ctr2]["field_name"];
                        var link_operator = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link_val'][ctr2]["link_operator"]
                        if(ctr2 != 0){    
                            self_children_group_link_box.find('.linkInputAdder-add-row:eq('+Number(ctr2-1)+')').trigger("click");
                        }
                        var linkInputAdder_fields = self_children_group_link_box.find('tbody.linkInputAdder-table-tbody:eq('+ctr2+')');
                        linkInputAdder_fields.find('[data-properties-type="linkInputAdder-field"]').val(link_field);
                        linkInputAdder_fields.find('[data-properties-type="linkInputAdder-operator"]').val(link_operator);
                        linkInputAdder_fields.find('[data-properties-type="linkInputAdder-value"]').val(link_value);

                    }
                }

                slave_container_ctr++;
                
            }); 
            
        }

    }
	
	
}
