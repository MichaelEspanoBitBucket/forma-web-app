function SaveEmbedSourceAndFiltering(form_id){
    var property_containment_new_embed_source_form = $('.new-embed-source-form-data-filtering')
    var fl_table_embed_source_form = $('.fl-tbl-embed-source-form');
    if($.type(form_id) != "undefined"){
        var fl_select_embed_source_form = form_id;
    }else{
        var fl_select_embed_source_form = property_containment_new_embed_source_form.find('.fl-embed-select-source-form').eq(0)
    }
    
    var fl_table_tbody_as_parenthesis = $('.fl-tbl-embed-source-form').children('tbody');
    var json_collector_embed_source = [];

    fl_table_tbody_as_parenthesis.each(function(){
        var self = $(this);
        var temporary_collector = {};
        temporary_collector['group_logical_operator'] = self.find('.fl-embed-filter-group-and-or:visible').val()||"";
        temporary_collector['filter_conditions'] = [];
        self.find('tr:not(:eq(0))').each(function(){
            var dis_self = $(this);
            var var_b_filter_condition_source_form_field = dis_self.find('.fl-embed-filter-condition-sform-field:visible').val();
            // if(var_b_filter_condition_source_form_field != ""){
                temporary_collector['filter_conditions'].push({
                    "a_filter_condition_logical_operator":dis_self.find('.fl-embed-filter-condition-and-or:visible').val()||"",
                    "b_filter_condition_source_form_field":var_b_filter_condition_source_form_field,
                    "c_filter_condition_conditional_operator":dis_self.find('.fl-embed-filter-condition-operator:visible').val(),
                    "d_filter_condition_value_switcher":dis_self.find('.fl-embed-filter-condition-value-switcher:visible').val(),
                    "e_filter_condition_static_value":dis_self.find('.fl-embed-filter-condition-static-value:visible').val()||"",
                    "f_filter_condition_dynamic_value":dis_self.find('.fl-embed-filter-condition-dynamic-value:visible').val()||""
                });
            // }
        });

        json_collector_embed_source.push(temporary_collector)
    });
    
    // json_collector_embed_source = json_collector_embed_source.filter(function(a,b){
    //     return a['filter_conditions'].length >= 1;
    // });

    if(fl_select_embed_source_form.val() == "" || $.type(fl_select_embed_source_form.val()) == "undefined"){
        return {};
    }else{
        return {
            "selected_form":{
                "form_name":fl_select_embed_source_form.map(function(a,b){
                    var self = $(this);
                    if(self.is('option')){
                        return self;
                    }else{
                        return self.children('option:selected');
                    }
                }).text(),
                "form_id":fl_select_embed_source_form.val()
            },
            "advance_filter_data":json_collector_embed_source
        };
    }


    
}

function embedcolumnsorter (columnheader,selected_form){
    return true;
    var obj_popup = $("#popup_container").attr("data-object-id");
    var obj_embedded_obj = $(".formbuilder_ws").find("#setObject_"+obj_popup).find('.getFields_'+obj_popup);
    var obj_embedded_attr = obj_embedded_obj.attr("data-embed-sortedcolumn");
    var pangselect_table = $("#popup_container").find('.embedded-view-columns');
     
    try{
        obj_embedded_attr_parsed=JSON.parse(obj_embedded_attr);
    }
    catch(error){
        //comment
        obj_embedded_attr_parsed=null;
    }
    
    if(obj_embedded_attr_parsed!=null){
    
        if(obj_embedded_attr_parsed["formsaverZ"] == selected_form){
    
            var formsaverZ_attr = obj_embedded_attr_parsed["sortedcols"];
           
            for(var ctr in formsaverZ_attr){
                
                  
                    
                    
                        var attr_fieldname = formsaverZ_attr[ctr]['FieldName'];
                        var attr_fieldlabel = formsaverZ_attr[ctr]['FieldLabel'];
                       $(pangselect_table).find('tr:eq('+(parseInt(ctr)+1)+') span div.fl-table-ellip').attr('data-original-title',attr_fieldname);
                       $(pangselect_table).find('tr:eq('+(parseInt(ctr)+1)+') span div.fl-table-ellip').text(attr_fieldname);
                       $(pangselect_table).find('tr:eq('+(parseInt(ctr)+1)+') td input.form-text').val(attr_fieldlabel);
    
                 
            }
             
    
        }
    
    }
}

function embedColumnSorterV2(columnheader,selected_form) {
    var obj_popup = $("#popup_container").attr("data-object-id");
    var obj_embedded_obj = $(".formbuilder_ws").find("#setObject_"+obj_popup).find('.getFields_'+obj_popup);
    var obj_embedded_attr = obj_embedded_obj.attr("data-embed-sortedcolumn");
    var pangselect_table = $("#popup_container").find('.embedded-view-columns').eq(0);
    var table_tbody = pangselect_table.children('tbody');
    var table_trs = table_tbody.children('tr:not(:eq(0))');
    var json_parsed_sorted_cols = null;
    var parsed_status = false;
    try{
        json_parsed_sorted_cols = JSON.parse(obj_embedded_attr);
        parsed_status = true;
    }catch(error){
        parsed_status = false;
    }
    if (parsed_status) {
        if (json_parsed_sorted_cols['selected_form'] == selected_form) {
            console.log("json_parsed_sorted_cols",json_parsed_sorted_cols);
            if ( $.type(json_parsed_sorted_cols['sortedcols']) == "array") {
                if (json_parsed_sorted_cols['sortedcols'].length >= 1) {
                    var data_field_name = null;
                    var data_field_label = null;
                    var target_ele = null;
                    var destination_ele = null;
                    //console.log("track1",target_ele)
                    for ( var key_ctr in json_parsed_sorted_cols['sortedcols']) {
                        //console.log("track2",target_ele)
                        data_field_label= json_parsed_sorted_cols['sortedcols'][key_ctr]['FieldLabel'];
                        data_field_name =json_parsed_sorted_cols['sortedcols'][key_ctr]['FieldName'];
                        //console.log("track3",target_ele)
                        target_ele = table_tbody.find('div').filter(function(){
                            return $(this).filter('.fl-table-ellip[data-original-title]').text() == data_field_name;
                        }).parents('tr').eq(0);
                        //console.log("track4",target_ele)
                        
                        destination_ele = $(table_tbody.children('tr:not(:eq(0))')).eq(key_ctr);
                        console.log("target",target_ele,"\n","destination",destination_ele);
                        destination_ele.before(target_ele);
                        //if (!target_ele.is(':data("embed_sorted")')) {
                        //    
                        //    target_ele.data("embed_sorted","ok");
                        //}
                        data_field_name = null;
                        data_field_label = null;
                        target_ele = null;
                        destination_ele = null;
                    }
                }
            }
        }
    }
}
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
function getUserFormsV2(callBack){
    if(typeof callBack === "function"){
        if($('.user-forms').length <= 0){
            console.log("user-forms not found.");
        }
        else{
            var ele_data_forms = $('.user-forms')
            if(ele_data_forms.length >= 1){
                console.log("after neto ung json");
                console.log(ele_data_forms.text());
                callBack(ele_data_forms.text());
            }
        }
        return;
        var json_data = {
            "column_list": [
                // 'workflow_id',
                // 'category_id',
                // 'category_name',
                'form_id',
                // 'wsID',
                'form_name',
                // 'form_table_name',
                'form_json',
                // 'workspace_version',
                // 'company_id',
                // 'form_description',
                // 'form_date_created',
                // 'form_date_updated',
                // 'form_active',
                'active_fields',
                // 'workspace_version',
                // 'form_alias',
                // 'allow_portal',
                // 'form_actiontype',
                // 'created_by_name',
                // 'update_by_name',
                // 'parent_id'
            ]
        }
        // ui.block();
        $.post('/ajax/sharelink', json_data, function(echo){
            var ele_data_forms = echo;
            if(echo.length >= 1){
                // ui.unblock();
                callBack(echo);
            }
        })

    }
}

function embedProperties(source_form_type) {
    var DOI = $("#popup_container").attr("data-object-id");
    this.json_spectrum_settings = {
        color: "#ECC",
        showInput: true,
        className: "full-spectrum",
        showInitial: true,
        showAlpha: true,
        showPalette: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: "hex",
        localStorageKey: "spectrum.demo",
        move: function (color) {
            
        },
        show: function() {

        },
        beforeShow: function() {

        },
        hide: function() {

        },
        change: function() {

        },
        palette: [
            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
        ]
    }

    if($(".enable_embedPopupDataSending").length>=1){
        $(".enable_embedPopupDataSending").on("change",function(){
            if($(this).is(":checked")){
                $(this).parents(".embedPopupDataSending_prop").children(".input_position_below").removeClass("display2");
            }else{
                 $(this).parents(".embedPopupDataSending_prop").children(".input_position_below").addClass("display2");
            }
        });
    }

    //panlabas ng textbox para sa Allow create new link caption

    if($("[data-properties-type='embed_row_click_creation_form']").length>=1){
        $("[data-properties-type='embed_row_click_creation_form']").on("change",function(){
            var object_id_embed = $('#popup_container').attr('data-object-id');
            var embed_container = $('.formbuilder_ws').find('.setObject[data-object-id="'+object_id_embed+'"]');
            var embed_field_container = embed_container.find('.embed-view-container.getFields_'+object_id_embed);
            var add_new_request_template = $('<a class="embed_newRequest fl-buttonEffect btn-basicBtn cursor" style="display:inline-block;padding:5px;margin-bottom:5px; font-size:12px; color: #fff;text-decoration:none;">Add Request</a>');
            if($(this).is(":checked")){
                $(this).parents(".input_position_below").children(".link_caption_container").removeClass("display2");
                if(!embed_field_container.prev().is('.embed_newRequest')){
                   embed_field_container.before(add_new_request_template);
                }else{
                   add_new_request_template = embed_field_container.prev();
                }
                
                add_new_request_template.show();
            }else{
                 $(this).parents(".input_position_below").children(".link_caption_container").addClass("display2");
                 if(!embed_field_container.prev().is('.embed_newRequest')){
                   embed_field_container.before(add_new_request_template);
                }else{
                    add_new_request_template = embed_field_container.prev();
                }
                add_new_request_template.hide();
            }
        });
    }
    if($('[data-properties-type="link_caption"]').length >= 1){
        $('[data-properties-type="link_caption"]').on("change",function(){
            var dis_val = $(this).val();
            var object_id_embed = $('#popup_container').attr('data-object-id');
            var embed_container = $('.formbuilder_ws').find('.setObject[data-object-id="'+object_id_embed+'"]');
            var embed_field_container = embed_container.find('.embed-view-container.getFields_'+object_id_embed);
            var action_button = embed_field_container.prev().filter('.embed_newRequest');
            action_button.text(dis_val);
        });
    }

    //kapag nag click data-properties-type="enable_embed_row_click"

    if($('[data-properties-type="enable_embed_row_click"]').length>=1){
        
        var ctrs = 0;
        $('[data-properties-type="enable_embed_row_click"]').on("change",function(){
            var attr_array = [];
            var parent_anchor = $(this).parents('.input_position_below.embed_row_click_prop');
            var actions_container = parent_anchor.children('.input_position_below').eq(0);
             var properties_checked = actions_container.find('[type="checkbox"]');
             var object_id_embed = actions_container.parents('#popup_container').attr('data-object-id');
             var embed_container = $('.formbuilder_ws').find('.setObject[data-object-id="'+object_id_embed+'"]');
            var embed_field_container = embed_container.find('.embed-view-container.getFields_'+object_id_embed);
            add_new_request_template = embed_field_container.prev().filter('.embed_newRequest');
            if($(this).is(":checked")){
                if($('.formbuilder_ws').find('.setObject[data-object-id="'+object_id_embed+'"][data-event-action]').length>=1){
                    attr_array =  JSON.parse($('.formbuilder_ws').find('.setObject[data-object-id="'+object_id_embed+'"]').attr('data-event-action'));
                }
                
                actions_container.removeClass("display2");
                properties_checked = properties_checked.filter(function(a){
                    console.log('sdsdd',$.inArray($(this).val(),attr_array));
                    return $.inArray($(this).val(),attr_array) >-1;
                });
                console.log('prop chec',properties_checked);
                 properties_checked.each(function(){
                    $(this).attr('checked',true);

                 });

                 embed_container.attr('data-event-action', '[]');
            }else{
                
                if(ctrs >= 1){
                   properties_checked = properties_checked.filter(function(a){
                        return $(this).attr('checked') == "checked";
                    });
                   properties_checked.each(function(){
                        attr_array.push($(this).val());
                        $(this).attr('checked',false);

                    });
                   var attr_array_json = JSON.stringify(attr_array);
                   embed_container.attr('data-event-action',attr_array_json);


                }
                
                 actions_container.addClass("display2");
            }
            console.log(add_new_request_template, $('[data-properties-type="embed_row_click_creation_form"]').is(':checked'))
            if($('[data-properties-type="embed_row_click_creation_form"]').is(':checked')){
                if($(this).is(":checked")){
                    add_new_request_template.show();
                }else{
                    add_new_request_template.hide();
                }
            }else{
                add_new_request_template.hide();
            }
            ctrs++;
        });
        $('[data-properties-type="enable_embed_row_click"]').trigger('change');
    }

    //WHEN CHECK ALLOW HIGHLIGHTS
    if ($('[name="allow_highlights"]').length >= 1) {
        var DOI = $("#popup_container").attr("data-object-id");
        var field_elem = $("#setObject_" + DOI).find(".getFields_" + DOI);//.attr("embed-hl-data")
        //RESTORE CHECKED ALLOW HIGHLIGHTS
        if (field_elem.attr("allow-highlights") == "true") {
            $('[name="allow_highlights"]').prop("checked", true);
        } else {
            $('[name="allow_highlights"]').prop("checked", false);
        }
        //SET ON CHANGE OF ALLOW HIGHLIGHTS
        $('[name="allow_highlights"]').on("change", function(ev) {
            var DOI = $("#popup_container").attr("data-object-id");
            var field_elem = $("#setObject_" + DOI).find(".getFields_" + DOI);//.attr("embed-hl-data")
            field_elem.attr("allow-highlights", $(this).prop("checked"));
        })
    }
    //WHEN RADIO CHECK ALLOW HIGHLIGHT TYPE
    if ($('[name="highlight_type"]').length >= 1) {
        var DOI = $("#popup_container").attr("data-object-id");
        var field_elem = $("#setObject_" + DOI).find(".getFields_" + DOI);
        //RESTORE CHECKED ALLOW HIGHLIGHTS
        if (field_elem.attr("highlight-type") == "row") {
            $('[name="highlight_type"][value="row"]').prop("checked", true);
        } else {
            $('[name="highlight_type"][value="cell"]').prop("checked", true);
        }
        //SET ON CHANGE OF ALLOW HIGHLIGHTS
        $('[name="highlight_type"]').on("change", function(ev) {
            var DOI = $("#popup_container").attr("data-object-id");
            var field_elem = $("#setObject_" + DOI).find(".getFields_" + DOI);
            field_elem.attr("highlight-type", $(this).val());
        })
    }

    //WHEN EXISTING HIGHLIGHTED CONTAINER
    if ($(".highlight-row-prop").length >= 1) {
        {//VERSION 1 HIGHLIGHT PROPERTY
            var containtment = $(".highlight-row-prop");
            containtment.find(".chooseColor").spectrum(json_spectrum_settings);

            //THIS IS FOR DYNAMIC HIGHLIGHTING
            containtment.find(".design-embed-highlights-prop").find("tr.HL_RULE_PROP1, tr.HL_RULE_PROP2").each(function(eqi) {
                var dis_rowprop = $(this);
                var row_action = $(
                        '<div style="float:right;margin-top:5px;">' + //position: absolute;right: 5px; bottom: -16px;z-index:100;
                        '<i class="addHLRule fa fa-plus icon-plus-sign cursor"></i>' +
                        '<i class="subtractHLRule fa fa-minus icon-minus-sign cursor" style="display:none; margin-left:5px;"></i>' +
                        '</div>'
                        );
                if (dis_rowprop.hasClass("HL_RULE_PROP2")) {
                    var select_relative_wrap = dis_rowprop.find(".relative-wrap");
                    select_relative_wrap.last().append(row_action);
                    var select_condition_type = select_relative_wrap.find(".choose-condition-type");
                    bindAddHLRULE(row_action);
                    bindSubtractHLRULE(row_action);
                    bindConditionTypeHLRULE(select_condition_type);
                } else if (dis_rowprop.hasClass("HL_RULE_PROP1")) {
                    //nothing
                }
            })
            restoreSetValHLRULE();
        }
        // {//VERSION 2 HIGHLIGHT PROPERTY
        //     var containtment = $(".highlight-row-prop");
        //     var tbl_design_embed_highlights_prop = containtment.find('.design-embed-highlights-prop');
        //     containtment.find(".chooseColor").spectrum(json_spectrum_settings);



        //     var tee_arrs = containtment.find(".design-embed-highlights-prop").find("tr");

        //     tee_arrs.each(function(eqi){
        //         var row_action = $(
        //             '<div style="position: absolute;left: 0px;bottom: 0px;z-index:100;">'+
        //                 '<i class="addHLRule icon-plus-sign cursor"></i>'+
        //                 '<i class="subtractHLRule icon-minus-sign cursor" style="display:none;"></i>'+
        //             '</div>'
        //         );
        //         $(this).children("td").first().children(".relative-wrap").append(row_action);
        //         bindAddHLRULE(row_action);
        //     })

        // }
        // {//VERSION 2 HIGHLIGHT PROPERTY
        //     function restoreSetValHLRULE(){
        //         var containtment = $(".highlight-row-prop");
        //         //RESTORE SET VALUES
        //         var hlc_tbl = containtment.find(".design-embed-highlights-prop");
        //         var DOI = $("#popup_container").attr("data-object-id");
        //         var field_elem = $("#setObject_"+DOI).find(".getFields_"+DOI);//.attr("embed-hl-data")
        //         if(typeof field_elem.attr("embed-hl-data") != "undefined"){
        //             try{
        //                 var json_e_hl = JSON.parse(field_elem.attr("embed-hl-data"));
        //                 $.each(json_e_hl,function(eqi,value){
        //                     if(eqi != 0){
        //                         $(".HL_RULE_PROP2").eq(eqi-1).find('.addHLRule').click();
        //                     }
        //                     if(hlc_tbl.length >= 1 ){
        //                         // value['hl_color']
        //                         // value['hl_fn']
        //                         // value['condition_type']
        //                         // value['hl_value']
        //                         // value['hl_value2']
        //                         $(".HL_RULE_PROP1").eq(eqi).find('.chooseColor.hl-color').spectrum("set", value['hl_color']);
        //                         $(".HL_RULE_PROP1").eq(eqi).find('.fieldname-basis-highlight.hl-fn').val(value['hl_fn']);
        //                         $(".HL_RULE_PROP2").eq(eqi).find('.choose-condition-type').val(value['condition_type']);
        //                         $(".HL_RULE_PROP2").eq(eqi).find('.hl-value').val(value['hl_value']);
        //                         $(".HL_RULE_PROP2").eq(eqi).find('.hl-value2').val(value['hl_value2']);
        //                         // var hlc_tr = hlc_tbl.find("tr").eq(eqi)
        //                         // hlc_tr.find(".chooseColor").spectrum("set",value["hl_color"]);
        //                         // //.css("background-color", value["hl_color"]);
        //                         // // hlc_tr.find(".hl-fn").val(value["hl_fn"]); // or this below
        //                         // //         hlc_tr.find(".hl-fn option[value='"+value["hl_fn"]+"']").prop("selected",true);
        //                         // hlc_tr.find(".hl-value").val(value["hl_value"])
        //                     }
        //                 })
        //             }catch(err){
        
        //             }
        //         }
        //     }
        //     function bindAddHLRULE(dis_ele){
        //         var element_add = $(dis_ele);
        //         element_add.find(".addHLRule").on("click",{"element_add":element_add},function(e){
        //             alert(123)
        //             var passed_data = e.data;
        //             var dis_tee_arr_prop = $(this).parents("tr").eq(0);
        //             var selected_clone = dis_tee_arr_prop.clone(true);
        //             var tbody_row_container = $(this).parents("tbody").eq(0);
        //             tbody_row_container.append(selected_clone);
        //             selected_clone.find(".sp-replacer.sp-light.full-spectrum").remove();
        //             var location_for_color_picking = $(selected_clone.find(".chooseColor.hl-color").parent());
        //             selected_clone.find(".chooseColor.hl-color").remove();
        //             var color_picking = $('<input type="text" class="chooseColor hl-color" data-properties-type="embRowColor" data-type="emb-highlight-color">');
        //             location_for_color_picking.append(color_picking);
        //             color_picking.spectrum(json_spectrum_settings);
        //             selected_to_clone.find(".subtractHLRule").css("display","inline-block");
        
        
        
        //             // var dis_prop2 = $(this).parents('tr').eq(0);
        //             // var dis_prop1 = dis_prop2.prev();
        //             // var selected_to_clone = dis_prop1.add(dis_prop2).clone(true);
        //             // var tbody_row_container = $(this).parents("tbody").eq(0);
        
        
        //             // tbody_row_container.append(selected_to_clone);
        //             // selected_to_clone.find(".sp-replacer.sp-light.full-spectrum").remove();
        //             // var location_for_color_picking = $(selected_to_clone.find(".chooseColor.hl-color").parent());
        //             // selected_to_clone.find(".chooseColor.hl-color").remove();
        //             // var color_picking = $('<input type="text" class="chooseColor hl-color" data-properties-type="embRowColor" data-type="emb-highlight-color">');
        //             // location_for_color_picking.append(color_picking);
        //             // color_picking.spectrum(json_spectrum_settings);
        //             // selected_to_clone.find(".subtractHLRule").css("display","inline-block");
        //         })
        //     }
        //     function bindSubtractHLRULE(dis_ele){
        //         var element_subtract = $(dis_ele);
        //         element_subtract.find(".subtractHLRule").on("click",function(){
        //             var dis_prop2 = $($(this).parents('tr.HL_RULE_PROP2').eq(0));
        //             var dis_prop1 = $(dis_prop2.prev());
        //             dis_prop1.remove();
        //             dis_prop2.remove();
        //         })
        //     }
        //     function bindConditionTypeHLRULE(dis_ele){
        //         var element_CT = $(dis_ele);
        //         element_CT.on("change",function(){
        //             var value_nito = $(this).val();
        //             var location_value_container = $(this).parent();
        //             if(value_nito == "range" || value_nito == "in_between"){
        //                 location_value_container.find(".hl-value2").css("display","inline-block");
        //                 location_value_container.find(".hl-value").css("width","49%");
        //             }else{
        //                 location_value_container.find(".hl-value2").css("display","none");
        //                 location_value_container.find(".hl-value").css("width","");
        //             }
        //         });
        //     }
        // }
    }

    //WHEN EMBED PROPERTY IS EXISTING
    if ($(".embedProperty").length >= 2) {
        var prop_choice_embed_ele = $('[name="embed_rfc_choice"]');
        if (prop_choice_embed_ele.length >= 1) {
            prop_choice_embed_ele.on("change", function(e) {
                var DOI = $("#popup_container").attr("data-object-id");
                var value_embed = $(this).val();
                if (value_embed == "static") {
                    $(".embed-result-field-computed-c").css("display", "none");
                    $(".embed-result-field-c").css("display", "");
                } else if (value_embed == "computed") {
                    $(".embed-result-field-c").css("display", "none");
                    $(".embed-result-field-computed-c").css("display", "");
                }
                $("#setObject_" + DOI).find(".getFields_" + DOI).attr('rfc-choice', value_embed);
            });
        }
        var DOI = $("#popup_container").attr("data-object-id");

        if ($("#setObject_" + DOI).find(".getFields_" + DOI).attr('rfc-choice')) {
            // alert($("#setObject_" + DOI).find(".getFields_"+ DOI).attr('sc-choice') );
            var embed_choice_value = $("#setObject_" + DOI).find(".getFields_" + DOI).attr('rfc-choice');
            if (embed_choice_value == "computed") {
                // $("[value='computed']",prop_choice_embed_ele).prop("checked",true);
                $('[value="computed"]' + prop_choice_embed_ele.selector).click();
                $(".embed-result-field-c").css("display", "none");
            } else {
                // $("[value='static']",prop_choice_embed_ele).prop("checked",true);
                $('[value="static"]' + prop_choice_embed_ele.selector).click();
                $(".embed-result-field-computed-c").css("display", "none");
            }
        }
        if ($(".embed-result-field-formula").length >= 1) {
            $(".embed-result-field-formula").on("keyup", function() {
                var DOI = $("#popup_container").attr("data-object-id");
                $("#setObject_" + DOI).find(".getFields_" + DOI).attr('embed-computed-formula', $(this).val());
            });
            if ($("#setObject_" + DOI).find(".getFields_" + DOI).attr('embed-computed-formula')) {
                $(".embed-result-field-formula").val(
                        $("#setObject_" + DOI).find(".getFields_" + DOI).attr('embed-computed-formula')
                );
            }
        }

        var embed_source_form_ele_container = $(".embedProperty").find(".embed_source_form");
        var loading_content = $('<div class="getFromsLoader form-select" style="display:inline-block;position:relative;vertical-align:middle;text-align:center;"><label style="color:black;position:reltive;font-size: 15px;display:inline-block;">Loading...</label><img src="/images/loader/loader.gif" class="display processorLoad" style="display: inline-block;"/></div>');

        //EMBED GET FORM LIST
        embed_source_form_ele_container.children(".embed-source-form").hide();
        embed_source_form_ele_container.prepend(loading_content);
        var embed_source_lookup_field_ele_container = $(".embedProperty").find(".embed_source_lookup_field");
        if (embed_source_lookup_field_ele_container.find(".getFromsLoader").length == 0) {
            var loading_content_active_fields = $('<div class="getFromsLoader form-select" style="display:inline-block;position:relative;vertical-align:middle;text-align:center;"><label style="color:black;position:reltive;font-size: 15px;display:inline-block;">Loading...</label><img src="/images/loader/loader.gif" class="display processorLoad" style="display: inline-block;"/></div>');
            embed_source_lookup_field_ele_container.find(".embed-source-lookup-field").hide();
            embed_source_lookup_field_ele_container.after(loading_content_active_fields);
        }
        // $.post("/ajax/formbuilder", {action: "getFormList"}, 
        getUserFormsV2(function(dataEchoResults) {
            var json_p = JSON.parse(dataEchoResults);
            var json_p_form_json = null;
            var json_p_form_fields = null;
            
            var embed_source_form_ele_container = $(".embedProperty").find(".embed_source_form");
            //GET LIST OF FORMS
            var collect_append_option = '<option>--Select--</option>';
			json_p = json_p.sort(function(a,b){
				return a['form_name'].toLowerCase().localeCompare(b['form_name'].toLowerCase());
			});
            for (var c in json_p) {
                try {
                    console.log("TERACOPY",json_p[c])
                    if ($.isArray(json_p)) {
                        json_p_form_json = JSON.parse(json_p[c]['form_json']);
                        json_p_form_fields = json_p_form_json['form_fields'];
                    }
                } catch(e) {
                    json_p_form_json = [];
                    json_p_form_fields = [];
                }
                collect_append_option
                        += '<option frm-fields-type="'+htmlEntities(json_p_form_fields)+'" frm-id="' + json_p[c]["form_id"] + '" active-fields="' + json_p[c]["active_fields"] + '" value="' + json_p[c]["form_id"] + '" >' + json_p[c]["form_name"] + '</option>';
            }

            loading_content.fadeOut(function() {
                var embed_source_form_ele_container = $(".embedProperty").find(".embed_source_form");

                embed_source_form_ele_container.children(".embed-source-form").html(collect_append_option);
                embed_source_form_ele_container.children(".embed-source-form").show();
                loading_content_active_fields.fadeOut(function() {
                    embed_source_lookup_field_ele_container.find(".embed-source-lookup-field").show();
                    $(this).remove();
                });

                //for multiple embed source form
                // if(){

                // }
                $('.m-embed-source-form').html(collect_append_option);
                var DOI = $("#popup_container").attr("data-object-id");
                embedMultipleFormsSetup(DOI);
                //RESTORE SAVED VALUES
                var field_id_on_form = $("#popup_container").attr("data-object-id");
                if (typeof $(".getFields_" + field_id_on_form).attr("embed-source-form-val-id") != "undefined") {
                    embed_source_form_ele_container.children(".embed-source-form").children("option[frm-id='" + $(".getFields_" + field_id_on_form).attr("embed-source-form-val-id") + "']").prop("selected", true);
                }
                //GET FORM ACTIVE FIELD LIST BASED ON SELECTED SOURCE LIST OF FORMS
                var collect_append_option_active_field = "<option>--Select--</option>";
                var columnHeaderTable = '<table class="table_data dataTable embedded-view-columns"><tr><th>Field Label</th><th>Field Name</th></tr>';
                if (embed_source_form_ele_container.children(".embed-source-form").children("option:selected").length >= 1) {
                    var active_fields = embed_source_form_ele_container.children(".embed-source-form").children("option:selected").attr("active-fields");
                    if (typeof active_fields != "undefined") {
                        if (active_fields.length >= 1) {
                            active_fields = active_fields.split(",");
                            var columnSelection = active_fields;
                            if ($.type(active_fields) == "array") {
                                active_fields.push("Status");
                                active_fields.push("TrackNo");
                                active_fields.push("DateCreated");
                                active_fields = active_fields.sort(function(a,b){
                                        return a.toLowerCase().localeCompare(b.toLowerCase());
                                });
                            }
                            for (var ctr = 0; ctr < active_fields.length; ctr++) {
                                columnHeaderTable += '<tr>';
                                columnHeaderTable += '<td><input type="text" class="form-text"></td><td><span style="padding:3px"><div class="fl-table-ellip" data-original-title="'+active_fields[ctr]+'">' + active_fields[ctr] + '</div></span></td>';
                                columnHeaderTable += '</tr>';
                                collect_append_option_active_field += '<option value="' + active_fields[ctr] + '">' + active_fields[ctr] + '</option>';
                                
                            }

                            var form_fields_type = embed_source_form_ele_container.children(".embed-source-form").children("option:selected").attr("frm-fields-type");
                            console.log("form_fields_type",form_fields_type)
                            
                            var form_fields_type_json_p = null; 
                            try {
                                form_fields_type_json_p = JSON.parse(form_fields_type);
                            } catch(e) {
                                form_fields_type_json_p = [];
                            }
                            if($('.escr-data-embed-column-field').length >= 1){
                                var esr_append_ele = null;
                                esr_append_ele = $(collect_append_option_active_field).clone();
                                
                                $('.escr-data-embed-column-field').each(function(){
                                    $(this).html(esr_append_ele);
                                    console.log("ROOTZ",esr_append_ele.parent());
                                    console.log("form_fields_type_json_p",form_fields_type_json_p)
                                    $.each(form_fields_type_json_p,function(ctr_index,val){
                                        console.log('val["fieldType"]', val["fieldType"],val["fieldName"])
                                        if (typeof val["fieldType"] !== "undefined") {
                                            if (val["fieldType"] === "double") {
                                                esr_append_ele.filter("[value='"+val["fieldName"].replace("[]","")+"']").attr("data-has-value-type", val["fieldType"]);
                                            }
                                        }
                                    })
                                    esr_append_ele.filter(':not([data-has-value-type="double"]):not([value="--Select--"])').remove();
                                });
                            }
                        }

                    }
                }
                columnHeaderTable += '</table>';
                
           
                
                console.log("data onchange", collect_append_option_active_field, embed_source_lookup_field_ele_container)
                
                var appended_options_active_field = $(collect_append_option_active_field);
                embed_source_lookup_field_ele_container.find(".embed-source-lookup-field").html(appended_options_active_field);
                $('.edsc-field-name').html(appended_options_active_field);
                if(appended_options_active_field.length >= 2){
                    appended_options_active_field.filter('option[value="--Select--"]').remove();
                }
                appended_options_active_field.filter('option[value="DateCreated"]').prop('selected',true);
                //RESTORING VALUE OF SELECTED ACTIVE FIELD OF SOURCE FORM
                if($(".getFields_" + field_id_on_form).filter("[data-default-sort-type],[data-default-sort-col]").length >= 1 ){
                    $('.edsc-field-name').val(  $(".getFields_" + field_id_on_form).attr('data-default-sort-col') );
                    $('.edsc-sort-type').val(  $(".getFields_" + field_id_on_form).attr('data-default-sort-type') );
                }

                var getFieldSourceFormID_val = $(".getFields_" + field_id_on_form).attr("embed-source-form-val-id");
                var getFieldSourceFormLookupField_val = $(".getFields_" + field_id_on_form).attr("embed-source-lookup-active-field-val");
                if (typeof getFieldSourceFormLookupField_val != "undefined") {
                    if (
                            getFieldSourceFormID_val == embed_source_form_ele_container.children(".embed-source-form").children("option:selected").val() &&
                            appended_options_active_field.parent().children("option[value='" + getFieldSourceFormLookupField_val + "']").length >= 1
                            ) {
                        appended_options_active_field.parent().children("option[value='" + getFieldSourceFormLookupField_val + "']").prop("selected", true);

                    }
                }
               
                //SET ONCHANGE EVENT EMBED SOURCE FORMS
                embed_source_form_ele_container.children(".embed-source-form").on({
                    "change": function() {
                        var embed_source_form_ele_container = $(".embedProperty").find(".embed_source_form");
                        var field_id_on_form = $("#popup_container").attr("data-object-id");
                        var selected_form = $(this).val();
                        $(".getFields_" + field_id_on_form).attr("embed-source-form-val-id", $(this).val());

                        //GET ACTIVE FIELDS OF SOURCE FORM
                        var collect_append_option_active_field = "<option value='--Select--'>--Select--</option>";
                        var columnHeaderTable = '<table class="table_data dataTable embedded-view-columns"><tr><th>Field Label</th><th>Field Name</th></tr>';

                        //if ($(this).val() != "--Select--") {
                        //    collect_append_option_active_field += "<option value='TrackNo' >TrackNo</option>";
                        //}
                        if (embed_source_form_ele_container.children(".embed-source-form").children("option:selected").length >= 1) {
                            var active_fields = embed_source_form_ele_container.children(".embed-source-form").children("option:selected").attr("active-fields");
                            if (typeof active_fields != "undefined") {
                                if (active_fields.length >= 1) {
                                    active_fields = active_fields.split(",");
                                    if ($.type(active_fields) == "array") {
                                        active_fields.push("Status");
                                        active_fields.push("TrackNo");
                                        active_fields.push("DateCreated");
                                        active_fields = active_fields.sort(function(a,b){
                                            return a.toLowerCase().localeCompare(b.toLowerCase());
                                        });
                                    }
                                    console.log(active_fields);
                                    // find here
                                    // for (var ctr = 0; ctr < active_fields.length; ctr++) {
                                    //     console.log(active_fields);
                                    //     console.log("Samuel");
                                    //     columnHeaderTable += '<tr>';
                                    //     columnHeaderTable += '<td><input type="text" class="form-text"></td><td><span style="padding:3px;cursor:move;"><div style="width:160px;" class="fl-table-ellip" data-original-title="'+active_fields[ctr]+'">' + active_fields[ctr] + '</div></span></td>';
                                    //     columnHeaderTable += '</tr>';
                                    //     collect_append_option_active_field += '<option value="' + active_fields[ctr] + '">' + active_fields[ctr] + '</option>';
                                    // }

                                    for (var ctr = 0; ctr < active_fields.length; ctr++) {
                                        console.log(active_fields);
                                        columnHeaderTable += '<tr>';
                                        columnHeaderTable += '<td><input type="text" class="form-text"></td><td><span style="padding:3px;cursor:move;"><div style="width:160px;" class="fl-table-ellip" data-original-title="'+active_fields[ctr]+'">' + active_fields[ctr] + '</div></span></td>';
                                        columnHeaderTable += '</tr>';
                                        collect_append_option_active_field += '<option value="' + active_fields[ctr] + '">' + active_fields[ctr] + '</option>';
                                    }
                                    var form_fields_type = embed_source_form_ele_container.children(".embed-source-form").children("option:selected").attr("frm-fields-type");
                                    console.log("form_fields_type",form_fields_type);
                                    
                                    var form_fields_type_json_p = null; 
                                    try {
                                        form_fields_type_json_p = JSON.parse(form_fields_type);
                                    } catch(e) {
                                        form_fields_type_json_p = [];
                                    }
                                    if($('.escr-data-embed-column-field').length >= 1){
                                        var esr_append_ele = null;
                                        esr_append_ele = $(collect_append_option_active_field).clone();
                                        
                                        $('.escr-data-embed-column-field').each(function(){
                                            $(this).html(esr_append_ele);
                                            console.log("ROOT2",esr_append_ele.parent());
                                            console.log("form_fields_type_json_p",form_fields_type_json_p)
                                            $.each(form_fields_type_json_p,function(ctr_index,val){
                                                console.log('val["fieldType"]', val["fieldType"],val["fieldName"])
                                                if (typeof val["fieldType"] !== "undefined") {
                                                    if (val["fieldType"] === "double") {
                                                        esr_append_ele.filter("[value='"+val["fieldName"].replace("[]","")+"']").attr("data-has-value-type", val["fieldType"]);
                                                    }
                                                }
                                            })
                                            esr_append_ele.filter(':not([data-has-value-type="double"]):not([value="--Select--"])').remove();
                                        });
                                    }
                                }
                            }
                        }
                        var appended_options_active_field = $(collect_append_option_active_field);
                        var appended_clone = appended_options_active_field.clone();
                        embed_source_lookup_field_ele_container.find(".embed-source-lookup-field").html(appended_options_active_field);
                        appended_options_active_field.filter('[value="DateCreated"]').remove();
                        $('.edsc-field-name').html(appended_clone);
                        if(appended_clone.length >= 2){
                            appended_clone.filter('option[value="--Select--"]').remove();
                        }
                        
                        appended_clone.filter('option[value="DateCreated"]').prop('selected',true);
                        console.log("VANANA",$(".getFields_" + field_id_on_form))
                        if($(".getFields_" + field_id_on_form).filter("[data-default-sort-type],[data-default-sort-col]").length >= 1 ){
                            $('.edsc-field-name').val(  $(".getFields_" + field_id_on_form).attr('data-default-sort-col') );
                            $('.edsc-sort-type').val(  $(".getFields_" + field_id_on_form).attr('data-default-sort-type') );
                        }

                        if ($('[data-name="epds-select-popup-form-field-side"]').length >= 1) {
                            $('[data-name="epds-select-popup-form-field-side"]').each(function() {
                                var cloned_options = appended_options_active_field.clone();
                                $(this).html(cloned_options);
                                cloned_options.filter('[value="TrackNo"]').remove()
                            });
                        }
                        if ($('.embedRoutingField_container').length >= 1) {
                            var embedRoutingField_container = $('.embedRoutingField_container');
                            embedRoutingField_container.find('.erf-route-popup-form-fields').html(appended_options_active_field.clone());
                            if($('.sourFormType').find('[name="sourceformtype"]:checked').val() == "single"){
                                if ($(".getFields_" + field_id_on_form).attr("erf-route-popup-form-fields")) {
                                    embedRoutingField_container.find('.erf-route-popup-form-fields').val($(".getFields_" + field_id_on_form).attr("erf-route-popup-form-fields"))
                                }
                            }
                        }
                        //RESTORING VALUE OF SELECTED ACTIVE FIELD OF SOURCE FORM
                        var getFieldSourceFormID_val = $(".getFields_" + field_id_on_form).attr("embed-source-form-val-id");
                        var getFieldSourceFormLookupField_val = $(".getFields_" + field_id_on_form).attr("embed-source-lookup-active-field-val");
                        if (typeof getFieldSourceFormLookupField_val != "undefined") {
                            if (
                                    getFieldSourceFormID_val == $(this).val() &&
                                    appended_options_active_field.parent().children("option[value='" + getFieldSourceFormLookupField_val + "']").length >= 1
                                    ) {
                                // alert("HALA")
                                // console.log(appended_options_active_field)
                                // console.log("kulangot")
                                // console.log(getFieldSourceFormLookupField_val)
                                // console.log($(appended_options_active_field,"[value='"+getFieldSourceFormLookupField_val+"']"))
                                appended_options_active_field.parent().children("option[value='" + getFieldSourceFormLookupField_val + "']").prop("selected", true);


                            }
                        }
                        // if($(".highlight-row-prop").length >= 1){
                        //     var highlight_containtment = $(".highlight-row-prop");
                        //     highlight_containtment.find(".fieldname-basis-highlight").html(collect_append_option_active_field)
                        // }
                        //RESTORE SELECTED DISPLAY COLUMN WHEN THE FORM IS CHANGED
                        if ($('.embed-display-column').length >= 1) {
                            var display_col_containtment = $(".embed-display-column");
                           // display_col_containtment.html(collect_append_option_active_field);
                            var columnHeaderTable_ele = $(columnHeaderTable)

                           
                            display_col_containtment.html(columnHeaderTable_ele);
                            console.log("CHECKING TEST", columnHeaderTable_ele)

                            columnHeaderTable_ele.find("[data-original-title]").tooltip();
                            

                            //display_col_containtment.children('option:contains("--Select--")').remove();
                            //RESTORE VALUE OF CHOSEN COLUMN
                            if ($(".getFields_" + field_id_on_form + "[embed-column-data]").length >= 1) {
                                var embed_col_choose_data = $(".getFields_" + field_id_on_form + "[embed-column-data]").attr('embed-column-data');
                                var embed_col_choose_data_json = JSON.parse(embed_col_choose_data);
                                for (var ii in embed_col_choose_data_json) {
                                    //display_col_containtment.children('option:contains(' + embed_col_choose_data_json[ii] + ')').prop("selected", true);
                                    //console.log("wrhaaaa", display_col_containtment.find("span:contains('" + embed_col_choose_data_json[ii]["FieldName"] + "')").filter(function(){return $(this).text() === embed_col_choose_data_json[ii]["FieldName"];}))
                                    display_col_containtment.find("span:contains('" + embed_col_choose_data_json[ii]["FieldName"] + "')").filter(function(){return $(this).text() === embed_col_choose_data_json[ii]["FieldName"];}).parents('tr').eq(0).find('input').val(embed_col_choose_data_json[ii]["FieldLabel"]);
                                };
                            };
                            columnHeaderTable_ele.find("tbody").sortable({
                                items: "tr:not(:eq(0))",
                                placeholder: {
                                    element: function(currentItem) {
                                        return $("<tr><td style='width:50%;height:40px;'></td><td style='width:50%;height:40px;'></td></tr>")[0];
                                    },
                                    update: function(container, p) {
                                        return;
                                    }
                                },
                                sort:function(ev,ui){
                                    $(ui["helper"]).children("td").css("width","181px");
                                },
                                axis:"y"
                            });
                            //searchme column header
                            embedColumnSorterV2(columnHeaderTable_ele,selected_form);
                        }
                        //RESTORE HIGHLIGHT FIELDNAME VALUE
                        if ($(".highlight-row-prop").length >= 1) { //onchange ng form pili
                            var highlight_containtment = $(".highlight-row-prop");
                            var hlc_tbl = highlight_containtment.find(".design-embed-highlights-prop");
                            highlight_containtment.find(".fieldname-basis-highlight").html(collect_append_option_active_field)
                            if (hlc_tbl.length >= 1) {
                                try {
                                    var json_e_hl = JSON.parse(field_elem.attr("embed-hl-data"));

                                    $.each(json_e_hl, function(eqi, value) {
                                        // value['hl_fn'];
                                        if ($('.HL_RULE_PROP1').eq(eqi).length >= 1) {
                                            if ($('.HL_RULE_PROP1').eq(eqi).find('.hl-fn').find('option[value="' + value['hl_fn'] + '"]').length >= 1) {
                                                $('.HL_RULE_PROP1').eq(eqi).find('.hl-fn').val(value['hl_fn']);
                                            }
                                        }
                                    })
                                } catch (err) {

                                }
                            }

                        }

                        $('.ps-container').perfectScrollbar('update');
                        //searchme embed sorter function

                    }
                });//embedRoutingField_container.find('.erf-route-my-form-fields')
                if($('.sourFormType').find('[name="sourceformtype"]:checked').val() == "single"){
                    embed_source_form_ele_container.children(".embed-source-form").change();   
                }
                //RESTORE HIGHLIGHT FIELDNAME VALUE
                if ($(".highlight-row-prop").length >= 1) { //onchange ng form pili
                    var highlight_containtment = $(".highlight-row-prop");
                    var hlc_tbl = highlight_containtment.find(".design-embed-highlights-prop");
                    highlight_containtment.find(".fieldname-basis-highlight").html(collect_append_option_active_field)
                    if (hlc_tbl.length >= 1) {
                        try {
                            var json_e_hl = JSON.parse(field_elem.attr("embed-hl-data"));

                            $.each(json_e_hl, function(eqi, value) {
                                // value['hl_fn'];
                                if ($('.HL_RULE_PROP1').eq(eqi).length >= 1) {
                                    $('.HL_RULE_PROP1').eq(eqi).find('.hl-fn').val(value['hl_fn']);
                                }
                            })
                        } catch (err) {

                        }
                    }

                }
                if ($(".embedPopupDataSending_container").length >= 1) {
                    {//RESTORING SELECTED DATA SEND

                        var DOI = $("#popup_container").attr("data-object-id");
                        var get_field_ele = $(".getFields_" + DOI);
                        var data_json_sending = get_field_ele.attr("stringified-json-data-send");
                        var json_parsed_val = "";
                        var parse_validate = false;
                        var embedPopupDataSending_container = $('.embedPopupDataSending_container');
                        var epds_data_send_collection = embedPopupDataSending_container.find('.epds-data-send-collection');
                        var tr_groups = "";
                        var tr_group_refresh_selection = "";
                        try {
                            json_parsed_val = JSON.parse(data_json_sending);
                            parse_validate = true;
                        } catch (error) {
                            parse_validate = false;
                            json_parsed_val = "";

                            console.error("RESTORING DATA SEND JSON PARSE ERROR",data_json_sending);
                        }

                        var counter_embed_data_send = 0;
                        if (parse_validate) {
                            for (var index_jpv in json_parsed_val) {
                                //dito rows
                                tr_group_refresh_selection = epds_data_send_collection.find('.epds-tr-group');
                                tr_group = tr_group_refresh_selection.eq(counter_embed_data_send);
                                for (var inner_index_jpv in json_parsed_val[index_jpv]) {
                                    //dito fields
                                    if (inner_index_jpv == "enable_embedPopupDataSending") {
                                        $('.enable_embedPopupDataSending').prop("checked", json_parsed_val[index_jpv][inner_index_jpv]);
                                        if(json_parsed_val[index_jpv][inner_index_jpv]==true){
                                             $(".enable_embedPopupDataSending").parents(".embedPopupDataSending_prop").children(".input_position_below.display2").removeClass("display2")
                                        }
                                    } else if (inner_index_jpv == "epds_my_form_side_chk") {
                                        tr_group.find('[data-name="epds-my-form-side-chk"][value="' + json_parsed_val[index_jpv][inner_index_jpv] + '"]').prop("checked", true);
                                    } else if (inner_index_jpv == "epds_popup_form_side_chk") {
                                        tr_group.find('[data-name="epds-my-form-side-chk"][value="' + json_parsed_val[index_jpv][inner_index_jpv] + '"]').prop("checked", true);
                                    } else if (inner_index_jpv == "epds_select_my_form_field_side") {
                                        tr_group.find('[data-name="epds-select-my-form-field-side"]').val(json_parsed_val[index_jpv][inner_index_jpv]);
                                    } else if (inner_index_jpv == "epds_formula_my_form_field_side") {
                                        tr_group.find('[data-name="epds-formula-my-form-field-side"]').val(json_parsed_val[index_jpv][inner_index_jpv]);
                                    } else if (inner_index_jpv == "epds_select_popup_form_field_side") {
                                        tr_group.find('[data-name="epds-select-popup-form-field-side"]').val(json_parsed_val[index_jpv][inner_index_jpv]);
                                    } else if (inner_index_jpv == "epds_formula_popup_form_field_side") {
                                        tr_group.find('[data-name="epds-formula-popup-form-field-side"]').val(json_parsed_val[index_jpv][inner_index_jpv]);
                                    }
                                }
                                if (index_jpv < (json_parsed_val.length - 1)) {
                                    if (tr_group_refresh_selection.eq(counter_embed_data_send + 1).length >= 1) {

                                    } else {
                                        tr_group.find('.epds-add-minus-row').children('.fa.fa-plus').trigger('click');
                                    }
                                }
                                counter_embed_data_send++;
                            }
                        }
                    }
                }

                if ($(".EmbedSetColumnResult_container").length >= 1) {
                    {//RESTORING SELECTED COLUMN TOTAL OUTPUT
                        var DOI = $("#popup_container").attr("data-object-id");
                        var get_field_ele = $(".getFields_" + DOI);
                        var data_json_output_col_total = get_field_ele.attr("data-output-col-total");
                        var escr_tr_group = $(".EmbedSetColumnResult_container").find('.escr-tr-group');
                        var escr_add_row = escr_tr_group.eq(0).find('.escr-add-minus-row').children('.fa.fa-plus');
                        var data_json_output_col_total_parsed = null;
                        try{
                            data_json_output_col_total_parsed = JSON.parse(data_json_output_col_total);
                        }catch(escr_error){
                            data_json_output_col_total_parsed = [];
                        }
                        for(ctr_index in data_json_output_col_total_parsed){
                            if(ctr_index == 0){
                                continue;
                            }
                            escr_add_row.trigger("click");
                        }
                        escr_tr_group = $(".EmbedSetColumnResult_container").find('.escr-tr-group');
                        for(ctr_index in data_json_output_col_total_parsed){
                            //escr_tr_group.eq(ctr_index).find('.escr-data-embed-column-field').children("option[value='"+data_json_output_col_total_parsed[ctr_index]["escr_embed_fs_col_data"]+"']").prop("selected",true);
                            //escr_tr_group.eq(ctr_index).find('.escr-data-result-output-field').children("option[value='"+data_json_output_col_total_parsed[ctr_index]["escr_embed_output_field"]+"']").prop("selected",true);
                            //escr_tr_group.eq(ctr_index).find('.escr-formula-based-col-comp').val(data_json_output_col_total_parsed[ctr_index]["escr_embed_fs_col_data"]);
                            //escr_tr_group.eq(ctr_index).find('.escr-data-result-output-field').children("option[value='"+data_json_output_col_total_parsed[ctr_index]["escr_embed_output_field"]+"']").prop("selected",true);
                            escr_tr_group.eq(ctr_index).find('.escr-data-embed-column-field').val(data_json_output_col_total_parsed[ctr_index]["escr_embed_fs_col_data"]);
                            escr_tr_group.eq(ctr_index).find('.escr-data-embed-computation-field').val(data_json_output_col_total_parsed[ctr_index]["escr_embed_computation_field"]);
                            escr_tr_group.eq(ctr_index).find('.escr-data-result-output-field').val(data_json_output_col_total_parsed[ctr_index]["escr_embed_output_field"]);
                            //console.log("eteoteoteoteoteoteo",data_json_output_col_total_parsed[ctr_index]["escr_embed_fs_col_data"]);
                        }
                    }
                }

                //appended_options_active_field
                $(this).remove();
                var embeddedtable = $('.embed-display-column');
           
            });




        });



        //EMBED GET FORM SOURCE LOOKUP ACTIVE FIELDS 
        var embed_source_lookup_field_ele_container = $(".embedProperty").find(".embed_source_lookup_field");
        embed_source_lookup_field_ele_container.find(".embed-source-lookup-field").html('<option>--Select--</option>');

        //SET ON CHANGE OF embed source lookup active field
        embed_source_lookup_field_ele_container.find(".embed-source-lookup-field").on({
            "change": function() {
                $(".getFields_" + field_id_on_form).attr("embed-source-lookup-active-field-val", $(this).val());
            }
        });




        //EMBED GET LIST OF FIELD NAMES
        var embed_result_field_ele_container = $(".embedProperty").find(".embed_result_field");
        //COLLECT FIELD NAMES
        // alert("TESTING")

        var field_names = '<option>--Select--</option>';
        var field_name_elements = $(".workspace.formbuilder_ws").find(".setObject").filter(function(){
            if (
                    $(this).find(".form-table").length >= 1 ||
                    $(this).find(".form-tabbable-pane").length >= 1 ||
                    $(this).find('[data-type="createLine"]').length >= 1 ||
                    $(this).attr("data-type") == "labelOnly" ||
                    $(this).attr("data-type") == "embeded-view" ||
                    $(this).attr("data-type") == "imageOnly"
            ) {
                
            }else{
                var obj_id = $(this).attr("data-object-id");
                var obj_fields = $(this).find(".getFields_" + obj_id).eq(0);
                if (obj_fields.is('[name]')) {
                    return true;
                }
            }
            return false;
        });
        field_name_elements.sort(function(a_dis,b_dis){
            var obj_id_a = $(a_dis).attr("data-object-id");
            var obj_fields_a = $(a_dis).find(".getFields_" + obj_id_a).eq(0);
            
            var obj_id_b = $(b_dis).attr("data-object-id");
            var obj_fields_b = $(b_dis).find(".getFields_" + obj_id_b).eq(0);
            
            var a_name = obj_fields_a.attr('name');
            var b_name = obj_fields_b.attr('name');
            if (!a_name) {
                a_name = "";
            }
            if (!b_name) {
                b_name = "";
            }
            return a_name.localeCompare(b_name);
        
        }).each(function() {
            // console.log("WHOA")
            // console.log($(this).find(".getFields").eq(0))
            var obj_id = $(this).attr("data-object-id");
            var obj_fields = $(this).find(".getFields_" + obj_id).eq(0);
            if (typeof obj_fields == "undefined") {
        
            } else {
                field_names
                        += '<option obj-id="' + obj_id + '" value="' + obj_fields.attr("name").replace('[]','') + '">' + obj_fields.attr("name").replace('[]','') + '</option>';
            }
        });
        
        
        
        
        embed_result_field_ele_container.find(".embed-result-field").html(field_names);
        if ($('[data-name="epds-select-my-form-field-side"]').length >= 1) {
            $('[data-name="epds-select-my-form-field-side"]').each(function() {
                $(this).html(field_names);
            });
        }
        if($('.escr-data-result-output-field').length >= 1 ){
            $('.escr-data-result-output-field').each(function(){
                $(this).html(field_names);
            });
        }
        var field_id_on_form = $("#popup_container").attr("data-object-id");
        if ($('.embedRoutingField_container').length >= 1) {
            var embedRoutingField_container = $('.embedRoutingField_container');
            embedRoutingField_container.find('.erf-route-my-form-fields').html(field_names)
            if ($(".getFields_" + field_id_on_form).attr("erf-route-my-form-fields")) {
                embedRoutingField_container.find('.erf-route-my-form-fields').val($(".getFields_" + field_id_on_form).attr("erf-route-my-form-fields"));
            }
        }

        //RESTORE SAVED VALUES

        if (typeof $(".getFields_" + field_id_on_form).attr("erf-obj-id-val") != "undefined") {
            embed_result_field_ele_container.find(".embed-result-field").find("option[obj-id='" + $(".getFields_" + field_id_on_form).attr("erf-obj-id-val") + "']").prop("selected", true);
        }

        //SET ONCHANGE EVENT
        // console.log(embed_result_field_ele_container)
        embed_result_field_ele_container.find(".embed-result-field").on({
            "change": function() {
                var selected_obj_id = $(this).children("option:selected").attr("obj-id");
                var field_id_on_form = $("#popup_container").attr("data-object-id");
                $(".getFields_" + field_id_on_form).attr("embed-result-field-val", $(this).val());
                $(".getFields_" + field_id_on_form).attr("erf-obj-id-val", selected_obj_id);
                //added by japhet morada
                //03-04-2016
                var search_field_data_type = $('#setObject_' + selected_obj_id).attr('data-type');
                $(".getFields_" + field_id_on_form).attr("search-field-object-name", search_field_data_type);
            }
        });
        //dito na ako
        if (typeof $(".getFields_" + field_id_on_form).attr("erf-obj-id-val") != "undefined") {
            embed_result_field_ele_container.find(".embed-result-field").find("option[obj-id='" + $(".getFields_" + field_id_on_form).attr("erf-obj-id-val") + "']").prop("selected", true);
        }
    }


    if ($(".embedPopupDataSending_container").length >= 1) {
        var popup_data_send_container_ele = $(".embedPopupDataSending_container");

        var action_add_min_container = popup_data_send_container_ele.find(".epds-add-minus-row");

        var epds_action_add = action_add_min_container.children(".fa.fa-plus");
        var epds_action_minus = action_add_min_container.children(".fa.fa-minus");

        var epds_my_form_side_chk = popup_data_send_container_ele.find('[name="epds-my-form-side-chk"]');
        var epds_popup_form_side_chk = popup_data_send_container_ele.find('[name="epds-popup-form-side-chk"]');

        {
            // events data send property
            epds_action_add.on({
                "click": function(e) {
                    var dis_ele_click = $(this);
                    var epds_data_send_collection_ele = dis_ele_click.parents(".epds-data-send-collection").eq(0);



                    {
                        var row_counter_for_unique = epds_data_send_collection_ele.data("row_counter");
                        console.log(typeof row_counter_for_unique, row_counter_for_unique)
                        if (typeof row_counter_for_unique === "undefined") {
                            row_counter_for_unique = 1;
                        } else {
                            row_counter_for_unique = row_counter_for_unique + 1;
                        }
                        console.log("COUNTERERER", row_counter_for_unique)

                        epds_data_send_collection_ele.data("row_counter", row_counter_for_unique);
                        console.log("COUNTERERER DATA", epds_data_send_collection_ele.data("row_counter"), epds_data_send_collection_ele)
                    }


                    var tr_group = dis_ele_click.parents(".epds-tr-group").eq(0);
                    var tr_group_cloned_w_evt = tr_group.clone(true);

                    tr_group_cloned_w_evt.find('[name][data-name]').each(function() {
                        $(this).attr("name", $(this).attr("data-name") + row_counter_for_unique);
                    });


                    tr_group.after(tr_group_cloned_w_evt);


                    var action_add_min_container = tr_group_cloned_w_evt.find(".epds-add-minus-row");
                    var epds_action_minus = action_add_min_container.children(".fa.fa-minus");
                    epds_action_minus.css("display", "block");
                }
            });

            epds_action_minus.on({
                "click": function() {
                    var tr_group = $(this).parents(".epds-tr-group").eq(0);
                    tr_group.remove();
                }
            });


            epds_my_form_side_chk.on({
                "click": function() {
                    var dis_ele_click = $(this);
                    var dis_val = dis_ele_click.val();
                    var dis_tr_next = dis_ele_click.parents("tr").eq(0).next();
                    var dis_tr_next_static = dis_tr_next.find('.epds-my-form-side-chk-static-selected');
                    var dis_tr_next_computed = dis_tr_next.find('.epds-my-form-side-chk-computed-selected');


                    if (dis_val == "static")
                    {
                        dis_tr_next_static.show();
                        dis_tr_next_computed.hide();
                    } else if (dis_val == "computed") {
                        dis_tr_next_computed.show();
                        dis_tr_next_static.hide();
                    }
                }
            });

            epds_popup_form_side_chk.on({
                "click": function() {
                    var dis_ele_click = $(this);
                    var dis_val = dis_ele_click.val();
                    if (dis_val == "static") {

                    } else if (dis_val == "computed") {

                    }
                }
            });
        }



    }
    if($('.EmbedSetColumnResult_container').length >= 1){
        var embed_escr_ele_container = $('.EmbedSetColumnResult_container');
        var escr_add_row = embed_escr_ele_container.find('.escr-add-minus-row').children('.fa.fa-plus');
        var escr_minus_row = embed_escr_ele_container.find('.escr-add-minus-row').children('.fa.fa-minus');
        escr_add_row.on({
            "click":function(){
                var ele_dis_row = $(this).parents('tbody.escr-tr-group').eq(0);
                var ele_dis_row_cloned = ele_dis_row.clone(true);
                ele_dis_row.after(ele_dis_row_cloned);
                ele_dis_row_cloned.find('.escr-add-minus-row').eq(0).children('.fa.fa-minus').show();
            }
        });
        escr_minus_row.on({
            "click":function(){
                var ele_dis_row = $(this).parents('tbody.escr-tr-group').eq(0);
                ele_dis_row.remove();
            }
        });
    }
    if($('.AdvanceEmbedActionEventVisibility').length >= 1){
        var table_container = $('.AdvanceEmbedActionEventVisibility').find(".advance-embed-action-event-visibility");
        MakePlusPlusRow({
            "tableElement":table_container,
            "selectorElePlus":".aeaev-add-minus-row .fa.fa-plus",
            "selectorToDuplicate":".aeaev-tr-group",
            "selectorEleMinus":".fa.fa-minus"
        });
        if($('.advance-embed-action-event-visibility').length >= 1){
            var DOI = $("#popup_container").attr("data-object-id");
            var adv_embed_act_ev_vi_ele = $('.advance-embed-action-event-visibility');
            var data_prop = $('body').data(""+DOI+"");
            if(data_prop){
                if(data_prop['visible-formula']){
                    var embed_visible_formula = data_prop['visible-formula'];
                    var embed_visible_formula_json = null;
                    try{
                        embed_visible_formula_json = JSON.parse(embed_visible_formula);
                    }catch(err){

                    }
                    if(embed_visible_formula_json!= null){
                        if($.type(embed_visible_formula_json) === "array" ) {
                            if(embed_visible_formula_json.length >= 1){
                                var action_clicks_add = -1;
                                for(var ctr_index in embed_visible_formula_json){
                                    if(ctr_index%2==0){
                                        action_clicks_add++;
                                    }
                                }
                                for(var ctr_ii = 1;ctr_ii <= action_clicks_add; ctr_ii++){
                                    adv_embed_act_ev_vi_ele.find(".aeaev-add-minus-row").last().children(".fa.fa-plus").trigger("click");
                                }
                                var selector_index_ctr = 0;
                                var temp_aeaev_tr_group_ele = adv_embed_act_ev_vi_ele.find(".aeaev-tr-group");
                                for(var ctr_index in embed_visible_formula_json){
                                    if(ctr_index%2==0){
                                        temp_aeaev_tr_group_ele.eq(selector_index_ctr).find(".aeaev-formula-based-visibility").val(embed_visible_formula_json[ctr_index]);
                                        temp_aeaev_tr_group_ele.eq(selector_index_ctr).find(".aeaev-data-target-visibility").val(embed_visible_formula_json[Number(ctr_index)+1]);
                                        selector_index_ctr++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    if($('.new-embed-source-form-data-filtering').length >=1){
        var fl_table_embed_source_form = $('.fl-tbl-embed-source-form');
        var fl_embed_select_source_form = $('.fl-embed-select-source-form');

        {//add minus and other event of ui
            MakePlusPlusRowV2({
                "tableElement":".fl-tbl-embed-source-form",
                "rules":[
                    {
                        "eleMinus":"tbody tr:eq(0) .fa.fa-minus.fl-addRemoveItem",
                        "eleMinusTarget":"tbody",
                        "elePlus":"tbody tr:eq(0) .fa.fa-plus.fl-addRemoveItem",
                        "elePlusTarget":"tbody",
                        "elePlusTargetFn":function(){
                            var self_ele = $(this);
                            self_ele.find('tr:not(:eq(1)) .fa.fa-minus.fl-addRemoveItem').show();
                            self_ele.find('tr:not(:eq(1)) .fl-embed-filter-group-and-or').show();
                        }
                    },
                    {
                        "eleMinus":"tbody tr:eq(1) .fa.fa-minus.fl-addRemoveItem",
                        "eleMinusTarget":"tr",
                        "elePlus":"tbody tr:eq(1) .fa.fa-plus.fl-addRemoveItem",
                        "elePlusTarget":"tr",
                        "elePlusTargetFn":function(){
                            var self_ele = $(this);
                            self_ele.find('.fa.fa-minus.fl-addRemoveItem').show();
                            self_ele.find('.fl-embed-filter-condition-and-or').show();
                        }
                    }
                ]
            });
            fl_table_embed_source_form.find('.fl-embed-filter-condition-value-switcher').on({
                "change":function(e){
                    var self = $(this);
                    var self_value = self.val();
                    var self_td_parent = self.parents('td');
                    var next_td_parent = self_td_parent.next();
                    var ele_static = next_td_parent.find('.fl-embed-filter-condition-static-value');
                    var ele_dynamic = next_td_parent.find('.fl-embed-filter-condition-dynamic-value');
                    if(self_value == "static"){
                        ele_static.parents('.properties_width_container').eq(0).removeClass('isDisplayNone');
                        ele_dynamic.parents('.properties_width_container').eq(0).addClass('isDisplayNone');
                    }else if(self_value == "dynamic"){
                        ele_static.parents('.properties_width_container').eq(0).addClass('isDisplayNone');
                        ele_dynamic.parents('.properties_width_container').eq(0).removeClass('isDisplayNone');
                    }
                }
            });
        }
        

        {//taga lagay lang to dapat ng mga data sa lahat ng select ng dialog
            
            var fl_embed_filter_condition_static_value = fl_table_embed_source_form.find('.fl-embed-filter-condition-static-value');
            var collect_formbuilder_fields = '<option value="">--Select--</option>'
            var current_field_names_in_formbuilder = $('.formbuilder_ws').find('.getFields[name]').get().map(function(a,b){
                return $(a).attr('name').replace('[]','');
            }).filter(Boolean).sort(function(a,b){
                return a.toLowerCase().localeCompare(b.toLowerCase());
            });
            $.unique(current_field_names_in_formbuilder);
            var collect_as_object_formbuilder_fields = current_field_names_in_formbuilder.map(function(a,b){
                var get_fields_selector = $('.getFields[name="'+a+'"]');
                if(get_fields_selector.length <= 0){
                    get_fields_selector = $('.getFields[name="'+a+'[]"]');
                }
                return {
                    'name':a,
                    "data_object_id":get_fields_selector.parents('.setObject').eq(0).attr('data-object-id')
                };
            });
            

            for(var ctr_ii in collect_as_object_formbuilder_fields){
                collect_formbuilder_fields += '<option obj-id="'+collect_as_object_formbuilder_fields[ctr_ii]['data_object_id']+'" value="'+collect_as_object_formbuilder_fields[ctr_ii]['name']+'">'+collect_as_object_formbuilder_fields[ctr_ii]['name']+'</option>';
            }
            fl_embed_filter_condition_static_value.html(collect_formbuilder_fields);

            getUserFormsV2(function(dataEchoResults){ 
                var json_echo_results = JSON.parse(dataEchoResults);
                var collect_form_source_selection = '<option value="">--Select--</option>';
                
                var form_selection = json_echo_results.map(function(vv,ii){
                    var form_fields_type = "";
                    var active_fields = vv['active_fields'];
                    try{
                        form_fields_type = JSON.parse(vv['form_json']);
                        form_fields_type = form_fields_type['form_fields'];
                    }catch(e){
                        console.error("ERROR getUserFormsV2 form_json",e);
                    }
                    active_fields = active_fields.split(',').sort(function(a,b){
                        return a.toLowerCase().localeCompare(b.toLowerCase());
                    }).join(',');
                    return {
                        "form_id":vv['form_id'],
                        "form_name":vv['form_name'],
                        "active_fields":active_fields,
                        "form_fields_type":form_fields_type,
                    };

                }).sort(function(a,b){
                    return a['form_name'].toLowerCase().localeCompare(b['form_name'].toLowerCase());
                });

                for(var ctr_ii in form_selection){
                    collect_form_source_selection += '<option frm-fields-type="'+htmlEntities(form_selection['form_fields_type'])+'" frm-id="' + form_selection['form_id'] + '"  active-fields="'+form_selection[ctr_ii]['active_fields']+'" value="'+form_selection[ctr_ii]['form_id']+'">'+form_selection[ctr_ii]['form_name']+'</option>';
                }
                
                fl_embed_select_source_form.html(collect_form_source_selection);
                //for multiple embed source form
                $('.m-embed-source-form').html(collect_form_source_selection);

                fl_embed_select_source_form.on({
                    "focus":function(){
                        var self = $(this);
                        self.data('data_last_selected_option', self.children('option:selected') );
                    },
                    "change":function(){
                        var self = $(this);
                        var active_fields = self.children('option:selected').attr('active-fields');
                        var collect_active_fields = '<option value="">--Select--</option>';
                        var fl_table_embed_source_form = $('.fl-tbl-embed-source-form');
                        var fl_embed_filter_condition_sform_field = fl_table_embed_source_form.find('.fl-embed-filter-condition-sform-field');
                        
                        {//save to last selected option
                            self.data( 'data_last_selected_option' ).data('data_set', SaveEmbedSourceAndFiltering( self.data( 'data_last_selected_option' ) ) );
                            //console.log("\nNAKARAAN",self.data('data_last_selected_option'), self.data( 'data_last_selected_option' ).data('data_set') );
                        }

                        if($.type(active_fields) == 'string'){
                            active_fields = active_fields.split(',');
                            for(var ctr_ii in active_fields){
                                collect_active_fields += '<option value="'+active_fields[ctr_ii]+'">'+active_fields[ctr_ii]+'</option>';
                            }
                        }

                        fl_embed_filter_condition_sform_field.each(function(){
                            $(this).html(collect_active_fields);
                        });



                        {//restore data set when there is data set for the current selected option
                            var selected_option = self.children('option:selected');
                            if(selected_option.is(':data("data_set")')){
                                var data_user_set = selected_option.data('data_set');
                                var dus_advance_filter_data = data_user_set['advance_filter_data'];

                                {//adding and removing tbody
                                    var current_len_of_tbody = fl_table_embed_source_form.children('tbody').length;
                                    var tbody_difference = dus_advance_filter_data.length - current_len_of_tbody;
                                    var tbodies = fl_table_embed_source_form.children('tbody');
                                    var tbody_add_button = tbodies.last().children('tr:eq(0)').find('.fa.fa-plus.fl-addRemoveItem');
                                    var tbody_remove_buttons = tbodies.filter(':not(:eq(0))').children('tr').filter(function(){ return $(this).index() == 0; }).find('.fa.fa-minus.fl-addRemoveItem').get().reverse();
                                    tbody_remove_buttons = $(tbody_remove_buttons);
                                    if(tbody_difference >= 0){
                                        for(var ctr_ii = 1 ; ctr_ii <= tbody_difference ; ctr_ii++ ){
                                            tbody_add_button.trigger('click');
                                        }
                                    }else if(tbody_difference < 0){
                                        var difference_absolute = Math.abs(tbody_difference);
                                        tbody_remove_buttons.each(function(eqi){
                                            if(eqi <= (difference_absolute-1)){
                                                $(this).trigger('click');
                                            }else{
                                                return false;
                                            }
                                        });
                                    }
                                }

                                {//restore user last data selected
                                    tbodies = fl_table_embed_source_form.children('tbody');
                                    tbodies.each(function(eq_index){
                                        var self = $(this);
                                        var dus_adv_fltr_dta = dus_advance_filter_data;
                                        var group_logical_operator_val = dus_adv_fltr_dta[eq_index]['group_logical_operator'];
                                        var filter_conditions = dus_adv_fltr_dta[eq_index]['filter_conditions'];
                                        var filter_conditions_len = filter_conditions.length;

                                        var temp_fltr_each_condition = "";
                                        var temp_a_val = "";
                                        var temp_b_val = "";
                                        var temp_c_val = "";
                                        var temp_d_val = "";
                                        var temp_e_val = "";
                                        var temp_f_val = "";

                                        var group_and_or_ele = self.find('.fl-embed-filter-group-and-or');
                                        var filter_conditions_ele = self.children('tr');
                                        var filter_conditions_ele_len = filter_conditions_ele.filter(':not(:eq(0))').length;
                                        var filter_conditions_add_buttons_ele = filter_conditions_ele.find('.fa.fa-plus.fl-addRemoveItem');
                                        var filter_conditions_remove_buttons_ele = $(filter_conditions_ele.filter(':not(:eq(1))').find('.fa.fa-minus.fl-addRemoveItem').get().reverse());
                                        var filter_conditions_len_difference = filter_conditions_len - filter_conditions_ele_len;
                                        console.log("filter_conditions_len_difference", filter_conditions_len,filter_conditions_ele_len)
                                        //restore group and or add remove
                                        group_and_or_ele.val(group_logical_operator_val);
                                        //restore elements
                                        if(filter_conditions_len_difference >= 0){
                                            for(var ctr_ii = 1 ; ctr_ii <= filter_conditions_len_difference ; ctr_ii ++){
                                                filter_conditions_add_buttons_ele.last().trigger('click');
                                            }
                                        }else if(filter_conditions_len_difference < 0){
                                            var difference_absolute = Math.abs(filter_conditions_len_difference);
                                            filter_conditions_remove_buttons_ele.each(function(eq_index){
                                                if(eq_index <= (difference_absolute-1)){
                                                    $(this).trigger('click');
                                                }else{
                                                    return false;
                                                }
                                            });
                                        }
                                        var each_tr_filter_condition = self.children('tr').filter(':not(:eq(0))');
                                        each_tr_filter_condition.each(function(ctr_ii){
                                            var self = $(this);
                                            temp_fltr_each_condition = filter_conditions[ctr_ii];

                                            temp_a_val = temp_fltr_each_condition['a_filter_condition_logical_operator'];
                                            temp_b_val = temp_fltr_each_condition['b_filter_condition_source_form_field'];
                                            temp_c_val = temp_fltr_each_condition['c_filter_condition_conditional_operator'];
                                            temp_d_val = temp_fltr_each_condition['d_filter_condition_value_switcher'];
                                            temp_e_val = temp_fltr_each_condition['e_filter_condition_static_value'];
                                            temp_f_val = temp_fltr_each_condition['f_filter_condition_dynamic_value'];

                                            self.find('.fl-embed-filter-condition-and-or').val(temp_a_val);
                                            self.find('.fl-embed-filter-condition-sform-field').val(temp_b_val);
                                            self.find('.fl-embed-filter-condition-operator').val(temp_c_val);
                                            self.find('.fl-embed-filter-condition-value-switcher').val(temp_d_val);
                                            self.find('.fl-embed-filter-condition-static-value').val(temp_e_val);
                                            self.find('.fl-embed-filter-condition-dynamic-value').val(temp_f_val);
                                        });
                                    });
                                }
                            }
                        }

                        {//save the current selected option
                            self.data( 'data_last_selected_option', self.children('option:selected') );
                        }
                    }
                });
                {//restore saved data property of embed
                    var DOI = $("#popup_container").attr("data-object-id");
                    var get_field_ele_embed = $('.getFields_'+DOI);
                    if(get_field_ele_embed.is('[data-new-embed-source]')){
                        var saved_data_new_embed_source = get_field_ele_embed.attr('data-new-embed-source');
                        if($.type(saved_data_new_embed_source) != 'undefined'){
                            var parsed_json_saved_data_new_embed_source = JSON.parse(saved_data_new_embed_source);
                            var restore_selected_form_id = parsed_json_saved_data_new_embed_source['selected_form']['form_id'];
                            var fl_embed_select_source_form_ele = $('.fl-embed-select-source-form');
                            fl_embed_select_source_form_ele.trigger('focus');
                            fl_embed_select_source_form_ele.children('option[value="'+restore_selected_form_id+'"]').data('data_set', parsed_json_saved_data_new_embed_source );
                            fl_embed_select_source_form_ele.val(restore_selected_form_id);
                            fl_embed_select_source_form_ele.trigger('change');
                        }
                    }
                }
            });
        }
    }


    if($('.embed_conditional_operator').length >= 1){
        if($.type($(".getFields_"+DOI).attr('embed-conditional-operator')) != "undefined"){
            $('.embed-conditional-operator').val($(".getFields_"+DOI).attr('embed-conditional-operator'));
        }
    }

    if($('.embed_additional_filter_formula').length >= 1){
        if($.type($(".getFields_"+DOI).attr('embed-additional-filter-formula')) != "undefined"){
            $('.embed-additional-filter-formula').val($(".getFields_"+DOI).attr('embed-additional-filter-formula'));
        }
    }
    //copy this...
    if($('.embedded_view_allow_import').length > 0){
        if($.type($(".getFields_"+DOI).attr('embed-allow-import')) != "undefined"){
            if($(".getFields_"+DOI).attr('embed-allow-import') == "true"){
                $('.embedded_view_allow_import').prop('checked', true);
            }
            else{
                $('.embedded_view_allow_import').prop('checked', false);
            }
        }
        $('.embedded_view_allow_import').on('click', function(e){
            if($(this).prop('checked') == true){
                var active_fields = $('.embed-source-form option:selected').attr('active-fields');
                $(".getFields_"+DOI).attr('embed-allow-import', $(this).prop('checked'));
                $(".getFields_"+DOI).attr('import-active-fields', active_fields);
            }
            else{
                 $(".getFields_"+DOI).removeAttr('embed-allow-import');
                 $(".getFields_"+ DOI).removeAttr('import-active-fields');
            }
            
        })
    }
    if($('.embed-dialog-type').length > 0){
        $('.embed-dialog-type').on('change', function(e){
            var dialog_val = $(this).val();
            if(dialog_val == "2"){
                $('.embed-popup-window-properties').show();
            }
            else{
                $('.embed-popup-window-properties').hide();
            }
        });
    }

    if($('.embed_single_form_add_column_icon').length > 0){
        $(document).on('click', '.embed-multiple-icon-settings', function(e){
            var self = $(this);
            var dialog = self.find('.embed-icon-display-config');
            console.log("samalaymalaykum", e.target, e.target != dialog[0], $(e.target).parents('.embed-icon-display-config').length <= 0);
            if(e.target != dialog[0] && $(e.target).parents('.embed-icon-display-config').length <= 0){
                if(dialog.is(':visible')){
                    console.log("visible");
                    dialog.css('display','none');
                }
                else{
                    dialog.css('display', '');
                }
            }
            // dialog.perfectScrollbar();
        })
        MakePlusPlusRowV2({
            "tableElement":".embed-multiple-icon-selection",
            "rules":[
                {
                    "eleMinus":"tbody:eq(0) > tr:eq(0) > td:eq(1) > .embed_column_icons_plus_minus > .fa.fa-minus",
                    "eleMinusTarget":"tbody:eq(0)",
                    "elePlus":"tbody:eq(0) > tr:eq(0) >  td:eq(1) > .embed_column_icons_plus_minus > .fa.fa-plus",
                    "elePlusTarget":"tbody:eq(0)",
                    "elePlusTargetFn":function(a,b,c){
                        var self_ele = $(this);
                        console.log("eto...", a ,b, $(this));
                        if(self_ele.find('.fa-minus').is(':visible') == false){
                            self_ele.find('tr:eq(0)').find('.fa.fa-minus').show();
                            self_ele.find('.embed-column-values-icon').attr('data-object-id', Number(b.prev_ele.find('.embed-column-values-icon').attr('data-object-id')) + 1);
                            self_ele.find('.embed_column_values_selected_icon > svg').html("");
                            self_ele.find('.embed-column-icon-formula').val("");
                        }
                        // self_ele.find('tr:not(:eq(1)) > .fa.fa-minus.fl-addRemoveItem').show();
                        // self_ele.find('tr:not(:eq(1)) > .fl-embed-filter-group-and-or').show();
                    },
                    "eleMinusTargetFn":function(a,b){
                        var self_ele = $(this);
                        // self_ele.find('tr:not(:eq(1)) > .fa.fa-minus.fl-addRemoveItem').show();
                        // self_ele.find('tr:not(:eq(1)) > .fl-embed-filter-group-and-or').show();
                    }
                }
                // {
                //     "eleMinus":"tbody tr:eq(1) .fa.fa-minus.fl-addRemoveItem",
                //     "eleMinusTarget":"tr",
                //     "elePlus":"tbody tr:eq(1) .fa.fa-plus.fl-addRemoveItem",
                //     "elePlusTarget":"tr",
                //     "elePlusTargetFn":function(){
                //         var self_ele = $(this);
                //         self_ele.find('.fa.fa-minus.fl-addRemoveItem').show();
                //         self_ele.find('.fl-embed-filter-condition-and-or').show();
                //     }
                // }
            ]
        });
        // $('.embed_single_form_add_column_icon').on('click', function(e){
        //     var ret = '<tr>';
        //         ret += '<td>';
        //             ret += '<input type="text" class="form-text">';
        //         ret += '</td>';
        //         ret += '<td>';
        //             ret += '<div class="embed-multiple-icon-settings">';
        //                 ret += '<span>Select Icon: </span>';
        //                 ret += '<div class="embed_newRequest embed-multiple-icon-button" style=" text-align: center; padding: 5px; cursor: pointer; width: 20px; margin-left: 5px; border-radius: 5px; position: relative; display: inline-block; ">';
        //                     ret += '<span style="color: white;">...</span>';
        //                     ret += '<div class="properties_width_container fl-field-style embed-icon-display-config" style=" position: absolute; width: 300px; height: 150px; z-index: 10; overflow: auto; cursor: default; left: -270px; display: none;">';
        //                         ret += '<div class="fields_below">';
        //                             ret += '<div class="column" style="width: 95%;">';
        //                                 ret += '<table class="embed-multiple-icon-selection" style="width:99%;">';
        //                                     ret += '<tbody>';
        //                                         ret += '<tr><td style="border:none;"></td><td style="border:none;"><span class="embed_column_icons_plus_minus" style="float: right;"><i class="fa fa-plus click_me123" style="margin-right: 3px;cursor: pointer;"></i><i class="fa fa-minus" style="margin-right: 3px;cursor: pointer; display: none;"></i></span></td></tr>';
        //                                             ret += "<tr>";
        //                                             ret += '<td style="border:none;width:30%;">';
        //                                                 ret += '<div style="position:relative;height:50px;width:70px;margin-left:5px;">';
        //                                                     ret += '<div style="position: absolute;top: 0;left: 0;">';
        //                                                         ret += '<span style="margin-top: 4px; margin-bottom:0px !important;">Set Icon: </span>';
        //                                                         ret += '<div class="embed_newRequest embed-column-values-icon" data-object-id="0" id="embed-column-values-icon" data-selected-icon="svg-column-values-icon-030" style="padding: 1px;height: 28px;width: 45px;left: 0px;border-radius: 3px;cursor: pointer;">';
        //                                                             ret += '<div class="embed_column_values_selected_icon" style="width: 20px;height: 20px;display:inline-block;padding: 4px;background-color: white;">';
        //                                                                 ret += '<svg style="width: 100%;" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50">';

        //                                                                  ret += '</svg>';
        //                                                              ret += '</div>';
        //                                                             ret += '<div class="embed_custom_action_selection" style="position: absolute;display: inline-block;height: 20px;padding: 4px;font-size: 12px;text-align: center;color: white;left:28px;">';
        //                                                                ret += '▼';
        //                                                             ret += '</div>';
        //                                                          ret += '</div>';
        //                                                      ret += '</div>';
        //                                                 ret += '</div>';
        //                                             ret += '</td>';
        //                                             ret += '<td>';
        //                                                 ret += '<textarea class="form-textarea ide-hover embed-column-icon-formula" data-ide-properties-type="m-embed-column-icon-formula" name="m-embed-column-icon-formula" placeholder=""></textarea>';
        //                                             ret += '</td>';
        //                                         ret += '</tr>';
        //                                     ret += '</tbody>';
        //                                 ret += '</table>';
        //                             ret += '</div>';
        //                         ret += '</div>';
        //                     ret += '</div>';
        //                 ret += '</div>';
        //             ret += '</div>';
        //         ret += '</td>';
        //     ret += '</tr>';
        //     var tbody = $('.embedded-view-columns > tbody');
        //     $(ret).insertAfter(tbody.children('tr:eq(0)'));
        // });
        $('body').on('click', function(e){
            if(e.target.id != "embed-column-values-icon" && e.target.parentNode.id != "embed-column-values-icon" && $(e.target).parents('.embed_column_icon_list').attr('id') != "embed_column_icon_list"){
                if($('body').find('.embed_column_icon_list').length > 0){
                    $('body').find('.embed_column_icon_list').remove();
                }
            }
        });
    }

    if($('.sourFormType').length > 0){
        var source_form_type = $('.getFields_' + DOI).attr('source-form-type')||"single";
        $('[name="sourceformtype"]').on('change', function(e){
            var prop_sequence = {};
            if($(this).val() == "single"){
                $('.embed-default-prop').removeClass('display');
                $('.embed-multiple-prop').addClass('display');
                prop_sequence = {
                    "embedLeftPanel": [
                        '.embed_row_click',
                        '.embedPopupDataSending_container',
                        '.highlightedRowProp',
                        '.embedHighlightProp',
                        '.embedRoutingField_container',
                        '.obj_hideModalCloseBtn',
                        '.AdvanceEmbedActionEventVisibility',
                        '.embedCommitDataRowProperty',
                        '.embedDefaultSorting',
                        '.embedRowSelection',
                        '.embedHighlightProp',
                        '.embedCustomActions'
                    ],
                    "embedRightPanel": [
                        '.embedProperty',
                        '.embedPropertyDisplayColumn',
                        '.EmbedSetColumnResult_container',
                        '.embedPropertyDialogType'
                    ]
                }
                $('.embedRowSelection').find('.single-form-row-category-refresh').show();
                $('.embed-source-form').trigger('change');
            }
            else{
                $('.embed-multiple-prop').removeClass('display');
                $('.embed-default-prop').addClass('display');
                prop_sequence = {
                    "multipleEmbedLeftPanel": [
                        '.embed_row_click',
                        '.embedRoutingField_container',
                        '.embedCustomActions'
                        
                    ],
                    "multipleEmbedRightPanel": [
                        '.highlightedRowProp',
                        '.AdvanceEmbedActionEventVisibility',
                        '.embedPropertyDialogType',
                        '.obj_hideModalCloseBtn',
                        '.embedRowSelection',
                        '.embedPopupDataSending_container',     
                        '.EmbedSetColumnResult_container'
                    ]
                }
                $('.embedRowSelection').find('.single-form-row-category-refresh').hide();
                $('.embedMultipleProperties').find('.m-embed-source-form').trigger('change', true);
            }
            $('.getFields_' + DOI).attr('source-form-type', $(this).val());

            shuffleProperties(prop_sequence);
        });
        if(source_form_type == "single"){
            $('[name="sourceformtype"][value="single"]').trigger('click');
            // $('[name="sourceformtype"][value="single"]').trigger('change');
        }
        else{
            $('[name="sourceformtype"][value="multiple"]').trigger('click');
            // $('[name="sourceformtype"][value="multiple"]').trigger('change');
        }
        if($('.embedRowSelection').length > 0){
            var data = $('body').data('' + $('.embedRowSelection').parents('#popup_container').attr('data-object-id'));
            var record_selection_config = data['allow_row_selection']||false;
            // if(record_selection_config == true){
                $('.embedRowSelection > .fields_below > .input_position_below > .div_1_of_1 > label > .embed_row_selection_prop').prop('checked', record_selection_config);
            // }
        }
        if($('.embed_row_category_prop').length > 0){
            var data = $('body').data(''+ $('.embed_row_category_prop').parents('#popup_container').attr('data-object-id'));
            var row_category_prop = data['allow_row_category']||false;
            var columns = "";
            $('.embedRowSelection > .fields_below > .input_position_below > .div_1_of_1 > label > .embed_row_category_prop').prop('checked', row_category_prop);
            if(source_form_type == "multiple"){
                for(i in data['column_settings']){
                    var column_data = data['column_settings'][i];
                    var is_selected = "";
                    if(column_data['column_field_name'] == data['allow_row_category_column']){
                       is_selected = "selected";
                    }   
                    columns += "<option value='" + column_data['column_field_name'] + "' " + is_selected + ">" + column_data['column_field_caption'] + "</option>";
                }
                $('.embedRowSelection').find('.single-form-row-category-refresh').hide();
            }
            else{
                var single_columns = JSON.parse($('.getFields_' + DOI).attr('embed-column-data')||"[]");
                if(single_columns.length > 0){
                    for(i in single_columns){
                        var column_data  = single_columns[i];
                        var is_selected = "";
                        if(column_data['FieldName'] == data['allow_row_category_column']){
                            is_selected = "selected";
                        }
                        columns += "<option value='" + column_data['FieldName'] + "' " + is_selected + ">" + column_data['FieldLabel'] + "</option>";
                    }
                }
                $('.embedRowSelection').find('.single-form-row-category-refresh').show();
            }
            $('.embedRowSelection > .fields_below .input_position_below > .div_1_of_1 > .column_category_selection > .embed_row_category_column_dropdown').html(columns);
            if(row_category_prop){
                $('.embedRowSelection > .fields_below .input_position_below > .div_1_of_1 > .column_category_selection').show();
            }
            else{
                $('.embedRowSelection > .fields_below .input_position_below > .div_1_of_1 > .column_category_selection').hide();
            }

        }
        if($('.embedCustomActions').length > 0){
            var custom_actions_data = $('body').data(""+$('.embedRowSelection').parents('#popup_container').attr('data-object-id'))['embed_custom_actions']||[];
            MakePlusPlusRowV2({
                "tableElement": ".embed-custom-actions-table",
                "rules":[
                    {
                        "eleMinus":"tbody:eq(0) > tr:eq(0) > td > .embed_custom_actions_plus_minus > .fa.fa-minus",
                        "eleMinusTarget":"tbody:eq(0)",
                        "elePlus":"tbody:eq(0) > tr:eq(0) >  td > .embed_custom_actions_plus_minus > .fa.fa-plus",
                        "elePlusTarget":"tbody:eq(0)",
                        "elePlusTargetFn":function(a,b,c){
                            var self_ele = $(this);
                            if(self_ele.find('.fa-minus').is(':visible') == false){
                                self_ele.find('.fa.fa-minus').show();
                            }
                            console.log("find me... ", b.prev_ele.find('.embed-custom-actions-icon').attr('data-object-id'));
                            self_ele.find('.embed-custom-actions-icon').attr('data-object-id', Number(b.prev_ele.find('.embed-custom-actions-icon').attr('data-object-id')) + 1);
                            // self_ele.find('tr:not(:eq(1)) > .fa.fa-minus.fl-addRemoveItem').show();
                            // self_ele.find('tr:not(:eq(1)) > .fl-embed-filter-group-and-or').show();
                        },
                        "eleMinusTargetFn":function(a,b){
                            var self_ele = $(this);
                            if(b.parent_ele.find('tr').length <= 1){
                                b.prev_ele.find('.fa.fa-minus').hide();
                            }
                            // self_ele.find('tr:not(:eq(1)) > .fa.fa-minus.fl-addRemoveItem').show();
                            // self_ele.find('tr:not(:eq(1)) > .fl-embed-filter-group-and-or').show();
                        }
                    }
                ]
            });
            if(custom_actions_data.length > 0){
                for(var i in custom_actions_data){
                    var cloned_tbody = $('.embed-repeated-actions').eq(i);
                    var custom_action_data = custom_actions_data[i];
                    cloned_tbody.find('.select_custom_action option[value="' + custom_action_data['custom_action_type'] + '"]').prop('selected', true).trigger('change');
                    cloned_tbody.find('[name="embed_custom_actions_label"]').val(custom_action_data['custom_action_label']||"");
                    cloned_tbody.find('.embed_custom_action_formula').val(custom_action_data['custom_action_formula']||"");
                    cloned_tbody.find('.embed-custom-actions-icon').attr('data-selected-icon',custom_action_data['custom_action_icon_id']||"");
                    cloned_tbody.find('.embedded-view-custom-actions-icon-pick').setSVGVal(custom_action_data['custom_action_icon_id']);
                    cloned_tbody.find('.fa-plus').trigger('click');
                }
                $('.embed-repeated-actions').last().find('.fa-minus').trigger('click');
            }
        }
    }
}

{//VERSION 1 HIGHLIGHT PROPERTY
        function restoreSetValHLRULE() {
            var containtment = $(".highlight-row-prop");
            //RESTORE SET VALUES
            var hlc_tbl = containtment.find(".design-embed-highlights-prop");
            var DOI = $("#popup_container").attr("data-object-id");
            var field_elem = $("#setObject_" + DOI).find(".getFields_" + DOI);//.attr("embed-hl-data")
            if (typeof field_elem.attr("embed-hl-data") != "undefined") {
                try {
                    var json_e_hl = JSON.parse(field_elem.attr("embed-hl-data"));
                    $.each(json_e_hl, function(eqi, value) {
                        if (eqi != 0) {
                            $(".HL_RULE_PROP2").eq(eqi - 1).find('.addHLRule').click();
                        }
                        if (hlc_tbl.length >= 1) {
                            // value['hl_color']
                            // value['hl_fn']
                            // value['condition_type']
                            // value['hl_value']
                            // value['hl_value2']
                            $(".HL_RULE_PROP1").eq(eqi).find('.chooseColor.hl-color').spectrum("set", value['hl_color']);
                            $(".HL_RULE_PROP1").eq(eqi).find('.fieldname-basis-highlight.hl-fn').val(value['hl_fn']);
                            $(".HL_RULE_PROP2").eq(eqi).find('.choose-condition-type').val(value['condition_type']);
                            $(".HL_RULE_PROP2").eq(eqi).find('.hl-value').val(value['hl_value']);
                            $(".HL_RULE_PROP2").eq(eqi).find('.hl-value2').val(value['hl_value2']);
                            $(".HL_RULE_PROP2").eq(eqi).find('.value-type-HL[value="' + value['value_type'] + '"]').prop("checked", true);
                            if (value['condition_type'] == "range" || value['condition_type'] == "in_between") {
                                $(".HL_RULE_PROP2").eq(eqi).find('.hl-value2').css("display", "inline-block");
                            }
                            // var hlc_tr = hlc_tbl.find("tr").eq(eqi)
                            // hlc_tr.find(".chooseColor").spectrum("set",value["hl_color"]);
                            // //.css("background-color", value["hl_color"]);
                            // // hlc_tr.find(".hl-fn").val(value["hl_fn"]); // or this below
                            // //         hlc_tr.find(".hl-fn option[value='"+value["hl_fn"]+"']").prop("selected",true);
                            // hlc_tr.find(".hl-value").val(value["hl_value"])
                        }
                    })
                } catch (err) {

                }
            }
        }
        function bindAddHLRULE(dis_ele) {
            var element_add = $(dis_ele);
            element_add.find(".addHLRule").on("click", function() {
                var dis_prop2 = $(this).parents('tr.HL_RULE_PROP2').eq(0);
                var dis_prop1 = dis_prop2.prev();
                var selected_to_clone = dis_prop1.add(dis_prop2).clone(true);
                var tbody_row_container = $(this).parents("tbody").eq(0);
                var new_index_row = tbody_row_container.children("tr").last().index() + 1;
                // console.log($('.highlight-row-prop').data('countAdds'))
                if (typeof $('.highlight-row-prop').data('countAdds') === "undefined") {
                    $('.highlight-row-prop').data('countAdds', 1)
                }
                var countAdds = $('.highlight-row-prop').data('countAdds');
                /* alert(countAdds)*/
                //modify display here
                selected_to_clone.find(".value-type-HL").each(function(eqi) {
                    $(this).attr("name", $(this).attr("data-name") + "_r" + countAdds);

                    if ($(this).attr('value') == 'static') {
                        $(this).attr("id", "DHLradioStatic_r" + countAdds);
                        $(this).next().attr('for', 'DHLradioStatic_r' + countAdds)
                    } else {
                        $(this).attr("id", "DHLradioComputed_r" + countAdds);
                        $(this).next().attr('for', 'DHLradioComputed_r' + countAdds)
                    }

                    if ($(this).val() == "static") {
                        // alert(23)
                        $(this).prop("checked", true);
                    }
                });
                dis_prop2.after(selected_to_clone);
                // tbody_row_container.append(selected_to_clone);
                selected_to_clone.find(".sp-replacer.sp-light.full-spectrum").remove();
                var location_for_color_picking = $(selected_to_clone.find(".chooseColor.hl-color").parent());
                selected_to_clone.find(".chooseColor.hl-color").remove();
                var color_picking = $('<input type="text" class="chooseColor hl-color" data-properties-type="embRowColor" data-type="emb-highlight-color">').clone();
                location_for_color_picking.append(color_picking);
                $(color_picking).spectrum(json_spectrum_settings);
                selected_to_clone.find(".subtractHLRule").css("display", "inline-block");

                $('.highlight-row-prop').data('countAdds', $('.highlight-row-prop').data('countAdds') + 1)
            })
        }
        function bindSubtractHLRULE(dis_ele) {
            var element_subtract = $(dis_ele);
            element_subtract.find(".subtractHLRule").on("click", function() {
                var dis_prop2 = $($(this).parents('tr.HL_RULE_PROP2').eq(0));
                var dis_prop1 = $(dis_prop2.prev());
                dis_prop1.remove();
                dis_prop2.remove();
            })
        }
        function bindConditionTypeHLRULE(dis_ele) {
            var element_CT = $(dis_ele);
            element_CT.on("change", function() {
                var value_nito = $(this).val();
                var location_value_container = $(this).parent();
                if (value_nito == "range" || value_nito == "in_between") {
                    location_value_container.find(".hl-value2").css("display", "inline-block");
                    location_value_container.find(".hl-value").css("width", "49%");
                } else {
                    location_value_container.find(".hl-value2").css("display", "none");
                    location_value_container.find(".hl-value").css("width", "");
                }
            });
        }
    }

//for embedded view new features
function loadEmbedMultipleFormPropertiesLeft(obj_float, data_type){
    var ret = "";
    //multiple form html structure
    ret += '<div class="' + obj_float + ' properties_width_container embedMultipleProperties">';
        ret += '<div class="fields_below">';
            ret += '<div class="input_position_below section clearing fl-field-style embed_multiple_form">';
                ret += '<table class="embed-multiple-form-table" style="width: 99%; margin-left: 5px; margin-right: 5px;">';
                    ret += '<thead>';
                        ret += '<tr>';
                            ret += '<th style="width:19%;padding:5px;">Embed Source Form</th>';
                            ret += '<th style="width:19%;padding:5px;">Lookup Field</th>';
                            ret += '<th style="width:19%;padding:5px;">Operator</th>';
                            ret += '<th style="width:19%;padding:5px;">Search Field</th>';
                            ret += '<th style="width:19%;padding:5px;">Addtional Filter Formula</th>';
                            ret += '<th></th>';
                        ret += '</tr>';
                    ret += '</thead>';
                    ret += '<tbody>';
                        ret += '<tr class="m-embed-pc">';//multiple embed prop container
                            ret += '<td style="padding:5px;"><select class=" form-select m-embed-source-form" ></select></td>';
                            ret += '<td style="padding:5px;"><select class=" form-select m-embed-lookup-field" ></select></td>';
                            ret += '<td style="padding:5px;"><select class=" form-select m-embed-operator" ><option value="=">=</option><option value="!=">!=</option></select></td>';
                            ret += '<td style="padding:5px;"><select class=" form-select m-embed-search-field" ></select></td>';
                            ret += '<td style="padding:5px;"><input type="text" class=" form-text m-embed-additional-filter ide-hover" data-ide-properties-type="m-embed-additional-filter-formula" name="m-embed-add-filter"/></td>';
                            ret += '<td><i class="fa fa-plus child_fields fl-addRemoveItem"></i><i class="fa fa-minus fl-addRemoveItem" style="display:none;"></i></td>';
                        ret += '</tr>';
                    ret += '<tbody>';
                ret += '</table>';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    //end of multiple form html structure

    //columns html structure
    ret += '<div class="' + obj_float + ' properties_width_container">';
        ret += '<div class="fields_below">';
            ret += '<div class="label_below2">';
                ret += 'Select column to display:<br><span style="font-size:9px;">*Fill-up the field label of the fields you would like to appear in the column header.<br>*Drag the field name up or down to rearrange.</span>';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="'+ obj_float + ' properties_width_container section clearing fl-field-style embedMergedColumns">';
        ret += '<div class="fields_below">';
            ret += '<div>';
                ret += '<div class="column_properties">';
                    ret += '<div class="fields_below">';
                        ret += '<div class="input_position_below column div_1_of_1">';
                            ret += '<table style="width:99%;">';
                                ret += '<thead>';
                                    ret += '<tr>';
                                        ret += '<th style="padding:5px;width:20%;">Field Name: </th>';
                                        ret += '<th style="padding:5px;width:20%;">Column Caption: </th>';
                                        ret += '<th style="padding:5px;width:10%;">Column Width: </th>';
                                        ret += '<th style="padding:5px;width:25%;">Value: ';
                                            ret += '<div class="embed-column-default-value-type" style="float:right;">';
                                                ret += '<label>Static <input type="radio" name="embed_merged_column_value_type" value="static" id="embed_merged_column_value_type_static" class="css-checkbox"/><label for="embed_merged_column_value_type_static" class="css-label"></label></label>';
                                                ret += '<label> Computed <input type="radio" name="embed_merged_column_value_type" value="computed" id="embed_merged_column_value_type_computed" class="css-checkbox"/><label for="embed_merged_column_value_type_computed" class="css-label"></label></label>';
                                                // ret += '<label> Icons <input type="radio" name="embed_merged_column_value_type" value="icons" id="embed_merged_column_value_type_computed" class="css-checkbox"/><label for="embed_merged_column_value_type_computed" class="css-label"></label></label>';
                                           ret += '</div>';
                                        ret += '</th>';
                                        ret += '<th style="padding:5px;width:13%;">Value Replacer: </th>';
                                        //add for new columns property here
                                        ret += '<th style="width:10%;"></th>';
                                    ret += '</tr>';
                                ret += '</thead>';
                                ret += '<tbody>';
                                    ret += '<tr>';
                                        ret += '<td style="padding:5px;"><select class="column_config form-select" data-column-id="0" name="column_field_name"></select><input type="text" class="form-text embed-column-display-update" name="column_field_name_update" disabled style="display:none;"/></td>';
                                        ret += '<td style="padding:5px;"><input type="text" name="column_field_caption" class="form-text column_config"/></td>';
                                        ret += '<td style="padding:5px;"><input type="text" name="column_field_width" class="form-text column_config"/></td>';
                                        ret += '<td style="padding:5px;">';
                                            ret += '<input type="text" field-input-type="static" name="column_field_value" class="form-text column_config"/>';
                                                ret += '<div class="embed-multiple-icon-settings" style="display:none;">';
                                                ret += '<span>Select Icon: </span>';
                                                ret += '<div class="embed_newRequest embed-multiple-icon-button" style=" text-align: center; padding: 5px; cursor: pointer; width: 20px; margin-left: 5px; border-radius: 5px; position: relative; display: inline-block; ">';
                                                    ret += '<span style="color: white;">...</span>';
                                                    ret += '<div class="properties_width_container fl-field-style embed-icon-display-config" style=" position: absolute; width: 275px; height: 150px; z-index: 10; overflow: auto; cursor: default; display: none;">';
                                                        ret += '<div class="fields_below">';
                                                            ret += '<div class="column" style="width: 95%;">';
                                                                ret += '<table class="embed-multiple-icon-selection" style="width:99%;">';
                                                                    ret += '<tbody>';
                                                                        ret += '<tr><td style="border:none;"></td><td style="border:none;"><span class="embed_column_icons_plus_minus" style="float: right;"><i class="fa fa-plus click_me123" style="margin-right: 3px;cursor: pointer;"></i><i class="fa fa-minus" style="margin-right: 3px;cursor: pointer; display: none;"></i></span></td></tr>';
                                                                            ret += "<tr>";
                                                                            ret += '<td style="border:none;width:30%;">';
                                                                                ret += '<div class="embedded-view-columns-icon-pick" icon_type="column_icons" style="width:48px;">';
                                                                                    // ret += '<div style="position: absolute;top: 0;left: 0;">';
                                                                                    //     ret += '<span style="margin-top: 4px; margin-bottom:0px !important;">Set Icon: </span>';
                                                                                    //     ret += '<div class="embed_newRequest embed-column-values-icon" data-object-id="0" id="embed-column-values-icon" data-selected-icon="svg-column-values-icon-030" style="padding: 1px;height: 28px;width: 45px;left: 0px;border-radius: 3px;cursor: pointer;">';
                                                                                    //         ret += '<div class="embed_column_values_selected_icon" style="width: 20px;height: 20px;display:inline-block;padding: 4px;background-color: white;">';
                                                                                    //             ret += '<svg style="width: 100%;" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50">';

                                                                                    //              ret += '</svg>';
                                                                                    //          ret += '</div>';
                                                                                    //         ret += '<div class="embed_custom_action_selection" style="position: absolute;display: inline-block;height: 20px;padding: 4px;font-size: 12px;text-align: center;color: white;left:28px;">';
                                                                                    //            ret += '▼';
                                                                                    //         ret += '</div>';
                                                                                    //      ret += '</div>';
                                                                                    //  ret += '</div>';
                                                                                ret += '</div>';
                                                                            ret += '</td>';
                                                                            ret += '<td style="border:none;">';
                                                                                ret += '<textarea class="form-textarea ide-hover embed-column-icon-formula" data-ide-properties-type="m-embed-column-icon-formula" name="m-embed-column-icon-formula"></textarea>';
                                                                            ret += '</td>';
                                                                        ret += '</tr>';
                                                                    ret += '</tbody>';
                                                                ret += '</table>';
                                                            ret += '</div>';
                                                        ret += '</div>';
                                                    ret += '</div>';
                                                ret += '</div>';
                                            ret += '</div>';
                                        ret += '</td>';
                                        ret += '<td style="padding:5px;"><input type="text" name="column_field_replacer" class="form-text column_config"/></td>';
                                        ret += '<td style="padding:5px;">';
                                            ret += '<div class="embed-column-add">';
                                                ret += '<div style="text-align:center;" function-type="add" class="fl-addRemoveItem m-embed-add-column">Add</div>';
                                            ret += '</div>';
                                            ret += '<div class="embed-column-update" style="display:none;">';
                                                ret += '<div style="text-align:center;width: 21%;display: inline-block;margin-left: 5px;" function-type="save" class="fl-addRemoveItem m-embed-save-column">';
                                                    ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 50 50" style="width: 100%;"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-formbuilder-icon-002"></use></svg>';
                                                ret += '</div>';
                                                ret += '<div style="text-align:center;width: 21%;display: inline-block;margin-left: 5px;" function-type="cancel" class="fl-addRemoveItem m-embed-cancel-column">';
                                                    ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 50 50" style="width: 100%;"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-formbuilder-icon-003"></use></svg>';
                                                ret += '</div>';
                                            ret += '</div>';
                                        ret += '</td>';
                                    ret += '</tr>';
                                ret += '</tbody>';
                                // ret += ;
                                // ret += '<span style="margin-top:4px;"></span>';
                                // ret += '';
                                // ret += '<span style="margin-top:4px;"></span>';
                                // ret += '<input type="text" name="column_field_label" class="form-text "/>';
                                // ret += '<span style="margin-top:4px;">Width: </span>';
                                // ret += '';
                                // ret += '<span style="margin-top:4px;"   >Field Value: </span>';
                                // ret += '<label style="padding-left:5px;"><input type="radio" class="css-checkbox" id="staticValue" name="field_value" value="static"/><label for="staticValue" class="css-label"></label>Static</label>'
                                // ret += '<label style="padding-left:5px;"><input type="radio" class="css-checkbox" id="computedValue" name="field_value" value="computed"/><label for="computedValue" class="css-label"></label>Computed</label>'
                                // ret += '<label style="padding-left:5px;"><input type="radio" class="css-checkbox" id="iconsOnly" name="field_value" value="icons"/><label for="iconsOnly" class="css-label"></label>Icons</label>'
                                // ret += '<div class="column_computed_value" style="display:none;">';
                                //     ret += '<textarea class="form-textarea" data-ide-properties-type="embed-column-computed-value"></textarea>';
                                // ret += '</div>';
                                // ret += '<div class="column_icons_value" style="display:none;">';
                                //     // ret += '<textarea class="form-textarea" data-ide-properties-type="embed-column-computed-value"></textarea>';
                                // ret += '</div>';
                            // ret += '</div>';
                            ret += '</table>';
                        ret += '</div>'
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div class=>';
                ret += '<div class="column div_1_of_1">';
                    ret += '<span style="padding: 3px;">Column Preview: </span>';
                        ret += '<div class="input_position_below embed_column_preview" style="width:99%;  position: relative; overflow: auto">';
                            ret += '<table class="table_data" style="width:auto;">';
                                ret += '<thead>';
                                    ret += '<tr class="preview_column_th">';
                                    ret += '</tr>';
                                ret += '</thead>';
                                ret += '<tbody>';
                                    ret += '<tr class="preview_column_td">';
                                    ret += '</tr>';
                                ret += '</tbody>';
                            ret += '</table>';
                        ret += '</div>';
                ret += '</div>'
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    //end of columns html structure

    //other properties
    ret += '<div class="fl-floatLeft" style="width:100%;">';
        ret += '<div class="fl-floatLeft multipleEmbedLeftPanel fl-form-prop-cont-wid">';
            // ret += embedRowClick("", data_type);
            // ret += embedRoutingField("", data_type, "multiple");
            // ret += embedPopupDataSending("", data_type, "multiple");
            // ret += obj_hideModalCloseBtn("",data_type, "multiple");
            // ret += propEmbedSetColumnResult("",data_type, "multiple");
            // // ret += embed_allowImport("",data_type);
        ret += "</div>";
        ret += '<div class="fl-floatRight multipleEmbedRightPanel fl-form-prop-cont-wid">';
            // ret += highlightedRowProp("", data_type, "multiple");
            // ret += propAdvanceEmbedActionEventVisibility("", data_type, "multiple");
            // ret += embedDialogType("",data_type, "multiple");
        ret += "</div>";
    ret += '</div>';
    //end of other properties
    return ret;
}

function embedMultipleFormsSetup(DOI){
    if($('.embedMultipleProperties').length > 0){
        var element_data = $('body').data(""+DOI);
        var source_forms_html = $('.embed-source-form').html();
        var embedMultiplePropObject = $('.embedMultipleProperties');
        var columnsPropObject = $('.embedMergedColumns');
        var collected_columns = {};
        var str_collected_columns = "";
        var collected_columns_html = ["<option value='0'>--Select--</option>","<option value='system_icon'>(System Icon...)</option>"];
        MakePlusPlusRowV2({
            "tableElement":".embed-multiple-form-table>tbody:eq(0)",
            "rules":[
                {
                    "eleMinus":"tr:eq(0) > td > .fa.fa-minus.fl-addRemoveItem",
                    "eleMinusTarget":"tr:eq(0)",
                    "elePlus":"tr:eq(0) >  td > .fa.fa-plus.fl-addRemoveItem",
                    "elePlusTarget":"tr:eq(0)",
                    "elePlusTargetFn":function(a,b,c){
                        var self_ele = $(this);
                        if(b.prev_ele.find('.fa-minus').is(':visible') == false){
                            b.prev_ele.closest('tr').next().find('.fa.fa-minus').show();
                        }
                        // self_ele.find('tr:not(:eq(1)) > .fa.fa-minus.fl-addRemoveItem').show();
                        // self_ele.find('tr:not(:eq(1)) > .fl-embed-filter-group-and-or').show();
                    },
                    "eleMinusTargetFn":function(a,b){
                        var self_ele = $(this);
                        if(b.parent_ele.find('tr').length <= 1){
                            b.prev_ele.find('.fa.fa-minus').hide();
                        }
                        // self_ele.find('tr:not(:eq(1)) > .fa.fa-minus.fl-addRemoveItem').show();
                        // self_ele.find('tr:not(:eq(1)) > .fl-embed-filter-group-and-or').show();
                    }
                }
                // {
                //     "eleMinus":"tbody tr:eq(1) .fa.fa-minus.fl-addRemoveItem",
                //     "eleMinusTarget":"tr",
                //     "elePlus":"tbody tr:eq(1) .fa.fa-plus.fl-addRemoveItem",
                //     "elePlusTarget":"tr",
                //     "elePlusTargetFn":function(){
                //         var self_ele = $(this);
                //         self_ele.find('.fa.fa-minus.fl-addRemoveItem').show();
                //         self_ele.find('.fl-embed-filter-condition-and-or').show();
                //     }
                // }
            ]
        });
        MakePlusPlusRowV2({
            "tableElement":".embed-multiple-icon-selection",
            "rules":[
                {
                    "eleMinus":"tbody:eq(0) > tr:eq(0) > td:eq(1) > .embed_column_icons_plus_minus > .fa.fa-minus",
                    "eleMinusTarget":"tbody:eq(0)",
                    "elePlus":"tbody:eq(0) > tr:eq(0) >  td:eq(1) > .embed_column_icons_plus_minus > .fa.fa-plus",
                    "elePlusTarget":"tbody:eq(0)",
                    "elePlusTargetFn":function(a,b,c){
                        var self_ele = $(this);
                        console.log("eto...", a ,b, $(this));
                        if(self_ele.find('.fa-minus').is(':visible') == false){
                            self_ele.find('tr:eq(0)').find('.fa.fa-minus').show();
                            self_ele.find('.embed-column-values-icon').attr('data-object-id', Number(b.prev_ele.find('.embed-column-values-icon').attr('data-object-id')) + 1);
                            self_ele.find('.embed_column_values_selected_icon > svg').html("");
                            self_ele.find('.embed-column-icon-formula').val("");
                        }
                        // self_ele.find('tr:not(:eq(1)) > .fa.fa-minus.fl-addRemoveItem').show();
                        // self_ele.find('tr:not(:eq(1)) > .fl-embed-filter-group-and-or').show();
                    },
                    "eleMinusTargetFn":function(a,b){
                        var self_ele = $(this);
                        // self_ele.find('tr:not(:eq(1)) > .fa.fa-minus.fl-addRemoveItem').show();
                        // self_ele.find('tr:not(:eq(1)) > .fl-embed-filter-group-and-or').show();
                    }
                }
                // {
                //     "eleMinus":"tbody tr:eq(1) .fa.fa-minus.fl-addRemoveItem",
                //     "eleMinusTarget":"tr",
                //     "elePlus":"tbody tr:eq(1) .fa.fa-plus.fl-addRemoveItem",
                //     "elePlusTarget":"tr",
                //     "elePlusTargetFn":function(){
                //         var self_ele = $(this);
                //         self_ele.find('.fa.fa-minus.fl-addRemoveItem').show();
                //         self_ele.find('.fl-embed-filter-condition-and-or').show();
                //     }
                // }
            ]
        });
        $(".embedded-view-columns-icon-pick").iconPicker();

        embedMultiplePropObject.find('.m-embed-source-form').on('change', function(e, triggered_only){
            var option_html = "<option>--Select--</option>";
            var active_fields = [];
            if(typeof triggered_only == "undefined"){
                var active_fields = $(this).find('option:selected').attr('active-fields').split(",");
            }
            // var active_fields = $(this).find('option:selected').attr('active-fields')||"";
            // if(str_collected_columns != ""){
            //     var reg =  new RegExp(str_collected_columns.split(",").join("\\,|"), "g");
            //     active_fields = active_fields.replace(reg, "");
            //     str_collected_columns += active_fields;
            // }
            // else{
            //     str_collected_columns = active_fields;
            // }

            // collected_columns_html += str_collected_columns.split(",").join("</option><option source-form='" + source_form + "'value='" + active_fields[i] + "'>" + active_fields[i] + ">")
            var source_form = $(this).val();
            for(i in active_fields){
                option_html += "<option";
                    option_html += " value='" + active_fields[i] + "'";
                option_html += ">" + active_fields[i] + "</option>";
                if(typeof collected_columns[active_fields[i]] == "undefined"){
                    collected_columns[active_fields[i]] = {
                        "form_ids": source_form
                    }
                    collected_columns_html.push("<option source-form='" + source_form + "' value='" + active_fields[i] + "'>" + active_fields[i] + "</option>");
                    // collected_columns.push(active_fields[i]);
                }
                else{
                    if(collected_columns[active_fields[i]]["form_ids"].indexOf(source_form) < 0){
                        // var option_index = collected_columns_html.findIndex(function(element, index, array));
                        var option_index = collected_columns_html.findIndex(function(element, index, array){
                            if(element == "<option source-form='" + collected_columns[active_fields[i]]["form_ids"] + "' value='" + active_fields[i] + "'>" + active_fields[i] + "</option>"){
                                return index;
                            }
                            // console.log("check this out", element, index, array);
                        });
                        collected_columns[active_fields[i]]["form_ids"] += ", " + source_form;
                        collected_columns_html[option_index] = "<option source-form='" + collected_columns[active_fields[i]]["form_ids"] + "' value='" + active_fields[i] + "'>" + active_fields[i] + "</option>";
                    }
                }
            }
            console.log("triggered_only", triggered_only, typeof triggered_only == "undefined");
            if(typeof triggered_only == "undefined"){
                columnsPropObject.find('[name="column_field_name"]')[0].innerHTML = collected_columns_html.join("");
                $(this).closest('tr').find('.m-embed-lookup-field').html(option_html);
            }
            // collected_columns_html.splice(1,1);
            var old_column_value = collected_columns_html[1];
            collected_columns_html[1] = "";
            if($('.embedPopupDataSending_container').length > 0){
                console.log(collected_columns_html.join(""));
                $('.embedPopupDataSending_container').find('[name="epds-select-popup-form-field-side"]').html(collected_columns_html.join(""));
            }
            if($('.embedRoutingField_prop').length > 0){
                $('.embedRoutingField_prop').find('.erf-route-popup-form-fields').html(collected_columns_html.join(""));
                $('.embedRoutingField_container').find('.erf-route-popup-form-fields').val($('.getFields_' + DOI).attr('erf-route-popup-form-fields'));
            }
            if($('.embedPopupDataSending_container').length > 0){
                var data_send_json = JSON.parse(($('.getFields_' + DOI).attr('stringified-json-data-send')||"[]"));
                if(data_send_json.length > 0){
                    console.log("check this out..", data_send_json);
                    var data_sending_obj = $('.embedPopupDataSending_prop > .input_position_below > div > .epds-data-send-collection');
                    data_sending_obj.find('tbody:not(:eq(0))').remove();
                    for(var i in data_send_json){
                        var tbody = data_sending_obj.find('tbody:eq(' + i + ')');
                        var plus_plus = tbody.find('tr:eq(0) > td > div > .epds-add-minus-row > .fa-plus');
                        console.log("asdasdasdasdasd",$('.embedPopupDataSending_container').find('[name="epds-select-popup-form-field-side"]').html());
                        tbody.find('tr:eq(1) > td:eq(0) > .epds-my-form-side-chk-static-selected > select').val(data_send_json[i]['epds_select_my_form_field_side']);
                        tbody.find('tr:eq(1) > td:eq(1) > .epds-popup-form-side-chk-static-selected > select > option[value="' + data_send_json[i]['epds_select_popup_form_field_side'] + '"]').prop('selected', true);
                        plus_plus.trigger('click');
                    }
                    data_sending_obj.find('tbody').last().find('.fa-minus').trigger('click');
                }
            }
            // console.log("str_collected_columns", collected_columns_html);
            collected_columns_html[1] = old_column_value;
        });
        
        columnsPropObject.find('[name="column_field_name"]').on('change', function(e){
            if($(this).val() == "system_icon"){// condition for icon column_configuration
                $('.embed-column-default-value-type').css('display', 'none');
                $('.embed-column-default-value-type').parent().css('width', '35px');
                $(this).closest('tr').find(' td > [name="column_field_value"]').css('display', 'none');
                $(this).closest('tr').find(' td > .embed-multiple-icon-settings').css('display', '');
            }
            else{
                $('.embed-column-default-value-type').css('display', '');
                $('.embed-column-default-value-type').parent().css('width', '25%');
                $(this).closest('tr').find(' td > [name="column_field_value"]').css('display', '');
                $(this).closest('tr').find(' td > .embed-multiple-icon-settings').css('display', 'none');
            }
        });

        columnsPropObject.find('[name="field_value"]').on('change', function(e){
            // alert("changed");
            var value = $('[name="field_value"]:checked').val();
            if(value == "computed"){
                columnsPropObject.find('.column_computed_value').show();
                columnsPropObject.find('.column_icons_value').hide();
            }
            else if(value == "icons"){
                columnsPropObject.find('.column_icons_value').show();
                columnsPropObject.find('.column_computed_value').hide();
            }
            else{
                columnsPropObject.find('.column_computed_value').hide();
                columnsPropObject.find('.column_icons_value').hide(); 
            }
        });

        columnsPropObject.find('.m-embed-add-column').on('click', function(e){
            var trline = $(this).closest('tr');
            var column_json = {};
            var error_ctr = 0;
            console.log("trlines", trline);
            trline.find('.column_config').each(function(){
                console.log("this", $(this));
                if($(this).attr('name') == "column_field_value"){
                    if(trline.parents('table').eq(0).find('[name="embed_merged_column_value_type"]:checked').val() == "computed"){
                        if($(this).val() == ""){
                            error_ctr++;
                            console.log("error in ", $(this).attr('name'));
                        }
                    }
                }
                else if($(this).val() == "0" || $(this).val() == "--Select--"){
                    error_ctr++;
                    console.log("error in ", $(this).attr('name'));
                }
                
            });
            if(trline.parents('table').eq(0).find('[name="embed_merged_column_value_type"]:checked').val() == "static" && trline.parents('table').eq(0).find('[name="column_field_name"]').val() == "0"){
                error_ctr++;
            }
            if(error_ctr > 0){
                showNotification({
                    message: "Please Fill up all the required field",
                    type: "error",
                    autoClose: true,
                    duration: 3 
                });
                return;
            }
            if(typeof element_data.column_settings != "undefined"){
                column_json['column_field_name'] = trline.find('[name="column_field_name"]').val();
                column_json['column_field_caption'] = trline.find('[name="column_field_caption"]').val();
                column_json['column_field_width'] = trline.find('[name="column_field_width"]').val();
                column_json['column_field_replacer'] = trline.find('[name="column_field_replacer"]').val()||"---";
                column_json['source_form'] = trline.find('[name="column_field_name"] option:selected').attr('source-form');
                var field_value_type = trline.parents('table').eq(0).find('[name="embed_merged_column_value_type"]:checked').val();
                if(trline.find('[name="column_field_name"]').val() == "system_icon"){
                    column_json['column_field_icon_formulas'] = trline.find('.embed-multiple-icon-selection > tbody').map(function(val){
                        var return_json = {};
                        return_json['icon_selected_id'] = $(this).find('.embedded-view-columns-icon-pick').getSVGVal();
                        return_json['icon_selected_formula'] = $(this).find('.embed-column-icon-formula').val();
                        return return_json;
                    }).get();
                }
                else{
                    if(field_value_type == "computed"){
                        column_json['field_value'] = {
                            field_type: "computed",
                            field_value: trline.find('[name="column_field_value"]').val()
                        }

                    }
                    else{
                        column_json['field_value'] = {
                            field_type: "static",
                            field_value: trline.find('[name="column_field_value"]').val()
                        }

                    }
                }
                column_json['field_dropdown_reference_index'] = trline.find('[name="column_field_name"] option[value="' + column_json['column_field_name'] + '"]').next().index();
                element_data.column_settings.push(column_json);
                loadPreviewColumn(column_json, columnsPropObject.find('.preview_column_th'), columnsPropObject.find('.preview_column_td'));
            }
            else{
                element_data.column_settings = [];
                column_json['column_field_name'] = trline.find('[name="column_field_name"]').val();
                column_json['column_field_caption'] = trline.find('[name="column_field_caption"]').val();
                column_json['column_field_width'] = trline.find('[name="column_field_width"]').val();
                column_json['column_field_replacer'] = trline.find('[name="column_field_replacer"]').val()||"---";
                column_json['source_form'] = trline.find('[name="column_field_name"] option:selected').attr('source-form');
                var field_value_type = trline.parents('table').eq(0).find('[name="embed_merged_column_value_type"]:checked').val();
                if(trline.find('[name="column_field_name"]').val() == "system_icon"){
                    column_json['column_field_icon_formulas'] = trline.find('.embed-multiple-icon-selection > tbody').map(function(val){
                        var return_json = {};
                        return_json['icon_selected_id'] = $(this).find('.embedded-view-columns-icon-pick').getSVGVal();
                        return_json['icon_selected_formula'] = $(this).find('.embed-column-icon-formula').val();
                        return return_json;
                    }).get();
                }
                else{
                    if(field_value_type == "computed"){
                        column_json['field_value'] = {
                            field_type: "computed",
                            field_value: trline.find('[name="column_field_value"]').val()
                        }

                    }
                    else{
                        column_json['field_value'] = {
                            field_type: "static",
                            field_value: trline.find('[name="column_field_value"]').val()
                        }

                    }
                }
                column_json['field_dropdown_reference_index'] = trline.find('[name="column_field_name"] option[value="' + column_json['column_field_name'] + '"]').next().index();
                element_data.column_settings.push(column_json);
                loadPreviewColumn(column_json, columnsPropObject.find('.preview_column_th'), columnsPropObject.find('.preview_column_td'));
            }
            console.log("find this...", '[name="column_field_name"] option[value="' + column_json['column_field_name'] + '"]');
            if(trline.find('[name="column_field_name"]').val() != 'system_icon' && trline.find('[name="column_field_name"]').val() != "0"){
                trline.find('[name="column_field_name"] option[value="' + column_json['column_field_name'] + '"]').remove();
            }
            trline.find('.embed-multiple-icon-settings').hide();
            trline.find('[name="column_field_name"] option:eq(0)').prop('selected', true);
            trline.find('[name="column_field_caption"]').val("");
            trline.find('[name="column_field_width"]').val("");
            trline.find('[name="column_field_value"]').val("");
            trline.find('[name="column_field_replacer"]').val("");
            trline.parents('table').eq(0).find('#embed_merged_column_value_type_static').prop('checked', true);
            columnsPropObject.find('.embed-multiple-icon-selection > tbody:not(:eq(0))').remove();
        });
        columnsPropObject.find('.m-embed-save-column').on('click', function(e){
            var self = $(this);
            var trline = $(this).closest('tr');
            // var column_field_name = $($(this).data('active_column'));
            var column_field_name = $('.m_embed_columnHeader:eq(' + self.data('column_index') + ')');
            var column_modified = columnsPropObject.find('.preview_column_th > [data-column-name="' + ($(this).attr('active-column')||"") + '"]');
            element_data.column_settings.map(function(element, index){
                console.log(element['column_field_name'], column_field_name.attr('data-column-name'));
                if(element['column_field_name'] == column_field_name.attr('data-column-name')){
                    element['column_field_name'] = trline.find('[name="column_field_name"]').val();
                    element['column_field_caption'] = trline.find('[name="column_field_caption"]').val();
                    element['column_field_width'] = trline.find('[name="column_field_width"]').val();
                    element['column_field_replacer'] = trline.find('[name="column_field_replacer"]').val()||"---";
                    element['source_form'] = trline.find('[name="column_field_name"] option:selected').attr('source-form');
                    var field_value_type = trline.parents('table').eq(0).find('[name="embed_merged_column_value_type"]:checked').val();
                    if(trline.find('[name="column_field_name"]').val() == "system_icon"){
                        element['column_field_icon_formulas'] = trline.find('.embed-multiple-icon-selection > tbody').map(function(val){
                            var return_json = {};
                            return_json['icon_selected_id'] = $(this).find('.embedded-view-columns-icon-pick').getSVGVal();
                            return_json['icon_selected_formula'] = $(this).find('.embed-column-icon-formula').val();
                            return return_json;
                        }).get();
                    }
                    else{
                        if(field_value_type == "computed"){
                            element['field_value'] = {
                                field_type: "computed",
                                field_value: trline.find('[name="column_field_value"]').val()
                            }

                        }
                        else{
                            element['field_value'] = {
                                field_type: "static",
                                field_value: trline.find('[name="column_field_value"]').val()
                            }

                        }
                    }
                    var active_column = $(self.data('active_column'));
                    active_column.attr('data-column-name', element['column_field_name']||"");
                    active_column.attr('data-source-form', element['source_form']||"");
                    active_column.attr('data-column-caption', element['column_field_caption']||"");
                    active_column.attr('data-column-width', element['column_field_width']||"");
                    active_column.attr('data-dropdown-index', element['field_dropdown_reference_index']||"");
                    active_column.attr('data-column-value-data', JSON.stringify(element['field_value']||""));
                    active_column.attr('data-column-replacer', element['column_field_replacer']||"");
                    active_column.attr('data-column-icons-formula', JSON.stringify(element['column_field_icon_formulas']||""));
                    active_column.children('span').text(element['column_field_caption']);
                    columnsPropObject.find('.embed-multiple-icon-selection > tbody:not(:eq(0))').remove();
                    // loadPreviewColumn(element, columnsPropObject.find('.preview_column_th'), columnsPropObject.find('.preview_column_td'), true, active_column);
                }
            })
            
            columnsPropObject.find('.embed-column-add').show();
            columnsPropObject.find('[name="column_field_name"]').show();
            columnsPropObject.find('.embed-column-update').hide();
            columnsPropObject.find('[name="column_field_name_update"]').hide();
            trline.find('.embed-multiple-icon-settings').hide();
            trline.find('[name="column_field_name"] option:eq(0)').prop('selected', true);
            trline.find('[name="column_field_caption"]').val("");
            trline.find('[name="column_field_width"]').val("");
            trline.find('[name="column_field_value"]').val("");
            trline.find('[name="column_field_replacer"]').val("");
            trline.parents('table').eq(0).find('#embed_merged_column_value_type_static').prop('checked', true);
            self.removeData('active_column');

        });
        columnsPropObject.find('.m-embed-cancel-column').on('click', function(e){
            var self = $(this);
            var trline = $(this).closest('tr');
            self.parent().children('.m-embed-save-column').removeAttr('active-column');
            trline.find('m-embed-column-add').show();
            trline.find('[name="column_field_name"]').show();
            columnsPropObject.find('[name="column_field_name_update"]').hide();
            trline.find('m-embed-column-update').hide();
            if(trline.find('[name="column_field_name"]').val() != 'system_icon' && trline.find('[name="column_field_name"]').val() != "0"){
                trline.find('[name="column_field_name"] option[value="' + column_json['column_field_name'] + '"]').remove();
            }
            trline.find('.embed-multiple-icon-settings').hide();
            trline.find('[name="column_field_name"] option:eq(0)').prop('selected', true);
            trline.find('[name="column_field_caption"]').val("");
            trline.find('[name="column_field_width"]').val("");
            trline.find('[name="column_field_value"]').val("");
            trline.find('[name="column_field_replacer"]').val("");
            trline.parents('table').eq(0).find('#embed_merged_column_value_type_static').prop('checked', true);

        })
        columnsPropObject.find('.embed-multiple-icon-settings').on('mousedown', function(e){
            var self = $(this);
            var dialog = self.find('.embed-icon-display-config');
            // console.log("samalaymalaykum", e.target, $(e.target).length > 0, e.target != dialog[0], $(e.target).parents('.embed-icon-display-config'));
            if($(e.target).length > 0){
                if(e.target != dialog[0] && $(e.target).parents('.embed-icon-display-config').length <= 0){
                    if(dialog.is(':visible')){
                        console.log("visible");
                        dialog.css('display','none');
                    }
                    else{
                        dialog.css('display', '');
                    }
                }
            }
        });

        columnsPropObject.on('click', '.mRemoveColumn', function(e){
            var th = $(this).closest('th');
            var td = $(this).closest('table').find('.preview_column_td').children().eq(th.index());
            var dropdown_index = Number(th.attr('data-dropdown-index')||0);
            var column_field_name = th.attr('data-column-name')||"";
            var column_source_form = th.attr('data-source-form')||"";
            var column_data_in_option = "<option source-form='" + column_source_form + "' value='" + column_field_name + "'>" + column_field_name + "</option>";
            console.log(element_data.column_settings);
            for(i in element_data.column_settings){
                console.log("checking...", element_data.column_settings[i]['column_field_name'], column_field_name);
                if(element_data.column_settings[i]['column_field_name'] == column_field_name){
                    element_data.column_settings[i] = undefined;
                    var new_element_data = JSON.stringify(element_data.column_settings);
                    element_data.column_settings = JSON.parse(new_element_data).filter(Boolean);
                    break;
                }
            }
            if(th.attr('data-column-name') != "system_icon"){
                $(column_data_in_option).insertBefore(columnsPropObject.find('[name="column_field_name"] option:eq(' + dropdown_index + ')'));
            }
            th.remove();
            td.remove();
        });
        
        columnsPropObject.on('change', '[name="embed_merged_column_value_type"]:checked', function(e){
            var value_type = $(this).val();
            var field = columnsPropObject.find('[name="column_field_value"]');
            if(value_type == "computed"){
                field.addClass('ide-hover');
                field.attr('data-ide-properties-type', 'embed-merged-column-field-value-type');
            }
            else{
                field.val("");
                field.removeClass('ide-hover');
                field.removeAttr('data-ide-properties-type');
                field.removeAttr('readonly');
            }
        });

        var parent_fields = $.unique($('.setObject').find('.getFields').map(function(){ return $(this).attr('name')})).get();
        var search_field_html = "<option>--Select--</option>";
        for(i in parent_fields){
            search_field_html += "<option";
                search_field_html += " value='" + parent_fields[i] + "'";
            search_field_html += ">" + parent_fields[i] + "</option>";
        }
        embedMultiplePropObject.find('.m-embed-search-field').html(search_field_html);
        if(typeof element_data.multiple_forms != "undefined"){
            // AsyncLoop(element_data.multiple_forms, function(a, b){
            //     console.log("o heay", a, b);
            //     var row = embedMultiplePropObject.find('tbody > tr:eq(' + a + ')');
            //     row.find('td > .m-embed-source-form option[value="' + b['source_form'] + '"]').prop('selected', true);
            //     row.find('td > .m-embed-source-form option').trigger('change');

            //     row.find('td > .m-embed-lookup-field option[value="' + b['lookup_field'] + '"]').prop('selected', true);
            //     row.find('td > .m-embed-search-field option[value="' + b['search_field'] + '"]').prop('selected', true);
            //     row.find('td > .m-embed-operator option[value="' + b['lookup_operator'] + '"]').prop('selected', true);
            //     row.find('td > .m-embed-additional-filter').val(b['advance_filter_formula']);
            //     row.find('td > .fa-plus').trigger('click');
            // }, function(){
            //     embedMultiplePropObject.find('tbody > tr').last().find('.fa-minus').trigger('click');
            // });
            // var row = embedMultiplePropObject.find('tbody > tr:eq(0)');
            for(j in element_data.multiple_forms){
                row = embedMultiplePropObject.find('tbody > tr:eq(' + j + ')');
                row.find('td > .m-embed-source-form option[value="' + element_data.multiple_forms[j]['source_form'] + '"]').prop('selected', true);
                row.find('td > .m-embed-source-form').trigger('change');

                row.find('td > .m-embed-lookup-field option[value="' + element_data.multiple_forms[j]['lookup_field'] + '"]').prop('selected', true);
                row.find('td > .m-embed-search-field option[value="' + element_data.multiple_forms[j]['search_field'] + '"]').prop('selected', true);
                row.find('td > .m-embed-operator option[value="' + element_data.multiple_forms[j]['lookup_operator'] + '"]').prop('selected', true);
                row.find('td > .m-embed-additional-filter').val(element_data.multiple_forms[j]['advance_filter_formula']);
                row.find('td > .fa-plus').trigger('click');
            }
            // console.log("asd123456",embedMultiplePropObject.find('tbody > tr').last().find('.fa-minus'));
            embedMultiplePropObject.find('tbody > tr').last().find('.fa-minus').trigger('click');
            console.log("collected", collected_columns);
        }
        columnsPropObject.find('[name="column_field_name"]').html(collected_columns_html);
        // collected_columns_html.splice(1, 1);
        // if($('.embedPopupDataSending_container').length > 0){
        //     $('.embedPopupDataSending_container').find('[name="epds-select-popup-form-field-side"]').html(collected_columns_html.join(""));
        // }
        // if($('.embedRoutingField_prop').length > 0){
        //     $('.embedRoutingField_prop').find('.erf-route-popup-form-fields').html(collected_columns_html.join(""));
        // }
        if(element_data.column_settings != "undefined"){
            for(k in element_data.column_settings){
                var column_data = element_data.column_settings[k];
                loadPreviewColumn(column_data, columnsPropObject.find('.preview_column_th'), columnsPropObject.find('.preview_column_td'));
                if(column_data['column_field_name'] != "system_icon" && column_data['column_field_name'] != "0"){ // 0 means formula type
                    columnsPropObject.find('[name="column_field_name"] option[value="' + column_data['column_field_name'] + '"]').remove();
                }
            }
        }
        columnsPropObject.find('.m_embed_columnHeader').on('click', function(e){
            var self = $(this);
            console.log(e.target);
            if(!$(e.target).hasClass('icon-remove')){
                if(self.attr('data-column-name') != "system_icon"){
                    var value = JSON.parse(self.attr('data-column-value-data')||"");
                    columnsPropObject.find('[name="column_field_name_update"]').show();
                    columnsPropObject.find('[name="column_field_name"]').hide();
                    columnsPropObject.find('[name="column_field_name_update"]').val(self.attr('data-column-name')).trigger('change');;
                    columnsPropObject.find('[name="column_field_caption"]').val(self.attr('data-column-caption'));
                    columnsPropObject.find('[name="column_field_width"]').val(self.attr('data-column-width'));
                    columnsPropObject.find('[name="embed_merged_column_value_type"][value="' + value['field_type'] + '"]').prop('checked', true);
                    columnsPropObject.find('[name="column_field_value"]').val(value['field_value']);
                    columnsPropObject.find('[name="column_field_replacer"]').val(self.attr('data-column-replacer'));

                }
                else{
                    var value = JSON.parse(self.attr('data-column-value-data')||"");
                    columnsPropObject.find('[name="column_field_name"]').show();
                    columnsPropObject.find('[name="column_field_name_update"]').hide();
                    columnsPropObject.find('[name="column_field_name"]').val(self.attr('data-column-name')).trigger('change');;
                    columnsPropObject.find('[name="column_field_caption"]').val(self.attr('data-column-caption'));
                    columnsPropObject.find('[name="column_field_width"]').val(self.attr('data-column-width'));
                    columnsPropObject.find('[name="embed_merged_column_value_type"][value="' + value['field_type'] + '"]').prop('checked', true);
                    columnsPropObject.find('[name="column_field_value"]').val(value['field_value'])
                    columnsPropObject.find('[name="column_field_replacer"]').val(self.attr('data-column-replacer'));
                    var json_icon_formula = JSON.parse(self.attr('data-column-icons-formula'));
                    for(i in json_icon_formula){
                        var tbody = columnsPropObject.find('.embed-multiple-icon-selection > tbody:eq(' + i + ')');
                        console.log("solid:::");
                        var field_icon_picker = tbody.find('tr:eq(1) > td:eq(0) > .embedded-view-columns-icon-pick');
                        var field_icon_formula = tbody.find('tr:eq(1) > td:eq(1) > .embed-column-icon-formula');
                        var plus_clicker = tbody.find('tr:eq(0) > td:eq(1) > span > .fa-plus');
                        field_icon_picker.find('.embedded-view-icon-picker-container > .embed-custom-actions-icon').attr('data-selected-icon', json_icon_formula[i]['icon_selected_id']);
                        field_icon_picker.setSVGVal(json_icon_formula[i]['icon_selected_id']);
                        field_icon_formula.val(json_icon_formula[i]['icon_selected_formula']);
                        plus_clicker.trigger('click');
                    }
                    columnsPropObject.find('.embed-multiple-icon-selection > tbody').last().find('.fa-minus').trigger('click');
                }
                columnsPropObject.find('.embed-column-add').hide();
                columnsPropObject.find('.m-embed-save-column').data('active_column', self);
                columnsPropObject.find('.m-embed-save-column').data('column_index', $('.m_embed_columnHeader').index(self));
                columnsPropObject.find('.embed-column-update').show();
                // columnsPropObject.find('.m-embed-add-column').text('Save');
            }
        })
        // $('.embed_column_preview').perfectScrollbar();
        $('.embed-icon-display-config').perfectScrollbar();
        // embedMultiplePropObject.find('.m-embed-lookup-field').html("<option>--Select--</option>");
        //restoring the properties
        // console.log("elementssssss data", element_data);
    }
}

function loadPreviewColumn(column_data, thead_selector, tbody_selector, is_update, active_column){
    var th = "";
    var td = "";
    console.log(column_data, thead_selector, tbody_selector, is_update, active_column);
    var icon_formulas = htmlEntities(JSON.stringify((column_data['column_field_icon_formulas']||"")));
    th += '<th class="m_embed_columnHeader" data-column-name="' + (column_data['column_field_name']||"") + '" data-source-form="' + (column_data['source_form']||"") + '" data-column-caption="' + (column_data['column_field_caption']||"") + '" data-column-width="' + (column_data['column_field_width']||"100px") + '" data-dropdown-index="' + (column_data['field_dropdown_reference_index']||0) + '" data-column-value-data="' + htmlEntities(JSON.stringify(column_data['field_value']||"")) + '" data-column-replacer="' + (column_data['column_field_replacer']||"---") + '" data-column-icons-formula="' + icon_formulas + '" style="padding:5px; /*width:' + column_data['column_field_width'] + ';*/"><span>' + column_data['column_field_caption'] + '</span><i class="fa fa-times icon-remove cursor fl-floatRight mRemoveColumn"></i></th>';
    td += "<td><div style='width:" + column_data['column_field_width'] + "; height: 20px;'></div></td>";
    if(typeof is_update != "undefined" && is_update == true){
        alert("update");
        if(typeof active_column != "undefined"){
            active_column.replaceWith(th);
        }
    }
    else{
        $(thead_selector).append(th);   
        $(tbody_selector).append(td);
    }
}

function saveMultipleFormProperties(object_id){
    var object_data = $('body').data(""+object_id);
    object_data.multiple_forms = [];
    var form_sources = $('.m-embed-pc');
    form_sources.each(function(index){
        var self = $(this);
        var form_prop_json = {};
        form_prop_json['source_form'] = self.find('.m-embed-source-form').val()||"";
        form_prop_json['lookup_field'] = self.find('.m-embed-lookup-field').val()||"";
        form_prop_json['lookup_operator'] = self.find('.m-embed-operator').val()||"";
        form_prop_json['search_field'] = self.find('.m-embed-search-field').val()||"";
        form_prop_json['advance_filter_formula'] = self.find('.m-embed-additional-filter').val()||"";
        form_prop_json['source_form_name'] = self.find('.m-embed-source-form option:selected').text();
        object_data.multiple_forms.push(form_prop_json);
    })
}

function saveCustomActions(object_id){
    var obj_data = $('body').data("" +object_id);
    var custom_actions_sources = $('.embed-custom-actions-table tbody');
    obj_data.embed_custom_actions = [];
    custom_actions_sources.each(function(index){
        var self = $(this);
        var custom_action_json = {};
        custom_action_json['custom_action_type'] = self.find('tr:eq(1) > td > .select_custom_action').val();
        custom_action_json['custom_action_label'] = self.find('tr:eq(1) > td > div > div > div > input[type="text"][name="embed_custom_actions_label"]').val();
        custom_action_json['custom_action_formula'] = self.find('tr:eq(1) > td > div > .embed_custom_action_formula').val();
        custom_action_json['custom_action_icon_id'] = self.find('.embedded-view-custom-actions-icon-pick').getSVGVal();
        obj_data.embed_custom_actions.push(custom_action_json);
    });
}

function shuffleProperties(command){
    for(i in command){
        var classname = $('.' + i);
        for(j in command[i]){
            // if(classname.find(command[i][j]).length <= 0){
                $(command[i][j]).appendTo(classname);
                // classname.append($(command[i][j]));
            // }
        }
    }
}


(function($){
    $.fn.iconPicker = function(){
        var self = $(this);
        self.data('svg-icon-field', true);
        self.initContent("Select Icon");
        self.loadIcons();
        self.bindPicker();
    }
    $.fn.initContent = function(icon_name){
        var self = $(this);
        var ret = "";
        ret += '<div class="embedded-view-icon-picker-container">';
            ret += '<span style="margin-top: 4px; margin-bottom:0px !important;">Set Icon: </span>';
            ret += '<div class="embed_newRequest embed-custom-actions-icon" data-object-id="0" id="embed-custom-actions-icon" data-selected-icon="svg-custom-actions-icon-004" style="padding: 1px;height: 28px;border-radius: 3px;cursor: pointer;">';
                ret += '<div style="width: 20px;height: 20px;display:inline-block;padding: 4px;background-color: white;">';
                    ret += '<svg style="" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50">';
                        ret += '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-custom-actions-icon-000"></use>';
                    ret += '</svg>';
                ret += '</div>';
            ret += '<div class="embed_custom_action_selection" style="display: inline-block;height: 20px;padding: 3px;font-size: 12px;text-align: center;color: white;width: 10px;position: relative;">';
                ret += '<div style="position: absolute;">▼</div>';
            ret += '</div>';
        ret += '</div>';
        self.html(ret);
    }
    $.fn.loadIcons = function(){
        //load all the icons
        var self = $(this);
        var ret = "";
        var icon_counter = "";
        switch(self.attr('icon_type')){
            case "custom_actions":
                for(i = 0; i < $('.fl-main-container .svg-custom-actions-icon').length; i++){
                    if(i < 10)
                        icon_counter = "00" + i;
                    else if(i < 100)
                        icon_counter = "0" + i;
                    else
                        icon_counter = i;
                    ret += '<div class="embed_icon_click" style="width:20px;display:inline-block;padding:2px; cursor:pointer;">'
                        ret += '<svg style="width: 100%;" id="icon-svg-custom-action-option" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50">';
                            ret += '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-custom-actions-icon-' + icon_counter + '"></use>';
                        ret += '</svg>';
                    ret += '</div>';
                }
                break;
            case "column_icons":
                for(i = 0; i < $('.fl-main-container .svg-column-icons').length; i++){
                    if(i < 10)
                        icon_counter = "00" + i;
                    else if(i < 100)
                        icon_counter = "0" + i;
                    else
                        icon_counter = i;
                    ret += '<div class="embed_icon_click" style="width:20px;display:inline-block;padding:2px; cursor:pointer;">'
                        ret += '<svg style="width: 100%;" id="icon-svg-custom-action-option" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50">';
                            ret += '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-column-icons-' + icon_counter + '"></use>';
                        ret += '</svg>';
                    ret += '</div>';
                }
                break;
        }
        self.data("svg_list", ret);
    }
    $.fn.bindPicker = function(){
        var self = $(this);
        self.on('click', function(e){
            var customActions = $(this).data('svg_list');
            var top = $(this).offset()['top'] + $(this).outerHeight();
            var left = $(this).offset()['left'];
            var icon_container = $('<div id="embed_icon_list" class="embed_newRequest" style="width:250px;height:150px;overflow:auto;position:absolute;background-color:black;top: ' + top + 'px;left: ' + left + 'px;z-index: 99999999999;">' + customActions + '</div>');
            if($('body').find('.embed_icon_list').length > 0){
                $('body').find('.embed_icon_list').remove();
            }
            $('body').append(icon_container);
            icon_container.data('target_element', $(this));
            icon_container.perfectScrollbar();
            //
            icon_container.children('.embed_icon_click').on('click', function(e){
                var selected_icon = $(this);
                var get_icon_name = selected_icon.find('svg > use').attr('xlink:href');
                icon_container.data('target_element').setSVGVal(get_icon_name.replace('#', ''));
                //.find('svg').html('<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' + get_icon_name + '"></use>');
                // self.data('svg_icon_value', get_icon_name.replace('#', ''));
                icon_container.remove();
            });
            //remove icon_list_container when loses focus
            if($('body').data('events')['click'].filter(function(elem, index){return elem.namespace == "embed_icon_clicks";}).length <= 0){
                $('body').on('mousedown.embed_icon_clicks', function(e){
                    var target = $(e.target);
                    console.log("target shooting",target);
                    if($('#embed_icon_list').length > 0){
                        if(target.parents('.embed_icon_click').length <= 0){
                            $('#embed_icon_list').remove();
                        }                        
                    }
                });
            }
        });
    }
    $.fn.getSVGVal = function(){
        var self = $(this);
        if(self.data('svg_icon_field')||false){
            return;
        }
        else{
            return self.data('svg_icon_value')||"";
        }
    }
    $.fn.setSVGVal = function(svg_value){
        var self = $(this);
        self.find('svg').html('<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' + svg_value + '"></use>');
        self.data('svg_icon_value', svg_value);
    }
})(jQuery);