var jsonFormCategoryData = {
    search_value: "",   
    "column-sort": "",
    "column-sort-type": "", 
    limit: 10,
}
var columnSort = [];
var originalFormCategoryProperty;
$(document).ready(function(){
	var pathname = window.location.pathname;
	FormCategory.init();
	UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".categoryUsersContainer");
	UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".categoryAdminContainer");

	$("body").on("keyup","#txtSearchFormCategoryDatatable",function(e){
		if (e.keyCode == "13") {
	        $("#loadFormCategory").dataTable().fnDestroy();
	        jsonFormCategoryData['search_value'] = $(this).val();
	        FormCategory.triggerDataTable();
	    }
    })
	$("body").on("click","#btnSearchFormCategoryDatatable",function(e){
	        $("#loadFormCategory").dataTable().fnDestroy();
	        jsonFormCategoryData['search_value'] = $("#txtSearchFormCategoryDatatable").val();
	        FormCategory.triggerDataTable();
    })


	$("body").on("click",".addCategoryModal",function(){
		ui.block();
    	var json = {};
    	json['title'] = "<svg class='icon-svg icon-svg-modal' viewBox='0 0 100.264 100.597'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-user-settings'></use></svg> Add Form Category";
    	json['value'] = '';
      json['description'] = '';
    	json['button'] = '<input type="button" class="btn-blueBtn addCategory fl-margin-right" value="Save">';
    	// json['admin'] = $(".admin_"+category_id).text();
    	var jsonAdmin = "";
    	var admin = {};
    	if($(".users-admin").val!=""){
    		jsonAdmin = JSON.parse($(".users-admin").text());
    		admin['users'] = [];
    		for(var i in jsonAdmin){
    			admin['users'].push(jsonAdmin[i]['id']);
    		}
    	}
    	json['admin'] = JSON.stringify(admin);
    	FormCategory.categoryModal(json);
    })
    $("body").on("click",".updateformCategory",function(){
    	ui.block();
    	var json = {};
    	var category_id = $(this).attr("data-id");
    	json['title'] = "<svg class='icon-svg icon-svg-modal' viewBox='0 0 100.264 100.597'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-user-settings'></use></svg> Edit Form Category";
    	json['value'] = $(this).attr("data-name");
      json['description'] = $(this).attr("data-description");
    	json['users'] = $(".users_"+category_id).text();
    	json['admin'] = $(".admin_"+category_id).text();
    	json['image'] = $(".image_"+category_id).text();
    	json['button'] = '<input type="button" data-id="'+ category_id +'" class="btn-blueBtn editCategory fl-margin-right" value="Save">';


    	//original
    	try{
			UserAccessFlagCheckBox.originalChecked['users'] = $(".users_"+category_id).text();
			UserAccessFlagCheckBox.originalChecked['admin'] = $(".admin_"+category_id).text();
		}catch(err){
			console.log(err)
			UserAccessFlagCheckBox.originalChecked['users'] = {}
			UserAccessFlagCheckBox.originalChecked['admin'] = {}
		}
		UserAccessFlagCheckBox.checkedEle = {};
		UserAccessFlagCheckBox.unCheckedEle = {};
		originalFormCategoryProperty = json;
    	FormCategory.categoryModal(json);

    })
    $("body").on("click","#loadFormCategory th",function(){
        var cursor = $(this).css("cursor");
        jsonFormCategoryData['column-sort'] = $(this).attr("field_name");
        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest("#loadFormCategory").find(".sortable-image").html("");
        if(indexcolumnSort==-1){
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonFormCategoryData['column-sort-type'] = "ASC";
        }else{
            columnSort.splice(indexcolumnSort,1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonFormCategoryData['column-sort-type'] = "DESC";
        }
        
        $("#loadFormCategory").dataTable().fnDestroy();
        var thisTh = this;
        FormCategory.triggerDataTable(function(){
        	addIndexClassOnSort(thisTh);
        });
    })
	
	$(".searchFormCategoryLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonFormCategoryData['limit'] = val;
            $("#loadFormCategory").dataTable().fnDestroy();
            // var self = this;
            var oTable = FormCategory.triggerDataTable();
        // }
    })
})
FormCategory = {
	init : function(){
		this.deleteformCategory();
		this.addCategory();
		this.editCategory();
		this.triggerDataTable();
	},
	triggerDataTable : function(callback){
		if (pathname=="/form_category") {
		    this.load();
		    
		}else if(pathname=="/user_view/form_category"){
			FormCategory.defaultData(callback);
		}
	},
	load : function(){
		//code here
		var oTable = $('#loadFormCategory').dataTable( {
                     "bProcessing": true,
                     "bRetrieve": true,
                     "aaSorting" : [[2,"desc"]],
                     //"bPaginate": true,//"sEcho":3,//"iTotalDisplayRecords":10,//"iTotalRecords":"10",
                     //"iTotalDisplayRecords":"10",//"bServerSide": true,
                     "sAjaxSource": "/ajax/form_category",
                     "sPaginationType": "full_numbers",
                     "sAjaxDataProp": "company_user",
                     "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     "aoColumns": [
                         { "mData": "title" },
                         { "mData": "actions" }
                     ],
                     "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                         $("#example_length > label > select").addClass("selectlength");
                     },
                     "aoColumnDefs": [ {
                         "bSortable": true, "aTargets": [ 0,1 ]
                         },{
                             "bSortable": false, "aTargets": [ "_all" ]
                         }/*, {
                             "bSearchable": true, "aTargets": [ 0 ]
                         }, {
                             "bSearchable": false, "aTargets": [ "_all" ]
                     }*/
                     ],
                     "fnServerData": function (sSource, aoData, fnCallback) {
                         $.ajax({
                             type: "POST",
                             cache: false,
                             dataType: 'json',
                             url: sSource,
                             data: {action:"loadFormCategory"},
                             success: function (json) {
                                 // Retrieve data
                                 //console.log(json)
                                 fnCallback(json);
                             }
                         });
                     }
                 });
                return oTable;
	},
	deleteformCategory : function(){
		//code here
		var self = this;
		$("body").on("click",".deleteformCategory",function(){
			var id = $(this).attr("data-id");
			var newConfirm = new jConfirm("Warning : All forms which uses this category will automatically move to 'Others' module. Do you want to proceed?", 'Confirmation Dialog','', '', '', function(r){
			 	if(r==true){
			 		//get forms
			 		// ui.block();
			 		$.post("/ajax/form_category",
			 		{
			 			action:"deleteformCategory",
			 			id:id
			 		},function(data){
			 			// console.log(data)
			 			data = JSON.parse(data);
			 			var form_json = "";
			 			var formToUpdate = [];
			 			for(var i in data){
			 				form_json = JSON.parse(data[i]['form_json']);
			 				form_json['form_json']['form_category'] = 0;
                        	form_json['categoryName'] = 0;
			 				formToUpdate.push({
			 					"id":data[i]['id'],
			 					"form_json":JSON.stringify(form_json)
			 				})
			 			}
			 			//update forms
			 			$.post("/ajax/form_category",{formToUpdate:formToUpdate,action:"updateForms_softDelete",id:id},function(data2){
			 				// console.log(data2);
			 				$('#loadFormCategory').dataTable().fnDestroy();
							FormCategory.triggerDataTable();
							// ui.unblock();
							showNotification({
				                 message: "The selected form category has been successfully deleted.",
				                 type: "success",
				                 autoClose: true,
				                 duration: 3
			                });
			 			})
			 		// 	$('#loadFormCategory').dataTable().fnDestroy();
						// self.load();
			 		})

				 	// $.ajax({
	     //                 type: "POST",
	     //                 cache: false,
	     //                 dataType: 'json',
	     //                 url: "/ajax/form_category",
	     //                 data: {action:"deleteformCategory"},
	     //                 success: function (jsondata) {
	     //                     // Retrieve data
	     //                     //console.log(json)
	     //                     // fnCallback(json);
	     //                     console.log(jsondata)
	     //                 }
	     //             });
			 	}
			 })
		newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'});
		});
	},

	categoryModal : function(json){
		var self = this;
		var departments = $(".departments").text();
		var positions = $(".positions").text();
		var users = $(".users").text();
		var groups = $(".form_group").text();
		var admin = $(".users-admin").text();
		var departments_arr;var positions_arr;var users_arr;var group_arr;var admin_arr = [];
		var formPrivacy = json['users'];
		var categoryAdmin = json['admin'];
		var image = json['image'];
		if(formPrivacy){
			formPrivacy = JSON.parse(formPrivacy);
			departments_arr = formPrivacy['departments'];
			positions_arr = formPrivacy['positions'];
			users_arr = formPrivacy['users'];
			group_arr = formPrivacy['groups'];
		}
		if(categoryAdmin){
			admin_arr = categoryAdmin;
		}
		// $("body").on("click",".addCategoryModal",function(){
			var ret = '<div><h3> '+ json.title +'</h3></div>';
                ret += '<div class="hr"></div>';
                ret += '</div>';
                ret += '<div id="content-dialog-scroll" style="">'
                ret += '<div class="content-dialog sub-container-comment requestComment" style="width: 100%;padding:0px;">';
                ret += '<div id="accordion-container">';
	                ret += '    <ul>';
	                ret += '        <li><a href="#tabs-1">Primary Settings</a></li>';
	                ret += '        <li><a href="#tabs-2">Category Admin</a></li>';
	                ret += '        <li class="display"><a href="#tabs-3">Logo</a></li>';
	                ret += '    </ul>';
               
	                ret += '<div id="tabs-1" style="padding:10px;">';
		                ret += '    <div>';
		                ret += '    	<div class="fields_below section clearing fl-field-style">';
		                ret += '    	<div class="input_position_below column div_1_of_1" >';
		                ret += '    		<span class="">Category Name: <font color="red">*</font></span>';
		                ret += '    		<input type="text" name="" data-type="textbox" class="form-text category_name" value="'+ htmlEntities(json.value) +'"></div>';
		               	ret += '    </div>';
		                ret += '    </div>';
	                   ret += '    <div>';
		                ret += '    	<div class="fields_below section clearing fl-field-style">';
		                ret += '    	<div class="input_position_below column div_1_of_1">';
		                ret += '    		<div class="label_below2">Description:</div>';
		                ret += '    		<input type="text" name="" data-type="textbox" class="form-text category_description" value="'+ htmlEntities(json.description) +'"></div>';
		               	ret += '    </div>';
		                ret += '    </div>';
		                ret += '<div class="fp-container CSID-search-container categoryUsersContainer" keyElement="users">';
						    ret += '<div class="input_position" style="width:100%">';
							    ret += '<div class="section clearing fl-field-style">';
									ret += '<div class="column div_1_of_1" style="display:table;">';
										ret += '<span style="display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="fp-allUsers7"/><label for="fp-allUsers7" class="css-label"></label> All Users</label></span>';
										ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
									ret +='</div>';
								ret += '</div>';
								ret += '<div>';
									// for department
									ret += '<div class="container-formUser  section clearing fl-field-style" CSID-search-type="departments">';
										ret += '<div class="column div_1_of_1>';
											ret += '<div class="formUser" rel="getDepartmentFF" ><label class="fa fa-minus"></label> <label class="font-bold">Department</label></div>';
											ret += '<div class="getDepartmentFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';
											
											var ctr_dept = 0;
											if(departments){
												departments = JSON.parse(departments);
												$.each(departments,function(key, val){
													ret += '<div style="width:32%;float:left;margin-bottom:2px" class="CSID-search-data"> ';
													//setCheckedArray(value, arr)
														ret += '<label class="tip" title="'+ htmlEntities(val.department) +'"><input type="checkbox" class="fp-user fp-user1 departments css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, departments_arr) +' value="'+ val.id +'" id="fp-user-dept_'+ val.id +'"/><label for="fp-user-dept_'+ val.id +'" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">'+ htmlEntities(val.department) +'</span></label>';
													ret += '</div>';
													ctr_dept++
												})
											}
											var displayEmptyDept = "display";
											if(ctr_dept==0){
												displayEmptyDept = ""
											}
											ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
											ret += '</div>';
										ret += '</div>';
									ret += '</div>';
									//for groups
									ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
										ret += '<div class="column div_1_of_1">';
											ret += '<div class="formUser" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
												ret += '<div class="getUsersFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';
													
													var ctr_groups = 0
													if(groups){
														groups = JSON.parse(groups);
														$.each(groups,function(key, val){
															ret += '<div style="width:32%;float:left;margin-bottom:2px;" class="CSID-search-data"> ';
																ret += '<label class="tip" title="'+ htmlEntities(val.group_name) +'"><input type="checkbox" class="fp-user fp-user1 groups css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, group_arr) +' value="'+ val.id +'" id="fp-user group_'+ val.id +'"/><label for="fp-user group_'+ val.id +'" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">'+ htmlEntities(val.group_name) +'</span></label>';
															ret += '</div>';
															ctr_groups++
														})
														
													}
													var displayEmptyGroups = "display";
													if(ctr_groups==0){
														displayEmptyGroups = ""
													}
													ret += '<div class="empty-users '+ displayEmptyGroups +'" style="color:red">No Groups</div>';
											ret += '</div>';
										ret += '</div>';
									ret += '</div>';
									// for position
									ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="positions">';
										ret += '<div class="column div_1_of_1">';
											ret += '<div class="formUser" rel="getPositionFF" ><label class="fa fa-minus"></label> <label class="font-bold">Position</label></div>';
											ret += '<div class="getPositionFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';
												
												var ctr_pos = 0;
												if(positions){
													positions = JSON.parse(positions);
													$.each(positions,function(key, val){
														if(htmlEntities(val.position) !=""){
															ret += '<div style="width:32%;float:left;margin-bottom:2px" class="CSID-search-data"> ';
																ret += '<label class="tip" title="'+ htmlEntities(val.position) +'"><input type="checkbox" class="fp-user fp-user1 positions css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, positions_arr) +' value="'+ val.id +'" id="fp-user_positions_'+ val.id +'"/><label for="fp-user_positions_'+ val.id +'" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">'+ htmlEntities(val.position) +'</span></label>';
															ret += '</div>';
															ctr_pos++;
														}
													})
													
												}
											var displayEmptyPos = "display";
											if(ctr_pos==0){
												displayEmptyPos = ""
											}
											ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
											ret += '</div>';
										ret += '</div>';
									ret += '</div>';
									//for users
									ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
										ret += '<div class="formUser div_1_of_1 column" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
											ret += '<div class="getUsersFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';
												
												var ctr_usr = 0
												if(users){
													users = JSON.parse(users);
													var user_name = "";
													$.each(users,function(key, val){
														// user_name = val.first_name +' '+ val.last_name;
	                                                    user_name = htmlEntities(val.display_name);
														ret += '<div style="width:32%;float:left;margin-bottom:2px;" class="CSID-search-data"> ';
															ret += '<label class="tip" title="'+ user_name +'"><input type="checkbox" class="fp-user fp-user1 users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, users_arr) +' value="'+ val.id +'" id="fp-user users_'+ val.id +'"/><label for="fp-user users_'+ val.id +'" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">'+ user_name +'</span></label>';
														ret += '</div>';
														ctr_usr++
													})
													
												}
												var displayEmptyUsers = "display";
												if(ctr_usr==0){
													displayEmptyUsers = ""
												}
												ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
										ret += '</div>';
									ret += '</div>';
								ret += '</div>';
							ret += '</div>';
						ret += '</div>';
						ret += '<div id="tabs-2" style="padding:10px;">';
						ret += '<div class="fp-container CSID-search-container categoryAdminContainer" keyElement="admin">';
					    ret += '<div class="input_position" style="width:100%">';
						    ret += '<div class="section clearing fl-field-style">';
								ret += '<div class="column div_1_of_1" style="display:table;">';
								ret += '<span style="display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="fp-allUsers_admin"/><label for="fp-allUsers_admin" class="css-label"></label> All Users</label></span>';
									ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="search icon fl-search-icon-list CSID-search-user-btn" style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
								ret += ' </div>';
							ret += '</div>';
							ret += '<div>';
								//for users
								ret += '<div class="container-formUser section clearing fl-field-style"  CSID-search-type="users">';
								ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus"></label> <label class="font-bold">Users</label></div>';
									ret += '<div class="getUsersFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';
										var ctr_usr = 0
										if(admin){
											var user_name = "";
											admin = JSON.parse(admin);
											$.each(admin,function(key, val){
												// user_name = val.first_name +' '+ val.last_name;
                                                user_name = htmlEntities(val.display_name);
												ret += '<div style="width:32%;float:left;margin-bottom:2px;" class="CSID-search-data"> ';
													ret += '<label class="tip" title="'+ user_name +'"><input type="checkbox" class="fp-user fp-user2 category-admin css-checkbox" '+ setCheckedFromArray(admin_arr,val.id) +' value="'+ val.id +'" id="fp-user users-admin_'+ val.id +'"/><label for="fp-user users-admin_'+ val.id +'" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">'+ user_name +'</span></label>';
												ret += '</div>';
												ctr_usr++
											})
										}
										var displayEmptyUsers = "display";
										if(ctr_usr==0){
											displayEmptyUsers = ""
										}
										ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
										ret += '</div>';
								ret += '</div>';
								ret += '</div>';
							ret += '</div>';
						ret += '</div>';
					ret += '</div>';
					
					ret += '</div>';
				ret += '</div>';
				ret += '</div>';
				
				ret += '</div>';
				ret += '    <div class="display" id="tabs-3">';
					for(var i in categoryLogo){
						ret += "<div style='float:left;width:27%;margin-right:30px;'><label><input type='radio' class='category-logo css-checkbox' name='category-logo' value='"+ categoryLogo[i]['id'] +"' id='categoryLogo_"+ categoryLogo[i]['id'] +"'><label for='categoryLogo_"+ categoryLogo[i]['id'] +"' class='css-label'></label> <div style='background-color:"+ categoryLogo[i]['bg-color'] +";width:100%;border-radius: 100%;'><img style='height:100%;width:100%' src='/images/icon/app-logo/"+ categoryLogo[i]['imgpath'] +"' /></div><label></div>";
					}
				ret += '	</div>';
				ret += '</div>';
                ret += '</div>';
                ret += '</div>';
                //ret += '<div class="fields" style="border-top:1px solid #ddd;">';
                ret += '<div class="label_basic"></div>';
                ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                ret += json.button;
                ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
                ret += '</div>';
                ret += '</div>';
               var newDialog = new jDialog(ret, "", "700", "", "60", function() {

                });
                newDialog.themeDialog("modal2");
                limitText(".limit-text-ws",20);
                RequestPivacy.setCheckAllChecked(".fp-user1");
                RequestPivacy.setCheckAllChecked(".fp-user2");
                $('.content-dialog-scroll').perfectScrollbar();

                $( "#accordion-container" ).tabs({
		            activate : function(){
		                $('.content-dialog-scroll').perfectScrollbar("update");
		            }
		        })
                RequestPivacy.bindOnClickCheckBox();
                if(image){
                	$(".category-logo[value='"+ image +"']").attr("checked","checked");
                }
                ui.unblock();
		// })
	},
	addCategory : function(){
		var self = this;
		$("body").on("click",".addCategory",function(){
			
			var category_name = $.trim($(".category_name").val());
         var category_description = $.trim($(".category_description").val());
         
			var departments = new Array();
			var positions = new Array();
			var users = new Array();
			var groups = new Array();
			var json_privacy = {};
			var logo = $(".category-logo:checked").val()
			var categoryAdmin = new Array();
			if(category_name==""){
				showNotification({
	                 message: "Please fill out required fields",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
                });
				return;
			}
			
			var user_ctr = 0
			//for deoartments
			$(".departments").each(function(){
				if($(this).prop("checked")){
					departments.push($(this).val());
					user_ctr++;
				}
			})
			//for positions
			$(".positions").each(function(){
				if($(this).prop("checked")){
					positions.push($(this).val());
					user_ctr++;
				}
			})
			//for users
			$(".users").each(function(){
				if($(this).prop("checked")){
					users.push($(this).val());
					user_ctr++
				}
			})

			//for users
			$(".groups").each(function(){
				if($(this).prop("checked")){
					groups.push($(this).val());
					user_ctr++
				}
			})
			console.log(user_ctr)
			if(user_ctr==0){
				showNotification({
	                 message: "Please select user of this category",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
                });
				return;
			}

			$(".category-admin").each(function(){
				if($(this).prop("checked")){
					categoryAdmin.push($(this).val());
				}
			})
			json_privacy['departments'] = departments;
			json_privacy['positions'] = positions;
			json_privacy['users'] = users;
			json_privacy['groups'] = groups;
			json_privacy = JSON.stringify(json_privacy);
			var admin = {};
			admin['users'] = categoryAdmin;
			categoryAdmin = JSON.stringify(admin);
			ui.block();
			$.post("/ajax/form_category",
	 		{
	 			action:"addCategory",
	 			category_name:category_name,
            category_description:category_description,
	 			users:json_privacy,
	 			admin:categoryAdmin,
	 			logo:logo
	 		},function(data){
	 			if(data=="0"){
	 				showNotification({
		                 message: "The entered form category is already in the list. Please modify.",
		                 type: "error",
		                 autoClose: true,
		                 duration: 3
	                });
	 			}else{
	 				$('#loadFormCategory').dataTable().fnDestroy();
					self.triggerDataTable();
		 			showNotification({
		                 message: "Form Category has been successfully saved.",
		                 type: "success",
		                 autoClose: true,
		                 duration: 3
	                });
		 			$("#popup_cancel").click();
	 			}
	 			ui.unblock();
	 			
	 		})
		})
	},
	editCategory : function(){
		var self = this;
		$("body").on("click",".editCategory",function(){
			var category_name = $.trim($(".category_name").val());
         var category_description = $.trim($(".category_description").val());
			var category_id = $(this).attr("data-id");
			var logo = $(".category-logo:checked").val()
			var user_ctr = 0;
			if(category_name==""){
				showNotification({
	                 message: "Please fill out required fields",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
                });
				return;
			}
			
			var departments = new Array();
			var positions = new Array();
			var users = new Array();
			var groups = new Array();
			var json_privacy = {};
			var categoryAdmin = new Array();
			var sampleCheckedEle = UserAccessFlagCheckBox.checkedEle;
			var sampleUnCheckedEle = UserAccessFlagCheckBox.unCheckedEle;
			var ctrEdited = 0;
			$.each(sampleCheckedEle,function(key, value){
				if(typeof value=="object"){
					$.each(value,function(key2, value2){
						ctrEdited+= value2.length
					})
				}
			})
			$.each(sampleUnCheckedEle,function(key, value){
				if(typeof value=="object"){
					$.each(value,function(key2, value2){
						ctrEdited+= value2.length
					})
				}
			})
			

			//for deoartments
			$(".departments").each(function(){
				if($(this).prop("checked")){
					departments.push($(this).val());
					user_ctr++;
				}
			})
			//for positions
			$(".positions").each(function(){
				if($(this).prop("checked")){
					positions.push($(this).val());
					user_ctr++;
				}
			})
			//for users
			$(".users").each(function(){
				if($(this).prop("checked")){
					users.push($(this).val());
					user_ctr++;
				}
			})

			//for users
			$(".groups").each(function(){
				if($(this).prop("checked")){
					groups.push($(this).val());
					user_ctr++
				}
			})
			if(user_ctr==0){
				showNotification({
	                 message: "Please select user of this category",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
                });
				return;
			}

			//walang nagbago
			if(originalFormCategoryProperty['value']==category_name && originalFormCategoryProperty['description']==category_description && ctrEdited==0){
				$("#popup_cancel").click();
				return false;
			}

			$(".category-admin").each(function(){
				if($(this).prop("checked")){
					categoryAdmin.push($(this).val());
				}
			})
			json_privacy['departments'] = departments;
			json_privacy['positions'] = positions;
			json_privacy['users'] = users;
			json_privacy['groups'] = groups;
			json_privacy = JSON.stringify(json_privacy);
			
			var admin = {};
			admin['users'] = categoryAdmin;
			categoryAdmin = JSON.stringify(admin);
			var newConfirm = new jConfirm("Warning : All form users will be synced to your updated Category Users. This may take a while. Do you want to proceed?", 'Confirmation Dialog','', '', '', function(r){
			 	var jsonData = {
		 			action:"editCategory",
		 			category_name:category_name,
	            	category_description:category_description,
		 			users:json_privacy,
		 			admin:categoryAdmin,
		 			category_id:category_id,
		 			checkedEle:UserAccessFlagCheckBox.checkedEle,
		 			uncheckedEle:UserAccessFlagCheckBox.unCheckedEle,
		 			logo:logo
		 		}
		 		if(r==true){
		 			
		 			ui.block();
					$.post("/ajax/form_category",
			 		jsonData
			 		,function(data){
			 			console.log(data)
			 			if(data=="0"){
			 				showNotification({
				                 message: "The entered form category is already in the list. Please modify.",
				                 type: "error",
				                 autoClose: true,
				                 duration: 3
			                });
			 			}else{
			 				$('#loadFormCategory').dataTable().fnDestroy();
							self.triggerDataTable();
				 			showNotification({
				                 message: "Form Category has been successfully updated.",
				                 type: "success",
				                 autoClose: true,
				                 duration: 3
			                });
				 			$("#popup_cancel").click();
			 			}

			 			ui.unblock();
			 		})
		 		}
		 	})
		 	newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'});
			
		})
	},
	"defaultData" : function(callback){
		var oColReorder = {
            "iFixedColumns": 0
        };
        var oTable = $('#loadFormCategory').dataTable( {
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/form_category",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "search_value", "value": $.trim(jsonFormCategoryData['search_value'])});
                aoData.push({"name": "action", "value": "loadFormCategoryDatatable"});
                aoData.push({"name": "column-sort", "value": jsonFormCategoryData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonFormCategoryData['column-sort-type']});
                aoData.push({"name": "limit", "value": jsonFormCategoryData['limit']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
            	var obj = '#loadFormCategory';
                var listOfappContainer = '.fl-list-of-app-record-wrapper';
                dataTable_widget(obj, listOfappContainer);
                // 
            },
            fnDrawCallback : function(){
            	if (callback) {
                    callback(1);
                }
                set_dataTable_th_width([1]);
                setDatatableTooltip(".dataTable_widget");
                
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonFormCategoryData['limit']),
        });
        return oTable;
    }
}