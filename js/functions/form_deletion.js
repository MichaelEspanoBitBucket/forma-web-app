form_deletion = {

	load_form_list : function () {
		$.post("/ajax/form-deletion",
        {
            action : "getFormList"
        },
        function (data) {
            var parse_json = JSON.parse(data);
            var option_data = '';
            $('[name="form_list"]').html("");
            $('[name="form_list"]').append("<option>--Select--</option>");
            $.each(parse_json, function(id, val){
				if (val.FRM_NAME) {
					option_data += '<option value="'+ val.FRM_NAME +'">'+ val.FRM_NAME +'</option>';
				}
			})
			$('[name="form_list"]').append(option_data);
			ui.unblock();
		})	
	},

	form_deletion_process : function () {
		var self = this;
		self.popup_process_destroy();
		self.process_detail("#statusColor", "Processing", "blue");

		var form_selected = $('[name="form_list"]').val();

		var form_deletion_action_type = $('input[name=fd_action_type]:checked').val();
		
		if (form_selected !== "--Select--") {
			if (form_deletion_action_type == "delete_form") {
				$('[name=form_deletion_process]').html("");
				self.load_delete_form_process(form_selected);
			} else if (form_deletion_action_type == "form_cleanup") {
				$('[name=form_deletion_process]').html("");
				self.load_form_cleanup_process(form_selected);
			} else if (form_deletion_action_type == "workflow_cleanup") {
				$('[name=form_deletion_process]').html("");
				self.load_workflow_cleanup_process(form_selected);
			}
		} else {
			$('[name=form_deletion_process]').html("");
			self.popup_process_destroy();
			self.process_detail("#statusColor", "Idle", "grey");
			ui.unblock();
		}
	},

	form_deletion_execution : function () {
		var self = this;
		$("body").on("click",'.btnDelete', function() {
			var form_selected = $('[name="form_list"]').val();
			if (form_selected !== "--Select--") {
				var form_deletion_action_type = $('input[name=fd_action_type]:checked').val();
				if (form_deletion_action_type == "delete_form") {
					self.form_deletion_execution_validator("delete_form");
				} else if (form_deletion_action_type == "form_cleanup" && ($('.form_cleanup_checklist:checked').length > 0)) {
					self.form_deletion_execution_validator("form_cleanup");
				} else if (form_deletion_action_type == "workflow_cleanup" && ($('.workflow_cleanup_checklist:checked').length > 0)) {
					self.form_deletion_execution_validator("workflow_cleanup");
				} else {
					showNotification({
				       	message: "No Action selected.",
				    	type: "error",
				        autoClose: true,
				        duration: 3
			       	});
			       	self.popup_process_load("");
			       	self.process_detail("#statusColor", "Idle", "grey");
				}	
			} else {
				showNotification({
			       	message: "No Form selected.",
			    	type: "error",
			        autoClose: true,
			        duration: 3
		       	});
		       	self.popup_process_load("");
		       	self.process_detail("#statusColor", "Idle", "grey");
			}
		})
	},

	form_deletion_execution_validator : function (action_type) {
		var self = this;
		var newConfirm = new jConfirm(" Take note, make sure you have a backup before commiting the deletion of record, once the deletion is executed there is no turning back. Proceed?", 'Confirmation Dialog', '', '', '', function (ret) {
			if (ret == true) {
				if (action_type == "delete_form") {
					ui.block();
					self.popup_process_destroy();
					self.exec_delete_action_form_delete();
				} else if (action_type == "form_cleanup") {
					ui.block();
					self.popup_process_destroy();
					self.exec_delete_action_form_cleanup();
				} else if (action_type == "workflow_cleanup") {
					ui.block();
					self.popup_process_destroy();
					self.exec_delete_action_workflow_cleanup();
				}
			} 
		})
		newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'});
	},

	load_delete_form_process : function (form_selected) {
		var self = this;
		$.post("/ajax/form-deletion",
		{
			action : "getProcess_DeleteForm",
			form_name : form_selected
		},
		function (data) {
			if (data == "0") {
				showNotification({
			       	message: "Some Primary Table in Formalistics not Exists, Please re-migrate your current Database.",
			    	type: "error",
			        autoClose: true,
			        duration: 3
		       	});
		       	self.popup_process_load("Form Deletion Failed!");
		       	self.process_detail("#statusColor", "Failed", "red");
		       	self.load_form_list();
		       	ui.unblock();
			} else {
				var parse_json = JSON.parse(data);
				var ws_ctr = 0;
				var wf_ctr = 0;
				var flds_ctr = 0;
				var frmusers_ctr = 0;
				var reqlogs_ctr = 0;
				var notif_ctr = 0;
				var requsers_ctr = 0;
				var reqseen_ctr = 0;
				var anno_ctr = 0;
				$.each(parse_json, function (id, val) {
					if (val != null) {
						if (id == "workspace") {
							ws_ctr = val;
						} else if (id == "workflow") {
							wf_ctr = val;
						} else if (id == "fields") {
							flds_ctr = val;
						} else if (id == "formusers") {
							frmusers_ctr = val;
						} else if (id == "requestlogs") {
							reqlogs_ctr = val;
						} else if (id == "notification") {
							notif_ctr = val;
						} else if (id == "requestusers") {
							requsers_ctr = val;
						} else if (id == "requestseen") {
							reqseen_ctr = val;
						} else if (id == "annotation") {
							anno_ctr = val;
						}
					}
				})
				var ret = '<br>';
				ret += '<br>';
				ret += '<table class="table_data display_data dataTable">';
				ret += '<th style="width:25% !important">Related Tables</th>';
				ret += '<th style="width:25% !important">Total Number of Records</th>';
				ret += self.load_delete_form_process_condition("Annotation", anno_ctr);
				ret += self.load_delete_form_process_condition("Fields", flds_ctr);
				ret += self.load_delete_form_process_condition("Form User", frmusers_ctr);
				ret += self.load_delete_form_process_condition("Notification", notif_ctr);
				ret += self.load_delete_form_process_condition("Request Log", reqlogs_ctr);
				ret += self.load_delete_form_process_condition("Request Seen", reqseen_ctr);
				ret += self.load_delete_form_process_condition("Request Users", requsers_ctr);
				ret += self.load_delete_form_process_condition("Workflow", wf_ctr);
				ret += self.load_delete_form_process_condition("Workspace", ws_ctr);
				ret += '</table>';
				$('[name=form_deletion_process]').append(ret);

				self.popup_process_load("Done!");
				self.process_detail("#statusColor", "Idle", "grey");
				ui.unblock();
			}
		})
	},

	load_delete_form_process_condition : function (title, counter) {
		if (counter == 0) {
			var value = "Empty";
			var color_status = "fl_badge_silver";
		} else {
			var value = counter;
			var color_status = "fl_badge_SpringGreen";
		}
		var ret = '<tr>';
			ret += '<td>';
				ret += '<div class="fl-table-ellip">';
					ret += '<label>'+title+'</label>';
					ret += '<input value="" style="visibility:hidden"/>';
				ret += '</div>';
			ret += '</td>';
			ret += '<td>';
				ret += '<div class="fl-table-ellip">';
					ret += '<font class="'+color_status+' isDisplayInlineBlock">'+ value +'</font>';
					ret += '<input value="" style="visibility:hidden"/>';
				ret += '</div>';
			ret += '</td>';
			ret += '</tr>'
		return ret;
	},

	load_form_cleanup_process : function (form_selected) {
		var self = this;
		$.post("/ajax/form-deletion",
		{
			action : "getProcess_FormCleanUp",
			form_name : form_selected
		}, 
		function (data) {
			if (data == "0") {
				showNotification({
			       	message: "Some Primary Table in Formalistics not Exists, Please re-migrate your current Database.",
			    	type: "error",
			        autoClose: true,
			        duration: 3
		       	});
		       	self.popup_process_load("Form Deletion Failed!");
		       	self.process_detail("#statusColor", "Failed", "red");
		       	self.load_form_list();
		       	ui.unblock();
			} else {
				var parse_json = JSON.parse(data);
				var ret = '<br>';
				ret += '<br>';
				ret += '<table class="table_data display_data dataTable">';
				ret += '<th style="width:25% !important">Related Tables</th>';
				ret += '<th style="width:25% !important">Total Number of Records</th>';
				ret += '<th style="width:25% !important">';
					ret += '<input type="checkbox" class="css-checkbox form_cleanup_checklist_head" undefined="" value="" id="form_cleanup_all">';
						ret += '<label for="form_cleanup_all" class="css-label"></label>';
					ret += 'Action';
				ret += '</th>';
				var request_logs_ctr = 0;
				var request_seen_ctr = 0;
				var notification_ctr = 0;
				var annotation_ctr = 0;
				$.each(parse_json, function (id, val) {
					if (id == "request_logs" && val != null) {
						request_logs_ctr = val;
					} else if (id == "request_seen" && val != null) {
						request_seen_ctr = val;
					} else if (id == "notification" && val != null) {
						notification_ctr = val;
					} else if (id == "annotation" && val != null) {
						annotation_ctr = val;
					}
				})
				ret += self.load_form_cleanup_process_condition(annotation_ctr, "annotation", "Annotation");
				ret += self.load_form_cleanup_process_condition(notification_ctr, "notification", "Notification");
				ret += self.load_form_cleanup_process_condition(request_logs_ctr, "request_logs", "Request Logs");
				ret += self.load_form_cleanup_process_condition(request_seen_ctr, "request_seen", "Request Seen");
				ret += '</table>';
				$('[name=form_deletion_process]').append(ret);

				self.check_all_checkbox('#form_cleanup_all', '.form_cleanup_checklist');

				self.popup_process_load("Done!");
				self.process_detail("#statusColor", "Idle", "grey");
				ui.unblock();
			}
		})
	},

	load_form_cleanup_process_condition : function (counter, id, title) {
		if (counter == 0) {
			var value = "Empty";
			var color_status = "fl_badge_silver";
		} else {
			var value = counter;
			var color_status = "fl_badge_SpringGreen";
		}
		var ret = '<tr>';
			ret += '<td>';
				ret += '<div class="fl-table-ellip">';
					ret += '<label>'+title+'</label>';
					ret += '<input name="'+id+'" value="'+id+'" style="visibility:hidden"/>';
				ret += '</div>';
			ret += '</td>';
			ret += '<td>';
				ret += '<div class="fl-table-ellip">';
					ret += '<font class="'+color_status+' isDisplayInlineBlock">'+value+'</font>';
				ret += '</div>';
			ret += '</td>';
			ret += '<td>';
				ret += '<div class="fl-table-ellip">';
					ret += '<label class="tip" title="Action for '+title+'">';
						ret += '<input type="checkbox" class="css-checkbox form_cleanup_checklist" undefined="" value="'+id+'_cleanup" id="'+id+'_cleanup">';
						ret += '<label for="'+id+'_cleanup" class="css-label"></label>'; 
						ret += '<span class="limit-text-ws CSID-search-data-text">Delete</span>';
					ret += '</label>';
				ret += '</div>';
			ret += '</td>';
		ret += '</tr>';
		return ret;
	},

	load_workflow_cleanup_process : function (form_selected) {
		var self = this;
		$.post("/ajax/form-deletion",
		{
			action : "getProcess_WorkflowCleanUp",
			form_name : form_selected
		},
		function (data) {
			if (data == "0") {
				showNotification({
			       	message: "Some Primary Table in Formalistics not Exists, Please re-migrate your current Database.",
			    	type: "error",
			        autoClose: true,
			        duration: 3
		       	});
		       	self.popup_process_load("Form Deletion Failed!");
		       	self.process_detail("#statusColor", "Failed", "red");
		       	self.load_form_list();
		       	ui.unblock();
			} else {
				var parse_json = JSON.parse(data);
				var counter = 1;
				var ret = '<table class="table_data display_data dataTable">';
					ret += '<th style="width:10% !important">Workflow ID</th>';
					ret += '<th style="width:25% !important">Title</th>';
					ret += '<th style="width:10% !important">Date Created</th>';
					ret += '<th style="width:10% !important">Status</th>';
					ret += '<th style="width:10% !important">';
						ret += '<input type="checkbox" class="css-checkbox workflow_cleanup_checklist_head" undefined="" value="" id="action_workflow_cleanup_all_id">';
							ret += '<label for="action_workflow_cleanup_all_id" class="css-label"></label>';
						ret += 'Action';
					ret += '</th>';

					ret += '<br>';
					ret += '<span><font color="blue">Workspace ID: </font>';
						ret += '<label>'+ parse_json['workspace_id'] +'</label>';
						ret += '<input name="workspace_id" value="'+ parse_json['workspace_id'] +'" style="visibility:hidden"/>';
					ret += '</span>';
					ret += '<br>';

				$.each(parse_json['workflow_id'], function (id, val) {
					if (typeof val.WorkflowID !== "undefined" && val.WorkflowID !== "") {
						ret += '<tr>';
							ret += '<td>';
								ret += '<div class="fl-table-ellip">'; //Workflow ID
									ret += '<label>'+val.WorkflowID+'</label>';
									ret += '<input name="workflow_id_'+counter+'" value="'+val.WorkflowID+'" style="visibility:hidden"/>';
								ret += '</div>';
							ret += '</td>';
							ret += '<td>';
								ret += '<div class="fl-table-ellip">'; //Title
									ret += '<label>'+val.WorkflowID_Title+'</label>';
								ret += '</div>';
							ret += '</td>';
							ret += '<td>';
								ret += '<div class="fl-table-ellip">'; //Date Created
									ret += '<label>'+val.WorkflowID_DateCreated+'</label>';
								ret += '</div>';
							ret += '</td>';
							var status = "";
							var color_status = "";
							if (val.WorkflowID_Active == "1") {
								status = "Active";
								color_status = "fl_badge_SpringGreen"; //Status
							} else {
								status = "Not Active";
								color_status = "fl_badge_silver";
							}
							ret += '<td><div class="fl-table-ellip"><font class="'+color_status+' isDisplayInlineBlock">'+status+'</font></div></td>';
							ret += '<td>';
								ret += '<div class="fl-table-ellip">'; //Action
									ret += '<label class="tip" title="Clean the Workflow ID">';
										ret += '<input type="checkbox" class="css-checkbox workflow_cleanup_checklist" undefined="" value="'+val.WorkflowID+'" id="action_workflow_cleanup_'+counter+'_id">';
										ret += '<label for="action_workflow_cleanup_'+counter+'_id" class="css-label"></label>'; 
										ret += '<span class="limit-text-ws CSID-search-data-text">Delete</span>';
									ret += '</label>';
								ret += '</div>';
							ret += '</td>';
						ret += '</tr>';
						counter += 1;
					}
				})
				ret += '</table>';
				$('[name=form_deletion_process]').append(ret);
					
				if (counter == 1) {
					self.popup_process_load("No Workflow Detected!");
				} else {
					self.popup_process_load("Done!");
				}

				self.check_all_checkbox('#action_workflow_cleanup_all_id', '.workflow_cleanup_checklist');

				self.process_detail("#statusColor", "Idle", "grey");
				ui.unblock();
			}
		})
	},

	exec_delete_action_form_delete : function () {
		var self = this;

		self.process_detail("#statusColor", "Processing", "blue");

		var form_selected = $('[name="form_list"]').val();

		$.post("/ajax/form-deletion",
		{
			action : "execute_DeleteForm",
			form_name : form_selected
		},
		function (data) {
			if (data == "0") {
		 		showNotification({
			       	message: "Error on Deletion. It might be data already deleted or table is empty",
			    	type: "error",
			        autoClose: true,
			        duration: 3
		       	});
		       	self.popup_process_load("Form Deletion Failed!");
		       	self.process_detail("#statusColor", "Failed", "red");
		       	ui.unblock();
		    } else if (data == "ERROR_NO_FORM_DELETION_LOG") {
		    	showNotification({
			       	message: "Error no Form Deletion History Table Detected! Re-migrate database and try again.",
			    	type: "error",
			        autoClose: true,
			        duration: 3
		       	});
		       	self.popup_process_load("Form Deletion Failed!");
		       	self.process_detail("#statusColor", "Failed", "red");
		       	ui.unblock();

		 	} else {
			 	showNotification({
			        message: "The selected Form has been successfully deleted.",
			        type: "success",
			        autoClose: true,
			        duration: 3
		    	});
		    	self.popup_process_load("Done! Process Took "+ data +" sec to finished.");
		    	self.process_detail("#statusColor", "Success", "green");
		    	setTimeout(function () {
		 			//Note: After Form Deletion executed no popup_process_destroy() will be call so the last ouput query will be remain.
		 			$('[name=form_deletion_process]').html("");
					self.load_form_list();
					self.process_detail("#statusColor", "Idle", "grey");
					ui.unblock();
		 		}, 3000);
		 	}
		})
	},

	exec_delete_action_form_cleanup : function () {
		var self = this;

		self.process_detail("#statusColor", "Processing", "blue");

		var form_selected = $('[name="form_list"]').val();

		var requestlogs_checked = $('#request_logs_cleanup').is(":checked");
		var requestseen_checked = $('#request_seen_cleanup').is(":checked");
		var notification_checked = $('#notification_cleanup').is(":checked");
		var annotation_checked = $('#annotation_cleanup').is(":checked");

		var list_to_cleanup_arr = new Array();

		if (requestlogs_checked == true) {
			list_to_cleanup_arr.push("request_logs");
		}
		if (requestseen_checked == true) {
			list_to_cleanup_arr.push("request_seen");
		}
		if (notification_checked == true) {
			list_to_cleanup_arr.push("notification");
		}
		if (annotation_checked == true) {
			list_to_cleanup_arr.push("annotation");
		}

		var list_to_cleanup_json = JSON.stringify(list_to_cleanup_arr);

		$.post("/ajax/form-deletion",
		{
			action : "execute_FormCleanUp",
			form_name : form_selected,
			list_to_cleanup : list_to_cleanup_json

		},
		function (data) {
			if (data == "0") {
		 		showNotification({
			       	message: "Error on Deletion. It might be data already deleted or table is empty.",
			    	type: "error",
			        autoClose: true,
			        duration: 3
		       	});
		       	self.popup_process_load("Form clean-up Failed!");
		       	self.process_detail("#statusColor", "Failed", "red");
		       	ui.unblock();
		 	} else {
			 	showNotification({
			        message: "The selected Form clean-up has been successfully deleted.",
			        type: "success",
			        autoClose: true,
			        duration: 3
		    	});
		    	self.popup_process_load("Done! Process Took "+ data +" sec to finished.");
		    	self.process_detail("#statusColor", "Success", "green");
		 		setTimeout(function () { 
		 			self.form_deletion_process(); 
		 		}, 3000);
		 	}
		})
	},

	exec_delete_action_workflow_cleanup : function () {
		var self = this;

		self.process_detail("#statusColor", "Processing", "blue");

		var obj_count = ($('.workflow_cleanup_checklist').length);
		var workflow_id_arr = new Array();

		for (var x = 0; x <= obj_count; x++ ) {
			var checkbox = $('#action_workflow_cleanup_'+x+'_id').is(":checked");
			if (checkbox == true) {
				var workflow_id = $('#action_workflow_cleanup_'+x+'_id').val();
				workflow_id_arr.push(workflow_id);
			}
		}

		var workflow_id_encoded_json = JSON.stringify(workflow_id_arr);
		var workspace_id = $('[name="workspace_id"]').val();
		var form_selected = $('[name="form_list"]').val();

		$.post("/ajax/form-deletion",
		{
			action : "execute_WorkflowCleanUp",
			workspace_id : workspace_id,
			workflow_id : workflow_id_encoded_json,
			form_name : form_selected
		},
		function (data) {
			if (data == "0") {
		 		showNotification({
			       	message: "Error on Deletion. It might be data already deleted or table is empty.",
			    	type: "error",
			        autoClose: true,
			        duration: 3
		       	});
		       	self.popup_process_load("Workflow clean-up Failed!");
		       	self.process_detail("#statusColor", "Failed", "red");
		       	ui.unblock();
		 	} else {
			 	showNotification({
			        message: "The Selected Workflow ID has been successfully deleted.",
			        type: "success",
			        autoClose: true,
			        duration: 3
		    	});
		    	self.popup_process_load("Done! Process Took "+ data +" sec to finished.");
		    	self.process_detail("#statusColor", "Success", "green");
		 		setTimeout(function () { 
		 			self.form_deletion_process(); 
		 		}, 3000);
		 	}
		})
	},
 
	form_deletion_action_type_onchange : function () {
		var self = this;
		$('input[name=fd_action_type').change(function() {
			self.popup_process_destroy();
			self.process_detail("#statusColor", "Idle", "grey");
			$('[name=form_deletion_process]').html("");
			$('[name="form_list"]').val("--Select--");
		});
	},

	process_detail : function (element, prompt, color) {
		// [Processing = Blue, Idle = Grey, Success = Green, Failed = Red]
		$(element).html("");
		$(element).append(" " + prompt);
		$(element).attr('color', color);
	},

	popup_process_load : function (title) {
		$("#processColor").html("");
		$("#processColor").append("PROCESS");
		$("#processColor").attr('color', "grey");
		$("#processColor").attr('data-original-title', title);
		$("#processColor").tooltip({ show: { duration: 1 } });
	    $("#processColor").trigger('mouseenter');
	    setTimeout(function () { $('#processColor').tooltip('hide'); }, 2000);
	},

	popup_process_destroy : function () {
		$("#processColor").tooltip('destroy');
	},

	disable_arrow_key_dropdown : function () {
		$('[name="form_list"]').on('keydown', function (e) {
		    if (e.keyCode === 38 || e.keyCode === 40 || e.keyCode === 37 || e.keyCode === 39) { // [38 = up, 40 = down, 37 = left, 39 = right]
		        e.preventDefault();
		        return false;
		    }
		})
	},

	check_all_checkbox : function (checkbox_id, checkbox_class) {
		$(checkbox_id).click(function(event) {
	        if (this.checked) {
	            $(checkbox_class).each(function() {
	                this.checked = true;            
	            });
	        } else {
	            $(checkbox_class).each(function() {
	                this.checked = false;                       
	            });         
	        }
    	})
	},

	form_deletion_history : function () {
		var self = this;
		$("body").on("click",'.btnFormDeletionHistory', function() {
			
	        var ret = '<h3 class="fl-margin-bottom"><i class="fa fa-history"></i> History</h3>';
	        ret += '<div class="hr"></div>';
	        ret += '<div class="fields fl-form-deletion-history-list-wrap">';
	        	ret += '<div class="fl-list-of-app-record-wrapper">';
	        		ret += '<div class="fl-table-wrapper"  style="min-height:250px">';
	        			ret += '<table class="display_data dataTable loadFormDeletionHistory"  id="loadFormDeletionHistory">';
	        				ret += '<thead class="fl-header-tbl-wrapper">';
	        					ret += '<tr>';
	        						// ret += '<th style="width: 70px !important;"><div class=""></div><div class="fl-table-ellip"></div></th>';
	       							// ret += '<th style="width:25% !important"><div class=""></div><div class="fl-table-ellip">Form ID</div></th>';
	        						ret += '<th style="width:15% !important"><div class=""></div><div class="fl-table-ellip">Form Name</div></th>';
							        ret += '<th style="width:15% !important"><div class=""></div><div class="fl-table-ellip">Action</div></th>';
							        ret += '<th style="width:25% !important"><div class=""></div><div class="fl-table-ellip">Affected Form/ID</div></th>';
							        ret += '<th style="width:15% !important"><div class=""></div><div class="fl-table-ellip">Date Changed</div></th>';
							        ret += '<th style="width:10% !important"><div class=""></div><div class="fl-table-ellip">User</div></th>';
	        					ret += '</tr>';
	        				ret += '</thead>';
	        			ret += '</table>';
	       			 ret += '</div>';
	        	ret += '</div>';
	        ret += '</div>';

	        var newDialog = new jDialog(ret, "", "800", "", "", function () {});
	        newDialog.themeDialog("modal2");
	        FormDeletionHistory.init(function () {});
		})
	},

	// sql_bulk_query : function () {
	// 	$("body").on("click",'.btnbulk', function() {
	// 		ui.block();
	// 		var data = new Array();
	// 		data['TrackNo'] = 'SS100';
	// 		data['Requestor'] = '333';
	// 		data['Status'] = 'Submitted'; 
	// 		data['Processor'] = '123';
	// 		data['ProcessorType'] = '';
	// 		data['ProcessorLevel'] = '4';
	// 		data['LastAction'] = 'qweqweqweqweqweqweqwe';
	// 		data['DateCreated'] = ''; 
	// 		data['DateUpdated'] = ''; 
	// 		data['CreatedBy'] = 'akaoaakodsasdasdsd';
	// 		data['UpdatedBy'] = 'qweasdasdasdaf'; 
	// 		data['Unread'] = 'qqdasdasdadssadadsadsadsadsadsadssdasdasdasd'; 
	// 		data['Node_ID'] = 'aaaaaaaaaaaaasdddddddddddddddddddddddddddddddddqweqweqweqweqweqwwdashdjasdhasdasjdqwhjehjqwjhehjashdhjdquweuqwyeyusauydayusxczxczxczxccqsweqwhehj123sddasdasdxcoxzpoiriwer7'; 
	// 		data['Workflow_ID'] = 'aaaaaaaaaaaaasdddddddddddddddddddddddddddddddddqweqweqweqweqweqwwdashdjasdhasdasjdqwhjehjqwjhehjashdhjdquweuqwyeyusauydayusxczxczxczxccqsweqwhehj123sddasdasdxcoxzpoiriwer7&qei123jasdjaksdjkqweqweqweasdasd'; 
	// 		data['fieldEnabled'] = 'wqeqweqw'; 
	// 		data['fieldRequired'] = 'aaaaaaaaaaaaasdddddddddddddddddddddddddddddddddqweqweqweqweqweqwwdashdjasdhasdasjdqwhjehjqwjhehjashdhjdquweuqwyeyusauydayusxczxczxczxccqsweqwhehj123sddasdasdxcoxzpoiriwer7aaaaaaaaaaaaasdddddddddddddddddddddddddddddddddqweqweqweqweqweqwwdashdjasdhasdasjdqwhjehjqwjhehjashdhjdquweuqwyeyusauydayusxczxczxczxccqsweqwhehj123sddasdasdxcoxzpoiriwer7'; 
	// 		data['fieldHiddenValues'] = 'qweqweqw'; 
	// 		data['imported'] = 'qweqweqw';
	// 		data['Repeater_Data'] = 'qweqwqwe';

	// 		var rec_arr = new Array();
	// 		for (var x=1; x<=1000; x++) {
	// 			rec_arr.push({
	//                 TrackNo: data['TrackNo'],
	//                 Requestor: data['Requestor'],
	//                 Status: data['Status'],
	//                 Processor : data['Processor'],
	//                 ProcessorType : data['ProcessorType'],
	//                 ProcessorLevel : data['ProcessorLevel'],
	//                 LastAction : data['LastAction'],
	//                 DateCreated : data['DateCreated'],
	//                 DateUpdated : data['DateUpdated'],
	//                 CreatedBy : data['CreatedBy'],
	//                 UpdatedBy : data['UpdatedBy'],
	//                 Unread : data['Unread'],
	//                 Node_ID : data['Node_ID'],
	//                 Workflow_ID : data['Workflow_ID'],
	//                 fieldEnabled : data['fieldEnabled'],
	//                 fieldRequired : data['fieldRequired'],
	//                 fieldHiddenValues : data['fieldHiddenValues'],
	//                 imported : data['imported'],
	//                 Repeater_Data : data['Repeater_Data']
 //            	})
	// 		}

	// 		var ddd = {};
	// 		ddd['records'] = JSON.stringify(rec_arr);

	// 		$.post("/ajax/form-deletion",
	// 		{
	// 			action : "sqlbulkquery",
	// 			dataaaa : ddd['records']
	// 		},
	// 		function (data) {
	// 			if (data == "0") {
	// 		 		showNotification({
	// 			       	message: "insert failed.",
	// 			    	type: "error",
	// 			        autoClose: true,
	// 			        duration: 3
	// 		       	});
	// 		       	ui.unblock();
	// 		 	} else {
	// 			 	showNotification({
	// 			        message: "record inserted.",
	// 			        type: "success",
	// 			        autoClose: true,
	// 			        duration: 3
	// 		    	});
	// 		    	ui.unblock();
	// 		 	}
	// 		})
	// 	})
	// }
}

$(document).ready( function () {
	var pathname = window.location.pathname;
	if (pathname == "/user_view/form-deletion") {
		ui.block();
		form_deletion.load_form_list();
		form_deletion.form_deletion_action_type_onchange();
		$('[name="form_list"]').change(function () {
			ui.block();
			form_deletion.form_deletion_process();
		});	
		form_deletion.form_deletion_execution();
		form_deletion.disable_arrow_key_dropdown();
		form_deletion.form_deletion_history();
		// form_deletion.sql_bulk_query();
	}
})

var jsonLatestModifiedFormDeletionData = {
    "search_value": "",
    "endlimit": "10",
    "column-sort": "Date_Changed",
    "column-sort-type": "DESC"
}

FormDeletionHistory = {
    init: function (callback) {
        var oTable = $('#loadFormDeletionHistory').dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> ' +
                        '<div class="bar1"></div> ' +
                        '<div class="bar2"></div> ' +
                        '<div class="bar3"></div> ' +
                        '<div class="bar4"></div> ' +
                        '<div class="bar5"></div> ' +
                        '<div class="bar6"></div> ' +
                        '<div class="bar7"></div> ' +
                        '<div class="bar8"></div> ' +
                        '<div class="bar9"></div> ' +
                        '<div class="bar10"></div> ' +
                        '</div>'
            },
            "oColReorder": false,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/form-deletion",
            "fnServerData": function (sSource, aoData, fnCallback) {
            	aoData.push({"name": "action", 				"value": "showList_FormDeletionHistory"});
                aoData.push({"name": "search_value", 		"value": $.trim(jsonLatestModifiedFormDeletionData['search_value'])});
                aoData.push({"name": "endlimit", 			"value": jsonLatestModifiedFormDeletionData['endlimit']});
                aoData.push({"name": "column-sort", 		"value": jsonLatestModifiedFormDeletionData['column-sort']});
                aoData.push({"name": "column-sort-type", 	"value": jsonLatestModifiedFormDeletionData['column-sort-type']});
                console.log(jsonLatestModifiedFormDeletionData)
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        // console.log(data)
                        // return;
                        if (data == "0") {
                        	showNotification({
						       	message: "Some Primary Table in Formalistics not Exists, Please re-migrate your current Database.",
						    	type: "error",
						        autoClose: true,
						        duration: 3
					       	});
					       	$('#loadFormDeletionHistory_processing').remove();
                        } else {
                        	fnCallback(data);
                        }	 
                    }
                });
            },
            "fnInitComplete": function (oSettings, json) {
                var obj = '#loadFormDeletionHistory';
                var listOfappContainer = '.fl-list-of-app-record-wrapper';
                dataTable_widget(obj, listOfappContainer);
                $(".content-dialog-scroll").perfectScrollbar("update");
            },
            fnDrawCallback: function () {

                $(".tip").tooltip();
                if (callback) {
                    callback(1);
                }
            }
        });
        return oTable;
    }
}