var jsonFormData = {
    search_value: "", 
    start: 0,
    limit: 10,  
}
var columnSort = [];
$(document).ready(function() {
	//load
    GetFormDataTable.defaultData();
	$("body").on("keyup","#txtSearchFormDatatable",function(e){
        if (e.keyCode == "13") {
            $(".dataTable_form").dataTable().fnDestroy();
            jsonFormData['search_value'] = $(this).val();
            GetFormDataTable.defaultData();
        }
    })
    //button
    $("#btnSearchFormDatatable").click(function(){
        $(".dataTable_form").dataTable().fnDestroy();
        jsonFormData['search_value'] = $("#txtSearchFormDatatable").val();
        GetFormDataTable.defaultData();
    })
    
    //sort
    $("body").on("click",".dataTable_form th",function(){
       
        var cursor = $(this).css("cursor");
        jsonFormData['column-sort'] = $(this).attr("field_name");
        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest(".dataTable_form").find(".sortable-image").html("");
        if(indexcolumnSort==-1){
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonFormData['column-sort-type'] = "ASC";
        }else{
            columnSort.splice(indexcolumnSort,1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonFormData['column-sort-type'] = "DESC";
        }
        
        $(".dataTable_form").dataTable().fnDestroy();
        var self = this;
        GetFormDataTable.defaultData(function(){
             addIndexClassOnSort(self);
        });
    })
    
    //show entries
    $(".searchFormLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonFormData['limit'] = val;
            $(".dataTable_form").dataTable().fnDestroy();
            // var self = this;
            var oTable = GetFormDataTable.defaultData();
        // }
    })
})

GetFormDataTable = {
    "defaultData" : function(callback){
        var oColReorder = {
            allowReorder : false,
            allowResize : true
        };
        var oTable = $(".dataTable_form").dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder" : oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/search",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "action", "value": "getFormDataTable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "limit", "value": jsonFormData['limit']});
                aoData.push({"name": "search_value", "value": $.trim(jsonFormData['search_value'])});
                aoData.push({"name": "column-sort", "value": jsonFormData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonFormData['column-sort-type']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        // console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {

                var obj = '.dataTable_form';
                var listOfappContainer = $('.fl-list-of-app-record-wrapper');
                dataTable_widget(obj, listOfappContainer);
                dataTableSetwidth(this.find('[field_name="is_active"]'), 130);
            },
            fnDrawCallback : function(){

                set_dataTable_th_width([1]);
                setDatatableTooltip(".dataTable_widget");

                if (callback) {
                    callback(1);
                }

                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonFormData['limit']),
        });
        return oTable;
    }
}