console.log("UI event handlers");
var file_uploader_ui_event_handlers = {
    uploadWidgetId: 0
};

var file_uploader = {
    uploadedFileMap: {},
    alreadyUploadedFiles: [],
    onRemoveCallback: null,
    onAllAttachmentsRemovedCallback: null,
    onAttachmentAddedCallback: null
};

(function () {
    
    file_uploader_ui_event_handlers.onUploadWidgetReady = function (uploadWidgetId) {
        file_uploader_ui_event_handlers.uploadWidgetId = uploadWidgetId;
        console.log("upload widget ready: "+file_uploader_ui_event_handlers.uploadWidgetId);  
    };

    file_uploader_ui_event_handlers.onFileUploadingDone = function (uploadResults) {
        console.log(uploadResults.error);
        console.log("onFileUploadingDone");
        if (uploadResults.error) {
            alert(uploadResults.error_message);
            return;
        }

        var filesUploaded = uploadResults.results;
        var uploadedFilenames = [];

        //  Remove the loading UI for each uploading file               
        for (var index in filesUploaded) {
            var fileName = filesUploaded[index].original_filename;
            var fileId = generateElementIdFromText(fileName);

            uploadedFilenames.push(filesUploaded[index].new_filename);
            file_uploader.uploadedFileMap[fileId] = filesUploaded[index].new_filename;
   
   
            $("#" + fileId).find(".fl-image-loading").css("display", "none");
        }
        
        file_uploader.addFiles(file_uploader_ui_event_handlers.uploadWidgetId, uploadedFilenames);

    };

    file_uploader_ui_event_handlers.onFileSelectedHandler = function (uploadedData) {
        console.log("onFileSelectedHandler");
        var continueUpload = false;
        var uploadWidgetId = file_uploader_ui_event_handlers.uploadWidgetId;
        
        if (uploadedData.files) {

            continueUpload = true;

            var totalFileSize = file_uploader.getCurrentUploadedFilesize(uploadWidgetId);
            for (var i = 0; i < uploadedData.files.length; i++) {
                totalFileSize += uploadedData.files[i].size;
            }

            if (totalFileSize > 2000000) {
                file_uploader.clearAttachments(uploadWidgetId);
                alert("Total attachment filesize must not exceed 2MB");
                return;
            }

            var validFiles = [];

            for (var i = 0; i < uploadedData.files.length; i++) {

                var fileType = getFileType(uploadedData.files[i]);

                console.log(file_uploader.alreadyUploadedFiles[uploadWidgetId]);

                var fileToken = uploadedData.files[i].lastModified + "" + uploadedData.files[i].name;
                if ($.inArray(fileToken, file_uploader.alreadyUploadedFiles[uploadWidgetId]) > -1) {
                   // alert(uploadedData.files[i].name + " is already uploaded");

                    $(uploadWidgetId + " > .generic-file-upload").val("");

                    if (uploadedData.files.length === 1) {
                        alert("The selected file is already uploaded.");
                    } else {
                        alert("Some or all of the selected files are already uploaded.");
                    }

                    continueUpload = false;
                    break;
                }
                console.log(fileToken)
                console.log(file_uploader.alreadyUploadedFiles[uploadWidgetId])
                if(!file_uploader.alreadyUploadedFiles[uploadWidgetId]){

                }else{

                     file_uploader.alreadyUploadedFiles[uploadWidgetId].push(fileToken);
                }
                var reader = new FileReader();
                reader.onload = (function (fileData) {
                    return function (e) {

                        //var fileData = uploadedData.files[i];
                        var fileTextData = e.target.result;
                        var generatedPreview;

                        var fileToken = fileData.lastModified + "" + fileData.name;

                        if (fileType === "image") {
                            fileData.fileTextData = fileTextData;
                            generatedPreview = createImagePreview(uploadWidgetId, fileData, fileToken);
                            appendPreviewPane(uploadWidgetId, generatedPreview, "IMAGE");
                        } else {
                            generatedPreview = createFilePreview(uploadWidgetId, fileData, fileToken);
                            appendPreviewPane(uploadWidgetId, generatedPreview, "OTHER");
                        }
                        var currentFileSize = fileData.size;

                        // $(".fl-file-uploader-widget").width(230);

                        // change tab display height
                        // var displayHeight = $("#" + uploadWidgetId).closest("[data-tab-name]").find(".fl-tab-display").height();
                        // var previewHeight = $("#" + uploadWidgetId).height();
                        // $("#" + uploadWidgetId).closest("[data-tab-name]").find(".fl-tab-display").height(displayHeight - previewHeight + "px");

                        //  Add the filesize
                        file_uploader.setCurrentUploadedFilesize(uploadWidgetId, totalFileSize + currentFileSize);

                        //  Register events to the newly added UI elements
                        var previewId = $(generatedPreview).attr("id");
                        $("#" + uploadWidgetId).find("#" + previewId).find(".fl-file-delete").click(function () {
                            console.log("file delete clicked ui event handlers");
                            var tokenString = $("#" + previewId).attr("file-token");
                            var index = $.inArray(tokenString, file_uploader.alreadyUploadedFiles[uploadWidgetId]);

                            if (index > -1) {
                                file_uploader.alreadyUploadedFiles[uploadWidgetId].splice(index, 1);
                            }

                            file_uploader.removeAttachment(
                                    uploadWidgetId,
                                    previewId, currentFileSize
                                    );
                        });

                        if (file_uploader.onAttachmentAddedCallback) {
                            file_uploader.onAttachmentAddedCallback();
                        }

                    };
                })(uploadedData.files[i]);  //  Continuous Async

                reader.readAsDataURL(uploadedData.files[i]);
            }

        }

        return continueUpload;
    };

    file_uploader.addFiles = function (uploadWidgetId, files) {

        var uploadedFilenames = $("#" + uploadWidgetId).attr("uploaded-filenames");
        var newFilenames = files.join(",");

        if (newFilenames) {
            if (uploadedFilenames) {
                uploadedFilenames = uploadedFilenames + "," + newFilenames;
            } else {
                uploadedFilenames = newFilenames;
            }
        }

        $("#" + uploadWidgetId).attr("uploaded-filenames", uploadedFilenames);

    };

    file_uploader.registerOnAttachmentRemovedCallback = function (onRemoveCallback) {
        file_uploader.onRemoveCallback = onRemoveCallback;
    };

    file_uploader.clearAttachments = function (uploadWidgetId) {
console.log("hoy!")
        $("#" + uploadWidgetId).find(".fl-file-resource-wrapper").html("");
        $("#" + uploadWidgetId).find(".fl-file-images-wrapper").html("");

        $("#" + uploadWidgetId).find(".fl-file-resource-wrapper").css("display", "none");
        $("#" + uploadWidgetId).find(".fl-file-images-wrapper").css("display", "none");

        $("#" + uploadWidgetId).attr("uploaded-filenames", "");

        file_uploader.setCurrentUploadedFilesize(uploadWidgetId, 0);
        file_uploader.alreadyUploadedFiles[uploadWidgetId] = [];
    };

    file_uploader.removeAttachment = function (uploadWidgetId, attachmentId, attachmentFilesize) {

        var attachmentClass = $("#" + attachmentId).attr("class");
        var attachmentCount;

        if (attachmentClass === "fl-file-image") {
            attachmentCount = $("#" + uploadWidgetId).find('.fl-file-images-wrapper').children("." + attachmentClass).length;
        } else {
            attachmentCount = $("#" + uploadWidgetId).find('.fl-file-resource-wrapper').children("." + attachmentClass).length;
        }

        if (attachmentCount <= 1) {

            if (attachmentClass === "fl-file-image") {
                $("#" + uploadWidgetId).find('.fl-file-images-wrapper').html("");
                $("#" + uploadWidgetId).find('.fl-file-images-wrapper').css("display", "none");
            } else {
                $("#" + uploadWidgetId).find('.fl-file-resource-wrapper').html("");
                $("#" + uploadWidgetId).find('.fl-file-resource-wrapper').css("display", "none");
            }

            if (file_uploader.onAllAttachmentsRemovedCallback) {
                file_uploader.onAllAttachmentsRemovedCallback();
            }

            file_uploader.setCurrentUploadedFilesize(uploadWidgetId, 0);
        } else {
            //  subtract the filesize
            var currentFilesize = file_uploader.getCurrentUploadedFilesize(uploadWidgetId);
            currentFilesize -= attachmentFilesize;
            file_uploader.setCurrentUploadedFilesize(uploadWidgetId, currentFilesize);

            $("#" + attachmentId).remove();
        }

        var uploadedFileNamesString = $("#" + uploadWidgetId).attr("uploaded-filenames");
        var uploadedFilenames = uploadedFileNamesString.split(",");

        var uploadedFileName = file_uploader.uploadedFileMap[attachmentId];
        var fileToRemoveIndex = uploadedFilenames.indexOf(uploadedFileName);

        if (fileToRemoveIndex > -1) {
            uploadedFilenames.splice(fileToRemoveIndex, 1);
        }
        var newUploadedFilenamesString = uploadedFilenames.join(",");

        $("#" + uploadWidgetId).attr("uploaded-filenames", newUploadedFilenamesString);

        if (file_uploader.onRemoveCallback) {
            file_uploader.onRemoveCallback(uploadWidgetId);
        }


    };

    file_uploader.getCurrentUploadedFilesize = function (uploadWidgetId) {
        return parseInt($("#" + uploadWidgetId).attr("uploaded-filesize")) || 0;
    };

    file_uploader.setCurrentUploadedFilesize = function (uploadWidgetId, filesize) {
        $("#" + uploadWidgetId).attr("uploaded-filesize", filesize);
    };

    // <editor-fold desc="Private Functions" defaultstate="collapsed">

    function getFileType(fileData) {
        var mimeType = fileData.type;
        return mimeType.split("/")[0];
    }

    function appendPreviewPane(uploadWidgetId, html, type) {

        if (type === "IMAGE") {
            $("#" + uploadWidgetId).find(".fl-file-images-wrapper").css("display", "block");
            $("#" + uploadWidgetId).find(".fl-file-images-wrapper").append(html);
            // console.log("hey YOU!",$("#" + uploadWidgetId))
            $("#" + uploadWidgetId).parents('.chat_tabs').find('.chat_text').focus();
        } else {
            $("#" + uploadWidgetId).find(".fl-file-resource-wrapper").css("display", "block");
            $("#" + uploadWidgetId).find(".fl-file-resource-wrapper").append(html);
        }
        //---scrollbar for image upload...
        if($("#" + uploadWidgetId).find(".fl-file-images-wrapper").height() > 230){
            console.log("here!")
            $("#" + uploadWidgetId).find(".fl-file-images-wrapper").height(227);
            $("#" + uploadWidgetId).find(".fl-file-images-wrapper").perfectScrollbar({minScrollbarLength: 50});
        }else if($("#" + uploadWidgetId).find(".fl-file-images-wrapper").height() == 227){
            console.log("dito");
            $("#" + uploadWidgetId).find(".fl-file-images-wrapper").perfectScrollbar('update');
        }
        //---scrollbar for file upload
        if($("#" + uploadWidgetId).find(".fl-file-resource-wrapper").height() > 50){
            console.log("here!")
            $("#" + uploadWidgetId).find(".fl-file-resource-wrapper").height(49);
            $("#" + uploadWidgetId).find(".fl-file-resource-wrapper").perfectScrollbar({minScrollbarLength: 30});
        }else if($("#" + uploadWidgetId).find(".fl-file-resource-wrapper").height() == 49){
            console.log("dito");
            $("#" + uploadWidgetId).find(".fl-file-resource-wrapper").perfectScrollbar('update');
        }
    }

    function createImagePreview(uploadWidgetId, fileData, fileToken) {

        var fileTextData = fileData.fileTextData;
        var imagePreview = $("#" + uploadWidgetId).find(".fl-file-uploader-widget-templates").find(".fl-file-image-template").clone();
        $(imagePreview).find(".fl-file-image").attr("id", generateElementIdFromText(fileData.name));
        $(imagePreview).find(".fl-file-image").attr("file-token", fileToken);
        // $(imagePreview).find("img").attr("height", "75px");
        // $(imagePreview).find("img").attr("width", "75px");
        $(imagePreview).find("img").attr("src", fileTextData);

        $(imagePreview).attr("file-token", fileToken);

        return imagePreview.html();

    }

    function createFilePreview(uploadWidgetId, fileData, fileToken) {

        var filePreview = $("#" + uploadWidgetId).find(".fl-file-uploader-widget-templates").find(".fl-file-template");
        filePreview.find(".fl-file").attr("id", generateElementIdFromText(fileData.name));
        filePreview.find(".fl-file").attr("file-token", fileToken);
        filePreview.find("span").html(fileData.name);

        return filePreview.html();

    }

    function generateElementIdFromText(plainText) {
        var decodedText = htmlEnDeCode.htmlDecode(plainText);
        var filteredSpaceText = decodedText.split(" ").join("_");
        var splittedText = filteredSpaceText.split(".");
        var extensionlessText = filteredSpaceText.replace("." + splittedText[splittedText.length - 1], "");
        var specialCharacterlessText = extensionlessText.replace(/([~!@#$%^&*()_+=`{}\[\]\|\\:;'<>,.\/? ])+/g, '-').replace(/^(-)+|(-)+$/g, '_');

        return specialCharacterlessText;
    }

    // </editor-fold>

}());

var htmlEnDeCode = (function () {
    var charToEntityRegex,
            entityToCharRegex,
            charToEntity,
            entityToChar;

    function resetCharacterEntities() {
        charToEntity = {};
        entityToChar = {};
        // add the default set
        addCharacterEntities({
            '&amp;': '&',
            '&gt;': '>',
            '&lt;': '<',
            '&quot;': '"',
            '&#39;': "'"
        });
    }

    function addCharacterEntities(newEntities) {
        var charKeys = [],
                entityKeys = [],
                key, echar;
        for (key in newEntities) {
            echar = newEntities[key];
            entityToChar[key] = echar;
            charToEntity[echar] = key;
            charKeys.push(echar);
            entityKeys.push(key);
        }
        charToEntityRegex = new RegExp('(' + charKeys.join('|') + ')', 'g');
        entityToCharRegex = new RegExp('(' + entityKeys.join('|') + '|&#[0-9]{1,5};' + ')', 'g');
    }

    function htmlEncode(value) {
        var htmlEncodeReplaceFn = function (match, capture) {
            return charToEntity[capture];
        };

        return (!value) ? value : String(value).replace(charToEntityRegex, htmlEncodeReplaceFn);
    }

    function htmlDecode(value) {
        var htmlDecodeReplaceFn = function (match, capture) {
            return (capture in entityToChar) ? entityToChar[capture] : String.fromCharCode(parseInt(capture.substr(2), 10));
        };

        return (!value) ? value : String(value).replace(entityToCharRegex, htmlDecodeReplaceFn);
    }

    resetCharacterEntities();

    return {
        htmlEncode: htmlEncode,
        htmlDecode: htmlDecode
    };
})();
