console.log("backend event handlers");
var file_uploader_backend_event_handlers = {};

(function () {

    file_uploader_backend_event_handlers.uploadFiles = function (uploadForm, onUploadDoneCallback) {

        $(uploadForm).ajaxForm(function (responseData) {
            console.log(responseData);
            responseData = $.parseJSON(responseData);

            // console.log($(uploadForm).find(".generic-file-upload").html());
            $(uploadForm).find(".generic-file-upload").val("");


            onUploadDoneCallback(responseData);

        }).submit();
    };

    file_uploader_backend_event_handlers.registerBackendEvents = function (fileData) {
        $(".fl-file-delete").click(function () {
            removeAttachment(fileData);
        });
    };

    function removeAttachment(fileData) {
        var fileName = fileData.name;
    }

}());
