
	var pathname = window.location.pathname;
	// pathname == "/user_view/forma_chat" || 
    if (pathname == "/user_view/app_configuration" || pathname =="/user_view/forma_chat" || pathname == "/user_view/message-app" || pathname == "/user_view/test_chat") {
    }else{
var chat_tab_on = [];
var continueToLoad = {};
(function () {
	var chat_window_template= "";

	// setInterval(function(){
	//             updateGroupChats();
	//             updateChatUsers();
	//         },20000);

	window.addEventListener('resize', setWindowSize);
		function setWindowSize() {
			var window_width= $(window).width() - 280;
			// console.log("original width",$(window).width())
			// console.log(window_width)
			$("#test_window_chat").width(window_width);
			// console.log("test chat width",window_width);
			// console.log("Current Width",$("#test_window_chat").width())
			display_tabs();
		}

	$(document).on('click','.refresh_page',function(){
		location.reload();
	});
	$(document).on('click','.cancel_newName',function(){
		var gthread_id = $(this).parents('.chat_tabs')
		cancelNewNameEdit(gthread_id);
	});

	$(document).on('click','.edit_group',function(){
		// groupNewName(this);
		// $('.settings_tab').trigger('click');
		$(this).parents('.obj_actions').toggleClass('display');
		var parent_window = $(this).parents('.chat_tabs');
		// console.log($(parent_window).attr('data-chat-type'));

		if($(parent_window).attr('data-chat-type') == "group"){
			$(parent_window).find('.chat_title').hide();
			$(parent_window).find('.chat_title_input').show();
		}
	});

	$(document).on('click','.save_newName',function(){
		var gthread_id =$(this).parents('.chat_tabs').attr('data-thread-id');
		var gNewName_input = $(this).siblings('.newName')
		groupNewName(gNewName_input,gthread_id);
	});   

	$(document).on('keydown','.newName',function(){
		if(event.which==13){
			var gthread_id =$(this).parents('.chat_tabs').attr('data-thread-id')
			groupNewName(this,gthread_id);
		}
	});

	$(document).on('change','.fl-widget-name-selector',function(){
		// console.log("hey hey hey hey");
		if($('#name_container').height() > 50){
			$('.message_content_NewMessage ').perfectScrollbar('update')
		}
	});

	$(document).on('keydown','.chat_tabs',function(event){
		if(event.which == 27){
			$('.chat_tabs').eq(0).find('.remove_tab').trigger('click')
		}else{

		}
	});

	$(document).on("mouseenter",".chat_button_show",function(){
		// console.log($(this))
		if($(this).parents(".chat_tabs").hasClass("chat_hide")){
			// console.log("SHOW!")`
			$(this).attr("title","Maximize")
		}else{
			// console.log("HIDE!")
			$(this).attr("title","Minimize")
		   // $(this).parents(".chat_tabs").find('chattext').attr("title","Click To Minimize.")           
		}
	})

	$(document).ready(function(){
		$('.fl-user-conversation-time').timeago();
		setWindowSize();
	});

	$(document).on('click','#online_count',function(){
	
		// console.log("he");
		$('.forma-online-list').show();
		$('#online_count').hide();
		if($('#online_list').height() > 239){
				$('#list_container').perfectScrollbar();
			}
	});

	$('body').on('click','.chat_container',function(){
		// chatWindowFocus();
		var threadId = $(this).parents('.chat_tabs').attr('data-thread-id');

		markThreadAsRead(threadId, getUnreadCountByThreadId(threadId));
		// console.log("ThreadID",threadId)
		updateGroupChats();
		updateChatUsers();

	});

	$(document).on('keydown','.chat_text',function(event){
		if(event.which==13){
			console.log(this)
			sendMessage(this);

		}else{
			var this_tab = $(this).parents('.chat_tabs');
			var this_tab_unread = $(this_tab).find('.chat-notification-count').html();
			console.log("unread",$(this_tab).attr('data-unread'));
			if(this_tab_unread != "" ){
				userTyping(this_tab);
			}

		}

	})

	$(document).on('keydown','.new_chat_text',function(event){
		if(event.which==13){
			sendMessageNew(this);
		}
	})

	$(document).on('keyup','#txt-search-user',function(event){

		var inputSearch = $(this).val().trim().toUpperCase();
		$('#online_list').find(".user-online").each(function(){
			$(this).show();    
			var onlineUserName = $(this).attr("data-keyword").toUpperCase();
			if(onlineUserName.search(inputSearch) < 0){
				$(this).hide();
			}
		if($('#fl-ul-online-users').find('.user-online').filter(':visible').length < 1){
			// console.log("none!")
			$('#list_container').scrollTop('0');
			$('#nomatch').show();

		}else{
			$('#nomatch').hide();
		}

		if($('#forma_groups').find('.user-online').filter(':visible').length < 1){
			// console.log("none!")
			$('#list_container').scrollTop('0');
			$('#nomatchg').show();

		}else{
			$('#nomatchg').hide();
		}

		if($('#txt-search-user').val() != "" && $('.user-online').filter(':visible').length > 0 )
			if(event.which==13){
				// console.log("hey!");
				$('.user-online:visible ').eq(0).trigger('click');
			}
		
		})
		console.log("group count",$('#forma_groups').find('.user-online').filter(':visible').length);
		console.log("user count",$('#fl-ul-online-users').find('.user-online').filter(':visible').length);

	});

	$(document).on('click','#chat_header',function(){
	
		// console.log("he");
		$('.forma-online-list').hide();
		$('#online_count').show();
	});

	$(document).on('click','.more_dudes',function(){
		// console.log("MORE")
		$('.more_dudes').toggleClass('more_hide')
		if($('.more_dudes').hasClass('more_hide') == true){
			$('.more_users').hide()
		}else{
			$('.more_users').show()
		}
	});

	$(document).on('click','.more-users',function(){
		$(this).parent().remove();
	})

	$(document).on('click','.remove_tab',function(){
		var toberemove = $(this).parents('.chat_tabs').attr('data-chat-name')
	   for(var i in chat_tab_on){
			if(chat_tab_on[i].chat_name == toberemove){
				chat_tab_on.splice(i, 1);
				break;
			}
		}
		$(this).parents('.chat_tabs').remove();
		display_tabs();
	});

	$(document).on('click','.chat_button_show',function(){
		// console.log('clicked!')
		$(this).parent('.chat_tabs').toggleClass("chat_hide");
		if($(this).parent('.chat_tabs').hasClass("chat_hide") == true){
			$(this).siblings('.chat_container').hide();         
			// console.log('d2 hide?')
		}else{
			// console.log("eto hide?")
			$(this).siblings('.chat_container').show();
		// var threadId = $(this).parent('.chat_tabs').attr("data-thread-id");
		// markThreadAsRead(threadId, getUnreadCountByThreadId(threadId));
		}
	});

	$(document).on('click','.user-online',function(){
		// console.log("User Clicked",$(this).attr("data-thread-id"));
		var chat_name = $(this).attr("data-keyword") || $(this).attr('tname') || "Group-" + $(this).attr("thread-id") ;
		var chat_id=$(this).attr('data-user-id') || $(this).attr('subscribers');
		var chat_thread_id=$(this).attr('thread-id') || $(this).attr('thread-id');
		var chat_unread=$(this).attr('data-unread-count')  || $(this).attr("unread-count");
		var isTabExist = $("[data-chat-name='"+chat_name+"']").length > 0;
		var msg_type = $(this).attr('data-msg-type');
		var sub_typ = $(this).attr('chat-type');
		var subs_name = $(this).attr('subscribernames');

		if(!isTabExist){
		  var chat_tab_data = {
				chat_name:  chat_name,
				chat_id:    chat_id,
				chat_thread_id:      chat_thread_id,
				chat_unread: chat_unread,
				chat_type: msg_type,
				sub_type : sub_typ,
				subs_name : subs_name
			};
		// console.log(chat_tab_data);

			for(var i in chat_tab_on){
				if(chat_tab_on[i].chat_name == chat_name){
					chat_tab_on.splice(i, 1);
					break;
				}
			}

			chat_tab_on.unshift(chat_tab_data);
			// console.log(chat_tab_on);
			display_tabs();


		}else{
			// $("[data-chat-name='"+chat_name+"']").find('.chat_button_show').trigger('click');

		}
		console.log(chat_tab_on);
		// appendChatTab(chat_tab_data);
	})

	$(document).on('click','.add_User',function(){
		// console.log("wa",this)
		
		$(this).parents('.obj_actions').toggleClass('display');
		var threadId = $(this).parents('.chat_tabs').attr('data-thread-id');
		// console.log("thread id ",$(this).parents('.chat_tabs').attr('data-thread-id'));
		var chat_tab_window = $(this).closest('.chat_tabs');
			chat_tab_window.find('.fl-widget-name-selector').toggleClass('display');
			chat_tab_window.find('.addUserDiv').toggleClass('display');
			chat_tab_window.find('.msg_content').toggleClass('display');
			chat_tab_window.find('.window_footer').toggleClass('display');
		var subsnames =$(".user-online[thread-id='"+threadId+"']").attr('subscribernames').split(","); 
		// console.log("subs",subsnames);
		var ret ='';
		for(var s in subsnames){
			ret += '<li style="margin-top:3px"><span>'+ subsnames[s]+'</span></li>';
		}
		$(this).parents('.chat_tabs').find('.label-readers').append(ret);
	})

	$(document).on('click','.addUserButton',function(){
		var self2 = $(this).closest('.chat_tabs')
		var threadId = $(this).closest('.chat_tabs').attr('data-thread-id');
		var chattextbox = $(self2).find('.chat_text'); 
		var data2 = [];
		var tobeadded = $(self2).find('.recipientList').select2('data').map(function(val){
			return val.id
		});
		console.log("tobeadded",tobeadded,threadId);
	if($(self2).find('.recipientList').select2('data').length > 0){

		var url = '/ajax/addremoveuser_collab';
		parameters ={
			thread_id : threadId,
			tobeadded : tobeadded
		}
		var recp = tobeadded.toString();
		// console.log('parameters',parameters,chattextbox,recp)

		$.post(url, parameters, function (data){
		// 	// console.log("data",data);
			var recips = self2.attr('data-user-id');
			self2.attr('data-user-id',recips+','+recp);
		// 	console.log("recps",self2.attr('data-user-id'),"data",data);
			updateGroupChats();
			updateChatUsers();
			chattextbox.val(data)
			sendMessage(chattextbox);
		});
		// console.log($(self2).find('.add_User.pull-right'))
		$(self2).find('.add_User').eq(1).trigger('click');
		// $(self2).find('.settings_tab').trigger('click');
		// $(self2).find('.recipientList').val(null).trigger("change");

		// label-readers
		}else{
			alert("Please Select A User to Be Added!");	
		}
	})

	$(document).on('click','.minimize_tab',function(){
		console.log($(this))
		$(this).siblings('.chat_button_show').trigger('click');
	})

	$(document).on('click','.settings_tab',function(e){
		
		var tab_id = $(this).parent(".chat_tabs")
		tab_id.find('.obj_actions').toggleClass('display');
		// console.log('clicked')
	})

	$(document).on("change",".recipientList",function(){
		console.log("IAN FUCKBOI!!")
	 	// console.log("this",$(this).parents('.chat_tabs').attr('data-thread-id'));
	 	var threadId = $(this).parents('.chat_tabs').attr('data-thread-id');
	 	var threadusers = $('.user-online[thread-id="'+threadId+'"]').attr('subscribernames').split(',');
	 	var datauserid= $(this).parents('.chat_tabs').attr('data-user-id').split(',');
	 	// console.log("threadusers",threadusers);
	 	// console.log("this select",$(this).next('.select2-container').find('.select2-selection__rendered').children('.select2-selection__choice'));
	 	for( t in threadusers){
 		console.log('Before',threadusers[t],$('.select2-selection__choice[title="'+ threadusers[t]+'"]').children('.select2-selection__choice__remove').length)
	 		$('.select2-selection__choice[title="'+ threadusers[t]+'"]').children('.select2-selection__choice__remove').remove();
 		console.log('After',threadusers[t],$('.select2-selection__choice[title="'+ threadusers[t]+'"]').children('.select2-selection__choice__remove').length)
	 	}
	});

	$(document).on('select','.recipientList',function(){
		console.log("ULTRA! POWER!");
	})


	function bindSelect2(threadId){
		console.log("find?",threadId,$('.chat_tabs[data-thread-id="'+threadId+'"]').find('.recipientList'));
		var datausers = [];
		var threadusers = $('.user-online[thread-id="'+threadId+'"]').attr('subscribernames').split(',');
		// console.log('threadusers',threadusers);
		var threadusersid = $('.user-online[thread-id="'+threadId+'"]').attr('subscribers').split(',');
		threadusersid.shift();
		// console.log("threadusersid",threadusersid);
		for(s in threadusersid){
			 datausers.push({text:threadusers[s], id:threadusersid[s]})
		}
		// console.log("datausers",datausers);
		$('.chat_tabs[data-thread-id="'+threadId+'"]').find('.recipientList').select2({
		placeholder: 'Select Recipient:',
		delay: 1000,
		data : datausers,
	    // allowClear: true,	
		ajax: {
		    url: '/ajax/get-user',
		    dataType: 'json',

		    processResults: function (data) {
		    	// console.log("console",$('.chat_tabs[data-thread-id="'+threadId+'"]').find('.chat_container'))
		    	// $('.chat_tabs[data-thread-id="'+threadId+'"]').find('.chat_container').perfectScrollbar('update');
		    	// console.log($('#recipients').attr('data-user-id'));
		    	// console.log(this.$element.val());
		    	// console.log("data",data)
		    	var addedExclusions = ($('.chat_tabs[data-thread-id="'+threadId+'"]').attr('data-user-id')||"").split(",");
	    		var self = this.$element;
		    	return {
			        results: data.filter(function(val){
			    		return (self.val()||[]).concat(addedExclusions).indexOf(val.id) <= -1;
			    	})
			    };
		    }
		  }
		})
		// .val(threadusersid).trigger('change');
		// $('.select2-selection__choice__remove').hide();


	}


    function bindEventsForDynamicElements() {

        $('body').on('click', '.fl-file-preview-image', onClickMessagePreview);
    }

    function onClickMessagePreview() {
        var thisID = $(this).attr('src');
        //alert(thisID);
        $(this).colorbox({rel: thisID, width: "75%", height: "80%"});
    }

	function appendNewChatTab(y,x){
		// console.log(chat_window_template);
		// console.log($('.chat_tabs').length);
		// console.log($('#test_window_chat').width());
		// // console.log(chat_tab_data)
		var chat_name = chat_tab_on[y].chat_name;
		if(chat_tab_on[y].chat_thread_id == undefined){
			var chat_thread_id= "";
		}else{
			var chat_thread_id= chat_tab_on[y].chat_thread_id;
		};
		var chat_id = chat_tab_on[y].chat_id;
		var chat_type = chat_tab_on[y].chat_type;
		var tab_count = $('.chat_tabs').length;
		// var chat_thread_id = "NewMessage";
		// var chat_name = "New_Message_Name";
		// var chat_id = "New_Message_ID";

		chat_window_template  = '<div class="chat_tabs" id="fl-thread-data" data-thread-id="'+chat_thread_id+'" data-chat-name="'+ chat_name +'" data-msg-type="'+ chat_type +'" data-user-id="'+ chat_id+'" style="right:'+x +'px;">';
		chat_window_template +=     '<div class="chat_button_show"><i class="minimize_tab fa fa-minus" aria-hidden="true"></i><i class="remove_tab fa fa-times"></i>';
		chat_window_template +=          '<div><span class="chattext" style="position:inherit!important;">New Message</span></div>';
		chat_window_template +=     '</div>';
		chat_window_template +=     '<div  class="chat_container">';
		chat_window_template +=         '<div id="chattab_'+chat_thread_id+'" class=" msg_content message_content_'+chat_thread_id +' " style="">';
				 
					 chat_window_template +='<div class="fl-widget-name-selector">';
						chat_window_template += '<span class="label-readers"></span>';
						 chat_window_template += '<div id="name_container"><div class="fl-selected-names-container"></div></div>';    
						 chat_window_template += '<input autofocus="autofocus" type="text" id="" class="input-small fl-autocomplete-input" placeholder="Enter recipient here:" data-tag-id="0" style="width:100%;">';
						 chat_window_template += '<div class="fl-autocomplete-loading" style="display: none;"><i class="fa fa-refresh fa-spin"></i></div>';
						 chat_window_template += '<div class="fl-widget-name-selector-result"></div>';
					 chat_window_template += '</div>';
					 	// chat_window_template +='<select class="recipientList js-data-example-ajax" multiple="multiple"></select>';

		chat_window_template +=         '</div>';
		// chat_window_template += '<div class="window_footer">';
		chat_window_template +=         '<div class="typing_cont"><div hidden class="typing_div"> is typing...</div></div>'
		chat_window_template += '<div class="uploaded-files" style="width:100%;bottom:0px;"></div>';
			chat_window_template += '<div class="chat-type-content" >';
				chat_window_template += '<span>';
					chat_window_template += '<input  type="text" placeholder="Type your message here" class="fl-btn-send-reply fl-input-text new_chat_text" style="">';
					chat_window_template += '<button class="file-upload-btn" title="Attach files"><i class="fa fa-paperclip"></i></button>';
				chat_window_template += '</span>';
		chat_window_template += '</div>';
		// chat_window_template += '</div>';
		chat_window_template += '   </div>';


		
		if($('[data-chat-name="'+ chat_name +'"]').length > 0){

			$('[data-chat-name="'+ chat_name +'"]').children('.chat_button_show').trigger('click');
		}else{

		var template_element = $(chat_window_template)  
		$(template_element).prependTo('#test_window_chat');
		bindUserSelectionFilterOnChangeEvent(onSearch,template_element);
		// bindSelectSearch($(template_element).find('.recipientList'));
		// console.log("appended successfully");
		var new_upload_id = chat_name.replace(/ /g, "");
			var url = "/file/file_uploader_widget";
			var params = {
				file_uploader_id: "fl-message-upload-"+chat_thread_id,
				click_trigger_class: "file-upload-btn",
				target_directory: "message_attachments"
			};


			$.post(url, params, function (uploadedFilesPreviewHTML){
				// console.log("upload?")
				var nonScript = $(uploadedFilesPreviewHTML).not("script");
			 $("[data-chat-name='"+chat_name+"']").find(".uploaded-files").html(nonScript);
			// rebindLooseEvents();
				uploadEvent(chat_thread_id);
			});
		
		// });
			$(template_element).find('.msg_content').on('scroll',function(){
				// console.log($(this).parents('.chat_tabs').attr('data-chat-name'))
				var chat_name = $(this).parents('.chat_tabs').attr('data-chat-name')
				onScrollThread(chat_thread_id)
			});

		}
		$('.message_content_NewMessage').perfectScrollbar();
		// rebindLooseEvents();
	}

	function appendChatTab(y,x){
		// console.log(chat_window_template);
		// console.log($('.chat_tabs').length);
		// console.log($('#test_window_chat').width());
		//console.log(chat_tab_on)
		var chat_name = chat_tab_on[y].chat_name;
		if(chat_tab_on[y].chat_thread_id == undefined){
			var chat_thread_id= "";
		}else{
			var chat_thread_id= chat_tab_on[y].chat_thread_id;
		};
		var chat_sub_names = chat_tab_on[y].subs_name.split(",");
		var chat_id = chat_tab_on[y].chat_id;
		var chat_typ= chat_tab_on[y].sub_type;
		var tab_count = $('.chat_tabs').length;
		console.log("name",chat_sub_names);

		chat_window_template  = '<div class="chat_tabs" data-thread-id="'+ chat_thread_id +'" data-chat-type="'+ chat_typ +'" data-chat-name="'+ chat_name +'" data-unread="" data-user-id="'+ chat_id+'" style="right:'+x +'px;">';
		chat_window_template += '<i class="remove_tab fa fa-times" title="Close" ></i>';
			if(chat_tab_on[y].sub_type == "group"){
				chat_window_template += '<i class="settings_tab fa fa-cogs" title="Settings"></i>';	
			}else{
				chat_window_template += '<i class="minimize_tab fa fa-minus" title="Minimize"></i>';			
			}
				chat_window_template += '<div class="setup_tab" style="height: 20px;position: absolute;width: auto;margin-top: 30px;right: 2px;z-index: 5;color: #fff;">';
					chat_window_template += '<div class="obj_actions display" id="" style="margin-top: 1px;border-radius:4px;">';
						chat_window_template +=	'<ul class="fl-options-data-type">';
							chat_window_template += '<li class="add_User"><i class="fa fa-plus pull-left cursor " title="Add Member" ></i></li>';
							chat_window_template += '<li class="edit_group"><i class="fa fa-pencil cursor " title="Edit Group Name"></i></li>';
						chat_window_template += '</ul>';
					chat_window_template += '</div>';
				chat_window_template += '</div>';
		chat_window_template += '<div hidden style="padding-top: 4px;"class="chattext chat_title_input elip" ><input required class="newName" type="text" value="'+chat_name+'">';
		chat_window_template +=			'<i style="padding-left:3px;font-size: medium!important;" class="save_newName fa fa-check-circle" aria-hidden="true"></i>';
		chat_window_template +=			'<i style="padding-left:3px;font-size: medium!important;" class="cancel_newName fa fa-times-circle" aria-hidden="true"></i></div>';
		chat_window_template +=     '<div class="chat_button_show" title="!">';
		chat_window_template += 		 '<div class="chattext chat_title elip" style="text-overflow: ellipsis;">'+chat_name+'</div>';
		chat_window_template +=          '<div><div class="chat-notification" style="display:none"><div class="chat-notification-count"></div></div></div>';
		chat_window_template +=     '</div>';
		chat_window_template +=     '<div  class="chat_container" style="height: 290px;">';
		// chat_window_template += '<div class="taggingdiv">';
					 chat_window_template +='<div class="recipientselect fl-widget-name-selector display" style="box-shadow:none!important;border:none!important;overflow:hidden;">';
					 	chat_window_template +='<select class="recipientList js-data-example-ajax" multiple="multiple" placeholder="Select Recipient/s:" style="width:200px;max-width:200px;height:30px;overflow:hidden;"></select>';
 						chat_window_template +='<div style="height:20px;"><span style="margin-left:10px;vertical-align: -webkit-baseline-middle;">Current Members:</span></div>';
 						chat_window_template +='<div style="max-height:115px;overflow-y:auto;">';
 							chat_window_template += '<ul style="margin-left:10px;" class="label-readers"></ul>';
 						chat_window_template +='</div>';
					chat_window_template += '</div>';
					chat_window_template +='<div class="addUserDiv display" style="bottom: 0;position: absolute;width: 100%;"><input class="add_User pull-right fl-buttonEffect	" type="button" value="Cancel" style="width:49%;"><input class="addUserButton fl-buttonEffect" type="button" value="Add User/s" style="width:50%;"></div>';
		// chat_window_template += '</div>';			
		chat_window_template +=         '<div id="chattab_'+chat_thread_id+'" class="msg_content message_content_'+chat_thread_id +' " style="">';

		chat_window_template +=         '</div>';
		chat_window_template +=         '<div class="window_footer">';
		chat_window_template +=         '<div class="typing_cont"><div hidden class="typing_div"><span> ...</span></div></div>'
		chat_window_template += '<div class="uploaded-files" style="width:100%;bottom:0px;"></div>';
			chat_window_template += '<div style=""  class="chat-type-content">';
				chat_window_template += '<span>';
					chat_window_template += '<input type="text" placeholder="Type your message here" class="fl-btn-send-reply fl-input-text chat_text">';
					chat_window_template += '<button class="file-upload-btn" title="Attach files"><i class="fa fa-paperclip"></i></button>';
				chat_window_template += '</span>';
		chat_window_template += '</div>';
		chat_window_template += '</div>';
		chat_window_template += '   </div>';


		
		if($('[data-chat-name="'+ chat_name +'"]').length > 0){

			$('[data-chat-name="'+ chat_name +'"]').children('.chat_button_show').trigger('click');
		}else{

		var template_element = $(chat_window_template)  
		$(template_element).prependTo('#test_window_chat');
		// console.log("template",$(template_element).attr('data-thread-id'))
		var threadId = $(template_element).attr('data-thread-id');
		// console.log('bind',$('.chat_tabs[data-thread-id="'+threadId+'"]'));
		// bindSelectSearch($(template_element).find('.recipientList'));
		// bindUserSelectionFilterOnChangeEvent(onSearch,template_element);
		var threadMessagesWrapper = $("[data-chat-name='"+chat_name+"']").find(".msg_content");
		loadThreadMessages(chat_thread_id, threadMessagesWrapper);
		bindSelect2(threadId);
		// console.log("appended successfully");
		var new_upload_id = chat_name.replace(/ /g, "");
		// console.log("ID Upload", new_upload_id)
			var url = "/file/file_uploader_widget";
			var params = {
				file_uploader_id: "fl-message-upload-"+chat_thread_id,
				click_trigger_class: "file-upload-btn",
				target_directory: "message_attachments"
			};


			$.post(url, params, function (uploadedFilesPreviewHTML){
				// console.log("upload?")
				var nonScript = $(uploadedFilesPreviewHTML).not("script");
			 $("[data-chat-name='"+chat_name+"']").find(".uploaded-files").html(nonScript);
				uploadEvent(chat_thread_id);
			});
		
			$(template_element).find('.msg_content').on('scroll',function(){
				// console.log($(this).parents('.chat_tabs').attr('data-chat-name'))
				var chat_name = $(this).parents('.chat_tabs').attr('data-chat-name')
				onScrollThread(chat_thread_id)
			});

		}
	}

	function uploadEvent(chat_thread_id){
		// console.log("upload event");
		// console.log(chat_name)
		var selectedTabByName = $("[data-thread-id='"+chat_thread_id+"']");
		var uploadBtn = $(selectedTabByName).find(".file-upload-btn");
		var fileUploaderWrapper = $(selectedTabByName).find(".fl-file-uploader-widget")
		var threadId = $(selectedTabByName).attr("data-thread-id");
		var new_upload_id = chat_thread_id.replace(/ /g, "");
		var uploadWidgetId = "fl-message-upload-"+chat_thread_id;
		var uploadWidgetUploadInput = $("#" + uploadWidgetId).find(".generic-file-upload");
		var uploadWidgetUploadForm = uploadWidgetUploadInput.parent(".uploader");
		file_uploader.alreadyUploadedFiles[uploadWidgetId] = [];

		// register upload button click event
		$(uploadBtn).unbind('click').click(function () {
			// console.log("file upload button clicked");
			file_uploader_ui_event_handlers.onUploadWidgetReady(uploadWidgetId);
			// console.log("widget baby",uploadWidgetId)
			uploadWidgetUploadInput.click();
		});

		uploadWidgetUploadInput.on('change',function () {
			// console.log("uploadWidgetUploadInput changed");
			var uploadedData = this;

			file_uploader_ui_event_handlers.onUploadWidgetReady(uploadWidgetId);

			var continueUpload = file_uploader_ui_event_handlers.onFileSelectedHandler(uploadedData);

			if (continueUpload) {
				file_uploader_backend_event_handlers.uploadFiles(
						uploadWidgetUploadForm,
						file_uploader_ui_event_handlers.onFileUploadingDone
						);
			}
		});
	}

	function display_tabs(){
		// console.log("display")
		$('.li-more-users').remove();
		$('.more_dudes').hide();
		var x,y;
		// var left_nav_bar =  parseInt($('.fl-infinite-nav-wrapper:visible').width());
		var screen_end2= parseInt($('#test_window_chat').css('width')) - 40;
		var screen_size = parseInt($("#test_window_chat").css("width"))
		// var screen_end = parseInt($(selectedTabByName).css('right'));
		var chat_tab_len = $('.chat_tabs').length;
		var z = chat_tab_on.length;
		// console.log(z);
		for(y=0,x=230; y< z ; y++,x+=230){
			// console.log("x",x)
			// console.log("y",y)
			if(chat_tab_on[y].chat_thread_id == ""){
			   var new_id =$("[data-keyword='"+chat_tab_on[y].chat_name+"']").attr('thread-id');
			   chat_tab_on[y].chat_thread_id = new_id;
			}
			// console.log(chat_tab_on[y].chat_name)
			var selectedTabByName = $("[data-chat-name='"+chat_tab_on[y].chat_name+"']");
			if(x > screen_end2){
				$(selectedTabByName).remove();
					var appendtotabs  = '<li class="li-more-users">';
					appendtotabs +='<div class="user-online more-users" data-keyword="'+chat_tab_on[y].chat_name+'"   thread-id="'+chat_tab_on[y].chat_thread_id+'" data-user-id="'+ chat_tab_on[y].chat_id +'" data-unread-count="'+ chat_tab_on[y].chat_unread +'" data-msg-type="'+ chat_tab_on[y].chat_type +'">';
					appendtotabs += chat_tab_on[y].chat_name+'</div>';
					appendtotabs += '</li>';
					$(appendtotabs).appendTo('#more_users');
					$('.more_dudes').show();
			}else{
				if($(selectedTabByName).length > 0 ){
					// console.log("exists")
					$(selectedTabByName).css("right", x+"px");        
				}else if(chat_tab_on[y].chat_type == "New"){
					// console.log("New")
					appendNewChatTab(y,x)
				} else{
					// console.log('OLD daw')
					appendChatTab(y,x);
				}
			}
			var more_right =$('.chat_tabs').length * 230;
			$('.more_dudes').css('right',more_right+230);
				if(parseInt($('#more_users').css("height")) > 279){
					// console.log("more height",$('#more_users').height())
					$('.more_users').css("height","279px");
					$('.more_users').perfectScrollbar();
			}
		}
		   if($('.chat_tabs:first').attr('data-msg-type') == 'New'){
				$('.chat_tabs:first').find('.fl-autocomplete-input').focus();
		   }else{
				$('.chat_tabs:first').find('.chat_text').focus();
		   }
	} 

	function sendMessage(textBox){
		var url = "/messaging/write_message";
		var selectedTab = $(textBox).closest(".chat_tabs");
		// console.log("tab",selectedTab)
		if($(textBox).hasClass('newName')){
			var newName_input = $(selectedTab).find(".newName").val(); 
			var message = "has renamed this conversation to '"+newName_input+"'";
		}else{
			var message = $(selectedTab).find("input.fl-input-text").val();          
		}

		console.log("msg",message)
		var tabName = $(selectedTab).attr("data-chat-name");
		console.log("tabname",tabName)
		var threadId = $(selectedTab).attr("data-thread-id");
		console.log("threadId",threadId) 
		var new_upload_id = $(selectedTab).attr("data-chat-name").replace(/ /g, "");
		console.log("new upload id ",threadId)
		var attachments = getAttachmentFileNames(threadId);
		var uploadWidgetId = "fl-message-upload-"+threadId;
		var recipients = $(selectedTab).find("#fl-thread-data").attr("thread-other-subscribers")  || $(selectedTab).attr("data-user-id");
		var nThreadId = '';
		console.log("attachments",attachments)
		// alert(recipients); // empty po to
		// var recipients = $(selectedTab).attr("data-user-id");

		if (message && message.trim() !== ''|| attachments) {

			var parameters = {
				message: message,
				attachments: attachments,
				requested_response: "HTML"
			};

			// if thread already exists
			if(threadId){
				var triggerType = "newReplySent";
				parameters.thread_id = threadId;      
				var triggerRecipients = recipients;       
			} else{
				var triggerType = "newMessageSent";

				var triggerRecipients = [{
					type: "1",
					display_text: tabName,
					value: recipients
				}];
				// console.log(triggerRecipients)
				parameters.recipients = triggerRecipients /*recipients.split(",")*/;
			}
			// console.log(parameters.recipients)
			console.log(parameters);
			// console.log(triggerType);

			$.post(url, parameters, function (messageViewHTML) {
				// console.log(messageViewHTML);
				// nThreadId = $(messageViewHTML).attr('thread-id');
				if(triggerType == 'newMessageSent'){
					threadId = $(messageViewHTML).attr("thread-id");
					// console.log("threadId",threadId)
					$(selectedTab).attr("data-thread-id", threadId);
					// console.log("selectedTab",selectedTab)
					continueToLoad[threadId] = true;
					// loadMore[threadId] = {
					//     start_index: 0,
					//     continueToLoad: true
					// };
				}

				// increment load more index
				// loadMore[threadId].start_index += 1;
				addMessageHTMLToCorrespondingThread(threadId, tabName, messageViewHTML, triggerType);

				$.event.trigger({
					type: triggerType,
					recipients: triggerRecipients,
					threadId: threadId,
					sendTo: "0",
					messageHTML: messageViewHTML,
					message_raw: message
				});
				
			});

			$(textBox).closest(".chat_tabs").find("input.fl-input-text").val("");
			file_uploader.clearAttachments(uploadWidgetId);
			// console.log("newThreadId",nThreadId)
			// $('#ghost_user').attr("thread-id",nThreadId).attr("data-keyword",recipients[0]["display_text"]).attr("data-user-id",recipients[0]["value"]);
					// $('#ghost_user').trigger("click");
			updateChatUsers();
			// display_tabs();
		}

		// console.log("send message successful");
	}

	function sendMessageNew() {
		// console.log("hahah")
		var url = "/messaging/write_message";
		var message = $(".new_chat_text").val();
		var selectedTab = $(".new_chat_text").closest(".chat_tabs");
			// var recipients = getSelectedUserIdList();
		var newThreadId = '';
		var threadId = $(selectedTab).attr("data-thread-id");
		var new_upload_id = $(selectedTab).attr("data-chat-name").replace(/ /g, "");
		var attachments = getAttachmentFileNames(threadId);
		var recipients = [];	
			$("#chattab_NewMessage").find(".fl-selected-names-container").find(".fl-selected-name").each(function () {
				recipients.push({
					type: $(this).attr("data-type-id"),
					display_text: $(this).find("span").html(),
					value: $(this).attr("data-id")
				});
			});
		//  validation
		if (recipients === null || recipients === "" || recipients == 0 || recipients.length <= 0) {
			alert("Please select recipients");
			return;
		}

		if (message.trim() === "" && !attachments) {
			alert("Your message cannot be empty");
			return;
		}

		console.log("recp",recipients);

		//  message sending
		var parameters = {
			message: message,
			attachments: attachments,
			recipients: recipients
		};
		// console.log(parameters)
		//  write the new message to server
		$.post(url, parameters, function (messageViewHTML) {
			// console.log('html',messageViewHTML);
			$.event.trigger({
				type: "newMessageSent",
				recipients: recipients,
				sendTo: "0",
				messageHTML: messageViewHTML
			});
			 // console.log(messageViewHTML)
			 newThreadId = $(messageViewHTML).attr('thread-id');
			 // console.log(newThreadId)
		});
		console.log('recp',recipients);
		$(".new_chat_text").val("")
		$.get("/portal/forma_group",{},function (list){
			$.get("/portal/forma_online_users", {}, function (html) {
				// console.log(newThreadId)
				// updateChatUsers();
				// updateGroupChats();
				$('#forma_groups').html(list);
				$('.online-list-single').html(html);
				$('[data-thread-id="NewMessage"]').find('.remove_tab').trigger('click');
				if($('#online_list').find("[thread-id='"+newThreadId+"']").length > 0){
					$('#online_list').find("[thread-id='"+newThreadId+"']").trigger('click');
				}else{
					$('#ghost_user').attr("thread-id",newThreadId).attr("data-keyword",recipients[0]["display_text"]).attr("data-user-id",recipients[0]["value"]);
					$('#ghost_user').trigger("click");
				}
			});
		});
		// clearNewMessageView();
			$('.fl-selected-names-container').html("")
	}

	function changeName (input_newname,threadId){
		console.log("textBox",input_newname);
		console.log("thread ID", threadId);
		var selectedTabChange = $('.chat_tabs[data-thread-id="'+threadId+'"]')
		var newChatName = $(input_newname).val();
		console.log(newChatName);
		var recievers = $('.chat_tabs[data-thread-id="'+threadId+'"]').attr('data-user-id');

			$.event.trigger({
				type: 'changeName',
				recipients: recievers,
				threadId: threadId,
				newName : newChatName
			});
	}

	function addMessageHTMLToCorrespondingThread(threadId, tabName, messageViewHTML, triggerType) {
		//  if the thread id is a valid thread id, reload the messages
		// console.log('add message to tab')
		// if (threadId && !isNaN(threadId)) {
			var selectedTabByName = $("[data-thread-id = '"+threadId+"']");
			if(triggerType == 'newReplySent'){
				// console.log("newreply")

				$(selectedTabByName).find(".fl-widget-msg-content-wrapper").append(messageViewHTML);
				var newtextifurl = $('.fl-user-msg').last().text().replace(/(\s|^|(https?:[\\\/]{2})?)[a-z0-9\-]+(?=\.|\\|\/)((\.|\\|\/)([a-z0-9\-_]|%\d{2})+)+(:\d+)?((\/[a-z0-9\-_]+)?|\/(?!\/))+(\.[a-z]+)?(\?[^\s]*)?(?=\s|$)/i,function(val){
					
					if(val.indexOf('http') == 0){
						return '<a href="'+$.trim(val)+'" target="blank">'+val+'</a>';
					}else{
						return '<a href="http://'+$.trim(val)+'" target="blank">'+val+'</a>';
					}

				});
				// console.log("url to kahet ilan",newtextifurl);
				$('.fl-user-msg').last().contents().filter(function() {
				 return this.nodeType === 3 && $.trim($(this).text()) != ""; }).replaceWith(newtextifurl);
				// $($('.fl-user-msg').last().contents().filter(function() { return this.nodeType === 3; }))
				// 	.replaceWith(newtextifurl);
				// $('.fl-user-msg').last().text(newtextifurl).append('<br><div class="section clearing"></div>');
			} else if(triggerType == 'newMessageSent'){
				// console.log("newMessage")
				$(selectedTabByName).find(".fl-widget-msg-content-wrapper").html(messageViewHTML);
				var newtextifurl = $('.fl-user-msg').last().text().replace(/(\s|^|(https?:[\\\/]{2})?)[a-z0-9\-]+(?=\.|\\|\/)((\.|\\|\/)([a-z0-9\-_]|%\d{2})+)+(:\d+)?((\/[a-z0-9\-]+)?|\/(?!\/))+(\?[^\s]*)?(?=\s|$)/g,"XXXXX")
			
			}
				// $("img").each(function(){
				//     $(this).attr("onerror","this.src='/images/avatar/small.png'");
				// });
			  
		// } else{
			// console.log("Invalid thread id, writing the message failed or there was a change in the template.");
			// console.log("Server responded with:");
			// console.log(messageViewHTML);
		// }
			if($('[data-chat-name="'+tabName+'"]').find(".hehehehe").height() > 256){
				// console.log("deams!")
				if($("#chattab_"+threadId).length == 0){
					// console.log("DEARM!")
					var threadId =  $('[data-chat-name="'+tabName+'"]').attr("data-thread-id")
					$('[data-chat-name="'+tabName+'"]').find('.msg_content').attr("id","chattab_"+threadId);
				}    
					$('[data-chat-name="'+tabName+'"]').find('.msg_content').perfectScrollbar()     
					var objDiv = document.getElementById("chattab_"+threadId);
					objDiv.scrollTop = objDiv.scrollHeight; 
				
			} 
		bindEventsForDynamicElements();                
		// console.log("add message html successful");
	}

	function getAttachmentFileNames(threadId) {
		// console.log("upload id",new_upload_id)
		var rawAttachmentFileNames = $('#fl-message-upload-'+threadId+'').attr('uploaded-filenames');

		if (rawAttachmentFileNames && rawAttachmentFileNames.trim() !== "") {
			return rawAttachmentFileNames.split(",");
		}

		return null;
	}

	function onScrollThread(chat_thread_id){
		// console.log("SCROLLED!")
		var selectedTab = $('.chat_tabs[data-thread-id="'+ chat_thread_id +'"]');
		// console.log(selectedTab)
		// var threadId = $(selectedTab).attr("data-thread-id");
		// console.log(threadId)
		var threadMessagesWrapper = $(selectedTab).find(".msg_content");
		// console.log(threadMessagesWrapper)
		var scrollTop = $(threadMessagesWrapper).scrollTop();

		if(scrollTop <= 10){
			// console.log(scrollTop)
			// $(this).unbind('scroll');
			// console.log("ew")
			loadMoreThreadMessages(threadId, threadMessagesWrapper);
			continueToLoad[threadId] = true
		}else{
			// continueToLoad[threadId] = false
		}
	}

	function loadThreadMessages(threadId, threadMessagesWrapper) {
		// console.log(threadId)

	   // if this thread has threadId
		// if(threadId)
		// 	continueToLoad[threadId] = true;

		// loadMore[threadId] = {
		//     start_index: 0,
		//     continueToLoad: true
		// };
		var url = "/messaging/chat_thread_messages";
		var params = {
			threadId: threadId,
			index:    $(threadMessagesWrapper).find("fl-msg-wraper").length
			// index: loadMore[threadId].start_index
		};

		$.get(url, params, function (threadMessagesHtml) {
			// console.log(threadMessagesHtml);
			// var newtextifurl = $(threadMessagesHtml).find('.fl-user-msg').text().replace(/(\s|^|(https?:[\\\/]{2})?)[a-z0-9\-]+(?=\.|\\|\/)((\.|\\|\/)([a-z0-9\-_]|%\d{2})+)+(:\d+)?((\/[a-z0-9\-]+)?|\/(?!\/))+(\?[^\s]*)?(?=\s|$)/g,function(val){
					
			// 		if(val.indexOf('http') == 0){
			// 			return '<a href="'+$.trim(val)+'" target="blank">'+val+'</a>';
			// 		}else{
			// 			return '<a href="http://'+$.trim(val)+'" target="blank">'+val+'</a>';
			// 		}

			// });				
			// 	$('.fl-user-msg').last().contents().filter(function() {
			// 	return this.nodeType === 3 && $.trim($(this).text()) != ""; }).replaceWith(newtextifurl);
			// 	console.log("url to kahet ilan",newtextifurl);
			// console.log("Msg Length",$(threadMessagesHtml).find('.fl-msg-wraper').length)
			var convo_length = $(threadMessagesHtml).find('.fl-msg-wraper').length
			var unreadCount = getUnreadCountByThreadId(threadId);
			// console.log(unreadCount)


				$('.chat_tabs[data-thread-id='+threadId+']').attr('data-unread',unreadCount);
				if(unreadCount > 0){
					$('.chat_tabs[data-thread-id='+threadId+']').find('.chat-notification').css('display','');
					if(unreadCount > 99){
						$('.chat_tabs[data-thread-id='+threadId+']').find('.chat-notification-count').html('99+')
					}else{
						$('.chat_tabs[data-thread-id='+threadId+']').find('.chat-notification-count').html(unreadCount)
					}
				}else{
					// $('.chat_tabs[data-thread-id='+threadId+']').find('.chat-notification').hide();
				}
				$(threadMessagesWrapper).html(threadMessagesHtml);
				
				bindEventsForDynamicElements();

					if(convo_length < 1 ){
						// console.log("NO CONVO!")
						var empty_convo = '<div style="text-align:center; margin-top:10px;width:100%">';
							empty_convo += '<h3><i class="fa fa-comments"></i>No previous conversation</h3></div>';
						$(threadMessagesWrapper).find('.fl-widget-msg-content-wrapper').html(empty_convo);
						$('.fl-widget-wrapper-scroll').css('border','0!important')
					}else{
						$('label.timeago').timeago()
						 // $(threadMessagesWrapper).find('.fl-widget-wrapper-scroll.fl-scroll-overflow.fl-widget-message-create-wrapper').css('display','inline-table');
							// if($(threadMessagesWrapper).find('.fl-msg-wraper').length > 4){
						   $(threadMessagesWrapper).find('.fl-widget-wrapper-scroll').css("border",'0');

							// console.log($(threadMessagesWrapper).find('.fl-widget-msg-content-wrapper').height())
							if($(threadMessagesWrapper).find('.fl-widget-msg-content-wrapper').height()  > 260){
								// console.log("sobra")
								var objDiv = document.getElementById("chattab_"+threadId);
								objDiv.scrollTop = objDiv.scrollHeight; 
								// $('.message_content_'+threadId+'').perfectScrollbar();
								$('.message_content_'+threadId+'').perfectScrollbar({minScrollbarLength: 50});
							}
							// $("img").each(function(){
							//     $(this).attr("onerror","this.src='/images/avatar/small.png'");
							// });
					}
		});

		var loading_panel = panel.load(threadMessagesWrapper);
		loading_panel.css("position", "relative");
		loading_panel.css("z-index", "9");
		
		// console.log("loaded thread successfully");
		// }else{

		// }
	}

	function loadMoreThreadMessages(threadId, threadMessagesWrapper){
		// console.log("load more thread messages");
		var url     = "/messaging/chat_thread_messages";
		var params  = {
			threadId: threadId,
			index:    $(threadMessagesWrapper).find(".fl-msg-wraper").length
		};
		// console.log(params.index)
		if(continueToLoad[threadId] == true){
			$.get(url, params, function (threadMessagesHtml) {
				var initialHeight    = $(threadMessagesWrapper).find(".fl-widget-msg-content-wrapper").height();
				// console.log("initialHeight",initialHeight)
				var moreMessagesHTML = $(threadMessagesHtml).find(".fl-widget-msg-content-wrapper").html();
				// console.log(moreMessagesHTML)
				// if messages are the last ones in the thread
				if($(threadMessagesHtml).find(".fl-msg-wraper").length < 11){
					continueToLoad[threadId] = false;
				}
								//  add more thread messages to view
				$(threadMessagesWrapper).find(".fl-widget-msg-content-wrapper").prepend(moreMessagesHTML);
				var finalHeight = $(threadMessagesWrapper).find(".fl-widget-message-create-wrapper").prop("scrollHeight");
				// console.log("finalHeight",finalHeight)
				$('.message_content_'+threadId+'').perfectScrollbar('update');
				$('.message_content_'+threadId+'').scrollTop(finalHeight - initialHeight);
				// find(".fl-widget-message-create-wrapper").scrollTop(finalHeight - initialHeight);
				// find(".fl-widget-message-create-wrapper").perfectScrollbar("update");
				// $('.message_content_'+threadId+'').perfectScrollbar('update');
			   	$('label.timeago').timeago()
				bindEventsForDynamicElements();               
			});
		}
	}

	function getUnreadCountByThreadId(threadId){
		return $(".user-online[thread-id='"+threadId+"']").attr("data-unread-count") || $(".user-online[thread-id='"+threadId+"']").attr("unread-count");
	}

	function markThreadAsRead(threadId, unreadCount){
		// console.log("mark thread as read");
		$('.chat_tabs[data-thread-id='+ threadId +']').find('.chat-notification-count').html('');
		$('.chat_tabs[data-thread-id='+ threadId +']').find('.chat-notification').css('display','none');
        $('.forma-online-list').find('.user-online[thread-id='+threadId+']').find('.chat-notif').hide();
		var url = "/messaging/change_thread_read_flag";
		var params = {
			thread_id: threadId,
			read_flag: 1,
		};

		$.post(url, params, function(){
			$.event.trigger({
				type: 'thread_opened',
				threadId: threadId,
				unreadCount: unreadCount
			});
		});
	}

	function userTyping (this_tab) {
		threadId = $(this_tab).attr('data-thread-id');
		recipients = $(this_tab).attr('data-user-id');
		sendering = $('#current-user-display-name');
		markThreadAsRead(threadId, getUnreadCountByThreadId(threadId))
		// console.log("Function Trigger Event");
			$.event.trigger({
				type: 'userTyping',
				recipients: recipients,
				threadId: threadId,
				sender:sendering
			});
	}

	function groupNewName(groupNewName,gthread_id){
		var newgroupNewName = $(groupNewName).val();
		var paren_id = $('.chat_tabs[data-thread-id='+gthread_id+']');
		var origName = $(paren_id).attr('data-chat-name');
		// console.log(paren_id);
		// console.log("Gname",newgroupNewName);
		// console.log("gthread_id",gthread_id);
	   
		if(newgroupNewName.replace(/ /g, "") ==""){
			alert("Group Name should be AtriggerUserTypingtleast 1 Character!");
		}else if(newgroupNewName.replace(/ /g, "").length > 12){
			alert("Group Name is Too Long!");
		}else if(/^[a-zA-Z0-9- ]*$/.test(newgroupNewName) == false){
			alert("Group Name contains illegal characters!");
		}else if(newgroupNewName == origName){
				$(paren_id).find('.chat_title_input').hide();
				$(paren_id).find('.chat_title').show();
		}else{
			var url = "/messaging/change_group_name";
			var params = {
				thread_id: gthread_id,
				thread_name : newgroupNewName
			};
			// console.log("kahit ano");
			$.post(url, params, function (data) {
				$(paren_id).find('.newName').attr('value',newgroupNewName);
				$(paren_id).find('.chat_title').html(newgroupNewName);
				$(paren_id).find('.chat_title_input').hide();
				$(paren_id).find('.chat_title').show();
				$(paren_id).attr("data-chat-name",newgroupNewName);
				updateGroupChats();
				for(var i in chat_tab_on){
					if(chat_tab_on[i].chat_thread_id == gthread_id){
						chat_tab_on[i].chat_name = newgroupNewName;
						break;
					}
				}
			// console.log($(paren_id).find('.newName'))
			var input_newname = $(paren_id).find('.newName');
			changeName(input_newname,gthread_id);
			sendMessage(input_newname)    
			});
		}
	}

	function cancelNewNameEdit(gthread_id){
		console.log('here!')
		// console.log(gthread_id.parents('.chat_tabs'))
		$(gthread_id).find('.chat_title_input').hide();
		$(gthread_id).find('.chat_title').show();
		$(gthread_id).find('.newName').attr('value',$(gthread_id).find('.chat_title').html());
	}
})();
}