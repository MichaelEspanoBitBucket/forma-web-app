    function checkSession(){
        var currentUser_ID = $('#list_container').find('#fl-ul-online-users').attr('data-auth-id');
        var currentUser_DisplayName = $('#fl-ul-online-users').attr('data-auth-name');
        var displayedName = $('#current-user-info').find('#current-user-display-name').html();
        var displayedID = $('#current-user-info').find('#current-user-id').html();
        // console.log('CHECKING',currentUser_ID,displayedID);
        if(currentUser_ID != displayedID){
            content =  '<div id="chat_header" class="" style="background-color:;">'+
                            '<span class="chattext">Chat</span>'+
                        '</div>'+
                        '<div style="text-align:center;padding-top:10px">'+
                            '<span>User has been changed</span><br/>'+

                            '<span>Click the button to </span><br/>'+

                            '<span>refresh the page</span><br/>'+
                        '</div>'+
                            '<div style="padding-top:10px"><center><input type="button" value="Refresh" class="refresh_page"></center>'+
                        '</div>';

            $('.forma-online-list').html(content);
            $('.chat_tabs').each(function(){
            $   (this).find('.remove_tab').trigger('click')
            })
        }
    }
    function updateChatUsers() {
        console.log("update users");
        $.get("/portal/forma_online_users", {}, function (html) {
            // console.log(html);
            $('.online-list-single').replaceWith(html);


            if (typeof announcement_posted_ui !== 'undefined') {
                announcement_posted_ui.tabNavsPortal();
            }
            var onlineUsersCount= $('#online_list').find('.online').length
            $('#online_count').find("#onlineUsersCount").html(onlineUsersCount);
            if($('#online_list').height > 239){
                $('#list_container').perfectScrollbar({minScrollbarLength: 30});
            }
                    // $("img").each(function(){
                    //     $(this).attr("onerror","this.src='/images/avatar/small.png'");
                    // });
                checkSession();
        });
        return;
    }
    function updateGroupChats(){
        console.log("Update Groups");
        $.get("/portal/forma_group",{},function (list){
            $('#forma_groups').replaceWith(list);
            if($('#online_list').height > 239){
                $('#list_container').perfectScrollbar({minScrollbarLength: 30});
            }
            // $("img").each(function(){
            //     $(this).attr("onerror","this.src='/images/avatar/small.png'");
            // });
        });
    }
    function setInputSelection(input, startPos, endPos) {
        input.focus();
        if (typeof input.selectionStart != "undefined") {
            input.selectionStart = startPos;
            input.selectionEnd = endPos;
        } else if (document.selection && document.selection.createRange) {
            // IE branch
            input.select();
            var range = document.selection.createRange();
            range.collapse(true);
            range.moveEnd("character", endPos);
            range.moveStart("character", startPos);
            range.select();
        }
    }

(function(){
    var blink = null;
    var blink_cancel = null;
    var orig_title = (function function_name (val) {
        return val;
    })(document.title);
    var audio = new Audio('/sounds/sounds-954-all-eyes-on-me.mp3');

   console.log("Form socket event handlers");
    $(document).on('socketActivated', function (event) {
        // updateOnlineUsersCount();
        updateChatUsers();
		console.log("socket Activated");
		var socket = event.socket;
 
        socket.on('thread_opened', function (data) {
          	// console.log("socket thread opened",data);
            // $("[data-thread-id='"+data.threadId+"']").find(".show-thread-btn").css("background-color", "#fff");
        });

        // socket.removeAllListeners('userLoggedIn');        
        socket.on('userLoggedIn', function (user) {
            console.log("socket user logged in");
            updateChatUsers();

            // updateOnlineUsersCount();   
        });

        socket.on('userLoggedOut', function (user) {
        	console.log("socket user logged out");
            updateChatUsers();
            // updateOnlineUsersCount();
        });

        socket.on('message_recieved', function (data) {
            console.log("Message Recieved!")
            console.log("Recieved",data)
            updateChatUsers();
            updateGroupChats();

          $.get("/portal/forma_group",{},function (list){
                $('#forma_groups').html(list);
        
                $.get("/portal/forma_online_users", {}, function (html) {
                var threadId = data.messageJSON.threadId;
                $('.online-list-single').html(html);
                if (typeof announcement_posted_ui !== 'undefined') {
                announcement_posted_ui.tabNavsPortal();
                }
            // console.log("Socket Message Received");
            // console.log(data.messageJSON.author.displayName);
                // var x=$('.user-online[data-keyword="'+ data.messageJSON.author.displayName +'"]').attr('data-thread-id');
                // console.log(data.messageJSON.author.id);
                var current_user_id = $('#current-user-id').html();
                if(data.messageJSON.author.id == current_user_id){

                }
                else{
                    // console.log('thread ',$('.user-online[thread-id="'+ threadId +'"]'))
                    $('.user-online[thread-id="'+ threadId +'"]').trigger('click');

                    var selectedTabByName = $("[data-thread-id='"+threadId+"']")
                        $(selectedTabByName).find(".fl-widget-msg-content-wrapper").append(data.messageHTML);
                        // $("img").each(function(){
                        //     $(this).attr("onerror","this.src='/images/avatar/small.png'");
                        // });
                    var objDiv = document.getElementById("chattab_"+threadId);
                    $('.message_content_'+threadId+'').perfectScrollbar({minScrollbarLength: 30});
                    objDiv.scrollTop = objDiv.scrollHeight;

                    var count_unread = $('.user-online[thread-id="'+ threadId +'"]').attr('data-unread-count') || $('.user-online[thread-id="'+ threadId +'"]').attr('unread-count');
                    // console.log(count_unread)
                        $('.chat_tab[data-thread-id='+threadId+']').attr("data-unread",count_unread)
                        
                        if($('.chat_tabs[data-thread-id='+threadId+']').find('.chat_text:focus').length > 0){
                            // console.log("focus");
                        }else{    
                            // console.log("hey");
                            if(count_unread > 0 ){
                                $('.chat_tabs[data-thread-id='+threadId+']').find('.chat-notification').css('display','');
                                if(count_unread >99){
                                    $('.chat_tabs[data-thread-id='+threadId+']').find('.chat-notification-count').html('99+')
                                }else{
                                    $('.chat_tabs[data-thread-id='+threadId+']').find('.chat-notification-count').html(count_unread)
                                }
                            }
                        }    
                    }

                    //-----Tab Title Blinking-----//
                    clearInterval(blink);
                    clearTimeout(blink_cancel);
                    
                    var new_title =""+data.messageJSON.author.displayName+' says....';
                    // console.log(orig_title)
                    blink = setInterval(function(){
                        // console.log("hello")
                          if (document.title == orig_title){
                            document.title = new_title;
                          }else{
                            document.title = orig_title;
                          }
                    },750);
                    $(window).focus(function() {
                        clearInterval(blink);
                        document.title = orig_title;
                    });
                    // blink_cancel = setTimeout(function(){
                    //     console.log("END Interval")
                    //     clearInterval(blink);
                    //     document.title = orig_title;
                    // },5000);
                    
                    
                    audio.play();
                });
            });
        });

        socket.on('typing',function (data){
            // console.log("type",data)
            // console.log("Name",data.sender)
            // console.log("ID",data.threadId)
            var parent_tab = $('.chat_tabs[data-thread-id="'+ data.threadId+'"]'); 
            $('.chat_tabs[data-thread-id="'+ data.threadId+'"]').find('.typing_div').show();
            
            if($('.typing_dudes[id='+ data.sender.replace(/ /g, "") +']').length > 0){

            }else{
                if($(parent_tab).find('.typing_dudes').length > 0){
                    var typer = '<span class="typing_dudes" id="'+ data.sender.replace(/ /g, "") +'"> '+ data.sender + '</span>';
                    var temp = $(typer);
                    $(parent_tab).find('.typing_div').prepend(temp);
                    $('.typing_dudes[id='+ data.sender.replace(/ /g, "") +']').next().prepend(", ");
                }else{  
                    var typer = '<span class="typing_dudes" id="'+ data.sender.replace(/ /g, "") +'"> '+ data.sender + '</span>';
                    var temp = $(typer);
                    $(parent_tab).find('.typing_div').prepend(temp);
                }

                    temp.delay(2000).queue(function(){
                        if($(this).parent().children('span .typing_dudes').length <= 1){
                           $(this).parent().hide(); 
                        }
                        $(this).remove();
                    });
                // setTimeout(function(){
                //     $('span[id="'+ data.sender +'"]').remove();
                // },5000);
            }

            // setTimeout(function(){
            //     $('.chat_tabs[data-thread-id="'+ data.threadId+'"]').find('.typing_div').hide();
            //     // $('span[id="'+ data.sender +'""]').remove();
            // }, 5000);
        });

        socket.on('nameChanged',function (data){
            console.log("Group Name Changed")
            updateGroupChats();
            // console.log(data);
            // $('.user-online[thread-id="'+ data.threadId +'"]').trigger('click');
            for(var i in chat_tab_on){
                    if(chat_tab_on[i].chat_thread_id == data.threadId){
                        chat_tab_on[i].chat_name = data.newName;
                        break;
                    }
                }
            var paren_id = $('.chat_tabs[data-thread-id="'+ data.threadId +'"]')
            $(paren_id).find('.newName').attr('value',data.newName);
            $(paren_id).find('.chat_title').html(data.newName);
            $(paren_id).find('.chat_title_input').hide();
            $(paren_id).find('.chat_title').show();
            $(paren_id).attr("data-chat-name",data.newName);    

        });
        // socket.on('extension_opened',function (data){
        //     console.log("extension_opened!!!!");
        // });


        // socket.on('selected_text',function (data){
        //     console.log("This Selected",data);
        //     var select_end = data.select_end;
        //     var select_start = data.select_start;
        //     var input_id = $('#'+data.cell_id);
        //     var cell_val =  $('#'+data.cell_id).attr('value');
        //     var selected_string = data.selected_Text;
        //     // var selected_string_index = cell_val.indexOf(selected_string);
        //     var selected_string_count = data.selected_word_index + selected_string.length;
        //     console.log(data.selected_word_index)
        //     console.log(selected_string_count);

        //     setInputSelection(input_id, 5, 10);
        // });

        // socket.on('text_changed',function(data){
        //     console.log("text Changed!",data);
        //     $('#'+data.cell_id).val(data.value);
        // });

        // socket.on('select_cell',function (data){
        //     console.log("select_cell!",data)
        //     $('#'+data.cell_id).parent().css('background-color',"purple");
        // })

});
})();