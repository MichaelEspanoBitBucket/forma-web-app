// SAMPLE USAGE
// var my_formula = new Formula("@Concat(@FIELDNAME1, @FIELDNAME2, @FIELDNAME3)",function(){
    // you may use this callback
// });
// var this_true_value = my_formula.getEvaluation();

$(document).ready(function(){
	GetLatLong();
});
var longitude = "";
var latitude = "";

// Number.prototype.currencyFormat = function () {
//     // (?<=\d)(?=(\d\d\d)+(?!\d))
//     // return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
//     // console.log(this)
//     // console.log("ow")
//     var check_expo = this.toString();
//     var reg_eks_expo = new RegExp("e\\+\\d", "g");
//     var matched_expo = check_expo.match(reg_eks_expo);
//     // console.log("CURRENCY FORMAT",matched_expo)
//     if (matched_expo == null) {
//         var negative_pad = "";
//         var float_value = "";
//         var integer = "";
//         if (this < 0) {
//             negative_pad = "-";
//             float_value = this.toString().split(".");
//             integer = float_value[0].replace("-", "");
//         } else {
//             float_value = this.toString().split(".");
//             integer = float_value[0];
//         }

//         var decimal = "";
//         if (float_value.length == 2) {
//             decimal = float_value[1];
//             integer = negative_pad + integer.split(/(?=(?:...)*$)/).join(",");
//             return integer + "." + decimal;
//         } else {
//             integer = negative_pad + integer.split(/(?=(?:...)*$)/).join(",");
//             return integer + ".00";
//         }
//     } else {
//         return this;
//     }
// };
// Date.prototype.formatDate = function (format) {
//     if (this == "Invalid Date") {
//         return this;
//     } else {
//         timestamp = this;
//         // http://kevin.vanzonneveld.net
//         // +   original by: Carlos R. L. Rodrigues (http://www.jsfromhell.com)
//         // +      parts by: Peter-Paul Koch (http://www.quirksmode.org/js/beat.html)
//         // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
//         // +   improved by: MeEtc (http://yass.meetcweb.com)
//         // +   improved by: Brad Touesnard
//         // +   improved by: Tim Wiel
//         // +   improved by: Bryan Elliott
//         // +   improved by: David Randall
//         // +      input by: Brett Zamir (http://brett-zamir.me)
//         // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
//         // +   improved by: Theriault
//         // +  derived from: gettimeofday
//         // +      input by: majak
//         // +   bugfixed by: majak
//         // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
//         // +      input by: Alex
//         // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
//         // +   improved by: Theriault
//         // +   improved by: Brett Zamir (http://brett-zamir.me)
//         // +   improved by: Theriault
//         // +   improved by: Thomas Beaucourt (http://www.webapp.fr)
//         // +   improved by: JT
//         // +   improved by: Theriault
//         // +   improved by: Rafał Kukawski (http://blog.kukawski.pl)
//         // +   bugfixed by: omid (http://phpjs.org/functions/380:380#comment_137122)
//         // +      input by: Martin
//         // +      input by: Alex Wilson
//         // +      input by: Haravikk
//         // +   improved by: Theriault
//         // +   bugfixed by: Chris (http://www.devotis.nl/)
//         // %        note 1: Uses global: php_js to store the default timezone
//         // %        note 2: Although the function potentially allows timezone info (see notes), it currently does not set
//         // %        note 2: per a timezone specified by date_default_timezone_set(). Implementers might use
//         // %        note 2: this.php_js.currentTimezoneOffset and this.php_js.currentTimezoneDST set by that function
//         // %        note 2: in order to adjust the dates in this function (or our other date functions!) accordingly
//         // *     example 1: date('H:m:s \\m \\i\\s \\m\\o\\n\\t\\h',  );
//         // *     returns 1: '09:09:40 m is month'
//         // *     example 2: date('F j, Y, g:i a', 1062462400);
//         // *     returns 2: 'September 2, 2003, 2:26 am'
//         // *     example 3: date('Y W o', 1062462400);
//         // *     returns 3: '2003 36 2003'
//         // *     example 4: x = date('Y m d', (new Date()).getTime()/1000);
//         // *     example 4: (x+'').length == 10 // 2009 01 09
//         // *     returns 4: true
//         // *     example 5: date('W', 1104534000);
//         // *     returns 5: '53'
//         // *     example 6: date('B t', 1104534000);
//         // *     returns 6: '999 31'
//         // *     example 7: date('W U', 1293750000.82); // 2010-12-31
//         // *     returns 7: '52 1293750000'
//         // *     example 8: date('W', 1293836400); // 2011-01-01
//         // *     returns 8: '52'
//         // *     example 9: date('W Y-m-d', 1293974054); // 2011-01-02
//         // *     returns 9: '52 2011-01-02'
//         var that = this,
//                 jsdate,
//                 f,
//                 // Keep this here (works, but for code commented-out
//                 // below for file size reasons)
//                 //, tal= [],
//                 txt_words = ["Sun", "Mon", "Tues", "Wednes", "Thurs", "Fri", "Satur", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
//                 // trailing backslash -> (dropped)
//                 // a backslash followed by any character (including backslash) -> the character
//                 // empty string -> empty string
//                 formatChr = /\\?(.?)/gi,
//                 formatChrCb = function (t, s) {
//                     return f[t] ? f[t]() : s;
//                 },
//                 _pad = function (n, c) {
//                     n = String(n);
//                     while (n.length < c) {
//                         n = '0' + n;
//                     }
//                     return n;
//                 };
//         f = {
//             // Day
//             d: function () { // Day of month w/leading 0; 01..31
//                 return _pad(f.j(), 2);
//             },
//             D: function () { // Shorthand day name; Mon...Sun
//                 return f.l().slice(0, 3);
//             },
//             j: function () { // Day of month; 1..31
//                 return jsdate.getDate();
//             },
//             l: function () { // Full day name; Monday...Sunday
//                 return txt_words[f.w()] + 'day';
//             },
//             N: function () { // ISO-8601 day of week; 1[Mon]..7[Sun]
//                 return f.w() || 7;
//             },
//             S: function () { // Ordinal suffix for day of month; st, nd, rd, th
//                 var j = f.j(),
//                         i = j % 10;
//                 if (i <= 3 && parseInt((j % 100) / 10, 10) == 1) {
//                     i = 0;
//                 }
//                 return ['st', 'nd', 'rd'][i - 1] || 'th';
//             },
//             w: function () { // Day of week; 0[Sun]..6[Sat]
//                 return jsdate.getDay();
//             },
//             z: function () { // Day of year; 0..365
//                 var a = new Date(f.Y(), f.n() - 1, f.j()),
//                         b = new Date(f.Y(), 0, 1);
//                 return Math.round((a - b) / 864e5);
//             },
//             // Week
//             W: function () { // ISO-8601 week number
//                 var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3),
//                         b = new Date(a.getFullYear(), 0, 4);
//                 return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
//             },
//             // Month
//             F: function () { // Full month name; January...December
//                 return txt_words[6 + f.n()];
//             },
//             m: function () { // Month w/leading 0; 01...12
//                 return _pad(f.n(), 2);
//             },
//             M: function () { // Shorthand month name; Jan...Dec
//                 return f.F().slice(0, 3);
//             },
//             n: function () { // Month; 1...12
//                 return jsdate.getMonth() + 1;
//             },
//             t: function () { // Days in month; 28...31
//                 return (new Date(f.Y(), f.n(), 0)).getDate();
//             },
//             // Year
//             L: function () { // Is leap year?; 0 or 1
//                 var j = f.Y();
//                 return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0;
//             },
//             o: function () { // ISO-8601 year
//                 var n = f.n(),
//                         W = f.W(),
//                         Y = f.Y();
//                 return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
//             },
//             Y: function () { // Full year; e.g. 1980...2010
//                 return jsdate.getFullYear();
//             },
//             y: function () { // Last two digits of year; 00...99
//                 return f.Y().toString().slice(-2);
//             },
//             // Time
//             a: function () { // am or pm
//                 return jsdate.getHours() > 11 ? "pm" : "am";
//             },
//             A: function () { // AM or PM
//                 return f.a().toUpperCase();
//             },
//             B: function () { // Swatch Internet time; 000..999
//                 var H = jsdate.getUTCHours() * 36e2,
//                         // Hours
//                         i = jsdate.getUTCMinutes() * 60,
//                         // Minutes
//                         s = jsdate.getUTCSeconds(); // Seconds
//                 return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
//             },
//             g: function () { // 12-Hours; 1..12
//                 return f.G() % 12 || 12;
//             },
//             G: function () { // 24-Hours; 0..23
//                 return jsdate.getHours();
//             },
//             h: function () { // 12-Hours w/leading 0; 01..12
//                 return _pad(f.g(), 2);
//             },
//             H: function () { // 24-Hours w/leading 0; 00..23
//                 return _pad(f.G(), 2);
//             },
//             i: function () { // Minutes w/leading 0; 00..59
//                 return _pad(jsdate.getMinutes(), 2);
//             },
//             s: function () { // Seconds w/leading 0; 00..59
//                 return _pad(jsdate.getSeconds(), 2);
//             },
//             u: function () { // Microseconds; 000000-999000
//                 return _pad(jsdate.getMilliseconds() * 1000, 6);
//             },
//             // Timezone
//             e: function () { // Timezone identifier; e.g. Atlantic/Azores, ...
//                 // The following works, but requires inclusion of the very large
//                 // timezone_abbreviations_list() function.

//                 throw 'Not supported (see source code of date() for timezone on how to add support)';
//             },
//             I: function () { // DST observed?; 0 or 1
//                 // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
//                 // If they are not equal, then DST is observed.
//                 var a = new Date(f.Y(), 0),
//                         // Jan 1
//                         c = Date.UTC(f.Y(), 0),
//                         // Jan 1 UTC
//                         b = new Date(f.Y(), 6),
//                         // Jul 1
//                         d = Date.UTC(f.Y(), 6); // Jul 1 UTC
//                 return ((a - c) !== (b - d)) ? 1 : 0;
//             },
//             O: function () { // Difference to GMT in hour format; e.g. +0200
//                 var tzo = jsdate.getTimezoneOffset(),
//                         a = Math.abs(tzo);
//                 return (tzo > 0 ? "-" : "+") + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
//             },
//             P: function () { // Difference to GMT w/colon; e.g. +02:00
//                 var O = f.O();
//                 return (O.substr(0, 3) + ":" + O.substr(3, 2));
//             },
//             T: function () { // Timezone abbreviation; e.g. EST, MDT, ...
//                 // The following works, but requires inclusion of the very
//                 // large timezone_abbreviations_list() function.

//                 return 'UTC';
//             },
//             Z: function () { // Timezone offset in seconds (-43200...50400)
//                 return -jsdate.getTimezoneOffset() * 60;
//             },
//             // Full Date/Time
//             c: function () { // ISO-8601 date.
//                 return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
//             },
//             r: function () { // RFC 2822
//                 return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
//             },
//             U: function () { // Seconds since UNIX epoch
//                 return jsdate / 1000 | 0;
//             }
//         };
//         this.date = function (format, timestamp) {
//             that = this;
//             jsdate = (timestamp === undefined ? new Date() : // Not provided
//                     (timestamp instanceof Date) ? new Date(timestamp) : // JS Date()
//                     new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
//                     );
//             return format.replace(formatChr, formatChrCb);
//         };
//         return this.date(format, timestamp);
//     }//end of if
// }
// Date.prototype.diffHoursIn = function (date2) {
//     // / 1000 / 60 / 60
//     if (typeof date2 != "undefined" && this != "Invalid Date") {
//         console.log("date2", date2)
//         var date2_dt_obj = ThisDate(date2);
//         console.log("date2_dt_obj", date2_dt_obj);
//         var t2 = date2_dt_obj.getTime();
//         var t1 = this.getTime();
//         return /*Math.abs*/((t1 - t2) / 1000 / 60 / 60);
//     } else {
//         return "";
//     }
// }
// Date.prototype.diffDaysIn = function (date2) {
//     if (typeof date2 != "undefined" && this != "Invalid Date") {
//         var date2_dt_obj = ThisDate(date2);
//         var t2 = date2_dt_obj.getTime();
//         var t1 = this.getTime();
//         return /*Math.abs*/(parseInt((t1 - t2) / (24 * 3600 * 1000), 10));
//     } else {
//         return "";
//     }
// }
// Date.prototype.diffWeeksIn = function (date2) {
//     if (typeof date2 != "undefined" && date2.length >= 1 && this != "Invalid Date") {
//         var date2_dt_obj = ThisDate(date2);
//         var t2 = date2_dt_obj.getTime();
//         var t1 = this.getTime();

//         return /*Math.abs*/(parseInt((t1 - t2) / (24 * 3600 * 1000 * 7)));
//     } else {
//         return "";
//     }
// }
// Date.prototype.diffMonthsIn = function (date2) {
//     if (typeof date2 != "undefined" && date2.length >= 1 && this != "Invalid Date") {
//         var date2_dt_obj = ThisDate(date2);
//         var d1Y = this.getFullYear();
//         var d2Y = date2_dt_obj.getFullYear();
//         var d1M = this.getMonth();
//         var d2M = date2_dt_obj.getMonth();
//         return /*Math.abs*/((d1M + 12 * d1Y) - (d2M + 12 * d2Y));
//     } else {
//         return "";
//     }
// }

// Date.prototype.diffYearsIn = function (date2) {
//     if (typeof date2 != "undefined" && date2.length >= 1 && this != "Invalid Date") {
//         var date2_dt_obj = ThisDate(date2);
//         return /*Math.abs*/(date2_dt_obj.getFullYear() - this.getFullYear());
//     } else {
//         return "";
//     }
// }

// Date.prototype.adjustSeconds = function (value) {
//     if ($.isNumeric(value)) {
//         var new_date = this.setSeconds(parseInt(this.getSeconds()) + parseInt(value));
//         var obj_new_date = ThisDate(new_date);
//         return obj_new_date;
//     } else {
//         return "";
//     }
// }
// Date.prototype.adjustMinutes = function (value) {
//     if ($.isNumeric(value)) {
//         var new_date = this.setMinutes(parseInt(this.getMinutes()) + parseInt(value));
//         var obj_new_date = ThisDate(new_date);
//         return obj_new_date;
//     } else {
//         return "";
//     }
// }
// Date.prototype.adjustHours = function (value) {
//     if ($.isNumeric(value)) {
//         var new_date = this.setHours(parseInt(this.getHours()) + parseInt(value));
//         var obj_new_date = ThisDate(new_date);
//         return obj_new_date;
//     } else {
//         return "";
//     }
// }
// Date.prototype.adjustDay = function (value) {
//     if ($.isNumeric(value)) {
//         var new_date = this.setDate(parseInt(this.getDate()) + parseInt(value));
//         var obj_new_date = ThisDate(new_date);
//         return obj_new_date;
//     } else {
//         return "";
//     }
// }
// Date.prototype.adjustMonth = function (value) {
//     if ($.isNumeric(value)) {
//         var new_date = this.setMonth(parseInt(this.getMonth()) + parseInt(value));
//         var obj_new_date = ThisDate(new_date);
//         return obj_new_date;
//     } else {
//         return "";
//     }
// }
// Date.prototype.adjustYear = function (value) {
//     if ($.isNumeric(value)) {
//         var new_date = this.setFullYear(parseInt(this.getFullYear()) + parseInt(value));
//         var obj_new_date = ThisDate(new_date);
//         return obj_new_date;
//     } else {
//         return "";
//     }
// }

//from request_view.js CountDayIf 0655PM 10222015

function CountDayIf(start_date, end_date) {
    var args = arguments;
    var starting_date = new Date(start_date);

    var ending_date = new Date(end_date);
    var counted_if_days = 0;

    if (typeof args[2] !== "undefined") {

        var days_difference = ending_date.diffDaysIn(starting_date);
        for (var ii = 0; ii <= days_difference; ii++) {
            if (ii == 0) {
                var adjusted_day = starting_date.adjustDay(0).formatDate('l');
            } else {
                var adjusted_day = starting_date.adjustDay(+1).formatDate('l');
            }
            console.log(ii, adjusted_day, starting_date)
            for (var ca = 0; ca < args.length; ca++) {

                var param_day = args[ca];
                if ((adjusted_day == param_day && ca >= 2) != false) {

                    // console.log(adjusted_day+" == "+param_day+" && "+ca+" >= "+2+"\n",(( adjusted_day == param_day && ca >= 2 ) != false ))
                    counted_if_days++;
                }
            }
        }
        return counted_if_days;
    }
}
function sortMyObjectByKey(arrayObject, keyNameValToSort) {
    var myArrayOfObj = arrayObject;
    // myArrayOfObj.sort(function (a, b) {
    //     if (a[keyNameValToSort] < b[keyNameValToSort]) {
    //         return -1;
    //     }
    //     else if (a[keyNameValToSort] > b[keyNameValToSort]) {
    //         return 1;
    //     }
    //     return 0;
    // });
    // return myArrayOfObj;
    return myArrayOfObj.sort(function (x, y) {
        return x[keyNameValToSort] - y[keyNameValToSort]
    });
}
function fieldExists(FIELDNAME) {
    var bool_checker = false;
    try {
        var replaced_str = FIELDNAME.replace(/@/g, "")
        var FN_OBJ = $("[name=" + replaced_str + "]");
        if (FN_OBJ.length >= 1) {
            bool_checker = true;
        } else {
            bool_checker = false;
        }
    } catch (error) {
        console.log("NAG ERROR SA FIELDEXISTS", replaced_str, error)
        bool_checker = false;
    }
    return bool_checker;
}
function ArraySearchBy(given_array, search, flag){
    if(!flag){
            flag = 'single';
    }
    if(flag == 'single'){
            return $.inArray(search, given_array);
    }
    else{
            if($.isArray(search) == true){
                var res = [];
                for(s in search){
                    if($.inArray(search[s], given_array) > -1){
                        res.push($.inArray(search[s], given_array));
                    }
                }
                if(res.length <= 0){
                        res = -1;
                }
                return res;
            }
            else{
                return $.inArray(search, given_array);
            }
    }
}


function GetLocation(current_location){
	var current_location_array = [];
	var data_location_array = [];
	current_location_array = current_location_array.concat(current_location);
	if($.inArray("latitude",current_location_array) >= 0){
		
		data_location_array.push(latitude);
	}
	if($.inArray("longitude",current_location_array) >= 0){
		data_location_array.push(longitude);
	}
	console.log("location",data_location_array)
	return data_location_array;
}
function showPosition(position){

	latitude = position.coords.latitude;
	longitude = position.coords.longitude;

}
function GetLatLong() {
	var data = {
		getData:function(){
			return {
				latitude:this.latitude,
				longitude:this.longitude
			};
		}
	}
    if (navigator.geolocation) {
    	navigator.geolocation.getCurrentPosition(function(position){
    		latitude = position.coords.latitude;
    		longitude = position.coords.longitude;
    		data.latitude = latitude;
    		data.longitude = longitude;
    	});
    } else { 
        alert("Geolocation is not supported by this browser.");
    }
    /*Add the script below to get address*/
    /*<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>*/
    // if($.inArray("address",current_location_array >= 0)){
    // 	var latlng = new google.maps.LatLng(latitude, longitude);
    //     var geocoder = geocoder = new google.maps.Geocoder();
    //     geocoder.geocode({ 'latLng': latlng }, function (results, status) {
    //         if (status == google.maps.GeocoderStatus.OK) {
    //             if (results[1]) {
    //                 alert("Location: " + results[1].formatted_address);
    //             }
    //         }
    //     });
    // }
    return data;
}


function GetFileName(pathname, extension){
	if(pathname){
        var path_str = pathname;
        try{
            path_str = JSON.parse(JSON.parse(pathname));
        }
        catch(e){
            path_str = pathname
        }
		// var path_str = pathname;
		//var regexp = new RegExp("[a-z\\(\\)A-Z0-9_][a-z\\(\\)A-Z0-9\\s_\\.]*\\.[a-zA-Z]{3}[a-zA-Z]?(?![a-zA-Z0-9_]|\\.|\\(|\\))" , "g");
		// var matches_str = pathname.match(regexp);
		//this function is use for multiple attachment
        if(typeof path_str === "object"){
            var field_names = [];
            for(i in path_str){
                if(extension == "*"){
                    field_names.push(path_str[i]['file_name']);
                }
                else{
                    if(path_str[i]['file_name'].indexOf(extension) > -1){
                        field_names.push(path_str[i]['file_name']);
                    }
                }
            }
            return field_names;
        }
        else{
            var matches_str = pathname.split("/");
    		if(matches_str){
    			if(matches_str.length >= 1){
    				if(extension){
    					if(extension == "*"){
    						return matches_str[ matches_str.length - 1 ];
    					}else{
    						var regexp_extension = new RegExp("^[a-zA-Z0-9_]*\\."+extension+"(?=([^a-zA-Z0-9_]|$))","g");
    						var extension_matches = matches_str[ matches_str.length - 1 ].match(regexp_extension);
    						if(extension_matches){
    							return extension_matches[ extension_matches.length - 1 ];
    						}else{
    							return "";
    						}
    					}
    				}else{
    					return matches_str[ matches_str.length - 1 ];
    				}
    				
    			}else{
    				return "";
    			}
    		}else{
    			return "";
    		}
        }
	}else{
		return "";
	}
}


function ArrayJoinBy(array_val,glue){
	var array_value = [];
	array_value = array_value.concat(array_val);
	var str_val = array_value.join(glue);
	return str_val;
}
function StrSplitBy(str_val,delimiter,index){
	var str_val_array = [];
	str_val_array = str_val.split(delimiter);
	var str_val_array_index = index;
	
	if(!index){
		index = "all";
	}

	if(index >= str_val_array.length){
		return "";
	}
	if(index == "first"){
		str_val_array_index = 0;
		return str_val_array[str_val_array_index];
	}
	else if(index == "last"){
		str_val_array_index = str_val_array.length-1;
		return str_val_array[str_val_array_index];
	}
	else if(index == "all"){
		return str_val_array;
	}
	str_val_array_index = Number(str_val_array_index);
	return str_val_array[str_val_array_index];
}
function SetHyperLink(label_name,link){
	var selector_link = $("#"+label_name);
	var address = "";
	var label="";
	label = selector_link.children('label:eq(0)').text();
	if(link=="current"){
		address = document.location.toString();
		
		selector_link.children('label:eq(0)').html('<a href="'+address+'" class="linkedIt">'+label+'</a>');
		
	}
	else{
		address = link;
		
		selector_link.children('label:eq(0)').html('<a href="'+address+'" class="linkedIt">'+label+'</a>');
		
	}
	return{
		"valueOf":function (){return ""},
		"execute":function(){
				if(selector_link.length>=1){
					
					selector_link.children('label:eq(0)').html('<a href="'+address+'" class="linkedIt">'+label+'</a>');
					
				}	

			}
	}

}

function StrReplace(string_val,old_string,new_string,string_flags){
	
	var string_flags_array = [];
	string_flags_array = string_flags_array.concat(string_flags);
	var old_string_array = [];
	//
	var escape_characters = 
		[
		"\\.",
		"\\^",
		"\\$",
		"\\*",
		"\\+",
		"\\-",
		"\\?",
		"\\(",
		"\\)",
		"\\[",
		"\\]",
		"\\{",
		"\\}",
		"\\\\",
		"\\|"
		];
	var given_strReg = new RegExp (escape_characters.join('|'),"g")
	old_string_array = old_string_array.concat(old_string);
	old_string_array = old_string_array.map(function(a,b){
		return a.replace(given_strReg, function(a2,b2,c2,d2){
			return "\\"+a2;
		});
	})

	//
	
	var string_modifier = "";
	var regExString_array = [];
	if($.inArray("global",string_flags_array)>=0){
		string_modifier += "g";
	}
	if($.inArray("ignore_case",string_flags_array)>=0){
		string_modifier += "i";
	}
	old_string_array = old_string_array.filter(Boolean);
	
	for(var osa_ctr=0; old_string_array.length-1 >= osa_ctr; osa_ctr++){
		var temp_string = old_string_array[osa_ctr];
		
		if($.inArray("whole_word",string_flags_array)>=0){
			//StringReg = new RegExp("(^| )"+temp_string+"(?![0-9a-zA-Z_])",string_modifier);
			regExString_array.push("(^| )"+temp_string+"(?![0-9a-zA-Z_])");
			// new_word = string_val.replace(StringReg," "+new_string);
		}
		else{
			regExString_array.push(temp_string);
		}
		
	}
	var NewRegexString = new RegExp(regExString_array.join("|"),string_modifier);
		
	
	var new_word = string_val.replace(NewRegexString,new_string);
	
	new_word = $.trim(new_word);

	return new_word;
	
}

function StrFind(string_val,string_search,return_val){
	if(string_search == ""){
		if(return_val === "count"){
			return 0;
		}
		if(return_val === "first_index"){
			return -1;
		}
		if(return_val === "last_index"){
			return -1;
		}
		if(return_val === "all_index"){
			return [];
		}
	}
	if(!return_val){
		return_val = "first_index";
	}

	var escape_characters = 
		[
		"\\.",
		"\\^",
		"\\$",
		"\\*",
		"\\+",
		"\\-",
		"\\?",
		"\\(",
		"\\)",
		"\\[",
		"\\]",
		"\\{",
		"\\}",
		"\\\\",
		"\\|"
		];
	var given_strReg = new RegExp (escape_characters.join('|'),"g");
	string_search = string_search.replace(given_strReg,function(a){
		return "\\"+a
	});
	var stringReg = new RegExp(string_search,"g");
	console.log(stringReg)
	var m = "";
	var stringArray = [];
	var m = null;
	while (m = stringReg.exec(string_val)) {
 		stringArray.push(m["index"]);
	} 
	if(return_val === "count"){
		return stringArray.length;
	}
	if(return_val === "first_index"){
		return stringArray[0]
	}
	if(return_val === "last_index"){
		return stringArray[stringArray.length-1];
	}
	if(return_val === "all_index"){
		return stringArray;
	}
}
function StrTrim(string_val){
	var trimmed_string = $.trim(string_val);
	
	return trimmed_string;
}
function SetPropertyV2(field_name,property_type,property_value){
	
	var property_type_array = [];
	
	property_type_array = property_type_array.concat(property_type);
	
	var selector = $();
	if($('.label_below.obj_label#'+field_name).length>=1){
		selector = selector.add($('.label_below.obj_label#'+field_name).children('label'));

	}
	else if($('[name="'+field_name+'"]').length>=1){
		selector = selector.add($('[name="'+field_name+'"]'));
	}
	var json_prop={};
	if($.inArray("font",property_type_array)>=0){
		$.extend(json_prop,{'color':property_value});
	}
	if($.inArray("background",property_type_array)>=0){
		$.extend(json_prop,{'background-color':property_value});
	}


	if($.inArray("border",property_type_array)>=0){
		$.extend(json_prop,{'border-color':property_value});
	}
	
	if(selector.length>=1){
		
		selector.css(json_prop);
		
	}	
	return {
		"valueOf":function (){return ""},
		"execute":function(){
				if(selector.length>=1){
				
					selector.css(json_prop);
					
				}	

			}

	}

}
function SetProperty(field_name,target,property_type,property_value){

	var target_array = [];
	var property_type_array = [];
	target_array = target_array.concat(target);
	property_type_array = property_type_array.concat(property_type);
	
	var selector = $();
	var json_prop={};

	if($.inArray("label",target_array)>=0){
		if($('label[name="'+field_name+'"]').length!=0){
			selector = selector.add($('label[name="'+field_name+'"]'));
		}
		else{

			selector = selector.add($('[name="'+field_name+'"]').closest('.setOBJ').find('.label_below.obj_label').children('label'));
		}
	}
	if($.inArray("field",target_array)>=0){
		
		selector = selector.add($('[name="'+field_name+'"]'));
	}
	if($.inArray("font",property_type_array)>=0){
		$.extend(json_prop,{'color':property_value});
	}
	if($.inArray("background",property_type_array)>=0){
		$.extend(json_prop,{'background-color':property_value});
	}


	if($.inArray("border",property_type_array)>=0){
		$.extend(json_prop,{'border-color':property_value});
	}
	
	if(selector.length>=1){
		selector.css(json_prop);
	}
	return {
		"valueOf":function (){return ""},
		"execute":function(){
				if(selector.length>=1){
				
					selector.css(json_prop);
					
				}
			}
	}

	

}

function GetType(field_name){
	if($('.setOBJ').find('.input_position_below').find('input[name='+field_name+']').length >=1){
		
		var value_field = $('.setOBJ').find('.input_position_below').find('input[name='+field_name+']');
		var data_type = value_field.attr("data-type");
		
		if(data_type=="longtext"){
			data_type = "string";
		}
		return data_type;
	}
}
function StrUpper( param_str ){
	var str_val = param_str;
	if($.type(str_val) != "undefined"){
		str_val = String(str_val);
		str_val = str_val.toUpperCase();
	}
	return str_val;
}
function StrLower( param_str ){
	var str_val = param_str;
	if($.type(str_val) != "undefined"){
		str_val = String(str_val);
		str_val = str_val.toLowerCase();
	}
	return str_val;
}

//functions in JS version
function GetRound(given_num, decimal_digit, type) {
	if($.type(given_num) == "string"){
		if($.isNumeric(given_num)){
			given_num = Number(given_num);
			//medz version
			if(isNaN(given_num)==true){
				return 0;
			}
		}else{
			return 0;
		}
	}
	// var decimal_places = "1";
	// for (var ii = 1; ii <= decimal_digit; ii++) {
	//     decimal_places += "" + "0";
	// }
	// decimal_places = Number(decimal_places);
	// return Math.round(given_num * decimal_places) / decimal_places;

	

	var gn = given_num;
	var multiplier = 1;

	if(type == "up"){

		for (var i = 0; i<= decimal_digit-1 ; i ++){
			multiplier = multiplier * 10;

		}
		gn = gn * multiplier;
		gn = Math.ceil(gn);
		gn = gn / multiplier;

		return Number(gn);
	}
	else if(type == "down"){
		for (var i = 0; i<= decimal_digit-1 ; i ++){
			multiplier = multiplier * 10;

		}
		gn = gn * multiplier;
		gn = Math.floor(gn);
		gn = gn / multiplier;

		return Number(gn);

	}
	else{
		return Number(gn.toFixed(decimal_digit));
	}





}
function GetData (obj_name, data_selection) {
    var total_val = 0,
	avg_total = 0,
	concatenated = "",
	temp_array = [],
	temp_object_name = obj_name,
	data_selected = data_selection,
	ctr = 0
	ele_obj_name= $('[name="'+temp_object_name+'"]');
	
    if (ele_obj_name.length == 0) {
	ele_obj_name= $('[embed-name="'+temp_object_name+'"]');
    }
    if (ele_obj_name.length == 0) {
	ele_obj_name= $('[name="'+temp_object_name+'[]"]');
    }
    
    if (ele_obj_name.length >= 1 && $('[embed-name="'+temp_object_name+'"]').length >=1) {
	var pangalan_ng_embed = ele_obj_name;
	var get_header_elem = pangalan_ng_embed.find("table").children('thead').children('tr').children('th').children('.columnHeader[data-fld-name="'+data_selected+'"]');
	var index_nya = get_header_elem.parent().index();
	var data_field_type = get_header_elem.attr("data-input-fld-type");
	
	
	pangalan_ng_embed.find("table").children('tbody').children('tr').children('td').filter(function(){
	    return $(this).index() == index_nya;
	}).each(function(){
	    var input_val = $(this).find("input").val();
	    temp_array.push(input_val);
	    if (data_field_type == "Currency"|| data_field_type == "Number") {
		var integer_val = Number(input_val.replace(/,/g,""));
		ctr++;
		total_val = total_val + integer_val;
		avg_total = total_val/ctr;
		
		if (data_field_type =="Currency") {
		    
		    total_val = total_val.toFixed(2);
		    avg_total = avg_total.toFixed(2);
		}
		
	    }
	   
	    
	});
    }
    else if (ele_obj_name.length >= 1 && ele_obj_name.parents(".setOBJ, .setObject").eq(0).is("[data-type='selectMany']") ) {
	var pangalan_ng_select = ele_obj_name,
	    selected_elem = pangalan_ng_select.children("option:selected"),
	    notselected_elem = pangalan_ng_select.children("option:not(:selected)");
	    
	    
	    if (data_selected=="selected") {
		
		selected_elem.each(function(){
		    var selected_val = $(this).val();
		    if ($.isNumeric(selected_val)==true) {
			selected_val = Number(selected_val.replace(/,/g,""));
		    }
		    temp_array.push(selected_val);
		    ctr++;
		    total_val = total_val + selected_val;
		    avg_total = total_val/ctr;
		    });
	    }
	    else if(data_selected =="not selected"){
		
		
		notselected_elem.each(function(){
		    var selected_val = $(this).val();
		    if ($.isNumeric(selected_val)==true) {
			selected_val = Number(selected_val.replace(/,/g,""));
		    }
		    temp_array.push(selected_val);
		    ctr++;
		    total_val = total_val + selected_val;
		    avg_total = total_val/ctr;
		    });
		
	    }
	    else{
			var all_elem = pangalan_ng_select.children("option");
		    
		    all_elem.each(function(){
			var all_val = $(this).val();
			if ($.isNumeric(all_val)==true) {
			all_val = Number(all_val.replace(/,/g,""));
			}
			temp_array.push(all_val);
			ctr++;
			total_val = total_val + all_val;
			avg_total = total_val/ctr;
		    });
		
		    
	    }
	    
	
    }
    else if (ele_obj_name.length >= 1 && ele_obj_name.parents(".setOBJ, .setObject").eq(0).is("[data-type='checkbox']")) {
	var pangalan_ng_check = ele_obj_name,
	    check_elem = pangalan_ng_check.filter(":checked"),
	    unchecked_elem = pangalan_ng_check.filter(":not(:checked)");
	    if (data_selected == "checked") {
		check_elem.each(function(){
		    var selected_val = $(this).val();
		    if ($.isNumeric(selected_val)==true) {
			selected_val = Number(selected_val.replace(/,/g,""));
		    }
		    temp_array.push(selected_val);
		    ctr++;
		    total_val = total_val+selected_val;
		    avg_total = total_val/ctr;
		});
	    }
	    else if (data_selected == "unchecked") {
		unchecked_elem.each(function(){
		    var selected_val = $(this).val();
		    if ($.isNumeric(selected_val)==true) {
			selected_val = Number(selected_val.replace(/,/g,""));
		    }
		    temp_array.push(selected_val);
		    ctr++;
		    total_val = total_val+selected_val;
		    avg_total = total_val/ctr;
		});
	    }
	    else{
		return 0;
	    }
    }
    else{
	return ele_obj_name.val();
    }
    return {
	"valueOf":function(){return "";},
	"Array":temp_array,
	"Average":avg_total,
	"Total": total_val,
	"Count": ctr
    };
}


// function CollectData(field_object_name_para, method){
//      var total_val = 0;
//      var avg_total = 0;
//      var concatenated = "";
//      var temp_array = [];
//      var field_object_name = field_object_name_para;
//
//      var collect_embed_data = "";
//
//      if (test) {
//	
//      }else if($('[embed-name="'+field_object_name+'"]').length >= 1 ){
//         var obj_embed = $('[embed-name="'+field_object_name+'"]');
//
//         var header_key = obj_embed.find("th").map(function(eqi){
//             return ($(this).text().trim() == "Actions")?null:{"index":eqi,"head_caption":$(this).find('[data-form-fld-name]').attr('data-form-fld-name')};
//         }).get().filter(Boolean);
//
//         // console.log(header_key)
//         collect_embed_data = {};
//         obj_embed.find("td").each(function(){
//             if(header_key[$(this).index()]){
//                 collect_embed_data[""+header_key[$(this).index()]["head_caption"]+""] = ( typeof collect_embed_data[""+header_key[$(this).index()]["head_caption"]+""] === "undefined" )?[]:collect_embed_data[""+header_key[$(this).index()]["head_caption"]+""];
//                 collect_embed_data[""+header_key[$(this).index()]["head_caption"]+""].push($(this).text());
//             }
//         });
//      }else if(method == "selected"){
//         temp_array = $('[name="'+field_object_name+'[]"]').children("option:selected").map(function(){ return Number($(this).val());}).get();
//         // console.log("temp_array",temp_array)
//         if($.isArray(temp_array)){
//             temp_array = temp_array.filter(Boolean);
//             for(var ctr_ikey in temp_array){
//                 total_val += temp_array[ctr_ikey];
//             }
//             // for(var ctr_ikey in temp_array){
//             //     avg_total += temp_array[ctr_ikey];
//             // }
//         }
//      }else if(method == "not selected"){
//         temp_array = $('[name="'+field_object_name+'[]"]').children("option:not(:selected)").map(function(){ return Number($(this).val());}).get();
//         if($.isArray(temp_array)){
//             temp_array = temp_array.filter(Boolean);
//             for(var ctr_ikey in temp_array){
//                 total_val += temp_array[ctr_ikey];
//             }
//             // for(var ctr_ikey in temp_array){
//             //     total_val += temp_array[ctr_ikey];
//             // }
//         }
//      }else if(method == "checked"){
//      }
//
//
//
//      if( $('[embed-name="'+field_object_name+'"]').length >= 1 ){
//         return {
//             valueOf:function(){
//		
//		return "";
//	     }
//         };
//      }else{
//         return {
//             "valueOf":function(){ return ''; },
//             "TOTAL":total_val,
//             "AVG":avg_total,
//             "CONCATENATED":"concat"
//         }	
//	}
// }


function Formula(formula,CB){
	if(typeof CB == "undefined"){
		//catch undefined
	}
    if($.type(formula) == "undefined"){
        formula = "";
    }
	this.formula = formula;
	this.processedFormula = "";
	//getting fieldnames with at sign
    var reg_parts_ff = [
        "@SDev\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
        "@GetAVG\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
        "@GetMin\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
        "@GetMax\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
        "@Sum\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
        "@[a-zA-Z][a-zA-Z0-9_]*"
    ];
    var reg_eks = new RegExp(reg_parts_ff.join("|"),"g");
    // console.log("reg_eks",reg_eks)
    // $("html").html(reg_parts_ff.join("|"))
    // return;
    console.log(formula)
	this.getSplittedFNWithAtSign = formula.match(reg_eks);
	//getting fieldnames without at sign
	this.getSplittedFieldNames = [];
	if(this.getSplittedFNWithAtSign != null){
        this.getSplittedFNWithAtSign.filter(Boolean);
        if(this.getSplittedFNWithAtSign.length >= 1){
            for(var ctr_i = 0 ; ctr_i < this.getSplittedFNWithAtSign.length ; ctr_i ++){ //skip function names
                if(CheckValueType(this.getSplittedFNWithAtSign[ctr_i]) == "function"){
                    //skip function keywords push
                }else{
                    this.getSplittedFieldNames.push(this.getSplittedFNWithAtSign[ctr_i].replace("@",""));
                }
            }

            // for(var ctr_i = 0 ; ctr_i < this.getSplittedFNWithAtSign.length ; ctr_i ++){
            //     // this.getSplittedFieldNames.push( this.getSplittedFNWithAtSign[ctr_i].replace(/@/,"") );
            //     var reg_ekis = new RegExp(reg_parts_ff.filter(function(arr_value, arr_index){
            //         if(arr_index==5){return false;}else{return true;}
            //     }).join("|"),"g");


            //     if(reg_ekis.test(this.getSplittedFNWithAtSign[ctr_i]) == true ){ //DITO NA AKO EHH PARA SA EMBED
            //         this.getSplittedFieldNames.push(this.getSplittedFNWithAtSign[ctr_i].replace(/@/g,""));
            //     }else{
                    
            //     }
            // }
        }
	}

	//call back with this keyword
	if(typeof CB != "undefined"){
		this.callBack = CB;
		this.callBack();
	}
}
function CheckValueType(str_to_eval){
    var value_type_of = "";
    try{
        eval("typeof "+str_to_eval);
        value_type_of = eval("typeof "+str_to_eval);
    }catch(error){
        value_type_of = "object";
    }
    return value_type_of;
}
function getJQElementsVal(JQEle){
    if(JQEle.is('input[type="radio"]')){
        return JQEle.filter(':checked');
    }else if(JQEle.is('select:not([multiple])')){
        return JQEle.filter('select:not([multiple])');
    }else{
        return JQEle;
    }
}
function HasValue(param1,param2){
    var args_param = arguments;
    if($.type(param1) ){//kapag array
        if(param1.indexOf(param2) >= 0){
            return true;
        }else{
            return false;
        }
    }else{ // kapag object or string
        if($(param1).is('select:not([multiple])')){
            if($(param1).val() == param2){
                return true;
            }else{
                return false;
            }
        }else if($(param1).is('select[multiple]')){
            if($(param1).val().indexOf(param2) >= 0){
                return true;
            }else{
                return false;
            }
        }else if($(param1).is('input[type="checkbox"]')){
            if(
                $(param1).filter('input[type="checkbox"]').filter(":checked").get().map(function(ele_val,eqindex){
                    return $(ele_val).val();
                }).indexOf(param2) >= 0
            ){
                return true;
            }else{
                return false;
            }
        }else if($(param1).is('input[type="radio"]')){
            if($(param1).filter('input[type="radio"]').val() == param2){
                return true;
            }else{
                return false;
            }
        }
    }
}
Formula.prototype.getFieldNames = function(){
	if($.type(this.getSplittedFNWithAtSign) == "array"){
		var splitted_ffs = this.getSplittedFNWithAtSign;
	}else{
		var splitted_ffs = [];
	}
    

   	splitted_ffs = $.unique(splitted_ffs.filter(Boolean)).map(function(a){
   		var each_val = a.replace("@","");
   		if($.type(window[each_val]) == "function"){
   			return {
   				"at_chars":each_val,
   				"type":"function"
   			};
		}else if($('label[name="'+each_val+'"]').length >= 1 ){
   			return {
   				"at_chars":each_val,
   				"type":"label_name"
   			};
   		}else if($('[name="'+each_val+'"]:not(label)').length >= 1 ){
   			return {
   				"at_chars":each_val,
   				"type":"field_name"
   			};
   		}else if($('[name="'+each_val+'[]"]').length >= 1){
   			return {
   				"at_chars":each_val,
   				"type":"multi_field_name"
   			};
   		}else if($('[embed-name="'+each_val+'"]').length >= 1){
   			return {
   				"at_chars":each_val,
   				"type":"embed_name"
   			};
		}else if($('[tab-name="'+each_val+'"]').length >= 1){
			return {
   				"at_chars":each_val,
   				"type":"tab_panel_name"
   			};
   		}
   	});
    return splitted_ffs;
};
Formula.prototype.getValidFieldNames = function(){
}
Formula.prototype.getMyFormula = function() {
	return this.formula;
};
Formula.prototype.getProcessedFormula = function() {
	var toProcessFormula = "";
	var splittedFieldNames = "";
	if(this.getSplittedFNWithAtSign != null){
	toProcessFormula = this.formula;
	var formula_keywords = ["Status", "Requestor", "RequestID", "CurrentUser", "TrackNo", "Department", "Processor","Today","Now","TimeStamp","ActionButton"]; /// keywords is an element too
	splittedFieldNames = this.getSplittedFieldNames;
	    for(var ii = 0 ; ii < splittedFieldNames.length ; ii++ ){
		var element_in_progress = $('[name="'+splittedFieldNames[ii]+'"]');
		if(element_in_progress.length <= 0){
			element_in_progress = $('[name="'+splittedFieldNames[ii]+'[]"]');
		}
		var fn_progress_keywords = splittedFieldNames[ii];
		if(formula_keywords.indexOf(fn_progress_keywords) < 0){ // KAPAG KEYWORD KASi EXISTING DIN UNG MGA TRACK NO KEWORDS
		    if(element_in_progress.length >=1){
			var re = new RegExp("@"+splittedFieldNames[ii]+"(?![0-9a-zA-Z_])","g");
			if(($(element_in_progress).val()||"").length >= 1){ // not empty value for a specific element
			    if( $.isNumeric( element_in_progress.val() ) && false ){//kapag number ung laman nung fieldname
				toProcessFormula = toProcessFormula.replace(re, Number(element_in_progress.val()) );
			    }else{ // kapag hindi number ung laman ni fieldname
				if(typeof element_in_progress.attr("data-input-type") != "undefined"){
				    if(element_in_progress.attr("data-input-type") == "Currency"){
					toProcessFormula = toProcessFormula.replace(re,
					    Number( cleanStringValue( cleanNumberValue(
						(element_in_progress.is('input[type="radio"]'))?element_in_progress.filter(":checked").val():element_in_progress.val()
					    ) ) )
					);
				    }else if(element_in_progress.attr("data-input-type") == "Number"){
					toProcessFormula = toProcessFormula.replace(re,
					    Number( cleanStringValue( cleanNumberValue(
						(element_in_progress.is('input[type="radio"]'))?element_in_progress.filter(":checked").val():element_in_progress.val()
					    ) ) )
					);
				    }else{ //if(element_in_progress.attr("data-input-type") == "Text")
						// toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(
						//     (element_in_progress.is('input[type="radio"]'))?element_in_progress.filter(":checked").val():element_in_progress.val()
						// ) + "\"" );
						toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(
						    element_in_progress.map(function(a,b){
						    	var self = $(this);
						    	if(self.is('select[multiple]')){
						    		return self.children('option:selected').map(function(a,b){ return $(b).val() }).get().join('|^|');
						    	}else if(self.is('input[type="checkbox"]') || self.is('input[type="radio"]')){
					    			return self.filter(':checked').val();
						    	}else{
						    		return self.val();
						    	}
						    }).get().join('|^|')
						) + "\"" );
				    }
				}else{ // DEFAULT
				    if(element_in_progress.is('input[type="radio"]')){
					toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(element_in_progress.filter(":checked").val()) + "\"" );
				    }else{
						// toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(element_in_progress.val()) + "\"" );
						toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(
						    element_in_progress.map(function(a,b){
						    	var self = $(this);
						    	if(self.is('select[multiple]')){
						    		return self.children('option:selected').map(function(a,b){ return $(b).val() }).get().join('|^|');
						    	}else if(self.is('input[type="checkbox"]') || self.is('input[type="radio"]')){
					    			return self.filter(':checked').val();
						    	}else{
						    		return self.val();
						    	}
						    }).get().join('|^|')
						) + "\"" );
				    }
				}
			    }
			}else{//kapag walang lamang ung value nung fieldname
			    toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(element_in_progress.val()) + "\"" );
			}
		    }
		}else if(formula_keywords.indexOf(fn_progress_keywords) >= 0){ // KAPAG keywords nga talaga
		    console.log("seadas",fn_progress_keywords)
		    var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
		    if(fn_progress_keywords == "Today"){
			var this_today = new Date(getUpdatedServerTime());
			this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2);
			var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
			toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(this_today) + "\"" );
		    }else if(fn_progress_keywords == "Now"){
			var this_today = new Date(getUpdatedServerTime());
			this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2) + " " + StrRight("0" + this_today.getHours(), 2) + ":" + StrRight("0" + this_today.getMinutes(), 2) + ":" + StrRight("0" + this_today.getSeconds(), 2);
			var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
			toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(this_today) + "\"" );
		    }else if(fn_progress_keywords == "RequestID"){
			var request_id_ele = $('[name="ID"]');
			var rid_val = request_id_ele.val();
			var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
			toProcessFormula = toProcessFormula.replace(re, cleanStringValue(rid_val) );
		    }else if(fn_progress_keywords == "TrackNo"){
			var track_no_ele = $("#frmrequest").find('[name="TrackNo"]').eq(0);
			if(track_no_ele.length >=1){
			    var getTrackNoVal = track_no_ele.val();
			    var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
			    toProcessFormula = toProcessFormula.replace(re, "\""+cleanStringValue(getTrackNoVal)+"\"" );
			}
		    }else if(fn_progress_keywords == "CurrentUser"){ // I MODIFY SPECIFIC KEYWORDS VALUE
			var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
			var CurrentUserName = "";
			if(typeof element_in_progress.attr("current-user-name") != "undefined"){
			    CurrentUserName = element_in_progress.attr("current-user-name");
			}
			toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(CurrentUserName) + "\"");
		    }else if(fn_progress_keywords == "Requestor"){
			var requestor_name = "";
			if ($("[name='ID']").val() == "0") {
			    var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
			    requestor_name = requestor_ele.attr("requestor-name");
			} else {
			    var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
			    requestor_name = requestor_ele.attr("requestor-name");
			}
			var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
			toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(requestor_name) + "\"");
		    }else if(fn_progress_keywords == "Status"){
			var status_ele = $("#frmrequest").find('[name="Status"]').eq(0);
			if(status_ele.length >=1){
			    var getStatusVal = status_ele.val();
			    toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(getStatusVal) + "\"");
			}
		    }else if(fn_progress_keywords == "Department"){
			var getDep_ele = $("[name='Department_Name']");
			var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
			if(getDep_ele.length >= 1){
			    var getDepName = getDep_ele.val();
			    toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(getDepName) + "\"");
			}
		    }else if(fn_progress_keywords == "Processor"){
			var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
			if ($("[name='ID']").val() == "0") {
			    var processor_user_ele = $('[name="Processor"]');
			    var processor_name = processor_user_ele.attr("processor-name");
			    toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(processor_name) + "\"");
			} else {
			    var processor_user_ele = $("#processor_display");
			    var processor_name = processor_user_ele.text();
			    toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(processor_name) + "\"");
			}
		    }else if (fn_progress_keywords == "ActionButton"){ // IAN REQUEST [08-25-2016]
		    	var action_button_click_ele = $(".temp-action-button-class-formula-keyword");
			    var action_button_name = action_button_click_ele.text();
			    toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(action_button_name) + "\"");
		    }else if(element_in_progress.length >= 1){// DEFAULT FOR KEYWORDS
			var re = new RegExp("@"+fn_progress_keywords+"(?![a-zA-Z0-9_])","g");
			toProcessFormula = toProcessFormula.replace(re, "\"" + cleanStringValue(element_in_progress.val()) + "\"");
		    }
		}
	    }
	}else{ // KAPAG WALANG NAKITANG MAY AT SAYN
	    toProcessFormula = this.formula;
	}


	toProcessFormula = toProcessFormula.replace(/@/g,"");
	this.processedFormula = toProcessFormula;
    // console.log(toProcessFormula);
	return toProcessFormula;
};
Formula.prototype.fieldBinderEvent = function(option){
	var self = this;
	var at_char_founds = self.getFieldNames().filter(function(val,c_index){
		return val['type'] == "field_name" || val['type'] == "multi_field_name"
	});
	var settings = {
		"callBack":function(){
		},
		"targetEle":$([])
	};
	$.extend(settings,option);

	var existing_field_ele = [];
	for( var ctr_index in at_char_founds ){
		if(at_char_founds[ctr_index]['type'] == 'field_name'){
			existing_field_ele = $('[name="'+at_char_founds[ctr_index]['at_chars']+'"]');
		}else if(at_char_founds[ctr_index]['type'] == 'multi_field_name'){
			existing_field_ele = $('[name="'+at_char_founds[ctr_index]['at_chars']+'[]"]');
		}


		if($.type(settings['callBack']) == "function"){
			settings['callBack'].call(existing_field_ele,{
				"formula":self.formula,
				"target":settings['targetEle'],
				"atChars":at_char_founds[ctr_index]['at_chars']
			});
		}
	}
}

Formula.prototype.getEvaluation = function() {
	var processed_formula = this.getProcessedFormula();
	var evaluation_status = false;
	try{
		eval(processed_formula)
		evaluation_status = true;
	}catch(err){
		evaluation_status = false;
	}


	if(evaluation_status == true){
        // alert("processed_formula"+processed_formula+"\nEMBEDDED COMPUTED VALUE= "+eval(processed_formula))
		return eval(processed_formula);
	}else{
		return "Invalid formula";
	}
};

// function GetRound(given_num, decimal_digit) {
//     var decimal_places = "1";
//     for (var ii = 1; ii <= decimal_digit; ii++) {
//         decimal_places += "" + "0";
//     }
//     decimal_places = Number(decimal_places);
//     return Math.round(given_num * decimal_places) / decimal_places;
// }

function getUpdatedServerTime() {
    if (typeof $("body").data() != "undefined") {
        if (typeof $("body").data().getServerTimeData != "undefined") {
            if (typeof $("body").data().getServerTimeData != "undefined") {
                return $("body").data().getServerTimeData;
            }
            // if (typeof $("body").data().get_counter_data.getDateTime != "undefined") {
            //     return $("body").data().get_counter_data.getDateTime;
            // }
        }
    }
    return "";
}

function cleanStringValue(value){
	var clean_string_progress = ""+value;
	clean_string_progress = clean_string_progress.replace(/\\/g, "\\\\");
    clean_string_progress = clean_string_progress.replace(/\"/g, "\\\"");
    return clean_string_progress;
}
function cleanNumberValue(value){
	var clean_number_progress = ""+value;
	var re = new RegExp(",","g");
	return clean_number_progress.replace(re,"");
}
function sanitizeString(){ /// FOR SECURITY

}

// function Sum(sum_fn) {
//     var fieldname = sum_fn;
//     var summation = 0;
//     if (fieldname.indexOf("@") >= 0) {
//         fieldname = fieldname.replace("@", "");
//     }
//     summation = summation + Number($('[name="' + fieldname + '"]').val().replace(/,/g,""));

//     $('[rep-original-name="' + fieldname + '"]').each(function(eqi) {
//         summation = summation + Number($(this).val().replace(/,/g,""));
//     });

//     return summation;
// }

function Sum(sum_fn) {
    var fieldname = sum_fn;
    var summation = 0;
    if (fieldname.indexOf("@") >= 0) {
        fieldname = fieldname.replace("@", "");
    }
    summation = summation + Number($('[name="' + fieldname + '"]').val().replace(/,/g,""));

    $('[rep-original-name="' + fieldname + '"]').each(function(eqi) {
        summation = summation + Number($(this).val().replace(/,/g,""));
    });

    return summation;
}

// function ThisDate(this_date) {
//     var parse_first = this_date;
//     if($.type(parse_first) == "date" || $.type(parse_first) == "string" || $.type(parse_first) == "number" ){
//     	var this_time = parse_first.toString().match(/..:..:..\s[AMPamp]{2}|..:..\s[AMPamp]{2}|..:..:..|..:../g);
//     	if (this_time != null) {
//     	    var this_time2 = this_time[0].replace(/\s[A-Za-z]{2}/g, "");
//     	    var this_time_splitted = this_time2.split(":");
//     	    var time_hours = "";
//     	    var time_minutes = "";
//     	    var time_seconds = "";
//     	    if (typeof this_time_splitted[0] != "undefined") { //hours
//     	        time_hours = this_time_splitted[0];
//     	    }
//     	    if (typeof this_time_splitted[1] != "undefined") { //minutes
//     	        time_minutes = this_time_splitted[1];
//     	    }
//     	    var dis_date = parse_first.toString().replace(this_time[0], "");
    	    
//     	    //console.log("TARGET taas",$.trim(dis_date))
//     	    var this_new_date = new Date($.trim(dis_date));

//     	    this_new_date = new Date(this_new_date.setHours(time_hours));

//     	    this_new_date = new Date(this_new_date.setMinutes(time_minutes));
//     	} else {
//     	    //console.log("TARGET baba",this_date)
//     	    var this_new_date = new Date(this_date);
//     	}


//     	// var converted_date = (this_new_date.getFullYear())+"-"+(this_new_date.getMonth()+1)+"-"+(this_new_date.getDate());
//     	if (this_new_date == "Invalid Date") {
//     	    return this_new_date;
//     	} else {
//     	    return this_new_date;
//     	}
//     }else{
//     	return ThisDate($.type(parse_first));
//     }
// }
function ThisDate(this_date) {
	var parse_first = this_date;
	if($.type(parse_first) == "date" || $.type(parse_first) == "string" || $.type(parse_first) == "number" ){
		var this_time = parse_first.toString().match(/..:..:..\s[AMPamp]{2}|..:..\s[AMPamp]{2}|..:..:..|..:../g);
		if (this_time != null) {
			var this_time2 = this_time[0].replace(/\s[A-Za-z]{2}/g, "");
			var this_time_splitted = this_time2.split(":");
			var time_hours = "";
			var time_minutes = "";
			var time_seconds = "";
			if (typeof this_time_splitted[0] != "undefined") { //hours
				time_hours = this_time_splitted[0];
			}
			if (typeof this_time_splitted[1] != "undefined") { //minutes
				time_minutes = this_time_splitted[1];
			}
			var dis_date = parse_first.toString().replace(this_time[0], "");

			//console.log("TARGET taas",$.trim(dis_date))
			var this_new_date = new Date($.trim(dis_date));
			this_new_date = new Date(this_new_date.setHours(time_hours));

			this_new_date = new Date(this_new_date.setMinutes(time_minutes));
			if(time_hours == "" && time_minutes == ""){
				offset = this_new_date.getTimezoneOffset();
				this_new_date = new Date(this_new_date.valueOf() + offset * 60000);
			}
			
		} else {
			//console.log("TARGET baba",this_date)u
			//new Date(curr_date.valueOf() + d * 60000);
			console.log(this_date);
			var get_time= undefined;
			try{
				get_time = this_date.spilt(' ')[1];
			}
			catch(e){

			}
			var this_new_date = new Date(this_date);
			console.log($.type(get_time));
			if(typeof get_time === "undefined"){
				var this_new_date = new Date(this_date);
				offset = this_new_date.getTimezoneOffset();
				// this_new_date = new Date(this_new_date.valueOf() + offset * 60000);
			}
		}


		// var converted_date = (this_new_date.getFullYear())+"-"+(this_new_date.getMonth()+1)+"-"+(this_new_date.getDate());
		if (this_new_date == "Invalid Date") {
			return this_new_date;
		} else {
			return this_new_date;
		}
	}else{
		return ThisDate($.type(parse_first));
	}
}
Number.prototype.currencyFormat = function(){
    // (?<=\d)(?=(\d\d\d)+(?!\d))
    // return this.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    // console.log(this)
    // console.log("ow")

    var float_value = this.toString().split(".");
    var integer = float_value[0];
    var decimal = "";
    var negative_value = false;
    var ret = "";
    if(integer[0] == "-"){
        integer = integer.substr(1,integer.length);
        negative_value = true;
    }

    if(float_value.length == 2){
        decimal = float_value[1];
        integer = integer.split(/(?=(?:...)*$)/).join(",");
        ret = integer+"."+(decimal+"00").substr(0,2);
    }else{
        integer = integer.split(/(?=(?:...)*$)/).join(",");
        ret = integer+".00";
    }
    if(negative_value){
        ret = "-"+ret;
    }
    return ret;
};

Date.prototype.formatDate = function(format) {
    if (this == "Invalid Date") {
        return this;
    } else {
        timestamp = this;
        // http://kevin.vanzonneveld.net
        // +   original by: Carlos R. L. Rodrigues (http://www.jsfromhell.com)
        // +      parts by: Peter-Paul Koch (http://www.quirksmode.org/js/beat.html)
        // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: MeEtc (http://yass.meetcweb.com)
        // +   improved by: Brad Touesnard
        // +   improved by: Tim Wiel
        // +   improved by: Bryan Elliott
        // +   improved by: David Randall
        // +      input by: Brett Zamir (http://brett-zamir.me)
        // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +   improved by: Theriault
        // +  derived from: gettimeofday
        // +      input by: majak
        // +   bugfixed by: majak
        // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +      input by: Alex
        // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
        // +   improved by: Theriault
        // +   improved by: Brett Zamir (http://brett-zamir.me)
        // +   improved by: Theriault
        // +   improved by: Thomas Beaucourt (http://www.webapp.fr)
        // +   improved by: JT
        // +   improved by: Theriault
        // +   improved by: Rafał Kukawski (http://blog.kukawski.pl)
        // +   bugfixed by: omid (http://phpjs.org/functions/380:380#comment_137122)
        // +      input by: Martin
        // +      input by: Alex Wilson
        // +      input by: Haravikk
        // +   improved by: Theriault
        // +   bugfixed by: Chris (http://www.devotis.nl/)
        // %        note 1: Uses global: php_js to store the default timezone
        // %        note 2: Although the function potentially allows timezone info (see notes), it currently does not set
        // %        note 2: per a timezone specified by date_default_timezone_set(). Implementers might use
        // %        note 2: this.php_js.currentTimezoneOffset and this.php_js.currentTimezoneDST set by that function
        // %        note 2: in order to adjust the dates in this function (or our other date functions!) accordingly
        // *     example 1: date('H:m:s \\m \\i\\s \\m\\o\\n\\t\\h',  );
        // *     returns 1: '09:09:40 m is month'
        // *     example 2: date('F j, Y, g:i a', 1062462400);
        // *     returns 2: 'September 2, 2003, 2:26 am'
        // *     example 3: date('Y W o', 1062462400);
        // *     returns 3: '2003 36 2003'
        // *     example 4: x = date('Y m d', (new Date()).getTime()/1000);
        // *     example 4: (x+'').length == 10 // 2009 01 09
        // *     returns 4: true
        // *     example 5: date('W', 1104534000);
        // *     returns 5: '53'
        // *     example 6: date('B t', 1104534000);
        // *     returns 6: '999 31'
        // *     example 7: date('W U', 1293750000.82); // 2010-12-31
        // *     returns 7: '52 1293750000'
        // *     example 8: date('W', 1293836400); // 2011-01-01
        // *     returns 8: '52'
        // *     example 9: date('W Y-m-d', 1293974054); // 2011-01-02
        // *     returns 9: '52 2011-01-02'
        var that = this,
                jsdate,
                f,
                // Keep this here (works, but for code commented-out
                // below for file size reasons)
                //, tal= [],
                txt_words = ["Sun", "Mon", "Tues", "Wednes", "Thurs", "Fri", "Satur", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                // trailing backslash -> (dropped)
                // a backslash followed by any character (including backslash) -> the character
                // empty string -> empty string
                formatChr = /\\?(.?)/gi,
                formatChrCb = function(t, s) {
                    return f[t] ? f[t]() : s;
                },
                _pad = function(n, c) {
                    n = String(n);
                    while (n.length < c) {
                        n = '0' + n;
                    }
                    return n;
                };
        f = {
            // Day
            d: function() { // Day of month w/leading 0; 01..31
                return _pad(f.j(), 2);
            },
            D: function() { // Shorthand day name; Mon...Sun
                return f.l().slice(0, 3);
            },
            j: function() { // Day of month; 1..31
                return jsdate.getDate();
            },
            l: function() { // Full day name; Monday...Sunday
                return txt_words[f.w()] + 'day';
            },
            N: function() { // ISO-8601 day of week; 1[Mon]..7[Sun]
                return f.w() || 7;
            },
            S: function() { // Ordinal suffix for day of month; st, nd, rd, th
                var j = f.j(),
                        i = j % 10;
                if (i <= 3 && parseInt((j % 100) / 10, 10) == 1) {
                    i = 0;
                }
                return ['st', 'nd', 'rd'][i - 1] || 'th';
            },
            w: function() { // Day of week; 0[Sun]..6[Sat]
                return jsdate.getDay();
            },
            z: function() { // Day of year; 0..365
                var a = new Date(f.Y(), f.n() - 1, f.j()),
                        b = new Date(f.Y(), 0, 1);
                return Math.round((a - b) / 864e5);
            },
            // Week
            W: function() { // ISO-8601 week number
                var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3),
                        b = new Date(a.getFullYear(), 0, 4);
                return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
            },
            // Month
            F: function() { // Full month name; January...December
                return txt_words[6 + f.n()];
            },
            m: function() { // Month w/leading 0; 01...12
                return _pad(f.n(), 2);
            },
            M: function() { // Shorthand month name; Jan...Dec
                return f.F().slice(0, 3);
            },
            n: function() { // Month; 1...12
                return jsdate.getMonth() + 1;
            },
            t: function() { // Days in month; 28...31
                return (new Date(f.Y(), f.n(), 0)).getDate();
            },
            // Year
            L: function() { // Is leap year?; 0 or 1
                var j = f.Y();
                return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0;
            },
            o: function() { // ISO-8601 year
                var n = f.n(),
                        W = f.W(),
                        Y = f.Y();
                return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
            },
            Y: function() { // Full year; e.g. 1980...2010
                return jsdate.getFullYear();
            },
            y: function() { // Last two digits of year; 00...99
                return f.Y().toString().slice(-2);
            },
            // Time
            a: function() { // am or pm
                return jsdate.getHours() > 11 ? "pm" : "am";
            },
            A: function() { // AM or PM
                return f.a().toUpperCase();
            },
            B: function() { // Swatch Internet time; 000..999
                var H = jsdate.getUTCHours() * 36e2,
                        // Hours
                        i = jsdate.getUTCMinutes() * 60,
                        // Minutes
                        s = jsdate.getUTCSeconds(); // Seconds
                return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
            },
            g: function() { // 12-Hours; 1..12
                return f.G() % 12 || 12;
            },
            G: function() { // 24-Hours; 0..23
                return jsdate.getHours();
            },
            h: function() { // 12-Hours w/leading 0; 01..12
                return _pad(f.g(), 2);
            },
            H: function() { // 24-Hours w/leading 0; 00..23
                return _pad(f.G(), 2);
            },
            i: function() { // Minutes w/leading 0; 00..59
                return _pad(jsdate.getMinutes(), 2);
            },
            s: function() { // Seconds w/leading 0; 00..59
                return _pad(jsdate.getSeconds(), 2);
            },
            u: function() { // Microseconds; 000000-999000
                return _pad(jsdate.getMilliseconds() * 1000, 6);
            },
            // Timezone
            e: function() { // Timezone identifier; e.g. Atlantic/Azores, ...
                // The following works, but requires inclusion of the very large
                // timezone_abbreviations_list() function.

                throw 'Not supported (see source code of date() for timezone on how to add support)';
            },
            I: function() { // DST observed?; 0 or 1
                // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
                // If they are not equal, then DST is observed.
                var a = new Date(f.Y(), 0),
                        // Jan 1
                        c = Date.UTC(f.Y(), 0),
                        // Jan 1 UTC
                        b = new Date(f.Y(), 6),
                        // Jul 1
                        d = Date.UTC(f.Y(), 6); // Jul 1 UTC
                return ((a - c) !== (b - d)) ? 1 : 0;
            },
            O: function() { // Difference to GMT in hour format; e.g. +0200
                var tzo = jsdate.getTimezoneOffset(),
                        a = Math.abs(tzo);
                return (tzo > 0 ? "-" : "+") + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
            },
            P: function() { // Difference to GMT w/colon; e.g. +02:00
                var O = f.O();
                return (O.substr(0, 3) + ":" + O.substr(3, 2));
            },
            T: function() { // Timezone abbreviation; e.g. EST, MDT, ...
                // The following works, but requires inclusion of the very
                // large timezone_abbreviations_list() function.

                return 'UTC';
            },
            Z: function() { // Timezone offset in seconds (-43200...50400)
                return -jsdate.getTimezoneOffset() * 60;
            },
            // Full Date/Time
            c: function() { // ISO-8601 date.
                return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
            },
            r: function() { // RFC 2822
                return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
            },
            U: function() { // Seconds since UNIX epoch
                return jsdate / 1000 | 0;
            }
        };
        this.date = function(format, timestamp) {
            that = this;
            jsdate = (timestamp === undefined ? new Date() : // Not provided
                    (timestamp instanceof Date) ? new Date(timestamp) : // JS Date()
                    new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
                    );
            return format.replace(formatChr, formatChrCb);
        };
        return this.date(format, timestamp);
    }//end of if
}
Date.prototype.diffHoursIn = function(date2) {
    // / 1000 / 60 / 60
    if (typeof date2 != "undefined" && this != "Invalid Date") {
        console.log("date2", date2)
        var date2_dt_obj =  ThisDate(date2);
        console.log("date2_dt_obj", date2_dt_obj);
        var t2 = date2_dt_obj.getTime();
        var t1 = this.getTime();
        return /*Math.abs*/((t1 - t2) / 1000 / 60 / 60);
    } else {
        return "";
    }
}
Date.prototype.diffDaysIn = function(date2) {
    if (typeof date2 != "undefined" && this != "Invalid Date") {
        var date2_dt_obj = ThisDate(date2);
        var t2 = date2_dt_obj.getTime();
        var t1 = this.getTime();
        return /*Math.abs*/(parseInt((t1 - t2) / (24 * 3600 * 1000), 10));
    } else {
        return "";
    }
}
Date.prototype.diffWeeksIn = function(date2) {
    if (typeof date2 != "undefined" && date2.length >= 1 && this != "Invalid Date") {
        var date2_dt_obj = ThisDate(date2);
        var t2 = date2_dt_obj.getTime();
        var t1 = this.getTime();

        return /*Math.abs*/(parseInt((t1 - t2) / (24 * 3600 * 1000 * 7)));
    } else {
        return "";
    }
}
Date.prototype.diffMonthsIn = function(date2) {
    if (typeof date2 != "undefined" && date2.length >= 1 && this != "Invalid Date") {
        var date2_dt_obj = ThisDate(date2);
        var d1Y = this.getFullYear();
        var d2Y = date2_dt_obj.getFullYear();
        var d1M = this.getMonth();
        var d2M = date2_dt_obj.getMonth();
        return /*Math.abs*/((d1M + 12 * d1Y) - (d2M + 12 * d2Y));
    } else {
        return "";
    }
}

Date.prototype.diffYearsIn = function(date2) {
    if (typeof date2 != "undefined" && date2.length >= 1 && this != "Invalid Date") {
        var date2_dt_obj = ThisDate(date2);
        return /*Math.abs*/(date2_dt_obj.getFullYear() - this.getFullYear());
    } else {
        return "";
    }
}

Date.prototype.adjustSeconds = function(value) {
    if ($.isNumeric(value)) {
        var new_date = this.setSeconds(parseInt(this.getSeconds()) + parseInt(value));
        var obj_new_date = ThisDate(new_date);
        return obj_new_date;
    } else {
        return "";
    }
}
Date.prototype.adjustMinutes = function(value) {
    if ($.isNumeric(value)) {
        var new_date = this.setMinutes(parseInt(this.getMinutes()) + parseInt(value));
        var obj_new_date = ThisDate(new_date);
        return obj_new_date;
    } else {
        return "";
    }
}
Date.prototype.adjustHours = function(value) {
    if ($.isNumeric(value)) {
        var new_date = this.setHours(parseInt(this.getHours()) + parseInt(value));
        var obj_new_date = ThisDate(new_date);
        return obj_new_date;
    } else {
        return "";
    }
}
Date.prototype.adjustDay = function(value) {
    if ($.isNumeric(value)) {
        var new_date = this.setDate(parseInt(this.getDate()) + parseInt(value));
        var obj_new_date = ThisDate(new_date);
        return obj_new_date;
    } else {
        return "";
    }
}
Date.prototype.adjustMonth = function(value) {
    if ($.isNumeric(value)) {
        var new_date = this.setMonth(parseInt(this.getMonth()) + parseInt(value));
        var obj_new_date = ThisDate(new_date);
        return obj_new_date;
    } else {
        return "";
    }
}
Date.prototype.adjustYear = function(value) {
    if ($.isNumeric(value)) {
        var new_date = this.setFullYear(parseInt(this.getFullYear()) + parseInt(value));
        var obj_new_date = ThisDate(new_date);
        return obj_new_date;
    } else {
        return "";
    }
}

function GivenIf(condition, valueTrue, valueFalse) {

    try {
        //splitted_condition_val = condition.split(/<=|>=|==|!=|<|>|\(|\)|!/g);
        if (eval(condition)) {
        	
        	if($.type(valueTrue)==="object"){

        		valueTrue["execute"]();
        		return valueTrue+"";
        	}
        	else{

        		return valueTrue;	
        	}
            
        } else {
            if($.type(valueFalse)==="object"){

	           	valueFalse["execute"]();
	           	return valueFalse+"";
            }else if($.type(valueFalse) != "undefined"){
            	
	            return valueFalse;
            
            
           }else{

				return valueFalse;
            }
        }
    } catch (error) {
        return "Conditional Error!";
    }
}
// function StrConcat() {
//     try {
//         var collect_strings = "";
//         for (var ctr = 0; ctr < arguments.length; ctr++) {
//             //
//             collect_strings += arguments[ctr];
//         }
//         return collect_strings;
//     } catch (error) {
//         console.log("ERROR ON USAGE OF CONCATENATION");
//     }
// }

// function Lookup(formName, returnField, filterName, filterValue) {
//     try {
//         $.get("ajax/", {
//             "access_formula": "Lookup",
//             "formName": formName,
//             "returnField": returnField,
//             "filterName": filterName,
//             "filterValue": filterValue
//         }, function(echoData) {

//         });
//     } catch (error) {

//     }
// }

function StrLeft(str_val, str_length) {
    var str_to_process = String(str_val);
    return str_to_process.substr(0, str_length);
}
function StrRight(str_val, str_length) {
    var str_to_process = String(str_val);
    var start = (str_to_process.length - (str_length));
    if (start <= -1) {
        start = 0;
    }
    return str_to_process.substr(start, str_length);
}
function StrCount(str_val) {
    if (typeof str_val != "undefined") {
        return String(str_val).length;
    }
}
function StrConcat(){
	var args = arguments;
	console.log("args",args)
	var args_length = args.length;
	var args_array = [];
	var args_string = "";
	for(var args_ctr = 0; args_ctr<=args_length-1; args_ctr++){
		args_array = args_array.concat(args[args_ctr]);
	}
	args_string = args_array.filter(Boolean).join("");

	return args_string;
}
// function StrConcat(){
//     var args = arguments;//infinite argument
//     var collect_strings = "";
//     for(var ctr = 0 ; ctr < args.length ; ctr++){
//         collect_strings = collect_strings + args[ctr];
//     }
//     return collect_strings;
// }
function StrGet(str_val, str_istart, str_char_len) {
    if (typeof str_val == "undefined") {
        return "";
    }
    var string_val = String(str_val);
    var string_start_index = Number(str_istart);
    if (string_start_index <= 0) {
        return "";
    }
    if (typeof str_char_len == "undefined") {
        return String(string_val.charAt((Number(str_istart) - 1)));
    } else {
        return string_val.substr((Number(str_istart) - 1), Number(str_char_len));
    }
}
//num to words prototype
function CheckFormat(currency){
	if(currency == 'peso' || currency == 'pesos' || currency =='php'){
		return 'centavos';
	}
	if(currency == 'dollars' || currency == 'dollar'){
		return 'cents';
	}


	else{
		return "";
	}

}

function NumToWord(num,format,currency){

	//equivalent words
	// 	var th = ['hundred', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion'];
	// 	var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
	// 	var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
	// 	var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
	// 	var dw = ['tenths', 'hundredths', 'thousandths', 'ten-thousandths', 'hundred-thousandths', 'millionths', 'ten-millionths', 'hundred-millionths', 'billionths', 'ten-billionths', 'hundred-billionths', 'trillionths', 'ten-trillionths', 'hundred-trillionths', 'quadrillionths', 'ten-quadrillionths', 'hundred-quadrillionths'];

	//     var num_string = parseFloat(num).toString();
	//     var array_form = num_string.split('.');
	//     var array_for_whole = [];
	//     var array_for_decimal = [];
	//     var array_form_len = array_form.length;
	 	
	//  	for(var i=0;i<=array_form_len-1;i++){
	//  		//para sa whole numbers
	//  		if(i == 0){
	//  			var whole_number = array_form[0];
	//  			var n = whole_number.length;
	//  			for(var toarray = 0 ; toarray <= n-1 ; toarray++){
	//  				array_for_whole.push(whole_number[toarray]);
	//  			}
 	
	//  		}
	//  		//para sa decimals
	//  		if(i == 1){
	//  			var decimals = array_form[1];
	//  			var n = decimals.length;

	//  		}
	//  	}
	 
	// return array_for_whole;

//  		}
//  	}
 
// return array_for_whole;

 var ConvertHere = function (s) {
	s = s.replace(/[\, ]/g, '');
	var reg_exp_dec = new RegExp('\\+\\d*', 'g');
	var str_e_digit = s.match(reg_exp_dec);
	var e_digit = 0;
	if (str_e_digit != null) {
	    str_e_digit = str_e_digit.filter(Boolean);

	    e_digit = Number(str_e_digit[0]);
	}
	// console.log("CONVERT DIGITS",e_digit,str_e_digit, typeof str_e_digit);

	if (s != parseFloat(s))
	    return 'not a number';

	var x = s.indexOf('.');
	if (x == -1)
	    x = s.length;
	if (x > 20 || e_digit > 20)
	    return 'too big';
	var n = s.split('');
	var str = '';
	var sk = 0;

	for (var i = 0; i < x; i++) {

	    if ((x - i) % 3 == 2) {

		if (n[i] == '1') {

		    str += tn[Number(n[i + 1])] + ' ';
		    i++;
		    sk = 1;
		} else if (n[i] != 0) {
		    str += tw[n[i] - 2] + ' ';
		    sk = 1;
		}
	    } else if (n[i] != 0) {

		str += dg[n[i]] + ' ';

		if ((x - i) % 3 == 0)
		    str += 'hundred ';

		sk = 1;
	    }

	    if ((x - i) % 3 == 1) {
		if (sk)
		    str += th[(x - i - 1) / 3] + ' ';
		sk = 0;
	    }
	}
	if (x != s.length) {
	    var y = s.length;
	    str += 'and ';
	    for (var i = x + 1; i < y; i++)
		str += dg[n[i]] + ' ';
	}
	return str.replace(/\s+/g, ' ');
    }
    if (typeof num !== "undefined" || num != null || num != "") {
	if (typeof num === 'string') {
	    var with_comma = num.match(/[\,]/g);
	    if (with_comma != null) {
		with_comma = with_comma.filter(Boolean);
		if (with_comma) {
		    if (with_comma.length >= 1) {
			num = num.replace(/[\,]/g, "");
		    }
		}
	    }
	}
    }
    if (!isNaN(Number(num))) {

		var s = parseFloat(num).toString();
		// Convert numbers to words
		// copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
		// permission to use this Javascript on your web page is granted
		// provided that all of the code (including this copyright notice) is
		// used exactly as shown (you can change the numbering system if you wish)

		// American Numbering System
		var th = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion'];
		// uncomment this line for English Number System
		// var th = ['','thousand','million','milliard','billion'];

		var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
		var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
		var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

		var dw = ['tenths', 'hundredths', 'thousandths', 'ten-thousandths', 'hundred-thousandths', 'millionths', 'ten-millionths', 'hundred-millionths', 'billionths', 'ten-billionths', 'hundred-billionths', 'trillionths', 'ten-trillionths', 'hundred-trillionths', 'quadrillionths', 'ten-quadrillionths', 'hundred-quadrillionths'];

		var w_point = s.match(/\./g);
		var collect_s = "";
		if (w_point != null && typeof w_point !== "undefined") {

	    w_point = w_point.filter(Boolean);

	    if (w_point.length == 1) {
		var w_point_s = s.split(/\./);
		
		s = w_point_s[0];
		collect_s += ConvertHere(s);

		if($.trim(format) !== "undefined" && $.trim(format) !== "" && $.trim(currency) !== "undefined" && $.trim(currency) !== ""){
			if($.trim(format)=="currency"){
				collect_s += ''+currency+" ";
				
			}
			
		}
		collect_s += "and ";
		s = w_point_s[1];
		
			

		
		if (typeof dw[s.length - 1] !== "undefined") {

			if($.trim(format) !== "undefined" && $.trim(format) !== "" && $.trim(currency) !== "undefined" && $.trim(currency) !== ""){
					if($.trim(format)=="check"){
						if(s.length == 1){
							s=s*10;
							s=s.toString();
							
						}
						collect_s += s +"/100";
						collect_s+= ' '+currency;
					}
					else if($.trim(format)=="currency"){
						if(s.length == 1){
							s=s*10;
							s=s.toString();
							
						}
						collect_s += ConvertHere(s);
						collect_s+= ''+CheckFormat(currency);	
					}
					else{
						collect_s += ConvertHere(s);
						collect_s += "" + dw[s.length - 1];	
					}
					
					
							
	   		}
			else{
					collect_s += ConvertHere(s);
					collect_s += "" + dw[s.length - 1];			    
		    }
		}
		return collect_s.charAt(0).toUpperCase()+collect_s.slice(1);

	    } else if (w_point.length >= 2) {
		return 'Invalid number!';
	    } else {

		//dito na ung my fifthy
		return ConvertHere(s).charAt(0).toUpperCase()+ConvertHere.slice(1);
	    }
	} else {
		if($.trim(format) !== "undefined" && $.trim(format) !== "" && num !="" && $.trim(currency) !== "undefined" && $.trim(currency) !== ""){
			
				
			if($.trim(format)=="check" || $.trim(format)=="currency"){
				collect_s += ConvertHere(s);
				collect_s += ''+currency;
				
			}
			else{
				collect_s += ConvertHere(s);
			}
			
		}
		else{
			collect_s += ConvertHere(s);	
		}
		
	    return collect_s.charAt(0).toUpperCase()+collect_s.slice(1);
	}
    } else {
	return 'Not a number!';
    }
}
//panda version
// function NumToWords(num) {

//     var ConvertHere = function (s) {
// 	s = s.replace(/[\, ]/g, '');
// 	var reg_exp_dec = new RegExp('\\+\\d*', 'g');
// 	var str_e_digit = s.match(reg_exp_dec);
// 	var e_digit = 0;
// 	if (str_e_digit != null) {
// 	    str_e_digit = str_e_digit.filter(Boolean);

// 	    e_digit = Number(str_e_digit[0]);
// 	}
// 	// console.log("CONVERT DIGITS",e_digit,str_e_digit, typeof str_e_digit);

// 	if (s != parseFloat(s))
// 	    return 'not a number';

// 	var x = s.indexOf('.');
// 	if (x == -1)
// 	    x = s.length;
// 	if (x > 20 || e_digit > 20)
// 	    return 'too big';
// 	var n = s.split('');
// 	var str = '';
// 	var sk = 0;

// 	for (var i = 0; i < x; i++) {

// 	    if ((x - i) % 3 == 2) {

// 		if (n[i] == '1') {

// 		    str += tn[Number(n[i + 1])] + ' ';
// 		    i++;
// 		    sk = 1;
// 		} else if (n[i] != 0) {
// 		    str += tw[n[i] - 2] + ' ';
// 		    sk = 1;
// 		}
// 	    } else if (n[i] != 0) {

// 		str += dg[n[i]] + ' ';

// 		if ((x - i) % 3 == 0)
// 		    str += 'hundred ';

// 		sk = 1;
// 	    }

// 	    if ((x - i) % 3 == 1) {
// 		if (sk)
// 		    str += th[(x - i - 1) / 3] + ' ';
// 		sk = 0;
// 	    }
// 	}
// 	if (x != s.length) {
// 	    var y = s.length;
// 	    str += 'and ';
// 	    for (var i = x + 1; i < y; i++)
// 		str += dg[n[i]] + ' ';
// 	}
// 	return str.replace(/\s+/g, ' ');
//     }
//     if (typeof num !== "undefined" || num != null || num != "") {
// 	if (typeof num === 'string') {
// 	    var with_comma = num.match(/[\,]/g);
// 	    if (with_comma != null) {
// 		with_comma = with_comma.filter(Boolean);
// 		if (with_comma) {
// 		    if (with_comma.length >= 1) {
// 			num = num.replace(/[\,]/g, "");
// 		    }
// 		}
// 	    }
// 	}
//     }
//     if (!isNaN(Number(num))) {
// 	var s = parseFloat(num).toString();
// 	// Convert numbers to words
// 	// copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
// 	// permission to use this Javascript on your web page is granted
// 	// provided that all of the code (including this copyright notice) is
// 	// used exactly as shown (you can change the numbering system if you wish)

// 	// American Numbering System
// 	var th = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion'];
// 	// uncomment this line for English Number System
// 	// var th = ['','thousand','million','milliard','billion'];

// 	var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
// 	var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
// 	var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

// 	var dw = ['tenths', 'hundredths', 'thousandths', 'ten-thousandths', 'hundred-thousandths', 'millionths', 'ten-millionths', 'hundred-millionths', 'billionths', 'ten-billionths', 'hundred-billionths', 'trillionths', 'ten-trillionths', 'hundred-trillionths', 'quadrillionths', 'ten-quadrillionths', 'hundred-quadrillionths'];

// 	var w_point = s.match(/\./g);
// 	if (w_point != null && typeof w_point !== "undefined") {
// 	    w_point = w_point.filter(Boolean);

// 	    if (w_point.length == 1) {
// 		var w_point_s = s.split(/\./);
// 		var collect_s = "";
// 		s = w_point_s[0];
// 		collect_s += ConvertHere(s);
// 		collect_s += "and ";
// 		s = w_point_s[1];
// 		collect_s += ConvertHere(s);
// 		if (typeof dw[s.length - 1] !== "undefined") {
// 		    collect_s += " " + dw[s.length - 1];
// 		}
// 		return collect_s;

// 	    } else if (w_point.length >= 2) {
// 		return 'Invalid number!';
// 	    } else {

// 		//dito na ung my fifthy
// 		return ConvertHere(s);
// 	    }
// 	} else {
// 	    return ConvertHere(s);
// 	}
//     } else {
// 	return 'Not a number!';
//     }
// }
function AdjustDate(type,dis_dates,added_value){
    
    if(ThisDate(dis_dates)=="Invalid Date"){

         var this_new_date = new Date($.trim(dis_dates));
     }
   
        var dis_date = new Date($.trim(dis_dates));
         var value_add = Number(added_value);
        
         switch(type){

            case 'day':

                if ($.isNumeric(value_add)) {

                     var new_date = dis_date.setDate(parseInt(dis_date.getDate()) + parseInt(value_add));
                      
                     var obj_new_date = ThisDate(new_date).formatDate('Y-m-d H:i:s');

                     return obj_new_date;

                } else {
                    return "";
                }
                 break;
            case 'month':
                 if ($.isNumeric(value_add)) {
                    var new_date = dis_date.setMonth(parseInt(dis_date.getMonth()) + parseInt(value_add));
                    var obj_new_date = ThisDate(new_date).formatDate('Y-m-d H:i:s');
                    return obj_new_date;
                } else {
                    return "";
                }
                break;
             case 'year':
                 if ($.isNumeric(value_add)) {
                    var new_date = dis_date.setFullYear(parseInt(dis_date.getFullYear()) + parseInt(value_add));
                    var obj_new_date = ThisDate(new_date).formatDate('Y-m-d H:i:s');
                    return obj_new_date;
                } else {
                    return "";
                }
                break;
            case 'minute':
                 if ($.isNumeric(value_add)) {
                    var new_date = dis_date.setMinutes(parseInt(dis_date.getMinutes()) + parseInt(value_add));
                    var obj_new_date = ThisDate(new_date).formatDate('Y-m-d H:i:s');
                    return obj_new_date;
                } else {
                    return "";
                }
                 break;
            case 'second':
                if ($.isNumeric(value_add)) {
                    var new_date = dis_date.setSeconds(parseInt(dis_date.getSeconds()) + parseInt(value_add));
                    var obj_new_date = ThisDate(new_date).formatDate('Y-m-d H:i:s');
                    return obj_new_date;
                } else {
                    return "";
                }
                break;
            case 'hour':
                 if ($.isNumeric(value_add)) {
                    var new_date = dis_date.setHours(parseInt(dis_date.getHours()) + parseInt(value_add));
                    var obj_new_date = ThisDate(new_date).formatDate('Y-m-d H:i:s');
                    return obj_new_date;
                } else {
                    return "";
                }
                 break;
            case 'week':
               
                if($.isNumeric(value_add)){
                   var int_date = parseInt(dis_date.getTime());
                   var new_date = int_date + parseInt((value_add)*(24 * 3600 * 1000 * 7));
                   var obj_new_date = ThisDate(new_date).formatDate('Y-m-d H:i:s');
                   return obj_new_date;

                }
                else{
                    return "";
                }
                break;

         }
        
   
}
function DiffDate(type,first_date,second_date){
    var date1 = ThisDate(first_date);
    var date2 = ThisDate(second_date);
	if(date1=="Invalid Date"){
		return "";
		var date1 = first_date;
	}
	if(date2=="Invalid Date"){
		return "";
		var date2 = second_date;
	}
   
        
        
         switch(type){
            case 'hour':
                if ($.type(date1) !== "undefined" && $.type(date2) !== "undefined") {
                   
                    var date2_dt_obj =  ThisDate(date2);
                   
                    var t2 = date2_dt_obj.getTime();
                    var t1 = date1.getTime();
                    return /*Math.abs*/((t1 - t2) / 1000 / 60 / 60);
                } else {
                    return "";
                }
                break;
            case 'day':

                if ($.type(date1) !== "undefined" && $.type(date2) !== "undefined") {
		    
                    var date2_dt_obj = ThisDate(date2);
                    var t2 = date2_dt_obj.getTime();
                    var t1 = date1.getTime();
                    return /*Math.abs*/(parseInt((t1 - t2) / (24 * 3600 * 1000), 10));
                } else {
                    return "";
                }
                break;
            case 'week':
                if ($.type(date1) !== "undefined" && $.type(date2) !== "undefined") {

                    var date2_dt_obj = ThisDate(date2);
                    var t2 = date2_dt_obj.getTime();
                    var t1 = date1.getTime();

                    return /*Math.abs*/(parseInt((t1 - t2) / (24 * 3600 * 1000 * 7)));
                } else {
                    return "";
                }
                 break;
            case 'month':
                 if ($.type(date1) !== "undefined" && $.type(date2) !== "undefined") {
                    var date2_dt_obj = ThisDate(date2);
                    var d1Y = date1.getFullYear();
                    var d2Y = date2_dt_obj.getFullYear();
                    var d1M = date1.getMonth();
                    var d2M = date2_dt_obj.getMonth();
                    return /*Math.abs*/((d1M + 12 * d1Y) - (d2M + 12 * d2Y));
                } else {
                    return "";
                }
                break;
            case 'year':
                if ($.type(date1) !== "undefined" && $.type(date2) !== "undefined") {
                    var date2_dt_obj = ThisDate(date2);
                    return (date1.getTime() - date2.getTime()) / (1000 * 60 * 60 * 24 * 365);
                    // return /*Math.abs*/(date2_dt_obj.getFullYear() - date1.getFullYear());
                } else {
                    return "";
                }
                break;
            case 'minute':
				 if ($.type(date1) !== "undefined" && $.type(date2) !== "undefined") {
                   
                    var date2_dt_obj =  ThisDate(date2);
                   
                    var t2 = date2_dt_obj.getTime();
                    var t1 = date1.getTime();
                    return /*Math.abs*/((t1 - t2)* 60 / 1000 / 60 / 60 );
                } else {
                    return "";
                }
                break;
        }


}

function GetMonth(str_date){
	var given_date = ThisDate(str_date);
	if(given_date == "Invalid Date"){
		return "";
	}else{
		return Number(FormatDate(given_date , 'm'));
	}
}

function FormatDate(dis_dates,format){
    var dis_date = ThisDate(dis_dates);
     if ( dis_date == "Invalid Date") {
        return dis_date;
    } else {
        var timestamp = dis_date;
       
        var that = dis_date,
                jsdate,
                f,
                // Keep this here (works, but for code commented-out
                // below for file size reasons)
                //, tal= [],
                txt_words = ["Sun", "Mon", "Tues", "Wednes", "Thurs", "Fri", "Satur", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                // trailing backslash -> (dropped)
                // a backslash followed by any character (including backslash) -> the character
                // empty string -> empty string
                formatChr = /\\?(.?)/gi,
                formatChrCb = function(t, s) {
                    return f[t] ? f[t]() : s;
                },
                _pad = function(n, c) {
                    n = String(n);
                    while (n.length < c) {
                        n = '0' + n;
                    }
                    return n;
                };
        f = {
            // Day
            d: function() { // Day of month w/leading 0; 01..31
                return _pad(f.j(), 2);
            },
            D: function() { // Shorthand day name; Mon...Sun
                return f.l().slice(0, 3);
            },
            j: function() { // Day of month; 1..31
                return jsdate.getDate();
            },
            l: function() { // Full day name; Monday...Sunday
                return txt_words[f.w()] + 'day';
            },
            N: function() { // ISO-8601 day of week; 1[Mon]..7[Sun]
                return f.w() || 7;
            },
            S: function() { // Ordinal suffix for day of month; st, nd, rd, th
                var j = f.j(),
                        i = j % 10;
                if (i <= 3 && parseInt((j % 100) / 10, 10) == 1) {
                    i = 0;
                }
                return ['st', 'nd', 'rd'][i - 1] || 'th';
            },
            w: function() { // Day of week; 0[Sun]..6[Sat]
                return jsdate.getDay();
            },
            z: function() { // Day of year; 0..365
                var a = new Date(f.Y(), f.n() - 1, f.j()),
                        b = new Date(f.Y(), 0, 1);
                return Math.round((a - b) / 864e5);
            },
            // Week
            W: function() { // ISO-8601 week number
                var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3),
                        b = new Date(a.getFullYear(), 0, 4);
                return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
            },
            // Month
            F: function() { // Full month name; January...December
                return txt_words[6 + f.n()];
            },
            m: function() { // Month w/leading 0; 01...12
                return _pad(f.n(), 2);
            },
            M: function() { // Shorthand month name; Jan...Dec
                return f.F().slice(0, 3);
            },
            n: function() { // Month; 1...12
                return jsdate.getMonth() + 1;
            },
            t: function() { // Days in month; 28...31
                return (new Date(f.Y(), f.n(), 0)).getDate();
            },
            // Year
            L: function() { // Is leap year?; 0 or 1
                var j = f.Y();
                return j % 4 === 0 & j % 100 !== 0 | j % 400 === 0;
            },
            o: function() { // ISO-8601 year
                var n = f.n(),
                        W = f.W(),
                        Y = f.Y();
                return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
            },
            Y: function() { // Full year; e.g. 1980...2010
                return jsdate.getFullYear();
            },
            y: function() { // Last two digits of year; 00...99
                return f.Y().toString().slice(-2);
            },
            // Time
            a: function() { // am or pm
                return jsdate.getHours() > 11 ? "pm" : "am";
            },
            A: function() { // AM or PM
                return f.a().toUpperCase();
            },
            B: function() { // Swatch Internet time; 000..999
                var H = jsdate.getUTCHours() * 36e2,
                        // Hours
                        i = jsdate.getUTCMinutes() * 60,
                        // Minutes
                        s = jsdate.getUTCSeconds(); // Seconds
                return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
            },
            g: function() { // 12-Hours; 1..12
                return f.G() % 12 || 12;
            },
            G: function() { // 24-Hours; 0..23
                return jsdate.getHours();
            },
            h: function() { // 12-Hours w/leading 0; 01..12
                return _pad(f.g(), 2);
            },
            H: function() { // 24-Hours w/leading 0; 00..23
                return _pad(f.G(), 2);
            },
            i: function() { // Minutes w/leading 0; 00..59
                return _pad(jsdate.getMinutes(), 2);
            },
            s: function() { // Seconds w/leading 0; 00..59
                return _pad(jsdate.getSeconds(), 2);
            },
            u: function() { // Microseconds; 000000-999000
                return _pad(jsdate.getMilliseconds() * 1000, 6);
            },
            // Timezone
            e: function() { // Timezone identifier; e.g. Atlantic/Azores, ...
                // The following works, but requires inclusion of the very large
                // timezone_abbreviations_list() function.

                throw 'Not supported (see source code of date() for timezone on how to add support)';
            },
            I: function() { // DST observed?; 0 or 1
                // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
                // If they are not equal, then DST is observed.
                var a = new Date(f.Y(), 0),
                        // Jan 1
                        c = Date.UTC(f.Y(), 0),
                        // Jan 1 UTC
                        b = new Date(f.Y(), 6),
                        // Jul 1
                        d = Date.UTC(f.Y(), 6); // Jul 1 UTC
                return ((a - c) !== (b - d)) ? 1 : 0;
            },
            O: function() { // Difference to GMT in hour format; e.g. +0200
                var tzo = jsdate.getTimezoneOffset(),
                        a = Math.abs(tzo);
                return (tzo > 0 ? "-" : "+") + _pad(Math.floor(a / 60) * 100 + a % 60, 4);
            },
            P: function() { // Difference to GMT w/colon; e.g. +02:00
                var O = f.O();
                return (O.substr(0, 3) + ":" + O.substr(3, 2));
            },
            T: function() { // Timezone abbreviation; e.g. EST, MDT, ...
                // The following works, but requires inclusion of the very
                // large timezone_abbreviations_list() function.

                return 'UTC';
            },
            Z: function() { // Timezone offset in seconds (-43200...50400)
                return -jsdate.getTimezoneOffset() * 60;
            },
            // Full Date/Time
            c: function() { // ISO-8601 date.
                return 'Y-m-d\\TH:i:sP'.replace(formatChr, formatChrCb);
            },
            r: function() { // RFC 2822
                return 'D, d M Y H:i:s O'.replace(formatChr, formatChrCb);
            },
            U: function() { // Seconds since UNIX epoch
                return jsdate / 1000 | 0;
            }
        };
        this.date = function(format, timestamp) {
            that = dis_date;
            jsdate = (timestamp === undefined ? new Date() : // Not provided
                    (timestamp instanceof Date) ? new Date(timestamp) : // JS Date()
                    new Date(timestamp * 1000) // UNIX timestamp (auto-convert to int)
                    );
            return format.replace(formatChr, formatChrCb);
        };
        return this.date(format, timestamp);
    }//end of if


}
function ArrayConcat(){
	var args = arguments;
	var collector = [];

	var temp_val = "";
	for(var key_ii in args){
		temp_val = args[key_ii];
		if($.type(temp_val) == "array"){
			collector = collector.concat(temp_val);
		}else{
			collector.push(temp_val);
		}
	}
	return collector;
}

function SDev(fieldname) {
	var fn_str = fieldname;
	var nums = [];

    if($.type(fn_str) == "array"){
        nums = fn_str;
    }else{
        nums.push(Number($('[name="' + fn_str + '"]').val()))

		$('[rep-original-name="' + fn_str + '"]').each(function() {
			nums.push(Number($(this).val()));
		});
    }
        

	

	var dataSD = {};
	var mean = 0;
	var mean_average = 0;
	var decimal = 1000000;

	dataSD.totalNumbers = nums.length;

	function getMean(nums) {
		for (var ii in nums) {
			mean = mean + parseFloat(nums[ii]);
		}
		mean = mean / nums.length;
		mean_average = Math.round(mean * decimal) / decimal;
		return Math.round(mean * decimal) / decimal;
	}
	dataSD.meanAverage = getMean(nums);

	var variance = 0;
	var varian = 0;

	function getVariance(nums) {
		for (var ii in nums) {
			variance = variance + Math.pow((parseFloat(nums[ii]) - mean), 2);
		}
		varian = variance / (nums.length - 1);
		varian = (isNaN(varian) == false) ? varian : 0;
		return Math.round(varian * decimal) / decimal;
	}
	dataSD.varianceSD = getVariance(nums);

	var sd = 0;

	function getStandardDeviation() {
		sd = Math.sqrt(varian);
		return Math.round(sd * decimal) / decimal;
	}
	dataSD.standardDeviation = getStandardDeviation();
	var pop = 0;

	function getPopulationSD(nums) {
		pop = variance / nums.length;
		pop_sqrt = Math.sqrt(pop);
		return Math.round(pop_sqrt * decimal) / decimal;
	}
	dataSD.populationSD = getPopulationSD(nums);

	var var_pop = 0;

	function getVariancePSD(nums) {
		var_pop = variance / nums.length;
		return Math.round(var_pop * decimal) / decimal;
	}
	dataSD.variancePSD = getVariancePSD(nums);
	return dataSD["standardDeviation"];
}
function Sum(sum_fn) {
	var fieldname = sum_fn;
	var summation = 0;
	if (fieldname.indexOf("@") >= 0) {
		fieldname = fieldname.replace("@", "");
	}
	summation = summation + Number($('[name="' + fieldname + '"]').val().replace(/,/g, ""));

	$('[rep-original-name="' + fieldname + '"]').each(function(eqi) {
		summation = summation + Number($(this).val().replace(/,/g, ""));
	});

	return summation;
}
function GetAVG(fns) {
	if($.type(fns) == "string"){
		var fieldname = "@" + fns;
		var collect_values = [];
		var totality = 0;
		var counted_names = 0;
		console.log('asd', fieldname);
		if (fieldname.indexOf("@") >= 0) {
			fieldname = fieldname.replace("@", "");
			totality = totality + Number($('[name="' + fieldname + '"]').val());
			counted_names++;
			$('[rep-original-name="' + fieldname + '"]').each(function(eqi) {
				totality = totality + Number($(this).val());
				counted_names++;
			});
			if (counted_names == 0) {
				return 0;
			} else {
				return (totality / counted_names);
			}
		} else {

		}
	}else{
		return 0;
	}
	
}
function GetMax(fns) {
	if($.type(fns) == 'string'){
		var fieldname = fns;
		var collect_values = [];
		if (fieldname.indexOf("@") >= 0) {
			fieldname = fieldname.replace("@", "");
		}
		collect_values.push(Number($('[name="' + fieldname + '"]').val()));

		$('[rep-original-name="' + fieldname + '"]').each(function(eqi) {
			collect_values.push(Number($(this).val()));
		});
		if (collect_values.length == 0) {
			return "";
		} else {
			return (collect_values.sort(function(a, b) {
				return b - a
			}))[0];
		}
	}else{
		return "";
	}
}

function GetMin(fns) {
	if($.type(fns) == "string"){
		var fieldname = fns;
		var collect_values = [];
		if (fieldname.indexOf("@") >= 0) {
			fieldname = fieldname.replace("@", "");
		}
		collect_values.push(Number($('[name="' + fieldname + '"]').val()));

		$('[rep-original-name="' + fieldname + '"]').each(function(eqi) {
			collect_values.push(Number($(this).val()));
		});

		if (collect_values.length == 0) {
			return "";
		} else {
			return (collect_values.sort(function(a, b) {
				return a - b
			}))[0];
		}
	}else{
		return "";
	}
}

// function LoopIn(param1, param2, param3){
//     var totality = 0;
//     var temp_value_param2 = null;
//     var DStorage = {};
// 	var paramCallBack = null;
// 	if($.type(param2) ==  "object"){
// 		DStorage = $.extend(DStorage, param2);
// 		if($.type(param3) == "function"){
// 			paramCallBack = param3;
// 		}
// 	}else if($.type(param2) == "function"){
// 		paramCallBack = param2;
// 	}
	
//     if($.type(param1) === "array"){
//         for(var ctr in param1){
//             if($.type(paramCallBack) === "function"){
// 				DStorage['__BREAK'] = false;
// 				DStorage['__CONTINUE'] = false;
// 				DStorage['__INDEX'] = ctr;
// 				DStorage['__VALUE'] = param1[ctr];
//                 DStorage = paramCallBack(DStorage);
//                 if( $.type(DStorage['__BREAK']) === "boolean" && DStorage['__BREAK'] == true )  {
//                     break;
//                 }else if( $.type(DStorage['__CONTINUE']) === "boolean" && DStorage['__CONTINUE'] == true ) {
// 					continue;
//                 }
//             }
//         }
//     }else if($.type(param1) === "object"){ 
//         // var start ;
//         // for(var ctr in param1){
// 			// DStorage['__BREAK'] = false;
// 			// DStorage['__CONTINUE'] = false;
// 			// DStorage['__INDEX'] = ctr;
// 			// DStorage['__VALUE'] = param1[ctr];
// 			// DStorage = paramCallBack(DStorage);
//         // }
//     }else{ //kapag mga fieldname object name
//         if($('[embed-name="'+param1+'"]').length >= 1 ){

//         }
//     }
	
// 	// if($.type(param3) == "function"){
// 		// DStorage = param3(DStorage);
// 	// }
//     if(DStorage != null){
// 		var rets = {
// 			"valueOf":function(){
// 				return "";
// 			}
// 		}
// 		DStorage = $.extend(rets,DStorage);
//         return DStorage;
//     }else{
//         return {
//             "TOTAL":totality
//         }
//     }
    
// }
function LoopIn(){
    param_args = arguments;
	client_variable_collector = {};
	loop_sys_variable_collector = {};
	temporary_cb_result = '';
    
    if( param_args.length == 3 ){//given_arr_val loop_content_val
		var variable_initialization = param_args[0];
		var loopcontent = param_args[1]; 
		var array_data_val = param_args[2];
		if($.type(array_data_val) == "undefined"){
			return client_variable_collector;
		}
		if($.type(loopcontent) == "function"){//if loop content is existing and a function
			var data_value = '';
			for( k_i in variable_initialization){ //[0] is key [1] is value
				data_value = variable_initialization[k_i];
				if(data_value.length == 2){
					client_variable_collector[data_value[0]] = data_value[1];
				}else{
					client_variable_collector[data_value[0]] = "";
				}
			}
			var temp_counter_loop = 1;
			data_value == '';
			loop_sys_variable_collector['_loop_len'] = array_data_val.length;
			for(var key_index in array_data_val ){
				data_value = array_data_val[key_index];
				loop_sys_variable_collector['_index'] = key_index;
				loop_sys_variable_collector['_value'] = data_value;
				loop_sys_variable_collector['_loop_ctr'] = temp_counter_loop;
				temporary_cb_result = loopcontent(loop_sys_variable_collector,$.extend({}, new Object(client_variable_collector) ));
				if(temporary_cb_result['_break']){
					if(temporary_cb_result['_break'] == true){
						client_variable_collector = temporary_cb_result;
						temporary_cb_result['_break'] = false;
						break;
					}
				}
				if($.type(temporary_cb_result) != "boolean" && $.type(temporary_cb_result) != 'undefined'){
					client_variable_collector = temporary_cb_result;
				}else if($.type(temporary_cb_result) == "boolean" && temporary_cb_result == false){
					break;
				}
				temp_counter_loop++;
			}
		}
	}else if( param_args.length == 4 ){// variable declaration, loop content, (start, end)
		var variable_initialization = param_args[0];
		var loopcontent = param_args[1];
		var start_val = param_args[2];
		var end_val = param_args[3];
		if($.type(loopcontent) == 'function'){//if loop content is existing and a function
			var data_value = '';
			for( k_i in variable_initialization){ //[0] is key [1] is value
				data_value = variable_initialization[k_i];
				if(data_value.length == 2){
					client_variable_collector[data_value[0]] = data_value[1];
				}else{
					client_variable_collector[data_value[0]] = "";
				}
			}
			
			
			if(
				( $.type(start_val) == 'date' && $.type(end_val) == 'date' ) ||
				( $.type(start_val) == 'string' && $.type(end_val) == 'string' )
			){//is_date_str($start_val) and is_date_str($end_val)
				start_val = ThisDate(start_val);
				end_val = ThisDate(end_val);
				if(start_val != "Invalid Date" && end_val != "Invalid Date"){
					if(start_val <= end_val){ //ascending loop and equal in date
						var date_difference_end_loop = DiffDate('day', end_val, start_val);
						var temporary_date_computation = "";
						var temporary_date_computation = start_val;
						var temp_counter_loop = 1;
						loop_sys_variable_collector['_loop_len'] = date_difference_end_loop + 1;
						for(ctr = 0 ; ctr <= date_difference_end_loop ; ctr++){
							loop_sys_variable_collector['_index'] = ctr;
							loop_sys_variable_collector['_value'] = temporary_date_computation;
							loop_sys_variable_collector['_loop_ctr'] = temp_counter_loop;
							temporary_cb_result = loopcontent(loop_sys_variable_collector,$.extend({}, new Object(client_variable_collector) ));
							if(temporary_cb_result['_break']){
								if(temporary_cb_result['_break'] == true){
									client_variable_collector = temporary_cb_result;
									temporary_cb_result['_break'] = false;
									break;
								}
							}
							if($.type(temporary_cb_result) != 'boolean' && $.type(temporary_cb_result) != 'undefined'){
								client_variable_collector = temporary_cb_result;
							}else if($.type(temporary_cb_result) == 'boolean' && temporary_cb_result == false){
								break;
							}
							temporary_date_computation = ThisDate(AdjustDate('day', temporary_date_computation, +1));
							temp_counter_loop++;
						}
					}else if(start_val > end_val){//descending loop in date
						var date_difference_end_loop = DiffDate('day', start_val, end_val);
						var temporary_date_computation = '';
						var temporary_date_computation = start_val;
						var temp_counter_loop = date_difference_end_loop + 1;
						loop_sys_variable_collector['_loop_len'] = date_difference_end_loop + 1;
						for(ctr = date_difference_end_loop ; ctr >= 0 ; ctr--){
							loop_sys_variable_collector['_index'] = ctr;
							loop_sys_variable_collector['_value'] = temporary_date_computation;
							loop_sys_variable_collector['_loop_ctr'] = temp_counter_loop;
							temporary_cb_result = loopcontent(loop_sys_variable_collector,$.extend({}, new Object(client_variable_collector) ));
							if(temporary_cb_result['_break']){
								if(temporary_cb_result['_break'] == true){
									client_variable_collector = temporary_cb_result;
									temporary_cb_result['_break'] = false;
									break;
								}
							}
							if($.type(temporary_cb_result) != 'boolean' && $.type(temporary_cb_result) != 'undefined'){
								client_variable_collector = temporary_cb_result;
							}else if($.type(temporary_cb_result) == 'boolean' && temporary_cb_result == false){
								break;
							}
							temporary_date_computation = ThisDate(AdjustDate('day', temporary_date_computation, -1));
							temp_counter_loop--;
						}
					}
				}
			}else if(//if start_val and end_val is a number
				$.isNumeric(start_val) && $.isNumeric(end_val)
			){
				if(start_val <= end_val ){//ascending loop and equal
					var temp_index_ctr = 0;
					var temp_counter_loop = 1;
					loop_sys_variable_collector['_loop_len'] = (end_val - start_val) + 1;
					for(var ctr = start_val ; ctr <= end_val ; ctr++){
						loop_sys_variable_collector['_index'] = temp_index_ctr;
						loop_sys_variable_collector['_value'] = ctr;
						loop_sys_variable_collector['_loop_ctr'] = temp_counter_loop;
						temporary_cb_result = loopcontent(loop_sys_variable_collector, $.extend({}, new Object(client_variable_collector) ));///*, {"length":(Object.keys(client_variable_collector)).length}*/
						if(temporary_cb_result['_break']){
							if(temporary_cb_result['_break'] == true){
								client_variable_collector = temporary_cb_result;
								temporary_cb_result['_break'] = false;
								break;
							}
						}
						if($.type(temporary_cb_result) != 'boolean' && $.type(temporary_cb_result) != 'undefined'){
							client_variable_collector = temporary_cb_result;
						}else if($.type(temporary_cb_result) == 'boolean' && temporary_cb_result == false){
							break;
						}
						temp_index_ctr++;
						temp_counter_loop++;
					}
				}else if(start_val > end_val ){//descending loop
					var temp_index_ctr = 0;
					var temp_counter_loop = ( start_val - end_val ) + 1;
					loop_sys_variable_collector['_loop_len'] = (start_val - end_val) + 1;
					for(var ctr = start_val ; ctr >= end_val ; ctr--){
						loop_sys_variable_collector['_index'] = temp_index_ctr;
						loop_sys_variable_collector['_value'] = ctr;
						loop_sys_variable_collector['_loop_ctr'] = temp_counter_loop;
						temporary_cb_result = loopcontent(loop_sys_variable_collector, $.extend({}, new Object(client_variable_collector) )); ///*, {"length":(Object.keys(client_variable_collector)).length}*/
						if(temporary_cb_result['_break']){
							if(temporary_cb_result['_break'] == true){
								client_variable_collector = temporary_cb_result;
								temporary_cb_result['_break'] = false;
								break;
							}
						}
						if($.type(temporary_cb_result) != 'boolean' && $.type(temporary_cb_result) != 'undefined'){
							client_variable_collector = temporary_cb_result;
						}else if($.type(temporary_cb_result) == 'boolean' && temporary_cb_result == false){
							break;
						}
						temp_index_ctr++;
						temp_counter_loop--;
					}
				}
			}
		}
	}else{ //too many parameters

	}

	return client_variable_collector;
}


//server side formulas

function DataSource(){
    return "";
}

function LookupWhereArray(){
    return '';
}
// function ArrayConcat(){
//     return '';
// }
function SDevDataArray(){
    return '';
}
// function Lookup(){
//     return '';
// }
function RecordCount(){
    return '';
}
// function LookupWhere(){
//     return '';
// }
function Total(){
    return '';
}
function LookupGetMax(){
    return '';
}
function LookupGetMin(){
    return '';
}
function LookupSDev(){
    return '';
}
function LookupAVG(){
    return '';
}
function LookupCountIf(){
    return '';
}
// function DiffDate(){
//     return '';
// }
// function FormatDate(){
//     return '';
// }
// function AdjustDate(){
//     return '';
// }
// function CustomScript(){
//     return '';
// }
function Head(){
    return '';
}
function AssistantHead(){
    return '';
}
function UserInfo(){
    return '';
}
function LookupAuthInfo(){
    return '';
}
function LookupExternalWhere(){
    return '';
}
function LookupGeoLocation(){
	return '';
}
//============================================================================================================================================================================


function FormulaPlan(){
    // private declarations
    this.fn_keys = [
	    '@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
	    '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
	    '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
	    '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
	    '@LookupAVG', '@GetRound', '@LookupCountIf','@Sum', 
	    '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now',
	    '@GetAuth', "@FormatDate","@NumToWord",
	    "@Head","@AssistantHead","@UserInfo","@GetData",
	    "@LookupAuthInfo",
	    "@GetAVG","@GetMin","@SDev","@GetMax",
	    "@ArrayConcat","@SDevDataArray","@LookupWhereArray","@SetProperty","@StrTrim",
	    "@StrFind","@StrReplace","@StrUpper","@StrLower","@CountDayIf","@LoopIn","@CurrentForm",
	    "@SetHyperLink","@StrSplitBy","@ArrayJoinBy","@DataSource", "@GetLocation","@LookupExternalWhere"
	];
	this.fn_key_server = [
	    '@Lookup','@LookupWhere','@LookupWhereArray','@Total','@RecordCount','@UserInfo','@GetAuth', '@LookupExternalWhere'
	];
    this.field_models = {};
    this.valid_field_selector = [
    	"input.getFields[name][type='radio']",
		"input.getFields[name][type='checkbox']",
		"input.getFields[name][type='text']",
		"textarea",
		"select"
	];
}
FormulaPlan.prototype.bindFieldModel = function(name,elem){
	var self = this;
	var param_ele = $(elem);
	var valid_field = this.valid_field_selector.join(",");
	var name_1 = name;
	if(param_ele.is(valid_field)){
		param_ele.on({
			"change.formulaEvent":function(){
				var name_origin = name_1;
				var dis_ele = $(this);
				var data_value_type = "string";
				if(dis_ele.is('select[multiple]')){
					self.field_models[name_origin] = dis_ele.children('option:selected').map(function(a,b){
						return $(b).val();
					}).get().join("|^|");
				}else if(dis_ele.is('input[type="checkbox"]')){
					self.field_models[name_origin] = dis_ele.filter(':checked').map(function(a,b){
						return $(b).val();
					}).get().join("|^|");
				}else{
					self.field_models[name_origin] = dis_ele.val();
				}
				if(dis_ele.is("")){

				}
			}
		});
	}else{
		var name_origin = name_1;
		self.field_models[name_origin] = "";
	}
}
FormulaPlan.prototype.unbindFieldModel = function(name){
	var self = this;
	var param_ele = $(elem);
	var valid_field = this.valid_field_selector.join(",");
	var name_1 = name;
	if(param_ele.is(valid_field)){
		param_ele.off("change.formulaEvent");
	}else{
		var name_origin = name_1;
		delete self.field_models[name_origin];
	}
}
FormulaPlan.prototype.getFieldModel = function(){
	var temp_field_model = {};
	return $.extend(temp_field_model, this.field_models);
}
FormulaPlan.prototype.resetFieldModel = function(){
	var field_model = this.field_models;
	var keys = Object.keys(field_model);
	for(var ii_key in keys){
		delete this.field_models[ keys[ii_key] ];
	}
}
FormulaPlan.prototype.setExecutionType = function(type_name,function_instruction){
	
}
FormulaPlan.prototype.parse = function(function_instruction){
	
}
var Formula2 = null;
$(document).ready(function(){
	Formula2 = new FormulaPlan();

	// var test = new FormulaPlan();

	// var all_setOBJ = $('.loaded_form_content').find('.setOBJ');
    
 //    //binding change events
 //    AsyncLoop(all_setOBJ, function(a,b){
 //    	var setOBJ_ele = $(this);
 //    	var setOBJ_data_obj_id = setOBJ_ele.attr("data-object-id");
 //    	var setOBJ_getFields = setOBJ_ele.find(".getFields_"+setOBJ_data_obj_id);
 //    	var closure_field_models = field_models;
 //    	if(setOBJ_getFields.is(valid_field_selector.join(","))){
    		
 //    	}
 //    },0);
})

//server sides
function FormulaList(){
    return "";
}

function DataSource(){
	return "";
}

function LookupWhereArray(){
	return '';
}
function ArrayConcat(){
	return '';
}
function SDevDataArray(){
	return '';
}
function Lookup(){
	return '';
}
function RecordCount(){
	return '';
}
function LookupWhere(){
	return '';
}
function Total(){
	return '';
}
function LookupGetMax(){
	return '';
}
function LookupGetMin(){
	return '';
}
function LookupSDev(){
	return '';
}
function LookupAVG(){
	return '';
}
function LookupCountIf(){
	return '';
}
function CustomScript(){
	return '';
}
function Head(){
	return '';
}
function AssistantHead(){
	return '';
}
function UserInfo(){
	return '';
}
function LookupAuthInfo(){
	return '';
}
function GetAuth(){
    return '';
}