var device_oscpu = window.navigator.oscpu||window.navigator.platform||window.navigator.cpuClass;
var browser = '';
var browserVersion = 0;
if ((navigator.userAgent.match(/Opera|OPR\//) ? true : false)) {
    browser = 'Opera';
// } else if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
}else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )){
    browser = 'MSIE';
} else if (/Navigator[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    browser = 'Netscape';
} else if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    browser = 'Chrome';
} else if (/Safari[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    browser = 'Safari';
    /Version[\/\s](\d+\.\d+)/.test(navigator.userAgent);
    browserVersion = new Number(RegExp.$1);
} else if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
    browser = 'Firefox';
}

if(browserVersion === 0){
    browserVersion = parseFloat(new Number(RegExp.$1));
}
// alert(browserVersion + "--" + browser)
var browser_name = browser;
var browser_version = browserVersion

var IE_version = 15; // For Internet Explore Allow browser IE 9 - 10.
var moz_version = 23; // For Mozilla FireFox Allow browser 23 Up.
var chrome_version = 27; // For Google Chrome Allow browser 27 Up.
var mac_version = 12; // For Safari Allow browser  Up.
var opera_version = 20;
// alert(browser_version)
if (browser_name == "MSIE" && browser_version < IE_version) {
    
    // window.location.replace("http://windows.microsoft.com/en-PH/internet-explorer/download-ie");
    // window.location.replace("/login")
    window.location.replace("/browser")
} else {
    if (browser_name == "Firefox" && browser_version < moz_version) {
        // window.location.replace("https://www.mozilla.org/en-US/firefox/new/");
        window.location.replace("/browser")
    } else {
        if (browser_name == "Chrome" && browser_version < chrome_version) {
            // window.location.replace("https://www.google.com/intl/en/chrome/browser/?hl=en&brand=CHMA&utm_campaign=en&utm_source=en-ha-sea-ph-bk&utm_medium=ha"); //alert($.browser.version)
            window.location.replace("/browser")
        }else if (browser_name == "Safari" && !(getParametersName("public_forma_page", window.location.href) == "1")  && browser_version < mac_version) {
            window.location.replace("/browser")
            // alert(browser_version)
        }else if (browser_name == "Opera" && browser_version < opera_version) {
            window.location.replace("/browser")
            // alert(browser_version)
        }
    }
}

var path = window.location.pathname;
var user_auth = $("#user_auth").val();
var USERAUTH = getCookie('USERAUTH');
//document.write('<scr' + 'ipt type="text/javascript" src="/js/functions/javascript_file.js" ></scr' + 'ipt>'); // Counter

// function get_browser() {
//     var N = navigator.appName, ua = navigator.userAgent, tem;
//     var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
//     if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null)
//         M[2] = tem[1];
//     M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
//     return M[0];
// }
// function get_browser_version() {
//     var N = navigator.appName, ua = navigator.userAgent, tem;
//     var M = ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
//     if (M && (tem = ua.match(/version\/([\.\d]+)/i)) != null)
//         M[2] = tem[1];
//     M = M ? [M[1], M[2]] : [N, navigator.appVersion, '-?'];
//     return M[1];
// }

// Browser Detection
// var browser_name = get_browser();
// var browser_version = get_browser_version()



// var IE_version = 10; // For Internet Explore Allow browser IE 9 - 10.
// var moz_version = 20; // For Mozilla FireFox Allow browser 21 Up.
// var chrome_version = 27; // For Google Chrome Allow browser 27 Up.

// if(browser_name == "MSIE" || browser_name == "Firefox"){
//     var $_print = console;
//     var console = {
//         log : function(){
//             return;
//         },
//         time : function(){
//             return;
//         },
//         timeEnd : function(){
//             return;
//         },
//         error : function(){
//             return;
//         }
//     }

// }

// if (browser_name == "MSIE" && browser_version < IE_version) {

//     window.location.replace("http://windows.microsoft.com/en-PH/internet-explorer/download-ie");
// } else {
//     if (browser_name == "Firefox" && browser_version < moz_version) {
//         window.location.replace("https://www.mozilla.org/en-US/firefox/new/");
//     } else {
//         if (browser_name == "Chrome" && browser_version < chrome_version) {
//             window.location.replace("https://www.google.com/intl/en/chrome/browser/?hl=en&brand=CHMA&utm_campaign=en&utm_source=en-ha-sea-ph-bk&utm_medium=ha"); //alert($.browser.version)
//         }
//     }
// }



//if (path != "/login" || path != "/index" || path != "/user_view/index") {
//    $(".oLayout").attr("href", null);
//}
//
//if (path == "/" || path == "/login" || path == "/index" || path == "/user_view/index") {
//    $(".oLayout").attr("href", "/css/layout2.css");
//}
//idleTime = 0;
$(document).ready(function() {

    // for Login @ps7
    var login_pathname = window.location;
    if(getParametersName("login_attempt", login_pathname) == "1" && getParametersName("account", login_pathname) == "1"){
        showNotification({
             message: "You provided an invalid username or password. Please sign in again.",
             type: "error",
             autoClose: true,
             duration: 3
         });
    }

    if(getParametersName("login_attempt", login_pathname) == "1" && getParametersName("login_type", login_pathname) == "ad"){
        showNotification({
             message: "Your account is not registered. Please contact your system administrator.",
             type: "error",
             autoClose: true,
             duration: 5
         });
    }

    if(getParametersName("login_attempt", login_pathname) == "1" && getParametersName("account", login_pathname) == "0"){
        showNotification({
            message: "Your account is currently inactive. Please contact your System Administrator to request for an activation.",
            type: "error",
            autoClose: true,
            duration: 3
        });
    }

    

    warningOnLeavePage();//warning before leaving the page(orgchart,formbuilder, and workflow)
    LoadingAnimation();
    $('body').on('click','a[href]:not([href=""]):not([href^="#"])',function(e){
        if(e.ctrlKey == false && e.shiftKey == false && e.altKey == false){
            console.log("abort all ajax when click a href");
            $.xhrpAbortAll();
        }
    });

    $("body").on("keypress","[number-value='true']",function(e){
        var isNumericChar =  $(this).val() + String.fromCharCode(e.which);
        if(!$.isNumeric(isNumericChar)){
            return false;
        }
    });
    
    $("body").on("paste","[number-value='true']",function(e){
        var data = e.originalEvent.clipboardData.getData('Text');
        if(!$.isNumeric(data)){
            return false;
        }
    });
    // Workspace
    var guest_allow_ws = $("#guest_allow_ws").val();

    if (guest_allow_ws == "0") {

    	$('[data-type="imageOnly"]').each(function(){
    	    var replace = "http://eforms.gs3.com.ph";
    	    var src = $(this).find(".getFields[src]").eq(0).attr("src");
    	    var change_src = src.replace(new RegExp(replace, "g"),"");
    	    //console.log(change_src)
    	    //console.log($(this).find(".getFields[src]").length)

    	    $(this).find(".getFields[src]").eq(0).attr("src",change_src);
    	});
    }
    

    
    // TEXT TO SPEECH
    //var input = " FORMA LISTICS is a software platform that makes possible creation of software systems based on electronic forms and process / approval flows (workflows) within minutes.  With automations introduced into the system following best practices and data security, systems can be designed, implemented and deployed within a few days,  this is value for your money!";
    //$("body").append("<audio autoplay><source src=http://tts-api.com/tts.mp3?q=" + escape(input) + " type=audio/mpeg></audio><p>\"" + input + "\"</p>");
    ////$(document).scrollTop($(document).height());
    ////alert(input);
    
    
    // Preview for uploading
    $(".upfile").change(function(){
	readURL(this,".imagePrev");

    });

    //$('.fl-toolbar-left-wrapper ul').find('li').addClass('fl-default-btn');
/*    $('.fl-for-saving-fitler').find('#saveFC').addClass('fl-positive-btn');
    $('.fl-for-saving-fitler').find('#cancelFC').addClass('fl-default-btn');*/
    //$('.fl-createViewreq').addClass('fl-default-btn');
    $('#fl-send-msg-form').find('input[type="button"][value="SEND"]').addClass('fl-positive-btn');
    $('input[type="button"][value="Send"]').addClass('fl-positive-btn');
    $('.fl-input_position').find('input[type="button"][value="Edit"]').addClass('fl-default-btn');
    $('.fl-input_position').find('input[type="button"][value="Save"]').addClass('fl-positive-btn');
    $('input[type="button"][value="Save"]').addClass('fl-positive-btn');
    $('.fl-input_position').find('input[type="button"][value="Cancel"]').addClass('fl-default-btn');
    $('input[type="submit"][value="Save"]').addClass('fl-positive-btn');
    //$('.fl-adding-comment').addClass('fl-default-btn');
    
    
//    var user_path = $("#user_url_view").val();
//    if (path != "/login" || path != "/index" || path != user_path + "index") {
//	$(".oLayout").attr("href", null);
//    }
//    
//    if (path == "/" || path == "/login" || path == "/index" || path == user_path + "index") {
//	$(".oLayout").attr("href", "/css/layout2.css");
//    }
    
    //console.log(user_path + "==" + path)
    
    $("img").error(function() {
        if(path.match(/user_view/g) != null){

        }else{
            $(this).unbind("error").attr("src", "/images/error/broken.png");
        }
    });

    // PostPrivacy.init();
    // UI Right Bar
    // $("#company-announcement-container").perfectScrollbar();


    //Increment the idle time counter every minute.
    var pathname = window.location.pathname;
    //console.log(pathname+"!=/ && "+ pathname+"!=/login");
    // if (pathname!="/" && pathname!="/login") {
    // 	var idleInterval = setInterval("timerIncrement()", 60000); // 1 minute
    //// 	//Zero the idle timer on mouse movement.
    // 	$(this).mousemove(function (e) {
    // 	    //idleTime = 0;
    //	    reset();
    // 	});
    // 	$(this).keypress(function (e) {
    //// 	    //idleTime = 0;
    // 	    reset();
    // 	});
    // }


    //for form list
    $("body").on("click", ".update_status_form", function() {
        var self = this;
        var status = $(this).attr("data-status");
        var id = $(this).attr("data-id")
        updateStatus(status, "form", id,function(){
            $(".dataTable_form").dataTable().fnDestroy();
            GetFormDataTable.defaultData();
        });
    })

    //soft delete form
    $("body").on("click", ".softDelete_form", function() {
        var id = $(this).attr("data-form-id");
        var status = "soft delete";
        updateStatus(status, "softDelete_form", id,function(){
            $(".dataTable_form").dataTable().fnDestroy();
            GetFormDataTable.defaultData();
        });
    })

    // $("body").on("click", ".delete_form", function() {
    //     var id = $(this).attr("data-form-id")
    //     delete_form("form", id, $(this));
    // })
    //for orgchart list
    $("body").on("click", ".update_status_orgchart", function() {
        var status = $(this).attr("data-status");
        var id = $(this).attr("data-id")
        updateStatus(status, "orgchart", id,function(){
            $(".dataTable_orgchart").dataTable().fnDestroy();
            GetOrgchartDataTable.defaultData();
        });
    })

    //soft delete orgchart
    $("body").on("click", ".softDelete_orgchart", function() {
        var id = $(this).attr("data-id");
        var status = "soft delete";
        updateStatus(status, "softDelete_orgchart", id,function(){
            $(".dataTable_orgchart").dataTable().fnDestroy();
            GetOrgchartDataTable.defaultData();
        });
    })

    // $("body").on("click", ".delete_orgchart", function() {
    //     var id = $(this).attr("data-id")
    //     delete_form("orgchart", id, $(this));
    // })
    //for workflow list
    $("body").on("click", ".update_status_workflow", function() {
        var status = $(this).attr("data-status");
        var id = $(this).attr("data-id")
        updateStatus(status, "workflow", id,function(){

            if(pathname=="/user_view/workflow" || pathname=="/user_view/formbuilder"){
                showNotification({
                    message: "Workflow has been updated!",
                    type: "success",
                    autoClose: true,
                    duration: 3
                });
                setTimeout(function(){
                    window.location.reload(true);
                },3000)
            }else{
                $(".dataTable_workflow").dataTable().fnDestroy();
                GetWorkflowDataTable.defaultData();
            }
        });
    })

    //soft delete workflow
    $("body").on("click", ".softDelete_workflow", function() {
        var id = $(this).attr("data-id");
        var status = "soft delete";
        if(status=="2"){
            showNotification({
                message: "Draft status is not allowed to activate.",
                type: "error",
                autoClose: true,
                duration: 3
            });
            return;
        }
        updateStatus(status, "softDelete_workflow", id,function(){
            $(".dataTable_workflow").dataTable().fnDestroy();
            GetWorkflowDataTable.defaultData();
        });
    })

    // $("body").on("click", ".delete_workflow", function() {
    //     var id = $(this).attr("data-id")
    //     delete_form("workflow", id, $(this));
    // })
    //for report list
    $("body").on("click", ".update_status_report", function() {
        var status = $(this).attr("data-status");
        var id = $(this).attr("data-id")
        updateStatus(status, "report", id,function(){
            $(".dataTable_report").dataTable().fnDestroy();
            GetReportDataTable.defaultData();
        });
    })
    
    //end

    
    // Expand Sub Menu on Dashboard
    $("body").on("click", "li > .subClosed", function() {
        $(this).next().slideToggle();
    });

    // // Window detect if browser resize
    // if ($(window).width() < 1260) {
    //     //console.log("Close" + $(window).width());
    //     // Functions for close panel settings
    //     var pos = {
    //         pos_top_action: "-212",
    //         pos_top_prop_action: "0"
    //     }
    //     form_settings(this, ".form_properties", ".form_properties_actions", "form_settings_close", "form_settings_open", pos);

    // } else {
    //     //console.log("Open" + $(window).width());
    //     // Functions for open panel settings
    //     var pos = {
    //         pos_top_action: "0",
    //         pos_top_prop_action: "211"
    //     }
    //     form_settings(this, ".form_properties", ".form_properties_actions", "form_settings_open", "form_settings_close", pos);

    // }

//    $(window).on("resize",function() {
//        if ($(window).width() < 1260) {
//            console.log("Close" + $(window).width());
//	    $(".welcome").hide();
//            // Functions for close panel settings
//            var pos = {
//		pos_top_action : "-212",
//		pos_top_prop_action : "0"
//	    }
//	    form_settings(this,".form_properties",".form_properties_actions","form_settings_close","form_settings_open",pos);
//	    
//        } else {
//            console.log("Open" + $(window).width());
//	    $(".welcome").show();
//            // Functions for open panel settings
//	    var pos = {
//		pos_top_action : "0",
//		pos_top_prop_action : "211"
//	    }
//	    form_settings(this,".form_properties",".form_properties_actions","form_settings_open","form_settings_close",pos);
//            
//        }
//	
//    });	

    // Scrolling
    //$(window).scroll(function(){
    //    if ($(window).scrollTop() == $(document).height() - $(window).height()){
    //        alert(1)
    //    }
    //});

    // Detect if your using mobile devices or not..
    if (jQuery.browser.mobile) {
        console.log('You are using a mobile device!');
    } else {
        console.log('You are not using a mobile device!');
    }
    // hide #back-top first
    jQuery(".back-to-top").hide();

    // fade in #back-top
    jQuery(function() {
        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > 1000) {
                jQuery('.back-to-top').fadeIn();
            } else {
                jQuery('.back-to-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        jQuery('.back-to-top a').click(function() {
            jQuery('body,html,header').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    });

    // get file path
    $("body").on("change", ".upfile", function() {
        var action = $(this).attr("data-action-id");
        var fpath = $(this).val().replace(/\\/g, '/');
        var a = fpath.split("/");
        var aParameter = a.length;
        //var fnameLength = fname.length;
        $("#uploadFilename_" + action).html("Filename: " + a[aParameter - 1]);

    });

    // Slide Show
    $("#slideshow > div:gt(0)").hide();

    setInterval(function() {
        $('#slideshow > div:first')
                .fadeOut(1000)
                .next()
                .fadeIn(1000)
                .end()
                .appendTo('#slideshow');
    }, 5000);

    // Tooltip
    $("a:not(.fl-user-navigation-wrapper #fl-infinite-nav a, .fl-user-navigation-wrapper-ui2 #fl-infinite-nav a, .horizontal-sub-menu a), .userAvatar, .tip").tooltip();

    // Message Menu
    $(".menuDropdown").on({
        "mouseenter": function() {
            var action = $(this).attr("data-action");
            if (action == "task") {
                var elements = "menu_task";
            } else if (action == "msg") {
                var elements = "menu_msg";
            } else if (action == "account") {
                var elements = "account_msg";
            }
            $("." + elements).stop(false, false).animate({
                "height": "toggle"
            }, 500);
        },
        "mouseleave": function() {
            var action = $(this).attr("data-action");
            if (action == "task") {
                var elements = "menu_task";
            } else if (action == "msg") {
                var elements = "menu_msg";
            } else if (action == "account") {
                var elements = "account_msg";
            }
            $("." + elements).stop(false, false).animate({
                "height": "toggle"
            }, 500);
        }
    });

    // Tabs
    $("ul.tabs li").click(function() {
        $("ul.tabs li").removeClass("active");
        $(this).addClass("active");
        $(".tab_content").removeClass('display').hide();
        var activeTab = $(this).attr("rel");
        $("#" + activeTab).addClass('display').show();
    });


    // Scroll show scroll top
    $(window).scroll(function() {
        var wTop = $(window).scrollTop();
        if (wTop > 500) {
            $(".scrollup").fadeIn().show();

        } else {
            $(".scrollup").fadeOut();
        }
    });
    $(".scrollup").click(function() {
        $("html, body").animate({scrollTop: 0}, 600);
        return false;
    });

    // Disable Right Click
    // $(document)[0].oncontextmenu = function() { return false;}

    //       $("body").mousedown(function(e){
    //         if( e.button == 2 ) {
    ////            jAlert( 'Sorry, this functionality is disabled!', "","", "", "", function(){})
// 		alert('Sorry, this functionality is disabled!');
    //            return false;
    //          } else {
    //            return true;
    //           }
    //       });
    //       window.addEventListener("keydown", keyListener, false);

    //       function keyListener(e) {
    //               if(e.keyCode == 123) {
    //                       e.returnValue = false;
    //               }
    //       }

    /*
     * Workspace Functions
     */
    //workspace_functions.preview_form(".preview_form");

    // Choose form to create a workflow
    // $("body").on("click",".getForm",function(){



    // 	//window.location="/workflow";
    // })
    $("body").on("click", ".request-redirect", function() {
        var request_id = $(this).attr("request-id");
        var form_id = $(this).attr("form-id");
        window.location = "/workspace?view_type=request&formID=" + form_id + "&requestID=" + request_id;
    });
    hoverRotate();

    // Save coords of the user
    // if (window.location.pathname === "/index" || window.location.pathname === "/login" || window.location.pathname === "/") {
    //     var c = function(pos) {
    //         var lat = pos.coords.latitude,
    //                 long = pos.coords.longitude,
    //                 acc = pos.coords.accuracy,
    //                 coords = lat + ', ' + long;
    //         $(".userCoords").val(coords);
    //         document.getElementById('google_map').setAttribute('src', 'https://maps.google.co.uk/?q=' + coords + '&z=60&output=embed');
    //     }

    //     navigator.geolocation.getCurrentPosition(c, e, {
    //         enableHighAccuracy: true
    //                 //timeout: 1
    //     })
    // }
    $('body').on('click', '.batchRegisterUpload', function() {
        var ret = '<form action="/ajax/regUser" class="fl-importBtn" id="batchRegistration" method="POST" enctype="multipart/form-data" style="margin: 0px 15px 15px;">'+
                        '<input type="hidden" name="action" value="batch"/> '+
                        '<div class="fields">'+
                            '<div class="label_below2 fl-margin-bottom">Please select file: </div>'+
                            '<div class="input_position">'+
                                '<div id="uniform-fileInput" class="uploader label_below2" style="width: auto; position: static;">'+
                                    '<input type="file" data-action-id="3" value="upload" name="csv" id="file" size="24" style="opacity: 0; width:100%;" class="import_request_file">'+
                                    '<span id="uploadFilename_3" class="filename import_request_txt">No file selected</span>'+
                                    '<span class="action">Choose File</span>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="fields">'+
                            '<div class="label_below2"></div>'+
                            '<div class="input_position" style="color:#ff0000;font-style:italic;margin-top:5px;">Note: File should be in Comma Separated Value(CSV) file format </div>'+
                        '</div>'+
                        '<div class="fields">'+
                            '<div class="label_below2"></div>'+
                            '<div class="input_position" style="margin-top:5px;">'+
                                '<input type="button" class=" fl-positive-btn btn-blueBtn fl-margin-right submit_users" value="OK" />'+
                                '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel" />'+
                            '</div>'+
                        '</div>'+
                    '</form>';
        var newDialog = new jDialog(ret, '', '', '', '', function() {

        });
        newDialog.themeDialog("modal2");
    });
    

   
});




/*
 * Load More Function on the post
 * @postID - last id of the loaded post.
 */

function loadMore(postID, lastModified) {
    var ret = "";
    ret += '<div class="padding_5">';
    ret += '<div class="more more_' + postID + '" data-date="' + lastModified + '" id="more_' + postID + '" data-id="' + postID + '" style="float: left;margin: 10px 0px; width:99%;height: 40px;cursor: pointer;border-radius:5px;">';
    ret += '<div id="moreLbl_' + postID + '" style="text-align: center;padding:10px;font-weight:bold; line-height:20px;">Load More</div>';
    ret += '<div  class="display" id="moreload_' + postID + '">';
    ret += '<center>';
    //ret += '<img src="/images/loader/loading.gif" style="margin-top: 5px;"/>';
   ret += '<div class="spinner">';
  ret += '<div class="bar1"></div>';
  ret += '<div class="bar2"></div>';
  ret += '<div class="bar3"></div>';
  ret +=  '<div class="bar4"></div>';
  ret +=  '<div class="bar5"></div>';
  ret +=  '<div class="bar6"></div>';
  ret += '<div class="bar7"></div>';
  ret += '<div class="bar8"></div>';
  ret += '<div class="bar9"></div>';
 ret +=  '<div class="bar10"></div>';
  ret += '</div>';
    ret += '</center>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    return ret;
}

//deleted function getParametersName(name, pathname) {  ... move to global.js by roni pinili 2:40 PM 10/8/2015

$.urlParam = function(name) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    } else {
        return results[1] || 0;
    }
}

//update status

function delete_form(type, id, self) {
    var originalType = type;
    var message = "Are you sure you want to delete?";
    if (type == "form") {
        message = "Warning all workflows connected to this forms will also be deleted. Are you sure you want to delete?";
    }
   var newConfirm = new jConfirm(message, 'Confirmation Dialog', '', '', '', function(r) {
        if (r == true) {
            // ui.block();
            type = "delete_" + type;
            $.post("/ajax/updateStatus", {type: type, id: id}, function(data) {
                // ui.unblock();
                if(originalType=="orgchart"){
                    $(".dataTable_orgchart").dataTable().fnDestroy();
                    GetOrgchartDataTable.defaultData();
                }else if(originalType=="workflow"){
                    $(".dataTable_workflow").dataTable().fnDestroy();
                    GetWorkflowDataTable.defaultData();
                }else if(originalType=="form"){
                    $(".dataTable_form").dataTable().fnDestroy();
                    GetFormDataTable.defaultData();
                }else if(originalType=="print_report"){
                    $(".dataTable_custom_report").dataTable().fnDestroy();
                    GetCustomReportDataTable.defaultData();
                }
                self.closest(".supTicket").fadeOut(function() {
                    $(this).next().remove();
                    $(this).remove();
                })
            })
        }
    });
   newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});
}
function updateStatus(status, type, id,callback) {
    if(status=="soft delete"){
        var conf = "Are you sure you want to delete?";
    }else{
        var conf = "Are you sure you want to " + getStatus_revert(status).toLowerCase() + "?";
    }
   var newConfirm = new jConfirm(conf, 'Confirmation Dialog', '', '', '', function(r) {
        if (r == true) {
            // ui.block();
            $.post("/ajax/updateStatus", {type: type, id: id, status: status}, function(data) {
                // alert(data)
                // if(type=="form"){

                // }else if(type=="orgchart"){

                // }else if(type=="workflow"){

                // }
                // ui.unblock();

                // window.location.reload();
                // console.log(data);
                callback(1);
            })
        }
    });
    newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});   
}

// Functions for Detecting if user is idle, away or back
/*
function timerIncrement() {

    // getReset(function(data){
    // 	idleTime =  parseInt(data) + 1;

    // 	// Update
    // 	$.ajax({
    // 		type	:	"POST",
    // 		url	:	"/ajax/idle",
    // 		data	:	{action:"update",idleTime:idleTime},
    // 		success	:	function(data){
    // 			//console.log(data);
    // 		}

    // 	});
    // 	//console.log("Time " + idleTime);
    // 	if (idleTime > 19) { // 20 minutes
    // 		window.location.replace("/user/logout");
    // 	    //window.location.replace("/user/logout");
    // 	//    jAlert('Please log in to continue', 'Not Logged In', '', '', '', function(ans){
    // 	//	    if(ans==true){
    // 	//		window.location.replace("/user/logout");
    // 	//	    }
    // 	//    });
    // 	}else{
    // 	    var timeout = $(this).val();
    // 	    setMessage("Timeout changed to: " + timeout);
    // 	    idle.setAwayTimeout(timeout);

    // 	}
    // })


}

function getReset(callback) {

    // $.ajax({
    // 	type	:	"POST",
    // 	url	:	"/ajax/idle",
    // 	data	:	{action:"getReset"},
    // 	success	:	function(data){
    // 		callback(data);
    // 	}

    // });
}
function reset() {
    // $.ajax({
    // 	type	:	"POST",
    // 	url	:	"/ajax/idle",
    // 	data	:	{action:"reset"},
    // 	success	:	function(data){
    // 		//console.log(data);

    // 	}

    // });
}
$(function() {
    function setMessage(msg) {
        //$('#ActivityList').append("<li>" + new Date().toTimeString() + ": " + msg + "</li>");
        //console.log(msg);
        var pathname = window.location.pathname;
        if (pathname != "/" && pathname != "/login") {
            switch (msg) {
                case "User away.":
                    //reset();
                    ////alert(1)
                    //$.ajax({
                    //	type	:	"POST",
                    //	url	:	"/ajax/idle",
                    //	data	:	{action:"userAway"},
                    //	success	:	function(data){
                    //		//console.log(msg);
                    //	}
                    //	
                    //});
                    break;

                case "User back.":
                    //reset();
                    //$.ajax({
                    //	type	:	"POST",
                    //	url	:	"/ajax/idle",
                    //	data	:	{action:"userBack"},
                    //	success	:	function(data){
                    //		//console.log(msg);
                    //	}
                    //	
                    //});
                    break;

                case "User is not looking at the page.":
                    //reset();
                    //$.ajax({
                    //	type	:	"POST",
                    //	url	:	"/ajax/idle",
                    //	data	:	{action:"userAway"},
                    //	success	:	function(data){
                    //		//console.log(msg);
                    //	}
                    //	
                    //});
                    break;

                case "User started looking at the page again.":
                    //reset();
                    //$.ajax({
                    //	type	:	"POST",
                    //	url	:	"/ajax/idle",
                    //	data	:	{action:"userBack"},
                    //	success	:	function(data){
                    //		console.log(msg);
                    //	}
                    //});
                    break;

            }
        }
    }
    var awayCallback = function() {
        setMessage("User away.");
    };
    var awayBackCallback = function() {
        setMessage("User back.");
    };
    var hiddenCallback = function() {
        setMessage("User is not looking at the page.");
    };
    var visibleCallback = function() {
        setMessage("User started looking at the page again.")
    };

    var idle = new Idle({
        onHidden: hiddenCallback,
        onVisible: visibleCallback,
        onAway: awayCallback,
        onAwayBack: awayBackCallback,
        awayTimeout: "5000" //away with default value of the textbox
    });

});
*/
//move to global js ... 10122015 0212PM
function json_length(json) {
    var ctr = 0;
    $.each(json, function(key, value) {
        ctr++;
    });
    return ctr;
}
/* Date Functions
 *
 * Set the date of the post and for the comment
 * dateTimeDate();
 * 
 */
function dateTimeDate() {
    var d = new Date();
    return d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2) + ' ' + d.getHours() + ':' + ('0' + d.getMinutes()).slice(-2) + ':' + ('0' + d.getSeconds()).slice(-2);
}
function dateFunc() {
    var d = new Date();
    return d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
}
function hoverRotate() {
    var animate_over = "";
    var i = 0;
    $(".depth").on({
        "mouseenter": function() {
            var self = this
            animate_over = setInterval(function() {
                $(self).css({
                    "background": "linear-gradient(" + i + "deg,  rgba(249,249,249,1) 1%,rgba(184,225,252,1) 26%,rgba(169,210,243,1) 32%,rgba(144,186,228,1) 41%,rgba(144,188,234,1) 46%,rgba(0,119,239,1) 51%,rgba(70,148,226,1) 52%,rgba(162,218,245,1) 62%,rgba(189,243,253,1) 70%,rgba(252,252,252,1) 100%)"
                            // "background":"radial-gradient(farthest-corner, rgba(249,249,249,1) 1%,rgba(184,225,252,1) 26%,rgba(169,210,243,1) 32%,rgba(144,186,228,1) 41%,rgba(144,188,234,1) 46%,rgba(0,119,239,1) 51%,rgba(70,148,226,1) 52%,rgba(162,218,245,1) 62%,rgba(189,243,253,1) 70%,rgba(252,252,252,1) 100%)",
                            // "transform":"rotate("+i+"deg)"
                });
                if (i == 360) {
                    i = 0;
                }

                i += 1;
            }, 1)
        },
        "mouseleave": function() {
            clearInterval(animate_over);
            $(this).css({
                "background": ""
            })
        },
        "mousedown": function() {
            clearInterval(animate_over);
            $(this).css({
                "background": ""
            })
        }
    })
}

function brokenImage() {
    $("img").error(function() {
        $(this).unbind("error").attr("src", "/images/error/broken.png");
    });
}



var MD5 = function (string) {
 
    function RotateLeft(lValue, iShiftBits) {
        return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
    }
 
    function AddUnsigned(lX,lY) {
        var lX4,lY4,lX8,lY8,lResult;
        lX8 = (lX & 0x80000000);
        lY8 = (lY & 0x80000000);
        lX4 = (lX & 0x40000000);
        lY4 = (lY & 0x40000000);
        lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
        if (lX4 & lY4) {
            return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
        }
        if (lX4 | lY4) {
            if (lResult & 0x40000000) {
                return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
            } else {
                return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
            }
        } else {
            return (lResult ^ lX8 ^ lY8);
        }
     }
 
     function F(x,y,z) { return (x & y) | ((~x) & z); }
     function G(x,y,z) { return (x & z) | (y & (~z)); }
     function H(x,y,z) { return (x ^ y ^ z); }
    function I(x,y,z) { return (y ^ (x | (~z))); }
 
    function FF(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };
 
    function GG(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };
 
    function HH(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };
 
    function II(a,b,c,d,x,s,ac) {
        a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
        return AddUnsigned(RotateLeft(a, s), b);
    };
 
    function ConvertToWordArray(string) {
        var lWordCount;
        var lMessageLength = string.length;
        var lNumberOfWords_temp1=lMessageLength + 8;
        var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
        var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
        var lWordArray=Array(lNumberOfWords-1);
        var lBytePosition = 0;
        var lByteCount = 0;
        while ( lByteCount < lMessageLength ) {
            lWordCount = (lByteCount-(lByteCount % 4))/4;
            lBytePosition = (lByteCount % 4)*8;
            lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
            lByteCount++;
        }
        lWordCount = (lByteCount-(lByteCount % 4))/4;
        lBytePosition = (lByteCount % 4)*8;
        lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
        lWordArray[lNumberOfWords-2] = lMessageLength<<3;
        lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
        return lWordArray;
    };
 
    function WordToHex(lValue) {
        var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
        for (lCount = 0;lCount<=3;lCount++) {
            lByte = (lValue>>>(lCount*8)) & 255;
            WordToHexValue_temp = "0" + lByte.toString(16);
            WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
        }
        return WordToHexValue;
    };
 
    function Utf8Encode(string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";
 
        for (var n = 0; n < string.length; n++) {
 
            var c = string.charCodeAt(n);
 
            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
 
        }
 
        return utftext;
    };
 
    var x=Array();
    var k,AA,BB,CC,DD,a,b,c,d;
    var S11=7, S12=12, S13=17, S14=22;
    var S21=5, S22=9 , S23=14, S24=20;
    var S31=4, S32=11, S33=16, S34=23;
    var S41=6, S42=10, S43=15, S44=21;
 
    string = Utf8Encode(string);
 
    x = ConvertToWordArray(string);
 
    a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;
 
    for (k=0;k<x.length;k+=16) {
        AA=a; BB=b; CC=c; DD=d;
        a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
        d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
        c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
        b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
        a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
        d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
        c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
        b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
        a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
        d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
        c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
        b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
        a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
        d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
        c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
        b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
        a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
        d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
        c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
        b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
        a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
        d=GG(d,a,b,c,x[k+10],S22,0x2441453);
        c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
        b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
        a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
        d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
        c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
        b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
        a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
        d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
        c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
        b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
        a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
        d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
        c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
        b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
        a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
        d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
        c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
        b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
        a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
        d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
        c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
        b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
        a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
        d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
        c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
        b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
        a=II(a,b,c,d,x[k+0], S41,0xF4292244);
        d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
        c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
        b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
        a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
        d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
        c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
        b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
        a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
        d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
        c=II(c,d,a,b,x[k+6], S43,0xA3014314);
        b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
        a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
        d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
        c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
        b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
        a=AddUnsigned(a,AA);
        b=AddUnsigned(b,BB);
        c=AddUnsigned(c,CC);
        d=AddUnsigned(d,DD);
    }
 
    var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);
 
    return temp.toLowerCase();
}

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function popupwindow(url, title, w, h) {
  var left = (screen.width/2)-(w/2);
  var top = (screen.height/2)-(h/2)-50;
  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

function getCookie(cname)
{
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++)
    {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}


//New diolog theme

/*function removeFadeout(){
    $(this).animate({
        opacity:0
    },function(){
        $(this).remove();
    })
}*/
// Image preview functions
function readURL(input,target_loc) {
    if (input.files && input.files[0]) {
	var reader = new FileReader();
	reader.onload = function(e) {
	    //'#previewHolder'
	    $(target_loc).attr('src', e.target.result);
	}

	reader.readAsDataURL(input.files[0]);
    }
}


var panel = {
	load:function(selector_div, option){
	
		var default_settings = {
			"bgColor":"rgba(250,250,250,0.2)"
		}
		if($.type(option) == "object"){
			$.extend(default_settings, option);
		}
		
		var div_ele = $(selector_div);
		
		var ele_prep = $(
			'<div class="container-level-loading-filter" style="position:absolute;width:100%;height:100%; display:table">\
				<div style="display:table-cell; vertical-align:middle;text-align:center;">\
					<div style="display:inline-block">\
						<div class="spinner">\
							   <div class="bar1"></div>\
							   <div class="bar2"></div>\
							   <div class="bar3"></div>\
							   <div class="bar4"></div>\
							   <div class="bar5"></div>\
							   <div class="bar6"></div>\
							  <div class="bar7"></div>\
							  <div class="bar8"></div>\
							  <div class="bar9"></div>\
							  <div class="bar10"></div>\
						 </div>\
					 <div>\
				<div style="display:table">\
			</div>'
		);
		div_ele.prepend(ele_prep);
		
		if(default_settings.bgColor){
			ele_prep.css("background-color",default_settings.bgColor);
		}
		
		if(default_settings.callBack){
			if($.type(default_settings.callBack) == "function"){
				default_settings.callBack.call(ele_prep,div_ele);
			}else if($.type(default_settings.callBack) == "array"){
				for(var ctr_i in default_settings.callBack){
					if($.type(default_settings.callBack[ctr_i]) == "function"){
						default_settings.callBack[ctr_i].call(ele_prep,div_ele);
					}
				}
			}
		}
		
		return ele_prep;
	}
}



function dataTable_widget(obj, listOfappContainer){

    var dataTableWid = $(obj).closest(listOfappContainer).find(".dataTable_widget");
    if($(obj).closest(listOfappContainer).find(".dataTables_info").html()==""){
        return false;
    }
    dataTableWid.html("");
    $(obj).closest(listOfappContainer).find(".dataTables_info").appendTo(dataTableWid);
    $(obj).closest(listOfappContainer).find(".dataTables_paginate").appendTo(dataTableWid);
    $(obj).closest(listOfappContainer).find(".dataTables_paginate").show();
}


function dataTable_widget_hide(obj, listOfappContainer){ 
    $(obj).closest(listOfappContainer).find(".dataTables_paginate").hide();
}

function setDatatableTooltip(container){
    
    if(typeof container == "object"){
        container.find(".tooltip").remove();
        $(".tip").tooltip({
            "container" : container
        });
    }else{
        $(container+" .tooltip").remove();
        $(".tip").tooltip({
            "container" : container
        });
    }
}

{//util functions
    function AsyncLoop2(multiple_value, callback_body_loop, callback_finish){
        if($.type(multiple_value) == "array"){
            var array_total = multiple_value.length - 1;
            var temp_ctr = 0;
            var loop_suppression = undefined;
            var looper = function(){//recursion
                setTimeout(function(){//async
                    if( temp_ctr <= array_total ){
                        loop_suppression = callback_body_loop.call( multiple_value[temp_ctr], temp_ctr, multiple_value[temp_ctr] );
                        if(loop_suppression || $.type(loop_suppression) == "undefined"){
                            looper();
                        }else{
                            callback_finish();
                        }
                        temp_ctr++;
                    }
                },0);
            };
            looper();
        }else if($.type(multiple_value) == "object"){
            var object_keys = Object.keys(multiple_value);
            var object_total = object_keys.length - 1;


            var temp_ctr = 0;
            var loop_suppression = undefined;
            var looper = function(){//recursion
                setTimeout(function(){//async
                    if( temp_ctr <= object_total ){
                        loop_suppression = callback_body_loop.call( multiple_value[object_keys[temp_ctr]], object_keys[temp_ctr], multiple_value[object_keys[temp_ctr]] );
                        if(loop_suppression || $.type(loop_suppression) == "undefined"){
                            looper();
                        }else{
                            callback_finish();
                        }
                        temp_ctr++;
                    }
                },0);
            };
            looper();
        }else if($.type(multiple_value) == "string"){
            var string_total = multiple_value.length - 1;
            var temp_ctr = 0;
            var loop_suppression = undefined;
            var looper = function(){//recursion
                setTimeout(function(){//async
                    if( temp_ctr <= string_total ){
                        loop_suppression = callback_body_loop.call( multiple_value[temp_ctr], temp_ctr, multiple_value[temp_ctr] );
                        if(loop_suppression || $.type(loop_suppression) == "undefined"){
                            looper();
                        }else{
                            callback_finish();
                        }
                        temp_ctr++;
                    }
                },0);
            };
            looper();
        }
    }
    function AsyncLoop(param1, callBack, callBack2) { // (index, value) //if object (key, value, index)
        var data_array_obj = param1;
        if ($.type(data_array_obj) == "array") {
            var array_len = data_array_obj.length;
            var counter = 0;
            var break_value = "default_data";
            var loop_async = setInterval(function() {
                if ($.type(callBack) == "function") {
                    break_value = callBack.call(data_array_obj[counter], counter, data_array_obj[counter]);
                }
                counter++;
                if (array_len <= counter || break_value == false) {
                    if ($.type(callBack2) == "function") {
                        callBack2.call(data_array_obj);
                    }
                    clearInterval(loop_async);
                }
            }, 0);
        } else if ($.type(data_array_obj) == "object") {
            var data_array_obj_temp = data_array_obj;
            if ($.type(jQuery) == "function") {
                if (data_array_obj_temp instanceof jQuery) {
                    var data_array_obj_temp = data_array_obj_temp.get().map(function(val_ele, ii) {
                        return $(val_ele);
                    });
                }
            }
            var object_keys = Object.keys(data_array_obj_temp);
            var array_len = object_keys.length;
            var counter = 0;
            var break_value = "default_data";
            var loop_async = setInterval(function() {
                if ($.type(callBack) == "function") {
                    break_value = callBack.call(data_array_obj_temp[ object_keys[counter] ], object_keys[counter], data_array_obj_temp[ object_keys[counter] ], counter);
                }
                counter++;
                if (array_len <= counter || break_value == false) {
                    if ($.type(callBack2) == "function") {
                        callBack2.call(data_array_obj_temp);
                    }
                    clearInterval(loop_async);
                }
            }, 0);
        }
    }
    function SetMyTimer(callback, time) {
        this.setTimeout(callback, time);
    }

    SetMyTimer.prototype.setTimeout = function(callback, time) {
        var self = this;
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.finished = false;
        this.callback = callback;
        this.time = time;
        this.timer = setTimeout(function() {
            self.finished = true;
            callback();
        }, time);
        this.start = Date.now();
    }

    SetMyTimer.prototype.add = function(time) {
        if (!this.finished) {
            // add time to time left
            time = this.time - (Date.now() - this.start) + time;
            this.setTimeout(this.callback, time);
        }
    }

    SetMyTimer.prototype.resetTimeout = function(time) {
        if (!this.finished) {
            this.setTimeout(this.callback, time);
        }
    }
    var console_time = {
        enable_console_time: true,
        collect_time: {},
        "start": function(name) {
            var self = this;
            if (self['enable_console_time']) {
                console.time(name);
                self['collect_time']['' + name + ''] = {};
                self['collect_time']['' + name + '']['start_stamp'] = window.performance.now();
            }
        },
        "end": function(name) {
            var self = this;
            if (self['enable_console_time']) {
                console.timeEnd(name);
                self['collect_time']['' + name + '']['end_stamp'] = window.performance.now();
                self['collect_time']['' + name + '']['result_stamp'] = self['collect_time']['' + name + '']['end_stamp'] - self['collect_time']['' + name + '']['start_stamp'];
            }
        },
        "getLogTimeDetails": function(option) {
            var self_dis = this;
            var settings = {
                "sort": "desc",
                "sortKey": "result_stamp"
            }
            $.extend(settings, option);
            var result_process = null;
            result_process = Object.keys(self_dis['collect_time']) //get keys

            if (settings['containsKey']) {
                var str_key_contains = settings['containsKey'];
                result_process = result_process.filter(function(val, ii) {
                    return val.indexOf(str_key_contains) >= 0;
                }) //get the 2nd loop only
            } else if (settings['containsExactKey']) {
                var str_key_contains = settings['containsExactKey'];
                result_process = result_process.filter(function(val, ii) {
                    var temp_re = new RegExp("(^|[^a-zA-Z0-9_])" + str_key_contains + "(?![a-zA-Z0-9_])", "g");
                    var temp_match_res = val.match(temp_re);
                    temp_match_res = (temp_match_res == null) ? [] : temp_match_res;
                    temp_match_res = temp_match_res //filtering mapping
                            /*.map(function(a,b){return a.replace(/[^a-zA-Z0-9_]/g, "");})*/
                            .filter(Boolean);

                    return temp_match_res.length >= 1;
                }); //get the specified key only
            }

            result_process = result_process.map(function(key_i, arr_i) {
                return {"name": key_i, "timeres": self_dis['collect_time'][key_i]};
            }) //parse as readable info

            if (settings['sort'] && settings['sortKey']) { // asc desc
                if (settings['sort'] == "desc") {
                    result_process = result_process.sort(function(a, b) {
                        return b['timeres'][settings['sortKey']] - a['timeres'][settings['sortKey']];
                    }) //sort info based on what is the long time
                } else if (settings['sort'] == "asc") {
                    result_process = result_process.sort(function(a, b) {
                        return a['timeres'][settings['sortKey']] - b['timeres'][settings['sortKey']];
                    }) //sort info based on what is the long time
                }
            }
            if (settings['displayDataOnly']) {//true false
                if (settings['getTimeResult']) { // result key start_stamp, end_stamp, result_stamp
                    var str_get_key = settings['getTimeResult'];
                    result_process = result_process.map(function(val_i, arr_i) {
                        return val_i['timeres'][str_get_key];
                    }); //get all result time only
                } else {
                    result_process = result_process.map(function(val_i, arr_i) {
                        return val_i['timeres'];
                    });
                }
            } else {
                if (settings['getTimeResult']) {
                    var str_get_key = settings['getTimeResult'];
                    result_process = result_process.map(function(val_i, arr_i) {
                        return {"name": val_i['name'], "result": val_i['timeres'][str_get_key]};
                    }); //get all result time only
                } else {
                    result_process = result_process.map(function(val_i, arr_i) {
                        return {"name": val_i['name'], "result": val_i['timeres']};
                    });
                }
            }

            return result_process;
        }
    }
    //usage
    // console_time.getLogTimeDetails({
    //  "containsKey":"CHECKPOINT2",
    //  "getTimeResult":"result_stamp",
    //  "displayDataOnly":true
    // });
    // console_time.getLogTimeDetails({
    //  "containsExactKey":"test",
    //  "getTimeResult":"result_stamp",
    //  "displayDataOnly":false
    // });
}

function capitaliseFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function capitalize(text) {
    var i, words, w, result = '';

    words = text.split(' ');

    for (i = 0; i < words.length; i += 1) {
        w = words[i];
        result += w.substr(0,1).toUpperCase() + w.substr(1);
        if (i < words.length - 1) {
            result += ' ';    // Add the spaces back in after splitting
        }
    }

    return result;
}

function RenewObject(object_var_param){
    var object_var = object_var_param;
    if( $.type(object_var) == "array" ){
        var collector = [];
        for(var ctr_ii in object_var){
            if( $.isPlainObject( object_var[ctr_ii] ) || $.type(object_var[ctr_ii]) == 'array' ){
                collector.push(RenewObject( object_var[ctr_ii] ));
            }else{
                collector.push( object_var[ctr_ii] );
            }
        }
    }else if( $.type(object_var) == "object" ){
        var collector = {};
        var o_k = Object.keys(object_var);
        for(var ctr_ii in o_k){
            if( $.isPlainObject( object_var[o_k[ctr_ii]] ) || $.type(object_var[o_k[ctr_ii]]) == 'array'){
                collector[o_k[ctr_ii]] = RenewObject(object_var[o_k[ctr_ii]]);
            }else{
                collector[o_k[ctr_ii]] = object_var[o_k[ctr_ii]];
            }
        }
    }
    return collector;
}
function DeepMerge(){
    var args = arguments;
    var first_param_host = args[0];
    var param_to_merge = args.filter(function(a,b){
        if ( (args.length-1) ==  b || b == 0){
            return false;
        }
        return true;
    });
    console.log("first_param_host",first_param_host,"param_to_merge",param_to_merge);
}
function DataTypeOf(var_data){
    var date_constructor = (new Date()).constructor;
    var number_constructor = (0).constructor;
    var array_constructor = ([]).constructor;
    var object_constructor = ({}).constructor;
    var string_constructor = ("").constructor;
    var null_var = (null);
    var undefined_var = (undefined);
    if(null_var === var_data){
        return 'null';
    }else if(undefined_var === var_data){
        return 'undefined';
    }else if(string_constructor === var_data.constructor){
        return 'string';
    }else if(object_constructor === var_data.constructor){
        return 'object';
    }else if(array_constructor === var_data.constructor){
        return 'array';
    }else if(number_constructor === var_data.constructor){
        return 'number';
    }else if(date_constructor === var_data.constructor){
        return 'date';
    }else{
        console.log("unknown data type of ",var_data);
        return 'unknown';
    }
}

function LoadingAnimation(){
    
    var loading = '<div class="bar1"></div> '+
                       '<div class="bar2"></div> '+
                       '<div class="bar3"></div> '+
                       '<div class="bar4"></div> '+
                       '<div class="bar5"></div> '+
                       '<div class="bar6"></div> '+
                      '<div class="bar7"></div> '+
                      '<div class="bar8"></div> '+
                      '<div class="bar9"></div> '+
                      '<div class="bar10"></div> ';
    //return loading;
    //console.log(loading);
}

function warningOnLeavePage(){
    var path = window.location.pathname;
    if(path=="/user_view/formbuilder"){
        var category_id = getURLParameter("category_id");
        if(category_id!=null){
            window.onbeforeunload = function (e) {
                return 'You will lose any unsaved edits';
            };
        }
    }else if(path=="/user_view/workflow"){
        var form_id = getURLParameter("form_id");
        if(form_id!=null){
            window.onbeforeunload = function (e) {
                return 'You will lose any unsaved edits';
            };
        }
    }else if(path=="/user_view/organizational_chart"){
        window.onbeforeunload = function (e) {
            return 'You will lose any unsaved edits';
        };
    }
}


// Validation for the required fields
function requiredFields(attribute){
    // @data-req-action
    // Validate Registration Fields
    var isValid = true;
    $(attribute).each(function() {
        if ($.trim($(this).val()) == '') {
            isValid = false;
            $(this).addClass("frequired-red");
            $(this).tooltip('show');
        }
        else{
            $(this).removeClass("frequired-red");
            $(this).tooltip('destroy');
        }
    });
    if (isValid == false)
        //e.preventDefault();
        return true;
}

