

angular.module("WidgetFormaGanttChart",[
	'GanttChart',
	'angularMoment',
	'gantt',
    'gantt.sortable',
    'gantt.movable',
    'gantt.drawtask',
    'gantt.tooltips',
    'gantt.bounds',
    'gantt.progress',
    'gantt.table',
    'gantt.tree',
    'gantt.groups',
    'gantt.dependencies',
    'gantt.overlap',
    'gantt.resizeSensor',
    'perfect_scrollbar'])
angular.module("GanttChart",[])
.constant("dateFieldsConstant",["estimated_start","estimated_end","actual_start","actual_end"])
.constant("numberFieldsConstant",["progress"])
.constant("parentFieldsConstant","parent_job_task")
