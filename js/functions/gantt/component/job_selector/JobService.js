

function JobService ($http){

	this.getAllJobs = function (){
		return $http.get('/project_management/get_all_jobs').then(
		function(response){
			return response.data
		},
		function(error){
			 return [];
		})
		
	}
}



angular.module("GanttChart")
.service("JobService",['$http',JobService])