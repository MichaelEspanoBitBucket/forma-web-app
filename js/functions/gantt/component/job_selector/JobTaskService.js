
function JobTaskService ($http,dateFieldsConstant,numberFieldsConstant,FormattingService){

	
	this.getAllJobTasksByJobId = function(jobId){

		var params = {
			field : "JobNo",
			value : jobId
		}
		

		return $http.get("/project_management/get_all_taskline_by",{params:params}).then(function(response){

			var jobs = []
			var job;
			console.log(response.data)

			for (var i in response.data)
			{
				job = response.data[i]

				for(var k in dateFieldsConstant)
				{
					job[dateFieldsConstant[k]] = FormattingService.stringToDate(job[dateFieldsConstant[k]])
				}

				for(var l in numberFieldsConstant)
				{
					job[numberFieldsConstant[l]] = FormattingService.stringToNumber(job[numberFieldsConstant[l]])
				}

				jobs.push(job)
			}
				console.log(response.data)
			 return response.data
		
		},function(error){
			return []
		})
	}



}

angular.module("GanttChart")
.service("JobTaskService",['$http',"dateFieldsConstant","numberFieldsConstant","FormattingService",JobTaskService])
