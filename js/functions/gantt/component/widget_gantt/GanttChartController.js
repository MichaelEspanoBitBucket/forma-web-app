
function GanttChartDirective(){
	return {
		restrict : 'EA',
		scope : {
			removeWidget : '&',
			parent : "="
		},
		controller : 'GanttChartController',
		controllerAs : 'ganttChartController',
		bindToController : true,
		templateUrl : '../../../js/functions/gantt/component/widget_gantt/widget-gantt-chart-template.html'

	}
}


function GanttChartController (GanttChartDataService,JobTaskService,JobService,GanttChartPreferencesService){


	var vm = this; 
	vm.content = '{{task.model.name}}</br>' +
					'<small>' +
					'{{task.isMilestone() === true && getFromLabel() || getFromLabel() + \' - \' + getToLabel()}}' +
					'</small></br>' +
					'<small>Status : {{task.model.data.status}}</small></br>' +
					'<small>Progress : {{task.model.data.progress}}%</small></br>' +
					'<small>Actual Start : {{task.model.data.actual_start | amDateFormat:"dddd, MMMM Do YYYY, h:mm:ss a"}}</small></br>' +
					'<small>Actual End : {{task.model.data.actual_end | amDateFormat:"dddd, MMMM Do YYYY, h:mm:ss a"}}</small>' 
	vm.isEditable = false;
	vm.isConfigurable = false;
	vm.ganttPreferences = [];
	vm.isNew = true;

	vm.viewScale = "day"
	vm.currentDateLine = "column"
	vm.autoWidth = false
	vm.autoHeight = false
	vm.resizableLabels = false
	vm.showLabels = true


	vm.toggleMode = function(){
		vm.isEditable = !vm.isEditable
	}

	vm.getAllJobs = function(){
		JobService.getAllJobs().then(function(jobs){
			vm.jobs = jobs
		})
	}

	vm.selectJob = function(job){
		vm.job = job
		vm.jobId = job.job_no
		vm.hideJobSelector()
		vm.setGanttDataSource(job.job_no)
	}

	vm.showJobSelector = function(){
		vm.isEditable = true
		vm.getAllJobs()
		vm.hideConfigurationBox()
	}

	vm.hideJobSelector = function(){
		vm.isEditable = false
	}

	vm.showConfigurationBox = function(){
		vm.isConfigurable = true
		vm.hideJobSelector()
	}

	vm.hideConfigurationBox = function(){
		vm.isConfigurable = false
	}

	vm.setGanttDataSource = function (jobId){
		JobTaskService.getAllJobTasksByJobId(jobId).then(function(jobTasks){
			GanttChartDataService.getDataSourceFromTasks(jobTasks).then(function(dataSource){
				vm.dataSource =  dataSource
				console.log(dataSource)

			})
		})
	}

	vm.saveGanttChartPreference = function(){
		
			var found = false;
			for (var i in vm.ganttPreferences){
				if (vm.ganttPreferences[i].id == vm.parent.id){
					vm.ganttPreferences[i].config = {
						viewScale : vm.viewScale,
						currentDateLine : vm.currentDateLine,
						autoWidth : vm.autoWidth,
						autoHeight : vm.autoHeight,
						resizableLabels : vm.resizableLabels,
						showLabels : vm.showLabels,
						jobId : vm.jobId,
						jobName : vm.job.description
					}
					found = true
					break;
				}
			}

			if (!found){
				vm.ganttPreferences.push({
					id : vm.parent.id,
					config : {
						viewScale : vm.viewScale,
						currentDateLine : vm.currentDateLine,
						autoWidth : vm.autoWidth,
						autoHeight : vm.autoHeight,
						resizableLabels : vm.resizableLabels,
						showLabels : vm.showLabels,
						jobId : vm.jobId,
						jobName : vm.job.description
					}
				})
			}
			
			GanttChartPreferencesService.saveGanttChartPreferences(vm.ganttPreferences)
		
	}



	vm.initializeGantt = function (){

		
		
		GanttChartPreferencesService.getAllGanttChartPreferences().then(function(data){
			for (var i in data){
				vm.ganttPreferences.push(data[i])
			}
		})

		
		GanttChartPreferencesService.getGanttChartPreferenceByWidgetId(vm.parent.id).then(function(preferences){
			if (preferences){
				vm.config = preferences.config
			}
			
			if (vm.config){
				vm.viewScale = vm.config.viewScale
				vm.currentDateLine = vm.config.currentDateLine
				vm.autoWidth = (vm.config.autoWidth == "true")
				vm.autoHeight = (vm.config.autoHeight == "true")
				vm.resizableLabels = (vm.config.resizableLabels == "true")
				vm.showLabels = (vm.config.showLabels == "true")
				vm.jobId = vm.config.jobId
				vm.job = {
					job_no : vm.config.jobId,
					description : vm.config.jobName
				}
				vm.isNew = false 
				
			}
			else{
				vm.viewScale = "day"
				vm.currentDateLine = "column"
				vm.autoWidth = false
				vm.autoHeight = false
				vm.resizableLabels = false
				vm.showLabels = true
				
			}
			if (vm.jobId){
				vm.setGanttDataSource(vm.jobId)
			}

			vm.currentDate = new Date()

		})
	}

	vm.initializeGantt()
	

}

angular.module("GanttChart")
.controller("GanttChartController",["GanttChartDataService","JobTaskService","JobService","GanttChartPreferencesService",GanttChartController])
.directive("widgetGanttChart", GanttChartDirective)


