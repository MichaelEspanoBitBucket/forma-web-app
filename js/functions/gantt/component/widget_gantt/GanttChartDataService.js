function GanttChartDataService(parentFieldsConstant,$q,$timeout){


	/*
	 *returns the remove tasks
	 *
	 */

	var self = this;

	this.removeTasks = function(jobTasks,parentTaskId){

		var tasks = []
		var parentTaskIdComparison = ""

		if (parentTaskId)
		{
			parentTaskIdComparison = parentTaskId
		}

		for(var i in jobTasks)
		{
			if (jobTasks[i][parentFieldsConstant] == parentTaskIdComparison){
				tasks.push(jobTasks[i])
				jobTasks.shift()
			}
		}

		return tasks
	}


	this.isParentGanttRow = function(jobTasks,parentTaskId){

		for (var i in jobTasks){
			if (jobTasks[i][parentFieldsConstant] == parentTaskId)
			{
				return true
			}
		}

		return false;
	}

	/*
	* returns a gantt row
	*/

	this.createGanttRow = function(jobTask,jobTasks,isWithLabel){

		var ganttRow = {
			name : jobTask["description"],
			id : jobTask["job_task_no"]
			
		};

		if (jobTask["parent_job_task"] != ''){
			ganttRow.parent = jobTask["parent_job_task"]
		}	

		if(isWithLabel){
			ganttRow.content = jobTask["description"]
		}

		if(!self.isParentGanttRow(jobTasks,jobTask["job_task_no"])){
			ganttRow.tasks = []
			ganttRow.tasks.push(self.createGanttTask(jobTask,true))
		}
		return ganttRow;
		
	}

	/*
	* returns a gantt task
	*/

	this.createGanttTask = function(jobTask,isWithLabel){

		var ganttTask =  {
			name : jobTask["description"],
			from : jobTask["estimated_start"],
			to : jobTask["estimated_end"],
			id : jobTask["job_task_no"],
			progress : jobTask["progress"],
			color : "#5bc0de",
			data : {}
		};

		

		if (jobTask['job_task_line_dependency'] && jobTask['job_task_line_dependency'].length > 0 ){

			ganttTask.dependencies = []

			console.log(jobTask['job_task_line_dependency'])
			console.log(jobTask["job_task_no"])

			if (typeof jobTask['job_task_line_dependency'] == "string"){
				ganttTask.dependencies.push({
					from : jobTask['job_task_line_dependency']
				}) 
			}
			else{
				for (var i in jobTask['job_task_line_dependency'])
				{
					ganttTask.dependencies.push({
						from : jobTask['job_task_line_dependency'][i]
					}) 
				}
			}

			
	
		}

		if (jobTask['job_task_status']){
			ganttTask.data.status = jobTask['job_task_status']
		}
		if (jobTask['actual_start']){
			ganttTask.data.actual_start = jobTask['actual_start']
		}
		if (jobTask['actual_end']){
			ganttTask.data.actual_end = jobTask['actual_end']
		}
		if (jobTask['progress'] != undefined){
			ganttTask.data.progress = jobTask['progress']
		}

		if(isWithLabel){
			ganttTask.content = jobTask["description"]
		}

		if (jobTask["color"])
		{
			ganttTask.color = jobTask["color"]
		}

		return ganttTask;

		
	}
		
	/*
	 *returns an object formatted with the data source of Gantt
	 *
	 */
	this.getDataSourceFromTasks = function(jobTasks){

		var jobTasksCopy = angular.copy(jobTasks)
		var dataSource = []
		var deferred = $q.defer()

		$timeout(function(){
			for(var i in jobTasksCopy){

				dataSource.push(self.createGanttRow(jobTasks[i],jobTasks,true))
			}
			deferred.resolve(dataSource)
		 }, 0);
		return deferred.promise
	}
}

angular.module("GanttChart")
.service("GanttChartDataService",['parentFieldsConstant','$q','$timeout',GanttChartDataService])