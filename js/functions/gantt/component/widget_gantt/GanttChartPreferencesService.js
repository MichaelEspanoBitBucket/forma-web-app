function GanttChartPreferencesService (UserPreferencesService,$http,$q,FormattingService){

	this.getAllGanttChartPreferences = function(){

		return UserPreferencesService.getPreferences.then(function(data){
			var configs = []
			for (var i in data.GanttConfig)
			{
				configs.push(data.GanttConfig[i])
			}
			return configs
		})
	}


	this.getGanttChartPreferenceByWidgetId = function(widgetId){
		
		return UserPreferencesService.getPreferences.then(function(data){
			for (var i in data.GanttConfig)
			{
				if (data.GanttConfig[i].id == widgetId)
				{
					return data.GanttConfig[i]
				}
				
			}
			return $q.reject()
		})
	}

	this.saveGanttChartPreferences = function(preferences){
		
		var ganttConfig = {GanttConfig : preferences}
		return $http.post('/modules/dashboard_data/save-dashboard-user-gantt-config.php',
			FormattingService.serialize(ganttConfig),
			{
				headers: {
	   				'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"
				}
				
			}
		).then(function(data){


		},function(error){
			return $q.reject(error)
		})

	}
}

angular.module("GanttChart")
.service("GanttChartPreferencesService",['UserPreferencesService','$http','$q','FormattingService',GanttChartPreferencesService])