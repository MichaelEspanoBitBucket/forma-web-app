function FormattingService(){

	var self = this 

	this.stringToDate = function(dateString){
		
		return new Date(dateString)

	}

	this.stringToNumber = function(numberString){
		return parseInt(numberString)
	}


	this.serialize = function (obj, prefix) {
	    var str = [];
	    for (var p in obj) {
	      if (obj.hasOwnProperty(p)) {
	        var k = prefix ? prefix + "[" + p + "]" : p,
	              v = obj[p];
	          str.push(typeof v == "object" ? self.serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
	      }
	    }
	    return str.join("&");
	}
	
}




angular.module("GanttChart")
.service("FormattingService",[FormattingService])