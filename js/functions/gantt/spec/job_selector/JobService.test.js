
describe("Job Service", function(){

	var jobService, httpBackend, requestHandler,jobs;

	beforeEach(function(){

		module("GanttChart")
	})

	beforeEach(inject(function(JobService,$httpBackend){
		httpBackend = $httpBackend;
		jobService = JobService

		requestHandler = httpBackend.when('GET','/project_management/get_all_jobs').respond(
			200,
			[
				{
					job_no : "JBTL0001" ,
					description : "Mock Project"
				},
				{
					job_no : "JBTL0002" ,
					description : "Mock Project"
				}
			]
		)

	}))

	 afterEach(function() {
	     httpBackend.verifyNoOutstandingExpectation();
	     httpBackend.verifyNoOutstandingRequest();
	     
   });

	 describe("Getting Job Listings",function(){
	 	
	 	it("should get all job listings",function(){
		
			jobService.getAllJobs().then(function(result){
				jobs = result
			})

			httpBackend.flush();
			expect(jobs).toBeDefined()
			expect(jobs.length).toBe(2)
		})

	 	it ("should return empty job listings on error status codes",function(){

	 		requestHandler.respond(400,"")
	 		jobService.getAllJobs().then(function(result){
				jobs = result
			})
	 		httpBackend.flush();
			expect(jobs.length).toBe(0)
		})
	

	 })

	

	




})