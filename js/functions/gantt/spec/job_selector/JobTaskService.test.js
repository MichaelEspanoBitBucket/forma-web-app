describe("Job Task Service",function(){


	var JobTaskService,$httpBackend, jobTasks,mockFormattingService, mockDateFields,mockNumberFields;

	beforeEach(function(){

		module("GanttChart");

	})

	beforeEach(module(function($provide){

		//mock up dependencies

		$provide.constant("dateFieldsConstant",["estimated_start","estimated_end","actual_start","actual_end"])
		$provide.constant("numberFieldsConstant",["progress"])
		$provide.service("FormattingService",function(){
			
			this.stringToNumber = jasmine.createSpy("stringToNumber").and.callFake(function(numberString){
				return 2;
			})
			
			this.stringToDate = jasmine.createSpy("stringToDate").and.callFake(function(dateString){
				return new Date();
			})

		})

	}))

	beforeEach(inject(function(_JobTaskService_,_$httpBackend_,_FormattingService_,_dateFieldsConstant_,_numberFieldsConstant_){
		JobTaskService = _JobTaskService_;
		$httpBackend = _$httpBackend_;
		mockFormattingService = _FormattingService_
		mockDateFields = _dateFieldsConstant_
		mockNumberFields = _numberFieldsConstant_
		
		requestHandler = $httpBackend.when('GET',"/project_management/get_all_taskline_by?field=JobNo&value=JOB0001").respond(
				200,
				 [{
						job_no : 'JOB0001',
						job_task_no : 'JBT0001',
						estimated_start : new Date("2016-01-30"),
						estimated_end : new Date("2016-02-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "",
						job_task_line_dependency : [],
						description : "Mock Task 1",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0002',
						estimated_start : new Date("2016-02-30"),
						estimated_end : new Date("2016-03-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0001",
						job_task_line_dependency : [],
						description : "Mock Task 2",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0003',
						estimated_start : new Date("2016-03-30"),
						estimated_end : new Date("2016-04-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0002",
						job_task_line_dependency : [],
						description : "Mock Task 3",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0004',
						estimated_start : new Date("2016-04-30"),
						estimated_end : new Date("2016-05-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0002",
						job_task_line_dependency : [],
						description : "Mock Task 4",
						job_task_status : "Ongoing",
						reader_support : [] 
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0005',
						estimated_start : new Date("2016-05-30"),
						estimated_end : new Date("2016-06-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0002",
						job_task_line_dependency : [],
						description : "Mock Task 5",
						job_task_status : "Ongoing",
						reader_support : [] 
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0006',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0001",
						job_task_line_dependency : [],
						description : "Mock Task 6",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0007',
						estimated_start : new Date("2016-07-30"),
						estimated_end : new Date("2016-08-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "",
						job_task_line_dependency : [],
						description : "Mock Task 7",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0008',
						estimated_start : new Date("2016-08-30"),
						estimated_end : new Date("2016-09-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0007", 
						job_task_line_dependency : [],
						description : "Mock Task 8",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0009',
						estimated_start : new Date("2016-09-30"),
						estimated_end : new Date("2016-10-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "", 
						job_task_line_dependency : [],
						description : "Mock Task 9",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0010',
						estimated_start : new Date("2016-10-30"),
						estimated_end : new Date("2016-11-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0007", 
						job_task_line_dependency : [],
						description : "Mock Task 10",
						job_task_status : "Ongoing",
						reader_support : []
					}]
			)

	}))


	afterEach(function() {
	     $httpBackend.verifyNoOutstandingExpectation();
	     $httpBackend.verifyNoOutstandingRequest();
	     
   	});

	describe("Mocking dependencies",function(){
		it ("should mock formatting service",function(){
   			expect(mockFormattingService.stringToNumber("test")).toBe(2)
   			expect(typeof mockFormattingService.stringToDate("test")).toBe("object")
	   	})

	   	it ("it should mock date field constants ",function(){
	   		expect(mockDateFields[0]).toEqual("estimated_start")

	   	})

	   	it ("it should mock numberFieldsConstant",function(){
	   		expect(mockNumberFields[0]).toEqual("progress")
	   	})
	})


	describe("Job Task Service (SELECT)",function(){

		it("should get all job tasks given a job id",function(){
			JobTaskService.getAllJobTasksByJobId('JOB0001').then(function(result){
				jobTasks = result
			})

			$httpBackend.flush()
			expect(jobTasks.length).toBe(10)
		})

		it("should return empty array when getting all job tasks on error status codes", function(){

			requestHandler.respond(404,"")
			JobTaskService.getAllJobTasksByJobId('JOB0001').then(function(result){
				jobTasks = result
			})
			$httpBackend.flush()
			expect(jobTasks.length).toBe(0)

		})

		

	})

})