
describe("Formatting Service", function(){

	var FormattingService;

	beforeEach(function(){
		module("GanttChart")
	})

	beforeEach(inject(function(_FormattingService_){
		FormattingService = _FormattingService_
	}))

	it("should be defined",function()
	{
		expect(FormattingService).toBeDefined()
	})
	it("should convert date strings to date objects",function(){
		var convertedDate = FormattingService.stringToDate("2016-06-30T03:30:34+00:00")
		expect(Object.prototype.toString.call(convertedDate)).toEqual('[object Date]')
	})

	it("should convert number strings to number values ",function(){
		var convertedNumber = FormattingService.stringToNumber("2")
		expect(convertedNumber).toBe(2)
		convertedNumber = FormattingService.stringToNumber("asasasd")
		expect(convertedNumber).toBeNaN()

	})

})