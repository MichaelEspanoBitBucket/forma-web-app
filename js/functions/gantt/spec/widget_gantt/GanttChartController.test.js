describe("Gantt Chart Controller",function(){

	var $controller,$provide , GanttChartDataService, JobTaskService, vm , $q, deferred,deferred_preference_save,deferred_jobs,deferred_dataSource,deferred_preference,deferred_preference_by_widget_id, $scope, JobService , GanttChartPreferencesService,  data;

	var jobsDummy = [
						{
							job_no : "JBTL0001" ,
							description : "Mock Project"
						},
						{
							job_no : "JBTL0002" ,
							description : "Mock Project"
						}
					]
	var jobTasksDummy = [{
						job_no : 'JOB0001',
						job_task_no : 'JBT0001',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 50,
						parent_job_task : "",
						job_task_line_dependency : [],
						description : "Mock Task 1",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0002',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0001",
						job_task_line_dependency : [],
						description : "Mock Task 2",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0003',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 68,
						parent_job_task : "JBT0002",
						job_task_line_dependency : [],
						description : "Mock Task 3",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0004',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0002",
						job_task_line_dependency : [],
						description : "Mock Task 4",
						job_task_status : "Ongoing",
						reader_support : [] 
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0005',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 100,
						parent_job_task : "JBT0002",
						job_task_line_dependency : [],
						description : "Mock Task 5",
						job_task_status : "Ongoing",
						reader_support : [] 
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0006',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0001",
						job_task_line_dependency : [],
						description : "Mock Task 6",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0007',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "",
						job_task_line_dependency : [],
						description : "Mock Task 7",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0008',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0007", 
						job_task_line_dependency : [],
						description : "Mock Task 8",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0009',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "", 
						job_task_line_dependency : [],
						description : "Mock Task 9",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0010',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0007", 
						job_task_line_dependency : [],
						description : "Mock Task 10",
						job_task_status : "Ongoing",
						reader_support : []
					}]

	var dataSource = [

				{
					name : "Mock Task 1",
					id: 'JBT0001',
					content:"Mock Task 1"
				},
				{
					name : "Mock Task 2",
					id: 'JBT0002',
					parent: 'JBT0001',
					content: "Mock Task 2"
				},
				{
					name : "Mock Task 3",
					id: 'JBT0003',
					parent: 'JBT0002',
					content: "Mock Task 3",
					tasks : [
						{
							name : "Mock Task 3",
							from: new Date("2016-06-30"),
							to: new Date("2016-07-30"),
							id: 'JBT0003',
							progress: 0,
							content: "Mock Task 3"
						}
					]
				},
				{
					name : "Mock Task 4",
					id: 'JBT0004',
					parent: 'JBT0002',
					content: "Mock Task 4",
					tasks : [
						{
							name : "Mock Task 4",
							from: new Date("2016-06-30"),
							to: new Date("2016-07-30"),
							id: 'JBT0004',
							progress: 0,
							content: "Mock Task 4"
						}
					]
				},
				{
					name : "Mock Task 5",
					id: 'JBT0005',
					parent: 'JBT0002',
					content: "Mock Task 5",
					tasks : [
						{
							name : "Mock Task 5",
							from: new Date("2016-06-30"),
							to: new Date("2016-07-30"),
							id: 'JBT0005',
							progress: 0,
							content: "Mock Task 5"
						}
					]
				},
				{
					name : "Mock Task 6",
					id: 'JBT0006',
					parent: 'JBT0001',
					content: "Mock Task 6",
					tasks : [
						{
							name : "Mock Task 6",
							from: new Date("2016-06-30"),
							to: new Date("2016-07-30"),
							id: 'JBT0006',
							progress: 0,
							content: "Mock Task 6"
						}
					]
				},
				{
					name : "Mock Task 7",
					id: 'JBT0007',
					content: "Mock Task 7"
				},
				{
					name : "Mock Task 8",
					id: 'JBT0008',
					parent: 'JBT0007',
					content: "Mock Task 8",
					tasks : [
						{
							name : "Mock Task 8",
							from: new Date("2016-06-30"),
							to: new Date("2016-07-30"),
							id: 'JBT0008',
							progress: 0,
							content: "Mock Task 8"
						}
					]
				},
				{
					name : "Mock Task 9",
					id: 'JBT0009',
					content: "Mock Task 9",
					tasks : [
						{
							name : "Mock Task 9",
							from: new Date("2016-06-30"),
							to: new Date("2016-07-30"),
							id: 'JBT0009',
							progress: 0,
							content: "Mock Task 9"
						}
					]
				},
				{
					name : "Mock Task 10",
					id: 'JBT0010',
					parent: 'JBT0007',
					content: "Mock Task 10",
					tasks : [
						{
							name : "Mock Task 10",
							from: new Date("2016-06-30"),
							to: new Date("2016-07-30"),
							id: 'JBT0010',
							progress: 0,
							content: "Mock Task 10"
						}
					]
				}

			]

	beforeEach(function(){

		module("GanttChart")

		
	})

	beforeEach(module(function(_$provide_){
			
			$provide = _$provide_
			
			$provide.service("JobTaskService",function(){
			})

			$provide.service("JobService",function(){
			})

			$provide.service("GanttChartDataService",function(){
				
			})

			$provide.service("GanttChartPreferencesService",function(){

			})


			

		}))

	beforeEach(inject(function(_$controller_,_JobTaskService_,_GanttChartDataService_,_$q_,_$rootScope_,_JobService_,_GanttChartPreferencesService_){

		$scope = _$rootScope_.$new()
		$controller = _$controller_
		$q = _$q_
		deferred = $q.defer()
		deferred_jobs = $q.defer()
		deferred_dataSource = $q.defer()
		deferred.resolve(jobTasksDummy)
		deferred_jobs.resolve(jobsDummy)
		deferred_dataSource.resolve(dataSource)
		deferred_preference_save = $q.defer()
		deferred_preference_save.resolve("Saved")
		deferred_preference = $q.defer()
		deferred_preference.resolve([
								{
									id : "0002",
									config : {
										viewScale : "week",
										currentDateLine : "column",
										autoWidth : "true",
										autoHeight : "true",
										resizableLabels : "false",
										showLabels : "true",
										jobId : "JOB0001",
										jobName : "Test1"
									}
								},
								{
									id : "0003",
									config : {
										viewScale : "month",
										currentDateLine : "line",
										autoWidth : "true",
										autoHeight : "true",
										resizableLabels : "false",
										showLabels : "true",
										jobId : "JOB0002",
										jobName : "Test2"
									}
								}
							])
		deferred_preference_by_widget_id = $q.defer()
		deferred_preference_by_widget_id.resolve(
								{
									id : "0002",
									config : {
										viewScale : "week",
										currentDateLine : "column",
										autoWidth : "true",
										autoHeight : "true",
										resizableLabels : "false",
										showLabels : "true",
										jobId : "JOB0001",
										jobName : "Test1"
									}
								})
		JobTaskService = _JobTaskService_
		JobService = _JobService_
		GanttChartDataService = _GanttChartDataService_
		GanttChartPreferencesService = _GanttChartPreferencesService_

		GanttChartPreferencesService.getAllGanttChartPreferences = jasmine.createSpy("getAllGanttChartPreferences").and.returnValue(deferred_preference.promise)
		GanttChartPreferencesService.getGanttChartPreferenceByWidgetId = jasmine.createSpy("getGanttChartPreferenceByWidgetId").and.returnValue(deferred_preference_by_widget_id.promise)
		GanttChartPreferencesService.saveGanttChartPreferences = jasmine.createSpy("saveGanttChartPreferences").and.returnValue(deferred_preference_save.promise)

		JobService.getAllJobs = jasmine.createSpy("getAllJobs").and.returnValue(deferred_jobs.promise)
		JobTaskService.getAllJobTasksByJobId = jasmine.createSpy("getAllJobTasksByJobId").and.returnValue(deferred.promise)
		GanttChartDataService.getDataSourceFromTasks = jasmine.createSpy("getDataSourceFromTasks").and.returnValue(
					deferred_dataSource.promise
				)

		 data = {
				parent : {
							id :'0002'
					}
		}

		vm = $controller("GanttChartController",{

				GanttChartDataService : GanttChartDataService,
				GanttChartPreferencesService : GanttChartPreferencesService,
				JobTaskService : JobTaskService,
				JobService : JobService
			},data)

		$scope.$apply()

	}))

	it("should be defined",function(){
		
		expect(vm).toBeDefined()
	
	})

	it ("should not set the widget initially to be not editable",function(){

		expect(vm.isEditable).toBe(false)
	})

	it ("should set the initial values of the gantt when newly added",function(){

		var deferred_local = $q.defer()
		deferred_local.resolve([])
		GanttChartPreferencesService.getGanttChartPreferenceByWidgetId = jasmine.createSpy("getGanttChartPreferenceByWidgetId").and.returnValue(deferred_local.promise)
		vm.initializeGantt()
		$scope.$apply()
		expect(vm.viewScale).toEqual("day")
		expect(vm.currentDateLine).toEqual("column")
		expect(vm.autoWidth).toBe(false)
		expect(vm.autoHeight).toBe(false)
		expect(vm.resizableLabels).toBe(false)
		expect(vm.showLabels).toBe(true)
		
	})

	it ("should set the initial values of the gantt equal to the passed config",function(){
		
		expect(vm.viewScale).toEqual("week")
		expect(vm.currentDateLine).toEqual("column")
		expect(vm.autoWidth).toBe(true)
		expect(vm.autoHeight).toBe(true)
		expect(vm.resizableLabels).toBe(false)
		expect(vm.showLabels).toBe(true)
		
	})

	describe("Job Selector",function(){
		it("should be able to toggle the gantt chart widget mode to be editable or not",function(){
			expect(vm.isEditable).toBe(false)
			vm.toggleMode()
			expect(vm.isEditable).toBe(true)
			vm.toggleMode()
			expect(vm.isEditable).toBe(false)
		})

		it("should be able to get a list of all Jobs",function(){
			 vm.getAllJobs()
			 $scope.$apply()
			 expect(vm.jobs.length).toBe(2)
			 expect(vm.jobs).toBe(jobsDummy)
		})


		it ("should show job selector",function(){
			vm.isEditable = false
			vm.showJobSelector()
			$scope.$apply()
			expect(vm.jobs).toBeDefined()
			expect(vm.isEditable).toBe(true)

		})

		it("should hide job selector",function(){
			vm.isEditable = true
			vm.hideJobSelector()
			expect(vm.isEditable).toBe(false)
		})

		it("should supply the Gantt view with the data source",function(){
			vm.setGanttDataSource("JBT0001")
			$scope.$apply()
			expect(JobTaskService.getAllJobTasksByJobId).toHaveBeenCalledWith("JBT0001")
			expect(GanttChartDataService.getDataSourceFromTasks).toHaveBeenCalledWith(jobTasksDummy)
			expect(vm.dataSource).toEqual(dataSource)

		})

		it ("should be able to select a job from the list of jobs and hide job selector",function(){
			vm.getAllJobs()
			spyOn(vm,"hideJobSelector")
			spyOn(vm, "setGanttDataSource")
			$scope.$apply()
			expect(vm.jobs[0].job_no).toEqual("JBTL0001")
			vm.selectJob(vm.jobs[0])
			expect(vm.hideJobSelector).toHaveBeenCalled()
			expect(vm.setGanttDataSource).toHaveBeenCalledWith(vm.jobs[0].job_no)
			expect(vm.job).toBe(vm.jobs[0])
			expect(vm.isEditable).toBe(false)
		})

		it("should hide Gantt Configuration if job selector is shown",function(){
			
			spyOn(vm,"hideConfigurationBox")
			vm.showJobSelector()
			expect(vm.hideConfigurationBox).toHaveBeenCalled()
			expect(vm.isConfigurable).toBeFalsy()
		})

	})

	describe("Gantt Configuration",function(){

		it("should show/hide configuration box",function(){
			expect(vm.isConfigurable).toBeFalsy()
			vm.showConfigurationBox()
			expect(vm.isConfigurable).toBeTruthy()
			vm.hideConfigurationBox()
			expect(vm.isConfigurable).toBeFalsy()
		})

		it("should hide Job Selector if configuration box is shown",function(){
			spyOn(vm,"hideJobSelector")
			vm.showConfigurationBox()
			expect(vm.hideJobSelector).toHaveBeenCalled()
			expect(vm.isEditable).toBeFalsy()
			
		})
	})

	describe("Gantt Chart User Preference Saving and Fetching",function(){

		it("should get all the gantt chart preferences",function(){
			$scope.$apply()
			expect(vm.ganttPreferences.length).toBe(2)
		})
		
		xit("should not be able to save the widget when the widget is still new",function(){
			vm.isNew = true
			vm.saveGanttChartPreference()
			expect(GanttChartPreferencesService.saveGanttChartPreferences).not.toHaveBeenCalled()
			$scope.$apply()
			expect(vm.ganttPreferences.length).toBe(2)
		})

		xit("should be able to save the widget when the widget is not new",function(){
			vm.isNew = false
			vm.saveGanttChartPreference()
			expect(GanttChartPreferencesService.saveGanttChartPreferences).toHaveBeenCalled()
			$scope.$apply()
			expect(vm.ganttPreferences.length).toBe(3)
		})

		it("should be able to add the widget preference when the widget id is not found in the current preferences ",function(){
			vm.isNew = false
			vm.parent = {}
			vm.parent.id = "0004"
			vm.viewScale = "month"
			$scope.$apply()
			vm.saveGanttChartPreference()
			expect(GanttChartPreferencesService.saveGanttChartPreferences).toHaveBeenCalled()
			$scope.$apply()
			expect(vm.ganttPreferences.length).toBe(3)
			expect(vm.ganttPreferences[2].id).toBe('0004')
			expect(vm.ganttPreferences[2].config.viewScale).toEqual("month")
		})

		it("should be able to replace the widget preference when the widget id is already found in the current preferences ",function(){
			
			var date = new Date()
			data = {
				parent : {
					id :'0002'
				},
			}

		vm = $controller("GanttChartController",{
				GanttChartDataService : GanttChartDataService,
				GanttChartPreferencesService : GanttChartPreferencesService,
				JobTaskService : JobTaskService,
				JobService : JobService
			},data)
			$scope.$apply()	
			vm.viewScale = "week"
			vm.showLabels = true
			vm.autoWidth = false
			vm.saveGanttChartPreference()
			expect(GanttChartPreferencesService.saveGanttChartPreferences).toHaveBeenCalled()
			$scope.$apply()
			expect(vm.ganttPreferences.length).toBe(2)
			expect(vm.ganttPreferences[0].config.showLabels).toBe(true)
			expect(vm.ganttPreferences[0].config.autoWidth).toBe(false)
			
		})

		xit("should be able to remove from the gantt chart preferences given the widget id",function(){

		})
		
		
	})

})