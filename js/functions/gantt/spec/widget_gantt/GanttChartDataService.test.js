
describe("Gantt Chart Data Service",function(){

	var GanttChartDataService,jobTasksDummy,row,dataSource,$timeout;

	beforeEach(module("GanttChart"))
	

	beforeEach(inject(function(_GanttChartDataService_,_$timeout_){

		$timeout = _$timeout_
		GanttChartDataService = _GanttChartDataService_
		jobTasksDummy = [{
						job_no : 'JOB0001',
						job_task_no : 'JBT0001',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "",
						job_task_line_dependency : [],
						description : "Mock Task 1",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0002',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0001",
						job_task_line_dependency : ['JBT0003'],
						description : "Mock Task 2",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0003',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0002",
						job_task_line_dependency : [],
						description : "Mock Task 3",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0004',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0002",
						job_task_line_dependency : [],
						description : "Mock Task 4",
						job_task_status : "Ongoing",
						reader_support : [] 
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0005',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0002",
						job_task_line_dependency : [],
						description : "Mock Task 5",
						job_task_status : "Ongoing",
						reader_support : [] 
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0006',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0001",
						job_task_line_dependency : [],
						description : "Mock Task 6",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0007',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "",
						job_task_line_dependency : [],
						description : "Mock Task 7",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0008',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "JBT0007", 
						job_task_line_dependency : [],
						description : "Mock Task 8",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0009',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "", 
						job_task_line_dependency : [],
						description : "Mock Task 9",
						job_task_status : "Ongoing",
						reader_support : []
					},
					{
						job_no : 'JOB0001',
						job_task_no : 'JBT0010',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						color : "#54AE6E",
						progress : 0,
						parent_job_task : "JBT0007", 
						job_task_line_dependency : [],
						description : "Mock Task 10",
						job_task_status : "Ongoing",
						reader_support : []
					}]

	}))


	it("should be defined",function(){
		expect(GanttChartDataService).toBeDefined()
	})


	it("should get all base tasks from the given tasks and remove them from the list",function(){
		
		var jobTasksDummyOrigLength = jobTasksDummy.length
		var baseTasks = GanttChartDataService.removeTasks(jobTasksDummy)
		expect(baseTasks.length).toBe(3)
		expect(baseTasks[0]['parent_job_task']).toEqual("")
		expect(jobTasksDummy.length).toEqual(jobTasksDummyOrigLength - baseTasks.length)

	})

	it("should get all tasks given their parent task id and remove it from the list",function(){
		var jobTasksDummyOrigLength = jobTasksDummy.length
		var tasks = GanttChartDataService.removeTasks(jobTasksDummy , "JBT0001")
		expect(tasks.length).toBe(2)
		expect(tasks[0]['parent_job_task']).toEqual("JBT0001")
		expect(jobTasksDummy.length).toEqual(jobTasksDummyOrigLength - tasks.length)

	})


	describe ("Building A Gantt Chart Data Source",function(){

		it ("should determine if a gantt task should be created for a job task",function(){

			var isTask = GanttChartDataService.isParentGanttRow(jobTasksDummy,"JBT0001")
			expect(isTask).toBe(true)
			isTask = GanttChartDataService.isParentGanttRow(jobTasksDummy,"JBT0010")
			expect(isTask).toBe(false)
		})

		
		describe("Gantt Chart Data Service : Gantt Row Creation",function(){

			var task;

			/*
				task definition : 
				job_no : 'JOB0001',
						job_task_no : 'JBT0001',
						estimated_start : new Date("2016-06-30"),
						estimated_end : new Date("2016-07-30"),
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0,
						parent_job_task : "",
						job_task_line_dependency : [],
						description : "Mock Task 1",
						job_task_status : "Ongoing",
						reader_support : []
			*/

			beforeEach(function(){
				row = GanttChartDataService.createGanttRow(jobTasksDummy[0],jobTasksDummy)
			})

			it("should create a gantt row",function(){
				expect(row).toBeDefined()
			})

			it("should create a gantt row with correct name",function(){
				expect(row.name).toEqual("Mock Task 1")

			})
			it("should create a gantt row with correct id",function(){
				expect(row.id).toEqual("JBT0001")
			})
			

			it("should create a gantt row with correct content",function(){
				expect(row.content).not.toBeDefined()
				row = GanttChartDataService.createGanttRow(jobTasksDummy[0],jobTasksDummy,false)
				expect(row.content).not.toBeDefined()
				row = GanttChartDataService.createGanttRow(jobTasksDummy[0],jobTasksDummy,true)
				expect(row.content).toEqual("Mock Task 1")

			})

			it("should create a gantt row with a correct parent",function(){
				expect(row.parent).not.toBeDefined()
				row = GanttChartDataService.createGanttRow(jobTasksDummy[1],jobTasksDummy,false)
				expect(row.parent).toEqual("JBT0001")
			})

			it("should create a gantt row with correct gantt tasks",function(){
				expect(row.tasks).not.toBeDefined()
				row = GanttChartDataService.createGanttRow(jobTasksDummy[9],jobTasksDummy,false)
				expect(row.tasks).toEqual([{
					name : "Mock Task 10",
					from : new Date("2016-06-30"),
					to: new Date("2016-07-30"),
					id: "JBT0010",
					color : "#54AE6E",
					progress: 0,
					content:  "Mock Task 10",
					data : {
						status : "Ongoing",
						actual_start : new Date("2016-06-30"),
						actual_end : new Date("2016-07-30"),
						progress : 0
					}
				}])
			})
			

		})

		describe("Creating a Gantt Task",function(){


			var task;

			beforeEach(function(){
				task = GanttChartDataService.createGanttTask(jobTasksDummy[0])
			})

			it("should create a gantt task",function(){
				expect(task).toBeDefined()
			})

			it("should create a gantt task with correct name",function(){
				expect(task.name).toBe("Mock Task 1")
			})
			it("should create a gantt task with correct from",function(){
				expect(task.from).toEqual(new Date("2016-06-30"))
			})

			it("should create a gantt task with correct to",function(){
				expect(task.to).toEqual(new Date("2016-07-30"))
			})

			it("should create a gantt task with correct id",function(){
				expect(task.id).toBe("JBT0001")
			})

			it("should create a gantt task with correct progress",function(){
				expect(task.progress).toBe(0)
			})

			it("should create a gantt task with correct color",function(){
				
				
				task = GanttChartDataService.createGanttTask(jobTasksDummy[0])
				expect(task.color).toEqual("#5bc0de")
				task = GanttChartDataService.createGanttTask(jobTasksDummy[9])
				expect(task.color).toEqual("#54AE6E")

			})

			it("should create a gantt task with correct dependency",function(){
				
				
				task = GanttChartDataService.createGanttTask(jobTasksDummy[9])
				expect(task.dependencies).not.toBeDefined()
				task = GanttChartDataService.createGanttTask(jobTasksDummy[1])
				expect(task.dependencies).toEqual([{from:'JBT0003'}])

			})

			it("should create a gantt task with status, actual start and end dates",function(){
				
				task = GanttChartDataService.createGanttTask(jobTasksDummy[0])
				
				expect(task.data.status).toBe(jobTasksDummy[0]['job_task_status'])
				expect(task.data['actual_start']).toBe(jobTasksDummy[0]['actual_start'])
				expect(task.data['actual_end']).toBe(jobTasksDummy[0]['actual_end'])
				expect(task.data.progress).toBe(jobTasksDummy[0]['progress'])

				jobTasksDummy[0]['job_task_status'] = undefined
				jobTasksDummy[0]['actual_start'] = undefined
				jobTasksDummy[0]['actual_end'] = undefined
				jobTasksDummy[0]['progress'] = undefined
				task = GanttChartDataService.createGanttTask(jobTasksDummy[0])
				expect(task.data.status).not.toBeDefined()
				expect(task.data['actual_start']).not.toBeDefined()
				expect(task.data['actual_end']).not.toBeDefined()
				expect(task.data.progress).not.toBeDefined()

			})



			it("should create a gantt task with correct content",function(){
				expect(task.content).not.toBeDefined()
				task = GanttChartDataService.createGanttTask(jobTasksDummy[0],false)
				expect(task.content).not.toBeDefined()
				task = GanttChartDataService.createGanttTask(jobTasksDummy[0],true)
				expect(task.content).toEqual("Mock Task 1")
			})
			

		})


		describe("Getting Final Data Source",function(){


			it("should get a final data source object with correct hierarchy",function(){

			
				var expectedDataSource = [

					{
						name : "Mock Task 1",
						id: 'JBT0001',
						content:"Mock Task 1"
					},
					{
						name : "Mock Task 2",
						id: 'JBT0002',
						parent: 'JBT0001',
						content: "Mock Task 2"
					},
					{
						name : "Mock Task 3",
						id: 'JBT0003',
						parent: 'JBT0002',
						content: "Mock Task 3",
						tasks : [
							{
								name : "Mock Task 3",
								from: new Date("2016-06-30"),
								to: new Date("2016-07-30"),
								id: 'JBT0003',
								color : "#5bc0de",
								progress: 0,
								content: "Mock Task 3",
								data : {
									status : "Ongoing",
									actual_start : new Date("2016-06-30"),
									actual_end : new Date("2016-07-30"),
									progress : 0
								}

							}
						]
					},
					{
						name : "Mock Task 4",
						id: 'JBT0004',
						parent: 'JBT0002',
						content: "Mock Task 4",
						tasks : [
							{
								name : "Mock Task 4",
								from: new Date("2016-06-30"),
								to: new Date("2016-07-30"),
								id: 'JBT0004',
								color : "#5bc0de",
								progress: 0,
								content: "Mock Task 4",
								data : {
									status : "Ongoing",
									actual_start : new Date("2016-06-30"),
									actual_end : new Date("2016-07-30"),
									progress : 0
								}
							}
						]
					},
					{
						name : "Mock Task 5",
						id: 'JBT0005',
						parent: 'JBT0002',
						content: "Mock Task 5",
						tasks : [
							{
								name : "Mock Task 5",
								from: new Date("2016-06-30"),
								to: new Date("2016-07-30"),
								id: 'JBT0005',
								color : "#5bc0de",
								progress: 0,
								content: "Mock Task 5",
								data : {
									status : "Ongoing",
									actual_start : new Date("2016-06-30"),
									actual_end : new Date("2016-07-30"),
									progress : 0
								}
							}
						]
					},
					{
						name : "Mock Task 6",
						id: 'JBT0006',
						parent: 'JBT0001',
						content: "Mock Task 6",
						tasks : [
							{
								name : "Mock Task 6",
								from: new Date("2016-06-30"),
								to: new Date("2016-07-30"),
								id: 'JBT0006',
								color : "#5bc0de",
								progress: 0,
								content: "Mock Task 6",
								data : {
									status : "Ongoing",
									actual_start : new Date("2016-06-30"),
									actual_end : new Date("2016-07-30"),
									progress : 0
								}
							}
						]
					},
					{
						name : "Mock Task 7",
						id: 'JBT0007',
						content: "Mock Task 7"
					},
					{
						name : "Mock Task 8",
						id: 'JBT0008',
						parent: 'JBT0007',
						content: "Mock Task 8",
						tasks : [
							{
								name : "Mock Task 8",
								from: new Date("2016-06-30"),
								to: new Date("2016-07-30"),
								id: 'JBT0008',
								color : "#5bc0de",
								progress: 0,
								content: "Mock Task 8",
								data : {
									status : "Ongoing",
									actual_start : new Date("2016-06-30"),
									actual_end : new Date("2016-07-30"),
									progress : 0
								}
							}
						]
					},
					{
						name : "Mock Task 9",
						id: 'JBT0009',
						content: "Mock Task 9",
						tasks : [
							{
								name : "Mock Task 9",
								from: new Date("2016-06-30"),
								to: new Date("2016-07-30"),
								id: 'JBT0009',
								color : "#5bc0de",
								progress: 0,
								content: "Mock Task 9",
								data : {
									status : "Ongoing",
									actual_start : new Date("2016-06-30"),
									actual_end : new Date("2016-07-30"),
									progress : 0
								}
							}
						]
					},
					{
						name : "Mock Task 10",
						id: 'JBT0010',
						parent: 'JBT0007',
						content: "Mock Task 10",
						tasks : [
							{
								name : "Mock Task 10",
								from: new Date("2016-06-30"),
								to: new Date("2016-07-30"),
								id: 'JBT0010',
								color : "#54AE6E",
								progress: 0,
								content: "Mock Task 10",
								data : {
									status : "Ongoing",
									actual_start : new Date("2016-06-30"),
									actual_end : new Date("2016-07-30"),
									progress : 0
								}
							}
						]
					}

				]

				GanttChartDataService.getDataSourceFromTasks(jobTasksDummy).then(function(data){
						dataSource = data	
				})
				$timeout.flush()
				
				expect(dataSource).toBeDefined(	)
				expect(dataSource).toEqual(expectedDataSource)
				
			})
		})

		

	})

})

/**

row
{
	name : 
	id: 
	parent:
	content:
}

task {
	name :
	from:
	to:
	id:
	progress:
	content:
}



*/