
describe("Gantt Chart Preferences Service",function(){

	var GanttChartPreferencesService, UserPreferencesService, $q, requestHandler,$timeout,deferred , $httpBackend;

	beforeEach(module("GanttChart"))
	beforeEach(module(function($provide){
		$provide.service("UserPreferencesService",function(){
		})
	}))

	beforeEach(inject(function(_GanttChartPreferencesService_,_UserPreferencesService_,_$q_,_$timeout_,_$httpBackend_){
		GanttChartPreferencesService = _GanttChartPreferencesService_
		UserPreferencesService = _UserPreferencesService_
		$q = _$q_
		$timeout = _$timeout_
		$httpBackend = _$httpBackend_

		deferred = $q.defer()
		UserPreferencesService.getPreferences = deferred.promise

	}))

	it ("should be defined",function(){
		expect(GanttChartPreferencesService).toBeDefined()
	})


	it ("should define a method that gets all the gantt chart preferences",function(){
		expect(GanttChartPreferencesService.getAllGanttChartPreferences).toBeDefined()
	})

	it ("should define a method that gets a gantt chart preferences by the widget id",function(){
		expect(GanttChartPreferencesService.getGanttChartPreferenceByWidgetId).toBeDefined()
	})

	describe ("Functions for getting all the Gantt Chart Preferences",function(){

		beforeEach(function(){
			
			deferred.resolve({
				Layout : [
							{
								id : "0001",
								type : "widgetFeeds"
							},
							{
								id : "0002",
								type : "widgetGanttChart"
							},
							{
								id : "0003",
								type : "widgetGanttChart"
							}
						],
				GanttConfig : [
								{
									id : "0002",
									config : {
										viewScale : "week",
										currentDateLine : "column",
										autoWidth : "true",
										autoHeight : "true",
										resizableLabels : "false",
										showLabels : "true",
										jobId : "JOB0001"
									}
								},
								{
									id : "0003",
									config : {
										viewScale : "month",
										currentDateLine : "line",
										autoWidth : "true",
										autoHeight : "true",
										resizableLabels : "false",
										showLabels : "true",
										jobId : "JOB0002"
									}
								}
							]
			})
			
			

		})
		
		it("should get an array of config objects/ ganttChart preferences ",function(){

			var ganttConfigs
			GanttChartPreferencesService.getAllGanttChartPreferences().then(function(data){
				ganttConfigs = data
			})
			$timeout.flush()
			expect(ganttConfigs).toEqual([
								{
									id : "0002",
									config : {
										viewScale : "week",
										currentDateLine : "column",
										autoWidth : "true",
										autoHeight : "true",
										resizableLabels : "false",
										showLabels : "true",
										jobId : "JOB0001"
									}
								},
								{
									id : "0003",
									config : {
										viewScale : "month",
										currentDateLine : "line",
										autoWidth : "true",
										autoHeight : "true",
										resizableLabels : "false",
										showLabels : "true",
										jobId : "JOB0002"
									}
								}
							])
		})

		it ("should get a gantt chart preference given the widget id",function(){
			var ganttConfig;
			GanttChartPreferencesService.getGanttChartPreferenceByWidgetId("0003").then(function(data){
				ganttConfig = data
			})
			$timeout.flush()
			expect(ganttConfig).toEqual({
									id : "0003",
									config : {
										viewScale : "month",
										currentDateLine : "line",
										autoWidth : "true",
										autoHeight : "true",
										resizableLabels : "false",
										showLabels : "true",
										jobId : "JOB0002"
									}
								})
		})

		it ("should return  an undefined when given the widget id does not have a gantt preference",function(){
			var ganttConfig;
			GanttChartPreferencesService.getGanttChartPreferenceByWidgetId("0001").then(function(data){
				ganttConfig = data
			})
			$timeout.flush()
			expect(ganttConfig).toEqual(undefined)
		})


	})

	describe("Functions for Saving Gantt Preferences",function(){

		it ("should be defined",function(){

			expect(GanttChartPreferencesService.saveGanttChartPreferences).toBeDefined()
		})

		it("should return rejection when response is error",function(){
			var preferences = {}
			$httpBackend.when("POST","/modules/dashboard_data/save-dashboard-user-gantt-config.php").respond(401,"")
			var data = GanttChartPreferencesService.saveGanttChartPreferences(preferences)
			$httpBackend.flush()
			expect(data.$$state.status).toEqual(2)
		})

		it("should save the gantt chart preference",function(){
			var preferences = {}
			$httpBackend.when("POST","/modules/dashboard_data/save-dashboard-user-gantt-config.php").respond(200,"")
			var data = GanttChartPreferencesService.saveGanttChartPreferences(preferences)
			$httpBackend.flush()
			expect(data.$$state.status).toEqual(1)
		})


		
	})


	

})