$(document).ready(function(){
	giGetData.init();
	giController.init();
	var $getdata;
});

var giGetData = {
	init:function(){
		this.addEventListener();
	},
	addEventListener:function(){
		var self = this;
		
	},
	allPaperSize:function(){
		var self = this;
		var res;
		$.ajax({
			url:'/gi_data/gi-rd-report-designer',
			type:'GET',
			success:function(result){
				res = result;		
			}
		})
		return res;
	},
	chartTypes:function(callback){
		var self = this;
		var rResult;
		$.ajax({
			url:'/gi_data/gi-rd-report-designer?GetData=Chart_Types',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	dataModels:function(callback){
		var self = this;
		var rResult;
		var $request = $.ajax({
			url:'/gi_data/gi-dm-data-models-json',
			type:'GET',
			success:function(result){
				callback(JSON.parse(result));
			}
		})
	},
	dataModel:function(id, callback){
		var self = this;
		var rResult;
		$.ajax({
			url:'/gi_data/gi-dm-data-model?id=' + id,
			type:'GET',
			success:function(result){
				callback(JSON.parse(result));
			}
		})
	},
	paperSizes:function(callback){
		$.ajax({
			url:'/gi_data/gi-rd-get-all-paper-size',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	aggregateFunctions:function(callback){
		$.ajax({
			url:'/gi_data/gi-aggregate-functions-json',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	functions:function(callback){
		$.ajax({
			url:'/gi_data/gi-functions-json',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	shapeTypes:function(callback){
		$.ajax({
			url:'/gi_data/gi-rd-get-shape-types',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	departments:function(callback){
		$.ajax({
			url:'/gi_data/gi-rd-get-departments',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	departmentPositionLevel:function(callback){
		$.ajax({
			url:'/gi_data/gi-rd-get-all-department-position-level',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	
	positions:function(callback){
		$.ajax({
			url:'/gi_data/gi-rd-get-all-positions',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},

	groups:function(callback){
		$.ajax({
			url:'/gi_data/gi-rd-get-all-groups',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	allObjects:function(callback){
		$.ajax({
			url:'/gi_data/gi-rd-get-all-objects',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	allDataSources:function(callback){
		$.ajax({
			url:'/gi_data/gi-rd-datasources',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	allDataSourceOnly:function(callback) {
		$.ajax({
			url:'/gi_data/gi-data-sources-json',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	allEntities:function(dsId, callback) {
		$.ajax({
			url:'/gi_data/gi-ds-entities-json?id=' + dsId,
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	entities:function(dsId, search, callback) {
		var entities = $.ajax({
			url:'/gi_data/gi-get-entities-json?id=' + dsId + '&search=' + search,
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
		//return entities;
	},
	//gi_data/gi-ds-entity-attributes-json?ds-id=1&entity-id=25_tbl_SupplierList
	entityAttributes:function(dsId, entityId, callback) {
		$.ajax({
			url:'/gi_data/gi-ds-entity-attributes-json?ds-id=' + dsId + '&entity-id=' + entityId,
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},
	sharedObjects:function(callback) {

		if(typeof($getdata) == 'object'){
			$getdata.abort();
			//console.log($getdata)
		}
		$getdata = $.ajax({
			url:'/gi_data/gi-get-shared-objects',
			type:'GET',
			success:function(result){
				callback(result);
			}
		})
	},

}


// var giReportRequest = {
// 	saveReport:function(jReport, callback){
// 		$.ajax({ 
// 			url: "/gi_data/gi-report-save",
// 			type: "POST",
// 			data: {report:jReport},
// 			dataType: "json",
// 			success: function(result){
// 				callback(result);
// 			}
// 		});
// 	},
// }

var giPostData = {
	init:function(){

	},
	addEventListener:function(){

	},
	loadGiqData:function(chartGiqs, callback){
		$.ajax({ 
			url: "/gi_data/gi-dp-chart-data",
			type: "POST",
			data: chartGiqs,
			dataType: "json",
			success: function(dataAndConfigSets){
				if($.type(callback)=="function"){
					callback(dataAndConfigSets);
				}				
			}
		});
	},
	saveReport:function(jReport, callback){
		$.ajax({ 
			url: "/gi_data/gi-rd-save-report",
			type: "POST",
			data: {report:JSON.stringify(jReport)},
			dataType: "json",
			success: function(result){
				callback(result);
			}
		});
	},
	getChartData:function(id, callback){
		$.ajax({ 
		url: "/gi_data/gi-dp-chart-data?id=" + id,
		type: "POST",
		
		dataType: "json",
		success: function(dataAndConfigSets){

			if($.type(callback)=="function"){
				callback(dataAndConfigSets);
			}	
		}
		});
	},
	getChartGiq:function(id,callback){
		$.ajax({ 
		url: "/gi_data/gi-rd-chart-giq?id=" + id,
		type: "POST",
		
		dataType: "json",
		success: function(giq){
			if($.type(callback)=="function"){
				callback(giq);
			}	
		}
		});
	},
	addSharedObject:function(object,callback){
		$.ajax({ 
		url: "/gi_data/gi-rd-save-shared-object",
		data:{object:JSON.stringify(object)},
		type: "POST",
		dataType: "json",
		success: function(res){
			if($.type(callback)=="function"){
				callback(res);
			}	
		}
		});
	},

	addDataSource:function(object,callback){
		$.ajax({ 
		url: "/gi_data/gi-ds-save-datasource",
		data:object,
		type: "POST",
		dataType: "json",
		success: function(res){
			
			if($.type(callback)=="function"){
				
				callback(res);
			}	
		}
		});
	},
	removeDataSource:function(id,callback){
		$.ajax({ 
		url: "/gi_data/gi-ds-delete-datasource",
		data:{id:id},
		type: "POST",
		dataType: "json",
		success: function(res){
			
			if($.type(callback)=="function"){
				
				callback(res);
			}	
		}
		});
	}
	
}


var giController = {
	init:function(){
		this.addEventListener();
	},
	addEventListener:function(){
		
	},
	workspaceDrop:function(event,ui,obj){
		giGetData.dataModels(function(models){
			giChartUI.generateObjectContainer(event, ui, obj, models);
		})
	},
	dataModels:function(obj,callback){
		giGetData.dataModel(obj.val(),function(data){
			callback(data);
		})
	},
	getSavedData:function(id,callback){
		if (id!="") {	
			$.ajax({ 
				url: "/gi_data/gi-rd-get-report?id=" + id,
				type: "GET",
				success: function(report){
					if($.type(callback)=="function"){
						callback(report);
					}				
				}
			});
		}
	},
	getSavedDataByDepartmentId:function(id,callback){
		if (id!="") {	
			$.ajax({ 
				url: "/gi_data/gi-rd-get-report-by-department-id?id=" + id,
				type: "GET",
				success: function(report){
					if($.type(callback)=="function"){
						callback(report);
					}				
				}
			});
		}
	},
	getSavedDataUserId:function(id,callback){
		if (id!="") {	
			var $reports = $.ajax({ 
				url: "/gi_data/gi-rd-get-report-by-user-id?id=" + id,
				type: "GET",
				success: function(report){
					if($.type(callback)=="function"){
						callback(report);
					}				
				}
			});
		}
	},
	getDashboardReportsByActiveUser:function(callback){
			var $reports = $.ajax({ 
				url: "/modules/gi_data_opt/gi-db-reports-by-active-user.php",
				type: "GET",
				success: function(report){
					if($.type(callback)=="function"){
						callback(report);
					}				
				}
			});
	}
	
}

var giUploadFile = {
	uploadExcel:function(data, callback){
		$.ajax({
		        url: '/gi_data/gi-rd-upload-excel-file?files',
		        type: 'POST',
		        data: data,
		        cache: false,
		        dataType: 'json',
		        processData: false, // Don't process the files
		        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
		        success: function(data, textStatus, jqXHR)
		        {
		        	callback(data, textStatus, jqXHR);
		           
		        },
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		            // Handle errors here
		            console.log('ERRORS: ' + textStatus);
		            // STOP LOADING SPINNER
		        }
		    });
	},
	submitUploadExcel:function(formData, callback, completeCallBack){
		$.ajax({
		        url: '/gi_data/gi-rd-upload-excel-file',
		        type: 'POST',
		        data: formData,
		        cache: false,
		        dataType: 'json',
		        success: function(data, textStatus, jqXHR)
		        {
		        	callback(data, textStatus, jqXHR);
		            
		        },
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		            // Handle errors here
		            console.log('ERRORS: ' + textStatus);
		        },
		        complete: function()
		        {
		        	completeCallBack();
		            // STOP LOADING SPINNER
		        }
		    });
	}
}

// var giData = {
// 	//this.departments = []
// }