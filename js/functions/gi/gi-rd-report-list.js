$(document).ready(function(){

	giUIReportDesigner.init();
	
	
	$('body').on('click','.gi_delete_report',function(){
		var id = $(this).data('id');

		var newConfirm = new jConfirm("Are you sure do you want to delete?", 'Confirmation Dialog','', '', '', function(r){
			if (r) {
				$.ajax({
				url:'/ajax/gi-rd-delete-report',
				type:"POST",
				data:{id:id},
				success:function(res){
				
					$(".dataTable_report").dataTable().fnDestroy();
					giUIReportDesigner.giReportGenerate($("#txtSearchReportDatatable").val());
				}	
			})
			}

		})
		newConfirm.themeConfirm("confirm2", {'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});
		
	})
	
});

function buttons(id) {  
	
	return '<a title="Edit" href="/user_view/gi-report-designer?id='+ id +'" target="_blank" class="tip cursor" data-original-title="" data-placement="top" id="" data-orgchart-id=""><i class="fa fa-pencil-square-o tip" data-placement="top" data-original-title="Edit"></i></a>' +
 			'<a title="View"  href="/user_view/gi-dashboard-viewer?id='+ id +'" target="_blank" class="tip cursor" data-original-title="" data-status="" data-placement="top" id="" data-id=""><i class="fa fa-globe tip" data-placement="top" data-original-title="Deactivate"></i></a>' +
 			'<a  title="Delete" class="cursor gi_delete_report" data-original-title="Delete Workflow"  data-placement="top" data-id="'+id+'"><i class="fa fa-trash-o tip" data-placement="top" data-original-title="Delete"></i></a>'
}
//var button = 
var giUIReportDesigner = {
	init:function(){
		this.addEventListener();
		//this.giReportGenerate();
	},
	addEventListener:function(){
		var self = this;
		$("body").on("keyup","#txtSearchReportDatatable",function(e){
			$(".dataTable_report").dataTable().fnDestroy();
			//jsonReportData['search_value'] = $(this).val();
			self.giReportGenerate($(this).val());//GetReportDataTable.defaultData();
		})
		self.giReportGenerate();
	},
	giReportGenerate:function(searchValue){
		var oTable = $(".dataTable_report").dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> '+
                       '<div class="bar1"></div> '+
                       '<div class="bar2"></div> '+
                       '<div class="bar3"></div> '+
                       '<div class="bar4"></div> '+
                       '<div class="bar5"></div> '+
                       '<div class="bar6"></div> '+
                       '<div class="bar7"></div> '+
                       '<div class="bar8"></div> '+
                       '<div class="bar9"></div> '+
                       '<div class="bar10"></div> '+
                 '</div>'
            },
            "oColReorder" : false,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "aaSorting": [[ 1, "asc" ]],
           // "bSort": true,
	    "aoColumnDefs": [ { "aTargets": [0], "bSortable": false },{ "aTargets": [1], "bSortable": true }],
            "sAjaxSource": "/ajax/gi-rd-report-list",//"/ajax/search",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "search_value", "value": searchValue});
		
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,	
                    "data": aoData,
                    "success": function(data) {		
			var reports = [];	
				$.each(data.data,function(index, report){
					var rep = [];
				
					rep.push(buttons(report.id));
					rep.push(report.name);
					rep.push(report.creator_name);
					rep.push(report.date_modified);
					rep.push(report.date_created);
					reports.push(rep);				
				})
				var sdata = {
					aaData:reports,//[[button,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5]],
					iTotalDisplayRecords: data.totalReports,//data.iTotalDisplayRecords,
					iTotalRecords: data.totalReports ,//data.length,//data.iTotalRecords,
					sEcho: data.sEcho,//data.sEcho,
					start: 0//data.start		
				}
				fnCallback(sdata);
			
                    }
                });
		/*
				console.log(aoData);
				
				data = {
							aaData: aoData,//[[button,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5],[1,2,3,4,5]],
							iTotalDisplayRecords: aoData.length,
							iTotalRecords: aoData.length,
							sEcho: 1,
							start: "0"
							
						}
						console.log(data);

				fnCallback(aoData);
				*/
            },
            "fnInitComplete": function(oSettings, json) {
                
            }
        });
        //return oTable;
	}
}