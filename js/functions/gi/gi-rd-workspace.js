$(document).ready(function(){
	ui.block()
	workspace.init();
	
});

workspace = {
	init:function(){
		setTimeout(function(){
				//ui.block()
			},0)
		//wrapFocusedObjects();
		this.addEventListner();
		this.setUI();
		this.setData();
		var reportID = $('#report_id').val()
		var $workspace = $('#workspace');
		var $giContent = $('#gi-content');
		$workspace.data('clipboard',[]);
		$workspace.data('history',[]);
		$workspace.data('historyCurrentPosition',-1);
		$workspace.data('historyLimit',20);
		$workspace.data('isTopContainer',true);
		$workspace.data('entityAttributes',[]);
		
		var jsonReport = {
			"Name":"New Report",
			"identifier_name Name":"giReport",
			"Object Type": "Report",
			"UID":"uid-giReport-" + (new Date()).getTime(),
			
			"Dashboard Department IDs":[],
			"Dashboard Group IDs":[],
			"Dashboard Position IDs":[],
			"Dashboard Published":0,
			"ID":null,
			"Objects":[{"Identifier Name":"giPage",
				"UID":"uid-giPage-" + (new Date()).getTime(),
				"Container ID":"content",
				"Name":"",
				"Ordinal":"0",
				"Page Size ID":"0",
				"Width":"816",
				"Height":"1056",
				"Margin":"8"}]
		}
		
		if(reportID != "" && !isNaN(reportID)){
			
			giController.getSavedData(reportID, function(reportData){
				
				$('#report_name').html(reportData.Name)
				giReport.Load(reportData, function(){
					$workspace.giContainer();
					$giContent.giPropertyWindow();
					ui.unblock()
				});		
			})
		}else{
			giReport.Load(jsonReport, function(){
				$workspace.giContainer()
				$giContent.giPropertyWindow();
				//ui.unblock()

			});
		}

		
		
		
	},
	
	addEventListner:function(){		
		$('body').delegate('.ui-obj-category','click',function(){
			$(this).next('.ui-obj-list').slideToggle();
		})
		
		$('body').delegate('.gi-action-history','click',function(){
			
			var clickedPos = $(this).data('history-position');
			var curPosition = giHistory.giWorkspaceData().historyCurrentPosition;
			if (curPosition > clickedPos) {
				var i = curPosition;
				while(i >clickedPos){
					giHistory.Undo();
					i--
				}
			}else{
				var i = curPosition;
				while(i < clickedPos){
					giHistory.Redo();
					i++
				}
			}
		});

		$('#gi-content').on('mousedown', '#gi-actionbar, #content' , function(event){	

			if(event.target.id == event.currentTarget.id){
				var $giContent = $('#gi-content');
				$giContent.giPropertyWindow();
			}
			
		//	event.stopPropagation();
		})
		
		$.contextMenu({
			selector:'.giObject',
			trigger: 'none',
			zIndex:1000002,	
			build:function($trigger, e){
				
				var items = $trigger.giContextMenu('setEvent', e);
				return {
					callback:function(){},
					items:items
				}
			}
		});

		// $.contextMenu({
		// 	selector:'.ui-attr-dragg-toggle',
		// 	trigger: 'right',
		// 	build:function($trigger, e){
		// 		var items = []
				
		// 		return {
		// 			callback:function(){},
		// 			items:{
		// 				DeleteItem: {
		// 					name:'Delete', 
		// 					icon:"delete",
		// 					callback:function(){
		// 						var $attribute = $trigger.parent().parent();
		// 						if($attribute.attr('data-pair-attribute') != undefined){
		// 							$attribute.closest('.ui-query-editor').find('[data-pair-attribute="'+ $attribute.attr('data-pair-attribute') +'"]').remove();
		// 						}else{
		// 							$attribute.remove();
		// 						}
								
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// })
				
		$('body').on('mousedown','.giObject',function(e){
			var $curFocus = $(this);
				
			if (!e.ctrlKey) {

				$('.giObject.focused').each(function(){
					if ($curFocus[0].id != this.id) {
						$(this).giFocus('state',false);
					}
				})
				$curFocus.giFocus();
				//console.log("focused")
				if($curFocus.data('giIdentifierName')!='giPage'){
					window.wrapperProps.el = $curFocus;
				}
				//console.log( window.wrapperProps.el)
			}else{
				
				if($curFocus.hasClass('focused')){
					$curFocus.giFocus('state',false);
				}else{
					$curFocus.giFocus();
					if($curFocus.data('giIdentifierName')!='giPage'){
						window.wrapperProps.el = $curFocus;
					}
					//console.log( window.wrapperProps.el)
				}
				// $('.giPage').giFocus('state',false);
			}
		
			if ($(e.target).hasClass('giObjectContainer')) {
				$('.giObjectContainer.focused-container').each(function(){
					$(this).removeClass('focused-container');
				})
				$(e.target).addClass('focused-container');
			}
			
			if (e.which == 3) {
				 $(this).contextMenu({x:e.clientX,y:e.clientY});
			}
			
			
			e.stopPropagation();
		})
		
		 window.onbeforeunload = function(e) {
			if($("#workspace").data('lastSavedHistoryID') != (giHistory.getCurrEvent().event==undefined?undefined:giHistory.getCurrEvent().event.id)){
				return "You've modified your report, reloading page will reset all changes.";
			}		
		}

		giEventEditors.init();
	},
	setUI:function(){
		$('.gi-scrollable').perfectScrollbar({
			wheelSpeed: 1,
			wheelPropagation: false,
			suppressScrollX: false,
			includePadding:true
		});
	},
	setData:function(){
		giGetData.allObjects(function(objects){
			//ui.block()
			giFunction.populateObjectList(objects);
			if($('#report_id').val()==""){
				ui.unblock();
			}
		});
	}
}


giFunction = {
	populateObjectList:function(objects){
		var $obj_container = $('.gi-report-objects');
		
		$.each(objects, function(index, objTypes){
			
			if ($obj_container.find('.gi' + objTypes.category + 'Category').length == 0){
				$obj_container.append('<div class="ui-obj-category gi' + objTypes.category + 'Category">'+
						      objTypes.category + '<span class="pull-right" fl-show-opt="true"><i class="fa fa-caret-down"></i></span>'+
						      '</div><div class="ui-obj-list">');
			}
			
			var objtool = $('<div class="ui-obj-item '+ (objTypes.isAvailable?"":"notAvailable") +'" data-gi-identifier-name="' + objTypes.identifier_name + '" title="'+objTypes.name+'">' +
				'<div class="ui-obj-icon"><div class="ui-obj-title">'+ objTypes.name +'</div></div>' +
			'</div>')
			
			var $drag = objtool;
			
			if (objTypes.isAvailable) {
				var $drag = objtool.draggable({
					revert:false,
					helper: function(){
						var a = $('<div></div>')
						return a.css({border:'black dashed 1px', height:(objTypes.name=="Text"?'37px':'200px'), width:'200px'})
					}, 
					cursorAt: {left:0,top:(-1)},
					opacity: 0.5,
					zIndex:10000,
					containment: '#workspace-content',
					appendTo: '#content',
					cursor:'pointer',
				});
			}
			
			
			
			$('.gi' + objTypes.category + 'Category').next().append($drag);
			
			
		
		})
	},

}

var dashboard = {
	init:function(){
		this.addEventListener();
	},
	addEventListener:function(){
		var context = this;
		giGetData.departments(function(deps){
			context.populateDepartmentList(deps);
		})
	},
	populateDepartmentList:function($deps){
		
		$.each($deps,function(index, dep){
			$(".assign-to-department").append('<option value="' + dep.id + '">'+ dep.department +'</option>');
		});
	}
}