$(document).ready(function(){
	
	
});


var giReport = {
	Save:function(callback){
		var reportData = $("#gi-content").data('giProperties');

		var ordinal = 0;
		reportData.Objects = $('.giObject').map(function(){
			var $props = $(this).data('giProperties')
			$props.Ordinal = ordinal++;
			// $(this)[$(this).data('giIdentifierName')]('setProperties', {'Ordinal':ordinal++})
			return $props;
		}).get()

		giPostData.saveReport(reportData, function(result){
			reportData.ID = result.ReportId;
			callback(result);
		})
	},
	Load:function(data, callback){
		

		var sTime = (new Date()).getTime()
		console.log("Start Time : " +  (new Date()));

		data["Identifier Name"] = "giReport";
		$('#gi-content').data('giProperties', data);

		$.each(data.Objects, function(index, object){
			var $element = $('<div></div>');
			$element.appendTo("#" + object["Container ID"]);
			$element[object['Identifier Name']]('setProperties',object);
		})


		console.log("End Time : " + (new Date()));
		console.log("Loading time : " + ((new Date()).getTime() - sTime) + " ms")
		console.log("Number of objects " + data.Objects.length);
		console.log("Average time per objects: " + (((new Date()).getTime() - sTime)/data.Objects.length) + "ms")
	
		

		if(typeof(callback) == "function"){
			callback();
		}
		
	}
}




