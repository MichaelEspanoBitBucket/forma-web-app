/* 
 *  Global Functions Only 
 *  No Trigger
 *
*/

    var pathname = window.location;
    // alert(getParametersName("print_form", pathname))
    if (getParametersName("print_form", pathname) == "true") {
        // For Annotation hiding
        $(".addAnnotation").hide()
        $("#fl-form-controlsv-li2t").hide()
        $("#wPaint").hide()
    }


var ui = {
    overlay: null,
    block: function(style) {
        var back_color = "";
        if(typeof style == "undefined"){
            //catch error

        }
        if ($('body').children(".submit-overlay").length == 0) {
            this.overlay = $('<div class="submit-overlay" style="z-index:999999;background-repeat:no-repeat;background-attachment:fixed;background-color:rgba(200,200,200,0.5);background-position:center;position:fixed;width:100%;height:100%;text-align:center;vertical-align:middle;">'
                     + '<div class="spinner load_m" style="position: absolute; top: 50%; left: 50%;"> '+ '<div class="bar1"></div> '+ '<div class="bar2"></div> '+ '<div class="bar3"></div> '+ '<div class="bar4"></div> '+ '<div class="bar5"></div> '+ '<div class="bar6"></div> '+ '<div class="bar7"></div> '+ '<div class="bar8"></div> '+ '<div class="bar9"></div> '+ '<div class="bar10"></div> '+
                 '</div>' + '</div>');
            $('body').prepend(this.overlay);
            if(typeof style != "undefined"){
                this.overlay.css(style);
            }
        }
    },
    blockPortion: function(container) {
        var back_color = "";
        if(typeof style == "undefined"){
            //catch error

        }
        if (container.children(".submit-overlay-portion").length == 0) {
            var portionOverlay = $('<div class="submit-overlay-portion" style="z-index:2;background-repeat:no-repeat;background-attachment:fixed;background-color:rgba(255, 255, 255, 0.7);background-position:center;position:absolute;top:0;left:0; height:100%; width:100%; border-radius:7px;">'+
                    '<div style="  display: table;text-align: center; height: 100%;margin: 0 auto;">'+
                    '<div style="display:table-cell;vertical-align:middle"><div class="spinner load_m" style=""> '+ '<div class="bar1"></div> '+ '<div class="bar2"></div> '+ '<div class="bar3"></div> '+ '<div class="bar4"></div> '+ '<div class="bar5"></div> '+ '<div class="bar6"></div> '+ '<div class="bar7"></div> '+ '<div class="bar8"></div> '+ '<div class="bar9"></div> '+ '<div class="bar10"></div> '+
                 '</div><div></div>');
            container.append(portionOverlay);
        }
    },
    unblock: function() {
        if(this.overlay != null){
            if (this.overlay.length >= 1) {
                this.overlay.fadeOut(function() {
                    $(this).remove();
                });
            }
        }
    },
    unblockPortion : function(container){
        container.find(".submit-overlay-portion").fadeOut(function() {
            $(this).remove();
        });
    }
}

function if_undefinded(value,else_value) {
    var ret = "";
    if (value!=undefined && value!="undefined") {
        ret = value;
    }else{
        ret = else_value;
    }
    return ret;
}

function removeFadeout(){
    $(this).not('#popup_overlay').addClass('fadeOutUp').queue(function() {  // Wait for 1 second.
            $(this).remove();
            $(".tooltip-arrow").closest(".tooltip").remove();
        });
    $('#popup_overlay').fadeOut(function(){
         $(this).remove();
         $(".tooltip-arrow").closest(".tooltip").remove();
    });

};

//from node_functions.js by michael espano 10:22 AM 10/8/2015
function setSelected(value, selectedValue){
    var ret = "";
    if(value == selectedValue){
        ret = "selected = 'selected'";
    }
    return ret;
}

//from function.js by roni pinili 2:40 PM 10/8/2015
function getParametersName(name, pathname) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(pathname);
    if (results != null) {
        return results[1];
    }

}

/*

This function is for notification on top nav
Note: This will not trigger autimatically! Except in notification page or the user clicked the notification icon in top nav


*/
{
    notifications = {
        view_notification: function() {

            $("body").on("click", ".show_notification", function() {
                $(".show_notification").popover({
                    html: true,
                    title: 'Notification',
                    placement: 'bottom',
                    //trigger: 'hov ',
                    container: '',
                    content: '<label style="color:#000;">Loading...</a>'
                }).popover('show');
                $.ajax({
                    type: "POST",
                    url: "/ajax/notification",
                    dataType: 'json',
                    data: {
                        action: "notificationPost"
                    },
                    cache: false,
                    success: function(result) {
                        //console.log(result)
                        var ret = "";
                        var notify = "";
                        if (result != null) {
                            var noti = result[0].notification;
                            var noti_like = noti['like'];
                            var noti_post = noti['post'];
                            var noti_comment = noti['comment'];
                            var noti_tasks = noti['tasks'];
                            var noti_request = noti['request'];

                            var list = [];
                            var list2 = [];
                            var la = [];
                            // For post comment
                            if (noti_post != null) {
                                for (var a = 0; a < noti_post.length; a++) {
                                    var n = noti_post[a];
                                    $("body").data("notiData_" + n.notiID_encrypt, n);
                                    ret += notiBody(n, result, "");
                                    list[n.notiID] = notiBody(n, result);
                                    //list2[n.notiID] = notiBody(n,result);
                                    list2.push({"id": n.notiID, "value": notiBody(n, result, "")});
                                    //list2.push({"a":notiBody(n,result)});
                                }
                            }

                            // For tasks
                            if (noti_tasks != null) {
                                for (var a = 0; a < noti_tasks.length; a++) {
                                    var n = noti_tasks[a];
                                    $("body").data("notiData_" + n.notiID_encrypt, n);
                                    ret += notiBody(n, result, "");
                                    list[n.notiID] = notiBody(n, result, "");
                                    //list2[n.notiID] = notiBody(n,result);
                                    list2.push({"id": n.notiID, "value": notiBody(n, result, "")});
                                }
                            }

                            // For Like
                            if (noti_like != null) {
                                for (var a = 0; a < noti_like.length; a++) {
                                    var n = noti_like[a];
                                    $("body").data("notiData_" + n.notiID_encrypt, n);
                                    ret += notiBody(n, result, "");
                                    list[n.notiID] = notiBody(n, result, "");
                                    //list2[n.notiID] = notiBody(n,result);
                                    list2.push({"id": n.notiID, "value": notiBody(n, result, "")});
                                }
                            }

                            // For request
                            if (noti_request != null) {
                                for (var a = 0; a < noti_request.length; a++) {
                                    var n = noti_request[a];
                                    $("body").data("notiData_" + n.notiID_encrypt, n);
                                    ret += notiBody(n, result, "");
                                    list[n.notiID] = notiBody(n, result, "");
                                    //list2[n.notiID] = notiBody(n,result);
                                    list2.push({"id": n.notiID, "value": notiBody(n, result, "")});
                                }
                            }



                            var gNoti = "";
                            //la.push(list2);


                            var l = list2.sort(function(a, b) {
                                return b.id - a.id;
                            });

                            //for (var i in list2) {
                            //    console.log(i)
                            //}
                            for (var a = 0; a < l.length; a++) {
                                //console.log(l[a]['id'])
                                gNoti += l[a]['value'];
                            }
                            notify = getNoty(gNoti);

                        } else {
                            var notify = "<lable style='color:#000'>No notification</label>";
                        }

                        $(".show_notification").data('popover').options.content = notify;
                        $(".show_notification").popover('show');
                        $("label.timeago").timeago();  // Time To go
                        $(".notification-container").perfectScrollbar();

                    }
                });




            });
            $("body").on("mouseleave", ".popover", function() {
                $(".show_notification").popover('hide');
            });
        },
        load_notification: function() {
            $.ajax({
                type: "POST",
                url: "/ajax/notification",
                dataType: 'json',
                data: {
                    action: "notificationPost"
                },
                cache: false,
                success: function(result) {
                    $("body").data("user_notification", result);
                }
            });
        },
        showNoti: function() {
            $("body").on("mousedown", ".showNoti", function(e) {
                var encType = $(this).attr("data-encrypt");
                var self = this;
                var data = $(".noti-only").data("notiData_" + encType);
                // var json = $("body").data();
                
                // data = json['notiData']["notiData_" + encType];
                //console.log(data)
                //console.log(data.postID_encrypt)
                //console.log(data.PostedBy)
                //console.log(data.notiID_encrypt)
                //
                $.ajax({
                    type: "POST",
                    url: "/ajax/notification",
                    data: {
                        action: "updateNoti",
                        notiID: data.notiID
                    },
                    success: function(e) {
                        //console.log(data)
                        var pathname = window.location.pathname;
                        var split_pathname = pathname.split("/");
                        var user_view = $("#user_url_view").val();
                        // if (split_pathname[2]=="notifications") {
                        var url_user = user_view;
                        // }else{
                        //     var url_user = "/";
                        // }            
                        // if (data.type == 1) {
                        //     window.location.replace(url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt);
                        // } else if (data.type == 2) {
                        //     window.location.replace(url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt);
                        // } else if (data.type == 6) {
                        //     window.location.replace(url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt);
                        // } else if (data.type == 1 || data.type == 2) {
                        //     window.location.replace(url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt);
                        // } else if (data.type == 3 || data.type == 4) {
                        //     var data_comment_id = $(self).attr("comment_id");
                        //     window.location.replace(url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt+"&comment_id="+data_comment_id);
                        // } else if (data.type == 5) {
                        //     window.location.replace(url_user + 'workspace?view_type=request&formID=' + data.formID + '&requestID=' + data.requestID);
                        // } else if (data.type == 10 || data.type == 11 || data.type == 12 || data.type == 13) {
                        //     window.location.replace(url_user + 'task?view_type=view_task&taskID=' + data.infoID);
                        // } else if(data.type == 14){
                        //     window.location.replace(url_user + 'workspace?view_type=request&formID=' + data.fID + '&requestID=' + data.postID);
                        // }
                    }
                });
            });
        },
        save_notifications: function() {
            $('.save_notifications').click(function() {

                var obj = [];
                obj.push(GetDetails());
                var json_details = JSON.stringify(obj)
                console.log(json_details)

                con = "Are you sure you want to save the changes?";
              var newConfirm = new  jConfirm(con, 'Confirmation Dialog', '', '', '', function(r) {
                    if (r == true) {
                        $.post("/ajax/settings", {action: "saveNotifications", json_details: json_details}, function(data) {
                           
                            if (data == "Updated.") {
                                showNotification({
                                    message: "Privacy Settings has been successfully updated.",
                                    type: "success",
                                    autoClose: true,
                                    duration: 3
                                });
                            } else {
                             //var newConfirm = new jConfirm("Setting was successfully updated.", "", "", "", "", "");
                            //newConfirm.themeConfirm("confirm2",{'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'});   
                            }
                            if ($("input[class='selectCheckBox_noti']:checked").length == "9") {
                                $(".selectAll_noti").attr("checked", true);
                            } else {
                                $(".selectAll_noti").attr("checked", false);
                            }
                        });
                    }
                });
             newConfirm.themeConfirm("confirm2",{'icon':'<i class="fa fa-question-circle fl-margin-right" style=" font-size:15px;"></i>'});

            });
        },
        selectOne_noti: function() {
            // Delete now the message
            $(".selectCheckBox_noti").on("click", function() {
                if (this.checked) {
                    $(this).attr('data-value', "1");
                } else {
                    $(this).attr('data-value', "0");
                }
            });

            $(".selectAll_noti").on("click", function() {
                if (this.checked) {
                    $(".selectCheckBox_noti").each(function() {
                        $(this).prop("checked", true)
                        $(this).attr('data-value', "1");
                    });
                } else {
                    $(".selectCheckBox_noti").each(function() {
                        $(this).prop("checked", false)
                        $(this).attr('data-value', "0");
                    });
                }
            });
        }
        // Allow Notifications Checkbox
    }
    function GetDetails() {
        var arr = [];
        var arr_1 = [];
        var arr_2 = [];
        var arr_3 = [];
        var arr_4 = [];
        var arr_5 = [];
        var arr_6 = [];
        var arr_json = {};
        var noti_json = {};
        var arr_count = [];
        var count_checked = $('.selectCheckBox_noti:checked').length;

        $('.selectCheckBox_noti').each(function() {
            var v_value = $(this).attr('data-value');
            var c_name = $(this).attr('data-name');
            var noti_name = $(this).attr("data-noti-name");

            if (noti_name == "Comment") {
                arr.push(v_value);
                noti_json[noti_name] = arr;
            } else if (noti_name == "Like") {
                arr_1.push(v_value);
                noti_json[noti_name] = arr_1;
            } else if (noti_name == "Tagged") {
                arr_2.push(v_value);
                noti_json[noti_name] = arr_2;
            } else if (noti_name == "Message") {
                arr_3.push(v_value);
                noti_json[noti_name] = arr_3;
            } else if (noti_name == "Request") {
                arr_4.push(v_value);
                noti_json[noti_name] = arr_4;
            }
        });
        if ($('.select_summar_noti').is(":checked") == true) {
            $summary_noti = "1";
        } else {
            $summary_noti = "0";
        }
        //$summary_notification = $('.selectAll_snoti:checked').map(function(){return}).
        arr_6.push($summary_noti);
        noti_json['summary_notification'] = arr_6;
        arr_5.push(count_checked);
        noti_json['count_checked'] = arr_5;
        arr_json['notifications'] = noti_json;
        return arr_json;
    }
    function sortmyway(data_A, data_B)
    {
        return (data_A - data_B);
    }
    //
    function notiBody(noti, result, nType) {
        var ret = "";
        var messageOnly = "";
        var countLength = result.length;

        // if (noti.type == 1) {
        //     var msg = '<a href="#">' + noti.name + '</a>&nbsp; mentioned you in a post.';
        //     messageOnly = 'mentioned you in a post.';
        //     var icon_noti = '<i class="icon-file"></i>';

        //     // Comment on post
        // } else if (noti.type == 2) {

        //     if (noti.countCommentPost == 1 && noti.postedIDBy != noti.authID && noti.notiUserID != noti.postedIDBy) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; commented on ' + noti.PostedBy + '`s post.';
        //         messageOnly = 'commented on ' + noti.PostedBy + '`s post.';
        //     } else if (noti.countCommentPost > 1 && noti.postedIDBy != noti.authID && noti.notiUserID != noti.postedIDBy) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; also commented on ' + noti.PostedBy + '`s post.';
        //         messageOnly = 'also commented on ' + noti.PostedBy + '`s post.';
        //     } else if (noti.countCommentPost == 1 && noti.postedIDBy == noti.authID && noti.notiUserID != noti.postedIDBy) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; commented on your post.';
        //         messageOnly = 'commented on your post.';
        //     } else if (noti.countCommentPost > 1 && noti.postedIDBy == noti.authID && noti.notiUserID != noti.postedIDBy) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; also commented on your post.';
        //         messageOnly = 'also commented on your post.';
        //     } else if (noti.countCommentPost > 1 && noti.name == noti.PostedBy && noti.notiUserID != noti.postedIDBy) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; also commented on your post.';
        //         messageOnly = 'also commented on your post.';
        //     } else if (noti.countCommentPost > 1 && noti.name == noti.PostedBy && noti.notiUserID == noti.postedIDBy
        //             || noti.countCommentPost == 1 && noti.name == noti.PostedBy && noti.notiUserID == noti.postedIDBy) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; also commented its post.';
        //         messageOnly = 'also commented its post.';
        //     } else {
        //         // var msg = '<a href="#">' + noti.name + '</a>&nbsp; commented "'+ noti['comment'] +'" on post.';
        //         // messageOnly = 'commented "'+ noti['comment'] +'" on post.';
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; commented on post.';
        //         messageOnly = 'commented on post.';
        //     }

        //     var icon_noti = '<i class="icon-comment"></i>';

        // } else if (noti.type == 3) {
        //     var lType = "post";
        //     var like = noti.countLike;

        //     if (like == 1 && noti.postedIDBy != noti.authID) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; like on ' + noti.PostedBy + '`s ' + lType + '.';
        //         messageOnly = 'like on ' + noti.PostedBy + '`s ' + lType + '.';
        //     } else if (like > 1 && noti.postedIDBy != noti.authID) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; also like on ' + noti.PostedBy + '`s ' + lType + '.';
        //         messageOnly = 'also like on ' + noti.PostedBy + '`s ' + lType + '.';
        //     } else if (like == 1 && noti.postedIDBy == noti.authID) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; like on your ' + lType + '.';
        //         messageOnly = 'like on your ' + lType + '.';
        //     } else if (like > 1 && noti.postedIDBy == noti.authID) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; also like on your ' + lType + '.';
        //         messageOnly = 'also like on your ' + lType + '.';
        //     } else {
        //         // var msg = '<a href="#">' + noti.name + '</a>&nbsp; like on your ' + lType +' "'+ noti.post + '".';
        //         // messageOnly = 'like on your ' + lType +' "'+ noti. + '" .';
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; like on your ' + lType;
        //         messageOnly = 'like on your ' + lType;   
        //     }

        //     var icon_noti = '<i class="icon-thumbs-up"></i>';post

        // } else if (noti.type == 4) {

        //     var lType = "comment";
        //     var like = noti.countLikeReply;

        //     if (like == 1 && noti.postedIDBy != noti.authID) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; like on ' + noti.PostedBy + '`s ' + lType + '.';
        //         messageOnly = 'like on ' + noti.PostedBy + '`s ' + lType + '.';
        //     } else if (like > 1 && noti.postedIDBy != noti.authID) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; also like on ' + noti.PostedBy + '`s ' + lType + '.';
        //         messageOnly = 'also like on ' + noti.PostedBy + '`s ' + lType + '.';
        //     } else if (like == 1 && noti.postedIDBy == noti.authID) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; like on your ' + lType + '.';
        //         messageOnly = 'like on your ' + lType + '.';
        //     } else if (like > 1 && noti.postedIDBy == noti.authID) {
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; also like on your ' + lType + '.';
        //         // messageOnly = 'also like on your ' + lType +'"'+ noti.comment + '".';
        //         messageOnly = 'also like on your ' + lType;
        //     } else {
        //         // var msg = '<a href="#">' + noti.name + '</a>&nbsp; like on your ' + lType +' "'+ noti.comment + '".';
        //         // messageOnly = 'like on your ' + lType +' "'+ noti.comment + '".';
        //         var msg = '<a href="#">' + noti.name + '</a>&nbsp; like on your ' + lType;
        //         messageOnly = 'like on your ' + lType;
        //     }
        //     var icon_noti = '<i class="icon-thumbs-up"></i>';

        //     // Request    
        // } else if (noti.type == 5) {
        //     var msg = '<a href="#">' + noti.name + '</a>&nbsp;';
        //     messageOnly = '';

        //     var icon_noti = '<i class="icon-reorder"></i>';
        // } else if (noti.type == 6) {
        //     var msg = '<a href="#">' + noti.name + '</a>&nbsp; mentioned you in a comment.';
        //     messageOnly = 'mentioned you in a comment.';

        //     var icon_noti = '<i class="icon-comment"></i>';

        // }

        // // For Task
        // else if (noti.type == 10) {
        //     var msg = '<a href="#">' + noti.name + '</a>&nbsp; added as a team in task.';
        //     messageOnly = 'added as a team in task.';

        //     var icon_noti = '<i class="icon-reorder"></i>';
        // } else if (noti.type == 11) {
        //     var msg = '<a href="#">' + noti.name + '</a>&nbsp; set you as an editor in task.';
        //     messageOnly = 'set you as an editor in task.';
        //     var icon_noti = '<i class="icon-reorder"></i>';

        // } else if (noti.type == 12) {
        //     var msg = '<a href="#">' + noti.name + '</a>&nbsp; set you as a viewer in task.';
        //     messageOnly = 'set you as a viewer in task.';

        //     var icon_noti = '<i class="icon-reorder"></i>';

        // } else if (noti.type == 13) {
        //     var msg = '<a href="#">' + noti.name + '</a>&nbsp; A task was not able to complete on its due date.';
        //     messageOnly = ' A task was not able to complete on its due date.';

        //     var icon_noti = '<i class="icon-reorder"></i>';

        // }else if (noti.type == 14) {
        //     // var msg = '<a href="#">' + noti.name + '</a>&nbsp; commented "'+ noti['comment'] +'" on request.';
        //     //     messageOnly = 'commented "'+ noti['comment'] +'" on request.';

        //     var msg = '<a href="#">' + noti.name + '</a>&nbsp; commented on request.';
        //         messageOnly = 'commented on request.';

        // }
        if (noti.type == 1) {
            var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; mentioned you in a post.';
            messageOnly = 'mentioned you in a post.';
            var icon_noti = '<i class="icon-file"></i>';

            // Comment on post
        } else if (noti.type == 2) {

            if (noti.countCommentPost == 1 && noti.postedIDBy != noti.authID && noti.notiUserID != noti.postedIDBy) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; commented on ' + noti.PostedBy + '`s post.';
                messageOnly = 'commented on ' + noti.PostedBy + '`s post.';
            } else if (noti.countCommentPost > 1 && noti.postedIDBy != noti.authID && noti.notiUserID != noti.postedIDBy) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; also commented on ' + noti.PostedBy + '`s post.';
                messageOnly = 'also commented on ' + noti.PostedBy + '`s post.';
            } else if (noti.countCommentPost == 1 && noti.postedIDBy == noti.authID && noti.notiUserID != noti.postedIDBy) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; commented on your post.';
                messageOnly = 'commented on your post.';
            } else if (noti.countCommentPost > 1 && noti.postedIDBy == noti.authID && noti.notiUserID != noti.postedIDBy) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; also commented on your post.';
                messageOnly = 'also commented on your post.';
            } else if (noti.countCommentPost > 1 && noti.name == noti.PostedBy && noti.notiUserID != noti.postedIDBy) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; also commented on your post.';
                messageOnly = 'also commented on your post.';
            } else if (noti.countCommentPost > 1 && noti.name == noti.PostedBy && noti.notiUserID == noti.postedIDBy
                    || noti.countCommentPost == 1 && noti.name == noti.PostedBy && noti.notiUserID == noti.postedIDBy) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; also commented its post.';
                messageOnly = 'also commented its post.';
            } else {
                // var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; commented "'+ noti['comment'] +'" on post.';
                // messageOnly = 'commented "'+ noti['comment'] +'" on post.';
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; commented on post.';
                messageOnly = 'commented on post.';
            }

            var icon_noti = '<i class="icon-comment"></i>';

        } else if (noti.type == 3) {
            var lType = "post";
            var like = noti.countLike;

            if (like == 1 && noti.postedIDBy != noti.authID) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; like on ' + noti.PostedBy + '`s ' + lType + '.';
                messageOnly = 'like on ' + noti.PostedBy + '`s ' + lType + '.';
            } else if (like > 1 && noti.postedIDBy != noti.authID) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; also like on ' + noti.PostedBy + '`s ' + lType + '.';
                messageOnly = 'also like on ' + noti.PostedBy + '`s ' + lType + '.';
            } else if (like == 1 && noti.postedIDBy == noti.authID) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; like on your ' + lType + '.';
                messageOnly = 'like on your ' + lType + '.';
            } else if (like > 1 && noti.postedIDBy == noti.authID) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; also like on your ' + lType + '.';
                messageOnly = 'also like on your ' + lType + '.';
            } else {
                // var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; like on your ' + lType +' "'+ noti.post + '".';
                // messageOnly = 'like on your ' + lType +' "'+ noti. + '" .';
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; like on your ' + lType;
                messageOnly = 'like on your ' + lType;   
            }

            var icon_noti = '<i class="icon-thumbs-up"></i>';//post

        } else if (noti.type == 4) {

            var lType = "comment";
            var like = noti.countLikeReply;

            if (like == 1 && noti.postedIDBy != noti.authID) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; like on ' + noti.PostedBy + '`s ' + lType + '.';
                messageOnly = 'like on ' + noti.PostedBy + '`s ' + lType + '.';
            } else if (like > 1 && noti.postedIDBy != noti.authID) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; also like on ' + noti.PostedBy + '`s ' + lType + '.';
                messageOnly = 'also like on ' + noti.PostedBy + '`s ' + lType + '.';
            } else if (like == 1 && noti.postedIDBy == noti.authID) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; like on your ' + lType + '.';
                messageOnly = 'like on your ' + lType + '.';
            } else if (like > 1 && noti.postedIDBy == noti.authID) {
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; also like on your ' + lType + '.';
                // messageOnly = 'also like on your ' + lType +'"'+ noti.comment + '".';
                messageOnly = 'also like on your ' + lType;
            } else {
                // var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; like on your ' + lType +' "'+ noti.comment + '".';
                // messageOnly = 'like on your ' + lType +' "'+ noti.comment + '".';
                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; like on your ' + lType;
                messageOnly = 'like on your ' + lType;
            }
            var icon_noti = '<i class="icon-thumbs-up"></i>';

            // Request    
        } else if (noti.type == 5) {
            var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp;';
            messageOnly = '';

            var icon_noti = '<i class="icon-reorder"></i>';
        } else if (noti.type == 6) {
            var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; mentioned you in a comment.';
            messageOnly = 'mentioned you in a comment.';

            var icon_noti = '<i class="icon-comment"></i>';

        }

        // For Task
        else if (noti.type == 10) {
            var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; added as a team in task.';
            messageOnly = 'added as a team in task.';

            var icon_noti = '<i class="icon-reorder"></i>';
        } else if (noti.type == 11) {
            var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; set you as an editor in task.';
            messageOnly = 'set you as an editor in task.';
            var icon_noti = '<i class="icon-reorder"></i>';

        } else if (noti.type == 12) {
            var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; set you as a viewer in task.';
            messageOnly = 'set you as a viewer in task.';

            var icon_noti = '<i class="icon-reorder"></i>';

        } else if (noti.type == 13) {
            var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; A task was not able to complete on its due date.';
            messageOnly = ' A task was not able to complete on its due date.';

            var icon_noti = '<i class="icon-reorder"></i>';

        }else if (noti.type == 14) {
            // var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; commented "'+ noti['comment'] +'" on request.';
            //     messageOnly = 'commented "'+ noti['comment'] +'" on request.';
            // COMMENT_NOTIFICATION
            
            if(noti['comment']){
                var comment_1 = "'"+noti['comment']+"'";var maxLength1 = 15;
                var comment_2 = "'"+noti['comment']+"'";var maxLength2 = 10;
                if(noti['comment'].length>maxLength1){
                    comment_1 = "'"+noti['comment'].substr(0,maxLength1)+"...'";
                }
                if(noti['comment'].length>maxLength2){
                    comment_2 = "'"+noti['comment'].substr(0,maxLength2)+"...'";
                }

                var msg = '<div class="noti-from-name">' + noti.name + '</div>&nbsp; commented '+ comment_1 +' on request with a Tracking Number: '+noti.TrackNo;
                messageOnly = 'commented '+ comment_2 +' on request with a Tracking Number: '+noti.TrackNo;
            }

        }

        //console.log(noti.user_view);
        //ret += '<a href="/post?view_type=view_post&postID=' + noti.postID_encrype + '" target="_blank">';
        ret += '<div style="float:left;width:100%;border-bottom: 1px solid #ddd; ' + noti.bg + '" class="showNoti" data-encrypt="' + noti.notiID_encrypt + '">';
        ret += '<div  class="padding_5">';
        ret += '<div class="pull-left" style="margin-top:5px;width: 22%;">';
        ret += '<div class="pull-left " style="width: 44px;height: 44px;">';
        ret += noti.image;
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="pull-left" style="width: 70%;margin-top:5px;margin-left: 10px;color:#000;">';
        ret += msg;
        ret += '<br>';
        ret += icon_noti + ' <label class="timeago" title="' + noti.notiDate + '" style="font-size:10px;color:#ddd;"></label>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        //ret += '</a>';

        if (nType!="") {
            if(nType=="messageOnly"){
                return messageOnly;
            }else{
                return msg;
            }
        }else{
            return ret;
        }
        
    }

    function getNoty(noti) {

        var ret = "";

        ret += '<div class="notification-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 200px;overflow: hidden;">';
        ret += noti;
        ret += '</div>';
        return ret;
    }

    var user_notification = {
        loadView: function(count_notif,json,callback) {
            var type = json['type'];
            var self = this;
            var limit = 30;
            if(type==2){
                limit = 10;
            }
            var loading = $('<div align="center" class="notif-loading"><img src="/images/icon/loading.gif"></div>');
            console.log(loading.length);
            if(type==1){
                if($('.noti-only').find(".notif-loading").length==0){
                    $('.noti-only').append(loading);
                }
                $(".fl-inmail-notifications-compose-content-wrapper").perfectScrollbar("update");
                
            }else{
                if($('.noti-fly').find(".notif-loading").length==0){
                    $('.noti-fly').append(loading);
                }
                //fly
                
                $(".fl-inmail-notifications-compose-content-wrapper").perfectScrollbar("update");
            }
            $.ajax({
                type: "POST",
                url: "/ajax/notification",
                dataType: 'json',
                data: {
                    action: "notificationPost",
                    count_notif: count_notif,
                    limit:limit
                },
                cache: false,
                success: function(result) {
                    var totalCount = result[0].notification.totalCount;

                    // console.log(result);
                    // return;
                    var comments = result[0].notification.comment;
                    var likes = result[0].notification.like;
                    var posts = result[0].notification.post;
                    var tasks = result[0].notification.tasks;
                    var requests = result[0].notification.request;
                    var data = [];
                    
                    //Comment
                    data = $.merge(notification_comments(comments,result,json),data);
                    
                    //requests
                    data = $.merge(notification_request(requests,result,json),data);
                    
                    //posts
                    data = $.merge(notification_post(posts,result,json),data);

                    //likes
                    data = $.merge(notification_likes(likes,result,json),data);

                    //sorting by date
                    data = data.sort(self.sortByDate('date'));

                    //remove inline loading
                    loading.remove();

                    //append data in the container
                    if(type==1){
                        
                        for (var index in data) {
                            $('.noti-only').append(data[index].html);
                            $("label.timeago").timeago();  // Time To go
                        }
                        //update perfect scroll bar
                        $(".fl-inmail-notifications-compose-content-wrapper, .fl-widget-wrapper-scroll").perfectScrollbar("update");

                        //will decide wheather the notification will continue to load via scrolling
                        if(totalCount==0){
                            load_notif = false;
                        }else{
                            load_notif = true;
                        }
                    }else{
                        //fly
                        for (var index in data) {
                            $('.noti-fly').append(data[index].html);
                            $("label.timeago").timeago();  // Time To go
                        }
                    }
                    callback(totalCount);
                }
            });
            
        },
        sortByDate: function(prop) {
            return function(a, b) {
                var aDate = new Date(a[prop].replace(/\-/g,"/"));
                var bDate = new Date(b[prop].replace(/\-/g,"/"));
                if (aDate > bDate) {
                    return -1;
                } else if (aDate < bDate) {
                    return 1;
                }
                return 0;
            }
        },
    };

    /*
    Additional function 
    By: aaron tolentino
    */

    //for notification likes
    function notification_likes(likes,result,json){
        var data = [];
        var type = json['type'];
        var path_user_view = $("#user_url_view").val();
        for (var index in likes) {
            if(type==1){
                var msg = notiBody(likes[index], result, "userNoti");
                likes[index]['msg'] = msg;
                html = pageNotiType(likes[index],result);
            }else{
                var msg = notiBody(likes[index], result, "messageOnly");
                likes[index]['msg'] = msg;
                likes[index]['noti_type'] = 'Like';
                likes[index]['icon'] = 'fa fa-thumbs-o-up';
                html = flyNotiYpe(likes[index],result);
            }

            data.push({
                date: likes[index].notiDate,
                html: html
            });
        };
        return data;
    }

    //for notification comment
    function notification_comments(comments,result,json){
        var path_user_view = $("#user_url_view").val();
        var type = json['type'];
        var data = [];
        for (var index in comments) {
            if(type==1){
                var msg = notiBody(comments[index], result, "userNoti");
                comments[index]['msg'] = msg;
                html = pageNotiType(comments[index],result);
            }else{
                var msg = notiBody(comments[index], result, "messageOnly");
                comments[index]['msg'] = msg;
                comments[index]['noti_type'] = 'Comment';
                comments[index]['icon'] = 'fa fa-comments-o';
                html = flyNotiYpe(comments[index],result);
            }

            data.push({
                date: comments[index].notiDate,
                html: html
            });
        }
        return data;
    }

    //for notification request
    function notification_request(requests,result,json){
        var path_user_view = $("#user_url_view").val();
        var type = json['type'];
        var data = [];
        var msg = "";
        for (var index in requests) {
            if(type==1){
                // var msg = '<a href="#">'+requests[index].Requestor_name + '</a> submitted a request with Tracking Number <b>"' +
                //             requests[index].TrackNo + '"</b> for your action.';
                // msg = notiBody(requests[index], result, "userNoti") + " with Tracking Number <b>" +
                //                  requests[index].TrackNo + '"</b>';

                msg = notiBody(requests[index], result, "userNoti") +" Request for process : "+requests[index].TrackNo;
                
                if(requests[index]['request_noti_message']!="" && requests[index]['request_noti_message']!="null" && requests[index]['request_noti_message']!=null){
                    msg = requests[index]['request_noti_message'];
                }
                requests[index]['msg'] = msg;
                html = pageNotiType(requests[index],result);
            }else{
                // var msg = 'Submitted a request with Tracking Number <b>"' +
                //             requests[index].TrackNo + '"</b> for your action.';
                // msg = notiBody(requests[index], result, "messageOnly") + " with Tracking Number <b>" +
                //                  requests[index].TrackNo + '"</b>';

                msg = notiBody(requests[index], result, "userNoti") +" Request for process : "+requests[index].TrackNo;
                if(requests[index]['request_noti_message']!="" && requests[index]['request_noti_message']!="null" && requests[index]['request_noti_message']!=null){
                    msg = requests[index]['request_noti_message'];
                }
                requests[index]['noti_type'] = 'Request';
                requests[index]['msg'] = msg;
                requests[index]['icon'] = 'fa fa-file-text-o';
                html = flyNotiYpe(requests[index],result);
            }

            data.push({
                date: requests[index].notiDate,
                html: html
            });

        }
        return data;
    }

    //for notification post
    function notification_post(posts,result,json){
        var path_user_view = $("#user_url_view").val();
        var data = [];
        var type = json['type'];
        for (var index in posts) {
            if(type==1){
                var msg = notiBody(posts[index], result, "userNoti"); // message / content
                posts[index]['msg'] = msg;
                html = pageNotiType(posts[index],result);
            }else{
                var msg = notiBody(posts[index], result, "messageOnly"); // message / content
                posts[index]['msg'] = msg;
                posts[index]['noti_type'] = 'Announcement';
                posts[index]['icon'] = 'fa fa-bullhorn';
                html = flyNotiYpe(posts[index],result);
            }
            
            data.push({
                date: posts[index].notiDate,
                html: html
            });
        }
        return data;
    }




    function pageNotiType(jsonData,result){
        $(".noti-only").data("notiData_" + jsonData.notiID_encrypt, jsonData);
        // $(".noti-fly").data("notiData_" + jsonData.notiID_encrypt, jsonData);
        // view or not view
        var exPoint = "";
        if (jsonData.bg!="") {
            exPoint = '<i class="icon-exclamation-sign fa fa-exclamation-circle fl-notification-color"></i>';
        }
        // console.log(jsonData)
        var msg = jsonData['msg'];
        var data_comment_id = "";
        if (jsonData.type==4 || jsonData.type==3) {
            data_comment_id = 'comment_id="'+ jsonData.noti_id +'"';
        }
        var html = 
                    '<li class="" >'+
                        '<div class="fl-status-and-type">'+
                            '<div class="fl-opt-type">'+
                                noti_icon(jsonData['type']) +
                                // '<i data-original-title="Public" class="fa fa-globe"></i>'+
                                // post_icon(jsonData.postPrivacyType) + 
                                // '<i data-original-title="Delete" class="fa fa-trash-o"></i>'+
                            '</div>'+
                        '</div>'+
                        '<a href="'+ setLink(jsonData) +'"> '+
                            '<div class="fl-inmail-notifications-compose-content showNoti" data-encrypt="' + jsonData.notiID_encrypt + '" '+ data_comment_id  +'>'+
                                // '<a href="#">Samuel Pulta</a> mentioned you in a comment.'+
                                exPoint +' '+
                                msg +
                            '</div>'+
                        '</a>'+
                        '<div class="fl-notification-del-time">'+
                            '<span class="time"><label class="time timeago" title="' + jsonData.notiDate + '"></label></span>'+
                        '</div>'+
                        '<div class="clearfix"></div>'+
                    '</li>';
        return html;
    }

    function flyNotiYpe(jsonData,result){
        $(".noti-fly").data("notiData_" + jsonData.notiID_encrypt, jsonData);
        var msg = jsonData['msg'];
        var data_comment_id = "";
        if (jsonData.type==4 || jsonData.type==3) {
            data_comment_id = 'comment_id="'+ jsonData.noti_id +'"';
        }
        var noti_icon = "";
        if(jsonData.icon!=undefined){
            noti_icon = jsonData.icon;
        }
        var user_view = $("#user_url_view").val();
        // if (split_pathname[2]=="notifications") {
        
            // console.log('<a href="'+ setLink(jsonData) +'">')
            var bgColor = "";
            if (jsonData.bg!="") {
                bgColor = "background-color: #E0E0E0;";
            }
        var html = '<a href="'+ setLink(jsonData) +'"> '+
                        '<div class="fl-dropflyout-wrapper showNoti" data-encrypt="' + jsonData.notiID_encrypt + '" '+ data_comment_id  +' style="cursor:pointer;'+ bgColor +'">'+
                            '<div class="fl-flyouheader">'+
                                '<strong><i class="'+ noti_icon +'"></i> '+ jsonData['noti_type']+ '</strong>'+
                                '<strong class="fl-floatRight"><label class="time timeago" title="' + jsonData.notiDate + '"></label></strong>'+
                                '<div class="clearfix"></div>'+
                            '</div>'+
                            '<strong class="fl-dropflyout-name">'+ jsonData.name +'</strong>'+
                            '<strong class="fl-dropflyout-content">'+ msg +'</strong>'+
                        '</div>'+
                    '</a>';
        return html;
    }

    function setLink(data){
        var user_view = $("#user_url_view").val();
        var url_user = user_view;
        var link  = "";
        if (data.type == 1) {
            link = url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt;
        } else if (data.type == 2) {
            link = url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt;
        } else if (data.type == 6) {
            link = url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt;
        } else if (data.type == 1 || data.type == 2) {
            link = url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt;
        } else if (data.type == 3 || data.type == 4) {
            var data_comment_id = $(self).attr("comment_id");
            link = url_user + 'post?view_type=view_post&postID=' + data.postID_encrypt+"&comment_id="+data_comment_id;
        } else if (data.type == 5) {
            link = url_user + 'workspace?view_type=request&formID=' + data.formID + '&requestID=' + data.requestID+'&trackNo='+ data.TrackNo +'';
        } else if (data.type == 10 || data.type == 11 || data.type == 12 || data.type == 13) {
            link = url_user + 'task?view_type=view_task&taskID=' + data.infoID;
        } else if(data.type == 14){
            link = url_user + 'workspace?view_type=request&formID=' + data.fID + '&requestID=' + data.postID+'&trackNo='+ data.TrackNo +'';
        }
        return link;
    }
    function post_icon(type){
        var ret = "";
        if(type==1){
            //group
            ret = '<i class="icon-lock fa fa-lock dataTip" data-original-title="Group"></i>';
        }else if(type==2){
            //department
            ret = '<i class="icon-sitemap fa fa-sitemap dataTip" data-original-title="Department"></i>';
        }else if(type==3){
            //position
            ret = '<i class="icon-user dataTip fa fa-user" data-original-title="Position"></i>';
        }else{
            //public
            ret = '<i class="icon-globe dataTip fa fa-globe" data-original-title="Public"></i>';
        }
        return ret;
    }

    function noti_icon(type){
        var ret = "";
        if(type=="5"){
            ret = '<i data-original-title="Request" class="fa fa-file-text-o" style="cursor:auto"></i>';
        }else{
            ret = '<i data-original-title="Company Announcement" class="fa fa-bullhorn" style="cursor:auto"></i>';
        }
        return ret;
    }
}

/*
* Set checked
*/

function setChecked(value, selectedValue){
    var ret = "";
    if(value == selectedValue){
        ret = "checked = 'checked'";
    }
    return ret;
}

/*
* Set checked in array
*/
function setCheckedFromArray(array,value){
    if(array.indexOf(value)>=0){
        return "checked='checked'";
    }else{
        return "";
    }
}

/*
* Suggestion Box
*/
AddSuggestion = {
    modal_suggestion : function(){
        var ret = "";
        ret += '<h3 class="fl-margin-bottom"><svg class="icon-svg icon-modal-svg" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-suggestion"></use></svg> Your Suggestion</h3>';
        ret += '<div class="hr"></div>';
                ret += '<br/>';
        ret += '<div class="section clearing fl-field-style">';
                        ret += '<div class="column div_1_of_1">';
                            ret += '<span class="isFontWeightBold">Title:</span> <font color="red">*</font>';
                            ret += '<div class="input_position">';
                ret += '<input type="text" class="suggestion-title fl-suggestion-input" maxlength="100" name="suggestion-title" placeholder="Title">';
                            ret += '</div>';
                        ret += '</div>';
        ret += '</div>';
                ret += '<div class="section clearing fl-field-style">';
                        ret += '<div class="column div_1_of_1">';
                            ret += '<span class="isFontWeightBold"> Message:</span> <font color="red">*</font>';
                            ret += '<div class="input_position">';
                                    ret += '<textarea class="form-textarea_save suggestion-message" style="height:80px;resize:none; margin-top:0px;" name="" id="" placeholder="Message" style="width:100%;"></textarea>';
                            ret += '</div>';
                        ret += '</div>';
        ret += '</div>';
        ret += '<div class="label_below2"><label class="isCursorPointer"><input type="checkbox" class="css-checkbox" id="include_suggested_by"><label for="include_suggested_by" class="css-label"></label> Check if you want to include your name</label></div>';
                ret += '<div class="label_below2"></div>';
                ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                        ret += '<img src="/images/loader/load.gif" class="display " style="margin-right:5px;margin-top:2px;margin-bottom:5px;">';
                        ret += '<input type="button" class="btn-blueBtn fl-margin-right"  id="suggest-submit" value="Save"> ';
                        ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel" style="margin-bottom:5px;">';
                ret += '</div>';
        
        var newDialog = new jDialog(ret, "","550", "250", "", function(){
            
        });
        newDialog.themeDialog("modal2");
        //bind function on click of suggestion
        this.addSuggestion();
    },
    addSuggestion : function(){
        $("#suggest-submit").click(function(){
           // jConfirm('Are you sure you want to submit your suggestion?', '', '', '', '', function(ans){
              //   if(ans==true){
                    var title = $.trim($(".suggestion-title").val());
                    var message = $.trim($(".suggestion-message").val());
                    var include_suggested_by = $("#include_suggested_by").prop("checked");
                    if(include_suggested_by==true){
                        include_suggested_by = 1;
                    }else{
                        include_suggested_by = 0;
                    }
                    if(title == "" || message == ""){
                        showNotification({
                            message: "Please fill out all required fields!",
                            type: "error",
                            autoClose: true,
                            duration: 3
                        })
                        return;
                    }
                    $.post("/ajax/suggestion",{action:"addSuggestion",
                                        title:title,message:message,include_suggested_by:include_suggested_by},function(data){
                        if(data>0){
                            showNotification({
                                message: "Suggestion Added",
                                type: "success",
                                autoClose: true,
                                duration: 3
                            })
                        }
                        $("#popup_cancel").click();
                    })
              //   }
           // });
        
        })
    },
}

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
}

UserAccessFlagCheckBox = {
    "checkedEle" : {},
    "unCheckedEle" : {},
    "originalChecked" : {},
    "init" : function(ele, eleCheckAll, container,keyElement){
       this.action(ele, eleCheckAll, container);
    },
    "action" : function(ele, eleCheckAll, container){
        var self = this;
        var keyElement;
        $("body").on("click",container+" "+ele,function(){
            var value = $(this).val();
            var key = $(this).closest(".container-formUser").attr("csid-search-type");
            keyElement = $(container).attr("keyElement")
            if($(this).prop("checked")){
                if(keyElement!=undefined){
                    self.pushMultiKeyCheckedEle(key,value,keyElement);
                }else{
                    self.pushCheckedEle(key,value);
                }
            }else{
                if(keyElement!=undefined){
                    self.pushMultiKeyUncheckedEle(key,value,keyElement);
                }else{
                    self.pushUncheckedEle(key,value);
                }
            }
        })

        $("body").on("click",container+" "+eleCheckAll,function(){
            var parent = this;
            keyElement = $(container).attr("keyElement");
            $(container+" "+ele).each(function(){
                var value = $(this).val();
                var key = $(this).closest(".container-formUser").attr("csid-search-type");
                if($(parent).prop("checked")){
                    if(keyElement!=undefined){
                        self.pushMultiKeyCheckedEle(key,value,keyElement);
                    }else{
                        self.pushCheckedEle(key,value);
                    }
                }else{
                    if(keyElement!=undefined){
                        self.pushMultiKeyUncheckedEle(key,value,keyElement);
                    }else{
                        self.pushUncheckedEle(key,value);
                    }
                }
            })
        })
    },
    pushCheckedEle : function(key,value){
        var index_arr;
        var self = this;
        // console.log(self.checkedEle)
        if(self.checkedEle[''+ key +''] == undefined){
            self.checkedEle[''+ key +''] = [];
        }

        //check if already checked in original state
        var checkedFlag = 0;
        var originalChecked = {};
        if(typeof self.originalChecked=="string"){
            try{
                originalChecked = JSON.parse(self.originalChecked);
            }catch(err){
                console.log(err)
            }
        }else{
            originalChecked = self.originalChecked;
        }

        console.log(originalChecked)

        if(originalChecked[''+ key +'']!=undefined){
            if(originalChecked[''+ key +''].indexOf(value)==-1){
                checkedFlag++
            }
        }else{
            //walang laman
            checkedFlag++;
        }


        if(checkedFlag>0){
            self.checkedEle[''+ key +''].push(value);
        }

        //remove to unchecked
        if(self.unCheckedEle[''+ key +'']){
            index_arr = self.unCheckedEle[''+ key +''].indexOf(value);
            if(index_arr>=0){
                self.unCheckedEle[''+ key +''].splice(index_arr,1);
            }
        }
    },
    pushUncheckedEle : function(key,value){
        var index_arr;
        var self = this;
        if(self.unCheckedEle[''+ key +''] == undefined){
            self.unCheckedEle[''+ key +''] = [];
        }
        //check if unchecked in original state
        var uncheckedFlag = 0;
        var originalChecked = {};
        if(typeof self.originalChecked=="string"){
            try{
                originalChecked = JSON.parse(self.originalChecked);
            }catch(err){
                console.log(err)
            }
        }else{
            originalChecked = self.originalChecked;
        }
        if(originalChecked[''+ key +'']!=undefined){
            if(originalChecked[''+ key +''].indexOf(value)>=0){
                uncheckedFlag++;
            }
        }

        if(uncheckedFlag>0){
            self.unCheckedEle[''+ key +''].push(value);
        }
        
        //remove to checked
        if(self.checkedEle[''+ key +'']){
            index_arr = self.checkedEle[''+ key +''].indexOf(value);
            if(index_arr>=0){
                self.checkedEle[''+ key +''].splice(index_arr,1);
            }
        }
    },
    pushMultiKeyCheckedEle : function(key,value,keyElement){
        var index_arr;
        var self = this;
        // console.log(self.checkedEle)
        if(self.checkedEle[''+ keyElement +'']==undefined){
            self.checkedEle[''+ keyElement +''] = {};
        }


        if(self.checkedEle[''+ keyElement +''][''+ key +''] == undefined){
            self.checkedEle[''+ keyElement +''][''+ key +''] = [];
        }

        //check if already checked in original state
        var checkedFlag = 0;
        var originalChecked = {};
        if(typeof self.originalChecked[''+ keyElement +'']=="string"){
            try{
                originalChecked = JSON.parse(self.originalChecked[''+ keyElement +'']);
            }catch(err){
                console.log(err)
            }
        }else{
            originalChecked = self.originalChecked[''+ keyElement +''];
        }

        console.log(originalChecked)

        if(originalChecked[''+ key +'']!=undefined){
            if(originalChecked[''+ key +''].indexOf(value)==-1){
                checkedFlag++
            }
        }else{
            //walang laman
            checkedFlag++;
        }


        if(checkedFlag>0){
            self.checkedEle[''+ keyElement +''][''+ key +''].push(value);
        }

        //remove to unchecked
        if(self.unCheckedEle[''+ keyElement +'']){
            if(self.unCheckedEle[''+ keyElement +''][''+ key +'']){
                index_arr = self.unCheckedEle[''+ keyElement +''][''+ key +''].indexOf(value);
                if(index_arr>=0){
                    self.unCheckedEle[''+ keyElement +''][''+ key +''].splice(index_arr,1);
                }
            }
        }
    },
    pushMultiKeyUncheckedEle : function(key,value,keyElement){
        var index_arr;
        var self = this;

        if(self.unCheckedEle[''+ keyElement +'']==undefined){
            self.unCheckedEle[''+ keyElement +''] = {};
        }

        if(self.unCheckedEle[''+ keyElement +''][''+ key +''] == undefined){
            self.unCheckedEle[''+ keyElement +''][''+ key +''] = [];
        }
        //check if unchecked in original state
        var uncheckedFlag = 0;
        var originalChecked = {};
        if(typeof self.originalChecked[''+ keyElement +'']=="string"){
            try{
                originalChecked = JSON.parse(self.originalChecked[''+ keyElement +'']);
            }catch(err){
                console.log(err)
            }
        }else{
            originalChecked = self.originalChecked[''+ keyElement +''];
        }

        if(originalChecked[''+ key +'']!=undefined){
            if(originalChecked[''+ key +''].indexOf(value)>=0){
                uncheckedFlag++;
            }
        }

        if(uncheckedFlag>0){
            self.unCheckedEle[''+ keyElement +''][''+ key +''].push(value);
        }
        
        //remove to checked
        if(self.checkedEle[''+ keyElement +'']){
            if(self.checkedEle[''+ keyElement +''][''+ key +'']){
                index_arr = self.checkedEle[''+ keyElement +''][''+ key +''].indexOf(value);
                if(index_arr>=0){
                    self.checkedEle[''+ keyElement +''][''+ key +''].splice(index_arr,1);
                }
            }
        }
    }
}
function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}
/*
 * functions
 * limitText(attribute,num)
 * @attribute - either class or ID /name
 * @num - number of string to show
 */
function limitText(attribute, num) {
    $(attribute).text(function(index, text) {
        if (num >= text.length) {
            return text.substr(0, num);
        } else {
            return text.substr(0, num) + " ...";
        }
    });
}

function CheckUrl(url_val){
    var url_pattern_checker = /^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
    return url_pattern_checker.test(url_val);
}

function MakePlusPlusRowV2(param_option_settings){

    var option_settings = {
        "tableElement":"",
        "rules":[
            {
                "eleMinus":"",
                "eleMinusTarget":"",
                "eleMinusTargetFn":"",
                "eleMinusTargetFnBefore":"",
                "eleMinusTargetFnAfter":"",

                "elePlus":"",
                "elePlusTarget":"",
                "elePlusTargetFn":"",
                "elePlusTargetFnBefore":"",
                "elePlusTargetFnAfter":""
            }
        ]
    }
    //EXAMPLE
    // MakePlusPlusRowV2({
    //     "tableElement":".fl-tbl-embed-source-form",
    //     "rules":[
    //         {
    //             "eleMinus":"tbody tr:eq(0) .fa.fa-minus.fl-addRemoveItem",
    //             "eleMinusTarget":"tbody",
    //             "elePlus":"tbody tr:eq(0) .fa.fa-plus.fl-addRemoveItem",
    //             "elePlusTarget":"tbody",
    //             "elePlusTargetFn":function(){
    //                 var self_ele = $(this);
    //                 self_ele.find('tr:not(:eq(1)) .fa.fa-minus.fl-addRemoveItem').show();
    //                 self_ele.find('tr:not(:eq(1)) .fl-embed-filter-group-and-or').show();
    //             }
    //         },
    //         {
    //             "eleMinus":"tbody tr:eq(1) .fa.fa-minus.fl-addRemoveItem",
    //             "eleMinusTarget":"tr",
    //             "elePlus":"tbody tr:eq(1) .fa.fa-plus.fl-addRemoveItem",
    //             "elePlusTarget":"tr",
    //             "elePlusTargetFn":function(){
    //                 var self_ele = $(this);
    //                 self_ele.find('.fa.fa-minus.fl-addRemoveItem').show();
    //                 self_ele.find('.fl-embed-filter-condition-and-or').show();
    //             }
    //         }
    //     ]
    // });



    $.extend(option_settings, param_option_settings)
    
    var mppr_event = {
        "eventRowAdd":function(e){
            var data = Array.prototype.slice.call(arguments);
            delete data[0];
            data = data.filter(Boolean);
            
            
            var opt_set = e['data']['this_rule'];
            var parent_to_duplicate = opt_set["elePlusTarget"];
            var element_minus = opt_set["eleMinus"];
            var dis_ele_click = $(this);//the plus
            var parent_to_duplicate_selected = dis_ele_click.parents(parent_to_duplicate);
            var cloned_to_duplicate = parent_to_duplicate_selected.eq(0).clone(true);
            var obj_data = {};
            var callbackBefore;
            //hide the  changelist and computed
            cloned_to_duplicate.find('.ui-tabs-nav').css('height', '0px');
            cloned_to_duplicate.find('[content-selector-id="flist-tabs-1"]').show();
            cloned_to_duplicate.find('[content-selector-id="flist-tabs-2"]').hide();
            if($.type(opt_set['elePlusTargetFnBefore']) == 'function'){
                obj_data = $.extend({
                    "prev_ele":$(parent_to_duplicate_selected.eq(0)).prev(),
                    "next_ele":$(parent_to_duplicate_selected.eq(0)).next(),
                    "parent_ele":$(parent_to_duplicate_selected.eq(0)).parent()
                },{"parameter":data}); 
                callbackBefore = opt_set['elePlusTargetFnBefore'].call(cloned_to_duplicate,e,obj_data);
            }
            
            if(callbackBefore==false){
                return false;
            }

            dis_ele_click.parents(parent_to_duplicate).after(cloned_to_duplicate);
            
            obj_data = $.extend({
                "prev_ele":$(cloned_to_duplicate.eq(0)).prev(),
                "next_ele":$(cloned_to_duplicate.eq(0)).next(),
                "parent_ele":$(cloned_to_duplicate.eq(0)).parent()
            },{"parameter":data});

            if($.type(opt_set['elePlusTargetFnAfter']) == 'function'){
                opt_set['elePlusTargetFnAfter'].call(cloned_to_duplicate,e,obj_data);
            }
            if($.type(opt_set['elePlusTargetFn']) == 'function'){
                opt_set['elePlusTargetFn'].call(cloned_to_duplicate,e,obj_data);
            }
        },
        "eventRowSubtract":function(e,data){
            var opt_set = e['data']['this_rule'];
            var table_element = e['data']['table_element'];
            var parent_to_duplicate = opt_set["eleMinusTarget"];
            var dis_ele_click = $(this);//the minus
            var parent_to_duplicate_selected = dis_ele_click.parents(parent_to_duplicate);
            var previous_element = $(parent_to_duplicate_selected.eq(0)).prev();
            var next_element = $(parent_to_duplicate_selected.eq(0)).next();    
            var parent_element = $(parent_to_duplicate_selected.eq(0)).parent();
            var obj_data = $.extend({
                    "prev_ele":previous_element,
                    "next_ele":next_element,
                    "parent_ele":parent_element
            },data);
            var callbackBefore;
            if($.type(opt_set['eleMinusTargetFnBefore']) == 'function'){
                callbackBefore = opt_set['eleMinusTargetFnBefore'].call(dis_ele_click.parents(parent_to_duplicate),e,obj_data);
            }
            if(callbackBefore==false){
                return false;
            }
            dis_ele_click.parents(parent_to_duplicate).remove();
            if($.type(opt_set['eleMinusTargetFnAfter']) == 'function'){
                opt_set['eleMinusTargetFnAfter'].call(table_element,e,obj_data);
            }
            if($.type(opt_set['eleMinusTargetFn']) == 'function'){
                opt_set['eleMinusTargetFn'].call(table_element,e,obj_data);
            }
        }
    }

    option_settings["init"] = function(){
        var self = this;
        var table_container = $(self["tableElement"]);

        var fn_rules = self['rules'];
        var table_container_ele_plus = "";//table_container.find(self["selectorElePlus"])
        var table_container_ele_minus = "";//table_container.find(self["selectorEleMinus"])
        for(var ctr_ii in fn_rules){
            table_container_ele_plus = table_container.find(fn_rules[ctr_ii]["elePlus"]);
            table_container_ele_minus = table_container.find(fn_rules[ctr_ii]["eleMinus"]);
            table_container_ele_plus.on("click",{"table_element":table_container,"this_rule":fn_rules[ctr_ii]},mppr_event["eventRowAdd"]);
            table_container_ele_minus.on("click",{"table_element":table_container,"this_rule":fn_rules[ctr_ii]},mppr_event["eventRowSubtract"]);
        }
    }
    
    option_settings.init();
}


$.fn.exists = function(){return this.length>0;}

function unsavedEditConfirmation(conf){

    if(conf){
        localStorage['unsavedEdits'] = 1;
    }else{
        if($.type(localStorage['unsavedEdits'])!='undefined'){
            
            localStorage.removeItem('unsavedEdits');
        }
       
    }
}
    
function bind_onBeforeOnload(status) {
    // return;
    if (status == 0) {
        window.onbeforeunload = function() {
            // blank function do nothing
        }
    } else {
        window.onbeforeunload = function(e) {
            return 'You will lose any unsaved edits';
        };
    }
}

function brokenUserImage() {
    $("img").error(function() {
        $(this).unbind("error").attr("src", "/js/functions/create_flowchart/adminnodes/avatardefault.jpg");
    });
}
//check all
CheckProperties = {
    checkAll: function(obj, container) {
        var self = $(obj);
        if (self.prop("checked") == true) {
            self.closest(container).find("[type='checkbox']:not(:disabled)").prop("checked", true);
        } else {
            self.closest(container).find("[type='checkbox']").prop("checked", false);
        }
    },
    setCheckAllChecked: function(obj, container, checkAll) {
        dialogContainer = $(container);
        totalCheckBox = dialogContainer.find(obj).length;
        totalCheckBoxChecked = dialogContainer.find(obj + ":checked").length;
        // console.log(dialogContainer.find(obj))
        // console.log(totalCheckBox + " " + totalCheckBoxChecked);
        if (totalCheckBox == totalCheckBoxChecked) {
            dialogContainer.find(checkAll).attr("checked", true);
        } else {
            dialogContainer.find(checkAll).attr("checked", false);
        }
    },
    bindOnClickCheckBox: function(obj, dialogContainer, checkAll) {
        self = this;
        $(dialogContainer).find(obj).on({
            "click": function(ee) {
                var obj = "." + $(this).attr("class").split(" ")[0];
                self.setCheckAllChecked(obj, dialogContainer, checkAll);
            }
        })
    }
}



/*
*   functions in prototyp.js
*/

String.prototype.isEmpty = function() {
    if (this == '') {
        return true;
    } else {
        return false;
    }
};

String.prototype.isStrictEmpty = function() {
    if ($.trim(this) == '') {
        return true;
    } else {
        return false;
    }
};

String.prototype.equalsIgnoreCase = function(val) {
    if (this.toLowerCase() == val.toLowerCase()) {
        return true;
    } else {
        return false;
    }
};

String.prototype.equals = function(val) {
    if (this == val) {
        return true;
    } else {
        return false;
    }
};

String.prototype.contains = function(val) {
    if (this.indexOf(val) > -1) {
        return true;
    } else {
        return false;
    }
};
//auto static all middleware
function MiddlewareToStatic(){
    $('[default-type="computed"]').each(function(){
        var self = $(this);
        var set_obj = self.closest(".setObject[data-object-id]");
        var doi = set_obj.attr("data-object-id");

        set_obj.find('.getFields_'+doi).attr("default-type","static");
        if($('body').data(doi)){
            if($('body').data(doi)["defaultValue"]){
                $('body').data(doi)["defaultValue"] = "";
            }
            if($('body').data(doi)["defaultValueType"]){
                $('body').data(doi)["defaultValueType"] = "static";
            }
        }
    });
}
//auto static all computed
function ComputedToStatic(){
    $('[default-type="computed"]').each(function(){
        var self = $(this);
        var set_obj = self.closest(".setObject[data-object-id]");
        var doi = set_obj.attr("data-object-id");
        var field_obj = set_obj.find('.getFields_'+doi);
        field_obj.data("previousData","computed");
        field_obj.attr("default-type","static");
        if($('body').data(doi)){
            if($('body').data(doi)["defaultValue"]){
                $('body').data(doi)["defaultValue"] = "";
            }
            if($('body').data(doi)["defaultValueType"]){
                $('body').data(doi)["defaultValueType"] = "static";
            }
            if( field_obj.is('select') && $('body').data(doi)["dynamicListing"] ){
                $('body').data(doi)["dynamicListing"] = "";
            }
        }

    });
}
function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

Element.prototype.remove = function() {
    return false; //conflicting scrollbar walang umaamin kung sino ang may gawa 
    this.parentElement.removeChild(this); 
}

NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        if(this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

var panel = {
    load:function(selector_div, option){
    
        var default_settings = {
            "bgColor":"rgba(250,250,250,0.2)"
        }
        if($.type(option) == "object"){
            $.extend(default_settings, option);
        }
        
        var div_ele = $(selector_div);
        
        var ele_prep = $(
            '<div class="container-level-loading-filter" style="position:absolute;width:100%;height:100%; display:table">\
                <div style="display:table-cell; vertical-align:middle;text-align:center;">\
                    <div style="display:inline-block">\
                        <div class="spinner">\
                               <div class="bar1"></div>\
                               <div class="bar2"></div>\
                               <div class="bar3"></div>\
                               <div class="bar4"></div>\
                               <div class="bar5"></div>\
                               <div class="bar6"></div>\
                              <div class="bar7"></div>\
                              <div class="bar8"></div>\
                              <div class="bar9"></div>\
                              <div class="bar10"></div>\
                         </div>\
                     <div>\
                <div style="display:table">\
            </div>'
        );
        div_ele.prepend(ele_prep);
        
        if(default_settings.bgColor){
            ele_prep.css("background-color",default_settings.bgColor);
        }
        
        if(default_settings.callBack){
            if($.type(default_settings.callBack) == "function"){
                default_settings.callBack.call(ele_prep,div_ele);
            }else if($.type(default_settings.callBack) == "array"){
                for(var ctr_i in default_settings.callBack){
                    if($.type(default_settings.callBack[ctr_i]) == "function"){
                        default_settings.callBack[ctr_i].call(ele_prep,div_ele);
                    }
                }
            }
        }
        
        return ele_prep;
    }
}

FieldCondition = {
    updatedFieldOperator: function(obj,operator,value,container){
        
        var field = $(obj).children("option").filter(":selected");
        // console.log(field)
        if((field.attr("field_input_type")=="Currency" || (field.attr("field_input_type")=="Number")) || (field.attr("data-type")=="datepicker" || field.attr("type")=="dateTime")){
            $(obj).closest(container).find($(".number_operator")).removeAttr("disabled");
            
            if(field.attr("field_input_type")=="Currency" || field.attr("field_input_type")=="Number"){
                $(obj).closest(container).find(value).attr("number-value","true")
            }else{
                $(obj).closest(container).find(value).removeAttr("number-value")
            }
        }else{
            $(obj).closest(container).find($(".number_operator")).attr("disabled","disabled");
            $(obj).closest(container).find(value).removeAttr("number-value")
        }
        // $(".search-request-operator").val("=");
    }
}

function NumToWord(num,format,currency){

    //equivalent words
    //  var th = ['hundred', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion'];
    //  var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
    //  var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
    //  var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
    //  var dw = ['tenths', 'hundredths', 'thousandths', 'ten-thousandths', 'hundred-thousandths', 'millionths', 'ten-millionths', 'hundred-millionths', 'billionths', 'ten-billionths', 'hundred-billionths', 'trillionths', 'ten-trillionths', 'hundred-trillionths', 'quadrillionths', 'ten-quadrillionths', 'hundred-quadrillionths'];

    //     var num_string = parseFloat(num).toString();
    //     var array_form = num_string.split('.');
    //     var array_for_whole = [];
    //     var array_for_decimal = [];
    //     var array_form_len = array_form.length;
        
    //      for(var i=0;i<=array_form_len-1;i++){
    //          //para sa whole numbers
    //          if(i == 0){
    //              var whole_number = array_form[0];
    //              var n = whole_number.length;
    //              for(var toarray = 0 ; toarray <= n-1 ; toarray++){
    //                  array_for_whole.push(whole_number[toarray]);
    //              }
    
    //          }
    //          //para sa decimals
    //          if(i == 1){
    //              var decimals = array_form[1];
    //              var n = decimals.length;

    //          }
    //      }
     
    // return array_for_whole;

//          }
//      }
 
// return array_for_whole;

 var ConvertHere = function (s) {
    s = s.replace(/[\, ]/g, '');
    var reg_exp_dec = new RegExp('\\+\\d*', 'g');
    var str_e_digit = s.match(reg_exp_dec);
    var e_digit = 0;
    if (str_e_digit != null) {
        str_e_digit = str_e_digit.filter(Boolean);

        e_digit = Number(str_e_digit[0]);
    }
    // console.log("CONVERT DIGITS",e_digit,str_e_digit, typeof str_e_digit);

    if (s != parseFloat(s))
        return 'not a number';

    var x = s.indexOf('.');
    if (x == -1)
        x = s.length;
    if (x > 20 || e_digit > 20)
        return 'too big';
    var n = s.split('');
    var str = '';
    var sk = 0;

    for (var i = 0; i < x; i++) {

        if ((x - i) % 3 == 2) {

        if (n[i] == '1') {

            str += tn[Number(n[i + 1])] + ' ';
            i++;
            sk = 1;
        } else if (n[i] != 0) {
            str += tw[n[i] - 2] + ' ';
            sk = 1;
        }
        } else if (n[i] != 0) {

        str += dg[n[i]] + ' ';

        if ((x - i) % 3 == 0)
            str += 'hundred ';

        sk = 1;
        }

        if ((x - i) % 3 == 1) {
        if (sk)
            str += th[(x - i - 1) / 3] + ' ';
        sk = 0;
        }
    }
    if (x != s.length) {
        var y = s.length;
        str += 'and ';
        for (var i = x + 1; i < y; i++)
        str += dg[n[i]] + ' ';
    }
    return str.replace(/\s+/g, ' ');
    }
    if (typeof num !== "undefined" || num != null || num != "") {
    if (typeof num === 'string') {
        var with_comma = num.match(/[\,]/g);
        if (with_comma != null) {
        with_comma = with_comma.filter(Boolean);
        if (with_comma) {
            if (with_comma.length >= 1) {
            num = num.replace(/[\,]/g, "");
            }
        }
        }
    }
    }
    if (!isNaN(Number(num))) {

        var s = parseFloat(num).toString();
        // Convert numbers to words
        // copyright 25th July 2006, by Stephen Chapman http://javascript.about.com
        // permission to use this Javascript on your web page is granted
        // provided that all of the code (including this copyright notice) is
        // used exactly as shown (you can change the numbering system if you wish)

        // American Numbering System
        var th = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion'];
        // uncomment this line for English Number System
        // var th = ['','thousand','million','milliard','billion'];

        var dg = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
        var tn = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
        var tw = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

        var dw = ['tenths', 'hundredths', 'thousandths', 'ten-thousandths', 'hundred-thousandths', 'millionths', 'ten-millionths', 'hundred-millionths', 'billionths', 'ten-billionths', 'hundred-billionths', 'trillionths', 'ten-trillionths', 'hundred-trillionths', 'quadrillionths', 'ten-quadrillionths', 'hundred-quadrillionths'];

        var w_point = s.match(/\./g);
        var collect_s = "";
        if (w_point != null && typeof w_point !== "undefined") {

        w_point = w_point.filter(Boolean);

        if (w_point.length == 1) {
        var w_point_s = s.split(/\./);
        
        s = w_point_s[0];
        collect_s += ConvertHere(s);

        if($.trim(format) !== "undefined" && $.trim(format) !== "" && $.trim(currency) !== "undefined" && $.trim(currency) !== ""){
            if($.trim(format)=="currency"){
                collect_s += ''+currency+" ";
                
            }
            
        }
        collect_s += "and ";
        s = w_point_s[1];
        
            

        
        if (typeof dw[s.length - 1] !== "undefined") {

            if($.trim(format) !== "undefined" && $.trim(format) !== "" && $.trim(currency) !== "undefined" && $.trim(currency) !== ""){
                    if($.trim(format)=="check"){
                        if(s.length == 1){
                            s=s*10;
                            s=s.toString();
                            
                        }
                        collect_s += s +"/100";
                        collect_s+= ' '+currency;
                    }
                    else if($.trim(format)=="currency"){
                        if(s.length == 1){
                            s=s*10;
                            s=s.toString();
                            
                        }
                        collect_s += ConvertHere(s);
                        collect_s+= ''+CheckFormat(currency);   
                    }
                    else{
                        collect_s += ConvertHere(s);
                        collect_s += "" + dw[s.length - 1]; 
                    }
                    
                    
                            
            }
            else{
                    collect_s += ConvertHere(s);
                    collect_s += "" + dw[s.length - 1];             
            }
        }
        return collect_s.charAt(0).toUpperCase()+collect_s.slice(1);

        } else if (w_point.length >= 2) {
        return 'Invalid number!';
        } else {

        //dito na ung my fifthy
        return ConvertHere(s).charAt(0).toUpperCase()+ConvertHere.slice(1);
        }
    } else {
        if($.trim(format) !== "undefined" && $.trim(format) !== "" && num !="" && $.trim(currency) !== "undefined" && $.trim(currency) !== ""){
            
                
            if($.trim(format)=="check" || $.trim(format)=="currency"){
                collect_s += ConvertHere(s);
                collect_s += ''+currency;
                
            }
            else{
                collect_s += ConvertHere(s);
            }
            
        }
        else{
            collect_s += ConvertHere(s);    
        }
        
        return collect_s.charAt(0).toUpperCase()+collect_s.slice(1);
    }
    } else {
    return 'Not a number!';
    }
}

function remove(arr, item) {
    for (var i = arr.length; i--; ) {
        if (arr[i] === item) {
            arr.splice(i, 1);
        }
    }
}

function brokenImage() {
    $("img").error(function() {
        $(this).unbind("error").attr("src", "/images/error/broken.png");
    });
}

{//util functions
    function AsyncLoop(param1, callBack, callBack2) { // (index, value) //if object (key, value, index)
        var data_array_obj = param1;
        if ($.type(data_array_obj) == "array") {
            var array_len = data_array_obj.length;
            var counter = 0;
            var break_value = "default_data";
            if (array_len == 0) {
                return data_array_obj;
            }
            var loop_async = setInterval(function () {
                if ($.type(callBack) == "function") {
                    break_value = callBack.call(data_array_obj[counter], counter, data_array_obj[counter]);
                }
                counter++;
                if (array_len == counter || break_value == false) {
                    if ($.type(callBack2) == "function") {
                        callBack2.call(data_array_obj);
                    }
                    clearInterval(loop_async);
                }
            }, 0);
        } else if ($.type(data_array_obj) == "object") {
            var data_array_obj_temp = data_array_obj;
            if ($.type(jQuery) == "function") {
                if (data_array_obj_temp instanceof jQuery) {
                    var data_array_obj_temp = data_array_obj_temp.get().map(function (val_ele, ii) {
                        return $(val_ele);
                    });
                }
            }
            var object_keys = Object.keys(data_array_obj_temp);
            var array_len = object_keys.length;
            var counter = 0;
            var break_value = "default_data";
            if (array_len == 0) {
                return data_array_obj_temp;
            }
            var loop_async = setInterval(function () {
                if ($.type(callBack) == "function") {
                    break_value = callBack.call(data_array_obj_temp[ object_keys[counter] ], object_keys[counter], data_array_obj_temp[ object_keys[counter] ], counter);
                }
                counter++;
                if (array_len == counter || break_value == false) {
                    if ($.type(callBack2) == "function") {
                        callBack2.call(data_array_obj_temp);
                    }
                    clearInterval(loop_async);
                }
            }, 0);
        }
    }
    function SetMyTimer(callback, time) {
        this.setTimeout(callback, time);
    }

    SetMyTimer.prototype.setTimeout = function (callback, time) {
        var self = this;
        if (this.timer) {
            clearTimeout(this.timer);
        }
        this.finished = false;
        this.callback = callback;
        this.time = time;
        this.timer = setTimeout(function () {
            self.finished = true;
            callback();
        }, time);
        this.start = Date.now();
    }

    SetMyTimer.prototype.add = function (time) {
        if (!this.finished) {
            // add time to time left
            time = this.time - (Date.now() - this.start) + time;
            this.setTimeout(this.callback, time);
        }
    }

    SetMyTimer.prototype.resetTimeout = function (time) {
        if (!this.finished) {
            this.setTimeout(this.callback, time);
        }
    }
    SetMyTimer.prototype.resetTimeoutV2 = function (time) {
        // if (!this.finished) {
        var self = this;
        var ung_function = self.callback;
        self.setTimeout(function () {
            if ($.type(ung_function) == "function") {
                ung_function();
            }
        }, time);
        // }
    }
    var console_time = {
        enable_console_time: true,
        collect_time: {},
        "start": function (name) {
            var isSafari = navigator.vendor.indexOf("Apple") == 0 && /\sSafari\//.test(navigator.userAgent); // true or false
            if (isSafari) {
                return '';

            }
            ;

            var self = this;
            if (self['enable_console_time']) {

                console.time(name);
                self['collect_time']['' + name + ''] = {};
                self['collect_time']['' + name + '']['start_stamp'] = window.performance.now();
            }
        },
        "end": function (name) {
            var isSafari = navigator.vendor.indexOf("Apple") == 0 && /\sSafari\//.test(navigator.userAgent); // true or false
            if (isSafari) {
                return '';
            }
            ;

            var self = this;
            if (self['enable_console_time']) {
                console.timeEnd(name);
                self['collect_time']['' + name + '']['end_stamp'] = window.performance.now();
                self['collect_time']['' + name + '']['result_stamp'] = self['collect_time']['' + name + '']['end_stamp'] - self['collect_time']['' + name + '']['start_stamp'];
            }
        },
        "getLogTimeDetails": function (option) {
            var self_dis = this;
            var settings = {
                "sort": "desc",
                "sortKey": "result_stamp"
            }
            $.extend(settings, option);
            var result_process = null;
            result_process = Object.keys(self_dis['collect_time']) //get keys

            if (settings['containsKey']) {
                var str_key_contains = settings['containsKey'];
                result_process = result_process.filter(function (val, ii) {
                    return val.indexOf(str_key_contains) >= 0;
                }) //get the 2nd loop only
            } else if (settings['containsExactKey']) {
                var str_key_contains = settings['containsExactKey'];
                result_process = result_process.filter(function (val, ii) {
                    var temp_re = new RegExp("(^|[^a-zA-Z0-9_])" + str_key_contains + "(?![a-zA-Z0-9_])", "g");
                    var temp_match_res = val.match(temp_re);
                    temp_match_res = (temp_match_res == null) ? [] : temp_match_res;
                    temp_match_res = temp_match_res //filtering mapping
                            /*.map(function(a,b){return a.replace(/[^a-zA-Z0-9_]/g, "");})*/
                            .filter(Boolean);

                    return temp_match_res.length >= 1;
                }); //get the specified key only
            }

            result_process = result_process.map(function (key_i, arr_i) {
                return {"name": key_i, "timeres": self_dis['collect_time'][key_i]};
            }) //parse as readable info

            if (settings['sort'] && settings['sortKey']) { // asc desc
                if (settings['sort'] == "desc") {
                    result_process = result_process.sort(function (a, b) {
                        return b['timeres'][settings['sortKey']] - a['timeres'][settings['sortKey']];
                    }) //sort info based on what is the long time
                } else if (settings['sort'] == "asc") {
                    result_process = result_process.sort(function (a, b) {
                        return a['timeres'][settings['sortKey']] - b['timeres'][settings['sortKey']];
                    }) //sort info based on what is the long time
                }
            }
            if (settings['displayDataOnly']) {//true false
                if (settings['getTimeResult']) { // result key start_stamp, end_stamp, result_stamp
                    var str_get_key = settings['getTimeResult'];
                    result_process = result_process.map(function (val_i, arr_i) {
                        return val_i['timeres'][str_get_key];
                    }); //get all result time only
                } else {
                    result_process = result_process.map(function (val_i, arr_i) {
                        return val_i['timeres'];
                    });
                }
            } else {
                if (settings['getTimeResult']) {
                    var str_get_key = settings['getTimeResult'];
                    result_process = result_process.map(function (val_i, arr_i) {
                        return {"name": val_i['name'], "result": val_i['timeres'][str_get_key]};
                    }); //get all result time only
                } else {
                    result_process = result_process.map(function (val_i, arr_i) {
                        return {"name": val_i['name'], "result": val_i['timeres']};
                    });
                }
            }

            return result_process;
        }
    }
    //usage
    // console_time.getLogTimeDetails({
    //  "containsKey":"CHECKPOINT2",
    //  "getTimeResult":"result_stamp",
    //  "displayDataOnly":true
    // });
    // console_time.getLogTimeDetails({
    //  "containsExactKey":"test",
    //  "getTimeResult":"result_stamp",
    //  "displayDataOnly":false
    // });
}


///moved by japhet 11-05-2015
function deciNumFunction(element){
    /*Accepts decimal point and numbers only*/
    $("body").on("keypress",element,function(e){
        
        var isNumericChar =  $(this).val() + String.fromCharCode(e.which);
        if(!$.isNumeric(isNumericChar)){
            return false;
        }
    });
    $("body").on("paste",element,function(e){
        var data = e.originalEvent.clipboardData.getData('Text');
        if(!$.isNumeric(data)){
            return false;
        }
    });
    
}

function numericFunction(element) {
    //called when key is pressed in textbox
    $("body").on("keypress", element, function (e) {
        //if the letter is not digit then display error and don't type anything

        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {

            return false;
        }
    });
    $('body').on('paste', element, function (e) {
        return false;
    });

}
//===============================================}

function array_store(classToEach,type){
    var array_var = new Array();
    if(type=="checkbox"){
        $(classToEach).each(function(){
            if($(this).prop("checked")){
                array_var.push($(this).val())
            }
        }); 
    }
    return array_var;
}

function getStatus(val) {
    if (val == "1") {
        return "Active";
    } else {
        return "Not Active";
    }
}
function getStatus_revert(val) {
    if (val == "1") {
        return "Deactivate";
    } else {
        return "Activate";
    }
}

// $('.link-container').animate({
//             scrollTop: $('.link-container').scrollTop() + $(".nav-menu-placeholder-1").position().top
//         }, 'fast');

SearchNextPrevious = {
    init  : function(json_param){
        //searchbox,next,previous,remove,container,search_ele
        this.searchEvt(json_param);
    },
    indexCtr : 0,
    totalSearchedCtr : 0,
    searchEvt : function(json_param){
        var searchbox = json_param['searchbox'];
        var searchNext = json_param['nextbtn'];
        var searchPrevous = json_param['previousbtn'];
        var searchCancel = json_param['cancelbtn'];
        var self = this;
        var searchValue = "";

        //all target in next previous
        searchbox.keyup(function(e){
            // if(e.shiftKey && e.keyCode==13){
            //     self.searchPreviousFn(json_param);
            // }else if(e.keyCode==13){
            //     self.searchNextFn(json_param);
            // }else{
            searchValue = $.trim($(this).val());
            self.searchFn(json_param,searchValue);
            // }
        })

        //searchNext
        searchNext.click(function(){
            self.searchNextFn(json_param);
        })
        
        //searchPrevous
        searchPrevous.click(function(){
            self.searchPreviousFn(json_param);
        })

        //searchCancel
        searchCancel.click(function(){
            self.searchCancelFn(json_param);
        })
    },
    searchFn : function(json_param,searchValue){
        var self = this;
        searchValue = searchValue.toLowerCase();
        var parentContainer = json_param['parentContainer'];
        var targetEleContainer = $(json_param['targetEleContainer']['selector']);
        if(searchValue==""){
            json_param.defaultState();
            // $(".target-searched-np").css("background-color","#FFF");
            // $(".target-searched-np").removeClass("target-searched-np");
            self.totalSearchedCtr = 0;
            self.indexCtr = 0;
            return false;
        }

        var target_ele_arr = json_param['target_ele'];
        var target_ele;var target_ele_text;var target_ele_parent;var target_ele_parent_obj;var target_ele_type;
        self.totalSearchedCtr = 0; //reset to 0
        self.indexCtr = 0;
        for(var i in target_ele_arr){
            target_ele_text = $(target_ele_arr[i]['target_text']['selector']);
            target_ele = target_ele_arr[i]['target'];
            target_ele_parent = target_ele_arr[i]['parent'];
            target_ele_type = target_ele_arr[i]['type'];

            target_ele_text.each(function(){ // each of every element in array
                if(target_ele_type=="text"){
                    target_ele_text =$(this).text().toLowerCase();
                }else{
                    target_ele_text =$(this).val().toLowerCase();
                }
                target_ele_parent_obj = $(this).closest(target_ele_parent);
                if(target_ele_text.indexOf(searchValue)>=0){
                    target_ele_arr[i].selectedState(target_ele_parent_obj);
                    target_ele_parent_obj.find(target_ele).addClass("target-searched-np");
                    self.totalSearchedCtr++;
                }else{
                    // console.log(target_ele_arr[i])
                    target_ele_arr[i].unselectedState(target_ele_parent_obj);
                    target_ele_parent_obj.find(target_ele).removeClass("target-searched-np");
                }
            })
        }
        if(self.totalSearchedCtr>0){
            var eleScrol = targetEleContainer.find(".target-searched-np").eq(0);
            console.log(parentContainer);
            console.log(eleScrol)
            parentContainer.animate({
                    scrollTop: parentContainer.scrollTop() + eleScrol.position().top
                }, 'fast');
        }
        // console.log("total searched: "+self.totalSearchedCtr)
    },
    searchNextFn : function(json_param){
        var self = this;
        var targetEleContainer = $(json_param['targetEleContainer']['selector']);
        var parentContainer = json_param['parentContainer'];
        if(self.totalSearchedCtr==0){
            showNotification({
                message: "No results found.",
                type: "information",
                autoClose: true,
                duration: 3
            })
            return false;
        }
        
        var eleScrol;
        self.indexCtr++;
        if(self.totalSearchedCtr-1>=self.indexCtr){
            
            eleScrol = targetEleContainer.find(".target-searched-np").eq(self.indexCtr);
            parentContainer.animate({
                scrollTop: parentContainer.scrollTop() + eleScrol.position().top
            }, 'fast');
        }else{
            showNotification({
                message: "End of search result.",
                type: "information",
                autoClose: true,
                duration: 3
            })

        }
    },
    searchPreviousFn : function(json_param){
        var self = this;
        var targetEleContainer = $(json_param['targetEleContainer']['selector']);
        var parentContainer = json_param['parentContainer'];
        if(self.totalSearchedCtr==0){
            showNotification({
                message: "No results found.",
                type: "information",
                autoClose: true,
                duration: 3
            })
        }
        var eleScrol;
        if(self.indexCtr>0){
            self.indexCtr--;
            if(self.indexCtr<0){
                self.indexCtr = 0;
                showNotification({
                    message: "End of search result.",
                    type: "information",
                    autoClose: true,
                    duration: 3
                })
                parentContainer.animate({
                    scrollTop: 0
                }, 'fast');
            }else{
                eleScrol = targetEleContainer.find(".target-searched-np").eq(self.indexCtr);
                parentContainer.animate({
                    scrollTop: parentContainer.scrollTop() + eleScrol.position().top
                }, 'fast');
            }
            
        }else{
            // alert("end")
            showNotification({
                message: "End of search result.",
                type: "information",
                autoClose: true,
                duration: 3
            })
        }
    },
    searchCancelFn : function(json_param){
        var self = this;
        var searchbox = json_param['searchbox'];
        var parentContainer = json_param['parentContainer'];
        var targetEleContainer = json_param['targetEleContainer'];
        self.totalSearchedCtr = 0;
        self.indexCtr = 0;
        searchbox.val("");
        parentContainer.animate({
                scrollTop: 0
            }, 'fast');
        $(".target-searched-np").removeClass("target-searched-np");
        json_param.defaultState();
    }


}

function NotifyMe(element_affected, message, uid_name) {
    if (typeof message == "undefined") {
        //catch errr
        message = '';
    }
    var this_element = $(element_affected);
    var this_id_object_data = $(element_affected).attr("data-object-id");
    var this_element_name = $(element_affected).attr("name");
    if (!this_element_name) {
        this_element_name = uid_name;
    }
    var element_container = this_element.parent();
    var element_ptop = this_element.position().top;
    var element_pleft = this_element.position().left;
    var element_pright = this_element.position().left + this_element.outerWidth();
    var element_pbottom = this_element.position().top + this_element.outerHeight();
    var noti_ele = $(
            '<div class="NM-notification" nm-unique-attr="' + this_element_name + this_id_object_data + '" style="z-index:500;padding:5px;"><div class="arrow-left"></div>' + message + '</div>'
            )
    if(this_element.is('[type="radio"]')||this_element.is('[type="checkbox"]')){
        this_element.parent().parent().addClass("NM-redborder");
        element_container = element_container.parent();
    }
    this_element.addClass("NM-redborder");
    this_element.attr("autofocus","autofocus");
    if (element_container.children(".NM-notification[nm-unique-attr='" + this_element_name + this_id_object_data + "']").length == 0) {
        element_container.append(noti_ele);
        $(noti_ele).css({
            "top": (element_ptop) + "px",
            "left": (element_pright + 5) + "px",
            "padding": "8px",
            "color": "#ffffff",
            "text-align": "center",
            "text-decoration": "none",
            "position": "absolute",
            "-webkit-border-radius": "4px",
            "-moz-border-radius": "4px",
            "border-radius": "4px",
            "background-color": "#000000",
            "z-index": (500)
        });
        $(noti_ele).children(".arrow-left").eq(0).css({
            "width": "0px",
            "height": "0px",
            "border-top": "10px solid transparent",
            "border-right": "10px solid #000000",
            "position": "absolute",
            "top": "5px",
            "left": "0px",
            "margin-left": "-5px"
        });
        $(noti_ele).hide();
        var mouse_entered = "not yet";
        var event_name_mouseenter = "mouseenter.EventNotifyMe" + this_id_object_data;
        // var event_name_focusin = "focusin.EventNotifyMe";

        noti_ele.on(event_name_mouseenter, function () {
            if (mouse_entered == "not yet") {
                noti_ele.fadeOut(2500, function () {
                    $(this).remove();
                });
                mouse_entered = "yes";
                $(this).off(event_name_mouseenter);
            }
        });
        // this_element.on(event_name_focusin, function() {
        //         $(this).removeClass("frequired-red");
        //         if (noti_ele.length >= 1) {
        //             noti_ele.fadeOut(1000, function() {
        //                 $(this).remove();
        //             });
        //         }
        // });
        this_element.on(event_name_mouseenter, function () {
            $(this).removeClass("NM-redborder");
            $(this).parent().parent().removeClass("NM-redborder");
            if (noti_ele.length >= 1) {
                noti_ele.fadeIn(500, function () {
                    noti_ele.fadeOut(2500, function (){
                        $(this).remove();
                    });
                    
                });
            }
            $(this).off(event_name_mouseenter);
        });
    } else if (element_container.children(".NM-notification[nm-unique-attr='" + this_element_name + this_id_object_data + "']").length == 1) {
        noti_ele = element_container.children(".NM-notification[nm-unique-attr='" + this_element_name + this_id_object_data + "']");
        if (noti_ele.text().indexOf(message) >= 0) {

        } else {
            noti_ele.append("<br/>" + message);
        }
    }
    return noti_ele;
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    return pattern.test(emailAddress);
}


//lipat dito 
ImportRequest = {
    generateImportDialog: function () {
        //$('body').on('click', '[link-type="import"]', function() {
        //    var formId = $(this).attr('link-form-id');
        //    var ret = '<h3 class="fl-margin-bottom"><i class="icon-asterisk"></i> Request Import</h3>' + 
        //            '<form action ="/ajax/request_import" class="fl-importBtn" method="POST" enctype="multipart/form-data"> ' +
        //            '<div class="hr"></div>' +
        //            '<input class="display" type="text" name="FormID" value = "' + formId + '" />' +
        //            '<div class="fields"><div class="label_below2 fl-margin-bottom">Please select file: </div><div class="input_position">' +
        //            '<div id="uniform-fileInput" class="uploader label_below2">' +
        //            '<input type="file" data-action-id="3" value="upload" name="fileupload" id="uploadForm" size="24" style="opacity: 0;" class="import_request_file" />' +
        //            '<span id="uploadFilename_3" class="filename import_request_txt">No file selected</span>' +
        //            '<span class="action">Choose File</span>' +
        //            '</div>' +
        //            '</div></div>' +
        //            '<div class="fields"><div class="label_below2"></div><div class="input_position" style="color:#ff0000;font-style:italic;margin-top:5px;">Note: File should be in Comma Separated Value(CSV) file format </div></div>' +
        //            '<div class="fields"><div class="label_below2"></div><div class="input_position" style="margin-top:5px;"><input type="submit" class="btn-blueBtn fl-margin-right" value="Ok"> <input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div></div>' +
        //            ' </form>';
        //    var newDialog = new jDialog(ret, '', '', '', '', function() {
        //
        //    });
        //    newDialog.themeDialog("modal2");
        //});

        $('body').on('click', '#import_option', function () {

            var checked = $(this).prop('checked');
            if (checked == true) {
                $('.div_update_field_reference').removeClass('display');
            } else {
                $('.div_update_field_reference').addClass('display');
            }
        });
        $('body').on('click', '[link-type="import"]', function () {
            var formId = $(this).attr('link-form-id');
            //modified by japhet morada, add options for embedded view import
            var activeFields= "";
            var embed_attr = "";
            var embed_id = "";
            var embed_reference_key = "";
            var embed_reference_value = "";
            var embed_submission = "";
            var display = "";
            if($(this).attr('embed-import') == "true"){
                activeFields = $(this).attr('import-active-fields');
                embed_attr = "embed-import";
                embed_id = 'embed-object-id="' + $(this).attr('embed-object-id') + '"';
                var get_field = '.getFields_' + $(this).attr('embed-object-id');
                embed_reference_key = $(get_field).attr('embed-source-lookup-active-field-val');
                embed_reference_value = $('[name="' + $(get_field).attr('embed-result-field-val') + '"]').val();
                embed_submission = $(get_field).attr('commit-data-row-event');
                display = "display";
            }
            else{
                activeFields = $(this).attr('link-form-active-fields');
            }
            var options = '';
            if (activeFields != '') {
                var activeFieldsArr = activeFields.split(',');
                for (var ctr = 0; ctr <= activeFieldsArr.length - 1; ctr++) {
                    options += '<option value="' + activeFieldsArr[ctr] + '">' + activeFieldsArr[ctr] + '</option>';
                }
            }
            var ret = '<h3 class="fl-margin-bottom"><i class="icon-asterisk"></i> Request Import</h3>' +
                    '<form action ="/ajax/request_import" class="fl-importBtn importCSVForm" method="POST" enctype="multipart/form-data"> ' +
                    '<div class="hr"></div>' +
                    '<input class="display" type="text" name="FormID" value = "' + formId + '" />' +
                    '<input type="text" class="display" name="embed_import_reference_key" value="' + embed_reference_key + '"/>' + 
                    '<input type="text" class="display" name="embed_import_reference_value" value="' + embed_reference_value + '"/>' + 
                    '<input type="text" class="display" name="embed_submission" value="' + embed_submission + '"/>' + 
                    '<div class="fields"><div class="label_below2 fl-margin-bottom">Please select file: </div><div class="input_position">' +
                    '<div id="uniform-fileInput" class="uploader label_below2">' +
                    '<input type="file" data-action-id="3" value="upload" name="fileupload" id="uploadForm" size="24" style="opacity: 0; width:100%;" class="import_request_file" />' +
                    '<span id="uploadFilename_3" class="filename import_request_txt">No file selected</span>' +
                    '<span class="action">Choose File</span>' +
                    '</div>' +
                    '</div></div>' +
                    '<div class="fields"><div class="input_position">' +
                    '<a href="/ajax/request_import_template?form_id=' + formId + '"><label style="cursor:pointer">Download Template</label></a>' +
                    '</div></div>' +
                    '<div class="fields ' + display + '"><div class="label_below2 fl-margin-bottom">Options: </div><div class="input_position">' +
                    '<label class="tip" title="" data-original-title="Validate Existing Document(s)"><input type="checkbox" class="fp-user groups css-checkbox" id="import_option" name="import_option" value="1"><label for="import_option" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">Validate Existing Document(s)</span></label>' +
                    '</div></div>' +
                    '<div class="div_update_field_reference display"><div class="fields"><div class="label_below2 fl-margin-bottom">Field Reference: </div><div class="input_position">' +
                    '<select class="form-select import_update_reference_field fl-margin-bottom" name="import_update_reference_field" id="import_update_reference_field" style="margin-top:5px;">' + options + '</select>' +
                    '</div></div>' +
                    '<div class="div_update_field_reference display"><div class="fields"><div class="label_below2 fl-margin-bottom">Action: </div><div class="input_position">' +
                    '<label><input class="css-checkbox" value="0" type="radio" checked="checked" name="action" id="rSkip"><label for="rSkip" class="css-label"></label>Skip </label>' +
                    '<label><input class="css-checkbox" value="1" type="radio" name="action" id="rUpdate"><label for="rUpdate" class="css-label"></label>Update </label>' +
                    '</div></div>' +
                    '<div class="fields"><div class="label_below2"></div><div class="input_position" style="color:#ff0000;font-style:italic;margin-top:5px;">Note: File should be in Comma Separated Value(CSV) file format </div></div>' +
                    '<div class="fields"><div class="label_below2"></div><div class="input_position" style="margin-top:5px;"><input type="button" class="btn-blueBtn fl-margin-right importCSV fl-positive-btn" ' + embed_attr + ' ' + embed_id + ' value="OK" active-fields="' + activeFields + '"> <input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div></div>' +
                    ' </form>';


            var newDialog = new jDialog(ret, '', '', '', '', function () {

            });
            newDialog.themeDialog("modal2");
        });

        $("body").on("click", ".importCSV", function () {
            var self = $(this);
            var fileName = $('#uploadForm').val();
//            $(".importCSVForm").submit();
//            return;
//


            if (fileName.isEmpty()) {
                showNotification({
                    message: "Please select file to upload.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });

                return;
            }

            var file = $('#uploadForm')[0].files[0].name;
            var type = file.split('.')[file.split('.').length - 1];

            if (type != 'csv') {
                showNotification({
                    message: "File format not valid.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });

                return;
            }


            ui.block();
            $(".importCSVForm").ajaxForm(function (data) {
                if (data == "0") {
                    showNotification({
                        message: "File should be in CSV format!",
                        type: "error",
                        autoClose: true,
                        duration: 3
                    });
                    ui.unblock();
                    return;
                }

                console.log('data', data);
                data = JSON.parse(data);
                if (data.Invalid_Field_Names) {
                    if (data.Invalid_Field_Names.length >= 1) {
                        var errHeaders = data.Invalid_Field_Names.join(', ');
//                    console.log('errHeaders', errHeaders);
                        showNotification({
                            message: "The following fields are not existing on the form - " + errHeaders + '. Please modify and re-import',
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                        ui.unblock();
                        return;

                    }
                }

                if (data.Invalid_Field_Format) {
                    if (data.Invalid_Field_Format.length >= 1) {
                        showNotification({
                            message: "There's a value format error on your import, an email has been sent to you for your reference. Please modify and re-import",
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                        ui.unblock();
                        return;
                    }
                }
                if($('.importCSVForm').find('[name="embed_import_reference_key"]').length > 0){
                    if(data.file_empty){
                        if(data.file_empty.length >= 1){
                            showNotification({
                                message: data.file_empty,
                                type: "error",
                                autoClose: true,
                                duration: 3
                            });
                            ui.unblock();
                            return;
                        }
                    }
                }
//                var importOpton = data['import_option'];
//                var message = 'Import Complete!<br/>' +
//                        'No. of document(s) added : ' + data['insert_count'];
//
//
//                if (importOpton == 1) {
//                    message += '; No. of document(s) updated : ' + data['update_count'];
//                }

                showNotification({
                    message: "Importing Data.... An email will be sent to you once done.",
                    type: "information",
                    autoClose: true,
                    duration: 3
                });
                var import_csv_key = $('.importCSVForm').find('[name="embed_import_reference_key"]').val();
                var import_csv_value = $('.importCSVForm').find('[name="embed_import_reference_value"]').val();
                var import_csv_embed_submission = $('.importCSVForm').find('[name="embed_submission"]').val();
                var import_csv_form_id = $('.importCSVForm').find('[name="FormID"]').val();

                ui.unblock();
                $("#popup_cancel").click();
                if(self.attr('embed-import')){

                    setTimeout(function () {
                        window.location = "/user_view/application?id=" + data['application_id'];
                    }, 2000)
                }
                else{
                    var obj_id = self.attr('embed-object-id');
                    var embed_object = $('.getFields_' + obj_id);
                    var result_field = embed_object.attr('embed-result-field-val');
                    var filename = "import_log_" + data.tmpname.split('.')[0] + '_' + data.formID;
                    var embed_refresh = embed_object.parent().find('.refresh_embed').eq(0);
                    var import_refresh_type = "auto";
                    var offset = 0;
                    embed_refresh.css('display', 'inline-block');
                    embed_refresh.html('<label style="margin-left:5px;"><i class="fa fa-rotate-right fl-fieldloader"></i> Importing Records...</label>')
                    if(import_refresh_type == "auto"){

                        function autoRefreshEmbed(){
                            setTimeout(function(){
                                console.log("check if inserted...");
                                var json_params = {
                                    "action":"auto",
                                    "keyword_to_be_checked": ['insert count'],
                                    "final_keyword": 'import done',
                                    "field_name": import_csv_key,
                                    "field_value": import_csv_value,
                                    "form_id": import_csv_form_id,
                                    "file_name": filename,
                                    "offset": offset
                                }
                                $.post('/ajax/check_import_file', json_params, function(data){
                                    if(data == "false"){
                                        autoRefreshEmbed();
                                    }
                                    else if(data == "true"){
                                        embed_refresh.html('<label style="margin-left:5px;"> Import Done... Click </label><a class="target_embed_refresh">here</a><label> to refresh.</labe>');
                                        $('.target_embed_refresh').click(function(){
                                            embeded_view.refreshEmbed(embed_object); 
                                            embed_refresh.html("");
                                        });
                                    }
                                    else{
                                        var json = JSON.parse(data);
                                        if(offset != json['offset']){
                                            offset = json['offset'];
                                            embeded_view.refreshEmbed(embed_object);
                                        }
                                        autoRefreshEmbed();
                                    }
                                });
                            }, 1000);
                        }
                        autoRefreshEmbed();
                    }
                    else{
                        var json_params = {
                            "action":"manual",
                            "keyword_to_be_checked": ['insert done', 'update done', 'import done'],
                            "file_name": filename
                        }
                        function checkIfDone(){
                            setTimeout(function(){
                                $.post('/ajax/check_import_file', json_params, function(data){
                                    console.log(data);
                                    if(data != "true"){
                                        checkIfDone();
                                    }
                                    else{

                                        // $('[name="' + result_field + '"').trigger('change');
                                        $('.target_embed_refresh').click(function(){
                                            embeded_view.refreshEmbed(embed_object); 
                                            embed_refresh.html("");
                                            if(import_csv_key != ""){
                                                // if(import_csv_embed_submission == 'parent'){
                                                //     var data = {
                                                //         "reference_field": import_csv_key,
                                                //         "reference_value": import_csv_value,
                                                //         "form_id": import_csv_form_id
                                                //     }
                                                //     $.post('/ajax/embed_import_update', data, function(echo){
                                                //         console.log("asdasdasdasdasd",echo);
                                                //     });
                                                // }
                                            }
                                        });
                                    }
                                })
                            }, 1000);
                        }
                        checkIfDone();
                    }
                    
                }
            }).submit();
        })
    },
}
function ApplyNumbersOnly(element){
    $(element).on('keydown.numbersOnly input.numbersOnly focus.numbersOnly',function(e){
        if(e.type == 'keydown'){
            var control_character = [00,01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,37,38,39,40,127];
            var key_index = e.keyCode||e.which;
            var test_keys = String.fromCharCode(key_index);
            if(
                !$.isNumeric(test_keys)
                && control_character.indexOf(key_index) <= -1 //tab
                && !( e.ctrlKey && ( key_index == 65 || key_index == 97 ) ) // select all
                && !( e.ctrlKey && ( key_index == 88 || key_index == 120 ) ) //cut
                && !( e.ctrlKey && ( key_index == 67 || key_index == 99 ) ) //copy
                && !( e.ctrlKey && ( key_index == 86 || key_index == 118 ) ) //paste
                && !( (e.ctrlKey || e.shiftKey) && ( key_index == 82 || key_index == 114 ) ) //reload
                && !( (e.ctrlKey || e.shiftKey) && control_character.indexOf(key_index) >= 0 ) //shift + tab //ctrl + tab
            ){
                e.preventDefault();
                return false;
            }
        }else if(e.type == 'input'){
            if(!$.isNumeric($(this).val())){
                $(this).val("");
            }
        }else if(e.type == 'focus'){ //DRAGGED DROP STRING
            if(!$.isNumeric($(this).val())){
                $(this).val("");
            }
        }
    });
}