var jsonGroupsData = {
    search_value: "",   
    "column-sort": "",
    "column-sort-type": "", 
    limit: 10,
}
var columnSort = [];
$(document).ready(function(){
	var pathname = window.location.pathname;
	FormGroups.init();
	UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".groupsUserContainer");

	$("body").on("keyup","#txtSearchGroupsDatatable",function(e){
		if (e.keyCode == "13") {
	        $("#loadGroups").dataTable().fnDestroy();
	        jsonGroupsData['search_value'] = $(this).val();
	        FormGroups.triggerDataTable();
	    }
    })

	$("body").on("click","#btnSearchGroupsDatatable",function(e){
		$("#loadGroups").dataTable().fnDestroy();
        jsonGroupsData['search_value'] = $("#txtSearchGroupsDatatable").val();
        FormGroups.triggerDataTable();
    })

	$("body").on("click",".addGroupsModal",function(){
    	var json = {};
    	json['title'] = "<svg class='icon-svg icon-svg-modal svg-icon-group-settings' viewBox='0 0 100.264 100.597'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-group-settings'></use></svg> <span>Add Group</span>";
    	json['value'] = '';
      json['description'] = "";
    	json['button'] = '<input type="button" class="btn-blueBtn addGroup fl-margin-right" value="Save">';
    	FormGroups.groupModal(json);
    })
    $("body").on("click",".updateGroup",function(){
    	var json = {};
    	var group_id = $(this).attr("data-id");
    	json['title'] = "<svg class='icon-svg icon-svg-modal svg-icon-group-settings' viewBox='0 0 100.264 100.597'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-group-settings'></use></svg> <span>Edit Group</span>";
    	json['value'] = $(this).attr("data-name");
    	json['users'] = $(".users_"+group_id).text();
      json['description'] = $(this).attr("data-description");
    	json['button'] = '<input type="button" data-id="'+ group_id +'" class="btn-blueBtn editGroups fl-margin-right" value="Save">';
    	//original
    	try{
			UserAccessFlagCheckBox.originalChecked = $(".users_"+group_id).text();
			
		}catch(err){
			console.log(err)
			UserAccessFlagCheckBox.originalChecked['users'] = {}
		}
		UserAccessFlagCheckBox.checkedEle = {};
		UserAccessFlagCheckBox.unCheckedEle = {};
    	FormGroups.groupModal(json);
    })
    $("body").on("click","#loadGroups th",function(){
        var cursor = $(this).css("cursor");
        jsonGroupsData['column-sort'] = $(this).attr("field_name");
        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest("#loadGroups").find(".sortable-image").html("");
        if(indexcolumnSort==-1){
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonGroupsData['column-sort-type'] = "ASC";
        }else{
            columnSort.splice(indexcolumnSort,1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonGroupsData['column-sort-type'] = "DESC";
        }
        var thisTh = this;
        $("#loadGroups").dataTable().fnDestroy();
        FormGroups.triggerDataTable(function(){
        	addIndexClassOnSort(thisTh);
        });
    })
	$(".searchGroupsLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonGroupsData['limit'] = val;
            $("#loadGroups").dataTable().fnDestroy();
            // var self = this;
            var oTable = FormGroups.triggerDataTable();
        // }
    })
})
FormGroups = {
	init : function(){
		this.deleteGroups();
		this.addGroups();
		this.editGroups();
		this.triggerDataTable();
	},
	triggerDataTable : function(callback){
		this.defaultData(callback);
	},
	deleteGroups : function(){
		//code here
		var self = this;
		$("body").on("click",".deleteGroup",function(){
			var id = $(this).attr("data-id");
			var newConfirm = new jConfirm("Are you sure you want to delete this group?", 'Confirmation Dialog','', '', '', function(r){
			 	if(r==true){
			 		//get forms
			 		// ui.block();
			 		$.post("/ajax/group_settings",
			 		{
			 			action:"deleteGroup",
			 			id:id
			 		},function(data){
			 			$('#loadGroups').dataTable().fnDestroy();
						FormGroups.triggerDataTable();
						// ui.unblock();
						showNotification({
			                 message: "The selected group has been successfully deleted.",
			                 type: "success",
			                 autoClose: true,
			                 duration: 3
		                });
			 		})

				 	// $.ajax({
	     //                 type: "POST",
	     //                 cache: false,
	     //                 dataType: 'json',
	     //                 url: "/ajax/form_category",
	     //                 data: {action:"deleteGroups"},
	     //                 success: function (jsondata) {
	     //                     // Retrieve data
	     //                     //console.log(json)
	     //                     // fnCallback(json);
	     //                     console.log(jsondata)
	     //                 }
	     //             });
			 	}
			 })
		newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right" font-size:15px;"></i>'});
		});
	},

	groupModal : function(json){
		var self = this;
		var users = $(".users_list").text();
		var users_arr;
		var formPrivacy = json['users'];
		// var image = json['image'];
		if(formPrivacy){
			formPrivacy = JSON.parse(formPrivacy);
			users_arr = formPrivacy['users'];
		}
		// $("body").on("click",".addGroupsModal",function(){
			var ret = '<div><h3><i class="icon-print"></i> '+ json.title +'</h3></div>';
                ret += '<div class="hr"></div>';
                ret += '</div>';
                 ret += '<div id="accordion-container">';
                ret += '    <ul>';
                ret += '        <li><a href="#tabs-1">Settings</a></li>';
                ret += '    </ul>';
                ret += '<div class="content-dialog-scroll" style="">'
                ret += '<div class="content-dialog sub-container-comment requestComment" style="width: 100%;padding:0px;">';
               
                ret += '    <div id="tabs-1" style="padding:10px;">';
	                ret += '    <div class="pull-left" style="width: 100%">';
	                ret += '    	<div class="fields_below section clearing fl-field-style">';
	                ret += '    	<div class="input_position_below column div_1_of_1">';
	                ret += '    		<span class="">Group Name: <font color="red">*</font></span>';
	                ret += '    		<input type="text" name="" data-type="textbox" class="form-text group_name" value="'+ json.value +'"></div>';
	               	ret += '     	</div>';
	                ret += '    </div>';
                   ret += '    	<div class="fields_below section clearing fl-field-style">';
	                
	                ret += '    	<div class="input_position_below column div_1_of_1">';
	                ret += '    		<span class="label_below2">Description: </span>';
	                ret += '    		<input type="text" name="" data-type="textbox" class="form-text group_description" value="'+ json.description +'"></div>';
	               	ret += '     	</div>';
	               	ret += '<div class="fp-container CSID-search-container groupsUserContainer">';
					    ret += '<div class="input_position" style="width:100%">';
						    ret += '<div class="section clearing fl-field-style">';
								ret += '<div class="column div_1_of_1" style="display:table;">';
									ret += '<span style="display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="fp-allUsers7"/><label for="fp-allUsers7" class="css-label"></label> All Users</label></span>';
									ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
								ret += '</div>';
							ret += '</div>';
							ret += '<div style="margin-left: 20px;">';
								//for users
								ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
								ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class="fa fa-minus display"></label> <label class="font-bold">Users</label></div>';
									ret += '<div class="getUsersFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';
										
										var ctr_usr = 0
										if(users){
											users = JSON.parse(users);
											var user_name = "";
											$.each(users,function(key, val){
												// user_name = val.first_name +' '+ val.last_name;
                                                user_name = val.display_name;
												ret += '<div style="width:32%;float:left;margin-bottom:2px;" class="CSID-search-data"> ';
													ret += '<label class="tip" title="'+ user_name +'"><input type="checkbox" class="fp-user fp-user1 users css-checkbox" '+ RequestPivacy.setCheckedArray(val.id, users_arr) +' value="'+ val.id +'" id="fp-user users_'+ val.id +'"/><label for="fp-user users_'+ val.id +'" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">'+ user_name +'</span></label>';
												ret += '</div>';
												ctr_usr++
											})
											
										}
										var displayEmptyUsers = "display";
										if(ctr_usr==0){
											displayEmptyUsers = ""
										}
										ret += '<div class="empty-users '+ displayEmptyUsers +'" style="color:red">No Users</div>';
								ret += '</div>';
								ret += '</div>';
							ret += '</div>';
						ret += '</div>';
					ret += '</div>';
	                ret += '    </div>';
	                
				ret += '</div>';
				ret += '</div>';
				ret += '</div>';
				ret += '</div>';
                ret += '</div>';
                ret += '</div>';
                //ret += '<div class="fields" style="border-top:1px solid #ddd;">';
                ret += '<div class="label_basic"></div>';
                ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                ret += json.button;
                ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
                ret += '</div>';
                ret += '</div>';
               var newDialog = new jDialog(ret, "", "700", "", "60", function() {

                });
                newDialog.themeDialog("modal2");
                limitText(".limit-text-ws",20);
                RequestPivacy.setCheckAllChecked(".fp-user1");
                RequestPivacy.setCheckAllChecked(".fp-user2");
                $('.content-dialog-scroll').perfectScrollbar();

                $( "#accordion-container" ).tabs({
		            activate : function(){
		                $('.content-dialog-scroll').perfectScrollbar("update");
		            }
		        })
                RequestPivacy.bindOnClickCheckBox();
                // if(image){
                // 	$(".category-logo[value='"+ image +"']").attr("checked","checked");
                // }
		// })
	},
	addGroups : function(){
		var self = this;
		$("body").on("click",".addGroup",function(){
			
			var group_name = $.trim($(".group_name").val());
         var group_description = $.trim($(".group_description").val());
			var users = new Array();
			var json_privacy = {};
			if(group_name==""){
				showNotification({
	                 message: "Please fill out required fields",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
                });
				return;
			}
			
			var user_ctr = 0
			//for users
			$(".users").each(function(){
				if($(this).prop("checked")){
					users.push($(this).val());
					user_ctr++
				}
			})

			if(user_ctr==0){
				showNotification({
	                 message: "Please select users for this group.",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
                });
				return;
			}

			json_privacy['users'] = users;
			json_privacy = JSON.stringify(json_privacy);

			ui.block();
			$.post("/ajax/group_settings",
	 		{
	 			action:"addGroups",
	 			group_name:group_name,
            group_description:group_description,
	 			users:json_privacy,
	 		},function(data){
	 			if(data=="0"){
	 				showNotification({
		                 message: "The entered group is already in the list. Please modify.",
		                 type: "error",
		                 autoClose: true,
		                 duration: 3
	                });
	 			}else{
	 				$('#loadGroups').dataTable().fnDestroy();
					self.triggerDataTable();
		 			showNotification({
		                 message: "Group has been successfully saved.",
		                 type: "success",
		                 autoClose: true,
		                 duration: 3
	                });
		 			$("#popup_cancel").click();
	 			}
	 			ui.unblock();
	 			
	 		})
		})
	},
	editGroups : function(){
		var self = this;
		$("body").on("click",".editGroups",function(){
			var group_name = $.trim($(".group_name").val());
         var group_description = $.trim($(".group_description").val());
			var group_id = $(this).attr("data-id");
			var user_ctr = 0;
			if(group_name==""){
				showNotification({
	                 message: "Please fill out required fields",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
                });
				return;
			}
			
			var users = new Array();
			var json_privacy = {};

			//for users
			$(".users").each(function(){
				if($(this).prop("checked")){
					users.push($(this).val());
					user_ctr++;
				}
			})

			if(user_ctr==0){
				showNotification({
	                 message: "Please select users for this group.",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
                });
				return;
			}
			json_privacy['users'] = users;
			json_privacy = JSON.stringify(json_privacy);

			ui.block();
			$.post("/ajax/group_settings",
	 		{
	 			action:"editGroups",
	 			group_name:group_name,
            group_description:group_description,
	 			users:json_privacy,
	 			group_id:group_id,
	 			userCheckedEle: UserAccessFlagCheckBox.checkedEle,
	 			userUnCheckedEle: UserAccessFlagCheckBox.unCheckedEle,

	 		},function(data){
	 			if(data=="0"){
	 				showNotification({
		                 message: "The entered group is already in the list. Please modify.",
		                 type: "error",
		                 autoClose: true,
		                 duration: 3
	                });
	 			}else{
	 				console.log(data)
	 				$('#loadGroups').dataTable().fnDestroy();
					self.triggerDataTable();
		 			showNotification({
		                 message: "Group has been successfully updated.",
		                 type: "success",
		                 autoClose: true,
		                 duration: 3
	                });
		 			$("#popup_cancel").click();
	 			}

	 			ui.unblock();
	 		})
		})
	},
	"defaultData" : function(callback){
		var oColReorder = {
            "iFixedColumns": 0
        };
        var oTable = $('#loadGroups').dataTable( {
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/group_settings",
            "fnServerData": function(sSource, aoData, fnCallback) {
            	aoData.push({"name": "limit", "value": jsonGroupsData['limit']});
                aoData.push({"name": "search_value", "value": $.trim(jsonGroupsData['search_value'])});
                aoData.push({"name": "action", "value": "loadGroupsDatatable"});
                aoData.push({"name": "column-sort", "value": jsonGroupsData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonGroupsData['column-sort-type']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        // console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
                var obj = '#loadGroups';
            	var listOfappContainer = '.fl-list-of-app-record-wrapper';
            	dataTable_widget(obj, listOfappContainer);
            },
            fnDrawCallback : function(){

                set_dataTable_th_width([1]);
                setDatatableTooltip(".dataTable_widget");
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonGroupsData['limit']),
        });
        return oTable;
    }
}