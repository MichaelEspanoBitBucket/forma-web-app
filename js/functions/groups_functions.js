Groups = {
	submit_type : "create",
	group_id : 0,
	init : function(){
		this.create_update();
	    this.update_view();
	    this.delete_group();
	    this.cancel_create_update();
	    this.load_groups();
	    this.removetagForUpdate();
	},
	load_groups : function(){
		var self = this;
		$.post("/ajax/groups",{action:"loadGroups"},function(data){
			var ret = "";var count = 0;
			if(data){
				data = JSON.parse(data);
				for(var i in data){
					count = data[i]['members'].split(",").length;
					ret += self.group_list_layout(data[i]['name'],count,data[i]['id']);
				}
				$(".groupCount").text(data.length);
			}
			if(ret==""){
				ret += '<div class="group-no-record"><label>'+
		                   '<label>No Groups Found</label>'+
		                '</label></div>';
			}
			$(".group_lists").html(ret)
			$('.scrollbarM').perfectScrollbar('update');
		})
	},
	create_update : function(){
		var self = this;
		$(".addGroup").click(function(){
			//alert(self.submit_type)
			var name = $.trim($(".gName").val());
			var count = $(".recipient").length;
			var type = self.submit_type;
			var users = new Array();
			var id = self.group_id;
			if(name!="" && count!=0){
				var message = "Are you sure you want to "+ self.submit_type.toLocaleLowerCase() +" this group?";
			   var newConfirm = new jConfirm(message, 'Confirmation Dialog','', '', '', function(r){
			        if(r==true){
			        	ui.block();
			        	$(".recipient").each(function(){
			        		users.push($(this).attr("data-id"));
			        	})
						$.post("/ajax/groups",{id:id,type:type,users:users,name:name,action:"create_update"},function(data){
							console.log(data)
							if(data=="existing"){
								showNotification({
				                   message: "The entered group name already exists.",
				                   type: "error",
				                   autoClose: true,
				                   duration: 3
					            });
					            return;
							}
							showNotification({
			                   message: "Group has been "+self.submit_type+"d.",
			                   type: "success",
			                   autoClose: true,
			                   duration: 3
				            });
							self.removeData(1);
							self.load_groups();
							ui.unblock();
						})
			        }
			    });
			   newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right"  font-size:15px;"></i>'});
			}else{
				showNotification({
	                   message: "Please fill out all required fields",
	                   type: "error",
	                   autoClose: true,
	                   duration: 3
	            });
			}
		})
	},
	update_view : function(){
		var self = this;
		$("body").on("click",".update_view",function(){
			var gName = $.trim($(this).closest(".group-container").find(".gName_list").text());
			var id = $(this).closest(".group-container").attr("data-id");
			var ret = "";
			var name = "";
			var membersID = "";
			ui.block();
			$.post("/ajax/groups",{action:"loadGroupsMembers",id:id},function(data){
				if(data){
					data = JSON.parse(data);
					for(var i in data){
						// name = data[i]['first_name']+" "+data[i]['last_name'];
						if(data[i]){
							name = data[i]['display_name'];
							ret += self.groupsMembers(data[i]['id'],name,"listRecipient1");
							membersID+=data[i]['id']+",";
						}
					}
				}
				self.removeData(0);
				self.group_id = id
				$("#listRecipient1").before(ret)
				$("#listRecipient1").attr("data-tag-id",membersID.substr(0,membersID.length-1))
				$(".gName").val(gName);
				ui.unblock();
			})
		})
	},
	delete_group : function(){
		var self = this;
		$("body").on("click",".delete_group",function(){
			var selfJQ = this;
			var message = "Are you sure you want to delete this group?";
		  var newConfirm = new jConfirm(message, 'Confirmation Dialog','', '', '', function(r){
		        if(r==true){
		        	var id = $(selfJQ).closest(".group-container").attr("data-id");
					$(selfJQ).closest(".group-container").fadeOut(function(){
						$.post("/ajax/groups",{action:"deleteGroup",id:id},function(data){
							$(this).remove();
							showNotification({
			                   message: "Group has been deleted.",
			                   type: "success",
			                   autoClose: true,
			                   duration: 3
				            });
				            self.load_groups();
						})
					})
		        }
		    });
		  	newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right"  font-size:15px;"></i>'});
		});		
	},
	cancel_create_update : function(){
		var self = this;
		$(".cancel_update").click(function(){
			self.removeData(1);
		})
	},
	group_list_layout : function(name,count,id){
		name = $.trim(name);
		var ret= '<div class="group-container" style="" data-id="'+ id +'">';
          ret+= '<div style="float:left;width:80%">';
            ret+= '<div class="gName_list" style="margin-top:7px;margin-left:10px;font-weight:bold;font-size: 12px;" title="'+ name +'">';
              ret+= '<div class="fl-table-ellip">'+name+'</div>';
            ret+= '</div>';
            ret+= '<div class="" style="margin-top:7px;margin-left:10px;font-size:10px;color:#C5C5C5">';
              ret+= '<i>('+ count +' Members)</i>';
            ret+= '</div>';
          ret+= '</div>';
          ret+= '<div style="float: right;margin-right: 8px;">';
            ret+= '<div class="" style="margin-top:7px">';
              ret+= '<span class="cursor update_view" style="margin-right:6px"><i class="icon-edit fa fa-edit btn-basicBtn padding_5 cursor" style="font-style:normal; font-size:15px;"></i></span>';
              ret+= '<span class="cursor delete_group"><i class="icon-trash btn-basicBtn padding_5 fa fa-trash-o cursor" style="font-style:normal; font-size:15px;"></i></span>';
            ret+= '</div>';
          ret+= '</div>';
        ret+= '</div>';
        return ret;
	},
	removeData : function(type){
		if(type==1){
			this.submit_type = "create";
			$("#uc-group").text("Create Group");
			self.group_id = 0;
		}else{
			this.submit_type = "update";
			$("#uc-group").text("Update Group");
		}
		$(".recipient").remove();
		$(".gName").val("");
		$("#listRecipient1").val("");
		$("#displayUser1").css("display","none");
		
	},
	groupsMembers : function(id,name,className){
		var userClassName = className;
		if(className==""){
			userClassName = "listRecipient1";
		}
		var ret = "";
		ret += '<div class="recipient '+ userClassName +'" id="listRecipient1_'+ id +'" data-id="'+ id +'">';
			ret += '<span style="margin-right:5px;">';
				ret += name;
			ret += '</span>';
			ret += '<icon class="cursor removeTag">X</icon>';
		ret += '</div>';
		return ret;
	},
	removetagForUpdate : function(){
		$("body").on("click",".removeTag",function(){
			$(this).closest(".recipient").remove();
		})
	}
}