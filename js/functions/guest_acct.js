$(document).ready(function(){
    
    SimpleTab2.init('#guest-tabs');
    SwitchElement.init('.fl-edit-acct', '.fl-acct-cancel', '.fl-save-acct');
    Guest.click_guest_modal(".addGuest");
    Guest.guest_password("#guest_password");
    
    Guest.search_request(".search_guest_request");
    Guest.add_guest("#addNewGuest");
    
    
    // Search
    $("input[name='search']").keypress(function (event) {
        console.log(event)
        if (event.keyCode == 10 || event.keyCode == 13) {
            var search_val = $("input[name='search']").val();
            
            window.location.replace("/index?search_query=" + search_val);
        }
    });
    // Delete Guest
    Guest.delete_guest_in_form(".deleteGuestinForm");
    
});

Guest = {
        search_request : function(elements){
          $("body").on("click",elements,function(){
                var search_val = $("input[name='search']").val();
                
                window.location.replace("/index?search_query=" + search_val);
          });
        },
    
        click_guest_modal : function(elements){
            $("body").on("click",elements,function(){
                var data_action = $(this).attr("data-action");
                var data_json = {data_action:data_action}
                var data = "";
                var getFormID = $("#getFormID").val();
                var requestID = $("#getID").val();
                var getTrackNo = $("#getTrackNo").val();
                $.post("/ajax/guest",{action:"view_list",getFormID:getFormID,requestID:requestID,getTrackNo:getTrackNo},function(e){
                    Guest.guest_modal(data_json,e);
                });
                
                
            })
        },
       
        
	guest_modal : function(data_json,data){
            
		var ret = "";
		ret += '<h3 class="fl-margin-bottom"><i class="icon-user"></i> Add New Guest</h3>';
		ret += '<div class="hr"></div>';
		ret += '<div class="fields">';
			ret += '<div class="label_below2"> Email:<font color="red">*</font> </div>';
			ret += '<div class="input_position"  style="width: 100%;">';
                            	ret += '<input type="text" class="suggestion-title fl-suggestion-input" maxlength="100" id="guest_email" name="guest_email" placeholder="Guest Email">';
			ret += '</div>';
		ret += '</div>';
                if (data_json['data_action'] == "guest_selected_form") {
                    var current_url = window.location;
                    ret += '<div class="fields">';
                            ret += '<div class="label_below2"> Url to Share in guest user:<font color="red"></font> </div>';
                            ret += '<div class="input_position"  style="width: 100%;">';
                                    ret += '<input type="text" disabled class="suggestion-title fl-suggestion-input" maxlength="100" id="" name="" placeholder="" value="' + current_url + '">';
                                    ret += '<input type="hidden"  class="suggestion-title fl-suggestion-input" maxlength="100" id="guest_workspace_url" name="guest_workspace_url" placeholder="" value="' + current_url + '">';
                            ret += '</div>';
                    ret += '</div>';
		    ret += '<div class="appendNewGuest">';
                    if (data != "none") {
                        ret += '<div class="fields">';
                                ret += '<div class="label_below2"> User Guest List:<font color="red"></font> </div>';
                                ret += '<div class="input_position view_guest_list"  style="width: 100%;">';
                                var getdata = JSON.parse(data);
                                console.log(getdata);
                                        for(var i = 0; i < getdata.length; i++){
                                            if (getdata[i]['id'] != null) {
                                                ret += '<li style="border-bottom: 1px solid;padding: 5px;border-color: #ddd;">' + getdata[i]['email'] + '<i data-g-id="' + getdata[i]['guest_id'] + '" class="deleteGuestinForm cursor tip pull-right fa fa-times"  data-original-title="Remove this Guest"></i></li>';
                                            }
                                        }
                                ret += '</div>';
                        ret += '</div>';
                    }
                    ret += '</div>';
                }
			ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
				ret += '<img src="/images/loader/load.gif" class="display " style="margin-right:5px;margin-top:2px;margin-bottom:5px;">';
				ret += '<input type="button" class="btn-blueBtn fl-margin-right" data-action="' + data_json['data_action'] + '"   id="addNewGuest" value="Save"> ';
				ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel" style="margin-bottom:5px;">';
			ret += '</div>';
		
		var newDialog = new jDialog(ret, "","550", "250", "", function(){
		});
		newDialog.themeDialog("modal2");
                

                $(".tip").tooltip();
	},
        
        // Delete Guest in a Request 
        delete_guest_in_form : function(elements){
            $("body").on("click",elements,function(){
                var guestID = $(this).attr("data-g-id");
                var action = "remove_guest_in_form";
                
                var guest_Delete = new jConfirm("Are you sure do you want to delete this?", 'Confirmation Dialog','', '', '', function(r){
                    if(r==true){
                        $.post("/ajax/guest",{action:action,guestID:guestID},function(e){
                            if (e == "Guest was successfully deleted.") {
                                showNotification({
                                                message: e,
                                                type: "success",
                                                autoClose: true,
                                                duration: 3
                                        })
                                //$(".addGuest").trigger("click");
				//Guest.click_guest_modal(".addGuest");
                            }
                        });
                    }
		    //else{
                        //$(".addGuest").trigger("click");
			//Guest.click_guest_modal(".addGuest");
                    //}
                });
                guest_Delete.themeConfirm("confirm2", {'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'});
            })
        },
        
        // Save new guest
        add_guest : function(elements){
            $("body").on("click",elements,function(){

                var email_val = $("#guest_email");
                var guest_email = email_val.val();
                var data_action = $(this).attr("data-action");
                var getFormID = $("#getFormID").val();
                var requestID = $("#getID").val();
                var getTrackNo = $("#getTrackNo").val();
                var emailValidationMessage = "Email is Invalid."
                ui.block();
                if (isValidEmailAddress(guest_email) == true) {
                    //if (data_action == "guest_all") {
                    //    $.post("/ajax/guest",{action:"add_guest",data_action:data_action,guest_email:guest_email},function(e){
                    //        //console.log(e)
                    //        if (e == "Successful") {
                    //            showNotification({
                    //                    message: "Email was successfully saved as guest account.",
                    //                    type: "success",
                    //                    autoClose: true,
                    //                    duration: 3
                    //            })
                    //            email_val.val(null);
                    //            ui.unblock();
                    //        }else{
                    //            showNotification({
                    //                    message: "This email is already registered.",
                    //                    type: "error",
                    //                    autoClose: true,
                    //                    duration: 3
                    //            })
                    //            ui.unblock();
                    //        }
                    //    })
                    //}else{
                        var get_cur_url = $("#guest_workspace_url").val();
                        //console.log("sad");
                        $.post("/ajax/guest",{getTrackNo:getTrackNo,requestID:requestID,getFormID:getFormID,action:"selected_module_for_guest",data_action:data_action,guest_email:guest_email,get_cur_url:get_cur_url},function(e){
                            //console.log(e)
                            if (e == "Successful") {
				$(".appendNewGuest").html(null);
                                showNotification({
                                        message: "This form was successfully added to guest list.",
                                        type: "success",
                                        autoClose: true,
                                        duration: 3
                                })
                                email_val.val(null);
                                ui.unblock();
				
                                $.post("/ajax/guest",{action:"view_list",getFormID:getFormID,requestID:requestID,getTrackNo:getTrackNo},function(e){
                                    
                                    var ret = "";
                                    var getdata = JSON.parse(e);
				    
				    ret += '<div class="fields">';
					    ret += '<div class="label_below2"> User Guest List:<font color="red"></font> </div>';
					    ret += '<div class="input_position view_guest_list"  style="width: 100%;">';
						    for(var i = 0; i < getdata.length; i++){
							if (getdata[i]['id'] != null) {
							    ret += '<li style="border-bottom: 1px solid;padding: 5px;border-color: #ddd;">' + getdata[i]['email'] + '<i data-g-id="' + getdata[i]['guest_id'] + '" class="deleteGuestinForm cursor tip pull-right fa fa-times"  data-original-title="Remove this Guest"></i></li>';
							}
						    }
					    ret += '</div>';
				    ret += '</div>';
				    
                                    $(".appendNewGuest").html(ret);
                                });
				    
                            }else{
                                showNotification({
                                        message: e,
                                        type: "error",
                                        autoClose: true,
                                        duration: 3
                                })
                                email_val.val(null);
                                ui.unblock();
                            }
                        })
                    //}
                    
                }else{
                    if($.trim(guest_email)==""){
                        
                        emailValidationMessage = "Please Provide an Email"
                    }
                    showNotification({
                            message: emailValidationMessage,
                            type: "error",
                            autoClose: true,
                            duration: 3
                    })
                    ui.unblock();
                }
            })
        },
        
        guest_password : function(elements){
            $("body").on("click",elements,function(){
                var new_password = $("#new_password").val();
                var retype_password = $("#retype_password").val();
                console.log(new_password +"=="+ retype_password);
                var user_id = $("#user_id").val();
                if (new_password != "" && retype_password != "") {
                    if (new_password == retype_password) {
                        $.post("/ajax/guest",{action:"guest_password",user_id:user_id,new_password:new_password,retype_password:retype_password},function(e){
                            if (e == "Successful") {
                                showNotification({
                                        message: "Your password was successfully set. Please wait while redirecting to the login page...",
                                        type: "success",
                                        autoClose: true,
                                        duration: 5
                                })
                                var url_redirect = $("#redirection").val();
                                setTimeout(function(){
                                    window.location.replace(url_redirect);
                                },5000)
                            }
                        });
                    }else{
                        showNotification({
                                message: "Your Password doesn't match.",
                                type: "error",
                                autoClose: true,
                                duration: 3
                        })
                    }
                }else{
                    showNotification({
                            message: "Type your preferred password for your account.",
                            type: "error",
                            autoClose: true,
                            duration: 3
                    })
                }
                
            });
        }
}
        

//function for tabs
SimpleTab2 = {
    "init" : function(container){
        var self = this;
        //on tab click
        $(container+' li').on({
            'click':function(){

                //set all as inactive
                $(container+' li').removeAttr("attr")

                //set press tab as active
                $(this).attr("attr","active")

                $(container+' li').each(function(){
                    if($(this).attr("attr")=="active"){
                        self.setActiveTAb(this);
                    }else{
                        self.setInactiveTab(this);

                    }

                });
                //init hide show function
                HideShowEle.init('.guest-fl-search-wrapper');
            }

        });
        //onload page
        $(container+' li[attr="active"]').trigger('click');
        //#tabs
    },
    "setActiveTAb" : function(self,container){
        var t = $(self).attr('id');
        $(self).css({
            'border-top': '1px solid #cdced1',
            'border-right': '1px solid #cdced1',
            'border-left': '1px solid #cdced1',
             'color':'#4E4E4E'
        });

        $(self).children('i').css({
            'color': '#4E4E4E'
        });

        $('#' + t + 'd').fadeIn('slow');
        $(self).removeClass('inactive')
    },

    "setInactiveTab" : function(self,container){
        var t = $(self).attr('id');
        $(self).css({
            'border-top': 'none',
            'border-left': 'none',
            'border-right': 'none',
            'color':'#33b5e5'
        });

        $(self).children('i').css({
            'color': '#33b5e5'
        });

        $(self).addClass('inactive')
        $('#' + t + 'd').hide();
    }
}


// Function for Hiding showing
HideShowEle = {
    "init" : function(container){ 
       var self = this;     
       self.SearchWrap(container);
    },
    
    "SearchWrap" : function(container){
        var t = $('#guest-tab1').attr('id');
        if ($('#guest-tab1').attr('attr') == 'active') {
            $(container).show();
        }else {
            $(container).hide();
        }

    }
}



//funtion for switching elements
SwitchElement = {
    "init": function(editacctBtn, cancelacctBtn, saveacctBtn){
        var self = this;
        $(editacctBtn).parents('.fl-userpro-header').next().children('.fl-right-wrapper-btn').find('.fl-guest-label').attr('show', 'active');
   
        $(editacctBtn).on({
            //switching event
            'click': function(){

                if ($('.fl-guest-label').attr('show') == 'active') {
                    self.show();                       
                }else {
                    self.hide();
    
                };
            }

        }); 

        $(cancelacctBtn).on({
            //switching event
            'click': function(){

                if ($('.fl-guest-label').attr('show') == 'active') {
                    self.show();

                }else {
                    self.hide();
                    $('.fl-right-wrapper-btn ul li:nth-child(3)').hide();
                    $('.fl-right-wrapper-btn ul li:nth-child(1)').show();    
                };
            }

        });
        $(saveacctBtn).on({
            'click': function (){
                if ($('.fl-guest-label').attr('show') == 'active') {
                    self.show();

                }else {
                    self.hide();
                    $('.fl-right-wrapper-btn ul li:nth-child(3)').hide();
                    $('.fl-right-wrapper-btn ul li:nth-child(1)').show();    
                };
            }
        });     
    },

    "show": function(){
        $('.fl-guest-label').hide().removeAttr('show');
        $('.fl-guest-field').show().attr('show', 'active');
        $('.fl-right-wrapper-btn ul li:nth-child(3)').show();
        $('.fl-right-wrapper-btn ul li:nth-child(2)').show();
        $('.fl-right-wrapper-btn ul li:nth-child(1)').hide();

    },

    "hide": function(){
        $('.fl-guest-label').show().removeAttr('show').attr('show', 'active');
        $('.fl-guest-field').hide().removeAttr('show');
        $('.fl-right-wrapper-btn ul li:nth-child(2)').hide();
        
    }

}
