(function($) {
	$.fn.hashtags = function(elements) {
		var event = elements;
		$(this).wrap('<div class="jqueryHashtags"><div class="highlighter ' + elements + '" data-tag-id="0"></div></div>').unwrap().before('<div class="highlighter ' + elements + '" data-tag-id="0"></div>').wrap('<div class="typehead"></div></div>');
		$(this).addClass("theSelector");
		$(this).autosize({append: "\n"});
		$(this).on("keyup", function() {
			var str = $(this).val();
			$(this).parent().parent().find("."+ elements).css("width",$(this).css("width"));
			str = str.replace(/\n/g, '<br>');
			//str = nl2br(str);
			str = htmlEntities(str);
			str = nl2br(str);
			//str = str.replace(/(<([^>]+)>)/ig,"");
			if(!str.match(/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?#([a-zA-Z0-9]+)/g)) {
				if(!str.match(/#([a-zA-Z0-9]+)#/g)) {
					str = str.replace(/#([a-zA-Z0-9]+)/g,'<span class="hashtag">#$1</span>');
				}else{
					str = str.replace(/#([a-zA-Z0-9]+)#([a-zA-Z0-9]+)/g,'<span class="hashtag">#$1</span>');
					
				}
				
			}
			$(this).parent().parent().find("." + elements).html(str);
		});
		$(this).parent().prev().on('click', function() {
			$(this).parent().find(".theSelector").focus();
		});
		
		//// Mentions
		//$("body").on("click","#search_info_" + event,function(){
		//	var dataID = $(this).attr("data-id");
		//	$div = $('.add_' + event);
		//	var name = $("#search_name_" + dataID).attr("data-name");
		//	$("." + event).append('<span class="hashtag">'+name+'</span>' + name);
		//	   console.log(name)
		//	   $div.eq(0).removeClass("activeS"); // remove active user selected on the search
		//	  $("#mentionsPost").remove();
		//     });

  	};
	function htmlEntities(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	}
	function nl2br(varTest){
		return varTest.replace(/&lt;br&gt;/g, "<br>");
	}
	function contacts1(user,event){
    var ret = '<div class="display_box add_' + event + '" id="search_info_' + event + '" data-id="' + user.userID + '"  align="left">';
	    ret += user.img;
	    ret += '<div class="search_name" data-name="' + user.name + '" id="search_name_' + user.userID + '">';
		ret += user.name;
	    ret += '</div>';
    ret += '</div>';
    return ret;	
}
})(jQuery);
