(function(){

	angular.module('iconSet', [])
	.controller('IconSetCtrl',['$scope', '$http', function($scope, $http){

		var onDataComplete = function(response){
			
			$scope.icons = response.data;
			console.log($scope.icons);
		}

		var onError = function(reason){
			$scope.error = "Fetching data problem.";
		}	

		$http.get('/json/icons.json')
			.then(onDataComplete);

	}]);
	
})();

