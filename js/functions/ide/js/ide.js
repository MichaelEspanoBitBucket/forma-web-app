//--------------------------------------------variables--------------------------------------------//
var formulas = [];
var ideUndo = [];
var ideRedo = [];
var ideClipboard = '';
var eventType = '';
var selected_element, current_element, firstLineChar, content, lastPressedKey, prevIndex = 0;
var autocomplete, numbering, totalLines = 0, autoCompleteHandler;
var isAutocompleteDisplayed = false, ignoreKey = false, blurred = false, isNumberingDisplayed = false;
var current_formula_type;
var ide_selected_title_code = "";
var ideconfig = {
	'keywordsjson': '', //keyword's json
	'width': '', //width of IDE
	'height': '', //height of IDE
	'zindex': '',
	'backgroundcolor': '',	//bg of IDE
	'keywordcolor': '',	//color of keyword
	'numbercolor': '',	//color of number
	'stringcolor': '',	//color of string
	'commentcolor': '',	//color of comment
	'variablecolor': '', //color of variable
	'font-size': '',	//font size of IDE
	'numbering': false, //enable numbering
	'autocomplete': false,	//enable autocomplete
	'menubar': false,	//enable menubar
	'overlay': true,
	'idecss': {	//css of IDE
		'width': '85%',
		'font-family': 'monospace',
		'color': '#BBBBBB',
		'float': 'left',
		'padding-left': '5px',
		'outline': 'none'
	},
	'menucss':{	//css of menubar
		'width': '100%',
		'background-color': '#AAAAAA'
	},
	'numberingcss': {	//css of numbering
		'width': '50px',
		'height': '100%',
		'background-color': '#888888',
		'font-family': 'monospace',
		'color': 'rgb(95, 95, 95)',
		'float': 'left',
		'text-align':'left',
		'padding-right': '5px'
	},
	'highlightlinenumbercss': { //css of highlighted number
		'background-color': '#444444',
		'padding-left': '5px',
		'color': '#888888'
	},
	'linenumbercss': {	//css of line numbers
		'padding-left': '5px',
		'color': '#BBBBBB'
	},
	'autocompletecss': {	//css of autocomplete
		'width': '150px',
		'height': '150px',
		'font-family': 'monospace',
		'font-size': '16px',
		'background-color': 'white',
		'list-style-type': 'none',
		'padding': '0',
		'z-index': '199999999',
		'overflow': 'scroll' 
	},
	'selectedcss': {
		'background-color': '#00CC8B',
		'font-family': 'monospace'
	},
	'overlaycss': {
		'background-color': '#333333',
		'width': window.innerWidth,
		'height': window.innerHeight,
		'z-index': '100'
	}
};

function getKeywordsFromJson(json){
	if(json){
		for(i in json){
			formulas.push(json[i]);
		}
	}
}

function getFieldNames(element){
	if($(element).length > 0){
		var get_field_names = [];
		var getFields = element.get();
		for(i in getFields){
			if($(getFields[i]).attr('name') != $(getFields[i-1]).attr('name')){
				get_field_names.push($(getFields[i]).attr('name'));
			}
		}
		for(i in get_field_names){
			if(get_field_names[i]){
				get_field_names[i] = get_field_names[i].replace(/\[|\]|\{|\}|\(|\)/g, '');
				var fieldName = {
					"keyword_name": "@" + get_field_names[i],
					"params": ""
				}
				formulas.push(fieldName);
			}
		}
	}
}

//-----------------------------------plugin style functions-----------------------------------//

$.fn.wordify = function(){
	this.find(":not(iframe,textarea)").addBack().contents().filter(function() {
		return this.nodeType === 3;
	}).each(function() {
		var textnode = $(this);
		var text = textnode.text();
		if(textnode.parent('span').is('span')){
			text = text;
		}
		else{
			text = text.replace(/([^\s-.,;:!?()[\]{}<>~\u200B-\u200D\uFEFF•]+)/g, '<span style="color:' + ideconfig.variablecolor + '">$1</span>');
			text = text.replace(/•/g, '<span class="remove_me" id="caret" style="color:' + ideconfig.variablecolor + '">•</span>');
			textnode.replaceWith(text);
		}
	});
	
	return this;
};

// $.fn.wordify = function(){
// 	this.find(":not(iframe,textarea)").addBack().contents().filter(function() {
// 		return this.nodeType === 3;
// 	}).each(function() {
// 		var textnode = $(this);
// 		var text = textnode.text();
// 		if(textnode.parent('span').is('span')){
// 			text = text;
// 		}
// 		else{
// 			text = text.replace(/([^\s-.,;:!?()[\]{}<>~\u200B-\u200D\uFEFF]+)/g, function(m1){
// 				var return_data = "";
// 				if(m1.indexOf('•') > -1){
// 					m1 = m1.replace(/•/g, '');
// 					return_data += "<span class='remove_me' id='caret'>•</span>";
// 				}
// 				if(m1.length > 0){
// 					return_data += '<span style="color:' + ideconfig.variablecolor + '">' + m1 + '</span>';
// 				}
// 				return return_data;
// 			});
// 			textnode.replaceWith(text);
// 		}
// 	});
	
// 	return this;
// };

$.fn.reArrangeNewLine = function(evt_type){
	if(evt_type == 'paste'){
		$(this).children().each(function(index){
				if($(this).find('div').length > 0){
					var brs = '';
					$(this).children().each(function(index){
						if($(this).prop('tagName') == 'DIV'){
							if($(this).text().length < 1 && $(this).find('br').length < 1){
								// $(this).replaceWith('<div><br></div>');
								$(this).remove();
							}
							if($(this).find('br') && $(this).find('br').length > 1){
								brs = '';
								$(this).find('br').each(function(index){
									brs += '<div><br></div>';
								});
								$(this).replaceWith(brs);
							}
						}
					});
					var innerHTML = $(this).html();
					$(this).replaceWith(innerHTML);
				}
				if($(this).find('br') && $(this).find('br').length > 0){
					var brs = '';
					brs = '';
					if($(this).text().length > 1){
						$(this).find('br').each(function(index){
							//brs += '<div><br></div>';
							brs += '<div><br></div>';
							// $(this).remove();
						});
						$(brs).insertBefore($(this));
					}
					else{
						$(this).find('br').each(function(index){
							brs += '<div><br></div>';
						});
						$(this).replaceWith(brs);
					}
				}
		});
	}
	else if(evt_type == 'cut'){
		$(this).children().each(function(){
			if($(this).text().length < 1 && $(this).find('br').length > 0){
				$(this).replaceWith('<div><br></div>');
			}
			else if($(this).text().length < 1 && $(this).find('br').length < 1){
				$(this).prepend('<br>');
			}
		});
	}
	if(evt_type == 'paste'){
		if($(this).children().first().prop('tagName') == 'DIV'){
			$(this).children().each(function(index){
				if(!$(this).find('br')){
					$(this).identifyElement();
				}
			});
		}
		else{
			if($(this).children.length <= 0){
				$(this).children().each(function(index){
					// console.log("divs", $(this).html());
					$(this).identifyElement();
				});
			}
			else if($(this).first().prop('tagName') == 'BR'){
				$(this).wrap('<div></div>');
				$(this).children().each(function(index){
					// console.log("divs", $(this).html());
					$(this).identifyElement();
				});
			}
		}
		$(this).children().each(function(index){
			if($(this).prop('tagName') != 'DIV'){
	    		$(this).replaceWith('<div>' + $(this).html() + '</div>');
	    		$(this).identifyElement();
	    	}
    	});
	}
}

$.fn.formatLine = function(){
	var elem = this;
	var currentLine = '';
	if($(this).find('div').length > 0){
		$(this).find('div').each(function(){
			var html = $(this).html();
			if($(this).find('div').length > 0){

			}
			$(this).parent().replaceWith(html);
			// if($(this).find('div').length > 0){
			// 	$(this).find('div').each(function(){
			// 		if($(this).prop('tagName') == 'DIV'){
			// 			console.log('boom div');
			// 			var firstDiv = $(this);
			// 			// if($(this).find('div').length > 0){
			// 				if(currentLine == ''){
			// 					$(this).insertAfter($(elem));
			// 					currentLine = $(this);
			// 				}
			// 				else{
			// 					$(this).insertAfter($(currentLine));
			// 					currentLine = $(this);	
			// 				}
			// 			// }
			// 		}
			// 	});
			// }
		});
	}
}

$.fn.formatContent = function(element){
	$(this).children().each(function(index){
    	if($(this).html().length < 1){
    		$(this).remove();
    	}
    });
	if($(this).children().length < 1){
		if(lastPressedKey == "8"){
			$(this).html("<div><br></div>");
		}
		else{
			$(this).html("<div id='caret'>" + $(this).text() + "</div>");
			moveCaretAfter('caret');
		}
	}
	else if($(this).children().length == 2){
		if($(this).children().first().prop("tagName") == "BR"){
			$(this).children().first().replaceWith("<div><br></div>");
		}
	}
	else if($(this).find("div").length < 1){
		doSave(element);
		$(this).html("<div>" + $(this).html() + "</div>");
		doRestore(element);
	}
	else if($(this).find("div") > 0){
		if($(this).children().length < 1){
			// doSave(element);
			$(this).html("<div><br></div>");
			// doRestore(element);
		}
	}
};

$.fn.recompute = function(){
	//alert("set timeout");
	if($(this).html()){
		if($(this).html().length > 0){
			var text = $(this).html();
			text = text.replace(/\n/g, "~");
			text = text.replace(/\s/g, "&nbsp;");
			text = text.replace(/\>/g, '&gt;');
			text = text.replace(/\</g, '&lt;');
			$(this).html(text);
			$(this).identifyElement();
			//console.log(text.match(/\n/g));
			text = $(this).html();
			text = text.replace(/~/g, "</div><div>");
			text = "<div>" + text + "</div>";
			text = text.replace(/<div><\/div>/g, "<div><br></div>");
			//text = text.replace(/,&#32;/g, ' ');
			$(this).html(text);
		}
	}
	if($(this).attr('init') == 'true'){
		if($(this).text().length < 1){
			if($(this).find('br').length < 1){
				$(this).parent().trigger('click');
				$(this).insertTextAtCursor('<br>', 'element', 'div');
				// $('#focus_dre').focus();
			}
		}
		else{
			$(this).parent().trigger('click');
			moveCaretAfter('caret');
			$(this).removeAttr('init')
		}
		if(ideconfig.numbering == true){
			applyNumbering($(this).parent());
			highlightCurrentLine($(this));
		}
	}
}

$.fn.insertTextAtCursor = function(element, type, tagName) {
    var sel, range, html;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
			range = sel.getRangeAt(0);
			range.deleteContents();
            if(type == "text"){
            	var newNode = document.createTextNode(element);
            	insertNodeAtCaret(newNode);
            }    
            else if(type = "element"){
            	var elem = document.createElement(tagName);
            	elem.innerHTML = element;
            	insertNodeAtCaret(elem);
        	}
            //move the cursor
            
        }
    } else if (document.selection && document.selection.createRange) {
    	alert("document selection");
        if(type == "text"){
        	document.selection.createRange().text = text;
        }
        else{
        	var node = document.createElement('div');
        	node.innerHTML = element;
        	var children = node.getElementsByTagName('div');
        	for(i in children){
        		range.insertNode(children[i]);
        	}
    	}
    }
}

$.fn.insertTextAtCursorV2 = function(element, type, tagName) {
    var sel, range, html;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
			range = sel.getRangeAt(0);
			range.deleteContents();
            if(type == "text"){
            	var newNode = document.createTextNode(element);
            	insertNodeAtCaret(newNode);
            }    
            else if(type = "element"){
            	var elem = document.createElement(tagName);
            	elem.innerHTML = element;
            	insertNodeAtCaret(elem);
        	}
            //move the cursor
            
        }
    } else if (document.selection && document.selection.createRange) {
    	alert("document selection");
        if(type == "text"){
        	document.selection.createRange().text = text;
        }
        else{
        	var node = document.createElement('div');
        	node.innerHTML = element;
        	var children = node.getElementsByTagName('div');
        	for(i in children){
        		range.insertNode(children[i]);
        	}
    	}
    }
}

$.fn.identifyElement = function(){
	var getLine = $(this).text();
	//var html_entity = {'<':'&lt', '>':'&gt'};
	getLine = getLine.replace(/</g, '&lt;');
	getLine = getLine.replace(/>/g, '&gt;');
	// getLine = getLine.replace(/\-/g, '&#8209;');
	getLine = correctCapture(getLine);
	$(this).html(getLine);
	var str = $(this).html();
	$(this).html();
	// $(this).html(str.replace(/([<\w\|<\/]+font[^>]*>)/g,''));	//remove the autogenerated font tag
	$(this).wordify();
	$(this).captureKeywords();
}

$.fn.captureKeywords = function(){
	var words = formulas;
	var keywordlist = [];
	for(i in words){
		keywordlist.push(words[i].keyword_name);
	}
	var lowerCaseWords = $.map(keywordlist, function(n,i){return n.toLowerCase();});
	$(this).find("span").each(function(){
		if($.inArray($(this).text().toLowerCase(), lowerCaseWords) > -1){
			var i = $.inArray($(this).text().toLowerCase(), lowerCaseWords);
			$(this).text(words[i].keyword_name);
			$(this).css("color", ideconfig.keywordcolor);
			// $(words[i].params).insertAfter($(this));
		}
	});
}

$.fn.closeIDE = function(element){
	formulas = [];
	doSave(element.getElementsbyClassName('codeField')[0]);
	$(this).remove();
}

$.fn.initialize = function(settings){
	var config = {
		'keywordsjson': 'json/keywords.json',
		'width': $(this).width(),	// height of IDE
		'height': $(this).height(),	// width of IDE
		'zindex': '101',
		'backgroundcolor': '#BBBBBB',// bg of IDE
		'keywordcolor': '#0066AD',	// color of keyword
		'numbercolor': '#60BA46',	// color of number
		'stringcolor': '#D08000',	// color of string
		'commentcolor': '#555151',	// color of comment
		'variablecolor': '#BBBBBB',	// color of variable
		'font-size': '16px',	// IDE font size
		'numbering': false,	//enable numbering
		'autocomplete': false,	//enable autocomplete
		'menubar': false,	//enable menu bar
		'overlay': true,
		'console': true,
		'context_menu':false,
		'overflow':'hidden',
		'idecss': {	//style of IDE (codeField)
			'width': '94%',
			'font-family': 'monospace',
			'color': '#BBBBBB',
			'float': 'left',
			'padding-left': '5px',
			'outline': 'none',
			'position': 'auto',
			'word-wrap': 'normal',
			'white-space': 'nowrap'
		},
		'idecontainercss': {	//style of IDE Container
			'width': '99%',
			'height': '90%',
			'font-family': 'monospace',
			'color': '#BBBBBB',
			'float': 'left',
			'padding-left': '5px',
			'outline': 'none',
			'overflow': 'auto',
			'transition': 'height .5s'
		},
		'menucss':{	//style of menubar
			'width': '100%',
			'background-color': '#2c3e50'
		},
		'numberingcss': {	//style of numbering
			'width': '50px',
			'height': 'auto',
			'background-color': 'rgb(45, 45, 45)',
			'font-family': 'monospace',
			'color': '#888888',
			'float': 'left',
			'text-align':'right',
		},
		'highlightlinenumbercss': { //style of highlighted number
			'background-color': '#444444',
			'padding-left': '5px',
			'color': '#888888'
		},
		'linenumbercss': { //line number style
			'background-color': 'rgb(45, 45, 45)',
			'padding-left': '5px',
			'color': '#888888'
		},
		'autocompletecss': {	//style of autocomplete
			'width': '160px',
			'height': '150px',
			'font-family': 'monospace',
			'font-size': '16px',
			'background-color': 'white',
			'list-style-type': 'none',
			'padding': '0',
			'overflow': 'auto',
			'z-index': '199999999'
		},
		'selectedcss': {
			'background-color': '#00CC8B',
			'font-family': 'monospace'
		},
		'overlaycss': {
			'position': 'fixed',
			'background-color': '#333333',
			'width': window.innerWidth,
			'height': window.innerHeight,
			'opacity': '0.2',
			'z-index': '100'
		},
		'error_handlercss': {
			'width': '99%',
			'height': '24%',
			'font-family': 'monospace',
			'color': '#BBBBBB',
			'float': 'left',
			'padding-left': '5px',
			'padding-top': '5px',
			'outline': 'none',
			
		},
		'error_handler_headercss': {
			'background-color': 'rgb(44, 62, 80)',
			'padding-left':'3px',
			'height':'20px'
		},
		'error_handler_containercss': {
			'height': '81%',
			'background-color':'rgb(60, 60, 60)',
			'overflow': 'auto'
		},
		'error_handler_containerlinescss': {
			'border-bottom':'1px solid rgb(75, 75, 75)'
		},
		'context_menu_css': {
			'position': 'fixed',
			'z-index': '999999999',
			'width': '175px',
			'height': '200px',
			'background-color': 'rgb(127, 127, 140)'
		}
	}
	totalLines = 0;
	ideUndo = [];
	ideRedo = [];
	eventType = '';
	if(settings){
		$.extend(config, settings);
		$.extend(ideconfig, config);
	}
	if(ideconfig.menubar == true){
		setupMenuBar($(this));
	}
	var content = $("<div class='container'></div>");
	$(content).css(ideconfig.idecontainercss);
	$(this).append($(content));
	if(ideconfig["console"] == true){
		setupErrorHandler($(this));
	}
	if(ideconfig.numbering == true){
		setupNumbering($(content));
		isNumberingDisplayed = true;
	}
	setupCodeField($(content));
	if(ideconfig.overlay == true){
		setupOverlay($(this));
	}
	formulas= [];
	ideClipboard = '';
	getKeywordsFromJson(formula_keywords);
	getFieldNames($('.getFields'));
	if($('[data-ide-properties-type="picklist-condition"]').length > 0){
		if($('[name="picklist_selection_type"]').val() == "1"){
			loadChildFields($('#FormId').val(), $('[data-ide-properties-type="picklist-condition"]').closest('.content-dialog').find('.external_database_connection_return_field option:not([value="0"])').map(function(){ return $(this).attr('value')}).get().join(","));
		}
		else{
			loadChildFields($('#FormId').val(), $('[data-ide-properties-type="picklist-condition"]').closest('.content-dialog').find('.picklist_form_type option:selected').attr('fields'));
		}
	}
	if($.type(current_formula_type) != "undefined"){
		$('.codeField').attr("default-value", current_formula_type);
	}
	if($('.codeField').attr("default-value") == "embed-additional-filter-formula"){
		$('.embedProperty').find('.embed-source-lookup-field').val();
		$('.embedProperty').find('.embed-source-form').val();
		loadChildFields($('.embedProperty').find('.embed-source-form').val());
	}
	if(window.pathname){
		if(window.pathname.indexOf('workflow') > -1){
			loadChildFields($('#form_id').val());
		}
		if(window.pathname.indexOf("report") > -1){
			loadChildFields($('#form_id').val(), $("#active_fields").val());
		}
	}
	else{
		if(location.pathname.indexOf('workflow') > -1){
			loadChildFields($('#form_id').val());
		}
		if(location.pathname.indexOf("report") > -1){
			loadChildFields($('#form_id').val(), $("#active_fields").val());
		}
	}
	
	loadEvents($(this));
}

//--------------------------------------------functions--------------------------------------------//

function setupMenuBar(element){
	var menu = $("<div></div>");
	var formula_title = "";
	if(ide_titles[ide_selected_title_code]){
		formula_title = ide_titles[ide_selected_title_code]['title'];
	}
	var ul = $('<ul>'+
		'<li class="menu">File'+
			'<ul class="sublist">'+
				'<li style="display:none" class="disabled display">Import</li>'+
				'<li style="display:none" class="disabled display">Export</li>'+
				'<li class="hide_number	">Hide Line Number</li>'+
				'<li class="show_number">Show Line Number</li>'
		 	+'</ul>'+
		'</li>'+
		
		'<li class="menu">Edit'+
			'<ul class="sublist">'+
				'<li  style="display:none" class="comment">Comment</li>'+
				'<li class="copy">Copy</li>'+
				'<li class="cut">Cut</li>' +
				'<li class="paste">Paste</li>'
		 	+'</ul>'+
		'</li>'+
		'<li style="display:none" class="menu">Tools'+
			'<ul class="sublist">'+
				'<li style="display:none" class="disabled display">Change theme color</li>'+
				'<li style="display:none" class="disabled display">Font size</li>'
		 	+'</ul>'+
		'</li>'+
		'<li style="display:none" class="menu">Help'+
			'<ul class="sublist">'+
				'<li style="display:none" class="disabled">About formulas</li>'+
				'<li style="display:none" class="disabled">Documentation</li>'
		 	+'</ul>'+
		'</li>'+
		'<li class="menu compile">Validate'+
		'</li>'+
		'<li class="title" style="float:right;">'+
		formula_title + 
		'</li>'+
	'</ul>');

	menu.attr("id", "menu");
	menu.append(ul);
	menu.css(ideconfig.menucss);
	element.append(menu);
}

function setupCodeField(element){
	var css = {
		'width': ideconfig.width,
		'height': ideconfig.height,
		'background-color': ideconfig.backgroundcolor,
		'font-family': ideconfig.fontfamily,
		'font-size': ideconfig.fontsize,
		'overflow': ideconfig.overflow
	}
	var ide = $("<div></div>");
	ide.attr("spellcheck", "false");
	ide.attr("contenteditable", "true");
	ide.attr("class","codeField");
	ide.attr("init","true");
	ide.css(ideconfig.idecss);
	element.append(ide);
	$(element).parent().css(css);
}

function setupNumbering(element){
		var numbering = $("<div><div>1</div></div>");
		numbering.attr("class","numbering");
		numbering.css(ideconfig.numberingcss);
		element.append(numbering);
}

function setupOverlay(element){
	// var overlay = $("<div></div>");
	// overlay.css(ideconfig.overlaycss);
	// $(overlay).insertAfter(element);
}

function loadChildFields(form_id, fields){
	if($.type(form_id) != "undefined"){
		if($.type(fields) != "undefined"){
			var arr_fields = fields.split(",");
			AsyncLoop(arr_fields, function(a, b){
				var fieldName = {
					"keyword_name": "@" + b,
					"params": ""
				}
				formulas.push(fieldName);
			});
		}
		else{
			var json_params = {
				'form_id': form_id
			}
			$.post('/ajax/embed_field_list', json_params, function(result){
				var json = JSON.parse(result);
				try{
					var get_fields = json['active_fields'].split(',');
					var ctr = 0;
					var new_get_fields = [];
					AsyncLoop(get_fields, function(a, b){
						var fieldName = {
							"keyword_name": "@" + b,
							"params": ""
						}
						formulas.push(fieldName);
					});
				}
				catch(e){

				}
			});
		}
	}
}

function setupErrorHandler(element){
	var error_handler = $('<div class="error_handler">' + 
			'<div class="header"><span style="padding-top:4px;float:left;">Formula Checking...</span><span class="close_error_handler" style="float:right;cursor: pointer;padding-top:4px">Close</span>&nbsp;</div>' +
			'<div class="error_handler_container">' + 
			'</div>' + 
		'</div>');
	error_handler.css(ideconfig.error_handlercss);
	error_handler.find('.header').css(ideconfig.error_handler_headercss);
	error_handler.find('.error_handler_container').css(ideconfig.error_handler_containercss);
	error_handler.find('.error_handler_container').children().css(ideconfig.error_handler_containerlinescss);
	element.append(error_handler);
}

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

//numbering....
function applyNumbering(element){
	var totalLines;
	if($(element).find('.numbering').children().length < $(element).find('.codeField').children().length){
		totalLines = ($(element).find('.codeField').children().length - $(element).find('.numbering').children().length);
		for(var ctr = 0; ctr < totalLines; ctr++){
			addLineNumber(element);
		}
	}
	if($(element).find('.numbering').children().length > $(element).find('.codeField').children().length){
		totalLines = ( $(element).find('.numbering').children().length - $(element).find('.codeField').children().length);
		if($(element).find('.numbering').children().length > 1){
			for(var ctr = 0; ctr < totalLines; ctr++){
				removeLineNumber(element);
			}
		}
	}
	if($(element).find('.numbering').children().length == $(element).find('.codeField').children().length){
		if($(element).find('.codeField').children().length <= 0){
			addLineNumber(element);
		}
	}
}

function addLineNumber(element){
	var number = $(element).find('.numbering').children().length;
	if(number < 1){
		number = 1;
	}
	else{
		number += 1;
	}
	var lineNumber = $("<div>" + number + "</div>");
	lineNumber.css(ideconfig.linenumbercss);
	$(".numbering").append(lineNumber);
}

function removeLineNumber(element){
	$(element).find('.numbering').children().last().remove();
}

function highlightCurrentLine(element){
	
	var numbers = $(".numbering").children();
	var index = $(".codeField div").index(element);
	
	if(index > -1){
		if(prevIndex != index){
			if(prevIndex > index){
				$(numbers[prevIndex]).css(ideconfig.linenumbercss);
				$(numbers[index]).css(ideconfig.highlightlinenumbercss);
			}
			if(prevIndex < index){
				$(numbers[prevIndex]).css(ideconfig.linenumbercss);
				$(numbers[index]).css(ideconfig.highlightlinenumbercss);
			}

			prevIndex = index;
		}
	}
	else{
		$(numbers[index]).css("background-color","#999999");
	}
}

function showOrHideLineNumber(element){
	console.log("hide/show");
	if(isNumberingDisplayed == true){
		$(element).find(".numbering").animate({width: "0px"}, 300, function(){
			$(this).hide();
			$(element).find('.codeField').animate({width: "99%"}, 50);
		});
	}
	else{
		$(element).find(".numbering").show().animate({width: "50px"}, 300);
		$(element).find('.codeField').animate({width: "94%"}, 300);
	}
}

//autocomplete...
function autoComplete(element){
	if(element.text() != ""){
		autoCompleteHandler = generateSuggestions(element, formulas);
		
		if(autoCompleteHandler.children().length > 0){
			isAutocompleteDisplayed = true;
			$('.container').append(autoCompleteHandler);
			autoCompleteHandler.offset({top: element.offset().top + 20, left: element.offset().left + 10});
		}
		else{
			$('#autocomplete').remove();
			isAutocompleteDisplayed = false;
		}
	}
	else{
		isAutocompleteDisplayed = false;
	}
	
}

function generateSuggestions(element, words){
	var ul = $("<ul id='autocomplete' style='color:black'></ul>");
	$(ul).css(ideconfig.autocompletecss);
	var indexing = 0;
	for(w in words){
		if(words[w].keyword_name.toLowerCase().indexOf(element.text().toLowerCase()) > -1 && words[w] != element.text()){
			var li = "";
			if(indexing == 0){
				li = "<li class='suggestion' id='selected' params='" + words[w].params + "'>" + words[w].keyword_name + "</li>";
				indexing++;
			}
			else{
				li = "<li class='suggestion' params='" + words[w].params + "'>" + words[w].keyword_name + "</li>";
				indexing++;
			}
			ul.append(li);
		}
	}
	return ul;	
}

function checkSimpleErrors(code_field){
	var error= {};
	var getspans= $(code_field).find('span');
	var keywordlist = [];
	var params = [];
	for(i in formulas){
		keywordlist.push(formulas[i].keyword_name);
		params.push(formulas[i].params);
	}
	$(getspans).each(function(e){
		if($(this).text().indexOf('&') > -1){
			if($(this).text().indexOf('\"') < 0 && $(this).text().indexOf('\'') < 0){
				if($(this).text().match(/(&&([^\s])\w+)|(([^\s])\w+&&)|(&([^\s&])\w+)|(([^\s&])\w+&)/g)){
					error.status = false;
					error.message = "Syntax Error!... Invalid Conditional Statement!";
					var index = $(code_field).find('div').index(this.parentNode);
					error.line_number = index + 1;
					error.element = this.parentNode;
				}
				else if($(this).text().length < 2){
						error.status = false;
						error.message = "Syntax Error!... Invalid Operator \"&\"!";
						var index = $(code_field).find('div').index(this.parentNode);
						error.line_number = index + 1;
						error.element = this.parentNode;
				}
			}
		}
		if($(this).text().indexOf('|') > -1){
			if($(this).text().match(/(\|\|([^\s])\w+)|(([^\s])\w+\|\|)|(\|([^\s\|])\w+)|(([^\s\|])\w+\|)/g)){
				error.status = false;
				error.message = "Invalid statement";
				var index = $(code_field).find('div').index(this.parentNode);
				error.line_number = index + 1;
				error.element = this.parentNode;
			}
			else if($(this).text().length < 2){
				error.status = false;
				error.message = "Syntax Error!... Invalid Operator \"|\"!";
				var index = $(code_field).find('div').index(this.parentNode);
				error.line_number = index + 1;
				error.element = this.parentNode;
			}
		}
		if($.inArray("@" + $(this).text(), keywordlist) > -1){
			error.status = false;
			error.message = "Syntax Error!... Invalid Word \'" + $(this).text() + "\'!";
			var index = $(code_field).find('div').index(this.parentNode);
			error.line_number = index + 1;
			error.element = this.parentNode;
		}
		if($(this).text().indexOf('@') == 0 && $.inArray($(this).text(), keywordlist) > -1){
			var index = $.inArray($(this).text(), keywordlist);
			if(params[index].length > 0){
				if(removeIDESpaces($('.codeField').parent()).indexOf($(this).text() + '(') < 0){
					error.status = false;
					error.message = "Syntax Error!... Undefined Keyword \'" + $(this).text() + "\'!";
					var index = $(code_field).find('div').index(this.parentNode);
					error.line_number = index + 1;
					error.element = this.parentNode;
				}
			}
		}
	});
	
	return error;
}
function removeIDESpaces(element){
	var new_str = ''; 
	var str = getText(element);
	new_str = str.replace(/\s/g, '');
	new_str = str.replace(/\n/g, '');
	return new_str;
}

//highlighting.....

function getSelectionStart() {
   var node = document.getSelection().anchorNode;
   return (node.nodeType == 3 ? node.parentNode : node);
}

function findLineElement(){
	var element = getSelectionStart();
	//
	if($(element).prop("tagName") == "SPAN" || $(element).prop("tagName") == "FONT"){
		element = $(element).parent();
		return element;
	}
	else if($(element).prop("tagName") == "DIV"){
		if(!$(element).attr('class')){
			return element;
		}
		else if($(element).attr('class') == 'codeField'){
			if($(element).children().length < 1){
				$(element).prepend('<div><br></div>');
				element = $(element).children().first();
			}
			else{
				element = $(element).children().first();	
			}
			return element;
		}
	}
}
function correctCapture(str){
	//replace space to ,&#32;
	// str = str.replace(/\s/g, '&nbsp;');
	str = str.replace(/("(\\\"|[^"])*"|"(\\\"|[^"])*|'(\\\'|[^'])*'|'(\\\'|[^'])*|(\/\/[\w\s!@#$%^&*,-_+=\|.\/:;\'\"(){}\[\]\\]*)|((^|[\w])*(\d)*))/g, function(m1){
		// m1= m1.replace(/<[^>]*>/g, 'tae');
		if($.isNumeric(m1) == true){
			m1 = "<span style='color:" + ideconfig.numbercolor + "'>" + m1 + "</span>";
		}
		else if(m1.indexOf("//") == 0){
			m1 = "<span style='color:" + ideconfig.commentcolor + "'>" + m1 + "</span>";			
		}
		else if(m1.indexOf("\"") == 0 || m1.indexOf("\'") == 0){
			m1 = "<span style='color:" + ideconfig.stringcolor + "'>" + m1 + "</span>";
		}
		return m1;
	});
	return str;
}

function placeCaretAfterNode(node) {
    if (typeof window.getSelection != "undefined") {
        var range = document.createRange();
        range.setStartAfter(node);
        range.collapse(true);
        var selection = window.getSelection();
        selection.removeAllRanges();
        selection.addRange(range);
        
    }
}

function moveCaretAfter(id) {
	try{
	    document.getElementById(id).focus();
	    placeCaretAfterNode( document.getElementById(id));
	    document.getElementById(id).removeAttribute('id');
	}catch(err){

	}
}

function moveCaretAtStart(el, bool){
	var textRange = document.body.createTextRange();
    textRange.moveToElementText(el);
    textRange.collapse(bool);
    textRange.select();
}

function putNodes(element, node){
	range = document.createRange();
	range.selectNode(element);
	range.insertNode(node);
	element.removeAttribute('id');
}

function nextNode(node) {
    if (node.hasChildNodes()) {
        return node.firstChild;
    } else {
    	var pnode = node.parentNode;
        while (node && !node.nextSibling) {
            node = node.parentNode;
        }
        if (!node) {
            return null;
        }
        return node.nextSibling;
    }
}

function getRangeSelectedNodes(range) {
    var node = range.startContainer;
    var endNode = range.endContainer;
    if (node == endNode) {
    	if(node.nodeType == 3){
    		node = node.parentNode;
    		if($(node).prop("tagName") == "DIV"){
    			return [node];
    		}
    		else if($(node).prop("tagName") == "SPAN"){
    			node = node.parentNode;
    			return [node];
    		}
    	}
    }	
    var rangeNodes = [];
    while (node && node != endNode) {
    	node = nextNode(node)
    	rangeNodes.push(node);
    }

    node = range.startContainer;
    while (node && node != range.commonAncestorContainer) {
        rangeNodes.unshift(node);
        node = node.parentNode;
    }
	
    var divNodes = [];
    $(rangeNodes).each(function(index){
    	if(!$(this).attr("id") && $(this).prop("tagName") == "DIV"){
    		divNodes.push($(this));
    	}
    	else if(!$(this).attr("id") && $(this).prop("tagName") == "SPAN"){
    		if(index == 0){
    			divNodes.push($(this).parent());
    		}
    	}
    	else if(!$(this).attr("id") && this.nodeType == 3){
    		if(index == 0){
    			var parent = this.parentNode;
    			if(parentNode.nodeType == 1){
    				if($(parentNode).prop("tagName") == "DIV"){
    					divNodes.push($(parentNode));
    				}
    				else if($(parentNode).prop("tagName") == "SPAN"){
    					divNodes.push($(parentNode).parent());
    				}
    			}
    		}
    	}
    });
    return divNodes;
}

function getSelectedNodes() {
    if (window.getSelection) {
        var sel = window.getSelection();
        if (!sel.isCollapsed) {
            return getRangeSelectedNodes(sel.getRangeAt(0));
        }
    }
    return [];
}

function formatHistory(history){
	var new_history = [];
		for(i in history){
			if(typeof(history[i].value) != "undefined"){
				new_history.push(history[i]);
			}
		}
		return new_history;
}

function getText(element){
	if($(element).find(".codeField").html()){
		var text = $(element).find(".codeField").html();
		text = text.replace(/&nbsp;/g, ' ');
		text = text.replace(/<\/div>/g, '\n');
		text = text.replace(/<[^>]*>/g, '');
		text = text.replace(/&gt;/g, '>');
		text = text.replace(/&lt;/g, '<');
		text = text.replace(/&amp;/g, '&');
		text = text.replace(/&#8722;/g, '-');
		var newtext = text.substring(0, text.length - 1);
		return newtext;
	}
	else{
		return '';
	}
}

function strictValidation(formula_container){
	var formula = getText(formula_container);
	if(formula.length > 0){
		formula = formula.replace(/"(\\\"|[^"])*"|"(\\\"|[^"])*|'(\\\'|[^'])*'|'(\\\'|[^'])*/g, '');
		if(formula.indexOf('@') > -1){
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return true;
	}
}
//-----------------------------------------error handling----------------------------------------//

var formulaChecker = {
	"checkFormula": function(element){
		var formula_list_tag_id = ['flist-field-equivalent-formula','flist-field-equivalent-formula-placeholder', 'flist-field-equivalent-formula-changelist'];
	    var check_formula = {status: true, message: "Valid"};
	    var input_formula = getText(element);
	    check_formula = client_formula.checkFormulaErrors(input_formula);
	    
	    if($('.idepos').eq(0).is('[id="'+formula_list_tag_id.join('"],[data-ide-properties-type="')+'"]')){
    		var invalid_formula_for_formula_list = ['\\.formatDate','\\.adjustDay','\\.adjustMonth','\\.adjustYear','\\.adjustMinutes','\\.adjustHours','\\.diffDaysIn','\\.diffMonthsIn','\\.diffWeeksIn','\\.diffYearsIn'];
    		var regexp_checker =  new RegExp( invalid_formula_for_formula_list.join("|") , "g" );
    		var input_formula_line = input_formula.split('\n');
    		var matches = []; //input_formula.match(regexp_checker)||
    		var error = [];
			for(var line_no in input_formula_line){
				matches = input_formula.match(regexp_checker)||[];
				if(matches.length >= 1){
					error.push({"line":Number(line_no)+1,"message":"Not allowed to use submethods: " + matches.join(" , ")});
				}
			}
    		if(error.length >= 1){
    			$('.codeField').attr( "custom_error_line", error[0]["line"] );
    			check_formula = {status: false, message: error[0]["message"] };//{status: false, message: "Not valid to use submethods for formula list: " + invalid_formula_for_formula_list.join(",").replace(/\\/g,"") };
    		}
	    }
	    if(strictValidation($(element)) == false){
	    	check_formula = {status: false, message: "Strict Validation Error. No formula process Found! Use Static Instead.","line_number": "(NONE)" };
	    }

	    return check_formula;
    },
    "none": function(){

    }
}
//-----------------------------------------------------------------------------------------------//

//-----------------------------------------context Menu------------------------------------------//

var context_menu = {
	"initialize": function(css, posx, posy){
		if($('.ide_context').length < 1){
			context_menu.setupOverlay();
			var ide_context = $('<div class="ide_context"> ' + 
				'<ul>' + 
					'<li class="menu copy" style="position:relative;height:10%;padding-top:5%;padding-bottom:5%;padding-left:2%;font-size:14px;">Copy' + 
					'</li>' + 
					'<li class="menu cut" style="position:relative;height:10%;padding-top:5%;padding-bottom:5%;padding-left:2%;font-size:14px">Cut' + 
					'</li>' + 
					'<li class="menu paste" style="position:relative;height:10%;padding-top:5%;padding-bottom:5%;padding-left:2%;font-size:14px">Paste' + 
					'</li>' + 
					'<li class="menu hide_number" style="position:relative;height:10%;padding-top:5%;padding-bottom:5%;padding-left:2%;font-size:14px;">Hide Line Number' + 
					'</li>' + 
					'<li class="menu show_number" style="position:relative;height:10%;padding-top:5%;padding-bottom:5%;padding-left:2%;font-size:14px">Show Line Number' + 
					'</li>' + 
				'</ul>' + 
			'</div>');
			css.top = posy;
			css.left = posx;
			$(ide_context).css(css);
			$('body').append($(ide_context));
			context_menu.loadEvents($('.ide_context'));
		}
	},
	"setupOverlay":function(){
		$('body').append('<div class="ide_context_overlay" style="position:fixed;z-index:999999990;left:0px;top:0px;width:' + $(window).width() + 'px;height:' + $(window).height() + 'px;background-color:rgb(45, 45, 45);opacity:0.3">context menu!</div>');
	},
	"removeContext": function(){
		if($('.ide_context').length > 0){
			$('.ide_context_overlay').remove();
			$('.ide_context').remove();
		}
	},
	"loadEvents": function(element){
		$(element).find('.copy').bind('click', function(){
			ideClipboard = window.getSelection().toString();
		});
		$(element).find('.cut').bind('click', function(){
			var sel = window.getSelection ? window.getSelection() : document.selection;;
			ideClipboard = window.getSelection().toString();
			var a = $('<div contenteditable="true"></div>');
			// $(a).text(ideClipboard);
			// $(a).recompute();
		    range = window.getSelection().getRangeAt(0);
		    range.deleteContents();
		    // range.insertNode(document.createElement('br'));
		    if($(findLineElement()).text().length < 1){
		    	$(findLineElement()).prepend('<br>');
		    	$(findLineElement()).focus();
		    }
		    $('.codeField').find('div').each(function(index){
		    	if($(this).html().length < 1){
		    		$(this).remove();
		    	}
		    	else{
		    		if($(this).find('br').length  < 1){
		    			if($(this).text().length < 1){
		    				$(this).remove();
		    			}
		    		}
		    	}
		    });
		    applyNumbering(element);
		});
	}
}
//-----------------------------------------------------------------------------------------------//


//--------------------------------------------events---------------------------------------------//
$(document).on("mousedown", '#selected', function(e){
	//
	eventType = 'mousedown';
	if(ideconfig.numbering == true){
		highlightCurrentLine($(findLineElement()));
	}
	e.preventDefault();
	$("#selected").css("background-color", "#00CC8B");
	
	current_element = $(getSelectionStart());
	$(current_element).text($("#selected").text());
	$(current_element).css("color", ideconfig.keywordcolor);
	//set the caret to the last position of a keyword
	$(current_element).attr("id","caret");
	var caret = document.getElementById("caret")
	caret.focus();
	var codeField;
	if(hasClass(caret.parentNode, 'codeField')){
		codeField = caret.parentNode;
	}
	else{
		codeField = caret.parentNode.parentNode;
	}
	moveCaretAfter("caret");
	//when indentifying element, the caret will go to the first position of the element.
	//we need to return it to the keyword.
	doSave(codeField);
	if($(current_element).next()){
		$(current_element).insertTextAtCursor($('#selected').attr('params'), 'text');
		$(findLineElement()).identifyElement();
	}
	//return the caret to the last position of keyword
	doRestore(codeField);
	$(autoCompleteHandler).remove();
	isAutocompleteDisplayed = false;
});

$(document).on("mouseover", ".suggestion", function(){
	eventType = 'mouseover';
	$(this).css("cursor", "pointer");
	$(this).attr("id", "selected");
});

$(document).on("mouseout", ".suggestion", function(){
	eventType = 'mouseout';
	$(this).css("cursor", "default");
	$("#selected").removeAttr("id");
});

$(document).on('mousedown', '#autocomplete', function(e){
	return false;
});

$(document).on("blur", ".codeField", function(e){
	eventType = 'blur';
	blurred = true;
	e.preventDefault();
	//doSave(this);
	if(ideconfig.autocomplete == true){
		if(isAutocompleteDisplayed == true){
			$(autoCompleteHandler).remove();
			isAutocompleteDisplayed = false;
		}
	}
});

$(document).on("mousedown", ".ide_context_overlay", function(e){
	if(ideconfig.context_menu == true){
		context_menu.removeContext();
	}
});

// window.onload = function (){
// 	alert("boom");
// };

// window.onfocus = function (){
// 	alert("kaboom");
// }

window.onerror = function(errorMsg, url, lineNumber){
	if(url.indexOf($('#current-user-id').text()) > -1){
		$('.codeField').attr('error_line', lineNumber);
		setTimeout(function(){
			$('.numbering').find('div').eq(lineNumber-1).css({'background-color': 'rgb(141, 25, 25)', 'transition' : 'all 0.1s ease', '-webkit-transition': 'all 0.1s ease'});
			$('.codeField').find('div').eq(lineNumber-1).css({'background-color': 'rgb(141, 25, 25)', 'transition' : 'all 0.1s ease', '-webkit-transition': 'all 0.1s ease'});
		}, 1000);
		setTimeout(function(){
			$('.numbering').find('div').eq(lineNumber-1).css({'background-color': 'rgb(45, 45, 45)', 'transition' : 'all 2s ease', '-webkit-transition': 'all 2s ease'});
			$('.codeField').find('div').eq(lineNumber-1).css({'background-color': 'rgb(45, 45, 45)', 'transition' : 'all 2s ease', '-webkit-transition': 'all 2s ease'});
		}, 2000);



	}
}
// $(document).on("click", ".codeField", function(e){
// 	e.stopImmediatePropagation();
// 	e.preventDefault();
// });

function loadEvents(element){
	$(element).keydown(function(e){
		eventType = 'keydown';
		current_element = $(getSelectionStart());
		selected_element = $(findLineElement());
		lastPressedKey = '' + e.keyCode;
		// if(e.keyCode == 27){
			// return false;
		// }
		if(e.keyCode == '9'){
			e.preventDefault();
			$(findLineElement()).insertTextAtCursor('&nbsp;&nbsp;&nbsp;&nbsp;', 'element', 'span');
		}
		if(ideconfig.autocomplete == true){
			if(isAutocompleteDisplayed == true){
				var arrowKeyCode = ["37", "38", "39", "40", "13"];
				if (ignoreKey){
			        e.preventDefault();
			        return;
			    }
			    if ($.inArray(""+e.keyCode, arrowKeyCode) > -1){
			        var pos = this.selectionStart;
			        this.value = (e.keyCode == 38?1:-1)+parseInt(this.value,10);
			        this.selectionStart = pos; this.selectionEnd = pos;
			        ignoreKey = true; setTimeout(function(){ignoreKey=false},1);
			        e.preventDefault();
			    }
				$("#autocomplete").focus();
				if($.inArray(""+e.keyCode, arrowKeyCode) > -1){
					var index = $("#autocomplete li").index($("#selected"));
					if(e.keyCode == "40"){
						if(index < $("#autocomplete").children().length -1){
							var next = $("#selected").next();
							document.getElementById("selected").removeAttribute("id");
							$(next).attr("id", "selected");
						}
						else{
							$("#autocomplete").remove();
							isAutocompleteDisplayed = false;
						}
					}
					else if(e.keyCode == "38"){
						if(index > 0){
							var next = $("#selected").prev();
							document.getElementById("selected").removeAttribute("id");
							$(next).attr("id", "selected");
						}
						else{
							$("#autocomplete").remove();
							isAutocompleteDisplayed = false;
						}
					}
					if($("#autocomplete").length > 0){
						var b1 = document.getElementById("autocomplete").getBoundingClientRect();
			        	var b2 = document.getElementById("selected").getBoundingClientRect();
			        	if (b1.top > b2.top) {
			            	$("#autocomplete").scrollTop($("#autocomplete").scrollTop() - b1.top + b2.top);
			        	}
			        	else if (b1.bottom < b2.bottom) {
			            	$("#autocomplete").scrollTop($("#autocomplete").scrollTop() - b1.bottom + b2.bottom);
			        	}
		        	}
				}
				if(e.keyCode == "13"){
					e.preventDefault();
					if(e.shiftKey == true &&  ($("#selected").attr('params')||"") == ""){
						var temp = $("#selected").text().replace("@", "\"") + "\"";
						$(current_element).text(temp);
					}
					else{
						$(current_element).text($("#selected").text());
					}
					// $(current_element).css("color", ideconfig.keywordcolor);
					$(current_element).attr("id","caret");
					var caret = document.getElementById("caret")
					caret.focus();
					var codeField;
					if(hasClass(caret.parentNode, 'codeField')){
						codeField = caret.parentNode;
					}
					else{
						codeField = caret.parentNode.parentNode;
					}
					moveCaretAfter('caret');
					//when indentifying element, the caret will go to the first position of the element.
					//we need to return it to the keyword.
					doSave(codeField);
					if($(current_element).next()){
						$(current_element).insertTextAtCursor($('#selected').attr('params'), 'text');
						$(findLineElement()).identifyElement();
					}
					else{
						$(current_element).css("color", ideconfig.keywordcolor);
					}
					//return the caret to the last position of keyword
					doRestore(codeField);
					$(autoCompleteHandler).remove();
					isAutocompleteDisplayed = false;
					
				}
			}
		}
		if(e.ctrlKey == true && (e.keyCode == "90" || e.keyCode == "89")){
			e.preventDefault();
			var newElem = this;
			doSave(newElem.getElementsByClassName('codeField')[0]);
			if(e.keyCode == "90"){
				if(ideUndo.length > 0){
					var index = ideUndo.length-1;
					$(element).find('.codeField').html(ideUndo[index].value);
					ideRedo.push(ideUndo[ideUndo.length-1]);
					delete ideUndo[index];
					ideUndo	= formatHistory(ideUndo);
				}
			}
			else if(e.keyCode == "89"){
				if(ideRedo.length > 0){
					var index = ideRedo.length - 1;
					$(element).find('.codeField').html(ideRedo[index].value);
					ideUndo.push(ideRedo[ideRedo.length-1]);
					delete ideRedo[ideRedo.length-1];
					ideRedo = formatHistory(ideRedo);
				}
			}
			doRestore(newElem.getElementsByClassName('codeField')[0]);
			applyNumbering(this);
		}
	});

	$(element).keyup(function(e){
		eventType = 'keyup';
		if(e.keyCode == 46){

			e.preventDefault();
		}
		if(e.keyCode == 27){
			$(element).parent().find('.ideclose').trigger('click');
			return false;// e.stopImmediatePropagation();
		}
		if($(this).find('.codeField').text().length < 1 && $(this).find('.codeField').find('br').length < 1){

		}
		if(ideconfig.numbering == true){
			var arrowKeyCode = ["37", "38", "39", "40"];
			if($.inArray(""+e.keyCode, arrowKeyCode) > -1){
				highlightCurrentLine($(findLineElement()));
			}
		}
	});
	$(element).bind("contextmenu",function(e){
		e.preventDefault();
		if(ideconfig.context_menu == true){
			context_menu.initialize(ideconfig.context_menu_css, e.pageX, e.pageY);
		}
		else{
			$(element).find('.error_handler').find('.header').find('span').first().html("Console");
			$(element).find('.error_handler_container').empty();
			$(".container").css('height', '65%');
			$(element).find('.error_handler_container').append('<div style="background-color:rgb(141, 25, 25);padding-left:5px;">Right click is not Allowed...</div>');
			return false;
		}
	});

	if(ideconfig.menubar == true){
		$(element).find(".comment").bind("mousedown", function(e){
			eventType = 'mousedown';
			e.preventDefault();
			 	
			var selectedLines = getSelectedNodes();
			var ide = this.parentNode.parentNode.parentNode.parentNode.parentNode;
			var codef = ide.getElementsByClassName('codeField')[0];
			doSave(codef);
			if(selectedLines.length > 0){
				$(selectedLines).each(function(index){
					$(this).prepend("//").identifyElement();
				});
			}
			else{
				$(findLineElement()).prepend("//").identifyElement();
			}
			doRestore(element.getElementsByClassName('codeField')[0]);

		});
		$(element).find(".hide_number").bind("mousedown",function(e){
			eventType = 'mousedown';
			console.log("hide");
			isNumberingDisplayed = true;
			showOrHideLineNumber($(element));
			
		});
		$(element).find(".show_number").bind("mousedown",function(e){
			eventType = 'mousedown';
			console.log("show");
			isNumberingDisplayed = false;
			showOrHideLineNumber($(element));

		});
		$(element).find(".menu").bind("mousedown", function(e){
			eventType = 'mousedown';
			return false;
		});
	}

	$(element).bind("input", function(e){
		var sel = window.getSelection();
		var node
		if(sel.focusNode.parentNode.tagName == 'SPAN'){
			node = sel.focusNode.parentNode.parentNode;
		}
		else if(sel.focusNode.parentNode.tagName == 'DIV'){
			node = 	sel.focusNode.parentNode;
		}
		if(ideconfig.autocomplete == true){
			$(autoCompleteHandler).remove();
			autoComplete($(getSelectionStart()));
		}
		if($(this).find(".codeField").text() == "" && $(this).find(".codeField").children().length < 1){
			e.preventDefault()
			// console.log('preventDefault1');
			isAutocompleteDisplayed = false;
			$(this).find(".codeField").empty();
			$(this).find('.container').trigger('click');
			$(this).insertTextAtCursor('<br>', 'element', 'div');
		}
		else if($(this).find(".codeField").text() == "" && $(this).find(".codeField").children().length < 2){
			e.preventDefault();
			// console.log('preventDefault2');
			if($(this).find(".codeField").children().first().prop('tagName') == 'DIV'){
				if($(this).find(".codeField").children().first().find('br').length <= 0){
					$(this).find('.container').trigger('click');
					$(this).insertTextAtCursor('', 'element', 'br');
				}
			}
			else if($(this).find(".codeField").children().first().prop('tagName') == 'BR'){
				$(this).find(".codeField").empty();
				$(this).find('.container').trigger('click');
				$(this).insertTextAtCursor('<br>', 'element', 'div');
			}
			// $(this).find('.container').trigger('click');
			isAutocompleteDisplayed = false;
			// return false;
		}
		else{
			$(this).find(".codeField").formatContent(this.getElementsByClassName('codeField')[0]);
			if(lastPressedKey == "8" || lastPressedKey == "13"){
				if(lastPressedKey == "13"){
					e.preventDefault();
					// var currentSpan = $(findLineElement()).children()[0];
					// $(currentSpan).attr('id','caret');
					// moveCaretAfter('caret');
				}
				if(firstLineChar != $(findLineElement()).text()[0]){
					e.preventDefault();
					firstLineChar = $(findLineElement()).text()[0];
				}
				else if($(findLineElement()).prev().text().length < 1){
					e.preventDefault();
				}
				else{
					if($(findLineElement()).text().length > 0){
						if(lastPressedKey == "8"){
							doSave(this.getElementsByClassName('codeField')[0]);
						}
						$(findLineElement()).identifyElement();
						if(lastPressedKey == "8" && $(this).find(".codeField").text() != ""){
							doRestore(this.getElementsByClassName('codeField')[0]);
						}
					}
					else{
						e.preventDefault();
					}
				}
			}
			if(lastPressedKey != "13" && lastPressedKey != "8"){
				doSave(this.getElementsByClassName('codeField')[0]);
				$(findLineElement()).identifyElement();
				doRestore(this.getElementsByClassName('codeField')[0]);
			}
		}
		if(ideconfig.numbering == true){
			applyNumbering($(element));
			try{
				highlightCurrentLine($(findLineElement()));
			}
			catch(e){
				// $(this).find('.container').trigger('click');
			}
		}
		if(eventType == "paste"){
			doSave(this.getElementsByClassName('codeField')[0]);
			$(this).find('.codeField').reArrangeNewLine(eventType);
			doRestore(this.getElementsByClassName('codeField')[0]);
		}
		if(eventType == "cut"){
			// console.log('findLineElement', $(findLineElement()).html());
			if($(this).find('.codeField').children().length  > 0){
				$(this).find('.codeField').children().each(function(){
					if($(this).text().length < 1 && $(this).find('br').length  < 1){
						$(this).prepend('<br>');
					}
				});
			}
			else{
				$(this).find('.codeField').prepend('<div><br></div>')
			}
			// e.preventDefault();
			// doSave(this.getElementsByClassName('codeField')[0]);
			// $(this).find('.codeField').reArrangeNewLine(eventType);
			// doRestore(this.getElementsByClassName('codeField')[0]);
		}
		lastPressedKey = '';
		// applyNumbering(this);
		if($(this).find('.codeField').text().length){
			ideUndo.push({
				"value": this.getElementsByClassName('codeField')[0].innerHTML
			});
		}
	});
	$(element).find('.codeField').bind('click', function(e){
		e.stopImmediatePropagation();
		e.preventDefault();
		selected_element = $(findLineElement());
		highlightCurrentLine($(selected_element));
	});
	$(element).find('.container').bind('mousedown', function(e){
		if(e.which == 3){
			$(this).find('.codeField').focus();	
			$(this).find('.codeField').children().last().attr('id', 'caret');
			moveCaretAfter('caret');
			return false;
		}
	}).bind('click', function(e){
		e.stopImmediatePropagation();
		e.preventDefault();
		$(this).find('.codeField').focus();	
		$(this).find('.codeField').find('div').last().attr('id', 'caret');
		moveCaretAfter('caret');
	});
	$(element).bind("click", function(e){
		eventType = 'click';
		e.preventDefault();
		current_element = $(getSelectionStart());
		selected_element = $(findLineElement());
		if(ideconfig.numbering == true){
			highlightCurrentLine($(selected_element));
		}
		if(ideconfig.autocomplete == true){
			if(isAutocompleteDisplayed == true){
				if($(current_element)[0] != $(getSelectionStart())[0]){
					$(autoCompleteHandler).remove();
					isAutocompleteDisplayed = false;
				}
			}
		}
		// if($('.numbering').data('clicked') == false){
		// 	$(this).focus();
		// 	$(this).find(".codeField").focus();
		// 	$(this).find(".codeField").children().last().attr('id','caret');
		// 	moveCaretAfter('caret');
		// }

		if(blurred == true){
			//doRestore(this.getElementsByClassName('codeField')[0]);
			blurred = false;
		}
	}).bind("mousemove", function(){
		eventType = 'mousemove';
		$(this).css("cursor","text");
	}).bind("mouseout", function(){
		eventType = 'mouseout';
		$(this).css("cursor","default");
	})
	.bind("cut", function(e){
		eventType = 'cut';
		if(!e.hasOwnProperty("originalEvent")){
			e.preventDefault();
			var sel = window.getSelection ? window.getSelection() : document.selection;;
			ideClipboard = window.getSelection().toString();
			var a = $('<div></div>');
			// $(a).text(ideClipboard);
			// $(a).recompute();
		    range = window.getSelection().getRangeAt(0);
		    range.deleteContents();
		    // range.insertNode(document.createElement('br'));
		    var current_line = $(findLineElement());
		    if(current_line.text().length < 1 && current_line.find('br').length < 1){
		    	$(this).insertTextAtCursor('','element','br');
		    }
		    $(this).find('.codeField').find('div').each(function(index){
		    	if($(this).html().length < 1){
		    		$(this).remove();
		    	}
		    	else{
		    		if($(this).find('br').length  < 1){
		    			if($(this).text().length < 1){
		    				$(this).remove();
		    			}
		    		}
		    	}
		    });
		    applyNumbering(element);
		    // ideClipboard = $(a).html();
		}
		else{
			// if($(findLineElement()).text().length < 1){
		 //    	$(findLineElement()).prepend('<div><br></div>');
		 //    	// $(findLineElement()).focus();
		 //    }
		}
	}).bind("copy", function(e){
		eventType = 'copy';
		if(!e.hasOwnProperty("originalEvent")){
			e.preventDefault();
			ideClipboard = window.getSelection().toString();
		}
		else{
			e.originalEvent.clipboardData.setData('Text', "1234567890");
		}
	}).bind("paste", function(e){
		eventType = 'paste';
		if(!e.hasOwnProperty('originalEvent')){
			if(ideClipboard.length > 0){
				e.preventDefault();
				$(this).insertTextAtCursor(ideClipboard, 'text');
				var elem = this;
				var a = $('<div></div>');
				a.html($(findLineElement()).text());
				$(a).recompute();
				$(a).children().last().attr('id', 'caret');
				$(findLineElement()).replaceWith($(a).html());
				moveCaretAfter('caret');
				applyNumbering(element);
			}
		}
		else{
			e.preventDefault();
			if(!$(e.target).parents('.codeField').length > 0){
				return;
			}
			// console.log("clipboard",e.originalEvent.clipboardData.getData('text/html'));
			ideClipboard = e.originalEvent.clipboardData.getData('text/plain');
			var last_character = ideClipboard[ideClipboard.length - 1];
			ideClipboard = ideClipboard.substring(0, Number(ideClipboard.length - 1));
			var current_line = $(findLineElement());

			var a = $('<div></div>');
			// console.log("last character", last_character);
			// console.log("clipboard", ideClipboard);
			var text = current_line.text();
			text = text.replace(/&nbsp;/g, ' ');
			text = text.replace(/<\/div>/g, '\n');
			text = text.replace(/<[^>]*>/g, '');
			text = text.replace(/&gt;/g, '>');
			text = text.replace(/&lt;/g, '<');
			text = text.replace(/&amp;/g, '&');
			text = text.replace(/&#8722;/g, '-');
			var lines;
			// console.log("clipboard", ideClipboard);
			$(this).insertTextAtCursor(ideClipboard + '•', 'text');
			// console.log("current line", current_line.text());
			if(current_line.text().match(/\r\n/g)){
				lines = current_line.text().split(/\r\n/g);
				current_line.text('');
				for(var i = 0; i < lines.length; i++){
					if(i == 0){
						current_line.text(lines[i]);
						current_line.identifyElement();
					}
					else{
						var new_line = $();
						// console.log("lines", lines[i], "length", lines[i].length);
						if(lines[i].length > 0){
							lines[i] = lines[i].replace(/</g, '&lt;');
							lines[i] = lines[i].replace(/>/g, '&gt;');
							new_line = $('<div>' + lines[i] + '</div>');
							new_line.identifyElement();
						}
						else{
							new_line = $('<div></br></div>');
						}
						current_line.after(new_line);
						current_line = new_line;
					}
				}
				console.log("caret length", $('#caret').text().length, last_character, last_character.match(/\r|\n/g));
				if(last_character.match(/\r|\n/g)){
					if($('#caret').parent().text().length <= 0){
						$('#caret').html('</br>');
						moveCaretAfter('caret');
					}
					else{
						$('#caret').html(last_character);
						moveCaretAfter('caret');
						doSave(this);
						$(findLineElement()).identifyElement();
						doRestore(this);
					}
				}
				else{
					console.log("hindi nag match e...");
					$('#caret').html(last_character);
					moveCaretAfter('caret');
					doSave(this);
					$(findLineElement()).identifyElement();
					doRestore(this);
				}
				
			}
			else{
				doSave(this);
				current_line.text(current_line.text().replace(/•/g, last_character));
				$(findLineElement()).identifyElement();
				doRestore(this);
			}
			// if(text.length > 0){
			// insert the copied text with extra character to find the ending of the text
			// 	$(this).insertTextAtCursor(ideClipboard + '•', 'text');
			// 	a.html(current_line.text());
				// a.recompute();
			// 	a.find(':contains("•")').map(function(){
			// 	    if($(this).prop('tagName') == "SPAN"){
			// 			$(this).attr('id', 'caret');
			// 			$(this).attr('class', 'remove_me');
			// 	    }
			// 	});
			// 	current_line.replaceWith(a.html());
			// 	moveCaretAfter('caret');
			// 	$('.remove_me').remove();
			// 	applyNumbering($(this).parent());
			// }
			// else{
			// 	a.html(ideClipboard);
			// 	a.recompute();
			// 	if($(a).find('div').length > 0){
			// 		$(a).find('div').last().append('<span class="remove_me" id="caret">&8203;</span>');
			// 	}
			// 	else{
			// 		$(a).append('<span class="remove_me" id="caret">&8203;</span>');
			// 	}
			// 	current_line.replaceWith($(a).html());
			// 	moveCaretAfter('caret');
			// 	$('.remove_me').remove();
			// 	applyNumbering($(this).parent());
			// }

			//insert the copied text with extra character to find the ending of the text
			// $(this).insertTextAtCursor(ideClipboard.replace(/\n/g, '</br>'), 'text');
			// current_line.recompute();
			// doSave(this);
			// a.html(current_line.text());
			// a.recompute();
			// console.log("html", a.html());
			// a.find(':contains("•")').map(function(){
			//     if($(this).prop('tagName') == "SPAN"){
			//     	console.log("eto yun...", $(this).html());
			// 		$(this).attr('id', 'caret');
			// 		$(this).attr('class', 'remove_me');
			//     }
			// });
			// current_line.replaceWith(a.html());
			// var rmv_me = $('.remove_me');
			// rmv_me.text(rmv_me.text().replace(/•/g, last_character));
			// rmv_me.parent().identifyElement();
			// moveCaretAfter('caret');
			// rmv_me.removeClass('remove_me');
			// var remove_parent = rmv_me.parent();
			// if(remove_parent.children().length <= 0){
			// 	insertNodeAtCaret(document.createElement('BR'));
			// }
			// $(findLineElement).identifyElement();
			// doRestore(this);
			applyNumbering($(this).parent());
			eventType = 'paste';
		}
		
	}).bind("drop", function(e){
		e.preventDefault();
		return false;
	});

	$(element).find(".copy").bind("mousedown",function(e){
		eventType = 'mousedown';
		$(this).closest('.numbering').data('clicked', true);
		e.preventDefault();
		$(this).trigger('copy');
		
	});
	$(element).find(".paste").bind("mousedown",function(e){
		eventType = 'mousedown';
		$(this).closest('.numbering').data('clicked', true);
		e.stopImmediatePropagation();
		e.preventDefault();
		$(this).trigger('paste');

	});
	$(element).find(".cut").bind("mousedown",function(e){
		eventType = 'mousedown';
		$(this).closest('.numbering').data('clicked', true);
		e.stopImmediatePropagation();
		e.preventDefault();
		$(this).trigger('cut');

	});
	$(element).find('.close_error_handler').click(function(){
		$(element).find('.container').css('height', '90%');
	});
	if($('.codeField').attr('default-value') != 'static'){
		$(element).find('.compile').click(function(){
			if($('.codeField').attr('default-value') != 'object_css'){
				var formula_test = formulaChecker.checkFormula($(element));
				$.ajax({
					type: 'POST',
					url: '/ajax/create_formula_file',
					data: {
						text: formula_test.formula,
						user: $('#current-user-id').text(),
						file: 'compile'
					},
					success: function(e){
						if(e != 'false'){
							$(element).find('.error_handler_container').empty();
							var script = $('<script></script>');
							$('body').append(script);
							$(script).attr('src', e);
							$(script).remove();
							$(element).find('.error_handler').find('.header').find('span').first().html("Formula Checking...");
							$(element).find('.error_handler_container').append('<div style="background-color:rgb(45, 45, 45);padding-left:5px;">Please wait...<span></span></div>');
							$(element).find('.container').css('height', '65%');
							$(element).find('.container').find('.codeField').removeAttr('error_line');
							var getError = {};
							setTimeout(function(){
								$(element).find('.error_handler_container').empty();
								if(formula_test.status == true){
									// condition for other errors
									var get_error = checkSimpleErrors($(element).find('.codeField'));
									getError = get_error;
									$(element).find('.error_handler').find('.header').find('span').first().html("Console");
									if(get_error.status == false){
										$(element).find('.codeField').attr('error_line', get_error.line_number);
										$(element).find('.numbering').find('div').eq(get_error.line_number - 1).css({'background-color': 'rgb(141, 25, 25)', 'transition' : 'all 0.1s ease', '-webkit-transition': 'all 0.1s ease'});
										$(element).find('.codeField').find('div').eq(get_error.line_number - 1).css({'background-color': 'rgb(141, 25, 25)', 'transition' : 'all 0.1s ease', '-webkit-transition': 'all 0.1s ease'});
										$(element).find('.error_handler_container').append('<div style="background-color:rgb(141, 25, 25);padding-left:5px;">' + get_error.message + '<span style="float:right"> Line number: ' + get_error.line_number + '</span></div>');
									}
									else{
										$(element).find('.error_handler_container').append('<div style="background-color:green;padding-left:5px;">Formula is Valid...<span></span></div>');
										$(element).find('.codeField').find('div').removeAttr('style');
										$(element).find('.numbering').find('div').removeAttr('style');
									}
								}
								else{
									$(element).find('.error_handler').find('.header').find('span').first().html("Console");
									$(element).find('.error_handler_container').append('<div style="background-color:rgb(141, 25, 25);padding-left:5px;">' + formula_test.message + '<span style="float:right"> Line number: ' + ($('.codeField').attr('error_line')||$('.codeField').attr('custom_error_line')) + '</span></div>');
									$(element).find('.codeField').find('div').removeAttr('style');
									$(element).find('.numbering').find('div').removeAttr('style');
								}
							}, 1000);
							setTimeout(function(){
								$(element).find('.numbering').find('div').eq(getError.line_number - 1).css({'background-color': 'rgb(45, 45, 45)', 'transition' : 'all 2s ease', '-webkit-transition': 'all 2s ease'});
								$(element).find('.codeField').find('div').eq(getError.line_number - 1).css({'background-color': 'rgb(45, 45, 45)', 'transition' : 'all 2s ease', '-webkit-transition': 'all 2s ease'});
							}, 2000);
							setTimeout(function(){
								$(element).find('.codeField').find('div').removeAttr('style');
								$(element).find('.numbering').find('div').removeAttr('style');
							}, 4100);
							$.ajax({
								type: 'POST',
								url: '/ajax/create_formula_file',
								data: {
									user: $('#current-user-id').text(),
									file: 'delete'

								},
								success: function(e){
								}

							});
						}
					}
				});
			}
			else{
				var test = $('.codeField').text();
				var ctr = 0;
				for(var str_index = 0 ; str_index < test.length ; str_index++ ){
					console.log(test[str_index] == "}");
					if(test[str_index] == "{"){
						ctr +=1;
					}else if(test[str_index] == "}"){
						ctr -=1;
					}
				}
				if(ctr != 0){
					var brace;
					if(ctr > 0){
						brace = 'Closing Bracket \'}\'';
					}
					else{
						brace = 'Opening Bracket \'{\'';
					}
					$('.codeField').attr('css-error', ctr);
					$(element).find('.error_handler_container').empty();
					$(element).find('.container').css('height', '65%');
					$(element).find('.error_handler_container').append('<div style="background-color:rgb(141, 25, 25);padding-left:5px;">CSS Error, Missing ' + brace + '</div>');
					
				}
				else{
					$('.codeField').attr('css-error', ctr);
					$(element).find('.error_handler_container').empty();
					$(element).find('.container').css('height', '65%');
					$(element).find('.error_handler_container').append('<div style="background-color:green;padding-left:5px;">Valid CSS.</div>');
				}
			}
		});
	}
}