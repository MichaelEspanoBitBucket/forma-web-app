
 $.fn.sessionTimeout = function(options) {
   var defaults = {
      in_active: 1200000, //20 Minutes 1200000
      no_confirm: 60000, //10 Seconds 60000
      session_alive: 1200000, //10 Minutes 600000
      redirect_url: '/user/logout',
      click_reset: true,
      alive_url: '/',
      logout_url: '/user/logout',
      showMsgbox: true,
      msgTitle: 'Session Timeout Warning',
      msgText: 'You will be logged-out in 60 seconds. Do you want to stay signed-in?',
      msgButtonVal: 'Stay'
   }
    
    var opts = $.extend(defaults, options);
    var liveTimeout, confTimeout, sessionTimeout;
    
   var start_liveTimeout = function(){
      clearTimeout(liveTimeout);
      clearTimeout(confTimeout);
      liveTimeout = setTimeout(logout, opts.in_active);
      
      if(opts.session_alive){
        clearTimeout(sessionTimeout);
        sessionTimeout = setTimeout(keep_session, opts.session_alive);
      }
   }
    
    var logout = function(){
      var my_dialog;
      var buttonsOpts = {};
	  
      confTimeout = setTimeout(redirect, opts.no_confirm);
	  
         buttonsOpts[opts.msgButtonVal] = function(){
               $("#popup_cancel").trigger("click");
               stay_logged_in();
         }
         
      if(opts.showMsgbox){
       
         var newConfirm = new jConfirm(opts.msgText, opts.msgTitle, '', '', '', function(r) {
               if (r == true) {
                   
                   keep_session();
               }else{
                  jAlert('Your session has expired. Please log in again.', 'Not Logged In', '', '', '', function(ans) {
                     if (ans == true) {
                         redirect();
                     }
                 });
               }
           });
           //newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-exclamation-triangle fl-margin-right fl-message-confirmation"></i>'}); 
      }
   }
    
   var redirect = function(){
      if(opts.logout_url){
        $.get(opts.logout_url);
      }
      window.location.href = opts.redirect_url;
   }
    
   var stay_logged_in = function(el){
      start_liveTimeout();
      if(opts.alive_url){
         $.get(opts.alive_url);
      }
   }
    
   var keep_session = function(){
      $.get(opts.alive_url);
      clearTimeout(sessionTimeout);
      sessionTimeout = setTimeout(keep_session, opts.session_alive);
   } 
    
    // Return Instance
   return this.each(function() {
      obj = $(this);
      start_liveTimeout();
      if(opts.click_reset){
        $(document).bind('click', start_liveTimeout);
      }
      if(opts.session_alive){
        keep_session();
      }
   });
    
   };
