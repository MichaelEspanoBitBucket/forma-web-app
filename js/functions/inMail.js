$(document).ready(function(){
	
	$('[data-original-title]').tooltip();

	//	Bookmark all 

	var  markAsImportantAll = $('.fl-inmail-more-option').find('li').eq(0);

	markAsImportantAll.on('click', function(){

		if ($('.in-mail-msg-single:checked').length >= 1) {
			
			$('.in-mail-msg-single:checked').each(function(){

				if ($(this).parent().find('.fl-opt-type').children('i').eq(0).hasClass('fa-bookmark-o') == true) {
					$(this).parent().find('.fl-opt-type').children('i').eq(0).removeClass('fa-bookmark-o').addClass('fa-bookmark').attr('data-original-title', 'Remove as important');
				};
				
			});
			
		};

	});

	var  removesImportantAll = $('.fl-inmail-more-option').find('li').eq(1);

	removesImportantAll.on('click', function(){

		if ($('.in-mail-msg-single:checked').length >= 1) {
			
			$('.in-mail-msg-single:checked').each(function(){

				if ($(this).parent().find('.fl-opt-type').children('i').eq(0).hasClass('fa-bookmark') == true) {
					$(this).parent().find('.fl-opt-type').children('i').eq(0).removeClass('fa-bookmark').addClass('fa-bookmark-o').attr('data-original-title', 'Mark as important');
				};
				
			});
			
		};

	});

	//	end of Bookmark all

	//	single Bookmark

	var bookIt = $('.fl-opt-type').find('.fa-bookmark-o');

	bookIt.on('click', function(){

		if ($(this).hasClass('fa-bookmark-o') == true) {
			$(this).removeClass('fa-bookmark-o').addClass('fa-bookmark');	
			$(this).attr('data-original-title', 'Remove as important');
		
		}else if($(this).hasClass('fa-bookmark-o') == false){
			$(this).removeClass('fa-bookmark').addClass('fa-bookmark-o');
			$(this).attr('data-original-title', 'Mark as important');
		};

	});

	//	end of single Bookmark

	//	single Delete

	var deleteIt = $('.fl-opt-type').find('.fa-trash-o');

	deleteIt.on('click', function(){
		
		deleteAllmsg();

	});

	//	end of single Delete

	// Inmail header toggle

	$('.fl-inmail-msg-header').on('click', function(){
		$(this).next().slideToggle();
	});

	// end of Inmail toggle

 	// Msg, Notification, Compose Button

	var composeBtn = $('.fl-inmail-composebtn');
	composeBtn.on('click', function(){
		$('#tabs').children('li').eq(0).hide();
		if ($('#tab2C .fl-inmail-notifications-compose-content-wrapper').css('display') != 'none'){			
			$('.fl-back-to-inbox').hide();
		}
	});

	var msgBtn = $ ('.fl-inmail-messagebtn');
	msgBtn.on('click', function(){
		
		$('.checkall-inmail-noti').css('display', 'none'); 
		$('.checkall-inmail-msg').css('display', 'block');	
		
		if ($('#tab1C .fl-inmail-notifications-compose-content-wrapper').css('display') != 'none' ){
			$('#tabs').children('li').eq(0).show();
			
		}else if($('#tab1C .fl-inmail-notifications-compose-content-wrapper').css('display') == 'none'){
			$('#tabs').children('li').eq(0).hide();
		};

		if ($('#tab1C .fl-inmail-msg-wrapper').css('display') != 'none') {
			$('.fl-back-to-inbox').show();
		};

	});	

	var notiBtn = $('.fl-inmail-notificationbtns');	
	notiBtn.on('click', function(){
		$('#tabs').children('li').eq(0).show();
		$('.checkall-inmail-noti').css('display', 'block'); 
		$('.checkall-inmail-msg').css('display', 'none');

		if (notiBtn.attr('hide') == 'count0') {
			$('#tabs').children('li').eq(0).hide();
		};
		
		if ($('#tab2C .fl-inmail-notifications-compose-content-wrapper').css('display') != 'none'){			
			$('.fl-back-to-inbox').hide();
		}	
	});



	// Msg, Notification, Compose Button

	// inbox back button 

	var backtoInbox = $('.fl-back-to-inbox');
	backtoInbox.on('click', function(event){
		event.preventDefault();
		$(this).parents('.fl-head-title-wrapper').siblings('#tab1C').children('.fl-inmail-msg-wrapper').fadeOut();
		$(this).parents('.fl-head-title-wrapper').siblings('#tab1C').children('.fl-inmail-notifications-compose-content-wrapper').fadeIn();
		$(this).fadeOut();
		$('#tabs').children('li').eq(0).fadeIn();
	});

	// end of inbox back button 

	//	CC BCC show and hiding 

	var showCC = $('.fl-inmail-show-CC');

		showCC.on('click', function(){
			$(this).hide();
			$(this).parent().siblings('.fl-inmail-cc').fadeIn();
		});

	var hideCC = $('.fl-inmail-hide-CC');

		hideCC.on('click', function(){
			$(this).parent().siblings('.fl-inmail-add-a-recipient').find('.fl-inmail-show-CC').show();
			$(this).parent().fadeOut();
		});

	var showBCC = $('.fl-inmail-show-BCC');
		
		showBCC.on('click', function(){
			$(this).hide();
			$(this).parent().siblings('.fl-inmail-bcc').fadeIn();
		});

	var hideBCC = $('.fl-inmail-hide-BCC');
		
		hideBCC.on('click', function(){
			showBCC.show();
			$(this).parent().fadeOut();
		});

	//	end of CC BCC show and hiding 

	$('.fl-drop-inmail-option').on('click',function(){
		$(this).parent().next().slideToggle();
	});
	
	checkAll.call($('#checkall-inmail-msg'), '.tab-container#tab1C', '.in-mail-msg-single', '.fl-inmail-notifications-compose-opt-wrapper');

	checkAll.call($('#checkall-inmail-noti'), '.tab-container#tab2C', '.in-mail-noti-single', '.fl-inmail-notifications-opt-wrapper');

	singleCHeck.call($('.in-mail-msg-single'), '.fl-inmail-notifications-compose-opt-wrapper');

	singleCHeck.call($('.in-mail-noti-single'), '.fl-inmail-notifications-opt-wrapper');

	deleteAll.call($('.fl-inmail-notifications-compose-opt-wrapper'), '.in-mail-msg-single');

	deleteAll.call($('.fl-inmail-notifications-opt-wrapper'), '.in-mail-noti-single');

	singleHightlight.call($('.in-mail-msg-single'), '#checkall-inmail-msg');

	singleHightlight.call($('.in-mail-noti-single'), '#checkall-inmail-noti');

	contentView.call($('#tab1C'), ' .fl-inmail-notifications-compose-content');

	contentView.call($('#tab2C'), ' .fl-inmail-notifications-compose-content', 'dontAnimate');

	//tab redirect link
	var tabHash = window.location.hash;
	$(tabHash).trigger('click');	
});	


	
// Function checkall	
function checkAll(tabconID, licheckboxClass, flInmailOptWrapwClass){
	$(this).on('click', function(){
		var checkAllCheckbox = $(this);
		var eleTab = $(tabconID);
		var licheckbox = $(eleTab).find(licheckboxClass)
		var ulcheckboxHightlight = $(eleTab).find('.fl-inmail-notifications-compose-content-wrapper');
		var flInmailOptWrap = $(flInmailOptWrapwClass);

		$(licheckbox).prop('checked', this.checked);

		if ($(checkAllCheckbox).is(':checked') == true) {
			$(flInmailOptWrap).fadeIn();
			$(checkAllCheckbox).parents('.fl-tab-wrapper').parent().siblings(eleTab).find(ulcheckboxHightlight).eq(0).children('li').css('background-color', '#e0e0e0');
		}else if($(checkAllCheckbox).is(':checked') == false)  {
			$(flInmailOptWrap).fadeOut();
			$('.fl-drop-inmail-option').parent().next().slideUp();
			$(checkAllCheckbox).parents('.fl-tab-wrapper').parent().siblings(eleTab).find(ulcheckboxHightlight).eq(0).children('li').css('background-color', '');			
		}
	});
};

// end  of Function checkall	

//Function Single Check
function singleCHeck(flInmailOptWrapClass){
	var singleCHeckbox = $(this);
	var flInmailOptWrap = $(flInmailOptWrapClass);
	$(this).on('click', function(){
		var ctr = 0;

		$(singleCHeckbox).each(function(){

			if($(this).prop("checked")==true){	

			ctr++
				if(ctr==2){
					return false;
				}
			}
		});
		
		if(ctr>=2){
			$(flInmailOptWrap).fadeIn();

		}else {
			$(flInmailOptWrap).fadeOut();
			$('.fl-drop-inmail-option').parent().next().slideUp();

		}
	});
};
// end of Function Single Check

// function single hightlight
function singleHightlight(checkAllID){

	var checkBoxes = $(this);
	
	var  elecheckAllID = $(checkAllID)
	
	$(this).on('click', function(){

		if ($(this).is(":checked") == true) {
				$(this).parents('li').css('background-color', '#e0e0e0');
	
		}else if($(this).is(":checked") == false){
			$(this).parents('li').css('background-color', '');
		};
		var total_checkele = $(checkBoxes);
		
		
		var total_checkele_check = $(checkBoxes).filter(':checked');
		

		if (total_checkele.length == total_checkele_check.length) {
			$(elecheckAllID).prop('checked', true);

		}else {
			$(elecheckAllID).prop('checked', false);
		};

	});
};
// end of function single hightlight

//function delete all message
function deleteAllmsg(){
	var msg = 'Are you sure you want to delete selected items?';

	var newConfirm = new jConfirm(msg, 'Confirmation Dialog', '', '', '', function(r) {

	});
	newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'});
};

function deleteAll(checkBoxesClass){
	
	var checkBoxes = $(checkBoxesClass);
	var  btndeleteAll = $(this).find('.fa-trash-o');
	
	$(btndeleteAll).on('click', function(){
		if ($(checkBoxes).filter(':checked').length >= 1) { 
			deleteAllmsg();
		};
	});
};
// end of function delete all message

// function click of Li's msg and notifications
function contentView(finccClass, action){

	var contentViewBtn = $(this).find(finccClass);
	var saveAction = action;
	
	$(contentViewBtn).on('click', function(event){
		event.preventDefault();

		if (action) {
			if (action == 'dontAnimate') {
				return false;
			};
		};

		$(this).parents('.fl-inmail-notifications-compose-content-wrapper').fadeOut();
		$(this).parents('.fl-inmail-notifications-compose-content-wrapper').next().fadeIn().css('display', 'inline-table');
		$('#tabs').children('li').eq(0).fadeOut();
		$('.fl-back-to-inbox').fadeIn();

	});	

};
// end function click of Li's msg and notifications