/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var keyword = {
    init: function() {
        this.addEventListeners();
    },
    addEventListeners: function() {
        var self = this;
        $('body').on('click', '.createkeyword', function() {
            self.showDialog(0);
        });

        $('body').on('click', '.editkeyword', function() {
            var id = $(this).attr('keyword-id');
            self.showDialog(id);
        });

        $('body').on('click', '#save_keyword', function() {
            var id = $(this).attr('request-id');
            self.save(id);
        });

        $('body').on('keydown', '.search-keyword-value', function(event) {
            var keyword = $(this).val();
            if (event.keyCode == 13) {
                keyword_view.load(keyword);
            }
        });

    },
    showDialog: function(id) {
        var lookup = this.populateLookup();

        if (id !== 0) {
            this.load(id, function(data) {
                var ret = '<h3 class="fl-margin-bottom"><i class="icon-key"></i> Keyword Maintenance</h3>';
                ret += '<div class="hr"></div>';
                ret += '<div class="fields">';
                ret += '<div class="label_below2"> Code:<font color="red">*</font> </div>';
                ret += '<div class="input_position"><input type="text" name="keyword_code" id="keyword_code" value="' + data.code + '" class="form-text"></div>';
                ret += '</div>';
                ret += '<div class="fields">';
                ret += '<div class="label_below2"> Description:<font color="red">*</font> </div>';
                ret += '<div class="input_position"><input type="text" name="keyword_description" id="keyword_description" value="' + data.description + '" class="form-text"></div>';
                ret += '</div>';
                ret += '<div class="fields">';
                ret += '<div class="label_below2"> Value Code:<font color="red">*</font> </div>';
                ret += '<div class="input_position"><input type="text" name="keyword_valuecode" id="keyword_valuecode" value="' + data.value_code + '" class="form-text"></div>';
                ret += '</div>';
                ret += '<div class="fields">';
                ret += '<div class="label_below2"> Value Description:<font color="red">*</font> </div>';
                ret += '<div class="input_position"><input type="text" name="keyword_valuedescription" id="keyword_valuedescription" value="' + data.value_description + '" class="form-text"></div>';
                ret += '</div>';
                ret += '<div class="fields">';
                ret += '<div class="label_below2"> Value:<font color="red">*</font> </div>';
                ret += '<div class="input_position"><input type="text" name="keyword_quantity" id="keyword_quantity" value="' + data.quantity + '" class="form-text"></div>';
                ret += '</div>';
                ret += '<div class="fields">';
                ret += '<div class="label_below2"></div>';
                ret += '<div class="input_position" style="text-align:right;"><img src="/images/loader/load.gif" class="display saveFormLoad" style="margin-right:5px;margin-top:2px;"><input type="button" class="btn-blueBtn save_keyword" request-id="' + id + '" id="save_keyword" value="Save"><input type="button" style="margin-left:5px;" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div>';
                ret += '</div>';

                var newDialog = new jDialog(ret, "", "", "", "", function() {
                });
                newDialog.themeDialog("modal2");
                $('#keyword_code').autocomplete({
                    source: lookup.codes
                });

                $('#keyword_description').autocomplete({
                    source: lookup.descriptions
                });
            });
        } else {
            var ret = '<h3 class="fl-margin-bottom"><i class="icon-key"></i> Keyword Maintenance</h3>';
            ret += '<div class="hr"></div>';
            ret += '<div class="fields">';
            ret += '<div class="label_below2"> Code:<font color="red">*</font> </div>';
            ret += '<div class="input_position"><input type="text" name="keyword_code" id="keyword_code" class="form-text"></div>';
            ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="label_below2"> Description:<font color="red">*</font> </div>';
            ret += '<div class="input_position"><input type="text" name="keyword_description" id="keyword_description" class="form-text"></div>';
            ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="label_below2"> Value Code:<font color="red">*</font> </div>';
            ret += '<div class="input_position"><input type="text" name="keyword_valuecode" id="keyword_valuecode" class="form-text"></div>';
            ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="label_below2"> Value Description:<font color="red">*</font> </div>';
            ret += '<div class="input_position"><input type="text" name="keyword_valuedescription" id="keyword_valuedescription" class="form-text"></div>';
            ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="label_below2"> Value:<font color="red">*</font> </div>';
            ret += '<div class="input_position"><input type="text" name="keyword_quantity" id="keyword_quantity" class="form-text"></div>';
            ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="label_below2"></div>';
            ret += '<div class="input_position" style="text-align:right;"><img src="/images/loader/load.gif" class="display saveFormLoad" style="margin-right:5px;margin-top:2px;"><input type="button" class="btn-blueBtn save_keyword fl-margin-right" request-id="' + id + '" id="save_keyword" value="Save"><input type="button" style="margin-left:5px;" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div>';
            ret += '</div>';

            var newDialog = new jDialog(ret, "", "", "", "", function() {
            });
             newDialog.themeDialog("modal2");

            $('#keyword_code').autocomplete({
                source: lookup.codes
            });

            $('#keyword_description').autocomplete({
                source: lookup.descriptions
            });
        }

    },
    load: function(id, callback) {
        ui.block();
        $.post('/ajax/keyword_load', {
            ID: id
        }, function(data) {
            
            ui.unblock();
            result = JSON.parse(data)
            callback(result);
        });
    },
    save: function(id) {
        var code = $('#keyword_code').val();
        var description = $('#keyword_description').val();
        var value_code = $('#keyword_valuecode').val();
        var value_description = $('#keyword_valuedescription').val();
        var quantity = $('#keyword_quantity').val();

        ui.block();
        $.post('/ajax/keyword_save', {
            ID: id,
            Code: code,
            Description: description,
            Value_Code: value_code,
            Value_Description: value_description,
            Quantity: quantity
        }, function(data) {
            ui.unblock();
            showNotification({
                message: data,
                type: "Success",
                autoClose: true,
                duration: 3
            });

            $('#popup_cancel').click();

            // setTimeout(function() {
            //     window.location.reload();
            // }, 3000)
            $(".dataTable_keyword").dataTable().fnDestroy();
            GetKeywordDataTable.defaultData();
        });
    },
    populateLookup: function() {
        var codes = [];
        var descriptions = [];
        $('.keywordheader').each(function() {
            var code = $(this).attr('code');
            var description = $(this).attr('description');

            codes.push(code);
            descriptions.push(description);
        });

        var lookup = {
            codes: array_unique(codes),
            descriptions: array_unique(descriptions)
        }
        return lookup;
    }
}
var keyword_view = {
    load: function(keyword) {
        $('.report-list-wrapper').html('');
        this.block();
        $.get('/ajax/keyword_load_view', {
            Keyword: keyword
        }, function(data) {
            $('.report-list-wrapper').html(data);
        });
    }
    , block: function() {
        $(".report-list-wrapper").append('<center><img src="/images/loader/loader.gif" class="load-app-request" style="display: inline;margin-top:30px;"></center>');
    }
}

function find_index(array, string) {
    var i = 0;
    for (i = 0; i < array.length; i++) {
        if (array[i] == string)
            break;
    }
    return array[i] == string ? i : -1;
}

function array_unique(array_to_modify, remove_empty_items) {
    new_array = [];
    for (var i = 0; i < array_to_modify.length; i++) {
        if (find_index(new_array, array_to_modify[i]) == -1 && ((remove_empty_items && remove_empty_items == true && array_to_modify[i].toString().replace(/\s/g, '') != '') || !remove_empty_items))
            new_array[new_array.length] = array_to_modify[i];
    }
    return new_array;
}


function updateKeyword(status, type, id,callback) {
    if(status=="delete"){
        var conf = "Are you sure you want to Delete ?";
    }else{
        var conf = "Are you sure you want to " + getStatus_revert(status) + " ?";
    }
   var newConfirm = new jConfirm(conf, 'Confirmation Dialog', '', '', '', function(r) {
        if (r == true) {
            // ui.block();
            $.post("/ajax/updateKeyword", {type: type, id: id, status: status}, function(data) {
                // alert(data)
                // if(type=="form"){

                // }else if(type=="orgchart"){

                // }else if(type=="workflow"){

                // }
                // ui.unblock();

                // window.location.reload();
                // console.log(data);
                callback(1);
            })
        }
    });
    newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});
}

$(document).ready(function() {
    keyword.init();
});

$(document).on('click', '.keyword_update', function(){
    var status = $(this).attr('data-status');
    var id = $(this).attr('data-keyword-id');
    updateKeyword(status, 'update_keyword_status', id, function(){
        $(".dataTable_keyword").dataTable().fnDestroy();
        GetKeywordDataTable.defaultData();
    });
});

$(document).on('click', '.keyword_delete', function(){
    var status = 'delete';
    var id = $(this).attr('data-keyword-id');
    updateKeyword(status, 'delete_keyword', id, function(){
        $(".dataTable_keyword").dataTable().fnDestroy();
        GetKeywordDataTable.defaultData();
    });
});


