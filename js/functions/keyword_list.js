var jsonKeywordData = {
    search_value: "",   
    start: 0,
    limit: 10,
}
$(document).ready(function() {
	$("body").on("keyup","#txtSearchKeywordDatatable",function(e){
        if (e.keyCode == "13") {
            $(".dataTable_keyword").dataTable().fnDestroy();
            jsonKeywordData['search_value'] = $(this).val();
            GetKeywordDataTable.defaultData();
        }
    });
    //show entries
    $(".searchKeywordLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonKeywordData['limit'] = val;
            $(".dataTable_keyword").dataTable().fnDestroy();
            // var self = this;
            var oTable = GetKeywordDataTable.defaultData();
        // }
    })
    //load
    GetKeywordDataTable.defaultData();
    //sort
    $("body").on("click",".dataTable_keyword th",function(){
       
        var cursor = $(this).css("cursor");
        jsonKeywordData['column-sort'] = $(this).attr("field_name");
        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest(".dataTable_keyword").find(".sortable-image").html("");
        if(indexcolumnSort==-1){
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonKeywordData['column-sort-type'] = "ASC";
        }else{
            columnSort.splice(indexcolumnSort,1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonKeywordData['column-sort-type'] = "DESC";
        }
        
        $(".dataTable_keyword").dataTable().fnDestroy();
        var self = this;
        GetKeywordDataTable.defaultData(function(){
             addIndexClassOnSort(self);
        });
    })

})

GetKeywordDataTable = {
        "defaultData" : function(callback){
        var oColReorder = {
            allowReorder : false,
            allowResize : true
        };
        var oTable = $(".dataTable_keyword").dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> '+
                       '<div class="bar1"></div> '+
                       '<div class="bar2"></div> '+
                       '<div class="bar3"></div> '+
                       '<div class="bar4"></div> '+
                       '<div class="bar5"></div> '+
                       '<div class="bar6"></div> '+
                      '<div class="bar7"></div> '+
                      '<div class="bar8"></div> '+
                      '<div class="bar9"></div> '+
                      '<div class="bar10"></div> '+
                 '</div>'
            },
            "oColReorder" : oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/search",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "action", "value": "getKeywordDataTable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "search_value", "value": jsonKeywordData['search_value']});
                aoData.push({"name": "limit", "value": jsonKeywordData['limit']});
                aoData.push({"name": "column-sort", "value": jsonKeywordData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonKeywordData['column-sort-type']});
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        // console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
                var obj = '.dataTable_keyword';
                var listOfappContainer = '.fl-list-of-app-record-wrapper';
                dataTable_widget(obj, listOfappContainer);
                // $(".tip").tooltip();

            },
            fnDrawCallback : function(){
                setDatatableTooltip(".dataTable_widget");
               
                if (callback) {
                    callback(1);
                }
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonKeywordData['limit']),
        });
        return oTable;
    }
}