(function(){

    $(document).ready(function(){

        SimpleTab.init('#fl-tabs-formcontrolsv2');
        
        if($('[name="FormId"]').val() == 0 || $('[name="FormId"]').val() == "" || $('[name="FormId"]').val() == "0"){
            Workspacev2.init('.workspacev2');
        }

        var workflowID = getParametersName("id",window.location);

     
       if (workflowID == undefined && $('.workflow_ws').length == 1) {
            Workspacev2.init('.workspacev2');
            

       };

       var reportID = getParametersName("id",window.location);
       
       if (reportID == "0" && $('.report_ws').length == 1) {
            Workspacev2.init('.workspacev2');
       };

        var orgchartViewType = getParametersName("view_type", window.location);

       if(orgchartViewType == undefined) {
            Workspacev2.init('.workspacev2');
        };

        FlToggleFullscreen.init('.fl-workspace-fullscreen'); 

    });

    var workspacePathName = window.location.pathname;
    //function for tabs
    SimpleTab = {
        "init" : function(container){
            var self = this;
            //on tab click
            $(container+' li').on({
                'click':function(){
                  
                    //set all as inactive
                    $(container+' li').removeAttr("attr");

                    //set press tab as active
                    $(this).attr("attr","active");

                    $(container+' li').each(function(){
                        if($(this).attr("attr")=="active"){

                            self.setActiveTAb(this);
                            $(window).resize();
                        }else{
                            self.setInactiveTab(this);
                            
                        }
                        
                    });
                }

            });
            //onload page
            $(container+' li[attr="active"]').trigger('click');
            //#tabs
            $(container+' li').on({
                'dblclick': function(){
                   
                    if ($(container+' li').attr('toggle-evt-show') == 'false') {
                         $('.contentBar_workspace').css({
                            'margin-top': + 44
                        });
                    }

                    if ($(container+' li').attr('toggle-evt-show') == 'true') {
                         $('.contentBar_workspace').css({
                            'margin-top': + 156
                        });
                    }
                    //console.log("dv from formcontrolsv2",$(this).attr('toggle-evt-show'))
                }
            })
        },
        "setActiveTAb" : function(self,container){
            
            var tabId = $(self).attr('id');
            
            if ($(self).parents('.fl-controlsv2-main-warpper').hasClass('fl-controlsv2-main-warpper-classic')) {
                
                $(self).removeClass('fl-workspace-control-classic-inactive').addClass('fl-workspace-control-classic-active');

            }else if ($(self).parents('.fl-controlsv2-main-warpper').hasClass('default-theme-dark')) {

                $(self).removeClass('default-theme-dark-fl-workspace-control-default-inactive').addClass('default-theme-dark-fl-workspace-control-default-active');
            
            }else if($(self).parents('.fl-controlsv2-main-warpper').hasClass('default-theme-light')){
                
                $(self).removeClass('default-theme-light-fl-workspace-control-default-inactive').addClass('default-theme-light-fl-workspace-control-default-active');
            
            }

            $('#' + tabId + 't').stop().fadeIn('slow');
            $(self).removeClass('inactive')
            FlWorkSpaceWrapV2.init('.contentBar_workspace');//initialize FlWorkSpaceWrapV2
        },

        "setInactiveTab" : function(self,container){
            
            var tabId = $(self).attr('id');

            if ($(self).parents('.fl-controlsv2-main-warpper').hasClass('fl-controlsv2-main-warpper-classic')) {
                
                $(self).removeClass('fl-workspace-control-classic-active').addClass('fl-workspace-control-classic-inactive');
            
            }else if ($(self).parents('.fl-controlsv2-main-warpper').hasClass('default-theme-dark')) {

                $(self).removeClass('default-theme-dark-fl-workspace-control-default-active').addClass('default-theme-dark-fl-workspace-control-default-inactive');
            
            }else if($(self).parents('.fl-controlsv2-main-warpper').hasClass('default-theme-light')){
                
                $(self).removeClass('default-theme-light-fl-workspace-control-default-active').addClass('default-theme-light-fl-workspace-control-default-inactive');
            
            }

            $(self).addClass('inactive')
            $('#' + tabId + 't').stop().hide();
           FlWorkSpaceWrapV2.init('.contentBar_workspace');//initialize FlWorkSpaceWrapV2
        }
    }
    //end function for tabs

    //function for workspace v2

    Workspacev2 = {
        'init': function(container){
            var self = this;
            var docWidth = $(window).outerWidth();
            var docWidthTotal = docWidth - 41;
           $(container).css({
                'width': + docWidthTotal
           });

           $('.form_size_width').val($(container).width());
           $('.form_size_height').val($(container).height());
      
           {//added
                var tppi_w = $('<div id="ppitestw" style="width:1in;visible:hidden;padding:0px"></div>');
                var tppi_h = $('<div id="ppitesth" style="height:1in;visible:hidden;padding:0px"></div>');
                $("body").append(tppi_w);
                $("body").append(tppi_h);
                var screenPIW = $("#ppitestw").width();
                var screenPIH = $("#ppitesth").height();
                var container_width = $(container).width();
                var container_height = $(container).height();
                var converted_to_inches = Number(container_width/screenPIW)+"x"+Number(container_height/screenPIH);
                //console.log(converted_to_inches);
                $('.form_size').children('option[value="custom_size"]:contains("Custom Size")').eq(0).val(converted_to_inches);
                tppi_w.remove();
                tppi_h.remove();

           }
        }
    }


    FlWorkSpaceWrapV2 = {
        'init': function(container){
            
            var flControlV2Wrap = $(container).parent().find('.fl-controlsv2-main-warpper');
            var flControlV2WrapAttrtoggleEvtShow = $(flControlV2Wrap).find('#fl-tabs-formcontrolsv2 li').attr('toggle-evt-show')
            
            flControlV2WrapHeight = flControlV2Wrap.outerHeight();
            
             $(container).css({
                'margin-top': + flControlV2WrapHeight
            });
        }
    }

    FlToggleFullscreen = {
        'init': function(elem){

            $(elem).on({
                'click': function(){

                    if ($(this).attr('isfullscreen') == 'exitFullScreen') {
                        
                        goFullScreen(document.documentElement);
                        $(this).attr('isfullscreen', 'goFullScreen');  
                        $(this).attr('data-original-title', 'Exit Fullscreen');
                        $(this).find('i.fa').removeClass('fa-expand').addClass('fa-compress'); 
                        
                    }else if ($(this).attr('isfullscreen') == 'goFullScreen') {
                        exitFullScreen(document.documentElement);
                        $(this).attr('isfullscreen', 'exitFullScreen'); 
                        $(this).attr('data-original-title', 'Go Fullscreen');
                        $(this).find('i.fa').removeClass('fa-compress').addClass('fa-expand'); 
                       //flWorkspaceFullscreenExitEsc.init('.fl-workspace-fullscreen');
                       exitHandler();

                    };

                 }
            });
            
        }
     }

    function goFullScreen(elem) {
        if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
            if (elem.requestFullScreen) {
                elem.requestFullScreen();
            } else if (elem.mozRequestFullScreen) {
                elem.mozRequestFullScreen();
            } else if (elem.webkitRequestFullScreen) {
                elem.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
            } else if (elem.msRequestFullscreen) {
                elem.msRequestFullscreen();
            }
        } 
    }

    function exitFullScreen(elem) {
      if(document.exitFullscreen) {
        document.exitFullscreen();
      } else if(document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if(document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      }
    }


    if (document.addEventListener)
    {
        document.addEventListener('webkitfullscreenchange', exitHandler, false);
        document.addEventListener('mozfullscreenchange', exitHandler, false);
        document.addEventListener('fullscreenchange', exitHandler, false);
        document.addEventListener('MSFullscreenChange', exitHandler, false);
    }

    function exitHandler()
    {
        if ((document.fullScreenElement && document.fullScreenElement !== null) || (document.msfullscreenElement && document.msfullscreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen))
        {   
           $('.fl-workspace-fullscreen').attr('data-original-title', 'Go Fullscreen');
           $('.fl-workspace-fullscreen').attr('isfullscreen', 'exitFullScreen');
           $('.fl-workspace-fullscreen').find('i.fa').removeClass('fa-compress').addClass('fa-expand');
        }
    }

})();