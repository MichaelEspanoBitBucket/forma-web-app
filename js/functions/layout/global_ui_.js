$(document).ready(function(){
    changeThemeAppColor();

});

function changeThemeAppColor() {

    var fullColors = $('#fullColors');
    var theme_color = $("#theme-color").val();

    //console.log($(fullColorCancelBtn).length)

    fullColors.spectrum({
        color: theme_color,
        showInput: true,
        className: "full-spectrum",
        showInitial: true,
        showPalette: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: "rgb",
        localStorageKey: "spectrum.demo",
        chooseText: "Choose",
        cancelText: "Cancel",
        containerClassName: 'app_theme_color',
        move: function (color) { //
            darkenColor(color,function(data){
                
            });
            //alert("move");
        },
        show: function () {

           var self = $(this);
           var selfNextTop = $('.sp-container:visible').position().top;
           var selfNextLeft = $('.sp-container:visible').position().left;
           var docScollTop = $(document).scrollTop();
           var docScollLeft = $(document).scrollLeft();
           $('.sp-container:visible').css({'position':'fixed', 'top':(selfNextTop-docScollTop)+'px', 'left':(selfNextLeft-docScollLeft)+'px' });

        },
        beforeShow: function () {

        },
        hide: function (color) {//
            //alert("hide")
        },

        change: function (color) { 
            ui.block();
            var theme = fullColors.val();
            
            $('.app_theme_color').find('.sp-thumb-active').addClass('pickerCurrentColor')
            //$('.sp-thumb-active').addClass('pickerCurrentColor');
            //console.log($('.sp-thumb-el').hasClass('pickerCurrentColor'));
             //ui.block();
            darkenColor(theme,function(dark_color){

                var json_theme_color = {"light_color":theme,dark_color:dark_color};
                var jsonData = {theme: JSON.stringify(json_theme_color),action:"setTheme"};
                $.post("/ajax/theme",jsonData,function(replaceStyle){
                    //alert(12313)
                    $("#theme-color").val(theme);
                    $(".customClass").html(replaceStyle)
                     ui.unblock();
                });
            
            }); 
            
        },
        
        cancelFN: function(color){
           // alert("cancel")
            ui.block();
            darkenColor(color,function(dark_color){
                if(typeof color == "object"){
                    color = color.toRgbString();
                }
                var json_theme_color = {"light_color":color,dark_color:dark_color};
                var jsonData = {theme: JSON.stringify(json_theme_color),action:"setTheme"};
                $.post("/ajax/theme",jsonData,function(replaceStyle){
                    $("#theme-color").val(color);
                    $(".customClass").html(replaceStyle)
                     ui.unblock();
                });
            });

        },

        getPrevColor: function(color){

            if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
                ui.block();
                darkenColor(color,function(dark_color){
                    if(typeof color == "object"){
                        color = color.toRgbString();
                    }
                    var json_theme_color = {"light_color":color,dark_color:dark_color};
                    var jsonData = {theme: JSON.stringify(json_theme_color),action:"setTheme"};
                    $.post("/ajax/theme",jsonData,function(replaceStyle){
                        $("#theme-color").val(color);
                        $(".customClass").html(replaceStyle)
                         ui.unblock();
                    });
                });
            };
        },

        palette: [
            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
        ]
    });
    
    fullColors.on("dragstop.spectrum", function(e, color) {
        ui.block();
        
        darkenColor(color,function(dark_color){
            if(typeof color === "object"){
                color = color.toRgbString();
            }
            var json_theme_color = {"light_color":color,dark_color:dark_color};
            var jsonData = {theme: JSON.stringify(json_theme_color),action:"setTheme"};
             $("#theme-color").val(color);
            $.post("/ajax/theme",jsonData,function(replaceStyle){
                $("#theme-color").val(color);
                $(".customClass").html(replaceStyle)
                ui.unblock();
            });
        });
    });
    
}

function darkenColor(color,callback) {
    if(typeof color == "object"){
       color = color.toRgbString();
    }
    var rgbColor = color.match(/[0-9]*/g).filter(Boolean);
    var rgb = "rgb(" + Math.round(Number(rgbColor[0]) * .90) + "," + Math.round(Number(rgbColor[1]) * .90) + "," + Math.round(Number(rgbColor[2]) * .90) + ")";
    if(callback){
        callback(rgb);
    }else{
        return rgb;
    }
};



function scrollPageTop() {

    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);

        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        console.log(target);
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - 100
            }, 1000);
            return false;
        }
    }
 
}