(function(){

	$(document).ready(function(){

		$(window).bind('enterBreakpoint480', function(){
			scrollUpEv("","",13,6);
		});
		

		$(window).bind('enterBreakpoint768', function(){
			scrollUpEv("","",13,13);
		});

		$(window).bind('enterBreakpoint1024', function(){
			scrollUpEv("","",13,6);
		});

		$(window).setBreakpoints();

	});

	function scrollUpEv(top,right,bottom,left){

		var scrollUpEvBtn = $('a[href="#scrollUp"]');

		$(scrollUpEvBtn).css({
			'position':'fixed',
			'top': top,
			'right':right,
			'bottom':bottom,
			'left':left,
			'z-index':100000
		});

		$(scrollUpEvBtn).on('click', scrollPageTop);
	}

})();

   