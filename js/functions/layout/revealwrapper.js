$(document).ready(function () {

    $('.fl-reveal-wrapper').revealWrapper();
    $('.fl-revealWrapperTrigger').each(function(){
        var isOpen = $(this).attr("data-detail-view-default");
        if(isOpen == "yes"){
            $(this).trigger('click').mouseout();
        }
    });
});


(function ($) {

    $.fn.revealWrapper = function () {

        return this.each(function () {

            var self = this;
            // table pagination links
            var tablePagination = $(self).parents('.fl-reveal-this-wrapper-btn').find('.dataTable_widget');

            //toggleUpdown button
            var disabletoggleUpDown = $(self).parents('.fl-reveal-this-wrapper-btn').find('.fl-toggleUpDown');
            //reveal button
            var revealWrapperbutton = $(self).parents('.fl-reveal-this-wrapper-btn').find('.fl-revealWrapperTrigger');
            // switch calendar table btn
            var switchCalBtn = $(self).parents('.fl-reveal-this-wrapper-btn').find('.switchingAppView');

            // wrapper to move
            var wrapperToMove = $(self).parents('.fl-reveal-this-wrapper-btn').find('.fl-wrapper-to-move');

            var fldatatableWrapper = $(self).parents('.fl-reveal-this-wrapper-btn').find('.fl-datatable-wrapper');

            var flWidgetWrapperScroll = $(self).find('.fl-widget-wrapper-scroll');

            $(self).attr('revealWrapper', 'hide');
            //console.log($(fldatatableWrapper).find('tr'));

            revealWrapperbutton.each(function () {

                $(this).on({
                    'click': function () {


                        var getHeightwrapperToMove = wrapperToMove.height();  // plus overflow scroll + 16;
                        //console.log(getHeightwrapperToMove);
                        $(self).css('height', +getHeightwrapperToMove);
                        $('.fl-datatable-wrapper').perfectScrollbar('update');

                        if ($(self).attr('revealWrapper') === 'hide') {

                            //console.log($(self).height());
                            $(self).attr('revealWrapper', 'show').removeClass('isDisplayNone').addClass('isDisplayBlock');

                            $(self).stop().animate({'width': '30%'});
                            wrapperToMove.stop().animate({'width': '70%'});

                            $(revealWrapperbutton).tooltip('hide')
                                    .attr('data-original-title', 'Hide details')
                                    .tooltip('fixTitle')
                                    .tooltip('show');

                            hideElement();
                            setTimeout(function () {
                                getHeightwrapperToMove = wrapperToMove.height();
                                $(self).css('height', (+getHeightwrapperToMove - 1) + 'px'); //- 30
                                $(flWidgetWrapperScroll).css('height', (+getHeightwrapperToMove - 37) + 'px');
                            }, 0);
                           
                            

                        } else if ($(self).attr('revealWrapper') === 'show') {
                            //console.log($(self).height());

                            $(self).attr('revealWrapper', 'hide').removeClass('isDisplayBlock').addClass('isDisplayNone');

                            $(self).stop().animate({'width': 'auto'});
                            wrapperToMove.stop().animate({'width': '100%'});

                            $(revealWrapperbutton).tooltip('hide')
                                    .attr('data-original-title', 'View details')
                                    .tooltip('fixTitle')
                                    .tooltip('show');

                            showElement();

                            setTimeout(function () {
                                getHeightwrapperToMove = wrapperToMove.height();
                                $(self).css('height', (+getHeightwrapperToMove - 1) + 'px'); //- 30
                                $(flWidgetWrapperScroll).css('height', (+getHeightwrapperToMove - 37) + 'px');
                            }, 0);
                        }

                        manipulateElement();
                    }
                });



            });

            function hideElement() {
                $(disabletoggleUpDown).parent().hide();
                $(tablePagination).hide();
                $(switchCalBtn).parent().hide();

            }

            function showElement() {
                $(disabletoggleUpDown).parent().show();
                $(tablePagination).show();
                $(switchCalBtn).parent().show();
            }

            function triggerElement() {
                //trigger some element

            }

            function manipulateElement() {

                if ($(self).attr('revealWrapper') === 'hide') {
                    $(fldatatableWrapper).css({
                        'border-bottom': ''
                    });
                } else if ($(self).attr('revealWrapper') === 'show') {
                    $(fldatatableWrapper).css({
                        'border-bottom': '1px solid #CFDBE2'
                    });

                }

                $(flWidgetWrapperScroll).css({
                    'border': 'none'
                });

            }

        });

    };


})(jQuery);

// TODO NEED TO DISABPLE OR DISPLAY NONE TOGGLE UP AND DOWN WHEN ACTIVE // done!
//NEED TO DISABPLE OR DISPLAY NONE PAGINATION WHEN ACTIVE