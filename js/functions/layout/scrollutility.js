$(document).ready(function(){
	$('.fl-workspace-button-wrapper-relatedforms').scrollUtility();
	$('#fl-workspace-tab-controls').scrollUtility();
	$('#fl-workspace-tab-properties').scrollUtility();
	$('#fl-workspace-tab-layout').scrollUtility();
	// $('#fl-workspace-tab-controls-generate-object').scrollUtility();
	$('#fl-workspace-tab-reqestview').scrollUtility();
	$('.fl-action-menu.extended-menu').scrollUtility();

});

(function ($){

	$.fn.hasScrollbarWidth = function() {
	    var divnode = this.get(0);
	    if(divnode.scrollWidth > divnode.clientWidth)
     	   return true;
	}
	  
	$.fn.scrollUtility = function(options){

		var self = $(this);
		//console.log(self);
	    	
	    if (!self.exists()) {
	    	return;
	    }	

	    //console.log(self)
		var left_arrow_ele = $('<div class="left-arrow-wrapper"><div class="isDisplayTable"><div class="isDisplayTableCell"><i class="fa fa-caret-left"></i></div></div></div>');
		var right_arrow_ele = $('<div class="right-arrow-wrapper"><div class="isDisplayTable"><div class="isDisplayTableCell"><i class="fa fa-caret-right"></i></div></div></div>');
	

		self.append($(left_arrow_ele));
	  	self.append($(right_arrow_ele));


		//console.log("LEFT", left_arrow_ele, "RIGHT", right_arrow_ele);

		$(self).on('scroll', function(e,data){
			
			if(data){

				$(this).scrollLeft($(this).scrollLeft() + data);

				//console.log('scrollWidth', $(this)[0].scrollWidth);

				$(self).data({'viewableWidth':$(self).width(), 'overAllWidth':$(this)[0].scrollWidth});
				console.log($(self).scrollLeft(),  $(self).data('overAllWidth') -  $(self).data('viewableWidth'))
				console.log($(self));
				if ($(self).scrollLeft() == 0) {
					$(left_arrow_ele).hide();
				}
				else if($(self).scrollLeft() > 0) {
					$(left_arrow_ele).show();
				}

				if ($(self).scrollLeft() == $(self).data('overAllWidth') - $(self).data('viewableWidth')) {
					$(right_arrow_ele).hide();
					//console.log('meet')

				}else {
					$(right_arrow_ele).show();
				}

				if ($('.fl-workspace-button-wrapper-relatedforms').length == 1) {
					
					if ($(self).scrollLeft() + 1 == $(self).data('overAllWidth') - $(self).data('viewableWidth')) {
						$(right_arrow_ele).hide();
						//console.log('meet')

					}else {
						$(right_arrow_ele).show();
					}
				
				};
			
			}

			
		});

		function recur(self,left_arrow_ele,right_arrow_ele){
			
			var selfz = $(self);
			
			var left_arrow_ele = $(left_arrow_ele);
			
			var right_arrow_ele = $(right_arrow_ele);
	
			var self_sl = selfz.hasScrollbarWidth();
			
			setTimeout(function(){

				if (self_sl == true) {
					right_arrow_ele.show();

				}else {
					
					left_arrow_ele.hide();
					right_arrow_ele.hide();
				};
				
			},0);
		}
    	
    	$(window).resize(function(){

    		recur(self,left_arrow_ele,right_arrow_ele);
    		
    		if ($(self).scrollLeft() == 0) {
				
				$(left_arrow_ele).hide();
			
			
			}			
			
			
    	});

    	$(window).resize();
    	
 		left_arrow_ele.on({

			'mousedown':function(){

				var accelaration = 0;
				$(this).data("event_happen_mousedown",true);
					
				console.log($(this).data("event_happen_mousedown",true));
				  $(this).data('runningScroll',setInterval(function(){
				  	
					self.trigger('scroll',[ -(accelaration = accelaration +  0.05) ]);
					
					},0));
				 
				},
			
			'mouseenter':function(){
				  $(this).data("event_happen_mouseenter",true);
				},  
			
			 'mouseup':function(){
				 clearInterval( $(this).data('runningScroll'));
				 $(this).data("event_happen_mousedown",false);
			 },
			
			'mouseleave':function(){
				 clearInterval( $(this).data('runningScroll'));
				 $(this).data("event_happen_mouseenter",false);
			 }
		});

	
		right_arrow_ele.on({

		    'mousedown':function(){
		    	
		    		var accelaration = 0;
					$(this).data("event_happen_mousedown",true);
					console.log($(this).data("event_happen_mousedown",true));
	
				  	$(this).data('runningScroll',setInterval(function(){
						self.trigger('scroll',[ +(accelaration = accelaration +  0.05) ]);
	
					},0));

				},
			
			'mouseenter':function(){
				  $(this).data("event_happen_mouseenter",true);
				},  
			
			 'mouseup':function(){
				 clearInterval($(this).data('runningScroll'));
				 $(this).data("event_happen_mousedown",false);
		
			 },
			
			'mouseleave':function(){
				 clearInterval($(this).data('runningScroll'));
				 $(this).data("event_happen_mouseenter",false);
				
			 }

		});

	};

		
}(jQuery));

