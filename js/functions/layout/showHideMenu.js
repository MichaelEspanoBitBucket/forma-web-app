$(document).ready(function(){

	//$('.fl-user-navigation-wrapper').showHideMenu();
	
});

(function($){

	$.fn.showHideMenu = function(options){

		var self  = $(this);
		var menuToggle = self.parents('.fl-main-container').find('.fl-showMenuIcon');
		var itemToToggle = $('.hideShowMenu');
		

		menuToggle.each(function(){

			$(this).on({

				'click': function(){
					
					if ($(itemToToggle).attr('show') == 'false') {
					
						$(itemToToggle).attr('show', 'true');
						
			            $(itemToToggle).addClass('fadeOutLeft');
			            
			            setTimeout(function(){
							$(itemToToggle).css({
								//'opacity':1,
								'display': 'none',
								'z-index':999999
							});

						},500);

					}else if($(itemToToggle).attr('show') == 'true'){
						
						$(itemToToggle).attr('show', 'false');

						//$*(itemToToggle).fadeIn();

						$(itemToToggle).removeClass('fadeOutLeft').addClass('fadeInLeft');
						
						setTimeout(function(){
							$(itemToToggle).css({
								//'opacity':1,
								'display': 'block',
								'z-index':999999
							});

						},500);
						
						setTimeout(function(){
			                $(itemToToggle).removeClass('fadeInLeft');
			            },1000);	

					};

				}

			});

		});
			
	}

}(jQuery));