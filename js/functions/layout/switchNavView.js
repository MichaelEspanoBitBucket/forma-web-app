var navuitype;

$(document).ready(function () {
    if($(document).children().eq(0).hasClass('view-embed-iframe') != true){
        navuitype = $("#navuitype").val();
        switchNavVIew.init('ul.fl-nav-view-sub');
        iconNavViewToggle.init('ul.fl-mainmenu-ui2');
        responsiveNav.init('.fl-user-navigation-wrapper');
    }
});

var pathname = window.location.pathname;

var urlOrganizationalChart = '/user_view/organizational_chart';
var urlPortal = '/user_view/application_portal';
var urlRequestview = '/user_view/workspace';
var urlPort =  "/user_view/port";
var urlCreateForms = '/user_view/formbuilder';
var urlGeneratePrint = '/user_view/generate';
var urlCreateReports = '/user_view/report';
var urlCreateWorkflow = '/user_view/workflow';
var urlAppConfig = '/user_view/app_configuration';
var urlFormWorkSpaceTest = '/user_view/form_workspace_test';
var urlUserOrganizationalChart = "/user_view/organizational-chart";
var urlgiReportDesigner = '/Insights/gi-report-designer';
var urlgiReport = '/Insights/gi-report';
var urlgiReportViewer = '/user_view/gi-report-viewer';
var urlyard = '/user_view/yard';
var urlyoureNotAllowedToVisitThisPage = '/user_view/youre-not-allowed-to-visit-this-page';
var urlErp = '/user_view/erp';



(function () {

    switchNavVIew = {
        'init': function (container) {

            var self = this;
            var switchNav = $(container + ' li');
            
            $(switchNav).on('click', function(){

                if ($(this).attr('navui') == 'iconAndLabel') {
                    
                    //console.log($(this).attr('navui'));
                    navuitype = $(this).attr('navui');
    
                    if (navuitype === 'iconAndLabel') {
                        self.iconViewLabel('.fl-content', '', '83%', '.fl-user-navigation-wrapper', 'block', '17%', '.fl-user-navigation-wrapper-ui2', 'none', '');
                    }

                    $(this).append('<i class="fa fa-check-square-o"></i>');

                    $(this).siblings().children().remove();
                    self.iconViewLabel('.fl-content', '', '83%', '.fl-user-navigation-wrapper', 'block', '17%', '.fl-user-navigation-wrapper-ui2', 'none', '');
                    var navuidisplay_a = 'block';
                    var navuidisplay_b = 'none';

                    $.post("/ajax/navui", {navuidisplay_a:navuidisplay_a, navuidisplay_b:navuidisplay_b, navuitype:navuitype, action: "setNavui"}, function (data) {
                        
                    });

                    
                };

                if ($(this).attr('navui') == 'iconOnly') {

                    //console.log($(this).attr('navui'));
                    navuitype = $(this).attr('navui');
        
                    if (navuitype === 'iconOnly') {
                        self.iconView('.fl-content', '', '96%', '.fl-user-navigation-wrapper-ui2', 'block', '4%', '.fl-user-navigation-wrapper', 'none', '');
                    }
                    
                    $(this).append('<i class="fa fa-check-square-o"></i>');
                    $(this).siblings().children().remove();
                
                    self.iconView('.fl-content', '', '96%', '.fl-user-navigation-wrapper-ui2', 'block', '4%', '.fl-user-navigation-wrapper', 'none', '');

                    var navuidisplay_a = 'none';
                    var navuidisplay_b = 'block';

                    $.post("/ajax/navui", {navuidisplay_a:navuidisplay_a, navuidisplay_b:navuidisplay_b, navuitype:navuitype, action: "setNavui"}, function (data) {
                        
                    });

                
                };

                $('.fl-header-option').slideUp();

            });

            $(window).bind('enterBreakpoint480', function () {
                
                //console.log($("#iconOnly").attr("navui"))
                navuitype = $("#iconOnly").attr("navui")
                var navuidisplay_a = 'none';
                var navuidisplay_b = 'block';

                $.post("/ajax/navui", {navuidisplay_a:navuidisplay_a, navuidisplay_b:navuidisplay_b, navuitype:navuitype, action: "setNavui"}, function (data) {
                    
                });
               
            });

            $(window).bind('enterBreakpoint768', function () {
                
                //console.log($("#iconOnly").attr("navui"))
                navuitype = $("#iconOnly").attr("navui")
                var navuidisplay_a = 'none';
                var navuidisplay_b = 'block';

                $.post("/ajax/navui", {navuidisplay_a:navuidisplay_a, navuidisplay_b:navuidisplay_b, navuitype:navuitype, action: "setNavui"}, function (data) {
                    
                });
               
            });

            if (navuitype === 'iconOnly') {

                //alert('iconOnly');
                self.iconView('.fl-content', '', '96%', '.fl-user-navigation-wrapper-ui2', 'block', '4%', '.fl-user-navigation-wrapper', 'none', '');
                $('#iconOnly').append('<i class="fa fa-check-square-o"></i>');
                $('#iconAndLabel').children('.fa-check-square-o').remove();
                /*$('svg.icon-svg').not('svg.icon-svg.icon-svg-announcement').css({'float':'none'});*/

            } else if (navuitype === 'iconAndLabel') {

                //alert('iconAndLabel');	
                self.iconViewLabel('.fl-content', '', '83%', '.fl-user-navigation-wrapper', 'block', '17%', '.fl-user-navigation-wrapper-ui2', 'none', '');
                $('#iconAndLabel').append('<i class="fa fa-check-square-o"></i>');
                $('#iconOnly').children('.fa-check-square-o').remove();
                /*$('svg.icon-svg').css({'float':'left'});*/
            } else if (navuitype == "" || navuitype == null) {
                //navigationVisibility();
            }

        },              
        'iconView': function (container, cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width) {
            //alert('icon view');
            iconViewInit(container, cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width);
           //console.log("ACTIVE", cont_width, navuiChoice, "NOT ACTIVE", navHide, 'ACTIVE DISPLAY', nav_choice_display, 'NOT ACTIVE DISPLAY', navHide_display);

        },
        'iconViewLabel': function (container,  cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width) {
            //alert('icon and label view');
            iconViewLabelInit(container, cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width);
           //console.log("ACTIVE", cont_width, navuiChoice, "NOT ACTIVE", navHide, 'ACTIVE DISPLAY', nav_choice_display, 'NOT ACTIVE DISPLAY', navHide_display);

        }

    };


    iconNavViewToggle = {
        'init': function (parent) {

            var self = this;
            var liToggleSub = $('li.fl-toggle-sub-ui2 > a');
            var liToggle = $(parent).children('li');
            var contentwrapper = $('.fl-content');
            //console.log(liToggleSub);
            //console.log(liToggle);

            $(contentwrapper).on({
                'mouseenter': function () {

                    $('ul.main-submenu-ui2').fadeOut();

                }

            });

            $(liToggle).on({
                'mouseenter': function () {

                    var secondLvlUl = $(this).parent().children('li').find('ul'); // ul li ul

                    $(liToggleSub).each(function () {

                        if ($(this).attr('navuiDrop') == 'false') {

                            $(secondLvlUl).fadeOut();


                        }
                        ;

                    });

                }

            });

            $(liToggleSub).on({
                'mouseenter': function () {

                    var thirdLvlUl = $(this).parents('ul.fl-mainmenu-ui2').children('li').find('ul').children('li').find('ul'); // ul li ul li ul


                    var liSubHeight = $(this).parent().height();

                    /*$('.fl-subli-icon').css({
                     'position': 'absolute',
                     
                     'right': 3
                     });*/
                    var caretItem = '<i class="fa fa-chevron-right fl-subli-icon" style="font-size:10px;"></i>';

                    $(this).append(caretItem);

                    $(liToggleSub).each(function () {

                        if ($(this).attr('navuiDrop') === 'false') {

                            $(thirdLvlUl).fadeOut();

                        }
                        ;

                    });
                },
                'mouseleave': function () {

                    $(this).find('.fa-chevron-right').remove();
                    $(this).children('.fa-folder-open-o').removeClass('fa-folder-open-o').addClass('fa-folder-o');

                }

            });

            $(liToggleSub).on({
                'click': function (event) {

                    var secondLvlUl = $(this).next('ul.main-submenu-ui2'); // li a li ul
                    event.preventDefault();

                    $(liToggleSub).attr('navuiDrop', 'false');

                    $(this).attr('navuiDrop', 'true');

                    $(this).tooltip('hide');

                    if ($(this).parent().closest('ul.main-submenu-ui2').length >= 1) {

                        $(this).parents('li.fl-toggle-sub-ui2').children('a').attr('navuiDrop', 'true');

                    }
                    ;

                    if ($(this).children('.fa-folder-o').length == 1) {
                        $(this).children('.fa-folder-o').removeClass('fa-folder-o').addClass('fa-folder-open-o');
                    }
                    ;

                    $(liToggleSub).each(function () {

                        if ($(this).attr('navuiDrop') == 'true') {
                            $(secondLvlUl).fadeIn();
                        }
                        ;

                        if ($(this).attr('navuiDrop') == 'false') {
                            //$(secondLvlUl).fadeOut();

                        }
                        ;

                    });

                }

            });

        }

    };


    responsiveNav = {
        'init': function (element) {

            var self = this;
            var fl_toggle_nav_view = $('.fl-toggle-nav-view');
            //alert('init')
            //Set breakpoint

            function navKey() {

                var key = 'navui';

                if (navuitype === 'iconOnly') {

                    //alert(1);
                    //self.iconView(this, '96%');
                    $('#iconOnly').append('<i class="fa fa-check-square-o"></i>');
                    $('#iconAndLabel').children('.fa-check-square-o').remove();
                    /*$('svg.icon-svg').not('svg.icon-svg.icon-svg-announcement').css({'float':'none'});*/

                } else if (navuitype === 'iconAndLabel') {
                    //alert(2);
                    //self.iconViewLabel(this, '83%');
                    $('#iconAndLabel').append('<i class="fa fa-check-square-o"></i>');
                    $('#iconOnly').children('.fa-check-square-o').remove();
                    // $('svg.icon-svg').css({'float':'left'});
                } else if (navuitype == "" || navuitype == null) {

                    //self.iconViewLabel(this, '83%');
                    navigationVisibility();
                   /* $('svg.icon-svg').css({'float':'left'});*/
                }
            }

            function initTriggerOnce() {

                if (pathname == urlCreateForms) {
                    triggerHidingNavOnce();
                }
                else if (pathname == urlOrganizationalChart) {
                    triggerHidingNavOnce();
                } else if (pathname == urlCreateWorkflow) {
                    triggerHidingNavOnce();
                }
                else if (pathname == urlCreateReports) {
                    triggerHidingNavOnce();
                } else if (pathname == urlGeneratePrint) {
                    triggerHidingNavOnce();
                } else if (pathname == urlAppConfig) {
                    triggerHidingNavOnce();
                } else if (pathname == urlPort) {
                    triggerHidingNavOnce();
                } else if (pathname == urlUserOrganizationalChart) {
                    triggerHidingNavOnce();
                } else if (pathname == urlgiReportDesigner) {
                    triggerHidingNavOnce();
                } else if (pathname == urlgiReportViewer) {
                     triggerHidingNavOnce();
                } else if (pathname == urlgiReport) {
                     triggerHidingNavOnce();
                } else if (pathname == urlFormWorkSpaceTest) {
                    triggerHidingNavOnce();
                } else if (pathname == urlRequestview) {
                    triggerHidingNavOnce();
                }else if (pathname == urlyard) {
                    triggerHidingNavOnce();
                }else if(pathname == urlyoureNotAllowedToVisitThisPage){
                    triggerHidingNavOnce();
                }else if(pathname == urlErp){
                    triggerHidingNavOnce();
                }else {
                    navKey();
                }
            }
            ;

            $(window).bind('exitBreakpoint320', function () {
                //console.log('EXIT BREAKPOINT 320');
               
            });
            $(window).bind('enterBreakpoint320', function () {
                //console.log('ENTER BREAKPOINT 320');
           
                navigationVisibility();
                /*$('svg.icon-svg').not('svg.icon-svg.icon-svg-announcement').css({'float':'none'});*/
                
            });

            $(window).bind('exitBreakpoint480', function () {
                //console.log('EXIT BREAKPOINT 480');
                navigationVisibility();
                
            });

            $(window).bind('enterBreakpoint480', function () {
                //console.log('ENTER BREAKPOINT 480');
                /*$('svg.icon-svg').not('svg.icon-svg.icon-svg-announcement').css({'float':'none'});*/
                navigationVisibility();

                if (navuitype === 'iconOnly') { 
                    self.iconView('.fl-content', '', '93%', '.fl-user-navigation-wrapper-ui2', 'block', '7%', '.fl-user-navigation-wrapper', 'none', '');
                }else if (navuitype === 'iconAndLabel'){
                    
                    self.iconView('.fl-content', '', '93%', '.fl-user-navigation-wrapper-ui2', 'block', '7%', '.fl-user-navigation-wrapper', 'none', '');
                    

                } else if (navuitype == "" || navuitype == null) {

                }
                fl_toggle_nav_view.addClass('isDisplayNone');
                
            });

            $(window).bind('exitBreakpoint768', function () {
                //console.log('EXIT BREAKPOINT 768');
                
                
                if (navuitype === 'iconOnly') { 

                }else if (navuitype === 'iconAndLabel'){


                } else if (navuitype == "" || navuitype == null) {

                }

                navigationVisibility();
               
            });

            $(window).bind('enterBreakpoint768', function () {
                console.log('ENTER BREAKPOINT 768');
                
                /*$('svg.icon-svg').not('svg.icon-svg.icon-svg-announcement').css({'float':'none'});*/
                
                fl_toggle_nav_view.addClass('isDisplayNone');
                navigationVisibility();
                
                
                if (navuitype === 'iconOnly') { 
                    self.iconView('.fl-content', '', '95%', '.fl-user-navigation-wrapper-ui2', 'block', '5%', '.fl-user-navigation-wrapper', 'none', '');
                }else if (navuitype === 'iconAndLabel'){
                    
                    self.iconView('.fl-content', '', '95%', '.fl-user-navigation-wrapper-ui2', 'block', '5%', '.fl-user-navigation-wrapper', 'none', '');
                    

                } else if (navuitype == "" || navuitype == null) {

                }

            });

            $(window).bind('exitBreakpoint1024', function () {
                //console.log('EXIT BREAKPOINT 1024');
                /*$('svg.icon-svg').not('svg.icon-svg.icon-svg-announcement').css({'float':'none'});*/
                navigationVisibility();

            });

            $(window).bind('enterBreakpoint1024', function () {
               //console.log('ENTER BREAKPOINT 1024');

                /*$('svg.icon-svg').not('svg.icon-svg.icon-svg-announcement').css({'float':'none'});*/
        
                fl_toggle_nav_view.removeClass('isDisplayNone').addClass('isDisplayBlock');
                
                if (navuitype === 'iconOnly') { 
                    
                    self.iconView('.fl-content', '', '96%', '.fl-user-navigation-wrapper-ui2', 'block', '4%', '.fl-user-navigation-wrapper', 'none', '');
                
                }else if (navuitype === 'iconAndLabel'){
                    
                    self.iconViewLabel('.fl-content', '', '83%', '.fl-user-navigation-wrapper', 'block', '17%', '.fl-user-navigation-wrapper-ui2', 'none', '');
                    

                } else if (navuitype == "" || navuitype == null) {

                }

                /*var pathname = window.location.pathname;*/

                if (navuitype === 'iconOnly') {

                     initTriggerOnce();   
                    $('#iconOnly').append('<i class="fa fa-check-square-o"></i>');
                    $('#iconAndLabel').children('.fa-check-square-o').remove();
                    /*$('svg.icon-svg').not('svg.icon-svg.icon-svg-announcement').css({'float':'none'});*/

                } else if (navuitype === 'iconAndLabel') {
           
                    initTriggerOnce();
                    $('#iconAndLabel').append('<i class="fa fa-check-square-o"></i>');
                    $('#iconOnly').children('.fa-check-square-o').remove();
                    /*$('svg.icon-svg').css({'float':'left'});*/
                } else if (navuitype == "" || navuitype == null) {

                    initTriggerOnce();
                    navigationVisibility();

                }
                initTriggerOnce();


            });
             
            $(window).setBreakpoints();
  

        },
        'iconView': function (container, cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width) {
            //alert('icon view');
            iconViewInit(container, cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width);

        },
        'iconViewLabel': function (container, cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width) {
            //alert('icon and label view');
            iconViewLabelInit(container, cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width);
        }

    };


    function triggerHidingNavOnce() {
        $('element').one('trigger', function () {
            self.iconViewLabel(this, '96%');
            navigationVisibility();
        });
    }

    function flcontentWandNavdisplay() {
        $('.fl-content').animate({'width': '100%'});
        $('.fl-user-navigation-wrapper').css('display', 'none');
        $('.fl-toggle-nav-view').css('display', 'none');
        
    }
    ;

    function navui2() {
        $('.fl-content').animate({'width': '100%'});
        $('.fl-user-navigation-wrapper-ui2').css('display', 'none');
        $('.fl-toggle-nav-view').css('display', 'none');
       
    }
    ;

    function notAllowedLeftbar(){
        var navDesign2 = $('.fl-user-navigation-wrapper-ui2');
        var navDesign = $('.fl-user-navigation-wrapper');
        if(navDesign2.length == 0 || navDesign.length == 0){
            navui2();
        }
    }
    notAllowedLeftbar();

    

    function navigationVisibility() {


        
        if (pathname ==  urlOrganizationalChart){
            flcontentWandNavdisplay();
            navui2();
            $('.fl-main-container').css('background', 'url(images/themes/workspace.jpg)');
        }
        ;

        if (pathname == urlPortal) {
            flcontentWandNavdisplay();
            navui2();
            $('.fl-header-option').find('li:first').hide();
        }
        ;

        
        if (pathname == urlRequestview) {
            flcontentWandNavdisplay();
            navui2();
          
        }
        ;


        if (pathname == urlPort) {
            flcontentWandNavdisplay();
            navui2();
        }
        ;

        
        if (pathname == urlCreateForms) {
            var breadCrumbs = $('ul.fl-breadcrums li a');
            var breadCrumbsActive = $('ul.fl-breadcrums li:eq(3) a');
            breadCrumbs.removeClass('breadcrumbs');
            //breadCrumbsActive.addClass('fl-breadcrums-active');
            var matches = breadCrumbs.filter(function () {
                return "/user_view/formbuilder?formID=0".indexOf($(this).attr('href')) >= 0;
            });

            matches.addClass('breadcrumbs');
            flcontentWandNavdisplay();
            navui2();
            //alert(urlCreateForms);
            
        }
        ;

        
        if (pathname == urlGeneratePrint) {
            flcontentWandNavdisplay();
            navui2();
        }
        ;

        
        if (pathname == urlCreateWorkflow) {
            var breadCrumbs = $('ul.fl-breadcrums li a');
            var breadCrumbsActive = $('ul.fl-breadcrums li:eq(4) a');
            breadCrumbs.removeClass('breadcrumbs');
            //breadCrumbsActive.addClass('fl-breadcrums-active');
            var matches = breadCrumbs.filter(function () {
                return document.location.href.indexOf($(this).attr('href')) >= 0;
            });

            matches.addClass('breadcrumbs');
            flcontentWandNavdisplay();
            navui2();
        }
        ;

        
        if (pathname == urlCreateReports) {
            flcontentWandNavdisplay();
            navui2();
        }
        ;

        
        if (pathname == urlAppConfig) {
            flcontentWandNavdisplay();
            navui2();
        }
        ;

        
        if (pathname == urlFormWorkSpaceTest) {
            flcontentWandNavdisplay();
            navui2();

        }
        ;


        if (pathname == urlUserOrganizationalChart) {
            flcontentWandNavdisplay();
            navui2()
            
        }
        ;

        
        if (pathname == urlgiReportDesigner) {
            flcontentWandNavdisplay();
            navui2();
        }
        ;

        
        if (pathname == urlgiReport) {
            flcontentWandNavdisplay();
            navui2();
        }
        ;

        
        if (pathname == urlgiReportViewer) {
            flcontentWandNavdisplay();
            navui2();
        };

        
        if (pathname == urlyard) {
            flcontentWandNavdisplay();
            navui2();
        };

        
        if (pathname == urlyoureNotAllowedToVisitThisPage) {
            flcontentWandNavdisplay();
            navui2();
        };
        
        if(pathname == urlErp){
            flcontentWandNavdisplay();
            navui2();
        } 

        

    }
    ;

    function iconViewInit(container, cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width) {

        $(container).css({
            'width': cont_width, // container width .content
            'display':cont_display
        });


        $(navuiChoice).css({
            'width': nav_choice_width, // width of chosen nav
            'display':nav_choice_display
        });

        $(navHide).css({
            'width': navHide_width, // width of chosen nav
            'display':navHide_display
        });

    }
    ;

    function iconViewLabelInit(container, cont_display, cont_width, navuiChoice, nav_choice_display, nav_choice_width, navHide, navHide_display, navHide_width) {


        $(container).css({
            'width': cont_width, // container width .content
            'display':cont_display
        });

        $(navuiChoice).css({
            'width': nav_choice_width, // width of chosen nav
            'display':nav_choice_display
        });

        $(navHide).css({
            'width': navHide_width, // width of chosen nav
            'display':navHide_display
        });
        
    }
    ;


})();