$(document).ready(function () {

    $('.group').attr('toggle-evt-option', 'singleClick');
    $('.group').toggleUpDown();
   
    $('.fl-toggleUpDownWrapper').attr('toggle-evt-option', 'singleClick');
    $('.fl-toggleUpDownWrapper').toggleUpDown();
    

    $('.fl-report-wrapper-content').attr('toggle-evt-option', 'singleClick');
    $('.fl-report-wrapper-content').toggleUpDown();
    
    $('#fl-workspace-controls').attr('toggle-evt-option', 'dbClick');
    $('#fl-workspace-controls').toggleUpDown();
    
});


//function for toggleUpDown
(function ($) {

    $.fn.toggleUpDown = function () {

        var OPTION = {
            dbClick: "dbClick",
            singleClick: "singleClick"
        }

        return this.each(function () {



            var self = $(this);
            var button = self.parents('.toggleParent').find('.fl-toggleUpDown');
            var viewDetailsBtn = self.parents('.toggleParent').find('.fl-revealWrapperTrigger');
            var switchingAppView = self.parents('.toggleParent').find('.switchingAppView');
            var defaultE = '';

            self.attr('toggleupdown', 'down');

            button.each(function () {
               
                if (self.attr('toggle-evt-option') == OPTION.singleClick) {
                  
                    $(this).on({
                        'click': function () {
                            
                            if (self.attr('toggleupdown') == 'down') {

                                self.attr('toggleupdown', 'up');

                                self.stop(false, false).slideUp('slow', function () {
                                    //  
                                });

                                button.removeClass('fa-arrow-circle-up').addClass('fa-arrow-circle-down');
                                button.tooltip('hide')
                                        .attr('data-original-title', 'Show This')
                                        .tooltip('fixTitle')
                                        .tooltip('show');

                                hideElement();

                            } else if (self.attr('toggleupdown') == 'up') {

                                self.attr('toggleupdown', 'down');
                                self.stop(false, false).slideDown('slow', function () {
                                    //  
                                });


                                button.removeClass('fa-arrow-circle-down').addClass('fa-arrow-circle-up');
                                button.tooltip('hide')
                                        .attr('data-original-title', 'Hide This')
                                        .tooltip('fixTitle')
                                        .tooltip('show');

                                showElement();

                            }
                            ;

                        }

                    });

                }

                if(self.attr('toggle-evt-option') == OPTION.dbClick){
                   // $(this).attr('toggle-evt-show', 'false')
                    
                    $(this).on({
                        'dblclick': function () {
                            if (self.attr('toggleupdown') == 'down') {
                                $('.fl-toggleUpDown').attr('toggle-evt-show', true)
                                console.log("toggleupdown", $(this).attr('toggle-evt-show'))
                                self.attr('toggleupdown', 'up');

                                self.stop(false, false).slideUp('slow', function () {
                                    //  
                                });

                                button.tooltip('hide')
                                        .attr('data-original-title', 'Double click to show')
                                        .tooltip('fixTitle')
                                        
                                
                                hideElement();

                            } else if (self.attr('toggleupdown') == 'up') {
                                $('.fl-toggleUpDown').attr('toggle-evt-show', false)
                                console.log("toggleupdown", $(this).attr('toggle-evt-show'))
                                self.attr('toggleupdown', 'down');
                                self.stop(false, false).slideDown('slow', function () {
                                    //  
                                });

                                button.tooltip('hide')
                                        .attr('data-original-title', 'Double click to hide')
                                        .tooltip('fixTitle')
                                        

                                showElement();

                            }
                            ;

                        }

                    });

                }
                

            });

            function hideElement() {
                $(viewDetailsBtn).parent().hide();
                $(switchingAppView).parent().hide();

            }
            ;

            function showElement() {
                $(viewDetailsBtn).parent().show();
                $(switchingAppView).parent().show();

                if ($(switchingAppView).attr('view') === 'calendar-view') {
                    $(viewDetailsBtn).parent().hide();
                }
            }
            ;


        });
    };

}(jQuery));