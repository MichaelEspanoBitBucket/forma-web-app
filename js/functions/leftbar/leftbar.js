$(document).ready(function(){
	//var USERAUTH = getCookie('MODULESCOOKIES');
	//console.log(USERAUTH)
	GetModules.getLeftBar();
	GetModules.getCustomNavigation();
})

GetModules = {
    "getLeftBar" : function(){
		$("body").on("click",".modules_load",function(){
			var self = this;
			var forms = $(self).data("forms");
			var nav_type_ext = if_undefinded($(self).attr("nav-type-ext"),"");
			var data_load = $(self).attr("data-load");
			
			if (forms == false) {
				console.time("StartTime");
				$.ajax({
				    "url": "/ajax/search",
				    "type": "POST",
				    "cache": true,
				    "data": {
						"action": data_load,
						"nav_type_ext":nav_type_ext
				    },
				    "success" : function(data){

					//console.timeEnd("StartTime");
					 //console.log(data)
					$(self).closest("ul").find(".modules-dropdown").html(data);
					$(self).data("forms",true);
					//alert(nav_type_ext)
					//
					if(nav_type_ext==""){
						$(".mainmenu li.fl-toggle-sub").children('a').off("click");
						toggleSubMenuLeftBar();
					}else if(nav_type_ext=="-ui2"){
						$(".fl-mainmenu-ui2 li.fl-toggle-sub-ui2 > a").children('a').off("click");
						iconNavViewToggle.init('ul.fl-mainmenu-ui2');
						
					}
					
					
				    }
				})
			}
			
		})
    },
    "getCustomNavigation" : function(){
    	$("body").on("mouseover",".getSideBarNavigation",function(){
    		var dis_parent = this;
    		if($(this).attr("load")=="true"){
    			$(dis_parent).attr("load","false");
    			$.ajax({
				    "url": "/ajax/navigation",
				    "type": "POST",
				    "cache": true,
				    "data": {
						"action": "getLeftBar",
				    },
				    "success" : function(data){
				    	// console.log("data",data);
				    	$(dis_parent).find(".loading_navigation").remove();
				    	$(dis_parent).append(data);
    				}
				})
    		}
    	})
    }
}