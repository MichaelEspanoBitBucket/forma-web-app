getFunctions = {
    selectOne : function(){
        // Delete now the message
	$(".selectCheckBox").on("click",function(){
	    if(this.checked){
                // Select Single Checkbox
                var list = $(':checkbox[name="selectCheckBox[]"]:checked').map(function(){
                    return $(this).attr("data-id"); // Get an ID of the document
                    }).get().join(',');
                    console.log(list)
               
            }else{
                // Remove Unchecked ID in the textbox
                var list = $(':checkbox[name="selectCheckBox[]"]:checked').map(function(){
                    return $(this).attr("data-id"); // Get an ID of the document
                    }).get().join(',');
                    console.log(list)
                
            }
	});
    },
    selectAll : function(){
        // Delete now the message
	$(".selectAll").on("click",function(){
	    if(this.checked){
                $(".selectCheckBox").each(function(){
                    $(this).prop("checked", true)
                    // Select Single Checkbox
                    var list = $(':checkbox[name="selectCheckBox[]"]:checked').map(function(){
                    return $(this).attr("data-id"); // Get an ID of the document
                    }).get().join(',');
                    console.log(list)
                    
                });
            }else{
                $(".selectCheckBox").each(function(){
                    $(this).prop("checked", false)
                    // Remove Unchecked ID in the textbox
                    var list = $(':checkbox[name="selectCheckBox[]"]:checked').map(function(){
                    return $(this).attr("data-id"); // Get an ID of the document
                    }).get().join(',');
                    console.log(list)
                });
            }
	});
    }
}

settings = {
    setting_landing_page : function(){
	// Set last page as landing page
	$("body").on("change","[name='set_last_page']",function(){
	    var this_val = $(this).val();
	    if(this.checked){
		$(this).val("1");
		$('[name="page_set"], [name="set_page"]').attr("disabled",true);
		$('[name="other_page"]').attr("disabled",true);
		//$(".setting_landing_page").attr("disabled",true);
		$(".setting_landing_page").trigger("click");
	    }else{
		$(this).val("0");
		$('[name="page_set"], [name="set_page"]').attr("disabled",false);
		$('[name="other_page"]').attr("disabled",false);
		//$(".setting_landing_page").attr("disabled",false);
	    }
	    
	});
    
	var page_set = $('[name="page_set"], select[name="set_page"]').val();
	if (page_set == "others") {
	    $("#other_page").show();
	}else{
	    $("#other_page").hide();
	}
	$("body").on("change",'[name="page_set"], select[name="set_page"]',function(){
	    var page_set = $(this).val();
	    if (page_set == "others") {
		$("#other_page").show();
	    }else{
		$("#other_page").hide();
	    }
	});
    
	$("body").on("click",".setting_landing_page",function(){
	    var page_set = $('[name="page_set"]').val();
	    var type = $('[name="page_set"]').find('option:selected').attr("data-type");
	    var noti = "";
	    
		var last_page = $("[name='set_last_page']").val();
		if (last_page != "1") {
		    if (page_set == "others") {
			var page = $('[name="other_page"]').val();
			var type = type;
			if (!validateURL(page)) {
			    showNotification({
				message: "Not valid URL.",
				type: "error",
				autoClose: true,
				duration: 3
			    });
			    noti = "no";
			}else{
			    if ($.trim(page) != "") {
				page = $('[name="other_page"]').val();
				var type = type;
				noti = "ok";
			    }else{
				showNotification({
				    message: "Not allowed space only",
				    type: "error",
				    autoClose: true,
				    duration: 3
				});
				noti = "no";
			    }
			}
			
		    }else{
			var page = page_set;
			var type = type;
			noti = "ok";
		    }
		}else{
		    var page = $('#set_last_page_landing').attr("data-page");
		    var type = type;
		    noti = "ok";
		}

	    
	    
	    ui.block();
	    if (noti == "ok") {
		$.ajax({
		    type : "POST",
		    url  : "/ajax/settings",
		    data : {action:"landing_page",page:page,type:type},
		    success : function(data){
			var data_json = JSON.parse(data);
			if (data_json['noti'] == "Page was successfully set.") {
			    showNotification({
				message: data_json['noti'],
				type: "success",
				autoClose: true,
				duration: 3
			    });
			    ui.unblock();
			}
			
		    }
		})
	    }else{
	    ui.unblock();
	    }
	    
	})    
	    //var param_url = String(window.location);
	//    if ($(this).is(':checked')) {
	//	//var conf = "Are you sure do you want to set this as the default landing page?";
	//	//jConfirm(conf, 'Confirmation Dialog', '', '', '', function(answer) {
	//	//    if (answer == true) {
	//		$.ajax({
	//		    type	:	"POST",
	//		    url	:	"/ajax/settings",
	//		    dataType:	"json",
	//		    data 	:	{action:"landing_page",param_url:param_url},
	//		    success	:	function(e){
	//		       if (e.noti=="Page was successfully set.") {
	//			   var newjAlert = new jAlert( e.noti, "","", "", "", function(){});
	//		       newjAlert.themeAlert('jAlert2');	
	//		       }
	//		    }
	//		});
	//	//    }else{
	//	//	 $(".setting_landing_page").prop('checked', false);
	//	//    }
	//	//});
	//    }else{
	//	//var conf = "Do you want to remove this as your default landing page?";
	//	//jConfirm(conf, 'Confirmation Dialog', '', '', '', function(answer) {
	//	//    if (answer == true) {
	//		$.ajax({
	//		    type	:	"POST",
	//		    url	:	"/ajax/settings",
	//		    dataType:	"json",
	//		    data 	:	{action:"landing_page_remove",param_url:param_url},
	//		    success	:	function(e){
	//		       if (e.noti=="Page was successfully remove.") {
	//			   var newjAlert = new jAlert( e.noti, "","", "", "", function(){});
	//			   newjAlert.themeAlert('jAlert2');
	//		       }
	//		    }
	//		});
	//	//    }else{
	//	//	$(".setting_landing_page").prop('checked', true);
	//	//    }
	//	//});
	//    }
	    
	    
	//})
    }
}

$(document).ready(function(){
    $("body").click(function(){
	// $(".updates_settings").popover('hide');	
    });
    
    getFunctions.selectOne();
    getFunctions.selectAll();
    // $(".updates_settings").popover("show");
    // Settings
    settings.setting_landing_page();
});


function validateURL(value) {
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
}