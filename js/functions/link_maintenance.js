$(document).ready(function(){
  // alert(123)
  
  linkMaintenance.init();


});


var linkMaintenance = {
  "init":function(){
    this.addLink('#link-newEntry');
    this.deleteLink('.link-remove');
    this.editLink('.link-settings');
    this.saveLinkMaintenance();
    this.bindEvent();
    this.clearAll('#link-reset');
    
    if(window.location.pathname == "/user_view/link_maintenance"){
      this.hotKeys();
    }
    
    $(".fl-widget-wrapper-scroll").perfectScrollbar();
  },
  "hotKeys":function(){
    $('body').on({
      "keydown":function(e){
        var keycode = e.keyCode||e.which;
        if((keycode == 83)&& e.ctrlKey ){
          e.preventDefault();
          $('#link-open-save-dialog').click();
        }
      }
    })
  },
  "bindEvent":function(){
    $('.link-menu-ul').sortable();
    $('body').on('input','.link-url-input',function(){

      var targetFieldChange = $(this).closest('.link-maintenance-duplicate-target').find('.link-name-input:eq(0)');
      var targetFieldValue = $.trim(targetFieldChange.val());
      var urlValue = $(this).val();
      
      if((CheckUrl(urlValue)&&CheckUrl(targetFieldValue)) || (targetFieldValue=="" &&CheckUrl(urlValue))){
        targetFieldChange.val(urlValue);
      }
      
    });
  },
  "addLink": function(element){
    var self = this;
    $(element).on('click',function(){
      
       var addLinkContent = '<div class="add-link-modal-container">'+
        '<div class="link-maintenance-duplicate-target">'+
                '<div class="fL"><h1 style="font-size: 17px;"></h1></div>'+
                '<div class="fR"><i class="fa fa-plus plus-link-maintenance cursor fl-fa-btn"></i> <i style="display:none;" class="fa fa-minus minus-link-maintenance cursor fl-fa-btn"></i></div>'+
                '<div class="section clearing fl-field-style">'+
                  
                      '<div class="column div_1_of_2">'+
                          '<span class="">Link Title:</span>'+
                          '<input type="text" class="form-text link-name-input">'+
                      '</div> '+
                  
                  
                      '<div class="column div_1_of_2">'+
                          '<span class="">URL:</span>'+
                          '<input type="text" class="form-text link-url-input">'+
                      '</div> '+
                  
              '</div>'+    
          '</div>'+    
          '</div>';

      var ret = '<h3 class="fl-margin-bottom">';
      ret += '<i class="fa fa-link"></i> Add Link';
      ret += '</h3>';
      ret += '<div class="hr"></div>';
      ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
      ret += '<div class="content-dialog" style="height: auto;">';
        ret += addLinkContent;
        
        ret += '<div class="fields" style="">';
              ret += '<div class="label_basic"></div>';
              ret += '<div class="input_position" style="margin-top:5px;">';
              ret += ' <input type="button" class="btn-blueBtn fl-margin-right fl-positive-btn" id="save-link-ele" value="OK">';
              ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
          ret += '</div>';
      ret+= '</div>';
      var newDialog = new jDialog(ret, "", "650", "800", "", function() {   

      });
      newDialog.themeDialog("modal2");
      MakePlusPlusRowV2({
        "tableElement": '.add-link-modal-container',
        "rules":[{
            "eleMinus":".minus-link-maintenance",
            "eleMinusTarget":".link-maintenance-duplicate-target",
            "elePlus":".plus-link-maintenance",
            "elePlusTarget":".link-maintenance-duplicate-target",
            "elePlusTargetFn":function(){

                var self = $(this);
                self.find('input.link-name-input, input.link-url-input').each(function(){
                    $(this).val("");
                });
                if(self.closest('.add-link-modal-container').find('.link-maintenance-duplicate-target').length > 1){
                    self.closest('.add-link-modal-container').find('.minus-link-maintenance').show();
                }
                self.closest('.content-dialog-scroll').perfectScrollbar("update");

            },
            "eleMinusTargetFn":function(){

            	
            	
            	if($('.add-link-modal-container').find('.link-maintenance-duplicate-target').length == 1 ){
            		 $('.add-link-modal-container').find('.minus-link-maintenance').each(function(){
            		 	$(this).hide();
            		 });
                }
            }
        }]
      }); 
     
      self.saveLink('#save-link-ele');
          
      


    });
  },
  "saveLink":function(element){

    $(element).on('click',function(){
      var nullFields = [];
      var message = ""; 
      var linkName = "";
      var ulLinkElement = $('.link-menu-ul');
      var urlValue="";
      var error_ctr = 0;
      $('.link-maintenance-duplicate-target').each(function(){
        linkName = $(this).find('.link-name-input:eq(0)').val();
        urlValue  = $(this).find('.link-url-input').val();
        if($.trim(linkName)==""){
          error_ctr ++;
           message = "Please Fill Up Parameter Link Title";
          return;
        }
        if($.trim(urlValue)==""){
          error_ctr ++;
           message = "Please Fill Up Parameter URL";
          return;
        }

        if(!CheckUrl(urlValue)){
          error_ctr ++;
          message = "Invalid URL, please check if your URL have http:// or https://";
          return;
        }
        var linkMenuLi = "";
            linkMenuLi += '<li class="link-menu-li" json-data="">';
              linkMenuLi += '<div class="">';
                linkMenuLi += '<div style="float:left" class="link-menu" title="'+linkName+'" data-link-title="'+linkName+'" data-url-val ="'+urlValue+'">';
                  linkMenuLi += '<div class="link-menu-left link-title fl-table-ellip" >'+linkName+'</div>';
                  linkMenuLi += '<div class="link-menu-right">';
                    linkMenuLi += '<span class="link-menu-type" style=""></span>';
                    linkMenuLi += '<a class="link-remove cursor"><i class="fa fa-trash"></i></a>';
                    linkMenuLi += ' <a class="link-settings cursor"><i class="fa fa-pencil"></i></a>';
                  linkMenuLi += '</div>';
                  linkMenuLi += '<div class="clearfix"></div>';
                linkMenuLi += '</div>';
                linkMenuLi += '<div class="clearfix"></div>';
              linkMenuLi += '</div>';
            linkMenuLi += '</li>';
       
           $('.link-menu-ul').append(linkMenuLi); 
          
       
      });
      
      if(error_ctr>0){
        showNotification({
          message: message,
          type: "error",
          autoClose: true,
          duration: 3
        });
        return;
      }
      $('.fl-closeDialog').click();

      if($('.link-menu-li').exists()){
        $('.no-nav').addClass('isDisplayNone');
      }else{
         $('.no-nav').removeClass('isDisplayNone');
      }
    });
  },
  "deleteLink": function(element){
      $('body').on('click',element,function(){
        var disEle = $(this);
        var message = 'Are you sure you want to delete this link?';
        var newConfirm = new jConfirm(message, 'Confirmation Dialog','', '', '', function(r){
            if(r==true){
                disEle.closest('.link-menu-li').remove();
                if($('.link-menu-li').exists()){
                  $('.no-nav').addClass('isDisplayNone');
                }else{
                   $('.no-nav').removeClass('isDisplayNone');
                }
            }
        });
        newConfirm.themeConfirm("confirm2", {
            'icon':'<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>' 
        });
      
      });
  },
  "editLink":function(element){
    $('body').on('click',element,function(){
        var disEle = $(this); 
        var linkName = $(this).closest('.link-menu').attr('data-link-title');
        var urlValue = $(this).closest('.link-menu').attr('data-url-val');
        var editLinkContent = '<div class="add-link-modal-container">'+
         '<div class="link-maintenance-duplicate-target">'+
                 '<div class="fL"><h1 style="font-size: 17px;"></h1></div>'+
                 '<div class="section clearing fl-field-style">'+
                   
                       '<div class="column div_1_of_2">'+
                           '<span class="">Link Title:</span>'+
                           '<input type="text" class="form-text link-name-input">'+
                       '</div> '+
                   
                   
                       '<div class="column div_1_of_2">'+
                           '<span class="">URL:</span>'+
                           '<input type="text" class="form-text link-url-input">'+
                       '</div> '+
                   
               '</div>'+    
           '</div>'+    
           '</div>';
        var ret = '<h3 class="fl-margin-bottom">';
        ret += '<i class="fa fa-link"></i> Edit Link';
        ret += '</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
        ret += '<div class="content-dialog" style="height: auto;">';
          ret += editLinkContent;
          
          ret += '<div class="fields" style="">';
                ret += '<div class="label_basic"></div>';
                ret += '<div class="input_position" style="margin-top:5px;">';
                ret += ' <input type="button" class="btn-blueBtn fl-margin-right" id="edit-link-ele" value="Save">';
                ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
            ret += '</div>';
        ret+= '</div>';
        var newDialog = new jDialog(ret, "", "650", "800", "", function() {   

        });
        newDialog.themeDialog("modal2");
        $('.link-name-input').val(linkName);
        $('.link-url-input').val(urlValue);
       
        $('#edit-link-ele').on('click',function(){
          var error_ctr=0;
          var newLinkName =  $('.link-name-input').val();
          var newURL = $('.link-url-input').val();
          var message = "";
          var nullFields = [];
         
           if(!CheckUrl(newURL)){
             message ="Invalid URL, please check if your URL have http:// or https://";
             error_ctr++;
             
           }
          if($.trim(newLinkName)==""){
             nullFields.push('Link Title');
            error_ctr++;
             message = "Please Fill Up Parameters:" + nullFields.join(',');
            
          }
          if($.trim(newURL)==""){
            nullFields.push('URL');
            error_ctr++;
             message = "Please Fill Up Parameters:" + nullFields.join(',');
          }
          
         
        if(error_ctr>0){
            showNotification({
                message: message,
                type: "error",
                autoClose: true,
                duration: 3
            });
            return;
        }
            disEle.closest('.link-menu').attr('data-link-title',newLinkName);
            disEle.closest('.link-menu').find('.link-title:eq(0)').text(newLinkName);
            disEle.closest('.link-menu').attr('data-url-val',newURL);
            $('.fl-closeDialog').click();
        });
    });
  },
  "saveLinkMaintenance" : function(){
    $("#link-open-save-dialog").click(function(){
        var linkArray = [];
        var linkObj = $(".link-menu-li");
        linkObj.each(function(){
            linkArray.push({
                "link-value" : $(this).find(".link-menu").attr("data-link-title"),
                "link-url" : $(this).find(".link-menu").attr("data-url-val")
            })
        })
        linkArray = JSON.stringify(linkArray);
        ui.block();
        $.post("/ajax/link_maintenance",{action:"saveLinkMaintenance",linkArray:linkArray},function(data){
            showNotification({
                message: "Link Maintenance has been successfully updated.",
                type: "success",
                autoClose: true,
                duration: 3
            })
            ui.unblock();
        })
    })
  },
  "clearAll":function(element){
    $(element).click(function(){
      $('.link-menu-ul').empty().next('.no-nav').removeClass('isDisplayNone');
    });
  }
}

