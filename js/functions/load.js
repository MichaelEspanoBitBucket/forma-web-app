/*FOR NEW UI*/
var jsonUserData = {
    search_value: "",
    limit: 10,
}
var jsonDeactivateUserData = {
    search_value: "",   
    limit: 10,
}
var jsonGuestUserData = {
    search_value: "",   
    limit: 10,
}
var jsonDeactivateGuestUserData = {
    search_value: "",   
    limit: 10,
}
var columnSortActivate = [];
var columnSortDeactivate = [];
$(document).ready(function(){
        
        
        
    // Load users lis on Table
    var pathname = window.location.pathname;
    if (pathname=="/settings") {
        load.loadTableUser();
        load.loadTableDeactivatedUser();
        
    }
    
    if (pathname=="/user_view/user_settings") {
        
        // Deactivated Guest User
        /* Guest User */
        $("body").on("click",".viewDeactivatedGuestUser",function(){
            $("#DeactivatedGuestloadUser").dataTable().fnDestroy();
            GuestUserDataTable.GuestDeactivateddefaultData();
        })
        // search
            $("body").on("keyup","#txtDeactivatedGuestUserDatatable",function(e){
                if (e.keyCode == "13") {
                    $("#DeactivatedGuestloadUser").dataTable().fnDestroy();
                    jsonDeactivateGuestUserData['search_value'] = $(this).val();
                    GuestUserDataTable.GuestDeactivateddefaultData();
                    $("#DeactivatedGuestloadUser").css("width","100%");
                    $('dataTables_paginate').remove();
                }
            })
            $("body").on("click","#btnSearchDeactivatedGuestUserDatatable",function(e){
                    $("#DeactivatedGuestloadUser").dataTable().fnDestroy();
                    jsonDeactivateGuestUserData['search_value'] = $("#txtDeactivatedGuestUserDatatable").val();
                    GuestUserDataTable.GuestDeactivateddefaultData();
                    $("#DeactivatedGuestloadUser").css("width","100%");
                    $('dataTables_paginate').remove();
            })
            // sort
            $("body").on("click","#DeactivatedGuestloadUser th",function(){
                var cursor = $(this).css("cursor");
                jsonDeactivateGuestUserData['column-sort'] = $(this).attr("field_name");
                if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
                    return;
                }
                var indexcolumnSort = columnSortActivate.indexOf($(this).attr("field_name"));
    
                $(this).closest("#DeactivatedGuestloadUser").find(".sortable-image").html("");
                if(indexcolumnSort==-1){
                    columnSortActivate.push($(this).attr("field_name"))
                    $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
                    jsonDeactivateGuestUserData['column-sort-type'] = "ASC";
                }else{
                    columnSortActivate.splice(indexcolumnSort,1);
                    $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
                    jsonDeactivateGuestUserData['column-sort-type'] = "DESC";
                }
                
                var thisTh = this;
                $("#DeactivatedGuestloadUser").dataTable().fnDestroy();
                GuestUserDataTable.GuestDeactivateddefaultData(function(){
                    addIndexClassOnSort(thisTh);
                });
            })
            $(".searchInactiveGuestUserLimitPerPage").change(function(e) {
                // if (e.keyCode == "13") {
                    var val = parseInt($(this).val());
                    jsonDeactivateGuestUserData['limit'] = val;
                    $("#DeactivatedGuestloadUser").dataTable().fnDestroy();
                    // var self = this;
                    var oTable = GuestUserDataTable.GuestDeactivateddefaultData();
                // }
            })
        
        /* Guest User */
        $("body").on("click",".viewGuestUser",function(){
            $("#GuestloadUser").dataTable().fnDestroy();
            GuestUserDataTable.GuestdefaultData();
        })
        // search
            $("body").on("keyup","#txtGuestUserDatatable",function(e){
                if (e.keyCode == "13") {
                    $("#GuestloadUser").dataTable().fnDestroy();
                    jsonGuestUserData['search_value'] = $(this).val();
                    GuestUserDataTable.GuestdefaultData();
                    $("#GuestloadUser").css("width","100%");
                }
            })
            $("body").on("click","#btnSearchGuestUserDatatable",function(e){
                    $("#GuestloadUser").dataTable().fnDestroy();
                    jsonGuestUserData['search_value'] = $("#txtGuestUserDatatable").val();
                    GuestUserDataTable.GuestdefaultData();
                    $("#GuestloadUser").css("width","100%");
            })
            // sort
            $("body").on("click","#GuestloadUser th",function(){
                var cursor = $(this).css("cursor");
                jsonGuestUserData['column-sort'] = $(this).attr("field_name");
                if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
                    return;
                }
                var indexcolumnSort = columnSortActivate.indexOf($(this).attr("field_name"));
    
                $(this).closest("#GuestloadUser").find(".sortable-image").html("");
                if(indexcolumnSort==-1){
                    columnSortActivate.push($(this).attr("field_name"))
                    $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
                    jsonGuestUserData['column-sort-type'] = "ASC";
                }else{
                    columnSortActivate.splice(indexcolumnSort,1);
                    $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
                    jsonGuestUserData['column-sort-type'] = "DESC";
                }
                var thisTh = this;
                $("#GuestloadUser").dataTable().fnDestroy();
                GuestUserDataTable.GuestdefaultData(function(){
                    addIndexClassOnSort(thisTh);
                });
            })
        
        $(".searchActiveGuestUserLimitPerPage").change(function(e) {
            // if (e.keyCode == "13") {
                var val = parseInt($(this).val());
                jsonGuestUserData['limit'] = val;
                $("#GuestloadUser").dataTable().fnDestroy();
                // var self = this;
                var oTable = GuestUserDataTable.GuestdefaultData();
            // }
        })
        
        /*  Active  */
        // onload
        GetUserDataTable.defaultData();
        // search
        $("body").on("keyup","#txtActivatedUserDatatable",function(e){
            if (e.keyCode == "13") {
                $("#loadUser").dataTable().fnDestroy();
                jsonUserData['search_value'] = $(this).val();
                GetUserDataTable.defaultData();
                $("#loadUser").css("width","100%");
            }
        })
        $("body").on("click","#btnSearchActivatedUserDatatable",function(e){
                $("#loadUser").dataTable().fnDestroy();
                jsonUserData['search_value'] = $("#txtActivatedUserDatatable").val();
                GetUserDataTable.defaultData();
                $("#loadUser").css("width","100%");
        })
        // sort
        $("body").on("click","#loadUser th",function(){
            var cursor = $(this).css("cursor");
            jsonUserData['column-sort'] = $(this).attr("field_name");
            if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
                return;
            }
            var indexcolumnSort = columnSortActivate.indexOf($(this).attr("field_name"));

            $(this).closest("#loadUser").find(".sortable-image").html("");
            if(indexcolumnSort==-1){
                columnSortActivate.push($(this).attr("field_name"))
                $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
                jsonUserData['column-sort-type'] = "ASC";
            }else{
                columnSortActivate.splice(indexcolumnSort,1);
                $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
                jsonUserData['column-sort-type'] = "DESC";
            }
            var thisTh = this;
            $("#loadUser").dataTable().fnDestroy();
            GetUserDataTable.defaultData(function(){
                addIndexClassOnSort(thisTh);
            });
        })
        $(".searchActiveUserLimitPerPage").change(function(e) {
            // if (e.keyCode == "13") {
                var val = parseInt($(this).val());
                jsonUserData['limit'] = val;
                $("#loadUser").dataTable().fnDestroy();
                // var self = this;
                var oTable = GetUserDataTable.defaultData();
            // }
        })

        /*  Deactivate  */
        // onload
        GetDeactivateUserDataTable.defaultData();
        // search
        $("body").on("keyup","#txtDeactivatedUserDatatable",function(e){
            if (e.keyCode == "13") {
                $("#loadDeactivatedUser").dataTable().fnDestroy();
                jsonDeactivateUserData['search_value'] = $(this).val();
                GetDeactivateUserDataTable.defaultData();
                $("#loadDeactivatedUser").css("width","100%");
            }
        })
        $("body").on("click","#btnSearchDeactivatedUserDatatable",function(e){
                $("#loadDeactivatedUser").dataTable().fnDestroy();
                jsonDeactivateUserData['search_value'] = $("#txtDeactivatedUserDatatable").val();
                GetDeactivateUserDataTable.defaultData();
                $("#loadDeactivatedUser").css("width","100%");
        })
        // sort
        $("body").on("click","#loadDeactivatedUser th",function(){
            var cursor = $(this).css("cursor");
            jsonDeactivateUserData['column-sort'] = $(this).attr("field_name");
            if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
                return;
            }
            var indexcolumnSort = columnSortDeactivate.indexOf($(this).attr("field_name"));

            $(this).closest("#loadDeactivatedUser").find(".sortable-image").html("");
            if(indexcolumnSort==-1){
                columnSortDeactivate.push($(this).attr("field_name"))
                $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
                jsonDeactivateUserData['column-sort-type'] = "ASC";
            }else{
                columnSortDeactivate.splice(indexcolumnSort,1);
                $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
                jsonDeactivateUserData['column-sort-type'] = "DESC";
            }
            var thisTh = this;
            $("#loadDeactivatedUser").dataTable().fnDestroy();
            GetDeactivateUserDataTable.defaultData(function(){
                addIndexClassOnSort(thisTh);
            });
        })
        $(".searchInactiveUserLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonDeactivateUserData['limit'] = val;
            $("#loadDeactivatedUser").dataTable().fnDestroy();
            // var self = this;
            var oTable = GetDeactivateUserDataTable.defaultData();
        // }
        })
    }

    if (pathname=="/audit_logs") {
        load.loadAuditLogs();
    }
        

        
    // Load User onclick
    $("body").on("click",".showAllUser",function(){
        $("#loadUser").dataTable().fnDestroy(); 
        load.loadTableUser();
      
    });
    
    // Load Deactivated User onclick
    $("body").on("click",".showAllUserDeactivatedUser",function(){
        $("#loadDeactivatedUser").dataTable().fnDestroy(); 
        load.loadTableDeactivatedUser();
        $("#loadDeactivatedUser").css("width","100%");
    });
    
});

var mTb;
load = {
        loadTableUser : function(){
                var oTable = $('#loadUser').dataTable( {
                     "bProcessing": true,
                     "bRetrieve": true,
                     //"bPaginate": true,//"sEcho":3,//"iTotalDisplayRecords":10,//"iTotalRecords":"10",
                     //"iTotalDisplayRecords":"10",//"bServerSide": true,
                     "sAjaxSource": "/ajax/load",
                     "sPaginationType": "full_numbers",
                     "sAjaxDataProp": "company_user",
                     "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     "aoColumns": [
                         //{ "mData": "" },
                        // { "mData": "image" },
                         // { "mData": "email" },
                         // { "mData": "displayName" },
                         // { "mData": "fullname" },
                         // { "mData": "position" },
                         // { "mData": "department" },
                         // { "mData": "dateRegistered" },
                           { 
                            "mData": function(source, type, val) {
                                return '<div align="center">'+ source['image'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['email'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['username'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['displayName'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['fullname'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['position'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['department'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['dateRegistered'] +'</div>';
                            }
                         },
                         { "mData": "actions" }
                     ],
                     "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                         $("#example_length > label > select").addClass("selectlength");
                     },
                     "aoColumnDefs": [ {
                         "bSortable": true, "aTargets": [ 1,2,3,4,5,6,7,8 ]
                         },{
                             "bSortable": false, "aTargets": [ "_all" ]
                         }/*, {
                             "bSearchable": true, "aTargets": [ 0 ]
                         }, {
                             "bSearchable": false, "aTargets": [ "_all" ]
                     }*/
                     ],
                     "fnServerData": function (sSource, aoData, fnCallback) {
                         $.ajax({
                             type: "POST",
                             cache: false,
                             dataType: 'json',
                             url: sSource,
                             data: {action:"companyUserActive"},
                             success: function (json) {
                                 // Retrieve data
                                 //console.log(json)
                                 fnCallback(json);
                                 brokenImage()

                             }
                         });
                     }
                 });
                return oTable;
            
        },
        
        // Load Deactivated User
        loadTableDeactivatedUser : function(){
                var oTable = $('#loadDeactivatedUser').dataTable( {
                     "bProcessing": true,
                     "bRetrieve": true,
                     //"bPaginate": true,//"sEcho":3,//"iTotalDisplayRecords":10,//"iTotalRecords":"10",
                     //"iTotalDisplayRecords":"10",//"bServerSide": true,
                     "sAjaxSource": "/ajax/load",
                     "sPaginationType": "full_numbers",
                     "sAjaxDataProp": "company_user",
                     "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     "aoColumns": [
                         //{ "mData": "" },
                         //{ "mData": "image" },
                         /*{ "mData": "email" },
                         { "mData": "displayName" },
                         { "mData": "fullname" },
                         { "mData": "position" },
                         { "mData": "department" },
                         { "mData": "dateRegistered" },
                         { "mData": "actions" }*/
                          { 
                            "mData": function(source, type, val) {
                                return '<div align="center">'+ source['image'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['email'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['username'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['displayName'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['fullname'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['position'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['department'] +'</div>';
                            }
                         },
                         { 
                            "mData": function(source, type, val) {
                                return '<div class="fl-table-ellip">'+ source['dateRegistered'] +'</div>';
                            }
                         },
                         { "mData": "actions" }
                     ],
                     "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                         $("#example_length > label > select").addClass("selectlength");
                     },
                     "aoColumnDefs": [ {
                         "bSortable": true, "aTargets": [ 1,2,3,4,5,6,7,8 ]
                         },{
                             "bSortable": false, "aTargets": [ "_all" ]
                         }/*, {
                             "bSearchable": true, "aTargets": [ 0 ]
                         }, {
                             "bSearchable": false, "aTargets": [ "_all" ]
                     }*/
                     ],
                     "fnServerData": function (sSource, aoData, fnCallback) {
                         $.ajax({
                             type: "POST",
                             cache: false,
                             dataType: 'json',
                             url: sSource,
                             data: {action:"companyUserDeactivated"},
                             success: function (json) {
                                 // Retrieve data
                                 //console.log(json)
                                 fnCallback(json);
                                 brokenImage()
                             }
                         });
                     }
                 });
                return oTable;
        },
        // Load Deactivated User
        loadAuditLogs : function(){
                var oTable = $('#loadAuditLogs').dataTable( {
                     "bProcessing": true,
                     "bRetrieve": true,
                     //"bPaginate": true,//"sEcho":3,//"iTotalDisplayRecords":10,//"iTotalRecords":"10",
                     //"iTotalDisplayRecords":"10",//"bServerSide": true,
                     "sAjaxSource": "/ajax/load",
                     "sPaginationType": "full_numbers",
                     "sAjaxDataProp": "company_user",
                     "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     "aoColumns": [
                         { "mData": "userCheckbox" },
                         { "mData": "image" },
                         { "mData": "fullname" },
                         { "mData": "action" },
                         //{ "mData": "tablename" },
                         { "mData": "type" },
                         { "mData": "dateRegistered" },
                         { "mData": "ip" },
                         { "mData": "actions" }
                     ],
                     "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                         $("#example_length > label > select").addClass("selectlength");
                     },
                     "aoColumnDefs": [ {
                         "bSortable": true, "aTargets": [ 1,2,3,4,5,6,7 ]
                         },{
                             "bSortable": false, "aTargets": [ "_all" ]
                         }/*, {
                             "bSearchable": true, "aTargets": [ 0 ]
                         }, {
                             "bSearchable": false, "aTargets": [ "_all" ]
                     }*/
                     ],
                     "fnServerData": function (sSource, aoData, fnCallback) {
                        
                         $.ajax({
                             type: "POST",
                             cache: false,
                             dataType: 'json',
                             url: sSource,
                             data: {action:"auditLogs"},
                             success: function (json) {
                                //console.log(json)
                                 // Retrieve data
                                 //console.log(json)
                                 fnCallback(json);
                                 brokenImage()
                             }
                         });
                     }
                 });
                return oTable;
        }
}
/*New ui*/
GetUserDataTable = {
    "defaultData" : function(callback){
        var oColReorder = {
            "iFixedColumns": 0
        };
        var obj = '#loadUser';
        var listOfappContainer = '#tab1C';

        var oTable = $(obj).dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder" : oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/load",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "action", "value": "companyUserActiveDatatable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "search_value", "value": $.trim(jsonUserData['search_value'])});
                aoData.push({"name": "column-sort", "value": jsonUserData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonUserData['column-sort-type']});
                aoData.push({"name": "limit", "value": jsonUserData['limit']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                // aoData.push({"name": "column-sort", "value": ""});
                // aoData.push({"name": "column-sort-type", "value": ""});
    
                $.ajax({
                    // "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        // console.log(data)
                        // return;
                        try{
                            data = JSON.parse(data);
                            fnCallback(data);
                        }catch(err){
                            console.log("wala",data)
                        }

                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
                dataTable_widget(obj, listOfappContainer);
                dataTableSetwidth(this.find('[field_name="position"]'), 130);
                dataTableSetwidth(this.find('[field_name="department"]'), 130);
                dataTableSetwidth(this.find('[field_name="form_group"]'), 130);

            },fnDrawCallback : function(){
                set_dataTable_th_width([1]);
                setDatatableTooltip('.dataTable_widget');
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonUserData['limit']),
        });
        return oTable;
    }
}
GuestUserDataTable = {
    "GuestdefaultData" : function(callback){
        var obj = '#GuestloadUser';
        var listOfappContainer = '#tab0C';
        var oColReorder = {
            "iFixedColumns": 0
        };
        var oTable = $(obj).dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder" : oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/load",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "action", "value": "companyUserGuestDatatable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "search_value", "value": $.trim(jsonGuestUserData['search_value'])});
                aoData.push({"name": "column-sort", "value": jsonGuestUserData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonGuestUserData['column-sort-type']});
                aoData.push({"name": "limit", "value": jsonGuestUserData['limit']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                // aoData.push({"name": "column-sort", "value": ""});
                // aoData.push({"name": "column-sort-type", "value": ""});

                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
                
                dataTable_widget(obj, listOfappContainer);
                
            },
            fnDrawCallback : function(){
                set_dataTable_th_width([1]);
                setDatatableTooltip('.dataTable_widget');
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonGuestUserData['limit']),
        });
        return oTable;
    },
    "GuestDeactivateddefaultData" : function(callback){

        var obj = '#DeactivatedGuestloadUser';
        var listOfappContainer = '#tab01C';
        var oColReorder = {
            "iFixedColumns": 0
        };
        var oTable = $(obj).dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder" : oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/load",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "action", "value": "companyUserDeactivateGuestDatatable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "search_value", "value": $.trim(jsonDeactivateGuestUserData['search_value'])});
                aoData.push({"name": "column-sort", "value": jsonDeactivateGuestUserData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonDeactivateGuestUserData['column-sort-type']});
                aoData.push({"name": "limit", "value": jsonDeactivateGuestUserData['limit']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                // aoData.push({"name": "column-sort", "value": ""});
                // aoData.push({"name": "column-sort-type", "value": ""});

                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        // console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {

                dataTable_widget(obj, listOfappContainer);

                
            },fnDrawCallback : function(){
                set_dataTable_th_width([1]);
                setDatatableTooltip('.dataTable_widget');
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonDeactivateGuestUserData['limit']),
        });
        return oTable;
    }
}
GetDeactivateUserDataTable = {
    "defaultData" : function(callback){
        var obj = '#loadDeactivatedUser';
        var listOfappContainer = '#tab3C';
        var oColReorder = {
            "iFixedColumns": 0
        };
        var oTable = $(obj).dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder" : oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/load",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "limit", "value": jsonDeactivateUserData['limit']});
                aoData.push({"name": "action", "value": "companyUserDeactivatedDatatable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "search_value", "value": $.trim(jsonDeactivateUserData['search_value'])});
                aoData.push({"name": "column-sort", "value": jsonDeactivateUserData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonDeactivateUserData['column-sort-type']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                // aoData.push({"name": "column-sort", "value": ""});
                // aoData.push({"name": "column-sort-type", "value": ""});

                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        // console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {

               
                dataTable_widget(obj, listOfappContainer);

                
            },fnDrawCallback : function(){
                set_dataTable_th_width([1]);
                setDatatableTooltip('.dataTable_widget');
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonDeactivateUserData['limit']),
        });
        return oTable;
    }
}