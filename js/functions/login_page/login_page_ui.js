(function(){

	$(document).ready(function(){
		loginPageConfiguration.loginPage('.fl-app-config-main-tab-wrapper');



	});


	loginPageConfiguration = {

		loginPage: function(container){

			var data_action = 'save_login_page_ui';
	    	var data_set = 'set_login_page_ui';
	    	var company_id =  $("#get_company").val();
	    	var data_type = $('#set_login_page_ui').attr("type");
	    	var json_parse = {};
			
			//elements colors
			var header_background_color = $(container).find('input[data-name="header_background_color"]');
			var header_button_color = $(container).find('input[data-name="header_button_color"]');
			var header_label_color = $(container).find('input[data-name="header_label_color"]');
			var footer_background_color = $(container).find('input[data-name="footer_background_color"]');
			var footer_font_color = $(container).find('input[data-name="footer_font_color"]');
			var header_logo_path = $(container).find('input[data-id="header_logo_path"]');
			var body_wallpaper_path = $(container).find('input[data-id="body_wallpaper_path"]');
			var body_wallpaper_height = $(container).find('input[data-id="body_wallpaper_height"]');
			var header_border_color = $(container).find('input[data-name="header_border_color"]');
			
			// element ui 
			var fl_ui_login_header_wrapper = $(container).find('#fl-ui-login-header-wrapper');
			var fl_ui_login_header_wrapper_border = $(container).find('#fl-ui-login-header-wrapper');
			var fl_btn_submit_login = $(container).find('#fl-btn-submit-login');
			var fl_header_font_color = $(container).find('.fl-header-font-color');
			var fl_ui_login_footer_wrapper = $(container).find('#fl-ui-login-footer-wrapper');
			var fl_copyright = $(container).find('.fl-copyright');

			//value color
			
			var header_background_color_value = $(header_background_color).val();
			var header_button_color_value = $(header_button_color).val();
			var header_label_color_value = $(header_label_color).val();
			var footer_background_color_value = $(footer_background_color).val();
			var footer_font_color_value  = $(footer_font_color).val();
			var header_border_color_value = $(header_border_color).val();

			$('body').data({
	            'header_background_color': $('[data-name="header_background_color"]').val(),
	            'header_button_color': $('[data-name="header_button_color"]').val(),
	            'header_label_color': $('[data-name="header_label_color"]').val(),
	            'footer_background_color': $('[data-name="footer_background_color"]').val(),
	            'footer_font_color': $('[data-name="footer_font_color"]').val(),
	            'header_logo_path': $('[data-id="header_logo_path"]').val(),
	            'body_wallpaper_path': $('[data-id="body_wallpaper_path"]').val(),
	            'body_wallpaper_height': $('[data-name="body_wallpaper_height"]').val(),
	            'header_border_color':$('[data-name="header_border_color"]').val()
	        });

			body_data_header_background_color = $('body').data('header_background_color');
			
			$(header_background_color).spectrum({
				color:header_background_color_value,
		        showInput: true,
		        className: "full-spectrum",
		        showInitial: true,
		        showPalette: true,
		        showSelectionPalette: true,
		        maxPaletteSize: 10,
		        preferredFormat: "rgb",
		        localStorageKey: "spectrum.demo",
		        chooseText: "Choose",
		        cancelText: "Cancel",
		        containerClassName: 'header_background_color_spectrum_wrapper',
		        palette: [
		            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
		                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
		            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
		                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
		            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
		                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
		                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
		                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
		                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
		                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
		                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
		                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
		                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
		                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
		        ],
		        
		        change: function (color) {
		        	//ui.block();

		        	$('.header_background_color_spectrum_wrapper').find('.sp-thumb-active').addClass('pickerCurrentColor')
		        	var active_color = $('.header_background_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        	var active_color_border_header = $('.header_border_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        	var header_background_color_value = $(header_background_color).val();
		        	loginPageConfiguration.element(fl_ui_login_header_wrapper, active_color, "", active_color_border_header);
					$('body').data('latest_header_bg_color', active_color);
					json_parse['action'] = data_action;	
					json_parse['company_id'] = company_id;
					json_parse['header_background_color'] = header_background_color_value;
					json_parse['header_button_color'] = $(header_button_color).val();
					json_parse['header_label_color'] = $(header_label_color).val();
					json_parse['footer_background_color'] = $(footer_background_color).val();
					json_parse['footer_font_color'] = $(footer_font_color).val();
					json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = $(header_border_color).val();
		        	
		        	
		        	//loginPageConfiguration.element(fl_ui_login_header_wrapper, active_color, "", json_parse['header_border_color']);
 		        	
 		        	/*console.log("header_background_color", json_parse);*/
 		        	//Store data temporarily
 		        	

		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(data){
		        			console.log("header_background_color", data)
		        			
		        			$('[data-name="header_background_color"]').attr('value', header_background_color_value);
		        			loginPageConfiguration.element(fl_ui_login_header_wrapper, header_background_color_value, "", json_parse['header_border_color']);
		        			
		        			ui.unblock();
		        		}
		        	});*/
					

		        },

		        hide: function(){
		        	
		        },

		        cancelFN: function(color) {
		        	//ui.block();
		       
		        	/*json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
					json_parse['header_background_color'] = header_background_color_value;
		        	json_parse['header_button_color'] = $(header_button_color).val();
		        	json_parse['header_label_color'] = $(header_label_color).val();
		        	json_parse['footer_background_color'] = $(footer_background_color).val();
		        	json_parse['footer_font_color'] = $(footer_font_color).val();
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = $(header_border_color).val();*/
					
					var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
					var active_color_border_header = $('.header_border_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
					$('body').data('latest_header_bg_color', revert_color);
					
		        	//console.log("FROM CANCEL header_background_color", json_parse);
					if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		
		        		$('[data-name="header_background_color"]').parent().find('.sp-preview-inner').css('background-color', body_data_header_background_color);
		        	}
		        	loginPageConfiguration.element(fl_ui_login_header_wrapper, revert_color, "", active_color_border_header);
		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			console.log("wwwwwwwwwwwwwwwww");
		        			
		        			var this_spectrum_active_data_color = $('.header_background_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        			var header_background_color = $('[data-name="header_background_color"]').attr('value', this_spectrum_active_data_color);
		        			var header_background_color_value = $(header_background_color).val();
		        			loginPageConfiguration.element(fl_ui_login_header_wrapper, header_background_color_value, "", json_parse['header_border_color']);
		        			
		        			ui.unblock();
		        		}
		        	});*/
		        },
		        getPrevColor: function(color){
		        	
		        	if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		ui.block();
		
			        	json_parse['action'] = data_action;
						json_parse['company_id'] = company_id;
						json_parse['header_background_color'] = header_background_color_value;
						json_parse['header_button_color'] = $(header_button_color).val();
						json_parse['header_label_color'] = $(header_label_color).val();
						json_parse['footer_background_color'] = $(footer_background_color).val();
						json_parse['footer_font_color'] = $(footer_font_color).val();
						json_parse['header_logo_path'] = $(header_logo_path).val();
						json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
						json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
						console.log("FROM getPrevColor", json_parse);

						/*$.ajax({
			        		type:"POST",
			        		url: "/ajax/app_configuration",
			        		data:{action:data_action, company_id:company_id, data_json:json_parse},
			        		success:function(){
			        			console.log("wwwwwwwwwwwwwwwww");
			        			var this_spectrum_active_data_color = $('.header_background_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        				var header_background_color = $('[data-name="header_background_color"]').attr('value', this_spectrum_active_data_color);
		        				var header_background_color_value = $(header_background_color).val();
			        			loginPageConfiguration.element(fl_ui_login_header_wrapper, header_background_color_value, "", json_parse['header_border_color']);
			        			ui.unblock();
			        		}
			        	});
*/
		        	}
		        }
			});

			$(header_button_color).spectrum({
				color:header_button_color_value,

		        showInput: true,
		        className: "full-spectrum",
		        showInitial: true,
		        showPalette: true,
		        showSelectionPalette: true,
		        maxPaletteSize: 10,
		        preferredFormat: "rgb",
		        localStorageKey: "spectrum.demo",
		        chooseText: "Choose",
		        cancelText: "Cancel",
		        containerClassName: 'header_button_color_spectrum_wrapper',
		        palette: [
		            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
		                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
		            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
		                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
		            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
		                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
		                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
		                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
		                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
		                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
		                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
		                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
		                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
		                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
		        ],
		        
		        change: function (color) {
		        	//ui.block();
		        	$('.header_button_color_spectrum_wrapper').find('.sp-thumb-active').addClass('pickerCurrentColor')
		        	var header_button_color_value = $(header_button_color).val();
		        	var active_color = $('.header_button_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        	loginPageConfiguration.element(fl_btn_submit_login, active_color, "");
		        	$('body').data('latest_header_button_color', active_color);
					json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
		        	json_parse['header_background_color'] = $(header_background_color).val();
		        	json_parse['header_button_color'] = header_button_color_value;
		        	json_parse['header_label_color'] = $(header_label_color).val();
		        	json_parse['footer_background_color'] = $(footer_background_color).val();
		        	json_parse['footer_font_color'] = $(footer_font_color).val();
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = $(header_border_color).val();

		   //      	console.log(json_parse);

		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			console.log("wwwwwwwwwwwwwwwww");
		        			$('[data-name="header_button_color"]').attr('value', header_button_color_value);
		        			loginPageConfiguration.element(fl_btn_submit_login, header_button_color_value, "");

		        			ui.unblock();
		        		}
		        	});*/
		        },

		        hide: function(){
		        	
		        },

		        cancelFN: function(color) {
		        	//ui.block();
		        
					// json_parse['action'] = data_action;
					// json_parse['company_id'] = company_id;
		   //      	json_parse['header_background_color'] = $(header_background_color).val();
		   //      	json_parse['header_button_color'] = header_button_color_value;
		   //      	json_parse['header_label_color'] = $(header_label_color).val();
		   //      	json_parse['footer_background_color'] = $(footer_background_color).val();
		   //      	json_parse['footer_font_color'] = $(footer_font_color).val();
		   //      	json_parse['header_logo_path'] = $(header_logo_path).val();
					// json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					// json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					// json_parse['header_border_color'] = $(header_border_color).val();
					
					var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
					$('body').data('latest_header_button_color', revert_color);
					loginPageConfiguration.element(fl_btn_submit_login, revert_color, "");
		        	/*console.log("FROM CANCEL", json_parse);

		        	$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			var this_spectrum_active_data_color = $('.header_button_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        			var header_button_color = $('[data-name="header_button_color"]').attr('value', this_spectrum_active_data_color);
		        			var header_button_color_value = $(header_button_color).val();

		        			console.log("wwwwwwwwwwwwwwwww");
		        			loginPageConfiguration.element(fl_btn_submit_login, json_parse['header_button_color'], "");
		        			ui.unblock();
		        		}
		        	});*/
		        
					if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		
		        		body_data_header_button_color = $('body').data('header_button_color');
		        		$('[data-name="header_button_color"]').parent().find('.sp-preview-inner').css('background-color', body_data_header_button_color);
		        	
		        	}
		        },
		        getPrevColor: function(color){
		        	
		        	if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		ui.block();
		        	
			        	json_parse['action'] = data_action;
						json_parse['company_id'] = company_id;
						json_parse['header_background_color'] = $(header_background_color).val();
						json_parse['header_button_color'] = header_button_color_value;
						json_parse['header_label_color'] = $(header_label_color).val();
						json_parse['footer_background_color'] = $(footer_background_color).val();
						json_parse['footer_font_color'] = $(footer_font_color).val();
						json_parse['header_logo_path'] = $(header_logo_path).val();
						json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
						json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
						json_parse['header_border_color'] = $(header_border_color).val();

						console.log("FROM getPrevColor", json_parse);

						$.ajax({
			        		type:"POST",
			        		url: "/ajax/app_configuration",
			        		data:{action:data_action, company_id:company_id, data_json:json_parse},
			        		success:function(){
			        			var this_spectrum_active_data_color = $('.header_button_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        				var header_button_color = $('[data-name="header_button_color"]').attr('value', this_spectrum_active_data_color);
		        				var header_button_color_value = $(header_button_color).val();

			        			console.log("wwwwwwwwwwwwwwwww");
			        			loginPageConfiguration.element(fl_btn_submit_login, json_parse['header_button_color'], "");
			        			ui.unblock();
			        		}
			        	});

		        	}
		        }
			});
			
			$(header_label_color).spectrum({
				color:header_label_color_value,
		        showInput: true,
		        className: "full-spectrum",
		        showInitial: true,
		        showPalette: true,
		        showSelectionPalette: true,
		        maxPaletteSize: 10,
		        preferredFormat: "rgb",
		        localStorageKey: "spectrum.demo",
		        chooseText: "Choose",
		        cancelText: "Cancel",
		        containerClassName: 'header_label_color_spectrum_wrapper',
		        palette: [
		            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
		                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
		            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
		                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
		            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
		                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
		                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
		                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
		                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
		                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
		                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
		                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
		                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
		                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
		        ],
		        
		        change: function (color) {
		        	//ui.block();
		        	$('.header_label_color_spectrum_wrapper').find('.sp-thumb-active').addClass('pickerCurrentColor')
		        	var header_label_color_value = $(header_label_color).val();
		        	var header_button_color_value = $(header_button_color).val();
		        	var active_color = $('.header_label_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        	loginPageConfiguration.element(fl_header_font_color, "", active_color);
		        	$('body').data('latest_header_lbl_color', active_color);

					json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
		        	json_parse['header_background_color'] = $(header_background_color).val();
		        	json_parse['header_button_color'] = $(header_button_color).val();
		        	json_parse['header_label_color'] = header_label_color_value;
		        	json_parse['footer_background_color'] = $(footer_background_color).val();
		        	json_parse['footer_font_color'] = $(footer_font_color).val();
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = $(header_border_color).val();

		        	console.log(json_parse);

		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			console.log("wwwwwwwwwwwwwwwww");
		        			$('[data-name="header_label_color"]').attr('value', header_label_color_value);
		        			loginPageConfiguration.element(fl_header_font_color, "", header_label_color_value);
		        			ui.unblock();
		        		}
		        	});*/
		        },

		        hide: function(){
		        	
		        },

		        cancelFN: function(color) {
		        	//ui.block();
		        	
		        	/*json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
		        	json_parse['header_background_color'] = $(header_background_color).val();
		        	json_parse['header_button_color'] = $(header_button_color).val();
		        	json_parse['header_label_color'] = header_label_color_value;
		        	json_parse['footer_background_color'] = $(footer_background_color).val();
		        	json_parse['footer_font_color'] = $(footer_font_color).val();
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = $(header_border_color).val();*/

					var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
					loginPageConfiguration.element(fl_header_font_color, "", revert_color);
					$('body').data('latest_header_lbl_color', revert_color);
		        	//console.log("FROM CANCEL", json_parse);
		        	//loginPageConfiguration.element(fl_header_font_color, "", );
		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			var this_spectrum_active_data_color = $('.header_label_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        			var header_label_color = $('[data-name="header_label_color"]').attr('value', this_spectrum_active_data_color);
		        			var header_label_color_value = $(header_label_color).val();
		        			console.log("wwwwwwwwwwwwwwwww");
		        			loginPageConfiguration.element(fl_header_font_color, "", json_parse['header_label_color']);
		        			ui.unblock();
		        		}
		        	});*/
		        
					if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		
		        		body_data_header_label_color = $('body').data('header_label_color');
		        		$('[data-name="header_label_color"]').parent().find('.sp-preview-inner').css('background-color', body_data_header_label_color);
		        	
		        	}
		        },
		        getPrevColor: function(color){
		        	
		        	if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		ui.block();
			        	
			        	json_parse['action'] = data_action;
						json_parse['company_id'] = company_id;
						json_parse['header_background_color'] = $(header_background_color).val();
						json_parse['header_button_color'] = $(header_button_color).val();
		        		json_parse['header_label_color'] = header_label_color_value;
		        		json_parse['footer_background_color'] = $(footer_background_color).val();
		        		json_parse['footer_font_color'] = $(footer_font_color).val();
						json_parse['header_logo_path'] = $(header_logo_path).val();
						json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
						json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
						json_parse['header_border_color'] = $(header_border_color).val();

						console.log("FROM getPrevColor", json_parse);

						$.ajax({
			        		type:"POST",
			        		url: "/ajax/app_configuration",
			        		data:{action:data_action, company_id:company_id, data_json:json_parse},
			        		success:function(){
			        			var this_spectrum_active_data_color = $('.header_label_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        				var header_label_color = $('[data-name="header_label_color"]').attr('value', this_spectrum_active_data_color);
		        				var header_label_color_value = $(header_label_color).val();
			        			console.log("wwwwwwwwwwwwwwwww");
			        			loginPageConfiguration.element(fl_header_font_color, "", json_parse['header_label_color']);
			        			ui.unblock();
			        		}
			        	});

		        	}
		        }
			});

			$(footer_background_color).spectrum({
				color:footer_background_color_value,
		        showInput: true,
		        className: "full-spectrum",
		        showInitial: true,
		        showPalette: true,
		        showSelectionPalette: true,
		        maxPaletteSize: 10,
		        preferredFormat: "rgb",
		        localStorageKey: "spectrum.demo",
		        chooseText: "Choose",
		        cancelText: "Cancel",
		        containerClassName: 'footer_background_color_spectrum_wrapper',
		        palette: [
		            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
		                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
		            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
		                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
		            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
		                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
		                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
		                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
		                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
		                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
		                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
		                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
		                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
		                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
		        ],
		        
		        change: function (color) {
		        	//ui.block();
		        	$('.footer_background_color_spectrum_wrapper').find('.sp-thumb-active').addClass('pickerCurrentColor')
		        	var footer_background_color_value = $(footer_background_color).val();
		        	var header_button_color_value = $(header_button_color).val();
		        	var active_color = $('.footer_background_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        	loginPageConfiguration.element(fl_ui_login_footer_wrapper, active_color, "");
		        	$('body').data('latest_footer_bg_color', active_color);

					json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
		        	json_parse['header_background_color'] = $(header_background_color).val();
		        	json_parse['header_button_color'] = $(header_button_color).val();
		        	json_parse['header_label_color'] = $(header_label_color).val();
		        	json_parse['footer_background_color'] = footer_background_color_value;
		        	json_parse['footer_font_color'] = $(footer_font_color).val();
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = $(header_border_color).val();

		        	console.log(json_parse);

		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			console.log("wwwwwwwwwwwwwwwww");
		        			$('[data-name="footer_background_color"]').attr('value', footer_background_color_value);
		        			loginPageConfiguration.element(fl_ui_login_footer_wrapper, footer_background_color_value, "");
		        			ui.unblock();
		        		}
		        	});*/
		        },

		        hide: function(){
		        	
		        },

		        cancelFN: function(color) {
		        	//ui.block();
		        
					/*json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
		        	json_parse['header_background_color'] = $(header_background_color).val();
		        	json_parse['header_button_color'] = $(header_button_color).val();
		        	json_parse['header_label_color'] = $(header_label_color).val();
		        	json_parse['footer_background_color'] = footer_background_color_value;
		        	json_parse['footer_font_color'] = $(footer_font_color).val();
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = $(header_border_color).val();*/

					var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
					loginPageConfiguration.element(fl_ui_login_footer_wrapper, revert_color, "");
					$('body').data('latest_footer_bg_color', revert_color);
		        	//console.log("FROM CANCEL", json_parse);

		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			var this_spectrum_active_data_color = $('.footer_background_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        			var footer_background_color = $('[data-name="footer_background_color"]').attr('value', this_spectrum_active_data_color);
		        			var footer_background_color_value = $(footer_background_color).val();
		        			console.log("wwwwwwwwwwwwwwwww");
		        			loginPageConfiguration.element(fl_ui_login_footer_wrapper, json_parse['footer_background_color'], "");
		        			ui.unblock();
		        		}
		        	});*/
		        	
		        	if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		
		        		body_data_footer_background_color = $('body').data('footer_background_color');
		        		$('[data-name="footer_background_color"]').parent().find('.sp-preview-inner').css('background-color', body_data_footer_background_color);
		        	
		        	}

		        },
		        getPrevColor: function(color){
		        	
		        	if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		ui.block();
		        	
			        	json_parse['action'] = data_action;
						json_parse['company_id'] = company_id;
						json_parse['header_background_color'] = $(header_background_color).val();
						json_parse['header_button_color'] = $(header_button_color).val();
		        		json_parse['header_label_color'] = $(header_label_color).val();
		        		json_parse['footer_background_color'] = footer_background_color_value;
		        		json_parse['footer_font_color'] = $(footer_font_color).val();
						json_parse['header_logo_path'] = $(header_logo_path).val();
						json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
						json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
						json_parse['header_border_color'] = $(header_border_color).val();	
						
						console.log("FROM getPrevColor", json_parse);

						$.ajax({
			        		type:"POST",
			        		url: "/ajax/app_configuration",
			        		data:{action:data_action, company_id:company_id, data_json:json_parse},
			        		success:function(){
			        			var this_spectrum_active_data_color = $('.footer_background_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        				var footer_background_color = $('[data-name="footer_background_color"]').attr('value', this_spectrum_active_data_color);
		        				var footer_background_color_value = $(footer_background_color).val();
			        			console.log("wwwwwwwwwwwwwwwww");
			        			loginPageConfiguration.element(fl_ui_login_footer_wrapper, json_parse['footer_background_color'], "");
			        			ui.unblock();
			        		}
			        	});

		        	}
		        }
			});

			$(footer_font_color).spectrum({
				color:footer_font_color_value,
		        showInput: true,
		        className: "full-spectrum",
		        showInitial: true,
		        showPalette: true,
		        showSelectionPalette: true,
		        maxPaletteSize: 10,
		        preferredFormat: "rgb",
		        localStorageKey: "spectrum.demo",
		        chooseText: "Choose",
		        cancelText: "Cancel",
		        containerClassName: 'footer_font_color_spectrum_wrapper',
		        palette: [
		            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
		                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
		            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
		                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
		            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
		                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
		                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
		                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
		                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
		                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
		                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
		                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
		                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
		                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
		        ],
		        
		        change: function (color) {
		        	//ui.block();
		        	$('.footer_font_color_spectrum_wrapper').find('.sp-thumb-active').addClass('pickerCurrentColor')
		        	var footer_font_color_value = $(footer_font_color).val();
		        	var active_color = $('.footer_font_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        	loginPageConfiguration.element(fl_copyright, "", active_color, "");
		        	$('body').data('latest_footer_font_color', active_color);
					
					json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
		        	json_parse['header_background_color'] = $(header_background_color).val();
		        	json_parse['header_button_color'] = $(header_button_color).val();
		        	json_parse['header_label_color'] = $(header_label_color).val();
		        	json_parse['footer_background_color'] = $(footer_background_color).val();
		        	json_parse['footer_font_color'] = footer_font_color_value;
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = $(header_border_color).val();

		        	/*console.log(json_parse);*/
/*
		        	$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			console.log("wwwwwwwwwwwwwwwww");
		        			$('[data-name="footer_font_color"]').attr('value', footer_font_color_value);
		        			loginPageConfiguration.element(fl_copyright, "", footer_font_color_value, "");
		        			ui.unblock();
		        		}
		        	});*/
		        },

		        hide: function(){
		        	
		        },

		        cancelFN: function(color) {
		        	//ui.block();
		        	
					/*json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
		        	json_parse['header_background_color'] = $(header_background_color).val();
		        	json_parse['header_button_color'] = $(header_button_color).val();
		        	json_parse['header_label_color'] = $(header_label_color).val();
		        	json_parse['footer_background_color'] = $(footer_background_color).val();
		        	json_parse['footer_font_color'] = footer_font_color_value;
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = $(header_border_color).val();*/

					var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
					loginPageConfiguration.element(fl_copyright, "", revert_color, "");
					$('body').data('latest_footer_font_color', revert_color);
		        	// console.log("FROM CANCEL", json_parse);

		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			var this_spectrum_active_data_color = $('.footer_font_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        			var footer_font_color = $('[data-name="footer_font_color"]').attr('value', this_spectrum_active_data_color);
		        			var footer_font_color_value = $(footer_font_color).val();
		        			console.log("wwwwwwwwwwwwwwwww");
		        			loginPageConfiguration.element(fl_copyright, "", json_parse['footer_font_color'], "");
		        			ui.unblock();
		        		}
		        	});*/
		        
					if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		
		        		body_data_footer_font_color = $('body').data('footer_font_color');
		        		$('[data-name="footer_font_color"]').parent().find('.sp-preview-inner').css('background-color', body_data_footer_font_color);
		        	
		        	}

		        },
		        getPrevColor: function(color){
		        	
		        	if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		ui.block();
		     
			        	json_parse['action'] = data_action;
						json_parse['company_id'] = company_id;
						json_parse['header_background_color'] = $(header_background_color).val();
						json_parse['header_button_color'] = $(header_button_color).val();
		        		json_parse['header_label_color'] = $(header_label_color).val();
		        		json_parse['footer_background_color'] = $(footer_background_color).val();
						json_parse['footer_font_color'] = footer_font_color_value;
						json_parse['header_logo_path'] = $(header_logo_path).val();
						json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
						json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
						json_parse['header_border_color'] = $(header_border_color).val();
						console.log("FROM getPrevColor", json_parse);

						$.ajax({
			        		type:"POST",
			        		url: "/ajax/app_configuration",
			        		data:{action:data_action, company_id:company_id, data_json:json_parse},
			        		success:function(){
			        			var this_spectrum_active_data_color = $('.footer_font_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        				var footer_font_color = $('[data-name="footer_font_color"]').attr('value', this_spectrum_active_data_color);
		        				var footer_font_color_value = $(footer_font_color).val();
			        			console.log("wwwwwwwwwwwwwwwww");
			        			loginPageConfiguration.element(fl_copyright,"", json_parse['footer_font_color'], "");
			        			ui.unblock();
			        		}
			        	});

		        	}
		        }
			});
			$(header_border_color).spectrum({
				color:header_border_color_value,
		        showInput: true,
		        className: "full-spectrum",
		        showInitial: true,
		        showPalette: true,
		        showSelectionPalette: true,
		        maxPaletteSize: 10,
		        preferredFormat: "rgb",
		        localStorageKey: "spectrum.demo",
		        chooseText: "Choose",
		        cancelText: "Cancel",
		        containerClassName: 'header_border_color_spectrum_wrapper',
		        palette: [
		            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
		                "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
		            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
		                "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
		            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
		                "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
		                "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
		                "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
		                "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
		                "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
		                "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
		                "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
		                "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
		                "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
		        ],
		        
		        change: function (color) {
		        	//ui.block();
		        	$('.header_border_color_spectrum_wrapper').find('.sp-thumb-active').addClass('pickerCurrentColor')
		        	var header_border_color_value = $(header_border_color).val();
		        	var active_color = $('.header_border_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        	var active_color_header = $('.header_background_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        	loginPageConfiguration.element(fl_ui_login_header_wrapper_border, active_color_header, "", active_color);
		        	$('body').data('latest_header_border_color', active_color);
					
					json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
		        	json_parse['header_background_color'] = $(header_background_color).val();
		        	json_parse['header_button_color'] = $(header_button_color).val();
		        	json_parse['header_label_color'] = $(header_label_color).val();
		        	json_parse['footer_background_color'] = $(footer_background_color).val();
		        	json_parse['footer_font_color'] = $(footer_font_color).val();
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = header_border_color_value;

		   //      	console.log(json_parse);

		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			console.log("wwwwwwwwwwwwwwwww");
		        			$('[data-name="header_border_color"]').attr('value', header_border_color_value);
		        			loginPageConfiguration.element(fl_ui_login_header_wrapper_border, json_parse['header_background_color'], "", header_border_color_value);
		        			ui.unblock();
		        		}
		        	});*/
		        },

		        hide: function(){
		        	
		        },

		        cancelFN: function(color) {
		        	//ui.block();
		        	
					/*json_parse['action'] = data_action;
					json_parse['company_id'] = company_id;
		        	json_parse['header_background_color'] = $(header_background_color).val();
		        	json_parse['header_button_color'] = $(header_button_color).val();
		        	json_parse['header_label_color'] = $(header_label_color).val();
		        	json_parse['footer_background_color'] = $(footer_background_color).val();
		        	json_parse['footer_font_color'] = $(footer_font_color).val();
		        	json_parse['header_logo_path'] = $(header_logo_path).val();
					json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
					json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
					json_parse['header_border_color'] = header_border_color_value;*/

					var revert_color = $('.sp-container:visible').find('.sp-palette-row-initial').children().eq(0).attr("data-color");
					var active_color_header = $('.header_background_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
					loginPageConfiguration.element(fl_ui_login_header_wrapper_border, active_color_header, "", revert_color);
					$('body').data('latest_header_border_color', revert_color);
		        	//console.log("FROM CANCEL", json_parse);

		        	/*$.ajax({
		        		type:"POST",
		        		url: "/ajax/app_configuration",
		        		data:{action:data_action, company_id:company_id, data_json:json_parse},
		        		success:function(){
		        			var this_spectrum_active_data_color = $('.header_border_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        			var header_border_color = $('[data-name="header_border_color"]').attr('value', this_spectrum_active_data_color);
		        			var header_border_color_value = $(header_border_color).val();
		        			console.log("wwwwwwwwwwwwwwwww");
		        			loginPageConfiguration.element(fl_ui_login_header_wrapper_border, json_parse['header_background_color'], "", header_border_color_value);
		        			ui.unblock();
		        		}
		        	});*/
		        	
		        	if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		
		        		body_data_header_border_color = $('body').data('header_border_color');
		        		$('[data-name="header_border_color"]').parent().find('.sp-preview-inner').css('background-color', body_data_header_border_color);
		        	
		        	}
		        },
		        getPrevColor: function(color){
		        	
		        	if ($('.sp-thumb-el').hasClass('pickerCurrentColor') == false) {
		        		ui.block();
		     
			        	json_parse['action'] = data_action;
						json_parse['company_id'] = company_id;
						json_parse['header_background_color'] = $(header_background_color).val();
						json_parse['header_button_color'] = $(header_button_color).val();
		        		json_parse['header_label_color'] = $(header_label_color).val();
		        		json_parse['footer_background_color'] = $(footer_background_color).val();
						json_parse['footer_font_color'] = $(footer_font_color).val();
						json_parse['header_logo_path'] = $(header_logo_path).val();
						json_parse['body_wallpaper_path'] = $(body_wallpaper_path).val();
						json_parse['body_wallpaper_height'] = $(body_wallpaper_height).val();
						json_parse['header_border_color'] = header_border_color_value;
						console.log("FROM getPrevColor", json_parse);

						$.ajax({
			        		type:"POST",
			        		url: "/ajax/app_configuration",
			        		data:{action:data_action, company_id:company_id, data_json:json_parse},
			        		success:function(){
			        			var this_spectrum_active_data_color = $('.header_border_color_spectrum_wrapper').find('.sp-thumb-active').attr('data-color');
		        				var header_border_color = $('[data-name="header_border_color"]').attr('value', this_spectrum_active_data_color);
		        				var header_border_color_value = $(header_border_color).val();
			        			console.log("wwwwwwwwwwwwwwwww");
			        			loginPageConfiguration.element(fl_ui_login_header_wrapper_border, json_parse['header_background_color'], "", header_border_color_value);
			        			ui.unblock();
			        		}
			        	});

		        	}
		        }
			});	

		},

		element: function(ele, style_background_color_value, style_font_color_value, style_border_bottom_value){
			ele.css({
				'background-color':style_background_color_value,
				'color':style_font_color_value,
				'border-bottom':'2px solid ' + style_border_bottom_value
			});
		} 

	}


})();