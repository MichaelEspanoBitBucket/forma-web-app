

  lot_area = {
    load_slot : function(){
      ui.block();
      // Load Details on lot area
      $.ajax({
        type : "GET",
        url  : "/ajax/get-lot-details",
        success : function(e){
          var parse_json = JSON.parse(e);
          $.each(parse_json,function(id,val){
             // console.log(parse_json[id])

             var lat = parse_json[id]['lat'];
             var lng = parse_json[id]['long'];

            var dimension = lot_area_ws("#lot-area-ws");
            console.log(dimension)
            
            $("body").data(parse_json[id]['unique_id'],val);
            append_object(lat,lng,dimension,parse_json[id],val)
          });
          ui.unblock();
        }

      })
    },

    slot_details_modal : function(){
      // View Lot Details
      $("body").on("click",".view-lot-details",function(){
          var get_unique_name = $(this).attr("id");
          var get_info = $("body").data(get_unique_name);
          console.log(get_info)
          var ret = "";
          ret += '<h3 class="fl-margin-bottom fl-add-comment-here"> Lot Information</h3>';
            ret += '<div class="hr"></div>';
                ret += '<div class="Fields fl-margin-bottom pull-left" style=" width: 100%;">';
                    ret += '<div class="label_below2" style="float: left;margin-bottom:0px;">';
                        ret += 'Area:';
                    ret += '</div>';
                    ret += '<div class="input_position" style="float: left;margin-left: 20px;margin-top: 10px;">';
                        ret += get_info['areaName'];
                    ret += '</div>';
                ret += '</div>';
                ret += '<div class="Fields fl-margin-bottom pull-left" style=" width: 100%;">';
                    ret += '<div class="label_below2" style="float: left;margin-bottom:0px;">';
                        ret += 'Price:';
                    ret += '</div>';
                    ret += '<div class="input_position" style="float: left;margin-left: 20px;margin-top: 10px;">';
                        ret += get_info['price'];
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
          ret += '</div>';

          var newDialog = new jDialog(ret, "", "500", "10000", "50", function() {
              });
          newDialog.themeDialog("modal2");
      })



    },

    view_lot_modal_img : function(){

      // View MODAL
      $("body").on("click",".view-lot-modal",function(){
       
          var ret = "";
          ret += '<h3 class="fl-margin-bottom fl-add-comment-here"> Lot Image</h3>';
            ret += '<div class="hr"></div>';
                ret += '<div class="Fields fl-margin-bottom pull-left" style=" width: 100%;">';
                    // ret += '<div class="label_below2" style="float: left;margin-bottom:0px;">';
                    //     ret += 'Area:';
                    // ret += '</div>';
                    ret += '<div class="input_position" style="float: left;margin-left: 20px;margin-top: 10px;">';
                        ret += '<img src="/images/custom-client-images/cfic/map/LotBMP.gif" class="lot-area" id="lot-area-ws-modal" style="position: relative;height: 725px;background-size: 100%;">';
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';
                
            ret += '</div>';
          ret += '</div>';

          var newDialog = new jDialog(ret, "", "1200", "10000", "50", function() {
              });
          newDialog.themeDialog("modal2");
      })

    },

    get_lat_lng : function(){

      // Click
      $("body").on("click",'#lot-area-ws-modal',function(e){
       
        var dimension = lot_area_ws("#lot-area-ws-modal");
        // console.log(this)
        // console.log(e)
            var x = e.offsetX;
            var y = e.offsetY;
            // console.log("X: " + x + " Y: " + y)

            var value1 = convert_x_y_to_long_lat(y,x,dimension['height'],dimension['width']);
            var value2 = convert_long_lat_to_x_y(value1['latitude'],value1['longitude'],dimension['height'],dimension['width']);
            console.log(dimension)
            console.log(value1)
            console.log(value2)

            var flds_latitude = value1['latitude'];
            var flds_longitude = value1['longitude'];

            // $("[name='']").val(flds_latitude); // For Latitude Fields
            // $("[name='']").val(flds_longitude); // For Longitude Fields

            // append_object(value1['latitude'],value1['longitude'],dimension)
      });

    }
  }



$(document).ready(function(){
  //Lat = Y Long = X
  var pathname = window.location.pathname;
  
  if(pathname == "/user_view/lot-view"){
    lot_area.load_slot(); // Load Slot from Database
  }
  
  
  lot_area.slot_details_modal(); // View Other info Per Slot


// Mag Loload lang to sa workspace module

  lot_area.view_lot_modal_img(); // Load Img Lot in Modal
  
  lot_area.get_lat_lng(); // Get Latitude and Longitude

})

function append_object(lat,lng,dimension,data,json){
  
  var conversion_lat_long_to_y_x = convert_long_lat_to_x_y(lat,lng,dimension['height'],dimension['width'])
  // var parse_conversion_lat_long_to_y_x = JSON.parse(conversion_lat_long_to_y_x);
  var x_coordinates = conversion_lat_long_to_y_x['x-coordinates']-10;
  var y_coordinates = conversion_lat_long_to_y_x['y-coordinates']-10;
  
  var ret = "";

  var legend_name = data['legend_name'];
  var legend_color = $("[data-legend-name='"+legend_name+"']").attr("data-legend-color");

  ret += '<div id="'+data['unique_id']+'" class="view-lot-details area-icon-holder" style="position:absolute;top:' + y_coordinates + 'px;left:' + x_coordinates + 'px;">';
    ret += '<svg class="icon-svg" id="" viewBox="0 0 90 90" style="fill:'+legend_color+'">';
        ret += '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-dashboard"></use>';
    ret += '</svg>';
  ret += '</div>';
            
  $("#lot-area-ws").append(ret);
}

function lot_area_ws(lot_name){
  var lot_name = $(lot_name);
  var get_lot_width = lot_name.width();
  var get_lot_height = lot_name.height();

  var json = {};

  json['width'] = get_lot_width;
  json['height'] = get_lot_height;

  return json;
}

function convert_long_lat_to_x_y(lat,lng,get_lot_height,get_lot_width){
  var y = Math.round(((-1 * Number(lat)) + 90) * (Number(get_lot_height) / 180));
  var x = Math.round((Number(lng) + 180) * (Number(get_lot_width) / 360));

  var json = {};

  json['y-coordinates'] = y;
  json['x-coordinates'] = x;

  return json;
}

function convert_x_y_to_long_lat(y,x,get_lot_height,get_lot_width){
  // Convert x,y into lattitude and longitude
  var lat=(Number(y)/(Number(get_lot_height)/180)-90)/-1;
  var lng = Number(x)/(Number(get_lot_width)/360)-180;

  var json = {};

  json['latitude'] = lat;
  json['longitude'] = lng;

  return json;
}

// Create Form For Legends

/*

Field Name

1) legende_name
2) legend_color


Double Check the legends
*/