function get_loadMsg(type, action, id, date, callback) {
    //console.log("ID => " + id);
    //console.log("Date => " + date)
    // Load More and Load Message Only
    if (id != "") {
        var dataString = "action=" + action + "&type=" + type + "&last_msgID=" + date;
    } else {
        var dataString = "action=" + action + "&type=" + type;
    }

    //var ret = "";
    if (id != undefined) {

        $.ajax({
            type: "POST",
            dataType: 'json',
            url: "/ajax/message",
            data: dataString,
            success: function(result) {
                //console.log(result)
                if (result == null) {
                    $(".load_m").hide();
                } else {
                    $(".inboxCount").html(result[0].msg.messageCount);
					$('.fl-drop-item-msg .counter').html(result[0].msg.messageCount);

                    $.each(result, function(id, val) {
                        var getMessage = result[id].msg;
                        //if (date=="") {
                        //$("#msg_" + result[0].msg.msgID).trigger("click");
                        //}
                        if (getMessage != null) {
                            //console.log(getMessage)
                            $(".loadMsgList").append(msgInbox(getMessage));
                        } else {
                            $(".load_m").hide();
                        }
                        limitText(".msgContent", 20); // Limit title text on msg
                        limitText(".recipient_msg_name", 20);

                        $(".timeago").timeago();  // Time To go
                        $(".dataTip").tooltip({
                            html: true,
                            placement: "left"
                        });

                        // Scrollbar
                        $('.scrollbarM').perfectScrollbar({
                            wheelSpeed: 1,
                            wheelPropagation: false
                        });
                        $('.scrollbarM').perfectScrollbar('update');
                        $(".load_m").hide();

                    });

                    // Validation if msg is greater than 7
                    if (result.length >= 10) {
                        //if(result.length >= 7){

                        var data = result[result.length - 1].msg.msgID;
                        var getDate = result[result.length - 1].msg.date;

                        //data.msg[result.length])
                        var last_mID = data;
                        $(loadMore(last_mID, getDate)).insertAfter(".loadMsgList");
                    }
                    if (id != "") {
                        $("#more_" + id).remove();
                    }


                }

                if (typeof callback !== 'undefined') {
                    callback();
                }
            }
        });
    }
}

function msgInbox(getMessage) {

    if (getMessage.messageBG == 1) {
        var unRead = 'gray_bg';
    } else {
        var unRead = "";
    }

    var countSeen = getMessage.countSeen;
    var lengthSeen = "";
    var seenName = "";
    var l = 0; // Count Length Seen not equal to false
    for (var s = 0; s < countSeen.length; s++) {
        if (countSeen[s] != false) {
            lengthSeen = countSeen[s];
            if (getMessage.authName != lengthSeen.first_name + " " + lengthSeen.last_name) {
                seenName += lengthSeen.first_name + " " + lengthSeen.last_name + "<br>";
            }
            l++;
        }

    }

    var name = "";
    if (getMessage.recipientCount != l && getMessage.authID == getMessage.senderID && getMessage.messageBG == 0) {
        name = seenName;
        var seen = '';
    } else if (getMessage.recipientCount != l && getMessage.authID != getMessage.senderID && getMessage.messageBG != 0) {
        name = seenName;
        var seen = '';
    } else if (getMessage.recipientCount == l && getMessage.messageBG == 0 && getMessage.authID == getMessage.senderID) {
        var seen = 'Seen: ';
        name = seenName;
    } else if (getMessage.recipientCount == l && getMessage.messageBG == 0 && getMessage.authID != getMessage.senderID) {
        var seen = 'Seen: ';
        name = seenName;
    } else {
        name = seenName;
        var seen = '';
    }

    var ret = "";
    var rep_name = [];
    var r_name = "";
    var recipient_id = getMessage.recipient.split(",");
    for (var r_auth_id = 0; r_auth_id < recipient_id.length; r_auth_id++) {
        if (recipient_id[r_auth_id] == getMessage.authID) {
            ret += '<li class="viewMsg cursor ' + unRead + '" title="' + getMessage.title + '" id="msg_' + getMessage.msgID + '" data-id="' + getMessage.msgID + '" style="float:left;width:100%;border-bottom:1px solid #ddd;">';
            ret += '<div style="padding: 5px;">';
            ret += '<div class="pull-left" style="margin-top:5px;">';
            ret += '<a href="#" title="" class="pull-left">';
            ret += getMessage.images;
            ret += '</a>';
            ret += '</div>';
            ret += '<div class="pull-left" style="width:75%;margin-top:5px;margin-left: 10px;">';
            var recipient_name = getMessage.recipientName;
            for (var a = 0; a < recipient_name.length; a++) {

                if (recipient_name[a].name != getMessage.authName) {
                    //console.log(recipient_name[a].name)
                    rep_name.push(recipient_name[a].name);
                }
            }
            $.each(rep_name, function(id, val) {
                r_name += rep_name[id] + ",";
            })
            var msg_recipient_name = r_name.slice(0, -1);
            ret += '<a href="#" title=""><strong title="' + msg_recipient_name + '" id="senderName_' + getMessage.msgID + '" class="recipient_msg_name">';

            ret += msg_recipient_name;
            ret += '</strong></a>';
            ret += '<br />';
            ret += '<i data-original-title="' + name + '" class="dataTip" id="msgSeen_' + getMessage.msgID + '" style="font-size: 11px;color: #C5C5C5;"> ' + seen + '  </i>';
            ret += '<span id="msgContent_' + getMessage.msgID + '" class="dataTip msgContent" title="' + msg_nl2br_2(getMessage.message) + '" style="font-size: 11px;color: #C5C5C5;">' + msg_nl2br(getMessage.message) + '</span>';
            ret += '<img src="/images/loader/load.gif" id="loading_' + getMessage.msgID + '" class="display" style="float:right;margin-top:5px;">';
            ret += '<br />';
            ret += '<span class="timeago" id="timeago_' + getMessage.msgID + '" style="font-size: 11px;color: #C5C5C5;" title="' + getMessage.date + '"></span>';
            ret += '</div>';
            ret += '<i class="icon-remove fa fa-times dataTip deleteMessage" data-id="' + getMessage.msgID + '" style="position: absolute;right: 15px; font-size:15px;"></i>';
            ret += '</div>';
            ret += '</li>';
        }
    }

    return ret;
}


function viewMsgDesign(getMessage) {
    var ret = "";
    var postImage = getMessage.message_attachment;
    if (getMessage.authID == getMessage.senderID) {
        ret += '<li class="by_user" data-by="' + getMessage.msgID + '">';
        ret += '<a href="#" title="">' + getMessage.images + '</a>';
        ret += '<div class="messageArea">';
        ret += '<span class="aro"></span>';
        ret += '<div class="infoRow">';
        ret += '<span class="name"><strong>' + getMessage.sendBy + '</strong> says:</span>';
        ret += '<span class="time timeago" title="' + getMessage.date + '"></span>';
        ret += '</div>';
        //ret += '<input type="checkbox" name="" class="" id="" style="float:right;">';
        ret += '<label class="updateMsg">';
        ret += msg_nl2br(getMessage.message);
        ret += '</label>';
        ret += '<br>';
        var valid_formats = ["jpg", "png", "JPG", "PNG", "GIF", "gif"];
        if (postImage.extension_img.length != 0) {
            var w = "";
            var h = "";
            // Image
            if (postImage.extension_img.length != 0) {
                var folderName = postImage.postFolderName;
                var listImg = postImage.img;
                // Image Class
                var imageView = "imageView openImageView";
                //width
                if (postImage.image_resize[0] > 600) {
                    w = "500";
                } else {
                    w = postImage.image_resize[0];
                }
                // Height
                if (postImage.image_resize[1] > 600) {
                    h = "500";
                } else {
                    h = postImage.image_resize[1];
                }
                for (var a in listImg) {
                    var listImg_extension = listImg[a].split(".");
                    var extension = listImg_extension[listImg_extension.length - 1];
                    if (valid_formats.indexOf(extension) != -1) {

                        if (postImage.lengthImg == "1") {
                            ret += '<center>';
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[a] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[a] + '"  width="' + w + '" height="' + h + '">';
                            ret += '</center>';
                        } else if (postImage.lengthImg == "2") {
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[a] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[a] + '"  style="width:40%;">';
                        } else if (postImage.lengthImg == "3") {
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[a] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[a] + '"  style="width:30%;">';
                        } else if (postImage.lengthImg > 4) {
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[a] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[a] + '"  style="width:30%;">';
                        } else if (postImage.lengthImg == "4") {
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[0] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[0] + '"  style="width:30%;">';
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[1] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[1] + '"  style="width:30%;">';
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[2] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[2] + '"  style="width:30%;">';
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[3] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[3] + '"    width="' + w + '" height="' + h + '">';

                            break;
                        }

                    }
                }
            }
        }
        if (postImage.extension_file.length != 0) {
            // Files
            if (postImage.extension_file.length != 0) {
                var folderName = postImage.postFolderName;
                var listImg = postImage.img;

                for (var a in listImg) {
                    var listImg_extension = listImg[a].split(".");
                    var extension = listImg_extension[listImg_extension.length - 1];
                    if (valid_formats.indexOf(extension) == -1) {
                        ret += '<form method="POST" style="text-decoration: underline;"><input type="submit" class="cursor tip" data-original-title="' + listImg[a] + '" style="opacity: 0;position: absolute;width: 100%;"><div class="">';
                        ret += '<a class="cursor apps">';
                        var num = "1";
                        ret += num++ + ") " + listImg[a];
                        ret += '</a>';
                        ret += '</div>';
                        ret += '<input type="hidden" value="' + listImg[a] + '" name="attachment_filename">';
                        ret += '<input type="hidden" value="images/messageUpload/' + folderName + '/' + listImg[a] + '" name="attachment_location">';
                        ret += '</form>';
                    }
                }
            }
        }
        ret += '</div>';
        ret += '</li>';
    } else {
        ret += '<li class="by_me">';
        ret += '<a href="#" title="">' + getMessage.images + '</a>';
        ret += '<div class="messageArea">';
        ret += '<span class="aro"></span>';
        ret += '<div class="infoRow">';
        ret += '<span class="name"><strong>' + getMessage.sendBy + '</strong> says:</span>';
        ret += '<span class="time timeago" title="' + getMessage.date + '"></span>';
        ret += '</div>';
        //ret += '<input type="checkbox" name="" class="" id="" style="float:right;">';
        ret += msg_nl2br(getMessage.message) + "<br>";
        var valid_formats = ["jpg", "png", "JPG", "PNG", "GIF", "gif"];
        if (postImage.extension_img.length != 0) {
            var w = "";
            var h = "";
            // Image
            if (postImage.extension_img.length != 0) {
                var folderName = postImage.postFolderName;
                var listImg = postImage.img;
                // Image Class
                var imageView = "imageView openImageView";
                //width
                if (postImage.image_resize[0] > 600) {
                    w = "500";
                } else {
                    w = postImage.image_resize[0];
                }
                // Height
                if (postImage.image_resize[1] > 600) {
                    h = "500";
                } else {
                    h = postImage.image_resize[1];
                }
                for (var a in listImg) {
                    var listImg_extension = listImg[a].split(".");
                    var extension = listImg_extension[listImg_extension.length - 1];
                    if (valid_formats.indexOf(extension) != -1) {

                        if (postImage.lengthImg == "1") {
                            ret += '<center>';
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[a] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[a] + '"  width="' + w + '" height="' + h + '">';
                            ret += '</center>';
                        } else if (postImage.lengthImg == "2") {
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[a] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[a] + '"  style="width:40%;">';
                        } else if (postImage.lengthImg == "3") {
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[a] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[a] + '"  style="width:30%;">';
                        } else if (postImage.lengthImg > 4) {
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[a] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[a] + '"  style="width:30%;">';
                        } else if (postImage.lengthImg == "4") {
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[0] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[0] + '"  style="width:30%;">';
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[1] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[1] + '"  style="width:30%;">';
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[2] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[2] + '"  style="width:30%;">';
                            ret += '<img src="/images/messageUpload/' + folderName + '/' + listImg[3] + '" class=" ' + imageView + ' group_1_' + getMessage.msgID + '"  href="/images/messageUpload/' + folderName + '/' + listImg[3] + '"    width="' + w + '" height="' + h + '">';

                            break;
                        }

                    }
                }
            }
        }
        if (postImage.extension_file.length != 0) {
            // Files
            if (postImage.extension_file.length != 0) {
                var folderName = postImage.postFolderName;
                var listImg = postImage.img;

                for (var a in listImg) {
                    var listImg_extension = listImg[a].split(".");
                    var extension = listImg_extension[listImg_extension.length - 1];
                    if (valid_formats.indexOf(extension) == -1) {
                        ret += '<form method="POST" style="text-decoration: underline;"><input type="submit" class="cursor tip" data-original-title="' + listImg[a] + '" style="opacity: 0;position: absolute;width: 100%;"><div class="">';
                        ret += '<a class="cursor apps">';
                        var num = "1";
                        ret += num++ + ") " + listImg[a];
                        ret += '</a>';
                        ret += '</div>';
                        ret += '<input type="hidden" value="' + listImg[a] + '" name="attachment_filename">';
                        ret += '<input type="hidden" value="images/messageUpload/' + folderName + '/' + listImg[a] + '" name="attachment_location">';
                        ret += '</form>';
                    }
                }
            }
        }

        ret += '</div>';
        ret += '</li>';
    }


    return ret;
}





function msg_nl2br(varTest) {
    return varTest.replace(/&lt;br&gt;/g, "<br>").replace(/&lt;a&gt;/g, "<a>").replace(/&lt;\/\a&gt;/g, "</a>");
}
function msg_nl2br_2(varTest) {
    return varTest.replace(/&lt;br&gt;/g, "");
}