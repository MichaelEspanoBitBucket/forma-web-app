
var custom_logger = {
    mode: "DEBUG",
    e: function (log) {
        console.log("%c" + log, "color: red; font-size: x-medium");
    },
    w: function (log) {
        console.log("%c" + log, "background: black; color: yellow; font-size: x-medium");
    },
    d: function (log) {
        if (custom_logger.mode === "DEBUG") {
            console.log("%c" + log, "background: black; color: white; font-size: x-medium");
        }

    },
    JSON: function (log) {
        var logString = JSON.stringify(log, undefined, 2);
        custom_logger.d(logString);
    },
    syntaxHighlight: function (json) {
        if (typeof json !== 'string') {
            json = JSON.stringify(json, undefined, 2);
        }
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
    }

};
