
//  public members holder
var MessagingBackend = {};

(function () {

    var MESSAGE_ON_NEW_THREAD = 0;
    var MESSAGE_ON_EXISTING_THREAD = 1;

    var DISPLAY_THREAD_APPEND = 0;
    var DISPLAY_THREAD_REPLACE = 1;

    var PREFFERED_DISPLAYED_THREAD_COUNT = 10;

    var currentThreadFocus = 0;

    //  assign public members
    MessagingBackend.reloadCurrentUserThreadList = reloadCurrentUserThreadList;
    MessagingBackend.markThreadAsRead = markThreadAsRead;

    $(document).ready(function () {

        bindEvents();
        rebindLooseEvents();

        custom_logger.mode = "DEBUG";

    });

    $(document).on('file_uploaded', function (event) {
        var attachmentFileNames = $('.fl-widget-compose-wrapper').attr('attachment-file-names');
        var files = event.uploadedFileData;

        for (var index in files) {
            attachmentFileNames += files[index].name + ";";
        }

        $('.fl-widget-compose-wrapper').attr('attachment-file-names', attachmentFileNames);
    });

    function rebindLooseEvents() {

        $(".fl-widget-user-conversation").unbind('click');
        $(".fl-delete-thread").unbind('click');
        $("#fl-btn-load-more-conversation").unbind('click');

        $(".fl-widget-user-conversation").click(onUserConversationSelected);
        $(".fl-delete-thread").click(onDeleteThreadCommand);
        $("#fl-btn-load-more-conversation").click(loadMoreCurrentUserThreads);

    }

    function bindEvents() {
        $("#fl-btn-send-reply").click(sendReplyMessage);
        $("#fl-btn-send-composed-message").click(sendComposedMessage);
    }

    // <editor-fold desc="Events" defaultState="collapsed">

    function onUserConversationSelected() {
        var unreadCount = $(this).attr("unread-count");
        var threadId = $(this).attr("thread-id");
        loadThreadMessages(threadId, unreadCount);
    }

    function onDeleteThreadCommand(event) {
        event.stopPropagation();

        var threadId = $(this).attr("thread-id");
        var unreadCount = $(this).attr("unread-count");
        var url = "/messaging/delete_thread";
        var params = {
            thread_id: threadId
        };

        $.post(url, params, function (responseData) {
            var unsubscribedMessageCount = responseData["unsubscribed_message_count"];
            var deactivatedMessageCount = responseData["deactivated_messages_count"];

            console.log("Unsubscribed to: " + unsubscribedMessageCount + " messages");
            console.log("Deactivated: " + deactivatedMessageCount + " messages");

            var currentlyViewedThread = getCurrentlyViewedThread();

            if (threadId === currentlyViewedThread) {
                reloadCurrentUserThreadList(0);
            } else {
                reloadCurrentUserThreadList(currentlyViewedThread);
               
            }

            $.event.trigger({
                type: 'thread_opened',
                threadId: threadId,
                unreadCount: unreadCount
            });

        });

    }

    // </editor-fold>

    // <editor-fold desc="Actions" defaultstate="collapsed">

    function loadThreadMessages(threadId, unreadCount) {

        var url = "/messaging/thread_messages";
        var params = {
            threadId: threadId
        };

        currentThreadFocus = threadId;

        $.get(url, params, function (threadMessagesHtml) {
            //  make sure the compose view is hidden and the thread view is shown
            $(".fl-widget-compose-message").css("display", "none");
            $(".fl-widget-message-create-wrapper").css("display", "block");
            $(".fl-widget-compose-reply").css("display", "block");

            //  add the thread messages to view
            $("#fl-thread-messages-widget-wrapper").html(threadMessagesHtml);
            $(".fl-widget-message-create-wrapper").perfectScrollbar();
            $(".fl-widget-message-create-wrapper").scrollTop($(".fl-widget-message-create-wrapper").prop("scrollHeight"));
            $(".fl-widget-message-create-wrapper").perfectScrollbar("update");

            markThreadAsRead(threadId, unreadCount);
                $("img").each(function(){
                    $(this).attr("onerror","this.src='/images/avatar/small.png'");
                });
        });

        var loading_panel = panel.load("#fl-thread-messages-widget-wrapper");
        loading_panel.parent().css("position", "relative");
        loading_panel.css("z-index", "9");

    }

    function sendReplyMessage() {
        var url = "/messaging/write_message";
        var message = $("#fl-textarea-reply").val();
//        var recipients = [14, 15];
        var threadId = getCurrentlyViewedThread();

        var attachments = getAttachmentFileNames();

        if (message && message.trim() !== '' || attachments) {

            var parameters = {
                message: message,
                attachments: attachments,
                thread_id: threadId,
                requested_response: "HTML"
            };

            $.post(url, parameters, function (messageViewHTML) {
//                console.log(messageViewHTML);
                appendMessageHTMLToCorrespondingThread(messageViewHTML, MESSAGE_ON_EXISTING_THREAD);

                $.event.trigger({
                    type: "newReplySent",
                    threadId: threadId,
                    sendTo: getSelectedUserIdList(),
                    messageHTML: messageViewHTML
                });
            });

            $("#fl-textarea-reply").val("");
        }

    }

    function sendComposedMessage() {
        var url = "/messaging/write_message";
        var message = $("#fl-textarea-new-message").val();
//        var recipients = getSelectedUserIdList();

        var attachments = getAttachmentFileNames();
        var recipients = name_selection.getSelectedNames();

        //  validation
        if (recipients === null || recipients === "" || recipients == 0 || recipients.length <= 0) {
            alert("Please select at least one recipient");
            return;
        }

        if (message.trim() === "" && !attachments) {
            alert("Your message cannot be empty");
            return;
        }

        console.log(recipients);

        //  message sending
        var parameters = {
            message: message,
            attachments: getAttachmentFileNames(),
            recipients: recipients
        };

        //  write the new message to server
        $.post(url, parameters, function (messageViewHTML) {
            appendMessageHTMLToCorrespondingThread(messageViewHTML, MESSAGE_ON_NEW_THREAD);

            $.event.trigger({
                type: "newMessageSent",
                recipients: recipients,
                sendTo: getSelectedUserIdList(),
                messageHTML: messageViewHTML
            });
        });

        clearNewMessageView();
    }

    function markThreadAsRead(threadId, unreadCount) {

        var url = "/messaging/change_thread_read_flag";
        var parameters = {
            thread_id: threadId,
            read_flag: 1
        };

        $.post(url, parameters, function (data) {
//            custom_logger.JSON(data);

            $.event.trigger({
                type: 'thread_opened',
                threadId: threadId,
                unreadCount: unreadCount
            });

        });

    }

    function appendMessageHTMLToCorrespondingThread(messageViewHTML, appendType) {

        var currentViewedThread = getCurrentlyViewedThread();
        var messageThreadId = getThreadIdFromMessageView(messageViewHTML);

        //  default append type
        appendType = typeof appendType !== 'undefined' ? appendType : MESSAGE_ON_EXISTING_THREAD;

        //  if the thread id is a valid thread id, reload the messages
        if (messageThreadId && !isNaN(messageThreadId)) {
            if (appendType === MESSAGE_ON_EXISTING_THREAD) {
                //  add the view only if the new message's thread is in focus
                if (messageThreadId === currentViewedThread) {
                    $(".fl-widget-msg-content-wrapper").append(messageViewHTML);
                    $(".fl-widget-message-create-wrapper").scrollTop($(".fl-widget-message-create-wrapper").prop("scrollHeight"));
                    $(".fl-widget-message-create-wrapper").perfectScrollbar("update");
                }
                $("img").each(function(){
                    $(this).attr("onerror","this.src='/images/avatar/small.png'");
                });                
            } else if (appendType === MESSAGE_ON_NEW_THREAD) {
                loadThreadMessages(messageThreadId);
            }

        } else {
            custom_logger.e("Invalid thread id, writing the message failed or there was a change in the template.");
            custom_logger.e("Server responded with:");
            custom_logger.e(messageViewHTML);
        }

        //  reload the thread list
        reloadCurrentUserThreadList(messageThreadId);
    }

    function reloadCurrentUserThreadList(newFocusedThreadId, onReloadDone) {
        var url = "/messaging/current_user_thread_list";
        var parameters = {
            thread_fetch_start_index: 0,
            thread_fetch_count: PREFFERED_DISPLAYED_THREAD_COUNT
        };

        $.post(url, parameters, function (currentUserThreadListHtml) {
            displayThreadList(
                currentUserThreadListHtml, newFocusedThreadId, DISPLAY_THREAD_REPLACE
                );
            if (onReloadDone) {
                onReloadDone();
            }
        });

    }

    function loadMoreCurrentUserThreads() {

        var url = "/messaging/current_user_thread_list";
        var parameters = {
            thread_fetch_start_index: getCurrentlyDisplayingThreadCount(),
            thread_fetch_count: PREFFERED_DISPLAYED_THREAD_COUNT
        };

        //  remove the button as this will be replaced by the response
        $("#fl-btn-load-more-conversation").remove();

        $.post(url, parameters, function (currentUserThreadListHtml) {
            displayThreadList(
                currentUserThreadListHtml, 0, DISPLAY_THREAD_APPEND
                );
        });
    }

    function displayThreadList(currentUserThreadListHtml, newFocusedThreadId, append) {


        if (append === DISPLAY_THREAD_REPLACE) {
            $("#fl-load-user-conversation").html(currentUserThreadListHtml);

        } else if (append === DISPLAY_THREAD_APPEND) {
            $("#fl-load-user-conversation").append(currentUserThreadListHtml);
            
        }

        $(".fl-widget-user-conversations-wrapper").perfectScrollbar();
        $(".fl-widget-user-conversations-wrapper").perfectScrollbar("update");

        console.log("Focus should be on thread: " + newFocusedThreadId);
        rebindLooseEvents();

        if (newFocusedThreadId != currentThreadFocus) {
            //  Reload the messages view
            if (newFocusedThreadId) {
                loadThreadMessages(newFocusedThreadId);
            } else {
                loadThreadMessages(0);
            }
        }

        $('.fl-user-conversation-time').timeago();
       

    }

    function clearNewMessageView() {
        $(".recipient .fl-message-recipients").remove();
    }

    // </editor-fold>

    // <editor-fold desc="Getters & Setters" defaultstate="collapsed">

    function getCurrentlyViewedThread() {
        return $(".fl-widget-msg-content-wrapper").attr("thread-id");
    }

    function setThreadTitle(threadTitle) {
        $("#fl-span-thread-title").html(threadTitle);
    }

    function getSelectedUserIdList() {
        var recipientsString = $("#fl-message-recipients").attr("data-tag-id");
        if (recipientsString && recipientsString.trim() !== '') {
            return recipientsString.trim().split(",");
        }

        return null;
    }

    function getThreadIdFromMessageView(messageViewHTML) {
        return $(messageViewHTML).attr("thread-id");
    }

    function getCurrentlyDisplayingThreadCount() {
        return $(".fl-widget-user-conversation").length;
    }

    function getAttachmentFileNames() {

        var rawAttachmentFileNames = $('#fl-message-upload').attr('uploaded-filenames');

        if (rawAttachmentFileNames && rawAttachmentFileNames.trim() !== "") {
            return rawAttachmentFileNames.split(",");
        }

        return null;
    }

    // </editor-fold>
})();