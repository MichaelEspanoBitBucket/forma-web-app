
/* global name_selection */

(function () {

    var uploadWidgetHeightAdjusted;

    $(document).ready(function () {
        
        $("#fl-btn-compose-new-message").click(onComposeNewMessageCommand);
         $('label.fl-user-conversation-time').timeago();
         
        $(document).on("thread_opened", onThreadOpened);
        $(document).on("newMessageSent", onMessageSent);
        $(document).on("newReplySent", onMessageSent);

        uploadTriggerHeight = $('#fl-msg-left-nav').children().eq(0);

        setThreadTitle("");

        var searchParams = getSearchParameters();

        if (searchParams && searchParams.recp && searchParams.recpname) {
            onComposeNewMessageCommand();
            name_selection.setSelectedNames([
                {
                    follower_type: 1,
                    follower: searchParams.recp,
                    follower_display_name: decodeURIComponent(searchParams.recpname)
                }
            ]);
        }

        if (searchParams && searchParams.threadId) {
            $('.fl-widget-user-conversation[thread-id=' + searchParams.threadId + ']').click();
        }

        bindEventsForDynamicElements();

    });

    function bindEventsForDynamicElements() {

        $('body').on('click', '.fl-file-preview-image', onClickMessagePreview);

    }

    function getSearchParameters() {
        var prmstr = window.location.search.substr(1);
        return prmstr != null && prmstr != "" ? transformToAssocArray(prmstr) : {};
    }

    function transformToAssocArray(prmstr) {
        var params = {};
        var prmarr = prmstr.split("&");
        for (var i = 0; i < prmarr.length; i++) {
            var tmparr = prmarr[i].split("=");
            params[tmparr[0]] = tmparr[1];
        }
        return params;
    }

    function onComposeNewMessageCommand() {
        $(".fl-widget-compose-wrapper").css("display", "block");
        $(".fl-widget-message-create-wrapper").css("display", "none");
        $(".fl-widget-compose-reply").css("display", "none");
        $(".fl-widget-compose-message").css("display", "block");

        setThreadTitle("Compose New");
    }

    function setThreadTitle(threadTitle) {
        $("#fl-span-thread-title").html(threadTitle);
        $("#fl-span-thread-title").tooltip();
    }

    function onThreadOpened(event) {

        clearMessageEditor();

        uploadWidgetHeightAdjusted = false;

        var threadId = event.threadId;
        var conversationView = (".fl-user-conversation-unread-count-wrapper[thread-id=" + threadId + "]");
        var conversationCountView = $(conversationView).find(".fl-user-conversation-unread-count");
        var conversationUnreadCount = $(conversationCountView).attr("unread-count");

//        subtractUnreadMessagesCounter(parseInt(conversationUnreadCount) || 0);
        if (conversationCountView) {
            $(conversationCountView).remove();
        }

        var attachmentNames = $(".fl-widget-compose-wrapper").attr("attachment-file-names");

        if (attachmentNames !== "") {
            adjustUploadWidgetHeight();
        }

        $(".fl-widget-compose-wrapper").css("display", "block");

        //  load the thread title and other data to the other views
        var otherSubscribers = $("#fl-thread-data").attr("thread-other-subscriber-names").split(",");
        var subscriberCount = otherSubscribers.length;
        var threadTitle = "";

        for (var i = 0; (i < subscriberCount && i < 2); i++) {
            threadTitle += otherSubscribers[i] + ",";
        }

        // remove trailing ,
        threadTitle = threadTitle.substring(0, threadTitle.length - 1);

        if (subscriberCount > 2) {
            var hiddenSubscribers = otherSubscribers.splice(2, subscriberCount - 1);
            threadTitle += ' and <span class="fl-other-subscribers" data-placement="bottom" data-original-title="'
                + hiddenSubscribers.join(", \n") + '">'
                + (subscriberCount - 2)
                + ' others</span>';
        }

        setThreadTitle(threadTitle);

        $(".fl-other-subscribers").tooltip();
    }

    function adjustUploadWidgetHeight() {

        var uploadTriggerHeight = $('#fl-msg-left-nav').children().eq(0);

        if (!uploadWidgetHeightAdjusted) {
            $.event.trigger({
                type: "change_msg_wrapper_height",
                triggerHeight: uploadTriggerHeight
            });
        }

        uploadWidgetHeightAdjusted = true;
    }

    $(document).on('change_msg_wrapper_height', function () {

        var uploadGetHeightCreateWrapper = $('.fl-widget-message-create-wrapper').height();
        var uploadgetHeightImageWrapper = $('.fl-image-loading').height() + 10;
        var uploadGetHeightDocFile = $('.fl-file-resource-wrapper').height();
        var uploadTotalHeightCreateWrappper = uploadGetHeightCreateWrapper - uploadgetHeightImageWrapper;
        var uploadTotalHeightWithDocFile = uploadGetHeightCreateWrapper - uploadGetHeightDocFile - 10;
        var uploadedImageCount = $('.fl-file-images-wrapper').children('.fl-file-image').length;
        var uploadedFileCount = $('.fl-file-resource-wrapper').children('.fl-file').length;

        if (uploadedImageCount >= 1) {
            //alert(1('image file exists');
            $('.fl-widget-message-create-wrapper').css({
                'height': uploadTotalHeightCreateWrappper
            });

        }

        if (uploadedFileCount >= 1) {
            // alert('document file exists');
            $('.fl-widget-message-create-wrapper').css({
                'height': uploadTotalHeightWithDocFile
            });
        }

        if (uploadedFileCount >= 1 && uploadedImageCount >= 1) {
            $('.fl-widget-message-create-wrapper').css({
                'height': 306
            });
        }

        //console.log($('.fl-widget-message-create-wrapper').height());
        //alert(uploadTotalHeightCreateWrappper);

        $(document).unbind(this);
    });

    function reAdjustUploadWidgetHeight() {

        if (uploadWidgetHeightAdjusted) {

            $('.fl-widget-message-create-wrapper').css({
                'height': 422
            });

        }
        uploadWidgetHeightAdjusted = false;

    }

    function reAdjustUploadWidgetHeightWithImageFileAndDocFile() {

        if ($('#fl-message-upload').height() == 117) {
            $('.fl-widget-message-create-wrapper').css({
                'height': 306
            });
        }

    }

    function clearMessage() {
        $("#fl-textarea-new-message").val("");
        //  remove all recipient badges
        $(".recipient").each(function () {
            $(this).remove();
        });
        $("#fl-message-recipients").attr("data-tag-id", "0");
    }

//    function subtractUnreadMessagesCounter(subtractBy) {
//        var currentMessageCount = $(".fl-message-counter").html();
//        currentMessageCount = parseInt(currentMessageCount.trim());
//
//        var newMessageCount = currentMessageCount - subtractBy;
//
//        if (newMessageCount <= 0) {
//            $(".fl-message-counter").html(0);
//            $(".fl-message-counter").css("display", "none");
//        } else {
//            $(".fl-message-counter").html(currentMessageCount - subtractBy);
//            $(".fl-message-counter").css("display", "block");
//        }
//
//    }

    function onMessageSent() {
        clearMessageEditor();
    }

    function onClickMessagePreview() {
        var thisID = $(this).attr('src');
        //alert(thisID);
        $(this).colorbox({rel: thisID, width: "75%", height: "80%"});
    }

    function clearMessageEditor() {
        file_uploader.clearAttachments("fl-message-upload");
        clearMessage();
        reAdjustUploadWidgetHeight();
    }

    // Register file uploader callbacks
    file_uploader.onAttachmentAddedCallback = function () {
        //  TODO: roni's updates to UI handling of uploading files
        //changeContentWrapperHeight();       
        adjustUploadWidgetHeight();
        reAdjustUploadWidgetHeightWithImageFileAndDocFile();
    };
    file_uploader.onAllAttachmentsRemovedCallback = function () {
        reAdjustUploadWidgetHeight();
    };

})();