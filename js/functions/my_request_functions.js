MyRequest  = {
	load : function(start,status,obj,callback){
		continueToLoad_myrequest = false;
		var search_value = $(".search-my-request-value").val();
		var self = this;
		if($(".my-request-wrapper").find(".load-app-myrequest").length==0){
			$(".my-request-wrapper").append('<center><img src="/images/loader/loader.gif" class="load-app-myrequest" style="display: inline;"></center>');
		}
		$.post("/ajax/my_request",{action:"loadData",start:start,search_value:search_value},function(data){
			console.log("my request",data);
   //          return;
			var data = JSON.parse(data);
			var result = "";
			if(obj['type']=="0"){
				result = self.view(data);
			}else if(obj['type']=="1"){
				result = self.view3(data);
			}
			if(status=="start"){
                //restore variables for load more
               	// $(".sub-container-mr").html(result);
               	// $(".my-request-wrapper").html(result);
                ctr_load_myrequest = 10;
                continueToLoad_myrequest = true;
                
                $("#my-request-container").perfectScrollbar();
                $("#my-request-container").perfectScrollbar("update");
            }else if(status == "load"){
            	// $(".my-request-wrapper").append(result);

                // $(".sub-container-mr").append(result);
                // $("#my-request-container").perfectScrollbar("update");
            }
			$(".load-app-myrequest").remove();
            

            callback(result);
		})
		// .fail(function(){
  //           alert("CONNECTION FAILED!!");
  //           continueToLoad_myrequest = false;
  //       })
		
	},
	view : function(data){
		var ret = "";
		if(data.length==0){
			continueToLoad_myrequest = false
		}else{
			//$.each(data,function(key, request){
			var padding = "";
			for (var i in data) {
				padding = "padding-top: 0px;";
				if(i==0){
					padding = "padding-top: 5px;";
				}
				ret += '<div class="supTicket nobg" style="'+ padding +'">';
				
	            ret +=        '<div class="issueType" >';
	            ret +=           '<span class="issueInfo" style="width: 52%;">'+ data[i]['form_name'] +'</span>';
	            ret +=           '<span class="issueNum" style="float:left">'+ data[i]['TrackNo'] +'</span>';

	            ret +=        '</div>';
	            ret +=        '<div class="issueSummary">';
	            ret +=           '<a href="#" title="" class="pull-left" data-original-title="">';
	            // ret +=              '<img src="/images/avatar/small.png" class="avatar" width="30" height="30" alt="">';
	            ret +=              data[i]['user_image'];
	            ret +=            '</a>';
	            ret +=           '<div class="ticketInfo">';
	            ret +=              '<ul>';
	            ret +=                  '<li><a href="/workspace?view_type=request&formID='+ data[i]['form_id'] +'&requestID='+ data[i]['request_id'] +'" title="" data-original-title="">'+ data[i]['first_name'] +' '+ data[i]['last_name'] +'</a></li>';
	            ret +=                  '<li>Status: <strong class="green">'+ data[i]['form_status'] +'</strong></li>';
	            ret +=                  '<li class="even" style="font-size: 10px;text-align: left;color: #888888;text-align:left">'+ data[i]['DateCreated'] +'</li>';
	            ret +=               '</ul>';
	            ret +=            '</div>';
	            ret +=         '</div>';
	            ret +=     '</div>';
	            ret +=    '<div class="hr" style=""></div>';
			};
				
			//})
		}
		
		return ret;
	},
	view2 : function(data){
		var ret = "";
		if(data.length==0){
			continueToLoad_myrequest = false
		}else{
			//$.each(data,function(key, request){
			var padding = "";
			for (var i in data) {
				// padding = "padding-top: 0px;";
				// if(i==0){
				// 	padding = "padding-top: 5px;";
				// }
				// ret += '<div class="supTicket nobg" style="'+ padding +'">';
				
	   //          ret +=        '<div class="issueType" >';
	   //          ret +=           '<span class="issueInfo" style="width: 52%;">'+ data[i]['form_name'] +'</span>';
	   //          ret +=           '<span class="issueNum" style="float:left">'+ data[i]['TrackNo'] +'</span>';

	   //          ret +=        '</div>';
	   //          ret +=        '<div class="issueSummary">';
	   //          ret +=           '<a href="#" title="" class="pull-left" data-original-title="">';
	   //          // ret +=              '<img src="/images/avatar/small.png" class="avatar" width="30" height="30" alt="">';
	   //          ret +=              data[i]['user_image'];
	   //          ret +=            '</a>';
	   //          ret +=           '<div class="ticketInfo">';
	   //          ret +=              '<ul>';
	   //          ret +=                  '<li><a href="/workspace?view_type=request&formID='+ data[i]['form_id'] +'&requestID='+ data[i]['request_id'] +'" title="" data-original-title="">'+ data[i]['first_name'] +' '+ data[i]['last_name'] +'</a></li>';
	   //          ret +=                  '<li>Status: <strong class="green">'+ data[i]['form_status'] +'</strong></li>';
	   //          ret +=                  '<li class="even" style="font-size: 10px;text-align: left;color: #888888;text-align:left">'+ data[i]['DateCreated'] +'</li>';
	   //          ret +=               '</ul>';
	   //          ret +=            '</div>';
	   //          ret +=         '</div>';
	   //          ret +=     '</div>';
	   //          ret +=    '<div class="hr" style=""></div>';
	   // console.log(data[i])
	   				ret +=    '<div class="fl-widget-content">';
	                ret +=    '  <div class="fl-avatar">'+ data[i]['user_image'] +'</div>';
	                ret +=    '  <div class="fl-widget-my-requests-content-wrapper">';
	                ret +=    '   <span>Form :</span> <span class="request-data theme-font-color">'+ data[i]['form_name'] +'</span><br/>';
	                ret +=    '    <span>Request ID:</span> <span class="request-data theme-font-color"><a href="/user_view/workspace?view_type=request&formID='+ data[i]['form_id'] +'&requestID='+ data[i]['request_id'] +'" title="" data-original-title="">'+ data[i]['TrackNo'] +'</a></span><br/>';
	                ret +=    '    <span>Status:</span> <span class="request-data request-status theme-font-color">'+ data[i]['Status'] +'</span> </div>';
	                ret +=    ' <span class="request-time">'+ data[i]['DateCreated'] +'</span>   ';
	                ret +=    '  <div class="clearfix"></div>';
	                // ret +=    '<div align="center"><img src="images/icon/loading.gif"></div>
	               	ret +=    '</div>';
			};
				
			//})
		}
		
		return ret;
	},
	view3 : function(data){
		var ret = "";
		if(data.length==0){
			continueToLoad_myrequest = false
		}else{
			
			var padding = "";
			for (var i in data) {
				
	   

	               	ret +=    '<div class="fl-widget-content fl-widget-contentv2">';
                        ret +=    '<div class="fl-widget-my-requests-content-wrapperv2 theme-font-color">';
                            ret +=    '<div class="fl-form-request-header">';
                                ret +=    '<strong>Form Category:</strong>';
                                ret +=    ' <strong class="request-data theme-font-color">' + data[i]['category_name'] + '</strong>';
                            ret +=    '</div>';
                            ret +=    '<div class="fl-form-request-body">';
                                ret +=    '<span>Form:</span>'
                                ret += 	  ' <span class="request-data theme-font-color">'+ data[i]['form_name'] +'</span><br/>';
                                ret += 	  '<span>Request ID:</span>';
                                ret += 	  ' <span class="request-data theme-font-color fl-request-id"><a href="/user_view/workspace?view_type=request&formID='+ data[i]['form_id'] +'&requestID='+ data[i]['request_id'] +'" title="" data-original-title="">'+ data[i]['TrackNo'] +'</a></span><br/>';
                                ret += 	  '<span>Status:</span>';
                                ret += 	  ' <span class="request-data request-status theme-font-color">'+ data[i]['Status'] +'</span><Br/>';
                                 ret += 	  '<span>Date Created:</span>';
                                ret += 	  ' <span class="request-data theme-font-color">'+ data[i]['DateCreated'] +'</span>';
                            ret +=    '</div>';
                        ret +=    '</div>';  
                    ret +=    '</div>';
			};
	
		}
		
		return ret;
	},
	count : function(){
		/*$.post("/ajax/my_request",{action:"countData"},function(data){
			// $("#my_request_count").text(data);
			$(".my_request_count").text(data);
		})*/
		// .fail(function(){
  //           alert("CONNECTION FAILED!!");
  //           $("#my_request_count").text("0");
  //       })
		
	}
}