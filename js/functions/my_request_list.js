var jsonMyRequestData = {
    search_value: "",
    "column-sort": "",
    "column-sort-type": "",  
    limit: 10,
}
var columnSort = [];
$(document).ready(function(){
    //My Request
    $("body").on("click","#btnSearchMyRequestDatatable",function(){
        $("#loadMyRequestTable").dataTable().fnDestroy();
        jsonMyRequestData['search_value'] = $("#txtSearchMyRequestDatatable").val();
        MyRequestDataTable.defaultData();
    })

    $("body").on("keyup","#txtSearchMyRequestDatatable",function(e){
        if (e.keyCode == "13") {
            $("#loadMyRequestTable").dataTable().fnDestroy();
            jsonMyRequestData['search_value'] = $(this).val();
            MyRequestDataTable.defaultData();
        }
    })
	MyRequestDataTable.defaultData();
    $("body").on("click","#loadMyRequestTable th",function(){
        var cursor = $(this).css("cursor");
        jsonMyRequestData['column-sort'] = $(this).attr("field_name");
        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined){
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest("#loadMyRequestTable").find(".sortable-image").html("");
        if(indexcolumnSort==-1){
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonMyRequestData['column-sort-type'] = "ASC";
        }else{
            columnSort.splice(indexcolumnSort,1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonMyRequestData['column-sort-type'] = "DESC";
        }
        
        $("#loadMyRequestTable").dataTable().fnDestroy();
        MyRequestDataTable.defaultData();
    })
    $("body").on("click","#loadMyRequestTable tbody tr td",function(e){
       var href = $(this).closest("tr").find(".linkTrigger").attr("href");
       if(href=="" || href==null || href=="undefined" || href==undefined){
        return;
       }
       window.location = href;
       // alert(123)
    })
    $(".searchMyRequestLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonMyRequestData['limit'] = val;
            $("#loadMyRequestTable").dataTable().fnDestroy();
            // var self = this;
            var oTable = MyRequestDataTable.defaultData();
        // }
    })

    /*
    * My request calendar view
    */
    $('#swicthingreq').on({

        'click':function(){
            var self = $(this);

            if (self.attr('view') == 'table-view') {

                self.removeClass('fa-toggle-off').addClass('fa-toggle-on');
                self.attr('view', 'calendar-view');

                 self.tooltip('hide')
                    .attr('data-original-title', 'Switch to Table View')
                    .tooltip('fixTitle')
                    .tooltip('show');


                $('.dataTable, .dataTables_info, .dataTables_paginate, .fl-table-search-wrapper, .fl-datatable-wrapper, .dataTable_widget, .fl-search-wrapper, .spinner').hide();
                $('#fl-reqcalendar').stop(true, true).fadeIn();
                $('#fl-reqcalendar').fullCalendar('render');
            
            }else if (self.attr('view') == 'calendar-view') {
                
                self.removeClass('fa-toggle-on').addClass('fa-toggle-off');
                self.attr('view', 'table-view');

                self.tooltip('hide')
                    .attr('data-original-title', 'Switch to Calendar View')
                    .tooltip('fixTitle')
                    .tooltip('show');

                $('#fl-reqcalendar').hide();
                $('.dataTable, .dataTables_info, .dataTables_paginate, .fl-table-search-wrapper, .fl-datatable-wrapper, .dataTable_widget, .fl-search-wrapper, .spinner').stop(true, true).fadeIn();
            };

        }

    });

    $('#fl-reqcalendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay,prevYear,nextYear'
        },
        eventLimit: true,
        nextDayThreshold: '24:00:00',
        events:  function(start, end, timezone, callback){
            var start_date = start.format();
            var end_date = end.format();

            console.log(start_date)
            console.log(end_date)

            $.ajax({
                url: '/ajax/my_request',
                type: 'POST',
                dataType: 'json',
                data: {
                    "action":"calendar_view",
                    "start_date":start_date,
                    "end_date":end_date
                },
                success: function(doc) {
                    // console.log(doc)
                    ui.unblock();
                    callback(doc);
                }
            });
       },
       eventClick: function(calEvent, jsEvent, view) {

            // alert('Event: ' + calEvent.link);
            var win = window.open(calEvent.link,'_blank');
            win.focus();
            // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            // alert('View: ' + view.name);

        },
        loading: function(bool) {
            ui.block();
        }
    });
})
MyRequestDataTable = {
    "defaultData" : function(){
        // var oTable = $('#loadMyRequestTable').dataTable( {
        //     "oLanguage": {
        //         "sProcessing": '<div class="spinner load_m"  style="margin-top:50px;"> '+
        //                '<div class="bar1"></div> '+
        //                '<div class="bar2"></div> '+
        //                '<div class="bar3"></div> '+
        //                '<div class="bar4"></div> '+
        //                '<div class="bar5"></div> '+
        //                '<div class="bar6"></div> '+
        //               '<div class="bar7"></div> '+
        //               '<div class="bar8"></div> '+
        //               '<div class="bar9"></div> '+
        //               '<div class="bar10"></div> '+
        //          '</div>'
        //     },
        //             "bLengthChange": false,
        //             "bFilter": false,
        //              "bProcessing": true,
        //              "bRetrieve": true,
        //              "aaSorting" : [[2,"desc"]],
        //              //"bPaginate": true,//"sEcho":3,//"iTotalDisplayRecords":10,//"iTotalRecords":"10",
        //              //"iTotalDisplayRecords":"10",//"bServerSide": true,
        //              "sAjaxSource": "/ajax/my_request",
        //              "sPaginationType": "full_numbers",
        //              "sAjaxDataProp": "",
        //              "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        //              "aoColumns": [
        //                  { "mData": "form_name" },
        //                  { "mData": function(source, type, val){
        //                         return '<a href="/user_view/workspace?view_type=request&formID='+ source['form_id'] +'&requestID='+ source['request_id'] +'" title="" data-original-title="">'+ source['TrackNo'] +'</a>';
        //                      } 
        //                  },
        //                  { "mData": "form_status" },
        //                  { "mData": "DateCreated" }
        //              ],
        //              "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
        //                  $("#example_length > label > select").addClass("selectlength");
        //              },
        //              "aoColumnDefs": [ {
        //                  "bSortable": true, "aTargets": [ 0,1,2,3 ]
        //                  },{
        //                      "bSortable": false, "aTargets": [ "_all" ]
        //                  }/*, {
        //                      "bSearchable": true, "aTargets": [ 0 ]
        //                  }, {
        //                      "bSearchable": false, "aTargets": [ "_all" ]
        //              }*/
        //              ],
        //              "fnServerData": function (sSource, aoData, fnCallback) {
        //                  $.ajax({
        //                      type: "POST",
        //                      cache: false,
        //                      dataType: 'json',
        //                      url: sSource,
        //                      data: {action:"loadData"},
        //                      success: function (json) {
        //                          // Retrieve data
        //                          // console.log(json)
        //                          // return;
        //                          fnCallback(json);
        //                      }
        //                  });
        //              }
        //          });
        //         return oTable;
        var oTable = $('#loadMyRequestTable').dataTable( {
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> '+
                       '<div class="bar1"></div> '+
                       '<div class="bar2"></div> '+
                       '<div class="bar3"></div> '+
                       '<div class="bar4"></div> '+
                       '<div class="bar5"></div> '+
                       '<div class="bar6"></div> '+
                      '<div class="bar7"></div> '+
                      '<div class="bar8"></div> '+
                      '<div class="bar9"></div> '+
                      '<div class="bar10"></div> '+
                 '</div>'
            },
            "oColReorder": false,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/my_request",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "search_value", "value": $.trim(jsonMyRequestData['search_value'])});
                aoData.push({"name": "action", "value": "loadDataDatatable"});
                aoData.push({"name": "limit", "value": jsonMyRequestData['limit']});
                aoData.push({"name": "column-sort", "value": jsonMyRequestData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonMyRequestData['column-sort-type']});
                
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        console.log("MY Request ",data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
                //
                var obj = '#loadMyRequestTable';
                var listOfappContainer = '.fl-list-of-app-record-wrapper';
                dataTable_widget(obj, listOfappContainer); 
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonMyRequestData['limit']),
        });
        return oTable;
    }
}