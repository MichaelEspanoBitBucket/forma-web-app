
// namespace name_selection
var name_selection = {};
var ajaxholder = null;
var CONTEXT = "/name_selection";
var UserType = {
    USER: 1,
    GROUP: 2,
    DEPARTMENT: 3,
    POSITION: 4,
    PERSONAL_GROUP: 5
};
function getSelectedNames() {

    var selectedUsers = [];

    $(".fl-selected-name").each(function () {
        selectedUsers.push({
            type: $(this).attr("data-type-id"),
            display_text: $(this).find("span").html(),
            value: $(this).attr("data-id")
        });
    });

    return selectedUsers;
}

function setSelectedNames(names) {
    clearSelectedNames();
    addSelectedNames(names);
}

function addSelectedNames(names) {
    for (var i in names) {
        onSelectUserItem(
            names[i]["follower_type"], names[i]["follower"], names[i]["follower_display_name"]
            );
    }
}

function clearSelectedNames() {
    $(".fl-selected-names-container").html("");
}

function bindUserSelectionFilterOnChangeEvent(onChangeCallback, e_para) {
    var default_selector = $(".fl-widget-name-selector > .fl-autocomplete-input");
    if(e_para){
        default_selector = $(e_para).find('.fl-autocomplete-input');
    }
    default_selector.bind("input propertychange", function (event) {
        var name = $(this).val();
        var filterSearchType = $(this).parent('.fl-widget-name-selector').data('filter-search-type');
        name = name.trim();
        displayUsers("");

        // If it's the propertychange event, make sure it's the value that changed.
        if (window.event && event.type == "propertychange" && event.propertyName != "value") {
            return;
        }

        if (name != "") {
            // Clear any previously set timer before setting a fresh one
            window.clearTimeout($(this).data("timeout"));
            $(this).data("timeout", setTimeout(function () {
                if (filterSearchType) {
                    onChangeCallback(name, filterSearchType.split(","));
                } else {
                    onChangeCallback(name);
                }

            }, 850));// 0.85s
        }
    // console.log("LIST HEIGHT!",$('#chattab_NewMessage').find('.fl-widget-name-selector').height())
    // if($('.message_content_NewMessage ').find('.fl-widget-name-selector').height() > 50 ){
    //     console.log("hey")
    //     // $('.message_content_NewMessage ').perfectScrollbar('');
    //             $('.message_content_NewMessage ').perfectScrollbar('update');
    // }

    });

}

function bindUserSelectionOnSelectItemEvent(onSelectItemCallback) {
    $(".fl-selectable-name").click(function () {
        var itemType = $(this).attr("data-type");
        var itemValue = $(this).attr("data-id");
        var itemDisplayText = $(this).find(".fl-ui-selectable-name-data-wrapper > span").html();

        onSelectItemCallback(itemType, itemValue, itemDisplayText);
        $(".fl-widget-name-selector-result").html('');
    });
}

function onSearch(name, filterSearchType) {

    var url = CONTEXT + "/search";
    var params = {
        search_string: name,
        data_type: "JSON",
        exclude_users: [],
        exclude_groups: [],
        exclude_departments: [],
        exclude_positions: [],
        exclude_personal_groups: []
    };

    if (filterSearchType) {
        params.filter_search_type = filterSearchType;
    }

    // add excluded names
    var selectedNames = getSelectedNames();

    console.log("selected",selectedNames);

    for (var index in selectedNames) {
        var selectedName = selectedNames[index];
        if (selectedName["type"] == UserType.USER) {
            params.exclude_users.push(selectedName["value"]);
        } else if (selectedName["type"] == UserType.GROUP) {
            params.exclude_groups.push(selectedName["value"]);
        } else if (selectedName["type"] == UserType.DEPARTMENT) {
            params.exclude_departments.push(selectedName["value"]);
        } else if (selectedName["type"] == UserType.POSITION) {
            params.exclude_positions.push(selectedName["value"]);
        } else if (selectedName["type"] == UserType.PERSONAL_GROUP) {
            params.exclude_personal_groups.push(selectedName["value"]);
        }
    }

    console.log(params);
    if (ajaxholder) ajaxholder.abort();
    ajaxholder = $.get(url, params, function (response) {
        $(".fl-autocomplete-loading").css("display", "none");
        console.log(response)
        displayUsers(response);
    });

    // show loading
    $(".fl-autocomplete-loading").css("display", "block");

}

function displayUsers(html) {
    // $(".fl-widget-name-selector-result").html(html);


    
        if (typeof html['results'] !== "undefined") {
        //var html_parsed = JSON.parse(html['results']);
            var ret = '';
            $(".fl-widget-name-selector-result").html('');
            $.each(html['results'], function(id, value){
                // console.log('TESTVALUE', value);
                if(value["type"] == "USER"){
                    ret += '<div class="fl-ui-selectable-name-wrapper">';
                        ret += '<div class="fl-selectable-name fl-ui-selectable-name" data-type="'+value["type"]+'" data-id="'+value["id"]+'">';
                            ret += '<div class="fl-ui-selectable-name-avatar-wrapper">';

                            if(value["type"] == "PERSONAL_GROUP"){
                                ret += '<div class="fl-avatar"><i class="fa fa-2x fa-group"></i></div>';
                            }else if(value["type"] == "DEPARTMENT"){
                                ret +=  '<svg class="icon-svg" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-deptorgchart"></use></svg>';
                            }else if(value["type"] == "POSITION"){
                                ret += '<svg class="icon-svg" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-position-settings"></use></svg>';
                            }else{
                                ret += '<img data-placement="bottom" data-original-title="'+value["result"]+'" src="'+value["image"]+'" class="avatar pull-left userAvatar">';
                            }

                           
                            ret += '</div>';
                            ret += '<div class="fl-ui-selectable-name-data-wrapper"><span title="' + value["result"] + '">'+ value["result"] +'</span></div>';
                        ret += '</div>';
                    ret += '</div>';
                }
            });
        }
        $(".fl-widget-name-selector-result").append(ret);
        bindUserSelectionOnSelectItemEvent(onSelectUserItem);
     $('.fl-widget-name-selector-result-inner').perfectScrollbar();
     $('.message_content_NewMessage').perfectScrollbar();
}

function onSelectUserItem(itemType, itemValue, itemDisplayText) {

    // add new entry to the selected names
    var itemHTML = createSelectedUserItemHTML(itemType, itemValue, itemDisplayText);
    $(".fl-selected-names-container").append(itemHTML);
    var cutter = '<div id="cutter" style="clear:both;"><div>';
    var cutterExist = $('#cutter').length;
    if(cutterExist > 0 ){

    }else{
        $('#name_container').append(cutter)
    }
    clearSelectableUsers();
}

function clearSelectableUsers() {
    $(".fl-widget-name-selector > .fl-autocomplete-input").val("");
    displayUsers("");

    $(".fl-widget-name-selector > .fl-autocomplete-input").focus();
}

function createSelectedUserItemHTML(itemType, itemValue, itemDisplayText) {
    var itemHTML = $($("#fl-selected-name-template").html());
    var itemTypeId = getDataTypeIdFromUserItemType(itemType);
    var iconHTML = getUserItemIcon(itemTypeId);

    itemHTML.find("span").html(itemDisplayText);
    itemHTML.find(".removeTag_listRecipient1").attr("data-id", itemValue);
    itemHTML.attr("id", "listRecipient1_" + itemValue);
    itemHTML.attr("data-id", itemValue);
    itemHTML.attr("data-type", itemType);
    itemHTML.attr("data-type-id", itemTypeId);

    return itemHTML;
}

function getUserItemIcon(itemTypeId) {

    switch (itemTypeId) {
        case UserType.USER:
            {

            }
            ;
            break;
    }

}

function getDataTypeIdFromUserItemType(itemType) {

    if (itemType == "USER") {
        return UserType.USER;
    } else if (itemType == "GROUP") {
        return UserType.GROUP;
    } else if (itemType == "POSITION") {
        return UserType.POSITION;
    } else if (itemType == "DEPARTMENT") {
        return UserType.DEPARTMENT;
    } else if (itemType == "PERSONAL_GROUP") {
        return UserType.PERSONAL_GROUP;
    } else {
        return itemType;
    }

    return 0;

}
(function () {

    

    $(document).ready(function () {

        bindUserSelectionFilterOnChangeEvent(onSearch);
        bindUserSelectionOnSelectItemEvent(onSelectUserItem);
        name_selection.getSelectedNames = getSelectedNames;
        name_selection.setSelectedNames = setSelectedNames;
        name_selection.clearSelectedNames = clearSelectedNames;

        // hide search user loading indicator by default
        $(".fl-autocomplete-loading").css("display", "none");

        $("body").on("click", ".removeTag_listRecipient1", function () {
            $(this).closest(".recipient").remove();
        });

    });

    //  clear selectable users on click outside of the selection field
    $(document).mouseup(function (e) {
//        var recipients = $(".fl-widget-name-selector > .fl-autocomplete-input");
//
//        if (!recipients.is(e.target) && recipients.has(e.target).length === 0) {
//            clearSelectableUsers();
//        }
    });

    

})();
