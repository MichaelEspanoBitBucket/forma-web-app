$(document).ready(function(){
	Navigation.init();

	AdvanceFilter2.addFilter();
    AdvanceFilter2.deleteFilter();
    AdvanceFilter2.updatedFieldOperator();


    var json_params = {
    	"searchbox":$("#searchNextPreviousNav"),
    	"previousbtn":$("#searchPreviousNav"),
    	"nextbtn":$("#searchNextNav"),
    	"cancelbtn":$("#cancelSearchNav"),
    	"parentContainer":$(".link-container"),
    	"targetEleContainer":$(".fl-widget-wrapper-scroll li"),
    	"defaultState":function(){
    		$(".target-searched-np").css("background-color","#FFF");
    		$(".target-searched-np").find(".nav-title").css("color","#6e6e6e");
            $(".target-searched-np").removeClass("target-searched-np");
    	},
    	"target_ele":[{
    		"parent":"li",
    		"target":".original_data",
    		"target_text":$("li .nav-menu-left"),
    		"type":"text",
    		"selectedState":function(target_ele_parent_obj){
    			var bg_color = $(".fl-user-header-wrapper").css("background-color");
    			target_ele_parent_obj.find(".original_data").css("background-color",bg_color);
                target_ele_parent_obj.find(".nav-title").css("color","#FFF");
    		},
    		"unselectedState":function(target_ele_parent_obj){
    			target_ele_parent_obj.find(".original_data").css("background-color","#FFF")
    			target_ele_parent_obj.find(".nav-title").css("color","#6e6e6e");
    		},
    	}]
    }
    SearchNextPrevious.init(json_params);
})
var numberOfLayers = 10;
var originalAddressIndex = 0;
var ph_width = 0;
var ph_height = 0;
var boundary = "";
var originalIndex = 0;
var originalPlaceholderIndex = 0;

function swapElements(ele1,ele2){
	// // create marker element and insert it where obj1 is
	// var temp = document.createElement("div");
	// obj1.parentNode.insertBefore(temp, obj1);

	// // move obj1 to right before obj2
	// obj2.parentNode.insertBefore(obj1, obj2);

	// // move obj2 to right before where obj1 used to be
	// temp.parentNode.insertBefore(obj2, temp);

	// // remove temporary marker node
	// temp.parentNode.removeChild(temp);
	//ele2.after(ele1);
	//var ele1Storage = ele1.detach();
	ele2.last().after(ele1)
	unsavedEditConfirmation(true);

}

Navigation = {
	count : 1,
	deletedMenu : [],
	init : function(){
		var pathname = window.location.pathname;
		this.indent();
		this.activate();
		this.indent();
		this.droppableContainer();
		this.settings();
		this.saveNavigation();
		this.removeNavigation();
		this.load();
		this.updateNavigationView();
		this.createNewNavigation();
		this.resetNavigation();
		this.deleteNavigation();
		UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".setAccessMenu");
		this.updateStatus();
		this.resizeContainer();
		this.changeFormFilter();
		this.enableFilters();
		if((pathname == "/user_view/nav_settings")){
			this.hotKeys();
		}
		// var countings = [];

		// $(".nav-menu-ul .nav-menu-li").each(function(){
		// 	countings.push($(this).attr("data_id"))
		// })

		// countings = countings.sort(function(a, b) {
		// 	return b - a
		// }); //getting how many objects created on the form and sort this variable to know what is the total countings
		// this.count = parseInt(if_undefinded(countings[0],0))+1;

		this.setMenuIndex();
		
	},
	resizeContainer:function(){
		$('.nav-menu-ul').closest('div.column').find('.ps-container').addClass('link-container');
		$('.nav-menu-ul').closest('div.column').resizable({
			handles: 's',
			alsoResize: '.link-container',
			stop:function(){
				$(this).find('.ps-container').perfectScrollbar('update');
			},
			minHeight:490
		});
		// $('.nav-menu-ul').closest('.nav-menu-ul').resizable();
	},
	hotKeys:function(){
		/*nav settings shortcut*/
		var selfNav = this;
		
			$(document).on({
				"keydown":function(e){
					if($('input[type="text"]').is(':focus') == false){
					var keycode = e.which||e.keyCode;
					var ele_selected = $('li[address]').has('.active-secondary-element-nav');
					var li_ele_address = ele_selected.attr('address');
					var primarySelectedEle = $('li[address]').has('.active-primary-element-nav');
					var eleBefore = primarySelectedEle.prev();
					var eleAfter = primarySelectedEle.next();
					var switcher =true;
					var eleAdjacentRankAfter = primarySelectedEle.nextAll().filter(function(){
						if(switcher && (Number($(this).attr('address'))>Number(li_ele_address))){
							return false;
						}
						if(switcher&&(Number($(this).attr('address'))==Number(li_ele_address))){
							return true;
						}
						else{
							switcher = false;
							return false;
						}
					});
					switcher =true;
					var eleAdjacentRankBefore = primarySelectedEle.prevAll().filter(function(){
						if(switcher && (Number($(this).attr('address'))>Number(li_ele_address))){
							return false;
						}
						if(switcher&&(Number($(this).attr('address'))==Number(li_ele_address))){
							return true;
						}
						else{
							switcher = false;
							return false;
						}
					});
					switcher = true;
					var eleFamilySelected = primarySelectedEle.nextAll().filter(function(){
						if(switcher && (Number($(this).attr('address'))>Number(li_ele_address))){
							return true;
						}
						else{
							switcher = false;
							return false;
						}
					}).add(primarySelectedEle);
					if(keycode == 46){
						e.preventDefault();
						 if ($('.active-secondary-element-nav').exists() && !$('#popup_container').exists()) {
						 	selfNav.removeMultipleNavigation();	
						 }
						
					}
					if((keycode == 83 && e.ctrlKey)){

						e.preventDefault();
						e.stopPropagation();
						$('#nav-open-save-dialog').trigger('click');

					}
					if(keycode == 39){
						e.preventDefault();
						if($('.nav-menu-placeholder-4').find('.active-secondary-element-nav').exists()|| ($('.active-secondary-element-nav').closest('li.nav-menu-li').index()=="0") || ($('[address="4"]').has('.active-secondary-element-nav').exists()) || (Number(li_ele_address)-Number(ele_selected.prev().attr('address')) > 0) ){
							return false;
						}
						
						$('.active-secondary-element-nav').each(function(){
							
							shortCutNavPosition($(this),"add");
							
						});
					}
					if(keycode == 37){
						e.preventDefault();
						
						var selected_aligned_ele = ele_selected.nextUntil($('[address="'+ele_selected.attr('address')+'"]')).filter(function(){
							return Number($(this).attr('address')) < Number(ele_selected.attr('address'));
						})
					
						if( ($('[address="0"]').has('.active-secondary-element-nav').exists()) || (ele_selected.nextAll('li[address="'+ele_selected.attr('address')+'"]').exists() && !selected_aligned_ele.exists()) ){
							
							return false;
						}
						$('.active-secondary-element-nav').each(function(){
							
							shortCutNavPosition($(this),"minus");
							
						});
						
					}
					if(keycode == 40){
						e.preventDefault();
						
						if(eleAfter.exists()){
							if(e.ctrlKey){
								if(e.shiftKey){
									var eTrigger = $.Event('mousedown');
									
									eTrigger.ctrlKey = true;
									eleAfter.find('.nav-menu').trigger(eTrigger);
								}else{
									eleAfter.find('.nav-menu').trigger('mousedown');
								}
								
								
							}
							else if(eleAdjacentRankAfter.exists()){
								console.log("eleAdjacentRankAfter",eleAdjacentRankAfter)
								
								switcher = true;
								var eleAdjacentRankAfterFamily = eleAdjacentRankAfter.eq(0).nextAll().filter(function(){
									if(switcher && (Number($(this).attr('address'))>Number(li_ele_address))){
										return true;
									}
									else{
										switcher = false;
										return false;
									}
								}).add(eleAdjacentRankAfter.eq(0));
								console.log('eleAdjacentRankAfterFamily',eleAdjacentRankAfterFamily);

								swapElements(eleFamilySelected,eleAdjacentRankAfterFamily);
								
							}
							
							
							
						}
						
						
					}
					if(keycode == 38){
						e.preventDefault();
						
						if(eleBefore.exists()){
							if(e.ctrlKey){
								if(e.shiftKey){
									var eTrigger = $.Event('mousedown');
									
									eTrigger.ctrlKey = true;
									eleBefore.find('.nav-menu').trigger(eTrigger);
								}else{
									eleBefore.find('.nav-menu').trigger('mousedown');
								}
								
							}
							else if(eleAdjacentRankBefore.exists()){
								console.log('eleAdjacentRankBefore',eleAdjacentRankBefore)
								switcher = true;
								var eleAdjacentRankBeforeFamily = eleAdjacentRankBefore.eq(0).nextAll().filter(function(){
									if(switcher && (Number($(this).attr('address'))>Number(li_ele_address))){
										return true;
									}
									else{
										switcher = false;
										return false;
									}
								}).add(eleAdjacentRankBefore.eq(0));
								console.log('eleAdjacentRankBeforeFamily',eleAdjacentRankBeforeFamily)
								
								swapElements(eleAdjacentRankBeforeFamily,eleFamilySelected);
								
								
							}
							// else if(li_ele_address === eleBefore.attr('address')){
							// 	swapElements(eleBefore,primarySelectedEle);
							// }

						}	
					}
					// if(keycode == 67){
					// 	e.preventDefault();
						
					// 	if(eleBefore.exists()){
					// 		if(e.ctrlKey){
					// 			console.log("CTRL C")
					// 			var samp = $('.active-primary-element-nav').parents('.nav-menu-li').attr('menu_id')
					// 			var json_data_copy = $('[menu_id="'+ samp+'"]').attr("json-data");
					// 			var json_nav_copy = JSON.parse(json_data_copy);
					// 			console.log(json_nav_copy.data_id);
								
								

					// 			// localStorage["navToStore"] = escape(divToStore.html());
					// 			// console.log(localStorage["navToStore"])
					// 		}
					// 	}
					// }
					// if(keycode == 86){
					// 	e.preventDefault();
						
					// 	if(eleBefore.exists()){
					// 		if(e.ctrlKey){
					// 			console.log("CTRL V")
					// 			$("#saveEleSettings").trigger('click');
					// 			// var divToPaste= unescape(localStorage["navToStore"])	;
					// 			// console.log($.type(divToPaste))
					// 			// $(divToPaste).appendTo('.nav-menu-ul')
					// 			$('[menu_id="189"]').clone().appendTo('.nav-menu-ul').attr('menu_id','400')
					// 			$('[menu_id="400"]').attr('data_id','16');
					// 			var copy_data = JSON.parse($('[menu_id="400"]').children('.original_data').attr("json-original-data"))
					// 			console.log(copy_data);
					// 			copy_data.data_id= "123";
					// 			console.log(copy_data);
					// 			console.log(copy_data.data_id);
					// 		}	
					// 	}
					// }//-End-//
					}
				}
			})
			
			
		
		/*end nav*/
	},
	load : function(callback){
		//code here
		
		$('body').on('mousedown.focusObj',function(e){
			
			if($(e.target).is('.nav-menu')|| $(e.target).closest('.nav-menu').exists()){
				var target_ele = $(e.target).closest('.nav-menu') || $(e.target); 
				var ul_nav_ele = $(target_ele).closest('ul');
				if(ul_nav_ele.find('.active-primary-element-nav').exists() && !e.ctrlKey){
					ul_nav_ele.find('.active-primary-element-nav, .active-secondary-element-nav').removeClass('active-primary-element-nav').removeClass('active-secondary-element-nav');
				}
				else if(e.ctrlKey){
					
					ul_nav_ele
					.find('.active-primary-element-nav')
					.removeClass('active-primary-element-nav');
					$(target_ele)
					.addClass('active-primary-element-nav')
					.addClass('active-secondary-element-nav');
				}
				$(target_ele)
				.addClass('active-primary-element-nav')
				.addClass('active-secondary-element-nav');	
			}
			else if($(e.target).is('.ps-container')){
				$('.active-primary-element-nav, .active-secondary-element-nav').removeClass('active-secondary-element-nav').removeClass('active-primary-element-nav');
			}
			
		});

		
		$(".dataTable_nav").dataTable().fnDestroy();
		var oColReorder = {
            "iFixedColumns": 0
        };
		var oTable = $('.dataTable_nav').dataTable( {
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> '+
                       '<div class="bar1"></div> '+
                       '<div class="bar2"></div> '+
                       '<div class="bar3"></div> '+
                       '<div class="bar4"></div> '+
                       '<div class="bar5"></div> '+
                       '<div class="bar6"></div> '+
                      '<div class="bar7"></div> '+
                      '<div class="bar8"></div> '+
                      '<div class="bar9"></div> '+
                      '<div class="bar10"></div> '+
                 '</div>'
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/navigation",
            "fnServerData": function(sSource, aoData, fnCallback) {
            	// aoData.push({"name": "limit", "value": jsonDelegateData['limit']});
                // aoData.push({"name": "search_value", "value": jsonDelegateData['search_value']});
                aoData.push({"name": "action", "value": "loadNavigation"});
                // aoData.push({"name": "column-sort", "value": jsonDelegateData['column-sort']});
                // aoData.push({"name": "column-sort-type", "value": jsonDelegateData['column-sort-type']});
                
                $.ajax({
                    // "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        try{
                            // console.log(data)
                            data = JSON.parse(data);
                            // return;
                            fnCallback(data);
                        }catch(err){
                            console.log(err)
                        }
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
            	
            	var obj = '.dataTable_nav';
            	var listOfappContainer = '.fl-list-of-app-record-wrapper';
            	dataTable_widget(obj, listOfappContainer)	
            	$(".fl-widget-wrapper-scroll").perfectScrollbar("update");
            
            },
            fnDrawCallback : function(){
                $(".tip").tooltip();
                if (callback) {
                    callback(1);
                }

            },
            iDisplayStart : 0,
            // iDisplayLength : parseInt(jsonDelegateData['limit']),
        });
        return oTable;
	},
	indent : function(){
		var self = this;
		
		$(".nav-menu-ul").sortable({
			placeholder: "nav-menu-placeholder",
			// containment: ".nav-menu-container",
			start : function(ev, ui){
				//get original indexes
				originalAddressIndex = $(ui.item).attr("address");
				originalIndex = $(ui.item).index();
				originalPlaceholderIndex = $(ui.item).index()+1;
				var thisEleAddress = $(ui.item).attr("address")
				thisEleAddress = parseInt(thisEleAddress);

				//get height and width of placeholder
				var childAddress = [];
				var flag = true;
				$(ui.item).nextAll().filter(function(a, b){
					var eleAddress = parseInt($(b).attr("address"));
					// console.log(eleAddress+" <= "+originalAddressIndex);
					if(eleAddress<=originalAddressIndex){
						flag = false;
					}else{
						if(eleAddress>originalAddressIndex && flag == true){
							childAddress.push(eleAddress)
							// $(thisEle).find(".to-transfer").append($(b));
							// $(b).remove();
							return true;
						}
					}
					return false;
				});

				childAddress = childAddress.sort(function(a, b) {
					return b - a
				});

				var highestAddress = parseInt(if_undefinded(childAddress[0],0));
				ph_width = ((highestAddress*30)+358)-(thisEleAddress*30);
				var padding_top = (childAddress.length*5)
				ph_height = ((childAddress.length+1)*40)+padding_top;


				//get next boundary
				var flagBound = true;
				boundary = $(ui.item).nextAll().filter(function(a, b){
					if($(b).attr("address")!=undefined){
						if(originalAddressIndex>=$(b).attr("address")){
							return true;
						}
					}
				})[0];

				
			},
			sort : function(ev, ui){
				self.sortToIndent($(ui.item),"sort");
			},
			beforeStop : function(ev, ui){
				var placeholderAddress = $(".nav-menu-placeholder").attr("placeholder-class");
				$(ui.item).addClass(placeholderAddress)


				var itemClass = $(ui.item).attr("class").split(" ");
				for (var i = 0; i < itemClass.length; i++) {
					if(itemClass[i]!="nav-menu-li" && itemClass[i]!=placeholderAddress){
						$(ui.item).removeClass(itemClass[i]);
					}
				};
			},
			stop : function(ev, ui){
				/*stop ng sortable nav*/
				unsavedEditConfirmation(true);
				var thisEleAddress = parseInt($(ui.item).attr("address"));
				var flag = true;
				$(ui.item).find("ul.to-transfer").eq(0).find("li.nav-menu-li").each(function(){
					var newAddressIndex = originalAddressIndex-thisEleAddress;
					var eleAddress = parseInt($(this).attr("address"));
					newAddressIndex = eleAddress-newAddressIndex;
					//pampigil
					if(newAddressIndex>=numberOfLayers){
						newAddressIndex = numberOfLayers-1;
					}else if(newAddressIndex<0){
						newAddressIndex = 0;
					}
					if(newAddressIndex!=eleAddress){

						var newAddress = "nav-menu-placeholder-"+newAddressIndex;
						var oldAddress = "nav-menu-placeholder-"+eleAddress;
						$(this).addClass(newAddress);
						$(this).removeClass(oldAddress);
						$(this).attr("address",newAddressIndex)

					}
				})
				$(ui.item).after($(ui.item).find("ul.to-transfer").eq(0).html());
				$(ui.item).find("ul.to-transfer").eq(0).html("")
			},
			change : function(ev, ui){
				self.sortToIndent($(ui.item),"change");
			},
			update : function(){
			}
		});
	},
	activate : function(){
		return false;
		$("body").on("click",".nav-menu",function(){
			if($(this).hasClass('nav-menu-activate')){
				$(this).removeClass("nav-menu-activate");
			}else{
				$(this).addClass("nav-menu-activate");
			}
			
		})
	},
	droppableContainer: function(){
		$(".fl-inner-content ul").droppable();
	},
	sortToIndent : function(thisEle,type){
				
		var placeholderAddress = "nav-menu-placeholder-";
		var thisEleAddress = thisEle.attr("address");
		//previous last
		var innerPreviousEle = thisEle.prevAll().filter(function(a, b){
			if($(b).attr("address")==0){
				return true;
			}
			return false;
		}).eq(0);
		var previousEle = $(".nav-menu-placeholder").prev();
		var previousAddress = previousEle.attr("address");
		if(thisEleAddress==previousAddress){
			previousEle = $(".nav-menu-placeholder").prev().prev();
			previousAddress = previousEle.attr("address");
		}

		var newAddress = 0;
		if(typeof previousAddress == "undefined"){
			newAddress = 0;
		}else{
			var addressIndex = parseInt(previousAddress);
			//logic in indention
			if(thisEle.offset().left>(innerPreviousEle.offset()||$('<anonymous/>').offset()).left){

				var countPrev = addressIndex+1;
				var inc = 0;
				for(var i = 0;i <= countPrev; i++){
					inc = i*30;
					if(thisEle.offset().left>=(innerPreviousEle.offset()||$('<anonymous/>').offset()).left+inc){
						newAddress = i;

					}
				}
			}
		}
		// console.log(newAddress+" < "+originalAddressIndex)
		// console.log(boundary)
		if(newAddress<originalAddressIndex){
			// console.log(boundary)
			if(boundary!=undefined){
				if(newAddress<$(boundary).attr("address")){
					newAddress = $(boundary).attr("address");
				}
				
			}
				// newAddress = if_undefinded($(boundary).attr("address"),newAddress);
		}
		//end logic for indention

		// if(type=="change"){
		if(originalPlaceholderIndex!=$(".nav-menu-placeholder").index()){
			// console.log(213213)
			if($(".nav-menu-placeholder").prev().attr("address")==undefined){
				newAddress = 0;
			}else{
				newAddress = parseInt($(".nav-menu-placeholder").prev().attr("address"))+1;
				
			}
		}
		if($(".nav-menu-placeholder").index()==0){
			newAddress = 0;
		}
		
		//pampigil
		if(newAddress>=numberOfLayers){
			newAddress = numberOfLayers-1;
		}else if(newAddress<0){
			newAddress = 0;
		}
		
		// indent class
		placeholderAddress = placeholderAddress+newAddress;
				
		// console.log(placeholderAddress)
		// console.log(placeholderAddress)
		var oldAddress = "nav-menu-placeholder-"+thisEleAddress
		
		$(".nav-menu-placeholder").removeClass(oldAddress);
		$(".nav-menu-placeholder").addClass(placeholderAddress);

		$(".nav-menu-placeholder").attr("placeholder-class",placeholderAddress)
		$(".nav-menu-placeholder").attr("addressIndex",newAddress);
		thisEle.attr("address",newAddress);
		originalAddressIndex = parseInt(originalAddressIndex);
		var flag = true;

		thisEle.nextAll().filter(function(a, b){
			var eleAddress = parseInt($(b).attr("address"));
			// console.log(eleAddress+" <= "+originalAddressIndex);
			// if(eleAddress>newAddress){
			// 	$(b).appendTo($(thisEle).find("ul.to-transfer").eq(0));
			// 	$(".nav-menu-ul").sortable( "refresh" );
			// 	// $(b).remove();
			// 	return true;
			// }
			if(eleAddress<=originalAddressIndex){
				flag = false;
			}else{
				if(eleAddress>originalAddressIndex && flag == true){
					var newAddressIndex = 0;
					// if(thisEleAddress>newAddress){
						$(b).appendTo($(thisEle).find("ul.to-transfer").eq(0));
						$(".nav-menu-ul").sortable( "refresh" );
						// $(b).remove();
						return true;
					// }
				}
			}
			return false;
		});

		
		$(".nav-menu-placeholder").css({
			"width" : ph_width+"px",
			"height": ph_height+"px"
		})

	},
	settings : function(){
		var self = this;
		$("body").on("click",".nav-settings, #nav-newEntry",function(){
			ui.block();
			var thisnNavSettings = this;
			var triggerType = "1";

			var fields_filter = "";
			var fields_filter_date = "";


			
			if($(this).hasClass("nav-settings")){
				triggerType = "2";
			}
			var json_body = $(this).closest(".nav-menu-li").attr("json-data");
			var json_original_data = $(this).closest(".original_data").attr("json-original-data");
			// console.log(json_original_data)
			try{
				json_body = JSON.parse(json_body);
				json_original_data = JSON.parse(json_original_data);
				UserAccessFlagCheckBox.originalChecked = json_original_data['navigation_users'];
			}catch(err){
				console.log(err)
				json_body = [];
				UserAccessFlagCheckBox.originalChecked = {};
			}
			if(json_body['checkedEle']){
				UserAccessFlagCheckBox.checkedEle = JSON.parse(json_body['checkedEle']);
			}else{
				UserAccessFlagCheckBox.checkedEle = {};
			}
			if(json_body['checkedEle']){
				UserAccessFlagCheckBox.unCheckedEle = JSON.parse(json_body['unCheckedEle']);
			}else{
				UserAccessFlagCheckBox.unCheckedEle = {};
			}
			
			var title = if_undefinded(json_body['title'],"");
			var link_type = if_undefinded(json_body['link_type'],"3");var displayForm = "display";var displayOthers = "display";var displayNoneType = "display";
			
			var form_link_type = if_undefinded(json_body['form_link_type'],"form_list");
			var form_id = if_undefinded(json_body['form_id'],"0");

			var enableFilters = if_undefinded(json_body['enableFilters'],"0");
			var saved_fields_filter = if_undefinded(json_body['fieldFilters'],false);

			var other_text = if_undefinded(json_body['other_text'],"");
			var data_id = if_undefinded(json_body['data_id'],0);
			var filterFields = self.getFilterFields(form_id);

			fields_filter = filterFields['fields_filter'];
			fields_filter_date = filterFields['fields_filter_date'];

			// alert(fields_filter);
			// return false;
			var ret = '<div><h3 class="fl-margin-bottom">';
				ret += '<svg class="icon-svg icon-svg-modal" viewBox="0 0 100.264 100.597"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-navigation-settings"></use></svg><span> Navigation Settings</span>';
				ret += '</h3></div>';
				ret += '<div class="hr"></div>';
				ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
				ret += '<div class="content-dialog" style="height: auto;">';
					ret += '<div id="tab-container" class="flas" style="width:100%;">';
		                ret += '<ul>';
		              		ret += '<li><a href="#tabs-1">Primary Settings</a></li>';
		                	ret += '<li><a href="#tabs-2">Access Settings</a></li>';
		                ret += '</ul>';

                    	ret += '<div id="tabs-1" style="padding:10px;">';
		                    ret += '<div class="fields_below section clearing fl-field-style">';
		                        ret += '<div class="input_position_below column div_1_of_1">';
		                        ret += '<span class="">Title: <font color="red">*</font></span>';
		                           ret += '<input type="text" name="" class="form-text" id="nav-title-settings" value="'+ htmlEntities(title) +'">';
		                        ret += '</div>';
		                    ret += '</div>';
		                    ret += '<div class="fields_below section clearing fl-field-style">';
		                        ret += '<div class="input_position_below column div_1_of_1">';
		                        ret += '<span class="">Link Type: <font color="red">*</font></span>';
		                        	// ret += '<div><label><input type="radio" name="nav-linkType" '+ setChecked("3",link_type) +' class="nav-linkType css-checkbox" value="3" id="nav-linkType3"><label for="nav-linkType3" class="css-label"></label> None</label></div>';
		                         //   	ret += '<div><label><input type="radio" name="nav-linkType" '+ setChecked("1",link_type) +' class="nav-linkType css-checkbox" value="1" id="nav-linkType1"><label for="nav-linkType1" class="css-label"></label> Form</label></div>';
		                         //   	ret += '<div><label><input type="radio" name="nav-linkType" '+ setChecked("2",link_type) +' class="nav-linkType css-checkbox" value="2" id="nav-linkType2"><label for="nav-linkType2" class="css-label"></label> Other Link</label></div>';
		                        	ret += '<select class="form-select nav-linkType" id="nav-linkType" style="margin-top:0px">';
		                        		ret += '<option '+ setSelected("3",link_type) +' value="3">None</option>';
		                        		ret += '<option '+ setSelected("1",link_type) +' value="1">Form</option>';
		                        		ret += '<option '+ setSelected("4",link_type) +' value="4">Page</option>';
		                        		ret += '<option '+ setSelected("2",link_type) +' value="2">Other Link</option>';
		                        	ret += '</select>';
		                        ret += '</div>';
		                    ret += '</div>';
		                    ret += '<div>';
		                    	if(link_type=="1"){
		                    		displayForm = "";
		                    	}else if(link_type=="2"){
		                    		displayOthers = "";
		                    	}else if(link_type=="3" || link_type=="4"){
		                    		displayNoneType = "";
		                    	}
		                    	ret += '<div class="'+ displayForm +'" nav-linkType="1">';
				                    ret += '<div class="fields_below section clearing fl-field-style">';
				                        
				                        ret += '<div class="input_position_below column div_1_of_1">';
				                        	ret += '<span class="">Type of Form Link: <font color="red">*</font></span><br/>';
				                           ret += '<label><input type="radio" name="nav-form-type" '+ setChecked("form_list",form_link_type) +' class="nav-form-type css-checkbox" value="form_list" id="nav-form_list"><label for="nav-form_list" class="css-label"></label>Request List </label>';
				                           ret += '<label> <input type="radio" name="nav-form-type" '+ setChecked("newRequest",form_link_type) +' class="nav-form-type css-checkbox" value="newRequest" id="nav-newRequest"><label for="nav-newRequest" class="css-label"></label>New Request</label>';
				                        ret += '</div>';
				                    ret += '</div>';
				                    ret += '<div class="fields_below section clearing fl-field-style">';
				                        ret += '<div class="input_position_below column div_1_of_1">';
				                        	 ret += '<span class="">Related Form: <font color="red">*</font></span>';
				                           	ret += '<select class="form-select" id="nav-form" style="margin-top:0px">';
			                                    ret += '<option value="0">--Select Form--</option>';
			                                    var company_forms = $.trim($(".company_forms").text());
			                                    try{
			                                    	company_forms = JSON.parse(company_forms);
			                                    	for(var i in company_forms){
			                                    		// console.log('formside',company_forms[i]['form_id'],form_id,data_id);
			                                    		ret += '<option value="'+ company_forms[i]['form_id'] +'" '+ setSelected(company_forms[i]['form_id'], form_id) +'>'+ company_forms[i]['form_name'] +'</option>'; //setSelected(company_forms[i]['form_id'], form)
			                                    	}
			                                    }catch(err){
			                                    	console.log(err)
			                                    }
			                                ret += '</select>';
				                        ret += '</div>';
				                    ret += '</div>';
				                    var display_filter = "";
				                    if(form_link_type=="newRequest"){
				                    	display_filter = "isDisplayNone";
				                    }
				                    
				                    ret += '<div class="form-list-filter-option '+ display_filter +'">';

					                    ret += '<div class="section clearing fl-field-style">';
					                    	ret += '<div class="column div_1_of_1">';
					                    		ret += '<label><input type="checkbox" name="enableFilters" '+ setChecked("1",enableFilters) +' class="css-checkbox" value="1" id="enableFilters"><label for="enableFilters" class="css-label"></label> Enable Advanced Filters</label>'; 
					                    	ret += '</div>';
					                    ret += '</div>';
					                    var enable_filter_display = "";
					                    if(enableFilters!="1"){
					                    	enable_filter_display = "isDisplayNone";
					                    }
				                    	ret += '<div class="fields_below adanceFiltersContainer '+ enable_filter_display +'">';
					                        
					                        ret += '<div class="input_position_below">';
					                        // ret += '<span class="">Advanced Filter: <font color="red">*</font></span>';
					                        ret += 	advanceFilterContent(fields_filter,fields_filter_date);
					                        ret += '</div>';
					                        ret += '</div>';
					                        ret += '</div>';
					                    ret += '</div>';
				                    ret += '</div>';
				                ret += '</div>';
				                ret += '<div class="'+ displayOthers +'" nav-linkType="2">';
				                    ret += '<div class="fields_below">';
				                        ret += '<div class="label_below2">URL: <font color="red">*</font></div>';
				                        ret += '<div class="input_position_below">';
				                           ret += '<input type="text" name="" class="form-text" id="nav-otherText" placeholder="http://sample.com" value="'+ htmlEntities(other_text) +'">';
				                        ret += '</div>';
				                    ret += '</div>';
				                ret += '</div>';
				                ret += '<div class="'+ displayNoneType +'" nav-linkType="3">';
				                ret += '</div>';
			                ret += '</div>';
			                // ret += '</div>';
                    	ret += '</div>';// end tabs 1

	                    ret += '<div class="setAccessMenu" id="tabs-2" style="padding:10px;">';
	                    	ret += RequestPivacy.setUsersDialog("navigation_users",json_body);
	                    ret += '</div>';// end tab 2
					ret+= '</div>';
					ret += '<div class="fields" style="">';
				        ret += '<div class="label_basic"></div>';
				        ret += '<div class="input_position" style="margin-top:5px;">';
				        ret += ' <input type="button" class="btn-blueBtn fl-margin-right fl-positive-btn" id="saveEleSettings" triggerType="'+ triggerType +'" value="OK">';
				        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
				    ret += '</div>';
				ret+= '</div>';
				var newDialog = new jDialog(ret, "", "650", "800", "", function() {   

				});
				$("#includeSearchMulti").closest(".label_below2").remove();
				newDialog.themeDialog("modal2"); 
				RequestPivacy.otherAttributes();
				$("#includeSearch").closest(".label_below2").remove();
				$(".dateRangeWrapper").remove();
				$("#tab-container").tabs({
					activate : function(){
 						$('.content-dialog-scroll').perfectScrollbar('update');
 					}
				});

				self.setSavedFilterFields(saved_fields_filter);


				ui.unblock();

				FieldCondition.updatedFieldOperator(".search-request-field_filter",".search-request-operator",".search-request-value_filter",".searchFiltereWrapper");
				$(".search-request_date_from").datetimepicker({
		            defaultDate: "+1w",
		            changeMonth: true,
		            dateFormat: 'yy-mm-dd',
		            onClose: function(selectedDate) {
		                $(".search-request_date_to").datetimepicker("option", "minDate", selectedDate);
		            }
		        });
		        $(".search-request_date_to").datetimepicker({
		            defaultDate: "+1w",
		            changeMonth: true,
		            dateFormat: 'yy-mm-dd',
		            onClose: function(selectedDate) {
		                $(".search-request_date_from").datetimepicker("option", "maxDate", selectedDate);
		            }
		        });
		        $(".nav-linkType").closest(".content-dialog-scroll").css({
		        	"max-height":"400px"
		        });
		        $(".nav-linkType").closest(".content-dialog-scroll").perfectScrollbar("update");
				/* Bind Function */

				//change of link type
				$(".nav-linkType").change(function(){
					var value = $(this).val();
					$("[nav-linkType]").removeClass("display");
					$("[nav-linkType]").addClass("display");
					$("[nav-linkType='"+ value +"']").removeClass("display");
				})


				//save nav object
				$("#saveEleSettings").click(function(){
					var type = $(this).attr("triggerType");
					var title = $.trim($("#nav-title-settings").val());
					var link_type = $(".nav-linkType").val();
					var enableFilters = $("#enableFilters").prop("checked");
					var err = 0;
					// form
					var form_link_type = $(".nav-form-type:checked").val();

					var form_id = $("#nav-form").val();
					console.log("hello!")
					// other
					var other_text = $.trim($("#nav-otherText").val());

					//access
					var json_privacy = {};
			        var departments = new Array();
			        var positions = new Array();
			        var users = new Array();
			        var groups = new Array();

			        //for deoartments
			        $(".departments").each(function() {
			            if ($(this).prop("checked")) {
			                departments.push($(this).val());
			            }
			        })
			        //for positions
			        $(".positions").each(function() {
			            if ($(this).prop("checked")) {
			                positions.push($(this).val());
			            }
			        })
			        //for users
			        $(".users").each(function() {
			            if ($(this).prop("checked")) {
			                users.push($(this).val());
			            }
			        })

			        //for users
			        $(".groups").each(function() {
			            if ($(this).prop("checked")) {
			                groups.push($(this).val());
			            }
			        })

			        //
			        json_privacy['departments'] = departments;
			        json_privacy['positions'] = positions;
			        json_privacy['users'] = users;
			        json_privacy['groups'] = groups;


			        //filters
			        var fields_filter = [];
			        // var fields_date_filter = [];
			        if(enableFilters==true){
			        	enableFilters = "1";
			        }
			        $(".searchFiltereWrapper").each(function(){
		        		fields_filter.push({
			        		"field_name":$(this).find(".search-request-field_filter").val(),
			        		"operator":$(this).find(".search-request-operator").val(),
			        		"value":$(this).find(".search-request-value_filter").val()
			        	})
		        	})
		        	// fields_date_filter.push({
		        	// 	"field":$(".search-request-field_filter_date").val(),
		        	// 	"operator":$(".search-request_date_from").val(),
		        	// 	"value":$(".search-request_date_to").val()
		        	// })
			        if(departments.length == 0 && positions.length == 0 && users.length == 0 && groups == 0){
			        	showNotification({
						    message: "Please select at least one (1) user.",
				            type: "error",
						    autoClose: true,
						    duration: 3
						})
						return false;
			        }
					if(title==""){
						err++;
					}

					if(link_type=="1"){
						if(form_id=="0"){
							err++;
						}
					}else if(link_type=="2"){
						if(other_text==""){
							err++;
						}
						if(!CheckUrl(other_text)){
				          	showNotification({
							    message: "Invalid URL. Please make sure your URL follows this format --> http://sample.com",
					            type: "error",
							    autoClose: true,
							    duration: 3
							})
							return false;
				        }
					}

					if(err>0){
						showNotification({
						    message: "Please fill out the required fields",
				            type: "error",
						    autoClose: true,
						    duration: 3
						})
						return false;
					}

					// console.log(UserAccessFlagCheckBox.checkedEle);
					// console.log(UserAccessFlagCheckBox.unCheckedEle);
					var data_id_json = self.count;
					if(type=="2"){
						data_id_json = data_id;
					}
					var json_data = {
						"title" : title,
						"link_type" : link_type,
						"form_link_type" : form_link_type,
						"form_id" : form_id,
						"other_text" : other_text,
						"navigation_users" : JSON.stringify(json_privacy),
						"data_id":data_id_json,
						"checkedEle" : JSON.stringify(UserAccessFlagCheckBox.checkedEle),
						"unCheckedEle" : JSON.stringify(UserAccessFlagCheckBox.unCheckedEle),
						"enableFilters" : enableFilters,
						"fieldFilters": JSON.stringify(fields_filter),
						// "fields_date_filter": JSON.stringify(fields_date_filter),

					};
					console.log("Data ",json_data)
					json_data = JSON.stringify(json_data);

					if(type=="1"){
						//insert
						var nav_model = $(".nav-menu-li-model .nav-menu-li").clone();
						nav_model.find(".nav-title").text(title);
						nav_model.attr("data_id",self.count);
						
						nav_model.attr("json-data",json_data);
						if($(".no-nav").hasClass("display")==false){
							$(".no-nav").addClass("display");
						}

						
						var focused_nav_ele = $('.active-primary-element-nav:eq(0)');
						if(focused_nav_ele.exists()){
							var nav_li_ele = focused_nav_ele.closest('.nav-menu-li');
							var nav_address = nav_li_ele.attr('address');
							var nav_active_class = nav_li_ele.attr('class');
							nav_model.attr('class',nav_active_class);
							nav_model.attr('address',nav_address)
							var conditionStopper = true;
							var lastEleTarget = nav_li_ele.nextAll('li[address]').filter(function(){
								var disAddress = $(this).attr('address');
								
								if(conditionStopper){
									if(nav_address < disAddress){
										return true;
									}else{
										conditionStopper = false;
										return false;
									}
								}else{
									return false;
								}
								

							});
							
							if(lastEleTarget.exists()){
								lastEleTarget.last().after(nav_model);
								
							}
							else{
								nav_li_ele.after(nav_model);
								
							}
						}else{
							$(".nav-menu-ul").append(nav_model)
						}
						//nav_model.find('.nav-menu:eq(0)').trigger('click');
						
						self.count++;
					}else if(type=="2"){
						//update
						var nav_model = $(thisnNavSettings).closest(".nav-menu-li");
						// console.log(nav_model)
						nav_model.find(".nav-title").text(title);
						
						nav_model.attr("json-data",json_data);
					}
					UserAccessFlagCheckBox.checkedEle = {};
					UserAccessFlagCheckBox.unCheckedEle = {};
					$(".fl-closeDialog").click();
					unsavedEditConfirmation(true);
					$(".fl-widget-wrapper-scroll").perfectScrollbar("update");
				})
		})

		$("body").on("dblclick",".nav-menu-li",function(){
			$(this).find(".nav-settings").trigger("click");
		})
		$("body").on("change",".nav-form-type",function(){
			var value = $(".nav-form-type:checked").val();
			if(value=="form_list"){
				$(".form-list-filter-option").show();
			}else{
				$(".form-list-filter-option").hide();
			}
			$(this).closest(".content-dialog-scroll").perfectScrollbar("update");
		})
	},
	getFilterFields : function(form_id){
		var ret = ""
		$.ajax({
            url: '/ajax/search',
            type: 'POST',
            async : false,
            // dataType: 'json',
            data: {
                "action":"getValidFilterFields",
                "form_id":form_id
            },

            success: function(data) {
            	// alert(data)
            	try{
            		data = JSON.parse(data);
            		ret = {"fields_filter":data['fields'],"fields_filter_date":data['dateFields']};
            		console.log(form_id);
            	}catch(err){
            		console.log(data)
            		console.log(err)
            	}
        	}
	    });
	    return ret;
	},
	changeFormFilter : function(){
		var self = this;
		$("body").on("change","#nav-form",function(){
			ui.block();
			var jsonFields = self.getFilterFields($(this).val());
			$(".searchFiltereWrapper:not(:eq(0))").remove();

			var fields = jsonFields['fields_filter'];
			// var fields_date = jsonFields['fields_filter_date'];
			var retFields = '<option value="Requestor">Requestor[Default]</option>'+
                            '<option value="Status">Status[Default]</option>'+
                            '<option value="TrackNo">Tracking Number[Default]</option>';
			// var retDateFields = "";

			//get fields
			for (var i in fields) {
	            if (fields[i]['value']) {
	                retFields += '<option value="' + fields[i]['value'] + '" field_input_type="'+ if_undefinded(fields[i]['field_input_type'],"") +'" data-type="'+ if_undefinded($(fields[i]).attr("data-type"),if_undefinded(fields[i]['type'],"")) +'">' + fields[i]['value'] + '</option>';
	            }
	        }

	        // get date fields
	        // retDateFields += '<option value="0">-------Select-------</option>';
	        // for (var i in fields_date) {
	        //     if (fields[i]['value']) {
	        //         retDateFields += '<option value="' + fields_date[i]['value'] + '">' + fields_date[i]['text'] + '</option>';
	        //     }
	        // }
	        // console.log(jsonFields)
	        //set fields
	        $(".searchFiltereWrapper:eq(0)").find(".search-request-field_filter").html(retFields);
	        $(".searchFiltereWrapper:eq(0)").find(".search-request-operator").val("=");
	        $(".searchFiltereWrapper:eq(0)").find(".search-request-value_filter").val("");


	        //set date fields
	  //       $(".search-request-field_filter_date").html(retDateFields);
			// $(".search-request_date_from").val("");
			// $(".search-request_date_to").val("");
	        //
	        FieldCondition.updatedFieldOperator(".search-request-field_filter",".search-request-operator",".search-request-value_filter",".searchFiltereWrapper");

			ui.unblock();
		})
	},
	enableFilters : function(){
		$("body").on("change","#enableFilters",function(){
			if($(this).prop("checked")){
				$(".adanceFiltersContainer").removeClass("isDisplayNone");
			}else{
				$(".adanceFiltersContainer").addClass("isDisplayNone");
			}
			$(this).closest(".content-dialog-scroll").perfectScrollbar("update");
		});
	},
	setSavedFilterFields : function(json_data){
		if(json_data!=false){
			if(typeof json_data==="string"){
				json_data = JSON.parse(json_data);
			}
			var counter = 1;
			for(var i in json_data){
				// console.log("saved_fields_filter",json_data[i]);
				$(".searchFiltereWrapper").eq(i).find(".search-request-field_filter").val(json_data[i]['field_name']);
				$(".searchFiltereWrapper").eq(i).find(".search-request-operator").val(json_data[i]['operator']);
				$(".searchFiltereWrapper").eq(i).find(".search-request-value_filter").val(json_data[i]['value']);
				if(json_data[counter]){
					$(".searchFiltereWrapper").eq(i).find(".addSearchFilter").trigger("click");
				}
				counter++;
			}
		}
	},
	
	saveNavigation : function(){

		var self = this;
		$("#nav-open-save-dialog").click(function(){
			var action = $(this).attr("action");
			var navID = $(this).attr("nav-id");
			var navObjects = [];
			var jsonObjectData = "";
			NavDataFixes();
			var navMenuHeight = $('.nav-menu-ul').closest('div.ps-container').closest('.ui-resizable').outerHeight()+"px";
			var navJSONProperties = {
				'height':navMenuHeight
			}
			var navJSONPropertiesArray = [];
			navJSONPropertiesArray.push(navJSONProperties)
			$(".nav-menu-ul .nav-menu-li").each(function(){
				var navThisEle = this;
				try{
					jsonObjectData = JSON.parse($(this).attr("json-data"));
					jsonObjectData['address'] = $(this).attr("address");
					console.log(jsonObjectData['address'])
					var parent_id = $(this).prevAll().filter(function(a, b){
						if(parseInt($(navThisEle).attr("address"))>parseInt($(b).attr("address"))){
							return true;
						}
						return false;
					}).eq(0);

					jsonObjectData['parent_id'] = if_undefinded(parent_id.attr("data_id"),"0");
					console.log(jsonObjectData['parent_id']);
					jsonObjectData['menu_id'] = $(navThisEle).attr("menu_id");
					console.log(jsonObjectData['menu_id']);
					jsonObjectData['indexing'] = $(navThisEle).index();
					console.log(jsonObjectData['indexing']);
					console.log("  ");
					navObjects.push(jsonObjectData);
					console.log(navObjects)
				}catch(err){
					console.log(err)
				}
			})
			if(navObjects.length==0){
				showNotification({
				    message: "Please add atleast 1 item menu.",
		            type: "error",
				    autoClose: true,
				    duration: 3
				})
				return false;
			}



			var ret = '<Div><h3 class="fl-margin-bottom">';
				ret += '<svg class="icon-svg icon-svg-modal" viewBox="0 0 100.264 100.597"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-navigation-settings"></use></svg> <span>Save Navigation</span>';
				ret += '</h3></div>';
				ret += '<div class="hr"></div>';
				ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
				ret += '<div class="content-dialog" style="height: auto;">';
					ret += '<div class="fields_below">';
                        ret += '<div class="label_below2">Title: <font color="red">*</font></div>';
                        ret += '<div class="input_position_below">';
                           ret += '<input type="text" name="" class="form-text" id="nav-parent-title" value="'+ htmlEntities($("#nav-info-title").text()) +'">';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields_below">';
                        ret += '<div class="label_below2">Description: <font color="red">*</font></div>';
                        ret += '<div class="input_position_below">';
                           ret += '<input type="text" name="" class="form-text" id="nav-parent-description" value="'+ htmlEntities($("#nav-info-desc").text()) +'">';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields_below">';
                        ret += '<div class="label_below2">Status: <font color="red">*</font></div>';
                        ret += '<div class="input_position_below">';
                           	ret += '<select class="form-select" id="nav-parent-status" style="margin-top:0px">';
                                ret += '<option value="1">Active</option>';
                                ret += '<option value="0">Deactivate</option>';
                            ret += '</select>';
                        ret += '</div>';
                    ret += '</div>';
					ret += '<div class="fields" style="">';
				        ret += '<div class="label_basic"></div>';
				        ret += '<div class="input_position" style="margin-top:5px;">';
				        ret += ' <input type="button" class="btn-blueBtn fl-margin-right" id="nav-save" value="Save">';
				        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
				    ret += '</div>';
				ret+= '</div>';
				var newDialog = new jDialog(ret, "", "", "300", "", function() {   
				});
				newDialog.themeDialog("modal2"); 

			//bind functions

			//save
			$("#nav-save").click(function(){
				var title = $.trim($("#nav-parent-title").val());
				var description = $.trim($("#nav-parent-description").val());
				var status = $("#nav-parent-status").val();
				var err = 0;

				if(title == "" || description == ""){
					showNotification({
					    message: "Please fill out required fields.",
			            type: "error",
					    autoClose: true,
					    duration: 3
					})
					return false;
				}

				var json_data = {
					action:action,
					title : title,
					description : description,
					status : status,
					navObjects : JSON.stringify(navObjects),
					navID : navID,
					deletedMenu : JSON.stringify(self.deletedMenu),
					navJSONProperties : JSON.stringify(navJSONPropertiesArray)
				}
				ui.block();
				//ajax save
				console.log('navjson',json_data,action)
				$.post("/ajax/navigation",json_data,function(data){
					try{

						data = JSON.parse(data);
						console.log('for nav',data)
						showNotification({
						    message: "Navigation has been saved!",
				            type: "success",
						    autoClose: true,
						    duration: 3
						});
						UserAccessFlagCheckBox.checkedEle = {};
						UserAccessFlagCheckBox.unCheckedEle = {};
						// self.setNavProperties(title,description,title);
						// self.setNavProperties(title,description,title,"saveNavigation",data);
						$(".updateNavigationView[data-id='"+ data['id'] +"']").trigger("click");
						$(".menu-context ul.first").remove();
						$(".menu-context").append(data['menu']);
						self.deletedMenu = [];
						$(".fl-closeDialog").click();
						unsavedEditConfirmation(false);
						console.log("nav",data.navObjects)
						ui.unblock();
						self.load(function(){
							
						});
					}catch(err){
						console.log(data)
						var message = "There's something wrong!";
						if(data=="logged out"){
							message = "You have been logged-out of the system. Please re-login.";
							window.location = "/";
						}
						showNotification({
						    message: message,
				            type: "error",
						    autoClose: true,
						    duration: 3
						});
						console.log(err)
						ui.unblock();
					}
				})
			})
		})
	},
	removeMultipleNavigation:function(){
		var self = this;
		var message = "Are you sure you want to delete this menu?";
		var eleToDelete = $('.active-secondary-element-nav');
		if(eleToDelete.length > 1){
			message = "Are you sure you want to delete the selected menu("+eleToDelete.length+")?";
		}
		var newConfirm = new jConfirm(message, 'Confirmation Dialog','', '', '', function(r){
			if(r==true){
				eleToDelete.each(function(){
					var selfEle = this;

					var switcher = true;
					var selfEleDeletedAddress = $(selfEle).closest("li.nav-menu-li").attr('address');
					// var selfWithChildren = $(selfEle).closest("li.nav-menu-li").nextAll().filter(function(){
					// 	if(switcher && (Number($(this).attr('address'))>Number(selfEleDeletedAddress))){
					// 		var menuChild_id = $(selfEle).closest("li.nav-menu-li").attr("menu_id");
					// 		if(menuChild_id!="0" && menuChild_id!="undefined"){
					// 			self.deletedMenu.push(menuChild_id);
					// 		}
					// 		return true;
					// 	}
					// 	else{
					// 		switcher = false;
					// 		return false;
					// 	}
					// });
					
					// selfWithChildren.remove();
				    $(selfEle).closest("li.nav-menu-li").remove();

				    var menu_id = $(selfEle).closest("li.nav-menu-li").attr("menu_id");
				    if(menu_id!="0" && menu_id!="undefined"){
				    	self.deletedMenu.push(menu_id);
				    }
				    if($("ul.nav-menu-ul li.nav-menu-li").length==0){
				    	$(".no-nav").removeClass("display");
				    }
				    NavDataFixes();
				    unsavedEditConfirmation(true);
				    $(".fl-widget-wrapper-scroll").perfectScrollbar("update");
					
				});	
			}
		});
	    newConfirm.themeConfirm("confirm2", {
			'icon':'<i class="fa fa-question-circle fl-margin-right" style=" font-size:15px;"></i>' 
		});
		
	},
	removeNavigation : function(){
		var self = this;
		$("body").on("click",".nav-remove",function(){
			var selfEle = this;
			var newConfirm = new jConfirm("Are you sure you want to delete this menu?", 'Confirmation Dialog','', '', '', function(r){
			
				if(r==true){
					var switcher = true;
					var selfEleDeletedAddress = $(selfEle).closest("li.nav-menu-li").attr('address');
					// var selfWithChildren = $(selfEle).closest("li.nav-menu-li").nextAll().filter(function(){
					// 	if(switcher && (Number($(this).attr('address'))>Number(selfEleDeletedAddress))){
					// 		var menu_child = $(this).closest("li.nav-menu-li").attr("menu_id");
					// 		if(menu_child!="0"){
					// 			self.deletedMenu.push(menu_child);
					// 		}
							
					// 		return true;
					// 	}
					// 	else{
					// 		switcher = false;
					// 		return false;
					// 	}
					// });
					
					// selfWithChildren.remove();
					
				    $(selfEle).closest("li.nav-menu-li").remove();
				    var menu_id = $(selfEle).closest("li.nav-menu-li").attr("menu_id");

				    if(menu_id!="0"){
				    	self.deletedMenu.push(menu_id);
				    }
				    if($("ul.nav-menu-ul li.nav-menu-li").length==0){
				    	$(".no-nav").removeClass("display");
				    }
				    NavDataFixes();
				    unsavedEditConfirmation(true);
				    $(".fl-widget-wrapper-scroll").perfectScrollbar("update");
				}
		    });
		    newConfirm.themeConfirm("confirm2", {
				'icon':'<i class="fa fa-question-circle fl-margin-right" style=" font-size:15px;"></i>' 
			});
		})
	},
	updateNavigationView : function(){
		var self = this;
		$("body").on("click",".updateNavigationView",function(){
			var data_id = $(this).attr("data-id");
			ui.block();
			$.post("/ajax/navigation",{action:"getNavigation",id:data_id},function(data){
				// try{
					data = JSON.parse(data);
					var jsonDetails = {
						"Title":data['nav_title'],
	                    "Date Created":data['date_created'],
	                    "Created By":data['created_display_name'],
	                    "Date Updated":data['date_updated'],
	                    "Updated By":data['updated_display_name'],
					}
					$(".getDetailFn").attr("json",JSON.stringify(jsonDetails));
					// console.log(data['menu']);
					$(".nav-menu-ul").html(data['menu']);
					var originalHeight = data['nav_json_data'];
					var initialHeight = 488;
					var linkContainerHeightChange = 0;
					var linkContainerHeight = 441;
					// if(originalHeight != ""){
					// 	originalHeight = JSON.parse(data['nav_json_data']) || "";
					// 	originalHeight = originalHeight[0]['height'];
					// 	originalHeight = Number(originalHeight.replace('px',""));
						// linkContainerHeightChange = originalHeight - initialHeight;
						// console.log('originalHeight',linkContainerHeight + linkContainerHeightChange);
						// $(".nav-menu-ul").closest('.link-container').closest('.ui-resizable').css('height',originalHeight);
						// $(".nav-menu-ul").closest('.link-container').css('height',linkContainerHeight + linkContainerHeightChange);
					// }else{
						// $(".nav-menu-ul").closest('.link-container').closest('.ui-resizable').css('height',linkContainerHeight + initialHeight);
						
					// }

					// $("#nav-info-title").text(data['title']);
					// $("#nav-info-desc").text(data['desc']);
					// $(".widget-nav-title").text(data['title']);
					self.setNavProperties(data['title'],data['desc'],data['title'],"saveNavigation",data_id);
					UserAccessFlagCheckBox.checkedEle = {};
					UserAccessFlagCheckBox.unCheckedEle = {};
					//
					self.setMenuIndex();
					$(".no-nav").addClass("display");
					$(".fl-widget-wrapper-scroll").perfectScrollbar("update");
				// }catch(err){
				// 	console.log(err)
				// }
				ui.unblock();
				$("#searchNextPreviousNav").val("").trigger("keyup");
			})
		})
	},
	createNewNavigation : function(){
		var self = this;
		$("#nav-createNew").click(function(){
			// var menuLength = $(".nav-menu-ul li").length;
			// if(menuLength>0){
			// 	var newConfirm = new jConfirm("Do you want to create new Navigation Settings?", 'Confirmation Dialog','', '', '', function(r){
				
			// 		if(r==true){
						self.setNavProperties("","","New Navigation","addNavigation","0");
						$("#nav-reset").trigger("click");
						self.count = 1;
						UserAccessFlagCheckBox.checkedEle = {};
						UserAccessFlagCheckBox.unCheckedEle = {};
						$(".fl-widget-wrapper-scroll").perfectScrollbar("update");
				// 	}
			 //    });
			 //    newConfirm.themeConfirm("confirm2", {
				// 	'icon':'<i class="fa fa-question-circle fl-margin-right" style="color:#f00; font-size:15px;"></i>' 
				// });
			// }
		})
	},
	resetNavigation : function(){
		$("#nav-reset").click(function(){
			$(".nav-menu-ul").html("");
			$(".no-nav").removeClass("display");
			UserAccessFlagCheckBox.checkedEle = {};
			UserAccessFlagCheckBox.unCheckedEle = {};
			unsavedEditConfirmation(true);	
		})
	},
	deleteNavigation : function(){
		var self = this;
		$("body").on("click",".deleteNavigation",function(){
			var data_id = $(this).attr("data-id");
			if(data_id==$("#nav-open-save-dialog").attr("nav-id")){
				showNotification({
				    message: "You cannot delete a navigation during edit mode.",
		            type: "warning",
				    autoClose: true,
				    duration: 3
				})
				return false;
			}
			var newConfirm = new jConfirm("Are you sure you want to delete this Navigation?", 'Confirmation Dialog','', '', '', function(r){
				
				if(r==true){
					var json_data = {
						action:"deleteNavigation",
						id:data_id
					}
					ui.block();
					$.post("/ajax/navigation",json_data,function(data){
						
						ui.unblock();
						self.load();
					})
				}
		    });
		    newConfirm.themeConfirm("confirm2", {
				'icon':'<i class="fa fa-question-circle fl-margin-right" style=" font-size:15px;"></i>' 
			});
		})
	},
	setNavProperties : function(title_info,desc_info,title,action,id){
		$("#nav-info-title").text(title_info);
		$("#nav-info-desc").text(desc_info);
		$(".widget-nav-title").text(title);
		if(action!=""){
			$("#nav-open-save-dialog").attr("action",action);
		}
		$("#nav-open-save-dialog").attr("nav-id",id);
	},

	setMenuIndex : function(){
		var countings = [];
		$(".nav-menu-ul .nav-menu-li").each(function(){
			countings.push($(this).attr("data_id"))
		})

		countings = countings.sort(function(a, b) {
			return b - a
		}); //getting how many objects created on the form and sort this variable to know what is the total countings
		this.count = parseInt(if_undefinded(countings[0],0))+1;
	},
	updateStatus : function(){
		return;
		$("body").on("click",".updateNavStatus",function(){
			var data_id = $(this).attr("data-id");
			var newConfirm = new jConfirm("Are you sure you want to delete this Navigation?", 'Confirmation Dialog','', '', '', function(r){
				
				if(r==true){
					var json_data = {
						action:"updateStatus",
						id:data_id
					}
					ui.block();
					$.post("/ajax/navigation",json_data,function(data){
						alert(data)
						ui.unblock();
						self.load();
					})
				}
		    });
		    newConfirm.themeConfirm("confirm2", {
				'icon':'<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>' 
			});
		})
	}
}
function NavDataFixes(){
	$('.nav-menu-ul').children('.nav-menu-li').each(function(){
		var self = $(this);
		var info_test = {
			"address_differential":Number(self.attr("address")) - Number(self.prev().attr("address")||-1),
			"li_element":self,
			"dis_address":Number(self.attr("address")),
			"prev_address":Number(self.prev().attr("address")||-1)
		};
		if(info_test['address_differential'] >= 2){
			// if(info_test['prev_address'] == 0){
			// 	self.removeClass('nav-menu-placeholder-'+info_test['dis_address']).addClass('nav-menu-placeholder-0');
			// 	self.attr("address", 0);
			// }else{
				var repaired_address = info_test['prev_address']+1;
				self.removeClass('nav-menu-placeholder-'+info_test['dis_address']).addClass('nav-menu-placeholder-'+repaired_address);
				self.attr("address", repaired_address);
			// }
		}
	});
}
