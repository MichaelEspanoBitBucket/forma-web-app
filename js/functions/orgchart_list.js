var jsonOrgchartData = {
    search_value: "",
    start: 0,
    limit: 10,
}
var columnSort = [];
$(document).ready(function() {
	//load
	GetOrgchartDataTable.defaultData();
	$("body").on("keyup","#txtSearchOrgchartDatatable",function(e){
	    if (e.keyCode == "13") {
	        $(".dataTable_orgchart").dataTable().fnDestroy();
	        jsonOrgchartData['search_value'] = $(this).val();
	        GetOrgchartDataTable.defaultData();
	    }
	})
	//button
	$("#btnSearchOrgchartDatatable").click(function(){
	    $(".dataTable_orgchart").dataTable().fnDestroy();
	    jsonOrgchartData['search_value'] = $("#txtSearchOrgchartDatatable").val();
	    GetOrgchartDataTable.defaultData();
	})

	//sort
	$("body").on("click",".dataTable_orgchart th",function(){
	    var cursor = $(this).css("cursor");
	    jsonOrgchartData['column-sort'] = $(this).attr("field_name");
	    if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
	        return;
	    }
	    var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

	    $(this).closest(".dataTable_orgchart").find(".sortable-image").html("");
	    if(indexcolumnSort==-1){
	        columnSort.push($(this).attr("field_name"))
	        $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
	        jsonOrgchartData['column-sort-type'] = "ASC";
	    }else{
	        columnSort.splice(indexcolumnSort,1);
	        $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
	        jsonOrgchartData['column-sort-type'] = "DESC";
	    }
	    
	    $(".dataTable_orgchart").dataTable().fnDestroy();
	    var self = this;
	    GetOrgchartDataTable.defaultData(function(){
	         addIndexClassOnSort(self);
	    });
	})

	//show entries
	$(".searchOrgchartLimitPerPage").change(function(e) {
	    // if (e.keyCode == "13") {
	        var val = parseInt($(this).val());
	        jsonOrgchartData['limit'] = val;
	        $(".dataTable_orgchart").dataTable().fnDestroy();
	        // var self = this;
	        var oTable = GetOrgchartDataTable.defaultData();

	        
	    // }
	})
})
GetOrgchartDataTable = {
    "defaultData" : function(callback){
        var oColReorder = {
            allowReorder : false,
            allowResize : true
        };
        var oTable = $(".dataTable_orgchart").dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder" : oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/search",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "action", "value": "getOrgchartDataTable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "limit", "value": jsonOrgchartData['limit']});
                aoData.push({"name": "search_value", "value": $.trim(jsonOrgchartData['search_value'])});
                aoData.push({"name": "column-sort", "value": jsonOrgchartData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonOrgchartData['column-sort-type']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
                // $(".tip").tooltip();

                var obj = '.dataTable_orgchart';
                var listOfappContainer = $('.fl-list-of-app-record-wrapper');
                dataTable_widget(obj, listOfappContainer);
                dataTableSetwidth(this.find('[field_name="status"]'), 130)
                
            },
            fnDrawCallback : function(){
                // $(".tip").tooltip();
                set_dataTable_th_width([1])
                setDatatableTooltip(".dataTable_widget");
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonOrgchartData['limit']),
        });
        return oTable;
    }
}