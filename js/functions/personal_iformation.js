$(document).ready(function(){
    
    user_personal_info.editPersonal_info("#edit_personalInfo");
    user_personal_info.save_personalInfo("#save_personalInfo");
    user_personal_info.cancel_personalInfo("#cancel_personalInfo");
    user_personal_info.updateUsername(".updateUsername");
    user_personal_info.updatePassword(".updatePassword");
    user_personal_info.user_configuration();
});


user_personal_info = {
    
    user_configuration : function(){
	$("body").on("click",".save_set_display",function(){
	    var set_display_name = $("input:radio[name=set_display_name]:checked").val();
	    var type = $(this).attr("data-type");
	    var action = "personal_config";
	    
	    $.ajax({
		type : "POST",
		url  : "/ajax/user_info",
		data : {action:action,type:type,set_display_name:set_display_name},
		success : function(e){
		    showNotification({
			    message: e,
			    type: "success",
			    autoClose: true,
			    duration: 3
		    });
		}
	    })
	})
    },
    
    editPersonal_info : function(elements){
        $("body").on("click",elements,function(){
            var dataID = $(this).attr("data-id");
            var actionType = $(this).attr("data-action-type");
            
            if (actionType=="personal_info") {
                $(".uInfo_" + dataID).hide();
                $(".eInfo_" + dataID).show();
            }else if (actionType=="company_info") {
                $(".uCInfo_" + dataID).hide();
                $(".eCInfo_" + dataID).show();
            }
                
        });
    },
    
    save_personalInfo : function(elements){
        $("body").on("click",elements,function(){
            var dataID = $(this).attr("data-id");
            var actionType = $(this).attr("data-action-type");
            var fname = $(".efInfo_" + dataID).val();
            var lname = $(".elInfo_" + dataID).val();
            
                
                // Personal Info
                if (actionType=="personal_info") {
					// if ( $.trim($(".efInfo_" + dataID).val()) == "" ||  $.trim($(".elInfo_" + dataID).val()) == "") {
						// showNotification({
							// message: "First name and Last name are mandatory fields.",
							// type: "error",
							// autoClose: true,
							// duration: 3
						// });
					// }else{
						var data = $(".personal_info").serialize() + "&action=edit_personal_info" + "&dataID=" + dataID;
						$.ajax({
							type : "POST",
							url  : "/ajax/user_info",
							data : data,
							cache : false,
							dataType : "json",
							success : function(result){
								showNotification({
									message: result.msg,
									type: "success",
									autoClose: true,
									duration: 3
								});
								
								//$("#uInfo_displayName" + dataID).html(result.display_name);
								// $("#uInfo_firstName" + dataID).html(result.first_name);
								// $("#uInfo_middleName" + dataID).html(result.middle_name);
								// $("#uInfo_lastName" + dataID).html(result.last_name);
								$("#uInfo_contactNumber" + dataID).html(result.contact_number);
								
								$(".uInfo_" + dataID).show();
								$(".eInfo_" + dataID).hide();
							}
						});
					// }
                // Company Info
                }else if (actionType=="company_info") {
		    if ( $.trim($('[name="companyName"]').val()) == "") {
			showNotification({
				message: "Company Name is mandatory fields.",
				type: "error",
				autoClose: true,
				duration: 3
			});
		    }else if ( $.trim($('[name="companyCode"]').val()) == "") {
            showNotification({
                message: "Company Code is mandatory fields.",
                type: "error",
                autoClose: true,
                duration: 3
            });
            }else if ( $.trim($('[name="companyNumber"]').val()) == "") {
            showNotification({
                message: "Company Number is mandatory fields.",
                type: "error",
                autoClose: true,
                duration: 3
            });
            }else {
			var data = $(".companyPersonal_info").serialize() + "&action=edit_company_info" + "&dataID=" + dataID;
			    $.ajax({
				type : "POST",
				url  : "/ajax/user_info",
				data : data,
				cache : false,
				dataType : "json",
				success : function(result){
				    showNotification({
					message: result.msg,
					type: "success",
					autoClose: true,
					duration: 3
				    });
				    
				    $("#uCInfo_companyName" + dataID).html(result.companyName);
				    $("#uCInfo_companyCode" + dataID).html(result.companyCode);
				    $("#uCInfo_companyNumber" + dataID).html(result.companyNumber);
				    
				    $(".uCInfo_" + dataID).show();
				    $(".eCInfo_" + dataID).hide();
				}
			    });
		    }
                    
                }
            
        });
    },
    
    cancel_personalInfo : function(elements){
        $("body").on("click",elements,function(){
            var dataID = $(this).attr("data-id");
            var actionType = $(this).attr("data-action-type");
            
            if (actionType=="personal_info") {
                $(".uInfo_" + dataID).show();
                $(".eInfo_" + dataID).hide();
            }else if (actionType=="company_info") {
                $(".uCInfo_" + dataID).show();
                $(".eCInfo_" + dataID).hide();
            }
            
        });
    },
    
    // Update Username  of the user
    updateUsername : function(elements){
       
        $("body").on("click",elements, function(){
            newUsername = $(".newUsername").val();
            oldUsername = $(".oldUsername").val();
            action = "editUsername";
            if (newUsername == oldUsername) {
                showNotification({
                    message: "Your old email and new email is the same.",
                    type: "error",
                    autoClose: true,
                    duration: 2
                });
            }else if ($.trim(newUsername) =="") {
                showNotification({
                    message: "Input your new Email as your new Username.",
                    type: "error",
                    autoClose: true,
                    duration: 2
                });
            }else{
                $.ajax({
                    type : "POST",
                    url  : "/ajax/user_info",
                    data : {action:action,newUsername:newUsername,oldUsername:oldUsername},
                    cache : false,
                    success : function(e){
                        if(e=="Input your new Email as your new Username."){
                            showNotification({
                                message: e,
                                type: "error",
                                autoClose: true,
                                duration: 2
                             });
                        }else if(e=="Your New Email was Successfully Updated."){
                                showNotification({
                                    message: e,
                                    type: "success",
                                    autoClose: true,
                                    duration: 3
                                 });
                                $(".oldUsername").val(newUsername);
                                $(".newUsername").val(null);
                        }else if (e=="Please type your correct email format.") {
                            showNotification({
                                message: e,
                                type: "error",
                                autoClose: true,
                                duration: 2
                             });
                        }else{
                            showNotification({
                                message: e,
                                type: "error",
                                autoClose: true,
                                duration: 2
                             });
                        }
                    }
                });
            }
            
        });
    },
    
    // Update Password of the user
    updatePassword : function(elements){
        $("body").on("click",elements, function(){
            oldPassword = $("#oldPassword").val();
            newPassword = $("#newPassword").val();
            retypePassword = $("#retypePassword").val();
            action = "newPassword";
	    

            if($.trim(oldPassword) =="" || $.trim(newPassword) ==""|| $.trim(retypePassword) ==""){
                showNotification({
                    message: "Please fill those required fields.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
            }else{
                if(oldPassword == newPassword){
                showNotification({
                    message: "You may not use your old password as your new password.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                }else if(newPassword!=retypePassword){
                    showNotification({
                        message: "Your new and retyped password do not match.",
                        type: "error",
                        autoClose: true,
                        duration: 3
                    });
                }else{
                    $.ajax({
                        type : "POST",
                        url  : "/ajax/user_info",
                        data : {action:action,newPassword:newPassword,oldPassword:oldPassword},
                        cache : false,
                        success : function(e){
                            if(e=="no"){
                                showNotification({
                                    message: "Your Old password does'nt match in our records.",
                                    type: "error",
                                    autoClose: true,
                                    duration: 3
                                });
                            }else{
                                showNotification({
                                    message: "Your password was successfully changed.",
                                    type: "success",
                                    autoClose: true,
                                    duration: 3
                                });
                                $("#oldPassword").val(null);
                                $("#newPassword").val(null);
                                $("#retypePassword").val(null);
                                 
                            }
                        }
                    });
                }
            }    
        });
    }
}