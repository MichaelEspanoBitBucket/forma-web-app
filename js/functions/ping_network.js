var LongPollCheckNetwork = {
    "delayed_status": "",
    "CheckMe": function () {
        setTimeout(function () {
            return;
            $.ajax({
                timeout: 30000,
                type: "GET",
                url: "/modules/data/ping_network",
                data: {},
                success: function (data) {
                    if (data == "200") {
                        if (LongPollCheckNetwork.delayed_status == "failed") {
                            if ($.xhrPool.length >= 1 && $.pauseAllXHR == true) {
                                $.xhrpContinueAll();
                            } else if ($.pauseAllXHR == true) {
                                $.pauseAllXHR = false;
                            }
                            if ($.type(window["ui"]) != "undefined") {
                                if ($('.submit-overlay').length >= 1) {
                                    ui.unblock();
                                }
                            }
                        }
                        LongPollCheckNetwork.delayed_status = "";
                    }
                    LongPollCheckNetwork.CheckMe();
                }
            }).fail(function (xhr, fail_status, specific_status) {
                if (specific_status == "") {
                    if (LongPollCheckNetwork.delayed_status == "") {
                        showNotification({
                            message: "Network connection lost. Please stand by...",
                            type: "error",
                            autoClose: true,
                            duration: 5
                        });
                        if ($.type(window["ui"]) != "undefined") {
                            if ($('.submit-overlay').length <= 0) {
                                ui.block();
                            }
                        }
                        setTimeout(function () {
                            ui.unblock();
                        }, 5000);
                        if ($.pauseAllXHR == false) {
                            $.xhrpPauseAll();
                        }
                    }
                    LongPollCheckNetwork.delayed_status = "failed";
                } else if (specific_status == "Not Found") {
                    ui.unblock();
                }
                LongPollCheckNetwork.CheckMe();
            });
        }, 1000);
    }
};
LongPollCheckNetwork.CheckMe(); 