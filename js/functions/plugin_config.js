/*legend: 0 == disable
		1 == enable
*/
var ide_config = "";
var context_menu_config = "";
var embed_inline_add = "";

$(document).ready(function(){
	var global_pathname = window.location.pathname;
	/*Global binding*/

	// $(document).on('mouseup',function(){
	// 	$('.resizeableTooltip-s').removeClass('resizeableTooltip-s').empty();
	// 	$('.resizeableTooltip-e').removeClass('resizeableTooltip-e').empty();
	// });
	$(window).on("unload", function(e) { 
	    
	    localStorage['variableToStore'] = "";
	    localStorage['countToStore'] = "";
	    localStorage['lastActionToStore'] = "";
	    unsavedEditConfirmation();

	});

	$(window).on('beforeunload',function(e){
		if(pathname = "/user_view/nav_settings" && localStorage['unsavedEdits'] == 1){
			var message = "You will lose any unsaved edits.";
			return message;

		}
	});

	ide_config = $('#enable_ide').html();
	context_menu_config = $('#enable_context_menu').html();
	embed_inline_add = $('#enable_inline_add_embed').html();
		/*live clicking plugin*/
	if(ide_config == 1){

		$('body').on("focus","[data-ide-properties-type]",function(){
			$(this).attr("readonly",true);
		});
		$('body').on('click','[data-ide-properties-type]',function(){
	   		var IdeID = $(this);
	            if($(".idecontent").length<1){
	                startIde(IdeID);
	            }
	            else{
	     		}
	           
		});

		$('body').on('mouseenter','[data-ide-properties-type]',function(){
	   		var self = $(this);
	   		// self.attr("data-temp-value",self.val());
	   		// self.val("Click to here to edit formula.");
	   		self.addClass('ide-hover');
	   		self.attr("placeholder","Click here to edit formula.");
		});
		$('body').on('mouseleave','[data-ide-properties-type]',function(){
	   		var self = $(this);
	   		// self.val(self.attr('data-temp-value'));
	   		self.attr("placeholder","");
		});	

	}
	if(context_menu_config == 1){
		var context_settings = formbuilder_context_menu;

		if($(document).find('.formbuilder_ws').length > 0){

			context_settings = formbuilder_context_menu;
		}
		if(embed_inline_add == 1){
			if($(document).find('.loaded_form_content').length > 0)
			{
				$('.embed-view-container').attr('oncontextmenu',"return false;");
				context_settings = embed_context_menu;

			}
		}
		
		$(document).contextmenu(context_settings);
	
	}

	
});