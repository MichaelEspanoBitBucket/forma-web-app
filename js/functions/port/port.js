$(document).ready(function(){
   getPortYard();
   
   
   $("body").on("click",".getSlotInfo",function(){
      var slot_id = $(this).attr("slot-id");
      
      $.post("/ajax/port",{"action":"getContainers","slot_id":slot_id},function(data){
         var ret = '<h3 class="fl-margin-bottom"><i class="fa fa-truck"></i> Slot</h3>';
         ret += '<div class="hr"></div>';
         ret += '</div>';
         ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 100px;overflow: hidden;" class="ps-container">'
            ret += '<div class="content-dialog sub-container-comment requestComment" style="width: 95%;height: auto;padding: 10px;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;font-size: 14px;line-height: 20px;color: #333333;">';
               ret += data;
            ret += '</div>';
         ret += '</div>';
         ret += '<div class="fields">';
         ret += '<div class="label_basic"></div>';
         ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
         //ret += ' <input type="button" class="btn-blueBtn fl-margin-right" id="" value="Print" style="margin-left:5px; margin-bottom:5px;">';
         ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
         ret += '</div>';
         ret += '</div>';
         var newDialog = new jDialog(ret, "", "600", "", "", function() {

         });
         newDialog.themeDialog("modal2");
         $("#content-dialog-scroll").css("height","300px");
      })
   })
   
   
   $(".allowLegend").change(function(){
      var checked = $(this).prop("checked");
      if (checked==true) {
         $(".legend-div").fadeIn();
      }else{
         $(".legend-div").fadeOut();
      }
   })
   
   $(".legend-div").draggable({
      handle:".legend-div-holder"
   });
})

function getPortYard() {
   $.post("/ajax/port",{"action":"getAvailableSlot"},function(data){
      $(".port-container").append(data);
      $(".start_tag, .configTag").remove();
      
   })
}