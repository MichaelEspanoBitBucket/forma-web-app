var announcement_posted_ui = {};
var getSCrollTop_ui = {};
var getSCrollTop_ui = {};
(function(){

	 $(document).ready(function(){

	 	announcementHeaderAction.init('ul.fl-ui-announcement-header-action-main');
	 	getHeightWindow.init();
	 	getSCrollTop.init();
	 	submiCommentAction.init('.fl-ui-comment-add-comment-wrapper');
        $('.fl-post-date-updated').timeago();

	 });


	announcementHeaderAction = {

	 	'init':function(container){

	 		var self = this;

	 		var mainNav =  $(container).find('li.mainHeaderAction');
	 
	 		//console.log(mainNav);

	 		mainNav.each(function(){

	 			 $(this).on({
	 			 	
	 			 	'click': function(){
	 			 	
	 			 		var caretIcon = $(this).find('#svg-icon-group-user-selection').children('use');
	 			 		var subNav = $(this).find($('.fl-ui-announcement-header-action-sub'));
	 			 		
	 			 		if ($(this).attr('header-action') == 'false') {
	 			 			
	 			 			$(this).attr('header-action', 'true');
	 			 				
	 			 				announcementHeaderAction.showNav(subNav);
	 			 			
	 			 			caretIcon.attr('xlink:href', '#svg-icon-caret-up-header-action');
	 			 		
	 			 		}else if ($(this).attr('header-action') == 'true') {

	 			 			$(this).attr('header-action', 'false');
	 			 				
	 			 				announcementHeaderAction.hideNav(subNav);

	 			 			caretIcon.attr('xlink:href', '#svg-icon-caret-down-header-action');
	 			 			

	 			 		};

	 			 	}

	 			 });
	 		});

	 	},

	 	'showNav': function(subNav){
	 		self = subNav;
	 		self.stop().fadeIn();
	 	},

	 	'hideNav': function(subNav){
	 		self = subNav;
	 		self.stop().fadeOut();
	 	}
	 };


	submiCommentAction = {
	 	
	 	'init':function(container){

	 		var self  = this;
	 		//var submitAction =  $(container).find('input[type="submit"].fl-ui-comment-btn');
	 		var fl_ui_comment_add_comment_field = $(container).find('.fl-ui-comment-add-comment-field');
	 		


	 		fl_ui_comment_add_comment_field.each(function(){


	 			$(this).focusin(function(){	 				
	 				var submitAction =  $(this).parent(container).find('input[type="submit"].fl-ui-comment-btn');
                    var commentSizeCounterContainer = $(this).parent(container).find('.fl-comment-size-counter-container');
                                        
	 				submiCommentAction.showELement(commentSizeCounterContainer);
	 				submiCommentAction.showELement(submitAction);
	 				

	 			});

	 			$(this).focusout(function(){
	 				
	 				var submitAction =  $(this).parent(container).find('input[type="submit"].fl-ui-comment-btn');

					//self.hideElement(submitAction);
	 			
	 			});		

	 		});
	 	},

	 	'hideElement':function(ele){

	 		ele.css({
	 			'display':'none'
	 		});

	 	},

	 	'showELement' : function(ele){

	 		ele.css({	
	 			'display':'block'
	 		});

	 	}
	 
	};

	getHeightWindow = {

	 	'init':function(){

	 		var self = this;
	 		var windowHeight = $(window).outerHeight() - 110; //
       		var pathname = window.location.pathname;

	 		var fl_ui_announcement_posted_column = $('.fl-ui-announcement-posted-column');
	 		var fl_ui_announcement_posted_column_header = fl_ui_announcement_posted_column.parent().find($('.fl-ui-announcement-posted-column-header'));
	 		var fl_widget_wrapper_scroll = $('#fl-ui-announcement-graph-wrapper').find('.fl-widget-wrapper-scroll');
	 		var fl_ui_app_wrapper = $('.fl-ui-announcement-writer-column-wrapper').find('#fl-ui-app-wrapper');
	 		var fl_ui_announcement_graph_wrapper = $('#fl-ui-announcement-graph-wrapper');
	 		var fl_ui_announcment_navs_wrapper = $('.fl-ui-announcment-navs-wrapper').height();
	 		var fl_ui_announment_editor_wrapper = $('.fl-ui-announment-editor-wrapper').height();
       		var fl_user_header_wrapper = $('.fl-user-header-wrapper');
       		var fl_user_wrapper = $('.fl-user-wrapper');
       		var  fl_content = $('.fl-content');
       			
       		// $(fl_ui_announcement_posted_column).perfectScrollbar().mouseenter(function(){
       		// 	$(this).perfectScrollbar('update');
       		// });

       		var urlportal = '/portal';

       		if (pathname === urlportal) {
       			//alert(urlportal)

       			//self.fixedUi(fl_user_header_wrapper, fl_user_wrapper, fl_content);

       		};
       		
       		//fl_ui_announcement_posted_column.css({'height': windowHeight});
       		//fl_widget_wrapper_scroll.css({'min-height': windowHeight - fl_ui_announment_editor_wrapper - fl_ui_announcment_navs_wrapper - 10 });
       		//fl_ui_app_wrapper.css({'height': windowHeight - 27});
       		//console.log("length", fl_ui_app_wrapper.length);
       		//console.log("DOC HEIGHT", windowHeight, "fl_ui_announcment_navs_wrapper", fl_ui_announcment_navs_wrapper, "Editor height", fl_ui_announment_editor_wrapper)
       		
       		fl_ui_announcement_posted_column.scroll(function(){
       			
       			var scrollTopvVal = $(this).scrollTop();

       			if (scrollTopvVal >= 30) {
       				fl_ui_announcement_posted_column_header.css({'box-shadow': '0 4px 10px -6px #d6d6d6'});
       				fl_ui_announcement_posted_column.css({'box-shadow': '0 4px 3px -2px rgb(231, 231, 231)'});
       			}else if (scrollTopvVal < 30) {
       				fl_ui_announcement_posted_column_header.css({'box-shadow': ''});
       				fl_ui_announcement_posted_column.css({'box-shadow': ''});
       			};

       		});

	 	},

	 	'fixedUi': function(ele_a, ele_b, ele_c){

	 		ele_a.css({

	 			'position':'relative'

	 		});

	 		ele_b.css({

	 			'top':0,
	 			'position':'relative',
	 			'min-height':'0px'

	 		});

	 		ele_c.css({

	 			'margin-bottom':0

	 		});

	 	}
	};


	var fl_ui_announcment_post_wrapper = $('#fl-ui-announcment-post-wrapper');
	var fl_ui_announcement_posted_column_header = $('.fl-ui-announcement-posted-column-header');
	
	getSCrollTop = {
	 	
	 	'init':function(container){

	 		var self = this;

	 		$(window).scroll(function(){
	 			
	 			var scrollTopvVal = $(this).scrollTop();
	 			var fl_ui_announcement_writer_column_wrapper = $('.fl-ui-announcement-writer-column-wrapper').height();
	 			var fl_ui_announcement_posted_column = $('.fl-ui-announcement-posted-column').height();

	 			// if (fl_ui_announcement_writer_column_wrapper >= fl_ui_announcement_posted_column) {
	 			
	 			// 	//$('.fl-ui-announcement-posted-column').perfectScrollbar('destroy');
	 			// 	$('.fl-ui-announcement-posted-column').css({'height': fl_ui_announcement_writer_column_wrapper - 38 });
	 			// 	//$('.fl-ui-announcement-posted-column').perfectScrollbar();

	 			// }

	 			// if ($('#fl-ui-announcment-post-wrapper:visible').length == 1) {
	 					
	 			// 	if (fl_ui_announcement_writer_column_wrapper < fl_ui_announcement_posted_column) {
	 			
		 		// 		//$('.fl-ui-announcement-posted-column').perfectScrollbar('destroy');
		 		// 		$('.fl-ui-announcement-posted-column').css({'height': fl_ui_announcement_writer_column_wrapper - 38 });
		 		// 		//$('.fl-ui-announcement-posted-column').perfectScrollbar();
	 			// 	};
	 			// };
	 			
	 			
	 		});


	 	},

	 	'elementUIAddStyleInit':function(ele_a, ele_b){
	 		
	 		elementUIAddStyle(ele_a, ele_b);

	 	},

	 	'elementUIRemoveStyleInit': function(ele_a, ele_b){

	 		elementUIRemoveStyle(ele_a, ele_b);

	 	}
	 
	};
    	
    function elementUIAddStyle(ele_a, ele_b){

    	var announcement_writer_column_wrapper_width = $('.fl-ui-announcement-writer-column-wrapper').width();

 		ele_a.css({	
 			"position": "fixed",
 			"width": announcement_writer_column_wrapper_width,
			"top": "65px"

 		});

 		ele_b.css({
 			"position":"fixed",
 			"z-index":1,
 			"width": announcement_writer_column_wrapper_width,
 			"top": "65px"
 		});
    }


    function elementUIRemoveStyle(ele_a, ele_b){
		var announcement_writer_column_wrapper_width = $('.fl-ui-announcement-writer-column-wrapper').width();
 		
 		ele_a.css({	
 			"position": "relative",
 			"width": announcement_writer_column_wrapper_width,
			"top":  "0px"

 		});

 		ele_b.css({
 			"position":"",
 			"width": "",
 			"top": ""
 		});
    }


    // public script members
    announcement_posted_ui.init = announcementHeaderAction.init;
    announcement_posted_ui.initSubmitCommentAction = submiCommentAction.init;
    getSCrollTop_ui = getSCrollTop.init;
   
})();