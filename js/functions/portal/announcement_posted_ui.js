/* global portal_moods */

var announcement_posted_ui = {};
var getSCrollTop_ui = {};
var getSCrollTop_ui = {};
var seeOthersMood = {};
(function () {

    $(document).ready(function () {

        announcementHeaderAction.init('ul.fl-ui-announcement-header-action-main');

        getSCrollTop.init();
        getHeightWindow.init();
        submiCommentAction.init('.fl-ui-comment-add-comment-wrapper');
        $('.fl-post-date-updated').timeago();
        announcementPostLikeAction.init('a.fl-action-like');
        seeOthersMood.init('.fl-action-mood-wrapper');
        
    });


    announcementHeaderAction = {
        'init': function (container) {

            var self = this;

            var mainNav = $(container).find('li.mainHeaderAction');

            //console.log(mainNav);

            mainNav.each(function () {

                $(this).on({
                    'click': function () {

                        var caretIcon = $(this).find('#svg-icon-group-user-selection').children('use');
                        var subNav = $(this).find($('.fl-ui-announcement-header-action-sub'));

                        if ($(this).attr('header-action') == 'false') {

                            $(this).attr('header-action', 'true');

                            announcementHeaderAction.showNav(subNav);

                            caretIcon.attr('xlink:href', '#svg-icon-caret-up-header-action');

                        } else if ($(this).attr('header-action') == 'true') {

                            $(this).attr('header-action', 'false');

                            announcementHeaderAction.hideNav(subNav);

                            caretIcon.attr('xlink:href', '#svg-icon-caret-down-header-action');


                        }

                    }

                });
            });

        },
        'showNav': function (subNav) {
            self = subNav;
            self.stop().fadeIn();
        },
        'hideNav': function (subNav) {
            self = subNav;
            self.stop().fadeOut();
        }
    };


    submiCommentAction = {
        'init': function (container) {

            var self = this;
            //var submitAction =  $(container).find('input[type="submit"].fl-ui-comment-btn');
            var fl_ui_comment_add_comment_field = $(container).find('.fl-ui-comment-add-comment-field');



            fl_ui_comment_add_comment_field.each(function () {


                $(this).focusin(function () {
                    var submitAction = $(this).parent(container).find('input[type="submit"].fl-ui-comment-btn');
                    var commentSizeCounterContainer = $(this).parent(container).find('.fl-comment-size-counter-container');

                    submiCommentAction.showELement(commentSizeCounterContainer);
                    submiCommentAction.showELement(submitAction);


                });

                $(this).focusout(function () {

                    var submitAction = $(this).parent(container).find('input[type="submit"].fl-ui-comment-btn');

                    //self.hideElement(submitAction);

                });

            });
        },
        'hideElement': function (ele) {

            ele.css({
                'display': 'none'
            });

        },
        'showELement': function (ele) {

            ele.css({
                'display': 'block'
            });

        }

    };

    getHeightWindow = {
        'init': function () {

            var self = this;
            var windowHeight = $(window).outerHeight() - 110; //
            var pathname = window.location.pathname;
            var posted_column_wrapper = $('.fl-ui-announcement-posted-column-wrapper').height();
            var fl_ui_announcement_posted_column = $('.fl-ui-announcement-posted-column');
            var fl_ui_announcement_posted_column_header = fl_ui_announcement_posted_column.parent().find($('.fl-ui-announcement-posted-column-header'));
            var fl_ui_announcement_graph_content_wrapper = $('.fl-ui-announcement-graph-content-wrapper')
            var fl_ui_announcement_apps_wrapper = $('.fl-ui-announcement-apps-wrapper');
            var fl_ui_announcement_graph_wrapper = $('#fl-ui-announcement-graph-wrapper');
            var fl_ui_app_content = $('.fl-ui-app-content');
            var fl_ui_app_wrapper =  $('#fl-ui-app-wrapper')

            var fl_ui_announcment_navs_wrapper = $('.fl-ui-announcment-navs-wrapper').height();
            var tab_navs_portal = $('.fl-ui-announcment-navs-wrapper > ul').find('li');
            var fl_ui_announment_editor_wrapper = $('.fl-ui-announment-editor-wrapper').height();
            var fl_option_toolbar = $('.fl-option-toolbar').height();

            var fl_user_header_wrapper = $('.fl-user-header-wrapper');
            var fl_user_wrapper = $('.fl-user-wrapper');
            var fl_content = $('.fl-content');

            var graph_wrapper_height = windowHeight + fl_ui_announcment_navs_wrapper - 7

            $(fl_ui_announcement_posted_column).perfectScrollbar({
                wheelSpeed: 1,
                wheelPropagation: false,
                minScrollbarLength: 50
                    //suppressScrollX: true
            }).mouseenter(function () {
                $(this).perfectScrollbar('update');
            });

            //console.log("announcment posted column",fl_ui_announcement_posted_column.height());

            var urlportal = '/portal';

            fl_ui_announcement_posted_column.css({
                'height': windowHeight - fl_ui_announment_editor_wrapper
            });

            // fl_ui_announcement_graph_content_wrapper.css({
            //     'height': posted_column_wrapper - fl_ui_announment_editor_wrapper
            // });


            if (pathname === urlportal) {
                //alert(urlportal)
                getHeightWindow.fixedUi(fl_user_header_wrapper, fl_user_wrapper, fl_content);
                //getHeightWindow.removeScrollResponsive(fl_ui_announcement_graph_content_wrapper);
            }

            fl_ui_announcement_posted_column.scroll(function () {

                var scrollTopvVal = $(this).scrollTop();

                if (scrollTopvVal >= 30) {
                    fl_ui_announcement_posted_column_header.css({'box-shadow': '0 4px 10px -6px #d6d6d6'});
                    fl_ui_announcement_posted_column.css({'box-shadow': '0 4px 3px -2px rgb(231, 231, 231)'});
                } else if (scrollTopvVal < 30) {
                    fl_ui_announcement_posted_column_header.css({'box-shadow': ''});
                    fl_ui_announcement_posted_column.css({'box-shadow': ''});
                }
                ;

            });

            getHeightWindow.removeScrollResponsive(fl_ui_announcement_posted_column, windowHeight, fl_ui_announment_editor_wrapper);
            getHeightWindow.removeScrollResponsive(fl_ui_announcement_graph_content_wrapper, graph_wrapper_height, "");
            tabNavsPortal();
            
        },
        'fixedUi': function (ele_a, ele_b, ele_c) {

            ele_a.css({
                'position': 'relative'

            });

            ele_b.css({
                'top': 0,
                'position': 'relative',
                'min-height': '0px'

            });

            ele_c.css({
                'margin-bottom': 0

            });

        },
        'removeScrollResponsive': function (ele, windowHeight, fl_ui_announment_editor_wrapper) {

            $(window).bind('enterBreakpoint320', function () {
                console.log('from getHeightWindow  ENTER BREAKPOINT 320');
                $(ele).perfectScrollbar('destroy');
                $(ele).css('height', 'auto');
            });


            $(window).bind('enterBreakpoint480', function () {
                console.log('from getHeightWindow  ENTER BREAKPOINT 480');
                $(ele).perfectScrollbar('destroy');
                $(ele).css('height', 'auto');
            });

            $(window).bind('exitBreakpoint480', function () {
                //$(ele).perfectScrollbar();
                //$(ele).css('height', windowHeight);

            });


            $(window).bind('enterBreakpoint768', function () {
                console.log('from getHeightWindow ENTER BREAKPOINT 768');
                $(ele).perfectScrollbar('destroy');
                $(ele).css('height', 'auto');
            });

            $(window).bind('exitBreakpoint768', function () {
                console.log('from getHeightWindow EXIT BREAKPOINT 768');
                $(ele).perfectScrollbar({
                    wheelSpeed: 1,
                    wheelPropagation: false,
                    minScrollbarLength: 50
                        //suppressScrollX: true
                });
                $(ele).css({
                    'height': windowHeight - fl_ui_announment_editor_wrapper - 5
                });
                console.log("on exit 768", windowHeight);
            });

            $(window).bind('enterBreakpoint1024', function () {
                console.log('from getHeightWindow EXIT BREAKPOINT 1024');

                $(ele).css({
                    'height': windowHeight - fl_ui_announment_editor_wrapper - 5
                });
                //$('body').css('overflow', 'hidden');
                $(ele).perfectScrollbar({
                    wheelSpeed: 1,
                    wheelPropagation: false,
                    minScrollbarLength: 50
                        //suppressScrollX: true
                });
                console.log("on enter 1024", windowHeight);
            });

            $(window).bind('exitBreakpoint1024', function () {
                $(ele).perfectScrollbar('destroy');
                $(ele).css('height', '100%');
                $('body').css('overflow', 'auto');
            });

            $(window).setBreakpoints();

        }
    };


    var fl_ui_announcment_post_wrapper = $('#fl-ui-announcment-post-wrapper');
    var fl_ui_announcement_posted_column_header = $('.fl-ui-announcement-posted-column-header');

    getSCrollTop = {
        'init': function (container) {

            var self = this;

            $(window).bind('enterBreakpoint768', function () {
                console.log('ENTER BREAKPOINT 768');
                $(window).scroll(function () {


                });

            });

            $(window).bind('exitBreakpoint768', function () {

                $(window).scroll(function () {

                    var scrollTopvVal = $(this).scrollTop();
                    var fl_ui_announcement_writer_column_wrapper = $('.fl-ui-announcement-writer-column-wrapper').height();
                    var fl_ui_announcement_posted_column = $('.fl-ui-announcement-posted-column').height();

                    if (fl_ui_announcement_writer_column_wrapper >= fl_ui_announcement_posted_column) {

                        //$('.fl-ui-announcement-posted-column').perfectScrollbar('destroy');
                        $('.fl-ui-announcement-posted-column').css({'height': fl_ui_announcement_writer_column_wrapper - 38});
                        //$('.fl-ui-announcement-posted-column').perfectScrollbar();

                    }

                    if ($('#fl-ui-announcment-post-wrapper:visible').length == 1) {

                        if (fl_ui_announcement_writer_column_wrapper < fl_ui_announcement_posted_column) {

                            //$('.fl-ui-announcement-posted-column').perfectScrollbar('destroy');
                            $('.fl-ui-announcement-posted-column').css({'height': fl_ui_announcement_writer_column_wrapper - 38});
                            //$('.fl-ui-announcement-posted-column').perfectScrollbar();
                        }
                        ;
                    }
                    ;

                });

            });
            $(window).setBreakpoints();


        },
        'elementUIAddStyleInit': function (ele_a, ele_b) {

            elementUIAddStyle(ele_a, ele_b);

        },
        'elementUIRemoveStyleInit': function (ele_a, ele_b) {

            elementUIRemoveStyle(ele_a, ele_b);

        }

    };

    announcementPostLikeAction = {
        'init': function (action) {
            $('body').on('click', action, function () {
                var portalLikeIcon = $(this).find('#svg-icon-like-post').children('use');

                if ($(this).attr('portal-like-action') == "false") {

                    $(this).attr('portal-like-action', "true");

                    portalLikeIcon.attr('xlink:href', '#svg-icon-dislike-post');

                } else if ($(this).attr('portal-like-action') == "true") {

                    $(this).attr('portal-like-action', "false");

                    portalLikeIcon.attr('xlink:href', '#svg-icon-like-post');
                }
            });
        }

    };

    seeOthersMood = {
        'init': function (container) {

            var btnShowOthersMood = $(container).find('li.fl-others-mood-inner');

            $(btnShowOthersMood).each(function () {
                $(this).unbind('click');
                $(this).on('click', function () {

                    var postId = $(this).data('post-id');
                    var commentId = $(this).data('comment-id');

//                    var userMoods = $($('#fl-user-moods').html().trim().replace(/(\r\n|\n|\r)/gm, ''));

                    var ret = '<h3 class="fl-margin-bottom">';
                    ret += '<svg class="icon-svg icon-modal-svg" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-mood-default"></use></svg> See how other people feel about this';
                    ret += '</h3>';
                    ret += '<div class="hr"></div>';
                    ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
                    ret += '<div class="content-dialog fl-show-how-other-feel-wrapper" style="height: auto;">';
                    ret += '<div class="fl-others-mood-inner fl-action-mood-inner">';
                    ret += '<ul class="fl-others-mood-wrapper fl-action-mood-options-wrapper">';
                    ret += '<div class="body"></div>';
                    ret += '</ul>';
                    ret += '</div>';
                    ret += '</div>';

                    var newDialog = new jDialog(ret, "", "", "600", "", function () {


                    });
                    newDialog.themeDialog("modal2");

                    $('.content-dialog-scroll').perfectScrollbar({
                        wheelSpeed: 1,
                        wheelPropagation: false,
                        minScrollbarLength: 50
                            //suppressScrollX: true
                    }).mouseenter(function () {
                        $(this).perfectScrollbar('update');
                    });

                    portal_moods.api.getUserMoods(postId, commentId,
                        function (userMoods) {
                            $("#popup_message").find('.body').html(buildUserMoodsUI(userMoods));
                            $('#fl-mood-tab-wrapper').accordion();

                        },
                        function (errorMessage) {
                            $("#popup_message").find('.body').html("Error: " + errorMessage);
                            $('#fl-mood-tab-wrapper').accordion();

                        });

                    $("#popup_message").find('.body').html('<i class="fa fa-refresh fa-spin"></i>');

                });

            });

        }

    };

    function buildUserMoodsUI(userMoods, moodIcons) {

        var sortedMoods = {
            Happy: [],
            Inspired: [],
            Sad: [],
            Angry: [],
            Annoyed: [],
            DontCare: []
        };

        // sort users first
        for (var i in userMoods) {
            var mood = userMoods[i]['mood'];
            if (mood == "Don't Care") {
                mood = 'DontCare';
            }
            sortedMoods[mood].push(userMoods[i]);
        }



        // determine the size of the largest number of people who voted for the same mood
        var largestSameMoodSize = 0;
        for (var key in sortedMoods) {
            largestSameMoodSize = largestSameMoodSize < sortedMoods[key].length ? sortedMoods[key].length : largestSameMoodSize;
        }

        var moodIcons = [
            'svg-icon-mood-happy-v2', //svg-icon-mood-happy
            'svg-icon-mood-inspired-v2', //svg-icon-mood-inspired
            'svg-icon-mood-sad-v2', //svg-icon-mood-sad
            'svg-icon-mood-angry-v2', //svg-icon-mood-angry
            'svg-icon-mood-annoyed-v2', //svg-icon-mood-annoyed
            'svg-icon-mood-dont-care-v2' //svg-icon-mood-dont-care
        ];

        // build the html
        var html = '<ul class="fl-others-mood-wrapper">';

        html += '<li class="fl-action-mood section clearing">';
        var i = 0;
        for (var key in sortedMoods) {
            html += '<div class="column div_1_of_6">';
            html += '<svg class="icon-svg icon-svg-announcement ' + moodIcons[i] + ' tip" viewBox="0 0 100.264 100.597" data-original-title="" title="" style="float: left;"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' + moodIcons[i] + '"></use></svg>';

            if (sortedMoods[key].length > 0) {
                html += '<span style="position:relative; top:5px;">';
                if (key == 'DontCare') {
                    html += "Don't Care (" + sortedMoods[key].length + ")";
                } else {
                    html += key + " (" + sortedMoods[key].length + ")";
                }
                html += '</span>';
            } else {
                html += '<span style="position:relative; top:5px;">';
                if (key == 'DontCare') {
                    html += "Don't Care";
                } else {
                    html += key;
                }
                html += '</span>';
            }

            html += "</div>";
            i++;
        }
        html += "</li>";

        for (var i = 0; i < largestSameMoodSize; i++) {
            html += '<li class="fl-action-mood section clearing">';
            for (var key in sortedMoods) {
                html += '<div class="column div_1_of_6">';
                if (sortedMoods[key][i]) {
                    html += '<span data-user-id="' + sortedMoods[key][i]['user_id'] + '">';
                    html += sortedMoods[key][i]['display_name'];
                    html += '</span>';
                } else {
//                    html += '<span>...</span>';
                }
                html += "</div>";
            }
            html += "</li>";
        }
        html += "</ul>";

//       return html;
        //Tab moods
        var tabhtml = "<div id='fl-mood-tab-wrapper'>";
        var iconIndex = 0;

        if (largestSameMoodSize > 0) {
            for (var key in sortedMoods) {
                if (sortedMoods[key].length > 0) {
                    tabhtml += '<h3>';
                    tabhtml += '<svg class="icon-svg icon-svg-announcement ' + moodIcons[iconIndex] + ' tip" viewBox="0 0 100.264 100.597" data-original-title="" title="" style="margin-left:4px;"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' + moodIcons[iconIndex] + '"></use></svg>';

                    if (sortedMoods[key].length > 0) {
                        tabhtml += '<span style="position: absolute; left: 25px; top: 6px;">';
                        if (key == 'DontCare') {
                            tabhtml += "Don't Care (" + sortedMoods[key].length + ")";
                        } else {
                            tabhtml += key + " (" + sortedMoods[key].length + ")";
                        }
                        tabhtml += '</span>';
                    } else {
                        tabhtml += '<span style="position: absolute; left: 25px; top: 6px;">';
                        if (key == 'DontCare') {
                            tabhtml += "Don't Care";
                        } else {
                            tabhtml += key;
                        }
                        tabhtml += '</span>';
                    }
                    tabhtml += "</h3>";
                    tabhtml += "<div class='appendlistmoods' style='padding:5px;'>";

                    for (var i = 0; i < sortedMoods[key].length; i++) {
                        tabhtml += '<strong data-user-id="' + sortedMoods[key][i]['user_id'] + '" class="" style="padding:3px;padding-left:20px;display:inline-block;">';
                        tabhtml += sortedMoods[key][i]['display_name'];
                        tabhtml += '</strong>';
                        tabhtml += '<br/>';
                    }

                    tabhtml += "</div>";
                }
                iconIndex++;
            }
        } else {
            tabhtml += "No reactions yet";
        }
        tabhtml += "</div>";
        return tabhtml;

    }



    function elementUIAddStyle(ele_a, ele_b) {

        var announcement_writer_column_wrapper_width = $('.fl-ui-announcement-writer-column-wrapper').width();

        ele_a.css({
            "position": "fixed",
            "width": announcement_writer_column_wrapper_width,
            "top": "65px"

        });

        ele_b.css({
            "position": "fixed",
            "z-index": 1,
            "width": announcement_writer_column_wrapper_width,
            "top": "65px"
        });
    }


    function elementUIRemoveStyle(ele_a, ele_b) {
        var announcement_writer_column_wrapper_width = $('.fl-ui-announcement-writer-column-wrapper').width();

        ele_a.css({
            "position": "relative",
            "width": announcement_writer_column_wrapper_width,
            "top": "0px"

        });

        ele_b.css({
            "position": "",
            "width": "",
            "top": ""
        });
    }

    function tabNavsPortal() {


        var tabNavsPortalBtn = $('.fl-ui-announcment-navs-wrapper ul li');
        var fl_ui_app_wrapper = $('#fl-ui-app-wrapper');
        var fl_ui_announcement_online_users_content = $('#fl-ui-announcement-online-users-content');
        var fl_ui_announcement_posted_column = $('.fl-ui-announcement-posted-column').height();

        fl_ui_announcement_online_users_content.perfectScrollbar({
            wheelSpeed: 1,
            wheelPropagation: false,
            minScrollbarLength: 50
                //suppressScrollX: true
        });
        $(fl_ui_announcement_online_users_content).perfectScrollbar({
            wheelSpeed: 1,
            wheelPropagation: false,
            minScrollbarLength: 50
                //suppressScrollX: true
        }).mouseenter(function () {
            $(this).perfectScrollbar('update');
        });



        if ($('#fl-ui-app-wrapper').length >= 1) {

            fl_ui_app_wrapper.css({
                'height': fl_ui_announcement_posted_column + 70

            });
            console.log(fl_ui_app_wrapper.height());

        }

        if ($('#fl-ui-announcement-online-users-content').length >= 1) {

            fl_ui_announcement_online_users_content.css({
                'height': fl_ui_announcement_posted_column + 125
            });
            console.log(fl_ui_announcement_online_users_content.height());

        }

    }


    // public script members
    announcement_posted_ui.init = announcementHeaderAction.init;
    announcement_posted_ui.tabNavsPortal = tabNavsPortal;
    announcement_posted_ui.initSubmitCommentAction = submiCommentAction.init;
    announcement_posted_ui.adjustPostListHeight = getHeightWindow.init;
    getSCrollTop_ui = getSCrollTop.init;

})();