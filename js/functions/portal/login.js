/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var portal_login = {};

(function () {

    $(document).ready(function () {

        $("#form-login").submit(function (event) {

            var email = $("#email").val();
            var password = $("#password").val();

            portal_login.login(email, password);
            event.preventDefault();
        });

    });

    portal_login.login = function (email, password) {

        var url = "/API/login";
        var params = {
            email: email,
            password: password
        };

        $.post(url, params, function (response) {
            console.log(response);

            if (response.status === "SUCCESS") {
                window.location.href = getRedirectURL();
            } else {
                alert(response.error_message);
            }

        });

    };

    function getRedirectURL() {
        return $("#form-login").attr("data-redirect-url");
    }

})();