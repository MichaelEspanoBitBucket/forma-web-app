
/* global browser_utilities, Handlebars, portal_request_list */

// namespace main
var main = {};
var portalActive = true;

(function () {

    //  templates
    var portalTabTemplate;
    var portalFormCreationTemplate;
    var portalLoadingFormCreationTemplate;

    //  states
    var currentlySelectedTabTarget;
    var previouslySelectedTabTarget;

    //pathname
    var pathname = window.location.pathname;

    main.minimum_supported_browser_versions = {
        Chrome: 40
    };

    $(document).ready(function () {
        portal_post_list.loadPosts(portal_post_list.FilterType.ALL_POSTS,0,portal_post_list.DEFAULT_FETCH_COUNT,function(){
            // alert(123)
            checkBrowserCompatibility();
            initializeLeftPanelTabs();
        })
    });

    $(document).bind("EVENT_DOCUMENT_CREATED_FROM_IFRAME", function (e) {
        var documentData = e.documentData;
        closeTab("form-" + documentData.formId, true);
        main.showTab("form-selection");

        if (portal_post_list.getSelectedFilterFormId() == documentData.formId) {
            portal_request_list.refreshRequests(documentData.formId);
        }

    });

    $(document).bind("EVENT_DOCUMENT_CLOSED_FROM_IFRAME", function (e) {
        clearClosableTabs();
        main.showTab("form-selection");
    });

    main.addFormCreationTab = function (formId, formName) {
        var tab = {
            id: "fl-action-show-form-" + formId,
            target: "form-" + formId,
            title: formName,
            is_closable: true
        };

        // lazy load portal tab template
        if (!portalTabTemplate) {
            portalTabTemplate = Handlebars.compile($("#fl-portal-tab-template").html());
        }

        clearClosableTabs();
        $(".fl-ui-announcment-navs-wrapper ul").append(portalTabTemplate(tab));
        main.showTab(tab.target);

        var formURL = "user_view/workspace?view_type=request&formID=" + formId + "&requestID=0";
//        var formURL = "portal/tester";
        createSection(tab.target);
        displayFormCreation(tab.target, {
            id: formId,
            name: formName,
            create_request_link: formURL
        });
    };

    main.getCurrentlyOpenTabTargets = function () {

        var targets = [];

        $(".fl-portal-tab").each(function () {
            targets.push($(this).attr("data-tab-target-id"));
        });

        return targets;

    };

    main.showTab = function (target) {
        $(".fl-portal-tab").removeClass("fl-tab-selected");
        $(".fl-portal-tab[data-tab-target-id='" + target + "']").addClass("fl-tab-selected");

        $(".fl-portal-section").addClass("isDisplayNone");
        $('.fl-portal-section[data-section-id="' + target + '"]').removeClass("isDisplayNone");


        if (currentlySelectedTabTarget != target) {
            previouslySelectedTabTarget = currentlySelectedTabTarget;
        }
        currentlySelectedTabTarget = target;
    };

    function checkBrowserCompatibility() {

        var browserVersion = browser_utilities.getBrowserVersion();

        if (main.minimum_supported_browser_versions.hasOwnProperty(browserVersion.browser)) {
            var minimumVersion = main.minimum_supported_browser_versions[browserVersion.browser];
            if (browserVersion.version < minimumVersion) {
                var msg = "This version of " + browserVersion.browser + " is unsupported, please update to the latest version to view this page. You will be redirected.";
                $.alerts.alert(msg, "", "500", "", "", function () {
                    window.location = "/user_view/announcements";
                });
            }
        }

    }

    function initializeLeftPanelTabs() {

        bindTabEvents();

        var tabExists = $(".fl-portal-tab[data-tab-target-id='" + previouslySelectedTabTarget + "']").length > 0;
        if (previouslySelectedTabTarget && tabExists) {
            main.showTab(previouslySelectedTabTarget);
        } else {
            main.showTab("composer");
        }

    }

    function bindTabEvents() {
        $(".fl-portal-tab").unbind("click");
        $(".fl-portal-tab").click(function () {
            var target = $(this).attr("data-tab-target-id");
            main.showTab(target);
            // updateHeightTabs('#fl-ui-app-wrapper');
            // updateHeightTabs('#fl-ui-announcement-online-users-content');
            
        });

        $(".fl-portal-tab-action-close").unbind("click");
        $(".fl-portal-tab-action-close").click(function () {
            var target = $(this).parent().attr("data-tab-target-id");
            closeTab(target);
        });

    }

    function createSection(target) {
        $(".fl-ui-announcement-writer-column-wrapper").append(
            '<div class="fl-portal-section" data-section-id="' + target + '" data-closable="true"></div>'
            );

        bindTabEvents();
    }

    function displayFormCreation(target, form) {
        // lazy load portal form creation template
        if (!portalFormCreationTemplate) {
            portalFormCreationTemplate = Handlebars.compile($("#fl-portal-form-creation-template").html());

        }

        $(".fl-portal-section[data-section-id='" + target + "']").append(portalFormCreationTemplate(form));
         

//        $(".fl-portal-section[data-section-id='" + target + "']").load(form.create_request_link);
//        return;
        $("#request_form_" + form.id).load(function () {
            console.log("HOHOHHOH", $('body').find('iframe').contents().find('#informuser-wrapper').length); 
            $('body').find('iframe').contents().find('#informuser-wrapper').remove();
               
            $(document.getElementById("request_form_" + form.id).contentWindow.document).on("documentCreated", function () {
                alert("test");
            });

            var iframe = $("#request_form_" + form.id).contents();
            iframe.bind("documentCreated", function () {
                alert("test");
            });

            var ui_portal_fl_portal_section = $('.fl-portal-section').find('fl-widget-head');
            var ui_portal_fl_report_wrapper_content = $('.fl-portal-section').find('.fl-report-wrapper-content').addClass('ui_portal_fl_report_wrapper_content');
            var ui_portal_body = iframe.find("body").addClass("ui-portal-body");
            var ui_portal_workspace_option_tab = $(ui_portal_body).find('.workspace_option_tab').addClass('ui_portal_workspace_option_tab');
            var ui_portal_loaded_form_content = $(ui_portal_body).find('.loaded_form_content').addClass('ui_portal_loaded_form_content');
            var ui_portal_fl_user_wrapper = $(ui_portal_body).find('.fl-user-wrapper').addClass('ui_portal_fl_user_wrapper');
            var ui_portal_fl_workspace_control_container = $(ui_portal_body).find('.fl-workspace-control-container').addClass('ui_portal_fl_workspace_control_container')
            var ui_portal_fl_widget_head = $(ui_portal_body).find('.fl-widget-head').addClass('ui_portal_fl_widget_head');

            adjustFormSizing(ui_portal_body, form.id);

            ui_portal_workspace_option_tab.remove();
            ui_portal_fl_portal_section.remove();
            $('#ui-portal-loading').remove();
        });

        createLoadingELement(target);

        $('.fl-portal-section').find($('.fl-home-object').children('.fl-widget-head')).remove();
        $('.fl-portal-section').find($('.fl-user-header-wrapper')).remove();
    }

    function clearClosableTabs() {
        $(".fl-portal-tab[data-closable='true']").remove();
        $(".fl-portal-section[data-closable='true']").remove();
    }

    function closeTab(target, noConfirm) {
        var message = "Are you sure you want to close this tab?";

        if (!(noConfirm && noConfirm == true)) {
            var newConfirm = new jConfirm(message, 'Confirmation Dialog', '', '', '', function (response) {
                if (response == true) {
                    if (target === currentlySelectedTabTarget) {
                        // reset
                        initializeLeftPanelTabs();
                    }

                    $(".fl-portal-tab[data-tab-target-id='" + target + "']").remove();
                    $(".fl-portal-section[data-section-id='" + target + "']").remove();
                }
            });
            newConfirm.themeConfirm("confirm2", {'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});
        } else {
            if (target === currentlySelectedTabTarget) {
                // reset
                initializeLeftPanelTabs();
            }

            $(".fl-portal-tab[data-tab-target-id='" + target + "']").remove();
            $(".fl-portal-section[data-section-id='" + target + "']").remove();
        }
    }

    function createLoadingELement(target) {

        if (!portalLoadingFormCreationTemplate) {
            portalLoadingFormCreationTemplate = Handlebars.compile($("#fl-portal-loading-form-creation-template").html());
        }

        $(portalLoadingFormCreationTemplate()).appendTo(".fl-portal-section[data-section-id='" + target + "']").attr('data-section-id');
    }

    function adjustFormSizing(formBody, formId) {

        // prevent showing of horizontal scrollbar
        formBody.attr("style", $(this).attr("style") + ";width: 98%;");

        // TODO: adjust form height here
//        var formBodyHeight = formBody.height();
//
//        alert("setting " + formBodyHeight);
////        $(".fl-request-form-container[data-form-id='" + formId + "']").height(formBodyHeight);
//        alert("set " + formBodyHeight);

    }

    // function updateHeightTabs(elem){

    //     var fl_ui_announcement_graph_content_wrapper = $('.fl-ui-announcement-graph-content-wrapper').height();

    //     $(elem).css({
    //         height: fl_ui_announcement_graph_content_wrapper - 25
    //     });
    // }

})();
