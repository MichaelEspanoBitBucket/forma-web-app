
/* global name_selection, main, portal_survey_list, portal_post_list */

var message_board_writer = {};

(function () {

    var API_CONTEXT = "/portal_api";

    var VisibilityType = {
        PUBLIC: 1,
        COMPANY: 2,
        CUSTOM: 3
    };

    var PostType = {
        ANNOUNCEMENT: 1,
        SURVEY: 2
    };

    var UpdateMode = {
        NEW: 'new',
        UPDATE: 'update'
    };

    $(document).ready(function () {
        survey.init();
        initializeSummernoteEditor();
        fixUiAnnouncement.init();

        $("#fl-btn-post").click(function () {
            var textContents = $(".fl-ui-announcement").code();
            var visibilityType = $("#fl-select-post-visibility-type").val();
            var type = $("#fl-select-post-type").val();
            var postId = $(this).attr("post-id");

            var cleanText = "";

            //  the post may be a plain text, clean it up for validation
            if (textContents.indexOf("img") === -1) {
                cleanText = textContents.replace(/<\/?[^>]+(>|$)/g, "");
                cleanText = cleanText.replace(/&nbsp;/g, "");
            } else {
                cleanText = textContents;
            }

            //  validate text content
            if (cleanText == "" || cleanText.trim() == "") {

                showNotification({
                    message: "Post must not be empty.",
                    type: "error",
                    autoClose: true,
                    duration: 10
                });

                return;
            }

            // validate readers
            if (visibilityType == VisibilityType.CUSTOM) {
                var selectedNames = name_selection.getSelectedNames();

                if (!selectedNames || selectedNames.length <= 0) {
                    showNotification({
                        message: "Readers are required!",
                        type: "error",
                        autoClose: true,
                        duration: 10
                    });
                    return;
                }

            }

            if (type == PostType.ANNOUNCEMENT) {
                saveAnnouncement(textContents, visibilityType, postId);
            } else if (type == PostType.SURVEY) {
                survey.save(textContents, visibilityType, postId);
            } else {
                alert("Invalid post type");
            }
        });

        $("#fl-btn-clear").click(function () {
            clearPostUIData();
            setNewPostMode();
            survey.clearResponses();
            survey.addResponse();
        });

        $(".fl-widget-name-selector").css("display", "none");
        $("body").on("change", "#fl-select-post-visibility-type", function () {
            var visibilityType = $(this).val();

            // if visibility = custom, display name selector
            displayReadersSelector(visibilityType == VisibilityType.CUSTOM);

            if (visibilityType == VisibilityType.CUSTOM) {
                fixUiAnnouncement.negativeUIfix();

            } else {
                fixUiAnnouncement.positiveUIfix();

            }
        });

        $('body').on('change', '#fl-select-post-type', function () {
            if ($(this).val() == PostType.SURVEY) {
                survey.showComponents();
                fixUiAnnouncement.negativeUIfix();
            } else {
                survey.hideComponents();
                fixUiAnnouncement.positiveUIfix();
            }

            if ($(this).val() == PostType.ANNOUNCEMENT) {
                fixUiAnnouncement.negativeUIfixLefttOnly();
            }

        });

        $('body').on('click', '.submit-response', function () {
            var $post = $(this).closest('.fl-ui-announcement-posted-wrapper');
            var post_id = $post.data('id');
            var selectedResponse = $('.survey-selectable-response[name="post_' + post_id + '"]').map(function () {
                return $(this).prop('checked') ? $(this).val() : null;
            }).get();

            submitSurveyResponse(post_id, selectedResponse);

        });

        $('body').on('click', '.edit-response', function () {
            var $post = $(this).closest('.fl-ui-announcement-posted-wrapper');
            var post_id = $post.data('id');
            portal_survey_list.unLockSurvey(post_id);
        });

        $('body').on('change', '.survey-publish-publish-survey-option', function () {
            var $survey = $(this).closest('.fl-request-widget');
            var survey_result = $(this).prop('checked') ? 1 : 0;
            var chart_type = $survey.find('.survey_chart_types:first').val();

            updateSurveyPublish($(this).val(), survey_result, chart_type);
        });

        $('body').on('change', '.survey_chart_types', function () {
            var $survey = $(this).closest('.fl-request-widget');
            var survey_result = $survey.find('.survey-publish-publish-survey-option').prop('checked') ? 1 : 0;
            var chart_type = $survey.find('.survey_chart_types:first').val();
            $survey.data('chart-type', chart_type);
            updateSurveyPublish($survey.data('survey-id'), survey_result, chart_type);
            setGrap($survey.data('survey-id'))
        });


        $('body').on('input propertychange', '.ui-survey-response-input', function () {
            $(this).siblings('.response-text-counter').text($(this).val().length + "/100")
        });

        setNewPostMode();

        //  work around: remove unlink button
        $('.btn[data-event="unlink"]').css("display", "none");

    });

    function initializeSummernoteEditor() {

        $('.fl-ui-announcement').summernote({
            toolbar: [
                //[groupname, [button list]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['link', 'picture']],
                ['misc', ['codeview']]
            ],
            focus: true,
            maximumImageFileSize: 4096000,
            onChange: function () {
//                var code = $('.fl-ui-announcement').code();
//                var filteredContent = $(code).text().replace(/\s+/g, '');                
//
//                if (filteredContent.length > 0) {
//                    alert(' notempty');
//                } else {
//                    alert('empty');
//                }
            },

        });

       

        $('.fl-ui-announcement').on('summernote.image.upload.error', function () {
            alert('Maximum image size exceeded!');
        });
    }

    function saveAnnouncement(postTextContents, postVisibilityType, postId) {
        var url = API_CONTEXT + "/save_announcement";
        var params = {
            post_visibility_type: postVisibilityType,
            post_text_contents: postTextContents,
            post_type: 1,
            post_id: postId
        };

        if (postVisibilityType == VisibilityType.CUSTOM) {
            params.post_followers = name_selection.getSelectedNames();
        }

        console.log(params);

        $.post(url, params, function (response) {
            enablePostButton(true);
            console.log(response);
            
            if (response.status == "SUCCESS") {
                
                clearPostUIData();
                setNewPostMode();
                portal_post_list.refreshPosts();
                var message = "Announcement has been created!";
                if(postId!="0"){
                    message = "Announcement has been updated!";
                }

                showNotification({
                    message: message,
                    type: "success",
                    autoClose: true,
                    duration: 3
                });

            } else {
//                alert(response.error_message);
                showNotification({
                    message: response.error_message,
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
            }

        });

        enablePostButton(false);

    }

    function saveSurvey(postId, postTextContents, postVisibilityType, responses, enable_multiple_responses, publish_results) {
        var url = API_CONTEXT + "/save_survey";
        var params = {
            post_visibility_type: postVisibilityType,
            post_text_contents: postTextContents,
            post_type: PostType.SURVEY,
            post_id: postId,
            survey_responses: responses,
            enable_multiple_responses: enable_multiple_responses,
            publish_results: publish_results
        };

        if (postVisibilityType == VisibilityType.CUSTOM) {
            params.post_followers = name_selection.getSelectedNames();
        }

        //Validation

        console.log(params);

        $.post(url, params, function (response) {
            enablePostButton(true);
            console.log(response);

            if (response.status == "SUCCESS") {
                clearPostUIData();
                setNewPostMode();
                survey.clearResponses();
                survey.addResponse();
                portal_post_list.refreshPosts();
                portal_survey_list.refreshSurvey();
                // refresh view here

                showNotification({
                    message: "Survey Posted",
                    type: "information",
                    autoClose: true,
                    duration: 3
                });

            } else {
//                alert(response.error_message);
                showNotification({
                    message: response.error_message,
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
            }
        });

        enablePostButton(false);
    }

    function submitSurveyResponse(survey_id, responses, callback) {
        var $survey = $(".fl-ui-announcement-posted-wrapper[data-id='" + survey_id + "']");
        var $submitButton = $survey.find('.submit-response')

        var url = API_CONTEXT + "/save_survey_response";
        var params = {
            survey_id: survey_id,
            survey_responses: responses
        };

        console.log(params);

        if (responses.length > 0) {
            $submitButton.prop('disabled', true);
            portal_survey_list.lockSurvey(survey_id);
            //$("<i class='fa fa-spinner fa-spin survey_option_loading'></i>").insertBefore($submitButton)

            $.post(url, params, function (response) {
                // enablePostButton(true);
                //console.log(response);

                if (response.status == "SUCCESS") {
                    // portal_survey_list.refreshSurveyById(survey_id);
                    $submitButton.prop('disabled', false);
                } else {
                    // TODO: add a designed notification to the user here                    
                    $submitButton.prop('disabled', false);
                    alert(response.error_message);
                }
            });
        }
        // enablePostButton(false);
    }

    function updateSurveyPublish(survey_id, publish_results, chart_type) {
        portal_survey_list.publishSurveyLoad(survey_id);
        var url = API_CONTEXT + "/update_survey_publish";
        var params = {
            survey_id: survey_id,
            publish_results: publish_results,
            chart_type: chart_type
        };



        console.log(params);



        $.post(url, params, function (response) {

            if (response.status == "SUCCESS") {
                // portal_survey_list.refreshSurveyById(survey_id);
                portal_survey_list.publishSurveyReady(survey_id);
            } else {
                // TODO: add a designed notification to the user here
                alert(response.error_message);
            }
        });

        // enablePostButton(false);
    }

    function enablePostButton(enable) {
        $("#fl-btn-post").prop("disabled", !enable);
    }

    function clearPostUIData() {

        if ($(".note-editor button[data-event='codeview']").hasClass("active")) {
            //  return back to non code view            
            $(".note-editor button[data-event='codeview']").click();
        }

        $(".fl-ui-announcement").code("");

        name_selection.clearSelectedNames();
        // TODO: add clear responses for surveys here

        $('#fl-select-post-type').removeAttr('disabled');

    }

    function setUpdatePostMode(postId) {
        $('#fl-btn-post').attr('mode', UpdateMode.UPDATE);
        $('#fl-btn-post').attr('value', 'Update');
        $('#fl-btn-post').attr('post-id', postId);
        $('#fl-btn-post').html("Update");

        $('#fl-btn-clear').attr('value', 'Cancel');
        $('#fl-select-post-type').attr('disabled', 'disabled');
    }

    function setNewPostMode() {
        $('#fl-btn-post').attr('mode', UpdateMode.NEW);
        $('#fl-btn-post').attr('value', 'Post');
        $('#fl-btn-post').attr('post-id', 0);
        $('#fl-btn-post').html("Post");

        $('#fl-btn-clear').attr('value', 'Clear');
        $('#fl-select-post-type').removeAttr('disabled');
    }

    function displayReadersSelector(display) {
        $(".fl-widget-name-selector").css("display", display ? "block" : "none");
    }

    var survey = {
        init: function () {
            this.addEventListener();
            this.hideComponents();
            this.addResponse();
        },
        responseLimit: 10,
        addEventListener: function () {
            var context = this;

            var $surveyWrapper = $('.ui-announcements-survey-wrapper');


            $surveyWrapper.on('focus', '.ui-survey-response-input', function () {
                if ($surveyWrapper.find('.ui-survey-response').length == ($(this).closest('.ui-survey-response').index() + 1)) {
                    context.addResponse();
                }
            });
        },
        addResponse: function (id, response) {
            var $responseList = $('.ui-announcements-survey-wrapper .ui-survey-response-list');
            var $newResponse = $('<div class="ui-survey-response" ' + (id != undefined ? 'data-res-id=' + id : '') + '><input type="text" maxlength="100" class="ui-survey-response-input" placeholder="Add an option..."  style="width:90%;border:0px" value=""><span style="float:right" class="response-text-counter">0/100</span></div>');
            $newResponse.find('input:first').val(response != undefined ? response : "");
            if ($responseList.find('.ui-survey-response').length < this.responseLimit) {
                $responseList.append($newResponse);
            }

        },
        collectResponse: function (id) {
            var context = this;
            var $responses = $('#fl-post-' + id).find('.survey-selectable-response');
            $.each($responses, function () {
                // console.log($(this).val() + " ==== " + $(this).siblings('span').text())
                context.addResponse($(this).val(), $(this).siblings('span').text());
            });
        },
        removeResponse: function () {

        },
        showComponents: function () {
            $('.ui-announcements-survey-wrapper').show()
        },
        hideComponents: function () {
            $('.ui-announcements-survey-wrapper').hide()
        },
        clearResponses: function () {
            var $multiple_res = $('.ui-announcements-survey-wrapper .survey-option-multiple-res:first');
            var $publish_results = $('.ui-announcements-survey-wrapper .survey-option-publish:first');

            $multiple_res.prop('checked', false);
            $publish_results.prop('checked', false);
            $multiple_res.prop('disabled', false);
            $publish_results.prop('disabled', false);

            $('.ui-announcements-survey-wrapper .ui-survey-response-list').html("");
        },
        save: function (textContents, visibilityType, postId) {
            var $surveyWrapper = $('.ui-announcements-survey-wrapper');

            var responses = $('.ui-announcements-survey-wrapper .ui-survey-response-input').map(function () {
                var resID = $(this).parent().data('res-id');
                return $.trim($(this).val()) == "" && resID == undefined ? null : {'id': resID == undefined ? null : resID, "text": $(this).val()};
            }).get();

            var responsesText = $(responses).map(function () {
                return $.trim(this.text);
            }).get();


            var uniqueText = $.unique(responsesText);


            var enable_multiple_responses = $surveyWrapper.find('.survey-option-multiple-res:first').prop('checked') ? 1 : 0;
            var publish_results = $surveyWrapper.find('#survey-option-publish').prop('checked') ? 1 : 0;

            if (responses.length <= 1) {
                showNotification({
                    message: "Requires 2 or more options.",
                    type: "error",
                    autoClose: true,
                    duration: 10
                });
            } else if (uniqueText.length != responses.length) {
                showNotification({
                    message: "Options must be unique.",
                    type: "error",
                    autoClose: true,
                    duration: 10
                });
            }
            else {
                saveSurvey(postId, textContents, visibilityType, responses, enable_multiple_responses, publish_results);
            }

            // 
        }
    };

    var fixUiAnnouncement = {
        init: function () {

        },
        positiveUIfix: function () {

            $('.ui-announcements-bottom-wrapper').add('.fl-ui-announcement-post-action #fl-btn-clear').add('.fl-widget-name-selector').css({
                'border-bottom-right-radius': '3px',
                '-moz-border-bottom-right-radius': '3px',
                '-webkit-border-bottom-right-radius': '3px',
                'border-bottom-left-radius': '3px',
                '-moz-border-bottom-left-radius': '3px',
                '-webkit-border-bottom-left-radius': '3px'
            });


        },
        negativeUIfix: function () {

            $('.ui-announcements-bottom-wrapper').add('.fl-ui-announcement-post-action #fl-btn-clear').css({
                'border-bottom-right-radius': '0px',
                '-moz-border-bottom-right-radius': '0px',
                '-webkit-border-bottom-right-radius': '0px',
                'border-bottom-left-radius': '0px',
                '-moz-border-bottom-left-radius': '0px',
                '-webkit-border-bottom-left-radius': '0px'
            });

        },
        negativeUIfixLefttOnly: function () {

            $('.ui-announcements-bottom-wrapper').add('.fl-ui-announcement-post-action #fl-btn-clear').css({
                'border-bottom-left-radius': '0px',
                '-moz-border-bottom-left-radius': '0px',
                '-webkit-border-bottom-left-radius': '0px'
            });

        },
        widgetnameselectorActive: function () {
            //$('.ui-survey-response-list').hide();
        },
        surveyresponselistActive: function () {
            //$('.fl-widget-name-selector').hide();

        }

    };

    message_board_writer.editPost = function (postData) {

        main.showTab("composer");

        $(".fl-ui-announcement").code(postData.textContent);
        $("#fl-select-post-visibility-type").val(postData.visibilityType);
        $("#fl-select-post-type").val(postData.type);

        initializeSummernoteEditor();

        if (postData.visibilityType == VisibilityType.CUSTOM) {
            console.log(postData.followers);
            var followers = followers = $.parseJSON(postData.followers);
            displayReadersSelector(followers.length > 0);
            name_selection.setSelectedNames(followers);
        }

        console.log(postData)
        if (postData.type == PostType.ANNOUNCEMENT) {
            survey.hideComponents();
        } else if (postData.type == PostType.SURVEY) {
            var $surveyWrapper = $('.ui-announcements-survey-wrapper');
            survey.clearResponses();
            survey.showComponents();
            $surveyWrapper.find('.survey-option-multiple-res:first').prop('checked', postData.allowMultipleResponses == 1 ? true : false);
            $surveyWrapper.find('.survey-option-multiple-res:first').prop('disabled', true);
            $surveyWrapper.find('.survey-option-publish:first').prop('checked', postData.publishResults == 1 ? true : false);
            survey.collectResponse(postData.id);
        }

        setUpdatePostMode(postData.id);
        $('#summernote').find('.note-editable').focus();

    };

})();
