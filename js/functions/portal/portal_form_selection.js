
/* global main, portal_post_list, portal_request_list */

// namespace portal_form_selection
var portal_form_selection = {};

(function () {

    $(document).ready(function () {
        $('body').on('input propertychange', '#appSearch', function () {
            var filterText = $(this).val().trim();
            portal_form_selection.filter(filterText);
        });

        $(".fl-selectable-form-action").click(function () {

            var action = $(this).attr("data-action");
            var formId = $(this).attr("data-form-id");
            var formName = $(this).attr("data-form-name");

            if (action === "create") {
                var currentlyOpenTargets = main.getCurrentlyOpenTabTargets();
                var target = "form-" + formId;

                if (indexOfArrayValue(target, currentlyOpenTargets) > -1) {
                    //  display the already created section
                    main.showTab(target);
                   
                } else {
                    //  create a new tab and section if the form is not yet displaying
                    main.addFormCreationTab(formId, formName);
                }
            } else if (action === "view") {
                portal_post_list.openPostFilterForRequest(formId, formName);
                portal_post_list.setSelectedFilterType(portal_post_list.FilterType.FORM_REQUESTS);
                portal_request_list.refreshRequests(formId);
            }

        });
    });

    portal_form_selection.filter = function (filterText) {

        clearFilteredFormsContainer();

        if (filterText == "") {
            displayAllFormsContainer(true);
            return;
        } else {
            displayAllFormsContainer(false);
        }

        var matches = $(".fl-selectable-form").filter(function () {
            return $(this).attr("data-form-name").toLowerCase().indexOf(filterText.toLowerCase()) === 0;
        });

        $(matches).each(function () {
            console.log($(this).attr("data-form-name"));
            addToFilteredFormContainer($(this).parent().clone());
        });
    };

    function addToFilteredFormContainer(html) {
        $(".fl-filtered-selectable-forms-container ul").append(html);
    }

    function clearFilteredFormsContainer() {
        $(".fl-filtered-selectable-forms-container ul").html("");
    }

    function displayAllFormsContainer(display) {
        $(".fl-all-selectable-forms-container").css("display", display ? "block" : "none");
    }

    function loadForm() {
        var url = "http://eforms.gs3.com.ph/user_view/workspace";
        var params = {
            view_type: "request",
            formID: 13,
            requestID: 0
        };

        $.get(url, params, function (response) {
            console.log(response);
        });

    }

    function indexOfArrayValue(arrayValue, array) {
        var i = -1, index = -1;

        for (i = 0; i < array.length; i++) {
            if (array[i] === arrayValue) {
                index = i;
                break;
            }
        }

        return index;
    }

})();