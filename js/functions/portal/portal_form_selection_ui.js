$(document).ready(function () {

    portalDragApp.init(".fl-ui-app-content");

});

(function () {

    portalDragApp = {
        'init': function (element) {

            self = $(this);

            var localStorageForms;
            var icon_key;

            try {
                localStorageForms = localStorage.getItem("listItemForm") || "[]";
                icon_key = JSON.parse(localStorageForms);
            } catch (e) {
                console.log('Failed to load forms from local storage: ' + e);
                console.log(localStorageForms);
                localStorageForms = [];
                icon_key = [];
            }
            
            var icon_sorted_object = icon_key.map(function (a) {
                return $(element).find('a[data-form-id="' + a + '"]').eq(0).parents('li').eq(0);
            }).reverse();
            $.each(icon_sorted_object, function (a, b) {
                $('.fl-ui-app-content').eq(1).prepend(b);
            });

            $(element).sortable({
                containment:"parent",
                start: function (event, ui) {
                    console.log("start");
                    $(this).attr('data-previndex', ui.item.index());
                },
                sort: function (event, ui) {
                    //console.log("sort");
                },
                beforeStop: function (event, ui) {
                    //console.log("beforeStop");
                },
                stop: function (event, ui) {
                    //console.log("stop");


                },
                change: function (event, ui) {
                    //console.log("change")
                },
                update: function (event, ui) {
                    console.log("update");

                    var listItem = $(this).find('li');
                    var collect = [];
                    var key = "listItemForm";
                    listItem.each(function () {
                        //console.log($(this).attr('data-icon-key'));
                        console.log($(this).find('a').attr('data-form-id'));
                        collect.push($(this).find('a').attr('data-form-id'));
                    })

                    localStorage.setItem(key, JSON.stringify(collect));
                }
            });


        }


    }

})();