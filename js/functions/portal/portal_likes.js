
var portal_likes = {};

(function () {

    portal_likes.likePost = function (postId, onDoneCallback) {
        likeOrUnlike('like', postId, 0, onDoneCallback);
    };

    portal_likes.unlikePost = function (postId, onDoneCallback) {
        likeOrUnlike('unlike', postId, 0, onDoneCallback);
    };

    function likeOrUnlike(action, postId, commentId, onDoneCallback) {
        var url;

        if (action == 'like') {
            url = '/portal_api/like';
        } else {
            url = '/portal_api/unlike';
        }

        var params = {
            post_id: postId,
            comment_id: commentId
        };

        $.get(url, params, function (response) {

            console.log(response);
            if (response.status == 'SUCCESS') {
                onDoneCallback(response.results.total_likes);
            } else {
                onDoneCallback(0, response.error_message);
            }
        });
    }

})();