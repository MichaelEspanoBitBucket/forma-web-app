
/* global seeOthersMood */

var portal_moods = {};
portal_moods.api = {};
portal_moods.view = {};

(function () {

    $(document).ready(function () {

        portal_moods.view.bindLooseEvents();

    });

    function showMoodLoading(show, postId, commentId) {

        var display = show ? "inline-block" : "none";

        if (commentId == 0) {
            $('.loading-mood[data-post-id=' + postId + ']').css('display', display);
        } else {
            $('.loading-mood[data-comment-id=' + commentId + ']').css('display', display);
        }

    }

    portal_moods.view.bindLooseEvents = function () {

        seeOthersMood.init('.fl-action-mood-wrapper');

        $('.fl-action-mood').unbind('click');
        $('.fl-action-mood').click(function () {
            var postId = $(this).data('post-id');
            var selectedMood = $(this).html();
            var selectedMoodIcon = $(this).data('post-icon');

            portal_moods.api.setMood(selectedMood, postId, 0, function () {
                showMoodLoading(false, postId, 0);
                portal_moods.view.setPostMood(postId, selectedMood, selectedMoodIcon);
            });

            showMoodLoading(true, postId, 0);
        });

        $('.fl-action-comment-mood').unbind('click');
        $('.fl-action-comment-mood').click(function () {
            var postId = $(this).data('post-id');
            var postCommentId = $(this).data('comment-id');
            var selectedMood = $(this).html();
            var selectedMoodIcon = $(this).data('post-icon');

            portal_moods.api.setMood(selectedMood, postId, postCommentId, function () {
                showMoodLoading(false, postId, postCommentId);
                portal_moods.view.setCommmentMood(postCommentId, selectedMood, selectedMoodIcon);
            });

            showMoodLoading(true, postId, postCommentId);
        });
    };

    portal_moods.view.setPostMood = function (postId, mood, postMoodIcon) {

        var moodText = "";

        if (mood == "Don't Care") {
            moodText = "You don't care about this.";
        } else {
            moodText = "You feel " + mood.toLowerCase() + " about this.";
        }

        //        alert(postId);

        $('.post-current-user-mood[data-post-id=' + postId + ']').html(moodText);
        var previousMoodIcon = $('.post-current-user-mood-icon[data-post-id=' + postId + '] > use').attr('xlink:href');
        previousMoodIcon = previousMoodIcon.substring(1);   // remove leading #

        $('.post-current-user-mood-icon[data-post-id=' + postId + '][data-comment-id=0]').removeClass(previousMoodIcon);
        $('.post-current-user-mood-icon[data-post-id=' + postId + '][data-comment-id=0]').addClass(postMoodIcon);
        $('.post-current-user-mood-icon[data-post-id=' + postId + '][data-comment-id=0] > use').attr('xlink:href', "#" + postMoodIcon);

        $('.fl-action-mood-options-wrapper[data-post-id=' + postId + '][data-comment-id=0] > li').each(function () {
            if ($(this).data('mood') == mood) {
                $(this).addClass('fl-action-your-mood');
            } else {
                $(this).removeClass('fl-action-your-mood');
            }
        });

    };

    portal_moods.view.setCommmentMood = function (commentId, mood, postMoodIcon) {

        var moodText = "";

        if (mood == "Don't Care") {
            moodText = "You don't care about this";
        } else {
            moodText = "You feel " + mood.toLowerCase() + " about this.";
        }

        $('.comment-current-user-mood[data-comment-id=' + commentId + ']').html(moodText);
        var previousMoodIcon = $('.comment-current-user-mood-icon[data-comment-id=' + commentId + '] > use').attr('xlink:href');
        previousMoodIcon = previousMoodIcon.substring(1);   // remove leading #

        $('.comment-current-user-mood-icon[data-comment-id=' + commentId + ']').removeClass(previousMoodIcon);
        $('.comment-current-user-mood-icon[data-comment-id=' + commentId + ']').addClass(postMoodIcon);
        $('.comment-current-user-mood-icon[data-comment-id=' + commentId + '] > use').attr('xlink:href', "#" + postMoodIcon);

        $('.fl-action-mood-options-wrapper[data-comment-id=' + commentId + '] > li').each(function () {
            if ($(this).data('mood') == mood) {
                $(this).addClass('fl-action-your-mood');
            } else {
                $(this).removeClass('fl-action-your-mood');
            }
        });

    };

    portal_moods.api.setMood = function (mood, postId, commentId, onFinishCallback) {

        if (!commentId) {
            commentId = 0;
        }

        var url = "/portal_api/set_mood";
        var params = {
            mood: mood,
            post_id: postId,
            comment_id: commentId
        };

        console.log(params);

        $.post(url, params, function (response) {
            console.log(response);
            if (response.status === 'SUCCESS') {
            } else {

            }

            onFinishCallback();
        });
    };


    portal_moods.api.getUserMoods = function (postId, commentId, onFinishCallback, onErrorCallback) {

        var url = "/portal_api/user_moods";
        var params = {
            post_id: postId,
            comment_id: commentId
        };

        $.post(url, params, function (response) {
            console.log(response);

            if (response.status == 'SUCCESS') {
                onFinishCallback(response.results);
            } else {
                if (onErrorCallback) {
                    onErrorCallback(response.error_message);
                }
            }
        });

    };

})();
