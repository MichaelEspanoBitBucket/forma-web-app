
/* global announcement_posted_ui */

(function () {

    $(document).ready(function () {

        $('#fl-action-show-online-users').click(function () {
            $('#fl-action-show-online-users').css('background-color', '');
            $('#fl-action-show-online-users').css('color', '#6E6E6E');
        });

        bindLooseEvents();
        updateOnlineUserCount();
    });

    function bindLooseEvents() {
//        $('.fl-link-view-messages').click(function () {
//            setTimeout(updateUsers, 6000);
//        });

        $('#fl-filter-online-users').on('input', function () {
            var keyword = $(this).val();
            filterOnlineUsers(keyword);
        });
    }

    $(document).on('socketActivated', function (event) {
        var socket = event.socket;

        socket.on('thread_opened', function (data) {
//            console.log(data);
            updateUsers();
        });

        socket.on('userLoggedIn', function (user) {
            updateUsers();
        });

        socket.on('userLoggedOut', function (user) {
            updateUsers();
        });

        socket.on('message_recieved', function (data) {
            if (!$('#fl-action-show-online-users').hasClass('fl-tab-selected')) {
                $('#fl-action-show-online-users').effect("shake", {direction: "up", times: 15, distance: 10}, 1500);
                $('#fl-action-show-online-users').css('background-color', 'red');
                $('#fl-action-show-online-users').css('color', 'white');
            }

            updateUsers();
        });
    });

    $(document).on('socketFailedActivation', function (event) {
        $('#online-users-message').html("<b>(Oops! There seems to be a connection problem, try refreshing the page.)</b>");
    });

    function updateUsers() {
        $.get("/portal/online_users", {}, function (html) {
            $('.fl-ui-announcement-online-users-wrapper').html(html);

            updateOnlineUserCount();
            bindLooseEvents();

            if (typeof announcement_posted_ui !== 'undefined') {
                announcement_posted_ui.tabNavsPortal();
            }

        });
    }

    function updateOnlineUserCount() {
        var onlineUserCount = $('#fl-ui-announcement-online-users-content').data('online-user-count');
        $('.fl-online-user-count').html(onlineUserCount);
    }

    function filterOnlineUsers(keyword) {

        console.log(keyword);

        if (!keyword) {
            clearOnlineUserFilter();
            return;
        }

        $('#fl-ul-filtered-online-users').removeClass('isDisplayNone');
        $('#fl-ul-online-users').addClass('isDisplayNone');

        $('#fl-ul-filtered-online-users').html('');

        var filteredOnlineUsers = $('.fl-li-online-user').filter(function () {
            return $(this).attr('data-keyword').toLowerCase().indexOf(keyword.toLowerCase()) > -1;
        });

        $(filteredOnlineUsers).each(function () {
            $('#fl-ul-filtered-online-users').append('<li class="section clearing">' + $(this).html() + '</li>');
        });

    }

    function clearOnlineUserFilter() {
        $('#fl-ul-filtered-online-users').addClass('isDisplayNone');
        $('#fl-ul-online-users').removeClass('isDisplayNone');
    }

})();
