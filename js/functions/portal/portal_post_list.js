
/* global portal_survey_list, post_comments, message_board_writer, announcement_posted_ui, getSCrollTop, portal_request_list */

var portal_post_list = {
    DEFAULT_FETCH_COUNT: 10,
    REQUEST_FETCH_COUNT: 20,
    IMAGE_WIDTH_LIMIT: 400,
    VIDEO_WIDTH_LIMIT: 400,
    FilterType: {
        OLD_ANNOUNCEMENTS: -1,
        ALL_POSTS: 0,
        ANNOUNCEMENTS: 1,
        SURVEYS: 2,
        FORM_REQUESTS: 3
    }

};

(function () {

    var CONTEXT = "/portal";

    var lastSelectedPostFilterType = 0;

    $(document).ready(function () {
        $("#fl-select-post-filter-type").on("change", function () {
            if (portal_post_list.getSelectedFilterType() == portal_post_list.FilterType.OLD_ANNOUNCEMENTS) {
                window.open("/user_view/announcements", '_blank');
                $("#fl-select-post-filter-type").val(lastSelectedPostFilterType);
            } else if (portal_post_list.getSelectedFilterType() == portal_post_list.FilterType.FORM_REQUESTS) {
                portal_request_list.refreshRequests(portal_post_list.getSelectedFilterFormId());
            } else {
                portal_post_list.refreshPosts();
                //announcement_posted_ui.init('ul.fl-ui-announcement-header-action-main');
//                submiCommentAction_ui.init('.fl-ui-comment-add-comment-wrapper');
            }
            lastSelectedPostFilterType = portal_post_list.getSelectedFilterType();
            var selectedPostFilterName = $(this).find("option[value='" + lastSelectedPostFilterType + "']").text();

            $(".ui-announcement-posted-column-header-label ").html(selectedPostFilterName);
        });
        bindPostItemEvents();
        adjustLargeWidthImages();
        adjustLargeWidthVideo();
    });

    portal_post_list.setSelectedFilterType = function (filterType) {
        $("#fl-select-post-filter-type").val(filterType);
        var selectedPostFilterName = $("#fl-select-post-filter-type").find("option[value='" + filterType + "']").text();
        $(".ui-announcement-posted-column-header-label ").html(selectedPostFilterName);
    };

    portal_post_list.refreshPosts = function () {

        loadPosts(portal_post_list.getSelectedFilterType(), 0, portal_post_list.DEFAULT_FETCH_COUNT);
        //announcement_posted_ui.adjustPostListHeight(); by roni
        //getSCrollTop.init();

    };

    portal_post_list.displayPostListLoading = function (display) {
        $("#fl-post-list-loading-container").css("display", display ? "block" : "none");
        $("#fl-post-list").css("display", !display ? "block" : "none");
    };

    portal_post_list.setPostListHtml = function (html) {
        $("#fl-post-list").html(html);
    };

    portal_post_list.appendPostListHTML = function (html) {
        $("#fl-post-list").append(html);
    };


    portal_post_list.openPostFilterForRequest = function (formId, title) {

        var formRequestsFilterType = portal_post_list.FilterType.FORM_REQUESTS;
        var optionExists = $("#fl-select-post-filter-type option[value='" + formRequestsFilterType + "']").length > 0;

        if (optionExists) {
            $("#fl-select-post-filter-type option[value='" + formRequestsFilterType + "']").attr("data-form-id", formId);
            $("#fl-select-post-filter-type option[value='" + formRequestsFilterType + "']").html(title);
        } else {
            $("#fl-select-post-filter-type").append('<option value="' + formRequestsFilterType + '" data-form-id="' + formId + '">' + title + '</option>');
        }

    };

    portal_post_list.getSelectedFilterType = function () {
        return $("#fl-select-post-filter-type").val();
    };

    portal_post_list.getSelectedFilterFormId = function () {
        return $("#fl-select-post-filter-type > option[value='" + portal_post_list.FilterType.FORM_REQUESTS + "']").attr("data-form-id");
    };

    function bindPostItemEvents() {
        $('.fl-li-action-edit-post').click(function () {
            var postId = $(this).attr('data-id');
            var postData = getPostData(postId);
            message_board_writer.editPost(postData);
            $('.fl-ui-announcment-navs-wrapper').children('ul').find('li').removeClass('fl-tab-selected');
            $('.fl-ui-announcment-navs-wrapper').children('ul').find('li:first').addClass('fl-tab-selected');
            $('#fl-ui-announcment-post-wrapper').removeClass('isDisplayNone').addClass('isDisplayBlock');
            $('#fl-ui-announcement-apps-wrapper').removeClass('isDisplayBlock').addClass('isDisplayNone');
        });

        $('.fl-li-action-delete-post').click(function () {
            var postId = $(this).attr('data-id');

            var newConfirm = new jConfirm("Are you sure you want to delete this post?", '', '', '', '', function (confirmed) {
                if (confirmed) {
                    deletePost(postId);
                    
                    var imagePostElem = $(this).parents('.fl-ui-announcement-posted-content-wrapper').find('.fl-post-text-contents').find('img');
                    if (imagePostElem != undefined) {
                        var imagePostElemLength = imagePostElem.length;
                        if (imagePostElemLength > 0) {
                            for (var x = 0; x < imagePostElemLength; x++ ) {
                                deletePostImage(imagePostElem.eq(x).attr('src'));
                            }
                        }
                    }
                    
                    portal_survey_list.refreshSurvey();
                }
            });
            newConfirm.themeConfirm("confirm2", {'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});

        });

        $('#fl-action-display-more-posts').click(function () {
            var currentFetchIndex = $(this).attr("data-current-fetch-index");
            loadPosts(portal_post_list.getSelectedFilterType(), currentFetchIndex, portal_post_list.DEFAULT_FETCH_COUNT);
        });

        $('.post-visibility-type-icon').click(function () {
            var visbilityTypeId = $(this).data('visibility-type-id');
            var postId = $(this).data('id');

            if (visbilityTypeId == 3) {
                showPostReaders(postId);
            }

        });

        post_comments.bindLooseEvents();

    }

    function loadPosts(filterType, fromIndex, fetchCount,callback) {

        var url = CONTEXT + "/post_list";
        var params = {
            filter_post_type: filterType,
            from_index: fromIndex,
            fetch_count: fetchCount
        };

        $.post(url, params, function (response) {
            displayPostListContainer(true);
            portal_post_list.displayPostListLoading(false);
            $(".fl-loading-more-posts").addClass("isDisplayNone");

            if (fromIndex == 0) {
                portal_post_list.setPostListHtml(response);
            } else {
                portal_post_list.appendPostListHTML(response);
            }

            // rebind events
            bindPostItemEvents();
            adjustLargeWidthImages();
            adjustLargeWidthVideo();
            announcement_posted_ui.init('ul.fl-ui-announcement-header-action-main');
            announcement_posted_ui.initSubmitCommentAction('.fl-ui-comment-add-comment-wrapper');


            //lazy load
            $(".fl-ui-announcement-posted-column img").lazyload({
                 effect : "fadeIn",
                 container: $(".fl-ui-announcement-posted-column"),
                 broken_image : $("#broken_image_post").text()
             });

            $(".fl-ui-announcement-posted-column img").lazyload({
                 effect : "fadeIn",
                 broken_image : $("#broken_image_post").text()
             });
            
            if(typeof callback =="function"){
                callback(true);
            }
        });

        if (fromIndex > 0) {
            $(".fl-loading-more-posts").removeClass("isDisplayNone");
            $("#fl-action-display-more-posts").remove();
        } else {
            displayPostListContainer(false);
            portal_post_list.displayPostListLoading(true);
        }

    }

    function deletePost(postId) {
        var url = "portal_api/delete_post";
        var params = {
            post_id: postId
        };

        $.get(url, params, function (response) {
            console.log(response);
            portal_post_list.refreshPosts();
        });

    }

    function deletePostImage(image){

        var url = "file/delete";
        var params = {
            imagefile: image
        };

        $.post(url, params, function (response) {
            console.log(response);
            portal_post_list.refreshPosts();
        });  
    
    }

    function getPostData(postId) {

        var postData = {
            id: postId,
            textContent: $('#fl-post-' + postId).find('.fl-post-text-contents').html(),
            type: $('#fl-post-' + postId).attr('data-type-id'),
            visibilityType: $('#fl-post-' + postId).attr('data-visibility-type'),
            selectableResponses: $('#fl-post-' + postId).attr('data-selectable-responses'),
            allowMultipleResponses: $('#fl-post-' + postId).attr('data-allow-multiple-responses'),
            followers: $('#fl-post-' + postId).attr('data-followers'),
            publishResults: $('#fl-post-' + postId).attr('data-publish-results')
        };

        return postData;
    }

    function adjustLargeWidthImages() {

        $(".fl-post-text-contents img").each(function () {
            var imageWidth = $(this).width();
            if (imageWidth > portal_post_list.IMAGE_WIDTH_LIMIT) {
                $(this).attr("style", $(this).attr("style") + ";width: 100%");
            }
        });

    }

    function adjustLargeWidthVideo() {
        $('.fl-post-text-contents iframe').each(function () {
            var videoWidth = $(this).width();
            if (videoWidth > portal_post_list.VIDEO_WIDTH_LIMIT) {
                $(this).attr('style', $(this).attr('style') + '; width:100%');
            }
            ;
        });
    }

    function displayPostListContainer(display) {
        if (display) {
            $(".fl-ui-announcement-posted-column").show();
        } else {
            $(".fl-ui-announcement-posted-column").hide();
        }
    }

    function showPostReaders(postId) {
        var ret = '<h3 class="fl-margin-bottom"><svg class="icon-svg icon-modal-svg" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-custom-post-portal"></use></svg>';
        ret += 'Readers';
        ret += '</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
        ret += '<div style="height: auto;">';
        ret += '<div class="body"></div>';
        ret += '</div>';

        var newDialog = new jDialog(ret, "", "450", "600", "", function () {
        });
        newDialog.themeDialog("modal2");

        $('.content-dialog-scroll').perfectScrollbar().mouseenter(function () {
            $(this).perfectScrollbar('update');
        });

        var url = "/portal_api/post_followers";
        var params = {
            post_id: postId
        };

        $.post(url, params, function (response) {
            console.log("RESPONSE READERS ",response);
            $("#popup_message").find('.body').html(response);
        });

    }

    portal_post_list.loadPosts = loadPosts;

})();
