
/* global portal_post_list, announcement_posted_ui */

var portal_request_list = {};

(function () {

    var CONTEXT = "/portal";
    var currentRequestIndex = 0;
    var currentFormId;

    $(document).ready(function () {
        announcement_posted_ui.adjustPostListHeight();
    });

    portal_request_list.refreshRequests = function (formId) {

        currentFormId = formId;
        currentRequestIndex = 0;
        loadRequests(formId, 0, portal_post_list.REQUEST_FETCH_COUNT);

    };

    portal_request_list.loadMoreRequests = function () {
        currentRequestIndex += portal_post_list.REQUEST_FETCH_COUNT;
        $('#fl-action-display-more-requests').remove();
        loadRequests(currentFormId, currentRequestIndex, portal_post_list.REQUEST_FETCH_COUNT);
    };

    function loadRequests(formId, fromIndex, fetchCount) {

        var url = CONTEXT + "/request_list";
        var params = {
            form_id: formId,
            start_index: fromIndex,
            fetch_count: fetchCount
        };

        $.post(url, params, function (response) {
            console.log(response);

            portal_post_list.displayPostListLoading(false);
            $(".fl-loading-more-posts").addClass("isDisplayNone");

            if (fromIndex === 0) {
                portal_post_list.setPostListHtml(response);
            } else {
                portal_post_list.appendPostListHTML(response);
            }

            // rebind events
            bindRequestItemEvents();
            announcement_posted_ui.init('ul.fl-ui-announcement-header-action-main');
            announcement_posted_ui.initSubmitCommentAction('.fl-ui-comment-add-comment-wrapper');
            
            portal_post_list.displayPostListLoading(false);
            
        });

        portal_post_list.displayPostListLoading(true);

    }

    function bindRequestItemEvents() {

        $('#fl-action-display-more-requests').click(function () {
            portal_request_list.loadMoreRequests();
        });

    }

})();
