
var portal_survey_list = {};
var survey_chart_types = [
  {"id":1, "name":"Bar", "giIdentifierName":"giBarGraph"},
  {"id":2, "name":"Line", "giIdentifierName":"giLineGraph"},
  {"id":3, "name":"Column", "giIdentifierName":"giColumnGraph"},
  {"id":4, "name":"Area", "giIdentifierName":"giAreaGraph"},
  {"id":5, "name":"Pie", "giIdentifierName":"giPieChart"},
  {"id":6, "name":"Table", "giIdentifierName":"giTableChart"}
];

// 1 Bar giBarGraph
// 2 Line  giLineGraph
// 3 Column  giColumnGraph
// 4 Area  giAreaGraph
// 5 Pie giPieChart
// 6 Table giTableChart
// 7 Gauge giGaugeChart
// 8 Tabs  giTabContainer

(function () {

    var CONTEXT = "/portal";

    $(document).ready(function () {
         portal_survey_list.setGraps();


    });


    $(window).resize(function(){
        $.each($(".fl-request-widget .giObject"), function() {
           // $(this).giPieChart('setProperties', {'Width':$('.fl-request-widget-data').width() - 2});
        });
    })


    portal_survey_list.publishSurveyLoad = function(id){
      var $publish = $('#publish_survey_' + id);
      var $chartType = $("#survey_chart_type_" + id);
      $chartType.prop('disabled',true);
      $publish.prop('disabled',true);
      $("<i class='fa fa-spinner fa-spin survey_option_loading'></i>").insertAfter($publish.parent())

    }

     portal_survey_list.publishSurveyReady = function(id){
      var $publish = $('#publish_survey_' + id);
      var $chartType = $("#survey_chart_type_" + id);
      $chartType.prop('disabled',false);
      $publish.prop('disabled',false);
      $publish.parent().parent().find('.survey_option_loading').remove()
    }

    portal_survey_list.refreshSurvey = function () {
        var url = CONTEXT + "/survey_list";
        var params = {};

        $.post(url, params, function (response) {
           portal_survey_list.setSurveyHtml(response);
            portal_survey_list.setGraps();
        });
       adjustImageWidthGraph();
    };


    portal_survey_list.setSurveyHtml = function(html){
        $('.fl-ui-announcement-graph-content-wrapper').html(html);
        
    }

    portal_survey_list.lockSurvey = function(id){
       
      $.each($(".survey-selectable-response[name='post_"+ id +"']"), function(){
            $(this).prop('disabled',true);
       })

      var $button = $('#fl-post-' + id).find('.btn-survey:first');
      $button.addClass('edit-response');
      $button.removeClass('submit-response')
      $button.val('Edit')


    }

    portal_survey_list.unLockSurvey = function(id){

       $.each($(".survey-selectable-response[name='post_"+ id +"']"), function(){
            $(this).prop('disabled',false);
       })

      var $button = $('#fl-post-' + id).find('.btn-survey:first');
      $button.addClass('submit-response');
      $button.removeClass('edit-response')
      $button.val('Submit')

            
    }

    portal_survey_list.setGraps = function(){
      $(".fl-request-widget").each(function() {
        //Delete
        var $survey = $(this)
        //var $chartContainer = $survey.find('.fl-request-widget-data');
       // var chartId = $survey.find('.survey_chart_types:first').val();
     //   var giIdentifierName = getChartByID(chartId).giIdentifierName;
        
        setGrap($survey.data("survey-id"));
      });
    }

    setGrap = function(surveyId){
      var $survey = $(".fl-request-widget[data-survey-id='" + surveyId + "']");
      var chartId = $survey.data('chart-type');
      var $chartContainer = $survey.find('.fl-request-widget-data');
      var giIdentifierName = getChartByID(chartId).giIdentifierName;
//delete here///
      
      var $giObject = $chartContainer.find('.giObject');
     $chartContainer.css('height',$chartContainer.height() + "px")
      if($giObject.size() > 0){
        $giObject[$giObject.data('giIdentifierName')]('delete')
      
      }

      if(surveyId != undefined){
        var $newItem = $('<div></div>');
        $newItem.appendTo($chartContainer);
        $newItem[giIdentifierName]('setProperties', {
          "Data URL":"/ajax/gi-get-survey-chart-data?surveyId=" + surveyId, 
          "Border Thickness":0, 
          "New Record Button": "hide",
          "Legends Position": "bottom", 
          "Show Session Filters":false,
          "Auto Update Data": false,
          "On Data Updated": function(result){
            if(result["Grand Total"] == null){
                $newItem[giIdentifierName]('setProperties', {Height:60})
            }else{
              if($newItem[giIdentifierName]('get Height') != "auto"){
                 $newItem[giIdentifierName]('setProperties', {Height:"auto"})
              }
            }
            $chartContainer.css('height',"auto")
            $survey.find('.survey_total_responses').text('Total number of responses : ' + (result["Grand Total"] == null? 0 : result["Grand Total"].Count));
          }
        });

      }
      $chartContainer.css('Height',"auto")
      
    }
    
    getChartByID = function(chartId){
      var chartProf ;
      $.each(survey_chart_types, function(index, val){
        if(val.id == chartId){
          chartProf = val
          return false;
        }
      })
       return chartProf;
    }
})();
