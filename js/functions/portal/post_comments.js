
/* global portal_moods */

var post_comments = {};

(function () {

    var COMMENT_FETCH_COUNT = 10;
    var MAX_COMMENT_SIZE = 200;

    $(document).ready(function () {

        bindLooseEvents();

    });

    function bindLooseEvents() {
        portal_moods.view.bindLooseEvents();
        $(".fl-action-display-comments").unbind("click");
        $(".fl-action-display-comments").click(function () {
            var postId = $(this).attr("data-post-id");
            var fetchIndex = $(this).attr("data-current-fetch-index");

            displayPostComments(postId, fetchIndex);
        });

        $('.fl-action-hide-comments').unbind('click');
        $('.fl-action-hide-comments').click(function () {
            var postId = $(this).attr("data-post-id");
            hidePostComments(postId);
            showHideCommentsAction(postId, false);
        });

        $(".fl-btn-post-comment").unbind("click");
        $(".fl-btn-post-comment").click(function () {
            var postId = $(this).attr("data-post-id");
            var comment = $('.fl-ui-comment-add-comment-field[data-post-id="' + postId + '"]').val();

            comment = comment.trim();
            if (comment.length === 0) {
                showNotification({
                    message: "You can't post an empty comment.",
                    type: "error",
                    autoClose: true,
                    duration: 10
                });
            } else {
                postComment(postId, comment);
            }

        });

        $(".fl-delete-post-comment").unbind("click");
        $(".fl-delete-post-comment").click(function () {
            var commentId = $(this).attr("data-comment-id");
            deleteComment(commentId);
        });

        $(".fl-ui-comment-add-comment-field").bind("input propertychange", function () {
            var postId = $(this).attr("data-post-id");
            var charactersLeft = MAX_COMMENT_SIZE - this.value.length;
            var disableComment = charactersLeft < 0 || charactersLeft >= MAX_COMMENT_SIZE;

            // trim comment to limit
            if (charactersLeft < 0) {
                $('.fl-ui-comment-add-comment-field[data-post-id="' + postId + '"]').val(this.value.substring(0, MAX_COMMENT_SIZE));
                charactersLeft = 0;

                // notify user about limit
                showNotification({
                    message: "Comments are limited to 200 characters only.",
                    type: "error",
                    autoClose: true,
                    duration: 10
                });
            }

            $('.fl-comment-size-counter[data-post-id="' + postId + '"]').html(charactersLeft);
            enablePostCommentButton(postId, !disableComment);

        });

    }

    function showHideCommentsAction(postId, show) {

        if (show) {
            $('.fl-action-hide-comments[data-post-id="' + postId + '"]').removeClass('isDisplayNone');
            $('.fl-action-display-comments[data-post-id="' + postId + '"][data-current-fetch-index=0]').addClass('isDisplayNone');
        } else {
            $('.fl-action-hide-comments[data-post-id="' + postId + '"]').addClass('isDisplayNone');
            $('.fl-action-display-comments[data-post-id="' + postId + '"][data-current-fetch-index=0]').removeClass('isDisplayNone');
        }

    }

    function hidePostComments(postId) {
        $('.fl-comments-container[data-post-id="' + postId + '"]').addClass("isDisplayNone");
    }

    function displayPostComments(postId, fetchIndex) {

        var url = "/portal/post_comment_list";
        var params = {
            post_id: postId,
            from_index: fetchIndex,
            fetch_count: COMMENT_FETCH_COUNT
        };

        $.get(url, params, function (response) {

            if (fetchIndex == 0) {
                $('.fl-comments-container[data-post-id="' + postId + '"]').removeClass("isDisplayNone");
                $('.fl-comments-container[data-post-id="' + postId + '"]').html(response);
            } else {
                $('.fl-action-display-comments-wrapper[data-post-id="' + postId + '"][data-current-fetch-index=' + fetchIndex + ']').addClass('isDisplayNone');
                $('.fl-comments-container[data-post-id="' + postId + '"]').append(response);
            }

            bindLooseEvents();
            showHideCommentsAction(postId, true);
        });

        clearPostCommentEditor(postId);

    }

    function postComment(postId, comment) {

        var url = "/portal_api/save_post_comment";
        var params = {
            post_id: postId,
            text_content: comment
        };

        console.log(params);

        $.post(url, params, function (response) {
            console.log(response);
            displayPostComments(postId, 0);
        });

        enablePostCommentButton(postId, false);

    }

    function deleteComment(commentId) {

        var url = "/portal_api/delete_post_comment";
        var params = {
            comment_id: commentId
        };

        $.post(url, params, function (response) {
            console.log(response);
            deleteCommentOnUI(commentId);
        });

        greyOutComment(commentId);

    }

    function deleteCommentOnUI(commentId) {
        $('.fl-ui-comment-posted-wrapper[data-id="' + commentId + '"]').remove();
    }

    function enablePostCommentButton(postId, enable) {
        if (enable) {
            $('.fl-btn-post-comment[data-post-id="' + postId + '"]').removeAttr("disabled");
        } else {
            $('.fl-btn-post-comment[data-post-id="' + postId + '"]').attr("disabled", "disabled");
        }
    }

    function greyOutComment(commentId) {
//        alert("greying out comment: " + commentId);
//        $('.fl-ui-comment-posted-wrapper[data-id="' + commentId + '"]').block({
//            message: '<h1>Premium Users only</h1>'
//        });

        // TODO: implement this
    }

    function clearPostCommentEditor(postId) {
        //$('.fl-action-display-comments[data-post-id="' + postId + '"]').css("display", "none");
//        $('.fl-action-display-comments-wrapper[data-post-id="' + postId + '"]').css('display', 'none');
        $('.fl-ui-comment-add-comment-field[data-post-id="' + postId + '"]').val("");

        // reset the comment count
        $('.fl-comment-size-counter[data-post-id="' + postId + '"]').html(200);
    }

    post_comments.bindLooseEvents = bindLooseEvents;

})();
