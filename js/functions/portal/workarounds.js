
$(document).ready(function () {

    // flyspray issue: 6426
    // link http://122.49.216.194:9000/flyspray/index.php?do=details&task_id=6426&dev=32&type%5B0%5D=&sev%5B0%5D=&due%5B0%5D=&cat%5B0%5D=&status%5B0%5D=open&percent%5B0%5D=&reported%5B0%5D=

    //   workaround for summernote bug
    $(".note-link-url").bind('input propertychange', function () {
        if ($(".note-link-text").val() == "") {
            $(".note-link-text").val("link");
        }
    });

});
