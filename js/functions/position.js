var jsonPositionData = {
    search_value: "",   
    "column-sort": "",
    "column-sort-type": "", 
    limit: 10,
}
var columnSort = [];
var pathname = window.location.pathname;
$(document).ready(function(){
	
	Position.init();
	

	$("body").on("keyup","#txtSearchPositionDatatable",function(e){
		if (e.keyCode == "13") {
	        $("#loadPosition").dataTable().fnDestroy();
	        jsonPositionData['search_value'] = $(this).val();
	        Position.triggerDataTable();
	    }
    })

    $("body").on("click","#btnSearchPositionDatatable",function(e){
	        $("#loadPosition").dataTable().fnDestroy();
	        jsonPositionData['search_value'] = $("#txtSearchPositionDatatable").val();
	        Position.triggerDataTable();
    })

	$("body").on("click",".addPositionModal",function(){
    	var json = {};
    	json['title'] = "<svg class='icon-svg icon-svg-modal svg-icon-position-settings' viewBox='0 0 100.264 100.597'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-position-settings'></use></svg> <span>Add Position</span>";
      json['type'] = 1;
    	json['value'] = '';
      json['description'] = '';
    	json['button'] = '<input type="button" class="btn-blueBtn addPosition fl-margin-right" value="Save" insert-type="1">';
    	Position.categoryModal(json);
    })
    $("body").on("click",".updatePosition",function(){
    	var json = {};
    	var category_id = $(this).attr("data-id");
    	json['title'] = "Edit Position";
      json['type'] = 2;
    	json['value'] = $(this).attr("data-name");
      json['description'] = $(this).attr("data-desc");
    	json['button'] = '<input type="button" data-id="'+ category_id +'" class="btn-blueBtn editPosition fl-margin-right" value="Save">';
    	Position.categoryModal(json);
    })
    $("body").on("click","#loadPosition th",function(){
        var cursor = $(this).css("cursor");
        jsonPositionData['column-sort'] = $(this).attr("field_name");
        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined  || cursor=="col-resize"){
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest("#loadPosition").find(".sortable-image").html("");
        if(indexcolumnSort==-1){
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonPositionData['column-sort-type'] = "ASC";
        }else{
            columnSort.splice(indexcolumnSort,1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonPositionData['column-sort-type'] = "DESC";
        }
        
        $("#loadPosition").dataTable().fnDestroy();
        var thisTh = this;
        Position.triggerDataTable(function(){
        	addIndexClassOnSort(thisTh);
        });
    })
    $("body").on("click",".addPositionSelectionUserRegister",function(){
    	var json = {};
    	json['title'] = "<svg class='icon-svg icon-svg-modal svg-icon-position-settings' viewBox='0 0 100.264 100.597'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-position-settings'></use></svg> <span>Add Position</span>";
      json['type'] = 1;
    	json['value'] = '';
      json['description'] = '';
    	json['button'] = '<input type="button" class="btn-blueBtn addPosition fl-margin-right" value="Save" insert-type="2">';
    	Position.categoryModal(json);

    })
    //  $("#batchPositionUpload").submit(function(){
    // 	ui.block();
    // })
    // $("#batchPositionUpload").ajaxForm(function(data){
        
    //     data = JSON.parse(data);
        
    //     showNotification({
    //         message: data['message'],
    //         type: data['type'],
    //         autoClose: true,
    //         duration: 3
    //     });
    //     $("#loadPosition").dataTable().fnDestroy();
		  // Position.triggerDataTable();
    //     ui.unblock();
    // });
	$("body").on("click",".submit_positions",function(){
		ui.block();
		$("#batchPositionUpload").ajaxForm(function(data){
	        data = JSON.parse(data);
	        
	        showNotification({
	            message: data['message'],
	            type: data['type'],
	            autoClose: true,
	            duration: 3
	        });
	        $("#loadPosition").dataTable().fnDestroy();
			  Position.triggerDataTable();
	        ui.unblock();
	        $(".fl-closeDialog").trigger("click");
	    }).submit();
	})
	$(".searchPositionsLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonPositionData['limit'] = val;
            $("#loadPosition").dataTable().fnDestroy();
            // var self = this;
            var oTable = Position.triggerDataTable();
        // }
    })

        $('body').on('click', '.batchPosUpload', function() {
        var ret = '<form action="/ajax/position" class="fl-importBtn" id="batchPositionUpload" method="POST" enctype="multipart/form-data" style="margin: 0px 15px 15px;">'+
                        '<input type="hidden" name="action" value="batch"/> '+
                        '<div class="fields">'+
                            '<div class="label_below2 fl-margin-bottom">Please select file: </div>'+
                            '<div class="input_position">'+
                                '<div id="uniform-fileInput" class="uploader label_below2" style="width: auto; position: static;">'+
                                    '<input type="file" data-action-id="3" value="upload" name="csv" id="file" size="24" style="opacity: 0; width:100%;" class="import_request_file">'+
                                    '<span id="uploadFilename_3" class="filename import_request_txt">No file selected</span>'+
                                    '<span class="action">Choose File</span>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="fields">'+
                            '<div class="label_below2"></div>'+
                            '<div class="input_position" style="color:#ff0000;font-style:italic;margin-top:5px;">Note: File should be in Comma Separated Value(CSV) file format </div>'+
                        '</div>'+
                        '<div class="fields">'+
                            '<div class="label_below2"></div>'+
                            '<div class="input_position" style="margin-top:5px;">'+
                                '<input type="button" class="btn-blueBtn fl-margin-right submit_positions" value="Ok" />'+
                                '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel" />'+
                            '</div>'+
                        '</div>'+
                    '</form>';
        var newDialog = new jDialog(ret, '', '', '', '', function() {

        });
        newDialog.themeDialog("modal2");
    });
})
Position = {
	init : function(){
		this.deleteformCategory();
		this.addPosition();
		this.editPosition();
		this.triggerDataTable();
		this.addPositionData();
	},
	triggerDataTable : function(callback){
		if (pathname=="/user_view/position_settings") {
		 	this.defaultData(callback);   
		}
	},
	deleteformCategory : function(){
		//code here
		var self = this;
		$("body").on("click",".deletePosition",function(){
			var id = $(this).attr("data-id");
			var newConfirm = new jConfirm("Are you sure you want to delete this position?", 'Confirmation Dialog','', '', '', function(r){
			 	if(r==true){
			 		//get forms
			 		ui.block();
			 		$.post("/ajax/position",
			 		{
			 			action:"deletePosition",
			 			id:id
			 		},function(data){
			 			data = JSON.parse(data);
			 			if(data['status']==0){
			 				showNotification({
				                 message: "The selected position cannot be deleted because it is being used in user or as processor in workflow",
				                 type: "error",
				                 autoClose: true,
				                 duration: 5
			                });
			 			}else{
			 				showNotification({
				                 message: "Position has been successfully deleted.",
				                 type: "success",
				                 autoClose: true,
				                 duration: 3
			                });
			 				$("#loadPosition").dataTable().fnDestroy();
			 				self.triggerDataTable();
			 			}
			 			ui.unblock();
			 		})
			 	}
			 })
			newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'});
		});
	},

	categoryModal : function(json){
		var self = this;
		// $("body").on("click",".addCategoryModal",function(){
			var ret = '<div><h3> '+ json.title +'</h3></div>';
                ret += '<div class="hr"></div>';
                ret += '</div>';
                ret += '<div id="comment-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; overflow: hidden;height: 192px" class="ps-container">'
	                ret += '<div class="content-dialog sub-container-comment position-subcontainer">';
	               		ret += '    <div class="pull-left container-position" style="width: 100%">';
			                ret += '    <div>';
			                ret += '    	<div class="fields_below section clearing fl-field-style">';
			               
			                ret += '    	<div class="input_position_below column div_1_of_1">';
			                 ret += '    		<div class="label_below2">Position Name: <font color="red">*</font></div>';
			                ret += '    		<input type="text" name="" data-type="textbox" class="form-text position" value="'+ json.value +'"></div>';
			                ret += '     	</div>';
                         ret += '    	<div class="fields_below section clearing fl-field-style">';
			                
			                ret += '    	<div class="input_position_below column div_1_of_1">';
			                ret += '    		<div class="label_below2">Description:</div>';
			                ret += '    		<input type="text" name="" data-type="textbox" class="form-text position-desc" value="'+ htmlEntities(json.description) +'"></div>';
			                ret += '     	</div>';
			                ret += '    </div>';
                         if (json.type==1) {
                            ret += '<div class="fa fa-plus cursor addPositionData" style="margin-right:5px"></div>';
                            ret += '<div class="fa fa-minus cursor minusPositionData display"></div>';
                         }
			            ret += '    </div>';
					ret += '</div>';
				ret += '</div>';
				ret += '</div>';
                //ret += '<div class="fields" style="border-top:1px solid #ddd;">';
                ret += '<div class="label_basic"></div>';
                ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                ret += json.button;
                ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
                ret += '</div>';
                ret += '</div>';
               var newDialog = new jDialog(ret, "", "500", "", "25", function() {

                });
                newDialog.themeDialog("modal2");
                limitText(".limit-text-ws",20);
                $('#comment-container').perfectScrollbar();
                $(".position").eq(0).focus()
		// })
	},
	addPosition : function(){
		var self = this;
		$("body").on("click",".addPosition",function(){
			var type = $(this).attr("insert-type");
			
			var positions = [];
         var desc = "";
			$(".position").each(function(){
				if($.trim($(this).val())==""){
					return true;
				}
				if(positions.indexOf($(this).val())==-1){
                desc = $(this).closest(".container-position").find(".position-desc").val();
					 positions.push({"value":$(this).val(),"description":desc});
				}else{
					//remove duplicate entries
					$(this).closest(".container-position").remove();
				}
			})
        console.log(positions)
			if(positions.length==0){
				showNotification({
	                 message: "Please fill out required field.",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
                });
                return false;
			}
			ui.block();
			$.post("/ajax/position",{action:"addPosition",positions:positions},function(data){
				data = JSON.parse(data);
				var inserted = data['inserted'];
				var notInserted = data['notInserted'];
				if(data['status']==0){
					//if there are records that are not inserted
					showNotification({
		                 message: "Warning there are "+ notInserted.length +" positions that are not inserted.",
		                 type: "warning",
		                 autoClose: true,
		                 duration: 3
	                });
	                for(var i in inserted){
	                	$(".position").each(function(){
							if($(this).val()==inserted[i]['value']){
								//all inserted positions will removed
								$(this).closest(".container-position").remove();
							}
						})
	                }
	                $(".minusPositionData").removeClass("display")
					$(".minusPositionData").eq(0).addClass("display")
	                // 
				}else{
					var grammar = "s are";
					if(positions.length==1){
						grammar = " is";
					}
					showNotification({
		                 message: "Position"+ grammar +" successfully saved",
		                 type: "success",
		                 autoClose: true,
		                 duration: 3
	                });
	                $(".fl-closeDialog").click()
				}
				if(type=="2"){
					//add selection in user registration position
					for(var i in inserted){
						$(".userRegPosition").append('<option value="'+ inserted[i]['id'] +'">'+ inserted[i]['value'] +'</option>')
	                }
				}else{
					$("#loadPosition").dataTable().fnDestroy();
        			self.triggerDataTable();
				}
        		ui.unblock();
			})
		})
	},
	editPosition : function(){
		var self = this;
		$("body").on("click",".editPosition",function(){
			var id = $(this).attr("data-id");
			var value = $.trim($(".position").val());
         var description = $.trim($(".position-desc").val());
			if(value==""){
				showNotification({
	                 message: "Please fill out required field",
	                 type: "error",
	                 autoClose: true,
	                 duration: 3
	            });
	            return;
			}
			ui.block();
			$.post("/ajax/position",{id:id,action:"editPosition",value:value,description:description},function(data){
				data = JSON.parse(data);
				if(data['status']==0){
					showNotification({
		                 message: "Please change your position, it is already in your database",
		                 type: "error",
		                 autoClose: true,
		                 duration: 3
	                });
				}else{
					showNotification({
		                 message: "Position has been successfully updated",
		                 type: "success",
		                 autoClose: true,
		                 duration: 3
	                });
					$("#loadPosition").dataTable().fnDestroy();
	        		self.triggerDataTable();
	        		$(".fl-closeDialog").click()
				}
				ui.unblock();
			})
		})
	},
	"defaultData" : function(callback){
		var oColReorder = {
            "iFixedColumns": 0
        };
        var oTable = $('#loadPosition').dataTable( {
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/position",
            "fnServerData": function(sSource, aoData, fnCallback) {
            	 aoData.push({"name": "limit", "value": jsonPositionData['limit']});
                aoData.push({"name": "search_value", "value": $.trim(jsonPositionData['search_value'])});
                aoData.push({"name": "action", "value": "loadPositionDatatable"});
                aoData.push({"name": "column-sort", "value": jsonPositionData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonPositionData['column-sort-type']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        // console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
            	var obj ='#loadPosition';
            	var listOfappContainer = '.fl-list-of-app-record-wrapper';
            	dataTable_widget(obj, listOfappContainer);
            	dataTableSetwidth(this.find('[field_name="createdBy"]'), 130);
             
            },
            fnDrawCallback : function(){
            	
            	set_dataTable_th_width([1]);
                setDatatableTooltip('.dataTable_widget')
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonPositionData['limit']),
        });
        return oTable;
    },
    addPositionData : function(){
    	$("body").on("click",".addPositionData",function(){
    		var html = $(this).closest(".container-position").clone();
    		$(".position-subcontainer").append(html);
    		html.find(".minusPositionData").removeClass("display");
    		html.find(".position").val("").focus();
    		$('#comment-container').perfectScrollbar("update");
    	})
    	$("body").on("click",".minusPositionData",function(){
    		$(this).closest(".container-position").remove();
    	})
    }
}