posting = {
    createPost: function(elements) {
        // $(".getPost").hashtags("hashTagPost");

        $(".ceatepost").on("click", function() {
	    /* Allow Notification */
	    
	    if ($("input[name='allow_announcement_notification']").is(':checked')) {
		var checkbox_notification = "1";
	    }else{
		var checkbox_notification = "0";
	    }
            var self = this;
            // Get All HashTag
            var getAllHashTag = $(".hashtag").map(function() {
                return $(this).html();
            }).get().join();
            var getAllTag = $(".getPost").closest(".mentionContainer").find(".mentionSelectedContainer").find("span").map(function() {
                return $(this).attr("data_id");
            }).get().join();
            // Get Post Value
            var cPost = $(".getPost").closest(".mentionContainer").find(".mentionHiddenText").val();
            var cPost_hashTag = cPost.replace(new RegExp('<span class="hashtag">', 'g'), "<a>").replace(new RegExp('</span>', 'g'), "</a>");

            var cPost = nl3br(cPost);
            if (!cPost.match(/(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?#([a-zA-Z0-9]+)/g)) {
                if (!cPost.match(/#([a-zA-Z0-9]+)#/g)) {
                    cPost = cPost.replace(/#([a-zA-Z0-9]+)/g, '<a class="hashtag-link">#$1</a>');
                } else {
                    cPost = cPost.replace(/#([a-zA-Z0-9]+)#([a-zA-Z0-9]+)/g, '<a class="hashtag-link">#$1</a>');
                }
            }
	    if ($.trim(cPost) != "") {
            cPost = convBreakLine(cPost);
            // cPost = replaceTagStr(cPost);
            var announcementType = $(".announcementType option:selected").val();
            if (announcementType == 0 || announcementType == 1) {
                var type = "1"; // Default Company Post
            } else {
                var type = "2"; // For Event Post
            }
            // Image Upload
            var file_name = $(this).attr("data-file");
            var files = $(this).attr("data-get-file");
            var folder_location = $(this).attr("data-location");
            var imageLength = $("." + files).length;
            var img = {};
            var postPrivacyType = $("#selectedPostChoice").attr("value");
            var postPrivacyIDs = $("#selectedPostChoice").attr("postIDs");
            $("." + files).each(function(eqindex) {
                img['name_' + eqindex] = $(this).attr("data-name");
            });

//            console.log(getAllTag);
//            alert('got tags');
//            return;

	    // Not Allowed space only
	    //console.log(cPost)
	    
		if (cPost != "") {
		    $(".loading").show();
		}
		if (cPost != "" || imageLength != 0) {
		    var json_encode = JSON.stringify(img);
		    $.post("/ajax/post", {action: "createPost", checkbox_notification:checkbox_notification, postPrivacyType: postPrivacyType, postPrivacyIDs: postPrivacyIDs, cPost: cPost, imagesGet: json_encode, type: type, getAllHashTag: getAllHashTag, folder_location: folder_location, getAllTag: getAllTag}, function(data) {
		       
			var obj = jQuery.parseJSON(data);
    
			$.each(obj, function(id, val) {
    
			    // POST JSON
			    var postDetails = obj[id].getpost.post;
			    var uploadedDetails = obj[id].getpost.uploadedImages;
			    var likeDetails = null;
			    var commentDetails = null;
			    var notification_result = obj[id].getpost.notification;
			    var postID = obj[id].getpost.postID;
			    var P = postDesign(postDetails, uploadedDetails, likeDetails, commentDetails);
    
    
			    $(P).fadeIn("slow").prependTo(".post-message");
			    $(P).fadeIn("slow").prependTo(".hashTag_post");
    
			    $("label.timeago").timeago();  // Time To go
			    $(".getPost").val(null);
			    $(".mentionHiddenText").val(null);
			    $(".getPostHash").val(null);
			    $("." + elements).html(null);
			    // Remove Email Checkbox
			    $("input[name='allow_announcement_notification']").attr("checked",false);
			    $(".getPost").css("height", "50px");
			    $(".getPostHash").css("height", "50px");
			    $(".getPost").closest(".mentionContainer").find(".mentionSelectedContainer").html("")
			    $(".loading").hide();
			    $.post("/ajax/request_viewer", {action: "getUsers"}, function(data) {
				data = JSON.parse(data);
				$(".post").eq(0).find(".reply").mention_post({
				    json: data
				});
    
				//  ervinne - trigger broadcast notification for new announcement
				$.event.trigger({
				    type: 'sendNotification',
				    sendTo: 'CURRENT_USER_GROUP',
				    notificationData: {
					notificationType: 'NEW_ANNOUNCEMENT',
					announcementId: postDetails.postID,
					mentionData: data,
					announcementHTML: P
				    }
				});
    
				if (getAllTag.length > 0) {
				    $.event.trigger({
					type: 'sendNotification',
					sendTo: getAllTag.split(','),
					notificationData: {
					    notificationType: 'USER_MENTIONED',
					}
				    });
				}
			    })
			    
			    // Sending Email
			    if (notification_result == "Successful" && checkbox_notification == "1") {
				var data_email = {action:"send_post_email",postID:postID,getAllTag: getAllTag,cPost:cPost,notification_result:notification_result,checkbox_notification:checkbox_notification, postPrivacyType: postPrivacyType, postPrivacyIDs: postPrivacyIDs};
				
				$.post("/ajax/post",data_email,function(data){
				    console.log(data)    
				});
			    }
			    
			    
			    // PopOver user information
			    $('#postUser_' + postDetails.postID).popover({
				html: true,
				content: function() {
				    return $('#popover_content_wrapper_' + postDetails.postID).html();
				}
			    });
    
			    // Remove images uploaded on the preview
			    $(".previewImagePostUpload").html(null);
			    $(".imagePreviewUpload").hide();
    
			    // On Image Slider
			    $('.flexslider_' + postDetails.postID).flexslider({
				controlNav: false
			    });
			    jQuery('.mycarousel').jcarousel();
						    $(".text").emotions();
    
			    $(".dataTip").tooltip({
				html: true,
				placement: "top"
			    });
			    $(".tip").tooltip({
				html: true,
				placement: "right"
			    });
			    $(".group_0_" + postDetails.postID).colorbox({rel: 'group_0_' + postDetails.postID, width: "75%", height: "80%"});
    
			});
		    });
    
		}

	    }

        });
    },
    loadPost: function() {
        var user_view = $("#user_url_view").val();
        if (pathname!="/user_view/announcements") {
            return;
        }
        //console.log(get_pathname)
        if (window.location.pathname == "/home" || window.location.pathname == user_view + "gi-dashboard-home" || window.location.pathname == user_view + "announcements") {
            $(".load_p").show();


            //setTimeout(function() {
                $.ajax({
                    type: "POST",
                    url: "/ajax/post",
                    dataType: 'json',
                    data: {
                        action: "loadPost"
                    },
                    cache: false,
                    success: function(result) {
					
                        //console.log(result);
		
                        if (result == "null" || result == null || result == "") {
                            $(".load_p").hide();
                        } else {
                            var obj = result;

                            var P = "";
                            $.each(obj, function(id, val) {

                                // POST JSON
                                var postDetails = obj[id].getpost.post;
                                var uploadedDetails = obj[id].getpost.uploadedImages;
                                var likeDetails = obj[id].getLike;
                                var commentDetails = obj[id].getcomment;

                                P += postDesign(postDetails, uploadedDetails, likeDetails, commentDetails);

                                // Json Body
                                $("body").data("post_" + postDetails.postID, obj[id]);

                                // PopOver user information
                                $('#postUser_' + postDetails.postID).popover({
                                    html: true,
                                    content: function() {
                                        return $('#popover_content_wrapper_' + postDetails.postID).html();
                                    }
                                });
                                $('.flexslider_' + postDetails.postID).flexslider({
                                    controlNav: false
                                });
                                if (commentDetails != null) {
                                    for (var a = 0; a < commentDetails.length; a++) {
                                        $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                            controlNav: false
                                        });
                                        $(".group_1_" + commentDetails[a].commentID).colorbox({rel: 'group_1_' + commentDetails[a].commentID, width: "75%", height: "80%"});
                                    }
                                }

                                jQuery('.mycarousel').jcarousel();
				
                                
                                $("img").error(function() {
                                    $(this).unbind("error").attr("src", "/images/error/broken.png");
                                });
                            });
                            $(".post-message").html(P);
                            $(".text").emotions();
                            $.each(obj, function(id, val) {

                                // POST JSON
                                var postDetails = obj[id].getpost.post;
                                var uploadedDetails = obj[id].getpost.uploadedImages;
                                var likeDetails = obj[id].getLike;
                                var commentDetails = obj[id].getcomment;
                                $(".group_0_" + postDetails.postID).colorbox({rel: 'group_0_' + postDetails.postID, width: "75%", height: "80%"});
                                if (commentDetails != null) {
                                    for (var a = 0; a < commentDetails.length; a++) {
                                        $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                            controlNav: false
                                        });
                                        $(".group_1_" + commentDetails[a].commentID).colorbox({rel: 'group_1_' + commentDetails[a].commentID, width: "75%", height: "80%"});
                                    }
                                }
                                $("label.timeago").timeago();  // Time To go
                                $(".dataTip").tooltip({
                                    html: true,
                                    placement: "top"
                                });
                                $(".tip").tooltip({
                                    html: true,
                                    placement: "right"
                                });
                            });

                            // Validation if post is greater than 5
                            if (obj.length >= 5) {

                                //var postID = obj[0].getpost.post.postID;
                                // Load More Actions
                                var post_ID = obj[obj.length - 1].getpost.post.postID;//result[0].getpost[0].postID;//result[result.length-1].getpost[result.length-1].postID;
                                var lastModified = obj[obj.length - 1].getpost.post.date_updated;
                                $(loadMore(post_ID, lastModified)).insertAfter(".updates");
                            }
                            $(".load_p").hide();
                            $.post("/ajax/request_viewer", {action: "getUsers"}, function(data) {
                                data = JSON.parse(data);
                                $(".input-reply").mention_post({
                                    json: data
                                });
                            })
                        }
                    }
                });
           // });
        }
    },
    
    loadCompanyEvent: function() {
        var user_view = $("#user_url_view").val();
        if (pathname!="/user_view/company_events") {
            return;
        }
        //console.log(get_pathname)
        if (window.location.pathname == user_view + "company_events") {
            $(".load_p").show();

            //setTimeout(function() {
                $.ajax({
                    type: "POST",
                    url: "/ajax/post",
                    dataType: 'json',
                    data: {
                        action: "loadCompanyEventList"
                    },
                    cache: false,
                    success: function(result) {
					
                        console.log(result)
                        if (result == "null" || result == null || result == "") {
                            $(".load_p").hide();
                        } else {
                            var obj = result;

                            var P = "";
                            $.each(obj, function(id, val) {

                                // POST JSON
                                var postDetails = obj[id].getpost.post;
                                var uploadedDetails = obj[id].getpost.uploadedImages;
                                var likeDetails = obj[id].getLike;
                                var commentDetails = obj[id].getcomment;

                                P += postDesign(postDetails, uploadedDetails, likeDetails, commentDetails);

                                

                                

                                // Json Body
                                $("body").data("post_" + postDetails.postID, obj[id]);


                                // PopOver user information
                                $('#postUser_' + postDetails.postID).popover({
                                    html: true,
                                    content: function() {
                                        return $('#popover_content_wrapper_' + postDetails.postID).html();
                                    }
                                });
                                $('.flexslider_' + postDetails.postID).flexslider({
                                    controlNav: false
                                });
                                if (commentDetails != null) {
                                    for (var a = 0; a < commentDetails.length; a++) {
                                        $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                            controlNav: false
                                        });
                                        $(".group_1_" + commentDetails[a].commentID).colorbox({rel: 'group_1_' + commentDetails[a].commentID, width: "75%", height: "80%"});
                                    }
                                }

                                jQuery('.mycarousel').jcarousel();
				
                                
                                $("img").error(function() {
                                    $(this).unbind("error").attr("src", "/images/error/broken.png");
                                });
                            });
			    
                            $(".post-event-message").html(P);
                            $(".text").emotions();
			    
                            $.each(obj, function(id, val) {

                                // POST JSON
                                var postDetails = obj[id].getpost.post;
                                var uploadedDetails = obj[id].getpost.uploadedImages;
                                var likeDetails = obj[id].getLike;
                                var commentDetails = obj[id].getcomment;
                                $(".group_0_" + postDetails.postID).colorbox({rel: 'group_0_' + postDetails.postID, width: "75%", height: "80%"});
                                if (commentDetails != null) {
                                    for (var a = 0; a < commentDetails.length; a++) {
                                        $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                            controlNav: false
                                        });
                                        $(".group_1_" + commentDetails[a].commentID).colorbox({rel: 'group_1_' + commentDetails[a].commentID, width: "75%", height: "80%"});
                                    }
                                }
                                $("label.timeago").timeago();  // Time To go
                                $(".dataTip").tooltip({
                                    html: true,
                                    placement: "top"
                                });
                                $(".tip").tooltip({
                                    html: true,
                                    placement: "right"
                                });
                            });
			    // $(".eventsUserWidget").perfectScrollbar();


                            // Validation if post is greater than 5
                            if (obj.length >= 5) {

                                //var postID = obj[0].getpost.post.postID;
                                // Load More Actions
                                var post_ID = obj[obj.length - 1].getpost.post.postID;//result[0].getpost[0].postID;//result[result.length-1].getpost[result.length-1].postID;
                                var lastModified = obj[obj.length - 1].getpost.post.date_updated;
                                $(loadMore(post_ID, lastModified)).insertAfter(".updates");
                            }
                            $(".load_p").hide();
			    $(".seeAllEvent").show();
                            $.post("/ajax/request_viewer", {action: "getUsers"}, function(data) {
                                data = JSON.parse(data);
                                $(".input-reply").mention_post({
                                    json: data
                                });
                            })
                        }
                    }
                });
           // });
        }

    },
    loadEvent: function() {
        var user_view = $("#user_url_view").val();
        //console.log(get_pathname)
        if (window.location.pathname == user_view + "home") {
            $(".load_pevent").show();


            //setTimeout(function() {
                $.ajax({
                    type: "POST",
                    url: "/ajax/post",
                    dataType: 'json',
                    data: {
                        action: "loadCompanyEvent"
                    },
                    cache: false,
                    success: function(result) {
					
                        if (result == "null" || result == null || result == "") {
                            // $(".load_p, .load_pevent").hide();
                            $(".load_p, .load_pevent .fl-loading-content").hide();
                            $(".eventsUserWidget").closest(".fl-widget").find(".fl-no-data-found").fadeIn();
                        } else {
                            var obj = result;

                            var P = "";
                            $.each(obj, function(id, val) {

                                // POST JSON
                                var postDetails = obj[id].getpost.post;
                                var uploadedDetails = obj[id].getpost.uploadedImages;
                                var likeDetails = obj[id].getLike;
                                var commentDetails = obj[id].getcomment;

                                P += loadEvent(postDetails, uploadedDetails, likeDetails, commentDetails);

                                

                                

                                // Json Body
                                $("body").data("post_" + postDetails.postID, obj[id]);


                                // PopOver user information
                                $('#postUser_' + postDetails.postID).popover({
                                    html: true,
                                    content: function() {
                                        return $('#popover_content_wrapper_' + postDetails.postID).html();
                                    }
                                });
                                $('.flexslider_' + postDetails.postID).flexslider({
                                    controlNav: false
                                });
                                if (commentDetails != null) {
                                    for (var a = 0; a < commentDetails.length; a++) {
                                        $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                            controlNav: false
                                        });
                                        $(".group_1_" + commentDetails[a].commentID).colorbox({rel: 'group_1_' + commentDetails[a].commentID, width: "75%", height: "80%"});
                                    }
                                }

                                jQuery('.mycarousel').jcarousel();
				
                                
                                $("img").error(function() {
                                    $(this).unbind("error").attr("src", "/images/error/broken.png");
                                });
                            });
			    
                            $(".eventsUserWidget").html(P);
                            $(".text").emotions();
			    
                            $.each(obj, function(id, val) {

                                // POST JSON
                                var postDetails = obj[id].getpost.post;
                                var uploadedDetails = obj[id].getpost.uploadedImages;
                                var likeDetails = obj[id].getLike;
                                var commentDetails = obj[id].getcomment;
                                $(".group_0_" + postDetails.postID).colorbox({rel: 'group_0_' + postDetails.postID, width: "75%", height: "80%"});
                                if (commentDetails != null) {
                                    for (var a = 0; a < commentDetails.length; a++) {
                                        $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                            controlNav: false
                                        });
                                        $(".group_1_" + commentDetails[a].commentID).colorbox({rel: 'group_1_' + commentDetails[a].commentID, width: "75%", height: "80%"});
                                    }
                                }
                                $("label.timeago").timeago();  // Time To go
                                $(".dataTip").tooltip({
                                    html: true,
                                    placement: "top"
                                });
                                $(".tip").tooltip({
                                    html: true,
                                    placement: "right"
                                });
                            });
			    $(".eventsUserWidgetWrapper").perfectScrollbar("update");


                            // Validation if post is greater than 5
                            if (obj.length >= 5) {

                                //var postID = obj[0].getpost.post.postID;
                                // Load More Actions
                                var post_ID = obj[obj.length - 1].getpost.post.postID;//result[0].getpost[0].postID;//result[result.length-1].getpost[result.length-1].postID;
                                var lastModified = obj[obj.length - 1].getpost.post.date_updated;
                                $(loadMore(post_ID, lastModified)).insertAfter(".updates");
                            }
                            $(".load_pevent").hide();
			    $(".seeAllEvent").show();
                            //$.post("/ajax/request_viewer", {action: "getUsers"}, function(data) {
                            //    data = JSON.parse(data);
                            //    $(".input-reply").mention_post({
                            //        json: data
                            //    });
                            //})
                        }
                    }
                });
           // });
        }

    },
    loadText: function() {
        $("body").on("click", ".loadText", function() {
            var id = $(this).attr("data-post-id");
            var actionType = $(this).attr("data-action");
            $("#loading_" + id).show();
            setTimeout(function() {
                $.post("/ajax/post", {action: "loadText", actionType: actionType, id: id}, function(data) {
                    // data = replaceTagStr(replaceStrHashtags_decode(htmlEntities(nl2br(replaceStrHashtags_encode(data)))));
                    data = replaceTagStr(replaceStrHashtags_decode(nl3br(replaceStrHashtags_encode(data))));
                    $("#postText_" + id + " > p").html(data);
                    $("#loading_" + id).show();
                });
            }, 2000);
            $(".text").emotions();
        });
    },
    deletePost: function() {
        $("body").on("click", ".delete", function() {
            postID = $(this).attr("data-id");
            delAction = $(this).attr("data-action");
            var del = "";
            if (delAction == "deletePost") {
                del = "#post_";
            } else {
                if (delAction == "deleteComment") {
                    del = "#reply_";
                }
            }
            con = "Are you sure you want to delete this?";
            var newConfirm = new jConfirm(con, 'Confirmation Dialog', '', '', '', function(r) {
                if (r == true) {
                    $.post("/ajax/post", {action: "delete", delAction: delAction, postID: postID}, function(data) {
		    console.log(data)
                    });
                    $(del + postID).animate({backgroundColor: "whiteSmoke"}, "fast")
                            .animate({opacity: "hide"}, "slow");
                }

            });
            newConfirm.themeConfirm("confirm2",{'icon':'<i class="fa fa-question-circle fl-margin-right" style="color:#f00; font-size:15px;"></i>'})
        });
    },
    showReply: function(elements, attribute) {
        var ajax_req_comm =  null;
        $("body").on("click.showReply", elements, function(event) {
            

            postID = $(this).attr("data-id");
            var type = $(this).attr("data-type");
            var fID = $(this).attr("data-f-id");
            var ret = "";
            switch (type) {
                case "1":
                  /*  ret += '<h3 class="fl-margin-bottom fl-add-comment-here"><i class="icon-comment fa fa-comment"></i> Add Your Comment Here</h3>';
                    ret += '<div class="hr"></div>';
                    ret += '<div class="fields_below">';
                    ret += '<div class="label_below"> </div>';
                    ret += '<div class="input_position_below">';
                    ret += '<div id="textarea-popup-comment" style="position: relative; margin: 0px auto; padding: 0px; width: 100%;max-height:150px;min-height:60px;overflow: hidden;" class="ps-container">';
                    ret += '<textarea placeholder="Your comment here" id="comment_' + postID + '_' + type + '" class="input-announcement comment-popup comment_' + postID + '_' + type + '" style="border:none!important;width:100%;resize:none"></textarea>';
                    ret += '</div>';
                    ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields">';
                    ret += '<div style="float:left;margin-top:5px;">';
                    if(window.location.pathname!="/user_view/workspace"){
                        //ret += '<div class="fl-createViewreq fl-buttonEffect fa fa-paperclip btn-basicBtn padding_5 cursor tip fl-margin-right" data-original-title="Add Files">';
                        //ret += '<form id="postFile" method="post" enctype="multipart/form-data" action="/ajax/uploadFile">';
                        //ret += '<input type="file" class="fl-input-file" data-action-id="2" data-get-file="imagePost_' + postID + '_' + type + '" value="upload" name="postFile" id="uploadForm" data-form="postFile" data-show="imagePreviewUpload_' + postID + '_' + type + '" data-location="previewImagePostUpload_' + postID + '_' + type + '" size="24" data-action-type="postFile" style="width: 58px;opacity: 0;position:absolute;" class="cursor postFile">';
                        //ret += '<input type="hidden" value="postFileUpload" name="uploadType">';
                        //ret += '<input type="hidden" value="commentAttachment" name="uploadFolder">';
                        //ret += '<i class="icon-paperclip"></i>';
                        //ret += '</form>';
                        //ret += '</div>';
                    }
                    ret += '<img src="/images/loader/load.gif" class="postFileLoad display" style="margin-left: 5px;margin-top:6px;"/>';
                    ret += '</div>';
                    ret += '<div style="float:right;margin-top:5px; margin-right:13px;">';
                    ret += '<input type="button" class="btn-blueBtn fl-adding-comment fl-margin-right" data-location="commentAttachment" data-get-file="imagePost_' + postID + '_' + type + '" data-file="imagePreviewUpload_' + postID + '_' + type + '" data-type="1"  id="reply" data-f-id="' + fID + '" data-id="' + postID + '" value="Add Comment" style="width:113px; margin-right:0px;">';
                    ret += ' <input type="button" class="fl-margin-right btn-basicBtn" id="popup_cancel" value="Cancel">';
                    ret += '</div>';
                    ret += '</div>';
                    // Post Selected Photos
                    ret += '<div class="fl-above-addfiles" style="padding: 5px;">';
                    ret += '<div class="imagePreviewUpload_' + postID + '_' + type + ' display" style="float: left;width: 100%;border: 1px solid #D5D5D5;margin-top:10px;background-color: #fff;">';
                    ret += '<div style="padding: 5px;" class="previewImagePostUpload_' + postID + '_' + type + '">';

                    ret += '</div>';
                    ret += '</div>';
                    ret += '</div>';
                    ret += '<div id="comment-container" style="position: relative; margin: 0px auto; top:10px; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">'
                    ret += '<div class="content-dialog sub-container-comment requestComment" style="height: auto;">';
                    ret += '<center><img src="/images/loader/loader.gif" class="load-mr " style="margin-top:20%;"></center>';
                    ret += '</div>';
                    ret += '</div>';
                   var newDialog = new jDialog(ret, "", "", "470", "70", function() {
                    });
                        newDialog.themeDialog("modal2");*/

                     if ($(this).attr('active-comment') === "false") {
                     
                            $(this).attr('active-comment', "true");
                            $(this).attr('style', $(this).attr('style') + ";background-color: rgb(207, 219, 226) !important;");
                            $(this).find('i.fa-comment').attr('style', $(this).find('i.fa-comment').attr('style') + ";color:#6E6E6E !important");

                            if( ajax_req_comm != null ){
                                ajax_req_comm.abort();
                            }
                            
                            ajax_req_comm = $.ajax({
                                type: "POST",
                                url: "/ajax/post",
                                dataType: "json",
                                cache: false,
                                data: "action=getComment" + "&postID=" + postID + "&formID=" + fID,
                                success: function(result) {
                                    if (result) {
                                        for (var a = 0; a < result.length; a++) {
                                            var postDetails = null;
                                            var uploadedDetails = null;
                                            var likeDetails = null;
                                            var comments = result[a];
                                            var showComment = replyPost(postDetails, uploadedDetails, likeDetails, comments);
                                            
                                            $(".requestComment").append(showComment);
                                            
                                            $("label.timeago").timeago();  // Time To go
                                            $(".load-mr").hide();
                                            if (comments != null) {
                                                //console.log($('.flexslider_1_' + comments.commentID).flexslider())
                                                //console.log(comments.commentID);
                                                $('.flexslider_1_' + comments.commentID).flexslider({
                                                    controlNav: false
                                                });
                                            }
                                            console.log("success", showComment);
                                            jQuery('.mycarousel').jcarousel();
                                            $(".text").emotions();
                                        }

                                    } else {
                                        $(".load-mr").hide();
                                    }
                                    $(".dataTip").tooltip({
                                        html: true,
                                        placement: "right"
                                    });
                                    
                                    $("#comment-container").perfectScrollbar();
                                    $("#textarea-popup-comment").perfectScrollbar(); 
                                    //console.log("================================");
                                    $.post("/ajax/request_viewer", {action: "getUsers"}, function(data) {
                                        data = JSON.parse(data);
                                        $(".comment-popup").mention_post({
                                            json: data
                                        });
                                    })
                                }
                            });
                            UiworkspaceReply(ret, postID, type, fID);


                       }else if ($(this).attr('active-comment') === "true") {
                             
                             $(this).attr('active-comment', "false");
                             $(this).removeAttr('style');
                             $(this).find('i.fa-comment').removeAttr('style');
                             $('.fl-ui-request-comment').remove();           
                       };
                    

                default:
                    $(attribute + postID + "_" + type).fadeIn('slow').show();
                    $(".attachment_comment_" + postID + "_" + type).show();
                    //$('html, body').animate({
                    //        scrollTop: $(attribute+postID).offset().top
                    //    });
                    $(attribute + postID + "_" + type).focus();
            }

        
        });
       
        function UiworkspaceReply(ret, postID, type, fID){

            var ret = "";
            ret += '<div class="fl-ui-request-comment" style=" width:419px; position: absolute; z-index: 1; background-color: #fff;top: 34px;   box-shadow: 1px 1px 10px #bbb; border-bottom-left-radius:3px; border-bottom-right-radius:3px;">';
                ret += '<p class="fl-add-comment-here" style="margin:5px; margin-left:14px;  color: #6E6E6E;"><i class="icon-comment fa fa-comment"></i> Add Your Comment Here</p>';
                ret += '<div class="fields_below" style="padding-left: 14px;">';
       
                ret += '<div class="input_position_below">';
                ret += '<textarea placeholder="Your comment here" id="comment_' + postID + '_' + type + '" class="input-announcement comment-popup comment_' + postID + '_' + type + '" style="width:100%;resize:none"></textarea>';
                ret += '</div>';  
                ret += '</div>';  

                ret += '<div id="fl-workspace-comment-btn-wrapper" style="padding: 5px 0px 0px 5px; height: 38px; z-index:5; position:relative;">';
                ret += '<input type="button" class="btn-blueBtn fl-adding-comment fl-margin-right" data-location="commentAttachment" data-get-file="imagePost_' + postID + '_' + type + '" data-file="imagePreviewUpload_' + postID + '_' + type + '" data-type="1"  id="reply" data-f-id="' + fID + '" data-id="' + postID + '" value="Add Comment" style="width:113px; margin-right:0px; margin-bottom:10px; margin-left:8px;">';
                
                ret += '</div>';    

                ret += '<div id="comment-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">'
                ret += '<div class="content-dialog sub-container-comment requestComment" style="height: auto; margin:0px !important; padding:5px !important;">';
                ret += '<center><img src="/images/loader/loader.gif" class="load-mr " style="margin-top:20%;"></center>';
                ret += '</div>';
                ret += '</div>';
            ret += '</div>';    
            $(ret).appendTo('#fl-form-basic-options');


            var comment_container_scroll = $('#comment-container');
            
            $(comment_container_scroll).scroll(function(){
               
                var comment_container_scroll = $(this).scrollTop();
                
                if (comment_container_scroll > 5) {

                    $(this).parents('.fl-ui-request-comment').find('#fl-workspace-comment-btn-wrapper').css('box-shadow', '0px 3px 8px -5px black');

                }else if (comment_container_scroll < 5) {
                    $(this).parents('.fl-ui-request-comment').find('#fl-workspace-comment-btn-wrapper').css('box-shadow', '');
                };

            });

            $('.mentionSearchChoice').css({'width': '390px'});

        }     

    },
    reply: function() {
        $("body").on("keypress", ".reply", function(e) {

            if (e.keyCode == 13 && !e.shiftKey) {
                e.preventDefault();
                //
                replyComment(this, "");
                //
            }
        });
    },
    likePost: function() {
        $("body").on("click", ".likePost", function() {
            postID = $(this).attr("data-id");
            likeType = $(this).attr("data-type");
            likeID = $(this).attr("data-like-id");
            authorId = $(this).attr("author-id");

	    if (likeType == "reply") {
		var lType = "Unlike Comment";
	    }else{
		var lType = "Unlike Post";
	    }
	    
            $(".likes_" + postID + "_" + likeType).html('<span id="you_' + postID + '">' +
                    '<a href="#">You</a></span> like this.');
            $(".like_" + postID + "_" + likeType).text("");
	    $(".like_" + postID + "_" + likeType).toggleClass('fa-thumbs-o-down fa-thumbs-o-up');
	    //$(".like_" + postID + "_" + likeType).addClass('fa-thumbs-o-up');
            $(".like_" + postID + "_" + likeType).toggleClass("likePost unlikePost");
	    
	    if (likeType == "reply") {
		var lType = "Unlike Comment";
	    }else{
		var lType = "Unlike Post";
	    }
	    
            $(".like_" + postID + "_" + likeType).attr("data-original-title", lType);
            $(".tooltip-inner").text(lType);

            $.post("/ajax/post", {action: "like", likeAction: "likePost", postID: postID, likeID: likeID, likeType: likeType}, function(data) {
                console.log(data);
                var obj = jQuery.parseJSON(data);
                console.log(obj);

                $.event.trigger({
                    type: 'sendNotification',
                    sendTo: [authorId],
                    notificationData: {
                        notificationType: 'NEW_LIKE',
                    }
                });
            });
        });
    },
    unlikePost: function() {
        $("body").on("click", ".unlikePost", function() {
            postID = $(this).attr("data-id");
            likeID = $(this).attr("data-like-id");
            likeType = $(this).attr("data-type");
	    
            $(".likes_" + postID + "_" + likeType).html('<span id="you_' + likeID + '">' +
                    '<a href="#">You</a></span> unlike this.');
            $(".like_" + postID + "_" + likeType).text("");
	    $(".like_" + postID + "_" + likeType).toggleClass('fa-thumbs-o-up fa-thumbs-o-down');
	    //$(".like_" + postID + "_" + likeType).addClass('fa-thumbs-o-down');
            $(".like_" + postID + "_" + likeType).toggleClass("unlikePost likePost");
	    
	    if (likeType == "reply") {
		var lType = "Like Comment";
	    }else{
		var lType = "Like Post";
	    }
	    
            $(".like_" + postID + "_" + likeType).attr("data-original-title", lType);
            $(".tooltip-inner").text(lType);


            $.post("/ajax/post", {action: "like", likeAction: "unlikePost", postID: postID, likeID: likeID, likeType: likeType}, function(data) {

            });
        });
    },
    loadMore: function() {
        //$("body").on("click",".more",function(){
        $(window).scroll(function() {
            
            var scroll_diff = $(window).scrollTop() - ($(document).height() - $(window).height());
            // console.log(scroll_diff)
            // console.log((scroll_diff+" >= "+(-1))+" && "+(scroll_diff+" <= "+1))
            if ((scroll_diff >= -2) && (scroll_diff <= 2)) {
                //if ($(window).scrollTop() == $(document).height() - $(window).height()){
                var lastID = $(".more").attr("data-id");
                var date_modified = $(".more").attr("data-date");
                if (lastID) {
                    //$(".more").html('<img src="/images/loader/loading.gif" style="float:left;text-align:center;margin-top: -5px;"/>');
                    $("#moreLbl_" + lastID).hide();
                    $("#moreload_" + lastID).show();
                    $.ajax({
                        type: "POST",
                        url: "/ajax/post",
                        dataType: 'json',
                        cache: false,
                        data: "action=loadMore" + "&lastID=" + date_modified,
                        success: function(obj) {
                            //console.log(obj)
                            if (obj != null) {
                                // Load More Content of post
                                $.each(obj, function(id, val) {

                                    // POST JSON
                                    var postDetails = obj[id].getpost.post;
                                    var uploadedDetails = obj[id].getpost.uploadedImages;
                                    var likeDetails = obj[id].getLike;
                                    var commentDetails = obj[id].getcomment;

                                    var P = postDesign(postDetails, uploadedDetails, likeDetails, commentDetails);

                                    $(".post-message").append(P);    
                
                                    $("label.timeago").timeago();  // Time To go


                                    // PopOver user information
                                    $('#postUser_' + postDetails.postID).popover({
                                        html: true,
                                        content: function() {
                                            return $('#popover_content_wrapper_' + postDetails.postID).html();
                                        }
                                    });
                                    $('.flexslider_' + postDetails.postID).flexslider({
                                        controlNav: false
                                    });
                                    if (commentDetails != null) {
                                        for (var a = 0; a < commentDetails.length; a++) {
                                            $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                                controlNav: false
                                            });
                                            $(".group_1_" + commentDetails[a].commentID).colorbox({rel: 'group_1_' + commentDetails[a].commentID, width: "75%", height: "80%"});
                                        }
                                    }
                                    jQuery('.mycarousel').jcarousel();
                                    $(".text").emotions();
                                    $(".dataTip").tooltip({
                                        html: true,
                                        placement: "top"
                                    });
                                    $(".tip").tooltip({
                                        html: true,
                                        placement: "right"
                                    });
                                    $("img").error(function() {
                                        $(this).unbind("error").attr("src", "/images/error/broken.png");
                                    });
                                    $(".group_0_" + postDetails.postID).colorbox({rel: 'group_0_' + postDetails.postID, width: "75%", height: "80%"});
                                });
                                //$(P).appendTo(".post:last");
                                //$(P).insertAfter(".post:last");
                                //$(".post-message:last-child").show('slow').append(P);
                                
                                // Load More Actions
                                var post_ID = obj[obj.length - 1].getpost.post.postID;//result[0].getpost[0].postID;//result[result.length-1].getpost[result.length-1].postID;
                                var lastModified = obj[obj.length - 1].getpost.post.date_updated;
                                $(loadMore(post_ID, lastModified)).insertAfter(".updates");

                            }
                            $("#more_" + lastID).remove();
                            $.post("/ajax/request_viewer", {action: "getUsers"}, function(data) {
                                data = JSON.parse(data);
                                $(".input-reply").mention_post({
                                    json: data
                                });
                            })

                        }
                    });
                }
            }
        });

        //});
    },
    loadComment: function() {
        $("body").on("click", ".loadComment", function() {

            var lastID = $(this).attr("data-id");
            var postID = $(this).attr("post-id");
            $("#loading_" + lastID).show();

            setTimeout(function() {
                $.ajax({
                    type: "POST",
                    url: "/ajax/post",
                    dataType: 'json',
                    cache: false,
                    data: "action=loadComment" + "&lastID=" + lastID + "&postID=" + postID,
                    success: function(comments) {

                        // Comment View

                        $.each(comments, function(id, val) {

                            var commentDetails = comments[id].getcomment;
                            $.each(commentDetails, function(id, val) {
                                var comm = commentDetails[id];
                                var C = replyPost(null, null, null, comm);
                                $(C).insertAfter(".commentView_" + postID + " .reply:last");
                            });
                            //if("1"==postID){
                            //var C = replyPost(null,null,null,commentDetails);
                            //var C = replyPost(commentID,img,comment,commentBy,datePosted,postID,likes,authID,postedID,"");

                            //}
                            $("label.timeago").timeago();  // Time To go
                            $(".dataTip").tooltip({
                                html: true,
                                placement: "top"
                            });
                            $(".text").emotions();
                            $("img").error(function() {
                                $(this).unbind("error").attr("src", "/images/error/broken.png");
                            });
                            $("#loadCommentMore_" + lastID).remove();
                            $("#loading_" + lastID).hide();
                            if (commentDetails != null) {
                                for (var a = 0; a < commentDetails.length; a++) {
                                    $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                        controlNav: false
                                    });
                                }
                            }
                        });
                    }
                });
            }, 2000);
        });
    },
    uploadFile: function(elements) {
        $("body").on("change", elements, function() {	
            var actionID = $(this).attr("data-action-id");
            var actionType = $(this).attr("data-action-type");
            $(".postFileLoad").show();
            var location = $(this).attr("data-location");
            var loc_show = $(this).attr("data-show");
            var formName = $(this).attr("data-form");
            var files = $(this).attr("data-get-file");
            var value_img = $(this).val();
            var ret = "";
            $("#uploadFilename_" + actionID).html(value_img);
            if (actionType == "postFile") {

                $("#" + formName).ajaxForm(function(data) {
                    if (data == "Invalid File Format") {
                        showNotification({
                            message: data,
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                        $(".filename").html("No file selected");
                        $(".postFileLoad").hide();
                        $("#uploadFilename_" + actionID).html("No file selected");
                    } else if (data == "File too large. File must be less than 4 megabytes.") {
                        showNotification({
                            message: data,
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                        $(".filename").html("No file selected");
                        $(".postFileLoad").hide();
                        $("#uploadFilename_" + actionID).html("No file selected");
                    } else if (data == "It seems that you have uploaded a nude picture.") {
                        showNotification({
                            message: data,
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                        $(".filename").html("No file selected");
                        $(".postFileLoad").hide();
                        $("#uploadFilename_" + actionID).html("No file selected");
                    } else {
                        var json = $.parseJSON(data);
                        //console.log(json)
                        if (json[0].successful == "Your new photos was successfully uploaded.") {
                            $("." + loc_show).show();
                            if (json[0].extension == "image") {
								if (json[0].image) {
									//	add a '/' if the image url does not start with it
									if (json[0].image.indexOf('/') > 0) {
										json[0].image = json[0].image;
									}
								}
							
                                ret += "<div class='pull-left' style='margin-left:5px;'><img src='" + json[0].image + "' data-name='" + json[0].name + "' data-original-title='" + json[0].filename + "' title='" + json[0].filename + "' width='50' height='50' class='tip " + files + "  userPhoto personalPhoto userAvatar' onerror='this.src="+ $('#broken_image').text() +"'  />";
                                ret += "<div class='postImageRemove cursor tip' data-original-title='Remove this' style='position: relative;top: -61px; right: -38px; background-color: whitesmoke; width: 15px; text-align: center;'><i class='fa fa-times icon-remove'></i></div></div>";
                            } else {
                                ret += '<div class="pull-left" style="margin-left:5px;"><a class="cursor apps ' + files + ' tip" data-name="' + json[0].name + '" data-original-title="' + json[0].filename + '" title="' + json[0].filename + '"><div style="width:50px;height:50px;text-align:center;"><i class="fa fa-file  icon-file" style="font-size: 55px;"></i></div></a>';
                                ret += '<div class="postImageRemove cursor tip" data-original-title="Remove this" style="position: relative;top: -49px; right: -30px; background-color: whitesmoke; width: 15px; text-align: center;"><i class="fa fa-times icon-remove"></i></div></div>';
                            }
                            $("." + location).append(ret);
                            $(".postFileLoad").hide();
                            $("#uploadFilename_" + actionID).html("No file selected");
                            
                            $.event.trigger({
                                type: "file_uploaded",
                                uploadedFileData: json
                            });
                        }
                        $(".tip").tooltip();
                    }
                }).submit();
            }
        });

        // Delete Image
        $("body").on("click", ".postImageRemove", function() {
            $(this).tooltip("destroy");
            $(this).parent().remove();
            //console.log($(".previewImagePostUpload > div").length);
            if ($(".previewImagePostUpload > div").length == 0) {
                
                $(".imagePreviewUpload").hide();
            }
        });
    },
    // View HashTag
    hashTag_view: function() {

        $("body").on("mouseenter", ".post > .message-body > .text > p > a", function() {
            $(this).addClass("cursor");
        });


        $("body").on("click", ".post > .message-body > .text > p > a", function() {
            var data_hashTag = $(this).html();
            var h = data_hashTag.substring(0, 1);
            var hashTag = data_hashTag.substring(1);
            var pathname = window.location.pathname;
            var user_view = $("#user_url_view").val();
            var split_pathname = pathname.split("/");
            if (split_pathname[2]=="announcements" || split_pathname[2]=="hashtag") {
                var path = user_view;
            }else{
                var path = "/";
            }
            if (h == "#") {
                window.location.replace(path + "hashtag?view_type=view_hashtag&hashTag=" + hashTag);
            }
            //
            //var data_hashTag = $(this).html();
            //var h = data_hashTag.substring(0, 1);
            //if (h=="#") {
            //    $(".reply").val(null); //  remove value of the reply
            //    
            //    $(".load-mr").show();
            //    var ret = '<h3><i class="icon-comment"></i> ' + data_hashTag + '</h3>';
            //    ret += '<div class="hr"></div>';
            //    ret += '<div class="jqueryHashtags"><div class="highlighter"></div><div class="typehead"><textarea placeholder="Whats the update?" name="getPostHash" class="input-announcement getPostHash theSelector" style="overflow: hidden; word-wrap: break-word; resize: none; height: 50px;"></textarea></div></div>';
            //    ret += '<form id="postFile" method="post" enctype="multipart/form-data" action="/ajax/uploadFile">';
            //            ret += '<div class="postActions" style="margin-left: 5px;margin-top: 5px;">';
            //               ret += '<div id="uniform-fileInput" class="uploader">';
            //                  ret += '<input type="file" data-action-id="2" value="upload" name="postFile" id="uploadForm" size="24" data-action-type="postFile" style="opacity: 0;" class="postFile">';
            //                  ret += '<span id="uploadFilename_2" class="filename">No file selected</span>';
            //                  ret += '<span class="action">Choose File</span>';
            //                  ret += '<input type="hidden" value="postFileUpload" name="uploadType">';
            //               ret += '</div>';
            //              ret +=  '<img src="/images/loader/loader.gif" class="postFileLoad pull-left display" style="margin-left: 5px;margin-top:6px;">';
            //            ret += '</div>';
            //         ret += '</form>';
            //    ret += '<div class="btn_post_holder" style="float:right;">';
            //       ret += '<input type="submit" class="btn-blueBtn ceatepost" value="POST">';
            //    ret += '</div>';
            //    // IMAGE
            //    ret += '<div style="padding: 5px;">';
            //       ret += '<div class="imagePreviewUpload display" style="float: left;width: 100%;border: 1px solid #D5D5D5;margin-top:10px;background-color: #fff;"> ';
            //          ret += '<div style="padding: 5px;" class="previewImagePostUpload">';
            //
            //          ret += '</div>';
            //       ret += '</div>';
            //    ret += '</div>';
            //    ret += '<div class="pull-left length12">';
            //       ret += '<div class="padding_5">';
            //          ret += '<div class="divider"><span></span></div>';
            //       ret += '</div>';
            //    ret += '</div>';
            //         
            //    ret += '</div>';
            //    ret +=	'<div id="hashTagView" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">'
            //    ret +=        '<div class="content-dialog hashTagView" style="width: 95%;height: auto;">';
            //    ret +=            '<center><img src="/images/loader/loader.gif" class="load-mr " style="margin-top:20%;"></center>';
            //    ret +=        '</div>';
            //    ret +=    '</div>';
            //    ret += '<div class="fields" style="border-top:1px solid #ddd;">';
            //    ret += '<div class="label_basic"></div>';
            //    ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
            //    ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
            //    ret += '</div>';
            //    ret += '</div>';
            //    jDialog(ret, "","600", "", "", function(){});
            //        $.ajax({
            //            type : "POST",
            //            url  : "/ajax/post",
            //            data : {action:"getHashTag",data_hashTag:data_hashTag},
            //            dataType : "json",
            //            cache: false,
            //            success : function(result){
            //                if(result=="null"||result==null||result==""){
            //                    $(".load_p").hide();
            //                }else{
            //                    var obj = result;
            //                    $.each(obj, function(id,val){
            //                    
            //                        // POST JSON
            //                            var postDetails     = obj[id].getpost.post;
            //                            var uploadedDetails = obj[id].getpost.uploadedImages;
            //                            var likeDetails     = obj[id].getLike;
            //                            var commentDetails  = obj[id].getcomment;
            //                           //console.log(obj[id]);
            //                            var P = postDesign(postDetails,uploadedDetails,likeDetails,commentDetails);
            //                            $(".hashTag_post").append(P);
            //                            $("label.timeago").timeago();  // Time To go
            //                            
            //                            // PopOver user information
            //                            $('#postUser_' + postDetails.postID).popover({ 
            //                                html : true, 
            //                                content: function() {
            //                                      return $('#popover_content_wrapper_' + postDetails.postID).html();
            //                                    }                                    
            //                            });
            //                            $(".load-mr").hide();
            //                                                
            //                            // Remove images uploaded on the preview
            //                            $(".previewImagePostUpload").html(null);
            //                            $(".imagePreviewUpload").hide();
            //                            
            //                            // On Image Slider
            //                            $('.flexslider_' + postDetails.postID).flexslider({
            //                                        controlNav: false
            //                                    });
            //                            if (commentDetails!=null) {
            //                                for (var a=0; a<commentDetails.length;a++) {
            //                                    $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
            //                                        controlNav: false
            //                                    });
            //                                }
            //                            }
            //                            jQuery('.mycarousel').jcarousel();
            //                            $(".dataTip").tooltip({
            //                                html : true,
            //                                placement : "top"
            //                            });
            //                            $(".tip").tooltip({
            //                                html : true,
            //                                placement : "right"
            //                            });
            //                            $("img").error(function () {
            //                                $(this).unbind("error").attr("src", "/images/error/broken.png");
            //                            });
            //                    });
            //                    $("#hashTagView").perfectScrollbar();
            //                }
            //                
            //            }
            //        });
            //            
            //        $(".getPostHash").val(data_hashTag);    
            //        $(".getPostHash").hashtags("gethashTagPost");
            //        posting.createPost("gethashTagPost");
            //            
            //    
            //}    

        });

    },
    // Selected Post
    selected_post: function() {
	var pathname = window.location;
	var get_pathname = window.location.pathname;
        if (get_pathname!="/user_view/post") {
            return;
        }
        
        var user_view = $("#user_url_view").val();
        
        //console.log(get_pathname)
	
        if (get_pathname == "/post" || get_pathname == user_view + "post") {
            //code
           
            if (getParametersName("view_type", pathname) == "view_post") {
                var postID = getParametersName("postID", pathname);
                $(".load_p").show();
     
                setTimeout(function() {
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "/ajax/post",
                        data: {action: "selected_post", postID: postID},
                        success: function(result) {
                            //console.log(result)
                            if (result == "null" || result == null || result == "") {
                                $(".load_p").hide();
                                $(".selected_post").html("This content is currently unavailable.");
                            } else {
                                var obj = result;
                                //console.log(obj)
                                $.each(obj, function(id, val) {
    
                                    // POST JSON
                                    var postDetails = obj[id].getpost.post;
                                    var uploadedDetails = obj[id].getpost.uploadedImages;
                                    var likeDetails = obj[id].getLike;
                                    var commentDetails = obj[id].getcomment;
                                    var P = postDesign(postDetails, uploadedDetails, likeDetails, commentDetails);
    
                                    $(".selected_post").append(P);
    
                                    $("label.timeago").timeago();  // Time To go
    
    
                                    // PopOver user information
                                    $('#postUser_' + postDetails.postID).popover({
                                        html: true,
                                        content: function() {
                                            return $('#popover_content_wrapper_' + postDetails.postID).html();
                                        }
                                    });
                                    $('.flexslider_' + postDetails.postID).flexslider({
                                        controlNav: false
                                    });
                                    if (commentDetails != null) {
                                        for (var a = 0; a < commentDetails.length; a++) {
                                            $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                                controlNav: false
                                            });
                                            $(".group_1_" + commentDetails[a].commentID).colorbox({rel: 'group_1_' + commentDetails[a].commentID, width: "75%", height: "80%"});
                                        }
    
                                    }
                                    $.post("/ajax/request_viewer", {action: "getUsers"}, function(data) {
                                        data = JSON.parse(data);
                                        $(".input-reply").mention_post({
                                            json: data
                                        });
                                    })
                                    jQuery('.mycarousel').jcarousel();
                                                                    $(".text").emotions();
                                    $(".dataTip").tooltip({
                                        html: true,
                                        placement: "top"
                                    });
                                    $(".tip").tooltip({
                                        html: true,
                                        placement: "right"
                                    });
                                    $("img").error(function() {
                                        $(this).unbind("error").attr("src", "/images/error/broken.png");
                                    });
                                    $(".group_0_" + postDetails.postID).colorbox({rel: 'group_0_' + postDetails.postID, width: "75%", height: "80%"});
                                    
                                    
                                });
                                $(".load_p").hide();
                                setHighlightComment.init();
                            }
                        }
                    });
                });
            }
        }
    },
    seen_post: function() {
        $(window).scroll(function() {
            //var position = $(".post").offset();
            var post = $(".post").eq(0).position();
            var fix = $(".fixed, .fl-user-header-wrapper");
            var fix_ele = fix.offset();
            $(".post").each(function(id, val) {
                var post_position = $(this).offset().top;
                var post_height = $(this).outerHeight();
                var postID = $(this).attr("data-id");
                var seen_post = $(this).attr("data-seen");
                var user = $("#postUser_" + postID).attr("data-auth");
                if (post_position <= (fix_ele.top + fix.outerHeight()) && (post_position + post_height) >= (fix_ele.top + fix.outerHeight())) {
                    if (seen_post == "no") {
                        $("#post_" + postID).attr("data-seen", "yes");
                        $("#seen_" + postID + "_post").show();
                        $("#count_seen_" + postID).html(parseInt($("#count_seen_" + postID).html()) + 1);
                        $("#seen_" + postID + "_post").attr("data-original-title", $("#seen_" + postID + "_post").attr("data-original-title") + user + "<br>");
                        $.ajax({
                            type: "POST",
                            url: "/ajax/post",
                            data: {action: "seenPost", postID: postID},
                            success: function(e) {
                                var data_json = jQuery.parseJSON(e);
                                //console.log(data_json)
                            }
                        });
                        return false;
                    }
                }

            });
        });
    },
    modal_view: function(elements) {
        $("body").on("click", elements, function() {
            //var dataID = $(this).attr("data-id");
            //var dataName = $(this).attr("data-name");
            //var ret = "";
            ////var img = '<input type="button" class="btn-basicBtn" value="&nbsp;No&nbsp;" id="popup_cancel">';
            //$.ajax({
            //    type    :   "POST",
            //    url     :   "/ajax/post",
            //    dataType:   'json',
            //    data    :   {action:"loadImage",dataID:dataID,dataName:dataName},
            //    success :   function(e){
            //        
            //        var postImage = e;
            //        console.log(postImage.extension_img);
            //        var valid_formats = ["jpg", "png", "JPG", "PNG", "GIF", "gif"];
            //        if (postImage.extension_img.length!=0) {
            //                // Image
            //                if (postImage.extension_img.length!=0) {
            //                var folderName = postImage.postFolderName; 
            //                var listImg = postImage.img;
            //                
            //                ret += '<div class="slider">';
            //                    ret += '<div class="flexslider">';
            //                      ret += '<ul class="slides">';
            //                      
            //                                for (var a in listImg) {
            //                                        var listImg_extension = listImg[a].split(".");
            //                                        var extension = listImg_extension[listImg_extension.length - 1];
            //                                        if (valid_formats.indexOf(extension)!=-1) {
            //                                            ret += '<li>';
            //                                                ret += '<center>';
            //                                                    ret += '<img src="/images/postUpload/' + folderName + '/' + dataName + '" class="avatar" width="'+ postImage.image_resize[0]  +'" height="'+ postImage.image_resize[1] +'">';
            //                                                ret += '</center>';
            //                                            ret += '</li>';
            //                                        }
            //                                   }
            //                                }
            //                      ret += '</ul>';
            //                    ret += '</div>';
            //                ret += '</div>';
            //                
            //        }
            //        jDialog(ret, "", "800", "500","", function(){});
            //    }
            //});

        });
    },
    load_hashTag: function() {
        var pathname = window.location;
        
        if (getParametersName("view_type", pathname) == "view_hashtag") {
            var data_hashTag = getParametersName("hashTag", pathname);
            $(".load_p").show();
            $.ajax({
                type: "POST",
                url: "/ajax/post",
                data: {action: "getHashTag", data_hashTag: data_hashTag},
                dataType: "json",
                cache: false,
                success: function(result) {
                    // console.log(result)
                    if (result == "null" || result == null || result == "") {
                        $(".load_p").hide();
                    } else {
                        var obj = result;
                        $.each(obj, function(id, val) {

                            // POST JSON
                            var postDetails = obj[id].getpost.post;
                            var uploadedDetails = obj[id].getpost.uploadedImages;
                            var likeDetails = obj[id].getLike;
                            var commentDetails = obj[id].getcomment;
                            //console.log(obj[id]);
                            var P = postDesign(postDetails, uploadedDetails, likeDetails, commentDetails);
                            $(".hashTag_post").append(P);
                            $("label.timeago").timeago();  // Time To go

                            // PopOver user information
                            $('#postUser_' + postDetails.postID).popover({
                                html: true,
                                content: function() {
                                    return $('#popover_content_wrapper_' + postDetails.postID).html();
                                }
                            });
                            $(".load-mr").hide();

                            // Remove images uploaded on the preview
                            $(".previewImagePostUpload").html(null);
                            $(".imagePreviewUpload").hide();

                            // On Image Slider
                            $('.flexslider_' + postDetails.postID).flexslider({
                                controlNav: false
                            });
                            if (commentDetails != null) {
                                for (var a = 0; a < commentDetails.length; a++) {
                                    $('.flexslider_1_' + commentDetails[a].commentID).flexslider({
                                        controlNav: false
                                    });
                                    $(".group_1_" + commentDetails[a].commentID).colorbox({rel: 'group_1_' + commentDetails[a].commentID, width: "75%", height: "80%"});
                                }

                            }
                            jQuery('.mycarousel').jcarousel();
							$(".text").emotions();
                            $(".dataTip").tooltip({
                                html: true,
                                placement: "top"
                            });
                            $(".tip").tooltip({
                                html: true,
                                placement: "right"
                            });
                            $("img").error(function() {
                                $(this).unbind("error").attr("src", "/images/error/broken.png");
                            });
                            $(".group_0_" + postDetails.postID).colorbox({rel: 'group_0_' + postDetails.postID, width: "75%", height: "80%"});
                            $(".load_p").hide();
                        });
                    }

                }
            });
        }
    }
}

PostPrivacy = {
    init: function() {
        this.showChoice();
        this.selectChoiceType();
        this.removePostChoices()
        var pathname = window.location.pathname;
        var user_view = $("#user_url_view").val();
        if(pathname!="/home" || pathname!= user_view + "announcements"){
            $(".post-privacy-container").css({
                "float":"right"
            })
        }
    },
    choiceDOM: "",
    postIDs: [],
    showChoice: function() {
        var self = this;
        $(".showPostChoices").click(function() {
            $(".postPrivacyChoice").toggle();
            if ($(".postPrivacyChoice").css("display") == "block") {
                $(".post-privacy").addClass("post-privacy-active")
            } else {
                $(".post-privacy").removeClass("post-privacy-active")
            }
        })
    },
    selectChoiceType: function() {
        var self = this;
        $(".postPrivacyChoice-choices").click(function() {

            self.choiceDOM = $(this);
            self.selectChoice();

            $(".postPrivacyChoice").hide()
            $(".post-privacy").removeClass("post-privacy-active");
        })
    },
    selectChoice: function() {
        var self = this;
        var choice = self.choiceDOM.find(".postChoice").text();
        var icon = self.choiceDOM.find(".postChoice-icon").find("i").attr("class");
        var selectValue = $("#selectedPostChoice").attr("value");
        var value = self.choiceDOM.find(".postChoice").attr("value");
        var selected = "";
        if (value != 0) {
            $.post("/ajax/post", {"action": "getPrivacy", type: value}, function(data) {
                var ret = '<h3 class="fl-margin-bottom"><!--<i class="' + icon + '"></i>--> ' + choice + '</h3>';
                ret += '<div class="hr"></div>';
                ret += '</div>';
                ret += '<div id="post-privacy-container" style="position: relative;  padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">'
                ret += '<div class="content-dialog sub-container-post-privacy CSID-search-container fp-container" style="width: 100%;height: auto;">';
                ret += '<div style="float:left;width:45%;margin-top: 5px;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="Allusers"/><label for="Allusers" class="css-label"></label> All '+ choice +'s</label></div>';
                ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>'
                ret += '<div class="clearfix"></div>';
                ret += '<div class="container-formUser" CSID-search-type="" style="margin-top:10px">';
                var ctr_users = 0;
                if (data) {
                    data = JSON.parse(data);
                    var data_value = "";
                    var data_id = "";
                    $.each(data, function(key, val) {
                        if (value == 1) {
                            //groups
                            data_id = val['id'];
                            data_value = val['name'];
                        } else if (value == 2) {
                            //department
                            data_id = val['id'];
                            data_value = val['department'];
                        } else if (value == 3) {
                            //position
                            data_id = val['id'];
                            data_value = val['position'];

                        }
                        if (data_value) {
                            if (selectValue == value) {
                                selected = RequestPivacy.setCheckedArray(data_id, self.postIDs);
                            }
                            ret += '<div style="width:30%;float:left;" class="CSID-search-data"> ';
                            ret += '<label style="margin-bottom:2px;display:inline-block;" class="tip" title="' + data_value + '"><input type="checkbox" class="fp-user post-ids css-checkbox" value="' + data_id + '" ' + selected + ' id="fp-user_post-ids_' + data_id + '"/><label for="fp-user_post-ids_' + data_id + '" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">' + data_value + '</span></label>';
                            ret += '</div>';
                        }
                    })
                    ctr_users++;
                }
                var displayEmptyDept = "display";
                if(ctr_users==0){
                    displayEmptyDept = ""
                }
                ret += '<span class="empty-users '+ displayEmptyDept +'" style="color:red">No '+ choice +'</span>';
                ret += '</div>';
                ret += '<br />';
                ret += '</div>';
                ret += '</div>';
                //ret += '<div class="fields" style="border-top:1px solid #ddd;">';
                ret += '<div class="label_basic"></div>';
                ret += '<div class="input_position" style="margin-top:5px;">';
                ret += ' <input type="button" class="btn-blueBtn   fl-margin-right" id="savePostPrivacy" value="Ok">';
                ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close" style="float:none;">';
                ret += '</div>';
                ret += '</div>';
                var newDialog = new jDialog(ret, "", "600", "", "", function() {
                });
                newDialog.themeDialog("modal2");
                limitText(".limit-text-wf, .limit-text-ws", 20); // set limit of text in the name   
                $(".tip").tooltip();
                RequestPivacy.setCheckAllChecked(".fp-user");
                RequestPivacy.bindOnClickCheckBox();
                $("#savePostPrivacy").click(function() {
                    self.postIDs = [];
                    $(".post-ids:checked").each(function() {
                        self.postIDs.push($(this).val())
                    })
                    if(self.postIDs.length==0){
                        showNotification({
                            message: "Please choose atleast one "+choice,
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                        return;
                    }
                    self.savePostPrivacy();
                    $("#popup_cancel").click();
                })
            })
        } else {
            self.postIDs = "";
            self.savePostPrivacy();
        }
    },
    savePostPrivacy: function() {
        var self = this;
        var choice = self.choiceDOM.find(".postChoice").text();
        var icon = self.choiceDOM.find(".postChoice-icon").html();
        var value = self.choiceDOM.find(".postChoice").attr("value");
        $(".postChoice-icon-selected").html(icon);
        $("#selectedPostChoice").attr("value", value);
        $("#selectedPostChoice").attr("postIDs", self.postIDs);
        $("#selectedPostChoice").text(choice);
    },
    removePostChoices: function() {
        $(document).on({
            "click": function(evs) {
                target = (window.event) ? window.event.srcElement /* for IE */ : evs.target;
                // console.log(target)
                // console.log($(target).closest(".post-privacy-container").length)
                if ($(target).closest(".post-privacy-container").length == 0) {
                    $(".postPrivacyChoice").hide()
                    $(".post-privacy").removeClass("post-privacy-active")
                }
            }
        })
    }
}
