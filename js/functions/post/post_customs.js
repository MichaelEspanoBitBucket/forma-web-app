$(document).ready(function(){
//    $(".fl-widget-cpnyevnts-content-wrapper").each(function(){
//	$(this).html(replaceTagStr(replaceStrHashtags_decode(replaceStrHashtags_encode(replaceURLWithHTMLLinks($(this).text())))));
//    });
    
    $("body").on("mouseenter",".text",function(){
        var dataID = $(this).attr("data-id");
        $(".post_settings_" + dataID).show();
    });
    $("body").on("mouseleave",".text",function(){
        var dataID = $(this).attr("data-id");
        $(".post_settings_" + dataID).hide();
        $(this).removeClass("open_settings");
        $("#postSettingChoice_" + dataID).hide();
    });
    $("body").on("click",".post_settings",function(){
        var dataID = $(this).attr("data-id");
        $(this).addClass("open_settings");
        $(this).removeClass("remove_settings");
        $("#postSettingChoice_" + dataID).show();
        //var settings = $(".postSettingChoice").html();
        //if (settings!="") {
        //    $("#postSettingChoice_" + dataID).show();
        //}
    });
    $("body").on("click",".remove_settings",function(){
        var dataID = $(this).attr("data-id");
        $(this).addClass("post_settings");
        $(this).removeClass("remove_settings");
        $("#postSettingChoice_" + dataID).hide();
    });
    // Click Starred
    $("body").on("click",".post_starred",function(){
        var dataID = $(this).attr("data-id");
        var addAction = $(this).attr("data-action");
        var ret = ""
        $(this).remove();
        /*sam*/
        // if (addAction=="addStarred") {
        //     $(".starred_show_" + dataID).show();
            
        // }else{
        //     $(".starred_show_" + dataID).hide();
        // }
        /*aaron*/
        if (addAction=="addStarred") {
            $(".starred_show_" + dataID).show();
            ret = '<div class="postPrivacyChoice-choices post_starred" id="removeStarred_' + dataID + '" data-action="removeStarred" data-id="' + dataID + '" style="">';
                ret += '<span class="postChoice-icon" style="">';
                    ret += '<i class="fa fa-star icon-star" style=""></i>';
                ret += '</span>';
                ret += '<span class="postChoice" value="0" style="float: left;">Remove Starred</span>';
            ret += '</div>'; 
            
        }else{
            $(".starred_show_" + dataID).hide();
            ret += '<div class=" postPrivacyChoice-choices post_starred" id="addStarred_' + dataID + '" data-action="addStarred" data-id="' + dataID + '" style="">';
                ret += '<span class="postChoice-icon" style="">';
                    ret += '<i class="fa fa-star-o icon-star-empty" style=""></i>';
                ret += '</span>';
                ret += '<span class="postChoice" value="0" style="float: left;">Starred Post</span>';
            ret += '</div>';    
        }
        /*end aaron*/
        $("#postSettingChoice_"+dataID).html(ret)
        $.ajax({
            type    : "POST",
            url     : "/ajax/post",
            dataType: "json",
            data    : {action:"starredPost",postID:dataID,addAction:addAction},
            success : function(e){
                //var starred = e.starred;
                //console.log(starred)
                //if (starred==null) {
                //    $("#addStarred_" + dataID).removeClass("display");
                //    $("#removeStarred_" + dataID).addClass("display");
                //    $("#sremoveStarred_" + dataID).addClass("display");
                //}else{
                //    $("#addStarred_" + dataID).addClass("display");
                //    $("#sremoveStarred_" + dataID).removeClass("display");
                //    $("#removeStarred_" + dataID).removeClass("display");
                //}
                var objStarred = {"type":"0"};
                Starred.load(0,"start",objStarred,function(result){
                    Starred.loadAfterAjaxStarredWidget(result);
                });
                hideSettings(dataID)
            }
        });
        // hideSettings(dataID)
    });
    
    $("body").on("click",".viewSeen",function(){
	var dataID = $(this).attr("data-id");
	var seen_names = $("body").data("Seen_" + dataID);
	var seen_array = [];
	var seen_ret = "";
	var userProfile = "";
	var pathname = window.location.pathname;
	var user_view = $("#user_url_view").val();
	var split_pathname = pathname.split("/");
	if (split_pathname[2]=="announcements" || split_pathname[2]=="hashtag" || split_pathname[2]=="post") {
	    var new_path = user_view;
	}else{
	    var new_path = "/";
	}
	if (seen_names!=null) {
	    var getSeen = seen_names;
	    for (var s = 0; s<getSeen.length; s++) {
		userProfile += '<div  style="float:left;width:100%;">';
		    userProfile += '<div class="pull-left" style="margin-top:5px;">';
			userProfile += '<div class="pull-left " style="width: 44px;height: 44px;">';
			//userProfile += '<div class="pull-left avatar" style="width: 44px;height: 44px;">';
			    userProfile += getSeen[s].image;
			userProfile += '</div>';
		    userProfile += '</div>';
		    userProfile += '<div class="pull-left" style="width:75%;margin-top:5px;margin-left: 10px;">';
			userProfile += "<a href='" + new_path + getSeen[s].urlProfile + "'>" + getSeen[s].nameSeen + "</a> <br/>" ;
                        if (getSeen[s].departmentName!=null) {
                            userProfile += "<label style='font-size: 11px;color: #C5C5C5;'>" + getSeen[s].departmentName + "</label>";
                        }
		    userProfile += '</div>';
		userProfile += '</div>';
		//var userProfile = "<a href='/" + getSeen[s].urlProfile + "'>" + getSeen[s].nameSeen + "</a>";
		
	    }
	    seen_array.push(userProfile);
	    seen_ret += '<h3><i class="icon-user"></i> People who saw this</h3>';
	    seen_ret += '<div class="hr"></div>';
	    seen_ret += '<div class="seen-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 200px;overflow: hidden;">';
	    
	    $.each(seen_array,function(id,val){
		seen_ret += seen_array[id] + "<br>";
	    });
	    seen_ret += '</div>';
	    seen_ret += '<div class="hr"></div>';
	    seen_ret += '<div class="fields">';
		seen_ret += '<div class="label_basic"></div>';
		    seen_ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
			seen_ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel"><br><br>';
		    seen_ret += '</div>';
		seen_ret += '</div>';
	    var newDialog = new jDialog(seen_ret, "","", "", "", function(){});
        newDialog.themeDialog("modal2");
	}
	$(".seen-container").perfectScrollbar();
        $("img").error(function () {
            $(this).unbind("error").attr("src", "/images/error/broken.png");
        });
    });
    // Edit Post
    //$("body").on("click",".edit_post",function(){
    //    var dataID = $(this).attr("data-id");
    //    
    //    $(".post_text_" + dataID).hide();
    //    $(".edit_post_text_" + dataID).show();
    //    var html_post = $(".edit_post_text_" + dataID).html();
    //    html_post.replace(new RegExp("<br>","g"), "\n");
    //    console.log(html_post.replace(new RegExp("<br>","g"), "\n"))
    //    hideSettings(dataID)
    //});
    
    function hideSettings(dataID) {
        /*sam*/
        
        // $(".post_settings_" + dataID).hide();
        // $(".text").addClass("post_settings");
        // $(".text").removeClass("open_settings");
        // $("#postSettingChoice_" + dataID).hide();

        /*aaron*/
        // $(".post_settings_" + dataID).hide();
        $(".text").addClass("post_settings");
        $(".text").removeClass("open_settings");
        $("#postSettingChoice_" + dataID).hide();
    }
    var user_view = $("#user_url_view").val();
    posting.createPost("hashTagPost");
    var pathname = window.location.pathname;
	if (pathname=="/home"||pathname=="/gi-dashboard-home"||pathname== user_view + "announcements") {
	    posting.loadPost();
	    
	}	
        posting.seen_post();
    
    posting.showReply(".showReply",".comment_");
    posting.reply();
    posting.likePost();
    posting.unlikePost();
    posting.deletePost();
    posting.loadComment();
    posting.loadMore();
    posting.loadText();
    posting.uploadFile(".postFile");
    posting.hashTag_view();
    posting.selected_post();
    posting.modal_view(".imageView");
    posting.loadCompanyEvent();
    var pathname = window.location.pathname;
	if (pathname=="/hashtag"||pathname== user_view + "hashtag") {
	    posting.load_hashTag();
	}
    
    
    $("body").on("click",".getMorePrivacyTagged",function(){
        var data = $(this).attr("json-data");
        data = JSON.parse(data);
        var privacyData = data['postPrivacyDetails'];
        var seen_ret = "";
        seen_ret += '<h3><i class="'+ getPrivacyIcon(data['postPrivacyType']) +'"></i>  Tagged '+ getPrivacyTypeName(data['postPrivacyType']) +'</h3>';
        seen_ret += '<div class="hr"></div>';
        seen_ret += '<div class="seen-container" style="margin-top:10px;position: relative;  padding: 0px; width: 100%; height: 200px;overflow: hidden;">';
            for (i in privacyData) {
                seen_ret += '<div  style="float:left;width:100%;">';
                    seen_ret += '<div class="" style="padding: 5px;background-color: #F0ECEC;border: 1px solid #D3D3D3;">';
                   seen_ret += "<label style=''>" + privacyData[i] + "</label>";
                    seen_ret += '</div>';
                seen_ret += '</div>';
            }
        seen_ret += '</div>';
        seen_ret += '<div class="hr"></div>';
        seen_ret += '<div class="fields">';
        seen_ret += '<div class="label_basic"></div>';
            seen_ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
           seen_ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel"><br><br>';
            seen_ret += '</div>';
        seen_ret += '</div>';
        var newDialog = new jDialog(seen_ret, "","", "", "", function(){});
        newDialog.themeDialog("modal2");
        $(".seen-container").perfectScrollbar();
            $("img").error(function () {
                $(this).unbind("error").attr("src", "/images/error/broken.png");
            });
        })
});
