//  Constant Strings that will be removed from ajax responses
var SMTP_ERROR = 'SMTP -> ERROR: Failed to connect to server: Unable to find the socket transport "ssl" - did you forget to enable it when you configured PHP? (0)';
var BREAK_LINE = '<br />';

//  trimmer limit limits the number of times the removal of constant strings may occur.
//  this prevents endless loops
var trimmerLimit = 5;

function replyComment(self,fID,elementType) {
    var file_name = $(self).attr("data-file");
    var files = $(self).attr("data-get-file");
    var folder_location = $(self).attr("data-location");
    var imageLength = $("." + files).length;
    var img = {};
    var reply = "";
    $("." + files).each(function(eqindex){
        img['name_' + eqindex] = $(this).attr("data-name");
    });
    var postID = $(self).attr("data-id");
    var type = $(self).attr("data-type");
    var authorId = $(self).closest('div#post_' + postID).attr('author-id');
    
    var getAllTag = "";
    $(".comment_"+postID+"_"+type).each(function(){
        if ($(this).val()!="") {
            // switch (type) {
            //     case "1":
                    reply = $(this).val();
            //         break;
            //     // Default Comment on the announcement post
            //     default:
                    // reply = $(this).closest(".mentionContainer").find(".mentionHiddenText").val();
                    // reply = nl3br(reply.replace(/\n/g, '<br>'));
                    // getAllTag = $(this).closest(".mentionContainer").find(".mentionSelectedContainer").find("span").map(function(){
                    //     return $(this).attr("data_id");
                    // }).get().join();
            //         break;
            // }
        }
        
    });
    // reply = replaceTagStr(reply);
    //console.log($("body").data("post_" + postID))
    if($.trim(reply) != "" || imageLength!= 0){
        var json_encode = JSON.stringify(img);
        $(".comment_"+postID+"_"+type).addClass("loadBar");
        $.post("/ajax/post",{action:"createReply",postID:postID,reply:reply,type:type,fID:fID,imagesGet:json_encode,folder_location:folder_location,getAllTag:getAllTag},function(data){
            console.log(data,'rep');                        
            
            //  remove prompts from configuration
            var removeCount = 0;
            while (data.indexOf(SMTP_ERROR) >= 0) {
                data = data.replace(SMTP_ERROR, '');
                
                if (removeCount >= trimmerLimit) {
                    break;
                }
                removeCount ++;
            }
                        
            var json_data = jQuery.parseJSON(data);
            var comments = json_data[0].returnComment;
            console.log(json_data[0]);
            //console.log(comments)
                var P = replyPost(null,null,null,comments);
                switch (type) {
                    case "1":
                        //showNotification({
                        //        message: "Your comment was successfully created.",
                        //        type: "success",
                        //        autoClose: true,
                        //        duration: 3
                        //});
                        $(P).fadeIn("slow").prependTo(".requestComment");
                        $(".comment_"+postID+"_"+type).removeClass("loadBar");
                        $("label.timeago").timeago();  // Time To go
                        $(".comment_"+postID+"_"+type).val(null);
                        //$("#popup_container, #popup_overlay").remove();
                        $(self).closest("#popup_content").find(".mentionSelectedContainer").html("");
                        break;
                    // Default Comment on the announcement post
                    default:
                        $(P).fadeIn("slow").insertAfter(self);
                        $("label.timeago").timeago();  // Time To go
                        $(".comment_"+postID+"_"+type).val(null);
                        $(".comment_"+postID+"_"+type).removeClass("loadBar");
                        $(self).closest(".mentionContainer").find(".mentionSelectedContainer").html("")
                        break;
                }
                
                // Remove images uploaded on the preview
                $(".previewImagePostUpload_"+postID+"_"+type).html(null);
                $(".imagePreviewUpload_"+postID+"_"+type).hide();
                //console.log($('.flexslider_1_' + comments.commentID).flexslider())
                //console.log(comments.commentID)
                $('.flexslider_1_' + comments.commentID).flexslider({
                    controlNav: false
                });
                jQuery('.mycarousel').jcarousel();
				$(".text").emotions();
                $(".dataTip").tooltip({
                                    html : true,
                                    placement : "right"
                                });                              
            
            if (typeof authorId !== 'undefined') {
                $.event.trigger({
                    type: 'sendNotification',
                    sendTo: authorId,
                    notificationData: {
                        notificationType: 'NEW_POST_REPLY',
                        notificationData: json_data[0],
                        replyHTML: P
                    }
                });
            } else {
                if (json_data.getNoti || json_data[0].notifiedUserIdList) {
                    
                    var notificationRecipients = [];
                    
                    if (json_data.getNoti && json_data.getNoti.notification_recipients) {
                        var notificationRecipientsRaw = json_data.getNoti.notification_recipients;
                        notificationRecipients = notificationRecipientsRaw.split(",");
                    } else if ( json_data[0].notifiedUserIdList) {
                        notificationRecipients = json_data[0].notifiedUserIdList;
                    } else {
                        console.log('getNoti.notification_recipients is undefined, unable to send live notifications');
                        return;
                    }
                    
                    $.event.trigger({
                        type: 'sendNotification',
                        sendTo: notificationRecipients,
                        notificationData: {
                            notificationType: 'NEW_POST_REPLY',
                            notificationData: json_data[0],
                            replyHTML: P
                        }
                    });
                    
                } else {
                    console.log('getNoti and/or notifiedUserIdList is undefined, unable to send live notifications');
                }
            }
                
        });
    }
    //
    $("#comment-container").perfectScrollbar("update");
    $(".mentionSelectedContainer").html("")
    $(".comment-popup").css("height","");
    $(".mentionSelectedContainer").css("height","");
}

/*
 * Create Post
 * @authID - user id authentication who currently logged on the machine
 * @post - created post of the user.
 * @createdBy - the one who create a post.
 * @datePosted - the date where the post created
 * @img - user Avatar
 * @postID - ID of the created post
 * @comments - comments on the post
 * @likes - likes on the post
 * @postedByID - the one who post
 * @user_level_id - for the admin of the company only
 */

function postDesign(postDetails,uploadedDetails,likeDetails,commentDetails){
    
    var ret = "";
    var postImg = "";
    //console.log(postDetails);
    // Seen 
    var sen_array = [];
    /*aaron*/
    var display_starr = "display";
    /*end aaron*/
    var seen_post = "no";
        if (postDetails.getSeen!=null) {
            var getSeen = postDetails.getSeen;
            for (var sen = 0; sen<getSeen.length; sen++) {
                sen_array.push(getSeen[sen].nameSeen);
            }
            if (jQuery.inArray( postDetails.authName, sen_array ) == -1) {
                seen_post = "no";
            }else{
                seen_post = "yes";
            }
        }
    var pathname = window.location.pathname;
    var user_view = $("#user_url_view").val();
    var split_pathname = pathname.split("/");
    if (split_pathname[2]=="announcements" || split_pathname[2]=="hashtag" || split_pathname[2]=="post") {
        var new_path = user_view;
    }else{
        var new_path = "/";
    }      
        ret += '<div class="post" id="post_' + postDetails.postID + '" author-id="' + postDetails.userID + '" data-seen="' + seen_post + '" data-id="' + postDetails.postID + '">';
            ret += '<a class="message-img" href="'+ new_path + postDetails.profile + '">' + postDetails.images + '</a>';
            ret += '<div class="message-body">';
                ret +=  '<div class="text" id="postText_' + postDetails.postID + '" author-id="' + postDetails.userID + '" data-id="' + postDetails.postID + '">';
                        
                        // Settings
                        //if (postDetails.starred!=null) {
                        //    var display = "";    
                        //}else{
                        //    var display = "display";
                        //}
                        /*sam*/
                        // if (postDetails.starred!=null) {
                        //     if (postDetails.starred[0].starredBy==postDetails.authID) {
                        //         ret += '<div class="starred_show_' + postDetails.postID + '" style="position:absolute;right: 32px;top: 0;height: 30px;width: 15px;border-left: 1px solid #5298C5;border-right: 1px solid #5298C5;border-bottom: 1px solid #5298C5;border-radius: 0px 0px 5px 5px;background-color: #79ACCE;padding: 5px;box-shadow: 0px 0px 10px #82B8DB;">';
                        //         ret += '<i class="icon-star" style=" margin-left: 2px; color: #fff; "></i>';
                        //         ret += '</div>';
                        //     }
                        // }
                        /*aaron*/
                        if (postDetails.starred!=null) {
                            var starred_details = postDetails.starred;
                            var starred_users = [];
                            for(var index in starred_details){
                                starred_users.push(starred_details[index]['starredBy'])
                            }
                            if (starred_users.indexOf(postDetails.authID)>=0) {
                                display_starr = "";
                            }
                        }

                        ret += '<div class="starred_show_' + postDetails.postID + ' '+ display_starr +'" style="position:absolute;right: 32px;top: 0;height: 30px;width: 15px;border-left: 1px solid #5298C5;border-right: 1px solid #5298C5;border-bottom: 1px solid #5298C5;border-radius: 0px 0px 5px 5px;background-color: #79ACCE;padding: 5px;/*box-shadow: 0px 0px 10px #82B8DB;*/">';
                        ret += '<i class="fa fa-star icon-star" style=" margin-left: 2px; color: #fff; "></i>';
                        ret += '</div>';
                        /*end aaron*/

                        ret += '<div class="fa fa-chevron-down icon-chevron-down cursor post_settings post_settings_' + postDetails.postID + '" data-id="' + postDetails.postID + '" style="display:none; position: absolute; right: 10px; color: #A3A3A3;"></div>';
                        ret += '<div class="postSettingChoice postPrivacyChoice-container" id="postSettingChoice_' + postDetails.postID + '" style="right:0;display: none;">';

                                    /*Sam*/
                                    // if (postDetails.starred==null) {
                                    //     //console.log(postDetails.starred)
                                    //         ret += '<div class=" postPrivacyChoice-choices post_starred" id="addStarred_' + postDetails.postID + '" data-action="addStarred" data-id="' + postDetails.postID + '" style="">';
                                    //             ret += '<span class="postChoice-icon" style="">';
                                    //                 ret += '<i class="icon-star-empty" style=""></i>';
                                    //             ret += '</span>';
                                    //             ret += '<span class="postChoice" value="0" style="float: left;">Starred Post</span>';
                                    //         ret += '</div>';    
                                    // }
                                        
                                    // if (postDetails.starred!=null) {
                                    //     if (postDetails.starred[0].starredBy==postDetails.authID) {
                                    //         ret += '<div class="postPrivacyChoice-choices post_starred" id="removeStarred_' + postDetails.postID + '" data-action="removeStarred" data-id="' + postDetails.postID + '" style="">';
                                    //             ret += '<span class="postChoice-icon" style="">';
                                    //                 ret += '<i class="icon-star" style=""></i>';
                                    //             ret += '</span>';
                                    //             ret += '<span class="postChoice" value="0" style="float: left;">Remove Starred</span>';
                                    //         ret += '</div>'; 
                                    //     }
                                    // }
                                    /*aaron*/
                                    if (postDetails.starred==null) {
                                        //
                                            ret += '<div class=" postPrivacyChoice-choices post_starred" id="addStarred_' + postDetails.postID + '" data-action="addStarred" data-id="' + postDetails.postID + '" style="">';
                                                ret += '<span class="postChoice-icon" style="">';
                                                    ret += '<i class="icon-star-empty" style=""></i>';
                                                ret += '</span>';
                                                ret += '<span class="postChoice" value="0" style="float: left;">Starred Post</span>';
                                            ret += '</div>';    
                                    }
                                    
                                    if (postDetails.starred!=null) {

                                        if (starred_users.indexOf(postDetails.authID)>=0) {
                                            ret += '<div class="postPrivacyChoice-choices post_starred" id="removeStarred_' + postDetails.postID + '" data-action="removeStarred" data-id="' + postDetails.postID + '" style="">';
                                                ret += '<span class="postChoice-icon" style="">';
                                                    ret += '<i class="icon-star" style=""></i>';
                                                ret += '</span>';
                                                ret += '<span class="postChoice" value="0" style="float: left;">Remove Starred</span>';
                                            ret += '</div>'; 
                                        }else{
                                            ret += '<div class=" postPrivacyChoice-choices post_starred" id="addStarred_' + postDetails.postID + '" data-action="addStarred" data-id="' + postDetails.postID + '" style="">';
                                                ret += '<span class="postChoice-icon" style="">';
                                                    ret += '<i class="icon-star-empty" style=""></i>';
                                                ret += '</span>';
                                                ret += '<span class="postChoice" value="0" style="float: left;">Starred Post</span>';
                                            ret += '</div>';    
                                        }
                                    }

                                    //if (postDetails.authID==postDetails.userID) {
                                    //    ret += '<div class="postPrivacyChoice-choices edit_post" data-id="' + postDetails.postID + '" style="">';
                                    //        ret += '<span class="postChoice-icon" style="">';
                                    //            ret += '<i class="icon-edit" style=""></i>';
                                    //        ret += '</span>';
                                    //        ret += '<span class="postChoice" value="0" style="float: left;">Edit Post</span>';
                                    //    ret += '</div>';
                                    //}       
                                        
                                ret += '</div>';
                        
                        
                        if (postDetails.view_all=="yes") {
                            // ret += '<p class="post_text_' + postDetails.postID + '">' + replaceTagStr(replaceStrHashtags_decode(htmlEntities(replaceStrHashtags_encode(replaceURLWithHTMLLinks(postDetails.post))))) + '</p>';
                            ret += '<p class="postEmoticon post_text_' + postDetails.postID + '">' + replaceTagStr(replaceStrHashtags_decode(replaceStrHashtags_encode(replaceURLWithHTMLLinks(postDetails.post)))) + '</p>';
                            //ret += '<textarea class="display input-announcement edit_post_text_' + postDetails.postID + '" style="margin-top: 35px;">' + postDetails.post + '</textarea>';
                        }else{
                            ret += '<p class="postEmoticon post_text_' + postDetails.postID + '">' + showMore(postDetails.post,postDetails.postID,"post") + '</p>';
                            //ret += '<textarea class="display input-announcement edit_post_text_' + postDetails.postID + '" style="margin-top: 35px;">' + postDetails.post + '</textarea>';
                        }
                            // Images Post
                            var postImage = uploadedDetails;
                            
                            var valid_formats = ["jpg", "png", "JPG", "PNG", "GIF", "gif"];
                            if (postImage.extension_img.length!=0) {
                                    var w = "";
                                    var h = "";
                                    
                                    // Image
                                    if (postImage.extension_img.length!=0) {
                                    var folderName = postImage.postFolderName; 
                                    var listImg = postImage.img;
                                    // Image Class
                                        var imageView = "imageView openImageView";
                                        //width
                                        if (postImage.image_resize[0] > 600) {
                                            w = "500";
                                        }else{
                                            w = postImage.image_resize[0];
                                        }
                                        // Height
                                        if (postImage.image_resize[1] > 600) {
                                            h = "400";
                                        }else{
                                            h = postImage.image_resize[1];
                                        }
                                    for (var a in listImg) {
                                            var listImg_extension = listImg[a].split(".");
                                            var extension = listImg_extension[listImg_extension.length - 1];
                                            if (valid_formats.indexOf(extension)!=-1) {
                                                
                                                if (postImage.lengthImg=="1") {
                                                    ret += '<center>';
                                                        ret += '<img src="/images/postUpload/' + folderName + '/' + listImg[a] + '" class=" '+ imageView +' group_0_' + postDetails.postID + '"  href="/images/postUpload/' + folderName + '/' + listImg[a] + '"  width="'+ w +'" height="'+ h +'">';
                                                    ret += '</center>';
                                                }else if (postImage.lengthImg=="2") {
                                                    ret += '<img src="/images/postUpload/' + folderName + '/' + listImg[a] + '" class=" '+ imageView +' group_0_' + postDetails.postID + '"  href="/images/postUpload/' + folderName + '/' + listImg[a] + '"  style="width: 40%;">';
                                                }else if (postImage.lengthImg=="3") {
                                                    ret += '<img src="/images/postUpload/' + folderName + '/' + listImg[a] + '" class=" '+ imageView +' group_0_' + postDetails.postID + '"  href="/images/postUpload/' + folderName + '/' + listImg[a] + '"  style="width: 30%;">';
                                                }else if (postImage.lengthImg > 4) {
                                                    ret += '<img src="/images/postUpload/' + folderName + '/' + listImg[a] + '" class=" '+ imageView +' group_0_' + postDetails.postID + '"  href="/images/postUpload/' + folderName + '/' + listImg[a] + '"  style="width: 30%;">';
                                                }else if (postImage.lengthImg == "4"){
                                                    ret += '<img src="/images/postUpload/' + folderName + '/' + listImg[0] + '" class=" '+ imageView +' group_0_' + postDetails.postID + '"  href="/images/postUpload/' + folderName + '/' + listImg[0] + '"  style="width: 30%;">';
                                                    ret += '<img src="/images/postUpload/' + folderName + '/' + listImg[1] + '" class=" '+ imageView +' group_0_' + postDetails.postID + '"  href="/images/postUpload/' + folderName + '/' + listImg[1] + '"  style="width: 30%;">';
                                                    ret += '<img src="/images/postUpload/' + folderName + '/' + listImg[2] + '" class=" '+ imageView +' group_0_' + postDetails.postID + '"  href="/images/postUpload/' + folderName + '/' + listImg[2] + '"  style="width: 30%;">';
                                                    ret += '<img src="/images/postUpload/' + folderName + '/' + listImg[3] + '" class=" '+ imageView +' group_0_' + postDetails.postID + '"  href="/images/postUpload/' + folderName + '/' + listImg[3] + '"    width="'+ w +'" height="'+ h +'">';
                                                    
                                                    break;
                                                }
                                                
                                            }
                                       }
                                    }
                            }
                            if(postImage.extension_file.length!=0){
                                    // Files
                                      if(postImage.extension_file.length!=0){
                                      var folderName = postImage.postFolderName; 
                                      var listImg = postImage.img;
                                     
                                      for (var a in listImg) {
                                              var listImg_extension = listImg[a].split(".");
                                              var extension = listImg_extension[listImg_extension.length - 1];
                                              if (valid_formats.indexOf(extension)==-1) {
                                              ret += '<form method="POST" style="text-decoration: underline;"><input type="submit" class="cursor tip" data-original-title="' + listImg[a] + '" style="opacity: 0;position: absolute;width: 100%;"><div class="">';
                                                      ret += '<a class="cursor apps">';
                                                      var num = "1";
                                                              ret += num++ + ") " + listImg[a];
                                                      ret += '</a>';
                                              ret += '</div>';
                                              ret += '<input type="hidden" value="' + listImg[a] + '" name="attachment_filename">';
                                              ret += '<input type="hidden" value="images/postUpload/' + folderName + '/' + listImg[a] + '" name="attachment_location">';
                                              ret += '</form>';
                                              }
                                     }
                                      }
                            }
                ret +=  '</div>';
                // console.log(postDetails)
                ret += '<p class="attribution" id="yourComment_' + postDetails.postID + '">';
                    
                    ret += getPrivacyType(postDetails);
                    
                    ret += 'by <a class="cursor imp" data-auth="' + postDetails.authName + '"  href="'+ new_path + postDetails.profile + '" id="postUser_' + postDetails.postID + '" data-toggle="popover" data-placement="top">' + postDetails.createdBy + '</a> at ';
                
                                // PopOver
                                ret += '<label id="popover_content_wrapper_' + postDetails.postID + '" class="display">';
                                    ret += '<label class="pull-left">';
                                        ret += postDetails.images;
                                    ret += '</label>';
                                    ret += '<label class="pull-right">';
                                        ret += '&nbsp;&nbsp;<b>Name: ' + postDetails.createdBy + '</b><br />';
                                        ret += '&nbsp;&nbsp;<b>Position:</b> ' + postDetails.position + '<br />';
                                    ret += '</label>';
                                ret += '</label>';
                            ret += '<label class="timeago" title="' + postDetails.date + '"></label>';
                            ret += '<label class="pull-right fl-post-attribution">';
                            
                            if(likeDetails!=null){
                                var like = likeDetails[0]['likeBtn'];
                                var likeCount = likeDetails[0]['likeCount'];
                                var likeID = likeDetails[0]['likeID'];
                                var userID = likeDetails[0]['userID'];
                                var postedID = likeDetails[0]['postedID'];
                                var user = likeDetails[0]['user'];
                                var likeType = likeDetails[0]['likeType'];
                            }else{
                                var like = ""; var likeCount = ""; var likeID = "";
                                var userID = ""; var postedID = ""; var user = ""; var likeType = "";
                            }
                                // Seen    
                                    var seen_array = [];
                                    var seen_ret = "";
                                    var countSeen = "0";
                                    $("body").data("Seen_" + postDetails.postID, postDetails.getSeen);
                                    if (postDetails.getSeen!=null) {
                                        var getSeen = postDetails.getSeen;
                                        for (var s = 0; s<getSeen.length; s++) {
                                            seen_array.push(getSeen[s].nameSeen);
                                            
                                        }
                                        var seen_lenth = parseInt(seen_array.length)-10;
                                        for (var useen=0;useen<10;useen++) {
                                            //code
                                        
                                        //$.each(seen_array,function(id,val){
                                            //if (seen_array[id]!=postDetails.authName) {
                                            //console.log(seen_array[useen])
                                            //if (seen_array.length>10) {
                                            if (seen_array[useen]!=undefined||seen_array[useen]!=null) {
                                                seen_ret += seen_array[useen] + "<br>";
                                            }
                                                
                                                
                                            //}else{
                                            //    seen_ret += seen_array[id] + "<br>";
                                            //}
                                                
                                                //countSeen = parseInt(getSeen.length) - 1;
                                            //}else{
                                                countSeen = getSeen.length;
                                            //}
                                        }
                                        
                                        if (seen_lenth > 0 && seen_lenth!=0) {
                                            
                                            seen_ret += " and " + seen_lenth + " others ";
                                        }
                                            
                                        //});
                                    }
                                    //console.log(seen_ret)
                                        if (countSeen!=0) {
                                            var display = "";
                                        }else{
                                            var display = "display";
                                        }  
                                            ret += '<a class="cursor viewSeen dataTip ' + display + '  imp seen_' + postDetails.postID + '_post" data-type="post" data-original-title="' + seen_ret + '" data-id="' + postDetails.postID + '"  id="seen_' + postDetails.postID + '_post">Seen by <span id="count_seen_' + postDetails.postID + '">' + countSeen + '</spa></a> &nbsp;&nbsp;';
                                        
                                    if(like!="1"){
                                        ret += '<a class="cursor dataTip likePost fa fa-thumbs-o-up imp like_' + postDetails.postID + '_post" data-type="post" data-original-title="Like Post" author-id="' + postDetails.userID + '" data-id="' + postDetails.postID + '" data-like-id="'+likeID+'" id="like_' + postDetails.postID + '_post"></a> &nbsp;&nbsp;';
                                    }else{
                                        ret += '<a class="cursor dataTip unlikePost  fa fa-thumbs-o-down imp like_' + postDetails.postID + '_post" data-type="post" data-original-title="Unlike Post" author-id="' + postDetails.userID + '" data-id="' + postDetails.postID + '" data-like-id="'+likeID+'" id="like_' + postDetails.postID + '_post"></a> &nbsp;&nbsp;';
                                    }
                                    ret += '<a href="' + postDetails.link + '" class="cursor dataTip fa fa-link" data-original-title="Show link"></a> &nbsp;&nbsp;';
                                    
                                    //
                                    ret += '<a class="cursor dataTip showReply imp fa fa-reply-all" data-type="0" data-original-title="Show Reply Field" data-id="' + postDetails.postID + '"></a> ';

                                    if(postDetails.userID==postDetails.authID||postDetails.user_level_id=="2"||postDetails.user_level_id=="1"){
                                        ret += '&nbsp;&nbsp;<a class="cursor delete dataTip imp fa fa-trash-o" data-original-title="Delete Post" data-id="' + postDetails.postID + '" data-action="deletePost"></a>';
                                    }
                            ret += '</label>';
                ret += '</p>';
                  
                ret += '<p class="attribution" style="margin-left:5px;height:10px; margin-top:7px;">';
                        ret += '<label class="pull-right">';    
                            // User who like the post Container
                            ret += '<label class="likeUsers likes_' + postDetails.postID  + '_post" id="likes_' + postDetails.postID  + '_post">';
                           
                                        // Match if userID == Who liked the post
                                        var gLike = "";
                                        if(likeDetails!=null){
                                            
                                            $.each(likeDetails, function(id, val) {
                                                var userID = likeDetails[id]['userID'];
                                                var postedID = likeDetails[id]['postedID'];
                                                if(userID==postDetails.authID){
                                                    gLike = "Correct";
                                                }
                                            });
                                            // Load Who like the post
                                            ret += getLikes(likeCount,userID,postDetails.authID,user,gLike,likeDetails,likeType,"post");
                                        }
                                            
                            ret += '</label>';
                        ret += '</label>';
                    
                        // Comment Count
                        if(commentDetails==null){
                            var countComment = "";
                        }else{
                            var countComment = commentDetails[0]['countComment'];
                            
                            if (postDetails.view_all=="yes") {
                                var countComment = 0;
                            }else{
                                var countComment = parseInt(countComment) - 5;;
                            }
                            
                            if(countComment>=5){
                                //console.log("With COmment = " + "(" + postDetails.post + ")" + countComment);
                                var lastCommentID = commentDetails[4].commentID;
                                ret += '<label id="loadCommentMore_'+lastCommentID+'" style="margin-left:5px;">';
                                    ret += '<label class="pull-left">';
                                        ret += '<i class="icon-comments-alt"></i>&nbsp;<a class="cursor loadComment imp" post-id="' + postDetails.postID + '" data-id="'+lastCommentID+'">view '+countComment+' more comments.</a>';
                                        ret += '&nbsp;&nbsp; <img src="/images/loader/load.gif" class="display" id="loading_'+lastCommentID+'" style="margin-right:10px;">';
                                    ret += '</label>';
                                ret += '</label>';
                            }
                        }
                    
                ret += '</p>';
                
                if (postDetails.view_all=="yes") {
                    var display = "";
                }else{
                    var display = "display";
                }
                
                
                // Field to Add Comment on the post
                ret += '<div id="commentView_' + postDetails.postID + '" class="commentView_' + postDetails.postID + '">';
                    ret += '<form id="postFile_' + postDetails.postID + '" method="post" enctype="multipart/form-data" action="/ajax/uploadFile">';
                        ret += '<p class="attribution ' + display + ' attachment_comment_' + postDetails.postID + '_'+'0">';
                                if(commentDetails==null){
                                    var countComment = "";
                                }else{
                                    var countComment = commentDetails[0]['countComment'];
                                    
                                    if (postDetails.view_all=="yes") {
                                        countComment = 0;
                                    }else{
                                        countComment = parseInt(countComment) - 2;
                                    }
                                    
                                    if(countComment>=2){
                                        ret += "<br>";
                                    }
                                }
                                ret += '<i class="fa fa-paperclip"></i>' + '<input type="file" data-get-file="imagePost_' + postDetails.postID + '_'+'0" data-action-id="' + postDetails.postID + '" value="upload" name="postFile" id="uploadForm" data-form="postFile_' + postDetails.postID + '" data-show="imagePreviewUpload_' + postDetails.postID + '_'+'0" data-location="previewImagePostUpload_' + postDetails.postID + '_'+'0" size="24" data-action-type="postFile" style="width: 58px;opacity: 0;position:absolute;" class="cursor postFile">';
                                ret += '<input type="hidden" value="postFileUpload" name="uploadType">';
                                ret += '<input type="hidden" value="commentAttachment" name="uploadFolder">';
                                ret += '<a class="cursor imp dataTip " data-toggle="popover" data-placement="top" data-original-title="Attach File" title=""><i class="icon-paper-clip"></i> Attach Files</a>';
                                
                            ret += '<div style="padding: 5px;">'
                                    ret +=     '<div class="imagePreviewUpload_' + postDetails.postID + '_'+'0 display" style="height:60px; float: left;width: 100%;border: 1px solid #D5D5D5;background-color: #fff;">';
                                    ret +=        '<div style="padding: 5px;" class="previewImagePostUpload_' + postDetails.postID + '_'+'0">';
                         
                                    ret +=        '</div>';
                                    ret +=     '</div>';
                                    ret += '</div>';
                        ret += '</p>';
                    ret += '</form>';
                    
                    
                    ret += '<textarea class=" fl-mentionContainer input-reply  reply ' + display + '  comment_' + postDetails.postID + '_'+'0" data-get-file="imagePost_' + postDetails.postID + '_'+'0" data-location="commentAttachment"  data-file="imagePreviewUpload_' + postDetails.postID + '_'+'0" data-type="0" id="comment_' + postDetails.postID + '_'+'0" data-id="' + postDetails.postID + '" placeholder="Comment" wrap="off"></textarea>';
                    //ret += '<p class="attribution"><label class="icon-camera"></label></p>';
                        if(commentDetails!=null){
                            $.each(commentDetails, function(id, val){
                                var comments = commentDetails[id];
                                ret += replyPost(postDetails,uploadedDetails,likeDetails,comments);
                            });
                        }
                ret += '</div>';    
                
            ret += '</div>';
        ret += '</div>';
    return ret;
}


function loadEvent(postDetails,uploadedDetails,likeDetails,commentDetails){
    var ret = "";
        
        
            ret += '<div class="fl-widget-info">';
                ret += '<div class="fl-widget-content fl-widget-contentv2">';
                    
                    ret += '<div class="fl-avatar">';
                         ret += postDetails.images;
                    ret += '</div>';

                    ret += '<div class="fl-widget-cpnyevnts-content-wrapperv2 theme-font-color">';

                        ret += '<div class="fl-wcheader">';
                            ret += ' <div class="fl-wchdate"><span>Posted <label class="timeago" title="' + postDetails.date  + '"></label></span></div>';
                            ret += '<div class="fl-wchname"><span>'+ postDetails.createdBy +'</span></div>';
                        ret += '</div>';  
                        ret += '<a href="' + postDetails.link + '" class="cursor dataTip fl-events-data">';// data-original-title="Show link"        
                            ret += '<span class="postEmoticon post_text_' + postDetails.postID + '">' + /*val.substr(0, 500)*/ showMore(postDetails.post,postDetails.postID,"post") + '</span>';
                        ret += '</a>';
                    ret += '</div>';

                ret += '</div>';
            ret += '</div>';
        

    return ret;
}



/*
 * Create reply
 * @replyID - ID of the created reply
 * @img - user Avatar
 * @reply - comment of the user to the post
 * @replyBy - the one who create the reply
 * @datePosted - date when user reply to the post
 * @postID - id of the created post
 * @likes - likes on the reply
 * @authID - the one who currently logged on the system
 * @commentIDBy - the one who comment on the post
 * @user_level_id - if user == 1 (Admin)
 */
function replyPost(postDetails,uploadedDetails,likeDetails,comments){
    var pathname = window.location.pathname;
    var user_view = $("#user_url_view").val();
    var split_pathname = pathname.split("/");
    if (split_pathname[2]=="announcements" || split_pathname[2]=="hashtag" || split_pathname[2]=="post" || split_pathname[2]=="workspace") {
        var new_path = user_view;
    }else{
        var new_path = "/";
    }
    //console.log(comments)
    if (comments.U_user_level_id != "4") {
	var img_link = comments.images;
	var cursor = "default";
    }else{
	var img_link = comments.guest_avatarPic;
	var cursor = "pointer";
    }
    
    var ret ="";
    var likeReply = comments.getLikeComment;
    ret += '<div class="reply" id="reply_' + comments.commentID + '" author-id="' + comments.postedBy + '">';
        ret += '<a class="message-img" style="cursor:' + cursor + ';">' + img_link + '</a>';
        ret += '<div class="message-body">';
            ret += '<div class="text" id="postText_' + comments.commentID + '">';
            ret += '<p class="postEmoticon">';
                ret += showMore(comments.comment,comments.commentID,"reply");
            ret += '</p>';
                var postImage = comments.attachment;
                
                var valid_formats = ["jpg", "png", "JPG", "PNG", "GIF", "gif"];
                if (postImage.extension_img.length!=0) {
                        var w = "";
                        var h = "";
                        // Image
                        if (postImage.extension_img.length!=0) {
                        var folderName = postImage.postFolderName; 
                        var listImg = postImage.img;
                        // Image Class
                            var imageView = "imageView openImageView";
                            //width
                            if (postImage.image_resize[0] > 600) {
                                w = "500";
                            }else{
                                w = postImage.image_resize[0];
                            }
                            // Height
                            if (postImage.image_resize[1] > 600) {
                                h = "400";
                            }else{
                                h = postImage.image_resize[1];
                            }
                        for (var a in listImg) {
                                var listImg_extension = listImg[a].split(".");
                                var extension = listImg_extension[listImg_extension.length - 1];
                                if (valid_formats.indexOf(extension)!=-1) {
                                    
                                    if (postImage.lengthImg=="1") {
                                        ret += '<center>';
                                            ret += '<img src="/images/commentAttachment/' + folderName + '/' + listImg[a] + '" class=" '+ imageView +' group_1_' + comments.commentID + '"  href="/images/commentAttachment/' + folderName + '/' + listImg[a] + '"  width="'+ w +'" height="'+ h +'">';
                                        ret += '</center>';
                                    }else if (postImage.lengthImg=="2") {
                                        ret += '<img src="/images/commentAttachment/' + folderName + '/' + listImg[a] + '" class=" '+ imageView +' group_1_' + comments.commentID + '"  href="/images/commentAttachment/' + folderName + '/' + listImg[a] + '"  style="width: 40%;">';
                                    }else if (postImage.lengthImg=="3") {
                                        ret += '<img src="/images/commentAttachment/' + folderName + '/' + listImg[a] + '" class=" '+ imageView +' group_1_' + comments.commentID + '"  href="/images/commentAttachment/' + folderName + '/' + listImg[a] + '"  style="width: 30%;">';
                                    }else if (postImage.lengthImg > 4) {
                                        ret += '<img src="/images/commentAttachment/' + folderName + '/' + listImg[a] + '" class=" '+ imageView +' group_1_' + comments.commentID + '"  href="/images/commentAttachment/' + folderName + '/' + listImg[a] + '"  style="width: 30%;">';
                                    }else if (postImage.lengthImg == "4"){
                                        ret += '<img src="/images/commentAttachment/' + folderName + '/' + listImg[0] + '" class=" '+ imageView +' group_1_' + comments.commentID + '"  href="/images/commentAttachment/' + folderName + '/' + listImg[0] + '"  style="width: 30%;">';
                                        ret += '<img src="/images/commentAttachment/' + folderName + '/' + listImg[1] + '" class=" '+ imageView +' group_1_' + comments.commentID + '"  href="/images/commentAttachment/' + folderName + '/' + listImg[1] + '"  style="width: 30%;">';
                                        ret += '<img src="/images/commentAttachment/' + folderName + '/' + listImg[2] + '" class=" '+ imageView +' group_1_' + comments.commentID + '"  href="/images/commentAttachment/' + folderName + '/' + listImg[2] + '"  style="width: 30%;">';
                                        ret += '<img src="/images/commentAttachment/' + folderName + '/' + listImg[3] + '" class=" '+ imageView +' group_1_' + comments.commentID + '"  href="/images/commentAttachment/' + folderName + '/' + listImg[3] + '"    width="'+ w +'" height="'+ h +'">';
                                        
                                        break;
                                    }
                                    
                                }
                           }
                        }
                }
                if(postImage.extension_file.length!=0){
                        // Files
                          if(postImage.extension_file.length!=0){
                          var folderName = postImage.postFolderName; 
                          var listImg = postImage.img;
                         
                          for (var a in listImg) {
                                  var listImg_extension = listImg[a].split(".");
                                  var extension = listImg_extension[listImg_extension.length - 1];
                                  if (valid_formats.indexOf(extension)==-1) {
                                  ret += '<form method="POST" style="text-decoration: underline;"><input type="submit" class="cursor tip" data-original-title="' + listImg[a] + '" style="opacity: 0;position: absolute;width: 100%;"><div class="">';
                                          ret += '<a class="cursor apps">';
                                          var num = "1";
                                                  ret += num++ + ") " + listImg[a];
                                          ret += '</a>';
                                  ret += '</div>';
                                  ret += '<input type="hidden" value="' + listImg[a] + '" name="attachment_filename">';
                                  ret += '<input type="hidden" value="images/commentAttachment/' + folderName + '/' + listImg[a] + '" name="attachment_location">';
                                  ret += '</form>';
                                  }
                         }
                          }
                }
            ret +='</div>';
	//    if (comments.user_level_id != "4") {
	//	var link = '<a href="'+ new_path + comments.profile + '">' + comments.commentBy + '</a>';
	//    }else{
		var link = comments.commentBy;
	    //}
            if(postDetails!="print"){
                ret += '<p class="attribution">by ' + link + '  at <label class="timeago" title="' + comments.datePosted + '"></label>';
            }else{
                ret += '<p class="attribution">by ' + link + ' at <label class="timeago" title="">' + comments.datePosted + '</label>';
            }
                ret += '<label class="pull-right fl-post-attribution">';
                 //Like clicking option
                 
                if(likeReply!=null){
                    //console.log(likeReply[0])
                    var like = likeReply[0]['likeBtn'];
                    var likeCount = likeReply[0]['likeCount'];
                    var likeID = likeReply[0]['likeID'];
                    var userID = likeReply[0]['userID'];
                    var postedID = likeReply[0]['postedID'];
                    var user = likeReply[0]['user'];
                    var likeType = likeReply[0]['likeType'];
                }else{
                    var like = ""; var likeCount = ""; var likeID = "";
                    var userID = ""; var postedID = ""; var user = ""; var likeType = "";
                }
                if(postDetails!="print"){
                    if(like!="1"){
                        ret += '<a author-id="' + comments.postedBy + '" class="cursor fa fa-thumbs-o-up dataTip likePost imp like_' + comments.commentID + '_reply" data-type="reply" data-original-title="Like Comment" data-id="' + comments.commentID + '" data-like-id="' + comments.postID + '" id="like_' + comments.commentID + '_reply"></a>';
                    }else{
                        ret += '<a author-id="' + comments.postedBy + '" class="cursor fa fa-thumbs-o-down dataTip unlikePost imp like_' + comments.commentID + '_reply" data-type="reply" data-original-title="Unlike Comment" data-id="' + comments.commentID + '" data-like-id="' + comments.postID + '" id="like_' + comments.commentID + '_reply"></a>';
                    }
                    
                // Delete Comment
                    if(comments.authID==comments.postedBy||comments.user_level_id=="2"||comments.user_level_id=="1"){
                        ret += '&nbsp;&nbsp;<a class="fa fa-trash-o cursor delete dataTip imp" data-original-title="Delete Comment" data-id="' + comments.commentID + '" data-action="deleteComment"></a>';
                    }
                }
                    
                ret += '</label>';
            ret += '</p>';
            
            // Likes Comment
            ret += '<p class="attribution" style="margin-left:5px;height:10px;margin-top:7px;">';
                    ret += '<label class="pull-right">';
                    
                        // User who like the post Container
                        ret += '<label class="likeUsers likes_' + comments.commentID  + '_reply" id="likes_' + comments.commentID  + '_reply">';
                        
                                    // Match if userID == Who liked the post
                                        var gLike = "";
                                        if(likeReply!=null){
                                            $.each(likeReply, function(id, val) {
                                                var userID = likeReply[id]['userID'];
                                                var postedID = likeReply[id]['postedID'];
                                                if(userID==comments.authID){
                                                    gLike = "Correct";
                                                }
                                                
                                            });
                                            // Load Who like the post
                                            ret += getLikes(likeCount,userID,comments.authID,user,gLike,likeReply,likeType,"reply");
                                        }
                        ret += '</label>';
                    ret += '</label>';
            ret += '</p>';
                    
        ret += '</div>';
    ret += '</div>';
    
    return ret;

}

/*
 * Get Like Functions
 * @likeCount - number of like on the post
 * @userID - the one who like the post
 * @authID - the one who currently logged on the site
 * @user - Fullname of the user who like the post
 * @gLike - check if user who logged is equal to the one who like the post
 * @likes - likes on the post
 * @likeType - if user like the post/reply
 * @type - default type of the like
 * 
 */

function getLikes(likeCount,userID,authID,user,gLike,likes,likeType,type){
    var ret = "";
    var count = likeCount-1;
    //console.log(likeCount + "==" +  "1" + "\n" + userID + "==" + authID + "\n" + likeType + "==" + type + "\n" + gLike + "==" + "Correct")
    if(likeCount >= 2 && gLike == "Correct" && likeType == type){
        ret += "<i class='icon-thumbs-up'></i>&nbsp;&nbsp;<a class='imp cursor'>You</a> and <a class='dataTip imp cursor' data-original-title='";
                $.each(likes, function(id, val) {
                    var userLike = likes[id]['user'];
                    var likeID = likes[id]['likeID'];
                    if(userLike!="You"){
                        ret += userLike;
                        if(userLike!=""){
                            ret += "<br />";
                        }
                    }
                });
        ret += "'>"+ count +" other people</a> like this.";
    }else{
        if(likeCount >= 2 && userID != authID && likeType == type){
            
            if(likeCount!=""){
                ret += "<i class='icon-thumbs-up'></i>&nbsp;&nbsp;<a class='dataTip imp cursor' data-original-title='";
                    $.each(likes, function(id, val) {
                        var userLike = likes[id]['user'];
                        var likeID = likes[id]['likeID'];
                        if(userLike!="You"){
                            ret += userLike;
                            if(userLike!=""){
                                ret += "<br />";
                            }
                        }
                    });
                ret += "'>"+likeCount + " people</a> like this.";
            }
        }else{
            if(likeCount == "1" && userID == authID && likeType == type){
                ret += "<i class='icon-thumbs-up'></i>&nbsp;&nbsp;<a class='imp cursor'>You</a> like this.";
            }else{
                if(likeCount == 1 && userID != authID && likeType == type){
                        ret += "<i class='icon-thumbs-up'></i>&nbsp;&nbsp;<a class='imp cursor'>"+user+ "</a> like this.";
                }else{
                    ret += "";
                }
            }
        }
    }
    return ret;
}




function nl2br(varTest){
	return varTest.replace(/&lt;br&gt;/g, "<br>").replace(/&lt;a&gt;/g, "<a>").replace(/&lt;\/\a&gt;/g, "</a>");
}


function showMore(val,postID,action){
    // val = htmlEntities(val);
    var eachLine = val.split('<br>');
    //console.log(eachLine)
    if(eachLine.length<="5"){
        num = "500";
    }else{
        num = "130";
    }
    var ret = "";
    val = replaceTagStr(replaceStrHashtags_decode(replaceURLWithHTMLLinks(htmlEntities(nl3br(replaceStrHashtags_encode(val))))))
    // val = replaceTagStr(replaceStrHashtags_decode(nl3br(replaceStrHashtags_encode(val))))
    
        if (eachLine.length<="5") {
            if(500<=val.length){
                ret +=  val.substr(0, 500) + " .... <br><br><a class='loadText cursor imp' data-action='"+action+"' data-post-id='"+postID+"'>Show More</a>";
                ret += '&nbsp;&nbsp; <img src="/images/loader/load.gif" class="display" id="loading_'+postID+'" style="margin-right:10px;">';
            }else{
                ret +=  val;
            }
        }else{
            //if(eachLine.length<="5"){
            //    ret +=  nl3br(val).substr(0, 50);
            //
            //}else{
                ret +=  val.substr(0, 100)+ " .... <br><br><a class='loadText cursor imp' data-action='"+action+"' data-post-id='"+postID+"'>Show More</a>";
                ret += '&nbsp;&nbsp; <img src="/images/loader/load.gif" class="display" id="loading_'+postID+'" style="margin-right:10px;">';
            
            //}
        }
    return ret;
}


function strip(html) {
    return html.replace(/<\/?([a-z][a-z0-9]*)\b[^>]*>?/gi, '').replace(/(\r\n|\n\r|\r|\n)/g, "<br>");
}

function replaceURLWithHTMLLinks(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    return text.replace(exp,"<a href='$1' target='_blank' class='imp'>$1</a>"); 
}

function postMention() {
    var exp = /(<([^>]+)>)/ig;
    return $(".mentionSelectedContainer > span").html().replace("<b>","<a>").replace("</b>","</a>");
}
function replaceTagStr(str){
    str = str.replace(/@\[([A-Z\s_a-z0-9]*):([0-9]*)\]/g,function(match, first, offset, s){
        var link = "";
        var pathname = window.location.pathname;

        // if(pathname=="/user_view/announcements"){
            link = first.replace(/\s+/g, '_');
            link = '/user_view/'+ link +'';
        // }else{
            // link = '/'+ first +'';
        // }
        // var name = first.split("_")
        return '<a href="'+ link +'">'+ first +'</a>';
    })
    return str;
}
function replaceStrHashtags_encode(str){
    str = str.replace(/<a class="hashtag-link">(#[a-zA-Z0-9]*)<\/a>/g,function(match, first, offset, s){
        return "#["+ first +":00]";
    })
    return replaceStrBreakLine_encode(str);
    // return str;
}
function replaceStrHashtags_decode(str,type){
    str = str.replace(/#\[(#[A-Za-z0-9]*):00\]/g,function(match, first, offset, s){
        return "<a class='hashtag-link'><span>"+ first + "</span></a>";
    })
    return replaceStrBreakLine_decode(str,type);
    // return str;
}
function convBreakLine(str){
    str = str.replace(/\n/g,function(match, first, offset, s){
        return "<br>";
    })
    return str;
}
function replaceStrBreakLine_encode(str){
    // console.log(str)
    str = str.replace(/&lt;br&gt;|<br>/g,function(match, first, offset, s){
        return "[br:00]";
    })
    return str;
}
function replaceStrBreakLine_decode(str,type){
    str = str.replace(/\[br:00\]/g,function(match, first, offset, s){
        if(type==1){
            return " ";
        }else{
            return "<br>";
        }
    })
    return str;
}

setHighlightComment = {
    "init" : function(){
        var comment_id = getParametersName("comment_id", window.location);
        $("#postText_"+comment_id).css({
            "box-shadow": "rgb(51, 181, 229) 1px 1px 10px",
            "transition": "0.5s",
            "-webkit-transition": "0.5s"
        })
        setTimeout(function(){
            $("#postText_"+comment_id).css({
            "box-shadow": "",
            })
        },3000)
    }
}


function getPrivacyType(postDetails){
    var ret = "";
    var tooltip = "Public";
    
    var icon = getPrivacyIcon(postDetails['postPrivacyType']);
    
    if (postDetails['postPrivacyType']>0) {
        privacyDetails = postDetails['postPrivacyDetails'];
        tooltip = "";
        for(var i in privacyDetails){
            tooltip += privacyDetails[i]+"<br />";
            if (i==9) {
                tooltip+="Click to see more.";
                icon += " cursor getMorePrivacyTagged";
                break;
            }
        }
    }
    ret += '<i class="icon-globe dataTip  '+ icon +'" json-data="'+ htmlEntities(JSON.stringify(postDetails)) +'" data-original-title="'+ tooltip +'"></i>&nbsp;';
    
    return ret;
}

function getPrivacyTypeName(type){
    var ret = "Home";
    if (type=="1") {
        ret = "Group";
    }else if(type=="2") {
        ret = "Department";
    }else if(type=="3") {
        ret = "Position";
    }
    
    return ret;
}


function getPrivacyIcon(type){
    var icon = "fa fa-globe";
    if(type==1){
        //group
        icon = "fa fa-lock";
    }else if(type==2){
        //department
        icon = "fa fa-sitemap";
    }else if(type==3){
        //position
        icon = "fa fa-user";
    }
    
    return icon;
}