var jsonCustomReportData = {
    search_value: "",   
    start: 0,
    limit: 10,
}
var columnSort = [];
$(document).ready(function() {
    $("body").on("click",".delete_printReport",function(){
        var id = $(this).attr("data-id")
        delete_form("print_report", id, $(this));
        
    })
    
	$("body").on("keyup","#txtSearchCustomReportDatatable",function(e){
        if (e.keyCode == "13") {
            $(".dataTable_custom_report").dataTable().fnDestroy();
            jsonCustomReportData['search_value'] = $(this).val();
            GetCustomReportDataTable.defaultData();
        }
    })
    //button
    $("body").on("click","#btnSearchCustomReportDatatable",function(e){
        $(".dataTable_custom_report").dataTable().fnDestroy();
        jsonCustomReportData['search_value'] = $("#txtSearchCustomReportDatatable").val();
        GetCustomReportDataTable.defaultData();
    })
    GetCustomReportDataTable.defaultData();

    $(".searchPrintFormLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonCustomReportData['limit'] = val;
            $(".dataTable_custom_report").dataTable().fnDestroy();
            // var self = this;
            var oTable = GetCustomReportDataTable.defaultData();

        // }
    })
    $("body").on("click",".dataTable_custom_report th",function(){
        var cursor = $(this).css("cursor");
        jsonCustomReportData['column-sort'] = $(this).attr("field_name");
        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest(".dataTable_custom_report").find(".sortable-image").html("");
        if(indexcolumnSort==-1){
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonCustomReportData['column-sort-type'] = "ASC";
        }else{
            columnSort.splice(indexcolumnSort,1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonCustomReportData['column-sort-type'] = "DESC";
        }
        
        $(".dataTable_custom_report").dataTable().fnDestroy();
        var self = this;
        GetCustomReportDataTable.defaultData(function(){
             addIndexClassOnSort(self);
        });
    })
})

GetCustomReportDataTable = {
    "defaultData" : function(callback){
        var oColReorder = {
            allowReorder : false,
            allowResize : true
        };
        var oTable = $(".dataTable_custom_report").dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> '+
                       '<div class="bar1"></div> '+
                       '<div class="bar2"></div> '+
                       '<div class="bar3"></div> '+
                       '<div class="bar4"></div> '+
                       '<div class="bar5"></div> '+
                       '<div class="bar6"></div> '+
                      '<div class="bar7"></div> '+
                      '<div class="bar8"></div> '+
                      '<div class="bar9"></div> '+
                      '<div class="bar10"></div> '+
                 '</div>'
            },
            "oColReorder" : oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/search",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "action", "value": "getCustomizeReportDataTable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "search_value", "value": $.trim(jsonCustomReportData['search_value'])});
                aoData.push({"name": "limit", "value": jsonCustomReportData['limit']});
                aoData.push({"name": "column-sort", "value": jsonCustomReportData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonCustomReportData['column-sort-type']});
                $.ajax({
                    // "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        console.log(data)
                        data = JSON.parse(data);
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
                var obj = '.dataTable_custom_report';
                var listOfappContainer = '.fl-list-of-app-record-wrapper';
                dataTable_widget(obj, listOfappContainer);
                // $(".tip").tooltip();

            },
            fnDrawCallback : function(){
                set_dataTable_th_width([1]);
                setDatatableTooltip(".dataTable_widget");
               
                if (callback) {
                    callback(1);
                }
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonCustomReportData['limit']),
        });
        return oTable;
    }
}