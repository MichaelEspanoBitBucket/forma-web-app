$(document).ready(function(){
	PublicEmbedSetup.init();
});
var PublicEmbedSetup = {};

PublicEmbedSetup.init = function(){
	var self = this;
	if(getParametersName("public_forma_page", window.location) == "1" && window.pathname == "/user_view/workspace"){
		self.originProtocol = getParametersName("protocol", window.location);
		self.originPort = getParametersName("port", window.location);
		self.originName = getParametersName("hostname", window.location);
		self.applySettings();
	}else if(window.pathname == "/user_view/formbuilder"){
		self.castInitialEvents()
		.then(function(e,ui){
			self.loadFormObjectsTo(ui['public-embed-object-list']);
			self.castDialogEvents(this);
		});
	}
}
PublicEmbedSetup.sendParentMessage = function(message){
	if( window.parent == window ){
		return;
	}
	if($.type(message) == 'function'){
		message = message();
	}
	if($.type(message) == 'object'){
		window.parent.postMessage(JSON.stringify(message),this.originProtocol+'//'+this.originName+':'+this.originPort);
	}else{
		window.parent.postMessage(String(message),this.originProtocol+'//'+this.originName+':'+this.originPort);	
	}
}
PublicEmbedSetup.recieveParentMessage = function(callBack){
	var self = this;
	var defer = {};
	defer.then = function(cb){
		this.callBack = cb;
	};
	window.addEventListener("message", function (ev) {
		// Do we trust the sender of this message?  (might be
		// different from what we originally opened, for example).
		// if (ev.origin !== "http://example.org")
		//   return;
		// ev.source is popup
		// ev.data is "hi there yourself!  the secret response is: rheeeeet!"
		var temp = JSON.parse(ev.data);
		if($.type(callBack) == "function"){
			callBack.call(self,ev,temp);
		}
		if($.type(defer.callBack) == "function"){
			defer.callBack.call(self,ev,temp);
		}
	}, false);
	return defer;
}
PublicEmbedSetup.dialogChecker = function(dialogType){
	var dataParams = arguments;
	var dialogContainer = $('[id="popup_container"]');
	var message = dialogContainer.find('[id="popup_message"]').text();
	var response = $('[id="popup_container"]').find('[type="button"]').map(function(){
		return {
			'buttonIndex':$(this).index(),
			'caption':$.trim($(this).text())||$(this).attr('value')||''
		}
	}).get();
	this.sendParentMessage({
		'event':'dialog',
		'data':{
			'response':response,
			'message':message,
			'dialogType':dialogType	
		}
	});
}
PublicEmbedSetup.applySettings = function(){
	var public_forma_page_settings = $('body').data('user_form_json_data');
	var self = this;
	window.onbeforeunload = null;
	if(public_forma_page_settings.form_json.public_forma_settings){
		var temp = public_forma_page_settings.form_json.public_forma_settings;
		$('.setOBJ[data-object-id]').addClass('display2');
		$.each(
			temp.map(function(val){
				val.elementObject = $('.setOBJ[data-object-id="'+val.objectId+'"]:eq(0)');
				if(val.containerId){
					val.parentElementObject = $('.setOBJ[data-object-id="'+val.parentObjectId+'"]').find('[id="'+val.containerId+'"]:eq(0)');
				}else{
					val.parentElementObject = $('.loaded_form_content');
				}
				return val;
			}).sort(function(a,b){
				return a.sortIndex-b.sortIndex;
			}) , function(a,b){
				// if(b.containerId){
					$('.setOBJ[data-object-id="'+b.objectId+'"]:eq(0)').appendTo(b.parentElementObject);
				// }else{
				// 	$('.setOBJ[data-object-id="'+b.objectId+'"]').appendTo(b.parentElementObject);
				// }
				$('.setOBJ[data-object-id="'+b.objectId+'"]:eq(0)').removeClass('display2');
			}
		);
		// temp1.filter(function(val){
		// 	return val.parentObjectId == "";
		// }).sort(function(a,b){
		// 	return a.sortIndex - b.sortIndex;
		// })

	}
	self.sendParentMessage({
		'event':'load',
		'data':{
			height:$(document).outerHeight(true)+4,
			// height:($('*').map(function(){ return $(this).outerHeight(true); }).sort(function(a,b){ return b-a})[0]||0)+4,
			buttons:$('.workflow_buttons').children(':not(".fl-action-close")').map(function(a,b){
				return {
					caption:$(this).text(),
					buttonIndex:$(this).index()
				};
			}).get()
		}
	});
	self.recieveParentMessage()
	.then(function(ev, data){
		var self = this;
		if (data.event=="button_clicked") {
			$('.workflow_buttons').children().filter(function(){
				return $(this).index() == data.buttonIndex;
			}).trigger('click');
		} else if(data.event == 'loaded') {
			self.sendParentMessage({
				'event':'load',
				'data':{
					height:$(document).outerHeight(true)+4,
					// height:($('*').map(function(){ return $(this).outerHeight(true); }).sort(function(a,b){ return b-a})[0]||0)+4,
					buttons:$('.workflow_buttons').children(':not(".fl-action-close")').map(function(a,b){
						return {
							caption:$(this).text(),
							buttonIndex:$(this).index()
						};
					}).get()
				}
			});
		}
	});
};
PublicEmbedSetup.createPublicEmbedSetupDialog = function(){
	var ret
	=	'<div class="public-embed-dialog-overlay generic-dialog-overlay-container" title="click the overlay background to close the dialog">'+
			'<div class="generic-dialog-middle">'+
				'<div class="generic-dialog-center">'+
					'<div class="generic-dialog-content public-embed-dialog-content">'+
						'<div class="generic-dialog-content-header" style="height: 25px;font-size: 20px;background-color: #b0c4de;">Public Iframe</div>'+
						'<div class="generic-dialog-content-body" style="width: 440px; background-color:#dcdcdc;position:relative;">'+
							'<div class="public-embed-unused-object-list" style="position:absolute;background-color:gainsboro; left: 0px; margin-left: calc(100% + 20px); width:100%; height:100%"></div>'+
							'<div class="public-embed-object-list" style=" padding: 10px; max-height: 470px; min-height: 300px; width: calc(100% - 20px); overflow: hidden; position: relative;"></div>'+
						'</div>'+
						'<div class="generic-dialog-content-footer" style="height: auto;"><div class="fields" style=""><input type="button" class="public-iframe-save" id="public-iframe-save" value="OK" style="width: 100%;"></div></div>'+
					'</div>'
				'</div>'+
			'</div>'+
		'</div>';
	return ret;
}



PublicEmbedSetup.castInitialEvents = function(){
	var self = this;
	var defer = {};
	$("body").on("click",".public-embed-setup",function (ev) {
		if($(".public-embed-overlay").length <= 0){
			var createdPublicEmbedDialog = $(self.createPublicEmbedSetupDialog());
			$("body").append(createdPublicEmbedDialog);
			if(defer.callBack){
				var ui = {
					'public-embed-unused-object-list':createdPublicEmbedDialog.find('.public-embed-unused-object-list'),
					'public-embed-object-list':createdPublicEmbedDialog.find('.public-embed-object-list')
				};
				defer.callBack.call(createdPublicEmbedDialog,ev,ui);
			}
		}
	});
	defer.then = function(cb){
		this.callBack = cb;
	};
	return defer;
};
PublicEmbedSetup.castDialogEvents = function(uiTarget){
	var dialogSector = {};
	var sortableSettings = {
		// connectWith: '.public-embed-object-list,.public-embed-unused-object-list,.ui-tabs-panel',
		containment: "parent",
		// containment: ".mobility-body",
		items: '>.setObject',
		axis: 'y',
		// cursorAt: { top:0, left: 0 },
		tolerance: "pointer",
		// helper: function(e, ui) {
	        // var helper = $(ui.item).clone(); // Untested - I create my helper using other means...
	        // // jquery.ui.sortable will override width of class unless we set the style explicitly.
	        // helper.css({'height': '30px'});
	        // return helper;
	    // },
		// revert:true,
		placeholder:"sortable-placeholder",
		handle: "sortable-handle",
		start: function(e, ui){
			$(this).sortable( "refreshPositions" );
			$(this).sortable("option","dragHeight",$(ui.item).outerHeight());
			$(ui.item).css({
				"opacity":"0.8",
				"height":"30px",
			});
		},
		stop: function(e, ui){
			dialogSector.sortableContents.perfectScrollbar('update');
			$(ui.item).css({
				"opacity":"",
				"height":($(this).sortable("option","dragHeight"))+"px"
			});
		},
		over: function(e, ui){
			$(this).sortable( "refreshPositions" );
		},
	};

	dialogSector.closeOverlay = uiTarget.filter('.public-embed-dialog-overlay');
	dialogSector.sortableContents = uiTarget.find('.public-embed-object-list,.public-embed-unused-object-list,.ui-tabs-panel,.accordion-div-content');
	dialogSector.removeButton = uiTarget.find('remove-element');
	dialogSector.scrollContainers = dialogSector.sortableContents;
	dialogSector.saveButton = uiTarget.find('.public-iframe-save');

	dialogSector.closeOverlay.click(function(e){
		if($(e.target).parents('.public-embed-dialog-content').length == 0){
			$(this).fadeOut(function(){
	        	$(this).remove();
	        });
		}
	});

	dialogSector.removeButton.on('click',function(){
		var self = $(this);
		if( self.parent().is(':data("unusedSourceContainer")') ){
			self.parent().data("unusedSourceContainer").prepend(self.parent());
			self.parent().removeData("unusedSourceContainer");
		}else{
			self.parent().data("unusedSourceContainer", self.parent().parent());
			uiTarget.find('.public-embed-unused-object-list:eq(0)').prepend(self.parent());
		}
	});
	dialogSector.saveButton.on('click',function(){
		var public_settings = [];
		var temporary = {};
		uiTarget.find('.public-embed-object-list:eq(0)').find('.setObject[data-object-id]').map(function(){
			temporary.objectType = $(this).attr('data-type');
			temporary.objectId = $(this).attr('data-object-id');
			temporary.parentObjectId = $(this).parents('.setObject[data-object-id]:eq(0)').attr('data-object-id')||'';
			temporary.containerId = $(this).parent().attr('id');
			temporary.sortIndex = $(this).parent().children('.setObject[data-object-id]').get().indexOf(this);
			public_settings.push(temporary);
			temporary = {};
		});
		$('body').data('public_forma_settings',public_settings);
		showNotification({
            message: "Settings are set, please save the form.",
            type: "success",
            autoClose: true,
            duration: 3
        });
		uiTarget.trigger('click');
	});
	dialogSector.scrollContainers.perfectScrollbar();
	dialogSector.sortableContents.sortable(sortableSettings);
};

PublicEmbedSetup.loadFormObjectsTo = function(uiTarget){
	var self = this;
	var excludeObjectSelector = '.setObject[data-object-id][data-type="table"],.setObject[data-object-id][data-type="embeded-view"]';
	var targetObjectSelector = '.setObject[data-object-id]';
	var formObjects = $('.formbuilder_ws').children(targetObjectSelector).clone();
	var objectFormatSettings = {
		top:0,
		left:0,
		position:'relative',
		width:'100%'
	};
	var tabPanelSettings = {
        resizableContent: true,
        resizableContentContainment: ".workspace.formbuilder_ws"
        ,tabNavSortable:false
    };
    var accordionSettings = default_accordion_settings
    var sortableGUIHandle = '<sortable-handle style="cursor: ns-resize; position: absolute; z-index:4; width: auto; height: auto; display: inline-block; right: 20px; font-size: 20px;"><i class="fa fa-sort"></i></sortable-handle>';
	var removeGUIButton = '<remove-element style="font-size: 20px; position:absolute;z-index:5;width:100%;height:0;overflow:visible;text-align:right;"><i class="fl-delete fa fa-times" style="opacity:1"></i></div>';
	
	uiTarget.append(formObjects);

	formObjects.removeClass('component-primary-focus').removeClass('component-ancillary-focus');
	formObjects.css(objectFormatSettings);
	// formObjects.prepend(sortableGUIHandle);
	// formObjects.prepend(removeGUIButton);
	formObjects.find('.setObject-drag-handle').css('cursor','auto');

	//Accordion and Tab panel fielobject sortable gui's
	formObjects.find('.ui-tabs-panel, .ui-accordion-content').children(targetObjectSelector).add(formObjects).prepend(sortableGUIHandle);
	formObjects.find('.ui-tabs-panel, .ui-accordion-content').children(targetObjectSelector).add(formObjects).prepend(removeGUIButton);

	//Accordion and Tab panel functionality
	formObjects.find('.form-tabbable-pane').attr('class','getFields form-tabbable-pane').tabPanel(tabPanelSettings);
	formObjects.find('.form-accordion').removeClass('ui-accordion ui-widget ui-helper-reset ui-sortable').accordion(accordionSettings).sortable({
        axis: "y",
        handle: "h3",
        stop: function (event, ui) {
            // IE doesn't register the blur when sorting
            // so trigger focusout handlers to remove .ui-state-focus
            ui.item.children("h3").triggerHandler("focusout");

            // Refresh accordion to handle new order
            $(this).accordion("refresh");
        }
    });	
		
	//Accordion and Tab panel gui cleanup
	formObjects.find('.ui-tabs-panel, .ui-accordion-content').children(targetObjectSelector).css(objectFormatSettings);
	formObjects.find('.ui-tabs-panel, .ui-accordion-content').children(targetObjectSelector).removeClass('component-primary-focus').removeClass('component-ancillary-focus');
	formObjects.find('.form-tabbable-pane').children('.ui-tabs-panel').css({
	    'min-width':''
	});
	formObjects.find('.li-add-new-tab-panel').remove();
	formObjects.find('.edit-title-tab-panel').prev().remove();
	formObjects.find('.edit-title-tab-panel').remove();
	formObjects.find('.form-accordion-action-icon').remove();
	
	//Remove resizable ui
	formObjects.find('.ui-resizable-handle').hide();

	//Remove Table, Embed
	formObjects.filter(excludeObjectSelector).add(formObjects.find(excludeObjectSelector)).remove();
};