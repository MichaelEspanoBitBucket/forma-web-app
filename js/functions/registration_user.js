//showNotification({
//    message: "Invalid Username & Password, Please try again.",
//    type: "error",
//    autoClose: true,
//    duration: 3
//});
$(document).ready(function(){
var pathname = window.location.pathname;
/*-------------------------------------------------------------*/

        // RESETTING USER PASSWORD
        $("body").on("click","#reset_password",function(){
            ui.block();
            var new_password = $("#new_password").val();
            var retype_password = $("#retype_password").val();
            console.log(new_password +"=="+ retype_password);
            var user_id = $("#user_id").val();
            if (new_password != "" && retype_password != "") {
                if (new_password == retype_password) {
                    $.post("/ajax/regUser",{action:"user_reset_password",user_id:user_id,new_password:new_password,retype_password:retype_password},function(e){
                        if (e == "Successful") {
                            showNotification({  
                                    message: "Your password was successfully set. Please wait while redirecting to the login page...",
                                    type: "success",
                                    autoClose: true,
                                    duration: 5
                            })
                            ui.unblock();
                            //var url_redirect = $("#redirection").val();
                            setTimeout(function(){
                                window.location.replace('/login');
                            },5000)
                        }
                    });
                }else{
                    showNotification({
                            message: "Your Password doesn't match.",
                            type: "error",
                            autoClose: true,
                            duration: 3
                    })
                    ui.unblock();
                }
            }else{
                showNotification({
                        message: "Type your preferred password for your account.",
                        type: "error",
                        autoClose: true,
                        duration: 3
                })
                ui.unblock();
            }
            
        });
    

     //Company Registration
    $("body").on("click","#regUser",function(){
    
        $(".companyRegistration").ajaxForm(function(data){
	console.log(data)
            if(data=="Company was successfully created."){
                //$(".noti").html(setNotification("correct","correct","Your company was successfully registered. Please confirm your email address."))
                //setTimeout(function(){
                //    window.location.reload("/home");
                //},3000);
                showNotification({
                             message: data,
                             type: "success",
                             autoClose: true,
                             duration: 3
                         });
                $('.companyRegistration').trigger("reset");
            }else{
                //$(".noti").html(data)
		showNotification({
                             message: data,
                             type: "error",
                             autoClose: true,
                             duration: 3
                         });
            }
        });
    });
    // AD Registration
    $("body").on("click","#ad_reg",function(){

        var email = $("#email").val();
	var cred = $("#cred").val();
	var action = $("#action").val();
	var name = $("#name").val();
	var gi = $("gi").val();
	
	if (gi != "1-1") {
	    var load = "/home";
	}else{
	    var load = "/gi-dashboard-home";
	}

	$.post("/ajax/regUser",{email:email,cred:cred,action:action,name:name},function(data){
	 
	        if(data=="User was successfully saved."){
		    window.location.replace(load);
		}
	});
    }); 
    
 //    $("#batchRegistration").ajaxForm(function(data){
	// ui.block();
	// if(data=="All user was successfully registered."){
	//     //$(".noti").html(setNotification("correct","correct","Your company was successfully registered. Please confirm your email address."))
	//     showNotification({
 //                            message: data,
 //                            type: "success",
 //                            autoClose: true,
 //                            duration: 3
 //                        });
	//     //$("input").val(null);
	//     ui.unblock();
	// }else{
	//     showNotification({
 //                            message: data,
 //                            type: "error",
 //                            autoClose: true,
 //                            duration: 3
 //                        });
	//     ui.unblock();
	// }
 //    });

    $("body").on("click",".submit_users",function(){
        ui.block();
        $("#batchRegistration").ajaxForm(function(data){
            console.log(data);
            if(data=="All user was successfully registered."){
                //$(".noti").html(setNotification("correct","correct","Your company was successfully registered. Please confirm your email address."))
                showNotification({
                                    message: data,
                                    type: "success",
                                    autoClose: true,
                                    duration: 3
                                });
                //$("input").val(null);
                ui.unblock();
                $("#txtActivatedUserDatatable").trigger("keyup")
            }else{
                showNotification({
                                    message: data,
                                    type: "error",
                                    autoClose: true,
                                    duration: 3
                                });
                ui.unblock();
            }
            $(".fl-closeDialog").trigger("click");
        }).submit();
    })
    // With Navigation
    $(".guestRegistration").ajaxForm(function(data){
	//console.log(data)
	if(data=="User was successfully saved."){
	    //$(".noti").html(setNotification("correct","correct","Your company was successfully registered. Please confirm your email address."))
	    showNotification({
                            message: "User was successfully registered. Please check your email to activate your account.",
                            type: "success",
                            autoClose: true,
                            duration: 3
                        });
	    $("input[type='text']").val(null);
	    
	}else{
	    showNotification({
                            message: data,
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
	}
    });
//    $("body").on("click","#guestregUser",function(){
//	
//        $(".guestRegistration").ajaxForm(function(data){
//	    console.log(data)
//            if(data=="User was successfully saved."){
//                $(".noti").html(setNotification("correct","correct","Your company was successfully registered. Please confirm your email address."))
//                setTimeout(function(){
//                    window.location.reload("/home");
//                },3000);
//                
//            }else{
//                $(".noti").html(data)
//            }
//        });
//    });
/*-------------------------------------------------------------*/     
    


/*-------------------------------------------------------------*/     
    // Register User/member of the company
    $(".regUser").on("click",function(){
        var action = $(this).attr("data-user-action");
        var regId = $(this).attr("data-reg-id"); // registration id of the field
        
        var regEmail = $("#regEmail"+regId).val();
	var regUsername = $("#regUsername"+regId).val();
        var regDisplayName = $("#regDisplayName"+regId).val();
        var regFname = $("#regFname"+regId).val();
        var regLname = $("#regLname"+regId).val();
        var regPosition  = $("#regPosition"+regId).val();
        
        $(this).hide();
        $("#loadRegUser" + regId).show();
        
		if($.trim(regDisplayName) != "" && $.trim(regFname) != "" &&  $.trim(regLname) != ""){
        
			
            // Save to the database
            $.post("/ajax/regUser",{regEmail:regEmail,regDisplayName:regDisplayName,regFname:regFname,regUsername:regUsername,
                regLname:regLname,regPosition:regPosition,action:action}, function(data){
		console.log(data)
                    if(data=="User was successfully saved."){
                        showNotification({
                            message: data,
                            type: "success",
                            autoClose: true,
                            duration: 3
                        });
                        $('input[type="text"]'+".regUserField"+regId).val(null);
                        $(".regUserField"+regId).val("");
                        $("#regUser" + regId).show();
                        $("#loadRegUser" + regId).hide();
                    }else{
                        // var data = "test me to me smtp"

                        if (data.indexOf("SMTP") >= 0){
                            showNotification({
                                    message: "Connection lost, cannot connect to SMTP server. But user was successfully created.",
                                    type: "warning",
                                    autoClose: true,
                                    duration: 3
                                });
                                $('input[type="text"]'+".regUserField"+regId).val(null);
                        $(".regUserField"+regId).val("");
                        $("#regUser" + regId).show();
                        $("#loadRegUser" + regId).hide();
                        }else{
                            showNotification({
                                            message: data,
                                            type: "error",
                                            autoClose: true,
                                            duration: 3
                                        });
                                        $("#regEmail"+regId).addClass("frequired-red");
                                        $("#regEmail"+regId).tooltip('show');
                                         $("#regUser" + regId).show();
                                    $("#loadRegUser" + regId).hide();
                        }
                            
                    }
            });
		}else{
			requiredFields('input[type="text"]'+".regUserField"+regId);
			 showNotification({
				message: "Please fill those blank fields",
				type: "error",
				autoClose: true,
				duration: 3
			});
			$("#regUser" + regId).show();
                        $("#loadRegUser" + regId).hide();
		}
       
		
    });
    
        $(".regUserField").on("keyup",function(){
            var action = $(this).attr("data-req-action");
            if(action=="edit"){
                var regId = $(this).attr("data-user-id");
                requiredFields('input[type="text"]'+".usereditInfo_"+userID);
            }else{
                var regId = $(this).attr("data-reg-id"); // registration id of the field
                requiredFields('input[type="text"]'+".regUserField"+regId);
            }
            
        });
/*-------------------------------------------------------------*/         
       
        


/*-------------------------------------------------------------*/         
    // Editing User List
    $("body").on("click",".editUserListInfo", function(){
   
        userID = $(this).attr("data-user-id");
        type = $(this).attr("data-type");
            $.ajax({
                type : "POST",
                url  : "/ajax/regUser",
                data : {action:"getUserInfo",userID:userID},
                // dataType : "json",
                success : function(result){
var jsonParse = JSON.parse(result);
console.log(jsonParse)
var id = 0;
// var result[id] = jsonParse[id];
                    var ctr = 0;
		    var AD = $("#AD").val();

                    // $.each(result, function(id,val){
                        var userInformation = "";
                        // console.log(result[id].json_tbl)
                        ctr++;
                        userInformation += '<h3 class="fl-margin-bottom"><i class="icon-edit"></i> Edit User</h3>';
                        userInformation += '<div class="hr"></div>';
                        userInformation += '<div class=""><center>';
                            userInformation += jsonParse[id].images;
                        userInformation += '</center></div>';
                        userInformation += '<div class="Fields section clearing fl-field-style">';
                            userInformation += '<div class="column div_1_of_1">';
                                userInformation += '<span class=""> Email: </span>';
                                userInformation += '<div class="input_position">';
                                    userInformation += '<input type="text" name="updateUser" id="update_email" value="' + jsonParse[id].email + '" class="fl-edituser usereditInfo_' + jsonParse[id].userID + ' input-medium" style="margin-top:5px;">';
                                userInformation += '</div>';
                            userInformation += '</div>';
                        userInformation += '</div>';
			if (AD == "1") {
				//code
				
				userInformation += '<div class="Fields section clearing fl-field-style">';
                    userInformation += '<div class="column div_1_of_1">';
                        userInformation += '<span class="label_below2"> AD User Name: </span>';
                        userInformation += '<div class="input_position">';
                        userInformation += '<input type="text" name="updateUsername" id="update_username" value="' + jsonParse[id].username + '" class="fl-edituser usereditInfo_' + jsonParse[id].userID + ' input-medium" style="margin-top:5px;">';
                        userInformation += '</div>';
                    userInformation += '</div>';
				userInformation += '</div>';
			}
                        userInformation += '<div class="Fields section clearing fl-field-style">';

                                userInformation += '<div class="column div_1_of_1">';
                                    userInformation += '<span class=""> Firstname: </span>';
                                    userInformation += '<div class="input_position">';
                                        userInformation += '<input type="text" name="updateUser" id="update_firstname" value="' + jsonParse[id].firstName + '"class="fl-edituser  usereditInfo_' + jsonParse[id].userID + ' input-medium" style="margin-top:5px;">';
                                    userInformation += '</div>';
                                userInformation += '</div>';
                            userInformation += '</div>';
                            
                        userInformation += '</div>';
                        userInformation += '<div class="Fields section clearing fl-field-style">';
                                userInformation += '<div class="column div_1_of_1">';
                                    userInformation += '<span class=""> Lastname: </span>';
                                    userInformation += '<div class="input_position">';
                                        userInformation += '<input type="text" name="updateUser" id="update_lastname" value="' + jsonParse[id].lastName + '"class="fl-edituser usereditInfo_' + jsonParse[id].userID + ' input-medium" style="margin-top:5px;">';
                                    userInformation += '</div>';
                                userInformation += '</div>';
                            userInformation +='</div>';
                            
                        userInformation += '</div>';
                        userInformation += '<div class="Fields section clearing fl-field-style">';
                            userInformation += '<div class="column div_1_of_1">';
                                userInformation += '<span class=""> Account Name: </span>';
                                userInformation += '<div class="input_position">';
                                    userInformation += '<input type="text" name="updateUser" id="update_displayName" value="' + jsonParse[id].displayName + '"class="fl-edituser usereditInfo_' + jsonParse[id].userID + ' input-medium" style="margin-top:5px;">';
                                userInformation += '</div>';
                            userInformation += '</div>';
                        userInformation += '</div>';
			
                        if (jsonParse[id].userLeveID == "4") {
			    userInformation += '<div class="Fields fl-margin-bottom">';
				userInformation += '<div class="label_below2"> Expiration Date: </div>';
				userInformation += '<div class="input_position">';
				
				    userInformation += '<input type="text" name="updateUser" id="update_date" value="' + jsonParse[id].dateExpiredRegister + '"class=" fl-edituser usereditInfo_' + jsonParse[id].userID + ' input-medium" style="margin-top:5px;">';
				userInformation += '</div>';
			    userInformation += '</div>';
			}
			// console.log(result[id]);
			if (jsonParse[id].userLeveID != "4") {
                        userInformation += '<div class="Fields section clearing fl-field-style">';
                            userInformation += '<div class="column div_1_of_1">';
                                userInformation += '<span class=""> Position: </span>';
                                userInformation += '<div class="input_position">';
                                    // userInformation += '<input type="text" name="updateUser" id="update_position" value="' + result[id].position + '"class="fl-edituser usereditInfo_' + result[id].userID + ' input-medium" style="margin-top:5px;">';
                                    userInformation += '<select name="updateUser" id="update_position" class="fl-edituser usereditInfo_' + jsonParse[id].userID + ' simple-text-select" style="margin-top:5px;border: 1px solid #D5D5D5;"></select>';
                                userInformation += '</div>';
                            userInformation += '</div>';
                        userInformation += '</div>';
			}
                        userInformation += '<div class="section clearing fl-field-style">';
                            userInformation += '<div class="Fields column div_1_of_2">';
                                userInformation += '<div class="label_basic"> Active: </div>';
                                userInformation += '<div class="input_position">';
                                    userInformation += '<select class="usereditInfo_' + jsonParse[id].userID + ' input-medium" name="updateUser" id="update_active" style="margin-top:5px; width:100%;">';
                                        if (jsonParse[id].isActive=="1") {
                                            var active = 'selected=selected';
                                        }else{
                                            var not_active = 'selected=selected';
                                        }
                                        userInformation += '<option ' + active + ' value="1">Yes</option>';
                                        userInformation += '<option ' + not_active + ' value="0">No</option>';
                                    userInformation += '</select>';
                                userInformation += '</div>';
                            userInformation += '</div>';
                            userInformation += '<div class="fields column div_1_of_2">';
                                userInformation += '<div class="label_basic"> Privileges: </div>';
                                userInformation += '<div class="input_position">';
                                    userInformation += '<select class="usereditInfo_' + jsonParse[id].userID + ' input-medium" name="updateUser" id="update_privileges" style="margin-top:5px;width:100%;">';
                                        
                                        
                                        if(jsonParse[id].userLeveID=="3"){
                                                var userSelect = "selected=selected";
    					    var action_guide = "user";
                                        }
                                        if(jsonParse[id].userLeveID=="2"){
                                                var adminSelect = "selected=selected";
    					    var action_guide = "admin";
                                        }
    				    
                                        userInformation += '<option ' + adminSelect + ' value="2">Admin</option>';
                                        userInformation += '<option ' + userSelect + ' value="3">User</option>';
    				    if (jsonParse[id].userLeveID == "4") {
    					var guestSelect = "selected=selected";
    					var action_guide = "guest";
    					userInformation += '<option ' + guestSelect + ' value="4">Guest</option>';
    				    }
    				    
                                        
                                    userInformation += '</select>';
                                userInformation += '</div>';
                            userInformation += '</div>';
                            userInformation += '<div class="label_below2"><a class="isCursorPointer fl_badge_DarkRed" id="admin_reset_password">Reset Password</a></div>';
                            userInformation += '<div class="fields">';
                                userInformation += '<div class="label_basic"></div>';
                                userInformation += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                                    userInformation += '<input type="button" data-type="' + type + '" data-type-id="' + jsonParse[id].userLeveID + '" class="btn-blueBtn updateEditUser fl-margin-right" data-id="' + jsonParse[id].userID + '"  id="" value="Update"> ';
                                    userInformation += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
                                userInformation += '</div>';
                            userInformation += '</div>';
                        userInformation += '</div>';


                        //Prop
                       
                        var jsonProp = jsonParse[id].json_tbl;
                        var jsonPropDecode = $.parseJSON(jsonProp);
                        if(jsonPropDecode == null){
                            jsonPropDecode = {"post_property":"1","dashboard_property":"1","chat_property":1};
                        }
// console.log(jsonPropDecode)
                        
                        userInformation += '<div class="section clearing fl-field-style">';
                            userInformation += '<div class="Fields column div_1_of_2">';
                                userInformation += '<div class="label_basic"> Allow Post: </div>';
                                userInformation += '<div class="input_position">';
                                    userInformation += '<select class="usereditInfo_' + jsonParse[id].userID + ' input-medium" name="updateUser" id="update_post" style="margin-top:5px; width:100%;">';
                                        if (jsonPropDecode['post_property']=="1") {
                                            var p_active = 'selected=selected';
                                        }else{
                                            var p_not_active = 'selected=selected';
                                        }
                                        userInformation += '<option ' + p_active + ' value="1">Yes</option>';
                                        userInformation += '<option ' + p_not_active + ' value="0">No</option>';
                                    userInformation += '</select>';
                                userInformation += '</div>';
                            userInformation += '</div>';

                            userInformation += '<div class="Fields column div_1_of_2">';
                                userInformation += '<div class="label_basic"> Allow Dashboard Bookmark: </div>';
                                userInformation += '<div class="input_position">';
                                    userInformation += '<select class="usereditInfo_' + jsonParse[id].userID + ' input-medium" name="updateUser" id="update_dashboard" style="margin-top:5px; width:100%;">';
                                        if (jsonPropDecode['dashboard_property']=="1") {
                                            var d_active = 'selected=selected';
                                        }else{
                                            var d_not_active = 'selected=selected';
                                        }
                                        userInformation += '<option ' + d_active + ' value="1">Yes</option>';
                                        userInformation += '<option ' + d_not_active + ' value="0">No</option>';
                                    userInformation += '</select>';
                                userInformation += '</div>';
                            userInformation += '</div>';

                        userInformation += '<div class="section clearing fl-field-style">';
                            userInformation += '<div class="Fields column div_1_of_2">';
                                userInformation += '<div class="label_basic"> Allow Chat: </div>';
                                userInformation += '<div class="input_position">';
                                    userInformation += '<select class="usereditInfo_' + jsonParse[id].userID + ' input-medium" name="updateUser" id="update_chat" style="margin-top:5px; width:100%;">';
                                        if (jsonPropDecode['chat_property']=="1") {
                                            var c_active = 'selected=selected';
                                        }else{
                                            var c_not_active = 'selected=selected';
                                        }
                                        userInformation += '<option ' + c_active + ' value="1">Yes</option>';
                                        userInformation += '<option ' + c_not_active + ' value="0">No</option>';
                                    userInformation += '</select>';
                                userInformation += '</div>';
                            userInformation += '</div>';





                        // if(jsonParse.length==ctr){
                             var newDialog = new jDialog(userInformation, "","", "", "70", function(){});
                            newDialog.themeDialog("modal2");
                        // }
                        $( "#update_date" ).datetimepicker({dateFormat: 'yy-mm-dd',timeFormat:  "HH:mm:ss"});
                        $("#update_position").html($(".userRegPosition").html()).val(jsonParse[id].position);
                        
                    // });
                     
                    
                }
            });
            
            
        //$(".cancelEditUser").on("click",function(){
        //    userID = $(this).attr("data-user-id");
        //    // Hide Label
        //        $(".userviewInfo_"+userID).show();
        //        $("#editUserListInfo_"+userID).show();
        //        $("#deleteUser_"+userID).show();
        //    
        //    // Show Field
        //        $(".usereditInfo_"+userID).hide();
        //        $("#cancelEditUser_"+userID).hide();
        //        $("#updateEditUser_"+userID).hide();
        //        
        //    requiredFields('input[type="text"]'+".usereditInfo_"+userID);
        //});
    });
 /*-------------------------------------------------------------*/    
    $("body").on("click","#admin_reset_password",function(){
        var userID = $(this).closest("#popup_container").find(".updateEditUser").attr("data-id");
        var user_display_name = $.trim($("#update_displayName").val());
        var editFname = $("#update_firstname").val();
        var editLname = $("#update_lastname").val();
        var editEmail = $("#update_email").val();
        if(user_display_name==""){
            user_display_name = editFname+" "+editLname;
        }
        var otherText = "password of user "+ user_display_name +"?";
        if(userID==$.trim($("#current-user-id").text())){
            otherText = "your password?";
        }
        var newConfirm = new jConfirm("Are you sure you want to reset "+otherText, 'Confirmation Dialog','', '', '', function(r){
                if(r==true){
                    ui.block();
                    $.post("/ajax/regUser",{action:"admin_reset_user_password",userID:userID},function (e) {
                        console.log(e)
                        showNotification({
                           message: "Password has been successfully reset.",
                           type: "success",
                           autoClose: true,
                           duration: 5
                        });
                        ui.unblock();
                    })
                }
            });
        newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});
    })


/*-------------------------------------------------------------*/    
    // Delete User
    $("body").on("click",".deleteUserListInfo", function(){
       
        userID = $(this).attr("data-user-id");
        type = $(this).attr("data-type");
        action = "deleteUser";
        con = "Are you sure do you want to deactivate this user?";
      var newConfirm = new jConfirm(con, 'Confirmation Dialog','', '', '', function(r){
            if(r==true){
                $.post("/ajax/regUser",{action:action,loggedAccount:"del",userID:userID},function(data){
                    if(data=="Deactivate User logged."){
                        jConfirm("Deactivating logged account may cause of automatic log off. Do you want to proceed?", 'Confirmation Dialog','', '', '', function(res){
                            if(res==true){
                                $.post("/ajax/reguser",{action:action,loggedAccount:"logout",userID:userID},function(e){
                                    if(e=="User will now deactivate."){
                                        window.location.replace("/user/logout");
                                    }
                                });
                                
                            }
                        });
                    }else{
                        // Refresh Data table of user
                                
                        if(pathname=="/settings"){
                            $("#loadUser").dataTable().fnDestroy(); 
                            load.loadTableUser();
                        }else if(pathname=="/user_view/user_settings"){
                            $("#loadUser").dataTable().fnDestroy(); 
                            GetUserDataTable.defaultData();
                            $("#loadDeactivatedUser").dataTable().fnDestroy(); 
                            GetDeactivateUserDataTable.defaultData();
                            $("#loadDeactivatedUser").css("width","100%");
                        }

                    }
                    
                    //$("#userInfo_"+userID).animate({ backgroundColor: "whiteSmoke" }, "fast")
                    //                .animate({ opacity: "hide" }, "slow");
                });
                
            }
        });
        newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});
    });
/*-------------------------------------------------------------*/     
    



/*-------------------------------------------------------------*/    
    // Update User
    $("body").on("click",".updateEditUser", function(){
	   var pathname = window.location.pathname;
        var userID = $(this).attr("data-id");
        var type = $(this).attr("data-type");
	var data_type_id = $(this).attr('data-type-id');
        var editEmail = $("#update_email").val();
	var AD = $("#AD").val();
	if (AD == "1") {
		var editUsername = $("#update_username").val();
	}else{
		var editUsername = "Null";
	}
	
        var editFname = $("#update_firstname").val();
        var editLname = $("#update_lastname").val();
        var editDisplayName = $("#update_displayName").val();
        var editPosition = $("#update_position").val();
        var editActive = $("#update_active").val();
        var editPrivileges = $("#update_privileges").val();
	var update_date = $("#update_date").val();
        var action = "updateUser";
        requiredFields('input[type="updateUser"]'+".usereditInfo_"+userID);

        var json = {};
        $('[name="updateUser"]').each(function(){
            json[$(this).attr("id")] = $(this).val();
        });
        var json_string = JSON.stringify(json);
     
	if ($.trim(editEmail) != "") {
		if (isValidEmailAddress(editEmail) == true) {
			
			if ( $.trim(editFname) == "" ||  $.trim(editLname) == "") {
				showNotification({
				    message: "First name and Last name are mandatory fields.",
				    type: "error",
				    autoClose: true,
				    duration: 3
				});
			}else if ( $.trim(editUsername) == "") {
				showNotification({
				    message: "AD User Name is a mandatory field.",
				    type: "error",
				    autoClose: true,
				    duration: 3
				});
			}else{
			    // Update to the database
			    $.post("/ajax/regUser",{editUsername:editUsername,editEmail:editEmail,editFname:editFname,editLname:editLname,
				   editDisplayName:editDisplayName,editPosition:editPosition,editActive:editActive,
				   editPrivileges:editPrivileges,action:action,userID:userID,type:type,data_type_id:data_type_id,update_date:update_date,json:json_string}, function(data){
console.log(data);
                    
                    var dataJSON = JSON.parse(data);
                    var data = dataJSON;
				    if(data['prompt'] =="User was successfully updated."){
					showNotification({
					    message: "User has been successfully updated.",
					    type: "success",
					    autoClose: true,
					    duration: 3
					});
					// Remove Dialog Box
					$("#popup_container").remove();
					$("#popup_overlay").remove();
					
					if(pathname=="/settings"){	
					    if (type=="deactivate") {
						// Refresh Data table of user
						$("#loadUser").dataTable().fnDestroy(); 
						load.loadTableUser();
					    }else if (type=="activate"){
						$("#loadDeactivatedUser").dataTable().fnDestroy(); 
						load.loadTableDeactivatedUser();
						$("#loadDeactivatedUser").css("width","100%");
					    }
					}else if(pathname=="/user_view/user_settings"){
					    if (type == "deactivate") {
						$("#GuestloadUser").dataTable().fnDestroy();
						GuestUserDataTable.GuestdefaultData();
						$("#GuestloadUser").css("width","100%");
					    }else if(type == "activate"){
						$("#DeactivatedGuestloadUser").dataTable().fnDestroy();
						GuestUserDataTable.GuestDeactivateddefaultData();
						$("#DeactivatedGuestloadUser").css("width","100%");
					    }
						$("#loadUser").dataTable().fnDestroy(); 
						GetUserDataTable.defaultData();
						$("#loadDeactivatedUser").dataTable().fnDestroy(); 
						GetDeactivateUserDataTable.defaultData();
					    
					}
					
					
					
					   
				    }else{
                        $(".frequired-red").removeClass("frequired-red");
                       showNotification({
                                                message: data['prompt'],
                                                type: "error",
                                                autoClose: true,
                                                duration: 3
                                            });
                        $("#" + data['fields']).addClass("frequired-red");
                        $("#" + data['fields']).tooltip('show');
				    }
			    });
			}
		}else{
			showNotification({
				message: "Email is Invalid.",
				type: "error",
				autoClose: true,
				duration: 3
			})
		     
		}
	}else{
		showNotification({
		message: "Email is empty.",
		type: "error",
		autoClose: true,
		duration: 3
	    });
	}
        
    });
/*-------------------------------------------------------------*/     
    



/*-------------------------------------------------------------*/
















Forgot_password.setDialog();

});










function setNotification(container,icon,msg){
    var ret="";
    ret += '<div class="notification_wrapper">';
        ret += '<div class="'+container+'">';
            ret += '<img src="/images/warning/'+icon+'.png" width="22" height="19" class="img-notification-position">';
            ret += '<div class="notification-position">';    
               ret += msg;
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    return ret;
}

Forgot_password = {
    "setDialog" : function(){
        var self = this;
        $(".forgot_password").click(function(){
            var ret = "";
            ret += '<div style="float:left;width:50%;">';
                ret += '<h3 class="pull-left fl-margin-bottom">';
                    ret += '<i class="icon-asterisk"></i> ' + "Forgot Password";
                ret += '</h3>';
            ret += '</div>';
            //ret += '<div style="float:right;width:50%;text-align:right;margin-top:-5px;">';
            //    ret += '<input type="button" class="btn-basicBtn getFormForWorkflow" id="" value="Ok" node-data-id="" style="">';
            //ret += '</div>';
            ret += '<div class="hr"></div>';
                    ret += '<div class="fields">';
                        ret += '<div class="label_below2" style="color:#A7A4A4;font-size:10px; font-style: italic;"> Note: Enter your email address below to receive an email on how to reset your password.</div>';
                    ret += '</div>';
                    ret += '<div class="fields">';
                        ret += '<div class="label_below2" style="font-weight:bold"> Email Address:</div>';
                        ret += '<div class="input_position">';
                            ret += '<input type="email" class="tip form-text email-forgot">';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields">';
                        ret += '<div class="label_basic"></div>';
                        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                            ret += '<input type="button" class="btn-blueBtn forgot_submit fl-margin-right" value="Ok"> ';
                            ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
                        ret += '</div>';
                    ret += '</div>';
           var newDialog = new jDialog(ret, "", "",  "", "", function() {
            });
           newDialog.themeDialog("modal2");
            $(".email-forgot").focus();
            self.forgot_password();
        })
    },
    "forgot_password" : function () {
        $(".forgot_submit").click(function (argument) {
            var email = $(".email-forgot").val();
            ui.block();
            // $("#popup_cancel").click();
            $.post("/ajax/regUser",{action:"forgot_password",email:email},function (e) {
                try{
                    var data = JSON.parse(e);
                    showNotification({
                       message: data.message,
                       type: data.status,
                       autoClose: true,
                       duration: 5
                    });
                }catch(error){
                    showNotification({
                       message: "Please try again. Somethings going wrong",
                       type: "error",
                       autoClose: true,
                       duration: 5
                    });
                }

                ui.unblock();
            })
        })
    }
}