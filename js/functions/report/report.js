/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var report = {
    init: function () {
        this.setUsers();
        this.addEventListers();

        var report_id = $('#report_id').val();
        if (report_id != '0') {
            //edit mode
            this.getDetails();
        }
        var pathname = window.location.pathname;
        if (pathname == "/user_view/report") {
            workspace_functions.view_save(".save_workspace");
            control_save_workspace(".save_workspace");
        }
    },
    addEventListers: function () {
        var self = this;
        this.selectForm();
        // this.selectAllParameters();
        //this.deselectAllParameters();
        this.selectAllColumns();
        this.deselectAllColumns();
        this.sortColumns();
        this.save();

        this.appendParameter();
        this.deleteParameter();

        var activeFields = $('#active_fields').val();
        if ($.trim(activeFields) != '') {
            var activeFieldsArr = activeFields.split(',');
            for (var ctrField = 0; ctrField <= activeFieldsArr.length - 1; ctrField++) {
                formulaJson.push({
                    name: '@' + activeFieldsArr[ctrField],
                    ws: 1,
                    id: formulaJson.length + 1
                });
            }
        }

        console.log('formulaJson', formulaJson);
        $('body').on('click', '#value_static', function () {
            self.removeIDE();
        });

        $('body').on('click', '#value_computed', function () {
            self.setIDE();
        });

        $('body').on('click', '[name="report_column_grouping"]', function () {
            var groupings = $('[name="report_column_grouping"]:checked').val();
//            alert(groupings);

            if (groupings != 'none') {
                $('#column_sort_ascending').click();
                $('[name="report_column_sort"]').attr('disabled', 'disabled');
            } else {
                $('[name="report_column_sort"]').removeAttr('disabled');
            }
        });

//        $('body').on('click', '#column_group_categorized', function() {
//            $('#column_sort_ascending').click();
//        });

        $('body').on('click', '[action="add_column"]', function () {
            self.setColumnProperties();
        });

        $('body').on('click', '[action="add_plot_bond"]', function () {
            self.setChartPlotBand();
        });

        $('body').on('click', '[name="report_defaultValueType"]', function () {
            var selectedColumnType = $('input[name="report_defaultValueType"]:checked').val();

            if (selectedColumnType == 'field') {
                $('#report_field_column_value').removeClass('display');
                $('#report_columnValue').addClass('display');
            } else {
                $('#report_field_column_value').addClass('display');
                $('#report_columnValue').removeClass('display');
            }
        });

        $('body').on('click', '.reportPlotBandOK', function () {
            var rowIndex = $(this).attr('row-index');
            var name = $('#report_plotName').val();
            var fromType = $('[name="report_chartPlotBand_from"]:checked').val();
            var from = $('#report_plotFromValue').val();
            var toType = $('[name="report_chartPlotBand_to"]:checked').val();
            var to = $('#report_plotToValue').val();
            var color = $('#report_plotColor').val();
            var showSymbol = $('#report_plotSymbol').prop('checked');

            if (name == '') {
                showNotification({
                    message: "Please fill-up Name",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return;

            }

            if (from == '') {
                showNotification({
                    message: "Please fill-up From Value",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return;

            }

            if (to == '') {
                showNotification({
                    message: "Please fill-up To Value",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return;

            }


            if (color == "") {
                color = "#ECC";
            }
            if (rowIndex) {
                var tr = $('#report_chartPlotBands tr').eq(rowIndex);

                console.log($(tr).find('td').eq(0));
                $(tr).attr('show-symbol', showSymbol);
                $(tr).find('td').eq(0).html(name);
                $(tr).find('td').eq(1).html(fromType);
                $(tr).find('td').eq(2).html(from);
                $(tr).find('td').eq(3).html(toType);
                $(tr).find('td').eq(4).html(to);
                $(tr).find('td').eq(5).css('background-color', color);
            } else {
                $('#report_chartPlotBands').append('<tr class="tr_plot_bands" show-symbol="' + showSymbol + '"><td class="fl-table-ellip">' + name + '</td><td class="fl-table-ellip">' + fromType + '</td><td class="fl-table-ellip">' + from + '</td><td class="fl-table-ellip">' + toType + '</td><td class="fl-table-ellip">' + to + '</td><td style="background-color:' + color + '"></td><td style="text-align:center"><div class="icon-trash fa fa-trash-o cursor deletePlotBands" style="margin-left: auto;margin-right: auto" data-type="deactivate" data-user-id="1"></div></td></tr>');
            }

            $('#popup_cancel').click();
        });
        $('body').on('click', '.reportColumnOK', function () {
            var columnName = $('#report_columnName').val();
            var columnValueType = $('[name="report_defaultValueType"]:checked').val();
            var columnIndex = $(this).attr('column-index');
            var columnSort = $('[name="report_column_sort"]:checked').val();
            var columnGroup = $('[name="report_column_grouping"]:checked').val();
            var columnWidth = $('#report_columnWidth').val();
            var columnType = $('#report_columnType').val();
            var columnShowTotal = $('#column_show_total').prop('checked');
            var columnShowAverage = $('#column_show_average').prop('checked');

            if (columnValueType == 'field') {
                var columnValue = $('#report_field_column_value').val();
            } else {
                var columnValue = $('#report_columnValue').val();
            }

            if ($.trim(columnName) == "") {
                showNotification({
                    message: "Please fill-up Name",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return;
            }

            if ($.trim(columnValue) == "") {
                showNotification({
                    message: "Please fill-up Value",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return;
            }

            if ($.trim(columnWidth) == "") {
                showNotification({
                    message: "Please fill-up Width.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return;
            }


            var currentWidth = 0;
            $('#report_columns').find('th').each(function () {

            });

            if (columnIndex) {
                var th = $('.sortableRow li').eq(columnIndex);

                console.log(th);
                $(th).html(columnName + '<div style="float:right"><i class="fa fa-times delete_column"></i></div>');
                $(th).attr('value-type', columnValueType);
                $(th).attr('data-value', columnValue);
                $(th).attr('sort', columnSort);
                $(th).attr('group', columnGroup);
                $(th).attr('format', columnType);
                $(th).attr('show-total', columnShowTotal);
                $(th).attr('show-average', columnShowAverage);
                $(th).css('width', columnWidth);
            } else {
                var addedTH = $('<li class="report_sorting_disabled" style="background: #EDEDED !important;display: inline-block;padding: 8px" value-type="' + columnValueType +
                        '" data-value="' + htmlEntities(columnValue) +
                        '" format="' + columnType +
                        '" show-total="' + columnShowTotal +
                        '" show-average="' + columnShowAverage +
                        '" sort="' + columnSort +
                        '" group="' + columnGroup + '">' +
                        columnName +
                        '<div style="float:right"><i class="fa fa-times delete_column"></i></div></li>');

                $('.sortableRow').append($(addedTH));
                $(addedTH).css('width', columnWidth);
//                var newWidth = parseFloat($(addedTH).parent().width()) + parseFloat(columnWidth);
//                $(addedTH).css('width', columnWidth);
//                $(addedTH).parent().css('width', newWidth);
            }

            self.setTableWidth();
            self.setHeaderResizable();

            $('#popup_cancel').click();

        });

        $('body').on('click', '.delete_column', function () {
            var self = this;
            jConfirm('Are you sure you want to delete this column?', '', '', '', '', function (ans) {
                if (ans) {
                    $(self).closest('li').remove();
                }
            });


        });

        $('body').on('click', '.deletePlotBands', function () {
            $(this).closest('tr').remove();
        });

        $('body').on('dblclick', '.report_sorting_disabled', function () {
            self.setColumnProperties($(this));
        });

        $('body').on('dblclick', '.tr_plot_bands', function () {
            self.setChartPlotBand($(this));
        });

        $('body').on('change', '#report_columnType', function () {
            console.log('selected format', $(this).val());

            if ($(this).val() == 'Text') {
                $('.addDetails').hide();
            } else {
                $('.addDetails').show();
            }
        });

        $('[data="data-parameter"] tbody').sortable({
            'handle': '.parameter_handle'
        });


        $('body').on('click', '.radio_fieldname', function () {
            var tr = $(this).closest('tr');
            var checked = $(tr).find('.radio_fieldname:checked').val();
            if (checked == 'computed') {
                $(tr).find('.fieldname_formula').removeClass('display');
                $(tr).find('[column="field"]').addClass('display');
                $(tr).find('.type_radio')[1].click();
            } else {
                $(tr).find('.fieldname_formula').addClass('display');
                $(tr).find('[column="field"]').removeClass('display');
                $(tr).find('.type_radio')[0].click();
            }
        });

        $('body').on('click', '.report_column_radio', function () {
            var radio_val = $(this).val();

        });

        $('body').on('click', '.type_radio', function () {
            var tr = $(this).closest('tr');
            var checked = $(tr).find('.type_radio:checked').val();
            if (checked == 'computed') {
                $(tr).find('.type_formula').removeClass('display');
                $(tr).find('.radio_fieldname')[1].click();
            } else {
                $(tr).find('.type_formula').addClass('display');
                $(tr).find('.radio_fieldname')[0].click();
            }
        });
    },
    setIDE: function (self) {
        $('#report_columnValue').attr('data-ide-properties-type', 'ide-report-columnValue');
        $('#report_columnValue').attr('readonly', 'readonly');
    },
    removeIDE: function (self) {
        $('#report_columnValue').removeAttr('data-ide-properties-type');
        $('#report_columnValue').removeAttr('readonly');
    },
    selectForm: function () {
        var form_id = $("#form_id").val();
        if (!form_id) {
            this.getForms(function (data) {
                var newDialog = new jDialog(data, "", "", "", "", function () {
                });
                newDialog.themeDialog("modal2");
                //select form
                $(".getFormForWorkflow").click(function () {
                    var value = $(".forms_selection").val();
                    if (value != 0) {
                        var user_view = $('#user_url_view').val();
                        var pathName = window.location.pathname;

                        if (pathName == user_view + 'report') {
                            window.location = user_view + "report?id=0&form_id=" + value;
                        } else {
                            window.location = "/report?id=0&form_id=" + value;
                        }

                        $("#popup_overlay,#popup_container").remove();
                    } else {
                        showNotification({
                            message: "Please choose a form to proceed.",
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                    }

                })
            })
        }
    },
    getForms: function (callback) {
        //var form_id = $(".form_chosen").val();
        $.post("/ajax/getFormProperties", {
            action: "getActiveForms"
        }, function (forms) {
            var forms_json = JSON.parse(forms);
            var ret = "";
            ret += '<div style="float:left;width:50%;">';
            ret += '<h3 class="fl-get-report pull-left fl-margin-bottom">';
            ret += '<i class="icon-asterisk"></i> ' + "Get Report";
            ret += '</h3>';
            ret += '</div>';
            //ret += '<div style="float:right;width:50%;text-align:right;margin-top:-5px;">';
            //    ret += '<input type="button" class="btn-basicBtn getFormForWorkflow" id="" value="Ok" node-data-id="" style="">';
            //ret += '</div>';
            ret += '<div class="hr"></div>';
            ret += '<div class="fields">';
            ret += '<div class="label_below2 fl-get-workspace-popup"> Please select a form: </div>';
            ret += '<div class="input_position">';
            ret += '<select class="form-select forms_selection fl-margin-bottom" style="margin-top:5px; margin-bottom:5px;">';
            ret += '<option value="0">----------------------------Select----------------------------</option>';
            for (var i in forms_json) {

                ret += '<option value="' + forms_json[i]['form_id'] + '">' + forms_json[i]['form_name'] + '</option> ';
            }
            ret += '</select>';
            ret += '</div>';
            ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="label_basic"></div>';
            ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
            ret += '<input type="button" class=" fl-positive-btn btn-blueBtn getFormForWorkflow fl-margin-right fl-margin-bottom" value="OK"> ';
            //ret += '<a href="/"><input type="button" class="btn-basicBtn  fl-margin-right fl-margin-bottom" id="workflow-cancel" value="Cancel"></a>';
            ret += '</div>';
            ret += '</div>';
            callback(ret);
        })
    },
    appendParameter: function () {
        $('body').on('click', '[action="append_parameter"]', function () {
            parameters.append();
        });
    },
    deleteParameter: function () {
        $('body').on('click', '.deleteParameter', function () {
            $(this).trigger('mouseout');
            parameters.remove(this);


        });
    },
    selectAllParameters: function () {
        $('body').on('click', '[action="selectall_parameters"]', function () {
            $('[data-type="field-parameter"]').each(function () {
                $(this).attr('checked', true)
            });
        });
    },
    deselectAllParameters: function () {
        $('body').on('click', '[action="deselectall_parameters"]', function () {
            $('[data-type="field-parameter"]').each(function () {
                $(this).attr('checked', false)
            });
        });
    },
    selectAllColumns: function () {
        $('body').on('click', '[action="selectall_columns"]', function () {
            $('[data-type="field-column"]').each(function () {
                $(this).attr('checked', true)
            });
        });
    },
    deselectAllColumns: function () {
        $('body').on('click', '[action="deselectall_columns"]', function () {
            $('[data-type="field-column"]').each(function () {
                $(this).attr('checked', false)
            });
        });
    },
    sortColumns: function () {
        $('.sortableRow').sortable();
    },
    save: function () {
        $('body').on('click', '.save_form_workspace', function () {
            var form_id = $("#form_id").val();
            var title = $('#workspace_title').val();
            var description = $('#workspace_description').val();

            if ($.trim(title) == "") {
                showNotification({
                    message: "Please indicate report title",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return;
            }

            var parameters = new Array();
            var columns = new Array();
            var plotBands = [];
            var yAxisMax = $('#yAxisMax').val();
            var yAxisMin = $('#yAxisMin').val();
            var yAxisInterval = $('#yAxisInterval').val();
            var report_id = $('#report_id').val();

            //            $('[data-type="field-parameter"]:checked').each(function(){
            //                parameters.push($(this).val());
            //            });

            var paramsContinue = true;
            var labelDuplicate = false;
            var computedNull = false;
            var labels = [];
            var paramsMessage = "";
            var paramsField = [];

            $('[data-type="tr_parameters"]').each(function () {

                var column = $(this).find('[column="label"]')[0].value;

                var radioField = $(this).find('.radio_fieldname:checked').val();
                var type_radio = $(this).find('.type_radio:checked').val();
                var type_formula = $(this).find('.type_formula').val();

                if (radioField == 'computed') {
                    var fieldFormula = $(this).find('.fieldname_formula').val();
                    if ($.trim(fieldFormula) === "" || $.trim(type_formula) === "") {
                        computedNull = true;
                        paramsField.push('Computed Fields');
                    }

                } else {
                    var field = $(this).find('[column="field"]')[0].value;
                }

                var operator = $(this).find('[column="operator"]')[0].value;

                if ($.trim(column) == '') {
                    paramsContinue = false;
                    paramsField.push('Label');

                }
                paramsMessage = "Please fill-up Parameters:" + paramsField.join(',');
                if (labels.indexOf(column) > -1) {
                    labelDuplicate = true;
                    paramsMessage = "Duplicate Parameter Label found.";
                }

                var row_parameter = {
                    Column: column,
                    Field: field,
                    FieldFormula: fieldFormula,
                    RadioField: radioField,
                    Operator: operator,
                    Type: type_radio,
                    TypeFormula: type_formula
                };

                labels.push(column);
                parameters.push(row_parameter);
            });
            if (computedNull == true || paramsContinue == false || labelDuplicate == true) {
                showNotification({
                    message: paramsMessage,
                    type: "error",
                    autoClose: true,
                    duration: 5
                });
                $('.fl-closeDialog').click();
                return;
            }

//            console.log('paramsContinue', paramsContinue);
//            console.log('parameters', parameters);
//
//            return;
//
//            $('[data-type="field-column"]:checked').each(function() {
//                columns.push($(this).val());
//            });

            $('.report_sorting_disabled').each(function () {
                columns.push({
                    Name: $(this).text(),
                    ValueType: $(this).attr('value-type'),
                    Value: $(this).attr('data-value'),
                    Width: $(this).css('width'),
                    Sort: $(this).attr('sort'),
                    Group: $(this).attr('group'),
                    Format: $(this).attr('format'),
                    ShowTotal: $(this).attr('show-total'),
                    ShowAverage: $(this).attr('show-average')
                });
            });


            //get plot bands
            $('.tr_plot_bands').each(function () {
                plotBands.push({
                    Name: $(this).find('td').eq(0).html(),
                    FromType: $(this).find('td').eq(1).html(),
                    From: $(this).find('td').eq(2).html(),
                    ToType: $(this).find('td').eq(3).html(),
                    To: $(this).find('td').eq(4).html(),
                    Color: $(this).find('td').eq(5).css('background-color'),
                    ShowSymbol: $(this).attr('show-symbol')
                });
            });
            var json = $("body").data();
            var report_users_checked = json['report-users_checked'];
            var report_users_unChecked = json['report-users_unChecked'];


            console.log(report_users_checked)
            console.log(report_users_unChecked)

            //symbols
            var symbol = {
                ValueType: $('[name="chart_symbol"]:checked').val(),
                Value: $('#symbol_value').val()
            }

            if (parameters.length == 0) {
                showNotification({
                    message: "Please add parameter.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return;
            }
            if (columns.length == 0) {
                showNotification({
                    message: "Please add column.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                $('.fl-closeDialog').click();
                return;
            }
            ui.block();
            $.post('/ajax/report',
                    {
                        report_id: report_id,
                        form_id: form_id,
                        title: title,
                        description: description,
                        parameters: JSON.stringify(parameters),
                        columns: JSON.stringify(columns),
                        plotBands: JSON.stringify(plotBands),
                        symbol: JSON.stringify(symbol),
                        yAxisMax: yAxisMax,
                        yAxisMin: yAxisMin,
                        yAxisInterval: yAxisInterval,
                        report_users_checked: JSON.stringify(report_users_checked), // insert
                        report_users_unChecked: JSON.stringify(report_users_unChecked), // delete

                    }, function (data) {
                var result = JSON.parse(data);

                reportId = result['id'];

                ui.unblock();
                showNotification({
                    message: result["message"],
                    type: result["mode"],
                    autoClose: true,
                    duration: 3
                });

                if (result['mode'] == 'error') {
                    return;
                }

                $("#popup_overlay,#popup_container").remove();

                var t = setTimeout(function () {
                    window.location.href = 'report?id=' + reportId + '&form_id=' + form_id;
                }, 3000);

            });
        });
    },
    setTableWidth: function () {
        var width = 500;
        $('#report_columns li').each(function () {
            width += $(this).width();
        });

        $('#report_columns').css('width', width);
    },
    setHeaderResizable: function () {
        $('.report_sorting_disabled').css({
            "min-width": "100px"
        });

        $('.report_sorting_disabled').resizable({
            ghost: true,
            handles: "e",
            stop: function (event, ui) {
                var width = parseFloat(ui.size.width) - parseFloat(ui.originalSize.width);

                var parentUL = $('#report_columns');
                var tableWidth = $(parentUL).css('width');
                var newTableWidth = parseFloat(tableWidth) + width;

                $(parentUL).css({
                    width: newTableWidth,
                    margin: 0
                });

            }
        });
    },
    getDetails: function () {
        var parameters = JSON.parse($.trim($('#parameter-data').text()));
        var columns = JSON.parse($.trim($('#column-data').text()));

        $('[data="data-parameter"] tbody').sortable({
            'handle': '.parameter_handle'
        });

        for (var index in columns) {
            $('.sortableRow').append('<li class="report_sorting_disabled"  style="background: #EDEDED !important;width:' + columns[index].Width + ';display:inline-block;padding:8px" value-type="' + columns[index].ValueType
                    + '" data-value="' + htmlEntities(columns[index].Value) +
                    '" sort="' + columns[index].Sort +
                    '" group="' + columns[index].Group +
                    '" format = "' + columns[index].Format +
                    '" show-total = "' + columns[index].ShowTotal +
                    '" show-average = "' + columns[index].ShowAverage + '">' +
                    columns[index].Name +
                    '<div style="float:right"><i class="fa fa-times delete_column"></i></div></li>');
        }

        if ($.trim($('#plot-bands-data').text()) != '') {
            console.log($('#plot-bands-data').text())
            var plotBands = JSON.parse($('#plot-bands-data').text());

            for (var index in plotBands) {
                if (plotBands[index].ShowSymbol) {
                    $('#report_chartPlotBands').append('<tr class="tr_plot_bands" show-symbol = "' + plotBands[index].ShowSymbol + '"><td class="fl-table-ellip">' + plotBands[index].Name + '</td><td class="fl-table-ellip">' + plotBands[index].FromType + '</td><td class="fl-table-ellip">' + plotBands[index].From + '</td><td class="fl-table-ellip">' + plotBands[index].ToType + '</td><td class="fl-table-ellip">' + plotBands[index].To + '</td><td style="background-color:' + plotBands[index].Color + '"></td><td  style="text-align:center"><div class="icon-trash fa fa-trash-o cursor deletePlotBands" style="margin-left: auto;margin-right: auto" data-type="deactivate" data-user-id="1"></div></td></tr>');
                } else {
                    $('#report_chartPlotBands').append('<tr class="tr_plot_bands"><td class="fl-table-ellip">' + plotBands[index].Name + '</td><td class="fl-table-ellip">' + plotBands[index].FromType + '</td><td class="fl-table-ellip">' + plotBands[index].From + '</td><td class="fl-table-ellip">' + plotBands[index].ToType + '</td><td class="fl-table-ellip">' + plotBands[index].To + '</td><td style="background-color:' + plotBands[index].Color + '"></td><td style="text-align:center"><div class="icon-trash fa fa-trash-o cursor deletePlotBands" style="margin-left: auto;margin-right: auto" data-type="deactivate" data-user-id="1"></div></td></tr>');
                }

            }
        }

        this.setTableWidth();
        this.setHeaderResizable();

        var json_data = $("body").data();
        var reportJson = $(".getDetailFn").attr("json");
        if (reportJson) {
            reportJson = JSON.parse(reportJson);

            json_data['workspace_title'] = reportJson['Title'];
            json_data['workspace_description'] = reportJson['Description'];
        }

//  
//  
//        $('[data-type="field-column"]').each(function() {
//            var val = $(this).val();
//
//            if (columns.indexOf(val) > -1) {
//                $(this).attr('checked', true);
//            } else {
//                $(this).attr('checked', false);
//            }
//        });


    },
    setColumnProperties: function (obj) {
        var ret = "";
        var active_fields = $('#active_fields').val();
        var reserved_fiels = ['Tracking Number', 'Requestor', 'Processor', 'Status'];
        var arr = active_fields.split(',');
        arr = arr.concat(reserved_fiels);
        arr = arr.sort();

        ret += '<div style="float:left;width:50%;">';
        ret += '<h3 class="pull-left fl-margin-bottom">';
        ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-report-addcolumn"></use></svg> ' + "<span>Column Properties</span>";
        ret += '</h3>';
        ret += '</div>';
        ret += '<div class="hr"></div>';

        ret += '<div class="fields section clearing fl-field-style">';
        ret += '<div class="input_position column div_1_of_1">';
        ret += '<span class="font-bold"> Name: </span>';
        // ret += '<label><input class="report_column_radio css-checkbox" value="static" type="radio" checked="checked" name="report_column_radio" id="report_column_radio_id_static" /><label for="report_column_radio_id_static" class="css-label"></label>Static </label>';
        // ret += '<label><input class="report_column_radio css-checkbox" value="computed" type="radio" name="report_column_radio" id="report_column_radio_id_computed" /><label for="report_column_radio_id_computed" class="css-label"></label>Computed </label>';
        ret += '<input type="text" name="report_columnName" id="report_columnName" class="report_columnName form-text" value="">';
        // ret += '<textarea class="form-textarea obj_prop fl-remove-margin-top report_columnName" name="report_columnName" id="report_columnName" data-ide-properties-type="ide-report-columnName-computed"  style="resize:vertical; margin-bottom:10px;"></textarea>';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="fields section clearing fl-field-style">';
        ret += '<div class="input_position column div_1_of_1">';
        ret += '<span class="font-bold"> Width: </span>';
        ret += '<input type="text" name="report_columnWidth" id="report_columnWidth" class="report_columnWidth form-text" value="100px">';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="fields section clearing fl-field-style">';
        ret += '<div class="input_position column div_1_of_1">';
        ret += '<span class="font-bold"> Type: </span>';
        ret += '<select name="report_columnType" id="report_columnType" data-type="computed" class="form-select obj_prop" data-properties-type="lblFieldType" data-object-id="28"><option data-input-type="Currency" value="Currency">Currency</option><option data-input-type="Number" value="Number">Number</option><option data-input-type="Text" selected="selected" value="Text">Text</option></select>';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="fields formulaContainer section clearing fl-field-style">'
        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span >Value:</span>';
        ret += '<label style="margin-bottom:10px; display:block; float:left;" class="fl-margin-right"><input id="value_static" data-properties-type="report_defaultValueType" class="formulaType css-checkbox" value="static" type="radio" checked="checked" name="report_defaultValueType" id="value_static"><label for="value_static" class="css-label"></label> Static </label> ';
        ret += '<label style="margin-bottom:10px;"><input id="value_computed" data-properties-type="report_defaultValueType" class="formulaType css-checkbox" value="computed" type="radio" name="report_defaultValueType" id="value_computed"><label for="value_computed" class="css-label"></label> Computed </label>';
        ret += '<label style="margin-bottom:10px;"><input id="value_field" data-properties-type="report_defaultValueType" class="formulaType css-checkbox" value="field" type="radio" name="report_defaultValueType" id="value_field"><label for="value_field" class="css-label"></label> Field </label>';

        ret += '<select class="form-select display" id="report_field_column_value">';
        for (var field in arr) {
            ret += '<option value="' + arr[field] + '">' + arr[field] + '</option>';
            console.log('field', arr[field]);
        }
        ret += '</select>';
        ret += '<textarea data-type="textbox" class="form-textarea report_columnValue report-field-formula wfSettings" id="report_columnValue" data-properties-type="defaultValue" data-object-id="1" style="resize:vertical; margin:0px;"></textarea>';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="fields section clearing fl-field-style">';

        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Sort:</span><Br/>';
        ret += '<label class="fl-margin-right"><input id="column_sort_none" data-properties-type="report_defaultValueType" class="css-checkbox" value="none" type="radio" checked="checked" name="report_column_sort"><label for="column_sort_none"class="css-label"></label> None </label>';
        ret += '<label class="fl-margin-right"><input id="column_sort_ascending" data-properties-type="report_defaultValueType" class="css-checkbox" value="ascending" type="radio" name="report_column_sort"><label for="column_sort_ascending"class="css-label"></label> Ascending </label>';
        ret += '<label><input id="column_sort_descending" data-properties-type="report_defaultValueType" class="css-checkbox" value="descending" type="radio" name="report_column_sort"><label for="column_sort_descending"class="css-label"></label> Descending </label>';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="fields groupings section clearing fl-field-style">';
        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">Grouping:</span><br/>';
        ret += '<label class="fl-margin-right"><input id="column_group_none" data-properties-type="report_defaultValueType" class="css-checkbox" value="none" type="radio" checked="checked" name="report_column_grouping"><label for="column_group_none" class="css-label"></label> None</label>';
        ret += '<label class="fl-margin-right"><input id="column_group_categorized" data-properties-type="report_defaultValueType" class="css-checkbox" value="categorized" type="radio" name="report_column_grouping"><label for="column_group_categorized" class="css-label"></label> Categorized </label>';
        ret += '</div>';
        ret += '</div>';

        ret += '</div>';
        ret += '<div class="fields addDetails">';
        ret += '<div class="label_below2">Additional Details:</div>';
        ret += '<div class="input_position_below">';
        ret += '<label><input id="column_show_total" data-properties-type="report_defaultValueType" class="" type="checkbox" name="column_show_total">Show Total</label>';
        ret += '<label><input id="column_show_average" data-properties-type="report_defaultValueType" class="" type="checkbox" name="column_show_average">Show Average</label>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += '<input type="button" class="btn-blueBtn reportColumnOK fl-margin-right" value="OK"> ';
        ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
        ret += '</div>';
        ret += '</div>';

        var newDialog = new jDialog(ret, "", "", "", "80", function () {
        });
        newDialog.themeDialog("modal2");


        if (obj) {
            var columnName = $(obj).text();
            var columnValueType = $(obj).attr('value-type');
            var columnValue = $(obj).attr('data-value');
            var columnSort = $(obj).attr('sort');
            var columnGroup = $(obj).attr('group');
            var columnWidth = $(obj).css('width');
            var columnType = $(obj).attr('format');
            var columnShowTotal = $(obj).attr('show-total');
            var columnShowAverage = $(obj).attr('show-average');

            $('#report_columnName').val(columnName);

            if (columnValueType == 'field') {
                $('#report_field_column_value').removeClass('display');
                $('#report_columnValue').addClass('display');
                $('#value_field').prop('checked', true);
                $('#report_field_column_value').val(columnValue);
                this.removeIDE();
            } else if (columnValueType == 'static') {
                $('#value_static').prop('checked', true);
                this.removeIDE();
            } else {
                $('#value_computed').prop('checked', true);
                this.setIDE();
                // $(".report-field-formula").mention_post({
                //     json: formulaJson, // from local file fomula.json
                //     highlightClass: "",
                //     prefix: "",
                //     countDisplayChoice: "4"
                // });
            }

            switch (columnSort) {
                case "ascending":
                    $('#column_sort_ascending').prop('checked', true);
                    break;
                case "descending":
                    $('#column_sort_descending').prop('checked', true);
                    break;
                default :
                    $('#column_sort_none').prop('checked', true);
                    break;
            }

            switch (columnGroup) {
                case "categorized":
                    $('#column_group_categorized').prop('checked', true);
                    break;
                default:
                    $('#column_group_none').prop('checked', true);
                    break;
            }

            $('#report_columnValue').val($('<textarea class="clean-column-data isDisplayNone"/>').html(columnValue).text());
            $('#report_columnWidth').val(columnWidth);
            $('#report_columnType').val(columnType);


            if (columnShowTotal == 'true') {
                $('#column_show_total').prop('checked', columnShowTotal);
            }


            if (columnShowAverage == 'true') {
                $('#column_show_average').prop('checked', columnShowAverage);
            }



            var index = $('.sortableRow li').index(obj);
            $('.reportColumnOK').attr('column-index', index);

            if (index > 0) {
                $(obj).attr('group', '');
                $('.groupings').hide();
            }

            if (columnType == 'Text') {
                $('.addDetails').hide();
            }

        } else {
            var headerCount = $('.sortableRow th').length;
            if (headerCount > 0) {
                $('.groupings').hide();
            }

            $('.addDetails').hide();

        }
    },
    setChartPlotBand: function (obj) {

        var ret = "";
        ret += '<div style="float:left;width:50%;">';
        ret += '<h3 class="pull-left fl-margin-bottom">';
        ret += '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-report-addplotband"></use></svg> ' + "<span>Plot Band</span>";
        ret += '</h3>';
        ret += '</div>';
        ret += '<div class="hr"></div>';
        ret += '<div class="fields section clearing fl-field-style">';

        ret += '<div class="input_position column div_1_of_1">';
        ret += '<span class="font-bold"> Name: </span>';
        ret += '<input type="text" name="report_plotName" id="report_plotName" class="report_plotName form-text" value="">';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="fields section clearing fl-field-style">';
        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">From:</span><br/>';
        ret += '<label class="fl-margin-right"><input id="plotFrom_static" data-properties-type="report_chartPlotBand_from" class="css-checkbox" value="static" type="radio" checked="checked" name="report_chartPlotBand_from"><label for="plotFrom_static" class="css-label"></label> Static </label>';
        ret += '<label class="fl-margin-right"><input id="plotFrom_computed" data-properties-type="report_chartPlotBand_from" class="css-checkbox" value="computed" type="radio" name="report_chartPlotBand_from"><label for="plotFrom_computed" class="css-label"></label> Computed </label>';
        ret += '<textarea data-type="textbox" class="form-textarea report_plotFromValue report-field-formula" id="report_plotFromValue" data-ide-properties-type="ide-report-plotFromValue" data-properties-type="defaultValue" data-object-id="1" style="resize:vertical; margin:0px;"></textarea>';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="fields section clearing fl-field-style">';
        ret += '<div class="input_position_below column div_1_of_1">';
        ret += '<span class="font-bold">To:</span><br/>';
        ret += '<label class="fl-margin-right"><input id="plotTo_static" data-properties-type="report_chartPlotBand_to" class="css-checkbox" value="static" type="radio" checked="checked" name="report_chartPlotBand_to"><label for="plotTo_static" class="css-label"></label> Static </label>';
        ret += '<label class="fl-margin-right"><input id="plotTo_computed" data-properties-type="report_chartPlotBand_to" class="css-checkbox" value="computed" type="radio" name="report_chartPlotBand_to"><label for="plotTo_computed" class="css-label"></label> Computed </label>';
        ret += '<textarea data-type="textbox" class="form-textarea report_plotToValue report-field-formula" id="report_plotToValue" data-ide-properties-type="ide-report-plotToValue" data-properties-type="defaultValue" data-object-id="1" style="resize:vertical; margin:0px;"></textarea>';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="fields section clearing fl-field-style">';
        ret += '<div class="column div_1_of_1">';
        ret += '<label><span class="font-bold"><input type="checkbox" id="report_plotSymbol" class="css-checkbox"><label for="report_plotSymbol" class="css-label"></label>Show Symbol</span></label>';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="section clearing fl-field-style ">';
        ret += '<div class="input_position_below column div_1_of_1 plotband-color">';
        ret += '<span class="font-bold">Color:</span><Br/>';
        ret += '<input type="text" name="report_plotColor" id="report_plotColor" class="report_plotColor form-text" style="margin-top:5px;" value="">';
        ret += '</div>';
        ret += '</div>';

        ret += '<div class="fields">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += '<input type="button" class="btn-blueBtn reportPlotBandOK fl-margin-right fl-positive-btn" value="OK"> ';
        ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
        ret += '</div>';
        ret += '</div>';



        var newDialog = new jDialog(ret, "", "", "", "60", function () {
        });
        newDialog.themeDialog("modal2");
        // $(".report-field-formula").mention_post({
        //     json: formulaJson, // from local file fomula.json
        //     highlightClass: "",
        //     prefix: "",
        //     countDisplayChoice: "4"
        // });
        var json_spectrum_settings = {
            color: "#ECC",
            showInput: true,
            className: "full-spectrum",
            showInitial: true,
            showPalette: true,
            showSelectionPalette: true,
            maxPaletteSize: 10,
            preferredFormat: "rgb",
            showAlpha: true,
            //localStorageKey: "spectrum.demo",
            move: function (color) {

            },
            show: function () {

            },
            beforeShow: function () {

            },
            hide: function () {

            },
            change: function () {

            },
            palette: [
                ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
                ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
            ]
        }




        $('.report_plotColor').spectrum(json_spectrum_settings)

        if (obj) {

            console.log($(obj).find('td'))
            var name = $(obj).find('td')[0].textContent;
            var fromType = $(obj).find('td')[1].textContent;
            var from = $(obj).find('td')[2].textContent;
            var toType = $(obj).find('td')[3].textContent;
            var to = $(obj).find('td')[4].textContent;
            var color = $(obj).find('td').eq(5).css('background-color');
            var showSymbol = $(obj).attr('show-symbol');

            console.log('showSymbol', showSymbol);
            console.log(name)
            $('#report_plotName').val(name);
            $('#report_plotFromValue').val(from);
            $('#report_plotToValue').val(to);
            $('#report_plotToValue').val(to);
            $('#report_plotColor').val(color);
            $('.report_plotColor').spectrum("set", color);

            if (showSymbol == 'true') {
                $('#report_plotSymbol').prop('checked', true);
            } else {
                $('#report_plotSymbol').prop('checked', false);
            }


            if (fromType == 'static') {
                $('#plotFrom_static').prop('checked', true);
            } else {
                $('#plotFrom_computed').prop('checked', true);
            }
            if (toType == 'static') {
                $('#plotTo_static').prop('checked', true);
            } else {
                $('#plotTo_computed').prop('checked', true);
            }

            var index = $('#report_chartPlotBands tr').index(obj);



            $('.reportPlotBandOK').attr('row-index', index);
        }
    },
    setUsers: function () {
        var json = $("body").data();
        var users = $(".form-user-accessor").text();
        var reportUsers = $(".form-report-users").text();


        try {
            if (users) {
                json['form-users'] = JSON.parse(users);
                console.log(json)
            }
            if (reportUsers) {
                json['report-users'] = JSON.parse(reportUsers);
            }
        } catch (err) {
            console.log(err)
        }
    }
};

var parameters = {
    append: function (column, field, operator) {
        var active_fields = $('#active_fields').val();
        var reserved_fiels = ['Tracking Number', 'Requestor', 'Processor', 'Status'];
        var rowCount = $('[data="data-parameter"]').find('tr').length;

        var arr = active_fields.split(',');
        arr = arr.concat(reserved_fiels);
        arr = arr.sort();

        var ret = '<tr data-type="tr_parameters"><td style="vertical-align: top;"><input column="label" type="text" class="form-text"/></td>';
        ret += '<td style="vertical-align: top;"><label><input class="css-checkbox radio_fieldname" value="static" type="radio" checked="checked" name="radio_fieldname_' + rowCount + '" id="fieldname_static_' + rowCount + '" /><label for="fieldname_static_' + rowCount + '" class="css-label"></label>Static </label>';
        ret += '<label><input class="css-checkbox radio_fieldname" value="computed" type="radio" name="radio_fieldname_' + rowCount + '" id="fieldname_computed_' + rowCount + '" /><label for="fieldname_computed_' + rowCount + '" class="css-label"></label>Computed </label><select column="field" class="form-select">';

        for (var index = 0; index <= arr.length - 1; index++) {
            ret += '<option value="' + arr[index] + '">' + arr[index] + '</option>';
        }
        ret += '</select><textarea class="fieldname_formula form-textarea obj_prop fl-remove-margin-top display" id="fieldname_formula_' + rowCount + '" data-ide-properties-type="ide-report-fieldname"  style="resize:vertical; margin-bottom:10px;"></textarea></td>';
        ret += '<td style="vertical-align: top;"><select column="operator" class="form-select">';
        ret += '<option value="=">Equal (==)</option>';
        ret += '<option value="<=">Less than equal (<=)</option>';
        ret += '<option value=">=">Greater than equal (>=)</option>';
        ret += '<option value=">">Greater than(>)</option>';
        ret += '<option value="<">Less than (<)</option>';
        ret += '<option value="!=">Not equal(!=)</option>';
        ret += '<option value="%">Contains(%)</option>';
        ret += '<option value="!%">Not Contains(!%)</option>';
        ret += '</select></td>';
        ret += '<td style="vertical-align: top;">';
        ret += '<label><input class="type_radio css-checkbox" value="freetext" type="radio" checked="checked" name="type_radio_' + rowCount + '" id="type_freetext_' + rowCount + '" /><label for="type_freetext_' + rowCount + '" class="css-label"></label>Free Text </label>';
        ret += '<label><input class="type_radio css-checkbox" value="computed" type="radio" name="type_radio_' + rowCount + '" id="type_computed_' + rowCount + '" /><label for="type_computed_' + rowCount + '" class="css-label"></label>Computed </label>';
        ret += '<textarea class="type_formula form-textarea obj_prop fl-remove-margin-top display" id="type_formula_' + rowCount + '" data-ide-properties-type="ide-report-fieldtype"  style="resize:vertical; margin-bottom:10px;"></textarea>';
        ret += '</td>';
        ret += '<td style="text-align: center;">';
        ret += '<div class="icon-trash fa fa-trash-o cursor deleteParameter" id="deleteUser_1" style="margin-left: auto;margin-right: auto" data-type="deactivate" data-user-id="1"></div>';
        ret += '<div class="icon-trash fa fa-arrows cursor parameter_handle" style="margin-left: 3px;margin-right: auto" data-original-title="Sort Parameters"></div>';
        ret += '</td>';

        $('[data="data-parameter"] tbody').eq(0).append(ret);
    },
    remove: function (self) {
        $(self).closest('tr').remove();
    }
}

function isNumber(e) {
    e = e || window.event;
    var charCode = e.which ? e.which : e.keyCode;
    return /\d/.test(String.fromCharCode(charCode));
}

$(document).ready(function () {
    report.init();
});


