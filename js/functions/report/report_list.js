/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var report_list = {
    init: function () {
        this.bind();
        this.changeExportType();
    },
    bind: function () {
        var self = this;
        $('body').on('click', '[link-type="report-generation"]', function () {
            var id = $(this).attr('data-report-id');

            ui.block();
            self.showDialog(id, function (data) {

                ui.unblock();
                var newDialog = new jDialog(data, "", '', "", "", function () {
                });
                newDialog.themeDialog("modal2");


            });
        });


        $('body').on('change', '#chart-type, #chart-type-option', function () {
            var thisVal = $('#chart-type option:selected').val();
            var chartTypeOption = $('#chart-type-option option:selected').val();

            if (chartTypeOption == 2) {
                $('.stat-y-axis, .stat-display').removeClass('display2');
            } else {
                $('.stat-y-axis, .stat-display').addClass('display2');
            }
        });

        $('body').on('change', '.stat-display', function () {
            var thisValue = $('.stat-display option:selected').val();
            if (thisValue == 1) {
                $('.stat-display-category').removeClass('display2');
            } else {
                $('.stat-display-category').addClass('display2');
            }
        });


        $('body').on('click', '[button-type="generate-report" ]', function () {
            var reportId = $(this).attr('report-id');
            var formId = $(this).attr('form-id');

            self.generateReport(reportId, formId);

        });
    },
    showDialog: function (id, callback) {

        $.post('/ajax/getReportProperties', {
            id: id
        }, function (data) {

            result = JSON.parse(data);
            var parameters = JSON.parse(result.parameters);
            if (result.form.active_fields == "null" || result.form.active_fields == null) {
                ui.unblock();
                showNotification({
                    message: "Do not have active fields",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
                return;
            }
            var active_fields = result.form.active_fields.split(',');

            var ret = "";
            ret += '<div style="float:left;width:50%;">';
            ret += '<h3 class="pull-left fl-margin-bottom">';
            ret += '<i class="icon-asterisk"></i> ' + result.title;
            ret += '</h3>';
            ret += '</div>';
            ret += '<div class="hr"></div>';

            console.log('parameters', parameters);
            for (var index in parameters) {

                var operator = '';
                switch (parameters[index].Operator) {
                    case '<=':
                        operator = 'Less than equal (<=)';
                        break;
                    case '>=':
                        operator = 'Greater than equal (>=)';
                        break;
                    case '>':
                        operator = 'Greater than(>)';
                        break;
                    case '<':
                        operator = 'Less than (<)';
                        break;
                    case '!=':
                        operator = 'Not equal(!=)';
                        break;
                    case '%':
                        operator = 'Contains(%)';
                        break;
                    case '!%':
                        operator = 'Not Contains(!%)';
                        break;
                    default:
                        operator = 'Equal (==)';
                        break;
                }
                
                ret += '<div class="fields">';
                ret += '<div class="label_below2"> ' + parameters[index].Column + ': ' + operator + ' </div>';
                if (parameters[index].Type == 'computed') {
                    var fieldName = '';
                    var operator = parameters[index].Operator;
                    var typeFormula = parameters[index].TypeFormula;

                    if (parameters[index].RadioField == 'computed') {
                        fieldName = parameters[index].FieldFormula;
                    } else {
                        fieldName = parameters[index].Field;
                    }

                    var placeholder = fieldName + ' ' + operator + ' ' + typeFormula;
                    ret += '<div class="input_position">' + placeholder + '</div>';
                } else {
                    var fieldName = '';
                    if (parameters[index].RadioField == 'computed') {
                        fieldName = parameters[index].FieldFormula;
                    } else {
                        fieldName = parameters[index].Field;
                    }
                    ret += '<div class="input_position"><input type="text" data-field="' + fieldName + '" data-type="parameters" id="txtsearch" class="form-text" placeholder="' + fieldName + '" /></div>';
                }

                ret += '</div>';
            }

            ret += '<div class="fields">';
            ret += '<div class="label_below2"> Export Option: </div>';
            ret += '<div class="input_position"><select class="form-select" id="export-option"><option value="1">PDF</option><option value="2">Excel</option><option value="3">Chart</option><option value="4">CSV</option></select></div>';
            ret += '</div>';

            ret += '<div class="fields chart-type display2">';
            ret += '<div class="label_below2"> Chart Type: </div>';
            ret += '<div class="input_position"><select class="form-select" id="chart-type"><option value="bar">Bar</option><option value="column">Column</option><option value="area">Area</option><option value="line">Line</option></select></div>';
            ret += '</div>';
            ret += '</div>';


            ret += '<div class="fields chart-report-type display2">';
            ret += '<div class="label_below2"> Data: </div>';
            ret += '<div class="input_position">';
            ret += '<select class="form-select" id="chart-type-option"><option value="1">Master File</option><option value="2">Statistics</option></select>';
            ret += '</div>';
            ret += '</div>';

            ret += '<div class="fields versus display2">';
            ret += '<div class="label_below2"> Fields: </div>';
            ret += '<div class="input_position"><div style="height:100px; overflow:auto">';
            ret += '<input type="checkbox" class="selectall" name="selectall" style="margin-top:5px;margin-bottom:5px"> Select All</input><br/>';
            ret += '<input type="checkbox" value="Status" name="chartField"> Status</input><br/>';
            ret += '<input type="checkbox" value="Requestor" name="chartField"> Requestor</input><br/>';
            ret += '<input type="checkbox" value="Processor" name="chartField"> Processor</input><br/>';
            for (var index = 0; index <= active_fields.length - 1; index++) {
                ret += '<input type="checkbox" value="' + active_fields[index] + '" name="chartField"> ' + active_fields[index] + '</input><br/>';
            }
            ret += '</div></div>';
            ret += '</div>';

            ret += '<div class="fields stat-series display2">';
            ret += '<div class="label_below2"> Series: </div>';
            ret += '<div class="input_position">';
            ret += '<select id="chart-stat-series"  class="form-select" style="margin-top:5px;margin-bottom:5px">';
            ret += '<option value="Status">Status</option>';
            ret += '<option value="Requestor">Requestor</option>';
            ret += '<option value="Processor">Processor</option>';
            for (var index = 0; index <= active_fields.length - 1; index++) {
                ret += '<option value="' + active_fields[index] + '">' + active_fields[index] + '</option>';
            }
            ret += '</select>';
            ret += '</div>';
            ret += '</div>';

            ret += '<div class="fields stat-versus display2">';
            ret += '<div class="label_below2"> Versus: </div>';
            ret += '<div class="input_position">';
            ret += '<select id="chart-stat-versus"  class="form-select" style="margin-top:5px;margin-bottom:5px">';
            ret += '<option value="Status">Status</option>';
            ret += '<option value="Requestor">Requestor</option>';
            ret += '<option value="Processor">Processor</option>';
            for (var index = 0; index <= active_fields.length - 1; index++) {
                ret += '<option value="' + active_fields[index] + '">' + active_fields[index] + '</option>';
            }
            ret += '</select>';
            ret += '</div>';
            ret += '</div>';

            ret += '<div class="fields stat-y-axis display2">';
            ret += '<div class="label_below2"> Y-Axis: </div>';
            ret += '<div class="input_position">';
            ret += '<select id="chart-stat-y-axis"  class="form-select" style="margin-top:5px;margin-bottom:5px">';
            ret += '<option value="0">Number of Records</option>';
            ret += '<option value="1">Series</option>';
            ret += '</select>';
            ret += '</div>';
            ret += '</div>';

            ret += '<div class="fields stat-display display2">';
            ret += '<div class="label_below2"> Display: </div>';
            ret += '<div class="input_position">';
            ret += '<select id="chart-stat-display"  class="form-select" style="margin-top:5px;margin-bottom:5px">';
            ret += '<option value="0">Single</option>';
            ret += '<option value="1">Multiple</option>';
            ret += '</select>';
            ret += '</div>';
            ret += '</div>';

            ret += '<div class="fields stat-display-category display2">';
            ret += '<div class="label_below2"> Category: </div>';
            ret += '<div class="input_position">';
            ret += '<select id="chart-stat-display-category"  class="form-select" style="margin-top:5px;margin-bottom:5px">';

            for (var index in parameters) {
                ret += '<option value="' + parameters[index].Field + '">' + parameters[index].Column + '</option>';
            }
            ret += '</select>';
            ret += '</div>';
            ret += '</div>';

//            ret += '<div class="fields stat-y-axis display2">';
//            ret += '<div class="label_basic"> Y-Axis Plot Band Max: </div>';
//            ret += '<div class="input_position">';
//            ret += '<select id="chart-stat-y-axis"  class="form-select" style="margin-top:5px;margin-bottom:5px">';
//            ret += '<option value="0">Number of Records</option>';
//            ret += '<option value="1">Series</option>';
//            ret += '</select>';
//            ret += '</div>';
//            ret += '</div>';
//
//            ret += '<div class="fields stat-y-axis display2">';
//            ret += '<div class="label_basic"> Y-Axis Plot Band Min: </div>';
//            ret += '<div class="input_position">';
//            ret += '<select id="chart-stat-y-axis"  class="form-select" style="margin-top:5px;margin-bottom:5px">';
//            ret += '<option value="0">Number of Records</option>';
//            ret += '<option value="1">Series</option>';
//            ret += '</select>';
//            ret += '</div>';
//            ret += '</div>';

            ret += '<div class="fields">';
            ret += '<div class="label_below2"></div>';
            ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
            ret += '<input type="button" class="fl-margin-right fl-margin-top fl-margin-bottom btn-blueBtn getFormForWorkflow" report-id="' + result.id + '" form-id="' + result.form_id + '" button-type="generate-report" value="Generate Report" style="width:113px;"> ';
            ret += '<input type="button" class="fl-margin-right fl-margin-top fl-margin-bottom btn-basicBtn" id="popup_cancel" value="Cancel">';
            ret += '</div>';
            ret += '</div>';

            callback(ret);
        });
    },
    generateReport: function (reportId, formId) {
        var keyword = [];
        var type = $('#export-option').val();
        var chartDataType = $('#chart-type-option').val();
        var chartDisplay = $('#chart-stat-display option:selected').val();
        var chartDisplayCategory = $('#chart-stat-display-category option:selected').val();

//        alert(chartDisplay);
//        alert(chartDisplayCategory);

        var chartFields = [];
        $('[data-type="parameters"]').each(function () {
            keyword.push($(this).attr('data-field') + '[]=' + $(this).val());
        });

        $('[name="chartField"]:checked').each(function () {
            chartFields.push($(this).attr('value'));
        });


        var chartType = "";
        var chartData = ''
        var chartField = '';
        var seriesParams = '';
        var versusParams = '';
        var yAxisPararms = '';
        var series = '';
        var versus = '';
        var yAxis = '';
        var windowLocation = '';



        if (type == 3) {
            chartType = "&chartType=" + $("#chart-type").val();
            chartData = "&chartDataType=" + chartDataType;

            if (chartDataType == '1') {
                chartField = "&chartFields=" + chartFields.join();
            } else {
                series = $('#chart-stat-series').val();
                versus = $('#chart-stat-versus').val();
                yAxis = $('#chart-stat-y-axis').val();
                //

                seriesParams = "&series=" + series;
                versusParams = "&versus=" + versus;
                yAxisPararms = "&yaxis=" + yAxis;
            }
        }

        var yaxisSettings = '&yaxisMax=10&yaxisMin=0&yaxisInterval=1';
        var category = '';
        if (chartDisplay == 1) {
            category = '&category=' + chartDisplayCategory;
            windowLocation = '/ajax/generate_report_tile?id=' + reportId + '&form_id=' + formId + '&type=' + type + '&' + keyword.join('&') + chartType + chartData + chartField + seriesParams + versusParams + yAxisPararms + yaxisSettings + category;
        } else {
            windowLocation = '/ajax/generateReport?id=' + reportId + '&form_id=' + formId + '&type=' + type + '&' + keyword.join('&') + chartType + chartData + chartField + seriesParams + versusParams + yAxisPararms + yaxisSettings + category;
        }


        // ui.block();
        //window.location = windowLocation;
        window.open(windowLocation, '_blank')

    },
    changeExportType: function () {
        $("body").on("change", "#export-option", function () {

            if ($(this).val() == "3") {
                //   $(".versus").removeClass("display2");
                $(".chart-report-type").removeClass("display2");
                $(".chart-type").removeClass("display2");
            } else {
                //   $(".versus").addClass("display2");
                $(".chart-report-type").addClass("display2");
                $(".chart-type").addClass("display2");
                $("#chart-type").val("bar")
            }

            if ($('#chart-type-option').val() == '1') {
                $(".versus").removeClass("display2");
                $(".stat-series ").addClass("display2");
                $(".stat-versus ").addClass("display2");
            } else {
                $(".versus").addClass("display2");
                $(".stat-series ").removeClass("display2");
                $(".stat-versus ").removeClass("display2");
                $('.stat-display').removeClass('display2');

                var statDisplay = $('.stat-display option:selected').val();
                if (statDisplay == 1) {
                    $('.stat-display-category').removeClass('display2');
                }
            }

            if ($(this).val() != "3") {
                $(".versus").addClass("display2");
                $(".stat-series ").addClass("display2");
                $(".stat-versus ").addClass("display2");
                $('.stat-y-axis').addClass('display2');
                $('.stat-display').addClass('display2');
                $('.stat-display-category').addClass('display2');
            }
            ;


            $('#content-dialog-scroll').perfectScrollbar('update');
        });

        $('body').on('click', '.selectall', function () {
            if ($(this).attr('checked')) {
                $('[name="chartField"]').prop('checked', true);
            } else {
                $('[name="chartField"]').prop('checked', false);
            }
        });

        $('body').on('change', '#chart-type-option', function () {
            var value = $(this).val();
            if (value == '1') {
                $(".versus").removeClass("display2");
                $(".stat-series ").addClass("display2");
                $(".stat-versus ").addClass("display2");
            } else {
                $(".versus").addClass("display2");
                $(".stat-series ").removeClass("display2");
                $(".stat-versus ").removeClass("display2");
            }
            $('#content-dialog-scroll').perfectScrollbar('update')
        });
    }

}

$(document).ready(function () {
    report_list.init();
    user_report.loadView();


    $('body').on('click', '[target-click="generate_payslip"]', function () {
        $.get('/ajax/createpayslip/', null, function (data) {
            console.log('payslip', data);
        });
    });
});


var user_report = {
    loadView: function () {
        $('#report_table').dataTable({
            "sPaginationType": "full_numbers",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"  style="margin-top:50px;"> ' +
                        '<div class="bar1"></div> ' +
                        '<div class="bar2"></div> ' +
                        '<div class="bar3"></div> ' +
                        '<div class="bar4"></div> ' +
                        '<div class="bar5"></div> ' +
                        '<div class="bar6"></div> ' +
                        '<div class="bar7"></div> ' +
                        '<div class="bar8"></div> ' +
                        '<div class="bar9"></div> ' +
                        '<div class="bar10"></div> ' +
                        '</div>'
            },
            "bProcessing": true,
            "sAjaxSource": '/ajax/getReports'
        });
    }
}