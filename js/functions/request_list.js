var jsonRequestData = {
    form_id: "1",
    search_value: "",
    field: "0",
    start: 0,
    limit: 10,
    date_field: "",
    date_from: "",
    date_to: "",
    "column-sort": "",
    "column-sort-type": "",
    "multi_search": []
}

var columnSort = [];
var dashboardItemContainer = ".fl-table-wrapper";
$(document).ready(function () {
    datatableHoverColor(".recordsDatatable tbody tr");
    ImportRequest.generateImportDialog();
    //request
    //default load
    $(".dataTable").each(function () {

        //restoring default value
        jsonRequestData['column-sort'] = '';
        jsonRequestData['column-sort-type'] = '';

        // for default sort
        var fieldDefaultSort = $(this).closest(dashboardItemContainer).find(".form-header-fieldDefaultSort").text();
        var fieldDefaultSortType = $(this).closest(dashboardItemContainer).find(".form-header-fieldDefaultSortType").text();

        //validation if field in default sort is not available
        var fieldFlagExist = 0;
        var formActiveFields = $(this).closest(dashboardItemContainer).find(".searchRequestUsersField").find("option");


        var defaultFields = ['TrackNo', 'Status', 'Requestor'];
        for (var i in formActiveFields) {
            if (formActiveFields[i]['value'] == fieldDefaultSort) {
                fieldFlagExist++;
                break;
            } else if (formActiveFields[i]['value'].indexOf(defaultFields)) { //
                fieldFlagExist++;
                break;
            }
        }
        if (fieldDefaultSort != "" && fieldFlagExist > 0) {
            jsonRequestData['column-sort'] = fieldDefaultSort;
            jsonRequestData['column-sort-type'] = fieldDefaultSortType;
            columnSort.push("fieldDefaultSort", fieldDefaultSort)
        }
        /* Added by Japhet Morada */
        /*===============================================================*/
        var related_info_seach = $.trim($('.get-returned-values').val());
        if (related_info_seach != "") {
            related_info_seach = JSON.parse(related_info_seach);
            var multi_search = [];
            var operator = "";
            for (var i in related_info_seach) {
                operator = "%";
                if (related_info_seach[i]['operator'] != undefined || related_info_seach[i]['operator'] != "undefined" || related_info_seach[i]['operator'] != "") {
                    operator = related_info_seach[i]['operator'];
                }
                multi_search.push({
                    "search_value": related_info_seach[i]['value'],
                    "search_field": related_info_seach[i]['field_name'],
                    "search_operator": operator,
                    "search_condition": related_info_seach[i]['condition'],
                });
                console.log("multi_search", multi_search);
            }
            // multi_search.push({
            //     "search_value":"japhet0006",
            //     "search_field":'TrackNo',
            //     "search_operator":'%'
            // });

            jsonRequestData['multi_search'] = JSON.stringify(multi_search);
        }
        /*===============================================================*/
        GetRequestsDataTable.defaultData(jsonRequestData, this);
    })
    //end default load

    $("body").on("click", ".recordsDatatable tbody tr td:not(td:first-child)", function (e) {
        var href = $(this).closest("tr").find(".linkTrigger").attr("href");
        if (e.ctrlKey) {
            window.open(href,'_blank').opener = null;
        } else {
            window.location = href;
        }
    });

    //sort
    $("body").on("click", ".recordsDatatable th", function () {
        var flSortdiv = $('<div class="flSortdiv"></div>');
        //console.log(flSortdiv);
        var cursor = $(this).css("cursor");
        jsonRequestData['column-sort'] = $(this).attr("field_name");
        if ($(this).attr("field_name") == "" || $(this).attr("field_name") == undefined || cursor == "col-resize") {
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest(dashboardItemContainer).find(".sortable-image").html("");
        if (indexcolumnSort == -1) {
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonRequestData['column-sort-type'] = "ASC";
        } else {
            columnSort.splice(indexcolumnSort, 1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonRequestData['column-sort-type'] = "DESC";
        }

        var tableID = $(this).closest(dashboardItemContainer).find(".dataTable").attr("id");
        $(this).closest(dashboardItemContainer).find(".dataTable").dataTable().fnDestroy();
        var self = this;
        GetRequestsDataTable.defaultData(jsonRequestData, this, function () {
            addIndexClassOnSort(self);
        });
    })

    //search
    $(".searchRequestUsersValue").keyup(function (e) {

        if (e.keyCode == "13") {
            jsonRequestData['search_value'] = $(this).val();
            jsonRequestData['multi_search'] = "";
            // jsonRequestData['field'] = $(this).closest(dashboardItemContainer).find(".searchRequestUsersField").val()
            jsonRequestData['field'] = "0";
            var tableID = $(this).closest(dashboardItemContainer).find(".dataTable").attr("id");
            $(this).closest(dashboardItemContainer).find(".dataTable").dataTable().fnDestroy();
            GetRequestsDataTable.defaultData(jsonRequestData, this);
            // $(this).closest(dashboardItemContainer).append(datatableClone)

            RequestFilter2.showFilterConfirmation(this);
        }
    });
    $(".searchRequestUsersButton").click(function () {
        jsonRequestData['search_value'] = $(this).closest(dashboardItemContainer).find(".searchRequestUsersValue").val();
        jsonRequestData['multi_search'] = "";
        // jsonRequestData['field'] = $(this).closest(dashboardItemContainer).find(".searchRequestUsersField").val()
        jsonRequestData['field'] = "0";
        $(this).closest(dashboardItemContainer).find(".dataTable").dataTable().fnDestroy();
        GetRequestsDataTable.defaultData(jsonRequestData, this);
        RequestFilter2.showFilterConfirmation(this);
    })
    $(".filterRequestsUser").change(function () {
        if ($(this).val() == 0) {

            $(this).closest(dashboardItemContainer).find(".cancelFC").trigger("click");
        } else {
            var limit = parseInt($(this).find("option:selected").attr("limit") == "0" ? "10" : $(this).find("option:selected").attr("limit"));
            jsonRequestData['search_value'] = $(this).find("option:selected").attr("search_value");
            jsonRequestData['field'] = $(this).find("option:selected").attr("search_field");
            jsonRequestData['date_field'] = $(this).find("option:selected").attr("date_field");
            jsonRequestData['date_from'] = $(this).find("option:selected").attr("date_from");
            jsonRequestData['date_to'] = $(this).find("option:selected").attr("date_to");
            jsonRequestData['multi_search'] = $(this).find("option:selected").attr("multi_search");
            jsonRequestData['isAdvanceSearch'] = "1";
            jsonRequestData['limit'] = limit;
            // $(this).closest(dashboardItemContainer).find(".dataTable").dataTable().fnDestroy();
            // GetRequestsDataTable.defaultData(jsonRequestData, this);
            var oTable = $(this).closest(dashboardItemContainer).find(".dataTable").dataTable();
            var oSettings = oTable.fnSettings();
            oSettings._iDisplayStart = 0;
            oSettings._iDisplayLength = limit;
            oTable.fnDraw();
            $(this).closest(dashboardItemContainer).find(".cancelFC").fadeIn();
        }
        $(this).closest(dashboardItemContainer).find(".saveFC").fadeOut();
        jsonRequestData['isAdvanceSearch'] = "0";
    })
    $(".requestFilterSettings").click(function () {
        RequestFilter2.settingsModal(this);
    })
    $("body").on("click", ".deleteFilter", function () {
        var id = $(this).attr("data-id");
        RequestFilter2.deleteFilter(id, this);
    })

    //ADVANCE FILTER
    AdvanceFilter2.addFilter();
    AdvanceFilter2.deleteFilter();
    AdvanceFilter2.updatedFieldOperator();

    $("body").on("click", ".advanceFilterDialog", function () {
        AdvanceFilter2.showDialog(this);
    })
    $("body").on("click", "#advanceFilter", function () {
        AdvanceFilter2.filter(this);
    })
    $("body").on("click", "#clearValue", function () {
        AdvanceFilter2.clearFilterFields();
    })
    $("body").on("click", ".cancelFC", function () {
        jsonRequestData['search_value'] = "";
        jsonRequestData['field'] = 0;
        jsonRequestData['date_field'] = "";
        jsonRequestData['date_from'] = "";
        jsonRequestData['date_to'] = "";
        jsonRequestData['multi_search'] = "";
        jsonRequestData['limit'] = "10";


        var related_info_seach = $.trim($('.get-returned-values').val());
        $(this).closest(dashboardItemContainer).find(".dataTable").dataTable().fnDestroy();
        GetRequestsDataTable.defaultData(jsonRequestData, this);
        // $(this).closest(dashboardItemContainer).find(".searchRequestLimitPerPage").val("10");
        RequestFilter2.hideFilterConfirmation(this);
        $(this).closest(dashboardItemContainer).find(".searchRequestUsersValue").val("");
    })
    $(".saveFC").click(function () {
        RequestFilter2.setConfirmation(this);
    })
    $("body").on("click", "#saveRF", function () {
        RequestFilter2.saveRF(this);
    })


    //show entries
    $(".searchRequestLimitPerPage").change(function (e) {
        // if (e.keyCode == "13") {
        var val = parseInt($(this).val());
        jsonRequestData['limit'] = val;
        $(this).closest(dashboardItemContainer).find(".dataTable").dataTable().fnDestroy();
        // var self = this;
        var oTable = GetRequestsDataTable.defaultData(jsonRequestData, this, function (data) {

        });


        RequestFilter2.showFilterConfirmation(this);
        // }
    })
    //end request


    //starred
    //hover in and hover out on starred
    $("body").on("mouseenter", ".starred", function () {
        Starred.hoverIn(this);
    });
    $("body").on("mouseleave", ".starred", function () {
        Starred.hoverOut(this);
    });
    $("body").on("click", ".starred", function () {
        var id = this.id;
        Starred.updateData(this, id);
        var objStarred_user_widget = {"type": "2"};
        Starred.load(0, "start", objStarred_user_widget, function (result) {
            Starred.loadAfterAjaxStarredWidget(result);
        });
    })

    //set buttons

    $("body").on("click", ".appCheckAll", function () {
        var formName = $(this).attr("data-type-form-name");
        var current_user_id = $("#current-user-id").html();
        var req_form_id = $(this).parents("[form_id]").attr("form_id");

        if ($(this).is(':checked')) {
            var parse_button = {};
            var workflow_id = "";
            var form_name = "";
            var buttons = "";
            $("." + formName).each(function () {

                buttons = $(this).attr("button-action-data");
                //console.log(buttons)
                var processor_id = $(this).attr("button-processor-id");
                var requestor_id = $(this).attr("button-requestor-id");

                workflow_id = $(this).attr("button-workflow-id");
                form_class = $(this).attr("class");
                form_name = $(this).attr("data-form-name");
                if (buttons != "") {

                    //parse_button = JSON.parse(buttons);
                    //$.extend(parse_button, JSON.parse(buttons));
                    parse_button = buttons;
                    $(this).attr("checked", true);
                }

            });

            // Buttons
            var btns = "";
            btns += '<option value="0">Button Selection</option>';
            var parse_button = JSON.parse(parse_button);
            for (button_request in parse_button) {

                //console.log(parse_button[button_request])
                //var form_id = $("#form_id").val();
                var btn_name = parse_button[button_request].button_name;
                var btn_action = parse_button[button_request].child_id;

                btns += '<option class="' + form_class + '" data-form-name="' + form_name + '" value="' + btn_name + '" form-id="' + req_form_id + '" data-workflow-id="' + workflow_id + '" action="' + btn_action + '">' + btn_name + '</option>';
            }
            //console.log(req_form_id)
            $("#request_buttons_" + req_form_id).html(btns);
        } else {
            $("." + formName).each(function () {
                $(this).attr("checked", false);
            });
            $("#request_buttons_" + req_form_id).html('<option value="0">Button Selection</option>');
        }
    });


    $("body").on("change", ".request_buttons", function () {

        var req_form_id = $('[fl-data-type="request"]').attr('form_id');
        var self = $("#request_buttons_" + req_form_id + " option:selected");
        var current_user_id = $("#current-user-id").html();
        var form_name = self.attr("data-form-name");
        var form_id = self.attr("form-id");


        var node_id = self.attr("action");
        var val = self.val();
        if (val == "0") {
            return false;
        }

        var form_request = [];
        var form_requestTrackNo = [];
        var btn_val = self.val();
        var self = this
        var validation_btn = "";
        var calculate_request_process = 0;
        $("." + form_name).each(function (id, val) {
            if ($(this).is(':checked')) {
                parseInt(calculate_request_process++) - 1;
            }
        });
        var calc = parseInt(calculate_request_process) - 1;
        if (calc == "1") {
            var text = "record is";
        } else {
            var text = "records are";
        }
        if (calc != "0") {
            var text_title = capitaliseFirstLetter(NumToWord(calc)) + text + ' selected to process. Are you sure you want to proceed?';
        } else {
            var text_title = 'Are you sure you want to proceed?';
        }

        if (node_id == "delete") {
            var text_title = "WARNING: Are you sure you want to delete this record?";
            var deletion_type = $(self).closest(".fl-table-wrapper").find(".deletion-type").val();
            if (deletion_type == "2") {
                text_title = text_title + " This action cannot be undone.";
            }
        }

        var newConfirm = new jConfirm(text_title, '', '', '', '', function (r) {//here
            if (r == true) {
                $("." + form_name).each(function (id, val) {
                    var saveOption = false;
                    if ($(this).is(':checked')) {

                        console.log('action', node_id);

                        try {
                            var buttons_data = JSON.parse($(this).attr('button-action-data'));
                            for (var buttonIndex in buttons_data) {
                                if (buttons_data[buttonIndex]["child_id"] == node_id) {
                                    saveOption = true;
                                }
                            }
                        } catch (e) {

                        }
                        var request_details = {};
                        var trackNo_list = {};
                        var request_id = $(this).attr('button-request-id');
                        var requestor_id = $(this).attr('button-requestor-id');
                        trackNo_list['trackNo'] = $(this).attr("id");
                        var formId = form_id;
                        var workflowId = $(this).attr('button-workflow-id');
                        var nodeId = node_id;

                        ui.block();

                        if (saveOption) {
                            request_details['FormID'] = formId;
                            request_details['ID'] = request_id;
                            request_details['Node_ID'] = nodeId;
                            request_details['Workflow_ID'] = workflowId;
                            request_details['Mode'] = "viewApproval";
                            request_details['Requestor_ID'] = requestor_id;

                            form_request.push(request_details);
                            form_requestTrackNo.push(trackNo_list);
                        }

                    }

                });
                console.log('form_request', form_request);
//                return;
                if (node_id == "delete") { // for delete
                    var records = [];
                    $(self).closest(".fl-table-wrapper").find(".recordsDatatable .appCheckSingle:checked").each(function () {
                        records.push($(this).attr("button-request-id"))
                    })
                    TrashBin.deleteManyData(form_id, records, function (data) {
                        $(self).closest(".fl-table-wrapper").find(".searchRequestUsersButton").trigger("click");
                        if (data != 0) {
                            showNotification({
                                message: "Record has been successfully deleted.",
                                type: "success",
                                autoClose: true,
                                duration: 3
                            })
                        }
                        ui.unblock();
                        // window.location = "/user_view/application?id="+data['form_id'];
                    });
                } else {
                    $.post('/ajax/requestBulk/', {
                        data: JSON.stringify(form_request)
                    }, function (data) {
                        console.log(data)
                        var dashboardItemContainer = ".fl-table-wrapper";
                        var tableID = $(self).closest(dashboardItemContainer).find(".dataTable").attr("id");
                        $(self).closest(dashboardItemContainer).find(".dataTable").dataTable().fnDestroy();
                        GetRequestsDataTable.defaultData(jsonRequestData, self);

                        var btns = '<option value="0">Button Selection</option>';
                        $(".checkAllAction").attr("checked", false);
                        $("#request_buttons_" + req_form_id).html(btns);
                        var trackNo_record = "";
                        for (var a = 0; a < form_requestTrackNo.length; a++) {
                            if (form_requestTrackNo[a].trackNo != undefined) {
                                trackNo_record += form_requestTrackNo[a].trackNo + ", ";
                            }
                        }
                        var track_No = trackNo_record.substring(0, trackNo_record.length - 2);
                        var set_btn = "";
                        if (btn_val == "Cancel") {
                            set_btn = "cancelled";
                        } else if (btn_val == "Approve") {
                            set_btn = "approved";
                        } else {
                            set_btn = btn_val;
                        }
                        var trackNo_process = "Document(s) with Tracking No. " + track_No + " has been successfully processed.";

                        showNotification({
                            message: trackNo_process,
                            type: "sucess",
                            autoClose: true,
                            duration: 3
                        });

                        $("#popup_cancel").click();
                        ui.unblock();
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    });
                }
            } else {
                $("#request_buttons_" + req_form_id + " option[value=0]").attr("selected", "selected");
            }
        });
        newConfirm.themeConfirm("confirm2", {'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});

    });

    // Check All Application
    $("body").on("click", ".appCheckSingle", function () {
        var dialogContainer = $(this).closest(".fl-table-wrapper");
        var obj = ".fl-table-actions .appCheckSingle";
        var checkAll = ".appCheckAll";
        var totalCheckBox = dialogContainer.find(obj + ":not([disabled])").length;
        var totalCheckBoxChecked = dialogContainer.find(obj + ":checked:not([disabled])").length;
        // console.log(dialogContainer.find(obj))
        console.log(totalCheckBox + " " + totalCheckBoxChecked);
        // console.log(dialogContainer.find(obj))
        if (totalCheckBox == totalCheckBoxChecked) {
            dialogContainer.find(checkAll).attr("checked", true);
        } else {
            dialogContainer.find(checkAll).attr("checked", false);
        }


        var formName = $(this).attr("data-form-name");
        var status = $(this).attr("form-status");
        var req_form_id = $(this).parents("[form_id]").attr("form_id");
        //alert(req_form_id)
        if (status == "disabled") {
            showNotification({
                message: "No Actions On this request.",
                type: "error",
                autoClose: true,
                duration: 3
            });
        } else {
            if ($(this).is(':checked')) {
                var parse_button = {};
                var workflow_id = "";
                var form_name = "";

                var buttons = $(this).attr("button-action-data");
                workflow_id = $(this).attr("button-workflow-id");
                form_class = $(this).attr("class");
                form_name = $(this).attr("data-form-name");

                if (buttons != "") {

                    //parse_button = JSON.parse(buttons);
                    //$.extend(parse_button, JSON.parse(buttons));
                    parse_button = buttons;
                    $(this).attr("checked", true);
                }


                // Buttons
                var btns = "";
                btns += '<option value="0">Button Selection</option>';
                var parse_button = JSON.parse(parse_button);
                for (button_request in parse_button) {
                    //console.log(parse_button[button_request])
                    var form_id = $("#form_id").val();
                    var btn_name = parse_button[button_request].button_name;
                    var btn_action = parse_button[button_request].child_id;
                    btns += '<option class="' + form_class + '" data-form-name="' + form_name + '" value="' + btn_name + '" form-id="' + req_form_id + '" data-workflow-id="' + workflow_id + '" action="' + btn_action + '">' + btn_name + '</option>';
                }
                $("#request_buttons_" + req_form_id).html(btns);
            } else {
                if ($(".appCheckSingle:checked").length == 0) {
                    $(this).attr("checked", false);

                    $("#request_buttons_" + req_form_id).html('<option value="0">Button Selection</option>');
                    $(".appCheckAll").attr("checked", false);
                } else {
                    var parse_button = {};
                    var workflow_id = "";
                    var form_name = "";

                    var buttons = $(this).attr("button-action-data");
                    workflow_id = $(this).attr("button-workflow-id");
                    form_class = $(this).attr("class");
                    form_name = $(this).attr("data-form-name");
                    if (buttons != "") {

                        //parse_button = JSON.parse(buttons);
                        //$.extend(parse_button, JSON.parse(buttons));
                        parse_button = buttons;
                    }


                    // Buttons
                    var btns = "";
                    btns += '<option value="0">Button Selection</option>';
                    var parse_button = JSON.parse(parse_button);
                    for (button_request in parse_button) {
                        //console.log(parse_button[button_request])
                        var form_id = $("#form_id").val();
                        var btn_name = parse_button[button_request].button_name;
                        var btn_action = parse_button[button_request].child_id;
                        btns += '<option class="' + form_class + '" data-form-name="' + form_name + '" value="' + btn_name + '" form-id="' + req_form_id + '" data-workflow-id="' + workflow_id + '" action="' + btn_action + '">' + btn_name + '</option>';
                    }
                    $("#request_buttons_" + req_form_id).html(btns);
                }
            }
        }

    });



    $('body').on('click', '.tour_button_refresh_view', function () {
        jsonRequestData['search_value'] = $(".searchRequestUsersValue").val();
        jsonRequestData['multi_search'] = "";
        // jsonRequestData['field'] = $(this).closest(dashboardItemContainer).find(".searchRequestUsersField").val()
        jsonRequestData['field'] = "0";
        $(this).closest(dashboardItemContainer).find(".dataTable").dataTable().fnDestroy();
        GetRequestsDataTable.defaultData(jsonRequestData, this);
        // $(this).closest(dashboardItemContainer).append(datatableClone)
    });


});

GetRequestsDataTable = {
    "defaultData": function (jsonRequestData, obj, callback) {
        if ($(obj).closest(dashboardItemContainer).find(".switchingAppView[default-view='1']").length > 0) {
            AppCalendarView.setCalendarDefaultView($(obj).closest(dashboardItemContainer));
        }
        /* Added by Japhet Morada */
        /*===============================================================*/
        var related_info_seach = $.trim($('.get-returned-values').val());
        if (related_info_seach != "") {
            related_info_seach = JSON.parse(related_info_seach);
            var multi_search = [];
            var operator = "";
            console.log(related_info_seach[i])

            for (var i in related_info_seach) {
                operator = "%";
                if (related_info_seach[i]['operator'] != undefined || related_info_seach[i]['operator'] != "undefined" || related_info_seach[i]['operator'] != "") {
                    operator = related_info_seach[i]['operator'];
                }
                multi_search.push({
                    "search_value": $.trim(related_info_seach[i]['value']),
                    "search_field": related_info_seach[i]['field_name'],
                    "search_operator": operator,
                    "search_condition": related_info_seach[i]['condition']
                });
            }
            // multi_search.push({
            //     "search_value":"japhet0006",
            //     "search_field":'TrackNo',
            //     "search_operator":'%'
            // });

            jsonRequestData['multi_search'] = JSON.stringify(multi_search);
            jsonRequestData['isAdvanceSearch'] = "1";
        }

        var form_header = $(obj).closest(dashboardItemContainer).find(".form-header").text();
        var form_id = $(obj).closest(dashboardItemContainer).find(".form_id").val();
        var allowReorder = $(obj).closest(dashboardItemContainer).find(".form-header-allowReorder").text();
        jsonRequestData['form_id'] = form_id;
        this.removeDataTableAttr(obj);

        // for allowing reorder
        var oColReorder = {
            "iFixedColumns": 1,
            "fnReorderCallback": function () {
                // alert('Columns reordered');
                var customView = [];
                var field_name = "";
                var field_label = "";
                var field_width = "";
                $(obj).closest(dashboardItemContainer).find(".dataTable th").each(function () {
                    field_label = $(this).text();
                    field_name = $(this).attr("field_name");
                    field_width = $(this).css("width");
                    if (field_label != "") {
                        customView.push({
                            "field_name": field_name,
                            "field_label": field_label,
                            "width": field_width
                        });
                    }
                });
                customView = JSON.stringify(customView);
                ui.block();
                $.post("/ajax/search", {action: "saveCustomView", form_id: form_id, customView: customView}, function (data) {
                    ui.unblock();
                })
            },
            "fnResizeCallback": function () {
                // alert("Callback Resize");
                var customView = [];
                var field_name = "";
                var field_label = "";
                var field_width = "";
                $(obj).closest(dashboardItemContainer).find(".dataTable th").each(function () {
                    field_label = $(this).text();
                    field_name = $(this).attr("field_name");
                    field_width = $(this).css("width");
                    if (field_label.trim() != "") {
                        customView.push({
                            "field_name": field_name,
                            "field_label": field_label,
                            "width": field_width
                        });
                    }
                });
                customView = JSON.stringify(customView);
                ui.block();
                $.post("/ajax/search", {action: "saveCustomView", form_id: form_id, customView: customView}, function (data) {
                    ui.unblock();
                })
            }
        };
        if (allowReorder != "") {
            if (allowReorder == "0") {
                oColReorder = {
                    allowReorder: false,
                    allowResize: true,
                    "fnResizeCallback": function () {
                        // alert("Callback Resize");
                        var customView = [];
                        var field_name = "";
                        var field_label = "";
                        var field_width = "";
                        $(obj).closest(dashboardItemContainer).find(".dataTable th").each(function () {
                            field_label = $(this).text();
                            field_name = $(this).attr("field_name");
                            field_width = $(this).css("width");
                            if (field_label.trim() != "") {
                                customView.push({
                                    "field_name": field_name,
                                    "field_label": field_label,
                                    "width": field_width
                                });
                            }
                        });
                        customView = JSON.stringify(customView);
                        ui.block();
                        $.post("/ajax/search", {action: "saveCustomView", form_id: form_id, customView: customView}, function (data) {
                            ui.unblock();
                        })
                    }
                };
            }
        }
        ;

        var oTable = $(obj).closest(dashboardItemContainer).find(".dataTable").dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            // "aoColumns": [
            //     {"mData": "TrackNo"},
            //     {
            //         "mData": function(source, type, val) {
            //             return source['first_name'] + " " + source['last_name'];
            //         }
            //     },
            //     {
            //         "mData": function(source, type, val) {
            //             var result = "";
            //             if (form_header && typeof form_header == "string") {
            //                 form_header = JSON.parse(form_header);
            //             }
            //             if (form_header == null || form_header == "") {
            //                 result = '<i ></i> <strong style="font-size: 10px;color: rgb(241, 79, 79);">No Header Information available</strong> ';
            //             } else {
            //                 var fieldValue = '';
            //                 for (var a in form_header) {
            //                     field_name = form_header[a]['field_name'];

            //                     if (field_name.indexOf('[]') >= 0) {
            //                         field_name = field_name.split('[]').join('');
            //                         console.log(field_name);

            //                         if (source['' + field_name + ''] != '') {
            //                             fieldValue = source['' + field_name + ''].split('|^|').join(',');
            //                         } else {
            //                             fieldValue = '';
            //                         }

            //                     } else {
            //                         fieldValue = source['' + field_name + ''];
            //                     }
            //                     result += '<i class="fa fa-circle" style="font-size:7px"></i> <strong class="detailed_info" style="font-size: 10px;color: #6D6868;">' + fieldValue + '</strong> ';
            //                 }
            //             }
            //             return result;
            //         }
            //     },
            //     {"mData": "form_status"},
            //     {"mData": "DateCreated"},
            //     {
            //         "mData": function(data, type, val) {
            //             var result = "";
            //             var starred_class = "fa fa-star-o";
            //             var starred = 0;
            //             if (data[0]['starred'] > 0) {
            //                 starred_class = "fa fa-star";
            //                 starred = data[0]['starred'];
            //             }
            //             result +=
            //                     '<a href="/user_view/workspace?view_type=request&formID=' + form_id + '&requestID=' + data['request_id'] + '&trackNo=' + data['TrackNo'] + '"><button class="tip fa fa-eye btn-basicBtn fl-basicsecondary-btn padding_5 cursor" style="margin-right:3px" form-id = "' + form_id + '" request-id="' + data['request_id'] + '" data-original-title="View Form"></button></a>' +
            //                     '<button class="tip ' + starred_class + ' btn-basicBtn padding_5 cursor fl-basicsecondary-btn starred" style="margin-right:3px" data-id="' + data['request_id'] + '" id="starred_' + data['request_id'] + '" data-starred="' + starred + '" data-original-title="Starred Request"></button>' +
            //                     '<button class="fa fa-comment btn-basicBtn padding_5 fl-basicsecondary-btn cursor showReply tip" data-type="1" data-f-id="' + form_id + '" data-id="' + data['request_id'] + '" data-original-title="Add/View Comments" ></button> ' +
            //                     '<button class="tip fa fa-book btn-basicBtn padding_5 fl-basicsecondary-btn cursor viewAuditLogs" style="margin-right:3px" data-request-id="' + data['request_id'] + '" data-form-id="' + form_id + '" data-original-title="Audit Logs"></button>';
            //             if (data[2].buttons != '' && data[2].buttons != 'null' && data[2].buttons != null) {
            //                 result += "<button class='tip fa fa-gear btn-basicBtn padding_5 fl-basicsecondary-btn cursor' button-action-data='" + data[2].buttons + "' button-request-id='" + data['ID'] + "'button-workflow-id='" + data['Workflow_ID'] + "'  button-requestor-id='" + data['Requestor'] + "' button-processor-id='" + data['Processor'] + "' button-type='action' data-original-title='Please select an Action'></button>";
            //             }
            //             return result;
            //         }
            //     },
            // ],
            "sAjaxSource": "/ajax/search",
            // "iDisplayLength": jsonRequestData['limit'],
            "fnServerData": function (sSource, aoData, fnCallback) {
                console.log("jsonRequestData", jsonRequestData);

                aoData.push({"name": "action", "value": "getRequestDataTable"});
                aoData.push({"name": "id", "value": form_id});
                aoData.push({"name": "search_value", "value": $.trim(jsonRequestData['search_value'])});
                aoData.push({"name": "field", "value": jsonRequestData['field']});
                aoData.push({"name": "multi_search", "value": jsonRequestData['multi_search']});
                aoData.push({"name": "start", "value": jsonRequestData['start']});
                aoData.push({"name": "limit", "value": jsonRequestData['limit']});
                aoData.push({"name": "date_field", "value": jsonRequestData['date_field']});
                aoData.push({"name": "date_from", "value": jsonRequestData['date_from']});
                aoData.push({"name": "date_to", "value": jsonRequestData['date_to']});
                aoData.push({"name": "column-sort", "value": jsonRequestData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonRequestData['column-sort-type']});
                aoData.push({"name": "isAdvanceSearch", "value": jsonRequestData['isAdvanceSearch']});

                ui.blockPortion($(obj).closest(dashboardItemContainer).find(".fl-main-app-view"))
                $.ajax({
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        try {
                            data = JSON.parse(data);
                            fnCallback(data);
                        } catch (err) {
                            //for debugger
                            console.log(data);
                            console.log("Error", err);
                        }
                    }
                });
            },
            "fnInitComplete": function (oSettings, json) {
                $('[button-type="action"]').each(function () {
                    var actions = JSON.parse($(this).attr('button-action-data'));
                    var request_id = $(this).attr('button-request-id');
                    var workflow_id = $(this).attr('button-workflow-id');
                    var requestor_id = $(this).attr('button-requestor-id');
                    var processor_id = $(this).attr('button-processor-id');


                    var html_action = '<ul style="padding:3px;">';
                    for (var index in actions) {
                        html_action += '<a style="padding:3px;" href="#" class="requestlink" link-type="request-actions" request-id="' + request_id + '"form-id="' + form_id + '" workflow-id="' + workflow_id + '" node-id="' + actions[index].child_id + '"><li>' + actions[index].button_name + '</li></a>';
                    }
                    html_action += '</ul>';

                    $(this).popover({
                        placement: 'bottom',
                        title: 'Please select Action',
                        html: true,
                        content: html_action
                    });

                });
                $(obj).closest(dashboardItemContainer).find(".dataTable").find(".sorting_disabled").first().css({
                    cursor: "auto"
                })
                // $(".tip").tooltip();
                dataTable_widget(obj, dashboardItemContainer);
                // $(obj).closest(dashboardItemContainer).find(".dataTables_info").appendTo($(obj).closest(dashboardItemContainer));
                // $(obj).closest(dashboardItemContainer).find(".dataTables_paginate").appendTo($(obj).closest(dashboardItemContainer));

                // $(obj).find("tbody tr td:not(td:first-child)").css({
                //     "cursor":"pointer"
                // });
                //dataTableSetwidth(this.find("[app-table-th='action']"), 100, "border-box");

                dataTableSetwidth(this.find("[app-table-th='action']"), 110, "border-box");


            },
            fnDrawCallback: function () {
                // $(".tip").tooltip();
                setDatatableTooltip($(obj).closest(dashboardItemContainer).find(".dataTable_widget"));
                if (callback) {
                    callback(1);
                }
                $(".appCheckAll").attr("checked", false);
                ui.unblockPortion($(obj).closest(dashboardItemContainer).find(".fl-main-app-view"));

                HighlightView.init(obj);


            },
            iDisplayStart: 0,
            iDisplayLength: parseInt(jsonRequestData['limit']),
        });

        return oTable;
    },
    removeDataTableAttr: function (obj) {
        var tableID = $(obj).closest(dashboardItemContainer).find(".dataTable").attr("id");
        $(obj).closest(dashboardItemContainer).find("#" + tableID + "_wrapper").remove();
    }

}
RequestFilter2 = {
    createSettingsModal: function (requestFilters, obj) {
        var form_id = $(obj).closest(dashboardItemContainer).attr("form_id");
        var text = "";
        var id = "";
        var ret = '<h3 class="fl-margin-bottom"><i class="icon-trash"></i> Remove Filter</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div id="" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; overflow: hidden;" class="ps-container">';
        ret += '<div class="content-dialog fl-remove-filter" style="width: 100%; margin-bottom: 0px;">';
        ret += '<table class="fl-filter-list" style="width:100%;">';
        ret += '<thead>';
        ret += '<tr style="font-weight:bold;">';
        ret += '<br/>';
        ret += '<th style="width:70%;font-weight:bold;">Title of you filter</th>';
        ret += '<th style="font-weight:bold;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>';
        ret += '</tr>';
        ret += '</thead>';
        ret += '<tbody>';
        for (var i in requestFilters) {
            if (requestFilters[i]['value'] != 0 && requestFilters[i]['value'] != undefined) {
                text = requestFilters[i]['text'];
                id = requestFilters[i]['value'];
                ret += '<tr id="row_' + id + '">';
                ret += '<td width="100%"><div class="fl-table-ellip">' + text + '</div></td>';
                ret += '<td style="text-align:center;">';
                ret += '<i class="tip padding_5 cursor fa fa-times deleteFilter" form_id="' + form_id + '" data-id="' + id + '" data-original-title="Delete"></i>';
                // ret += '<button class="tip btn-basicBtn padding_5 cursor icon-edit editFilter" data-id="'+ id +'" style="margin-right:3px;" data-original-title="Delete"></button>';
                ret += '</td>';
                ret += '</tr>';
            }
        }
        ret += '</tbody>';
        ret += '</table>';
        ret += '</div>';
        ret += '<div class="fields" style="">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
        ret += '</div>';
        ret += '</div>';
        return ret;
    },
    settingsModal: function (obj) {
        var requestFilters = $(obj).closest(dashboardItemContainer).find(".loadRF").find("option");
        var ret = this.createSettingsModal(requestFilters, obj);
        if (requestFilters.length == 1) {
            showNotification({
                message: "You do not have filtered search",
                type: "warning",
                autoClose: true,
                duration: 3
            });
            return;
        }
        var newDialog = new jDialog(ret, "", "", "300", "", function () {
        });
        newDialog.themeDialog("modal2");
    },
    deleteFilter: function (id, obj) {
        // var conf = "Do you want to remove this permanently?";
        // jConfirm(conf, 'Confirmation Dialog','', '', '', function(r){
        //     if(r){
        var form_id = $(obj).attr("form_id");
        var wrapper = $("[form_id='" + form_id + "']");
        var action = "deleteFilter";
        $.post("/ajax/search",
                {
                    action: action,
                    id: id
                },
        function (data) {
            $("#row_" + id).fadeOut();
            wrapper.find(".loadRF").find("option[value='" + id + "']").remove();
        })
        //     }
        // })
    },
    showFilterConfirmation: function (obj) {
        $(obj).closest(dashboardItemContainer).find(".filterAction").fadeIn();
    },
    hideFilterConfirmation: function (obj) {
        $(obj).closest(dashboardItemContainer).find(".filterAction").fadeOut();
    },
    "setConfirmation": function (obj) {
        var ret = this.createModal(obj);
        var newDialog = new jDialog(ret, "", "", "200", "", function () {

        });
        newDialog.themeDialog("modal2");
    },
    createModal: function (obj) {
        var form_id = $(obj).closest(dashboardItemContainer).attr("form_id");

        var ret = '<h3 class="fl-margin-bottom"><i class="fa fa-filter"></i> Filtered Request</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div id="comment-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; overflow: hidden;" class="ps-container">'
        ret += '<div class="content-dialog fl-filter-title sub-container-comment requestComment" style="width: 100%;">';
        ret += '    <div class="pull-left" style="width: 100%">';
        ret += '        <div class="fields_below" style="margin-bottom: 10px;">';
        ret += '            <div class="label_below2">Filter Title: <font color="red">*</font></div>';
        ret += '        <div class="input_position_below" style="">';
        ret += '            <input type="text" name="" data-type="textbox" class="form-text filter_title"></div>';
        ret += '        </div>';
        ret += '    </div>';
        ret += '</div>';
        ret += '<div class="fields" style="">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;">';
        ret += ' <input type="button" class="btn-blueBtn fl-margin-right" id="saveRF" form_id="' + form_id + '" value="Save">';
        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
        ret += '</div>';
        ret += '</div>';
        return ret;
    },
    "saveRF": function (obj) {
        var search_value = $.trim(jsonRequestData['search_value']);
        var search_field = jsonRequestData['field'];
        var multi_search = jsonRequestData['multi_search'];
        var title = $.trim($(".filter_title").val());
        var form_id = $(obj).attr("form_id");
        var date_field = jsonRequestData['date_field'];
        var date_from = jsonRequestData['date_from'];
        var date_to = jsonRequestData['date_to'];
        var limit = jsonRequestData['limit'];
        var wrapper = $("[form_id='" + form_id + "']");
        //save Request Filter
        if (title == "") {
            showNotification({
                message: "Please indicate your filter title.",
                type: "error",
                autoClose: true,
                duration: 3
            });
            return;
        }
        var err = 0;
        wrapper.find(".filterRequestsUser").find("option").each(function () {
            if (title == $(this).text()) {
                err++;
                return true;
            }

        })
        if (err > 0) {
            showNotification({
                message: "Your title has already used.",
                type: "error",
                autoClose: true,
                duration: 3
            });
            return;
        }
        $.post("/ajax/search",
                {
                    action: "saveRequestFilter",
                    search_value: search_value,
                    search_field: search_field,
                    multi_search: multi_search,
                    title: title,
                    form_id: form_id,
                    date_field: date_field,
                    date_from: date_from,
                    date_to: date_to,
                    limit: limit,
                }
        , function (data) {
            showNotification({
                message: "Filter has been saved",
                type: "success",
                autoClose: true,
                duration: 3
            });
            wrapper.find(".loadRF").append('<option value="' + data + '" ' + "multi_search='" + multi_search + "'" + ' search_value="' + search_value + '" search_field = "' + search_field + '" date_field = "' + date_field + '" date_from = "' + date_from + '" date_to = "' + date_to + '" limit = "' + limit + '">' + title + '</option>');
            $("#popup_cancel").click();
            wrapper.find(".saveFC").slideUp();
            // $(".cancelLoadFRWrapper").slideUp();
            wrapper.find(".loadRF").val(0);
        })
    },
}





ImportRequest = {
    generateImportDialog: function () {
        //$('body').on('click', '[link-type="import"]', function() {
        //    var formId = $(this).attr('link-form-id');
        //    var ret = '<h3 class="fl-margin-bottom"><i class="icon-asterisk"></i> Request Import</h3>' + 
        //            '<form action ="/ajax/request_import" class="fl-importBtn" method="POST" enctype="multipart/form-data"> ' +
        //            '<div class="hr"></div>' +
        //            '<input class="display" type="text" name="FormID" value = "' + formId + '" />' +
        //            '<div class="fields"><div class="label_below2 fl-margin-bottom">Please select file: </div><div class="input_position">' +
        //            '<div id="uniform-fileInput" class="uploader label_below2">' +
        //            '<input type="file" data-action-id="3" value="upload" name="fileupload" id="uploadForm" size="24" style="opacity: 0;" class="import_request_file" />' +
        //            '<span id="uploadFilename_3" class="filename import_request_txt">No file selected</span>' +
        //            '<span class="action">Choose File</span>' +
        //            '</div>' +
        //            '</div></div>' +
        //            '<div class="fields"><div class="label_below2"></div><div class="input_position" style="color:#ff0000;font-style:italic;margin-top:5px;">Note: File should be in Comma Separated Value(CSV) file format </div></div>' +
        //            '<div class="fields"><div class="label_below2"></div><div class="input_position" style="margin-top:5px;"><input type="submit" class="btn-blueBtn fl-margin-right" value="Ok"> <input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div></div>' +
        //            ' </form>';
        //    var newDialog = new jDialog(ret, '', '', '', '', function() {
        //
        //    });
        //    newDialog.themeDialog("modal2");
        //});

        $('body').on('click', '#import_option', function () {

            var checked = $(this).prop('checked');
            var isSkipAction = $('#rSkip').prop('checked');
            if (checked == true) {
                $('.div_update_field_reference').removeClass('display');
                if (isSkipAction) {
                    $('.div_update_option').addClass('display');
                } else {
                    $('.div_update_option').removeClass('display');
                }
            } else {
                $('.div_update_field_reference').addClass('display');
                $('.div_update_option').addClass('display');
                $('.div_add_value_field').addClass('display');
            }
        });

        $('body').on('click', '[link-type="import"]', function () {
            var formId = $(this).attr('link-form-id');
            var activeFields = $(this).attr('link-form-active-fields');
            var options = '';
            if (activeFields != '') {
                var activeFieldsArr = activeFields.split(',');
                for (var ctr = 0; ctr <= activeFieldsArr.length - 1; ctr++) {
                    options += '<option value="' + activeFieldsArr[ctr] + '">' + activeFieldsArr[ctr] + '</option>';
                }
            }
            var ret = '<h3 class="fl-margin-bottom"><i class="icon-asterisk"></i> Request Import</h3>' +
                    '<form action ="/ajax/request_import" class="fl-importBtn importCSVForm" method="POST" enctype="multipart/form-data"> ' +
                    '<div class="hr"></div>' +
                    '<input class="display" type="text" name="FormID" value = "' + formId + '" />' +
                    '<div class="fields"><div class="label_below2 fl-margin-bottom">Please select file: </div><div class="input_position">' +
                    '<div id="uniform-fileInput" class="uploader label_below2">' +
                    '<input type="file" data-action-id="3" value="upload" name="fileupload" id="uploadForm" size="24" style="opacity: 0; width:100%;" class="import_request_file" />' +
                    '<span id="uploadFilename_3" class="filename import_request_txt">No file selected</span>' +
                    '<span class="action">Choose File</span>' +
                    '</div>' +
                    '</div></div>' +
                    '<div class="fields"><div class="input_position">' +
                    '<a href="/ajax/request_import_template?form_id=' + formId + '"><label style="cursor:pointer">Download Template</label></a>' +
                    '</div></div>' +
                    '<div class="fields"><div class="label_below2 fl-margin-bottom">Options: </div><div class="input_position">' +
                    '<label class="tip" title="" data-original-title="Validate Existing Document(s)"><input type="checkbox" class="fp-user groups css-checkbox" id="import_option" name="import_option" value="1"><label for="import_option" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">Validate Existing Document(s)</span></label>' +
                    '</div></div></div>' +
                    '<div class="div_update_field_reference display"><div class="fields"><div class="label_below2 fl-margin-bottom">Field Reference: </div><div class="input_position">' +
                    '<select class="form-select import_update_reference_field fl-margin-bottom" name="import_update_reference_field" id="import_update_reference_field" style="margin-top:5px;">' + options + '</select>' +
                    '</div></div></div>' +
                    '<div class="div_update_field_reference display"><div class="fields"><div class="label_below2 fl-margin-bottom">Action: </div><div class="input_position">' +
                    '<label><input class="css-checkbox" value="0" type="radio" checked="checked" name="action" id="rSkip"><label for="rSkip" class="css-label"></label>Skip </label>' +
                    '<label><input class="css-checkbox" value="1" type="radio" name="action" id="rUpdate"><label for="rUpdate" class="css-label"></label>Update </label>' +
                    '</div></div></div>' +
                    '<div class="div_update_option display"><div class="fields"><div class="label_below2 fl-margin-bottom">Update Option: </div><div class="input_position">' +
                    '<label><input class="css-checkbox" value="0" type="radio" checked="checked" name="update_option" id="update_option_overwrite"><label for="update_option_overwrite" class="css-label"></label>Overwite Existing Data </label>' +
                    '<label><input class="css-checkbox" value="1" type="radio" name="update_option" id="update_option_merge"><label for="update_option_merge" class="css-label"></label>Add Value to Existing Data</label>' +
                    '</div></div></div>' +
                    '<div class="div_add_value_field display"><div class="fields"><div class="label_below2 fl-margin-bottom">Field to Add: </div><div class="input_position">' +
                    '<select class="form-select import_update_add_value_field fl-margin-bottom" name="import_update_add_value_field" id="import_update_add_value_field" style="margin-top:5px;">' + options + '</select>' +
                    '</div></div></div>' +
                    '<div class="fields"><div class="label_below2"></div><div class="input_position" style="color:#ff0000;font-style:italic;margin-top:5px;">Note: File should be in Comma Separated Value(CSV) file format.<br/><br/> Add Value to Existing Data works only for fields with Number or Currency Type.</div></div>' +
                    '<div class="fields"><div class="label_below2"></div><div class="input_position" style="margin-top:5px;"><input type="button" class="btn-blueBtn fl-margin-right importCSV fl-positive-btn" value="OK" active-fields="' + activeFields + '"> <input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div></div>' +
                    ' </form>';


            var newDialog = new jDialog(ret, '', '', '', '', function () {

            });
            newDialog.themeDialog("modal2");
        });

        $('body').on('click', '[name="update_option"]', function () {
            var isAdd = $('#update_option_merge').prop('checked');
            if (isAdd) {
                $('.div_add_value_field').removeClass('display');
            } else {
                $('.div_add_value_field').addClass('display');
            }
        });

        $('body').on('click', '[name="action"]', function () {
            var isUpdate = $('#rUpdate').prop('checked');
            var isAdd = $('#update_option_merge').prop('checked');
            if (isUpdate) {
                $('.div_update_option').removeClass('display');

                if (isAdd) {
                    $('.div_add_value_field').removeClass('display');
                } else {
                    $('.div_add_value_field').addClass('display');
                }

            } else {
                $('.div_update_option').addClass('display');

                if (isAdd) {
                    $('.div_add_value_field').removeClass('display');
                } else {
                    $('.div_add_value_field').addClass('display');
                }
            }
        });

        $("body").on("click", ".importCSV", function () {
            var fileName = $('#uploadForm').val();

//            $(".importCSVForm").submit();
//            return;
//


            if (fileName.isEmpty()) {
                showNotification({
                    message: "Please select file to upload.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });

                return;
            }

            var file = $('#uploadForm')[0].files[0].name;
            var type = file.split('.')[file.split('.').length - 1];

            if (type != 'csv') {
                showNotification({
                    message: "File format not valid.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });

                return;
            }


            ui.block();
            $(".importCSVForm").ajaxForm(function (data) {
                if (data == "0") {
                    showNotification({
                        message: "File should be in CSV format!",
                        type: "error",
                        autoClose: true,
                        duration: 3
                    });
                    ui.unblock();
                    return;
                }


                data = JSON.parse(data);
                console.log('data', data);

                if (data.Invalid_Field_Names) {
                    if (data.Invalid_Field_Names.length >= 1) {
                        var errHeaders = data.Invalid_Field_Names.join(', ');
//                    console.log('errHeaders', errHeaders);
                        showNotification({
                            message: "The following fields are not existing on the form - " + errHeaders + '. Please modify and re-import',
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                        ui.unblock();
                        return;

                    }
                }

                if (data.Invalid_Field_Format) {
                    if (data.Invalid_Field_Format.length >= 1) {
                        showNotification({
                            message: "There's a value format error on your import, an email has been sent to you for your reference. Please modify and re-import",
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                        ui.unblock();
                        return;
                    }
                }


//                var importOpton = data['import_option'];
//                var message = 'Import Complete!<br/>' +
//                        'No. of document(s) added : ' + data['insert_count'];
//
//
//                if (importOpton == 1) {
//                    message += '; No. of document(s) updated : ' + data['update_count'];
//                }

                showNotification({
                    message: "Importing Data.... An email will be sent to you once done.",
                    type: "information",
                    autoClose: true,
                    duration: 3
                });

                $("#popup_cancel").click();
                ui.unblock();
                setTimeout(function () {
                    // window.location = "/user_view/application?id=" + data['application_id'];
                    $(".searchRequestUsersButton").trigger("click");
                }, 2000)
            }).submit();
        })
    },
}

HighlightView = {
    init: function (obj) {

        this.background_color(obj);
        this.font_color(obj);
    },
    background_color: function (obj) {
        //set background per row
        var allow_backgorund_highlight = $.trim($("#allow-background-highlight").text());
        if (allow_backgorund_highlight == "1") {

            var color = "";
            var background_prop;
            var cell_to_highlight;

            $(obj).closest(dashboardItemContainer).find(".dataTable tbody tr").each(function () {
                var self_tr = $(this);
                background_prop = $(this).find(".fl-table-actions").attr("background-highlight");
                try {
                    background_prop = JSON.parse(background_prop);
                    color = background_prop['color'];
                    cell_to_highlight = background_prop['cell_to_highlight']

                    console.log(background_prop);

                    if (color != "" && color != null) {
                        // alert(background_prop['highlight_type'])
                        if (background_prop['highlight_type'] == "row") {
                            console.log("self_tr", self_tr);
                            self_tr.css({
                                "background-color": color
                            })
                        } else if (background_prop['highlight_type'] == "cell") {

                            // alert(cell_to_highlight.length)
                            for (var i in cell_to_highlight) {
                                var td_eq = $(obj).closest(dashboardItemContainer).find(".dataTable thead th[field_name='" + cell_to_highlight[i] + "']").index();
                                if (td_eq != -1) {
                                    self_tr.find("td").eq(td_eq).css({
                                        "background-color": color
                                    })
                                }

                            }

                        }
                    }
                } catch (e) {

                }


            })
        }
    },
    font_color: function (obj) {
        //set font per row
        var allow_backgorund_highlight_font = $.trim($("#allow-background-font").text());

        if (allow_backgorund_highlight_font == "1") {

            var color = "";
            var font_prop;
            var cell_to_highlight_font;

            $(obj).closest(dashboardItemContainer).find(".dataTable tbody tr").each(function () {
                var self_tr = $(this);
                font_prop = $(this).find(".fl-table-actions").attr("font-highlight");
                try {
                    font_prop = JSON.parse(font_prop);
                    color = font_prop['color'];
                    cell_to_highlight_font = font_prop['cell_to_highlight_font']

                    console.log(font_prop);

                    if (color != "" && color != null) {
                        // alert(font_prop['highlight_type_font'])
                        if (font_prop['highlight_type_font'] == "row") {
                            console.log("self_tr", self_tr);
                            self_tr.css({
                                "color": color
                            })
                        } else if (font_prop['highlight_type_font'] == "cell") {

                            // alert(cell_to_highlight_font.length)
                            for (var i in cell_to_highlight_font) {
                                var td_eq = $(obj).closest(dashboardItemContainer).find(".dataTable thead th[field_name='" + cell_to_highlight_font[i] + "']").index();
                                if (td_eq != -1) {
                                    self_tr.find("td").eq(td_eq).css({
                                        "color": color
                                    })
                                }

                            }

                        }
                    }
                } catch (e) {

                }


            })
        }
    }
}


