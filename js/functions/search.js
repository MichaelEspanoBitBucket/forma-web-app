var continueToLoad = true;
var continueToLoad_workflow = true;
var continueToLoad_form = true;
var continueToLoad_orgchart = true;
var continueToLoad_report = true;
var ctr_load = 10;
var ctr_load_workflow = 10;
var ctr_load_form = 10;
var ctr_load_orgchart = 10;
var ctr_load_report = 10;


$(document).ready(function() {

    // $('body').on('click', '.icon-book', function() {
        // var request_id = $(this).attr('data-request-id');
        // var form_id = $(this).attr('data-form-id');

        // RequestWidgets.view_logs(request_id, form_id);
    // });

    $('body').on('click', '.requestlink', function() {
        var request_id = $(this).attr('request-id');
        var formId = $(this).attr('form-id');
        var workflowId = $(this).attr('workflow-id');
        var nodeId = $(this).attr('node-id');

        var request_details = {
            FormID: formId,
            ID: request_id,
            Node_ID: nodeId,
            Workflow_ID: workflowId,
            Mode: "viewApproval"
        }

        jConfirm('Are you sure you want to proceed?', '', '', '', '', function(r) {
            if (r) {
                RequestViewSearch.process(request_details);
            }
        });
    });

    var pathname = window.location.pathname;
    if (pathname == "/application") {
        //get request initially
        RequestViewSearch.getRequest(0, "start", function() {

        });
        //loadmore for requests
        $(window).scroll(function() {
            var scroll_diff = $(window).scrollTop() - ($(document).height() - $(window).height());
            // console.log(scroll_diff)
            // console.log((scroll_diff+" >= "+(-1))+" && "+(scroll_diff+" <= "+1))
            if ((scroll_diff >= -2) && (scroll_diff <= 2)) {
                if (continueToLoad == true) {
                    RequestViewSearch.getRequest(ctr_load, "load", function() {

                    });

                }
            }
        });
        //Search request
        $(".search-request-value").keydown(function(e) {
            if (e.keyCode == "13") {
                RequestFilter.showConfirmation();
                RequestViewSearch.getRequest(0, "start", function() {

                });

            }
        })

        $(".search-requests").click(function() {
            RequestFilter.showConfirmation();
            RequestViewSearch.getRequest(0, "start", function() {

            });
        })

        $(".search-request-field").keydown(function(e) {
            if (e.keyCode == "13") {
                RequestFilter.showConfirmation();
                RequestViewSearch.getRequest(0, "start", function() {

                });
            }
        })
        $("#cancelFC").click(function() {
            RequestFilter.hideConfirmation();
        })
        $("#saveFC").click(function() {
            RequestFilter.setConfirmation();
        })
        $("#loadRF").change(function() {
            RequestFilter.loadRF(this);
        })
        $("body").on("click", "#saveRF", function() {
            RequestFilter.saveRF();
        })
        $("#cancelLoadFR").click(function() {
            RequestFilter.loadDefault();
        })
        $(".requestFilterSettings").click(function() {
            RequestFilter.settingsModal();
        })
        $("body").on("click", ".deleteFilter", function() {
            var id = $(this).attr("data-id");
            RequestFilter.deleteFilter(id);
        })

        //ADVANCE FILTER
        $("body").on("click", ".advanceFilterDialog", function() {
            AdvanceFilter.showDialog();
        })
        $("body").on("click", "#advanceFilter", function() {
            AdvanceFilter.filter();
        })
        $("body").on("click", "#clearValue", function() {
            AdvanceFilter.clearFilterFields();
        })
    } else if (pathname == "/flowchart_list") {
        //get workflow initially
        searchWorkflow($(".search-workflow-value"), "0", "start", function() {

        });

        //loadmore for requests
        $(window).scroll(function() {
            var scroll_diff = $(window).scrollTop() - ($(document).height() - $(window).height());
            // console.log(scroll_diff)
            // console.log((scroll_diff+" >= "+(-1))+" && "+(scroll_diff+" <= "+1))
            if ((scroll_diff >= -2) && (scroll_diff <= 2)) {

                if (continueToLoad_workflow == true) {
                    searchWorkflow($(".search-workflow-value"), ctr_load_workflow, "load", function() {
                    });
                }
            }
        });
        //search workflow
        $(".search-workflow-value").keydown(function(e) {
            if (e.keyCode == "13") {
                searchWorkflow(this, "0", "start", function() {

                });
            }
        });


    } else if (pathname == "/forms") {
        //get forms initially
        searchForm($(".search-forms-value"), 0, "start", function() {

        });

        //loadmore for requests
        $(window).scroll(function() {
            var scroll_diff = $(window).scrollTop() - ($(document).height() - $(window).height());
            // console.log(scroll_diff)
            // console.log((scroll_diff+" >= "+(-1))+" && "+(scroll_diff+" <= "+1))
            if ((scroll_diff >= -2) && (scroll_diff <= 2)) {
                if (continueToLoad_form == true) {
                    searchForm($(".search-forms-value"), ctr_load_form, "load", function() {
                    });
                }
            }
        });

        //search forms
        $(".search-forms-value").keydown(function(e) {
            if (e.keyCode == "13") {
                searchForm(this, 0, "start", function() {
                });
            }
        })
    } else if (pathname == "/orgchart_list") {
        //get orgchart initially
        searchOrgchart($(".search-orgchart-value"), 0, "start", function() {

        });

        //loadmore for requests
        $(window).scroll(function() {
            var scroll_diff = $(window).scrollTop() - ($(document).height() - $(window).height());
            // console.log(scroll_diff)
            // console.log((scroll_diff+" >= "+(-1))+" && "+(scroll_diff+" <= "+1))
            if ((scroll_diff >= -2) && (scroll_diff <= 2)) {
                if (continueToLoad_orgchart == true) {
                    searchOrgchart($(".search-orgchart-value"), ctr_load_orgchart, "load", function() {

                    });
                }
            }
        });

        //search orgchart
        $(".search-orgchart-value").keydown(function(e) {
            if (e.keyCode == "13") {
                searchOrgchart(this, 0, "start", function() {

                });
            }
        })
    } else if (pathname == "/rep_list") {
        //loadmore for reports
        $(window).scroll(function() {
            var scroll_diff = $(window).scrollTop() - ($(document).height() - $(window).height());
            // console.log(scroll_diff)
            // console.log((scroll_diff+" >= "+(-1))+" && "+(scroll_diff+" <= "+1))

            if ((scroll_diff >= -2) && (scroll_diff <= 2)) {

                if (continueToLoad_report == true) {
                    searchReports($(".search-report-value"), ctr_load_report, "load", function() {

                    });
                }
            }
        });

        //search reports
        $(".search-report-value").keydown(function(e) {
            if (e.keyCode == "13") {
                searchReports(this, "0", "start", function() {

                });
            }
        });
    } else if (pathname == "/print_form_list") {
        searchPrintReports($(".search-printreport-value"), "0", "start", function() {

        });
    }





    // $('body').on('click', '.icon-book', function() {
    //     var request_id = $(this).attr('data-request-id');
    //     var form_id = $(this).attr('data-form-id');

    //     RequestWidgets.view_logs(request_id, form_id);
    // });

    $('body').on('click', '.requestlink', function() {
        var request_id = $(this).attr('request-id');
        var formId = $(this).attr('form-id');
        var workflowId = $(this).attr('workflow-id');
        var nodeId = $(this).attr('node-id');

        var request_details = {
            FormID: formId,
            ID: request_id,
            Node_ID: nodeId,
            Workflow_ID: workflowId,
            Mode: "viewApproval"
        }

        jConfirm('Are you sure you want to proceed?', '', '', '', '', function(r) {
            if (r) {
                RequestViewSearch.process(request_details);
            }
        });
    });



});

RequestViewSearch = {
    process: function(request_details) {
        var self = this;

        if ($('body').children(".submit-overlay").length == 0) {
            var submit_overlay = $('<div class="submit-overlay" style="background:url(/images/loader/loader.gif);z-index:999999;background-repeat:no-repeat;background-attachment:fixed;background-color:rgba(200,200,200,0.5);background-position:center;position:fixed;width:100%;height:100%;text-align:center;vertical-align:middle;">Loading...</div>');
            $('body').prepend(submit_overlay);
        }
        $.post('/ajax/request', request_details,
                function(data) {
                    if (submit_overlay.length >= 1) {
                        submit_overlay.fadeOut(function() {
                            $(this).remove();
                        });
                    }
                    showNotification({
                        message: "Request has been processed.",
                        type: "success",
                        autoClose: true,
                        duration: 3
                    });

                    self.getRequest(0, "start", function() {

                    });
                });
    },
    getRequest: function(start, status, callback) {
        continueToLoad = false;
        var form_id = $("#form_id").val();
        var search_value = $(".search-request-value").val();
        var search_field = $(".search-request-field").val();
        var self = this;
        var date_field = "";
        var date_from = "";
        var date_to = "";
        if ($(".dateRangeWrapper").length == 1) {
            date_field = $(".search-request-field_filter_date").val();
            date_from = $(".search-request_date_from").val();
            date_to = $(".search-request_date_to").val();
        } else {
            date_field = $(".search-request-field_filter_date_form").val();
            date_from = $(".search-request_date_from_form").val();
            date_to = $(".search-request_date_to_form").val();
        }
        if ($(".request-wrapper").find(".load-app-request").length == 0) {
            $(".request-wrapper").append('<center><img src="/images/loader/loader.gif" class="load-app-request" style="display: inline;margin-top:30px;"></center>');
        }
        // console.log("id : "+form_id+",\nsearch_value : "+search_value+",\nfield : "+search_field+",\nstart : "+start+",\ndate_field : "+date_field+",\ndate_from : "+date_from+",\ndate_to : "+date_to)
        $.post("/ajax/search", {
            action: "getRequest",
            id: form_id,
            search_value: search_value,
            field: search_field,
            start: start,
            date_field: date_field,
            date_from: date_from,
            date_to: date_to,
        }, function(data) {
            // console.log(data)
            // return;
            data = JSON.parse(data);

            var result = self.viewRequest(data, status);

            if (status == "start") {
                $(".request-wrapper").html(result);
                //restore variables for load more
                ctr_load = 10;
                continueToLoad = true;
            } else if (status == "load") {
                ctr_load += 10;
                $(".request-wrapper").append(result);
                continueToLoad = false;

            }
            limitText(".detailed_info", 100);
            $('[button-type="action"]').each(function() {
                var actions = JSON.parse($(this).attr('button-action-data'));
                var request_id = $(this).attr('button-request-id');
                var workflow_id = $(this).attr('button-workflow-id');
                var requestor_id = $(this).attr('button-requestor-id');
                var processor_id = $(this).attr('button-processor-id');


                var html_action = '<ul>';
                for (var index in actions) {
                    html_action += '<a href="#" class="requestlink" link-type="request-actions" request-id="' + request_id + '"form-id="' + form_id + '" workflow-id="' + workflow_id + '" node-id="' + actions[index].child_id + '"><li>' + actions[index].button_name + '</li></a>';
                }
                html_action += '</ul>';

                $(this).popover({
                    placement: 'bottom',
                    title: 'Please select Action',
                    html: true,
                    content: html_action
                });

            });

            $(".load-app-request").remove();
            $(".tip").tooltip();
            continueToLoad = true;
            if (data.length == 0) {
                continueToLoad = false;
            }


            callback();
        });

    },
    viewRequest: function(data, status) {
        var form_id = $("#form_id").val();
        var print_form_id = $("#print_form_id").val(); // print form ID
        var result = "";
        var form_name = $(".form-name").val();
        var form_header = $(".form-header").text();
        if (form_header) {
            form_header = JSON.parse(form_header);
        }
        var field_name = "";
        if (data.length == 0) {
            if (status == "start") {
                result += returnEmptyRequest();
            } else {
                if ($(".request-wrapper").find(".wrong").length == 0) {
                    // result += returnEmptyRequest();
                }
            }
        } else {

            //$.each(data,function(key, request){
            for (var i in data) {
                var starred_class = "icon-star-empty";
                var starred = 0;
                if (data[i][0]['starred'] > 0) {
                    starred_class = "icon-star";
                    starred = data[i][0]['starred'];
                }

                result +=
                        '<div class="request-box" style="" form-id = "' + form_id + '" request-id="' + data[i]['request_id'] + '">' +
                        '<div class="supTicket nobg"  style="float: left;width: 30%;">' +
                        '<div class="issueType">' +
                        '<span class="issueInfo" style="background-color: transparent;">' + form_name + '</span>' +
                        '</div>' +
                        '<div class="issueSummary" style="padding-top:5px">' +
                        '<a href="#" title="" class="pull-left">' +
                        data[i][1]['user_image'] +
                        '</a>' +
                        '</div>' +
                        '<div class="ticketInfo">' +
                        '<ul style="margin-left: 10px;margin-top: 10px;">' +
                        '<li style="width:100%;"><a href="#" title="" style="font-weight: bolder">' + data[i]['first_name'] + ' ' + data[i]['last_name'] + '</a></li>' +
                        '<li style="width:100%;">Status: <strong class="green">[ ' + data[i]['form_status'] + ' ]</strong></li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>' +
                        '<div class="supTicket nobg"  style="float: left;width: 30%;">' +
                        '<div class="issueType">' +
                        '<span class="issueInfo" style="background-color: transparent;">Detailed Info</span>' +
                        '</div>' +
                        '<div class="issueSummary" style="padding-top:5px">';

                if (form_header == null || form_header == "") {
                    result += '<i ></i> <strong style="font-size: 10px;color: rgb(241, 79, 79);">No Header Information available</strong> ';
                } else {
                    var fieldValue = '';
                    for (var a in form_header) {
                        field_name = form_header[a]['field_name'];

                        if (field_name.indexOf('[]') >= 0) {
                            field_name = field_name.split('[]').join('');
                            console.log(field_name);

                            if (data[i]['' + field_name + ''] != '') {
                                fieldValue = data[i]['' + field_name + ''].split('|^|').join(',');
                            } else {
                                fieldValue = '';
                            }

                        } else {
                            fieldValue = data[i]['' + field_name + ''];
                        }

                        result += '<i class="icon-circle" style="font-size:7px"></i> <strong class="detailed_info" style="font-size: 10px;color: #6D6868;">' + fieldValue + '</strong> ';
                    }
                }

                result +=
                        '</div>' +
                        '</div>' +
                        '<div class="supTicket nobg" style="float: right;width: 28%;">' +
                        '<div class="issueType">' +
                        '<span class="issueNum pull-right" style="background-color: transparent;">';
                if (print_form_id != "") {
                    result += '<a href="/workspace?view_type=update&ID=' + print_form_id + '&formID=' + form_id + '&requestID=' + data[i]['request_id'] + '&trackNo=' + data[i]['TrackNo'] + '"><button class="tip icon-bar-chart btn-basicBtn padding_5 cursor" style="margin-right:3px" form-id = "' + form_id + '" request-id="' + data[i]['request_id'] + '" data-original-title="Customize Report"></button></a>';
                }
                result +=
                        '<a href="/workspace?view_type=request&formID=' + form_id + '&requestID=' + data[i]['request_id'] + '&trackNo=' + data[i]['TrackNo'] + '"><button class="tip icon-eye-open btn-basicBtn padding_5 cursor" style="margin-right:3px" form-id = "' + form_id + '" request-id="' + data[i]['request_id'] + '" data-original-title="View Form"></button></a>' +
                        '<button class="tip ' + starred_class + ' btn-basicBtn padding_5 cursor starred" style="margin-right:3px" data-id="' + data[i]['request_id'] + '" id="starred_' + data[i]['request_id'] + '" data-starred="' + starred + '" data-original-title="Starred Request"></button>' +
                        '<button class="icon-comment btn-basicBtn padding_5 cursor showReply tip" data-type="1" data-f-id="' + form_id + '" data-id="' + data[i]['request_id'] + '" data-original-title="Add/View Comments" ></button> ' +
                        '<button class="tip icon-book btn-basicBtn padding_5 cursor viewAuditLogs" style="margin-right:3px" data-request-id="' + data[i]['request_id'] + '" data-form-id="' + form_id + '" data-original-title="Audit Logs"></button>';


                if (data[i][2].buttons != '' && data[i][2].buttons != 'null' && data[i][2].buttons != null) {
                    result += "<button class='tip icon-cogs btn-basicBtn padding_5 cursor' button-action-data='" + data[i][2].buttons + "' button-request-id='" + data[i]['ID'] + "'button-workflow-id='" + data[i]['Workflow_ID'] + "'  button-requestor-id='" + data[i]['Requestor'] + "' button-processor-id='" + data[i]['Processor'] + "' button-type='action' data-original-title='Please select an Action'></button>";
                }

                result += '</span>' +
                        '</div>' +
                        '<div class="ticketInfo">' +
                        '<ul style="margin-top: 35px;">' +
                        '<li style="width:100%;" class="even"><strong class="">[ ' + data[i]['TrackNo'] + '  ]</strong></li>' +
                        '<li style="width:100%;" class="even" style="font-size: 10px;color: rgb(158, 144, 144);">' + data[i]['DateCreated'] + '</li>' +
                        '</ul>' +
                        '</div>' +
                        '</div>' +
                        '<div class="hr"></div>' +
                        '</div>';


                // result +='<div class="request-box" style="" form-id = "'+ form_id +'" request-id="'+ data[i]['request_id'] +'">'+
                //     '<div class="supTicket nobg">'+
                //        '<div class="issueType">'+
                //           '<span class="issueInfo" style="background-color: transparent;">'+ form_name +'</span>'+
                //           '<span class="issueNum pull-right" style="background-color: transparent;">'+
                //              '<a href="/workspace?view_type=request&formID='+ form_id +'&requestID='+ data[i]['request_id'] +'"><button class="tip icon-eye-open btn-basicBtn padding_5 cursor" style="margin-right:3px" form-id = "'+ form_id +'" request-id="'+ data[i]['request_id'] +'" data-original-title="View Form">'+
                //                 // '<i class="" ></i>'+
                //              '</button></a>'+
                //              '<button class="tip '+starred_class +' btn-basicBtn padding_5 cursor starred" data-id="'+ data[i]['request_id'] +'" id="starred_'+ data[i]['request_id'] +'" data-starred="'+ starred +'" data-original-title="Starred Request">'+
                //                 // '<i class=""></i>'+
                //              '</button>'+
                //           '</span>' +
                //        '</div>'+
                //        '<div class="issueSummary" style="padding-top:5px">'+
                //              '<a href="#" title="" class="pull-left">'+
                //              //'<img src="/images/avatar/small.png" class="" width="50" height="50" alt="" style="border:none;">'+
                //              data[i][1]['user_image'] + 
                //              '</a>'+    
                //           '<div class="ticketInfo" >'+
                //              '<ul style="margin-left: 10px">'+
                //                 '<li><a href="#" title="" style="font-weight: bolder">'+ data[i]['first_name'] +' '+ data[i]['last_name'] +'</a></li>'+
                //                 '<li class="even"><strong class="">[ '+ data[i]['TrackNo'] +'  ]</strong></li>'+
                //                 '<li>Status: <strong class="green">[ '+ data[i]['form_status'] +' ]</strong></li>'+
                //                 '<li class="even" style="font-size: 10px;color: rgb(158, 144, 144);">'+ data[i]['DateCreated'] +'</li>'+
                //              '</ul>'+
                //           '</div>'+
                //        '</div>'+
                //     '</div>'+
                //     '<div class="hr" style="margin: 0;height: 3px"></div>'+
                //  '</div>';
            }
            //});

        }

        return result;

    }

}



function searchOrgchart(obj, start, type, callback) {
    continueToLoad_orgchart = false;
    var search_value = $(obj).val();
    if ($(".orgchart-list-wrapper").find(".load-app-request").length == 0) {
        $(".orgchart-list-wrapper").append('<center><img src="/images/loader/loader.gif" class="load-app-request" style="display: inline;margin-top:30px;"></center>');
    }
    $.post("/ajax/search", {
        action: "getOrgchart",
        search_value: search_value,
        start: start
    }, function(data) {
        data = JSON.parse(data);
        var ret = "";
        if (data.length == 0) {
            if (type == "start") {
                ret += '<div class="notification_wrapper" style="height:auto">' +
                        '<div class="wrong" style="width:98.5%">' +
                        '<div class="padding_5">' +
                        '<img src="/images/warning/wrong.png" width="22" height="19" class="img-notification-position">' +
                        '<div class="notification-position">No Record Found</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
            }

        }
        $.each(data, function(key, val) {
            ret += '<div class="supTicket nobg">';
            ret += '<div class="issueType">';
            ret += '<span class="issueInfo">' + val['title'] + '</span>';
            ret += '<span class="issueNum">';
            ret += '<a href="/organizational_chart?view_type=edit&id=' + val['id'] + '" class="tip cursor" data-original-title="Edit ' + val['title'] + '" data-placement="top" id="" data-orgchart-id="" style="margin-right:3px"><button class="btn-basicBtn padding_5 cursor"><i class="icon-edit" ></i></button></a>';
            ret += '<a class="tip cursor update_status_orgchart" data-original-title="' + getStatus_revert(val['status']) + '" data-status="' + val['status'] + '" data-placement="top" id="" data-id="' + val['id'] + '" style="margin-right:3px"><button class="btn-basicBtn padding_5 cursor"><i class="icon-globe"></i></button></a>';
            ret += '<a class="tip cursor delete_orgchart" data-original-title="Delete Orgchart" data-placement="top" id="" data-id="' + val['id'] + '"><button class="btn-basicBtn padding_5 cursor"><i class="icon-trash"></i></button></a>';
            ret += '</span>';

            ret += '</div>';

            ret += '<div class="issueSummary">';
            ret += '<a href="#" title="" class="pull-left"><i style="height:30px; width:30px;font-size:30px;color: #676767;" class="icon-sitemap"></i></a>';
            ret += '<div class="ticketInfo">';
            ret += '<ul>';
            ret += '<li><a href="/organizational_chart?view_type=edit&id=' + val['id'] + '" class="tip cursor" data-original-title="Edit ' + val['title'] + '" title="">' + val['title'] + '</a></li>';
            ret += '<li class="even"><strong class="red">[ Organizational Chart ]</strong></li>';
            ret += '<li>Status: <strong class="green">[ ' + getStatus(val['status']) + ' ]</strong></li>';
            ret += '<li class="even">' + val['date'] + '</li>';
            ret += '</ul>';
            ret += '</div>';
            ret += '</div>';
            ret += '</div>';
            ret += '<div class="hr"></div>';
        })
        if (type == "start") {
            $(".orgchart-list-wrapper").html(ret);
            ctr_load_orgchart = 10;
        } else {
            $(".orgchart-list-wrapper").append(ret);
            ctr_load_orgchart += 10;
            ;
        }
        continueToLoad_orgchart = true;
        if (data.length == 0) {
            continueToLoad_orgchart = false;
        }
        $(".tip").tooltip();
        $(".load-app-request").remove();
        callback();
    })
}

function searchForm(obj, start, type, callback) {
    continueToLoad_form = false;
    var search_value = $(obj).val();
    if ($(".form-list-wrapper").find(".load-app-request").length == 0) {
        $(".form-list-wrapper").append('<center><img src="/images/loader/loader.gif" class="load-app-request" style="display: inline;margin-top:30px;"></center>');
    }
    $.post("/ajax/search", {
        action: "getForm",
        search_value: search_value,
        start: start
    }, function(data) {
        data = JSON.parse(data);
        var ret = "";
        if (data.length == 0) {
            if (type == "start") {
                ret += '<div class="notification_wrapper" style="height:auto">' +
                        '<div class="wrong" style="width:98.5%">' +
                        '<div class="padding_5">' +
                        '<img src="/images/warning/wrong.png" width="22" height="19" class="img-notification-position">' +
                        '<div class="notification-position">No Record Found</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
            }
        }
        $.each(data, function(key, val) {
            ret += '<div class="supTicket nobg">' +
                    '<div class="issueType">' +
                    '<span class="issueInfo">' + val['form_name'] + '</span>' +
                    '<span class="issueNum">' +
                    '<a href="/formbuilder?formID=' + val['wsID'] + '&view_type=edit" class="tip cursor" data-original-title="Edit" data-placement="top" id="" data-form-id="' + val['wsID'] + '" style="margin-right:3px">' +
                    '<button class="btn-basicBtn padding_5 cursor">' +
                    '<i class="icon-edit"></i>' +
                    '</button>' +
                    '</a>' +
                    '<a class="tip cursor update_status_form" data-original-title="' + getStatus_revert(val['ws_status']) + '" data-status="' + val['ws_status'] + '" data-placement="top" id="" data-id="' + val['wsID'] + '" style="margin-right:3px">' +
                    '<button class="btn-basicBtn padding_5 cursor">' +
                    '<i class="icon-globe"></i>' +
                    '</button>' +
                    '</a>' +
                    '<a class="tip cursor delete_form" data-original-title="Delete Form" data-form-id="' + val['wsID'] + '">' +
                    '<button class="btn-basicBtn padding_5 cursor">' +
                    '<i class="icon-trash"></i>' +
                    '</button>' +
                    '</a>' +
                    // '<a href="/formbuilder?formID='+ val['wsID'] +'&view_type=edit" class="tip cursor" data-original-title="View" data-placement="top" id="" data-form-id="'+ val['wsID'] +'"><button class="btn-basicBtn padding_5"><i class="icon-edit" ></i></button></a>'+
                    // '<a class="tip cursor" data-original-title="'+ getStatus_revert(val['ws_status']) +'" data-placement="top" id="" data-form-id="'+ val['wsID'] +'"><button class="btn-basicBtn padding_5"><i class="icon-globe"></i></button></a>'+
                    '</span>' +
                    '</div>' +
                    '<div class="issueSummary">' +
                    '<a href="#" title="" class="pull-left"><i style="height:30px; width:30px;font-size:30px;color: #676767;" class="icon-reorder"></i></a> ' +
                    '<div class="ticketInfo">' +
                    '<ul>' +
                    '<li><a href="/formbuilder?formID=' + val['wsID'] + '&view_type=edit" title="">' + val['form_name'] + '</a></li>' +
                    '<li class="even"><strong class="red">[ Form ]</strong></li>' +
                    '<li>Status: <strong class="green">' + getStatus(val['ws_status']) + '</strong></li>' +
                    '<li class="even">' + val['date_created'] + '</li>' +
                    '</ul>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="hr"></div>';
        })
        if (type == "start") {
            $(".form-list-wrapper").html(ret);
            ctr_load_form = 10;
        } else {
            $(".form-list-wrapper").append(ret);
            ctr_load_form += 10;
        }
        $(".tip").tooltip();
        $(".load-app-request").remove();
        continueToLoad_form = true;
        if (data.length == 0) {
            continueToLoad_form = false;
        }
        callback();
    })
}
function searchWorkflow(obj, start, type, callback) {
    continueToLoad_workflow = false;
    var search_value = $(obj).val();
    if ($(".workflow-list-wrapper").find(".load-app-request").length == 0) {
        $(".workflow-list-wrapper").append('<center><img src="/images/loader/loader.gif" class="load-app-request" style="display: inline;margin-top:30px;"></center>');
    }

    $.post("/ajax/search", {
        action: "getWorkflow",
        search_value: search_value,
        start: start
    }, function(data) {
        // console.log(data)
        // return;
        data = JSON.parse(data);
        var ret = "";
        if (data.length == 0) {
            ret += '<div class="notification_wrapper" style="height:auto">' +
                    '<div class="wrong" style="width:98.5%">' +
                    '<div class="padding_5">' +
                    '<img src="/images/warning/wrong.png" width="22" height="19" class="img-notification-position">' +
                    '<div class="notification-position">No Record Found</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
        }
        // $.each(data,function(key, val){
        var val = "";
        for (var i in data) {
            val = data[i];
            ret += '<div class="supTicket nobg">';
            ret += '<div class="issueType">';
            ret += '<span class="issueInfo"><i class="icon-reorder"></i>&nbsp;&nbsp;' + val['form_name'] + '</span>';
            ret += '<span class="issueNum">';
            ret += '<a href="/workflow?view_type=edit&form_id=' + val['form_id'] + '&id=' + val['wf_id'] + '" class="tip cursor" data-original-title="Edit ' + val['wf_title'] + '" data-placement="top" id="" data-orgchart-id="" style="margin-right:3px"><button class="btn-basicBtn padding_5 cursor"><i class="icon-edit" ></i></button></a>';
            ret += '<a class="tip cursor update_status_workflow" data-original-title="' + getStatus_revert(val['Is_Active']) + '" data-status="' + val['Is_Active'] + '" data-placement="top" id="" data-id="' + val['wf_id'] + '" style="margin-right:3px"><button class="btn-basicBtn padding_5 cursor"><i class="icon-globe"></i></button></a>';
            ret += '<a class="tip cursor delete_workflow" data-original-title="Delete Workflow" data-placement="top" id="" data-id="' + val['wf_id'] + '"><button class="btn-basicBtn padding_5 cursor"><i class="icon-trash"></i></button></a>';
            ret += '</span>';

            ret += '</div>';

            ret += '<div class="issueSummary">';
            ret += '<a href="#" title="" class="pull-left"><i style="height:30px; width:30px;font-size:30px;color: #676767;" class="icon-retweet"></i></a>';
            ret += '<div class="ticketInfo">';
            ret += '<ul>';
            ret += '<li><a href="/workflow?view_type=edit&form_id=' + val['form_id'] + '&id=' + val['wf_id'] + '" title="">' + val['wf_title'] + '</a></li>';
            ret += '<li class="even"><strong class="red">[ Workflow ]</strong></li>';
            ret += '<li>Status: <strong class="green">[ ' + getStatus(val['Is_Active']) + ' ]</strong></li>';
            ret += '<li class="even">' + val['wf_date'] + '</li>';
            ret += '</ul>';
            ret += '</div>';
            ret += '</div>';
            ret += '</div>';
            ret += '<div class="hr"></div>';
        }
        // })
        if (type == "start") {
            $(".workflow-list-wrapper").html(ret);
            ctr_load_workflow = 10;
        } else {
            $(".workflow-list-wrapper").append(ret);
            ctr_load_workflow += 10;
        }

        $(".tip").tooltip();
        $(".load-app-request").remove();
        continueToLoad_workflow = true;
        if (data.length == 0) {
            continueToLoad_workflow = false;
        }
        callback()
    })
}

function searchReports(obj, start, type, callback) {
    continueToLoad_report = false;
    var search_value = $(obj).val();
    if ($(".report-list-wrapper").find(".load-app-request").length == 0) {
        $(".report-list-wrapper").append('<center><img src="/images/loader/loader.gif" class="load-app-request" style="display: inline;margin-top:30px;"></center>');
    }
    $.post("/ajax/search", {
        action: "getReports",
        search_value: search_value,
        start: start
    }, function(data) {
        data = JSON.parse(data);
        var ret = "";

        $.each(data, function(key, val) {
            ret += '<div class="hr"></div>';
            ret += '<div class="supTicket nobg">';
            ret += '<div class="issueType">';
            ret += '<span class="issueInfo">' + val['title'] + '</span>';
            ret += '<span class="issueNum">';

            if (val["is_active"] == 1) {
                ret += '<a class="tip cursor" data-original-title="Generate ' + val['title'] + '" data-placement="top" id="" link-type="report-generation" data-report-id="' + val['id'] + '"><button class="btn-basicBtn padding_5 cursor"><i class="icon-edit" ></i></button></a> ';
            }

            if ($(".userLevel").val() == 2) {
                ret += '<a href="/report?id=' + val['id'] + '&form_id=' + val['form_id'] + '" class="tip cursor" data-original-title="Edit ' + val['title'] + '" data-placement="top" id="" data-orgchart-id="" style="margin-right:3px"><button class="btn-basicBtn padding_5 cursor"><i class="icon-edit" ></i></button></a>';
                ret += '<a class="tip cursor update_status_report" data-original-title="' + getStatus_revert(val['is_active']) + '" data-status="' + val['is_active'] + '" data-placement="top" id="" data-id="' + val['id'] + '"><button class="btn-basicBtn padding_5 cursor"><i class="icon-globe"></i></button></a>';
            }
            ret += '</span>';

            ret += '</div>';

            ret += '<div class="issueSummary">';
            ret += '<a href="#" title="" class="pull-left"><i style="height:30px; width:30px;font-size:30px;color: #676767;" class="icon-bar-chart"></i></a>';
            ret += '<div class="ticketInfo">';
            ret += '<ul>';
            ret += '<li><a href="/report?id=' + val['id'] + '&form_id=' + val['form_id'] + '" title="">' + val['title'] + '</a></li>';
            ret += '<li class="even"><strong class="red">[ Report ]</strong></li>';
            ret += '<li>Status: <strong class="green">[ ' + getStatus(val['is_active']) + ' ]</strong></li>';
            ret += '<li class="even">' + val['date_created'] + '</li>';
            ret += '</ul>';
            ret += '</div>';
            ret += '</div>';
            ret += '</div>';
            ret += '<div class="hr"></div>';
        })
        if (type == "start") {
            $(".report-list-wrapper").html(ret);
            ctr_load_report = 10;
        } else {
            $(".report-list-wrapper").append(ret);
            ctr_load_report += 10;
        }
        $(".tip").tooltip();
        $(".load-app-request").remove();
        continueToLoad_report = true;
        if (data.length == 0) {
            continueToLoad_report = false;
        }
        callback();
    })
}
function searchPrintReports(obj, start, type, callback) {
    var search_value = $(obj).val();
    if ($(".printreport-list-wrapper").find(".load-app-request").length == 0) {
        $(".printreport-list-wrapper").append('<center><img src="/images/loader/loader.gif" class="load-app-request" style="display: inline;margin-top:30px;"></center>');
    }
    $.post("/ajax/search", {
        action: "getPrintReport",
        // search_value: search_value,
        // start:start
    }, function(data) {
        data = JSON.parse(data);
        var ret = "";
        if (data.length == 0) {
            ret += '<div class="notification_wrapper" style="height:auto">' +
                    '<div class="wrong" style="width:98.5%">' +
                    '<div class="padding_5">' +
                    '<img src="/images/warning/wrong.png" width="22" height="19" class="img-notification-position">' +
                    '<div class="notification-position">No Record Found</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
        }
        // $.each(data,function(key, val){
        var val = "";
        for (var i in data) {
            val = data[i];
            console.log(val)
            ret += '<div class="supTicket nobg">';
            ret += '<div class="issueType">';
            ret += '<span class="issueInfo"><i class="icon-bar-chart"></i>&nbsp;&nbsp;' + val['form_name'] + '</span>';
            ret += '<span class="issueNum">';
            ret += '<a href="/generate?view_type=update&formID=' + val.id + '&printID=' + val.form_id + '" class="tip cursor" data-original-title="Edit ' + val['form_name'] + '" data-placement="top" id="" data-orgchart-id="" style="margin-right:3px"><button class="btn-basicBtn padding_5 cursor"><i class="icon-edit" ></i></button></a>';
            ret += '<a class="tip cursor delete_printReport" data-original-title="Delete Print Report" data-placement="top" id="" data-id="' + val['id'] + '"><button class="btn-basicBtn padding_5 cursor"><i class="icon-trash"></i></button></a>';
            ret += '</span>';

            ret += '</div>';

            ret += '<div class="issueSummary">';
            ret += '<a href="#" title="" class="pull-left"><i style="height:30px; width:30px;font-size:30px;color: #676767;" class="icon-retweet"></i></a>';
            ret += '<div class="ticketInfo">';
            ret += '<ul>';
            ret += '<li><a href="#" title="">' + val['form_name'] + '</a></li>';
            ret += '<li class="even"><strong class="red">[ Print Report ]</strong></li>';
            ret += '<li>Status: <strong class="green">[ Active ]</strong></li>';
            ret += '<li class="even">' + val['date_created'] + '</li>';
            ret += '</ul>';
            ret += '</div>';
            ret += '</div>';
            ret += '</div>';
            ret += '<div class="hr"></div>';
        }
        // })
        if (type == "start") {
            $(".printreport-list-wrapper").html(ret);
            // ctr_load_workflow =10;
        } else {
            $(".printreport-list-wrapper").append(ret);
            // ctr_load_workflow+=10;
        }

        $(".tip").tooltip();
        $(".load-app-request").remove();
        // continueToLoad_workflow = true;
        // if(data.length==0){
        //     continueToLoad_workflow = false;
        // }
        callback()
    })
}

function returnEmptyRequest() {
    return '<div class="notification_wrapper" style="height:auto">' +
            '<div class="wrong" style="width:98.5%">' +
            '<div class="padding_5">' +
            '<img src="/images/warning/wrong.png" width="22" height="19" class="img-notification-position">' +
            '<div class="notification-position">No Record Found</div>' +
            '</div>' +
            '</div>' +
            '</div>';
}

RequestFilter = {
    "setConfirmation": function() {
        var ret = this.createModal();
        jDialog(ret, "", "", "200", "", function() {

        });
    },
    "showConfirmation": function() {
        var json = $("body").data();
        if (json['hidePermanentConf'] == 0 || json['hidePermanentConf'] == undefined) {
            $(".filterAction").slideDown();
            $(".cancelLoadFRWrapper").slideDown();
        }
        $("#loadRF").val(0);
    },
    "hideConfirmation": function() {
        var conf = "Do you want to remove this permanently?";
        var json = $("body").data();
        jConfirm(conf, 'Confirmation Dialog', '', '', '', function(r) {
            if (r) {
                json['hidePermanentConf'] = 1;
            } else {
                json['hidePermanentConf'] = 0;
            }
            $(".filterAction").slideUp();
        })
    },
    createModal: function() {
        var ret = '<h3 class="fl-margin-bottom"><i class="icon-print"></i> Filtered Request</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div id="comment-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; overflow: hidden;" class="ps-container">'
        ret += '<div class="content-dialog sub-container-comment requestComment" style="width: 100%;">';
        ret += '    <div class="pull-left" style="width: 95%">';
        ret += '        <div class="fields_below">';
        ret += '            <div class="label_below2">Filter Title: <font color="red">*</font></div>';
        ret += '        <div class="input_position_below" style="">';
        ret += '            <input type="text" name="" data-type="textbox" class="form-text filter_title"></div>';
        ret += '        </div>';
        ret += '    </div>';
        ret += '</div>';
        ret += '<div class="fields" style="">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += ' <input type="button" class="btn-blueBtn fl-margin-right" id="saveRF" value="Save">';
        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
        ret += '</div>';
        ret += '</div>';
        return ret;
    },
    loadRF: function(obj) {
        var search_value = $(obj).find("option:selected").attr("search_value");
        var search_field = $(obj).find("option:selected").attr("search_field");
        var date_field = $(obj).find("option:selected").attr("date_field");
        var date_from = $(obj).find("option:selected").attr("date_from");
        var date_to = $(obj).find("option:selected").attr("date_to");
        $(".search-request-field_filter_date_form").val(date_field);
        $(".search-request_date_from_form").val(date_from);
        $(".search-request_date_to_form").val(date_to);

        $(".search-request-value").val(search_value);
        $(".search-request-field").val(search_field);
        RequestViewSearch.getRequest(0, "start", function() {
            $(".filterAction").slideUp();
            $(".cancelLoadFRWrapper").slideDown();
        });
    },
    "saveRF": function() {
        var search_value = $(".search-request-value").val();
        var search_field = $(".search-request-field").val();
        var title = $(".filter_title").val();
        var form_id = $("#form_id").val();
        var date_field = $(".search-request-field_filter_date_form").val();
        var date_from = $(".search-request_date_from_form").val();
        var date_to = $(".search-request_date_to_form").val();
        //save Request Filter
        if (title == "") {
            showNotification({
                message: "Please indicate your filter title.",
                type: "error",
                autoClose: true,
                duration: 3
            });
            return;
        }
        $.post("/ajax/search",
                {
                    action: "saveRequestFilter",
                    search_value: search_value,
                    search_field: search_field,
                    title: title,
                    form_id: form_id,
                    date_field: date_field,
                    date_from: date_from,
                    date_to: date_to
                }
        , function(data) {
            showNotification({
                message: "Filter has been saved",
                type: "success",
                autoClose: true,
                duration: 3
            });
            $("#loadRF").append('<option value="' + data + '" search_value="' + search_value + '" search_field = "' + search_field + '" date_field = "' + date_field + '" date_from = "' + date_from + '" date_to = "' + date_to + '">' + title + '</option>');
            $("#popup_cancel").click();
            $(".filterAction").slideUp();
            // $(".cancelLoadFRWrapper").slideUp();
            $("#loadRF").val(0);
        })
    },
    "loadDefault": function() {
        $(".search-request-value").val("");
        $(".search-request-field").val(0);
        $("#loadRF").val(0);
        $(".search-request-field_filter_date_form").val("");
        $(".search-request_date_from_form").val("");
        $(".search-request_date_to_form").val("");
        RequestViewSearch.getRequest(0, "start", function() {
            $(".filterAction").slideUp();
            $(".cancelLoadFRWrapper").slideUp();
        });
    },
    "settingsModal": function() {
        var requestFilters = $("#loadRF").find("option");
        var ret = this.createSettingsModal(requestFilters);
        if (requestFilters.length == 1) {
            showNotification({
                message: "You do not have filtered search",
                type: "warning",
                autoClose: true,
                duration: 3
            });
            return;
        }
        jDialog(ret, "", "", "300", "", function() {

        });
    },
    createSettingsModal: function(requestFilters) {

        var text = "";
        var id = "";
        var ret = '<h3 class="fl-margin-bottom"><i class="icon-trash"></i> Remove Filter</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div id="comment-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; overflow: hidden;" class="ps-container">'
        ret += '<div class="content-dialog" style="width: 100%;height: 180px;">';
        ret += '<table style="width:100%;">';
        ret += '<thead>';
        ret += '<tr style="font-weight:bold;">';
        ret += '<td style="width:70%">Title</td>';
        ret += '<td>Action</td>';
        ret += '</tr>';
        ret += '</thead>';
        ret += '<tbody>';
        for (var i in requestFilters) {
            if (requestFilters[i]['value'] != 0 && requestFilters[i]['value'] != undefined) {
                text = requestFilters[i]['text'];
                id = requestFilters[i]['value'];
                ret += '<tr id="row_' + id + '">';
                ret += '<td style="width:70%">' + text + '</td>';
                ret += '<td>';
                ret += '<button class="tip btn-basicBtn padding_5 cursor icon-remove deleteFilter" data-id="' + id + '" style="margin-right:3px;" data-original-title="Delete"></button>';
                // ret += '<button class="tip btn-basicBtn padding_5 cursor icon-edit editFilter" data-id="'+ id +'" style="margin-right:3px;" data-original-title="Delete"></button>';
                ret += '</td>';
                ret += '</tr>';
            }
        }
        ret += '</tbody>';
        ret += '</table>';
        ret += '</div>';
        ret += '<div class="fields" style="">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
        ret += '</div>';
        ret += '</div>';
        return ret;
    },
    deleteFilter: function(id) {
        // var conf = "Do you want to remove this permanently?";
        // jConfirm(conf, 'Confirmation Dialog','', '', '', function(r){
        //     if(r){
        var action = "deleteFilter";
        $.post("/ajax/search",
                {
                    action: action,
                    id: id
                },
        function(data) {
            $("#row_" + id).fadeOut();
            $("#loadRF").find("option[value='" + id + "']").remove();
        })
        //     }
        // })
    },
    editFilter: function(id) {
        var action = "editFilter";
    }

}
AdvanceFilter = {
    showDialog: function() {
        var search_value = $(".search-request-value").val();
        var search_field = $(".search-request-field").val();
        var fields = $(".search-request-field").find("option");
        var fields_date = $(".search-request-field_date").find("option");
        var ret = '<h3 class="fl-margin-bottom"><i class="icon-cog"></i> Advance Filter</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div id="advanceFilterContainer" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; overflow: hidden;" class="ps-container">'
        ret += '<div class="content-dialog" style="height: 200px;">';
        ret += '        <div class="pull-left" style="width:47%">';
        ret += '        <div class="fields_below" style="">';
        ret += '        <div class="label_below2">Search Value: <font color="red">*</font></div>';
        ret += '        <div class="input_position_below">';
        ret += '            <input type="text" name="" data-type="textbox" class="form-text search-request-value_filter" value="">';
        ret += '        </div>';
        ret += '        </div>';
        ret += '        </div>';
        ret += '        <div class="pull-right" style="width:47%">';
        ret += '        <div class="fields_below">';
        ret += '        <div class="label_below2">Search Field: <font color="red">*</font></div>';
        ret += '        <div class="input_position_below">';
        ret += '            <select class="input-select search-request-field_filter" style="margin-top:0px;width: 100%;margin-right:0px;padding-top: 5px;">';
        for (var i in fields) {
            if (fields[i]['value']) {
                ret += '<option value="' + fields[i]['value'] + '">' + fields[i]['text'] + '</option>';
            }
        }
        ret += '            </select>';
        ret += '        </div>';
        ret += '        </div>';
        ret += '        </div>';


        ret += '<div class="pull-left" style="width:100%;margin-top: 15px;">';
        ret += '<h4><i class="icon-calendar"></i> Date Range</h4>';
        ret += '</div>';
        ret += '<div class="dateRangeWrapper">';
        ret += '        <div class="pull-left" style="width:47%">';
        ret += '        <div class="fields_below">';
        ret += '        <div class="label_below2">Search Field (Date): <font color="red">*</font></div>';
        ret += '        <div class="input_position_below">';
        ret += '            <select class="input-select search-request-field_filter_date" style="margin-top:0px;width: 100%;margin-right:0px;padding-top: 5px;">';
        ret += '<option value="0">-------Select-------</option>';
        for (var i in fields_date) {
            if (fields[i]['value']) {
                ret += '<option value="' + fields_date[i]['value'] + '">' + fields_date[i]['text'] + '</option>';
            }
        }
        ret += '            </select>';
        ret += '        </div>';
        ret += '        </div>';
        ret += '        </div>';
        ret += '        <div class="pull-right" style="width:47%;height:53px;border:1px solid transparent">';

        ret += '        </div>';
        ret += '        <div class="pull-left" style="width:47%">';
        ret += '        <div class="fields_below">';
        ret += '        <div class="label_below2">From: <font color="red">*</font></div>';
        ret += '        <div class="input_position_below">';
        ret += '            <input type="text" name="" data-type="textbox" class="form-text date_range search-request_date_from" value="">';
        ret += '        </div>';
        ret += '        </div>';
        ret += '        </div>';
        ret += '        <div class="pull-right" style="width:47%">';
        ret += '        <div class="fields_below">';
        ret += '        <div class="label_below2">To: <font color="red">*</font></div>';
        ret += '        <div class="input_position_below">';
        ret += '            <input type="text" name="" data-type="textbox" class="form-text date_range search-request_date_to" value="">';
        ret += '        </div>';
        ret += '        </div>';
        ret += '        </div>';
        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields" style="margin-top: 40px;">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += ' <input type="button" class="btn-blueBtn" id="advanceFilter" value="Filter">';
        ret += ' <input type="button" class="btn-basicBtn" id="clearValue" value="Clear">';
        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
        ret += '</div>';
        ret += '</div>';
        jDialog(ret, "", "400", "350", "", function() {

        });
        // $(".date_range").datetimepicker({
        //     dateFormat: 'yy-mm-dd'
        // });
        $(".search-request_date_from").datetimepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            onClose: function(selectedDate) {
                $(".search-request_date_to").datetimepicker("option", "minDate", selectedDate);
            }
        });
        $(".search-request_date_to").datetimepicker({
            defaultDate: "+1w",
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            onClose: function(selectedDate) {
                $(".search-request_date_from").datetimepicker("option", "maxDate", selectedDate);
            }
        });
        if (search_value != "") {
            $(".search-request-field_filter").val(search_field)
            $(".search-request-value_filter").val(search_value);
        }
    },
    filter: function() {
        var search_value = $(".search-request-value_filter").val();
        var search_field = $(".search-request-field_filter").val();
        var date_field = $(".search-request-field_filter_date").val();
        var date_from = $(".search-request_date_from").val();
        var date_to = $(".search-request_date_to").val();

        $(".search-request-value").val(search_value);
        $(".search-request-field").val(search_field);
        RequestViewSearch.getRequest(0, "start", function() {
            $("#popup_cancel").click();
            RequestFilter.showConfirmation();
            $(".search-request-field_filter_date_form").val(date_field);
            $(".search-request_date_from_form").val(date_from);
            $(".search-request_date_to_form").val(date_to);
        });
    },
    clearFilterFields: function() {
        $("#advanceFilterContainer").find("input[type='text'], select").each(function() {
            if ($(this).prop("tagName") == "INPUT") {
                $(this).val("")
            } else {
                $(this).val(0)
            }
        })
    }
}