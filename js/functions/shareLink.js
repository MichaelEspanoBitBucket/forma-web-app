//if($("#config_external_link").val() == 1){
var link_list = [];
var links_selected = [];

$(document).on('click', '.share-link', function(){
	// return false;
	if($("#config_external_link").val() == 1){
		
		ExternalLinksDialog.init(function(data){
			links_selected = [];
			shareLink.loadFormLinks();
			shareLinkDialog();
			setTimeout(function(){
				shareLink.loadSelectedLinks();
			}, 50);
		});
	}
});

$(document).on('click', '.setShareLinks', function(){
	//alert("alert");
	var check_error  = advanceFilterValidation($('.field_container'));
	if(check_error == false){
		var body_data = $("body").data();
		body_data['external_link'] = links_selected;
		$('#popup_cancel').trigger('click');
	}
	else{
		showNotification({
             message: 'Please fill up the required fields.',
             type: "error",
             autoClose: true,
             duration: 3
        });
	}
});

$(document).on("click", ".ext-btn", function(e){
	//$(this).find(".sublist").css("display", "block");
		$(this).find(".sublist").slideToggle('fast');
	//	alert("yeah!");
});

$(document).on("click", '.picked-related-info', function(e){
	var check_error  = advanceFilterValidation($('.field_container'));
	if(check_error == false){
		var wtf    = $('.content-dialog-scroll');
		wtf.css('transition', '0.5s');
		var height = wtf[0].scrollHeight;
		wtf.scrollTop(height);
		$('.show_advance_opt').empty();
		$('.put_selected_field').text("(" + $(this).attr('name') + ")");
		$('[selected="selected"]').css({'cursor':'pointer','background-color':'','color': ''});
		$('[selected="selected"]').removeAttr('selected');
		$(this).css({'cursor':'pointer','background-color':'#989898','color': '#EEEEEE'});
		$(this).attr('selected',"selected");
		shareLink.showAdvanceOptions($('.getFields'), $(this).attr('name'), $(this).attr('id'), $('.show_advance_opt'));
		for(i in links_selected){
			if(links_selected[i].id == $(this).attr('id')){
				if($.type(links_selected[i]['advance_option']) != 'undefined'){
					$('.adv_form_filter').attr('data-form-name', $(this).attr('name'));
					$('.adv_form_filter').attr('data-form-id', $(this).attr('id'));
					$('.adv_form_filter').prop('checked', true);
					$('.adv_controls').removeAttr('disabled');
				}
				else{
					$('.adv_form_filter').attr('data-form-name', $(this).attr('name'));
					$('.adv_form_filter').attr('data-form-id', $(this).attr('id'));
					$('.adv_form_filter').prop('checked', false);
					$('.show_advance_opt').empty();
					$('.adv_controls').attr('disabled', 'disabled');
				}
			}
		}
		$('.adv_form_filter').removeAttr('disabled');
	}
	else{
		showNotification({
             message: 'Please fill up the required fields.',
             type: "error",
             autoClose: true,
             duration: 3
        });
	}
});
$(document).on('mousedown', '.child_minus', function(e){
	var index = $(this).index('.child_minus');
	AsyncLoop(links_selected, function(a, b){
		if(b['id'] == $('.child_minus').parent().parent().attr('data-form-id')){
			if($.type(b['advance_option']) != "undefined"){
				b['advance_option'][index] = undefined;
				var new_advance_options = [];
				var ctr = 0;
				for(i in b['advance_option']){
					if(!isNaN(i)){
						if($.type(b['advance_option'][i]) != "undefined"){
							new_advance_options.push(b['advance_option'][i]);
							// new_advance_options[ctr] = b['advance_option'][i];
							ctr += 1;
						}
					}
				}
				b['advance_option'] = new_advance_options;
			}
		}
	});

})

$(document).on('click', '.adv_form_filter', function(e){
	if($(this).prop('checked') == true){
		try{
			shareLink.showAdvanceOptions($('.getFields'), $(this).attr('data-form-name'), $(this).attr('data-form-id'), $('.show_advance_opt'));
			$('.adv_controls').removeAttr('disabled');
		}catch(e){
			showNotification({
                 message: 'Please choose item/s in Selected Forms.',
                 type: "warning",
                 autoClose: true,
                 duration: 3
            });
		}

	}
	else{
		for(i in links_selected){
			if(links_selected[i].id == $(this).attr('data-form-id')){
				if($.type(links_selected[i]['advance_option']) != 'undefined'){
					links_selected[i]['advance_option'] = undefined;
				}
			}
		}
		$('.adv_controls').attr('disabled', 'disabled');
		$('.show_advance_opt').empty();
	}
});

$(document).on('change', '.get_child_select', function(e){
	for(i in links_selected){
		if(links_selected[i].id == $(this).attr('data-form-id')){
			if($.type(links_selected[i]['advance_option']) != "undefined"){
				if($.type(links_selected[i]['advance_option'][$(this).index('.get_child_select')]) != "undefined"){
					links_selected[i]['advance_option'][$(this).index('.get_child_select')]['child_field'] = $(this).val();
				}
				else{
					var filter = {
						"parent_field": "",
						"child_field": $(this).val(),
						"condition":"",
						"operator":""
					}
					links_selected[i]['advance_option'][$(this).index('.get_child_select')] = filter;
				}
			}
			else{
				links_selected[i]['advance_option'] = {};
				var filter = {
					"parent_field": "",
					"child_field": $(this).val(),
					"condition":"",
					"operator":""
				}
				links_selected[i]['advance_option'][$(this).index('.get_child_select')] = filter;

			}
		}
	}
	advanceFilterValidation($('.field_container'));
})
$(document).on('change', '.get_parent_select', function(e){
	for(i in links_selected){
		if(links_selected[i].id == $(this).attr('data-form-id')){
			if($.type(links_selected[i]['advance_option']) != "undefined"){
				if($.type(links_selected[i]['advance_option'][$(this).index('.get_parent_select')]) != "undefined"){
					links_selected[i]['advance_option'][$(this).index('.get_parent_select')]['parent_field'] = $(this).val();
				}
				else{
					var filter = {
						"parent_field": $(this).val(),
						"child_field": "",
						"condition": "",
						"operator": ""
					}
					links_selected[i]['advance_option'][$(this).index('.get_parent_select')] = filter;
				}
			}
			else{
				links_selected[i]['advance_option'] = {};
				var filter = {
					"parent_field": $(this).val(),
					"child_field": "",
					"condition": "",
					"operator": ""
				}
				links_selected[i]['advance_option'][$(this).index('.get_parent_select')] = filter;
			}
		}
	}
	advanceFilterValidation($('.field_container'));
});
$(document).on('change', '.get_operator', function(e){
	console.log('get_operator', $(this).index('.get_operator'));
	for(i in links_selected){
		if(links_selected[i].id == $(this).attr('data-form-id')){
			if($.type(links_selected[i]['advance_option']) != "undefined"){
				if($.type(links_selected[i]['advance_option'][$(this).index('.get_operator')]) != "undefined"){
					links_selected[i]['advance_option'][$(this).index('.get_operator')]['operator'] = $(this).val();
				}
				else{
					var filter = {
						"parent_field": "",
						"child_field": "",
						"condition": "",
						"operator": $(this).val()
					}
					links_selected[i]['advance_option'][$(this).index('.get_operator')] = filter;
				}
			}
			else{
				links_selected[i]['advance_option'] = {};
				var filter = {
					"parent_field": "",
					"child_field": "",
					"condition": "",
					"operator": $(this).val()
				}
				links_selected[i]['advance_option'][$(this).index('.get_operator')] = filter;
			}
		}
	}
	advanceFilterValidation($('.field_container'));
})


$(document).on('change', '.get_filter_condition', function(e){
	for(i in links_selected){
		if(links_selected[i].id == $(this).attr('data-form-id')){
			if($.type(links_selected[i]['advance_option']) != "undefined"){
				if($.type(links_selected[i]['advance_option'][$(this).index('.get_filter_condition')]) != "undefined"){
					links_selected[i]['advance_option'][$(this).index('.get_filter_condition')]['condition'] = $(this).val();
				}
				else{
					var filter = {
						"parent_field": "",
						"child_field": "",
						"condition": $(this).val(),
						"operator": ""
					}
					links_selected[i]['advance_option'][$(this).index('.get_filter_condition')] = filter;
				}
			}
			else{
				links_selected[i]['advance_option'] = {};
				var filter = {
					"parent_field": "",
					"child_field": "",
					"condition": $(this).val(),
					"operator": ""
				}
				links_selected[i]['advance_option'][$(this).index('.get_filter_condition')] = filter;
			}
		}
	}
	// advanceFilterValidation($('.field_container'));	
})
// $('body').on('mouseup', '.save_form_workspace', function(e){
// 	alert("boomskie");
// });

$(document).on('click', '.add-link-category', function(){
	if($(this).prop("checked") == true){
		$(this).closest('ul').find('.add-link').each(function(index){
			if($(this).prop("checked") == false){
				$(this).trigger('click');
			}
		});
	}
	else{
		$(this).closest('ul').find('.add-link').each(function(index){
			if($(this).prop("checked") == true){
				$(this).trigger('click');
			}
		});	
	}
});

$(document).on('click', '.ext-links', function(e){
	var new_return_fields = [];
	// // var return_fields = '[';
	// // var ret_fields = '';
	e.preventDefault();
	var get_ext_links = $('body').data('user_form_json_data')['form_json']['external_link'];
	var get_adv_options = undefined;
	for(i in get_ext_links){
		console.log("id", get_ext_links[i]['id'] , "related form id", $(this).attr('form-id'));
		if(get_ext_links[i]['id'] == $(this).attr('form-id')){
			console.log("same id");
			if($.type(get_ext_links[i]['advance_option']) != "undefined"){
				get_adv_options = get_ext_links[i]['advance_option']
				break;
			}
		}
	}
	get_adv_options = (adjust(get_adv_options, 'condition')||[]).reverse();
	if($.type(get_adv_options) != "undefined"){
		get_adv_options.reverse();
		for(i in get_adv_options){
			var value = $('[name="' + get_adv_options[i]['parent_field'] + '"]');
			var value_text = '';
			if(value.is('input[type="checkbox"]')){
			    value_text = value.filter(":checked").map(function(){return $(this).val()||null;}).get().join("|^|");
			    // fields[value.attr('name')] = val;
			}else if(value.is('select[multiple]')){
			    value_text = value.children("option").map(function(){return $(this).filter(":selected").val()||null;}).get().join("|^|");
			    // fields[value.attr('name')] = val;
			}
			else{
				value_text = value.val();
			}
			if(value.attr('data-input-type') == "Currency"){
				value_text = value_text.replace(/\,/g,'');
			}
			console.log(value_text, get_adv_options[i]['parent_field']);
			 new_return_fields.push({
				'form_id': $(this).attr('form-id'),
				'field_name': get_adv_options[i]['child_field'],
				'value': value_text,
				'operator': get_adv_options[i]['operator'],
				'condition': get_adv_options[i]['condition']
	    	});
		}
	}
	var adv_related_info = encodeURIComponent(JSON.stringify(new_return_fields));
	// return_fields = return_fields.replace(/\}\{/g, '},{');
	// return_fields += ']';
	// console.log("old return fields:", return_fields);
	// console.log("adv_related_info", adv_related_info);
	var link = $(this).find('a').attr('href');
	//return fields format return_fields = [{"form_id":"", "field_name":"","value":"","operator":""},{"form_id":"", "field_name":"","value":"","operator":""}]
	if(link.indexOf('return_fields') < 0){
		//link += '&mode=1&return_fields=' + return_fields;
		link += '&mode=1&return_fields=' + adv_related_info;
	}
	window.open(link);
	// var uriEncode = encodeURIComponent(link);
	// alert(link);
	// $(this).find('a').attr('href', link);
});
$(document).on('click', '.form_link_select_all', function(){
	if($(this).prop('checked') == true){
		$('.form-list').find('.add-link').each(function(){
			var link_name = $(this).val();
			if($(this).prop('checked') == false){
				$(this).prop('checked', true);
				shareLink.selectExernalLink(link_name);
				shareLink.sort_form_names(links_selected, '.links-shared');
			}
		});
	}
	else{
		$('.form-list').find('.add-link').each(function(){
			var link_name = $(this).val();
			$(this).prop('checked', false);
			shareLink.removeExternalLink(link_name);
		});
	}
});

$(document).on('keyup', '#form_link_search', function(){
	shareLink.searchRelatedLinks($.trim($(this).val()));
	console.log('changed');
})

$(document).on('mouseover', '._hover_', function(){
	$(this).css('cursor', 'pointer');
})

$(document).on('mouseover', '.picked-related-info', function(){
	if(!$(this).attr('selected')){
		$(this).css({'cursor':'pointer','background-color':'#B8B8B8','color': ''});
	}
	else{
		$(this).css({'cursor':'pointer','background-color':'#989898', 'color': '#EEEEEE'});
	}
	
})

$(document).on('mouseout', '.picked-related-info', function(){
	if(!$(this).attr('selected')){
		$(this).css({'cursor':'pointer','background-color':''});
	}
})



$(document).on('click', '._hover_', function(e){
	if($(e.target)[0] == $(this)[0]){
		if($(this).find('.form_link_select_all').length > 0){
			$(this).find('.form_link_select_all').trigger('click');
		}
		else{
			$(this).find('.add-link').trigger('click');
			shareLink.sort_form_names(links_selected, '.links-shared');
		}
	}
})

$(document).on('click', '.enable_advance_option', function(e){
	if($(e.target)[0] == $(this)[0]){
		if($(this).find('.enable_advance_option').length > 0){
			$(this).find('.enable_advance_option').trigger('click');
		}
		else{
			$(this).find('.adv_form_filter').trigger('click');
		}
	}
})

$(document).on('click', '.add-link', function(){
	var link_name = $(this).val();
	if($(this).prop("checked") == true){
		shareLink.selectExernalLink(link_name);
		shareLink.sort_form_names(links_selected, '.links-shared');
		
	}
	else{
		shareLink.removeExternalLink(link_name);
		if($('.add-link-category').prop("checked") == true){
			$(this).prop("checked", false);
			shareLink.selectExernalLink(link_name);
			shareLink.sort_form_names(links_selected, '.links-shared');
		}
	}
	if($('.add-link:checked').length == $('.add-link').length){
		$('.form_link_select_all').prop('checked', true);
	}
	else{
		$('.form_link_select_all').prop('checked', false);
	}
});

function shareLinkDialog(){

	var newDialog = new jDialog(ExternalLinksDialog.setOldDialog('Related Information', 'share-link', '', '', 'faiconColor', '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-related-info"></use></svg>'), "", "740", "", "40", function() {
		
    });

	newDialog.themeDialog("modal2");
}


ExternalLinksDialog = {
	"init" : function(callback){
		callback();

	},
	"setOldDialog": function(title, type, note, icon, faiconColor, svgIcon){

		var layout = '<div><h3 class="fl-margin-bottom">';
		layout += svgIcon + '<span>'+title+'</span>'; //'<i class=" '+ icon +' '+ faiconColor +' icon-save"></i> ' + title;
	    layout += '</h3></div>';
	    layout += '<div class="hr"></div>';
	    //layout +='<div style="margin:2px 0px 0px 2px;font-size:11px;color:red">' + note + '</div>';
	    layout += '<div class="content-dialog" id="related_info_content" style="height: 300px;">';
	    //left panel
	    layout += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
	    	layout += '<div class="properties width container">';
	    		layout += '<div>';
	    		layout += '<div class="field below section clearing fl-field-style">';
					layout += '<div class="input position below column div_1_of_1">';
						layout += '<span class="font-bold">Forms</span>';
						layout += '<input type="text" id="form_link_search" class="form-text" placeholder="Search Related form..." />';
					layout += '</div>';
				layout += '</div>';
	   		layout += '</div>';
	    	layout += '<ul class="drag-handle section clearing fl-field-style">';
	    		layout += '<div class="field below column div_1_of_1" disabled>';
					layout += '<div class="input position below _hover_">';
						layout += '<label><input type="checkbox" id="form_link_select_all" class="form_link_select_all css-checkbox" value="Select All" />';
						layout += '<label for="form_link_select_all" class="css-label"></label>';
						layout += '<span class="font-bold">Select All</span></label>';
					layout += '</div>';
				layout += '</div>';
			layout += '</ul>';
	    	layout += '<div class="section clearing fl-field-style" style="height: 200px; width: auto; overflow: auto; ">';
	    
	    	layout += '<ul class="fl-margin-top form-list column div_1_of_1" style="overflow: auto">';
	    var ctr = 1;
    	for(h in link_list){
	    		layout += '<li class="drag-handle check_link" style="margin-bottom: 2px; ">';
		    		layout += '<div class="fields below">';
		    			layout += '<div class="label below2"></div>';
		    			layout += '<div class="input position below fl-table-ellip _hover_">';
		    				layout += '<input type="checkbox" id="form_link_check' + link_list[h].id + '" class="add-link css-checkbox" value="' + link_list[h].form_name + '" />';
		    				layout += '<label for="form_link_check' + link_list[h].id + '" class="css-label"></label>';
		    				layout += link_list[h].form_name;
		    			layout += '</div>';
		    		layout += '</div>';
		    	layout += '</li>';
    	}
	    //-----------------
	    	layout += '</ul>';
	    layout += '</div>';
	    layout += '</div>';
	    layout += '</div>';
	    //right panel
	    layout += '<div class="fl-floatRight fl-form-prop-cont-wid">';
			layout += '<div class="properties width container section clearing fl-field-style">';
			layout += '<div class="column div_1_of_1"><span class="font-bold">Selected Forms</span>';
				layout += '<div style="height: 275px; width: auto; overflow: auto; margin-right: 5px;">';
					layout += '<ul class="fl-margin-top links-shared">';
					layout += '</ul>';
				layout += '</div>';
			layout += '</div>';
	    layout += '</div>';
	    	
	    layout += '</div>';
	    layout += '<div>';
	    	layout += '<div <div class="properties width container section clearing fl-field-style">';
	    		
	    		layout += '<div class="field below">';
					layout += '<div class="label below2"></div>';
					layout += '<div class="input position column div_1_of_1 below _hover_ enable_advance_option">';
						layout += '<span class="font-bold">Advanced Options <span class="put_selected_field"></span></span><br/>';
						layout += '<label><input disabled type="checkbox" id="adv_form_filter" class="adv_form_filter css-checkbox" value="Select All" />';
						layout += '<label for="adv_form_filter" class="css-label"></label>';
						layout += '<span class="font-bold">Enable</span></label>';
					layout += '</div>';
				layout += '</div>';
				layout += '<div class="show_advance_opt">';
				layout += '</div>';
			layout += '</div>';
	    layout += '<input type="button" class="btn-blueBtn fl-margin-right setShareLinks fl-positive-btn" value="OK"/>';
	    layout += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel" style="display:none;"/>';
	    layout += '</div>';
	    return layout;
	}
}

shareLink = {
	"loadSelectedLinks": function(){
		var body_data = $("body").data();
		var ext_link = body_data['external_link'];
		for(i in ext_link){
			var id = '#form_link_check' + ext_link[i].id;
			$(id).prop('checked', true);
			if($.type(ext_link[i]['advance_option']) != "undefined"){
				shareLink.selectExernalLink(ext_link[i].form_name, ext_link[i]['advance_option']);
			}
			else{
				shareLink.selectExernalLink(ext_link[i].form_name);
			}
			
		}
		if($('.add-link:checked').length == $('.add-link').length){
			$('.form_link_select_all').prop('checked', true);
		}
		else{
			$('.form_link_select_all').prop('checked', false);
		}
	},
	"loadFormLinks": function(){
		var json_forms = JSON.parse($('.user-forms').text());
		$.extend(link_list, json_forms);

		link_list = link_list.filter(function(a,b){
			return a['form_id']!=$("#FormId").val();
		})
		
		for(h in link_list){
			link_list[h].id = link_list[h].form_id;
			link_list[h].formlink = "/user_view/workspace?view_type=request&formID=" + link_list[h].id + "&requestID=0";
		}
		link_list.sort(shareLink.SortByName);
	},
	"loadLinkProperties":function(form_id){
		var layout = '<div class="fields form_selection">';
		layout += '<span class="fl-margin-bottom">*Fill-up the field label of the fields you would like to appear in the column header.';
		layout += ' <br/>';
		layout += ' *Drag the field name up or down to rearrange.</span>';
		layout += '<div class="input_position display_columns" style="overflow:auto;padding:5px;border:1px Solid #ccc;height:200px;">';
		layout += '</div>';
		layout += '</div>';
		return layout;
	},
	"loadRelatedInfo": function(title, type, form_id, icon, faiconColor){
		var layout = '<h3 class="fl-margin-bottom">';
		layout += '<i class=" '+ icon +' '+ faiconColor +' icon-save"></i> ' + title;
	    layout += '</h3>';
	    layout += '<div class="hr"></div>';
	    //layout +='<div style="margin:2px 0px 0px 2px;font-size:11px;color:red">' + form_id + '</div>';
	    layout += '<div class="content-dialog" style="height: 300px;">';
	    //left panel
	    layout += '<div class="fl-floatLeft" style="width: 45 %; height: 100ss%;">';
	    layout += '<div class="properties width container">';
	    layout += '<h3 class="fl-margin-top  fl-margin-bottom"></h3>';
	    layout += '<div>';
		    layout += '<table class="display_data dataTable relatedInfoList">';
				layout += '<thead>';
			        layout += '<tr>';
			        layout += '<th>Head 1</th>';
			        layout += '<th>Head 2</th>';
			        layout += '<th>Head 3</th>';
			        layout += '</tr>';
			    layout += '</thead>';
			    layout += '<tbody>';
				layout += '</tbody>'
			layout += '</table>'
	    layout += '</div>';
	   	layout += '</div>';
	   	layout += '</div>';
	   	layout += '</div>';
	   	return layout;
	},
	"formatSelectedLinks": function(obj){
		var formatted_selected_links = [];
		for(i in obj){
			if(typeof(obj[i].form_name) != "undefined"){
				formatted_selected_links.push(obj[i]);
			}
		}
		 return formatted_selected_links;
	},
	"SortByName": function(a, b){
		var aName = a.form_name.toLowerCase();
		var bName = b.form_name.toLowerCase(); 
		return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
	},
	"selectExernalLink": function(link_name, adv_opt){
		for(h in link_list){
			//for(i in link_list[h].categorylist){
				if(link_list[h].form_name ==  link_name){
					var selected_link = {};
					selected_link.id = link_list[h].id;
					selected_link.form_name = link_list[h].form_name;
					selected_link.form_table_name = link_list[h].form_table_name;
					selected_link.formlink = link_list[h].formlink;
					if($.type(adv_opt) != "undefined"){
						selected_link.advance_option = adv_opt;
					}
					else{
					}
					links_selected.push(selected_link);
					link_name = "<li style='cursor:pointer;' class='drag-handle picked-related-info' id='" + link_list[h].id + "' name='" + link_list[h].form_name + "'><div class='fl-table-ellip'><span style='overflow: hidden; display: block; text-overflow: ellipsis;'>" + link_list[h].form_name +"</span></div></li>";
					$('.links-shared').append(link_name);
				}
			//}
		}
	},
	"removeExternalLink": function(link_name){
		for(i in links_selected){
			//console.log("links_selected", links_selected[i].formname + ", " + link_name);
			if(links_selected[i].form_name == link_name){
				delete links_selected[i];
					$('.links-shared').find("li").each(function(){
					if($(this).attr("name") == link_name){
						$(this).remove();
					}
				});
			}
		}
		links_selected = shareLink.formatSelectedLinks(links_selected);
	},
	"searchRelatedLinks": function(key){
		key = key.toLowerCase();
		$('.check_link').each(function(){
			$(this).show();
			if($(this).find('.add-link').val().toLowerCase().indexOf(key) < 0){
				$(this).css('display', 'none');
			}
		})
	},
	"showAdvanceOptions": function(fields, selected_form_name, selected_form_id, selector_to_append){
		var layout = '<table cellspacing="10" style="width: 99%; margin-bottom: 5%;margin-left: 5px; margin-right: 5px;" class="adv_controls">';
		layout += '<thead><tr><th class="isDisplayNone"><div class="label_below2 fl-table-ellip">Filter Condition: <font color="red"></font></div></th><th><div class="label_below2 fl-table-ellip">Current Form Field: <font color="red"></font></div></th><th><div class="label_below2 fl-table-ellip">Operator: <font color="red"></font></div></th><th><div class="label_below2 fl-table-ellip">Selected Form Field: <font color="red"></font></div></th></tr></thead>';
		layout += '<tbody class="field_container"><tr class="child_fields" style="margin-bottom:5px;" data-form-name="' + selected_form_name + '"  data-form-id="' + selected_form_id + '">';
		layout += '<td class="isDisplayNone"><select style="margin-right:5px; margin-top:5px; display:none; width:90%;" class="form-select get_filter_condition" data-form-name="' + selected_form_name + '"  data-form-id="' + selected_form_id + '">';
		layout += '<option value="">Condition...</option>';
		layout += '<option value="AND" selected="selected">AND</option>';
		layout += '<option value="OR">OR</option>';
		layout += '</select></td>'
		layout += '<td><select style="margin-right:5px; margin-top:5px; width:90%;" class="form-select get_parent_select" data-form-name="' + selected_form_name + '"  data-form-id="' + selected_form_id + '">';
		layout += '<option value="">Select a Field...</option>';
		layout += '</select></td>';
		layout += '<td><select style="margin-right:5px; margin-top:5px; width:90%;" class="form-select get_operator" data-form-name="' + selected_form_name + '"  data-form-id="' + selected_form_id + '">';
		layout += '<option value="">Operator...</option>';
		layout += '<option value="=">=</option>';
		layout += '<option value="!=">!=</option>';
		layout += '</td>';
		layout += '<td><select style="margin-right:5px; margin-top:5px; width:90%;"; class="form-select get_child_select" data-form-name="' + selected_form_name + '"  data-form-id="' + selected_form_id + '">';
		layout += '<option  value="">Select a Field...</option>';
		layout += '</select></td>';
		layout += '<td><div class="fa fa-plus  child_plus" style="float:left;margin-right:5px;"></div><div class="fa fa-minus child_minus" style="display:none; float:left;"></div></td>';
		layout += '</tr>';
		layout += '</tbody>'
		layout += '</table>';
		$(selector_to_append).append(layout);
		$('.get_parent_select').append(putFieldNamesInSelect($(fields)));
		getChildFormFields(selected_form_name, selected_form_id);
	},
	"sort_form_names": function(form_links, selector_str){
		form_links.sort(shareLink.SortByName);
		link_name = "";
		for(i in form_links){
			link_name += "<li style='cursor:pointer;' class='drag-handle picked-related-info' id='" + form_links[i].id + "' name='" + form_links[i].form_name + "'><div class='fl-table-ellip'><span style='overflow: hidden; display: block; text-overflow: ellipsis;'>" + form_links[i].form_name +"</span></div></li>";
		}
		$(selector_str).empty();
		$(selector_str).append(link_name);
	}
};


function putFieldNamesInSelect(fields){
	var get_fields = $(fields).map(function(){
		return $(this).attr("name");
	}).get();
	get_fields = $.unique(get_fields).filter(Boolean);
	get_fields.sort(function (a, b) {
    	return a.toLowerCase().localeCompare(b.toLowerCase());
	});
	var layout = '';
	for(i in get_fields){
		var display_fields = get_fields[i];
		if(display_fields.match(/\[\]/g)){
			display_fields = display_fields.replace(/\[\]/g, '');
		}
		layout += '<option value="' + get_fields[i] + '">' + display_fields + '</option>';
	}
	return layout;
}

function getChildFormFields(selected_form_name, selected_form_id){
	if($.type(selected_form_id) != "undefined" && $.type(selected_form_name) != "undefined"){
		var json_params = {
			"form_id": selected_form_id,
			"form_name": selected_form_name
		}
		$.post('/ajax/get_form_fields', json_params, function(result_echoes){
			var get_json = JSON.parse(result_echoes);
			get_json = JSON.parse(get_json[0]['form_json']);
			var field_arr = $.map(JSON.parse(get_json['form_fields']), function(val, index){
				if(!isNaN(index)){
					if($.type(val['fieldName']) != "undefined"){
						return val['fieldName'];
					}
				}
			});
			var layout = '';
			field_arr.sort(function (a, b) {
		    	return a.toLowerCase().localeCompare(b.toLowerCase());
			});
			field_arr = unique(field_arr);

			if(field_arr.length > 0){
				for(i in field_arr){
					if(field_arr[i] != ""){
						layout += '<option value="' + field_arr[i] + '">' + field_arr[i] + '</option>';
					}
				}
			}
			else{
				showNotification({
	                message: 'No field/s found.',
	                type: "information",
	                autoClose: true,
	                duration: 3
	            });
			}
			console.log("layout", layout);
			$('.get_child_select').append(layout);
			MakePlusPlusRowV2({
			    "tableElement": '.field_container',
			    "rules":[{
			        "eleMinus":".child_minus",
			        "eleMinusTarget":".child_fields",
			        "elePlus":".child_plus",
			        "elePlusTarget":".child_fields",
			        "elePlusTargetFn":function(e,data){
		            	//data-form-id

			            var self = $(this);
			            // console.log("self", self, self.index())
			   			$(this).find('select:data("tooltip")').removeData("tooltip").tooltip(); //.tooltip('destroy');
			            if(self.closest('.field_container').find('.child_fields').length > 1){
			                self.closest('.child_fields').find('.child_minus').show();
			                self.closest('.child_fields').find('.get_filter_condition').show();
			            }
			            self.closest('.content-dialog-scroll').perfectScrollbar("update");
						// console.log('data', data['advance_option']);
						if($.type(e.originalEvent) == "object"){
							for(var ctr in links_selected){
								if(links_selected[ctr]['id'] == self.attr("data-form-id")){
									links_selected[ctr]['advance_option'].splice( self.index(), 0, {});
								}
							}
						}
						self.find('.get_filter_condition').trigger('change');
						
			        },
			        "eleMinusTargetFn": function(e,data){
			        	var self = $(this).find('.child_minus');	
			        	formatAdvanceOption(links_selected, selected_form_id, self.index('.child_minus'));

			   //      	if($.type(e.originalEvent) == "object"){
						// 	for(var ctr in links_selected){
						// 		if(links_selected[ctr]['id'] == self.attr("data-form-id")){
						// 			links_selected[ctr]['advance_option'].splice( self.index(), 0, {});
						// 		}
						// 	}
						// }


						
						// console.log("WHAT",links_selected,e,data,this)
						// var self2 = "";
						// if(data.prev_ele.length >= 1){
						// 	self2 = data.prev_ele;
						// 	for(var ctr in links_selected){
						// 		if(links_selected[ctr]['id'] == self2.attr("data-form-id")){
						// 			console.log(links_selected[ctr])
						// 			links_selected[ctr]['advance_option'].splice( self2.index() + 1, 1 );
						// 		}
						// 	}
						// }else if(data.next_ele.length >= 1){
						// 	self2 = data.next_ele;
						// 	for(var ctr in links_selected){
						// 		if(links_selected[ctr]['id'] == self2.attr("data-form-id")){
						// 			links_selected[ctr]['advance_option'].splice( self2.index() - 1, 1 );
						// 		}
						// 	}
						// }
			        }
			    }]
			});
			getAdvanceOptions(selected_form_name, links_selected);
		})
	}
	else{
		showNotification({
            message: 'Please Choose item/s in Selected Forms',
            type: "warning",
            autoClose: true,
            duration: 3
        });
	}
}

function advanceFilterValidation(tr){
	var is_empty = 0;
	$(tr).each(function(){
		if($(this).find('select').length > 0){
			$(this).find('select').each(function(){
				if($(this).is(':visible')){
					if($(this).val() == ""){
						$(this).css('background-color', 'rgb(255,125,125)');
						$(this).attr('data-original-title', 'This field is required');
						$(this).addClass('tip');
						$(this).tooltip();
						is_empty++;
					}
					else{
						$(this).css('background-color', '');
						$(this).removeClass('tip');
						$(this).removeAttr('data-original-title');
						$(this).tooltip('destroy');
					}
				}
			});
		}
		else{
			return false;
		}
	});
	if(is_empty > 0){
		return true;
	}
	else{
		return false;
	}
}

function getAdvanceOptions(selected_form_name, link_list){
	var ext_links = link_list;
	var get_adv_opt = ext_links.filter(function(val, index){
		return val['form_name'] == selected_form_name;
	})[0]['advance_option'];
	// for(i in ext_links){
	// 	if(selected_form_name == ext_links[i].form_name){
	// 		get_adv_opt = ext_links[i]['advance_option'];
	// 		break;
	// 	}
	// }
	console.log("check this", get_adv_opt);
	if($.type(get_adv_opt) != "undefined"){
		for(var i in get_adv_opt){
			if(!isNaN(i)){
				if(parseInt(i) < 1){
					$('.child_fields').eq(i).find('.get_child_select').val(get_adv_opt[i]['child_field']);
					$('.child_fields').eq(i).find('.get_parent_select').val(get_adv_opt[i]['parent_field']);
					$('.child_fields').eq(i).find('.get_operator').val(get_adv_opt[i]['operator']);
					$('.child_fields').eq(i).find('.get_filter_condition').val(get_adv_opt[i]['condition']);
				}
				else{
					$('.child_fields').eq(i-1).find('.child_plus').trigger('click');
					$('.child_fields').eq(i).find('.get_child_select').val(get_adv_opt[i]['child_field']);
					$('.child_fields').eq(i).find('.get_parent_select').val(get_adv_opt[i]['parent_field']);
					$('.child_fields').eq(i).find('.get_operator').val(get_adv_opt[i]['operator']);
					$('.child_fields').eq(i).find('.get_filter_condition').val(get_adv_opt[i]['condition']);
				}
			}
		}
	}
}

function formatAdvanceOption(link_list, form_id, index){
	console.log("link_list", link_list, "form_id", form_id, "index", index);
	var get_adv_option = undefined;
	for(i in link_list){
		if(link_list[i] == form_id){
			get_adv_option = link_list[i]['advance_option'];
		}
	}
	if($.type(get_adv_option) != "undefined"){
		for(i in get_adv_option){
			console.log("key", i, "value", get_adv_option[i]);
		}
	}
}

function adjust(json, str_json_key){
	var new_json = [];
	for(i in json){
		if(i > 0){
			json[i-1][str_json_key] = json[i][str_json_key];
			json[i][str_json_key] = '';
		}
	}
	return json;
}

function unique(array) {
    return $.grep(array, function(el, index) {
        return index == $.inArray(el, array);
    });
}