/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {

    var socketServer = '';
    var socketActive = 0;
    var currentUserId;
    var socket;
    $(document).ready(function () {
        
//  get socket configuration
        socketActive = $('#current-user-info').attr('data-socket-active');
        socketServer = $('#current-user-info').attr('data-socket-server');
        currentUserId = $('#current-user-id').html();
        //  initialize basic user data
        var currentUserData = {
            id: $('#current-user-id').html(),
            companyId: $('#current-user-company-id').html(),
            displayName: $('#current-user-display-name').html(),
            firstName: $('#current-user-first-name').html(),
            lastName: $('#current-user-last-name').html(),
            profileLink: $('#current-user-profile-link').html(),
            image: $('#current-user-profile-image').html()
        };
        //  Subscription addresses are used for broadcasting events. Whenever an
        //  event is broadcasted to a certain address, any user registered to that
        //  address will receive that event. This is used to filter out what events
        //  the user will receive, thus improving the performance of the socket service.
        currentUserData.subscriptionAddresses = [
            //  general functions currently include notifications and messaging
            'general_functions_' + currentUserData.companyId
        ];
        //  Search if there are any reports in the dashboard. For every dashboard
        //  found, add a subscription address.
        $('.fl-report-wrapper-content').each(function () {
            currentUserData.subscriptionAddresses.push('live_reports_' + $(this).attr('fl-data-form-id'));
        });
        //  ====================================================================
        //  --- special publish addresses ---

        //  whenever the "publishAddress" is not specified, the data will be
        //  broadcasted to the "defaultPublishAddress"
        currentUserData.defaultPublishAddress = 'general_functions_' + currentUserData.companyId;
        //  disconnection publish address will be the address where disconnection
        //  events from the user will be published. If not specified, this event
        //  will be broadcasted to all subscribers regardless of publish address.
        currentUserData.disconnectionPublishAddress = 'general_functions_' + currentUserData.companyId;
        //  if the socket server is active and available, get the socket script
        //  and trigger the socketActivated event so that the other socket event
        //  handlers may now do their work
        if (socketActive == 1 && socketServer.trim() != '' && $('#current-user-msg_restriction').text() != 1 ) {
            $.ajax({
                url: socketServer + '/connection_test',
                type: 'HEAD',
                timeout: 1000,
                statusCode: {
                    0: function () {       
                        var FailedConTest = "<br><center><h3>Socket Server is not responding. Please contact your System Administrator.</h3></center>"
                        $('#failedConTest').html(FailedConTest).siblings().hide();
                        $('.send_new_message').hide(); 
                        $('#txt-search-user').hide();
                        $.event.trigger({
                            type: 'socketFailedActivation',
                            currentUserData: currentUserData
                        });
                    },
                    200: function () {

                        $.getScript(socketServer + '/socket.io/socket.io.js', function () {
                            socket = io.connect(socketServer);
                            $.event.trigger({
                                type: 'socketActivated',
                                currentUserData: currentUserData,
                                socket: socket
                            });
                        });
                        $('#failedConTest').hide();
                    }
                }
            });
        }
    });
})();