(function () {

    var socket;
    var currentUserData;

    $(document).on('socketActivated', function (event) {

        socket = event.socket;
        // console.log(socket)
        currentUserData = event.currentUserData;
        initializeEvents();
    });

    function initializeEvents() {
        $(document).on("newReplySent", triggerMessageSent);
        $(document).on("newMessageSent", triggerNewMessageSent);
        $(document).on("thread_opened", triggerThreadOpened);
        $(document).on("userTyping",triggerUserTyping);
        $(document).on("changeName",triggerchangeName);
        $(document).on("AngularSent",triggerAngularSent);
        $(document).on("selectedAText",triggerSelectedText);
        $(document).on("textChanged",triggerTextChanged);
        $(document).on('selectCell',triggerSelectCell);
//        socket.on('messageRecieved', function (data) {
        socket.on('message_recieved', function (data) {
            var pathname = window.location.pathname;
            if(pathname == "/user_view/message-app"){
            // console.log(data.threadId)
                if (data.threadId === getCurrentThreadFocus()) {
                    //  append the new message to the thread messages
                    $(".fl-widget-msg-content-wrapper").append(data.messageHTML);
                    $(".fl-widget-message-create-wrapper").scrollTop($(".fl-widget-message-create-wrapper").prop("scrollHeight"));
                    $(".fl-widget-message-create-wrapper").perfectScrollbar("update");

                    MessagingBackend.markThreadAsRead(data.threadId, 1);
                } else {
                    //  refresh the current user thread list   
                    var currentThreadFocus = getCurrentThreadFocus();
                    if (typeof MessagingBackend !== "undefined") { //  trigger events for messaging if on the messaging page
                        MessagingBackend.reloadCurrentUserThreadList(currentThreadFocus);  //  message_backend.js   
                    }

                    appendUnreadMessagesCounter();
                }
            }
        });

        socket.on('thread_opened', function (data) {
            // console.log('thread_opened');
            // console.log(data);

            //  refresh the current user thread list   
            var currentThreadFocus = getCurrentThreadFocus();
            if (typeof MessagingBackend !== "undefined") { //  trigger events for messaging if on the messaging page                
                MessagingBackend.reloadCurrentUserThreadList(currentThreadFocus, function () {
                    subtractUnreadMessagesCounter(data.unreadCount);
                });  //  message_backend.js                   
            } else {
                subtractUnreadMessagesCounter(data.unreadCount);
            }
        });

        // socket.on('user_typing', function (data) {
        //     console.log('what?')
        // });
    }
    function triggerAngularSent(data){
        console.log("thread_id",data.thread_id);
    }
    function subtractUnreadMessagesCounter(subtractBy) {
        var currentMessageCount = $(".fl-message-counter").html() || "0";
        currentMessageCount = parseInt(currentMessageCount.trim());

        var newMessageCount = currentMessageCount - subtractBy;

        //  validate again via UI
        var unreadCount = 0;
        $('.fl-user-conversation-unread-count').each(function () {
            unreadCount += parseInt($(this).attr('unread-count'));
        });

        if (unreadCount > 0) {
            newMessageCount = unreadCount;
        }

        if (newMessageCount && newMessageCount >= 1) {
            $(".fl-message-counter").html(newMessageCount);
            $(".fl-message-counter").css("display", "block");
        } else {
            $(".fl-message-counter").html(0);
            $(".fl-message-counter").css("display", "none");
        }
        updateTitleWithCountNotif();
    }

    function triggerMessageSent(event) {
        console.log(event);
        //  convert the message to a message received from other users
        var convertedJSON = convertMessageToJSON(event.messageHTML);
        var convertedHtml = convertOwnMessageToReceivedMessage(event.messageHTML);

        convertedJSON.message_raw = event.message_raw;
        if(event.recipients){
            var recipients = event.recipients;    
        } else{
            var recipients = getThreadOtherSubscribers();
        }
        console.log(recipients.split(","));

        socket.emit("custom_event", {
            event_triggered: "message_recieved",
            sendTo: recipients.split(","),
            data: {
                recipients: recipients,
                messageHTML: convertedHtml,
                messageJSON: convertedJSON,
                threadId: convertedJSON.threadId 
            }
        });

    }

    function triggerNewMessageSent(event) {
        //  convert the message to a message received from other users
        var convertedJSON = convertMessageToJSON(event.messageHTML);
        var convertedHtml = convertOwnMessageToReceivedMessage(event.messageHTML);
        var recipientObjects = event.recipients;
        var recipients = [];
        
        for (var i in recipientObjects) {
            recipients.push(recipientObjects[i].value);
        }

        socket.emit("custom_event", {
            event_triggered: "message_recieved",
            sendTo: recipients,
            data: {
                recipients: recipients,
                messageHTML: convertedHtml,
                messageJSON: convertedJSON,
                threadId: convertedJSON.threadId
            }
        });
        console.log("Message Sent");
    }

    function triggerThreadOpened(event) {

        // console.log(event);

        socket.emit("custom_event", {
            event_triggered: "thread_opened",
            sendTo: [currentUserData.id],
            data: {
                threadId: event.threadId,
                unreadCount: event.unreadCount
            }
        });

    }

    function appendUnreadMessagesCounter() {
        var currentMessageCount = $(".fl-message-counter").html() || "0";
        currentMessageCount = parseInt(currentMessageCount.trim());
        $(".fl-message-counter").html(currentMessageCount + 1);
        $(".fl-message-counter").css("display", "block");
    }

    //  <editor-fold desc="Converters" defaultstate="collapsed">

    function convertOwnMessageToReceivedMessage(htmlString) {

        var html = $.parseHTML(htmlString);

        $(html).addClass("fl-msg-user-other-wrapper");
        $(html).removeClass("fl-msg-user-you-wrapper");

        return $(html)[1].outerHTML;

    }

    function convertMessageToJSON(htmlString) {

        var html = $(htmlString);
        var json = {
            threadId: html.attr("thread-id"),
            messageId: html.attr("message-id"),
            message: html.find(".fl-user-msg").html(),
            author: {
                id: html.attr("message-author-id"),
                displayName: html.find(".fl-msg-user-details").find(".name").html(),
                imageURL: html.find(".fl-user-avatar-wrapper").find("img").attr("src")
            }
        };

        return json;

    }

    //  </editor-fold>

    //  <editor-fold desc="Getters & Setters" defaultstate="collapsed">

    function getThreadOtherSubscribers() {
        return $("#fl-thread-data").attr("thread-other-subscribers");
    }

    function getCurrentThreadFocus() {
        return $("#fl-thread-data").attr("thread-id");
    }

    function triggerUserTyping (this_tab){
        // console.log("function Trigger Socket")
        // console.log("Hello",this_tab)
        var threadId = this_tab.threadId;
        var other_recips = recipients.split(",")
        console.log("other",other_recips)
        var current_id = $("#current-user-id").html();
        var current_fname = $('#current-user-first-name').html();
            for(var i in other_recips){
                if(other_recips[i] == current_id){
                    other_recips.splice(i, 1);
                    break;
                }
            }
        var new_recips = other_recips
        socket.emit("custom_event", {
            event_triggered: "typing",
            sendTo: new_recips,
            data: {
                threadId: threadId,
                sender : current_fname
            }
        });   
    }

    function triggerchangeName(data){
        console.log(data);
        var current_id = $("#current-user-id").html();
        var other_recips = data.recipients.split(",")
        for(var i in other_recips){
                if(other_recips[i] == current_id){
                    other_recips.splice(i, 1);
                    break;
                }
            }
        socket.emit("custom_event", {
            event_triggered: "nameChanged",
            sendTo: other_recips,
            data: {
                threadId: data.threadId,
                newName : data.newName
            }
        });      
    }

    function triggerSelectedText(data){
        console.log('Data Content',data);
        socket.emit("custom_event", {
            event_triggered: "selected_text",
            sendTo : data.recipients,
            data: {
                selecting: data.selecting,
                cell_id : data.cell_id,
                selected_Text : data.selectedText,
                select_start : data.select_start,
                select_end : data.select_end,
                selected_word_index : data.selected_word_index
            }
        });
    } 
    //  </editor-fold>
    function triggerTextChanged (data){
        console.log("changed!",data);
        socket.emit("custom_event", {
            event_triggered: "text_changed",
            sendTo: data.recipients,
            data: {
                cell_id: data.cell_id
            }
        });
    }

    function triggerSelectCell (data){
        console.log("Select Cell!",data);
        socket.emit("custom_event", {
            event_triggered: "select_cell",
            sendTo: data.recipients,
            data: {
                value: data.value,
                cell_id: data.cell_id
            }
        });
    }

})();