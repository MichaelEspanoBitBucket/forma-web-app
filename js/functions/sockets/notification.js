/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function() {

    /*
     * Notification Type Constants (Strings)
     * NEW_ANNOUNCEMENT
     * NEW_POST_REPLY - reply to announcement
     * USER_MENTIONED - posts where the user is tagged
     * NEW_REQUEST
     * NEW_LIKE
     * 
     * //   TODO:   list the other constants used for notifications here for reference
     * 
     */

    var socket;
    var currentUserData;

    $(document).on('socketActivated', function(event) {
        socket = event.socket;
        currentUserData = event.currentUserData;
        initializeEvents();
    });

    $(document).on('sendNotification', function(event) {

        //  validate if the event object is missing or is not supplied
        if (typeof event === 'undefined') {
            return;
        }

        console.log(event);

        var notificationData = {
            sendTo: 'broadcast',
            data: event.notificationData
        };

        try {
            if (typeof event.sendTo !== 'undefined') {
                if (event.sendTo instanceof Array && event.sendTo.length > 0) {
                    if (typeof currentUserData.id != "undefined") {
                        if (event.sendTo.length == 1 && event.sendTo[0] == currentUserData.id) {
                            //  do not send notification to the current user
                            return;
                        }
                    }
                    notificationData.sendTo = event.sendTo;
                }
            }
            socket.emit('notify', notificationData);
        } catch (e) {
            console.log('Failed to send notification:');
            console.log(e);
        }
    });
    $(document).on('updateChart', function(event) {

        //  validate if the event object is missing or is not supplied
        if (typeof event === 'undefined') {
            return;
        }

        console.log(event);

        var notificationData = {
            sendTo: 'broadcast',
            data: event.notificationData
        };

        try {
            socket.emit('notify', notificationData);
        } catch (e) {
            console.log('Failed to send notification:');
            console.log(e);
        }
    });
    $(document).on('updateEmbed', function(event) {
        //  validate if the event object is missing or is not supplied
        if (typeof event === 'undefined') {
            return;
        }

        // console.log(event);

        var notificationData = {
            sendTo: 'broadcast',
            data: event.notificationData
        };

        try {
            socket.emit('notify', notificationData);
        } catch (e) {
            console.log('Failed to send notification:');
            console.log(e);
        }
    });

    function initializeEvents() {

        socket.on('notificationRecieved', function(data) {

            if (typeof data.notificationType === 'undefined') {
                return;
            }

            if (data.notificationType == 'NEW_POST_REPLY') {
                appendNotificationCount();
                appendPostReply(data.notificationData);
            } else if (data.notificationType == 'NEW_ANNOUNCEMENT') {
                appendNewAnnouncement(data.announcementId, data.mentionData, data.announcementHTML);
            } else if (data.notificationType == 'USER_MENTIONED') {
                appendNotificationCount();
            } else if (data.notificationType == 'NEW_REQUEST') {
                appendNotificationCount();
            } else if (data.notificationType == 'NEW_LIKE') {
                appendNotificationCount();
            }else if(data.notificationType == 'UPDATE_CHART'){
                refreshGraphReport(data)
            }else if(data.notificationType == 'UPDATE_EMBED'){
                refreshEmbeddedView(data);
            }
        });

    }

    function appendNotificationCount() {
        var previousNotificationCount = parseInt($('.notiCount').html());
        $('.notiCount').html(previousNotificationCount + 1);
        $('.notiCount').effect('shake', {
            direction: 'up',
            distance: 5
        });
    }

    function appendNewAnnouncement(announcementId, mentionData, html) {
        $(html).fadeIn("slow").prependTo(".post-message");
        $(html).fadeIn("slow").prependTo(".hashTag_post");

        $("label.timeago").timeago();
        $(".getPost").val(null);
        $(".getPostHash").val(null);
//        $("." + elements).html(null);
        $(".getPost").css("height", "50px");
        $(".getPostHash").css("height", "50px");
        $(".getPost").closest(".mentionContainer").find(".mentionSelectedContainer").html("")
        $(".loading").hide();

        $(".post").eq(0).find(".reply").mention_post({
            json: mentionData
        });

        $('#postUser_' + announcementId).popover({
            html: true,
            content: function() {
                return $('#popover_content_wrapper_' + announcementId).html();
            }
        });

        // Remove images uploaded on the preview
        $(".previewImagePostUpload").html(null);
        $(".imagePreviewUpload").hide();

        // On Image Slider
        $('.flexslider_' + announcementId).flexslider({
            controlNav: false
        });

        jQuery('.mycarousel').jcarousel();

        $(".dataTip").tooltip({
            html: true,
            placement: "top"
        });

        $(".tip").tooltip({
            html: true,
            placement: "right"
        });

        $(".group_0_" + announcementId).colorbox({
            rel: 'group_0_' + announcementId,
            width: "75%",
            height: "80%"
        });

    }

    function appendPostReply(notificationData) {
        var comment = notificationData.returnComment;

        if (typeof comment === 'undefined') {
            return;
        }

        //  append the reply only if the user is in the home page or the post's page
        if ($('#commentView_' + comment.postID).length <= 0) {
            return;
        }

        var html = replyPost(null, null, null, comment);
        $(html).fadeIn("slow").insertAfter($('#commentView_' + comment.postID + ' .mentionContainer'));
//        $(html).fadeIn("slow").insertAfter($('.input-announcement'));
        $("label.timeago").timeago();
        $('.flexslider_1_' + comment.commentID).flexslider({
            controlNav: false
        });

        jQuery('.mycarousel').jcarousel();

        $(".tip").tooltip({
            html: true,
            placement: "right"
        });

    }
    
    /* Refresh the page if the socket recieve for and the generated report form is equals */
    function refreshGraphReport(data){
        var pathname = window.location.pathname;
        if(pathname!="/ajax/generateReport"){
            return;
        }
        var flag = $(".formIDForSocket");
        var formReportID = flag.val();
        var requestTriggerFormId = data.requestData.formId
        if(flag.length>=1){
            if(formReportID==requestTriggerFormId){
                window.location.reload(true);
            }
        }
    }

    function refreshEmbeddedView(data){
        // var pathname = window.location.pathname;
        // if(pathname!="/user_view/workspace"){
        //     return; 
        // }
        console.log("TESTING data refreh", data)
        var id_collectors = [ data['requestData']['formId'] ];
        if($.type(data['requestData']['responses']) == "array"){
            if(data['requestData']['responses'].length >= 1){
                for(var ctr_ii in data['requestData']['responses']){
                    if(data['requestData']['responses'][ctr_ii] != null){
                        id_collectors.push(data['requestData']['responses'][ctr_ii][0]['form_id']);
                    }
                }
            }
        }
        $.unique(id_collectors);
        for(var ctr_ii in id_collectors){
            var form_id = id_collectors[ctr_ii];
            var select_form_content = $('#frmrequest').find('.loaded_form_content');
            var affected_embed_objs = $('.embed-view-container[embed-source-form-val-id="'+form_id+'"]');
            affected_embed_objs.each(function(){
                embeded_view.refreshEmbed($(this), null, null, function() {
                    //callback
                });
            });
        }
        
    }
})();