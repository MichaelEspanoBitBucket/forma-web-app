/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var jsonStagingData = {
    search_value: "",   
    "column-sort": "",
    "column-sort-type": "", 
    limit: 10,
    form_id:0
}

$(document).ready(function(){
    var enable_staging = $("#enable_staging").text();
    if(enable_staging==="1"){
        StagingProcess.init();
    }
});

StagingProcess = {
    "init" : function(){
        this.getDevFormsActions();
        this.reflect_staging_action();
        this.sort();
    },
    "getDevFormsActions" : function(){
        var self = this;
        $("body").on("click",".reflect-form",function(){
            self.getDevFormsFunctions(this);
        });
    },
    "getDevFormsFunctions" : function(obj){
        var self = this;
        var form_id = $(obj).attr("form_id");
        var count_dev_forms = parseInt($(obj).attr("count_dev_forms"));
        //set form_id
        jsonStagingData['form_id'] = form_id;

        if(count_dev_forms>0){
            var ret = '<h3 class="fl-margin-bottom">';
                ret += '<i class="icon-save"></i> Development Forms';
                ret += '</h3>';
                ret += '<div class="hr"></div>';
                ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
                ret += '<div class="content-dialog" style="height: auto;">';
                    ret += '<div class="fl-datatable-wrapper">';
                        ret += '<table id="dev_forms" class="display_data fl-form-list dataTable">';
                            ret += '<thead>';
                                ret += '<tr>';
                                    ret += '<th></th>';
                                    ret += '<th field_name="form_name"><div class="sortable-image fl-module-tbl-arrow"></div><div class="fl-table-ellip"><div>Title</div></th>';
                                    ret += '<th field_name="author"><div class="sortable-image fl-module-tbl-arrow"></div><div class="fl-table-ellip"><div>Author</div></th>';
                                    ret += '<th field_name="date_created"><div class="sortable-image fl-module-tbl-arrow"></div><div class="fl-table-ellip"><div>Date Created</div></th>';
                                    ret += '<th field_name="date_updated"><div class="sortable-image fl-module-tbl-arrow"></div><div class="fl-table-ellip"><div>Date Updated</div></th>';
                                ret += '</tr>';
                            ret += '</thead>';
                            ret += '<tbody>';
                            ret += '</tbody>';
                        ret += '</table>';
                    ret+= '</div>';
                    ret += '<div class="dataTable_widget"></div>';
                ret+= '</div>';
            var newjDialog = new jDialog(ret, "","700", "", "70", function(){});
            newjDialog.themeDialog("modal2");
            self.load();
            
        }else{
            showNotification({
                message: "Your form doesn't have staging version.",
                type: "warning",
                autoClose: true,
                duration: 3
            });
        }
    },
    load : function(){
        var oColReorder = {
            "iFixedColumns": 0
        };
        $("#dev_forms").dataTable({
            "sDom": 'Rlfrtip',
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> '+
                       '<div class="bar1"></div> '+
                       '<div class="bar2"></div> '+
                       '<div class="bar3"></div> '+
                       '<div class="bar4"></div> '+
                       '<div class="bar5"></div> '+
                       '<div class="bar6"></div> '+
                      '<div class="bar7"></div> '+
                      '<div class="bar8"></div> '+
                      '<div class="bar9"></div> '+
                      '<div class="bar10"></div> '+
                 '</div>'
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/search",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "form_id", "value": jsonStagingData['form_id']});
                aoData.push({"name": "column-sort", "value": jsonStagingData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonStagingData['column-sort-type']});
                aoData.push({"name": "action", "value": "getDevForms"});

                $.ajax({
//                        "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        try{
                            data = JSON.parse(data);
                            fnCallback(data);
                        }catch(err){
                            console.log(data);
                        }
//                            console.log(data);
                        // return;
                        
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {

               var obj = '#dev_forms';
               var listOfappContainer = '.content-dialog';
               dataTable_widget(obj, listOfappContainer);
            },
            fnDrawCallback : function(){
                $(".tip").tooltip();

            }
        });
    },
    "sort" : function(){
        var self = this;
        $("body").on("click","#dev_forms th",function(){
            var cursor = $(this).css("cursor");
            jsonStagingData['column-sort'] = $(this).attr("field_name");
            if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
                return;
            }
            var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

            $(this).closest("#dev_forms").find(".sortable-image").html("");
            if(indexcolumnSort==-1){
                columnSort.push($(this).attr("field_name"))
                $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
                jsonStagingData['column-sort-type'] = "ASC";
            }else{
                columnSort.splice(indexcolumnSort,1);
                $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
                jsonStagingData['column-sort-type'] = "DESC";
            }
            var thisTh = this;
            $("#dev_forms").dataTable().fnDestroy();
            self.load(function(){
                addIndexClassOnSort(thisTh);
            });
        })
    },
    "search" : function(){
        // var self = this;
        // $("body").on("keyup","#txtSearchRuleDatatable",function(e){
        //     $("#loadRuleSettings").dataTable().fnDestroy();
        //     jsonStagingData['search_value'] = $(this).val();
        //     self.load();
        // })
    },
    "limit" : function(){
        // var self = this;
        // $(".searchRuleLimitPerPage").change(function(e) {
        //     // if (e.keyCode == "13") {
        //         var val = parseInt($(this).val());
        //         jsonStagingData['limit'] = val;
        //         $("#loadRuleSettings").dataTable().fnDestroy();
        //         // var self = this;
        //         self.load();
        //     // }
        // })
    },
    "reflect_staging_action" : function(){
        var self = this;
        
        $("body").on("click",".reflect_staging",function(){
            var staging_id = $(this).attr("form_id");
            var newConfirm = new jConfirm("WARNING: You are about to reflect a staging version to your form. Please make sure to create a back up in case you want to revert your design.<br/>Are you sure you want to continue?", 'Confirmation Dialog', '', '', '', function(r) {
                if (r == true) {
                    self.reflect_staging_function(staging_id);
                } 
            });
            newConfirm.themeConfirm("confirm2", {
                    'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
            });
        });
    },
    "reflect_staging_function" : function(staging_id){
        
        ui.block();
        $.post("/ajax/formbuilder",{staging_id:staging_id,action:"form_reflectDesign"},function(data){
            console.log(data)
            ui.unblock();
            $(".fl-closeDialog").trigger("click");
            showNotification({
                message: "Your development form has been successfully reflected to production form!",
                type: "success",
                autoClose: true,
                duration: 3
            });
        })
    }
};

