var ctr_load_starred = 20;
var continueToLoad_starred = true;
var continueToLoad_starred_page = true;
$(document).ready(function(){
    var pathname = window.location.pathname;
    var objStarred = {"type":"0"};
    var objMyRequest = {"type":"0"};
    if(
        pathname!="/organizational_chart" &&
        pathname!="/workflow" &&
        pathname!="/formbuilder"
    ){
        //removed 5/14/2015 aaron tolentino
        // Starred.load(0,"start",objStarred,function(result){
        //     Starred.loadAfterAjaxStarredWidget(result);
        //     MyRequest.load(0,"start",objMyRequest,function(result_myrequest){
        //         $(".sub-container-mr").html(result_myrequest);

        //         if(result_myrequest.length>0 && $(".sm-myr").length==0){
        //             $("#my-request-container").append("<div class='sm-myr' style='margin-top: 50px;padding: 0px 0px 10px 0px;text-align:center;font-weight: bold;'><a href='/my_request'>See All</a></div>");
        //         }
        //         $("#my-request-container").perfectScrollbar("update");
        //     });
	    
        // });
    }
    //widget user view
    if(pathname=="/user_view/home" || pathname=="/user_view/gi-dashboard-home"){
        
    }
    // alert(123)
    //end widget user view
    $("body").on("click",".search-starred",function(){
        Starred.load(0,"start",objStarred,function(result){
           $(".starred-wrapper").html(result);
        });
    })
    $("body").on("keydown",".search-starred-value",function(e){
        if (e.keyCode == "13") {
            Starred.load(0,"start",objStarred,function(result){
               $(".starred-wrapper").html(result);
            });
        }
    })
	
    $("body").on("mouseenter",".starred-row",function(){
        $(this).find(".removeStarred").css({
            "display":"block"   
        })
    })
    $("body").on("mouseleave",".starred-row",function(){
        $(this).find(".removeStarred").css({
            "display":"none"   
        })
    })
	
    
    //count starred
     Starred.count();
    // $("#starred-container").scroll(function(){
    //     var pads = 12; //padding
    //     if($(this).scrollTop() == $(this).find(".sub-container-starred").height() - $(this).height()){ 
    //        // for load request

    //        if(continueToLoad_starred==true){
    //             Starred.load(ctr_load_starred,"load",function(){
    //                 ctr_load_starred+=10;
    //             }); 
    //        }
    //     }
    // })
    var pathname = window.location.pathname; 
    if(pathname=="/user_view/starred"){
        objStarred_user = {"type":1};
        $(".starred-notif").fadeIn();
        $(".fl-loading-content").fadeIn();
        Starred.load(0,"start",objStarred_user,function(result){
            $(".starred-wrapper2").html(result);
            $(".fl-starred-wrapper-scroll").perfectScrollbar("destroy");
            $(".fl-starred-container-users").perfectScrollbar();
            if(result==""){
                $(".fl-no-record-found").fadeIn();
            }
            $(".fl-loading-content").fadeOut();
            $(".timeago").timeago();
            $(".fl-starred-scrollhere").perfectScrollbar("update");
        })
        $(".fl-starred-scrollhere").scroll(function() {
            var scroll_diff = $(this).scrollTop() - ($(this).find(".fl-starred-wrapper-scroll").height() - $(this).height());
            // console.log(scroll_diff)
            // console.log((scroll_diff+" >= "+(-2))+" && "+(scroll_diff+" <= "+2))
            if ((scroll_diff >= -2) && (scroll_diff <= 2)) {
                if(continueToLoad_starred==true){
                    Starred.load(ctr_load_starred,"load",objStarred_user,function(result){
                        if(result.length>0){
                            ctr_load_starred+=20;
                            // continueToLoad_starred = true;
                            $(".starred-wrapper2").append(result);
                            $(".timeago").timeago();
                            $(".fl-starred-container-users").perfectScrollbar("destroy");
                            $(".fl-starred-container-users").perfectScrollbar();
                        }
                        $(".starred-wrapper2").find(".notif-loading").remove();
                    }); 
                }
            }
        });
    }
    if(pathname=="/starred"){
        Starred.load(0,"start",objStarred,function(result){
            $(".starred-wrapper").html(result);
        })
        $(window).scroll(function() {
            var scroll_diff = $(window).scrollTop() - ($(document).height() - $(window).height());
            // console.log(scroll_diff)
            // console.log((scroll_diff+" >= "+(-1))+" && "+(scroll_diff+" <= "+1))
            if ((scroll_diff >= -2) && (scroll_diff <= 2)) {
                if(continueToLoad_starred==true){
                    Starred.load(ctr_load_starred,"load",objStarred,function(result){
                        if(result.length>0){
                            ctr_load_starred+=10;
                            continueToLoad_starred = true;
                            $(".starred-wrapper").append(result);
                        }
                    }); 
                }
            }
        });
    }
})
