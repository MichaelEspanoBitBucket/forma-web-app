Starred = {
    load : function(start,status,obj,callback){
        continueToLoad_starred = false;
        var search_value = $(".search-starred-value").val();
        var self = this;
        var loading = $('<div align="center" class="notif-loading"><img src="/images/icon/loading.gif"></div>');
        if($(".starred-wrapper2").find(".notif-loading").length==0){
            $(".starred-wrapper2").append(loading);
        }
        $.post("/ajax/starred",{action:"loadData",start:start,search_value:search_value,view_type:obj['type']},function(data){
            // console.log(data);
            // return;
            var data = JSON.parse(data);
            var result = "";
            if(obj['type']==0){
                result = self.view(data);
            }else if(obj['type']==1){
                result = self.view2(data);
            }else if(obj['type']==2){
                result = self.view4(data);
            }
           
            if(status=="start"){
                //restore variables for load more
                // $(".sub-container-starred").html(result);
                // $(".starred-wrapper").html(result);
                
                ctr_load_starred = 20;
                continueToLoad_starred = true;
                // if(data.length>0 && $(".sm-sr").length==0){
                //     $(".sub-container-starred").append("<div class='sm-sr' style='margin-top: -5px;padding: 0px 0px 10px 0px;text-align:center;font-weight: bold;'><a href='/starred'>See All</a></div>");
                // }
                
            }else if(status == "load"){
                // $(".starred-wrapper").append(result);
                // $(".sub-container-starred").append(result);
                // $("#starred-container").perfectScrollbar("update");
                continueToLoad_starred = true;
            }
            $(".timeago").timeago();
            // $(".load-app-starred").remove();
             $(loading).remove();
            if(data.length==0){
                continueToLoad_starred = false;
            }

            callback(result)
        })

        // .fail(function(){
        //     alert("CONNECTION FAILED!!");
        //     continueToLoad_starred = false;
        // })
    },
    view : function(data){
        var ret = "";
        var stat = "";
        if(data.length==0){
            continueToLoad_starred = false;
        }else{
            // $.each(data,function(key, request){

            var requestor_id = "";
            var padding = "";
            var link = "";
            var icon = "";
            for(var i in data){
                if(data[i]!="undefined"){
                    padding = "padding-top: 0px;";
                    if(i==0){
                        padding = "padding-top: 5px;";
                    }
                    if(data[i]['type']==1){
                        //console.log(data[i])
                        link =                  '<li><a href="post?view_type=view_post&postID='+ data[i]['link'] +'" title="" data-original-title="">'+ data[i]['first_name'] +' '+ data[i]['last_name'] +'</a></li>';
                        icon = "icon-file";
                    }else{
                        link =                  '<li><a href="/workspace?view_type=request&formID='+ data[i]['ws_id'] +'&requestID='+ data[i]['data_id'] +'" title="" data-original-title="">'+ data[i]['first_name'] +' '+ data[i]['last_name'] +'</a></li>';
                        icon = "icon-reorder";
                    }
                    ret += '<div class="supTicket nobg starred-row" style="'+ padding +'">';
                    ret +=        '<div class="pull-right" style="right: 25px;height:12px"><i class="icon-remove cursor removeStarred" data-starred="'+ data[i]['starred_id'] +'" style="display:none" data-type="'+ data[i]['type'] +'" data-id="'+ data[i]['starred_id'] +'" counterPart-id="starred_'+ data[i]['data_id'] +'" post-id="'+ data[i]['data_id'] +'"></i></div>';
                    ret +=        '<div class="issueType" style="margin-top: 12px;">';
                    ret +=           '<span class="issueInfo" style="width: 70%;"><i class="'+ icon +'"></i> '+ data[i]['form_name'] +'</span>';
                    ret +=           '<span class="issueNum" style="float:left">'+ if_undefinded(data[i]['TrackNo'],"") +'</span>';
                    ret +=        '</div>';
                    ret +=        '<div class="issueSummary">';
                    ret +=           '<a href="#" title="" class="pull-left" data-original-title="">';
                    ret +=              data[i]['user_image'];
                    ret +=            '</a>';
                    ret +=           '<div class="ticketInfo">';
                    ret +=              '<ul>';
                    ret += link;
                    stat = 'Status: <strong class="green">'+ data[i]['form_status'] +'</strong>';
                    if(data[i]['form_status']==undefined){
                        stat = "<br />";
                    }
                    ret +=                  '<li>'+ stat +'</li>';
                    ret +=                  '<li class="even timeago" style="font-size: 10px;text-align: left;color: #888888;text-align:left">'+ data[i]['DateCreated'] +'</li>';
                    // ret +=                  '<li><a style="font-size: 11px;cursor: pointer;">Remove</a></li>'
                    ret +=               '</ul>';
                    ret +=            '</div>';
                    ret +=         '</div>';
                    ret +=     '</div>';
                    ret +=    '<div class="hr" style=""></div>';    
                }
                
            }
            // })             
        }
        return ret;
    },
    view2 : function(data){
        var ret = "";
        var stat = "";
        if(data.length==0){
            continueToLoad_starred = false;
        }else{
            // $.each(data,function(key, request){

            var requestor_id = "";
            var padding = "";
            var link = "";
            var icon = "";
            var dataText = "";
            var form_json = "";
            var header_info = "";
            var header_info_value = "";
            for(var i in data){
                if(data[i]!="undefined"){
                    //For Request
                    // if(data[i]['type']=="0"){
                        form_json = data[i]['form_json'];
                        if(form_json){
                            form_json = JSON.parse(form_json);
                            if(!data[i]['TrackNo']){
                                data[i]['TrackNo'] = "Null";
                            }
                            dataText = "Tracking Number : "+data[i]['TrackNo']+", Requestor : "+data[i]['display_name'];
                            dataText += ", Status : "+data[i]['Status']+", Date Created : "+data[i]['DateCreated'];
                            if(form_json['form_json']['collected_data_header_info']){
                                dataText = "";
                                header_info = form_json['form_json']['collected_data_header_info']['listedchoice'];
                                for(var j in header_info){
                                    header_info_value = header_info[j]['field_name'];
                                    if(header_info_value=="Requestor"){
                                        header_info_value = "display_name";
                                    }
                                    dataText += header_info[j]['field_label']+" : "+if_undefinded(data[i][''+ header_info_value +''],"")+" ";
                                    if(j<header_info.length){
                                        dataText+=", ";
                                    }
                                }
                            }
                        }
                        link = '<a href="/user_view/workspace?view_type=request&formID='+ data[i]['ws_id'] +'&requestID='+ data[i]['data_id'] +'" title="" data-original-title="" style="color:#ffffff">Request</a>';
                        dataText = this.limitText(dataText,150);
                    // }else{
                    //     //For Announcement / Post
                    //     link = '<a href="post?view_type=view_post&postID='+ data[i]['link'] +'" title="" data-original-title="" style="color:#ffffff">Announcements</a>';
                    //     dataText = this.limitText(data[i]['display_name']+": "+replaceTagStr(replaceStrHashtags_decode(replaceStrHashtags_encode(replaceURLWithHTMLLinks(data[i]['post'])),1)),100);
                    // }
                    ret +=    '<li>';
                        ret +=    '<div class="fl-status-and-type">';
                            ret +=    '<i class="fa fa-star fl-starred-color"></i>';
                            ret +=    '<span class="fl-from">'+ link +'</span>';
                        ret +=    '</div>';
                        ret +=    '<div class="fl-notification-content">';
                            ret +=    dataText;
                            ret += "";
                        ret +=    '</div>';
                        ret +=    '<div class="fl-notification-del-time">';
                            ret +=    '<span class="fl-delete">';
                                ret +=    '<i class="fa fa-trash-o removeStarred_user" form_id = "'+ data[i]['ws_id'] +'" data-starred="'+ data[i]['starred_id'] +'" data-type="'+ data[i]['type'] +'" data-id="'+ data[i]['starred_id'] +'" counterPart-id="starred_'+ data[i]['data_id'] +'" post-id="'+ data[i]['data_id'] +'"></i>';
                            ret +=    '</span> ';
                            var dateStarred = data[i]['datetime'];
                            if(dateStarred=="0000-00-00 00:00:00"){
                                dateStarred = data[i]['DateCreated'];
                            }
                            ret +=    '<span class="time timeago" title="'+ dateStarred +'">'+ dateStarred +'</span>';
                        ret +=    '</div>';
                        ret +=    '<div class="clearfix"></div>';
                    ret +=    '</li>';
                }
                
            }
            // })             
        }
        return ret;
    },
    view3 : function(data){
        var ret = "";
        var stat = "";
        if(data.length==0){
            continueToLoad_starred = false;
        }else{
           
            var requestor_id = "";
            var padding = "";
            var link = "";
            var icon = "";
            var lengthStarredWidget = $(".starredUserWidgetWrapper").find(".fl-widget-content").length;
            for(var i in data){
                if(data[i]!="undefined"){
                    if(lengthStarredWidget>=10){
                        continueToLoad_starred = false;
                        break;
                    }
                    
                    var starred_type = '<a href="/user_view/post?view_type=view_post&postID='+ data[i]['link'] +'" title="" data-original-title="">Announcements</a>';
                    if(data[i]['TrackNo']){
                        starred_type = '<a href="/user_view/workspace?view_type=request&formID='+ data[i]['ws_id'] +'&requestID='+ data[i]['data_id'] +'" title="" data-original-title="">Request</a>';
                    }
                    ret +=    '<div class="fl-widget-content">';
                      ret +=    '<div class="fl-avatar">'+ data[i]['user_image'] +'</div>';
                      ret +=    '<div class="fl-widget-starred-content-wrapper">';
                        ret +=    '<i class="fl-delete fa fa-times removeStarred_user_widget" form_id = "'+ data[i]['ws_id'] +'" data-starred="'+ data[i]['starred_id'] +'" data-type="'+ data[i]['type'] +'" data-id="'+ data[i]['starred_id'] +'" counterPart-id="starred_'+ data[i]['data_id'] +'" post-id="'+ data[i]['data_id'] +'"></i>';
                        ret +=    '<span class="starredtype-data theme-font-color">'+ starred_type +'</span><br/>';
                        ret +=    '<span class="starredname-data">'+ data[i]['first_name'] +' '+ data[i]['last_name'] +'</span><br/>';
                        ret +=    '<span class="starreddate-data">'+ data[i]['DateCreated'] +'</span>';
                      ret +=    '</div>';
                      ret +=    '<div class="clearfix"></div>';
                   ret +=    '</div>';
                 
                }
                
            }
            // })             
        }
        return ret;
    },

    view4: function(data){

        var ret = "";
        var stat = "";
        if(data.length==0){
            continueToLoad_starred = false;
        }else{
     
            var requestor_id = "";
            var padding = "";
            var link = "";
            var icon = "";
            var lengthStarredWidget = $(".starredUserWidgetWrapper").find(".fl-widget-content").length;
            for(var i in data){
                if(data[i]!="undefined"){
                    if(lengthStarredWidget>=10){
                        continueToLoad_starred = false;
                        break;
                    }
                    var starred_type = '<a href="/user_view/post?view_type=view_post&postID='+ data[i]['link'] +'" title="" data-original-title="">Announcements</a>';
                    if(data[i]['TrackNo']){
                        starred_type = '<a href="/user_view/workspace?view_type=request&formID='+ data[i]['ws_id'] +'&requestID='+ data[i]['data_id'] +'" title="" data-original-title="">Request</a>';
                    }
                  
                        ret += '<div class="fl-widget-content fl-widget-contentv2">';
                            ret += '<div class="fl-avatar">' + data[i]['user_image'] + '</div>';
                            ret += '<div class="fl-widget-starred-content-wrapperv2 theme-font-color">';
                                ret += '<i class="fl-delete fa fa-times removeStarred_user_widget" form_id = "'+ data[i]['ws_id'] +'" data-starred="'+ data[i]['starred_id'] +'" data-type="'+ data[i]['type'] +'" data-id="'+ data[i]['starred_id'] +'" counterPart-id="starred_'+ data[i]['data_id'] +'" post-id="'+ data[i]['data_id'] +'"></i>';
                                ret += '<div class="fl-wcheader">';
                                    // ret += '<span class="fl-wchname ">' + data[i]['first_name'] +' '+ data[i]['last_name'] + '</span><br/>';
                                    ret += '<span class="fl-wchname ">' + data[i]['display_name']+ '</span><br/>';
                                    ret += '<span class="fl-wchdate">' + data[i]['DateCreated'] + '</span> ';                                             
                                ret += '</div>';
                                ret += '<span class="starredtype-data theme-font-color">'+ starred_type +'</span>';
                            ret += '</div>';  
                        ret += '</div>';
                
                }
                
            }            
        }
        return ret;

    },
    count : function(){
        return;
        $.post("/ajax/starred",{action:"countData"},function(data){
            if(data==0){
                $("#starred_count").remove();
                $(".starred_count").remove();
            }else{
                $("#starred_count").text(data);
                $(".starred_count").text(data);
            }
        })
        // .fail(function(){
        //     alert("CONNECTION FAILED!!");
        //     $("#starred_count").text("0");
        // })
    },
	 // Set Starred hover
    hoverIn : function(obj){
        var data_starred = $(obj).attr("data-starred");
        if(data_starred==0){
            $(obj).removeClass('icon-star-empty').addClass("icon-star");
        }
    },
    hoverOut : function(obj){
    	var data_starred = $(obj).attr("data-starred");
        if(data_starred==0){
        	$(obj).removeClass('icon-star').addClass("icon-star-empty");
        }
    },
    updateData : function(obj,ele_id){
    	var data_starred = $(obj).attr("data-starred");
        var data_type = $(obj).attr("data-type");
    	var tablename = $(obj).closest(".fl-table-wrapper").find(".table-name").val();
        var form_id = $(obj).closest(".fl-table-wrapper").attr("form_id");
        
        
        if(tablename!=""){
            tablename = $(".table-name").val();
        }

        if(!form_id){
            form_id = $(obj).attr("form_id");
        }
    	var data_id = $(obj).attr("data-id");
        var post_id = $(obj).attr("post-id");
        var starred_title = "";
        // alert("data_starred = "+data_starred+"\ndata_type = "+data_type+"\ntablename = "+tablename+"\ndata_id = "+data_id+"\npost_id = "+post_id)
        // return;
    	$.post("/ajax/starred",{action:"updateData",
    					data_starred:data_starred,tablename:tablename,
                        form_id:form_id,
    					data_id:data_id},function(data){
                            // console.log(data)
    		data = JSON.parse(data);
            if($(obj).hasClass("removeStarred_user")){
                $(obj).closest("li").fadeOut();
            }else if($(obj).hasClass("removeStarred_user_widget")){
                $(obj).closest(".fl-widget-content").fadeOut();
            }else{
                if(data['status']=="added"){
                    $("#"+ele_id).attr("data-starred",data['id']);
                    $("#"+ele_id).removeClass('icon-star-empty');
                    $("#"+ele_id).addClass('icon-star');
                    if($(obj).hasClass("fa-star-o")){
                        $("#"+ele_id).removeClass('fa-star-o');
                        $("#"+ele_id).addClass('fa-star');
                        starred_title = "Remove Starred";
                    }
                }else{
                    $("#"+ele_id).attr("data-starred","0");
                    $("#"+ele_id).toggleClass('icon-star icon-star-empty');
                    if($(obj).hasClass("fa-star")){
                        $("#"+ele_id).removeClass('fa-star');
                        $("#"+ele_id).addClass('fa-star-o');
                        starred_title = "Starred Request";
                    }
                }

                $("#"+ele_id).tooltip('hide').attr("data-original-title",starred_title).tooltip('fixTitle');
                if(data_type==1){
                    $(".starred_show_" + post_id).hide();
                    var ret = '<div class=" postPrivacyChoice-choices post_starred" id="addStarred_' + post_id + '" data-action="addStarred" data-id="' + post_id + '" style="">';
                        ret += '<span class="postChoice-icon" style="">';
                            ret += '<i class=" icon-star-empty" style=""></i>';
                        ret += '</span>';
                        ret += '<span class="postChoice" value="0" style="float: left;">Starred Post</span>';
                    ret += '</div>';    
                    $("#postSettingChoice_"+post_id).html(ret)
                }
            }
   //          showNotification({
			//     message: data['message'],
	  //           type: "success",
			//     autoClose: true,
			//     duration: 3
			// })
    	})
    },
    loadAfterAjaxStarredWidget : function(result){
        $(".sub-container-starred").html(result);
        if(result.length>0 && $(".sm-sr").length==0){
            $(".sub-container-starred").append("<div class='sm-sr' style='margin-top: 50px;padding: 0px 0px 10px 0px;text-align:center;font-weight: bold;'><a href='/starred'>See All</a></div>");
        }
        $("#starred-container").perfectScrollbar();
        $("#starred-container").perfectScrollbar("update");

        //user widget
        $(".starredUserWidgetWrapper").perfectScrollbar();
        $(".starredUserWidgetWrapper").perfectScrollbar("update");
        this.count();
    },
    limitText : function(str,length){
        var strlength = str.length;
        var ret = ""
        ret =  str.substr(0,length);
        if(strlength>length){
            ret+= "..";
        }
        return ret;
    }
}