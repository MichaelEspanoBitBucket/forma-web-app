var jsonSuggestionData = {
    search_value: "",   
    "column-sort": "",
    "column-sort-type": "", 
    limit: 10,
}
var columnSort = [];
$(document).ready(function(){
	var pathname = window.location.pathname;
	if (pathname=="/suggestions") {
	    Suggestion.loadSuggestion();
	}else if(pathname=="/user_view/suggestions"){
		//Suggestion
	    $("body").on("keyup","#txtSearchSuggestionDatatable",function(e){
            if (e.keyCode == "13") {
    	        $("#loadSuggestion").dataTable().fnDestroy();
    	        jsonSuggestionData['search_value'] = $(this).val();
    	        Suggestion.defaultData();
            }
	    })

        $("body").on("click","#btnSearchSuggestionDatatable",function(e){
                $("#loadSuggestion").dataTable().fnDestroy();
                jsonSuggestionData['search_value'] = $("#txtSearchSuggestionDatatable").val();
                Suggestion.defaultData();
        })

		Suggestion.defaultData();
	}
	$("body").on("click","#loadSuggestion th",function(){
        var cursor = $(this).css("cursor");
        jsonSuggestionData['column-sort'] = $(this).attr("field_name");
        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest("#loadSuggestion").find(".sortable-image").html("");
        if(indexcolumnSort==-1){
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonSuggestionData['column-sort-type'] = "ASC";
        }else{
            columnSort.splice(indexcolumnSort,1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonSuggestionData['column-sort-type'] = "DESC";
        }
        
        $("#loadSuggestion").dataTable().fnDestroy();
        Suggestion.defaultData();
    })

    $(".searchSuggestionLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonSuggestionData['limit'] = val;
            $("#loadSuggestion").dataTable().fnDestroy();
            // var self = this;
            var oTable = Suggestion.defaultData();
        // }
    })
})
Suggestion = {
	loadSuggestion : function(){
		//code here
		var oTable = $('#loadSuggestion').dataTable( {
                     "bProcessing": true,
                     "bRetrieve": true,
                     "aaSorting" : [[2,"desc"]],
                     //"bPaginate": true,//"sEcho":3,//"iTotalDisplayRecords":10,//"iTotalRecords":"10",
                     //"iTotalDisplayRecords":"10",//"bServerSide": true,
                     "sAjaxSource": "/ajax/suggestion",
                     "sPaginationType": "full_numbers",
                     "sAjaxDataProp": "company_user",
                     "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                     "aoColumns": [
                         { "mData": "title" },
                         { "mData": "message" },
                         { "mData": "datetime" },
		 				 { "mData": "status" },
                         // { "mData": "actions" }
                     ],
                     "fnRowCallback": function( nRow, aData, iDisplayIndex ) {
                         $("#example_length > label > select").addClass("selectlength");
                     },
                     "aoColumnDefs": [ {
                         "bSortable": true, "aTargets": [ 0,1,2,3 ]
                         },{
                             "bSortable": false, "aTargets": [ "_all" ]
                         }/*, {
                             "bSearchable": true, "aTargets": [ 0 ]
                         }, {
                             "bSearchable": false, "aTargets": [ "_all" ]
                     }*/
                     ],
                     "fnServerData": function (sSource, aoData, fnCallback) {
                         $.ajax({
                             type: "POST",
                             cache: false,
                             dataType: 'json',
                             url: sSource,
                             data: {action:"loadSuggestion"},
                             success: function (json) {
                                 // Retrieve data
                                 //console.log(json)
                                 fnCallback(json);
                             }
                         });
                     }
                 });
                return oTable;
	},
	viewFullSuggestion : function(){
		$(".viewFullSuggestion").click(function(){

		})
	},
	removeSuggestion : function(){
		//code here
		$(".deleteFullSuggestion").click(function(){

		})
	},
	"defaultData" : function(){
        var oColReorder = {
            "iFixedColumns": 0
        };
        var oTable = $('#loadSuggestion').dataTable( {
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/suggestion",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "limit", "value": jsonSuggestionData['limit']});
                aoData.push({"name": "search_value", "value": $.trim(jsonSuggestionData['search_value'])});
                aoData.push({"name": "action", "value": "loadSuggestionDatatable"});
                aoData.push({"name": "column-sort", "value": jsonSuggestionData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonSuggestionData['column-sort-type']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {

                var obj = '#loadSuggestion';
                var listOfappContainer = '.fl-list-of-app-record-wrapper';
               dataTable_widget(obj, listOfappContainer);
               dataTableSetwidth(this.find('[field_name="display_name"]'), 130);
            },
            fnDrawCallback : function(){
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                set_dataTable_th_width([1]);
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonSuggestionData['limit']),
        });
        return oTable;
    }
}