tagging = {
    // Allow Tag on the form
    allow_form_tagging : function(elements){
	$("body").on("click",elements,function(){
	    $form_size_width = $(".form_size_width").val();
	    $form_size_height = $(".form_size_height").val();
	    if ($(this).is(":checked")) {
		var tagging_border = '<div class="start_tag" style="border:1px solid;opacity:0.4;background-color: #fff;width: '+ $form_size_width + 'px;height: ' + $form_size_height + 'px;z-index: 11111;position: absolute;opacity: 0.4;"></div>';
		$(".workspace").append(tagging_border);
		$("#formControls,#formControls").hide();
		$("#allow_form_tagging").val("1");
	    }else{
		con = "Are you sure do you want to remove the tagging?";
		var newConfirm = new jConfirm(con, 'Confirmation Dialog', '', '', '', function(r) {
		    if (r == true) {
			$(".start_tag").remove();
			$(".TaggedImage").remove();
			$("#formControls,#formControls").show();
			$(".configTag").remove();
			$("#allow_form_tagging").val("0");
		    }
    
		});
		newConfirm.themeConfirm("confirm2",{'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'})
		     
	    }
	});
	
    },
    
    start_tag : function(elements){
	var count = "1";
	$("body").on("click",elements,function(e){
	    //console.log(e);
	    //console.log("Width:" + e.pageX);
		height = $(this).height(); // Image Height
		imageTop = e.offsetY; // Get Top of the selected image where you want to tagged it.
		imageLeft = e.offsetX; // Get left of the selected image where you want to tagged it.
		$('<div class="TaggedImage" data-id="' + count + '" id="TaggedImage_' + count +'" style="z-index:11111;width:100%; background-color:#fff;position:absolute;height:100%;border:1px solid;opacity:0.4;"></div>').insertAfter(this);	
	       // Set where you want to tagged the selected images
		$("#TaggedImage_" + count).append('<div class="TagDrag" data-id="' + count + '" data-properties = "#properties_' + count + '"  id="TagDrag_' + count + '" data-image-id="" style="background-color:#000;top:'+imageTop+'px;left:'+imageLeft+'px;min-width:70px;min-height:70px;width:100px;position:absolute;height:70px;border:3px dotted #fff;opacity:1;">',
		'</div>');
	       
		$("#TagDrag_" + count).draggable({
			 containment: this,
			 scroll: false
		}).css("cursor","move");
		$("#TagDrag_" + count).resizable({
			 containment: this,
			 scroll: false
			 
		}).css("cursor","move");
		//alert($("#TagDrag_" + count).height() / 2)
		$("#TagDrag_" + count).append('<div class="" data-id="' + count + '" id="properties_' + count + '" style="width: 50%;margin: 0 auto;text-align: center;"><i class="fa fa-check properties_actions cursor tip" data-original-title="Set this tag" data-action-type="ok" style="color:#fff;"></i>&nbsp;<i class="fa fa-pencil properties_actions cursor tip" data-original-title="Show Yard Properties" data-action-type="edit" style="color:#fff;"></i>&nbsp;<i class="fa fa-trash-o properties_actions cursor tip" data-original-title="Delete Tag" data-action-type="delete" style="color:#fff;"></i></div>');
		$(".tip").tooltip();
		count++;
      })
    },
    
    tag_properties : function(elements){
	$("body").on("click",elements,function(){
	    var action_type = $(this).attr("data-action-type");
	    var dataID = $(this).parent().attr("data-id");
	    var current_tag = $("#TagDrag_" + dataID);
	    var tag_width = current_tag.css("width");
	    var tag_height = current_tag.css("height");
	    var tag_top = current_tag.css("top");
	    var tag_left = current_tag.css("left");
	    var tag_bg = current_tag.css("background-color");
	   
	    if (action_type == "edit") {
		var data_info = $("body").data("data_" + dataID);
		if (data_info) {
		    var info = data_info;
		}else{
		    var info = "";
		}
		var reference = {
		    "tag_id" : dataID
		};
		 
		tag_propertiest(info,reference);
	    }else if (action_type == "delete") {
		con = "Are you sure do you want to delete this?";
		var newConfirm = new jConfirm(con, 'Confirmation Dialog', '', '', '', function(r) {
		    if (r == true) {
			$("#TaggedImage_" + dataID).remove();
		    }
		});
		newConfirm.themeConfirm("confirm2",{'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'})
	    }else if (action_type == "ok") {
		$(this).parent().parent().removeClass("tagConfigure");
		var statusBlink = "";
		$("<div style='position:absolute;z-index: 11111;'><div class='configTag reconfigTag' id='configTag_"+dataID+"' data-image-id='' data-tag-id='"+dataID+"' style='border:3px solid #000;opacity:0.4;background-color:"+tag_bg+";cursor:pointer;top:"+tag_top+";left:"+tag_left+";position:absolute;width:"+tag_width+";height:"+tag_height+";'></div></div>").insertAfter(".start_tag");
		$(".tip").tooltip('destroy');
		$("#TaggedImage_" + dataID).remove();
		
		
	    }
	})
    },
    
    tag_reconfig : function(elements,tag_drop){
	$("body").on("dblclick",elements,function(){
	    
	    // Count if theres an active tag need to be set before to configure other tag
	    var count_tag_configure = $(".tagConfigure").length;
	    if (count_tag_configure != "0") {
		jAlert('Please set other active tag before you reconfigure other tags.', 'Validation', '', '', '', function() {});
	    }else{
		$new_class = "tagConfigure";
	    
		var current_tag = $(this);
		var tag_width = current_tag.css("width");
		var tag_height = current_tag.css("height");
		var tag_top = current_tag.css("top");
		var tag_left = current_tag.css("left");
		var tag_bg = current_tag.css("background-color");
		var dataID = $(this).attr("data-tag-id");
    
		$('<div class="TaggedImage" data-id="' + dataID + '" id="TaggedImage_' + dataID +'" style="z-index:11111;width:100%; background-color:#fff;position:absolute;height:100%;border:1px solid;opacity:0.4;"></div>').insertAfter(tag_drop);	
		// Set where you want to tagged the selected images
		 $("#TaggedImage_" + dataID).append('<div class="TagDrag ' + $new_class + ' " data-id="' + dataID + '" data-properties = "#properties_' + dataID + '"  id="TagDrag_' + dataID + '" data-image-id="" style="background-color:#000;top:'+tag_top+';left:'+tag_left+';min-width:70px;min-height:70px;width:' + tag_width + ';position:absolute;height:' + tag_height + ';border:3px dotted #fff;opacity:1;">',
		 '</div>');
		 $("#TagDrag_" + dataID).draggable({
			  containment: tag_drop,
			  scroll: false
		 }).css("cursor","move");
		 $("#TagDrag_" + dataID).resizable({
			  containment: tag_drop,
			  scroll: false
		 }).css("cursor","move");
		 //alert($("#TagDrag_" + count).height() / 2)
		 $("#TagDrag_" + dataID).append('<div class="" data-id="' + dataID + '" id="properties_' + dataID + '" style="width: 50%;margin: 0 auto;text-align: center;"><i class="fa fa-check properties_actions cursor tip" data-original-title="Set this tag" data-action-type="ok" style="color:#fff;"></i>&nbsp;<i class="fa fa-pencil properties_actions cursor tip" data-original-title="Show Yard Properties" data-action-type="edit" style="color:#fff;"></i>&nbsp;<i class="fa fa-trash-o properties_actions cursor tip" data-original-title="Delete Tag" data-action-type="delete" style="color:#fff;"></i></div>');
		 $(".tip").tooltip();
		 
		 
		 $(this).remove();
	    }
	    
	})
    },
    
    save_tag_properties : function(elements){
	$("body").on("click",elements,function(){
	    var data_id = $(this).attr("data-id");
	    var customer = $('input[name="customer"]').val();
	    var desciption = $('input[name="desciption"]').val();
	    var container_number = $('input[name="container_number"]').val();
	    var type = $('input[name="type"]').val();
	    var tag_class = $('input[name="tag_class"]').val();
	    var tag_length = $('input[name="tag_length"]').val();
	    var tag_width = $('input[name="tag_width"]').val();
	    var tag_height = $('input[name="tag_height"]').val();
	    var tag_time_storage = $('input[name="tag_time_storage"]').val();
	    var tag_date_storage = $('input[name="tag_date_storage"]').val();
	   
	    var tags = {};
	    tags['tag_id'] = data_id;
	    tags['tag_customer'] = customer;
	    tags['tag_desciption'] = desciption;
	    tags['tag_container_number'] = container_number;
	    tags['tag_type'] = type;
	    tags['tag_class'] = tag_class;
	    tags['tag_length'] = tag_length;
	    
	    tags['tag_time_storage'] = tag_time_storage;
	    tags['tag_date_storage'] = tag_date_storage;
	    
	    tags['tag_slot_width'] = tag_width;
	    tags['tag_slot_height'] = tag_height;
	    
	    tags['tag_height'] = $("#TagDrag_" + data_id).css("height");
	    tags['tag_width'] = $("#TagDrag_" + data_id).css("width");
	    tags['tag_top'] = $("#TagDrag_" + data_id).css("top");
	    tags['tag_left'] = $("#TagDrag_" + data_id).css("left");
	    
	    $("body").data("data_" + data_id, tags);
	    console.log($("body").data("data_" + data_id))
	    //$bg_color = $(".chngColor").val();
	    $(".fl-closeDialog").trigger('click');
	    //$("#popup_container").remove();
	})
    },
    
    load_tag : function(){
	var pathname = window.location.pathname;
	if (pathname == "/user_view/formbuilder") {
	
	    $.ajax({
		type	: "POST",
		url	: "/ajax/tagging",
		data	: {"action":"load_tag"},
		success : function(e){
		    var data_json = JSON.parse(e);
		    // Load
		    for (var i = 0; i < data_json['get_query'].length; i++) {
			var tag_id = data_json['get_query'][i]['tag_id'];
			console.log(tag_id,data_json['get_query'][i])
			$("body").data("data_" + tag_id,data_json['get_query'][i]);
		    }
		}
	    });
	}
    }
    
}