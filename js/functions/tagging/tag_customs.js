$(document).ready(function(){

    // Allow Taggin on the form
    tagging.allow_form_tagging("#allow_form_tagging");
    // Start Tagging
    tagging.start_tag(".start_tag");
    // Tag Properties
    tagging.tag_properties(".properties_actions");
    // Reconfigure Tag
    tagging.tag_reconfig(".reconfigTag",".start_tag");
    // Save Properties of the tag
    tagging.save_tag_properties("#save_tag_properties");
    // Load Tag on the form
    tagging.load_tag();
});
