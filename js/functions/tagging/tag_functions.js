

function tag_propertiest(data_info,reference) {
    var ret = "";
    ret += '<h3 class="fl-margin-bottom"><i class="icon-cogs"></i> Yard Properties</h3>';
    ret += '<div class="hr"></div>';
    ret += '<div class="content-dialog-scroll">';
	ret += '<div class="fl-floatLeft fl-form-prop-cont-wid">';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Customer: </div>';
		     ret += '<div class="input_position">';
			var tag_customer = "";
			if(data_info['tag_customer']){
			    tag_customer = data_info['tag_customer'];
			}
			 ret += '<input type="text" name="customer" id="" value="' + tag_customer + '" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Description:: </div>';
		     ret += '<div class="input_position">';
			var tag_desciption = "";
			if(data_info['tag_desciption']){
			    tag_desciption = data_info['tag_desciption'];
			}
			 ret += '<input type="text" name="desciption" id="" value="' + tag_desciption + '" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Container Number: </div>';
		     ret += '<div class="input_position">';
			var tag_container_number = "";
			if(data_info['tag_container_number']){
			    tag_container_number = data_info['tag_container_number'];
			}
			 ret += '<input type="text" name="container_number" id="" value="' + tag_container_number + '" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Type: </div>';
		     ret += '<div class="input_position">';
			var tag_type = "";
			if(data_info['tag_type']){
			    tag_type = data_info['tag_type'];
			}
			 ret += '<input type="text" name="type" id="" value="' + tag_type + '" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Class: </div>';
		     ret += '<div class="input_position">';
			var tag_class = "";
			if(data_info['tag_class']){
			    tag_class = data_info['tag_class'];
			}
			 ret += '<input type="text" name="tag_class" id="" value="' + tag_class + '" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 
	ret += '</div>';
	ret += '<div class="fl-floatRight fl-form-prop-cont-wid">';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Length: </div>';
		     ret += '<div class="input_position">';
			var tag_length = "";
			if(data_info['tag_length']){
			    tag_length = data_info['tag_length'];
			}
			 ret += '<input type="text" name="tag_length" id="" value="' + tag_length + '" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Width: </div>';
		     ret += '<div class="input_position">';
			var tag_slot_width = "";
			if(data_info['tag_slot_width']){
			    tag_slot_width = data_info['tag_slot_width'];
			}
			 ret += '<input type="text" name="tag_width" id="" value="' + tag_slot_width + '" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Height: </div>';
		     ret += '<div class="input_position">';
			var tag_slot_height = "";
			if(data_info['tag_slot_height']){
			    tag_slot_height = data_info['tag_slot_height'];
			}
			 ret += '<input type="text" name="tag_height" id="" value="' + tag_slot_height + '" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Time in Storage: </div>';
		     ret += '<div class="input_position">';
			var tag_time_storage = "";
			if(data_info['tag_time_storage']){
			    tag_time_storage = data_info['tag_time_storage'];
			}
			 ret += '<input type="text" name="tag_time_storage" id="' + tag_time_storage + '" value="" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Start date of storage: </div>';
		     ret += '<div class="input_position">';
			var tag_date_storage = "";
			if(data_info['tag_date_storage']){
			    tag_date_storage = data_info['tag_date_storage'];
			}
			 ret += '<input type="text" name="tag_date_storage" id="' + tag_date_storage + '" value="" class="form-text obj_prop">';
		     ret += '</div>';
		 ret += '</div>';
		 ret += '<div class="Fields fl-margin-bottom">';
		     ret += '<div class="label_below2"> Yard Background Color: </div>';
		     ret += '<div class="input_position">';
			 ret += '<input type="text" data-type="" class="chngColor" data-properties-type="lblFontolor" data-type="label"/>';
		     ret += '</div>';
		 ret += '</div>';
		 
	ret += '</div>';
    ret += '</div>';
    ret += '<div class="fields">';
	ret += '<div class="label_basic"></div>';
	ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
	    ret += '<input type="button" data-type="" data-type-id="" class="btn-blueBtn  fl-margin-right" data-id="' + reference['tag_id'] + '"  id="save_tag_properties" value="Save"> ';
	    ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
	ret += '</div>';
    ret += '</div>';
    var newDialog = new jDialog(ret, "", "600", "", "", function() {
    });
    newDialog.themeDialog("modal2");
    $('.content-dialog-scroll').perfectScrollbar();
    $( 'input[name="tag_date_storage"]' ).datepicker({dateFormat: 'yy-mm-dd'}).datepicker("setDate",new Date());
    $( 'input[name="tag_time_storage"]' ).timepicker({
	    timeFormat: "H:mm:ss"
    }).datepicker("setDate",new Date());
    $(".chngColor").spectrum({
	preferredFormat : "Hex"
	});
}