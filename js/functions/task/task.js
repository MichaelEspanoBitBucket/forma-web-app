

task = {
    
    create_task : function(){
        $("body").on("click",".cNewTask",function(){
            
            var ret = "";
                    ret += '<h3><i class="icon-reorder"></i> Create Task</h3>';
                    ret += '<div class="hr"></div>';
            ret += '<div style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="task-container">';
                ret += '<form id="taskAction" method="POST">';    
                    ret += '<div style="float: left;width:50%;">';
                        ret += '<div class="fields_below" style="display: inline-block;">';
                            ret += '<div class="label_below">Date Created: <font color="red">*</font></div>';
                            ret += '<div class="input_position_below">';
                                ret += '<input type="text" name="task_date_created" id="dateCreated" class="task_value form-text task_date" placeholder="Select Date">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float:right;width:45%;margin-right:5px;">';
                        ret += '<div class="fields_below" style="display: inline-block;">';
                            ret += '<div class="label_below">Date Closed: </div>';
                            ret += '<div class="input_position_below" style="text-align: right;">';
                                ret += '<input type="text" name="task_date_closed" id="dateClosed" class=" form-text task_date" placeholder="Select Date">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields_below" style="display: inline-block;">';
                        ret += '<div class="label_below">Project: <font color="red">*</font></div>';
                        ret += '<div class="input_position_below">';
                            ret += '<input type="text" name="task_project" id="projectName" class="task_value form-text" placeholder="Input project Name">';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields_below" style="display: inline-block;">';
                        ret += '<div class="label_below">Task: <font color="red">*</font></div>';
                        ret += '<div class="input_position_below">';
                            ret += '<input type="text" name="task_name" id="taskName" class="task_value form-text" placeholder="Input task name">';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields_below" style="display: inline-block;">';
                        ret += '<div class="label_below">Description of task: </div>';
                        ret += '<div class="input_position_below">';
                            ret += '<textarea  name="task_description" id="taskDescription" class=" form-textarea_save" placeholder="Input task description" style="resize: none;"></textarea>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields_below" style="display: inline-block;">';
                        ret += '<div class="label_below">Team : <font color="red">*</font></div>';
                        ret += '<div class="input_position_below">';
                            //ret += '<input type="text" name="task_team" name="companyName" class="task_value form-text" placeholder="Choose user for a team">';
                            ret += '<div id="teamList" class=" task_value_users" style="background-color:white;float: left;border: 1px solid #ddd;min-height: 40px;width:480px;margin-top: 5px;padding-bottom: 8px;">';
                                ret += '<input type="text"  name="task_team" id="teamMember" class="task_value_user input-small " placeholder="Choose user for a team" data-tag-id="0" style="border: none;box-shadow:none;margin-left: 12px;margin-top: 8px;">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float: left;width: 100%;">';
                        ret += '<div id="displayTeamUser"></div>';
                    ret += '</div>';
                    ret += '<div style="float: left;width:50%;">';
                        ret += '<div class="fields_below" style="display: inline-block;">';
                            ret += '<div class="label_below">Status: <font color="red">*</font></div>';
                            ret += '<div class="input_position_below">';
                                ret += '<select class="task_value form-select " id="taskStatus" name="task_status"  placeholder="Select status">';
                                    ret += '<option value="">----------------------------Select----------------------------</option>';
                                    ret += '<option value="1">Open</option>';
                                    ret += '<option value="2">On Going</option>';
                                    ret += '<option value="3">Closed</option>';
                                    ret += '<option value="4">Waiting</option>';
                                ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float:right;width:45%;margin-right:5px;">';
                        ret += '<div class="fields_below" style="display: inline-block;">';
                            ret += '<div class="label_below">Due Date: <font color="red">*</font></div>';
                            ret += '<div class="input_position_below" style="text-align: right;">';
                                ret += '<input type="text" name="task_due_date"  id="dueDate" class="task_value form-text task_date" placeholder="Choose date">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields_below" style="display: inline-block;">';
                        ret += '<div class="label_below">Remarks: </div>';
                        ret += '<div class="input_position_below">';
                            ret += '<textarea  class=" form-textarea_save"  id="taskRemarks" name="task_remarks" placeholder="Input remarks" style="resize: none;"></textarea>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="text-align: right;font-weight:bold;">Authorization</div>';
                    ret += '<div class="hr"></div>';
                    ret += '<div class="fields_below" style="display: inline-block;">';
                        ret += '<div class="label_below">Editor(s) : <font color="red">*</font></div>';
                        ret += '<div class="input_position_below">';
                            //ret += '<input type="text" name="task_editos" class="task_value form-text" placeholder="Choose editor of your task">';
                            ret += '<div id="teamEditors" class=" task_value_users" style="background-color:white;float: left;border: 1px solid #ddd;min-height: 40px;width:480px;margin-top: 5px;padding-bottom: 8px;">';
                                ret += '<input type="text"  name="task_editos" id="taskEditors" class="task_value_user input-small " placeholder="Choose editor of your task" data-tag-id="0" style="border: none;box-shadow:none;margin-left: 12px;margin-top: 8px;">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float: left;width: 100%;">';
                        ret += '<div id="displaytaskEditors"></div>';
                    ret += '</div>';
                    ret += '<div class="fields_below" style="display: inline-block;">';
                        ret += '<div class="label_below">Viewer(s) : <font color="red">*</font></div>';
                        ret += '<div class="input_position_below">';
                            //ret += '<input type="text" name="task_viewers" class="task_value form-text" placeholder="Choose viewer of your task">';
                            ret += '<div id="teamViewers" class=" task_value_users" style="background-color:white;float: left;border: 1px solid #ddd;min-height: 40px;width:480px;margin-top: 5px;padding-bottom: 8px;">';
                                ret += '<input type="text"  name="task_viewers" id="taskViewers" class="task_value_user input-small " placeholder="Choose viewer of your task" data-tag-id="0" style="border: none;box-shadow:none;margin-left: 12px;margin-top: 8px;">';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float: left;width: 100%;">';
                        ret += '<div id="displaytaskViewers"></div>';
                    ret += '</div>';
                ret += '</form>';
                ret += '<button class="btn-basicBtn padding_5 cursor tip" data-original-title="Add Files" style="margin-top: 3px;">';
                                ret += '<form id="taskFile_0" method="post" enctype="multipart/form-data" action="/ajax/uploadFile">';
                                    ret += '<div class="postActions" style="margin-left: 5px;margin-top: 5px;">';
                                        ret += '<div id="uniform-fileInput" class="uploader">';
                                            ret += '<input type="file" data-action-id="0" value="upload" name="postFile" id="uploadForm_0" size="24" data-get-file="imagePost_0" data-show="imagePreviewUpload_0" data-form="taskFile_0" data-location="previewImagePostUpload_0" data-action-type="postFile" style="opacity: 0;" class="postFile">';
                                            ret += '<span id="uploadFilename_0" class="filename">No file selected</span>';
                                            ret += '<span class="action">Choose File</span>';
                                            ret += '<input type="hidden" value="taskFileUpload" name="uploadType">';
                                            ret += '<input type="hidden" value="taskUpload" name="uploadFolder" id="taskUploadFolder">';
                                        ret += '</div>';
                                        ret += '<img src="/images/loader/loader.gif" class="postFileLoad pull-left display" style="margin-left: 5px;margin-top:6px;">';
                                    ret += '</div>';
                                ret += '</form>';
                            ret += '</button>';
                    ret += '<div class="imagePreviewUpload_0 display" style="float: left;width: 100%;border: 1px solid #D5D5D5;margin-top:10px;background-color: #fff;margin-bottom: 5px;"> <div style="padding: 5px;" class="previewImagePostUpload_0"></div></div>';
                    
            ret += '</div>';
            ret += '<div class="hr"></div>';
            ret += '<div class="fields">';
                ret += '<div class="label_basic"></div>';
                ret += '<div class="input_position" style="margin-top:5px;text-align:right;"><img src="/images/loader/load.gif" class="display saveFormLoad" style="margin-right:5px;margin-top:2px;">';
                    ret += '<input type="button" class="btn-blueBtn save_tasks" data-type="save" data-get-file="imagePost_0" data-location="taskUpload" data-file="imagePreviewUpload_0"  id="save_tasks" value="Save"> ';
                    ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
                ret += '</div>';
            ret += '</div>';
            
            jDialog(ret, "", "500", "", "", function() { });
            
            $(".task-container").scrollTop(0);
            $(".task-container").perfectScrollbar('update');
            $(".task-container").perfectScrollbar();
            $("#teamMember").autoComplete({
                    table_search: "tbuser",
                    search_type:"users",
                    event:"#teamMember",
                    display:"#displayTeamUser",
                    allow_select: true,
                    allow_delete: true
            });
            $("#taskEditors").autoComplete({
                    table_search: "tbuser",
                    search_type:"users",
                    event:"#taskEditors",
                    display:"#displaytaskEditors",
                    allow_select: true,
                    allow_delete: true
            });
            $("#taskViewers").autoComplete({
                    table_search: "tbuser",
                    search_type:"users",
                    event:"#taskViewers",
                    display:"#displaytaskViewers",
                    allow_select: true,
                    allow_delete: true
            });
            var dateToday = new Date();
            $(".task_date").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: dateToday
            });
            $("#projectName").focus();
            //$(this).attr("data-tag-id")=="0"
            // Compute null value
                $("body").on("keyup, change",".task_value",function(){
                    if($(this).val()==""){
                        requiredFields(this);
                    }else{
                        $(this).removeClass("frequired-red");
                    }    
                });
                $("body").on("change",".task_value_user",function(){
                    if($(this).val()==""){
                        requiredFields(this);
                        $(this).parent().addClass("frequired-red");
                    }else{
                        $(this).removeClass("frequired-red");
                        $(this).parent().removeClass("frequired-red");
                    }    
                });
                
                $(".task_container").html(null);
        });
    },
    
    // Save task
    
    save_tasks : function(){
        $("body").on("click",".save_tasks",function(){
            var task_fields_val = {};
            var count_field = "";
            var type = $(this).attr("data-type");
            $(".task_value").each(function(){
                if($(this).val()==""){
                    var pholder = $(this).attr("placeholder");
                    var field_position = $(this).offset().top;
                    $(".task-container").scrollTop(field_position);
                    $(".task-container").perfectScrollbar('update');
                    requiredFields(this);
                    count_field = $(this).length;
                }else{
                    //console.log($(this).attr("data-tag-id"));
                    $(this).removeClass("frequired-red");
                    task_fields_val[$(this).attr("name")] = $(this).val();
                    //task_fields_val.push({"task_name_id":$(this).attr("name"),
                    //                 "task_value":$(this).val()});
                    
                }
                
            });
            $(".task_value_user").each(function(){
                if($(this).attr("data-tag-id")=="0"){
                    var pholder = $(this).attr("placeholder");
                    var field_position = $(this).offset().top;
                    $(".task-container").scrollTop(field_position);
                    $(".task-container").perfectScrollbar('update');
                    requiredFields(this);
                    $(this).parent().addClass("frequired-red");
                    count_field = $(this).length;
                }else{
                    //console.log($(this).attr("data-tag-id"));
                    $(this).removeClass("frequired-red");
                    $(this).parent().removeClass("frequired-red");
                    task_fields_val[$(this).attr("name")] = $(this).attr("data-tag-id");
                    //task_fields_val.push({"task_name_id":$(this).attr("name"),
                    //                 "task_value":$(this).attr("data-tag-id")});
                    
                }
                
            });
            
            // Validation
                if (count_field=="1") {
                    showNotification({
                        message: "Please complete all required fields.",
                        type: "error",
                        autoClose: true,
                        duration: 3
                    });
                }else{
                    task_fields_val['task_description'] = $('textarea[name="task_description"]').val();
                    task_fields_val['task_remarks'] = $('textarea[name="task_remarks"]').val();
                    
                    if (type=="save") {
                        task_fields_val['action'] = "insertTask";
                        var dataString = task_fields_val;
                        var msg = "Your task was successfully created."
                    }else if(type=="update"){
                        task_fields_val['action'] = "updateTask";
                        var dataID = $(this).attr("data-id");
                        task_fields_val['taskID'] = dataID;
                        task_fields_val['createdBy'] = $('input[name="task_createdBy"]').val();
                        
                        var dataString = task_fields_val;
                        var msg = "Your task was successfully updated.";
                    }
                    
                    //upload
                    // File attachment
                    var file_name = $(this).attr("data-file");
                    var files = $(this).attr("data-get-file");
                    var img = {};
                    $("." + files).each(function(eqindex){
                        img['name_' + eqindex] = $(this).attr("data-name");
                    });
                    var json_encode = JSON.stringify(img);
                    
                        task_fields_val['folder_location'] = $('#taskUploadFolder').val();
                        task_fields_val['imagesGet'] = json_encode;
                        
                     //console.log(count_field)
                     
                    $.ajax({
                        type    :   "POST",
                        url     :   "/ajax/tasks",
                        data    :   dataString,
                        dataType:   "json",
                        success :   function(e){
                            console.log(e)
                            showNotification({
                                message: msg,
                                type: "success",
                                autoClose: true,
                                duration: 3
                            });
                            task.loadTask();
                            if (type=="save") {
                                $("#popup_cancel").trigger("click");
                            }
                            //console.log(e)
                        }
                    });
                    //console.log(dataString);
                }
            
            
           
        });
    },
    
    loadTask : function(){
        var ret = "";
        $.ajax({
            type    :   "POST",
            url     :   "/ajax/tasks",
            dataType:   "json",
            cache   :   false,
            data    :   {action:"loadTask"},
            success :   function(data){
//                console.log(data)
                if(data=="null"||data==null||data==""){
                    $(".task_list > div > img").hide();
                }else{
                    var task = data;
                    var task_length = task.length;
                    for (var a = 0; a<task_length; a++) {
                        //console.log(task[a].Tasks);
                        var task_record = task[a].Tasks;
                        $("body").data("tasks_" + task_record.tID, task_record);
                        
                        ret += task_container(task_record);
                        $(".task_list > div > img").hide();
                    }
                    $(".task_list").html(ret);
                    $(".task-container").perfectScrollbar();
                    var pathname = window.location;
                        if (getParametersName("view_type", pathname)=="view_task") {
                            var id = getParametersName("taskID", pathname);
                          
                                $("#viewTask_" + id).trigger("click");
                        }       
                }
                
            }
        });
        
    },
    
    
    updateTask : function(){
        $("body").on("click",".update_task",function(){
            
            var dataID = $(this).attr("data-id");
            var dataObject = $("body").data("tasks_" + dataID);
            
            $(".msg_container").html(task_update(dataObject));
            $(".group_1_" + dataObject.tID).colorbox({rel:'group_1_' + dataObject.tID,width:"75%", height:"80%"});
            var dateToday = new Date();
            $(".utask_date").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: dateToday
            });
            $("#uteamMember").autoComplete({
                    table_search: "tbuser",
                    search_type:"users",
                    event:"#uteamMember",
                    display:"#udisplayTeamUser",
                    allow_select: true,
                    allow_delete: true
            });
            $("#utaskEditors").autoComplete({
                    table_search: "tbuser",
                    search_type:"users",
                    event:"#utaskEditors",
                    display:"#udisplaytaskEditors",
                    allow_select: true,
                    allow_delete: true
            });
            $("#utaskViewers").autoComplete({
                    table_search: "tbuser",
                    search_type:"users",
                    event:"#utaskViewers",
                    display:"#udisplaytaskViewers",
                    allow_select: true,
                    allow_delete: true
            });
            $("#uprojectName").focus();
            $('.task-container').perfectScrollbar('destroy');
            
        });
    },
    
    view_task : function(){
        $("body").on("click",".view_task",function(){
           $(".msg_container").html(null);
           var dataID = $(this).attr("data-id");
           var dataObject = $("body").data("tasks_" + dataID);
           $(".msg_container").html(task_view(dataObject));
           $(".removeTag_uteamMember").removeClass("removeTag_uteamMember");
        });
    },
    
    delete_task : function(){
        $("body").on("click",".delete_task",function(){
            var dataID = $(this).attr("data-id");
            
            con = "Are you sure do you want to delete this?";
            jConfirm(con, 'Confirmation Dialog','', '', '', function(r){
                if(r==true){
                    $.post("/ajax/tasks",{action:"deleteTask",dataID:dataID},function(data){});
                    $("#task_" + dataID).animate({ backgroundColor: "whiteSmoke" }, "fast")
                                .animate({ opacity: "hide" }, "slow");
                }
                
            });
            
        });
    },
    
    updateSelect : function(){
        $("body").on("change",".updateSelect",function(){
            var sel = $(".updateSelect option:selected").val();
            if (sel == "3") {
                $("input[name='task_date_closed']").addClass("task_value");
                $("input[name='task_date_closed']").val(null)
            }else{
                $("input[name='task_date_closed']").removeClass("task_value");
                $("input[name='task_date_closed']").val(null)
            }
            $(".task_value").each(function(){
                if($(this).val()==""){
                    var pholder = $(this).attr("placeholder");
                    var field_position = $(this).offset().top;
                    $(".task-container").scrollTop(field_position);
                    $(".task-container").perfectScrollbar('update');
                    requiredFields(this);
                    count_field = $(this).length;
                }else{
                    //console.log($(this).attr("data-tag-id"));
                    $(this).removeClass("frequired-red");
                    task_fields_val[$(this).attr("name")] = $(this).val();
                    //task_fields_val.push({"task_name_id":$(this).attr("name"),
                    //                 "task_value":$(this).val()});
                    
                }
                
            });
            $(".task_value_user").each(function(){
                if($(this).attr("data-tag-id")=="0"){
                    var pholder = $(this).attr("placeholder");
                    var field_position = $(this).offset().top;
                    $(".task-container").scrollTop(field_position);
                    $(".task-container").perfectScrollbar('update');
                    requiredFields(this);
                    $(this).parent().addClass("frequired-red");
                    count_field = $(this).length;
                }else{
                    //console.log($(this).attr("data-tag-id"));
                    $(this).removeClass("frequired-red");
                    $(this).parent().removeClass("frequired-red");
                    task_fields_val[$(this).attr("name")] = $(this).attr("data-tag-id");
                    //task_fields_val.push({"task_name_id":$(this).attr("name"),
                    //                 "task_value":$(this).attr("data-tag-id")});
                    
                }
                
            });
        });
    }
    
}