

function task_container(task_record) {
    var ret = "";
    var vViewers = task_record.Viewers;
    var vViewers_view = vViewers.split(",");
    
    var vMembers_view = task_record.member;
    var vMiewers_view = vMembers_view.split(",");
    if (jQuery.inArray( task_record.authID, vViewers_view ) != -1 || task_record.authID ==task_record.createdBy || task_record.user_level=="1" || task_record.user_level=="2"
        || jQuery.inArray( task_record.authID, vMiewers_view ) != -1) {
      
        
        ret += '<div class="group-container" style="float:left;" id="task_' + task_record.tID + '" data-id="' + task_record.tID + '">';
            ret += '<div style="float:left;width:73%;">';
                ret += '<div class="gName_list" style="margin-top:7px;margin-left:10px;font-weight:bold">' + task_record.project_title + '</div>';
                ret += '<div class="" style="margin-top:2px;margin-left:10px;font-size:10px;color:#ACAAAA">';
                    if (task_record.status == "1") {
                        var status = "Open";
                    }else if (task_record.status == "2") {
                        var status = "On Going";
                    }else if (task_record.status == "3") {
                        var status = "Closed";
                    }else if (task_record.status == "4") {
                        var status = "Waiting";
                    }
                    ret += '<i>(' + task_record.countMembers +  ' Member`s ) Status - ' + status + '</i>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div style="float:right;width:27%;text-align:right;">';
                ret += '<div class="" style="margin-top:7px">';
                        ret += '<span class="cursor"  style="margin-right:6px">';
                            ret += '<button class="icon-eye-open btn-basicBtn padding_5 cursor view_task" id="viewTask_' + task_record.tID + '" data-id="' + task_record.tID + '"></button>';
                        ret += '</span>';
                    var editors = task_record.Editors;
                    var editor_edit = editors.split(",");
                    
                    if (jQuery.inArray( task_record.authID, editor_edit ) != -1 || task_record.authID ==task_record.createdBy || task_record.user_level=="1" || task_record.user_level=="2"
                        || jQuery.inArray( task_record.authID, vMiewers_view ) != -1) {
                        ret += '<span class="cursor"  style="margin-right:6px">';
                            ret += '<button class="icon-edit btn-basicBtn padding_5 cursor update_task" id="updateTask_' + task_record.tID + '" data-id="' + task_record.tID + '"></button>';
                        ret += '</span>';
                        
                    }
                    if (task_record.authID == task_record.createdBy) {
                        ret += '<span class="cursor"  style="margin-right:6px">';
                            ret += '<button class="icon-trash btn-basicBtn padding_5 cursor delete_task" id="deleteTask_' + task_record.tID + '" data-id="' + task_record.tID + '"></button>';
                        ret += '</span>';
                    }
                ret += '</div>';
            ret += '</div>';
        ret += '</div>';    
    }
    return ret;
}



function task_update(dataObject){
    var ret = "";
    ret += '<form id="taskAction" method="POST">';    
    ret += '<div style="float: left;width:50%;">';
        ret += '<div class="fields_below" style="display: inline-block;">';
            ret += '<div class="label_below">Date Created: <font color="red">*</font></div>';
            ret += '<div class="input_position_below">';
                ret += '<input type="text" name="task_date_created" value="'+ dataObject.date_created +'" id="dateCreated" class="task_value form-text utask_date" placeholder="Select Date">';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    if (dataObject.status == "3") {
        var addC = "task_value";
    }else{
        var addC = "";
    }
    ret += '<div style="float:right;width:45%;margin-right:5px;">';
        ret += '<div class="fields_below" style="display: inline-block;">';
            ret += '<div class="label_below">Date Closed: </div>';
            ret += '<div class="input_position_below" style="text-align: right;">';
                ret += '<input type="text" name="task_date_closed"  value="'+ dataObject.date_closed +'" id="dateClosed" class=" form-text utask_date ' + addC + '" placeholder="Select Date">';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Project: <font color="red">*</font></div>';
        ret += '<div class="input_position_below">';
            ret += '<input type="text" name="task_project" id="uprojectName"  value="'+ dataObject.project_title +'" class="task_value form-text" placeholder="Input project Name">';
            ret += '<input type="hidden" name="task_createdBy" id="createdBy"  value="'+ dataObject.createdBy +'" class="task_value form-text" placeholder="Input project Name">';
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Task: <font color="red">*</font></div>';
        ret += '<div class="input_position_below">';
            ret += '<input type="text" name="task_name" id="taskName"  value="'+ dataObject.task +'" class="task_value form-text" placeholder="Input task name">';
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Description of task: </div>';
        ret += '<div class="input_position_below">';
            ret += '<textarea  name="task_description" id="taskDescription" class=" form-textarea_save" placeholder="Input task description" style="resize: none;">'+ dataObject.description +'</textarea>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Team : <font color="red">*</font></div>';
        ret += '<div class="input_position_below">';
            //ret += '<input type="text" name="task_team" name="companyName" class="task_value form-text" placeholder="Choose user for a team">';
            ret += '<div id="uteamList" class=" task_value_users" style="background-color:white;float: left;border: 1px solid #ddd;min-height: 40px;width:480px;margin-top: 5px;padding-bottom: 8px;">';
                for (var a = 0; a < dataObject.membersName.length; a++) {
                    ret += updateUserTag(dataObject.membersName[a].name,dataObject.membersName[a].id,"#uteamMember");
                }   
                ret += '<input type="text"  name="task_team" id="uteamMember"  class="task_value_user input-small " placeholder="Choose user for a team" data-tag-id="'+ dataObject.member +'" style="border: none;box-shadow:none;margin-left: 12px;margin-top: 8px;">';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="float: left;width: 100%;">';
        ret += '<div id="udisplayTeamUser">';
            
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="float: left;width:50%;">';
        ret += '<div class="fields_below" style="display: inline-block;">';
            ret += '<div class="label_below">Status: <font color="red">*</font></div>';
            ret += '<div class="input_position_below">';
                    // Status
                    if (dataObject.status == "1") {
                        var open = "selected=selected";
                    }else if (dataObject.status == "2") {
                        var going = "selected=selected";
                    }else if (dataObject.status == "3") {
                        var closed = "selected=selected";
                    }else if (dataObject.status == "4") {
                        var waiting = "selected=selected";
                    }
            
                ret += '<select class="task_value form-select updateSelect" id="taskStatus" name="task_status"  placeholder="Select status">';
                    ret += '<option value="">----------------------------Select----------------------------</option>';
                    ret += '<option value="1" ' + open + '>Open</option>';
                    ret += '<option value="2" ' + going + '>On Going</option>';
                    ret += '<option value="3" ' + closed + '>Closed</option>';
                    ret += '<option value="4" ' + waiting + '>Waiting</option>';
                ret += '</select>';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="float:right;width:45%;margin-right:5px;">';
        ret += '<div class="fields_below" style="display: inline-block;">';
            ret += '<div class="label_below">Due Date: <font color="red">*</font></div>';
            ret += '<div class="input_position_below" style="text-align: right;">';
                ret += '<input type="text" name="task_due_date" value="'+ dataObject.due_date +'"  id="dueDate" class="task_value form-text utask_date" placeholder="Choose date">';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Remarks: </div>';
        ret += '<div class="input_position_below">';
            ret += '<textarea  class=" form-textarea_save"  id="taskRemarks" name="task_remarks" placeholder="Input remarks" style="resize: none;">'+ dataObject.remarks +'</textarea>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="text-align: right;font-weight:bold;">Authorization</div>';
    ret += '<div class="hr"></div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Editor(s) : <font color="red">*</font></div>';
        ret += '<div class="input_position_below">';
            //ret += '<input type="text" name="task_editos" class="task_value form-text" placeholder="Choose editor of your task">';
            ret += '<div id="uteamEditors" class=" task_value_users" style="background-color:white;float: left;border: 1px solid #ddd;min-height: 40px;width:480px;margin-top: 5px;padding-bottom: 8px;">';
                for (var a = 0; a < dataObject.editorsName.length; a++) {
                    ret += updateUserTag(dataObject.editorsName[a].name,dataObject.editorsName[a].id,"#utaskEditors");
                }   
                ret += '<input type="text"  name="task_editos" id="utaskEditors" class="task_value_user input-small " placeholder="Choose editor of your task" data-tag-id="'+ dataObject.Editors +'" style="border: none;box-shadow:none;margin-left: 12px;margin-top: 8px;">';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="float: left;width: 100%;">';
        ret += '<div id="udisplaytaskEditors"></div>';
    ret += '</div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Viewer(s) : <font color="red">*</font></div>';
        ret += '<div class="input_position_below">';
            //ret += '<input type="text" name="task_viewers" class="task_value form-text" placeholder="Choose viewer of your task">';
            ret += '<div id="uteamViewers" class=" task_value_users" style="background-color:white;float: left;border: 1px solid #ddd;min-height: 40px;width:480px;margin-top: 5px;padding-bottom: 8px;">';
                for (var a = 0; a < dataObject.viewersName.length; a++) {
                    ret += updateUserTag(dataObject.viewersName[a].name,dataObject.viewersName[a].id,"#utaskViewers");
                }   
                ret += '<input type="text"  name="task_viewers" id="utaskViewers" class="task_value_user input-small " placeholder="Choose viewer of your task" data-tag-id="'+ dataObject.Viewers +'" style="border: none;box-shadow:none;margin-left: 12px;margin-top: 8px;">';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="float: left;width: 100%;">';
        ret += '<div id="udisplaytaskViewers"></div>';
    ret += '</form>';
    ret += '</div>';
    var postImage = dataObject.taskAttachment;
    var valid_formats = ["jpg", "png", "JPG", "PNG", "GIF", "gif"];
    if (postImage.extension_img.length!=0) {
    ret += '<div style="font-weight:bold;">Attachment</div>';
    ret += '<div class="hr"></div>';
    }
    
    if (postImage.extension_img.length!=0) {
            var w = "";
            var h = "";
            // Image
            if (postImage.extension_img.length!=0) {
            var folderName = postImage.postFolderName; 
            var listImg = postImage.img;
            // Image Class
                var imageView = "imageView openImageView";
                //width
                if (postImage.image_resize[0] > 600) {
                    w = "500";
                }else{
                    w = postImage.image_resize[0];
                }
                // Height
                if (postImage.image_resize[1] > 600) {
                    h = "500";
                }else{
                    h = postImage.image_resize[1];
                }
            for (var a in listImg) {
                    var listImg_extension = listImg[a].split(".");
                    var extension = listImg_extension[listImg_extension.length - 1];
                    if (valid_formats.indexOf(extension)!=-1) {
                        
                        if (postImage.lengthImg=="1") {
                            ret += '<center>';
                                ret += '<img src="/images/taskUpload/' + folderName + '/' + listImg[a] + '" class="avatar '+ imageView +' group_1_' + dataObject.tID + '"  href="/images/taskUpload/' + folderName + '/' + listImg[a] + '"  width="'+ w +'" height="'+ h +'">';
                            ret += '</center>';
                        }else if (postImage.lengthImg=="2") {
                            ret += '<img src="/images/taskUpload/' + folderName + '/' + listImg[a] + '" class="avatar '+ imageView +' group_1_' + dataObject.tID + '"  href="/images/taskUpload/' + folderName + '/' + listImg[a] + '"  style="width:40%;">';
                        }else if (postImage.lengthImg=="3") {
                            ret += '<img src="/images/taskUpload/' + folderName + '/' + listImg[a] + '" class="avatar '+ imageView +' group_1_' + dataObject.tID + '"  href="/images/taskUpload/' + folderName + '/' + listImg[a] + '"  style="width:30%;">';
                        }else if (postImage.lengthImg > 4) {
                            ret += '<img src="/images/taskUpload/' + folderName + '/' + listImg[a] + '" class="avatar '+ imageView +' group_1_' + dataObject.tID + '"  href="/images/taskUpload/' + folderName + '/' + listImg[a] + '"  style="width:30%;">';
                        }else if (postImage.lengthImg == "4"){
                            ret += '<img src="/images/taskUpload/' + folderName + '/' + listImg[0] + '" class="avatar '+ imageView +' group_1_' + dataObject.tID + '"  href="/images/taskUpload/' + folderName + '/' + listImg[0] + '"  style="width:30%;">';
                            ret += '<img src="/images/taskUpload/' + folderName + '/' + listImg[1] + '" class="avatar '+ imageView +' group_1_' + dataObject.tID + '"  href="/images/taskUpload/' + folderName + '/' + listImg[1] + '"  style="width:30%;">';
                            ret += '<img src="/images/taskUpload/' + folderName + '/' + listImg[2] + '" class="avatar '+ imageView +' group_1_' + dataObject.tID + '"  href="/images/taskUpload/' + folderName + '/' + listImg[2] + '"  style="width:30%;">';
                            ret += '<img src="/images/taskUpload/' + folderName + '/' + listImg[3] + '" class="avatar '+ imageView +' group_1_' + dataObject.tID + '"  href="/images/taskUpload/' + folderName + '/' + listImg[3] + '"    width="'+ w +'" height="'+ h +'">';
                            
                            break;
                        }
                        
                    }
               }
            }
    }
    if(postImage.extension_file.length!=0){
            // Files
              if(postImage.extension_file.length!=0){
              var folderName = postImage.postFolderName; 
              var listImg = postImage.img;
             
              for (var a in listImg) {
                      var listImg_extension = listImg[a].split(".");
                      var extension = listImg_extension[listImg_extension.length - 1];
                      if (valid_formats.indexOf(extension)==-1) {
                      ret += '<form method="POST" style="text-decoration: underline;"><input type="submit" class="cursor tip" data-original-title="' + listImg[a] + '" style="opacity: 0;position: absolute;width: 100%;"><div class="">';
                              ret += '<a class="cursor apps">';
                              var num = "1";
                                      ret += num++ + ") " + listImg[a];
                              ret += '</a>';
                      ret += '</div>';
                      ret += '<input type="hidden" value="' + listImg[a] + '" name="attachment_filename">';
                      ret += '<input type="hidden" value="images/taskUpload/' + folderName + '/' + listImg[a] + '" name="attachment_location">';
                      ret += '</form>';
                      }
             }
              }
    }
    ret += '<div class="hr"></div>';
    ret += '<div class="fields">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;"><img src="/images/loader/load.gif" class="display saveFormLoad" style="margin-right:5px;margin-top:2px;">';
            ret += '<input type="button" class="btn-blueBtn save_tasks"  id="save_tasks" data-type="update" data-id="' + dataObject.tID + '" value="Save"> ';
            
        ret += '</div>';
    ret += '</div>';
    
    return ret;
}


function task_view(dataObject) {
    var ret = "";
    ret += '<div style="float: left;width:50%;">';
        ret += '<div class="fields_below" style="display: inline-block;">';
            ret += '<div class="label_below">Date Created: </div>';
            ret += '<div class="input_position_below">';
                ret += dataObject.date_created;
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="float:right;width:45%;margin-right:5px;">';
        ret += '<div class="fields_below" style="display: inline-block;">';
            ret += '<div class="label_below">Date Closed: </div>';
            ret += '<div class="input_position_below" style="">';
                ret += dataObject.date_closed;
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="hr"></div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Project: </div>';
        ret += '<div class="input_position_below" style="word-break: break-all;">';
            ret += dataObject.project_title;
            //ret += '<input type="text" name="task_createdBy" id="createdBy"  value="'+ dataObject.createdBy +'" class="task_value form-text" placeholder="Input project Name">';
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="hr"></div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Task: </div>';
        ret += '<div class="input_position_below" style="word-break: break-all;">';
            ret += dataObject.task;
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="hr"></div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Description of task: </div>';
        ret += '<div class="input_position_below" style="word-break: break-all;">';
            ret += dataObject.description;
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="hr"></div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Team : </div>';
        ret += '<div class="input_position_below">';
            //ret += '<input type="text" name="task_team" name="companyName" class="task_value form-text" placeholder="Choose user for a team">';
            //ret += '<div id="uteamList" class=" task_value_users" style="background-color:white;float: left;border: 1px solid #ddd;min-height: 40px;width:480px;margin-top: 5px;padding-bottom: 8px;">';
                for (var a = 0; a < dataObject.membersName.length; a++) {
                    ret += updateUserTag(dataObject.membersName[a].name,dataObject.membersName[a].id,"#uteamMember");
                }   
                //ret += '<input type="text"  name="task_team" id="uteamMember"  class="task_value_user input-small " placeholder="Choose user for a team" data-tag-id="'+ dataObject.member +'" style="border: none;box-shadow:none;margin-left: 12px;margin-top: 8px;">';
            //ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    
    ret += '<div style="float: left;width: 100%;">';
        ret += '<div id="udisplayTeamUser">';
            
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="hr"></div>';
    ret += '<div style="float: left;width:50%;">';
        ret += '<div class="fields_below" style="display: inline-block;">';
            ret += '<div class="label_below">Status: </div>';
            ret += '<div class="input_position_below">';
                    // Status
                    if (dataObject.status == "1") {
                        var open = "selected=selected";
                        var val = "open";
                    }else if (dataObject.status == "2") {
                        var going = "selected=selected";
                        var val = "going";
                    }else if (dataObject.status == "3") {
                        var closed = "selected=selected";
                        var val = "closed";
                    }else if (dataObject.status == "4") {
                        var waiting = "selected=selected";
                        var val = "going";
                    }
                ret += val;
                //ret += '<select class="task_value form-select " id="taskStatus" name="task_status"  placeholder="Select status">';
                //    ret += '<option value="">----------------------------Select----------------------------</option>';
                //    ret += '<option value="1" ' + open + '>Open</option>';
                //    ret += '<option value="2" ' + going + '>On Going</option>';
                //    ret += '<option value="3" ' + closed + '>Closed</option>';
                //    ret += '<option value="4" ' + waiting + '>Waiting</option>';
                //ret += '</select>';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    
    ret += '<div style="float:right;width:45%;margin-right:5px;">';
        ret += '<div class="fields_below" style="display: inline-block;">';
            ret += '<div class="label_below">Due Date: </div>';
            ret += '<div class="input_position_below">';
                ret += dataObject.due_date;
                //ret += '<input type="text" name="task_due_date" value="'+ dataObject.due_date +'"  id="dueDate" class="task_value form-text utask_date" placeholder="Choose date">';
            ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div class="hr"></div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Remarks: </div>';
        ret += '<div class="input_position_below" style="word-break: break-all;">';
            ret += dataObject.remarks;
            //ret += '<textarea  class=" form-textarea_save"  id="taskRemarks" name="task_remarks" placeholder="Input remarks">'+ dataObject.remarks +'</textarea>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="text-align: right;font-weight:bold;">Authorization</div>';
    ret += '<div class="hr"></div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Editor(s) : </div>';
        ret += '<div class="input_position_below">';
            //ret += '<input type="text" name="task_editos" class="task_value form-text" placeholder="Choose editor of your task">';
            //ret += '<div id="uteamEditors" class=" task_value_users" style="background-color:white;float: left;border: 1px solid #ddd;min-height: 40px;width:480px;margin-top: 5px;padding-bottom: 8px;">';
                for (var a = 0; a < dataObject.editorsName.length; a++) {
                    ret += updateUserTag(dataObject.editorsName[a].name,dataObject.editorsName[a].id,"#utaskEditors");
                }   
                //ret += '<input type="text"  name="task_editos" id="utaskEditors" class="task_value_user input-small " placeholder="Choose editor of your task" data-tag-id="'+ dataObject.Editors +'" style="border: none;box-shadow:none;margin-left: 12px;margin-top: 8px;">';
            //ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="float: left;width: 100%;">';
        ret += '<div id="udisplaytaskEditors"></div>';
    ret += '</div>';
    ret += '<div class="hr"></div>';
    ret += '<div class="fields_below" style="display: inline-block;">';
        ret += '<div class="label_below">Viewer(s) : </div>';
        ret += '<div class="input_position_below">';
            //ret += '<input type="text" name="task_viewers" class="task_value form-text" placeholder="Choose viewer of your task">';
            //ret += '<div id="uteamViewers" class=" task_value_users" style="background-color:white;float: left;border: 1px solid #ddd;min-height: 40px;width:480px;margin-top: 5px;padding-bottom: 8px;">';
                for (var a = 0; a < dataObject.viewersName.length; a++) {
                    ret += updateUserTag(dataObject.viewersName[a].name,dataObject.viewersName[a].id,"#utaskViewers");
                }   
                //ret += '<input type="text"  name="task_viewers" id="utaskViewers" class="task_value_user input-small " placeholder="Choose viewer of your task" data-tag-id="'+ dataObject.Viewers +'" style="border: none;box-shadow:none;margin-left: 12px;margin-top: 8px;">';
            //ret += '</div>';
        ret += '</div>';
    ret += '</div>';
    ret += '<div style="float: left;width: 100%;">';
        ret += '<div id="udisplaytaskViewers"></div>';
    ret += '</div>';
    ret += '<div class="hr"></div>';
    return ret;
}

function updateUserTag(name,dataID,option) {
    var ret = "";
    var event = option.replace("#","").replace(".","");
    if(dataID!=undefined){
       ret += '<div class="recipient ' + event + '" id="' + event + '_' + dataID + '" data-id="' + dataID + '">';
	  ret += '<span style="margin-right:5px;">' + name + '</span>';
	  ret += '<icon class="cursor removeTag_' + event + '" data-id="' + dataID + '">X</icon>';
       ret += '</div>';
       $(ret).insertBefore(option.event);
       getUpdateTagID(option);
       //removing(option);	
    }
    
    return ret;
}

function getUpdateTagID(elements){
    // get all selected user id
    
    var event = elements.replace("#","").replace(".","");
    
    var listRecipient =  $("." + event).map(function(){
	return $(this).attr("data-id"); // Get an ID of the user
    }).get().join(',');
    
    if(listRecipient!=""){
       $(elements.event).attr("data-tag-id",listRecipient);
    }else{
       $(elements.event).attr("data-tag-id","0");
    }
    
 }
 
