var jsonTrashData = {
    search_value: "",   
    "column-sort": "DateCreated",
    "column-sort-type": "DESC", 
    limit: 10,
}

var columnSort = [];
$(document).ready(function(){
	TrashBin.init();


	//delete per button

	$("body").on("click","#fl-action-delete",function(){
		var form_id = $("#FormID").val();
		var record_id = $("#ID").val();
		var deletion_type = $("#deletion-type").val()
		var warning_message_trash = "WARNING: Are you sure you want to delete this record?";
		if(deletion_type=="2"){
			warning_message_trash = warning_message_trash+" This action cannot be undone.";
		}
		var newConfirm = new jConfirm(warning_message_trash, 'Confirmation Dialog', '', '', '', function(r) {
            if (r == true) {
            	ui.block();
                TrashBin.deleteData(form_id,record_id,function(data){
                	if(data!=0){
                		window.onbeforeunload = null;
                		window.location = "/user_view/application?id="+data['form_id'];
                	}else{
                		window.location.reload(true);
                	}
                });
            } 
        });
        newConfirm.themeConfirm("confirm2", {
                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
        });
		
	})

	$("body").on("click",".trash-record-single",function(){
		var self = this;
		var form_id = $(this).closest(".fl-table-wrapper").attr("form_id");
		var record_id = $(this).attr("data-id");

		var deletion_type = $(this).closest(".fl-table-wrapper").find(".deletion-type").val();
		var warning_message_trash = "WARNING: Are you sure you want to delete this record?";
		if(deletion_type=="2"){
			warning_message_trash = warning_message_trash+" This action cannot be undone.";
		}
		var newConfirm = new jConfirm(warning_message_trash, 'Confirmation Dialog', '', '', '', function(r) {
            if (r == true) {
            	// alert("Form ID: "+form_id+"\n"+"Record ID: "+record_id)
            	ui.block();
                TrashBin.deleteData(form_id,record_id,function(data){
                	$(self).closest(".fl-table-wrapper").find(".searchRequestUsersButton").trigger("click");
                	if(data!=0){
	                	showNotification({
						    message: "Record has been successfully deleted.",
				            type: "success",
						    autoClose: true,
						    duration: 3
						})
                	}
                	ui.unblock();
                	// window.location = "/user_view/application?id="+data['form_id'];
                });
            } 
        });
        newConfirm.themeConfirm("confirm2", {
                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
        });
		
	})

	$("body").on("click",".retrieveTrash",function(){
		var id = $(this).attr("data-id");
		var form_id = $(this).attr("form_id");
		ui.block();
		$.post("/ajax/trash-bin",{action:"retrieveRequest",record_id:id,form_id:form_id},function(data){
			ui.unblock();
			showNotification({
			    message: "Request has been restored.",
	            type: "success",
			    autoClose: true,
			    duration: 3
			})
			$("#loadTrashRecord").dataTable().fnDestroy();
			TrashBin.load();
		})
	})


	// template for multiple check
	$("body").on("click", "#retrieveManyDeletedRecord", function() {
		// CheckProperties.checkAll(thisObj, container);
        CheckProperties.checkAll(this, "#loadTrashRecord");
    })

    $("body").on("click","#loadTrashRecord .retrieveRecordSingle",function(){
    	// CheckProperties.setCheckAllChecked(singleCheckboxOrChild, container, multiCheckBoxOrParent);
    	CheckProperties.setCheckAllChecked(".retrieveRecordSingle", "#loadTrashRecord", "#retrieveManyDeletedRecord");
    })

    $("#retrieveManyRecordsButton").click(function(){
    	var retrieveRecordSelected = TrashBin.getSelectedRecord();

    	
    	if(retrieveRecordSelected.length==0){
    		showNotification({
			    message: "Please select at least one (1) record.",
	            type: "error",
			    autoClose: true,
			    duration: 3
			})
    		return false;
    	}
    	ui.block();
    	$.post("/ajax/trash-bin",{action:"retrieveMultiRequest",retrieveRecordSelected:retrieveRecordSelected},function(data){
    		console.log(data)
			ui.unblock();
			showNotification({
			    message: "Request(s) has been restored.",
	            type: "success",
			    autoClose: true,
			    duration: 3
			})
			$("#loadTrashRecord").dataTable().fnDestroy();
			TrashBin.load();
			$("#retrieveManyDeletedRecord").prop("checked",false)
		})
    })

	//de;ete permanent single
	
	$("body").on("click",".deletTrashPermanentSingle",function(){
		var self = this;
		var request_id = $(this).attr("request-id");
		var form_id = $(this).attr("form_id");
		var table_name = $(this).attr("table_name");
		var warning_message_trash = "WARNING: Are you sure you want to delete this record? This action cannot be undone.";
		var newConfirm = new jConfirm(warning_message_trash, 'Confirmation Dialog', '', '', '', function(r) {
            if (r == true) {
           		ui.block();

				$.post("/ajax/trash-bin",{action:"deleteRecordPemanentSingle",request_id:request_id,form_id:form_id,table_name:table_name},function(data){
					console.log(data)
					ui.unblock();
					showNotification({
					    message: "Request has been permanently deleted.",
			            type: "success",
					    autoClose: true,
					    duration: 3
					})
					$("#loadTrashRecord").dataTable().fnDestroy();
					TrashBin.load();
				}) 	
            } 
        });
        newConfirm.themeConfirm("confirm2", {
                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
        });
	})

	//delete permanent multi
	$("#deletTrashPermanentMany").click(function(){
		var self = this;
		var retrieveRecordSelected = TrashBin.getSelectedRecord();
		if(retrieveRecordSelected.length==0){
    		showNotification({
			    message: "Please select at least one (1) record.",
	            type: "error",
			    autoClose: true,
			    duration: 3
			})
    		return false;
    	}
    	var warning_message_trash = "WARNING: Are you sure you want to delete this record/s? This action cannot be undone.";
		var newConfirm = new jConfirm(warning_message_trash, 'Confirmation Dialog', '', '', '', function(r) {
            if (r == true) {
           		ui.block();

				$.post("/ajax/trash-bin",{
					action:"deleteRecordPemanentMulti",
					retrieveRecordSelected:retrieveRecordSelected
				},function(data){
					console.log(data)
					ui.unblock();
					showNotification({
					    message: "Request/s has been permanently deleted.",
			            type: "success",
					    autoClose: true,
					    duration: 3
					})
					$("#loadTrashRecord").dataTable().fnDestroy();
					TrashBin.load();
				}) 	
            } 
        });
        newConfirm.themeConfirm("confirm2", {
                'icon': '<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>'
        });
	})
})

TrashBin = {
	"init" : function(){
		var pathname = window.location.pathname;

		if(pathname!="/user_view/trash-bin"){
			return false;
		}
		//for load
		this.load();
		//for sort
		this.sort();
		//for search
		this.search();

		//for limit
		this.limit();
	},
	load : function(callback){

		//code here
		$("#loadTrashRecord").dataTable().fnDestroy();
		var oColReorder = {
            allowReorder : false,
            allowResize : true
        };
		var oTable = $('#loadTrashRecord').dataTable( {
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> '+
                       '<div class="bar1"></div> '+
                       '<div class="bar2"></div> '+
                       '<div class="bar3"></div> '+
                       '<div class="bar4"></div> '+
                       '<div class="bar5"></div> '+
                       '<div class="bar6"></div> '+
                      '<div class="bar7"></div> '+
                      '<div class="bar8"></div> '+
                      '<div class="bar9"></div> '+
                      '<div class="bar10"></div> '+
                 '</div>'
            },
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/trash-bin",
            "fnServerData": function(sSource, aoData, fnCallback) {
            	aoData.push({"name": "limit", "value": jsonTrashData['limit']});
                aoData.push({"name": "search_value", "value": jsonTrashData['search_value']});
                aoData.push({"name": "action", "value": "loadTrashRecord"});
                aoData.push({"name": "column-sort", "value": jsonTrashData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonTrashData['column-sort-type']});
                
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                    	console.log(data)
                        //added by japhet morada
                        //fix the trash bin show entries 
                        if(data['iTotalDisplayRecords'] == null){
                           data['iTotalDisplayRecords'] = 0; 
                        }
                        if(data['iTotalRecords'] == null){
                            data['iTotalDisplayRecords'] = 0;
                        }
                        console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {
            	
            	var obj = '#loadTrashRecord';
            	var listOfappContainer = '.fl-list-of-app-record-wrapper';
            	dataTable_widget(obj, listOfappContainer)
            	dataTableSetwidth(this.find('[field_name="Status"]'), 130);	
            
            },
            fnDrawCallback : function(){
            	$("#loadTrashRecord tr .rule-data").each(function(){
            		var json_data = $(this).text()
            		json_data = JSON.parse(json_data);
            		$(this).closest("tr").data("row_data",json_data);
            	})
            	$(".tooltip").remove();
                $(".tip").tooltip();
                if (callback) {
                    callback(1);
                }
                $("#retrieveManyDeletedRecord").prop("checked",false)

            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonTrashData['limit']),
        });
        return oTable;
	},
	"sort" : function(){ 
		var self = this;
		$("body").on("click","#loadTrashRecord th",function(){
	        var cursor = $(this).css("cursor");
	        jsonTrashData['column-sort'] = $(this).attr("field_name");
	        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
	            return;
	        }
	        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

	        $(this).closest("#loadTrashRecord").find(".sortable-image").html("");
	        if(indexcolumnSort==-1){
	            columnSort.push($(this).attr("field_name"))
	            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
	            jsonTrashData['column-sort-type'] = "ASC";
	        }else{
	            columnSort.splice(indexcolumnSort,1);
	            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
	            jsonTrashData['column-sort-type'] = "DESC";
	        }
	        var thisTh = this;
	        self.load(function(){
	        	addIndexClassOnSort(thisTh);
	        });
	    })
	},
	"search" : function(){
		var self = this;
		$("body").on("keyup","#txtSearchDeletedRecordDatatable",function(e){
			if (e.keyCode == "13") {
		        $("#loadTrashRecord").dataTable().fnDestroy();
		        jsonTrashData['search_value'] = $(this).val();
		        self.load();
		    }
	    })

		$("body").on("click","#btnSearchDeletedRecordDatatable",function(e){
	        $("#loadTrashRecord").dataTable().fnDestroy();
	        jsonTrashData['search_value'] = $("#txtSearchDeletedRecordDatatable").val();
	        self.load();
	    })

	},
	"limit" : function(){
		var self = this;
		$(".searchDeletedRecordLimitPerPage").change(function(e) {
	        // if (e.keyCode == "13") {
	            var val = parseInt($(this).val());
	            jsonTrashData['limit'] = val;
	            $("#loadTrashRecord").dataTable().fnDestroy();
	            // var self = this;
	            self.load();
	        // }
	    })
	},
	"deleteData" : function(form_id,record_id,callback){

		$.post("/ajax/trash-bin",{action:"deleteRequest",form_id:form_id,record_id:record_id},function(data){
			if(data=="0"){
				showNotification({
				    message: "You do not have access to delete.",
		            type: "error",
				    autoClose: true,
				    duration: 3
				})
				callback(0);
			}else if(data=="1"){
				alert("pasok")
				showNotification({
				    message: "This record is already deleted!",
		            type: "error",
				    autoClose: true,
				    duration: 3
				})
				callback(0);
			}else{
				try{
					data = JSON.parse(data);
					callback(data)
				}catch(err){
					
					console.log(err)
				}
			}
		})
	},
	"deleteManyData" : function(form_id,records,callback){

		$.post("/ajax/trash-bin",{action:"deleteManyRequest",form_id:form_id,records:records},function(data){
			if(data=="0"){
				showNotification({
				    message: "You do not have access to delete.",
		            type: "error",
				    autoClose: true,
				    duration: 3
				})
				callback(0);
			}else if(data=="1"){
				alert("pasok")
				showNotification({
				    message: "This record is already deleted!",
		            type: "error",
				    autoClose: true,
				    duration: 3
				})
				callback(0);
			}else{
				try{
					data = JSON.parse(data);
					callback(data)
				}catch(err){
					
					console.log(err)
				}
			}
		})
	},
	"getSelectedRecord" : function(){
		var retrieveRecordSelected = [];
		$("#loadTrashRecord .retrieveRecordSingle:checked").each(function(){
			retrieveRecordSelected.push({
				"record_id":$(this).attr("request-id"),
				"table_name":$(this).attr("table_name"),
				"trash_id":$(this).attr("data-id"),
				"form_id":$(this).attr("form_id")
			});
		})
		return retrieveRecordSelected;
	}
}
