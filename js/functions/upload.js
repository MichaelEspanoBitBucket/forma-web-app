
upload = {
    up : function(){
        $("body").on("change",".upfile", function(){
            var actionType = $(this).attr("data-action-type");
	    var dataID = $(this).attr("data-action-id");
        var filez = this.files[0];
        if($.type(filez) == "undefined"){
            $('.getFields_'+dataID).val("");
            return false;
        }
	    var img = $(this).val();
	    $("#uploadFilename_" + dataID).html(img);
/* ========== User Personal Photo and Company logo ========== */          
            if (actionType=="userPhoto") {
                $(".loaderUploadProfile").show();
                $("#personal_photo").ajaxForm(function(data){
                    //console.log(data)
                    if (data=="Invalid File Format") {
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".filename").html("No file selected");
                        $(".loaderUploadProfile").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="File too large. File must be less than 4 megabytes."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".filename").html("No file selected");
                        $(".loaderUploadProfile").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="It seems that you have uploaded a nude picture."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".filename").html("No file selected");
                        $(".loaderUploadProfile").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else{
                        var json = $.parseJSON(data);
                        
                        if(json[0].successful=="Your new photo was successfully uploaded."){
                                showNotification({
                                        message: json[0].successful,
                                        type: "success",
                                        autoClose: true,
                                        duration: 3
                                });
                                $(".personalPhoto, .userProfile").fadeOut();
                                $(".personalPhoto, .userProfile").fadeIn();
                                $(".personalPhoto, .userProfile").attr("src",json[0].image);
                                $(".filename").html("No file selected");
                                $(".loaderUploadProfile").hide();
				$("#uploadFilename_" + dataID).html("No file selected");
                        }   
                    }
                    
                }).submit();    
            }else if (actionType=="companyPhoto") {
                $(".loaderUploadCompany").show();
                $("#company_photo").ajaxForm(function(data){
                    //console.log(data)
                    if (data=="Invalid File Format") {
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".filename").html("No file selected");
                        $(".loaderUploadCompany").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="File too large. File must be less than 4 megabytes."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".filename").html("No file selected");
                        $(".loaderUploadCompany").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="It seems that you have uploaded a nude picture."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".filename").html("No file selected");
                        $(".loaderUploadCompany").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else{
                        var json = $.parseJSON(data);
                        if(json[0].successful=="Your new photo was successfully uploaded."){
						console.log(json)
                                showNotification({
                                        message: json[0].successful,
                                        type: "success",
                                        autoClose: true,
                                        duration: 3
                                });
                                $(".companyPhoto, .companyPic").fadeOut();
                                $(".companyPhoto, .companyPic").fadeIn();
                                $(".companyPhoto, .companyPic").attr("src",json[0].image);
                                $(".filename").html("No file selected");
                                $(".loaderUploadCompany").hide();
				$("#uploadFilename_" + dataID).html("No file selected");
                        }
                    }
                    
                }).submit(); 
            }
/* ========== Upload Form Logo ========== */

            else if (actionType=="uploadLogoForm") {
                $(".postLogoLoader").show();
              
                $("#formLogo").ajaxForm(function(data){
                    if (data=="Invalid File Format") {
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".postLogoLoader").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="File too large. File must be less than 4 megabytes."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".postLogoLoader").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="It seems that you have uploaded a nude picture."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".postLogoLoader").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else{
                        var json = $.parseJSON(data);
			$("body").data("form_logo",json);
			//console.log(json)
                        showNotification({
                                message: "Your photos was successfully uploaded.",
                                type: "success",
                                autoClose: true,
                                duration: 3
                        });
			$(".form_logo").attr("src",json[0].image); // Set Image to a form when done uploading
                        $(".postLogoLoader").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }
                }).submit(); 
            }
/* ========== Upload Form Logo ========== */

            else if (actionType=="upload_form_icon_image") {
                $(".postFileLoad").show();
              
                $("#postFile").ajaxForm(function(data){
                    if (data=="Invalid File Format") {
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".postFileLoad").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="File too large. File must be less than 4 megabytes."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".postFileLoad").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="It seems that you have uploaded a nude picture."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });$(".postFileLoad").hide();$(".postLogoLoader").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else{
                        var json = $.parseJSON(data);
			$("body").data("form_logo_img",json);
			//console.log(json)
                        showNotification({
                                message: "Your photos was successfully uploaded.",
                                type: "success",
                                autoClose: true,
                                duration: 3
                        });
			$(".imagePrev").attr("src",json[0].image); // Set Image to a form when done uploading
                        $(".postFileLoad").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }
                }).submit(); 
            }            
/* ========== Attach File on request ========== */
            else if (actionType=="attachFile") {
                $(".attachFiles").show();
                var ret = "";
                $("#fille_attach").ajaxForm(function(data){
                    if (data=="Invalid File Format") {
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".attachFiles").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="File too large. File must be less than 4 megabytes."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".attachFiles").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else if(data=="It seems that you have uploaded a nude picture."){
                        showNotification({
                                message: data,
                                type: "error",
                                autoClose: true,
                                duration: 3
                        });
                        $(".attachFiles").hide();
			$("#uploadFilename_" + dataID).html("No file selected");
                    }else{
                        var json = $.parseJSON(data);
                        showNotification({
                                message: "Attached file was successfully added.",
                                type: "success",
                                autoClose: true,
                                duration: 3
                        });
                        $(".attachFiles").hide();
                        
                        if (json[0].extension=="file") {
                            ret += '<div>';
                            ret += '<form class="pull-left" method="POST" >';
                            ret += '<div>';
                            ret += '<input type="submit" class="cursor tip" data-original-title="Download Attachment"/><i class="fa fa-download icon-download"></i> '; 
                            ret += '<i class="fa fa-trash-o icon-trash cursor tip removeFiles" data-original-title="Remove Attachment" data-file="' + json[0].filename + '" data-location="/' + json[0].img + '"></i>';
                            ret += '</div>';
                            ret += '<a class="download_attachment">';
                            ret += '<img data-name="' + json[0].filename + '" src="/'+ json[0].img + '" width="25" height="25" class="AFiles imagetoPost avatar userPhoto personalPhoto userAvatar tip" onerror="this.src='+ $('#broken_image').text() +'" data-placement="bottom" data-original-title="'+ json[0].filename + '"  title="' + json[0].filename + '">'; //
                            ret += '</a>';
                            //ret += '<input type="submit"/>';
                            ret += '<input type="hidden" value="'+ json[0].filename +'" name="attachment_filename"/>';
                            ret += '<input type="hidden" value="' + json[0].img + '" name="attachment_location"/>';
                            
                            ret += '</form>';
                            ret += '</div>';
                            $(".attached_file_container").append(ret);
                        }else{
                            ret += '<div>';
                            ret += '<form class="pull-left" method="POST" >';
                            ret += '<div>';
                            ret += '<input type="submit" class="cursor tip" data-original-title="Download Attachment"/><i class="fa fa-download icon-download"></i> '; 
                            ret += '<i class="fa fa-trash-o icon-trash cursor tip removeFiles" data-original-title="Remove Attachment" data-file="' + json[0].filename + '" data-location="/' + json[0].img + '"></i>';
                            ret += '</div>';
                            ret += '<a class="download_attachment">';
                            ret += '<div data-name="'+ json[0].filename +'" class="AFiles_only avatar AFiles tip" data-placement="bottom" data-original-title="'+ json[0].filename +'"  title="'+ json[0].filename +'"><i class="fa fa-file icon-file"></i></div>'; // //
                            ret += '</a>';
                            //ret += '<input type="submit"/>';
                            ret += '<input type="hidden" value="'+ json[0].filename +'" name="attachment_filename"/>';
                            ret += '<input type="hidden" value="' + json[0].img + '" name="attachment_location"/>';
                            
                            ret += '</form>';
                            ret += '</div>';
                            $(".attached_file_container").append(ret);
                        }
                        
                        // ADD to Display FrmRequest
                        var files_attachment = $(".AFiles").map(function(index){
                            return $(this).attr("data-name");
                        }).get().join();
                        $("#view_attachment_files").val(files_attachment);
                        //console.log($(".attached_file_container > div").length);
                        $(".tip").tooltip();
                        if ($(".attached_file_container > div").length=="1") {
                           //$("<br><br>").insertAfter(".attached_file_container");
                        }
			$("#uploadFilename_" + dataID).html("No file selected");
                    }
                }).submit(); 
            }
        });
    }
}
$(document).ready(function(){
    upload.up(); // Uploading profile pic
    
    //Multiple Attachment
    $("body").on("change",".multiple_file_attachement",function(){
      
    var getID = $(this).attr("id");
	var action_id = $(this).attr("data-action-id");
    var flds = $(".getFields_" + action_id).val();
    var filez = this.files[0];
    var bName = $(".getFields_" + action_id).attr("name");

    ui.block();
    if($.type(filez) == "undefined"){
        // $('.getFields_'+action_id).val("");
        ui.unblock();
        return false;
    }
	$("#multiple_on_attach_" + action_id).ajaxForm(function(data) {
        var json = $.parseJSON(data);
	     console.log(json)
         
         
        if (json[0].message == "ok") {
            var new_arr = [];
            if(flds != ""){
                // console.log(flds);
                new_arr = flds;
            }

            var fileUploaded = [];
            $.each(json,function(id,val){
                var setFileUploaded = {};
                if(json[id].message == "ok"){
                    // console.log(JSON.stringify(data));
                    console.log(JSON.stringify(json[id]))
                    console.log(json[id]['userID'])
                    // Msg
                    // showNotification({
                    //     message: json[id]['successful'],
                    //     type: json[id]['etype'],
                    //     autoClose: true,
                    //     duration: 3
                    // });

                    setFileUploaded['location'] = json[id]['location'];
                    setFileUploaded['file_name'] = json[id]['file_name'];
                    setFileUploaded['auth_id'] = json[id]['userID'];
                    fileUploaded[id] = setFileUploaded;
                    $("#" + getID).val(null);

                }

            })
            if(flds != ""){
               
                // console.log(flds)
                // console.log(jQuery.parseJSON(flds));
                // console.log(JSON.stringify(fileUploaded));
                // console.log(fileUploaded);
                var nArr = jQuery.parseJSON(flds);
                var mergeArr = $.merge(nArr,fileUploaded);
                var fileLength = mergeArr.length;
                
                console.log(mergeArr)
                console.log(JSON.stringify(mergeArr))

                $(".getFields_" + action_id).attr("value",JSON.stringify(mergeArr));
                

                $("body").data(bName,mergeArr);
                $("[name='" + bName + "']").val(JSON.stringify(mergeArr));
                $('[data-body-name="' + bName + '"]').children().html('<i class="fa fa-search"><i> <label style="cursor:pointer;font-family:Arial;text-decoration:underline;">Show ('+ fileLength +') File/s</label> ');
                $("#" + getID).val(null);

            }else{
                
                var fileLength = fileUploaded.length;
                $(".getFields_" + action_id).attr("value",JSON.stringify(fileUploaded));
                
                $("body").data(bName, fileUploaded);
               
                var view_html = '<a id="view_files_in_modal" data-body-name="' + bName + '" style="';
                view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
                view_html += '"><u style="font-family:Arial;"> <i class="fa fa-search"></i> Show (' + fileLength + ') File/s </u></a>';
                // if (json[key] != "") {
                    // if ($(".getFields_" + action_id).attr("data-attachment")) {
                        $(".getFields_" + action_id).parent().append(view_html);
                    // }
                // }
                $("#" + getID).val(null);
            }



            // var a = new_arr;
            // var b = JSON.stringify(fileUploaded);
            // console.log(a);
            // console.log(b);
            // var merge_arr = $.merge( $.merge( [], a ), b );//$.extend([],a,b);//$.merge(a,b);
            // var merge_arr = concatArraysUniqueWithSort(new_arr,JSON.stringify(fileUploaded));
            // console.log(JSON.stringify(fileUploaded))
            // console.log($.merge(new_arr,JSON.stringify(fileUploaded)))
            // $(".getFields_" + action_id).attr("value",JSON.stringify($.merge(a,b)));
            // var a = $.merge(new_arr,JSON.stringify(fileUploaded));
            
            // var c = JSON.stringify(a)
            // console.log(c)
            //$('[name="multiple_on_attachment_'+action_id+'"]').val(c)
            
            $(".getFields_" + action_id).trigger("change");
            // $("#multiple_on_attachment_" + action_id).html("No file selected");
            // $("#multiple_on_attachment_" + action_id).val("");

            // var total_arr = parseInt(arr1L) + parseInt(arr2L);



            ui.unblock();
        }else{
            
            showNotification({
                message: json[0].message,
                type: "error",
                autoClose: true,
                duration: 3
            });
            $("#multiple_on_attachment_" + action_id).html("No file selected");
            $("#multiple_on_attachment_" + action_id).val("");
            ui.unblock();
        }
	 //    if (data == "Invalid File Format") {
  //   		showNotification({
  //   		    message: data,
  //   		    type: "error",
  //   		    autoClose: true,
  //   		    duration: 3
  //   		});
  //   		$(".filename").html("No file selected");
	 //    } else if (data == "File too large. File must be less than 4 megabytes.") {
		// showNotification({
		//     message: data,
		//     type: "error",
		//     autoClose: true,
		//     duration: 3
		// });
		// $(".filename").html("No file selected");
	 //    } else {
		
		// //var json = $.parseJSON(data);
		// //var parse_file_type = $.parseJSON(json[0].file_type);
		// //console.log(parse_file_type)
		// //var blkstr = [];
		// //$.each(parse_file_type['name'],function(id,value){
		// //    blkstr.push(value);
		// //});
		// //var files = blkstr.join(", ");
		// //console.log(files)
		// //if (json[0].successful == "Your file was successfully uploaded.") {
		//     $(".getFields_" + action_id).attr("value",JSON.stringify(data));
  //           $(".getFields_" + action_id).trigger("change");
		//     //var stringify = JSON.stringify(data);
		//     //console.log($.parseJSON($.parseJSON(stringify)))
		// //}
	 //    }
	}).submit();
    });
    
    // Object Attachment 
    $("body").on("change",".file_attachement",function(){
	var action_id = $(this).attr("data-action-id");
    var filez = this.files[0];
    ui.block();
    if($.type(filez) == "undefined"){
        // $('.getFields_'+action_id).val("");
        ui.unblock();
        return false;
    }
	//console.log(action_id);
	$("#on_attach_" + action_id).ajaxForm(function(data) {
	    //console.log(data)

        var json = $.parseJSON(data);
         console.log(json)
        if (json[0].message == "ok") {
          
            

            $(".ImagegetFields_" + action_id).attr("src",json[0].img);
            $(".getFields_" + action_id).attr("value",json[0].img);
            $(".getFields_" + action_id).trigger("change");
            // $("#on_attachment_" + action_id).html("No file selected");
            // $("#on_attachment_" + action_id).val("");
            //console.log(json[0].img)
            ui.unblock();
        }else{
            
            showNotification({
                message: json[0].message,
                type: "error",
                autoClose: true,
                duration: 3
            });
            // $(".filename").html("No file selected");
            $("#on_attachment_" + action_id).html("No file selected");
            $("#on_attachment_" + action_id).val("");
            ui.unblock();
        }

	 //    if (data == "Invalid File Format") {
		// showNotification({
		//     message: data,
		//     type: "error",
		//     autoClose: true,
		//     duration: 3
		// });
		// $(".filename").html("No file selected");
	 //    } else if (data == "File too large. File must be less than 4 megabytes.") {
		// showNotification({
		//     message: data,
		//     type: "error",
		//     autoClose: true,
		//     duration: 3
		// });
		// $(".filename").html("No file selected");
	 //    } else {
		
		// var json = $.parseJSON(data);
		// //console.log(json)
		// if (json[0].successful == "Your file was successfully uploaded.") {
		//     //imageForm = $(getimageForm(count, "imageForm", json[0].img));
		//     $(".ImagegetFields_" + action_id).attr("src",json[0].img);
		//     $(".getFields_" + action_id).attr("value",json[0].img);
  //           $(".getFields_" + action_id).trigger("change");
		//     //console.log(json[0].img)
		// }
	 //    }
	}).submit();
    });

    //Embedded View Multiple Attachment
    //added by japhet
    //03-30-2016
    $("body").on("change",".embed_multiple_file_attachement",function(){
    var action_id = $(this).attr("data-action-id");
    var filez = this.files[0];
    var id = $(this).attr('id');
    var field_id = $(this).attr('field-id');
    if($.type(filez) == "undefined"){
        $('.getFields_'+action_id).val("");
        return false;
    }
    $("#embed_multiple_on_attach_" + action_id + '_' + field_id).ajaxForm(function(data) {
        console.log(data)
        if (data == "Invalid File Format") {
        showNotification({
            message: data,
            type: "error",
            autoClose: true,
            duration: 3
        });
        // $(".filename").html("No file selected");
        } else if (data == "File too large. File must be less than 4 megabytes.") {
        showNotification({
            message: data,
            type: "error",
            autoClose: true,
            duration: 3
        });
        // $(".filename").html("No file selected");
        } else {
            console.log($(this));
            
            //var json = $.parseJSON(data);
            //var parse_file_type = $.parseJSON(json[0].file_type);
            //console.log(parse_file_type)
            //var blkstr = [];
            //$.each(parse_file_type['name'],function(id,value){
            //    blkstr.push(value);
            //});
            //var files = blkstr.join(", ");
            //console.log(files)
            alert(id);
            $("[data-field-name='" + id + "']").val(data);
            $("[data-field-name='" + id + "']").trigger("change");
        }
    }).submit();
    });
    
    // Object Attachment 
    //added by japhet
    //03-30-2016
    $("body").on("change",".embed_file_attachement",function(){
    var action_id = $(this).attr("data-action-id");
    var filez = this.files[0];
    var id = $(this).attr('id');
    var field_id = $(this).attr('field-id');
    if($.type(filez) == "undefined"){
        $('.getFields_'+action_id).val("");
        return false;
    }
    //console.log(action_id);
    $("#embed_on_attach_" + action_id + '_' + field_id).ajaxForm(function(data) {
        //console.log(data)
        if (data == "Invalid File Format") {
        showNotification({
            message: data,
            type: "error",
            autoClose: true,
            duration: 3
        });
        // $(".filename").html("No file selected");
        } else if (data == "File too large. File must be less than 4 megabytes.") {
        showNotification({
            message: data,
            type: "error",
            autoClose: true,
            duration: 3
        });
        // $(".filename").html("No file selected");
        } else {
            var json = $.parseJSON(data);
            //console.log(json)
            if (json[0].successful == "Your file was successfully uploaded.") {
                //imageForm = $(getimageForm(count, "imageForm", json[0].img));
                $(".ImagegetFields_" + action_id).attr("src",json[0].img);
                $("[data-field-name='" + id + "']").attr("value",json[0].img);
                $("[data-field-name='" + id + "']").trigger("change");
                //console.log(json[0].img)
            }
        }
    }).submit();
    });
    
    $('body').on('click','.form_upload_photos[data-upload="photo_upload"]',function(){
        //alert(1)
    	var dataOBJID = $(this).attr("data-object-id");
    	//var c = $(this).children().children().children().html();
    	var getFormID = $("#getFormID").val();
    	var getID = $("#getID").val();
    	var getTrackNo = $("#getTrackNo").val();
    	var ret = 
        '<h3><i class="icon-save"></i> Upload Image</h3>'+
    	'<div class="hr"></div>'+
        '<label><input type="checkbox" class="switch-draw"> Use draw signature instead</label><br/><br/>'+
    	'<form id="getpostSelectFormPhotos_' + dataOBJID + '" method="post" enctype="multipart/form-data" action="/ajax/formUpload" style="z-index: 1000000000000;position: relative;">'+
    	    //'<input type="file" data-action-id="2" value="upload" name="image" id="formImage" size="24" data-action-type="postImageForm" style="opacity: 0;position: absolute;" class="">'+
    	    //'<i class="icon-picture"></i> Image'+
    	    '<div id="uniform-fileInput" class="uploader2">'+
                '<input type="file" data-action-id="' + dataOBJID + '" value="upload" name="image" id="postSelectFormPhotos_' + dataOBJID + '" size="24" data-action-type="postImageForm">'+
                //'<span id="uploadFilename_2" class="filename">No file selected</span>'+
    		    //'<span class="action">Choose File</span>'+
    		    // '</div>'+
    	    '</div>'+
    	'</form>'+
        '<div class="draw-pad-container isDisplayNone" style="position:relative;width:100%;height:calc(100% - 30px);">'+
            '<div id="signature-pad" class="m-signature-pad" style="width: 100% !important; height: 100% !important; margin: 0px !important; top: 0px !important; left: 0px !important;">'+
                '<div class="m-signature-pad--body">'+
                    '<canvas></canvas>'+
                '</div>'+
                '<div class="m-signature-pad--footer">'+
                    '<div class="description">Sign above</div>'+
                    '<button type="button" class="button clear request-image-draw-clear" data-action="request-image-draw-clear">Clear</button>'+
                    '<button type="button" data-action-id="' + dataOBJID + '" class="button save request-image-draw-save" data-action="request-image-draw-save">Save</button>'+
                '</div>'+
            '</div>'+
        '</div>'+
    	'<div class="hr"></div>'+
    	'<div class="fields"><div class="label_basic"></div><div class="input_position" style="margin-top:5px;text-align:right;"><img src="/images/loader/load.gif" class="display saveFormLoad" style="margin-right:5px;margin-top:2px;"><input type="button" class="btn-basicBtn" id="popup_cancel" value="Ok"> <input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div></div></div>';
    	
    	var newDialog = new jDialog(ret, "", "300", "", "", function() {
    	});
        newDialog.themeDialog("modal2");

        $('.popup_container_display_table').css({
            'z-index':'1000000',
        })
        $('[id="popup_container"]').attr('style', ($('[id="popup_container"]').attr('style')?$('[id="popup_container"]').attr('style')+';':'')+'top: 0px !important;');
        var canvasElement = $('[id="popup_container"]').find('[id="signature-pad"] canvas').get(0);
        var resizeCanvas = function (canvas) {
            // When zoomed out to less than 100%, for some very strange reason,
            // some browsers report devicePixelRatio as less than 1
            // and only part of the canvas is cleared then.
            var ratio =  Math.max(window.devicePixelRatio || 1, 1);
            canvas.width = canvas.offsetWidth * ratio;
            canvas.height = canvas.offsetHeight * ratio;
            canvas.getContext("2d").scale(ratio, ratio);
        }
        resizeCanvas(canvasElement);
        //
        $('[id="popup_container"]').find('.switch-draw').on('change.drawSignature',function(){
            var self = $(this);
            var popup_container = self.closest('[id="popup_container"]');
            var container = self.parent().parent();
            var eleWidth = $(".ImagegetFields_" + dataOBJID).width();
            var eleHeight = $(".ImagegetFields_" + dataOBJID).height();
            if(self.is(':checked')){

                container.find('form[action="/ajax/formUpload"]').hide();
                container.find('.draw-pad-container').removeClass('isDisplayNone');
                container.css('height', (eleHeight+142)+'px');
                container.parent().css('max-height', (eleHeight+142)+'px');
                popup_container.css({
                    'width':(eleWidth+72)+'px',
                    'max-width':(eleWidth+72)+'px'
                });
                resizeCanvas(canvasElement);
            }else{
                container.find('form[action="/ajax/formUpload"]').show();
                container.find('.draw-pad-container').addClass('isDisplayNone');
                container.css('height', '');
                container.parent().css('max-height', '');
                popup_container.css('width','');
            }
        });

        setTimeout(function(){
            var signaturePad = new SignaturePad(canvasElement);

            if(getParametersName('signature', ( $(".getFields_" + dataOBJID).attr('value')|| $(".getFields_" + dataOBJID).val() ) ) == 'true'){
                $('[id="popup_container"]').find('.switch-draw').prop('checked', true ).trigger('change.drawSignature');
                var imgEle = $(".ImagegetFields_" + dataOBJID).get(0);
                var ctx = canvasElement.getContext('2d')
                var tempImg = new Image();
                tempImg.src = imgEle.src;
                ctx.drawImage(tempImg, 0, 0, tempImg.width,    tempImg.height,
                   0, 0, canvasElement.width, canvasElement.height);
                // ctx.drawImage( imgEle , 0, 0 );
            }

            $('[id="popup_container"]').find('[id="signature-pad"] .request-image-draw-clear').on('click', function(){
                signaturePad.clear();
            });
            $('[id="popup_container"]').find('[id="signature-pad"] .request-image-draw-save').on('click', function(){
                var dataID = $(this).attr("data-action-id");
                // alert('save image here');
                // console.log(signaturePad.toDataURL())
                $.post('/ajax/formUpload',{
                    action: 'saveDrawSignatureImage',
                    image: signaturePad.toDataURL()
                },function(data){
                    console.log(data)
                    var json = $.parseJSON(data);
                    console.log(json)
                    console.log(dataID)
                    $(".ImagegetFields_" + dataID).attr("src",json[0].img);
                    $(".getFields_" + dataID).attr("value",json[0].img);
                    //console.log(json[0].img)
                    $("#popup_cancel").trigger('click');
                    console.log(json[0].img)
                });
            });
        },0);
        // Upload Image to the form 
        $("#postSelectFormPhotos_" + dataOBJID).change(function() {
            var dataID = $(this).attr("data-action-id");
            var filez = this.files[0];
            ui.block();
            if($.type(filez) == "undefined"){
                $('.getFields_'+dataID).val("");
                ui.unblock();
                return false;
            }
            $("#getpostSelectFormPhotos_" + dataOBJID).ajaxForm(function(data) {
                var json = $.parseJSON(data);
                console.log(json)

                if (json[0].message == "Your new photos was successfully uploaded.") {
                    //imageForm = $(getimageForm(count, "imageForm", json[0].img));
                    $(".ImagegetFields_" + dataID).attr("src",json[0].img);
                    $(".getFields_" + dataID).attr("value",json[0].img);
                    //console.log(json[0].img)
                    $("#popup_cancel").trigger('click');
                    ui.unblock();
           
                }else{
                    showNotification({
                        message: json[0].message,
                        type: "error",
                        autoClose: true,
                        duration: 3
                    });
                    ui.unblock();

                }

                // if (json[0].message == "Invalid File Format") {
                //     showNotification({
                //         message: data,
                //         type: "error",
                //         autoClose: true,
                //         duration: 3
                //     });
                //     //$(".filename").html("No file selected");
                // } else if (json[0].message == "File too large. File must be less than 4 megabytes.") {
                //     showNotification({
                //         message: data,
                //         type: "error",
                //         autoClose: true,
                //         duration: 3
                //     });
                //     //$(".filename").html("No file selected");
                // } else {
                    
                //     // var json = $.parseJSON(data);
                //     // console.log(json)
                    
                // }
            }).submit();
        });
    });
})

function unique() {
    var a = this.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};
