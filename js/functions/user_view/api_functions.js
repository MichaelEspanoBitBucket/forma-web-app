$(document).ready(function(){
    
	
    
    $("body").on("click","#search_btn",function(){

	var claim_val = $("#claim_search_field").val();
	var policy_val = $("#policy_search_field").val();
	
	$("#search_loading").show();
	
	if (claim_val == "" && policy_val == "") {
	    $("#none_search").show();
	    $("#search_loading").hide();
	}else{
	    
	    $.ajax({
		type : "POST",
		url  : "/ajax/static_search",
		data : {action:"load_claim_request",form_id:263,claim_val:claim_val,policy_val:policy_val},
		dataType : 'json',
		success : function(e){
		    var get_data = e;
		    if (jQuery.isEmptyObject(get_data) != true) {
			var ret = "";
			for(var i = 0; i < get_data.length; i++){
			    var set_data = get_data[i]['data'];
			    
			    ret += api_search_html(set_data);
			}
			$("#search_api_display").html(ret);
			$("#none_search").hide();
		    }else{
			$("#none_search").show();
			$("#search_api_display").html(null);
		    }
			$("#search_loading").hide();
		}
	    })
	    
	    
	}
	
	
    })


    
})


function api_search_html(data_query){
    var ret = "";
    
	ret += '<li style="float: left;width:100%;padding:10px;border:1px solid #ddd;margin-top: 10px;margin-top: 10;margin-top: 1;min-height: 100px;background-color: #fff;border-radius:5px;" >';
	ret += '<div style="float:left;width:100%;margin-bottom:5px;font-size: 12px;font-weight:bold;">Native Claim File</div>';
	ret += '<div class="hr"></div>';
	ret += '<div style="float: left;width: 35%">';
	  ret += '<div class="fields_below" style="margin-top: 10px;">';
		ret += '<div class="label_below obj_label">';
		  ret += '<label id="" style="font-weight:bold;font-size: 12px;">Nature Of Claim:</label>';
		ret += '</div>';
		ret += '<div class="input_position_below">';
		  ret += data_query['NatureOfClaim'];
		ret += '</div>';
	  ret += '</div>';
	ret += '</div>';
	ret += '<div style="float: left;width: 35%">';
	  ret += '<div class="fields_below" style="margin-top: 10px;">';
		ret += '<div class="label_below obj_label">';
		  ret += '<label id="" style="font-weight:bold;font-size: 12px;">Claimants Name:</label>';
		ret += '</div>';
		ret += '<div class="input_position_below">';
		  ret += data_query['ClaimantsName'];
		ret += '</div>';
	  ret += '</div>';
	  ret += '<div class="fields_below" style="margin-top: 10px;">';
		ret += '<div class="label_below obj_label">';
		  ret += '<label id="" style="font-weight:bold;font-size: 12px;">Mobile No:</label>';
		ret += '</div>';
		ret += '<div class="input_position_below">';
		  ret += data_query['MobileNo'];
		ret += '</div>';
	  ret += '</div>';
	ret += '</div>';
	ret += '<div style="float: left;width: 30%">';
	  ret += '<div class="fields_below" style="margin-top: 10px;">';
		ret += '<div class="label_below obj_label">';
		  ret += '<label id="" style="font-weight:bold;font-size: 12px;">Email Address:</label>';
		ret += '</div>';
		ret += '<div class="input_position_below">';
		  ret += data_query['ClaimantEmailAddress'];
		ret += '</div>';
	  ret += '</div>';
	      
	  ret += '<div class="fields_below" style="margin-top: 10px;">';
		ret += '<div class="label_below obj_label">';
		  ret += '<label id="" style="font-weight:bold;font-size: 12px;">Status of Claimant:</label>';
		ret += '</div>';
		ret += '<div class="input_position_below">';
		  ret += data_query['ClaimantStatus'];
		ret += '</div>';
	  ret += '</div>';
	      
	ret += '</div>';
	ret += '</li>';

  
  return ret;
}