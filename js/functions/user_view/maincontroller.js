var pathname = window.location.pathname;
var sessionPath = getCookie('application'); //sessionStorage.getItem("application");
var document_title = document.title;
//
//
//alert( " name = " + sessionPath + "\n" + "path = " + pathname)
//  if((sessionPath==""  && pathname != "/user_view/application_portal") ||
//  	(sessionPath==""  && pathname != "/" ) ||
//  	sessionPath==""  &&  pathname != "/login")){
// window.location.replace("/user_view/application_portal");
//  }

//if (sessionPath == "" && pathname != "/user_view/application_portal" && pathname != "/" && pathname != "/login") {
//    window.location.replace("/user_view/application_portal");
//}
//if (sessionPath == "" && pathname != "/user_view/application_portal" && pathname != "/" && pathname != "/login" && pathname != "/user_view/workspace" && pathname != "/user_view/tour") {
//    window.location.replace("/user_view/application_portal");
//}
$(document).ready(function () {
    //tooltip
    $(".tip").tooltip();
    CSIDSearchUser.init();
    displayDraftWorkflows();
    urlTofixedLayout();
    showUserIconNav.init('.fl-onlineuser-nav-navui2');
    checkResponsiveWidth();
    updateTitleWithCountNotif();
    
    $('.ps-scrollbar-y').mousedown(function () {
        //alert(1);
    });
    // View Clicking Multiple File Attachment
    $("body").on("click", "#view_files_in_modal", function () {
        var lbl_name = $(this).parent().parent().children().eq(0).children().text();
        var data_body_name = $(this).attr("data-body-name");
        var data_json = $("body").data(data_body_name);
        var current_user_id = $("#current-user-id").text();

        console.log(data_json)
        console.log(JSON.stringify(data_json))
        var ret = "";
        ret += '<h3 class="fl-margin-bottom fl-add-comment-here"><i class="icon-comment fa fa-paperclip"></i> ' + lbl_name + '</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div class="fields">'; //style="padding:10px;"
        ret += '<ol style="padding:10px;list-style-type:decimal;">';
        for (var i = 0; i < data_json.length; i++) {

            ret += '<li style="padding:10px;border: 1px solid #ddd;margin-bottom: 5px; "><div style="float: left;height: 15px;width: 280px;  white-space: nowrap;  overflow: hidden;text-overflow: ellipsis;">' + data_json[i]['file_name'] + '</div>';

            ret += '<div style="float: right;">';
            // ret += '<a href="' + data_json[i]['location'] + "/" + data_json[i]['file_name'] + '" target="_blank">View File</a>';
            ret += '<a href="' + data_json[i]['location'] + data_json[i]['file_name'] + '" target="_blank"><i data-title="View File" class="tip fa fa-file cursor" style="color:#000;"></i></a>';
            if(current_user_id == data_json[i]['auth_id']){
                ret += '&nbsp;&nbsp;<i data-body-name="'+data_body_name+'" data-title="Delete File" data-location="' + data_json[i]['location'] + '" data-json="'+JSON.stringify(data_json)+'"  data-file="' + data_json[i]['file_name'] + '" id="deleteAttachmentFile" class="deleteAttachmentFile tip fa fa-trash cursor" style="color:#000;"></i>';
            }
            //ret += '<form method="POST">';
            //ret += '<input type="submit" class="cursor tip" data-original-title="Download this Attachment" style="opacity: 0;position: absolute;width: 10px;">';
            //ret += '<i class="fa fa-cloud-download cursor tip" data-original-title="Download this attachment" style="color: #000 !important;"></i>';
            //ret += '<input type="hidden" value="' + data_json[i]['file_name'] + '" name="attachment_filename">';
            //ret += '<input type="hidden" value="' + data_json[i]['location'] + "/" + data_json[i]['file_name'] + '" name="attachment_location">';
            //ret += '</form>';
            ret += '</div>';

            ret += '</li>';
        }
        ret += '</ol>';
        ret += '</div>';

        var newDialog = new jDialog(ret, "", "", "470", "", function () {
        });
        newDialog.themeDialog("modal2");
        $(".tip").tooltip();
    });

    // Delete Attachment
    $("body").on("click",".deleteAttachmentFile",function(){
        var loc_file = $(this).attr("data-file");
        var dataLoc = $(this).attr("data-location");
        var body_name = $(this).attr("data-body-name");
        var flds = body_name;
        var getFormID = $("#getFormID").val();
        var getID = $("#getID").val();
        var getTrackNo = $("#getTrackNo").val();
        console.log(body_name)
        var data_json = $("body").data(body_name);
        // console.log(data_json)
        var data_j = JSON.stringify(data_json);
        var fldName = $("#view_files_in_modal").prev().attr("name");
        var newConfirm = new jConfirm("Are you sure you want to delete this file? ", 'Confirmation Dialog', '', '', '', function (answer) {
            if (answer == true) {

                ui.block();
                $.ajax({
                    type:"POST",
                    url:"/ajax/delete-multiple-attachment",
                    data:{loc_file:loc_file,data_j:data_j,getFormID:getFormID,getID:getID,getTrackNo:getTrackNo,flds:flds,dataLoc:dataLoc },
                    success:function(e){
                        console.log(e)
                        var c = JSON.parse(e);
                        var l = c.length
                        // console.log(l);
                        console.log(e)

                        $("body").data(body_name,c);
                        $("[name='" + fldName + "']").val(e);
                        $('[data-body-name="' + fldName + '"]').children().html('<i class="fa fa-search"><i> <label style="cursor:pointer;font-family:Arial;text-decoration:underline;">Show ('+l+') File/s</label> ');
                        ui.unblock();
                        $("#popup_overlay").remove();
                        $("#popup_container").remove();
                        $("#view_files_in_modal").trigger("click")
                        // $(this).parent().parent().remove();
                    }
                })
            }
        });

        newConfirm.themeConfirm("confirm2", {
            'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'
        })

        $("#popup_cancel").click(function(){
            $("#popup_overlay").remove();
            $("#popup_container").remove();
            $("#view_files_in_modal").trigger("click")
        })
        

        
    })

    var pathname = window.location.pathname;

    if (pathname == "/") {
        $("[name='username']").focus();
    }


    $('.show-menu-workspace').on('click', function () {
        var showmenuworkspace = $(this).attr('show-menu-workspace');

        if (showmenuworkspace == 'false') {
            if ($('input[name="ID"]').val() == 0) {
                $(this).attr('show-menu-workspace', true);
                $('.fl-user-navigation-wrapper').css({
                    'z-index': 2,
                    'top': '40px'
                }).fadeIn();
            } else if ($('input[name="ID"]').val() >= 1) {
                $(this).attr('show-menu-workspace', true);
                $('.fl-user-navigation-wrapper').css({
                    'z-index': 2,
                    'top': '75px'
                }).fadeIn();
            }
            ;


            if ($('.fl-user-header-wrapper').length == 0) {
                $('.fl-user-navigation-wrapper').css({
                    'top': 0
                });
            }
            ;

            if ($('.fl-user-header-wrapper').length == 0 && $('.display_form_status').length == 1) {
                $('.fl-user-navigation-wrapper').css({
                    'top': 40
                });
            }
            ;

        } else if (showmenuworkspace == 'true') {
            $(this).attr('show-menu-workspace', false);
            $('.fl-user-navigation-wrapper').fadeOut();
        }
        ;
    });

    if ($('input[name="ID"]').val() >= 1) {
        $('.fl-content').css('top', '0px');
    }


    var urlNotifications = '/user_view/notifications';
    if (pathname == urlNotifications) {
    }
    ;


    // For WOrkspace width configuration
    var user_view = $("#user_url_view").val();
    var path = window.location.pathname;
    if (path == user_view + "workspace") {
        $(".fl-content").css("min-width", "100%");

    }



    /*   var menuItem = $('.mainmenu li a');
     menuItem.removeClass('current');
     
     var matches = menuItem.filter(function() {    
     //return document.location.href.indexOf($(this).attr('href')) >= 0;
     return pathname.indexOf($(this).attr('href')) >= 0;       
     });
     
     matches.addClass('current');*/


    $("body").hide();
    $("body").fadeIn("slow", "swing");

    toggleSubMenuLeftBar();

    $(".fl-main-content-wrapper").delay(1000).animate({
        left: 0,
        opacity: 1
    }, "fast");

    $('.fl-user-avatar-wrapper').click(function () {
        var theme = $("#theme-color").val();

        // $("#fullColors").spectrum('set', theme);
        $('.fl-header-option').slideToggle();
    });

    $('.fl-user-wrapper').on('click', function () {
        $('.fl-header-option').slideUp();
        $('#fl-messages-dropflyout').fadeOut();
        $('#fl-notification-dropflyout').fadeOut();
        $('#fl-linklist-dropflyout').fadeOut();
        notiFlyoutBtn.attr('show-fl-flyout', false);
        msgFlyoutBtn.attr('show-fl-flyout', false);
        $('.fl-drop-item-link-list').find('.fa-caret-up').css({'display': 'none'});
        $('li.fl-drop-item-noti').find('.fa-caret-up').css({'display': 'none'});

        //$('.fl-user-navigation-wrapper').fadeOut();
    });

    $('.loaded_form_content').on('click', function(){
        $('.showReply').attr('active-comment', 'false');
        $('.showReply').removeAttr('style');
        $('.showReply').find('i.fa-comment').removeAttr('style');
        $('.fl-ui-request-comment').remove();  
    });

    function me(eleToOpme) {
        $(eleToOpme).find('.fl-delete').stop(false, false).animate({
            opacity: 1
        });
    }

    function ml(eleToOple) {
        $(eleToOple).find('.fl-delete').stop(false, false).animate({
            opacity: 0
        });
    }

    $("body").on('mouseenter', ".fl-widget-starred-content-wrapper", function () {
        me(this);
    });

    $("body").on('mouseleave', ".fl-widget-starred-content-wrapper", function () {
        ml(this);
    });

    $("body").on('mouseenter', ".fl-widget-starred-content-wrapperv2", function () {
        me(this);
    });

    $("body").on('mouseleave', ".fl-widget-starred-content-wrapperv2", function () {
        ml(this);
    });

    $('.fl-inbox-item').on('mouseenter', function () {
        $(this).find('.fl-delete').stop(false, false).animate({
            opacity: 1
        });
    });

    $(".fl-inbox-item").on('mouseleave', function () {
        $(this).find('.fl-delete').stop(false, false).animate({
            opacity: 0
        });
    });

    $('body').on('mouseenter', 'ul.notification li', function () {
        $(this).find('.fl-delete').stop(false, false).animate({
            opacity: 1
        });
    });

    $("body").on('mouseleave', "ul.notification li", function () {
        $(this).find('.fl-delete').stop(false, false).animate({
            opacity: 0
        });
    });

    $(".fl-module-icon li").on('mouseenter', function () {
        $(this).find('.fl-mod-title').addClass('fl-mod-title-hover')
    });

    $(".fl-module-icon li").on('mouseleave', function () {
        $(this).find('.fl-mod-title').removeClass('fl-mod-title-hover');
    });

    $(".fl-reply").click(function () {
        $(this).parents('.fl-message-body').eq(0).find('.fl-reply-box').fadeIn(500, function () {
            $(this).show();
        });

    });

    //$('#fl-create-btn').click(function(){
    //	$('#fl-send-msg-form').fadeIn(1000, function(){
    //		$(this).show();
    //		$(".crt_new_msg").show();
    //	});
    //	$('#fl-no-conversation-wrapper').hide();
    //});

//    $("#fl-action-btn").click(function(e) {
//        e.preventDefault();
//        $("#fl-action-option").slideToggle('fast');
//    });
    $('#tabs li a:not(:first)').addClass('inactive')
    $('.tab-container').hide();
    $('.tab-container:first').show();

    $('#tabs li a').click(function () {
        var t = $(this).attr('id');

        if ($(this).hasClass('inactive')) { //this is the start of our condition 
            $(this).removeClass('inactive').addClass('active');
            $('#tabs li a').not($(this)).removeClass('active').addClass('inactive');
            $('.tab-container').hide();
            $('#' + t + 'C').fadeIn('slow');
        }
        ;
    });

    $('#fl-save_personalInfo').click(function () {
        $('.uInfo_211, .eInfo_211').toggle();
        $('#fl-cancel_personalInfo').toggle();

        $(this).val() == "Edit" ? Edit() : Save();

    });


    function Edit() {
        $('#fl-save_personalInfo').val("Save");
    }

    function Save() {
        $('#fl-save_personalInfo').val("Edit");
    }

    $('#fl-cancel_personalInfo').click(function () {
        $('.uInfo_211, .eInfo_211').toggle();
        $(this).hide();

        $('#fl-save_personalInfo').val() == "Edit" ? Edit() : Save();
    });

    $('#portal-app li').on('mouseenter', function () {
        $(this).find('.fl-app-btn').stop(false, false).animate({
            'margin-top': 50,
            'border-width': '20px',
            'border-color': '#f00'
        }, 'fast');
    });

    $('#portal-app li').on('mouseleave', function () {
        $(this).find('.fl-app-btn').stop(false, false).animate({
            'margin-top': 180
        }, 'slow');
    });

    $('.fl-search-wrapper form').find('input[type="text"]').focusin(function () {
        $('input[type="button"]').fadeIn(400, function () {
            $(this).show();
        })
    });

    function toggleZoomScreen() {
        document.body.style.zoom = "50%"
    }

    $(document).scroll(function () {

        var scrollval = $(document).scrollTop();
        var scrollvalLeft = $(document).scrollLeft();
        //console.log("TOP", scrollval);
        //console.log("LEFT", scrollvalLeft);

        if (scrollval >= 50) {

            $('.fl-controlsv2-wrapper').css('background-color', 'rgb(245, 247, 250)');
            $('.tooltip').remove(); // remove tooltip on scroll

            if ($('.fl-user-header-wrapper').parents(".bgs-header").length >= 1) {

            } else {
                if (pathname == '/portal') {
                    $('.fl-user-header-wrapper').css('box-shadow', '');
                }else {
                    $('.fl-user-header-wrapper').css('box-shadow', '1px 1px 10px rgba(34, 34, 34, 0.58)');
                    $('#fl-scrollTotop').removeClass('isDisplayNone').addClass('isDisplayBlock');
                }
               // $('.fl-user-header-wrapper').css('box-shadow', '1px 1px 10px rgba(34, 34, 34, 0.58)');
                //$('#fl-scrollTotop').removeClass('isDisplayNone').addClass('isDisplayBlock');
                
            }
           
        } else if (scrollval < 50) {
            $('.fl-user-header-wrapper').css('box-shadow', '');
            $('#fl-scrollTotop').removeClass('isDisplayBlock').addClass('isDisplayNone');
            $('.fl-controlsv2-wrapper').css('background-color', '');
            $('.tooltip').remove(); // remove tooltip on scroll
            
        };

        //todo for preventing unwanted scroll
        if (scrollval >= 1) {


            if ($(document).height() == 696) {

            }else {
               $('.fl-user-header-wrapper').css({ 'position':'fixed' });
            }

        }else if (scrollval < 1) {
            $('.fl-user-header-wrapper').css({ 'position':'' });
        };

        if (scrollvalLeft >= 1) {
            $('.fl-user-header-wrapper').css({ 'position':'fixed' });
            $('.contentBar_workspace').css({'position':'relative', 'top':52});
            $('.loaded_form_content').not('body.ui-portal-body .loaded_form_content').css({'margin-top':52});
            $('.topPointerRuler-tip').css({'top':0});
        }else if (scrollvalLeft < 1) {
            $('.contentBar_workspace').css({'position':'relative', 'top': ''});
            $('.loaded_form_content').css({'margin-top':0});
            $('.topPointerRuler-tip').css({'top':''});
        };

    });

    

    var flWW = $('.fl-widget-wrapper');
    $(flWW).scroll(function () {
        var scrollvalWidgetWrapper = $(this).scrollTop();

        if (scrollvalWidgetWrapper >= 30) {
            $(this).parent().find('.fl-widget-head').css({
                'box-shadow': ' 0 4px 10px -6px #222',
                'position': 'relative',
                //z-index': 999 // conflicting with infinite nav
            });
        } else if (scrollvalWidgetWrapper < 30) {
            $(this).parent().find('.fl-widget-head').css('box-shadow', '');
        }
        ;
    });

    var flWW2 = $('.fl-widget-wrapper-scroll');
    $(flWW2).scroll(function () {
        var scrollvalWidgetWrapper = $(this).scrollTop();

        if (scrollvalWidgetWrapper >= 30) {
            $(this).parent().find('.fl-widget-head').css({
                'box-shadow': ' 0 4px 10px -6px #222',
                'position': 'relative',
                //'z-index': 999 // conflicting with infinite nav
            });
        } else if (scrollvalWidgetWrapper < 30) {
            $(this).parent().find('.fl-widget-head').css('box-shadow', '');
        }
        ;
    });

    var fDFS = $('.fl-drop-flyout-scroll');
    $(fDFS).scroll(function () {
        var scrollvalFlDropHead = $(this).scrollTop();

        if (scrollvalFlDropHead >= 30) {
            $(this).parent().find('.fl-dropflyout-head').css('box-shadow', '0px 3px 8px -5px black');
        } else if (scrollvalFlDropHead < 30) {
            $(this).parent().find('.fl-dropflyout-head').css('box-shadow', '');
        }

    });


    var gO = $('.generate_objects');
    $(gO).scroll(function () {

        var scrollvalflFormContrHd = $(this).scrollTop();
        if (scrollvalflFormContrHd >= 5) {
            $(this).parent().find('.fl-form-contr-hd').css('box-shadow', '0px 3px 8px -5px black');
        } else if (scrollvalflFormContrHd < 5) {
            $(this).parent().find('.fl-form-contr-hd').css('box-shadow', '');
        }
        ;

    });

    var workspaceBtnRelatedforms = $('.fl-workspace-button-wrapper-relatedforms');
    $(workspaceBtnRelatedforms).scroll(function () {

        var scrollvalflFormContrHd = $(this).scrollTop();

        if (scrollvalflFormContrHd >= 5) {
            $(this).parent().find('.fl-form-contr-hd').css('box-shadow', '0px 3px 8px -5px black');
        } else if (scrollvalflFormContrHd < 5) {
            $(this).parent().find('.fl-form-contr-hd').css('box-shadow', '');
        }

    });



    var flWdH = $('.fl-widget-wrapper').outerHeight();
    var flWCC = $('.fl-widget-wrapper-center-content').outerHeight();
    //$('.fl-widget-wrapper-center-content').css({'margin-top': flWdH / 2 - flWCC / 2 - 23});


    $("body").on("click", ".setCategory", function () {
        var dataID = $(this).attr("data-id")
        var cookie_url = $('#cookie_url').val();
        // $.cookie("application", dataID);
        document.cookie = "application=" + dataID + ';domain=' + cookie_url + ';path=/';
        //sessionStorage.setItem("application",dataID);
    })

    $("body").on("click", ".themeChange>li", function () {
        var theme = $(this).attr("data-theme");
        $.post("/ajax/theme", {theme: theme, action: "setTheme"}, function (data) {
            // alert(data)
            $(".user_css").attr("href", "/css/user_css/stylesheets/" + theme + ".css");
        })
    });

    $('body').on('click', '.fl-font-toggle, .themeChangeLi, .fl-toggle-nav-view, #AppThemePicker', function (event) {
        event.preventDefault();
        $(this).children('ul').slideToggle();
        $(this).children('div').slideToggle();
    });


    if (pathname == "/user_view/application_portal") {
        $(".themeChangeLi").hide();
    }

    //workspace control hide show 
    $('.fl-form-properties-toggle').on('click', function () {
        $(this).attr('datas', true);
        if ($(this).attr('fl-form-prop') === undefined || $(this).attr('fl-form-prop') == 'true') {
            $(this).attr('fl-form-prop', false);
            $(this).closest('.fl-form-properties-wrapper').animate({
                right: '-283'
            }, function () {
                $(this).find('.fl-form-properties-toggle').find('i').removeClass('fa-chevron-right').addClass('fa-chevron-left');
            });

        } else {
            $(this).attr('fl-form-prop', true);
            $(this).closest('.fl-form-properties-wrapper').stop(false, false).animate({
                right: 0
            }, function () {
                $(this).find('.fl-form-properties-toggle').find('i').removeClass('fa-chevron-left').addClass('fa-chevron-right');
            });
        }
    });

    $('.fl-form-properties-wrapper .fl-widget-head:first').on('mouseenter', function () {
        $('.fl-form-properties-toggle').stop(false, false).animate({
            left: '-39'
        });
    });

    $('.fl-form-properties-wrapper .fl-widget-head').on('mouseleave', function () {
        $('.fl-form-properties-toggle').stop(false, false).animate({
            left: '-6'
        });
    });

    //slide up / slide down of form options
    $('.fl-min-max-properties').on('click', function () {
        var properties_dropdown = $(this).attr('fl-show-opt');
        //$(this).parents('fl-form-properties-wrapper').css('position', 'relative')
        $(this).parent().next().css('max-height', '132px');
        $(this).parent().next().slideToggle();
        if (properties_dropdown == 'true') {
            $(this).attr('fl-show-opt', false);
            $(this).find('i').removeClass('fa-minus').addClass('fa-plus');

        } else {
            $(this).attr('fl-show-opt', true);
            $(this).find('i').removeClass('fa-plus').addClass('fa-minus');

        }
    });

    //$('.fl-min-max-properties').click();

    $('.fl-min-max-properties2').on('click', function () {
        var properties_dropdown = $(this).attr('fl-show-opt');
        //$(this).parents('fl-form-properties-wrapper').css('position', 'relative')
        //$(this).parent().next().css('height', '86px');
        $(this).parent().next().css('max-height', '132px');
        $(this).parent().next().slideToggle();
        if (properties_dropdown == 'true') {
            $(this).attr('fl-show-opt', false);
            $(this).find('i').removeClass('fa-minus').addClass('fa-plus');

        } else {
            $(this).attr('fl-show-opt', true);
            $(this).find('i').removeClass('fa-plus').addClass('fa-minus');

        }
    });

    //$('.fl-min-max-properties2').click();

    $('.fl-props-container').children('div:last-child').css('margin-bottom', '10px');


    // Tour Guide
    $("body").on("click", ".tour_guide_help", function () {
        var tour_name = $(this).attr("data-name");
        $("#wizard").stepy('destroy');
        $("#wizard-titles").remove();
        $("#wizard").html("");
        $(".tour_title_selected").html("");
        var self = $(this);
        self.find("img").show();
        //$('.wizard').stepy()
        //$("#takeATour").load("/json/take_tour/Sample.html");
        var ret = "";
        $.ajax({
            type: 'GET',
            url: '/json/tour_guide.json',
            dataType: 'json',
            context: document.body,
            global: false,
            async: false,
            success: function (data) {
                var name = data['tour_guide'][tour_name];
                console.log(name)
                // Set title of the selected tour guide link
                var tour_title_selected = name['title'];
                $(".tour_title_selected").text(tour_title_selected.replace(/_/g, " "));
                // Steps
                var tour_steps = name['steps'];

                for (var i = 0; i < tour_steps.length; i++) {
                    ret += '<fieldset title="' + tour_steps[i]['title'] + '">';
                    ret += '<legend>' + tour_steps[i]['subtitle'] + '</legend>';
                    ret += '<div class="row-form">';
                    ret += '<table>';
                    ret += '<tr>';
                    ret += '<td class="tour_img">' + tour_steps[i]['images'] + '</td>';
                    ret += '<td style="vertical-align: baseline;">';
                    if (tour_steps[i]['data-title'] != "") {
                        ret += '<p class="tour_title">' + tour_steps[i]['data-title'] + '</p>';
                    }

                    var content = tour_steps[i]['data-content'];
                    //console.log(content)
                    //var ret_content = "";

                    for (var i_content = 0; i_content < content.length; i_content++) {
                        //console.log(content[i_content])
                        ret += '<p class="tour_content">'
                        ret_content = content[i_content]['data'];
                        //console.log(ret_content)
                        ret += ret_content + "<br>";
                        ret += '</p>';
                    }

                    ret += '</td>';
                    ret += '</tr>';
                    ret += '</table>';
                    ret += '</div>';
                    ret += '</fieldset>';
                }
                self.find("img").hide();
                //return data;
            }
        });
        $("#wizard").html(ret);
        if ($("#wizard").length > 0)
            $('#wizard').stepy();

//		.responseText
    });

    function flgoBack() {
        history.back();
    }

    if (history.length > 1) {
        $('.fl-backhistory').on('click', function () {
            flgoBack();
        });
    } else {
        $('.fl-backhistory').css('display', 'none');
    }

// message , notification flyout

    var msgFlyoutBtn = $('.fl-top-nav-options').find('ul').children('li.fl-drop-item-msg');

    msgFlyoutBtn.on('click', function (event) {
        return; // turn off this.
        event.preventDefault();
        var attrShow = $(this).attr('show-fl-flyout');

        if (attrShow == 'false') {
            $(this).attr('show-fl-flyout', true);
            $('#fl-messages-dropflyout').fadeIn('fast', function () {
                notiFlyoutBtn.attr('show-fl-flyout', false);

            });
            $('#fl-notification-dropflyout').fadeOut()
            $('.fl-drop-flyout-scroll').perfectScrollbar('update');

        } else if (attrShow == 'true') {
            $(this).attr('show-fl-flyout', false);
            $('#fl-messages-dropflyout').fadeOut(function () {
                $(this).css('top', '50px');
            });
        }
        ;

    });
    var notiFlyoutBtn = $('.fl-top-nav-options').find('ul').children('li.fl-drop-item-noti');

    notiFlyoutBtn.on('click', function (event) {
        event.preventDefault();
        console.log("notif clicked")
        var attrShow = $(this).attr('show-fl-flyout');
        var self = this;
        var flyoutCaret = $(self).find('.fa-caret-up');
        //console.log(flyoutCaret);

        var loaded = $(self).attr("loaded");
        if (attrShow == 'false') {
            $(self).attr('show-fl-flyout', true);
            $('#fl-notification-dropflyout').fadeIn('fast', function () {
                msgFlyoutBtn.attr('show-fl-flyout', false);
                linkListBtn.attr('show-fl-flyout', false);
            });
            $('#fl-messages-dropflyout').fadeOut();
            $('#fl-linklist-dropflyout').fadeOut();
            $('.fl-drop-flyout-scroll').perfectScrollbar('update');
            //$(".noti-counter").text("0");
            // $(".noti-counter").remove();
            updateTitleWithCountNotif()
            $(flyoutCaret).css({'display': 'block'});
            $('.fl-drop-item-link-list').find('.fa-caret-up').css({'display': 'none'});
            
            if(loaded==undefined){
                //on fly load
                user_notification.loadView(0,{"type":2},function(count){
                    if(count==0){
                        $(".noti-fly").html("<div class='noti-fly-text' style='color: rgb(66, 66, 66); text-align: center; display: inherit; padding: 16px; background-color: #f4f4f4; box-sizing: border-box;border-radius: 6px;'>No Notification Found</div>");
                        $('#fl-notification-dropflyout').css({
                            'height': '100%',
                            'padding-bottom': '4px'
                        });
                        $('#fl-notification-dropflyout').find('.fl-dropflyout-foot').hide();  
                        $('label[for="checkall-inmail-noti"]').hide();

                        $('.fl-inmail-notificationbtns').attr('hide', 'count0');
                    }
                    $(self).attr("loaded","loaded");
                    $('.fl-drop-flyout-scroll').perfectScrollbar('destroy');
                    $('.fl-drop-flyout-scroll').perfectScrollbar();
                })
            }

            // $.post("/ajax/notification", {"action": "updateisRead"}, function (data) {

            // });
        } else if (attrShow == 'true') {
            $(this).attr('show-fl-flyout', false);
            $('#fl-notification-dropflyout').fadeOut(function () {
                $(this).css('top', '53px');
            });
            $(flyoutCaret).css({'display': 'none'});
        }
        ;
    });

    getDetailFn();

    var linkListBtn = $('.fl-top-nav-options').find('ul').children('li.fl-drop-item-link-list');

    linkListBtn.on('click', function(event){
        event.preventDefault();
        var attrShow = $(this).attr('show-fl-flyout');
        var self = this;
        var flyoutCaret = $(self).find('.fa-caret-up');
        var loaded = $(self).attr("loaded");
        if (attrShow == 'false') {
            $(self).attr('show-fl-flyout', true);
            $('#fl-linklist-dropflyout').fadeIn('fast', function () {
                notiFlyoutBtn.attr('show-fl-flyout', false);
            });
            $('#fl-notification-dropflyout').fadeOut();
            $('li.fl-drop-item-noti').find('.fa-caret-up').css({'display': 'none'});
            $(flyoutCaret).css({'display': 'block'});
            if(loaded==undefined){
                var loading = $('<div align="center" class="notif-loading"><img src="/images/icon/loading.gif"></div>');
                $('.link-fly-drop').append(loading);

                $.post("/ajax/link_maintenance",{action:"loadLinkMaintenance"},function(data){
                    $('.link-fly-drop').html(data)
                    $(self).attr("loaded","loaded")
                    $('.fl-drop-flyout-scroll').perfectScrollbar('destroy');
                    $('.fl-drop-flyout-scroll').perfectScrollbar();
                })
            }
        }
        else if (attrShow == 'true') {
            
            $(self).attr('show-fl-flyout', false);
              $('#fl-linklist-dropflyout').fadeOut(function () {
                $(this).css('top', '53px');
            });
            $(flyoutCaret).css({'display': 'none'});
        }; 



    })
    // Suggestion Box
    $("body").on("click",".suggestionBox",function(){
        AddSuggestion.modal_suggestion();
    });

    $("body").on("change", ".import_request_file", function () {
        var fpath = $(this).val().replace(/\\/g, '/');
        var a = fpath.split("/");
        var aParameter = a.length;
        //var fnameLength = fname.length;
        $(".import_request_txt").text("Filename: " + a[aParameter - 1]);
    });

    var scrollval = $(document).scrollTop();

    if (scrollval >= 1) {
        $('.fl-user-header-wrapper').css({ 'position':'fixed' }); 
    }

    $("body").on("click",".removeStarred, .removeStarred_user, .removeStarred_user_widget",function(){
        var id = $(this).attr("counterPart-id");
        Starred.updateData(this,id);
        var objStarred = {"type":"0"};
        Starred.load(0,"start",objStarred,function(result){
            Starred.loadAfterAjaxStarredWidget(result);
            $(".fl-starred-container-users").perfectScrollbar("destroy");
            $(".fl-starred-container-users").perfectScrollbar();
        });
    })
});

function updateTitleWithCountNotif(sounds){
    if(!sounds){
        sounds = false;
    }

    var count_notif = if_undefinded($.trim($("#noti-counter").text()),0);
    count_notif = (count_notif || 0);
    count_notif = parseInt(count_notif);


    var count_message = if_undefinded($.trim($("#fl-message-counter").text()),0);
    count_message = (count_message || 0);
    count_message = parseInt(count_message);

    var totalNotif = count_notif + count_message;

    if(totalNotif == 0){
        return false;
    }

    displayTitleCount(totalNotif, sounds);



    // var title = $("title").text();
   
    // var orig_title = $("title").attr("orig_title");

    // if(typeof orig_title!= "undefined"){
    //     title = orig_title;
    // }


    // $("title").attr("orig_title",title);

    // $("title").text("("+ totalNotif +") "+title)
    
}

function displayTitleCount(messageCount, playSound) {

    if (messageCount && messageCount >= 1) {
        if(messageCount>99){
            messageCount = "99+";
        }
        document.title = '(' + messageCount + ') ' + document_title;

    } else {
        document.title = title;
    }

    if (playSound && playSound === true) {
        if (Audio) {
            var snd = new Audio("/js/assets/sound/ping.wav"); // buffers automatically when created
            snd.play();
        } else {
            console.log('Audio not available');
        }
    }

}

function getCookie(cname)
{
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++)
    {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}


CSIDSearchUser = {
    "init": function () {
        this.eventKeyUp();
        this.toggleContainer();
    },
    "eventKeyUp": function () {
        var self = this;
        $("body").on("keyup", ".CSID-search-user-txt", function () {
            var val = $.trim($(this).val().toLowerCase());
            self.fnSearch(val, this)
            $(".content-dialog-scroll").perfectScrollbar("update");

            var this_container_scroll = $(this).closest(".CSID-search-container").find('.content-dialog-scroll');
            $(this_container_scroll).animate({
                scrollTop: 0
            }, 1);
            setTimeout(function () {
                $(this_container_scroll).perfectScrollbar("destroy");
                $(this_container_scroll).perfectScrollbar();
            }, 100)
        })
    },
    "fnSearch": function (value, self) {
        var container = $(self).closest(".CSID-search-container");
        var text = "";
        var totalData = "";
        var totalHideData = ""
        container.find(".container-formUser").each(function () {
            $(this).find(".CSID-search-data").each(function () {
                text = $(this).find(".CSID-search-data-text").text().toLowerCase();
                if (text.indexOf(value) == -1) {
                    $(this).addClass("display")
                } else {
                    $(this).removeClass("display")
                }
            })
            totalData = $(this).find(".CSID-search-data").length;
            totalHideData = $(this).find(".CSID-search-data.display").length
            if (totalData == totalHideData) {
                $(this).find(".empty-users").removeClass("display");
            } else {
                $(this).find(".empty-users").addClass("display");
            }
        })
    },
    "toggleContainer":function(){
        //added aaron for hiding of container
        $("body").on("click",".formUser",function(){
            var showPanel = $(this).attr("rel");
            var container = $(this).closest(".container-formUser");
            container.find("."+showPanel).slideToggle(function(){
                var displ = $(this).css("display");
                if(displ=="block" || displ=="inline-block" ){
                    $(this).css("display","inline-block");
                    $(this).closest(".container-formUser").find("label.icon").removeClass("icon-plus").addClass("icon-minus")
                    $(this).closest(".container-formUser").find("label.fa").removeClass("fa-plus").addClass("fa-minus")
                }else{
                    $(this).closest(".container-formUser").find("label.icon").removeClass("icon-minus").addClass("icon-plus")
                    $(this).closest(".container-formUser").find("label.fa").removeClass("fa-minus").addClass("fa-plus")
                }
                $('.content-dialog-scroll').perfectScrollbar('update');
                //$('#content-dialog-scroll').perfectScrollbar();

            });
        }) 
    }
}

function toggleSubMenuLeftBar() {
    $(".mainmenu li.fl-toggle-sub").children('a').click(function (event) {
        event.preventDefault();
        $(this).parent().children('.main-submenu').slideToggle('fast');

        if ($(this).children('.fa-folder-o').length == 1) {
            $(this).children('.fa-folder-o').removeClass('fa-folder-o').addClass('fa-folder-open-o');
        } else {
            $(this).children('.fa-folder-open-o').removeClass('fa-folder-open-o').addClass('fa-folder-o');
        }
        ;

        if ($(this).children('.fl-nav-down-icon').find('.fa-chevron-down').length == 1) {
            $(this).children('.fl-nav-down-icon').find('.fa-chevron-down').removeClass('fa-chevron-down').addClass('fa-chevron-up')
        } else {
            $(this).children('.fl-nav-down-icon').find('.fa-chevron-up').removeClass('fa-chevron-up').addClass('fa-chevron-down')
        }
        ;
    });
}

function getDetailFn() {
    $(".getDetailFn").click(function () {
        var json = if_undefinded($(this).attr("json"), "");
        if (json != "") {
            json = JSON.parse(json);
            var ret = "";
            ret += '<h3 class="fl-margin-bottom"><i class="fa fa-align-left"></i> Details</h3>';
            ret += '<div class="hr"></div>';
            ret += '<div class="fields fl-form-details-wrap">';
            $.each(json, function (key, value) {
                ret += '<div class="fl-form-details fl-table-ellip" title="' + value + '">' + key + ': ' + value + '</div>';
            })
            ret += '</div>';

            var newDialog = new jDialog(ret, "", "", "", "", function () {
            });
            newDialog.themeDialog("modal2");

        }
    })
}

function displayDraftWorkflows() {
    $('.fl-inProgress-workflow').click(function () {
        var ret = "";
        ret += '<h3 class="fl-margin-bottom"><i class="fa fa-spinner"></i> In Progress Workflows</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div class="fields fl-inProgress-workflow-list-wrap">';
        ret += '<div class="fl-list-of-app-record-wrapper">';
        ret += '<div class="fl-table-wrapper"  style="min-height:250px">';
        ret += '<table class="display_data dataTable loadLatestModifiedWorkflow"  id="loadLatestModifiedWorkflow">';
        ret += '<thead class="fl-header-tbl-wrapper">';
        ret += '<tr>';
        ret += '<th style="width: 20px !important;"><div class=""></div><div class="fl-table-ellip"></div></th>';
        ret += '<th><div class=""></div><div class="fl-table-ellip">Title</div></th>';
        ret += '<th><div class=""></div><div class="fl-table-ellip">Status</div></th>';
        ret += '<th><div class=""></div><div class="fl-table-ellip">Date Created</div></th>';
        ret += '<th><div class=""></div><div class="fl-table-ellip">Editor</div></th>';
        ret += '</tr>';
        ret += '</thead>';
        ret += '</table>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';

        var newDialog = new jDialog(ret, "", "600", "", "", function () {
        });
        newDialog.themeDialog("modal2");
        loadLatestModifiedWorkflow.init(function () {
        });
    });
}
var jsonLatestModifiedWorkflowData = {
    "endlimit": "5",
    "search_value": "",
    "column-sort": "date",
    "column-sort-type": "DESC"

}
loadLatestModifiedWorkflow = {
    init: function (callback) {
        var oTable = $('#loadLatestModifiedWorkflow').dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> ' +
                        '<div class="bar1"></div> ' +
                        '<div class="bar2"></div> ' +
                        '<div class="bar3"></div> ' +
                        '<div class="bar4"></div> ' +
                        '<div class="bar5"></div> ' +
                        '<div class="bar6"></div> ' +
                        '<div class="bar7"></div> ' +
                        '<div class="bar8"></div> ' +
                        '<div class="bar9"></div> ' +
                        '<div class="bar10"></div> ' +
                        '</div>'
            },
            "oColReorder": false,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/search",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({"name": "search_value", "value": jsonLatestModifiedWorkflowData['search_value']});
                aoData.push({"name": "action", "value": "loadLatestModifiedWorkflow"});
                aoData.push({"name": "column-sort", "value": jsonLatestModifiedWorkflowData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonLatestModifiedWorkflowData['column-sort-type']});
                aoData.push({"name": "endlimit", "value": jsonLatestModifiedWorkflowData['endlimit']});
                aoData.push({"name": "form_id", "value": $("#FormId").val()});
                console.log(jsonLatestModifiedWorkflowData)
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        // console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function (oSettings, json) {
                var obj = '#loadLatestModifiedWorkflow';
                var listOfappContainer = '.fl-list-of-app-record-wrapper';
                dataTable_widget(obj, listOfappContainer);
            },
            fnDrawCallback: function () {

                $(".tip").tooltip();
                if (callback) {
                    callback(1);
                }
            }
        });
        return oTable;
    }
}

function urlTofixedLayout() {
    
    var pathname = window.location.pathname;

    var urlFormWorkSpaceTest = '/user_view/workspace'; 
    if (pathname === urlFormWorkSpaceTest) {
        fixedLayout();

    }
    
    var urlCreateForms = '/user_view/formbuilder'; 
    if (pathname === urlCreateForms) {

        var isSafari = navigator.vendor.indexOf("Apple")==0 && /\sSafari\//.test(navigator.userAgent); // true or false
        if (isSafari) {
           $('.fl-input-fields_a').addClass('fl-input-fields_a-safariB');
            $('.fl-input-fields_d').addClass('fl-input-fields_d-safariB');
        };   

    };

    // var fl_datatable_wrapper = $('.fl-datatable-wrapper');
    // var window_height = $(window).height();
    // if(fl_datatable_wrapper.length == 1){
    //     console.log(window_height);
    // }

 

}

function fixedLayout() {

    $('.fl-content').css({
        'padding': '0px'
    });

    var flWorkspaceControlContainerHeight = $('.fl-workspace-control-container').height() + 10;
    //console.log("HSIASD", flWorkspaceControlContainerHeight);
    
    if ($('.fl-workspace-control-container').css('display') == 'none') {
        $('.loaded_form_content').css({
            'top':'0px'
        });
    }else {
        $('.loaded_form_content').css({
            'top': +flWorkspaceControlContainerHeight + 'px'
        });
    }

    function uploadFileWorkspace() {

        var uploadfileWorkspaceWrap = $('.fl-uploadfile-workspace');
        var download_attachment = $('.download_attachment');
        //console.log($(download_attachment).length);

        if (uploadfileWorkspaceWrap.hasClass('isDisplayNone')) {
            if (download_attachment.length == 0) {

            } else {
                uploadfileWorkspaceWrap.removeClass('isDisplayNone').addClass('isDisplayBlock');
                //alert('1');
                var flWorkspaceControlContainerHeight = $('.fl-workspace-control-container').height() + 10;
                $('.loaded_form_content').css({
                    'top': +flWorkspaceControlContainerHeight + 'px'
                });

            }
        }

        $('.uploadFileWorkspace').on('click', function () {

            if (uploadfileWorkspaceWrap.hasClass('isDisplayBlock')) {
                var flWorkspaceControlContainerHeight = $('.fl-workspace-control-container').height() + 10;
                $('.loaded_form_content').css({
                    'top': +flWorkspaceControlContainerHeight + 'px'
                });
            }

        });

        $('.uploadFileWorkspace').change(function () {

            uploadfileWorkspaceWrap.removeClass('isDisplayNone').addClass('isDisplayBlock');

            if (uploadfileWorkspaceWrap.hasClass('isDisplayBlock')) {
                var flWorkspaceControlContainerHeight = $('.fl-workspace-control-container').height() + 10;
                $('.loaded_form_content').css({
                    'top': +flWorkspaceControlContainerHeight + 'px'
                });
            }

        });

        $('body').on('click', '#popup_ok', function () {

            if ($('.removeFiles').length === 1) {
                uploadfileWorkspaceWrap.addClass('isDisplayNone').removeClass('isDisplayBlock');
                var flWorkspaceControlContainerHeight = $('.fl-workspace-control-container').height() + 10;
                $('.loaded_form_content').css({
                    'top': +flWorkspaceControlContainerHeight + 'px'
                });
            }

        });

    }
    uploadFileWorkspace();
  

}



showUserIconNav = {

    'init': function(parent){
        var self = this;
      /*  console.log(parent)*/
        var item = $(parent).find('.fl-ShowUsers');
        var divToShow = $(parent).find('.fl-showUserdiv');
        //console.log(item);

        $(item).on({

            'mouseenter': function(){
                self.showUserDiv(this, divToShow, parent);
            }

        });

        $(item).on({

            'mouseleave': function(){
               self.hideUserDiv(this, divToShow, parent);
            }

        });

        $(divToShow).on({
            'mouseenter': function(){
                self.showUserDiv(this, divToShow, parent);
            }
        });

        $(divToShow).on({
            'mouseleave': function(){
                self.hideUserDiv(this, divToShow, parent);
            }
        });

    },

    'showUserDiv': function(self, item, parent){
       $(item).removeClass('isDisplayNone').addClass('isDisplayBlock');
    },

    'hideUserDiv': function(self, item, parent){
        $(item).removeClass('isDisplayBlock').addClass('isDisplayNone');
    }

};


function checkResponsiveWidth(){

    var portraitPhonea = 320;
    var windowWidth = $(window).width();
    var informuserMsg = 'Please rotate your phone to landscape';
    var informuser  = $('<div id="informuser-wrapper" class="isDisplayTable"><div class="isDisplayTableCell">'+ informuserMsg +'</div></div>');
    var fl_toggle_nav_view  = $('.fl-toggle-nav-view');

    $(window).on("resize", function () {
        if($(window).width() == 480){
            fl_toggle_nav_view.addClass('isDisplayNone');
        }
    }).resize();
    $(window).bind('enterBreakpoint320', function () {
        
        var isInIframe = (window.location != window.parent.location) ? true : false;
        if(!isInIframe){
            //$('body.breakpoint-320').append(informuser);
        }

        
        
    });

    $(window).bind('exitBreakpoint320', function () {
        console.log('EXIT BREAKPOINT 320');
        $('#informuser-wrapper').remove();
        //console.log('EXIT BREAKPOINT 320', $('body').find('iframe')[0].contentWindow.$('#informuser-wrapper').length); //$('body').find('iframe').contents().find('#informuser-wrapper').length
        fl_toggle_nav_view.addClass('isDisplayNone');       
    });


    $(window).setBreakpoints();

}

checkWindowHeight = {

    'init':function(objelem, elea, eleb, elec, container){
   
        checkWindowHeight.element(objelem, elea, eleb, elec, container);
        console.log("objelem", $(objelem).css('height'), "elea", $(elea).css('height'), $(eleb).css('height'), $(elec).css('height'));
       
    },

    'element':function(objelem, elea, eleb, elec, container){

       var windowHeight = $(window).height();
       var container_padding_top = $(container).css('padding-top');
       var container_padding_top_str = container_padding_top.replace("px", "");
       var container_padding_bottom = $(container).css('padding-bottom');
       var container_padding_bottom_str = container_padding_top.replace("px", "");
       var elea = $(elea).outerHeight();
       var eleb = $(eleb).outerHeight();
       var elec = $(elec).outerHeight();
       $(objelem).css({
            'height':windowHeight - elea - eleb - elec - container_padding_top_str - container_padding_bottom_str
       });


    }

}

//force set width on fnInitComplete datatable
function dataTableSetwidth(element, width, boxSizing){

    $(element).css({
        'width':width+'px',
        'box-sizing':boxSizing
    });

}