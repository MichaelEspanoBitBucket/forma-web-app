// var pathname = window.location.pathname;
// if (pathname == "/user_view/workspace") { }
function cancel_embed_action(element){

	var resultFld = "";
	// if($.type($(this).closest('[embed-result-field-val]').attr('source-form-type')) != "undefined"){
	// 	if($(this).closest('[embed-result-field-val]').attr('source-form-type') == "multiple"){
	// 		var data_object = $('body').data('user_form_json_data')['form_json'][$(this).closest('[embed-result-field-val]').closest('.setOBJ').attr('data-object-id')];
	// 		var form_id = $(this).closest('tr').attr('form-id');
	// 		var get_form_data  = data_object['multiple_forms'].filter(function(val, index){
	// 			console.log("val", val, "index", index);
	// 			return val['source_form'] == form_id;
	// 		});
	// 		resultFld = get_form_data[0]['search_field'];
	// 		var cloned_element = $(this).parents('tr').data('clone_element');
	// 		// $(this).closest('tbody').append($(cloned_element));
	// 		var tr = $(this).parents('tr');
	// 		var embedded_view_element = $(tr).closest('[embed-result-field-val]');
	// 		var element_data = $('body').data('user_form_json_data')['form_json'][embedded_view_element.closest('.setOBJ').attr('data-object-id')];
	// 		if(typeof embedded_view_element.data('selected_rows') == "undefined" || embedded_view_element.data('selected_rows').length <= 0){
	// 			initializeMultipleEmbedddedView(embedded_view_element, "oncancel");
	// 			if(embed_inline_add==1){
	// 				$(document).contextmenu("enableEntry", "add", true);
	// 				$(document).contextmenu("enableEntry", "copy", true);
	// 			}
	// 		}
	// 		else{
	// 			var selected_rows = embedded_view_element.data('selected_rows')||[];
	// 			for(i in selected_rows){
	// 				var row = $(selected_rows[i]);
	// 				var clone_html = row.data('clone_element');
	// 				row.html(clone_html);
	// 				row.find('[name="record_selector"]').prop('checked', true);
	// 			}
	// 			// tr.html(cloned_element);
	// 			// console.log("trasdasdasd", tr.find('[name="record_selector"]'));
	// 			// tr.find('[name="record_selector"]').prop('checked', true);
	// 			// console.log("element_data", element_data, embedded_view_element.closest('.setOBJ').attr('data-object-id'));
	// 			rebindViewEvents($(tr).find('.viewTDEmbedded'), embedded_view_element, element_data['multiple_forms']);
	// 		}
	// 	}
	// 	else{
	// 		resultFld = $(this).closest('[embed-result-field-val]').attr("embed-result-field-val");
	// 	}
	// }
	// else{
	resultFld = $(this).closest('[embed-result-field-val]').attr("embed-result-field-val");
	// }
	var recordID = $(this).parent().parent().parent().parent().attr("record-id");
	/*cancel inline add*/
	// if($(this).parents('tbody').find('.record_selector:checked').length <= 0){
	$('.editEmbedClass').not($(element)).each(function(){
	    $(this).removeClass("disable-clickable");
	});
	// }
	if($(element).is("#canceladdEmbedded")){
		
		$("[name='" + resultFld + "']").trigger("change");
		if(embed_inline_add==1){
			$(document).contextmenu("enableEntry", "add", true);
			$(document).contextmenu("enableEntry", "copy", true);
		}
		
		
	}
	/*cancel inline edit*/
	else if($(element).is("#canceleditDEmbedded")){
		if($.type($(this).closest('[embed-result-field-val]').attr('source-form-type')) != "undefined"){
			if($(this).closest('[embed-result-field-val]').attr('source-form-type') == "multiple"){
				initializeMultipleEmbedddedView($(this).closest('.embed-view-container'), 'oncancel');
			}
			else{
				$("[name='" + resultFld + "']").trigger("change");
				if(embed_inline_add==1){
					$(document).contextmenu("enableEntry", "copy", true);
				}
			}
		}
		else{
			$("[name='" + resultFld + "']").trigger("change");
			if(embed_inline_add==1){
				$(document).contextmenu("enableEntry", "copy", true);
			}
		}
		
		   
	}
	$('.tooltip').remove();	
}

function embed_row_fixer(element){
	var target = $(element);
	var record_id_array = [];
	var tr_clone = "";
	
	if(target.is('tr')){

		tr_clone = target.clone();
	}
	else{
		tr_clone = target.closest('tr').clone();
		
	}
	tr_clone.removeClass('embed-context-menu');
	target.closest('tbody').find('tr').not('tr.appendNewRecord:last').each(function(){
		var existing_record_id = $(this).attr('record-id');
		record_id_array.push(existing_record_id);
	});
	record_id_array = record_id_array.sort(function(a, b){return b-a});
	var new_record_id = Number(record_id_array[0])+1;
	var old_record_id = tr_clone.attr('record-id');
	tr_clone.attr('record-id',new_record_id);
	tr_clone.addClass('tr_'+(new_record_id)).removeClass('tr_'+old_record_id);
	
	tr_clone.find('.inlineEdit_'+old_record_id).each(function(){
		var radio_tr = $(this).closest('tr').index();
		var radio_td = $(this).closest('td').index();

		if($(this).is('[type="radio"]')){
			
			$(this).prop("checked",false).addClass('inlineEdit_'+new_record_id).removeClass('inlineEdit_'+old_record_id);
			$(this).attr('name','radio_'+(radio_tr+""+radio_td));
		}
		else if($(this).is('[type="checkbox"]')){
			$(this).prop("checked",false).addClass('inlineEdit_'+new_record_id).removeClass('inlineEdit_'+old_record_id);
			$(this).attr('name','checkbox_'+(radio_tr+""+radio_td));
		}
		else{
			$(this).val("").addClass('inlineEdit_'+new_record_id).removeClass('inlineEdit_'+old_record_id);
		}
	});
	tr_clone.find('.lblEmbed_'+old_record_id).each(function(){

		$(this).addClass('display').siblings().removeClass('display');
	});
	tr_clone.find('td:last').find('i').not('.inline_add_request, .cancel_inline_add_request').addClass('display');
	tr_clone.find('td:last').find('i.inline_add_request, .cancel_inline_add_request').removeClass('display').children().removeClass('display');
	
	return tr_clone;

}

function checkEmbedAttr(element){
	element.addClass('activeActionClass');
	var self_ele = $(element).find('td:eq(0)');	
  	
	if($('#enable_inline_add_embed').text() == "1"){
        if($(this).parents('tbody').find('.record_selector:checked').length > 0){
        	$('.editEmbedClass').not(self_ele).each(function(){
        	    $(this).addClass("disable-clickable");
        	});
        }
    }

	var recordID = self_ele.closest("[record-id]").attr('record-id');
	// console.log("self_ele",self_ele,recordID);
	var col_header = $(element).find(".inlineEdit_" + recordID).closest('td:not(.picklist-td)').map(function(){
	    var c_index = $(this).index();
	    console.log("c_index", c_index);
	    var col_head_ele = $(this).closest("table").find(".columnHeader").filter(function(){
	    	console.log("parent_index", $(this).parent().index(), "c_index", c_index, "parent", $(this).parent());
		return $(this).parent().index() === c_index;
	    });
	    
	    return {
		"column_index":c_index,
		"column_title":col_head_ele.attr("data-fld-name"),
		"column_fld_name":col_head_ele.attr("data-form-fld-name"),
		"column_fld_type":col_head_ele.attr("data-fld-type"),
		"column_input_fld_type":col_head_ele.attr("data-input-fld-type"),
		"column_object_fld_type":col_head_ele.attr("data-field-object-type"),
		"column_object_fld_formula":col_head_ele.attr("data-field-formula"),
		"column_object_fld_formula_type":col_head_ele.attr("data-field-formula-type"),
		"column_object_fld_readonly_state":col_head_ele.attr("data-readonly-state"),
		"column_listed_used_atkeys_formula":col_head_ele.attr('data-all-used-atkey-formula'),

	    };
	}).get();

	console.log('col_header',col_header);
	$(element).find(".inlineEdit_" + recordID).each(function(){

	    var inline_edit_ele_index = $(this).closest('td:not(.picklist-td)').index();
	    var header_title_equivalent = col_header.filter(function(val,ai){
	    	//console.log("filteredcol",inline_edit_ele_index,val['column_index']);
		return val['column_index'] == inline_edit_ele_index;
	    })[0]['column_title'];
	    var header_fld_title_equivalent = col_header.filter(function(val,ai){
		return val['column_index'] == inline_edit_ele_index;
	    })[0]['column_fld_name'];
	    var attr_fld_type = col_header.filter(function(val,ai){
		return val['column_index'] == inline_edit_ele_index;
	    })[0]['column_fld_type'];
	    var attr_fld_type_input = col_header.filter(function(val,ai){
			return val['column_index'] == inline_edit_ele_index;
	    })[0]['column_input_fld_type'];
	    //console.log("BROOM",inline_edit_ele_index,header_title_equivalent);
	    $(this).attr('equivalent-header-title',header_title_equivalent);
	    $(this).attr('equivalent-fld-title',header_fld_title_equivalent);
	    $(this).attr('equivalent-fld-type',attr_fld_type);
	    $(this).attr('equivalent-fld-input-type',attr_fld_type_input);
	    if($(this).attr('type') == "radio"){
	    	$(this).attr('old-value', $(element).find('[name="' + $(this).attr('name') + '"]:checked').val());
	    }
	    else if($(this).attr('type') == "checkbox"){
	    	$(this).attr('old-value', $(element).find('[name="' + $(this).attr('name') + '"]:checked').map(function(){return $(this).val(); }).get().join("|^|"));
	    }
	    else if($(this).is('SELECT') == true && $(this).is('[multiple="multiple"]')){
	    	$(this).attr('old-value', $(this).children('option:selected').map(function(){$(this).val()}).get().join('|^|'));
	    }
	    else{
	    	$(this).attr('old-value', $(this).val()||"");
	    }
	    // prepareForValidation.call($(this),[attr_fld_type,attr_fld_type_input]);
	   

	});
	validateEmbedFields();
	
	// Label
	var empty_col = $(element).find(".lblEmbed_" + recordID).filter(function(){
		return $(this).siblings().length != 0;
	});
	
	empty_col.each(function(){
		
	    var inline_edit_ele_index = $(this).closest('td:not(.picklist-td)').index();
	        
	    var header_title_equivalent = col_header.filter(function(val,ai){
	    	
		return val['column_index'] == inline_edit_ele_index;
	    })[0]['column_title'];

	    var header_fld_title_equivalent = col_header.filter(function(val,ai){
		return val['column_index'] == inline_edit_ele_index;
	    })[0]['column_fld_name'];
	    var attr_fld_type = col_header.filter(function(val,ai){
		return val['column_index'] == inline_edit_ele_index;
	    })[0]['column_fld_type'];
	    console.log($(this), attr_fld_type, header_title_equivalent, header_fld_title_equivalent);
	    //console.log("BROOM",inline_edit_ele_index,header_title_equivalent);
	    $(this).attr('equivalent-header-title',header_title_equivalent);
	    $(this).attr('equivalent-fld-title',header_fld_title_equivalent);
	    $(this).attr('equivalent-fld-type',attr_fld_type);
	});
	
	var filter_not_valid_fields = function(index_val, ele_val){
	    return true;
	    var dis_ele = $(ele_val);
	    var pass_col_header = col_header;
	    var td_ele = dis_ele.parents('td:not(.picklist-td)').eq(0);
	    var td_index = td_ele.index();
	    var col_header_eq_data = pass_col_header.filter(function(json_val, index_val){
			return json_val['column_index'] == td_index;
	    });
	   // console.log("col_header_eq_data",pass_col_header,col_header_eq_data,col_header_eq_data[0]['column_object_fld_formula'])
	    if(col_header_eq_data[0]['column_object_fld_formula'].indexOf('//')==0){
	    	
	    	col_header_eq_data[0]['column_object_fld_formula'] = "";
	    	col_header_eq_data[0]['column_object_fld_formula_type']="";
	    }
	    var temp_regexp = new RegExp("@"+col_header_eq_data[0]['column_fld_name']+"(?![a-zA-Z0-9_])", "g");
	    var match_used_in_formula = col_header_eq_data[0]['column_listed_used_atkeys_formula'].match(temp_regexp);
	    match_used_in_formula = ($.type(match_used_in_formula) == "array")?match_used_in_formula:[];
	    console.log('used formula',match_used_in_formula,col_header_eq_data[0]['column_fld_name'],'test',col_header_eq_data[0]['column_object_fld_formula'])
	    // alert(col_header_eq_data[0]['column_fld_name']+" ====== "+col_header_eq_data[0]['column_object_fld_readonly_state']);
	    // alert(col_header_eq_data[0]['column_listed_used_atkeys_formula'].indexOf(col_header_eq_data[0]['column_fld_name'])+" ~~~~ "+col_header_eq_data[0]['column_fld_name']);
	    if (
	    	match_used_in_formula.length <= 0 &&
	    	col_header_eq_data[0]['column_object_fld_readonly_state'] != "1" &&
	    	col_header_eq_data[0]['column_object_fld_formula_type'] != 1 &&
			// col_header_eq_data[0]['column_object_fld_type'] != 'radioButton' &&
			//col_header_eq_data[0]['column_object_fld_type'] != 'dropdown' &&
			( $.type(col_header_eq_data[0]['column_object_fld_formula'] ) == "undefined" || col_header_eq_data[0]['column_object_fld_formula']  == "" ) &&
			( col_header_eq_data[0]['column_object_fld_formula_type'] == "static" || col_header_eq_data[0]['column_object_fld_formula_type'] == "" || $.type(col_header_eq_data[0]['column_object_fld_formula_type']) == "undefined" ) &&
			
			col_header_eq_data[0]['column_object_fld_type'] != '' && (
			    col_header_eq_data[0]['column_fld_type'] == 'longtext' ||
			    col_header_eq_data[0]['column_input_fld_type'] == 'Currency' ||
			    col_header_eq_data[0]['column_input_fld_type'] == 'Number' ||
			    col_header_eq_data[0]['column_fld_type'] == 'date' ||
			    col_header_eq_data[0]['column_fld_type'] == 'dateTime'||
			    col_header_eq_data[0]['column_fld_type'] == 'time'
			)
	    ) {
			return true;
	    }
	    return false;
	};
	//console.log("filtered fields",filter_not_valid_fields);
	$(element).parents('.embed-view-container').find('th div').each(function(index){
	    if($(this).attr('data-readonly-state') == "1"){
	        var field_name = $(this).attr('data-fld-name');
	        $('[data-field-name="' + field_name + '"]').attr('readonly', true);
	        $('[data-field-name="' + field_name + '"]').css('border', 'none');
	    }
	});
	$(element).find(".inlineEdit_" + recordID).filter(filter_not_valid_fields).removeClass("display").siblings(".lblEmbed_" + recordID).addClass("display");
	if($(element).find(".inlineEdit_" + recordID).filter(filter_not_valid_fields).is("[type='radio']")){
		
		$(element).find(".inlineEdit_" + recordID).filter(filter_not_valid_fields).closest('label').removeClass("display").siblings(".lblEmbed_" + recordID).addClass("display");
	}
	if($(element).find('.inlineEdit_' + recordID).filter(filter_not_valid_fields).is('[data-attachment]')){
		$(element).find(".inlineEdit_" + recordID + '[data-attachment]').addClass('display').siblings(".lblEmbed_" + recordID).addClass("display");
		$(element).find(".inlineEdit_" + recordID + '[data-attachment]').filter(filter_not_valid_fields).prev().find('input').removeClass("display");
	}
	if($(element).find('.inlineEdit_' + recordID).filter(filter_not_valid_fields).is('[data-single-attachment]')){
		$(element).find(".inlineEdit_" + recordID + '[data-single-attachment]').addClass('display').siblings(".lblEmbed_" + recordID).addClass("display");
		$(element).find(".inlineEdit_" + recordID + '[data-single-attachment]').filter(filter_not_valid_fields).prev().find('input').removeClass("display");
	}

	if($(element).find(".inlineEdit_" + recordID).filter(filter_not_valid_fields).closest('table').is('.picklist-table')){
		$(element).find(".inlineEdit_" + recordID).filter(filter_not_valid_fields).closest('table.picklist-table').removeClass("display").siblings(".lblEmbed_"+recordID).addClass("display");
	}
	//$(".lblEmbed_" + recordID).filter(filter_not_valid_fields).addClass("display");
	$(element).find("#showEditActions_" + recordID).addClass("display");
	$(element).find("#hideEditActions_" + recordID).removeClass("display");
	// console.log($(element));
	// console.log($(element).parents('table').attr('form-events'), "&&", $(element).parents('table').attr('form-events'), "!=", "", "&&", $(element).parents('table').attr('form-events'), "!=", "[]");
	var embed = $(element).closest('.embed-view-container');
	var cloned = $(element).clone();
	if($(element).parents('table').attr('form-events') && $(element).parents('table').attr('form-events') != "" && $(element).parents('table').attr('form-events') != "[]"){
        if(embed.find('.tr_'+ recordID).attr('is-pending') == "true"){
			if($.type(embed.attr('source-form-type')) != "undefined"){
				if(embed.attr('source-form-type') == "multiple"){
					EmbedFormEvents.init(embed.find('.tr_' + $(element).attr('form-id') + '_' +  recordID), $(element).attr('form-id'), "0", $(element).parents('.embed-view-container'), 'onload', true, 'edit');
				}
				else{
					EmbedFormEvents.init(embed.find('.tr_'+ recordID), $(element).parents('.embed-view-container').attr('embed-source-form-val-id'), "0", $(element).parents('.embed-view-container'), 'onload', true, 'edit');
				}
			}
			else{
				EmbedFormEvents.init(embed.find('.tr_'+ recordID), $(element).parents('.embed-view-container').attr('embed-source-form-val-id'), "0", $(element).parents('.embed-view-container'), 'onload', true, 'edit');
			}
		}
		else{
			if($.type(embed.attr('source-form-type')) != "undefined"){
				if(embed.attr('source-form-type') == "multiple"){
					EmbedFormEvents.init(embed.find('.tr_' + $(element).attr('form-id') + '_' + recordID), $(element).attr('form-id'), recordID, $(element).parents('.embed-view-container'), 'onload', 'edit');
				}
				else{
					EmbedFormEvents.init(embed.find('.tr_'+ recordID), $(element).parents('.embed-view-container').attr('embed-source-form-val-id'), recordID, $(element).parents('.embed-view-container'), 'onload', 'edit');
				}
			}
			else{
				EmbedFormEvents.init(embed.find('.tr_'+ recordID), $(element).parents('.embed-view-container').attr('embed-source-form-val-id'), recordID, $(element).parents('.embed-view-container'), 'onload', 'edit');
			}
		}
	}
	else{
		var data_obj_id = $(element).closest('.setOBJ').attr('data-object-id');
		if($(element).attr('is-pending') == "true"){
			if($.type(embed.attr('source-form-type')) != "undefined"){
				if(embed.attr('source-form-type') == "multiple"){
					EmbedFields.initV2('.tr_' + $(element).attr('form-id') + '_' + recordID, $(element).attr('form-id'), "0", true, data_obj_id, $(element));
				}
				else{
					EmbedFields.initV2('.tr_'+ recordID, $(element).parents('.embed-view-container').attr('embed-source-form-val-id'), "0", true, data_obj_id, $(element));
				}	
			}
			else{
				EmbedFields.initV2('.tr_'+ recordID, $(element).parents('.embed-view-container').attr('embed-source-form-val-id'), "0", true, data_obj_id, $(element));
			}
		}
		else{
			if($.type(embed.attr('source-form-type')) != "undefined"){
				if(embed.attr('source-form-type') == "multiple"){
					console.log("kulambo",embed.find('.tr_' + $(element).attr('form-id') + '_' + recordID));
					EmbedFields.initV2('.tr_' + $(element).attr('form-id') + '_' + recordID, $(element).attr('form-id'), "0", true, embed.closest('.setOBJ').attr('data-object-id'), embed.find('.tr_' + $(element).attr('form-id') + '_' + recordID));	
				}
				else{
					EmbedFields.initV2('.tr_'+ recordID, $(element).parents('.embed-view-container').attr('embed-source-form-val-id'), recordID, true, embed.closest('.setOBJ').attr('data-object-id'), embed.find('.tr_' + recordID));
				}
			}	
			else{
				EmbedFields.initV2('.tr_'+ recordID, $(element).parents('.embed-view-container').attr('embed-source-form-val-id'), recordID, true, embed.find('.tr_'+ recordID));
			}
		}
	}
	// $(".inlineEdit_" + recordID).each(function(){
	//     var fld = $(this).attr('equivalent-fld-title')
	//     if (fld.indexOf("[]") >= 0){
	// 	$("label[equivalent-fld-title='" + fld + "']").removeClass("display");
	// 	$("input[equivalent-fld-title='" + fld + "']").addClass("display");
		
	//     }
	// })

	$(element).find('.viewTDEmbedded').addClass('display');
	$(element).find('#deleteTDEmbedded').addClass('display');
	// Hide Copy
	// $(element).find('.deleteEmbedded').addClass("display");
	$(element).find(".showpasteActions_" + recordID).addClass("display");
	//$(".hidepasteActions_" + recordID).removeClass("display");
	
	//console.log(recordID)
	
	$(".dataTip").tooltip();
	var disabledDays = [];
	$("input[equivalent-fld-type='date']").datepicker({ dateFormat: 'yy-mm-dd' });
	$("input[equivalent-fld-type='dateTime']").datetimepicker({
	    changeMonth: true,
	    dateFormat: 'yy-mm-dd',
	    beforeShowDay: function(date) {
	        //console.log(this);
	        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
	        for (i = 0; i < disabledDays.length; i++) {
	            if ($.inArray((m + 1) + '-' + d + '-' + y, disabledDays) != -1) {
	                return [false, 'red', 'This is Holiday'];
	            }
	        }
	        return [true];
	    }
	});;

	$("input[equivalent-fld-type='time']").datetimepicker({
        changeMonth: true,
        dateFormat: '',
        timeFormat: 'HH:mm',
        timeOnly: true
    });
    EmbeddedFixHeader.reFixHeader($(element).parents('.embed-view-container'));
	// $(element).parents('.embed-view-container').animate({
	//     scrollTop: $(element).offset().top
	// }, 1000);
	ui.unblockPortion($(element).parents('.embed-view-container'));
}
function getEmbedData(element){
	/*"/ajax/embedded_view_functions"*/
	var formID = element.closest('[embed-source-form-val-id]').attr("embed-source-form-val-id");



	var action = "embed_searchRecord";
	var data = {action:action,formID:formID}
    $.post("/ajax/embedded_view_functions",data,function(e){
		var json_data = JSON.parse(e);
		var required_fields = json_data['fieldRequired'];
		var enabled_fields = json_data['fieldEnabled'];
		var workflow_id = json_data['workflowID'];
		console.log("json_data",json_data)
		element.attr("data-required-field",required_fields);
		element.attr("data-enabled-field",enabled_fields);
		element.attr("data-workflow-id",workflow_id);
		checkEmbedAttr(element);
    });

}
function submitEmbedData(action_embed){
	
	var tr_element = $(this).parents('tr:not(.picklist-tr)').eq(0);
	var trackNo = $(this).closest('[record-trackno]').attr("record-trackno");
	var recordID = $(this).closest('[record-id]').attr("record-id");
	var formID = $(this).closest('[embed-source-form-val-id]').attr("embed-source-form-val-id");
	var required_fields_attr = tr_element.attr('data-required-field');
	var enabled_fields_attr = tr_element.attr('data-enabled-field');
	var workflow_id = tr_element.attr('data-workflow-id');
	var embed_view_cont = $(this).closest('.embed-view-container');
	var embed_view_cont_data = embed_view_cont.attr("stringified-json-data-send");

	var action = action_embed;
	var resultFld = $(this).closest('[embed-result-field-val]').attr("embed-result-field-val");
	 
			// Get Fields
	var error_ctr = 0;				
	var flds = {};
	if(typeof tr_element.closest('.embed-view-container').attr('source-form-type') != "undefined" && tr_element.closest('.embed-view-container').attr('source-form-type') == "multiple"){
		formID = tr_element.attr('form-id');
	}
	tr_element.find(".inlineEdit_" + recordID).each(function(){
		//check if required is empty
		var value_selected = $(this).val() /*|| $(this).val().join(",")*/;
		if($(this).is('[type="radio"]:last')){

			if(!$(this).closest('td').find('[type="radio"]:checked').exists()){

				value_selected = "";
			}
		}
		
		if($(this).is('.fieldRequiredClass:visible') && $.trim(value_selected)=="" && !$(this).is('.display')){

			error_ctr++;
			NotifyMe($(this), "Required!");
		}

		if($(this).is('[type="checkbox"]')){
			var collected_value = [];
			$(this).parents('td').find($(".inlineEdit_" + recordID+":checked")).each(function(){
				collected_value.push($(this).val());
			});
			value_selected = collected_value.join(",");
		}

		if($(this).is('[equivalent-fld-input-type="Currency"]')){
			value_selected = value_selected.replace(/,/g,"");
		}
		
		var name = $(this).attr("equivalent-header-title");
	    var trimmer = $.trim(value_selected);	
		if($(this).is('[type="radio"]:not(:checked)')){
			return;
		}
		flds[name] = trimmer;

	});
	console.log("aksyun",action)
	if(action === "embed_newRecord"){
		if(embed_view_cont_data && embed_view_cont_data!=""){
			var parent_form = $(".fl-body-content-wrapper.preview_content").eq(0)
			var parsed_json_data_send = JSON.parse(embed_view_cont_data);
			for (var index_json_data_send in parsed_json_data_send) {
				if (parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == "true" || parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == true) {
					var data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '"]');

					if (data_send_embed_ele_parent.length == 0) {
					    data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '[]"]'); /// for multiples
					}
					var data_send_embed_ele_parent_value = data_send_embed_ele_parent.val();
					
				}
				flds[parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"]] = data_send_embed_ele_parent_value;
			}
		}
	}
	
	
	
	
	var data = {action:action,trackNo:trackNo,recordID:recordID,formID:formID,flds:flds,fieldRequired:required_fields_attr,fieldEnabled:enabled_fields_attr,Workflow_ID:workflow_id,form_parent_id:$('[name="FormID"]').val()}
	var counted_error = (function(tr_ele){
	    
	    if (tr_ele.is('[data-required-field]')) {
			if (tr_ele.attr('data-required-field') != '') {
			    var get_mandatory_fields = tr_ele.attr('data-required-field');
			    try{
					get_mandatory_fields = JSON.parse(get_mandatory_fields);
			    }catch(errorski){
				
			    }
			    if ($.type(get_mandatory_fields)=="array") {

					var temp_field_ele = null;
					var temp_field_ele_val = null;
					for(var ii in get_mandatory_fields){//equivalent-fld-title="string_text"
					    temp_field_ele = tr_ele.find('[data-field-name="'+get_mandatory_fields[ii]+'"]');
						temp_field_ele_val = temp_field_ele.val();
						if(temp_field_ele.is('[type="radio"]') || temp_field_ele.is('[type="checkbox"]')){
							temp_field_ele_checked= tr_ele.find('[data-field-name="'+get_mandatory_fields[ii]+'"]:checked');
							if(temp_field_ele_checked.exists()){
								temp_field_ele_val = temp_field_ele_checked.val();
							}else{
								temp_field_ele_val = null;
							}
						}
						console.log("field is required",get_mandatory_fields[ii],temp_field_ele_val,$.trim(temp_field_ele_val) == '' || temp_field_ele_val == null)
						if(temp_field_ele.filter(":visible").length >= 1 ){

							if ($.trim(temp_field_ele_val) == '' || temp_field_ele_val == null) {

								error_ctr++;
								NotifyMe(temp_field_ele, "Required!");
						    }
						}
					}
			    }

			}
	    }
	    return error_ctr;
	})(tr_element);
	
	if (counted_error == 0) {
	    // Confirmation
	    var message = "Are you sure you want to add this record?";
	    
	    if(action_embed == "embed_saveRecord" || action_embed == "embed_saveRecordWithWorkflow"){
	    	message = "Are you sure you want to update this record?";
	    }
	    var newConfirm = new jConfirm(message, 'Confirmation Dialog', '', '', '', function(r) {
		if (r == true) {
		    //console.log("Trank No: " + trackNo + "\n")
		    //console.log("Record ID: " + recordID + "\n")
		    //console.log("Form ID: " + formID + "\n")
		    $(this).parents('tr').off();
		    if($.type($(this).parents('tr').data('field_models')) != "undefined"){
		    	data.fieldEnabled = $(this).parents('tr').data('field_models')['fieldEnabled'];
		    }
		    ui.blockPortion(tr_element.parents('.embed-view-container'));
		    $.post("/ajax/embedded_view_functions",data,function(e){
				console.log("data", data);
				// return;
				
				var json_data = JSON.parse(e);
				
				if (json_data['notification'] == "Successfull") {
				    showNotification({
					message: json_data['message'],
					type: "success",
					autoClose: true,
					duration: 3
				    });
				    
				    // Change value of lbl by the value of textbox
				    tr_element.find(".inlineEdit_" + recordID).each(function(){
						var flds_val = $(this).val();
						var self_header = $(this).attr("data-field-name");
						//console.log(flds_val)
						//console.log("sam");
						//console.log("." + self_header + "_" + recordID);
						// console.log("labeling", $(this).parents('tr'));
						console.log("thisisit", $(this).attr('data-field-name'), flds_val, $(this)[0]);
						$(this).closest('td:not(.picklist-td)').eq(0).find('label').html(flds_val);
						// $("label[data-header-title='" + self_header + "_" + recordID + "']").html(flds_val);
				    })
						    
				    $(".inlineEdit_" + recordID).addClass("display");
				    $(".lblEmbed_" + recordID).removeClass("display");
				    
				    $("#showEditActions_" + recordID).removeClass("display");
				    $("#hideEditActions_" + recordID).addClass("display");
				    $(".inlineEdit_" + recordID).addClass("display").siblings(".lblEmbed_" + recordID).removeClass("display");
				    if($(".inlineEdit_" + recordID).parent().is("label")){
				    	//console.log("radio",$(".inlineEdit_" + recordID).closest('label').last().siblings(".lblEmbed_" + recordID))
				    	$(".inlineEdit_" + recordID).closest('label').addClass("display").siblings(".lblEmbed_" + recordID).removeClass("display");
				    }
				    //$(".lblEmbed_" + recordID).filter(filter_not_valid_fields).addClass("display");
				    $("#showEditActions_" + recordID).removeClass("display");
				    $("#hideEditActions_" + recordID).addClass("display");

				    // embeded_view.refreshEmbed($(this).parents('.embed-view-container'));
				    $("[name='" + resultFld + "']").trigger("change");
				    if(embed_inline_add==1){
				    	$(document).contextmenu("enableEntry", "copy", true);
				    }
				    ui.blockPortion(tr_element.parents('.embed-view-container'));
				    if(typeof tr_element.parents('.embed-view-container').attr('source-form-type') != "undefined" && tr_element.parents('.embed-view-container').attr('source-form-type') == "multiple"){
				    	initializeMultipleEmbedddedView(tr_element.parents('.embed-view-container'), 'onerefresh');
				    }
				    else{
					    embeded_view.refreshEmbed(tr_element.parents('.embed-view-container'));			    	
				    }

				}
		    });
		    
		}
	    });
	    
	    newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right" style="color:#000; font-size:15px;"></i>'});   
	    
	    
	}
	   
}
function prepareForValidation(data_type){
	var data_checker = [];
	var self = $(this);
	var required_fields_attr = [];
	required_fields_attr = required_fields_attr.concat((JSON.parse(self.closest('tr[data-required-field]').attr('data-required-field'))||""));
	data_checker = data_checker.concat(data_type);
	
	var field_name = $(this).attr('data-field-name');
	//console.log("data_type",field_name,$.inArray(field_name,required_fields_attr),self.closest('tr[data-required-field]').attr('data-required-field'),required_fields_attr);
	if($.inArray("Number",data_checker) >=0 ){
		self.addClass("prop_numeric_val");
	}
	if($.inArray("Currency",data_checker) >=0 ){
		self.addClass("deciNumberClass");
	}
	if($.inArray(field_name,required_fields_attr)  >=0){
		self.addClass("fieldRequiredClass");
	}
	

}
function validateEmbedFields(){
	// deciNumFunction('.deciNumberClass');
	// numericFunction('.prop_numeric_val');

}
var addedRequest = [];
var addedRequestID = [];
/*Search embed EVENTS*/
$(document).ready(function(){

	/*ADD KEYBOARD SHORTCUT*/
	embedded_view_fs.keyboard_shortcuts();
	/*ADD RECORD IN EMBEDDED VIEW*/
	embedded_view_fs.save_record_in_embedded("#addEmbedded");

/* DELETE RECORD IN EMBEDDED VIEW  */
/* ====================================================== */
    embedded_view_fs.delete_record_in_embedded("#deleteTDEmbedded");
/* DELETE RECORD IN EMBEDDED VIEW  */
/* ====================================================== */  
  	
/* EDIT / UPDATE RECORD IN EMBEDDED VIEW  */
/* ====================================================== */    
    embedded_view_fs.edit_record_in_embedded("#editDEmbedded");
 
/* COPY RECORD IN EMBEDDED VIEW  */
/* ====================================================== */    
    embedded_view_fs.copy_record_in_embedded("#copyTDEmbedded");
    
/* PASTE RECORD IN EMBEDDED VIEW  */
/* ====================================================== */    
    embedded_view_fs.paste_record_in_embedded("#pasteTDEmbedded");   
   
});

/* EMBEDDED VIEW FUNCTIONS  */
/* ====================================================== */

    embedded_view_fs = {
    /*SAVE RECORD IN EMBEDDED VIEW*/

    keyboard_shortcuts : function(){
    	$(document).on({
    		"keydown":function(e){

    			if($('.activeActionClass').length == 1){
    				if(e.keyCode == 83 && e.ctrlKey){
    					e.preventDefault();
    					$('.activeActionClass').find('#addEmbedded, #okeditDEmbedded').not(':hidden').trigger('click');
    				}
    				if(e.keyCode == 27){
    					e.preventDefault();
    					if($('.activeActionClass').is('#copyTDEmbedded')){
    						$('.activeActionClass').closest('div').find('.cancelpasteDEmbedded').trigger('click');
    					}
    					$('.activeActionClass').find('#canceladdEmbedded, #canceleditDEmbedded').not(':hidden').trigger('click');

    				}
    			}

    			
    		}
    	});
    },
    save_record_in_embedded : function(elements){

    	$("body").on("click",elements,function(){
    		var embed = $(this).parents('.embed-view-container');
    		embed.parent().find('.custom_action_cancel_edit').trigger('click');
    		submitEmbedData.call(this,"embed_newRecord", $(this).parents('tr'));
    		 if(embed_inline_add==1){
			    $(document).contextmenu("enableEntry", "add", true);
			 }
    		
    	});
    	$('body').on("click","#canceladdEmbedded",function(){
    		//added by japhet morada
    		//02-18-2016
    		//fix the parent submission editing inline
    		if($(this).parents('.embed-view-container').attr('commit-data-row-event') == "parent"){
    			var embed = $(this).parents('.embed-view-container');
    			var form_id = embed.attr('embed-source-form-val-id');
    			var ref_id = $(this).parents('tr').attr('reference-id');
    			$('body').data('embed_row_data')[form_id][ref_id]['saved'] = true;
    			embed.parent().find('.custom_action_cancel_edit').trigger('click');
    		}
			cancel_embed_action.call(this,"#canceladdEmbedded");
    		

    	});

    },

	/* DELETE RECORD IN EMBEDDED VIEW  */
	/* ====================================================== */
	delete_record_in_embedded : function(elements){
	    $("body").on("click",elements,function(){
	    var self = $(this);
		var trackNo = $(this).parent().parent().parent().attr("record-trackno");
		var recordID = $(this).parent().parent().parent().attr("record-id");
		var formID = $(this).parent().parent().parent().parent().parent().parent().attr("embed-source-form-val-id");
		var action = "embed_deleteRecord";
		var data = {action:action,trackNo:trackNo,recordID:recordID,formID:formID}
		var resultFld = $(this).parent().parent().parent().parent().parent().parent().attr("embed-result-field-val");
		var message = "Are you sure you want to delete this record?";
		// if(){

		// }
		if(self.closest('tr').is('[is-locked]')){
        	return;
        }
		var newConfirm = new jConfirm(message, 'Confirmation Dialog', '', '', '', function(r) {
		    if (r == true) {
				//console.log("Trank No: " + trackNo + "\n")
				//console.log("Record ID: " + recordID + "\n")
				//console.log("Form ID: " + formID + "\n")s
				if(self.closest('.embed-view-container').attr('commit-data-row-event') != 'parent'){
					var data = {action:action,trackNo:trackNo,recordID:recordID,formID:formID}
					$.post("/ajax/embedded_view_functions",data,function(e){
					    var json_data = JSON.parse(e);
					    
					    if (json_data['notification'] == "Successfull") {
						showNotification({
						    message: json_data['message'],
						    type: "success",
						    autoClose: true,
						    duration: 3
						});
							
						// Remove Row
						$('tr[record-trackno="' + trackNo + '"]').remove();
						
						$("[name='" + resultFld + "']").trigger("change");
					    }
					});
				}
				else{
					var tr_reference = self.parents('tr').attr('reference-id');
					var record_id = self.parents('tr').attr('record-id');
					var obj_id = self.closest('.setOBJ').attr('data-object-id');
					var data = $('body').data('embed_row_data');
					var embed = self.parents('.embed-view-container');
					var form_id = embed.attr('embed-source-form-val-id');
					if($.type(data) == 'undefined'){
						if($.type($('body').data('embed_deleted_row')) != "undefined"){
							var deleted_row = $('body').data('embed_deleted_row');
							if($.type(deleted_row[obj_id]) != "undefined"){
								deleted_row[obj_id]['id_list'].push(record_id);
							}
							else{
								deleted_row[obj_id]['form_id'] = form_id;
								deleted_row[obj_id]['id_list'] = [];
								deleted_row[obj_id]['id_list'].push(record_id);
							}
						}
						else{
							var deleted_row = {};
							deleted_row[obj_id] = {};
							deleted_row[obj_id]['form_id'] = form_id;
							deleted_row[obj_id]['id_list'] = [];
							deleted_row[obj_id]['id_list'].push(record_id);
							$('body').data('embed_deleted_row', deleted_row);
						}
					}
					else{
						data[obj_id][tr_reference] = undefined;
						self.parents('tr').remove();
					}
					embeded_view.getSetTotalOfColumn.call(embed, embed.children());
					embeded_view.fixNumbering(embed);
					if($('.okInsertDEmbedded:visible, .okeditDEmbedded:visible').length <= 0){
						$('.embed-edit-mode').removeClass('embed-edit-mode');
					}
					$("[name='" + resultFld + "']").trigger("change");
				}
			
		    }
		});
		
		newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>'});   
		
	    });
	},
	/* DELETE RECORD IN EMBEDDED VIEW  */
	/* ====================================================== */
	delete_multiple_records : function(){
		
			var formID = $(this).parent().parent().parent().parent().parent().parent().attr("embed-source-form-val-id");
			var trackNos = addedRequest;
			if(trackNos!=""){
				trackNos = JSON.stringify(trackNos);
				var action = "embed_multiple_delete";
				
				var recordIDs = [];
				var data = {action:action,trackNos:trackNos};
				console.log('trackNos',data)
				$.post("/ajax/embedded_view_functions",data,function(datas){
					console.log("close Delete",datas);
				});
			}
			
		
	},
	
	/* EDIT / UPDATE RECORD IN EMBEDDED VIEW  */
	/* ====================================================== */
	edit_record_in_embedded : function(elements){

	    $("body").on("click",elements,function(){
	    	var tr_ele = $(this).closest('tr');
	    	// if($.type(tr_ele.closest('.embed-view-container').attr('source-form-type')) != "undefined"){
	    	// 	var embedded_view = tr_ele.closest('.embed-view-container');
		    // 	if(tr_ele.closest('.embed-view-container').attr('source-form-type') == "multiple"){

		    // 		var selected_rows = embedded_view.data('selected_rows')||[];
		    // 		// console.log("selected_rows", selected_rows, embedded_view);
		    // 		if(selected_rows.length > 0){
			   //  		for(i in selected_rows){
			   //  			var row = $(selected_rows[i]);
			   //  			if(row.attr('is-editable') == "1" && !tr_ele.is('[is-locked]')){
				  //   			var row_html = row.html();
				  //   			row.data('clone_element', row_html);
				  //   			if($.type(row.attr('reference-id')) == 'undefined'){
						//     		var form_id = row.parents('.embed-view-container').attr('embed-source-form-val-id');
						// 	    	var tr_reference = row.attr('reference-id')||form_id + "_" + row.attr('record-id');
						// 	    	row.attr('reference-id', tr_reference);
						// 	    	embeded_view.createTemporaryData(row.parents('.embed-view-container'), 'update', 'in-line', tr_reference, true);
						// 	    }
					 //    		checkEmbedAttr(row);
					 //    	}
			   //  		}
			   //  	}
			   //  	else{
			   //  		if(tr_ele.attr('is-editable') == "0" || tr_ele.is('[is-locked]')){
			   //  			return false;
			   //  		}
    		// 	    	if($.type(tr_ele.attr('reference-id')) == 'undefined'){
    		// 	    		var form_id = $(this).parents('.embed-view-container').attr('embed-source-form-val-id');
    		// 		    	var tr_reference = $(this).parents('tr').attr('reference-id')||form_id + "_" + $(this).parents('tr').attr('record-id');
    		// 		    	tr_ele.attr('reference-id', tr_reference);
    		// 		    	embeded_view.createTemporaryData(tr_ele.parents('.embed-view-container'), 'update', 'in-line', tr_reference, true);
    		// 		    }
    		//     		checkEmbedAttr(tr_ele);
			   //  	}
		    // 	}
		    // 	else{
		    // 		if(tr_ele.attr('is-editable') == "0" || tr_ele.is('[is-locked]')){
		    // 			return false;
		    // 		}
			   //  	if($.type(tr_ele.attr('reference-id')) == 'undefined'){
			   //  		var form_id = $(this).parents('.embed-view-container').attr('embed-source-form-val-id');
				  //   	var tr_reference = $(this).parents('tr').attr('reference-id')||form_id + "_" + $(this).parents('tr').attr('record-id');
				  //   	tr_ele.attr('reference-id', tr_reference);
				  //   	embeded_view.createTemporaryData(tr_ele.parents('.embed-view-container'), 'update', 'in-line', tr_reference, true);
				  //   }
		    // 		checkEmbedAttr(tr_ele);
		    // 	}
		    // }
		    // else{
		    console.log("tr_ele", tr_ele);
	    	if(tr_ele.attr('is-editable') == "0" || tr_ele.is('[is-locked]')){
	    		return false;
	    	}
	    	if($.type(tr_ele.attr('reference-id')) == 'undefined'){
	    		var form_id = "";
	    		if(typeof $(this).parents('.embed-view-container').attr('source-form-type') != "undefined" && $(this).parents('.embed-view-container').attr('source-form-type') == "multiple"){
	    			form_id = tr_ele.attr('form-id');
	    		}
	    		else{
	    			form_id = $(this).parents('.embed-view-container').attr('embed-source-form-val-id');
	    		}
		    	var tr_reference = $(this).parents('tr').attr('reference-id')||form_id + "_" + $(this).parents('tr').attr('record-id');
		    	tr_ele.attr('reference-id', tr_reference);
		    	embeded_view.createTemporaryData(tr_ele.parents('.embed-view-container'), 'update', 'in-line', tr_reference, true);
		    }
    		checkEmbedAttr(tr_ele);
		    // }
	    	// $('.disable-clickable').removeClass('disable-clickable');
	    	//==========================================
	    	if($(this).is('.disable-clickable')){
	    		return false;
	    	}
	    });
	    // Cancel Edit
	    $("body").on("click","#canceleditDEmbedded",function(){
	    	//added by japhet morada
	    	//02-18-2016
	    	//fix the parent submission editing inline
	    	var embed = $(this).parents('.embed-view-container');
	    	var form_id = embed.attr('embed-source-form-val-id');
	    	var ref_id = $(this).parents('tr').attr('reference-id');
	    	if($(this).parents('.embed-view-container').attr('commit-data-row-event') == "parent"){
	    		if($(this).parents('tr').data('old_field_models')){
	    			$('body').data('embed_row_data')[form_id][ref_id]['field_models'] = $(this).parents('tr').data('old_field_models');
	    			$('body').data('embed_row_data')[form_id][ref_id]['fields_data'] = $('body').data('embed_row_data')[form_id][ref_id]['old_fields_data'];
	    			$('body').data('embed_row_data')[form_id][ref_id]['fields_data']['saved']= true;
	    		}
	    		else{
	    			delete $('body').data('embed_row_data')[form_id][ref_id];
	    		}
	    		// $('body').data('embed_row_data')[form_id][ref_id]['saved'] = true;
	    		// $('body').data('embed_row_data')[form_id][ref_id] = undefined;
	    	}
    		
	    	cancel_embed_action.call(this,"#canceleditDEmbedded");
	    });


	    
	    // Save Edit
	    $("body").on("click","#okeditDEmbedded",function(){
		   	var tr_ele = $(this).closest('tr');
		   	var embed = $(this).closest('.embed-view-container');
		   	if(tr_ele.parents('.embed-view-container').attr('commit-data-row-event') == "parent" && $.type(tr_ele.parents('.embed-view-container').attr('commit-data-row-event')) != "undefined"){
		   	    // for in-line creation
		   	    if($.type(tr_ele.parents('.embed-view-container').attr('source-form-type')) != "undefined"){
		   	    	if(tr_ele.parents('.embed-view-container').attr('source-form-type') == "multiple"){
		   	    		var embedded_view = tr_ele.parents('.embed-view-container');
		   	    		if(embedded_view.find('[name="record_selector"]:checked').length <= 0){
		   	    			var rows = embedded_view.find('[name="record_selector"]:checked');
		   	    			rows.each(function(index){
   	    				   	    EmbedFields.error_counter = 0;
   	    				   	    // EmbedFields.fieldValueFixes(undefined,tr_ele);
   	    				   	    ui.blockPortion(tr_ele.parents('.embed-view-container'));
   	    				   	    if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
   	    				   	    	EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('form-id') + '_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'),tr_ele.attr('form-id'));
   	    				   	   	}
   	    				   	   	else{
   	    				   	   		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'));
   	    				   	   	}
   	    				   	    
   	    				   	    if(EmbedFields.error_counter <= 0){
   	    					   	    var tr_reference = $(this).parents('tr').attr('reference-id');
   	    					   	    var form_id = $(this).parents('.embed-view-container').attr('embed-source-form-val-id');
   	    					   	    var data = $('body').data('embed_row_data');
   	    					   	    $(this).parents('tr').attr('current', '');
   	    					   	    console.log($(this).attr('current'));
   	    					   	    //dito
   	    					   	    delete data[form_id][tr_reference]['old_fields_data'];
   	    					   	    var stringify = JSON.stringify(data[form_id][tr_reference]);
   	    					   	    tr_ele.removeData('old_field_models');
   	    					   	    data[form_id][tr_reference] = JSON.parse(stringify);
   	    					   	    //=====
   	    					   	    embeded_view.createTemporaryData(tr_ele.parents('.embed-view-container'), "update", "in-line", tr_reference);
   	    					   	    $('.disable-clickable').removeClass('disable-clickable');
   	    					   	    $('.showpasteActions_' + tr_ele.attr('record-id')).removeClass('display');
   	    					   	    tr_ele.find('.viewTDEmbedded').removeClass('display');
   	    					   	    data[form_id][tr_reference]['saved'] = true;
   	    					   	    data[form_id][tr_reference]['submitted'] = false;
   	    					   	    $(this).parents('tr').removeAttr('current');
   	    					   	    console.log($(this).attr('current'));
   	    					   	}
		   	    			})
		   	    		}
		   	    		else{	
   	    			   	    EmbedFields.error_counter = 0;
   	    			   	    // EmbedFields.fieldValueFixes(undefined,tr_ele);
   	    			   	    ui.blockPortion(tr_ele.parents('.embed-view-container'));
   	    			   	   	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
    			   	     		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('form-id') + '_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'), tr_ele.attr('form-id'));
    			   	    	}
    			   	    	else{
    			   	    		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'));
    			   	    	}
   	    			   	    if(EmbedFields.error_counter <= 0){
   	    				   	    var tr_reference = $(this).parents('tr').attr('reference-id');
   	    				   	    var form_id = $(this).parents('.embed-view-container').attr('embed-source-form-val-id');
   	    				   	    if(typeof $(this).parents('.embed-view-container').attr('source-form-type') != "undefined" && $(this).parents('.embed-view-container').attr('source-form-type') == "multiple"){
   	    				   	    	form_id = $(this).parents('tr').attr('form-id');
   	    				   	    }
   	    				   	    var data = $('body').data('embed_row_data');
   	    				   	    $(this).parents('tr').attr('current', '');
   	    				   	    console.log($(this).attr('current'));
   	    				   	    //dito
   	    				   	    delete data[form_id][tr_reference]['old_fields_data'];
   	    				   	    var stringify = JSON.stringify(data[form_id][tr_reference]);
   	    				   	    tr_ele.removeData('old_field_models');
   	    				   	    data[form_id][tr_reference] = JSON.parse(stringify);
   	    				   	    //=====
   	    				   	    embeded_view.createTemporaryData(tr_ele.parents('.embed-view-container'), "update", "in-line", tr_reference);
   	    				   	    $('.disable-clickable').removeClass('disable-clickable');
   	    				   	    $('.showpasteActions_' + tr_ele.attr('record-id')).removeClass('display');
   	    				   	    tr_ele.find('.viewTDEmbedded').removeClass('display');
   	    				   	    data[form_id][tr_reference]['saved'] = true;
   	    				   	    data[form_id][tr_reference]['submitted'] = false;
   	    				   	    $(this).parents('tr').removeAttr('current');
   	    				   	    console.log($(this).attr('current'));
   	    				   	}
   	    				   	ui.unblockPortion(tr_ele.parents('.embed-view-container'));	
		   	    		}
		   	    	}
		   	    	else{
	    		   	    EmbedFields.error_counter = 0;
	    		   	    // EmbedFields.fieldValueFixes(undefined,tr_ele);
	    		   	    ui.blockPortion(tr_ele.parents('.embed-view-container'));
	    			   	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
 			   	     		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('form-id') + '_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'), tr_ele.attr('form-id'));
 			   	    	}
 			   	    	else{
 			   	    		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'));
 			   	    	}
	    		   	    if(EmbedFields.error_counter <= 0){
	    			   	    var tr_reference = $(this).parents('tr').attr('reference-id');
	    			   	    var form_id = $(this).parents('.embed-view-container').attr('embed-source-form-val-id');
	    			   	    var data = $('body').data('embed_row_data');
	    			   	    $(this).parents('tr').attr('current', '');
	    			   	    console.log($(this).attr('current'));
	    			   	    //dito
	    			   	    delete data[form_id][tr_reference]['old_fields_data'];
	    			   	    var stringify = JSON.stringify(data[form_id][tr_reference]);
	    			   	    tr_ele.removeData('old_field_models');
	    			   	    data[form_id][tr_reference] = JSON.parse(stringify);
	    			   	    //=====
	    			   	    embeded_view.createTemporaryData(tr_ele.parents('.embed-view-container'), "update", "in-line", tr_reference);
	    			   	    $('.disable-clickable').removeClass('disable-clickable');
	    			   	    $('.showpasteActions_' + tr_ele.attr('record-id')).removeClass('display');
	    			   	    tr_ele.find('.viewTDEmbedded').removeClass('display');
	    			   	    data[form_id][tr_reference]['saved'] = true;
	    			   	    data[form_id][tr_reference]['submitted'] = false;
	    			   	    $(this).parents('tr').removeAttr('current');
	    			   	    console.log($(this).attr('current'));
	    			   	}
	    			   	ui.unblockPortion(tr_ele.parents('.embed-view-container'));
		   	    	}
		   	    }
		   	    else{
			   	    EmbedFields.error_counter = 0;
			   	    // EmbedFields.fieldValueFixes(undefined,tr_ele);
			   	    ui.blockPortion(tr_ele.parents('.embed-view-container'));
    			   	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
		   	     		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('form-id') + '_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'), tr_ele.attr('form-id'));
		   	    	}
		   	    	else{
		   	    		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'));
		   	    	}
			   	    if(EmbedFields.error_counter <= 0){
				   	    var tr_reference = $(this).parents('tr').attr('reference-id');
				   	    var form_id = $(this).parents('.embed-view-container').attr('embed-source-form-val-id');
				   	    var data = $('body').data('embed_row_data');
				   	    $(this).parents('tr').attr('current', '');
				   	    console.log($(this).attr('current'));
				   	    //dito
				   	    delete data[form_id][tr_reference]['old_fields_data'];
				   	    var stringify = JSON.stringify(data[form_id][tr_reference]);
				   	    tr_ele.removeData('old_field_models');
				   	    data[form_id][tr_reference] = JSON.parse(stringify);
				   	    //=====
				   	    embeded_view.createTemporaryData(tr_ele.parents('.embed-view-container'), "update", "in-line", tr_reference);
				   	    $('.disable-clickable').removeClass('disable-clickable');
				   	    $('.showpasteActions_' + tr_ele.attr('record-id')).removeClass('display');
				   	    tr_ele.find('.viewTDEmbedded').removeClass('display');
				   	    data[form_id][tr_reference]['saved'] = true;
				   	    data[form_id][tr_reference]['submitted'] = false;
				   	    $(this).parents('tr').removeAttr('current');
				   	    console.log($(this).attr('current'));
				   	}
				   	ui.unblockPortion(tr_ele.parents('.embed-view-container'));
				}
		   	}
		   	else{
		   		//old function
		   		EmbedFields.error_counter = 0;
		   		// EmbedFields.fieldValueFixes(undefined,tr_ele);
			   	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
	   	     		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('form-id') + '_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'), tr_ele.attr('form-id'));
     		   		if(EmbedFields.error_counter <= 0){
     			   	    submitEmbedData.call(this,"embed_saveRecordWithWorkflow");
     			   	    delete $('body').data('embed_row_data')[tr_ele.attr('form-id')][$(tr_ele).attr('reference-id')];
     			   	    $('.disable-clickable').removeClass('disable-clickable');
     			   	}
	   	    	}
	   	    	else{
	   	    		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_ele.attr('record-id')), tr_ele.attr('record-id'), tr_ele.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'));
    		   		if(EmbedFields.error_counter <= 0){
    			   	    submitEmbedData.call(this,"embed_saveRecordWithWorkflow");
    			   	    delete $('body').data('embed_row_data')[$(tr_ele).parents('.embed-view-container').eq(0).attr('embed-source-form-val-id')][$(tr_ele).attr('reference-id')];
    			   	    $('.disable-clickable').removeClass('disable-clickable');
    			   	}
	   	    	}
		   	}
		   	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
		   		embed.children('.custom_action_cancel_edit').trigger('click');
		   	}
	    });
	},
	
	/* COPY RECORD IN EMBEDDED VIEW  */
	/* ====================================================== */
	copy_record_in_embedded : function(elements){
	    $("body").on("click",elements,function(){
	    	$(this).addClass('activeActionClass');
			var parentTR = $(this).parent().parent().parent().parent();
			var recordID = $(this).parent().parent().parent().parent().attr("record-id");
			var collect_setCopy = $(".setCopy").length;
			if(parentTR.closest('tr').is('[is-locked]')){
	        	return;
	        }
			if(typeof parentTR.closest('.embed-view-container').attr('source-form-type') != "undefined"){

				if(parentTR.closest('.embed-view-container').attr('source-form-type') == "multiple"){
					var selected_rows = parentTR.closest('.embed-view-container').data('selected_rows')||[];
					if(collect_setCopy > 0){
						for(var i in selected_rows){
							var row = $(selected_rows[i]);
							var old_recordID = row.attr("record-id");
							row.css("background-color","");
							row.removeClass("setCopy");
							//$("#pasteTDEmbedded").removeClass("display");
							row.find(".showpasteActions_" + old_recordID).removeClass("display");
							row.find(".hidepasteActions_" + old_recordID).addClass("display");
							row.css("background-color","rgb(223, 220, 220)");
							row.addClass("setCopy");
						}
					}
					else{
						for(var i in selected_rows){
							var row = $(selected_rows[i]);
							var old_recordID = row.attr("record-id");
							console.log("row", row, old_recordID, row.find(".showpasteActions_" + old_recordID));
							row.css("background-color","rgb(223, 220, 220)");
							row.find(".showpasteActions_" + old_recordID).addClass("display");
							row.find(".hidepasteActions_" + old_recordID).removeClass("display");
							row.addClass("setCopy");
						}
					}	
				}
				else{
					if (collect_setCopy == "1") {
					    var old_recordID = $(".setCopy").attr("record-id");
					    $(".setCopy").css("background-color","");
					    $(".setCopy").removeClass("setCopy");
					    //$("#pasteTDEmbedded").removeClass("display");
					    $(".showpasteActions_" + old_recordID).removeClass("display");
					    $(".hidepasteActions_" + old_recordID).addClass("display");
					    parentTR.css("background-color","rgb(223, 220, 220)");
					    parentTR.addClass("setCopy");
					}else{
					    parentTR.css("background-color","rgb(223, 220, 220)");
					    parentTR.addClass("setCopy");
					}
					//$("#pasteTDEmbedded").removeClass("display");
					
					$(".showpasteActions_" + recordID).addClass("display");
					$(".hidepasteActions_" + recordID).removeClass("display");
				}
			}
			else{
				if (collect_setCopy == "1") {
				    var old_recordID = $(".setCopy").attr("record-id");
				    $(".setCopy").css("background-color","");
				    $(".setCopy").removeClass("setCopy");
				    //$("#pasteTDEmbedded").removeClass("display");
				    $(".showpasteActions_" + old_recordID).removeClass("display");
				    $(".hidepasteActions_" + old_recordID).addClass("display");
				    parentTR.css("background-color","rgb(223, 220, 220)");
				    parentTR.addClass("setCopy");
				}else{
				    parentTR.css("background-color","rgb(223, 220, 220)");
				    parentTR.addClass("setCopy");
				}
				//$("#pasteTDEmbedded").removeClass("display");
				
				$(".showpasteActions_" + recordID).addClass("display");
				$(".hidepasteActions_" + recordID).removeClass("display");
			}
	    });
	},
	
	/* PASTE RECORD IN EMBEDDED VIEW  */
	/* ====================================================== */
	paste_record_in_embedded : function(elements){
	    $("body").on("click",elements,function(){
		var setCopy = $(".setCopy");
		var trackNo = setCopy.attr("record-trackno");
		var recordID = setCopy.attr("record-id");
		var formID = setCopy.parent().parent().parent().attr("embed-source-form-val-id");
		var resultFld = setCopy.parent().parent().parent().attr("embed-result-field-val");
		var collect_setCopy = setCopy.length;
		var action = "embed_copyRecord";
		var self_parent = $(this).parents('tr');
		var object_id = setCopy.parent().parent().parent().parent().parent().parent().attr('data-object-id');
		var data = {action:action,trackNo:trackNo,recordID:recordID,formID:formID}
		var embed = $(this).parents('.embed-view-container');
		// Confirmation
		var message = "Are you sure you want to paste your selected record? It will append at the bottom of this table.";
		var newConfirm = new jConfirm(message, 'Confirmation Dialog', '', '', '', function(r) {
			if($('#popup_container').find('#popup_ok').hasClass('temp_disable')){
				return;
			}
			else{
				$('#popup_container').find('#popup_ok').addClass('temp_disable');
			}
			if(typeof embed.attr('source-form-type') != "undefined"){
				if(embed.attr('source-form-type') == "multiple"){
					var selected_rows = embed.data('selected_rows');
				    if (r == true) {
				    	if(selected_rows.length > 0){
		    		    	for(i in selected_rows){
		    		    		var self_parent = $(selected_rows[i]);
		    					if(self_parent.attr('is-pending')){
		    						var cloned_tr = self_parent.clone();
		    						var record_id = cloned_tr.attr('record-id');
		    						var ref_id = cloned_tr.attr('reference-id');
		    						var fields_data = $('body').data('embed_row_data')[object_id][ref_id];
		    						cloned_tr.removeClass('tr_' + record_id);
		    						cloned_tr.removeClass(ref_id);
		    						var tbody = setCopy.parent();
		    						console.log('tbody', tbody);
		    						var record_ids = tbody.find('tr:not(.appendNewRecord)').map(function(){
		    							return $(this).attr('record-id');
		    						}).get();
		    						var new_record_id = (Math.max.apply(Math,record_ids) + 1);
		    						var new_ref_id = object_id + '_' + new_record_id;
		    						var new_class = 'tr_' + new_record_id;
		    						cloned_tr.removeClass('setCopy');
		    						self_parent.removeClass('setCopy');
		    						cloned_tr.find('.embed_getFields').removeClass('inlineEdit_' + record_id).addClass('inlineEdit_' + new_record_id).attr('data-embed-name', 'embed_textbox_' + new_record_id);
		    						cloned_tr.find('.embed_getFields[type="radio"], .embed_getFields[type="checkbox"]').each(function(){
		    							$(this).attr('name', $(this).attr('data-field-name') + new_record_id);
		    						});
		    						cloned_tr.find('.embed_getFields[obj-type="pickList"], .embed_getFields[obj-type="listNames"]').each(function(){
		    							if($(this).attr('obj-type') == "pickList"){
		    								var id = $(this).attr('id');
		    								var reg = new RegExp(record_id, 'g');
		    								id = id.replace(reg, new_record_id);
		    								$(this).attr('id', id);
		    								$(this).parents('tr').find('a.pickListButton').attr('return-field', id);
		    							}
		    							else{
		    								var id = $(this).attr('id');
		    								var reg = new RegExp(record_id, 'g');
		    								id = id.replace(reg, new_record_id);
		    								$(this).attr('id', id);
		    								$(this).parents('tr').find('a.viewNames').attr('return-field', id);
		    							}
		    						});
		    						cloned_tr.find('.lblEmbed_' + record_id).removeClass('lblEmbed_' + record_id).addClass('lblEmbed_' + new_record_id).attr('id', 'lblEmbed_' + new_record_id);
		    						cloned_tr.attr('record-id', new_record_id);
		    						cloned_tr.attr('reference-id', new_ref_id);
		    						cloned_tr.addClass(new_class);
		    						cloned_tr.addClass(new_ref_id);
		    						self_parent.find('.showpasteActions_' + record_id).removeClass('display');
		    						self_parent.find('.hidepasteActions_' + record_id).addClass('display');
		    						cloned_tr.find('.showpasteActions_' + record_id).addClass('showpasteActions_' + new_record_id).removeClass('showpasteActions_' + record_id).removeClass('display');
		    						cloned_tr.find('.hidepasteActions_' + record_id).addClass('hidepasteActions_' + new_record_id).removeClass('hidepasteActions_' + record_id).addClass('display');
		    						cloned_tr.find('#showEditActions_' + record_id).attr('id','showEditActions_' + new_record_id).removeClass('display');
		    						cloned_tr.find('#hideEditActions_' + record_id).attr('id','hideEditActions_' + new_record_id).addClass('display');
		    						self_parent.removeAttr('style');
		    						cloned_tr.removeAttr('style');
		    						$('body').data('embed_row_data')[object_id][new_ref_id] = fields_data;
		    						setCopy.parents('.embed-view-container').find('.appendNewRecord').before(cloned_tr);
		    						var data = self_parent.data();
		    						for(i in data){
		    							cloned_tr.data(i, data[i]);
		    						}
		    						cloned_tr.find('.embed_getFields').off('change.embedFormulaEvent');
		    						embeded_view.bindCreateEvents(setCopy.closest('.embed-view-container'), embed.find('.tr_' + new_record_id));
		    						embeded_view.bindEvents(embed.find('.tr_' + new_record_id).find('.viewTDEmbedded'));
		    						embeded_view.fixNumbering(setCopy.closest('.embed-view-container'));
		    						embeded_view.getSetTotalOfColumn.call(setCopy.parent().parent().parent(), setCopy.parent().parent().parent().children());
		    					}
		    					else{
		    						trackNo = self_parent.attr("record-trackno");
		    						recordID = self_parent.attr("record-id");
		    						formID = self_parent.attr("form-id");
		    						var data = {
		    							'action':action,
		    							'trackNo':trackNo,
		    							'recordID':recordID,
		    							'formID':formID,
		    							'index': i,
		    						}
		    						$.post("/ajax/embedded_view_functions",data,function(e){
		    						    console.log("data_copied", e);
		    						    var json_data = JSON.parse(e);
		    						    var coun_tr = $("tr_" + recordID).length;
		    						    var new_tr = parseInt(coun_tr) + 1;
		    						    if (json_data['notification'] == "Successfull") {
		    							showNotification({
		    							    message: json_data['message'],
		    							    type: "success",
		    							    autoClose: true,
		    							    duration: 3
		    							});
		    							var ret = "";
		    							var target_row = $(selected_rows[json_data['referenceIndex']]);
		    							var td = target_row.html();
		    							ret += '<tr teest="' + new_tr + '" record-trackno="' + json_data['trackNo'] + '" record-id="' + json_data['recordID'] + '" class="">';
		    							    ret += td.replace(new RegExp('_' + recordID, "g"),'_' + json_data['recordID']);
		    							ret += '</tr>';
		    							$(ret).insertAfter(embed.find('.appendNewRecord'));
		    							
		    							embed.find(".showpasteActions_" + json_data['oldRecordID'] ).removeClass("display");
		    							embed.find(".hidepasteActions_" + json_data['oldRecordID'] ).addClass("display");
		    							
		    							embed.find(".showpasteActions_" + json_data['recordID']).removeClass("display");
		    							embed.find(".hidepasteActions_" + json_data['recordID']).addClass("display");
		    							
		    							target_row.css("background-color","");
		    							target_row.removeClass("setCopy");
		    							embeded_view.fixNumbering(embed);
		    							$("[name='" + resultFld + "']").trigger("change");
		    						    }
		    						});
		    					}
		    				}

				    	}
				    	else{
				    		if(self_parent.attr('is-pending')){
				    			var cloned_tr = self_parent.clone();
				    			var record_id = cloned_tr.attr('record-id');
				    			var ref_id = cloned_tr.attr('reference-id');
				    			var fields_data = $('body').data('embed_row_data')[object_id][ref_id];
				    			cloned_tr.removeClass('tr_' + record_id);
				    			cloned_tr.removeClass(ref_id);
				    			var tbody = setCopy.parent();
				    			console.log('tbody', tbody);
				    			var record_ids = tbody.find('tr:not(.appendNewRecord)').map(function(){
				    				return $(this).attr('record-id');
				    			}).get();
				    			var new_record_id = (Math.max.apply(Math,record_ids) + 1);
				    			var new_ref_id = object_id + '_' + new_record_id;
				    			var new_class = 'tr_' + new_record_id;
				    			cloned_tr.removeClass('setCopy');
				    			self_parent.removeClass('setCopy');
				    			cloned_tr.find('.embed_getFields').removeClass('inlineEdit_' + record_id).addClass('inlineEdit_' + new_record_id).attr('data-embed-name', 'embed_textbox_' + new_record_id);
				    			cloned_tr.find('.embed_getFields[type="radio"], .embed_getFields[type="checkbox"]').each(function(){
				    				$(this).attr('name', $(this).attr('data-field-name') + new_record_id);
				    			});
				    			cloned_tr.find('.embed_getFields[obj-type="pickList"], .embed_getFields[obj-type="listNames"]').each(function(){
				    				if($(this).attr('obj-type') == "pickList"){
				    					var id = $(this).attr('id');
				    					var reg = new RegExp(record_id, 'g');
				    					id = id.replace(reg, new_record_id);
				    					$(this).attr('id', id);
				    					$(this).parents('tr').find('a.pickListButton').attr('return-field', id);
				    				}
				    				else{
				    					var id = $(this).attr('id');
				    					var reg = new RegExp(record_id, 'g');
				    					id = id.replace(reg, new_record_id);
				    					$(this).attr('id', id);
				    					$(this).parents('tr').find('a.viewNames').attr('return-field', id);
				    				}
				    			});
				    			cloned_tr.find('.lblEmbed_' + record_id).removeClass('lblEmbed_' + record_id).addClass('lblEmbed_' + new_record_id).attr('id', 'lblEmbed_' + new_record_id);
				    			cloned_tr.attr('record-id', new_record_id);
				    			cloned_tr.attr('reference-id', new_ref_id);
				    			cloned_tr.addClass(new_class);
				    			cloned_tr.addClass(new_ref_id);
				    			self_parent.find('.showpasteActions_' + record_id).removeClass('display');
				    			self_parent.find('.hidepasteActions_' + record_id).addClass('display');
				    			cloned_tr.find('.showpasteActions_' + record_id).addClass('showpasteActions_' + new_record_id).removeClass('showpasteActions_' + record_id).removeClass('display');
				    			cloned_tr.find('.hidepasteActions_' + record_id).addClass('hidepasteActions_' + new_record_id).removeClass('hidepasteActions_' + record_id).addClass('display');
				    			cloned_tr.find('#showEditActions_' + record_id).attr('id','showEditActions_' + new_record_id).removeClass('display');
				    			cloned_tr.find('#hideEditActions_' + record_id).attr('id','hideEditActions_' + new_record_id).addClass('display');
				    			self_parent.removeAttr('style');
				    			cloned_tr.removeAttr('style');
				    			$('body').data('embed_row_data')[object_id][new_ref_id] = fields_data;
				    			setCopy.parents('.embed-view-container').find('.appendNewRecord').before(cloned_tr);
				    			var data = self_parent.data();
				    			for(i in data){
				    				cloned_tr.data(i, data[i]);
				    			}
				    			cloned_tr.find('.embed_getFields').off('change.embedFormulaEvent');
				    			embeded_view.bindCreateEvents(setCopy.closest('.embed-view-container'), embed.find('.tr_' + new_record_id));
				    			embeded_view.bindEvents(embed.find('.tr_' + new_record_id).find('.viewTDEmbedded'));
				    			embeded_view.fixNumbering(setCopy.closest('.embed-view-container'));
				    			embeded_view.getSetTotalOfColumn.call(setCopy.parent().parent().parent(), setCopy.parent().parent().parent().children());
				    		}
				    		else{
				    			var data = {
				    				'action':action,
				    				'trackNo':trackNo,
				    				'recordID':recordID,
				    				'formID':formID
				    			}
				    			$.post("/ajax/embedded_view_functions",data,function(e){
				    			    console.log("data_copied", e);
				    			    var json_data = JSON.parse(e);
				    			    var coun_tr = $("tr_" + recordID).length;
				    			    var new_tr = parseInt(coun_tr) + 1;
				    			    if (json_data['notification'] == "Successfull") {
				    				showNotification({
				    				    message: json_data['message'],
				    				    type: "success",
				    				    autoClose: true,
				    				    duration: 3
				    				});
				    				var ret = "";
				    				var td = $(".setCopy").html();
				    				ret += '<tr teest="' + new_tr + '" record-trackno="' + json_data['trackNo'] + '" record-id="' + json_data['recordID'] + '" class="">';
				    				    ret += td.replace(new RegExp('_' + recordID, "g"),'_' + json_data['recordID']);
				    				ret += '</tr>';
				    				$(ret).insertAfter(".appendNewRecord");
				    				
				    				$(".showpasteActions_" + recordID).removeClass("display");
				    				$(".hidepasteActions_" + recordID).addClass("display");
				    				
				    				$(".showpasteActions_" + json_data['recordID']).removeClass("display");
				    				$(".hidepasteActions_" + json_data['recordID']).addClass("display");
				    				
				    				setCopy.css("background-color","");
				    				setCopy.removeClass("setCopy");
				    				$("[name='" + resultFld + "']").trigger("change");
				    			    }
				    			});
				    		}
				    	}
				    }
				}
				else{
				    if (r == true) {
						if(self_parent.attr('is-pending')){
							var cloned_tr = self_parent.clone();
							var record_id = cloned_tr.attr('record-id');
							var ref_id = cloned_tr.attr('reference-id');
							var fields_data = $('body').data('embed_row_data')[object_id][ref_id];
							cloned_tr.removeClass('tr_' + record_id);
							cloned_tr.removeClass(ref_id);
							var tbody = setCopy.parent();
							console.log('tbody', tbody);
							var record_ids = tbody.find('tr:not(.appendNewRecord)').map(function(){
								return $(this).attr('record-id');
							}).get();
							var new_record_id = (Math.max.apply(Math,record_ids) + 1);
							var new_ref_id = object_id + '_' + new_record_id;
							var new_class = 'tr_' + new_record_id;
							cloned_tr.removeClass('setCopy');
							self_parent.removeClass('setCopy');
							cloned_tr.find('.embed_getFields').removeClass('inlineEdit_' + record_id).addClass('inlineEdit_' + new_record_id).attr('data-embed-name', 'embed_textbox_' + new_record_id);
							cloned_tr.find('.embed_getFields[type="radio"], .embed_getFields[type="checkbox"]').each(function(){
								$(this).attr('name', $(this).attr('data-field-name') + new_record_id);
							});
							cloned_tr.find('.embed_getFields[obj-type="pickList"], .embed_getFields[obj-type="listNames"]').each(function(){
								if($(this).attr('obj-type') == "pickList"){
									var id = $(this).attr('id');
									var reg = new RegExp(record_id, 'g');
									id = id.replace(reg, new_record_id);
									$(this).attr('id', id);
									$(this).parents('tr').find('a.pickListButton').attr('return-field', id);
								}
								else{
									var id = $(this).attr('id');
									var reg = new RegExp(record_id, 'g');
									id = id.replace(reg, new_record_id);
									$(this).attr('id', id);
									$(this).parents('tr').find('a.viewNames').attr('return-field', id);
								}
							});
							cloned_tr.find('.lblEmbed_' + record_id).removeClass('lblEmbed_' + record_id).addClass('lblEmbed_' + new_record_id).attr('id', 'lblEmbed_' + new_record_id);
							cloned_tr.attr('record-id', new_record_id);
							cloned_tr.attr('reference-id', new_ref_id);
							cloned_tr.addClass(new_class);
							cloned_tr.addClass(new_ref_id);
							self_parent.find('.showpasteActions_' + record_id).removeClass('display');
							self_parent.find('.hidepasteActions_' + record_id).addClass('display');
							cloned_tr.find('.showpasteActions_' + record_id).addClass('showpasteActions_' + new_record_id).removeClass('showpasteActions_' + record_id).removeClass('display');
							cloned_tr.find('.hidepasteActions_' + record_id).addClass('hidepasteActions_' + new_record_id).removeClass('hidepasteActions_' + record_id).addClass('display');
							cloned_tr.find('#showEditActions_' + record_id).attr('id','showEditActions_' + new_record_id).removeClass('display');
							cloned_tr.find('#hideEditActions_' + record_id).attr('id','hideEditActions_' + new_record_id).addClass('display');
							self_parent.removeAttr('style');
							cloned_tr.removeAttr('style');
							$('body').data('embed_row_data')[object_id][new_ref_id] = fields_data;
							setCopy.parents('.embed-view-container').find('.appendNewRecord').before(cloned_tr);
							var data = self_parent.data();
							for(i in data){
								cloned_tr.data(i, data[i]);
							}
							cloned_tr.find('.embed_getFields').off('change.embedFormulaEvent');
							embeded_view.bindCreateEvents(setCopy.closest('.embed-view-container'), embed.find('.tr_' + new_record_id));
							embeded_view.bindEvents(embed.find('.tr_' + new_record_id).find('.viewTDEmbedded'));
							embeded_view.fixNumbering(setCopy.closest('.embed-view-container'));
							embeded_view.getSetTotalOfColumn.call(setCopy.parent().parent().parent(), setCopy.parent().parent().parent().children());
						}
						else{
							var data = {
								'action':action,
								'trackNo':trackNo,
								'recordID':recordID,
								'formID':formID
							}
							$.post("/ajax/embedded_view_functions",data,function(e){
							    console.log("data_copied", e);
							    var json_data = JSON.parse(e);
							    var coun_tr = $("tr_" + recordID).length;
							    var new_tr = parseInt(coun_tr) + 1;
							    if (json_data['notification'] == "Successfull") {
								showNotification({
								    message: json_data['message'],
								    type: "success",
								    autoClose: true,
								    duration: 3
								});
								var ret = "";
								var td = $(".setCopy").html();
								ret += '<tr teest="' + new_tr + '" record-trackno="' + json_data['trackNo'] + '" record-id="' + json_data['recordID'] + '" class="">';
								    ret += td.replace(new RegExp('_' + recordID, "g"),'_' + json_data['recordID']);
								ret += '</tr>';
								$(ret).insertAfter(".appendNewRecord");
								
								$(".showpasteActions_" + recordID).removeClass("display");
								$(".hidepasteActions_" + recordID).addClass("display");
								
								$(".showpasteActions_" + json_data['recordID']).removeClass("display");
								$(".hidepasteActions_" + json_data['recordID']).addClass("display");
								
								setCopy.css("background-color","");
								setCopy.removeClass("setCopy");
								$("[name='" + resultFld + "']").trigger("change");
							    }
							});
						}
				    }
				}
			}
			else{
			    if (r == true) {
					if(self_parent.attr('is-pending')){
						var cloned_tr = self_parent.clone();
						var record_id = cloned_tr.attr('record-id');
						var ref_id = cloned_tr.attr('reference-id');
						var fields_data = $('body').data('embed_row_data')[object_id][ref_id];
						cloned_tr.removeClass('tr_' + record_id);
						cloned_tr.removeClass(ref_id);
						var tbody = setCopy.parent();
						console.log('tbody', tbody);
						var record_ids = tbody.find('tr:not(.appendNewRecord)').map(function(){
							return $(this).attr('record-id');
						}).get();
						var new_record_id = (Math.max.apply(Math,record_ids) + 1);
						var new_ref_id = object_id + '_' + new_record_id;
						var new_class = 'tr_' + new_record_id;
						cloned_tr.removeClass('setCopy');
						self_parent.removeClass('setCopy');
						cloned_tr.find('.embed_getFields').removeClass('inlineEdit_' + record_id).addClass('inlineEdit_' + new_record_id).attr('data-embed-name', 'embed_textbox_' + new_record_id);
						cloned_tr.find('.embed_getFields[type="radio"], .embed_getFields[type="checkbox"]').each(function(){
							$(this).attr('name', $(this).attr('data-field-name') + new_record_id);
						});
						cloned_tr.find('.embed_getFields[obj-type="pickList"], .embed_getFields[obj-type="listNames"]').each(function(){
							if($(this).attr('obj-type') == "pickList"){
								var id = $(this).attr('id');
								var reg = new RegExp(record_id, 'g');
								id = id.replace(reg, new_record_id);
								$(this).attr('id', id);
								$(this).parents('tr').find('a.pickListButton').attr('return-field', id);
							}
							else{
								var id = $(this).attr('id');
								var reg = new RegExp(record_id, 'g');
								id = id.replace(reg, new_record_id);
								$(this).attr('id', id);
								$(this).parents('tr').find('a.viewNames').attr('return-field', id);
							}
						});
						cloned_tr.find('.lblEmbed_' + record_id).removeClass('lblEmbed_' + record_id).addClass('lblEmbed_' + new_record_id).attr('id', 'lblEmbed_' + new_record_id);
						cloned_tr.attr('record-id', new_record_id);
						cloned_tr.attr('reference-id', new_ref_id);
						cloned_tr.addClass(new_class);
						cloned_tr.addClass(new_ref_id);
						self_parent.find('.showpasteActions_' + record_id).removeClass('display');
						self_parent.find('.hidepasteActions_' + record_id).addClass('display');
						cloned_tr.find('.showpasteActions_' + record_id).addClass('showpasteActions_' + new_record_id).removeClass('showpasteActions_' + record_id).removeClass('display');
						cloned_tr.find('.hidepasteActions_' + record_id).addClass('hidepasteActions_' + new_record_id).removeClass('hidepasteActions_' + record_id).addClass('display');
						cloned_tr.find('#showEditActions_' + record_id).attr('id','showEditActions_' + new_record_id).removeClass('display');
						cloned_tr.find('#hideEditActions_' + record_id).attr('id','hideEditActions_' + new_record_id).addClass('display');
						self_parent.removeAttr('style');
						cloned_tr.removeAttr('style');
						$('body').data('embed_row_data')[object_id][new_ref_id] = fields_data;
						setCopy.parents('.embed-view-container').find('.appendNewRecord').before(cloned_tr);
						var data = self_parent.data();
						for(i in data){
							cloned_tr.data(i, data[i]);
						}
						cloned_tr.find('.embed_getFields').off('change.embedFormulaEvent');
						embeded_view.bindCreateEvents(setCopy.closest('.embed-view-container'), embed.find('.tr_' + new_record_id));
						embeded_view.bindEvents(embed.find('.tr_' + new_record_id).find('.viewTDEmbedded'));
						embeded_view.fixNumbering(setCopy.closest('.embed-view-container'));
						embeded_view.getSetTotalOfColumn.call(setCopy.parent().parent().parent(), setCopy.parent().parent().parent().children());
					}
					else{
						var data = {
							'action':action,
							'trackNo':trackNo,
							'recordID':recordID,
							'formID':formID
						}
						$.post("/ajax/embedded_view_functions",data,function(e){
						    console.log("data_copied", e);
						    var json_data = JSON.parse(e);
						    var coun_tr = $("tr_" + recordID).length;
						    var new_tr = parseInt(coun_tr) + 1;
						    if (json_data['notification'] == "Successfull") {
							showNotification({
							    message: json_data['message'],
							    type: "success",
							    autoClose: true,
							    duration: 3
							});
							var ret = "";
							var td = $(".setCopy").html();
							ret += '<tr teest="' + new_tr + '" record-trackno="' + json_data['trackNo'] + '" record-id="' + json_data['recordID'] + '" class="">';
							    ret += td.replace(new RegExp('_' + recordID, "g"),'_' + json_data['recordID']);
							ret += '</tr>';
							$(ret).insertAfter(".appendNewRecord");
							
							$(".showpasteActions_" + recordID).removeClass("display");
							$(".hidepasteActions_" + recordID).addClass("display");
							
							$(".showpasteActions_" + json_data['recordID']).removeClass("display");
							$(".hidepasteActions_" + json_data['recordID']).addClass("display");
							
							setCopy.css("background-color","");
							setCopy.removeClass("setCopy");
							$("[name='" + resultFld + "']").trigger("change");
						    }
						});
					}
			    }
			}
		});
		newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right" style="color:#f00; font-size:15px;"></i>'});   
		
		
	    });
	    
	    // Cancel Edit
	    $("body").on("click",".cancelpasteDEmbedded",function(){
		   	if(embed_inline_add==1){
		   		$(document).contextmenu("enableEntry", "paste", false);
		    }
		    var embedded_view = $(this).parents('.embed-view-container');
			if(typeof embedded_view.attr('source-form-type') != "undefined"){
				if(embedded_view.attr('source-form-type') == "multiple"){	
					var selected_rows = embedded_view.data('selected_rows')||[];
					if(selected_rows.length > 0){
						for(var i in selected_rows){
							var row = $(selected_rows[i]);
							var id = row.attr('record-id');
							if(row.hasClass('setCopy')){
								row.find(".showpasteActions_" + id).removeClass("display");
								row.find(".hidepasteActions_" + id).addClass("display");
								row.css("background-color","");
								row.removeClass("setCopy");
							}
						}
					}
					else{
						var setCopy = $(".setCopy");
						var recordID = setCopy.attr("record-id");
						
						$(".showpasteActions_" + recordID).removeClass("display");
						$(".hidepasteActions_" + recordID).addClass("display");
						setCopy.css("background-color","");
						setCopy.removeClass("setCopy");
					}
				}
				else{
					var setCopy = $(".setCopy");
					var recordID = setCopy.attr("record-id");
					
					$(".showpasteActions_" + recordID).removeClass("display");
					$(".hidepasteActions_" + recordID).addClass("display");
					setCopy.css("background-color","");
					setCopy.removeClass("setCopy");
				}
			}
			else{
				var setCopy = $(".setCopy");
				var recordID = setCopy.attr("record-id");
				
				$(".showpasteActions_" + recordID).removeClass("display");
				$(".hidepasteActions_" + recordID).addClass("display");
				setCopy.css("background-color","");
				setCopy.removeClass("setCopy");
			}
	    })
	}
	
    }

//modify by japhet morada
var embeded_view = {
    init: function () {
        this.load();
    },
    load: function () {
        var self = this;
        $('.embed-view-container').each(function () {
        	if($.type($(this).attr('source-form-type')) != "undefined"){
        		if($(this).attr('source-form-type') == "multiple"){
      //   			var do_id = $(this).parents('.setOBJ[data-type="embeded-view"][data-object-id]:eq(0)').attr('data-object-id');
      //   			var element_data = $('body').data('user_form_json_data')['form_json']['' + do_id];
      //   			var form_id_list = element_data.multiple_forms;
      //   			var column_list = element_data.column_settings;
      //   			var embedded_view_object = $(this);
      //   			var field_values = {};
      //   			ui.blockPortion(embedded_view_object);
      //   			for(i in form_id_list){
    		// 			var ref_field = $('[name="' + form_id_list[i]['search_field'] + '"]');
    		// 			var ref_field_val = "";
    		// 			if(ref_field.length > 0){
    		// 				if(ref_field.closest('.setOBJ').attr('data-type') == "radioButton"){
    		// 					ref_field_val = $('[name="' + form_id_list[i]['search_field'] + '"]:checked').val();
    		// 					field_values[form_id_list[i]['lookup_field']] = ref_field_val||"";
    		// 				}
    		// 				else if(ref_field.closest('.setOBJ').attr('data-type') == "checkbox"){
    		// 					ref_field_val = $('[name="' + form_id_list[i]['search_field'] + '[]"]:checked').map(function(a, b){ return $.trim($(this).val()); });
    		// 					field_values[form_id_list[i]['lookup_field']] = ref_field_val||[];
    		// 				}
    		// 				else if(ref_field.closest('.setOBJ').attr('data-type') == "selectMany"){
    		// 					ref_field_val = $('[name="' + form_id_list[i]['search_field'] + '[]"]>option:selected').map(function(){ return $.trim($(this).val()); });
    		// 					field_values[form_id_list[i]['lookup_field']] = ref_field_val||[];
    		// 				}
    		// 				else{
    		// 					ref_field_val =ref_field.val();
    		// 					field_values[form_id_list[i]['lookup_field']] = ref_field_val||"";
    		// 				}
    		// 			}
    		// 			field_values[form_id_list[i]['lookup_field']] = ref_field.val()||"";
      //   			}
      //   			var postdata = {
      //   				"formsourcetype": embedded_view_object.attr('source-form-type'),
      //   				"form_id_list": form_id_list,
      //   				"column_reference_fields": field_values,
      //   				"column_settings": column_list,
      //   				"enable_embed_row_click": embedded_view_object.attr("enable-embed-row-click"),
      //   				"embed_action_click_copy": embedded_view_object.attr('embed-action-click-copy'),
      //   				"embed_action_click_delete": embedded_view_object.attr('embed-action-click-delete'),
      //   				"embed_action_click_edit": embedded_view_object.attr('embed-action-click-edit'),
      //   				"embed_action_click_view": embedded_view_object.attr('embed-action-click-view'),
      //   				"embed_action_click_number": embedded_view_object.attr('embed-action-click-number'),
      //   				"embed_action_click_edit_popup": embedded_view_object.attr('embed-action-click-edit-popup'),
      //   				"embed_default_sorting": {"column": "ID", "type": "ASC"}

      //   			}
      //   			$.post("/ajax/embedded_view", postdata, function(echo_result){
      //   				var rows_element;
      //   				ui.unblockPortion(embedded_view_object);
      //   				embedded_view_object.html(echo_result);
      //   				applyMultipleSelection(embedded_view_object, do_id);
      //   				rows_element = embedded_view_object.find("tr").filter(":not(:eq(0))").find(".viewTDEmbedded");
      //   				embedded_view_object.find("tr").filter(":not(:eq(0))").addClass("hovereffect");
						// //applying checkboxes
						// EmbeddedFixHeader.init(embedded_view_object);
						// EmbeddedFixHeader.reFixHeader(embedded_view_object);
						// rebindViewEvents(rows_element, embedded_view_object, element_data['multiple_forms']);
      //   			});
					var embedded_view = $(this);
					ui.blockPortion(embedded_view);
					initializeMultipleEmbedddedView(embedded_view, "onload");
					var do_id = embedded_view.parents('.setOBJ[data-type="embeded-view"][data-object-id]:eq(0)').attr('data-object-id');
					var m_forms = $('body').data('user_form_json_data')['form_json']['' + do_id]['multiple_forms'];
					var field_name_collectors = [];
					for(i in m_forms){
						if(field_name_collectors.indexOf(m_forms[i]['search_field']) <= -1){
							var search_field_i = m_forms[i]['search_field'];
							// console.log("check this field...", '[name="' + search_field_i + '"]');
							$('[name="' + search_field_i + '"]').on('change.multipleEmbedView', function(e){
								ui.blockPortion(embedded_view);
								initializeMultipleEmbedddedView(embedded_view, "onchange");
							})
							field_name_collectors.push(search_field_i);
						}
					}
					embedded_view.data("field_reference", field_name_collectors);
        			// console.log("multiple_test", form_id_list, column_list, field_values);
        		}
        		else{
		        	var do_id = $(this).parents('.setOBJ[data-type="embeded-view"][data-object-id]:eq(0)').attr('data-object-id');
		            var formId = $(this).attr('embed-source-form-val-id');
		            var fieldReference = $(this).attr('embed-result-field-val');
		            var fieldValue;
		            var fieldReferenceObjectName = $(this).attr('search-field-object-name');
		            if(fieldReferenceObjectName == 'pickList' || fieldReferenceObjectName == 'checkbox' || fieldReferenceObjectName == 'selectMany'){
		            	if(fieldReferenceObjectName == 'pickList'){
		            		fieldValue = $('[name="' + fieldReference + '"]').val().split('|');
		            	}
		            	else if(fieldReferenceObjectName == 'checkbox'){
		            		fieldValue = $('[name="' + fieldReference + '[]"]:checked').map(function(a, b){ return $.trim($(this).val()); });
		            	}
		            	else if(fieldReferenceObjectName == 'selectMany'){
		            		fieldValue = $('[name="' + fieldReference + '[]"]>option:selected').map(function(){ return $.trim($(this).val()); });
		            	}
		            }
		            else{
		            	fieldValue = $('[name="' + fieldReference + '"]').val();
		            }
		            
		            var fieldFilter = $(this).attr('embed-source-lookup-active-field-val');
		            var fieldFilterConditionalOperator = $(this).attr('embed-conditional-operator');
		            
		            if ($.type(fieldFilterConditionalOperator) == "undefined") {
		                fieldFilterConditionalOperator = "=";
		            } else if (fieldFilterConditionalOperator == "") {
		                fieldFilterConditionalOperator = "=";
		            }
		            var embedAdditionalFilterFormula = $(this).attr('embed-additional-filter-formula');
		            if ($.type(embedAdditionalFilterFormula) == "undefined") {
		                embedAdditionalFilterFormula = "";
		            }
		            var fieldReferenceType = $(this).attr('rfc-choice');
		            var enable_embed_row_click = $(this).attr("enable-embed-row-click");
		            var embed_action_click_copy = $(this).attr('embed-action-click-copy');
		            var embed_action_click_delete = $(this).attr('embed-action-click-delete');
		            var embed_action_click_edit = $(this).attr('embed-action-click-edit');
		            var embed_action_click_view = $(this).attr('embed-action-click-view');
		            var embed_action_click_number = $(this).attr('embed-action-click-number');
		            var embed_action_click_edit_popup = $(this).attr('embed-action-click-edit-popup');
		            if (fieldReferenceType == 'computed') {
		                var thisFormula = $(this).attr('embed-computed-formula');
		                var formulaDoc = new Formula(thisFormula);
		                fieldValue = formulaDoc.getEvaluation();
		                embeded_view.embedSetupComputedSearch($(this));
		            }
		            //added by japhet morada
		            var embed_row_catgory = "";
		            if($('body').data('user_form_json_data')['form_json'][""+do_id]['allow_row_category'] == true){
		            	embed_row_catgory = $('body').data('user_form_json_data')['form_json'][""+do_id]['allow_row_category_column']||"";
		            }
	            	var embedded_view = $(this);
	            	loadCustomActions(embedded_view, do_id);
	            	//====================================================//
		            var container = this;
		            if (fieldValue !== null && fieldValue !== 'null') {
		                var reference = {
		                    FormID: formId,
		                    FieldReference: fieldReference,
		                    FieldValue: fieldValue,
		                    FieldFilter: fieldFilter,
		                    "field_conditional_operator": fieldFilterConditionalOperator,
		                    "embed_additional_filter_formula": self.parseCondition(embedAdditionalFilterFormula),
		                    HLData: "",
		                    HLType: "row",
		                    HLAllow: "false",
		                    column_data: "",
		                    current_form_fields_data: [],
		                    "enable_embed_row_click": enable_embed_row_click,
		                    "embed_action_click_copy": embed_action_click_copy,
		                    "embed_action_click_delete": embed_action_click_delete,
		                    "embed_action_click_edit": embed_action_click_edit,
		                    "embed_action_click_view": embed_action_click_view,
		                    "embed_action_click_number": embed_action_click_number,
		                    "embed_action_click_edit_popup": embed_action_click_edit_popup,
		                    "embed_default_sorting": {"column": "ID", "type": "ASC"},
		                    "embed_row_category": embed_row_catgory

		                };
		                if ($(container).is('[data-default-sort-col]')) {
		                    if ($(container).attr('data-default-sort-col') != '--Selected--') {
		                        var sort_type = $(container).attr('data-default-sort-type');
		                        var sort_column = $(container).attr('data-default-sort-col');
		                        reference['embed_default_sorting']['column'] = sort_column;
		                        reference['embed_default_sorting']['type'] = sort_type;
		                    }
		                }
		                if (typeof $(container).attr('embed-hl-data') != "undefined") {
		                    reference["HLData"] = $(container).attr('embed-hl-data');
		                }
		                if (typeof $(container).attr('allow-highlights') != "undefined") {
		                    reference["HLAllow"] = $(container).attr('allow-highlights');
		                }
		                if (typeof $(container).attr('allow-highlights') != "undefined") {
		                    reference["HLType"] = $(container).attr('highlight-type');
		                }
		                if (typeof $(container).attr('embed-column-data') != "undefined") {
		                    reference["column_data"] = $(container).attr('embed-column-data');
		                }
		                if($(this).attr('commit-data-row-event') == "parent"){
		                	$(this).addClass('embed-edit-mode');
		                }
		                reference['search_field_obj_name'] = fieldReferenceObjectName;
		                $('#frmrequest').find(".setOBJ").each(function (eqi) {
		                    var data_object_id = $(this).attr("data-object-id");
		                    var getFIELDS = $(this).find(".getFields_" + data_object_id);
		                    var field_name = $(this).find(".getFields_" + data_object_id).eq(0).attr("name");
		                    field_name_values = [];
		                    getFIELDS.each(function (eqi2) {
		                        field_name_values.push($(this).val());
		                    })
		                    field_name_values = field_name_values.join("|^|");
		                    reference["current_form_fields_data"].push({
		                        "f_name": field_name,
		                        "values": field_name_values
		                    });
		                });
		                reference["current_form_fields_data"] = JSON.stringify(reference["current_form_fields_data"]);
		                $(container).html('<center>' + '<div class="spinner display load-app-request" style="margin-top:30px;">' +
		                        '<div class="bar1"></div>' +
		                        '<div class="bar2"></div>' +
		                        '<div class="bar3"></div>' +
		                        '<div class="bar4"></div>' +
		                        '<div class="bar5"></div>' +
		                        '<div class="bar6"></div>' +
		                        '<div class="bar7"></div>' +
		                        '<div class="bar8"></div>' +
		                        '<div class="bar9"></div>' +
		                        '<div class="bar10"></div>' +
		                        '</div>' + '</center>');


		                $(container).data("embed_reference", reference);
		                reference["embed_container_id"] = do_id;
		                self.getRequests(reference, function (data) {
		                    var obj_jq = $(data);
		                    var parent_container_embed = $(container)
		                    parent_container_embed.html(obj_jq);
		                    applyMultipleSelection(parent_container_embed, do_id);
		                    if(embed_row_catgory != ""){
		                    	categorizeRow(embedded_view_object);
		                    }
		                    var rows_element = obj_jq.find("tr").filter(":not(:eq(0))").find(".viewTDEmbedded");

		                    obj_jq.find("tr").filter(":not(:eq(0))").addClass("hovereffect");

		                    if (obj_jq.attr("author-valid")) {

		                        if (obj_jq.attr("author-valid") == "true") {

		                        } else {
		                            parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
		                        }
		                    } else {
		                        parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
		                    }
		                    //console.log("rows_element",rows_element)
		                    self.getSetTotalOfColumn.call(parent_container_embed, obj_jq);
		                    //self.embedBindHeaderSorting(obj_jq);
		                    rows_element.each(function(){ //FS#7646
		                    	var dis_row_click_ele = $(this).parents("tr").eq(0);
		                    	var row_click_data = {
		                    	    "record_trackno": dis_row_click_ele.attr("record-trackno"),
		                    	    "record_id": dis_row_click_ele.attr("record-id"),
		                    	    "formId": dis_row_click_ele.attr('form-id')
		                    	}
		                    	if ($(this).attr("data-action-attr") == "view") {
		                    	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
		                    	} else {
		                    	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
		                    	}
		                    	$(this).attr("data-embed-href-link",src); //FS#7646
		                    });
		                    rows_element.on({
		                        "click": function () {

		                            var dis_ele = $(this);
		                            var embed_view_cont = dis_ele.parents(".embed-view-container").eq(0);
		                            $('html').data("embed_container", embed_view_cont);
		                            var embeded_attr = $(this).attr("data-action-attr");
		                            var reference_id = dis_ele.parents('tr').attr('reference-id')||dis_ele.parents('.embed-view-container').attr('embed-source-form-val-id') + '_' + dis_ele.parents('tr').attr('record-id');
		                            var allow_row_click = $(container).attr("enable-embed-row-click")
		                            if (allow_row_click) {
		                                if (allow_row_click != "true") {
		                                    return;
		                                }
		                            } else {
		                                return;
		                            }
		                            var dis_row_click_ele = $(this).parents("tr").eq(0);
		                            var row_click_data = {
		                                "record_trackno": dis_row_click_ele.attr("record-trackno"),
		                                "record_id": dis_row_click_ele.attr("record-id")
		                            }
		                            var embed_dialog_type = embed_view_cont.attr('embed-dialog-type');

		                            var ret =
		                                    '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
		                                    '<div class="hr"></div>';
		                            if (embeded_attr == "view") {
		                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
		                            } else {
		                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
		                            }
		                            if(embed_dialog_type == "2"){
		                            	var existingWindow = false; //FS#7646
		                            	if(embed_view_cont.is(":data('embedDialog')")){ //FS#7646
		                            	    if( embed_view_cont.data("embedDialog").closed == false ){
		                            	        existingWindow = true;
		                            	        embed_view_cont.data("embedDialog").focus();
		                            	        // embed_view_cont.data("embedDialog").location.href = str_link;
		                            	        if( (embed_view_cont.data("embedDialog").location.pathname+embed_view_cont.data("embedDialog").location.search) != src){
	                            	        		embed_view_cont.data("embedDialog").location.href = src;
		                            	        }
		                            	        if(embed_view_cont.data("embedDialog").$('html').is('.view-embed-iframe') == false){ //this means the dialog window has been reloaded
		                            	        	embed_view_cont.data("embedDialog").$('html').addClass('view-embed-iframe');
		                            	        	embed_view_cont.data("embedDialog").onload = function ( ) {
				                            		    var iframe_document = embed_view_cont.data("embedDialog").$('html');
				                            		    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
				                            		    embedDialog.onbeforeunload = function (e) {
				                            		        if(iframe_document.find('.getFields[name]').filter(function(){ return embedDialog.$(this)[0].value != embedDialog.$(this)[0].defaultValue; }).length >= 1 ) return "You have changes made.";
				                            		    };
				                            		};
		                            	        }
		                            	    }
		                            	}
		                            	if(existingWindow == false){ //FS#7646
		                            		var embedDialog = window.open(src,'_blank','fullscreen=no');
		                            		embed_view_cont.data("embedDialog",embedDialog);
		                            		embedDialog.setTimeout(function(){
		                            		    embedDialog.addingViewEmbedIframe = setInterval(function(){
		                            		        embedDialog.document.getElementsByTagName('html')[0].className += " view-embed-iframe";
		                            		    },0);
		                            		},0);
		                            		embedDialog.fromParent = embedDialog;
		                            		embedDialog.onload = function ( ) {
		                            		    clearInterval(embedDialog.addingViewEmbedIframe);
		                            		    var iframe_document = embedDialog.$('html');
		                            		    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
		                            		    embedDialog.onbeforeunload = function (e) {
		                            		        if(iframe_document.find('.getFields[name]').filter(function(){ return embedDialog.$(this)[0].value != embedDialog.$(this)[0].defaultValue; }).length >= 1 ) return "You have changes made.";
		                            		    };
		                            		};
		                            	}
		                            }else{
		                        	    var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "", function () {
		                        	    });
		                        	    dis_dialog.themeDialog("modal2");
		                        	    var ele_dialog = $('#popup_container');
		                        	    //console.log('/user_view/workspace?view_type=request&formID='+formId+'&requestID='+row_click_data["record_id"]+'&trackNo='+row_click_data["record_trackno"]);
		                        	    
		                        	    var my_dialog = $(
		                        	            '<div class="embed-iframe" style="height: 100%;">' +
		                        	            '<iframe style="height:100%;width:100%;" src="' + src + '">' +
		                        	            '</iframe>' + '</div>'
		                        	            )

		                        	    ele_dialog.find("#popup_content").html(my_dialog);

		                        	    ele_dialog.find("#popup_content").css({
		                        	        "height": ($(window).outerHeight() - 120) + "px"
		                        	    });
		                        	    ele_dialog.css({
		                        	        "top": "30px"
		                        	    });
		                        	    ele_dialog.data("field_updater", fieldReference);
		                        	    ele_dialog.data("embed_container", parent_container_embed);
		                        	    var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
		                        	    var loading_panel = panel.load(ele_dialog.find("#popup_content"));
		                        	    loading_panel.parent().css("position", "relative");
		                        	    iframe_ele_dialog.load(function () {
		                        	        var iframe_document = $(this)[0].contentWindow.$('html');
		                        	        EmbedPopUpDisplayFixes.call(iframe_document);
		                        	        loading_panel.fadeOut();
		                        	        iframe_document.addClass('view-embed-iframe');
		                        	        if(embeded_attr == "view"){
		                        	            iframe_document.find('[data-type="embeded-view"]').css({"z-index":"9999"});
		                        	        }
		                        	    })
		                        	    // if(get_request_type.indexOf('pending') > -1){
		                        	        // embeded_view.createTemporaryData($(parent_container_embed), "insert", "popup");
		                        	    // }
		                        	    // else{
		                        		if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
		                        			embeded_view.createTemporaryData($(parent_container_embed), "update", "popup", reference_id);
		                        		}
		                        	    // }
		                        	    // embeded_view.getRequests(parent_container_embed.data("embed_view_reference"), function(data) {
		                        	    //     $(container).html(data);
		                        	    // });

		                        	    // var iframe_ele = my_dialog.find("iframe").eq(0);
		                        	    // iframe_ele.load(function(){
		                        	    //     var iframe_doc = $(iframe_ele).contents()[0];
		                        	    //     console.log("FRAAA",$(iframe_doc))
		                        	    // })

		                        	    // Roni Pinili close button modal configuration

		                        	    if ($('body').data("user_form_json_data")) {

		                        	        var embeddedViewObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

		                        	        if ($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]) {
		                        	            var embededViewActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
		                        	            console.log("VALUE", embededViewActionVisibilityVal);
		                        	            if (embededViewActionVisibilityVal == "Yes") {
		                        	                var close_dial = setInterval(function () {
		                        	                    $('.fl-closeDialog').hide();
		                        	                    if ($('.fl-closeDialog').length >= 1) {
		                        	                        clearInterval(close_dial);
		                        	                    }
		                        	                }, 0);
		                        	            }
		                        	        }

		                        	    }
		                            }

		                        }
		                        
		                    })
		                    obj_jq.find(".columnHeader").on("click", function () {//carlo
		                    	if($(this).closest('.embed-view-container').is('.embed-edit-mode')){
		                    		return;
		                    	}
		                    	if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
		                    		return;
		                    	}
		                    	self.embedrowsSorter.call(this, self);
		                    });
		                    console.log("HARRTEST")
		                    initializePage.exePrintDisplayFixes(parent_container_embed);
		                    initializePage.visibilityOnChange2(parent_container_embed);
		                    //trigger default sorting
		                    // if($(parent_container_embed).is('[data-default-sort-col]')){
		                    //     if($(parent_container_embed).attr('data-default-sort-col') != '--Selected--'){
		                    //        var sort_type = $(parent_container_embed).attr('data-default-sort-type');
		                    //        var sort_column = $(parent_container_embed).attr('data-default-sort-col');
		                    //        obj_jq.find(".columnHeader[data-fld-name='"+sort_column+"']").trigger("click");
		                    //     }
		                    // }
		                    if(parent_container_embed.attr('commit-data-row-event') == 'parent'){
		                    	embeded_view.tempoEmbeddedListRefresh(parent_container_embed);
		                    	embeded_view.embedHideDeletedRecords(parent_container_embed.closest('.setOBJ').attr('data-object-id'));
		                    }
		                    embeded_view.embedAdditionalDisplayFixes($(parent_container_embed));
		                    EmbeddedFixHeader.init(parent_container_embed);
		                    EmbeddedFixHeader.maintainCellWidth(parent_container_embed);
		                    EmbeddedFixHeader.reFixHeader(parent_container_embed);
		                });
		            }
		            if(!$(this).attr('embed-computed-formula') || ($(this).attr('embed-computed-formula') && $(this).attr('rfc-choice') == "static")){
			            $('[name="' + fieldReference + '"]').on("change.embedSearchReference",function () {
			                // fieldValue = $(this).val();
			                fieldValue = $(this).val();
			                fieldReferenceObjectName = $(container).attr('search-field-object-name');
			                if(fieldReferenceObjectName == 'pickList' || fieldReferenceObjectName == 'checkbox' || fieldReferenceObjectName == 'selectMany'){
			                	if(fieldReferenceObjectName == 'pickList'){
			                		fieldValue = $('[name="' + fieldReference + '"]').val().split('|');
			                	}
			                	else if(fieldReferenceObjectName == 'checkbox'){
			                		fieldValue = $('[name="' + fieldReference + '[]"]:checked').map(function(a, b){ return $.trim($(this).val()); });
			                	}
			                	else if(fieldReferenceObjectName == 'selectMany'){
			                		fieldValue = $('[name="' + fieldReference + '[]"]>option:selected').map(function(){ return $.trim($(this).val()); });
			                	}
			                }
			                else{
			                	fieldValue = $('[name="' + fieldReference + '"]').val();
			                }
			                var enable_embed_row_click = $(container).attr("enable-embed-row-click");
			                var embed_action_click_copy = $(container).attr('embed-action-click-copy');
			                var embed_action_click_delete = $(container).attr('embed-action-click-delete');
			                var embed_action_click_edit = $(container).attr('embed-action-click-edit');
			                var embed_action_click_view = $(container).attr('embed-action-click-view');
			                var embed_action_click_number = $(container).attr('embed-action-click-number');
			                var embed_action_click_edit_popup = $(container).attr('embed-action-click-edit-popup');

			                var fieldFilterConditionalOperator = $(container).attr('embed-conditional-operator');
			                if ($.type(fieldFilterConditionalOperator) == "undefined") {
			                    fieldFilterConditionalOperator = "=";
			                } else if (fieldFilterConditionalOperator == "") {
			                    fieldFilterConditionalOperator = "=";
			                }
			                var embedAdditionalFilterFormula = $(container).attr('embed-additional-filter-formula');
			                if ($.type(embedAdditionalFilterFormula) == "undefined") {
			                    embedAdditionalFilterFormula = "";
			                }
			                var fieldReferenceType = $(container).attr('rfc-choice');
			                if (fieldReferenceType == 'computed') {
			                    var thisFormula = $(container).attr('embed-computed-formula');
			                    var formulaDoc = new Formula(thisFormula);
			                    fieldValue = formulaDoc.getEvaluation();
			                }
			                var embed_row_catgory = "";
			                if($('body').data('user_form_json_data')['form_json'][""+do_id]['allow_row_category'] == true){
			                	embed_row_catgory = $('body').data('user_form_json_data')['form_json'][""+do_id]['allow_row_category_column']||"";
			                }
			                reference = {
			                    FormID: formId,
			                    FieldReferece: fieldReference,
			                    FieldValue: fieldValue,
			                    FieldFilter: fieldFilter,
			                    "field_conditional_operator": fieldFilterConditionalOperator,
			                    "embed_additional_filter_formula": self.parseCondition(embedAdditionalFilterFormula),
			                    HLData: "",
			                    HLType: "row",
			                    HLAllow: "false",
			                    column_data: "",
			                    current_form_fields_data: [],
			                    "enable_embed_row_click": enable_embed_row_click,
			                    "embed_action_click_copy": embed_action_click_copy,
			                    "embed_action_click_delete": embed_action_click_delete,
			                    "embed_action_click_edit": embed_action_click_edit,
			                    "embed_action_click_view": embed_action_click_view,
			                    "embed_action_click_number": embed_action_click_number,
			                    "embed_action_click_edit_popup": embed_action_click_edit_popup,
			                    "embed_default_sorting": {"column": "ID", "type": "ASC"},
			                    "embed_row_category": embed_row_catgory
			                };
			                reference['search_field_obj_name'] = fieldReferenceObjectName;
			                if ($(container).is('[data-default-sort-col]')) {
			                    if ($(container).attr('data-default-sort-col') != '--Selected--') {
			                        var sort_type = $(container).attr('data-default-sort-type');
			                        var sort_column = $(container).attr('data-default-sort-col');
			                        reference['embed_default_sorting']['column'] = sort_column;
			                        reference['embed_default_sorting']['type'] = sort_type;
			                    }
			                }
			                if (typeof $(container).attr('embed-hl-data') != "undefined") {
			                    reference["HLData"] = $(container).attr('embed-hl-data');
			                }
			                if (typeof $(container).attr('allow-highlights') != "undefined") {
			                    reference["HLAllow"] = $(container).attr('allow-highlights');
			                }
			                if (typeof $(container).attr('allow-highlights') != "undefined") {
			                    reference["HLType"] = $(container).attr('highlight-type');
			                }
			                if (typeof $(container).attr('embed-column-data') != "undefined") {
			                    reference["column_data"] = $(container).attr('embed-column-data');
			                }

			                $('#frmrequest').find(".setOBJ").each(function (eqi) {
			                    var data_object_id = $(this).attr("data-object-id");
			                    var getFIELDS = $(this).find(".getFields_" + data_object_id);
			                    var field_name = $(this).find(".getFields_" + data_object_id).eq(0).attr("name");
			                    field_name_values = [];
			                    getFIELDS.each(function (eqi2) {
			                        field_name_values.push($(this).val());
			                    });
			                    field_name_values = field_name_values.join("|^|");
			                    reference["current_form_fields_data"].push({
			                        "f_name": field_name,
			                        "values": field_name_values
			                    })
			                })
			                reference["current_form_fields_data"] = JSON.stringify(reference["current_form_fields_data"]);
			                $(container).html('<center>' + '<div class="spinner load-app-request" style="margin-top:30px;">' +
			                        '<div class="bar1"></div>' +
			                        '<div class="bar2"></div>' +
			                        '<div class="bar3"></div>' +
			                        '<div class="bar4"></div>' +
			                        '<div class="bar5"></div>' +
			                        '<div class="bar6"></div>' +
			                        '<div class="bar7"></div>' +
			                        '<div class="bar8"></div>' +
			                        '<div class="bar9"></div>' +
			                        '<div class="bar10"></div>' +
			                        '</div>' + '</center>');
			                // $(container).data("embed_view_reference",reference);
			                // console.log("update embed reference", reference)

			                reference["embed_container_id"] = $(container).parents('.setOBJ[data-object-id]:eq(0)').attr('data-object-id');
			                self.getRequests(reference, function (data) {
			                    console.log(data);
			                    var obj_jq = $(data)
			                    var parent_container_embed = $(container)
			                    //added by japhet
			                    if(parent_container_embed.attr('commit-data-row-event') == "parent"){
			                    	parent_container_embed.find('th').css({'cursor': 'default'})
			                    }

			                    parent_container_embed.html(obj_jq);
			                    applyMultipleSelection(parent_container_embed, do_id);
			                    if(embed_row_catgory != ""){
			                    	categorizeRow(embedded_view_object);
			                    }
			                    var rows_element = obj_jq.find("tr").filter(":not(:eq(0))").find(".viewTDEmbedded");

			                    obj_jq.find("tr").filter(":not(:eq(0))").addClass("hovereffect");
			                    // console.log("KLASSS",obj_jq.attr("author-valid"))
			                    if (obj_jq.attr("author-valid")) {

			                        if (obj_jq.attr("author-valid") == "true") {

			                        } else {
			                            parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
			                        }
			                    } else {
			                        parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
			                    }
			                    self.getSetTotalOfColumn.call(parent_container_embed, obj_jq);
			                    rows_element.each(function(){ //FS#7646
			                    	var dis_row_click_ele = $(this).parents("tr").eq(0);
			                    	var row_click_data = {
			                    	    "record_trackno": dis_row_click_ele.attr("record-trackno"),
			                    	    "record_id": dis_row_click_ele.attr("record-id"),
			                    	    "formId": dis_row_click_ele.attr('form-id')
			                    	}
			                    	if ($(this).attr("data-action-attr") == "view") {
			                    	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
			                    	} else {
			                    	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
			                    	}
			                    	$(this).attr("data-embed-href-link",src); //FS#7646
			                    });
			                    rows_element.on({
			                        "click": function () {
			                            var dis_ele = $(this);
			                            var embed_view_cont = parent_container_embed||dis_ele.parent().find(".embed-view-container").eq(0);
			                            var embeded_attr = $(this).attr("data-action-attr");
			                            var reference_id = $(this).parents('tr').eq(0).attr('reference-id')||$(this).parents('.embed-view-container').eq(0).attr('embed-source-form-val-id') + '_' + $(this).parents('tr').eq(0).attr('record-id');
			                            var allow_row_click = $(container).attr("enable-embed-row-click")
			                            if (allow_row_click) {
			                                if (allow_row_click != "true") {
			                                    return;
			                                }
			                            } else {
			                                return;
			                            }
			                            var dis_row_click_ele = $(this).parents("tr").eq(0);
			                            var row_click_data = {
			                                "record_trackno": dis_row_click_ele.attr("record-trackno"),
			                                "record_id": dis_row_click_ele.attr("record-id")
			                            }
			                            if (embeded_attr == "view") {
			                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
			                            } else {

			                                $('.fl-closeDialog').addClass('close-confirmation');
			                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
			                            }
			                            if(embed_view_cont.attr('embed-dialog-type') == "2"){
			                            	var existingWindow = false; //FS#7646
			                            	if(embed_view_cont.is(":data('embedDialog')")){ //FS#7646
			                            	    if( embed_view_cont.data("embedDialog").closed == false ){
			                            	        existingWindow = true;
			                            	        embed_view_cont.data("embedDialog").focus();
			                            	        // embed_view_cont.data("embedDialog").location.href = str_link;
			                            	        if( (embed_view_cont.data("embedDialog").location.pathname+embed_view_cont.data("embedDialog").location.search) != src){
		                            	        		embed_view_cont.data("embedDialog").location.href = src;
			                            	        }
			                            	        if(embed_view_cont.data("embedDialog").$('html').is('.view-embed-iframe') == false){ //this means the dialog window has been reloaded
			                            	        	embed_view_cont.data("embedDialog").$('html').addClass('view-embed-iframe');
			                            	        }
			                            	    }
			                            	}
			                            	if(existingWindow == false){ //FS#7646
		                            			var embedDialog = window.open(src,'_blank','fullscreen=no');
		                            			embed_view_cont.data("embedDialog",embedDialog);
		                            			embedDialog.setTimeout(function(){
		                            			    embedDialog.addingViewEmbedIframe = setInterval(function(){
		                            			        if(embedDialog.document.getElementsByTagName('html').length >= 1){
		                            			            embedDialog.document.getElementsByTagName('html')[0].className += " view-embed-iframe";
		                            			        }
		                            			    },0);
		                            			},0);
		                            			embedDialog.fromParent= true;
		                            			embedDialog.field_updater =  function(){
		                            		        $('[name="'+fieldReference+'"]').trigger('change');
		                            		    };
		                            			embedDialog.onload = function ( ) {
		                            			    clearInterval(embedDialog.addingViewEmbedIframe);
		                            			    var iframe_document = embedDialog.$('html');
		                            			    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
		                            			    embedDialog.onbeforeunload = null;
		                            			    EmbedPopUpDisplayFixes.call(iframe_document);
		                            			    iframe_document.addClass('view-embed-iframe');
		                            			};
			                            	}
			                            	
			                            }else{
			                            	var ret =
			                            	        '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
			                            	        '<div class="hr"></div>';
			                            	var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "", function () {
			                            	});
			                            	dis_dialog.themeDialog("modal2");
			                            	console.log("check natin row data", row_click_data);
			                            	var ele_dialog = $('#popup_container');
			                            	
			                            	var my_dialog = $(
			                            	        '<div class="embed-iframe" style="height: 100%;">' +
			                            	        '<iframe style="height:100%;width:100%;" src="' + src + '">' +
			                            	        '</iframe>' +
			                            	        '</div>'
			                            	        )
			                            	console.log("embed_view_cont", embed_view_cont)
			                            	$('html').data("embed_container", embed_view_cont);

			                            	ele_dialog.find("#popup_content").html(my_dialog);
			                            	ele_dialog.find("#popup_content").css({
			                            	    "height": ($(window).outerHeight() - 120) + "px"
			                            	});

			                            	ele_dialog.css({
			                            	    "top": "30px"
			                            	});
			                            	ele_dialog.data("field_updater", fieldReference);
			                            	ele_dialog.data("embed_container", parent_container_embed);
			                            	var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
			                            	var loading_panel = panel.load(ele_dialog.find("#popup_content"));
			                            	loading_panel.parent().css("position", "relative");
			                            	iframe_ele_dialog.load(function () {
			                            	    var iframe_document = $(this)[0].contentWindow.$('html');
			                            	    EmbedPopUpDisplayFixes.call(iframe_document);
			                            	    loading_panel.fadeOut();
			                            	    iframe_document.addClass('view-embed-iframe');
			                            	    if(embeded_attr == "view"){
			                            	        iframe_document.find('[data-type="embeded-view"]').css({"z-index":"9999"});
			                            	    }
			                            	});
			                            if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
			                            	embeded_view.createTemporaryData($(parent_container_embed), "update", "popup", reference_id);
			                            }
			                            // embeded_view.getRequests(parent_container_embed.data("embed_view_reference"), function(data) {
			                            //     $(container).html(data);
			                            // });

			                            	// var iframe_ele = my_dialog.find("iframe").eq(0);
			                            	// iframe_ele.load(function(){
			                            	//     var iframe_doc = $(iframe_ele).contents()[0];
			                            	//     console.log("FRAAA",$(iframe_doc))
			                            	// })
			                            	// Roni Pinili close button modal configuration

			                            	if ($('body').data("user_form_json_data")) {

			                            	    var embeddedViewObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

			                            	    if ($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]) {
			                            	        var embededViewActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
			                            	        console.log("VALUE", embededViewActionVisibilityVal);
			                            	        if (embededViewActionVisibilityVal == "Yes") {
			                            	            var close_dial = setInterval(function () {
			                            	                $('.fl-closeDialog').hide();
			                            	                if ($('.fl-closeDialog').length >= 1) {
			                            	                    clearInterval(close_dial);
			                            	                }
			                            	            }, 0);
			                            	        }
			                            	    }

			                            	}
			                            }
			                        }
			                    });
			                    $(".dataTip").tooltip();
			                    //test sorter

			                    obj_jq.find(".columnHeader").on("click", function () {//carlo
			                    	if($(this).closest('.embed-view-container').is('.embed-edit-mode')){
			                    		return;
			                    	}
			                    	// console.log("columnheader", $(this).parent());
			                    	if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
			                    		return;
			                    	}
			                    	self.embedrowsSorter.call(this, self);
			                    });
			                    initializePage.visibilityOnChange2(parent_container_embed);
			                    //trigger default sorting
			                    // if($(parent_container_embed).is('[data-default-sort-col]')){
			                    //     if($(parent_container_embed).attr('data-default-sort-col') != '--Selected--'){
			                    //        var sort_type = $(parent_container_embed).attr('data-default-sort-type');
			                    //        var sort_column = $(parent_container_embed).attr('data-default-sort-col');
			                    //        obj_jq.find(".columnHeader[data-fld-name='"+sort_column+"']").trigger("click");
			                    //     }
			                    // }
			                    if(parent_container_embed.attr('commit-data-row-event') == "parent"){
				                    embeded_view.tempoEmbeddedListRefresh(parent_container_embed, true);
				                    embeded_view.embedHideDeletedRecords(parent_container_embed.closest('.setOBJ').attr('data-object-id'));
				                }
				                //added by japhet morada, for fix header in embedded view
				                embeded_view.embedAdditionalDisplayFixes($(parent_container_embed));
				                EmbeddedFixHeader.init(parent_container_embed);
				                EmbeddedFixHeader.maintainCellWidth(parent_container_embed);
				                EmbeddedFixHeader.reFixHeader(parent_container_embed);
			                });

			            });
		            }
			        //for embedded view refresh if import is enable
		            if($(this).attr('embed-allow-import') && $(this).attr('embed-allow-import') == "true"){
		            	var embedded_view_object = $(this);
		            	$(this).parents('.setOBJ').find('.refresh_embed_view').on('click', function(e){
		            		embeded_view.refreshEmbed(embedded_view_object);
		            	});
		            }
					console.log($(this).attr('embed-name'));
					// EmbeddedFixHeader.init($(this));
					// EmbeddedFixHeader.maintainCellWidth($(this));
		            // $(this).perfectScrollbar("refresh")
        		}
        	}
        	else{
	        	var do_id = $(this).parents('.setOBJ[data-type="embeded-view"][data-object-id]:eq(0)').attr('data-object-id');
	            var formId = $(this).attr('embed-source-form-val-id');
	            var fieldReference = $(this).attr('embed-result-field-val');
	            var fieldValue;
	            var fieldReferenceObjectName = $(this).attr('search-field-object-name');
	            if(fieldReferenceObjectName == 'pickList' || fieldReferenceObjectName == 'checkbox' || fieldReferenceObjectName == 'selectMany'){
	            	if(fieldReferenceObjectName == 'pickList'){
	            		fieldValue = $('[name="' + fieldReference + '"]').val().split('|');
	            	}
	            	else if(fieldReferenceObjectName == 'checkbox'){
	            		fieldValue = $('[name="' + fieldReference + '[]"]:checked').map(function(a, b){ return $.trim($(this).val()); });
	            	}
	            	else if(fieldReferenceObjectName == 'selectMany'){
	            		fieldValue = $('[name="' + fieldReference + '[]"]>option:selected').map(function(){ return $.trim($(this).val()); });
	            	}
	            }
	            else{
	            	fieldValue = $('[name="' + fieldReference + '"]').val();
	            }
	            
	            var fieldFilter = $(this).attr('embed-source-lookup-active-field-val');
	            var fieldFilterConditionalOperator = $(this).attr('embed-conditional-operator');
	            
	            if ($.type(fieldFilterConditionalOperator) == "undefined") {
	                fieldFilterConditionalOperator = "=";
	            } else if (fieldFilterConditionalOperator == "") {
	                fieldFilterConditionalOperator = "=";
	            }
	            var embedAdditionalFilterFormula = $(this).attr('embed-additional-filter-formula');
	            if ($.type(embedAdditionalFilterFormula) == "undefined") {
	                embedAdditionalFilterFormula = "";
	            }
	            var fieldReferenceType = $(this).attr('rfc-choice');
	            var enable_embed_row_click = $(this).attr("enable-embed-row-click");
	            var embed_action_click_copy = $(this).attr('embed-action-click-copy');
	            var embed_action_click_delete = $(this).attr('embed-action-click-delete');
	            var embed_action_click_edit = $(this).attr('embed-action-click-edit');
	            var embed_action_click_view = $(this).attr('embed-action-click-view');
	            var embed_action_click_number = $(this).attr('embed-action-click-number');
	            var embed_action_click_edit_popup = $(this).attr('embed-action-click-edit-popup');
	            if (fieldReferenceType == 'computed') {
	                var thisFormula = $(this).attr('embed-computed-formula');
	                var formulaDoc = new Formula(thisFormula);
	                fieldValue = formulaDoc.getEvaluation();
	                embeded_view.embedSetupComputedSearch($(this));
	            }

	            var container = this;
	            if (fieldValue !== null && fieldValue !== 'null') {
	                var reference = {
	                    FormID: formId,
	                    FieldReference: fieldReference,
	                    FieldValue: fieldValue,
	                    FieldFilter: fieldFilter,
	                    "field_conditional_operator": fieldFilterConditionalOperator,
	                    "embed_additional_filter_formula": self.parseCondition(embedAdditionalFilterFormula),
	                    HLData: "",
	                    HLType: "row",
	                    HLAllow: "false",
	                    column_data: "",
	                    current_form_fields_data: [],
	                    "enable_embed_row_click": enable_embed_row_click,
	                    "embed_action_click_copy": embed_action_click_copy,
	                    "embed_action_click_delete": embed_action_click_delete,
	                    "embed_action_click_edit": embed_action_click_edit,
	                    "embed_action_click_view": embed_action_click_view,
	                    "embed_action_click_number": embed_action_click_number,
	                    "embed_action_click_edit_popup": embed_action_click_edit_popup,
	                    "embed_default_sorting": {"column": "ID", "type": "ASC"}

	                };
	                if ($(container).is('[data-default-sort-col]')) {
	                    if ($(container).attr('data-default-sort-col') != '--Selected--') {
	                        var sort_type = $(container).attr('data-default-sort-type');
	                        var sort_column = $(container).attr('data-default-sort-col');
	                        reference['embed_default_sorting']['column'] = sort_column;
	                        reference['embed_default_sorting']['type'] = sort_type;
	                    }
	                }
	                if (typeof $(container).attr('embed-hl-data') != "undefined") {
	                    reference["HLData"] = $(container).attr('embed-hl-data');
	                }
	                if (typeof $(container).attr('allow-highlights') != "undefined") {
	                    reference["HLAllow"] = $(container).attr('allow-highlights');
	                }
	                if (typeof $(container).attr('allow-highlights') != "undefined") {
	                    reference["HLType"] = $(container).attr('highlight-type');
	                }
	                if (typeof $(container).attr('embed-column-data') != "undefined") {
	                    reference["column_data"] = $(container).attr('embed-column-data');
	                }
	                if($(this).attr('commit-data-row-event') == "parent"){
	                	$(this).addClass('embed-edit-mode');
	                }
	                reference['search_field_obj_name'] = fieldReferenceObjectName;
	                $('#frmrequest').find(".setOBJ").each(function (eqi) {
	                    var data_object_id = $(this).attr("data-object-id");
	                    var getFIELDS = $(this).find(".getFields_" + data_object_id);
	                    var field_name = $(this).find(".getFields_" + data_object_id).eq(0).attr("name");
	                    field_name_values = [];
	                    getFIELDS.each(function (eqi2) {
	                        field_name_values.push($(this).val());
	                    })
	                    field_name_values = field_name_values.join("|^|");
	                    reference["current_form_fields_data"].push({
	                        "f_name": field_name,
	                        "values": field_name_values
	                    });
	                });
	                reference["current_form_fields_data"] = JSON.stringify(reference["current_form_fields_data"]);
	                $(container).html('<center>' + '<div class="spinner display load-app-request" style="margin-top:30px;">' +
	                        '<div class="bar1"></div>' +
	                        '<div class="bar2"></div>' +
	                        '<div class="bar3"></div>' +
	                        '<div class="bar4"></div>' +
	                        '<div class="bar5"></div>' +
	                        '<div class="bar6"></div>' +
	                        '<div class="bar7"></div>' +
	                        '<div class="bar8"></div>' +
	                        '<div class="bar9"></div>' +
	                        '<div class="bar10"></div>' +
	                        '</div>' + '</center>');


	                $(container).data("embed_reference", reference);
	                reference["embed_container_id"] = do_id;
	                self.getRequests(reference, function (data) {
	                    var obj_jq = $(data);
	                    var parent_container_embed = $(container)
	                    parent_container_embed.html(obj_jq);
	                    var rows_element = obj_jq.find("tr").filter(":not(:eq(0))").find(".viewTDEmbedded");

	                    obj_jq.find("tr").filter(":not(:eq(0))").addClass("hovereffect");

	                    if (obj_jq.attr("author-valid")) {

	                        if (obj_jq.attr("author-valid") == "true") {

	                        } else {
	                            parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
	                        }
	                    } else {
	                        parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
	                    }
	                    //console.log("rows_element",rows_element)
	                    self.getSetTotalOfColumn.call(parent_container_embed, obj_jq);
	                    //self.embedBindHeaderSorting(obj_jq);
	                    rows_element.each(function(){ //FS#7646
	                    	var dis_row_click_ele = $(this).parents("tr").eq(0);
	                    	var row_click_data = {
	                    	    "record_trackno": dis_row_click_ele.attr("record-trackno"),
	                    	    "record_id": dis_row_click_ele.attr("record-id"),
	                    	    "formId": dis_row_click_ele.attr('form-id')
	                    	}
	                    	if ($(this).attr("data-action-attr") == "view") {
	                    	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
	                    	} else {
	                    	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
	                    	}
	                    	$(this).attr("data-embed-href-link",src); //FS#7646
	                    });
	                    rows_element.on({
	                        "click": function () {

	                            var dis_ele = $(this);
	                            var embed_view_cont = dis_ele.parents(".embed-view-container").eq(0);
	                            $('html').data("embed_container", embed_view_cont);
	                            var embeded_attr = $(this).attr("data-action-attr");
	                            var reference_id = dis_ele.parents('tr').attr('reference-id')||dis_ele.parents('.embed-view-container').attr('embed-source-form-val-id') + '_' + dis_ele.parents('tr').attr('record-id');
	                            var allow_row_click = $(container).attr("enable-embed-row-click")
	                            if (allow_row_click) {
	                                if (allow_row_click != "true") {
	                                    return;
	                                }
	                            } else {
	                                return;
	                            }
	                            var dis_row_click_ele = $(this).parents("tr").eq(0);
	                            var row_click_data = {
	                                "record_trackno": dis_row_click_ele.attr("record-trackno"),
	                                "record_id": dis_row_click_ele.attr("record-id")
	                            }
	                            var embed_dialog_type = embed_view_cont.attr('embed-dialog-type');

	                            var ret =
	                                    '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
	                                    '<div class="hr"></div>';
	                            if (embeded_attr == "view") {
	                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
	                            } else {
	                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
	                            }
	                            if(embed_dialog_type == "2"){
	                            	var existingWindow = false; //FS#7646
	                            	if(embed_view_cont.is(":data('embedDialog')")){ //FS#7646
	                            	    if( embed_view_cont.data("embedDialog").closed == false ){
	                            	        existingWindow = true;
	                            	        embed_view_cont.data("embedDialog").focus();
	                            	        // embed_view_cont.data("embedDialog").location.href = str_link;
	                            	        if( (embed_view_cont.data("embedDialog").location.pathname+embed_view_cont.data("embedDialog").location.search) != src){
                            	        		embed_view_cont.data("embedDialog").location.href = src;
	                            	        }
	                            	        if(embed_view_cont.data("embedDialog").$('html').is('.view-embed-iframe') == false){ //this means the dialog window has been reloaded
	                            	        	embed_view_cont.data("embedDialog").$('html').addClass('view-embed-iframe');
	                            	        }
	                            	    }
	                            	}
	                            	if(existingWindow == false){ //FS#7646
	                            		var embedDialog = window.open(src,'_blank','fullscreen=no');
	                            		embed_view_cont.data("embedDialog",embedDialog);
	                            		embedDialog.setTimeout(function(){
	                            		    embedDialog.addingViewEmbedIframe = setInterval(function(){
	                            		        embedDialog.document.getElementsByTagName('html')[0].className += " view-embed-iframe";
	                            		    },0);
	                            		},0);
	                            		embedDialog.fromParent = embedDialog;
	                            		embedDialog.onload = function ( ) {
	                            		    clearInterval(embedDialog.addingViewEmbedIframe);
	                            		    var iframe_document = embedDialog.$('html');
	                            		    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
	                            		    embedDialog.onbeforeunload = function (e) {
	                            		        if(iframe_document.find('.getFields[name]').filter(function(){ return embedDialog.$(this)[0].value != embedDialog.$(this)[0].defaultValue; }).length >= 1 ) return "You have changes made.";
	                            		    };
	                            		};
	                            	}
	                            	
	                            }else{
	                        	    var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "", function () {
	                        	    });
	                        	    dis_dialog.themeDialog("modal2");
	                        	    var ele_dialog = $('#popup_container');
	                        	    //console.log('/user_view/workspace?view_type=request&formID='+formId+'&requestID='+row_click_data["record_id"]+'&trackNo='+row_click_data["record_trackno"]);
	                        	    
	                        	    var my_dialog = $(
	                        	            '<div class="embed-iframe" style="height: 100%;">' +
	                        	            '<iframe style="height:100%;width:100%;" src="' + src + '">' +
	                        	            '</iframe>' + '</div>'
	                        	            )

	                        	    ele_dialog.find("#popup_content").html(my_dialog);

	                        	    ele_dialog.find("#popup_content").css({
	                        	        "height": ($(window).outerHeight() - 120) + "px"
	                        	    });
	                        	    ele_dialog.css({
	                        	        "top": "30px"
	                        	    });
	                        	    ele_dialog.data("field_updater", fieldReference);
	                        	    ele_dialog.data("embed_container", parent_container_embed);
	                        	    var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
	                        	    var loading_panel = panel.load(ele_dialog.find("#popup_content"));
	                        	    loading_panel.parent().css("position", "relative");
	                        	    iframe_ele_dialog.load(function () {
	                        	        var iframe_document = $(this)[0].contentWindow.$('html');
	                        	        EmbedPopUpDisplayFixes.call(iframe_document);
	                        	        loading_panel.fadeOut();
	                        	        iframe_document.addClass('view-embed-iframe');
	                        	        if(embeded_attr == "view"){
	                        	            iframe_document.find('[data-type="embeded-view"]').css({"z-index":"9999"});
	                        	        }
	                        	    })
	                        	    // if(get_request_type.indexOf('pending') > -1){
	                        	        // embeded_view.createTemporaryData($(parent_container_embed), "insert", "popup");
	                        	    // }
	                        	    // else{
	                        		if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
	                        			embeded_view.createTemporaryData($(parent_container_embed), "update", "popup", reference_id);
	                        		}
	                        	    // }
	                        	    // embeded_view.getRequests(parent_container_embed.data("embed_view_reference"), function(data) {
	                        	    //     $(container).html(data);
	                        	    // });

	                        	    // var iframe_ele = my_dialog.find("iframe").eq(0);
	                        	    // iframe_ele.load(function(){
	                        	    //     var iframe_doc = $(iframe_ele).contents()[0];
	                        	    //     console.log("FRAAA",$(iframe_doc))
	                        	    // })

	                        	    // Roni Pinili close button modal configuration

	                        	    if ($('body').data("user_form_json_data")) {

	                        	        var embeddedViewObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

	                        	        if ($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]) {
	                        	            var embededViewActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
	                        	            console.log("VALUE", embededViewActionVisibilityVal);
	                        	            if (embededViewActionVisibilityVal == "Yes") {
	                        	                var close_dial = setInterval(function () {
	                        	                    $('.fl-closeDialog').hide();
	                        	                    if ($('.fl-closeDialog').length >= 1) {
	                        	                        clearInterval(close_dial);
	                        	                    }
	                        	                }, 0);
	                        	            }
	                        	        }

	                        	    }
	                            }

	                        }
	                        
	                    })
	                    obj_jq.find(".columnHeader").on("click", function () {//carlo
	                    	if($(this).closest('.embed-view-container').is('.embed-edit-mode')){
	                    		return;
	                    	}
	                    	if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
	                    		return;
	                    	}
	                    	self.embedrowsSorter.call(this, self);
	                    });
	                    console.log("HARRTEST")
	                    initializePage.exePrintDisplayFixes(parent_container_embed);
	                    initializePage.visibilityOnChange2(parent_container_embed);
	                    //trigger default sorting
	                    // if($(parent_container_embed).is('[data-default-sort-col]')){
	                    //     if($(parent_container_embed).attr('data-default-sort-col') != '--Selected--'){
	                    //        var sort_type = $(parent_container_embed).attr('data-default-sort-type');
	                    //        var sort_column = $(parent_container_embed).attr('data-default-sort-col');
	                    //        obj_jq.find(".columnHeader[data-fld-name='"+sort_column+"']").trigger("click");
	                    //     }
	                    // }
	                    if(parent_container_embed.attr('commit-data-row-event') == 'parent'){
	                    	embeded_view.tempoEmbeddedListRefresh(parent_container_embed);
	                    	embeded_view.embedHideDeletedRecords(parent_container_embed.closest('.setOBJ').attr('data-object-id'));
	                    }
	                    embeded_view.embedAdditionalDisplayFixes($(parent_container_embed));
	                });
	            }
	            if(!$(this).attr('embed-computed-formula') || ($(this).attr('embed-computed-formula') && $(this).attr('rfc-choice') == "static")){
		            $('[name="' + fieldReference + '"]').on("change.embedSearchReference",function () {
		                // fieldValue = $(this).val();
		                fieldValue = $(this).val();
		                fieldReferenceObjectName = $(container).attr('search-field-object-name');
		                if(fieldReferenceObjectName == 'pickList' || fieldReferenceObjectName == 'checkbox' || fieldReferenceObjectName == 'selectMany'){
		                	if(fieldReferenceObjectName == 'pickList'){
		                		fieldValue = $('[name="' + fieldReference + '"]').val().split('|');
		                	}
		                	else if(fieldReferenceObjectName == 'checkbox'){
		                		fieldValue = $('[name="' + fieldReference + '[]"]:checked').map(function(a, b){ return $.trim($(this).val()); });
		                	}
		                	else if(fieldReferenceObjectName == 'selectMany'){
		                		fieldValue = $('[name="' + fieldReference + '[]"]>option:selected').map(function(){ return $.trim($(this).val()); });
		                	}
		                }
		                else{
		                	fieldValue = $('[name="' + fieldReference + '"]').val();
		                }
		                var enable_embed_row_click = $(container).attr("enable-embed-row-click");
		                var embed_action_click_copy = $(container).attr('embed-action-click-copy');
		                var embed_action_click_delete = $(container).attr('embed-action-click-delete');
		                var embed_action_click_edit = $(container).attr('embed-action-click-edit');
		                var embed_action_click_view = $(container).attr('embed-action-click-view');
		                var embed_action_click_number = $(container).attr('embed-action-click-number');
		                var embed_action_click_edit_popup = $(container).attr('embed-action-click-edit-popup');

		                var fieldFilterConditionalOperator = $(container).attr('embed-conditional-operator');
		                if ($.type(fieldFilterConditionalOperator) == "undefined") {
		                    fieldFilterConditionalOperator = "=";
		                } else if (fieldFilterConditionalOperator == "") {
		                    fieldFilterConditionalOperator = "=";
		                }
		                var embedAdditionalFilterFormula = $(container).attr('embed-additional-filter-formula');
		                if ($.type(embedAdditionalFilterFormula) == "undefined") {
		                    embedAdditionalFilterFormula = "";
		                }
		                var fieldReferenceType = $(container).attr('rfc-choice');
		                if (fieldReferenceType == 'computed') {
		                    var thisFormula = $(container).attr('embed-computed-formula');
		                    var formulaDoc = new Formula(thisFormula);
		                    fieldValue = formulaDoc.getEvaluation();
		                }

		                reference = {
		                    FormID: formId,
		                    FieldReferece: fieldReference,
		                    FieldValue: fieldValue,
		                    FieldFilter: fieldFilter,
		                    "field_conditional_operator": fieldFilterConditionalOperator,
		                    "embed_additional_filter_formula": self.parseCondition(embedAdditionalFilterFormula),
		                    HLData: "",
		                    HLType: "row",
		                    HLAllow: "false",
		                    column_data: "",
		                    current_form_fields_data: [],
		                    "enable_embed_row_click": enable_embed_row_click,
		                    "embed_action_click_copy": embed_action_click_copy,
		                    "embed_action_click_delete": embed_action_click_delete,
		                    "embed_action_click_edit": embed_action_click_edit,
		                    "embed_action_click_view": embed_action_click_view,
		                    "embed_action_click_number": embed_action_click_number,
		                    "embed_action_click_edit_popup": embed_action_click_edit_popup,
		                    "embed_default_sorting": {"column": "ID", "type": "ASC"}
		                };
		                reference['search_field_obj_name'] = fieldReferenceObjectName;
		                if ($(container).is('[data-default-sort-col]')) {
		                    if ($(container).attr('data-default-sort-col') != '--Selected--') {
		                        var sort_type = $(container).attr('data-default-sort-type');
		                        var sort_column = $(container).attr('data-default-sort-col');
		                        reference['embed_default_sorting']['column'] = sort_column;
		                        reference['embed_default_sorting']['type'] = sort_type;
		                    }
		                }
		                if (typeof $(container).attr('embed-hl-data') != "undefined") {
		                    reference["HLData"] = $(container).attr('embed-hl-data');
		                }
		                if (typeof $(container).attr('allow-highlights') != "undefined") {
		                    reference["HLAllow"] = $(container).attr('allow-highlights');
		                }
		                if (typeof $(container).attr('allow-highlights') != "undefined") {
		                    reference["HLType"] = $(container).attr('highlight-type');
		                }
		                if (typeof $(container).attr('embed-column-data') != "undefined") {
		                    reference["column_data"] = $(container).attr('embed-column-data');
		                }

		                $('#frmrequest').find(".setOBJ").each(function (eqi) {
		                    var data_object_id = $(this).attr("data-object-id");
		                    var getFIELDS = $(this).find(".getFields_" + data_object_id);
		                    var field_name = $(this).find(".getFields_" + data_object_id).eq(0).attr("name");
		                    field_name_values = [];
		                    getFIELDS.each(function (eqi2) {
		                        field_name_values.push($(this).val());
		                    });
		                    field_name_values = field_name_values.join("|^|");
		                    reference["current_form_fields_data"].push({
		                        "f_name": field_name,
		                        "values": field_name_values
		                    })
		                })
		                reference["current_form_fields_data"] = JSON.stringify(reference["current_form_fields_data"]);
		                $(container).html('<center>' + '<div class="spinner load-app-request" style="margin-top:30px;">' +
		                        '<div class="bar1"></div>' +
		                        '<div class="bar2"></div>' +
		                        '<div class="bar3"></div>' +
		                        '<div class="bar4"></div>' +
		                        '<div class="bar5"></div>' +
		                        '<div class="bar6"></div>' +
		                        '<div class="bar7"></div>' +
		                        '<div class="bar8"></div>' +
		                        '<div class="bar9"></div>' +
		                        '<div class="bar10"></div>' +
		                        '</div>' + '</center>');
		                // $(container).data("embed_view_reference",reference);
		                // console.log("update embed reference", reference)

		                reference["embed_container_id"] = $(container).parents('.setOBJ[data-object-id]:eq(0)').attr('data-object-id');
		                self.getRequests(reference, function (data) {
		                    console.log(data);
		                    var obj_jq = $(data)
		                    var parent_container_embed = $(container)
		                    //added by japhet
		                    if(parent_container_embed.attr('commit-data-row-event') == "parent"){
		                    	parent_container_embed.find('th').css({'cursor': 'default'})
		                    }

		                    parent_container_embed.html(obj_jq);

		                    var rows_element = obj_jq.find("tr").filter(":not(:eq(0))").find(".viewTDEmbedded");

		                    obj_jq.find("tr").filter(":not(:eq(0))").addClass("hovereffect");
		                    // console.log("KLASSS",obj_jq.attr("author-valid"))
		                    if (obj_jq.attr("author-valid")) {

		                        if (obj_jq.attr("author-valid") == "true") {

		                        } else {
		                            parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
		                        }
		                    } else {
		                        parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
		                    }
		                    self.getSetTotalOfColumn.call(parent_container_embed, obj_jq);
		                    rows_element.each(function(){ //FS#7646
		                    	var dis_row_click_ele = $(this).parents("tr").eq(0);
		                    	var row_click_data = {
		                    	    "record_trackno": dis_row_click_ele.attr("record-trackno"),
		                    	    "record_id": dis_row_click_ele.attr("record-id"),
		                    	    "formId": formId
		                    	}
		                    	if ($(this).attr("data-action-attr") == "view") {
		                    	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
		                    	} else {
		                    	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
		                    	}
		                    	$(this).attr("data-embed-href-link",src); //FS#7646
		                    });
		                    rows_element.on({
		                        "click": function () {
		                            var dis_ele = $(this);
		                            var embed_view_cont = parent_container_embed||dis_ele.parent().find(".embed-view-container").eq(0);
		                            var embeded_attr = $(this).attr("data-action-attr");
		                            var reference_id = $(this).parents('tr').eq(0).attr('reference-id')||$(this).parents('.embed-view-container').eq(0).attr('embed-source-form-val-id') + '_' + $(this).parents('tr').eq(0).attr('record-id');
		                            var allow_row_click = $(container).attr("enable-embed-row-click")
		                            if (allow_row_click) {
		                                if (allow_row_click != "true") {
		                                    return;
		                                }
		                            } else {
		                                return;
		                            }
		                            var dis_row_click_ele = $(this).parents("tr").eq(0);
		                            var row_click_data = {
		                                "record_trackno": dis_row_click_ele.attr("record-trackno"),
		                                "record_id": dis_row_click_ele.attr("record-id")
		                            }
		                            if (embeded_attr == "view") {
		                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
		                            } else {

		                                $('.fl-closeDialog').addClass('close-confirmation');
		                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
		                            }
		                            if(embed_view_cont.attr('embed-dialog-type') == "2"){
		                            	var existingWindow = false; //FS#7646
		                            	if(embed_view_cont.is(":data('embedDialog')")){ //FS#7646
		                            	    if( embed_view_cont.data("embedDialog").closed == false ){
		                            	        existingWindow = true;
		                            	        embed_view_cont.data("embedDialog").focus();
		                            	        // embed_view_cont.data("embedDialog").location.href = str_link;
		                            	        if( (embed_view_cont.data("embedDialog").location.pathname+embed_view_cont.data("embedDialog").location.search) != src){
	                            	        		embed_view_cont.data("embedDialog").location.href = src;
		                            	        }
		                            	        if(embed_view_cont.data("embedDialog").$('html').is('.view-embed-iframe') == false){ //this means the dialog window has been reloaded
		                            	        	embed_view_cont.data("embedDialog").$('html').addClass('view-embed-iframe');
		                            	        	embed_view_cont.data("embedDialog").onload = function ( ) {
		                            	        	    var iframe_document = embed_view_cont.data("embedDialog").$('html');
		                            	        	    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
		                            	        	    embedDialog.onbeforeunload = null;
		                            	        	    EmbedPopUpDisplayFixes.call(iframe_document);
		                            	        	    iframe_document.addClass('view-embed-iframe');
		                            	        	};
		                            	        }
		                            	    }
		                            	}
		                            	if(existingWindow == false){ //FS#7646
	                            			var embedDialog = window.open(src,'_blank','fullscreen=no');
	                            			embed_view_cont.data("embedDialog",embedDialog);
	                            			embedDialog.setTimeout(function(){
	                            			    embedDialog.addingViewEmbedIframe = setInterval(function(){
	                            			        if(embedDialog.document.getElementsByTagName('html').length >= 1){
	                            			            embedDialog.document.getElementsByTagName('html')[0].className += " view-embed-iframe";
	                            			        }
	                            			    },0);
	                            			},0);
	                            			embedDialog.fromParent= true;
	                            			embedDialog.field_updater =  function(){
	                            		        $('[name="'+fieldReference+'"]').trigger('change');
	                            		    };
	                            			embedDialog.onload = function ( ) {
	                            			    clearInterval(embedDialog.addingViewEmbedIframe);
	                            			    var iframe_document = embedDialog.$('html');
	                            			    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
	                            			    embedDialog.onbeforeunload = null;
	                            			    EmbedPopUpDisplayFixes.call(iframe_document);
	                            			    iframe_document.addClass('view-embed-iframe');
	                            			};
		                            	}
		                            }else{
		                            	var ret =
		                            	        '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
		                            	        '<div class="hr"></div>';
		                            	var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "", function () {
		                            	});
		                            	dis_dialog.themeDialog("modal2");
		                            	console.log("check natin row data", row_click_data);
		                            	var ele_dialog = $('#popup_container');
		                            	
		                            	var my_dialog = $(
		                            	        '<div class="embed-iframe" style="height: 100%;">' +
		                            	        '<iframe style="height:100%;width:100%;" src="' + src + '">' +
		                            	        '</iframe>' +
		                            	        '</div>'
		                            	        )
		                            	console.log("embed_view_cont", embed_view_cont)
		                            	$('html').data("embed_container", embed_view_cont);

		                            	ele_dialog.find("#popup_content").html(my_dialog);
		                            	ele_dialog.find("#popup_content").css({
		                            	    "height": ($(window).outerHeight() - 120) + "px"
		                            	});

		                            	ele_dialog.css({
		                            	    "top": "30px"
		                            	});
		                            	ele_dialog.data("field_updater", fieldReference);
		                            	ele_dialog.data("embed_container", parent_container_embed);
		                            	var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
		                            	var loading_panel = panel.load(ele_dialog.find("#popup_content"));
		                            	loading_panel.parent().css("position", "relative");
		                            	iframe_ele_dialog.load(function () {
		                            	    var iframe_document = $(this)[0].contentWindow.$('html');
		                            	    EmbedPopUpDisplayFixes.call(iframe_document);
		                            	    loading_panel.fadeOut();
		                            	    iframe_document.addClass('view-embed-iframe');
		                            	    if(embeded_attr == "view"){
		                            	        iframe_document.find('[data-type="embeded-view"]').css({"z-index":"9999"});
		                            	    }
		                            	});
		                            if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
		                            	embeded_view.createTemporaryData($(parent_container_embed), "update", "popup", reference_id);
		                            }
		                            // embeded_view.getRequests(parent_container_embed.data("embed_view_reference"), function(data) {
		                            //     $(container).html(data);
		                            // });

		                            	// var iframe_ele = my_dialog.find("iframe").eq(0);
		                            	// iframe_ele.load(function(){
		                            	//     var iframe_doc = $(iframe_ele).contents()[0];
		                            	//     console.log("FRAAA",$(iframe_doc))
		                            	// })
		                            	// Roni Pinili close button modal configuration

		                            	if ($('body').data("user_form_json_data")) {

		                            	    var embeddedViewObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

		                            	    if ($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]) {
		                            	        var embededViewActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
		                            	        console.log("VALUE", embededViewActionVisibilityVal);
		                            	        if (embededViewActionVisibilityVal == "Yes") {
		                            	            var close_dial = setInterval(function () {
		                            	                $('.fl-closeDialog').hide();
		                            	                if ($('.fl-closeDialog').length >= 1) {
		                            	                    clearInterval(close_dial);
		                            	                }
		                            	            }, 0);
		                            	        }
		                            	    }

		                            	}
		                            }
		                        }
		                    });
		                    $(".dataTip").tooltip();
		                    //test sorter

		                    obj_jq.find(".columnHeader").on("click", function () {//carlo
		                    	if($(this).closest('.embed-view-container').is('.embed-edit-mode')){
		                    		return;
		                    	}
		                    	// console.log("columnheader", $(this).parent());
		                    	if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
		                    		return;
		                    	}
		                    	self.embedrowsSorter.call(this, self);
		                    });
		                    initializePage.visibilityOnChange2(parent_container_embed);
		                    //trigger default sorting
		                    // if($(parent_container_embed).is('[data-default-sort-col]')){
		                    //     if($(parent_container_embed).attr('data-default-sort-col') != '--Selected--'){
		                    //        var sort_type = $(parent_container_embed).attr('data-default-sort-type');
		                    //        var sort_column = $(parent_container_embed).attr('data-default-sort-col');
		                    //        obj_jq.find(".columnHeader[data-fld-name='"+sort_column+"']").trigger("click");
		                    //     }
		                    // }
		                    if(parent_container_embed.attr('commit-data-row-event') == "parent"){
			                    embeded_view.tempoEmbeddedListRefresh(parent_container_embed, true);
			                    embeded_view.embedHideDeletedRecords(parent_container_embed.closest('.setOBJ').attr('data-object-id'));
			                }
			                //added by japhet morada, for fix header in embedded view
			                embeded_view.embedAdditionalDisplayFixes($(parent_container_embed));
			                EmbeddedFixHeader.init(parent_container_embed);
			                EmbeddedFixHeader.maintainCellWidth(parent_container_embed);
		                });

		            });
	            }
		        //for embedded view refresh if import is enable
	            if($(this).attr('embed-allow-import') && $(this).attr('embed-allow-import') == "true"){
	            	var embedded_view_object = $(this);
	            	$(this).parents('.setOBJ').find('.refresh_embed_view').on('click', function(e){
	            		embeded_view.refreshEmbed(embedded_view_object);
	            	});
	            }
				console.log($(this).attr('embed-name'));
				// EmbeddedFixHeader.init($(this));
				// EmbeddedFixHeader.maintainCellWidth($(this));
	            // $(this).perfectScrollbar("refresh")
        	}
        });
    },
    getRequests: function (reference, callback) {
        var pathname = window.location;
        var path_var = '?';

        //ADDED FOR ALVIN
    	var embed_contianer = $('.setOBJ[data-object-id="'+reference["embed_container_id"]+'"]').find('.embed-view-container:eq(0)');
    	if( embed_contianer.is(':not(:visible)') && embed_contianer.is(':not(:data("visible_refresh"))') ){
    		embed_contianer.data("visible_refresh",function(){
    			setTimeout(function(){
    				if(embed_contianer.is(':visible')){
    					embed_contianer.removeData("visible_refresh");
    					if(embed_contianer.is(':data("ajaxRequest")')){
    						embed_contianer.data('ajaxRequest').abort();
    						console_time.end('embeded_view loading');
    					}
    					console_time.start('embeded_view loading');
    					embed_contianer.data('ajaxRequest',$.post('/ajax/embedded_view' + path_var, reference, function (data) {
				            callback(data);
				            console_time.end('embeded_view loading');
				        }));
    					embed_contianer.data('ajaxRequest').fail(function (a, b, c) {
				            console.log("NAG ERROR UNG AJAX", " ", a, " ", b, " ", c);
				        });
    				}else{
    					embed_contianer.data("visible_refresh")();
    				}
    			},500);
    		});
    		embed_contianer.data("visible_refresh")();
    		return;
    	}else if(embed_contianer.is(':not(:visible)') && embed_contianer.is(':data("visible_refresh")') ){
    		embed_contianer.data("visible_refresh",function(){
    			setTimeout(function(){
    				if(embed_contianer.is(':visible')){
    					embed_contianer.removeData("visible_refresh");
    					if(embed_contianer.is(':data("ajaxRequest")')){
    						embed_contianer.data('ajaxRequest').abort();
    						console_time.end('embeded_view loading');
    					}
						console_time.start('embeded_view loading');
    					embed_contianer.data('ajaxRequest',$.post('/ajax/embedded_view' + path_var, reference, function (data) {
				            callback(data);
				            console_time.end('embeded_view loading');
				        }));
    					embed_contianer.data('ajaxRequest').fail(function (a, b, c) {
				            console.log("NAG ERROR UNG AJAX", " ", a, " ", b, " ", c);
				        });
    				}else{
    					embed_contianer.data("visible_refresh")();
    				}
    			},500);
    		});
    		return;
    	}

        if (getParametersName("embed_type", pathname) == "viewEmbedOnly") {
            path_var += "embed_type=viewEmbedOnly&";
        }
        if (getParametersName("print_form", pathname) == "true") {
            path_var += "print_form=true";
        }
        if(embed_contianer.is(':data("ajaxRequest")')){
			embed_contianer.data('ajaxRequest').abort();
			console_time.end('embeded_view loading');
		}
		console_time.start('embeded_view loading');
		embed_contianer.data('ajaxRequest',$.post('/ajax/embedded_view' + path_var, reference, function (data) {
            callback(data);
            console_time.end('embeded_view loading');
        }));
        return embed_contianer.data('ajaxRequest').fail(function (a, b, c) {
            console.log("NAG ERROR UNG AJAX", " ", a, " ", b, " ", c);
        });
    },
    "getSetTotalOfColumn": function (response_table) {

        var embed_container = $(this);
        console.log("embed_container", embed_container);
        var response_embed_table = $(response_table);
        var data_output_col_total = embed_container.attr("data-output-col-total");
        var json_data_output_col_total = embed_container.attr("data-output-col-total");
        console.log(data_output_col_total)
        try {
            json_data_output_col_total = JSON.parse(data_output_col_total);
        } catch (e) {
            json_data_output_col_total = [];
        }

        var response_table_col_fld_name = null;
        var output_res_fld_name = null;
        var formula_proc = null;
        var get_col_index = null;
        var col_total_value = 0;
        var count_values = 0;
        var collect_elements_to_trigger = $();
        var result_processed_value = null;
        for (var key_ctr_i in json_data_output_col_total) {
            {
                //console.log("TESTERETE", json_data_output_col_total[key_ctr_i]['escr_embed_fs_col_data'])
                //output_res_fld_name = json_data_output_col_total[key_ctr_i]['escr_embed_output_field'];
                //formula_proc = new Formula(json_data_output_col_total[key_ctr_i]['escr_embed_fs_col_data']);
                //console.log("hahahahahahahahahahahaBOOM", formula_proc.getEvaluation(), output_res_fld_name);
                //var dito_ilalagay_val = $("div.loaded_form_content").children("div.setOBJ").children("div.fields_below").find("input[name=" + output_res_fld_name + "]");
                //console.log("memdemdmedmedz", dito_ilalagay_val);
                //col_total_value = formula_proc.getEvaluation();
                //if (dito_ilalagay_val.is('[data-input-type="Currency"]')) {
                //    if ( $.isNumeric(col_total_value) ) {
                //        col_total_value = Number(col_total_value).currencyFormat();
                //        dito_ilalagay_val.val(col_total_value);
                //    }
                //}else if (dito_ilalagay_val.is('[data-input-type="Number"]') ){
                //    if ( $.isNumeric(col_total_value) ) {
                //        dito_ilalagay_val.val(col_total_value);
                //    }
                //}else{
                //    dito_ilalagay_val.val(col_total_value);
                //}
            }


            col_total_value = 0;
            count_values = 0;
            response_table_col_fld_name = json_data_output_col_total[key_ctr_i]['escr_embed_fs_col_data'];
            output_computation_tag = json_data_output_col_total[key_ctr_i]['escr_embed_computation_field'];
            output_res_fld_name = json_data_output_col_total[key_ctr_i]['escr_embed_output_field'];
            get_col_index = response_embed_table.find("th").children("[data-fld-name='" + response_table_col_fld_name + "']").parent().index();
            console.log("STEAm", get_col_index, response_embed_table, response_embed_table.find("td").filter(":eq(" + get_col_index + ")"))
            response_embed_table.children("tbody").children("tr").children("td").filter(function () {
                if ($(this).index() == get_col_index) {
                    return true;
                } else {
                    return false;
                }
            }).each(function () {
                console.log("KDOT", $(this), $(this).children("input.embed_getFields"));
                col_total_value += Number($(this).children("input.embed_getFields").val().replace(/,/g, ""));
                count_values += 1;
            });

            if (output_computation_tag == "Average") {
                result_processed_value = col_total_value / count_values;
            } else if (output_computation_tag == "Count") {
                result_processed_value = count_values;
            } else {
                result_processed_value = col_total_value;
            }

            if (isNaN(result_processed_value)) {
                result_processed_value = 0;
            }
            if ($('[data-type-fontfamily]').length >= 1) {

                embed_container.find('table.table_data').find('th').css("font-family", embed_container.closest('.setOBJ[data-type="embeded-view"]').attr('data-type-fontfamily'));
                embed_container.find('table.table_data').find('td').children('label').css("font-family", embed_container.closest('.setOBJ[data-type="embeded-view"]').attr('data-type-fontfamily'));
                embed_container.closest('.setOBJ[data-type="embeded-view"]').find('.embed_newRequest').css("font-family", embed_container.closest('.setOBJ[data-type="embeded-view"]').attr('data-type-fontfamily'));
            }
            if ($('.getFields[name="' + output_res_fld_name + '"]').length >= 1 && $('.getFields[name="' + output_res_fld_name + '"]').is('[data-input-type="Currency"]')) {
                $('.getFields[name="' + output_res_fld_name + '"]').val(GetRound(result_processed_value, 2).currencyFormat());
                collect_elements_to_trigger = collect_elements_to_trigger.add($('.getFields[name="' + output_res_fld_name + '"]'));
            } else {
                $('.getFields[name="' + output_res_fld_name + '"]').val(GetRound(result_processed_value, 2));
                collect_elements_to_trigger = collect_elements_to_trigger.add($('.getFields[name="' + output_res_fld_name + '"]'));
            }
        }
        // collect_elements_to_trigger.trigger("change",["embededTrigger"]); // kaya naka comment to dahil may sarili nang onchange formula si embed
        collect_elements_to_trigger.trigger('change.readOnlyControl');
        embed_container.trigger("embed.FormulaOnchange");
    },
    embedrowsSorter: function (akosiself) {

        var own_this = $(this);
        var data_object_id = own_this.parents(".setOBJ").eq(0).attr("data-object-id");
        var type_ng_header = own_this.attr("data-form-fld-name");
        if (type_ng_header.indexOf('[]') >= 0) {
            type_ng_header = type_ng_header.replace('[]', '');
        }
        var container_ng_embed = own_this.parents(".embed-view-container.getFields_" + data_object_id + "");
        if (own_this.is("[sorted='asc']")) {
            own_this.attr("sorted", "desc");
            var type_ng_sort = "desc";
        } else {
            own_this.attr("sorted", "asc");
            var type_ng_sort = "asc";
        }

        akosiself.refreshEmbed.call(akosiself, container_ng_embed, type_ng_sort, type_ng_header);

    },
    refreshEmbed: function (embed_container, sort_type, header_click_name, callBack_refresh) {
        //alert(""+sort_type+"~~"+header_click_name);
        var self = this;
        var container = $(embed_container);
        var do_id = $(container).parents('.setOBJ[data-type="embeded-view"][data-object-id]:eq(0)').attr('data-object-id');
        var formId = container.attr('embed-source-form-val-id');
        var fieldReference = container.attr('embed-result-field-val');
        var fieldReference_ele = $('[name="' + fieldReference + '"]');
        if (fieldReference_ele.length <= 0) {
            fieldReference_ele = $('[name="' + fieldReference + '[]"]');
        }
        if (fieldReference_ele.is("select")) {
            var fieldValue = fieldReference_ele.children('option:selected').map(function () {
                return $(this).val();
            }).get();
            if ($.type(fieldValue) == "array") {
                fieldValue = fieldValue.filter(Boolean);
                fieldValue = fieldValue.join("|^|");
            }
        } else if (fieldReference_ele.is("input[type='checkbox']")) {
            var fieldValue = fieldReference_ele.filter(':checked').val();
            if ($.type(fieldValue) == "array") {
                fieldValue = fieldValue.filter(Boolean);
                fieldValue = fieldValue.join("|^|");
            }
        }
        else if (fieldReference_ele.closest('.setOBJ[data-type="pickList"]').length > 0) {
        	var fieldValue = fieldReference_ele.val().split('|');
        } else {
            var fieldValue = fieldReference_ele.val();
        }

        var fieldFilter = container.attr('embed-source-lookup-active-field-val');
        var fieldReferenceType = container.attr('rfc-choice');
        if (fieldReferenceType == 'computed') {
            var thisFormula = container.attr('embed-computed-formula');
            var formulaDoc = new Formula(thisFormula);
            fieldValue = formulaDoc.getEvaluation();
        }
        var fieldFilterConditionalOperator = $(container).attr('embed-conditional-operator');
        if ($.type(fieldFilterConditionalOperator) == "undefined") {
            fieldFilterConditionalOperator = "=";
        } else if (fieldFilterConditionalOperator == "") {
            fieldFilterConditionalOperator = "=";
        }
        var embedAdditionalFilterFormula = $(container).attr('embed-additional-filter-formula');
        if ($.type(embedAdditionalFilterFormula) == "undefined") {
            embedAdditionalFilterFormula = "";
        }

        var enable_embed_row_click = container.attr("enable-embed-row-click");
        var embed_action_click_copy = container.attr('embed-action-click-copy');
        var embed_action_click_delete = container.attr('embed-action-click-delete');
        var embed_action_click_edit = container.attr('embed-action-click-edit');
        var embed_action_click_view = container.attr('embed-action-click-view');
        var embed_action_click_number = container.attr('embed-action-click-number');
        var embed_action_click_edit_popup = container.attr('embed-action-click-edit-popup');
        var reference = {
            FormID: formId,
            FieldReferece: fieldReference,
            FieldValue: (fieldValue||""),
            FieldFilter: fieldFilter,
            "field_conditional_operator": fieldFilterConditionalOperator,
            "embed_additional_filter_formula": self.parseCondition(embedAdditionalFilterFormula),
            HLData: "",
            HLType: "row",
            HLAllow: "false",
            column_data: "",
            current_form_fields_data: [],
            "enable_embed_row_click": enable_embed_row_click,
            "embed_action_click_copy": embed_action_click_copy,
            "embed_action_click_delete": embed_action_click_delete,
            "embed_action_click_edit": embed_action_click_edit,
            "embed_action_click_view": embed_action_click_view,
            "embed_action_click_number": embed_action_click_number,
            "embed_action_click_edit_popup": embed_action_click_edit_popup,
            "embed_default_sorting": {"column": "ID", "type": "ASC"}
        };
        if ($.type(sort_type) != "undefined") {
            reference['embed_sort_type'] = sort_type;
        }
        if ($.type(header_click_name) != "undefined") {
            reference['embed_sort_header_click_name'] = header_click_name;
        }
        if (typeof $(container).attr('embed-hl-data') != "undefined") {
            reference["HLData"] = $(container).attr('embed-hl-data');
        }
        if (typeof $(container).attr('allow-highlights') != "undefined") {
            reference["HLAllow"] = $(container).attr('allow-highlights');
        }
        if (typeof $(container).attr('allow-highlights') != "undefined") {
            reference["HLType"] = $(container).attr('highlight-type');
        }
        if (typeof $(container).attr('embed-column-data') != "undefined") {
            reference["column_data"] = $(container).attr('embed-column-data');
        }
        console.log("BOOM11", reference);
        $('#frmrequest').find(".setOBJ").each(function (eqi) {
            var data_object_id = $(this).attr("data-object-id");
            var getFIELDS = $(this).find(".getFields_" + data_object_id);
            var field_name = $(this).find(".getFields_" + data_object_id).eq(0).attr("name");
            field_name_values = [];
            getFIELDS.each(function (eqi2) {
                field_name_values.push($(this).val());
            })
            field_name_values = field_name_values.join("|^|");
            reference["current_form_fields_data"].push({
                "f_name": field_name,
                "values": field_name_values
            })
        });
        console.log("BOOM123", reference);
        reference["current_form_fields_data"] = JSON.stringify(reference["current_form_fields_data"]);
        $(container).html('<center><img src="/images/loader/loader.gif" class="load-app-request" style="display: inline;margin-top:30px;"></center>');
        // $(container).data("embed_view_reference",reference);
        reference["embed_container_id"] = do_id;
        self.getRequests(reference, function (data) {
            console.log("BOOMomg", data);
            var obj_jq = $(data)
            var parent_container_embed = $(container)

            parent_container_embed.html(obj_jq);
            EmbeddedFixHeader.reFixHeader($(parent_container_embed));
            var rows_element = obj_jq.find("tr").filter(":not(:eq(0))").find(".viewTDEmbedded");


            obj_jq.find("tr").filter(":not(:eq(0))").addClass("hovereffect");

            // console.log("KLASSS",obj_jq.attr("author-valid"))
            if (obj_jq.attr("author-valid")) {

                if (obj_jq.attr("author-valid") == "true") {
                } else {
                    parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
                }
            } else {
                parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
            }
            self.getSetTotalOfColumn.call(parent_container_embed, parent_container_embed.children());
            rows_element.each(function(){ //FS#7646
            	var dis_row_click_ele = $(this).parents("tr").eq(0);
            	var row_click_data = {
            	    "record_trackno": dis_row_click_ele.attr("record-trackno"),
            	    "record_id": dis_row_click_ele.attr("record-id"),
            	    "formId": formId
            	}
            	if ($(this).attr("data-action-attr") == "view") {
            	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
            	} else {
            	    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
            	}
            	$(this).attr("data-embed-href-link",src); //FS#7646
            });
            rows_element.on({
                "click": function () {

                    var dis_ele = $(this);
                    var embed_view_cont = dis_ele.parent().find(".embed-view-container").eq(0);
                    $('html').data("embed_container", embed_view_cont);
                    var embeded_attr = $(this).attr("data-action-attr");
                    var allow_row_click = $(container).attr("enable-embed-row-click")
                    if (allow_row_click) {
                        if (allow_row_click != "true") {
                            return;
                        }
                    } else {
                        return;
                    }
                    var dis_row_click_ele = $(this).parents("tr").eq(0);
                    var row_click_data = {
                        "record_trackno": dis_row_click_ele.attr("record-trackno"),
                        "record_id": dis_row_click_ele.attr("record-id")
                    }


                    var ret =
                            '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
                            '<div class="hr"></div>';
                    var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "", function () {
                    });
                    dis_dialog.themeDialog("modal2");
                    var ele_dialog = $('#popup_container');

                    if (embeded_attr == "view") {
                        var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
                    } else {
                        var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
                    }
                    var my_dialog = $(
                        '<div class="embed-iframe" style="height: 100%;">' +
                        '<iframe style="height:100%;width:100%;" src="' + src + '">' +
                        '</iframe>' +
                        '</div>'
                    )

                    ele_dialog.find("#popup_content").html(my_dialog);
                    ele_dialog.find("#popup_content").css({
                        "height": ($(window).outerHeight() - 120) + "px"
                    });

                    ele_dialog.css({
                        "top": "30px"
                    });
                    ele_dialog.data("field_updater", fieldReference)
                    ele_dialog.data("embed_container", parent_container_embed);
                    var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
                    var loading_panel = panel.load(ele_dialog.find("#popup_content"));
                    loading_panel.parent().css("position", "relative");
                    iframe_ele_dialog.load(function () {
                        var iframe_document = $(this)[0].contentWindow.$('html');
                        EmbedPopUpDisplayFixes.call(iframe_document);
                        iframe_document.addClass('view-embed-iframe');
                        // iframe_document.find('.fl-user-header-wrapper').remove();
                        // iframe_document.find('.form-action-navs').css({"position": "fixed", "top": "0px"});
                        // iframe_document.find('.fl-content, .fl-user-navigation-wrapper, .display_form_status').css('top', '40px');
                        //kanina  
                        loading_panel.fadeOut();
                        if(embeded_attr == "view"){
                            iframe_document.find('[data-type="embeded-view"]').css({"z-index":"9999"});
                        }
                    })
                    // embeded_view.getRequests(parent_container_embed.data("embed_view_reference"), function(data) {
                    //     $(container).html(data);
                    // });

                    // var iframe_ele = my_dialog.find("iframe").eq(0);
                    // iframe_ele.load(function(){
                    //     var iframe_doc = $(iframe_ele).contents()[0];
                    //     console.log("FRAAA",$(iframe_doc))
                    // })
                    // Roni Pinili close button modal configuration

                    if ($('body').data("user_form_json_data")) {

                        var embeddedViewObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

                        if ($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]) {
                            var embededViewActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
                            console.log("VALUE", embededViewActionVisibilityVal);
                            if (embededViewActionVisibilityVal == "Yes") {
                                var close_dial = setInterval(function () {
                                    $('.fl-closeDialog').hide();
                                    if ($('.fl-closeDialog').length >= 1) {
                                        clearInterval(close_dial);
                                    }
                                }, 0);
                            }
                        }

                    }

                }
            })
            if ($('[data-type-fontfamily]').length >= 1) {
                //console.log("hey medz", embed_container.closest('.setOBJ[data-type="embeded-view"]').attr('data-type-fontfamily'));
                embed_container.find('table.table_data').find('th').css("font-family", embed_container.closest('.setOBJ[data-type="embeded-view"]').attr('data-type-fontfamily'));
                embed_container.find('table.table_data').find('td').children('label').css("font-family", embed_container.closest('.setOBJ[data-type="embeded-view"]').attr('data-type-fontfamily'));
                embed_container.closest('.setOBJ[data-type="embeded-view"]').find('.embed_newRequest').css("font-family", embed_container.closest('.setOBJ[data-type="embeded-view"]').attr('data-type-fontfamily'));
            }
            $(".dataTip").tooltip();
            obj_jq.find(".columnHeader").on("click", function () {
            	// console.log("columnheader", $(this).parent());
            	if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
            		return;
            	}
            	if($(this).closest('.embed-view-container').is('.embed-edit-mode')){
            		return;
            	}
            	self.embedrowsSorter.call(this, self);
            });

            if ($.type(callBack_refresh) == "function") {
                callBack_refresh();
            }
            // setTimeout(function(){
            	if(container.attr('commit-data-row-event') == "parent"){
            		// embeded_view.displayTemporaryData(container);
            		embeded_view.tempoEmbeddedListRefresh(container, true);
            		embeded_view.embedHideDeletedRecords(container.closest('.setOBJ').attr('data-object-id'));
            	    // embeded_view.createTemporaryData(container, "insert");
            	}
            // }, 3000);
            initializePage.visibilityOnChange2(embed_container);
            //$(obj_jq).attr('style',"width: auto !important;");
            EmbeddedFixHeader.init(embed_container);
            EmbeddedFixHeader.maintainCellWidth(embed_container);
        });
		

    },
    parseCondition: function (string_condition) {
        var temp_val = null;
        var reg_eks_temp = null;
        var temp_string = "";

        var condition_formula = string_condition;
        if (!condition_formula) {
            return;
        }
        var condition_formula_temp = condition_formula;
        var get_reg_eks_fns = new RegExp("@CurrentForm\\\[[\\\'\\\"][a-zA-Z][a-zA-Z0-9_]*[\\\'\\\"]\\\]", "g");
        var matches_fns = condition_formula.match(get_reg_eks_fns);

        if ($.isArray(matches_fns)) {
            matches_fns = matches_fns.filter(Boolean);
            if (matches_fns.length >= 1) {
                for (var ctr_ikey in matches_fns) {
                    temp_string = matches_fns[ctr_ikey];
                    reg_eks_temp = new RegExp("\[[\'\"][a-zA-Z][a-zA-Z0-9_]*[\'\"]\]", "g");
                    temp_string = temp_string.match(reg_eks_temp);
                    if ($.isArray(temp_string)) {
                        temp_string = temp_string.filter(Boolean);
                        if (temp_string.length >= 1) {
                            temp_string = temp_string[0].replace(/[\[\]\'\"]/g, "");
                            if ($('[name="' + temp_string + '[]"]').is('select[multiple]')) {
                                temp_val = $('[name="' + temp_string + '[]"]').children('option:selected').map(function () {
                                    return $(this).val();
                                }).get().join("|^|");
                                reg_eks_temp = new RegExp("@CurrentForm\\\[[\\\'\\\"]" + temp_string + "[\\\'\\\"]\\\](?![a-zA-Z0-9_])", "g");
                                if ($('[name="' + temp_string + '[]"]').is('[data-input-type="Number"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else if ($('[name="' + temp_string + '[]"]').is('[data-input-type="Currency"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "\"" + temp_val + "\"");
                                }
                            } else if ($('[name="' + temp_string + '[]"]').is('input[type="checkbox"]')) {
                                temp_val = $('[name="' + temp_string + '[]"]').filter(':checked').map(function () {
                                    return $(this).val();
                                }).get().join("|^|");
                                reg_eks_temp = new RegExp("@CurrentForm\\\[[\\\'\\\"]" + temp_string + "[\\\'\\\"]\\\](?![a-zA-Z0-9_])", "g");
                                if ($('[name="' + temp_string + '[]"]').is('[data-input-type="Number"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else if ($('[name="' + temp_string + '[]"]').is('[data-input-type="Currency"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "\"" + temp_val + "\"");
                                }
                            } else if ($('[name="' + temp_string + '"]').length >= 1) {
                                temp_val = $('[name="' + temp_string + '"]').val();
                                reg_eks_temp = new RegExp("@CurrentForm\\\[[\\\'\\\"]" + temp_string + "[\\\'\\\"]\\\](?![a-zA-Z0-9_])", "g");
                                if ($('[name="' + temp_string + '"]').is('[data-input-type="Number"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else if ($('[name="' + temp_string + '"]').is('[data-input-type="Currency"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "\"" + temp_val + "\"");
                                }
                            }
                        }
                    }
                }
                // for(var ctr_i in matches_fns){
                //     temp_string = matches_fns[ctr_i];
                //     alert(temp_string)
                //     if($('[name="'+temp_string+'[]"]').is('select[multiple]') ){
                //         temp_val = $('[name="'+temp_string+'[]"]').children('option:selected').map(function(){ return $(this).val(); }).join("|^|");
                //         reg_eks_temp = new RegExp("@CurrentForm\[[\'\"]"+temp_string+"[\'\"]\](?![a-zA-Z0-9_])","g");
                //         condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "\""+temp_val+"\"");
                //     }else if($('[name="'+temp_string+'[]"]').is('input[type="checkbox"]') ){
                //         temp_val = $('[name="'+temp_string+'[]"]').filter(':checked').map(function(){ return $(this).val(); }).join("|^|");
                //         reg_eks_temp = new RegExp("@CurrentForm\[[\'\"]"+temp_string+"[\'\"]\](?![a-zA-Z0-9_])","g");
                //         condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "\""+temp_val+"\"");
                //     }else if($('[name="'+temp_string+'"]').length >= 1 ){
                //         alert(234)
                //         temp_val = $('[name="'+temp_string+'"]').val();
                //         reg_eks_temp = new RegExp("@CurrentForm\[[\'\"]"+temp_string+"[\'\"]\](?![a-zA-Z0-9_])","g");
                //         condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "\""+temp_val+"\"");
                //     }else{

                //     }
                // }
            }
        }
        return condition_formula_temp;
    },
    //embed - embedded view container
    //type - ("insert" or "update");
    //method - ("in-line" or "popup");
    //data_only - "true" or "false" create a temporary data with data only and it will not generate a new line
    //callback - not yet implemented. 
    "createTemporaryData": function(embed, type, method, tr_reference, data_only, callBack, window_dialog){
        //this function will create a json data of the newly insert request from embedded
        var embedded_view = $(embed);
        var emd_view = this;
        if(data_only){
        	var parameters = {
        	    "form_id": embedded_view.attr('embed-source-form-val-id'),
        	    "columns_needed": ["field_name", "field_type", "formula", "multiple_values", "other_attributes", "formula_type", "field_input_type", "readonly"],
        	    "get_row_count": true,
        	    "get_request_details": true
        	}
        	if(embedded_view.attr('commit-data-row-event') == "default"){
        		embedded_view.addClass('embed-edit-mode');
        	}
        	$.post('/ajax/embed_request', parameters, function(echo_result){
        	    console.log(echo_result);
        	    try{
        	        var result = JSON.parse(echo_result);
        	        var data = $('body').data();
        	        if(!data['embed_row_data']){
        	            data['embed_row_data'] = {};
        	        }
        	        row_id = embedded_view.closest('.setOBJ').attr('data-object-id');
        	        if(!data['embed_row_data'][row_id]){
        	            data['embed_row_data'][row_id] = {};
        	        }
        	        var get_count_current_process_row = $('tr[is-pending="true"]').length||0;
        	        var row_name_count = tr_reference || embedded_view.closest('.setOBJ').attr('data-object-id') + '_' + (Number(result['row_count']));
        	        console.log("row_name", row_name_count);
        	        data['embed_row_data'][row_id][row_name_count] = {
        	            "query_type": type,
        	            "origin": 'in-line',
        	            "saved": false,
        	            "form_id": embedded_view.attr('embed-source-form-val-id'),
        	            "fields_data": {}
        	        };
        	        for(details in result['other_details']){
        	            if(details == 'buttonStatus'){
        	                var getBtnStatus = JSON.parse(result['other_details'][details]);
        	                data['embed_row_data'][row_id][row_name_count]['fields_data']['Node_ID'] = {
        	                    "type": undefined, 
        	                    "name": details,
        	                    "value": getBtnStatus['child_id']
        	                }
        	            }
        	            else{
        	                data['embed_row_data'][row_id][row_name_count]['fields_data'][details] = {
        	                    "type": undefined,
        	                    "name": details,
        	                    "value": result['other_details'][details]
        	                }
        	            }
        	        }
        	        for(r in result){
        	        	if(!isNaN(r)){
        	            	if(result[r]['field_type'] == "TrackNo"){
        	            	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
        	            	        "type": result[r]['field_type'],
        	            	        "name": result[r]['field_name'],
        	            	        "formula_type": result[r]['formula_type']||"static",
        	            	        "field_input_type": result[r]['field_input_type']||"",
        	            	        "value": 'pending...'
        	            	    }
        	            	}
        	            	else if(result[r]['field_type'] == "pickList"){
        	            	    var other_attr = JSON.parse(result[r]['other_attributes']);
        	            	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
        	            	        "type": result[r]['field_type'],
        	            	        "name": result[r]['field_name'],
        	            	        "id": other_attr['return-field'],
        	            	        "picklist_button_id": other_attr['picklist-button-id'],
        	            	        "picklist_type": other_attr['picklist-type'],
        	            	        "return_field": other_attr['return-field'],
        	            	        "form_id": other_attr['form-id'],
        	            	        "return_field_name": other_attr['return-field-name'],
        	            	        "formname": other_attr['formname'],
        	            	        "display_columns": other_attr['display_columns'],
        	            	        "display_column_sequence": other_attr['display_column_sequence'],
        	            	        // "selection_type": other_attr['selection-type'],
        	            	        "enable_add_entry": other_attr['enable_entry'],
        	            	        "allow_values_not_in_list": other_attr['allow-values-not-in-list'],
        	            	        "show_picklist_view": '',
        	            	        "formula_type": result[r]['formula_type']||"static",
        	            	        "formula": result[r]['formula']||"",
        	            	        "field_input_type": result[r]['field_input_type']||"",
        	            	        "readonly": result[r]['readonly']||0,
        	            	        "value": ''
        	            	    }
        	            	}
        	            	else if(result[r]['field_type'] == "radioButton"){
        	            	    console.log(JSON.parse(result[r]['multiple_values'])[0]);
        	            	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
        	            	    var options = $.map(other_attr, function(value, index){
        	            	        return value;
        	            	    });
        	            	    var get_id = result[r]['field_name'];
        	            	    var get_selected = '';
        	            	    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
        	            	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
        	            	        "type": result[r]['field_type'],
        	            	        "name": result[r]['field_name'],
        	            	        "options": options,
        	            	        "formula_type": result[r]['formula_type']||"static",
        	            	        "formula": result[r]['formula']||"",
        	            	        "field_input_type": result[r]['field_input_type']||"",
        	            	        "readonly": result[r]['readonly']||0,
        	            	        "value": []
        	            	    
        	            	    }
        	            	}
        	            	else if(result[r]['field_type'] == "checkbox"){
        	            	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
        	            	    var options = $.map(other_attr, function(value, index){
        	            	        return value;
        	            	    });
        	            	    var get_id = result[r]['field_name'];
        	            	    var get_selected = '';
        	            	    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
        	            	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
        	            	        "type": result[r]['field_type'],
        	            	        "name": result[r]['field_name'],
        	            	        "options": options,
        	            	        "formula_type": result[r]['formula_type']||"static",
        	            	        "formula": result[r]['formula']||"",
        	            	        "field_input_type": result[r]['field_input_type']||"",
        	            	        "readonly": result[r]['readonly']||0,
        	            	        "value": []
        	            	    }
        	            	}
        	            	else if(result[r]['field_type'] == "selectMany"){
        	            	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
        	            	    var options = $.map(other_attr, function(value, index){
        	            	        return value;
        	            	    });
        	            	    var get_id = result[r]['field_name'];
        	            	    var get_selected = '';
        	            	    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
        	            	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
        	            	        "type": result[r]['field_type'],
        	            	        "name": result[r]['field_name'],
        	            	        "options": options,
        	            	        "formula_type": result[r]['formula_type']||"static",
        	            	        "formula": result[r]['formula']||"",
        	            	        "field_input_type": result[r]['field_input_type']||"",
        	            	        "readonly": result[r]['readonly']||0,
        	            	        "value": []
        	            	    }
        	            	}
        	            	else if(result[r]['field_type'] == "dropdown"){
        	            	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
        	            	    var options = $.map(other_attr, function(value, index){
        	            	        return value;
        	            	    });
        	            	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
        	            	        "type": result[r]['field_type'],
        	            	        "name": result[r]['field_name'],
        	            	        "options": options,
        	            	        "formula_type": result[r]['formula_type']||"static",
        	            	        "formula": result[r]['formula']||"",
        	            	        "field_input_type": result[r]['field_input_type']||"",
        	            	        "readonly": result[r]['readonly']||0,
        	            	        "value": options[0]
        	            	    }
        	            	}
        	            	else if(result[r]['field_type'] == "listNames"){
        	            	    var other_attr = JSON.parse(result[r]['other_attributes']);
        	            	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
        	            	        "type": result[r]['field_type'],
        	            	        "name": result[r]['field_name'],
        	            	        "id": other_attr['return-field'],
        	            	        "return_field": other_attr['return-field'],
        	            	        // "list_type_selection": other_attr['list-type-selection'],
        	            	        "data_original_title": '',
        	            	        "formula_type": result[r]['formula_type']||"static",
        	            	        "formula": result[r]['formula']||"",
        	            	        "field_input_type": result[r]['field_input_type']||"",
        	            	        "readonly": result[r]['readonly']||0,
        	            	        "value": ''
        	            	    }

        	            	}
        	            	else{
        	            	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
        	            	        "type": result[r]['field_type'],
        	            	        "name": result[r]['field_name'],
        	            	        "formula_type": result[r]['formula_type']||"static",
        	            	        "formula": result[r]['formula']||"",
        	            	        "field_input_type": result[r]['field_input_type']||"",
        	            	        "readonly": result[r]['readonly']||0,
        	            	        "value": ''
        	            	    }
        	            	}
        	            }
        	        }
        	        data['embed_row_data'][row_id][row_name_count]['fields_data']['TrackNo'] = {
        	            "type": undefined,
        	            "name": 'TrackNo',
        	            "value": 'pending...'
        	        }
        	        data['embed_row_data'][row_id][row_name_count]['fields_data']['Processor'] = {
        	            "type": undefined,
        	            "name": 'Processor',
        	            "value": $('[name="Processor"]').val()
        	        }
        	        data['embed_row_data'][row_id][row_name_count]['fields_data']['Requestor'] = {
        	            "type": undefined,
        	            "name": 'Requestor',
        	            "value": $('[name="Requestor"]').val()
        	        }
        	        data['embed_row_data'][row_id][row_name_count]['fields_data']['UpdatedBy'] = {
        	            "type": undefined,
        	            "name": 'UpdatedBy',
        	            "value": $('[name="CurrentUser"]').val()
        	        }
        	        data['embed_row_data'][row_id][row_name_count]['fields_data']['requestor-name'] = {
        	            "type": undefined,
        	            "name": 'requestor-name',
        	            "value": $('[name="Requestor"]').attr('requestor-name')
        	        }
        	        data['embed_row_data'][row_id][row_name_count]['fields_data']['status-embed'] = {
        	            "type": undefined,
        	            "name": 'status-embed',
        	            "value": 'Draft'
        	        }
        	        var parameters = {
        	            'form_id': row_id,
        	            "record_id": embedded_view.find('tr[reference-id="' + row_name_count + '"]').attr('record-id')
        	        }
        	        // console.log("parameters", parameters);
        	        console.log("multiple_on_attachment....", data['embed_row_data'][row_id][row_name_count]['fields_data']);
        	        $.post('/ajax/get_fields_data', parameters, function(result){
        	        	// console.log(result);
        	            var res = JSON.parse(result);
        	            for(i in res['tbl_values'][0]){
        	            	if(i != "other_details"){
        	            		if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][i]) == "undefined"){
        	            			data['embed_row_data'][row_id][row_name_count]['fields_data'][i] = {
        	            			    "type": $(this).parents('.setOBJ').attr('data-type'),
        	            			    "name": i,
        	            			    "value": res['tbl_values'][0][i]
        	            			}
        	            		}
        	            	}
        	            }
        	            // alert($.type(callBack));
        	            // if(callBack){
        	            // 	alert(123);	
        	            // 	callBack();
        	            // }
        	            checkEmbedAttr($('[reference-id="' + row_name_count + '"]'));
        	        });
        	        
        	        // embeded_view.addToEmbedList(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, type, true);
        	        // ui.unblock();
        	        // EmbeddedUI.unblock(embeded_view);
        	    }
        	    catch(e){
        	        console.error("error", "something is wrong...", e);
        	    }
        	    if(callBack){
        	    	callBack();
        	    }
        	});
        }
        else{
	        if($.type(embedded_view.attr('commit-data-row-event')) != "undefined"){
	            // if(embedded_view.attr('commit-data-row-event') == 'parent'){
	                if(type == 'insert'){
	                    if(method == "in-line"){
	                        var data = $('body').data();
	                        if(!data['embed_row_data']){
	                        	console.log("walang embed row data");
	                            data['embed_row_data'] = {};
	                        }
	                        row_id = embedded_view.closest('.setOBJ').attr('data-object-id');
	                        if(!data['embed_row_data'][row_id]){
	                        	console.log("row id");
	                            data['embed_row_data'][row_id] = {};
	                        }
	                        var row_name_count = "";
	                        var record_id = "";
	                        if(tr_reference){
	                        	row_name_count = tr_reference;
	                        }
	                        else{
	                        	row_name_count = embedded_view.closest('.setOBJ').attr('data-object-id') + '_' + $('tr[current]').attr('record-id');
	                        }
	                        record_id = row_name_count.split("_")[1];
	                        console.log("row_name", row_name_count);
	                        if(!data['embed_row_data'][row_id][row_name_count]){
	                        	data['embed_row_data'][row_id][row_name_count] = {
	                        	    "query_type": type,
	                        	    "origin": method,
	                        	    "saved": false,
	                        	    "form_id": embedded_view.attr('embed-source-form-val-id'),
	                        	    "fields_data": {}
	                        	};
	                        }
	                        var get_data_type = function(field_name){
	                            var getTheDataTypes = {};
	                            if(field_name == ""){
	                                $(embedded_view).find('.columnHeader').map(function(){
	                                    getTheDataTypes[$(this).attr('data-fld-name')] = $(this).attr('data-fld-name');
	                                });
	                            }
	                            else{
	                                $(embedded_view).find('.columnHeader').map(function(){
	                                    getTheDataTypes[$(this).attr('data-fld-name')] = $(this).attr('data-field-object-type');
	                                });
	                                
	                            }
	                            return getTheDataTypes[field_name];
	                        }
	                        var get_formula_type = function(field_name){
	                            var getTheDataTypes = {};
	                            if(field_name == ""){
	                                $(embedded_view).find('.columnHeader').map(function(){
	                                    getTheDataTypes[$(this).attr('data-fld-name')] = $(this).attr('data-field-formula-type')||"static";
	                                });
	                            }
	                            else{
	                                $(embedded_view).find('.columnHeader').map(function(){
	                                    getTheDataTypes[$(this).attr('data-fld-name')] = $(this).attr('data-field-object-type')||"static";
	                                });
	                                
	                            }
	                            return getTheDataTypes[field_name];
	                        }
	                        var get_fields  = $('[reference-id="' + row_name_count + '"]').find('.getFields,.embed_getFields');
	                        get_fields.each(function(){
	                            console.log("field_name", $(this).attr('data-field-name'));
	                            var get_field_name = $(this).attr('data-field-name').replace(/\[\]/g, '')
	                            if(get_data_type(get_field_name) == "pickList"){
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                    var value = "";
	                                    if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                    	value = $(this).val().replace(/,/g, '');
	                                    }
	                                    else{
	                                    	value = $(this).val();
	                                    }
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]["value"] = value;
	                                }
	                                else{
	                                	var value = "";
	                                	if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                		value = $(this).val().replace(/,/g, '');
	                                	}
	                                	else{
	                                		value = $(this).val();
	                                	}
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "picklist_button_id": $(this).parents('td').find('a').attr('picklist-button-id'),
	                                        "picklist_type": $(this).parents('td').find('a').attr('picklist-type'),
	                                        "return_field": $(this).parents('td').find('a').attr('return-field'),
	                                        "form_id": $(this).parents('td').find('a').attr('form-id'),
	                                        "return_field_name": $(this).parents('td').find('a').attr('return-field-name'),
	                                        "formname": $(this).parents('td').find('a').attr('formname'),
	                                        "display_columns": $(this).parents('td').find('a').attr('display_columns'),
	                                        "display_column_sequence": $(this).parents('td').find('a').attr('display_column_sequence'),
	                                        // "selection_type": $(this).parents('td').find('a').attr('selection-type'),
	                                        "enable_add_entry": $(this).parents('td').find('a').attr('enable-add-entry'),
	                                        "allow_values_not_in_list": $(this).parents('td').find('a').attr('allow-values-not-in-list'),
	                                        "show_picklist_view": $(this).parents('td').find('a').attr('show-picklist-view'),
	                                        "formula_type": get_formula_type(get_field_name),
	                                       	"condition": $(this).parents('td').find('a').attr('show-picklist-view'),
	                                        "value": value
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "radioButton"){
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('tr').find('[data-field-name="' + get_id + '"]:checked').val();
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = get_selected;
	                                }
	                                else{
	                                    var options = $(this).parents('td').find('.embed_getFields').map(function(){
	                                        return $(this).val();
	                                    }).get();
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('tr').find('[data-field-name="' + get_id + '"]:checked').val();
	                                    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "options": options,
	                                        "formula_type": get_formula_type(get_field_name),
	                                        "value": get_selected
	                                    
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "checkbox"){
	                                var field_name = $(this).attr('data-field-name').replace(/\[\]/g,'');
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('tr').find('[data-field-name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
	                                }
	                                else{
	                                    var options = $(this).parents('td').find('.embed_getFields').map(function(){
	                                        return $(this).val();
	                                    }).get();
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('td').find('[data-field-name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
	                                    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
	                                    // var field_name = $(this).attr('data-field-name').replace(/\[\]/g,'');
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "options": options,
	                                        "formula_type": get_formula_type(get_field_name),
	                                        "value": get_selected
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "selectMany"){
	                            	console.log("select many e");
	                                var field_name = $(this).attr('data-field-name').replace(/\[\]/g,'');
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
	                                	var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('tr').find('[data-field-name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
	                                    console.log("get_selected", get_selected);
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected
	                                }
	                                else{
	                                    var options = $(this).parents('td').find('option').map(function(){
	                                        return $(this).val();
	                                    }).get();
	                                    var get_id = $(this).attr('data-field-name');
	                                    console.log("select many get_id", get_id);
	                                    var get_selected = $(this).parents('tr').find('[data-field-name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
	                                    console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
	                                    // var field_name = $(this).attr('data-field-name').replace(/\[\]/g,'');
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "options": options,
	                                        "formula_type": get_formula_type(get_field_name),
	                                        "value": get_selected
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "dropdown"){
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = $(this).val()
	                                }
	                                else{
	                                    var options = $(this).parents('td').find('option').map(function(){
	                                        return $(this).val();
	                                    }).get();
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "options": options,
	                                        "formula_type": get_formula_type(get_field_name),
	                                        "value": $(this).val()
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "listNames"){
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                	var value = "";
	                                	if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                		value = $(this).val().replace(/,/g, '');
	                                	}
	                                	else{
	                                		value = $(this).val();
	                                	}
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = value;
	                                }
	                                else{
	                                	var value = "";
	                                	if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                		value = $(this).val().replace(/,/g, '');
	                                	}
	                                	else{
	                                		value = $(this).val();
	                                	}
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "return_field": $(this).attr('id'),
	                                        // "list_type_selection": $(this).attr('list-type-selection'),
	                                        "data_original_title": $(this).parents('td').find('a').attr('data-original-title'),
	                                        "formula_type": get_formula_type(get_field_name),
	                                        "value": value                                    }
	                                }

	                            }
	                            else if($(this).parents('.setOBJ').attr('data-type') == "attachment_on_request"){
	                            	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                            	}
	                            	else{
	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                            			"type": get_data_type(get_field_name),
	                            			"name": $(this).attr('data-field-name'),
	                            			"id": $(this).attr('id'),
	                            			"obj_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                            			"form_id": $(this).prev().attr('id'),
	                            			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                            			"form_data_id": $(this).prev().children().eq(0).attr('id'),
	                            			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
	                            			"value": $(this).val()
	                            		}
	                            	}
	                            }
	                            else if($(this).parents('.setOBJ').attr('data-type') == "multiple_attachment_on_request"){
	                            	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                            	}
	                            	else{
	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                            			"type": get_data_type(get_field_name),
	                            			"name": $(this).attr('data-field-name'),
	                            			"id": $(this).attr('id'),
	                            			"obj_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                            			"form_id": $(this).prev().attr('id'),
	                            			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                            			"form_data_id": $(this).prev().children().eq(0).attr('id'),
	                            			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
	                            			"value": $(this).val()
	                            		}
	                            	}
	                            }
	                            else{
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                    var value = "";
	                                    if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                    	value = $(this).val().replace(/,/g, '');
	                                    }
	                                    else{
	                                    	value = $(this).val();
	                                    }
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = value;
	                                }
	                                else{
	                                	var value = "";
	                                	if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                		value = $(this).val().replace(/,/g, '');
	                                	}
	                                	else{
	                                		value = $(this).val();
	                                	}
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "formula_type": get_formula_type(get_field_name),
	                                        "value": value
	                                    }
	                                }
	                            }
	                        });
	                        data['embed_row_data'][row_id][row_name_count]['fields_data']['DateCreated'] = {
	                            "type": undefined,
	                            "name": "DateCreated",
	                            "value": ""
	                        }
	                        data['embed_row_data'][row_id][row_name_count]['fields_data']['TrackNo'] = {
	                            "type": undefined,
	                            "name": "TrackNo",
	                            "value": $('tr[current]').attr('record-trackno')
	                        }
	                        data['embed_row_data'][row_id][row_name_count]['fields_data']['ID'] = {
	                            "type": undefined,
	                            "name": "ID",
	                            "value": record_id
	                        }
	                        // console.log("select many na ituu", data['embed_row_data'][row_id][row_name_count]['fields_data']['select_1']);
	                        emd_view.updateEmbedRow(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, record_id);
	                    }
	                    else{
	                    	if(window_dialog){
	                            var opener = $(window_dialog.document);
	                            var track_no = "";
	                            var record_id = "";
	                            opener.on('click', '.fl-action-submit', function(e){
	                                //when any button was click
	                                // alert('fl-action-submit clicked');
	                                var getPopup = setTimeout(function(){
	                                    // waiting until the dialog appears
	                                    // console.log("finding nemo");
	                                    if(opener.find('[id="popup_ok"]').length > 0){
	                                    	// console.log("popup_ok found");
	                                    	clearInterval(getPopup);
	                                        var popup_ok = '<input type="button" class="fl-margin-right  btn-blueBtn fl-positive-btn" value="Yes" id="embed_okay">';
	                                        opener.find('[id="popup_ok"]').replaceWith(popup_ok);//replaece the submit button
	                                        opener.find('[id="embed_okay"]').on('click', function(e){//add event to the replaced button
	                                            var parameters = {
	                                                "form_id": embedded_view.attr('embed-source-form-val-id'),
	                                                "columns_needed": ["field_name", "field_type", "formula", "multiple_values", "other_attributes"],
	                                                "get_row_count": true
	                                            }
	                                            $.post('/ajax/embed_request', parameters, function(echo_result){
	                                                var result = JSON.parse(echo_result);
	                                                var data = $('body').data();
	                                                if(!data['embed_row_data']){
	                                                    data['embed_row_data'] = {};
	                                                }
	                                                row_id = embedded_view.closest('.setOBJ').attr('data-object-id');
	                                                if(!data['embed_row_data'][row_id]){
	                                                    data['embed_row_data'][row_id] = {};
	                                                }
	                                                var get_count_current_process_row = $('tr[is-pending="true"]').length||0;
	                                                get_count_current_process_row += 1;
	                                                var row_name_count = embedded_view.closest('.setOBJ').attr('data-object-id') + '_' + (Number(result['row_count']) + Number(get_count_current_process_row));
	                                                console.log("row_name", row_name_count);
	                                                data['embed_row_data'][row_id][row_name_count] = {
	                                                    "query_type": type,
	                                                    "origin": method,
	                                                    "saved": true,
	                                                    "form_id": embedded_view.attr('embed-source-form-val-id'),
	                                                    "fields_data": {}
	                                                };
	                                                var get_fields  = opener.find('.getFields');
	                                                get_fields.each(function(){
	                                                    if($(this).attr('name') == "TrackNo"){
	                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                                                            // data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = "pe"
	                                                        }
	                                                        else{
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
	                                                                "name": $(this).attr('name'),
	                                                                "value": 'pending...'
	                                                            }
	                                                        }
	                                                    }
	                                                    else if($(this).parents('.setOBJ').attr('data-type') == "pickList"){
	                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                                                            var value = "";
	                                                            if($(this).attr('data-input-type')){
	                                                            	if($(this).attr('data-input-type') == 'Currency'){
	                                                            		value = $(this).val().replace(/,/g, '');
	                                                            	}
	                                                            	else{
	                                                            		value = $(this).val();
	                                                            	}
	                                                            }
	                                                            else{
	                                                            	value = $(this).val();
	                                                            }
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                                                        }
	                                                        else{
	                                                        	var value = "";
	                                                        	if($(this).attr('data-input-type')){
	                                                        		if($(this).attr('data-input-type') == 'Currency'){
	                                                        			value = $(this).val().replace(/,/g, '');
	                                                        		}
	                                                        		else{
	                                                        			value = $(this).val();
	                                                        		}
	                                                        	}
	                                                        	else{
	                                                        		value = $(this).val();
	                                                        	}
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
	                                                                "name": $(this).attr('name'),
	                                                                "id": $(this).attr('id'),
	                                                                "picklist_button_id": $(this).parents('.setOBJ').find('a').attr('picklist-button-id'),
	                                                                "picklist_type": $(this).parents('.setOBJ').find('a').attr('picklist-type'),
	                                                                "return_field": $(this).parents('.setOBJ').find('a').attr('return-field'),
	                                                                "form_id": $(this).parents('.setOBJ').find('a').attr('form-id'),
	                                                                "return_field_name": $(this).parents('.setOBJ').find('a').attr('return-field-name'),
	                                                                "formname": $(this).parents('.setOBJ').find('a').attr('formname'),
	                                                                "display_columns": $(this).parents('.setOBJ').find('a').attr('display_columns'),
	                                                                "display_column_sequence": $(this).parents('.setOBJ').find('a').attr('display_column_sequence'),
	                                                                "enable_add_entry": $(this).parents('.setOBJ').find('a').attr('enable-add-entry'),
	                                                                "allow_values_not_in_list": $(this).parents('.setOBJ').find('a').attr('allow-values-not-in-list'),
	                                                                "show_picklist_view": $(this).parents('.setOBJ').find('a').attr('show-picklist-view'),
	                                                                "field_input_type": $(this).attr('data-input-type')||"",
	                                                                "value": value
	                                                            }
	                                                        }
	                                                    }
	                                                    else if($(this).parents('.setOBJ').attr('data-type') == "radioButton"){
	                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                                                            var get_id = $(this).attr('name');
	                                                            var get_selected = opener.find('[name="' + get_id + '"]:checked').val();
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = get_selected;
	                                                        }
	                                                        else{
	                                                            var options = $(this).parents('.setOBJ').find('.getFields').map(function(){
	                                                                return $(this).val();
	                                                            }).get();
	                                                            var get_id = $(this).attr('name');
	                                                            console.log("radio button name:", opener.find('[name="' + get_id + '"]:checked').val());
	                                                            var get_selected = opener.find('[name="' + get_id + '"]:checked').val();

	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
	                                                                "name": $(this).attr('name'),
	                                                                "id": $(this).attr('id'),
	                                                                "options": options,
	                                                                "field_input_type": $(this).attr('data-input-type')||"",
	                                                                "value": get_selected
	                                                            
	                                                            }
	                                                        }
	                                                    }
	                                                    else if($(this).parents('.setOBJ').attr('data-type') == "checkbox"){
	                                                        var field_name = $(this).attr('name').replace(/\[\]/g,'');
	                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
	                                                            var get_id = $(this).attr('name');
	                                                            var get_selected = opener.find('[name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
	                                                        }
	                                                        else{
	                                                            var options = $(this).parents('.setOBJ').find('.getFields').map(function(){
	                                                                return $(this).val();
	                                                            }).get();
	                                                            var get_id = $(this).attr('name');
	                                                            var get_selected = opener.find('[name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
	                                                            // console.log("options", options, "getid", get_id, "get_selected", opener.find('[name="' + get_id + '"]').filter(':checked').val());
	                                                            
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
	                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
	                                                                "name": field_name,
	                                                                "id": $(this).attr('id'),
	                                                                "options": options,
	                                                                "field_input_type": $(this).attr('data-input-type')||"",
	                                                                "value": get_selected
	                                                            }
	                                                        }
	                                                    }
	                                                    else if($(this).parents('.setOBJ').attr('data-type') == "selectMany"){
	                                                        var field_name = $(this).attr('name').replace(/\[\]/g,'');
	                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
	                                                            var get_id = $(this).attr('name');
	                                                            var get_selected = opener.find('[name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
	                                                        }   
	                                                        else{
	                                                            var options = $(this).parents('.setOBJ').find('option').map(function(){
	                                                                return $(this).val();
	                                                            }).get();
	                                                            var get_id = $(this).attr('name');
	                                                            var get_selected = opener.find('[name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
	                                                            // console.log("options", options, "getid", get_id, "get_selected", opener.find('[name="' + get_id + '"]').filter(':checked').val());
	                                                            var field_name = $(this).attr('name').replace(/\[\]/g,'');
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
	                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
	                                                                "name": field_name,
	                                                                "id": $(this).attr('id'),
	                                                                "options": options,
	                                                                "field_input_type": $(this).attr('data-input-type')||"",
	                                                                "value": get_selected
	                                                            }
	                                                        }
	                                                    }
	                                                    else if($(this).parents('.setOBJ').attr('data-type') == "dropdown"){
	                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = $(this).val();
	                                                        }
	                                                        else{
	                                                            var options = $(this).parents('.setOBJ').find('option').map(function(){
	                                                                return $(this).val();
	                                                            }).get();
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
	                                                                "name": $(this).attr('name'),
	                                                                "id": $(this).attr('id'),
	                                                                "options": options,
	                                                                "field_input_type": $(this).attr('data-input-type')||"",
	                                                                "value": $(this).val()
	                                                            }
	                                                        }
	                                                    }
	                                                    else if($(this).parents('.setOBJ').attr('data-type') == "listNames"){
	                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                                                            var value = "";
	                                                            if($(this).attr('data-input-type')){
	                                                            	if($(this).attr('data-input-type') == 'Currency'){
	                                                            		value = $(this).val().replace(/,/g, '');
	                                                            	}
	                                                            	else{
	                                                            		value = $(this).val();
	                                                            	}
	                                                            }
	                                                            else{
	                                                            	value = $(this).val();
	                                                            }
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                                                        }
	                                                        else{
	                                                        	var value = "";
	                                                        	if($(this).attr('data-input-type')){
	                                                        		if($(this).attr('data-input-type') == 'Currency'){
	                                                        			value = $(this).val().replace(/,/g, '');
	                                                        		}
	                                                        		else{
	                                                        			value = $(this).val();
	                                                        		}
	                                                        	}
	                                                        	else{
	                                                        		value = $(this).val();
	                                                        	}
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
	                                                                "name": $(this).attr('name'),
	                                                                "id": $(this).attr('id'),
	                                                                "return_field": $(this).attr('id'),
	                                                                "data_original_title": $(this).parents('.setOBJ').find('a').attr('data-original-title'),
	                                                                "field_input_type": $(this).attr('data-input-type')||"",
	                                                                "value": value
	                                                            }
	                                                        }
	                                                    }
	                                                    else if($(this).parents('.setOBJ').attr('data-type') == "attachment_on_request"){
	                                                    	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                                                    		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                                                    	}
	                                                    	else{
	                                                    		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                                                    			"type": $(this).parents('.setOBJ').attr('data-type'),
	                                                    			"name": $(this).attr('name'),
	                                                    			"id": $(this).attr('id'),
	                                                    			"obj_id": $(this).parents('.setOBJ').attr('data-object-id'),
	                                                    			"form_id": $(this).prev().attr('id'),
	                                                    			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                                                    			"form_data_id": $(this).prev().children().eq(0).attr('id'),
	                                                    			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
	                                                    			"value": $(this).val()
	                                                    		}
	                                                    	}
	                                                    }
	                                                    else if($(this).parents('.setOBJ').attr('data-type') == "multiple_attachment_on_request"){
	                                                    	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                                                    		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                                                    	}
	                                                    	else{
	                                                    		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                                                    			"type": $(this).parents('.setOBJ').attr('data-type'),
	                                                    			"name": $(this).attr('name'),
	                                                    			"id": $(this).attr('id'),
	                                                    			"obj_id": $(this).parents('.setOBJ').attr('data-object-id'),
	                                                    			"form_id": $(this).prev().attr('id'),
	                                                    			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                                                    			"form_data_id": $(this).prev().children().eq(0).attr('id'),
	                                                    			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
	                                                    			"form_data_attachment": $(this).attr('data-attachment'),
	                                                    			"value": $(this).val()
	                                                    		}
	                                                    	}
	                                                    }
	                                                    else{
	                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                                                        	var value = "";
	                                                        	if($(this).attr('data-input-type')){
	                                                        		if($(this).attr('data-input-type') == 'Currency'){
	                                                        			value = $(this).val().replace(/,/g, '');
	                                                        		}
	                                                        		else{
	                                                        			value = $(this).val();
	                                                        		}
	                                                        	}
	                                                        	else{
	                                                        		value = $(this).val();
	                                                        	}
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                                                        }
	                                                        else{
	                                                        	var value = "";
	                                                        	if($(this).attr('data-input-type')){
	                                                        		if($(this).attr('data-input-type') == 'Currency'){
	                                                        			value = $(this).val().replace(/,/g, '');
	                                                        		}
	                                                        		else{
	                                                        			value = $(this).val();
	                                                        		}
	                                                        	}
	                                                        	else{
	                                                        		value = $(this).val();
	                                                        	}
	                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
	                                                                "name": $(this).attr('name'),
	                                                                "field_input_type": $(this).attr('data-input-type')||"",
	                                                                "value": value
	                                                            }
	                                                            if($(this).attr('name') == 'Requestor'){
	                                                            	data['embed_row_data'][row_id][row_name_count]['fields_data']['requestor-name'] = {
	                                                            	    "type": undefined,
	                                                            	    "name": 'requestor-name',
	                                                            	    "value": $(this).attr('requestor-name')
	                                                            	}
	                                                            }	
	                                                        }
	                                                    }
	                                                });
													var keywords = ['CreatedBy','UpdatedBy','Unread','Node_ID','Workflow_ID','fieldEnabled','fieldRequired','fieldHiddenValues','imported','Repeater_Data','Editor','Viewer','middleware_process','ProcessorType','ProcessorLevel','SaveFormula','CancelFormula']
	                                                for(i in keywords){
		                                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][keywords[i]]) == 'undefined'){
		                                                	data['embed_row_data'][row_id][row_name_count]['fields_data'][keywords[i]] = {
		                                                		"type": undefined,
		                                                		"name": keywords[i],
		                                                		"value": opener.find('[name="' + keywords[i] + '"]').val()||""
		                                                	}
		                                                }
		                                                else{
		                                                	try{
		                                                		data['embed_row_data'][row_id][row_name_count]['fields_data'][keywords[i]]['value'] = opener.find('[name="' + keywords[i] + '"]').val()||"";
		                                                	}
		                                                	catch(e){
		                                                		data['embed_row_data'][row_id][row_name_count]['fields_data'][keywords[i]]['value'] = "";
		                                                	}
		                                                }
                                                	}
                                                    data['embed_row_data'][row_id][row_name_count]['submitted'] == false;
                                                    if(track_no != ""){
                                                    	emd_view.updateEmbedRow(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, record_id);
                                                    }
                                                    else{
                                                    	emd_view.addToEmbedList(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, type);
                                                    	emd_view.getSetTotalOfColumn($(embedded_view), $(embedded_view).children());
                                                    }	
                                                    embeded_view.getSetTotalOfColumn.call(embedded_view, embedded_view.children());
	                                                // setTimeout(function(){
	                                                //     var parameters = {
	                                                //         'form_id': row_id,
	                                                //     }
	                                                //     $.post('/ajax/get_fields_data', parameters, function(result){
	                                                //         var res = JSON.parse(result);
	                                                //         data['embed_row_data'][row_id][row_name_count]['fields_data']['LastAction'] = {
	                                                //             "type": $(this).parents('.setOBJ').attr('data-type'),
	                                                //             "name": 'LastAction',
	                                                //             "value": res['LastAction']
	                                                //         }
	                                                //     });
	                                                //     data['embed_row_data'][row_id][row_name_count]['submitted'] == false;
	                                                //     if(track_no != ""){
	                                                //     	emd_view.updateEmbedRow(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, record_id);
	                                                //     }
	                                                //     else{
	                                                //     	emd_view.addToEmbedList(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, type);
	                                                //     	emd_view.getSetTotalOfColumn($(embedded_view), $(embedded_view).children());
	                                                //     }	
	                                                //     embeded_view.getSetTotalOfColumn.call(embedded_view, embedded_view.children());
	                                                //     $('body').find('.fl-closeDialog').removeClass('close-confirmation');
	                                                //     $('body').find('.fl-closeDialog').trigger('click');
	                                                // }, 1000);
													window_dialog.close();
	                                            });
	                                        });
	                                    }
	                                }, 500);
	                            });
	                    	}
	                    	else{
		                        $('iframe').load(function(){
		                        	var track_no = $(this).attr('track_no') || "";
		                        	var record_id = $(this).attr('record-id') || "";
		                        	var popup_create = $(this);
		                            //load of new request from iframe
		                            // console.log("fl-action-submit",$(this).contents().find('[class="fl-action-submit"]'));
		                            $(this).contents().on('click', '.fl-action-submit', function(e){
		                                //when any button was click
		                                // alert('fl-action-submit clicked');
		                                var getPopup = setTimeout(function(){
		                                    // waiting until the dialog appears
		                                    // console.log("finding nemo");
		                                    if($('iframe').contents().find('[id="popup_ok"]').length > 0){
		                                    	// console.log("popup_ok found");
		                                    	clearInterval(getPopup);
		                                        var popup_ok = '<input type="button" class="fl-margin-right  btn-blueBtn fl-positive-btn" value="Yes" id="embed_okay">';
		                                        $('iframe').contents().find('[id="popup_ok"]').replaceWith(popup_ok);//replaece the submit button
		                                        $('iframe').contents().find('[id="embed_okay"]').on('click', function(e){//add event to the replaced button
		                                            var parameters = {
		                                                "form_id": embedded_view.attr('embed-source-form-val-id'),
		                                                "columns_needed": ["field_name", "field_type", "formula", "multiple_values", "other_attributes"],
		                                                "get_row_count": true
		                                            }
		                                            $.post('/ajax/embed_request', parameters, function(echo_result){
		                                                var result = JSON.parse(echo_result);
		                                                var data = $('body').data();
		                                                if(!data['embed_row_data']){
		                                                    data['embed_row_data'] = {};
		                                                }
		                                                row_id = embedded_view.closest('.setOBJ').attr('data-object-id');
		                                                if(!data['embed_row_data'][row_id]){
		                                                    data['embed_row_data'][row_id] = {};
		                                                }
		                                                var get_count_current_process_row = $('tr[is-pending="true"]').length||0;
		                                                get_count_current_process_row += 1;
		                                                var row_name_count = embedded_view.closest('.setOBJ').attr('data-object-id') + '_' + (Number(result['row_count']) + Number(get_count_current_process_row));
		                                                console.log("row_name", row_name_count);
		                                                data['embed_row_data'][row_id][row_name_count] = {
		                                                    "query_type": type,
		                                                    "origin": method,
		                                                    "saved": true,
		                                                    "form_id": embedded_view.attr('embed-source-form-val-id'),
		                                                    "fields_data": {}
		                                                };
		                                                var get_fields  = $('iframe').contents().find('.getFields');
		                                                get_fields.each(function(){
		                                                    if($(this).attr('name') == "TrackNo"){
		                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
		                                                            // data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = "pe"
		                                                        }
		                                                        else{
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
		                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
		                                                                "name": $(this).attr('name'),
		                                                                "value": 'pending...'
		                                                            }
		                                                        }
		                                                    }
		                                                    else if($(this).parents('.setOBJ').attr('data-type') == "pickList"){
		                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
		                                                            var value = "";
		                                                            if($(this).attr('data-input-type')){
		                                                            	if($(this).attr('data-input-type') == 'Currency'){
		                                                            		value = $(this).val().replace(/,/g, '');
		                                                            	}
		                                                            	else{
		                                                            		value = $(this).val();
		                                                            	}
		                                                            }
		                                                            else{
		                                                            	value = $(this).val();
		                                                            }
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
		                                                        }
		                                                        else{
		                                                        	var value = "";
		                                                        	if($(this).attr('data-input-type')){
		                                                        		if($(this).attr('data-input-type') == 'Currency'){
		                                                        			value = $(this).val().replace(/,/g, '');
		                                                        		}
		                                                        		else{
		                                                        			value = $(this).val();
		                                                        		}
		                                                        	}
		                                                        	else{
		                                                        		value = $(this).val();
		                                                        	}
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
		                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
		                                                                "name": $(this).attr('name'),
		                                                                "id": $(this).attr('id'),
		                                                                "picklist_button_id": $(this).parents('.setOBJ').find('a').attr('picklist-button-id'),
		                                                                "picklist_type": $(this).parents('.setOBJ').find('a').attr('picklist-type'),
		                                                                "return_field": $(this).parents('.setOBJ').find('a').attr('return-field'),
		                                                                "form_id": $(this).parents('.setOBJ').find('a').attr('form-id'),
		                                                                "return_field_name": $(this).parents('.setOBJ').find('a').attr('return-field-name'),
		                                                                "formname": $(this).parents('.setOBJ').find('a').attr('formname'),
		                                                                "display_columns": $(this).parents('.setOBJ').find('a').attr('display_columns'),
		                                                                "display_column_sequence": $(this).parents('.setOBJ').find('a').attr('display_column_sequence'),
		                                                                "enable_add_entry": $(this).parents('.setOBJ').find('a').attr('enable-add-entry'),
		                                                                "allow_values_not_in_list": $(this).parents('.setOBJ').find('a').attr('allow-values-not-in-list'),
		                                                                "show_picklist_view": $(this).parents('.setOBJ').find('a').attr('show-picklist-view'),
		                                                                "field_input_type": $(this).attr('data-input-type')||"",
		                                                                "value": value
		                                                            }
		                                                        }
		                                                    }
		                                                    else if($(this).parents('.setOBJ').attr('data-type') == "radioButton"){
		                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
		                                                            var get_id = $(this).attr('name');
		                                                            var get_selected = $('iframe').contents().find('[name="' + get_id + '"]:checked').val();
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = get_selected;
		                                                        }
		                                                        else{
		                                                            var options = $(this).parents('.setOBJ').find('.getFields').map(function(){
		                                                                return $(this).val();
		                                                            }).get();
		                                                            var get_id = $(this).attr('name');
		                                                            var get_selected = $('iframe').contents().find('[name="' + get_id + '"]:checked').val();
		                                                            // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
		                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
		                                                                "name": $(this).attr('name'),
		                                                                "id": $(this).attr('id'),
		                                                                "options": options,
		                                                                "field_input_type": $(this).attr('data-input-type')||"",
		                                                                "value": get_selected
		                                                            
		                                                            }
		                                                        }
		                                                    }
		                                                    else if($(this).parents('.setOBJ').attr('data-type') == "checkbox"){
		                                                        var field_name = $(this).attr('name').replace(/\[\]/g,'');
		                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
		                                                            var get_id = $(this).attr('name');
		                                                            var get_selected = $('iframe').contents().find('[name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
		                                                        }
		                                                        else{
		                                                            var options = $(this).parents('.setOBJ').find('.getFields').map(function(){
		                                                                return $(this).val();
		                                                            }).get();
		                                                            var get_id = $(this).attr('name');
		                                                            var get_selected = $('iframe').contents().find('[name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
		                                                            // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
		                                                            
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
		                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
		                                                                "name": field_name,
		                                                                "id": $(this).attr('id'),
		                                                                "options": options,
		                                                                "field_input_type": $(this).attr('data-input-type')||"",
		                                                                "value": get_selected
		                                                            }
		                                                        }
		                                                    }
		                                                    else if($(this).parents('.setOBJ').attr('data-type') == "selectMany"){
		                                                        var field_name = $(this).attr('name').replace(/\[\]/g,'');
		                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
		                                                            var get_id = $(this).attr('name');
		                                                            var get_selected = $('iframe').contents().find('[name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
		                                                        }   
		                                                        else{
		                                                            var options = $(this).parents('.setOBJ').find('option').map(function(){
		                                                                return $(this).val();
		                                                            }).get();
		                                                            var get_id = $(this).attr('name');
		                                                            var get_selected = $('iframe').contents().find('[name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
		                                                            // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
		                                                            var field_name = $(this).attr('name').replace(/\[\]/g,'');
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
		                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
		                                                                "name": field_name,
		                                                                "id": $(this).attr('id'),
		                                                                "options": options,
		                                                                "field_input_type": $(this).attr('data-input-type')||"",
		                                                                "value": get_selected
		                                                            }
		                                                        }
		                                                    }
		                                                    else if($(this).parents('.setOBJ').attr('data-type') == "dropdown"){
		                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = $(this).val();
		                                                        }
		                                                        else{
		                                                            var options = $(this).parents('.setOBJ').find('option').map(function(){
		                                                                return $(this).val();
		                                                            }).get();
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
		                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
		                                                                "name": $(this).attr('name'),
		                                                                "id": $(this).attr('id'),
		                                                                "options": options,
		                                                                "field_input_type": $(this).attr('data-input-type')||"",
		                                                                "value": $(this).val()
		                                                            }
		                                                        }
		                                                    }
		                                                    else if($(this).parents('.setOBJ').attr('data-type') == "listNames"){
		                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
		                                                            var value = "";
		                                                            if($(this).attr('data-input-type')){
		                                                            	if($(this).attr('data-input-type') == 'Currency'){
		                                                            		value = $(this).val().replace(/,/g, '');
		                                                            	}
		                                                            	else{
		                                                            		value = $(this).val();
		                                                            	}
		                                                            }
		                                                            else{
		                                                            	value = $(this).val();
		                                                            }
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
		                                                        }
		                                                        else{
		                                                        	var value = "";
		                                                        	if($(this).attr('data-input-type')){
		                                                        		if($(this).attr('data-input-type') == 'Currency'){
		                                                        			value = $(this).val().replace(/,/g, '');
		                                                        		}
		                                                        		else{
		                                                        			value = $(this).val();
		                                                        		}
		                                                        	}
		                                                        	else{
		                                                        		value = $(this).val();
		                                                        	}
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
		                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
		                                                                "name": $(this).attr('name'),
		                                                                "id": $(this).attr('id'),
		                                                                "return_field": $(this).attr('id'),
		                                                                "data_original_title": $(this).parents('.setOBJ').find('a').attr('data-original-title'),
		                                                                "field_input_type": $(this).attr('data-input-type')||"",
		                                                                "value": value
		                                                            }
		                                                        }
		                                                    }
		                                                    else if($(this).parents('.setOBJ').attr('data-type') == "attachment_on_request"){
		                                                    	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
		                                                    		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
		                                                    	}
		                                                    	else{
		                                                    		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
		                                                    			"type": $(this).parents('.setOBJ').attr('data-type'),
		                                                    			"name": $(this).attr('name'),
		                                                    			"id": $(this).attr('id'),
		                                                    			"obj_id": $(this).parents('.setOBJ').attr('data-object-id'),
		                                                    			"form_id": $(this).prev().attr('id'),
		                                                    			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
		                                                    			"form_data_id": $(this).prev().children().eq(0).attr('id'),
		                                                    			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
		                                                    			"value": $(this).val()
		                                                    		}
		                                                    	}
		                                                    }
		                                                    else if($(this).parents('.setOBJ').attr('data-type') == "multiple_attachment_on_request"){
		                                                    	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
		                                                    		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
		                                                    	}
		                                                    	else{
		                                                    		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
		                                                    			"type": $(this).parents('.setOBJ').attr('data-type'),
		                                                    			"name": $(this).attr('name'),
		                                                    			"id": $(this).attr('id'),
		                                                    			"obj_id": $(this).parents('.setOBJ').attr('data-object-id'),
		                                                    			"form_id": $(this).prev().attr('id'),
		                                                    			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
		                                                    			"form_data_id": $(this).prev().children().eq(0).attr('id'),
		                                                    			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
		                                                    			"form_data_attachment": $(this).attr('data-attachment'),
		                                                    			"value": $(this).val()
		                                                    		}
		                                                    	}
		                                                    }
		                                                    else{
		                                                        if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
		                                                        	var value = "";
		                                                        	if($(this).attr('data-input-type')){
		                                                        		if($(this).attr('data-input-type') == 'Currency'){
		                                                        			value = $(this).val().replace(/,/g, '');
		                                                        		}
		                                                        		else{
		                                                        			value = $(this).val();
		                                                        		}
		                                                        	}
		                                                        	else{
		                                                        		value = $(this).val();
		                                                        	}
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
		                                                        }
		                                                        else{
		                                                        	var value = "";
		                                                        	if($(this).attr('data-input-type')){
		                                                        		if($(this).attr('data-input-type') == 'Currency'){
		                                                        			value = $(this).val().replace(/,/g, '');
		                                                        		}
		                                                        		else{
		                                                        			value = $(this).val();
		                                                        		}
		                                                        	}
		                                                        	else{
		                                                        		value = $(this).val();
		                                                        	}
		                                                            data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
		                                                                "type": $(this).parents('.setOBJ').attr('data-type'),
		                                                                "name": $(this).attr('name'),
		                                                                "field_input_type": $(this).attr('data-input-type')||"",
		                                                                "value": value
		                                                            }
		                                                            if($(this).attr('name') == 'Requestor'){
		                                                            	data['embed_row_data'][row_id][row_name_count]['fields_data']['requestor-name'] = {
		                                                            	    "type": undefined,
		                                                            	    "name": 'requestor-name',
		                                                            	    "value": $(this).attr('requestor-name')
		                                                            	}
		                                                            }	
		                                                        }
		                                                    }
		                                                });
														var keywords = ['CreatedBy','UpdatedBy','Unread','Node_ID','Workflow_ID','fieldEnabled','fieldRequired','fieldHiddenValues','imported','Repeater_Data','Editor','Viewer','middleware_process','ProcessorType','ProcessorLevel','SaveFormula','CancelFormula']
		                                                for(i in keywords){
			                                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][keywords[i]]) == 'undefined'){
			                                                	data['embed_row_data'][row_id][row_name_count]['fields_data'][keywords[i]] = {
			                                                		"type": undefined,
			                                                		"name": keywords[i],
			                                                		"value": popup_create.contents().find('[name="' + keywords[i] + '"]').val()||""
			                                                	}
			                                                }
			                                                else{
			                                                	try{
			                                                		data['embed_row_data'][row_id][row_name_count]['fields_data'][keywords[i]]['value'] = popup_create.contents().find('[name="' + keywords[i] + '"]').val()||"";
			                                                	}
			                                                	catch(e){
			                                                		data['embed_row_data'][row_id][row_name_count]['fields_data'][keywords[i]]['value'] = "";
			                                                	}
			                                                }
	                                                	}
	                                                    data['embed_row_data'][row_id][row_name_count]['submitted'] == false;
	                                                    if(track_no != ""){
	                                                    	emd_view.updateEmbedRow(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, record_id);
	                                                    }
	                                                    else{
	                                                    	emd_view.addToEmbedList(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, type);
	                                                    	emd_view.getSetTotalOfColumn($(embedded_view), $(embedded_view).children());
	                                                    }	
	                                                    embeded_view.getSetTotalOfColumn.call(embedded_view, embedded_view.children());
		                                                $('body').find('.fl-closeDialog').removeClass('close-confirmation');
		                                                $('body').find('.fl-closeDialog').trigger('click');
		                                                // setTimeout(function(){
		                                                //     var parameters = {
		                                                //         'form_id': row_id,
		                                                //     }
		                                                //     $.post('/ajax/get_fields_data', parameters, function(result){
		                                                //         var res = JSON.parse(result);
		                                                //         data['embed_row_data'][row_id][row_name_count]['fields_data']['LastAction'] = {
		                                                //             "type": $(this).parents('.setOBJ').attr('data-type'),
		                                                //             "name": 'LastAction',
		                                                //             "value": res['LastAction']
		                                                //         }
		                                                //     });
		                                                //     data['embed_row_data'][row_id][row_name_count]['submitted'] == false;
		                                                //     if(track_no != ""){
		                                                //     	emd_view.updateEmbedRow(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, record_id);
		                                                //     }
		                                                //     else{
		                                                //     	emd_view.addToEmbedList(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, type);
		                                                //     	emd_view.getSetTotalOfColumn($(embedded_view), $(embedded_view).children());
		                                                //     }	
		                                                //     embeded_view.getSetTotalOfColumn.call(embedded_view, embedded_view.children());
		                                                //     $('body').find('.fl-closeDialog').removeClass('close-confirmation');
		                                                //     $('body').find('.fl-closeDialog').trigger('click');
		                                                // }, 1000);
		                                            });
		                                        });
		                                    }
		                                }, 500);
		                            });
		                        });
							}
	                    }
	                }
	                else{
	                    var data = $('body').data();
	                    row_id = embedded_view.closest('.setOBJ').attr('data-object-id');
	                    var row_name_count = "";
	                    if(tr_reference){
	                    	row_name_count = tr_reference;
	                    }
	                    else{
	                    	row_name_count = embedded_view.closest('.setOBJ').attr('data-object-id') + '_' + $('tr[current]').attr('record-id');
	                    }
	                    console.log("tr_reference", tr_reference);
	                    var record_id = row_name_count.split("_")[1];
	                    console.log("record_id", record_id);
	                    if(!data['embed_row_data']){
	                        console.log("new embed_row_data");
	                        data['embed_row_data'] = {};
	                    }
	                    if(!data['embed_row_data'][row_id]){
	                        console.log("new embed_row_data row");
	                        data['embed_row_data'][row_id] = {};
	                    }
	                    if(!data['embed_row_data'][row_id][row_name_count]){
	                        data['embed_row_data'][row_id][row_name_count] = {
	                            "query_type": type,
	                            "origin": method,
	                            "saved": false,
	                            "form_id": embedded_view.attr('embed-source-form-val-id'),
	                            "fields_data": {}
	                        };
	                    }
	                    var get_fields  = $('[reference-id="' + row_name_count + '"]').find('.getFields,.embed_getFields');
	                    if(method == "in-line"){
	                        var get_data_type = function(field_name){
	                            var getTheDataTypes = {};
	                            if(field_name == ""){
	                                $(embedded_view).find('.columnHeader').map(function(){
	                                    getTheDataTypes[$(this).attr('data-fld-name')] = $(this).attr('data-fld-name');
	                                });
	                            }
	                            else{
	                                $(embedded_view).find('.columnHeader').map(function(){
	                                    getTheDataTypes[$(this).attr('data-fld-name')] = $(this).attr('data-field-object-type');
	                                });
	                                
	                            }
	                            return getTheDataTypes[field_name];
	                        }
	                        get_fields.each(function(){
	                            console.log("field_name", $(this).attr('data-field-name'));
	                            var get_field_name = $(this).attr('data-field-name').replace(/\[\]/g, '')
	                            if(get_data_type(get_field_name) == "pickList"){
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                    var value = ""
	                                    if($(this).attr('equivalent-fld-input-type')){
	                                    	if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                    		value = $(this).val().replace(/,/g, '');
	                                    	}
	                                    	else{
	                                    		value = $(this).val();
	                                    	}
	                                    }
	                                    else{
	                                    	value = $(this).val();
	                                    }
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = value;
	                                }
	                                else{
	                                	var value = ""
	                                	if($(this).attr('equivalent-fld-input-type')){
	                                		if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                			value = $(this).val().replace(/,/g, '');
	                                		}
	                                		else{
	                                			value = $(this).val();
	                                		}
	                                	}
	                                	else{
	                                		value = $(this).val();
	                                	}
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "picklist_button_id": $(this).parents('td').find('a').attr('picklist-button-id'),
	                                        "picklist_type": $(this).parents('td').find('a').attr('picklist-type'),
	                                        "return_field": $(this).parents('td').find('a').attr('return-field'),
	                                        "form_id": $(this).parents('td').find('a').attr('form-id'),
	                                        "return_field_name": $(this).parents('td').find('a').attr('return-field-name'),
	                                        "formname": $(this).parents('td').find('a').attr('formname'),
	                                        "display_columns": $(this).parents('td').find('a').attr('display_columns'),
	                                        "display_column_sequence": $(this).parents('td').find('a').attr('display_column_sequence'),
	                                        "enable_add_entry": $(this).parents('td').find('a').attr('enable-add-entry'),
	                                        "allow_values_not_in_list": $(this).parents('td').find('a').attr('allow-values-not-in-list'),
	                                        "show_picklist_view": $(this).parents('td').find('a').attr('show-picklist-view'),
	                                        "field_input_type": $(this).attr('equivalent-fld-input-type')||"",
	                                        "value": value
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "radioButton"){
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('td').find('[data-field-name="' + get_id + '"]:checked').val();
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = get_selected;
	                                }
	                                else{
	                                    var options = $(this).parents('td').find('.embed_getFields').map(function(){
	                                        return $(this).val();
	                                    }).get();
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('tr').find('[data-field-name="' + get_id + '"]:checked').val();
	                                    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "options": options,
	                                        "field_input_type": $(this).attr('equivalent-fld-input-type')||"",
	                                        "value": get_selected
	                                    
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "checkbox"){
	                                var field_name = $(this).attr('data-field-name').replace(/\[\]/g,'');
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('tr').find('[data-field-name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
	                                }
	                                else{
	                                    var options = $(this).parents('td').find('.embed_getFields').map(function(){
	                                        return $(this).val();
	                                    }).get();
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('tr').find('[data-field-name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
	                                    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
	                                    
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "options": options,
	                                        "field_input_type": $(this).attr('equivalent-fld-input-type')||"",
	                                        "value": get_selected
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "selectMany"){
	                                var field_name = $(this).attr('data-field-name').replace(/\[\]/g,'');
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('td').find('[data-field-name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
	                                }
	                                else{
	                                    var options = $(this).parents('td').find('option').map(function(){
	                                        return $(this).val();
	                                    }).get();
	                                    var get_id = $(this).attr('data-field-name');
	                                    var get_selected = $(this).parents('td').find('[data-field-name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
	                                    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
	                                    
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "options": options,
	                                        "field_input_type": $(this).attr('equivalent-fld-input-type')||"",
	                                        "value": get_selected
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "dropdown"){
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = $(this).val();
	                                }
	                                else{
	                                    var options = $(this).parents('td').find('option').map(function(){
	                                        return $(this).val();
	                                    }).get();
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "options": options,
	                                        "field_input_type": $(this).attr('equivalent-fld-input-type')||"",
	                                        "value": $(this).val()
	                                    }
	                                }
	                            }
	                            else if(get_data_type(get_field_name) == "listNames"){
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                    var value = ""
	                                    if($(this).attr('equivalent-fld-input-type')){
	                                    	if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                    		value = $(this).val().replace(/,/g, '');
	                                    	}
	                                    	else{
	                                    		value = $(this).val();
	                                    	}
	                                    }
	                                    else{
	                                    	value = $(this).val();
	                                    }
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = value;
	                                }
	                                else{
	                                	var value = ""
	                                	if($(this).attr('equivalent-fld-input-type')){
	                                		if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                			value = $(this).val().replace(/,/g, '');
	                                		}
	                                		else{
	                                			value = $(this).val();
	                                		}
	                                	}
	                                	else{
	                                		value = $(this).val();
	                                	}
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "id": $(this).attr('id'),
	                                        "return_field": $(this).attr('id'),
	                                        "data_original_title": $(this).parents('td').find('a').attr('data-original-title'),
	                                        "field_input_type": $(this).attr('equivalent-fld-input-type')||"",
	                                        "value": value
	                                    }
	                                }

	                            }
	                            else if(get_data_type(get_field_name) == "attachment_on_request"){
	                            	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = $(this).val();
	                            	}
	                            	else{
	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                            			"type": get_data_type(get_field_name),
	                            			"name": $(this).attr('data-field-name'),
	                            			"id": $(this).attr('id'),
	                            			"obj_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                            			"form_id": $(this).prev().attr('id'),
	                            			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                            			"form_data_id": $(this).prev().children().eq(0).attr('id'),
	                            			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
	                            			"value": $(this).val()
	                            		}
	                            	}
	                            }
	                            else if(get_data_type(get_field_name) == "multiple_attachment_on_request"){
	                            	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = $(this).val();
	                            	}
	                            	else{
	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                            			"type": get_data_type(get_field_name),
	                            			"name": $(this).attr('data-field-name'),
	                            			"id": $(this).attr('id'),
	                            			"obj_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                            			"form_id": $(this).prev().attr('id'),
	                            			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                            			"form_data_id": $(this).prev().children().eq(0).attr('id'),
	                            			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
	                            			"value": JSON.parse($(this).val())
	                            		}
	                            	}
	                            }
	                            else{
	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]) != "undefined"){
	                                    var value = ""
	                                    if($(this).attr('equivalent-fld-input-type')){
	                                    	if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                    		value = $(this).val().replace(/,/g, '');
	                                    	}
	                                    	else{
	                                    		value = $(this).val();
	                                    	}
	                                    }
	                                    else{
	                                    	value = $(this).val();
	                                    }
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')]['value'] = value;
	                                }
	                                else{
	                                	var value = ""
	                                	if($(this).attr('equivalent-fld-input-type')){
	                                		if($(this).attr('equivalent-fld-input-type') == "Currency"){
	                                			value = $(this).val().replace(/,/g, '');
	                                		}
	                                		else{
	                                			value = $(this).val();
	                                		}
	                                	}
	                                	else{
	                                		value = $(this).val();
	                                	}
	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('data-field-name')] = {
	                                        "type": get_data_type(get_field_name),
	                                        "name": $(this).attr('data-field-name'),
	                                        "field_input_type": $(this).attr('equivalent-fld-input-type')||"",
	                                        "value": value
	                                    }
	                                }
	                            }
	                        });
	                        data['embed_row_data'][row_id][row_name_count]['fields_data']['TrackNo'] = {
	                            "type": undefined,
	                            "name": "TrackNo",
	                            "value": $('tr[current]').attr('record-trackno')
	                        }
	                        data['embed_row_data'][row_id][row_name_count]['fields_data']['ID'] = {
	                            "type": undefined,
	                            "name": "TrackNo",
	                            "value": $('tr[current]').attr('record-id')
	                        }
	                        data['embed_row_data'][row_id][row_name_count]['fields_data']['fieldEnabled'] = {
	                            "type": undefined,
	                            "name": "fieldEnabled",
	                            "value": JSON.stringify($.map(JSON.parse(embedded_view.attr('data-embed-sortedcolumn'))['sortedcols'], function(value, index){ return value['FieldName']; }))
	                        }
	                        var parameters = {
	                            'form_id': row_id,
	                            "record_id": embedded_view.find('tr[reference-id="' + row_name_count + '"]').attr('record-id')
	                        }
	                        // console.log("parameters", parameters);
	                        console.log("multiple_on_attachment....", data['embed_row_data'][row_id][row_name_count]['fields_data']);
	                        $.post('/ajax/get_fields_data', parameters, function(result){
	                        	// console.log(result);
	                            var res = JSON.parse(result);
	                            for(i in res['tbl_values'][0]){
	                            	if(i != "other_details"){
	                            		if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][i]) == "undefined"){
	                            			data['embed_row_data'][row_id][row_name_count]['fields_data'][i] = {
	                            			    "type": $(this).parents('.setOBJ').attr('data-type'),
	                            			    "name": i,
	                            			    "value": res['tbl_values'][0][i]
	                            			}
	                            		}
	                            	}
	                            }
	                            // alert($.type(callBack));
	                            // if(callBack){
	                            // 	alert(123);	
	                            // 	callBack();
	                            // }
	                            data['embed_row_data'][row_id][row_name_count]['fields_data']['submitted'] == false;
	                            console.log("multiple_on_attachment....", data['embed_row_data'][row_id][row_name_count]['fields_data']);
	                            emd_view.updateEmbedRow(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, record_id);
	                        });
							
	                    }
	                    else{
	                        if(window_dialog){
	                        	var opener = $(window_dialog.document);
                        		var track_no = $(this).attr('track_no') || "";
                        		var record_id = $(this).attr('record-id') || "";
                        	    //load of new request from iframe
                        	    opener.on('click', '.fl-action-submit', function(e){
                        	        //when any button was click
                        	        var getPopup = setInterval(function(){
                        	            // waiting until the dialog appears
                        	            if(opener.find('[id="popup_ok"]').length > 0){
                        	                var popup_ok = '<input type="button" class="fl-margin-right  btn-blueBtn fl-positive-btn" value="Yes" id="embed_okay">';
                        	                opener.find('[id="popup_ok"]').replaceWith(popup_ok);//replaece the submit button
                        	                opener.find('[id="embed_okay"]').on('click', function(e){//add event to the replaced button
                        	                    var parameters = {
                        	                        "form_id": embedded_view.attr('embed-source-form-val-id'),
                        	                        "columns_needed": ["field_name", "field_type", "formula", "multiple_values", "other_attributes"],
                        	                        "get_row_count": true
                        	                    }
                        	                    $.post('/ajax/embed_request', parameters, function(echo_result){
                        	                        var result = JSON.parse(echo_result);
                        	                        console.log(echo_result);
                        	                        var data = $('body').data();
                        	                        if(!data['embed_row_data']){
                        	                            data['embed_row_data'] = {};
                        	                        }
                        	                        row_id = embedded_view.closest('.setOBJ').attr('data-object-id');
                        	                        if(!data['embed_row_data'][row_id]){
                        	                            data['embed_row_data'][row_id] = {};
                        	                        }
                        	                        var get_count_current_process_row = $('tr[is-pending="true"]').length||0;
                        	                        get_count_current_process_row += 1;
                        	                        var row_name_count = "";
                        	                        if(tr_reference){
                        	                        	row_name_count = tr_reference;
                        	                        }
                        	                        else{
                        	                        	row_name_count = embedded_view.closest('.setOBJ').attr('data-object-id') + '_' + (Number(result['row_count']) + Number(get_count_current_process_row));
                        	                        }
                        	                        console.log("row_name", row_name_count, "tr_reference", tr_reference);
                        	                        data['embed_row_data'][row_id][row_name_count] = {
                        	                            "query_type": type,
                        	                            "origin": method,
                        	                            "saved": true,
                        	                            "form_id": embedded_view.attr('embed-source-form-val-id'),
                        	                            "fields_data": {}
                        	                        };
                        	                        var get_fields  = opener.find('.getFields');
                        	                        get_fields.each(function(){
                        	                            if($(this).attr('name') == "TrackNo"){
                        	                                data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
                        	                                    "type": $(this).parents('.setOBJ').attr('data-type'),
                        	                                    "name": $(this).attr('name'),
                        	                                    "value": $(this).val()
                        	                                }
                        	                            }
                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "pickList"){
                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
                        	                                        var value = "";
                        	                                        if($(this).attr('data-input-type')){
                        	                                        	if($(this).attr('data-input-type') == "Currency"){
                        	                                        		value = $(this).val().replace(/,/g, '');
                        	                                        	}
                        	                                        	else{
                        	                                        		value = $(this).val();
                        	                                        	}
                        	                                        }
                        	                                        else{
                        	                                        	value = $(this).val();
                        	                                        }
                        	                                        data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
                        	                                }
                        	                                else{
                        	                                	var value = "";
                        	                                	if($(this).attr('data-input-type')){
                        	                                		if($(this).attr('data-input-type') == "Currency"){
                        	                                			value = $(this).val().replace(/,/g, '');
                        	                                		}
                        	                                		else{
                        	                                			value = $(this).val();
                        	                                		}
                        	                                	}
                        	                                	else{
                        	                                		value = $(this).val();
                        	                                	}
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
                        	                                        "name": $(this).attr('name'),
                        	                                        "id": $(this).attr('id'),
                        	                                        "picklist_button_id": $(this).parents('.setOBJ').find('a').attr('picklist-button-id'),
                        	                                        "picklist_type": $(this).parents('.setOBJ').find('a').attr('picklist-type'),
                        	                                        "return_field": $(this).parents('.setOBJ').find('a').attr('return-field'),
                        	                                        "form_id": $(this).parents('.setOBJ').find('a').attr('form-id'),
                        	                                        "return_field_name": $(this).parents('.setOBJ').find('a').attr('return-field-name'),
                        	                                        "formname": $(this).parents('.setOBJ').find('a').attr('formname'),
                        	                                        "display_columns": $(this).parents('.setOBJ').find('a').attr('display_columns'),
                        	                                        "display_column_sequence": $(this).parents('.setOBJ').find('a').attr('display_column_sequence'),
                        	                                        "enable_add_entry": $(this).parents('.setOBJ').find('a').attr('enable-add-entry'),
                        	                                        "allow_values_not_in_list": $(this).parents('.setOBJ').find('a').attr('allow-values-not-in-list'),
                        	                                        "show_picklist_view": $(this).parents('.setOBJ').find('a').attr('show-picklist-view'),
                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
                        	                                        "value": value
                        	                                    }
                        	                                }
                        	                            }
                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "radioButton"){
                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
                        	                                    var get_id = $(this).attr('name');
                        	                                    var get_selected = opener.find('[name="' + get_id + '"]:checked').val();
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = get_selected;
                        	                                }
                        	                                else{
                        	                                    var options = $(this).parents('.setOBJ').find('.getFields').map(function(){
                        	                                        return $(this).val();
                        	                                    }).get();
                        	                                    var get_id = $(this).attr('name');
                        	                                    var get_selected = opener.find('[name="' + get_id + '"]:checked').val();
                        	                                    // console.log("options", options, "getid", get_id, "get_selected", opener.find('[name="' + get_id + '"]').filter(':checked').val());
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
                        	                                        "name": $(this).attr('name'),
                        	                                        "id": $(this).attr('id'),
                        	                                        "options": options,
                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
                        	                                        "value": get_selected
                        	                                    }
                        	                                }
                        	                            }
                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "checkbox"){
                        	                                var field_name = $(this).attr('name').replace(/\[\]/g,'');
                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
                        	                                    var get_id = $(this).attr('name');
                        	                                    var get_selected = opener.find('[name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
                        	                                }
                        	                                else{
                        	                                    var options = $(this).parents('.setOBJ').find('.getFields').map(function(){
                        	                                        return $(this).val();
                        	                                    }).get();
                        	                                    var get_id = $(this).attr('name');
                        	                                    var get_selected = opener.find('[name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
                        	                                    // console.log("options", options, "getid", get_id, "get_selected", opener.find('[name="' + get_id + '"]').filter(':checked').val());
                        	                                    
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
                        	                                        "name": field_name,
                        	                                        "id": $(this).attr('id'),
                        	                                        "options": options,
                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
                        	                                        "value": get_selected
                        	                                    }
                        	                                }
                        	                            }
                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "selectMany"){
                        	                                var field_name = $(this).attr('name').replace(/\[\]/g,'');
                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
                        	                                    var get_id = $(this).attr('name');
                        	                                    var get_selected = opener.find('[name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
                        	                                }
                        	                                else{
                        	                                    var options = $(this).parents('.setOBJ').find('option').map(function(){
                        	                                        return $(this).val();
                        	                                    }).get();
                        	                                    var get_id = $(this).attr('name');
                        	                                    var get_selected = opener.find('[name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
                        	                                    // console.log("options", options, "getid", get_id, "get_selected", opener.find('[name="' + get_id + '"]').filter(':checked').val());
                        	                                    
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
                        	                                        "name": field_name,
                        	                                        "id": $(this).attr('id'),
                        	                                        "options": options,
                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
                        	                                        "value": get_selected
                        	                                    }
                        	                                }
                        	                            }
                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "dropdown"){
                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = $(this).val();
                        	                                }
                        	                                else{
                        	                                    var options = $(this).parents('.setOBJ').find('option').map(function(){
                        	                                        return $(this).val();
                        	                                    }).get();
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
                        	                                        "name": $(this).attr('name'),
                        	                                        "id": $(this).attr('id'),
                        	                                        "options": options,
                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
                        	                                        "value": $(this).val()
                        	                                    }
                        	                                }
                        	                            }
                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "listNames"){
                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
                        	                                    var value = "";
                        	                                    if($(this).attr('data-input-type')){
                        	                                    	if($(this).attr('data-input-type') == "Currency"){
                        	                                    		value = $(this).val().replace(/,/g, '');
                        	                                    	}
                        	                                    	else{
                        	                                    		value = $(this).val();
                        	                                    	}
                        	                                    }
                        	                                    else{
                        	                                    	value = $(this).val();
                        	                                    }
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
                        	                                }
                        	                                else{
                        	                                	var value = "";
                        	                                	if($(this).attr('data-input-type')){
                        	                                		if($(this).attr('data-input-type') == "Currency"){
                        	                                			value = $(this).val().replace(/,/g, '');
                        	                                		}
                        	                                		else{
                        	                                			value = $(this).val();
                        	                                		}
                        	                                	}
                        	                                	else{
                        	                                		value = $(this).val();
                        	                                	}
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
                        	                                        "name": $(this).attr('name'),
                        	                                        "id": $(this).attr('id'),
                        	                                        "return_field": $(this).attr('id'),
                        	                                        "data_original_title": $(this).parents('.setOBJ').find('a').attr('data-original-title'),
                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
                        	                                        "value": value
                        	                                    }
                        	                                }
                        	                            }
                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "attachment_on_request"){
                        	                            	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
                        	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
                        	                            	}
                        	                            	else{
                        	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
                        	                            			"type": $(this).parents('.setOBJ').attr('data-type'),
                        	                            			"name": $(this).attr('name'),
                        	                            			"id": $(this).attr('id'),
                        	                            			"obj_id": $(this).parents('.setOBJ').attr('data-object-id'),
                        	                            			"form_id": $(this).prev().attr('id'),
                        	                            			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
                        	                            			"form_data_id": $(this).prev().children().eq(0).attr('id'),
                        	                            			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
                        	                            			"value": $(this).val()
                        	                            		}
                        	                            	}
                        	                            }
                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "multiple_attachment_on_request"){
                        	                            	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
                        	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
                        	                            	}
                        	                            	else{
                        	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
                        	                            			"type": $(this).parents('.setOBJ').attr('data-type'),
                        	                            			"name": $(this).attr('name'),
                        	                            			"id": $(this).attr('id'),
                        	                            			"obj_id": $(this).parents('.setOBJ').attr('data-object-id'),
                        	                            			"form_id": $(this).prev().attr('id'),
                        	                            			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
                        	                            			"form_data_id": $(this).prev().children().eq(0).attr('id'),
                        	                            			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
                        	                            			"form_data_attachment": $(this).attr('data-attachment'),
                        	                            			"value": $(this).val()
                        	                            		}
                        	                            	}
                        	                            }
                        	                            else{
                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
                        	                                    var value = "";
                        	                                    if($(this).attr('data-input-type')){
                        	                                    	if($(this).attr('data-input-type') == "Currency"){
                        	                                    		value = $(this).val().replace(/,/g, '');
                        	                                    	}
                        	                                    	else{
                        	                                    		value = $(this).val();
                        	                                    	}
                        	                                    }
                        	                                    else{
                        	                                    	value = $(this).val();
                        	                                    }
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
                        	                                }   
                        	                                else{
                        	                                	var value = "";
                        	                                	if($(this).attr('data-input-type')){
                        	                                		if($(this).attr('data-input-type') == "Currency"){
                        	                                			value = $(this).val().replace(/,/g, '');
                        	                                		}
                        	                                		else{
                        	                                			value = $(this).val();
                        	                                		}
                        	                                	}
                        	                                	else{
                        	                                		value = $(this).val();
                        	                                	}
                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
                        	                                        "name": $(this).attr('name'),
                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
                        	                                        "value": value
                        	                                    }
                        	                                    if($(this).attr('name') == 'Requestor'){
                        	                                    	data['embed_row_data'][row_id][row_name_count]['fields_data']['requestor-name'] = {
                        	                                    	    "type": undefined,
                        	                                    	    "name": 'requestor-name',
                        	                                    	    "value": $(this).attr('requestor-name')
                        	                                    	}
                        	                                    }	
                        	                                }

                        	                            }
                        	                        });
                        	                        setTimeout(function(){
                        	                            var parameters = {
                        	                                'form_id': row_id,
                        	                                "record_id": embedded_view.find('tr[on-edit]').attr('record-id')
                        	                            }
                        	                            $.post('/ajax/get_fields_data', parameters, function(result){
                        	                                var res = JSON.parse(result);
                        	                                data['embed_row_data'][row_id][row_name_count]['fields_data']['LastAction'] = {
                        	                                    "type": $(this).parents('.setOBJ').attr('data-type'),
                        	                                    "name": 'LastAction',
                        	                                    "value": res['LastAction']
                        	                                }
                        	                            });
                        	                            emd_view.addToEmbedList(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, type);
                        	                            emd_view.getSetTotalOfColumn.call(embedded_view, embedded_view.children());
                        	                            $('body').find('.fl-closeDialog').removeClass('close-confirmation');
                        	                            $('body').find('.fl-closeDialog').trigger('click');
                        	                        }, 1000);
                        	                    });
                        	                });
                        	                clearInterval(getPopup);
                        	            }
                        	        }, 0);
                        	    });
	                        }
	                        else{
	                        	$('iframe').load(function(){
	                        		var track_no = $(this).attr('track_no') || "";
	                        		var record_id = $(this).attr('record-id') || "";
	                        	    //load of new request from iframe
	                        	    $(this).contents().on('click', '.fl-action-submit', function(e){
	                        	        //when any button was click
	                        	        var getPopup = setInterval(function(){
	                        	            // waiting until the dialog appears
	                        	            if($('iframe').contents().find('[id="popup_ok"]').length > 0){
	                        	                var popup_ok = '<input type="button" class="fl-margin-right  btn-blueBtn fl-positive-btn" value="Yes" id="embed_okay">';
	                        	                $('iframe').contents().find('[id="popup_ok"]').replaceWith(popup_ok);//replaece the submit button
	                        	                $('iframe').contents().find('[id="embed_okay"]').on('click', function(e){//add event to the replaced button
	                        	                    var parameters = {
	                        	                        "form_id": embedded_view.attr('embed-source-form-val-id'),
	                        	                        "columns_needed": ["field_name", "field_type", "formula", "multiple_values", "other_attributes"],
	                        	                        "get_row_count": true
	                        	                    }
	                        	                    $.post('/ajax/embed_request', parameters, function(echo_result){
	                        	                        var result = JSON.parse(echo_result);
	                        	                        console.log(echo_result);
	                        	                        var data = $('body').data();
	                        	                        if(!data['embed_row_data']){
	                        	                            data['embed_row_data'] = {};
	                        	                        }
	                        	                        row_id = embedded_view.closest('.setOBJ').attr('data-object-id');
	                        	                        if(!data['embed_row_data'][row_id]){
	                        	                            data['embed_row_data'][row_id] = {};
	                        	                        }
	                        	                        var get_count_current_process_row = $('tr[is-pending="true"]').length||0;
	                        	                        get_count_current_process_row += 1;
	                        	                        var row_name_count = "";
	                        	                        if(tr_reference){
	                        	                        	row_name_count = tr_reference;
	                        	                        }
	                        	                        else{
	                        	                        	row_name_count = embedded_view.closest('.setOBJ').attr('data-object-id') + '_' + (Number(result['row_count']) + Number(get_count_current_process_row));
	                        	                        }
	                        	                        console.log("row_name", row_name_count, "tr_reference", tr_reference);
	                        	                        data['embed_row_data'][row_id][row_name_count] = {
	                        	                            "query_type": type,
	                        	                            "origin": method,
	                        	                            "saved": true,
	                        	                            "form_id": embedded_view.attr('embed-source-form-val-id'),
	                        	                            "fields_data": {}
	                        	                        };
	                        	                        var get_fields  = $('iframe').contents().find('.getFields');
	                        	                        get_fields.each(function(){
	                        	                            if($(this).attr('name') == "TrackNo"){
	                        	                                data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                        	                                    "type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                                    "name": $(this).attr('name'),
	                        	                                    "value": $(this).val()
	                        	                                }
	                        	                            }
	                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "pickList"){
	                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                        	                                        var value = "";
	                        	                                        if($(this).attr('data-input-type')){
	                        	                                        	if($(this).attr('data-input-type') == "Currency"){
	                        	                                        		value = $(this).val().replace(/,/g, '');
	                        	                                        	}
	                        	                                        	else{
	                        	                                        		value = $(this).val();
	                        	                                        	}
	                        	                                        }
	                        	                                        else{
	                        	                                        	value = $(this).val();
	                        	                                        }
	                        	                                        data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                        	                                }
	                        	                                else{
	                        	                                	var value = "";
	                        	                                	if($(this).attr('data-input-type')){
	                        	                                		if($(this).attr('data-input-type') == "Currency"){
	                        	                                			value = $(this).val().replace(/,/g, '');
	                        	                                		}
	                        	                                		else{
	                        	                                			value = $(this).val();
	                        	                                		}
	                        	                                	}
	                        	                                	else{
	                        	                                		value = $(this).val();
	                        	                                	}
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                                        "name": $(this).attr('name'),
	                        	                                        "id": $(this).attr('id'),
	                        	                                        "picklist_button_id": $(this).parents('.setOBJ').find('a').attr('picklist-button-id'),
	                        	                                        "picklist_type": $(this).parents('.setOBJ').find('a').attr('picklist-type'),
	                        	                                        "return_field": $(this).parents('.setOBJ').find('a').attr('return-field'),
	                        	                                        "form_id": $(this).parents('.setOBJ').find('a').attr('form-id'),
	                        	                                        "return_field_name": $(this).parents('.setOBJ').find('a').attr('return-field-name'),
	                        	                                        "formname": $(this).parents('.setOBJ').find('a').attr('formname'),
	                        	                                        "display_columns": $(this).parents('.setOBJ').find('a').attr('display_columns'),
	                        	                                        "display_column_sequence": $(this).parents('.setOBJ').find('a').attr('display_column_sequence'),
	                        	                                        "enable_add_entry": $(this).parents('.setOBJ').find('a').attr('enable-add-entry'),
	                        	                                        "allow_values_not_in_list": $(this).parents('.setOBJ').find('a').attr('allow-values-not-in-list'),
	                        	                                        "show_picklist_view": $(this).parents('.setOBJ').find('a').attr('show-picklist-view'),
	                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
	                        	                                        "value": value
	                        	                                    }
	                        	                                }
	                        	                            }
	                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "radioButton"){
	                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                        	                                    var get_id = $(this).attr('name');
	                        	                                    var get_selected = $('iframe').contents().find('[name="' + get_id + '"]:checked').val();
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = get_selected;
	                        	                                }
	                        	                                else{
	                        	                                    var options = $(this).parents('.setOBJ').find('.getFields').map(function(){
	                        	                                        return $(this).val();
	                        	                                    }).get();
	                        	                                    var get_id = $(this).attr('name');
	                        	                                    var get_selected = $('iframe').contents().find('[name="' + get_id + '"]:checked').val();
	                        	                                    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                                        "name": $(this).attr('name'),
	                        	                                        "id": $(this).attr('id'),
	                        	                                        "options": options,
	                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
	                        	                                        "value": get_selected
	                        	                                    }
	                        	                                }
	                        	                            }
	                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "checkbox"){
	                        	                                var field_name = $(this).attr('name').replace(/\[\]/g,'');
	                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
	                        	                                    var get_id = $(this).attr('name');
	                        	                                    var get_selected = $('iframe').contents().find('[name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
	                        	                                }
	                        	                                else{
	                        	                                    var options = $(this).parents('.setOBJ').find('.getFields').map(function(){
	                        	                                        return $(this).val();
	                        	                                    }).get();
	                        	                                    var get_id = $(this).attr('name');
	                        	                                    var get_selected = $('iframe').contents().find('[name="' + get_id + '"]').map(function(){return $(this).filter(":checked").val()||null;}).get();
	                        	                                    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
	                        	                                    
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
	                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                                        "name": field_name,
	                        	                                        "id": $(this).attr('id'),
	                        	                                        "options": options,
	                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
	                        	                                        "value": get_selected
	                        	                                    }
	                        	                                }
	                        	                            }
	                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "selectMany"){
	                        	                                var field_name = $(this).attr('name').replace(/\[\]/g,'');
	                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]) != "undefined"){
	                        	                                    var get_id = $(this).attr('name');
	                        	                                    var get_selected = $('iframe').contents().find('[name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name]['value'] = get_selected;
	                        	                                }
	                        	                                else{
	                        	                                    var options = $(this).parents('.setOBJ').find('option').map(function(){
	                        	                                        return $(this).val();
	                        	                                    }).get();
	                        	                                    var get_id = $(this).attr('name');
	                        	                                    var get_selected = $('iframe').contents().find('[name="' + get_id + '"]').find('option').map(function(){return $(this).filter(":selected").val()||null;}).get();
	                        	                                    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
	                        	                                    
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][field_name] = {
	                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                                        "name": field_name,
	                        	                                        "id": $(this).attr('id'),
	                        	                                        "options": options,
	                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
	                        	                                        "value": get_selected
	                        	                                    }
	                        	                                }
	                        	                            }
	                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "dropdown"){
	                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = $(this).val();
	                        	                                }
	                        	                                else{
	                        	                                    var options = $(this).parents('.setOBJ').find('option').map(function(){
	                        	                                        return $(this).val();
	                        	                                    }).get();
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                                        "name": $(this).attr('name'),
	                        	                                        "id": $(this).attr('id'),
	                        	                                        "options": options,
	                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
	                        	                                        "value": $(this).val()
	                        	                                    }
	                        	                                }
	                        	                            }
	                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "listNames"){
	                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                        	                                    var value = "";
	                        	                                    if($(this).attr('data-input-type')){
	                        	                                    	if($(this).attr('data-input-type') == "Currency"){
	                        	                                    		value = $(this).val().replace(/,/g, '');
	                        	                                    	}
	                        	                                    	else{
	                        	                                    		value = $(this).val();
	                        	                                    	}
	                        	                                    }
	                        	                                    else{
	                        	                                    	value = $(this).val();
	                        	                                    }
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                        	                                }
	                        	                                else{
	                        	                                	var value = "";
	                        	                                	if($(this).attr('data-input-type')){
	                        	                                		if($(this).attr('data-input-type') == "Currency"){
	                        	                                			value = $(this).val().replace(/,/g, '');
	                        	                                		}
	                        	                                		else{
	                        	                                			value = $(this).val();
	                        	                                		}
	                        	                                	}
	                        	                                	else{
	                        	                                		value = $(this).val();
	                        	                                	}
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                                        "name": $(this).attr('name'),
	                        	                                        "id": $(this).attr('id'),
	                        	                                        "return_field": $(this).attr('id'),
	                        	                                        "data_original_title": $(this).parents('.setOBJ').find('a').attr('data-original-title'),
	                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
	                        	                                        "value": value
	                        	                                    }
	                        	                                }
	                        	                            }
	                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "attachment_on_request"){
	                        	                            	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                        	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                        	                            	}
	                        	                            	else{
	                        	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                        	                            			"type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                            			"name": $(this).attr('name'),
	                        	                            			"id": $(this).attr('id'),
	                        	                            			"obj_id": $(this).parents('.setOBJ').attr('data-object-id'),
	                        	                            			"form_id": $(this).prev().attr('id'),
	                        	                            			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                        	                            			"form_data_id": $(this).prev().children().eq(0).attr('id'),
	                        	                            			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
	                        	                            			"value": $(this).val()
	                        	                            		}
	                        	                            	}
	                        	                            }
	                        	                            else if($(this).parents('.setOBJ').attr('data-type') == "multiple_attachment_on_request"){
	                        	                            	if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                        	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                        	                            	}
	                        	                            	else{
	                        	                            		data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                        	                            			"type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                            			"name": $(this).attr('name'),
	                        	                            			"id": $(this).attr('id'),
	                        	                            			"obj_id": $(this).parents('.setOBJ').attr('data-object-id'),
	                        	                            			"form_id": $(this).prev().attr('id'),
	                        	                            			"form_data_action_id": $(this).prev().children().eq(0).attr('data-action-id'),
	                        	                            			"form_data_id": $(this).prev().children().eq(0).attr('id'),
	                        	                            			"form_data_action_type": $(this).prev().children().eq(0).attr('data-action-type'),
	                        	                            			"form_data_attachment": $(this).attr('data-attachment'),
	                        	                            			"value": $(this).val()
	                        	                            		}
	                        	                            	}
	                        	                            }
	                        	                            else{
	                        	                                if($.type(data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]) != "undefined"){
	                        	                                    var value = "";
	                        	                                    if($(this).attr('data-input-type')){
	                        	                                    	if($(this).attr('data-input-type') == "Currency"){
	                        	                                    		value = $(this).val().replace(/,/g, '');
	                        	                                    	}
	                        	                                    	else{
	                        	                                    		value = $(this).val();
	                        	                                    	}
	                        	                                    }
	                        	                                    else{
	                        	                                    	value = $(this).val();
	                        	                                    }
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')]['value'] = value;
	                        	                                }   
	                        	                                else{
	                        	                                	var value = "";
	                        	                                	if($(this).attr('data-input-type')){
	                        	                                		if($(this).attr('data-input-type') == "Currency"){
	                        	                                			value = $(this).val().replace(/,/g, '');
	                        	                                		}
	                        	                                		else{
	                        	                                			value = $(this).val();
	                        	                                		}
	                        	                                	}
	                        	                                	else{
	                        	                                		value = $(this).val();
	                        	                                	}
	                        	                                    data['embed_row_data'][row_id][row_name_count]['fields_data'][$(this).attr('name')] = {
	                        	                                        "type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                                        "name": $(this).attr('name'),
	                        	                                        "field_input_type": $(this).attr('data-input-type')||"",
	                        	                                        "value": value
	                        	                                    }
	                        	                                    if($(this).attr('name') == 'Requestor'){
	                        	                                    	data['embed_row_data'][row_id][row_name_count]['fields_data']['requestor-name'] = {
	                        	                                    	    "type": undefined,
	                        	                                    	    "name": 'requestor-name',
	                        	                                    	    "value": $(this).attr('requestor-name')
	                        	                                    	}
	                        	                                    }	
	                        	                                }

	                        	                            }
	                        	                        });
	                        	                        setTimeout(function(){
	                        	                            var parameters = {
	                        	                                'form_id': row_id,
	                        	                                "record_id": embedded_view.find('tr[on-edit]').attr('record-id')
	                        	                            }
	                        	                            $.post('/ajax/get_fields_data', parameters, function(result){
	                        	                                var res = JSON.parse(result);
	                        	                                data['embed_row_data'][row_id][row_name_count]['fields_data']['LastAction'] = {
	                        	                                    "type": $(this).parents('.setOBJ').attr('data-type'),
	                        	                                    "name": 'LastAction',
	                        	                                    "value": res['LastAction']
	                        	                                }
	                        	                            });
	                        	                            emd_view.addToEmbedList(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, type);
	                        	                            emd_view.getSetTotalOfColumn.call(embedded_view, embedded_view.children());
	                        	                            $('body').find('.fl-closeDialog').removeClass('close-confirmation');
	                        	                            $('body').find('.fl-closeDialog').trigger('click');
	                        	                        }, 1000);
	                        	                    });
	                        	                });
	                        	                clearInterval(getPopup);
	                        	            }
	                        	        }, 0);
	                        	    });
	                        	});
	                        }
	                    }
	                }    
	            }
	        }
        // }
    },
    "updateEmbedRow": function(embed, data_json, row_count, record_id){
        var embed = $(embed);
        var getLastRecordId = Number(record_id);
        if(getLastRecordId <= 0){
            getLastRecordId = 1;
        }
        var current_line = $('tr[record-id="' + getLastRecordId + '"]');
        console.log("current_line",  current_line);
        for(i in data_json){
            // i = i.replace(/\[\]/g, '');
            console.log("field", i, "index", getLastRecordId, "details", data_json[i]);
            if(data_json[i]['type'] == "radioButton"){
                current_line.find('[data-field-name="' + i + '"][value="' + data_json[i]['value'] + '"]').prop('checked', true);
                current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').text(data_json[i]['value']).removeClass('display');
                current_line.find('[data-field-name="' + i + '"]').parent().addClass('display');
            }
            else if(data_json[i]['type'] == "checkbox"){
                // var new_str = i + "[]";
                for(v in data_json[i]['value']){
                    current_line.find('[data-field-name="' + i + '"][value="' + data_json[i]['value'][v] + '"]').prop('checked', true);
                }
                var val_checkbox = data_json[i]['value'];
                if(typeof data_json[i]['value']=="string"){
                	val_checkbox = [data_json[i]['value']];
                }
                console.log("value ng checkbox", val_checkbox);
                current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').text((val_checkbox||[]).join(', ')).removeClass('display');
                console.log("parents", $('[data-field-name="' + i + '"]').parent());
                current_line.find('[data-field-name="' + i + '"]').parent().addClass('display');
            }
            else if(data_json[i]['type'] == "selectMany"){
                // var new_str = i + "[]";
                for(v in data_json[i]['value']){
                    current_line.find('[data-field-name="' + i + '"]').find('option[value="' + data_json[i]['value'][v] + '"]').attr('selected', true);
                }
                var val_select_many = data_json[i]['value'];
                if(typeof data_json[i]['value']=="string"){
                	val_select_many = [data_json[i]['value']];
                }
                // current_line.find('[data-field-name="' + i + '"]').val(data_json[i]['value'].join(', '));
                current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').text((val_select_many||[]).join(', ')).removeClass('display');
                current_line.find('[data-field-name="' + i + '"]').addClass('display');
            }
            else if(data_json[i]['type'] == "dropdown"){
            	if(typeof data_json[i]['value'] == "object"){
        			current_line.find('[data-field-name="' + i + '"]').find('option[selected]').attr('selected', false);
        			current_line.find('[data-field-name="' + i + '"]').find('option[value="' + data_json[i]['value'] + '"]').attr('selected', true);
            	}
            	else{
            		console.log("value:", '`' + data_json[i]['value'] + '`');
            		current_line.find('[data-field-name="' + i + '"]').find('option[selected]').attr('selected', false);
            		console.log("data-field-name", current_line.find('[data-field-name="' + i + '"]').find('option[value="' + data_json[i]['value'] + '"]'));
            		current_line.find('[data-field-name="' + i + '"]').find('option[value="' + data_json[i]['value'] + '"]').attr('selected', true);
            	}
                
                // current_line.find('[data-field-name="' + i + '"]').attr("value", data_json[i]['value']);
                current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').text(data_json[i]['value']).removeClass('display');
                current_line.find('[data-field-name="' + i + '"]').addClass('display');
            }
            else if(data_json[i]['type'] == "listNames"){
                current_line.find('[data-field-name="' + i + '"]').val(data_json[i]['value']);
                // current_line.find('[data-field-name="' + i + '"]').attr("value", data_json[i]['value']);
                current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').text(data_json[i]['value']).removeClass('display');
                current_line.find('[data-field-name="' + i + '"]').parents('.picklist-table').addClass('display');
            }
            else if(data_json[i]['type'] == "pickList"){
                current_line.find('[data-field-name="' + i + '"]').val(data_json[i]['value']);
                // current_line.find('[data-field-name="' + i + '"]').attr("value", data_json[i]['value']);
                current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').text(data_json[i]['value']).removeClass('display');
                current_line.find('[data-field-name="' + i + '"]').parents('.picklist-table').addClass('display');
            }
            else if(data_json[i]['type'] == "attachment_on_request"){
            	var view_html = '<a target="_blank" href="' + data_json[i]['value'] + '" style="';
            	view_html += 'float: left;color: #000;white-space:nowrap'; //margin-top: 5px;margin-left: 5px;
            	var file = data_json[i]['value'].split("/");
            	view_html += '"><u> <i class="fa fa-search"></i> View File </u> ( <strong>' + file[file.length - 1] + '</strong> )</a>';
            	current_line.find('[data-field-name="' + i + '"]').prev().children().eq(0).addClass('display');
            	current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').html(view_html).removeClass('display');
            }
            else if(data_json[i]['type'] == "multiple_attachment_on_request"){
            	var parse_file_type = $.parseJSON(data_json[i]['value']);
            	if($.type(parse_file_type) == 'string'){
            		parse_file_type = $.parseJSON(parse_file_type);
            	}
            	console.log("parsed_file_type", parse_file_type);
            	if (parse_file_type == null) {
            	    var view_html = '<a id="view_files_in_modal" data-body-name="embed_' + data_json[i]['name'] + '_' + record_id + '" style="';
            	    view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
            	    view_html += '">No file attachment.</a>';
            	    current_line.find('[data-field-name="' + i + '"]').prev().children().eq(0).addClass('display');
            	    current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').html(view_html).removeClass('display');
            	}
            	else{
            		var location = parse_file_type[0]['location'];
            		var parse_files = $.parseJSON(parse_file_type[0]['file_type']);
            		var blkstr = [];
            		var file_name = parse_files['name'];
            		var file_length = file_name.length;
            		$.each(file_name, function (id, value) {
            		    blkstr.push(value);
            		});
            		var files = blkstr.join(", ");
            		// Save to body data
            		$("body").data('embed_' + data_json[i]['name'] + '_' + record_id, parse_file_type);
            		var view_html = '<a id="view_files_in_modal" data-body-name="embed_' + data_json[i]['name'] + '_' + record_id + '" style="';
            		view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
            		view_html += '"><u> <i class="fa fa-search"></i> Show (' + file_length + ') File`s </u></a>';
            		current_line.find('[data-field-name="' + i + '"]').prev().children().eq(0).addClass('display');
            		current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').html(view_html).removeClass('display');
            	}
            	
            }
            else{
            	if(current_line.find('[data-field-name="' + i + '"]').attr('equivalent-fld-input-type') == 'Number'){
            		current_line.find('[data-field-name="' + i + '"]').val(Number(data_json[i]['value']));
            		current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').text(data_json[i]['value']).removeClass('display');
            	}
            	else if(current_line.find('[data-field-name="' + i + '"]').attr('equivalent-fld-input-type') == 'Currency'){
            		var value;
            		try{
            			value = data_json[i]['value'].replace(/,/g, '');
            		}
            		catch(e){
            			value = data_json[i]['value'];
            		}
            		current_line.find('[data-field-name="' + i + '"]').val(Number(value).currencyFormat());
            		current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').text(Number(data_json[i]['value']).currencyFormat()).removeClass('display');
            	}
            	else{
            		current_line.find('[data-field-name="' + i + '"]').val(data_json[i]['value']);
            		current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').text(data_json[i]['value']).removeClass('display');
            	}
                current_line.find('[data-field-name="' + i + '"]').addClass('display');
            }
        }
        var tr_reference = row_count;
        var obj_id = tr_reference.split('_')[0];
        var record_id = tr_reference.split('_')[1];
        var data = $('body').data('embed_row_data');
        if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
        	form_id = embed.find('.' + tr_reference).attr('form-id');
        	record_id = embed.find('.' + tr_reference).attr('record-id');
        	data[obj_id][tr_reference]['field_models'] = embed.find('.tr_' + form_id + '_' + record_id).data('field_models');

        }
        else{
        	data[obj_id][tr_reference]['field_models'] = embed.find('.tr_' + record_id).data('field_models');
        }
        data[obj_id][tr_reference]['saved'] = true;
        current_line.find(".viewTDEmbedded").removeClass("display");
        var reserved_words = embeded_view.RESERVED_WORDS;
        for(i in reserved_words){
        	if($.type(data[obj_id][tr_reference]['field_models'][reserved_words[i]]) == 'undefined' || data[obj_id][tr_reference]['field_models'][reserved_words[i]] == "" || data[obj_id][tr_reference]['field_models'][reserved_words[i]] == null){
        		try{
        			data[obj_id][tr_reference]['field_models'][reserved_words[i]] = data[obj_id][tr_reference]['fields_data'][reserved_words[i]]['value'];
        		}catch(e){
        			
        		}
        	}
        }
        current_line.find(".showpasteActions_" + getLastRecordId).removeClass("display");
        current_line.find("#showEditActions_" + getLastRecordId).removeClass("display");
        current_line.find("#showEditActions_" + getLastRecordId).find('[id="editDEmbedded"]').removeClass('disable-clickable');
        current_line.find("#hideEditActions_" + getLastRecordId).addClass("display");
        current_line.find(".deleteEmbedded").removeClass("display");
        embed.trigger('change.exeVisibilityOnChange');
        embeded_view.getSetTotalOfColumn.call($(embed), $(embed).children());
        if(embed.attr('commit-data-row-event') == 'default'){
	        if($('.okInsertDEmbedded:visible, .okeditDEmbedded:visible').length <= 0){
	        	$('.embed-edit-mode').removeClass('embed-edit-mode');
	        }
	    }
	    EmbeddedFixHeader.reFixHeader(embed);
    },
    "addToEmbedList": function(embed, data_json, row_count, type, isInlineInsert, form_id, is_import){
    	console.log("embedded view", embed);
        var embed = $(embed);
        var obj_id = embed.closest('.setOBJ').attr('data-object-id');
        var column_data = {};
        embed.find("th").each(function(ele){
        	var th_child = $(this).children();
        	column_data[''+th_child.attr('data-form-fld-name')] = {
        		"input_field_type": th_child.attr('data-input-fld-type')
        	};
        });
        var inline_insert = isInlineInsert;
        var display_columns = [];
        if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
        	display_columns = $('body').data('user_form_json_data')['form_json'][""+ embed.closest('.setOBJ').attr('data-object-id')]['column_settings'].map(function(val, index){
        		return val['column_field_name'];
        	});
        }
        else{
        	display_columns = JSON.parse(embed.attr('embed-column-data')).map(function(val, index){
	            return val['FieldName'];
	        });
	    }
        var getLastRecordId = 0;
        var labelDisplay = '';
        var setDisplay = 'display';
        if(type == "insert"){
            // getLastRecordId = $(embed).children('table').children('tbody').find('tr[record-id]').map(function(){
            //     return $(this).attr('record-id');
            // }).sort(function(a, b){
            //     return b - a;
            // })[0]||0;
            getLastRecordId = Number(row_count.split('_')[1]);
        }
        else{
            getLastRecordId = Number(row_count.split('_')[1]||data_json['ID']['value']);
        }
        var record_id = 0;
        if(getLastRecordId <= 0){
            record_id = Number(getLastRecordId) + 1;
        }
        else{
            record_id = Number(getLastRecordId);
        }
        //=============================================
        if(type == "insert"){
            record_id = Number(getLastRecordId);
        }
        if($.type(inline_insert) != "undefined"){
            if(inline_insert == true){
                setDisplay = "";
                labelDisplay = "display";
            }
        }
        // var record_id = row_count.split('_')[1];
        var tr_string = "";
        var frm_id = $('body').data('embed_row_data')[row_count.split('_')[0]][row_count]['form_id']||"";
        if(typeof embed.attr('source-form-type')  != "undefined" && embed.attr('source-form-type') == "multiple"){
        	tr_string = '<tr class="tr_'+ frm_id + '_' + record_id + ' ' + row_count + ' embed-context-menu hovereffect" form-id="' + frm_id + '" record-trackno="' + data_json['TrackNo']['value'] + '" reference-id="'+ row_count + '" record-id="' + record_id + '" is-pending="true" data-required-field="null"><td></td>';
        }
        else{
        	tr_string = '<tr class="tr_'+ record_id + ' ' + row_count + ' embed-context-menu hovereffect" record-trackno="' + data_json['TrackNo']['value'] + '" reference-id="'+ row_count + '" record-id="' + record_id + '" is-pending="true" data-required-field="null">';
        }
        if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') != "multiple"){
	        if(typeof $('body').data('user_form_json_data')['form_json'][''+obj_id]['allow_row_selection'] != "undefined" || $('body').data('user_form_json_data')['form_json'][''+obj_id]['allow_row_selection'] == true){
				tr_string += '<td>';
					tr_string += '<label>';
						tr_string += '<input type="checkbox" class="css-checkbox record_selector" name="record_selector" id="record_selector_' + frm_id + '_' + record_id + '">';
						tr_string += '<label for="record_selector_' + frm_id + '_' + record_id + '" class="css-label"></label>';
					tr_string += '</label>';
				tr_string += '</td>';
			}
        }

        //add the action buttons
        if(embed.find('.embed-table-actions').length > 0){
            console.log("embed actions found");
            if($.type(inline_insert) != "undefined"){
                if(inline_insert == true){
                    tr_string += embeded_view.setupEmbedActions(embed, record_id, inline_insert);

                }
                else{
                    tr_string += embeded_view.setupEmbedActions(embed, record_id);
                }
            }
            else{
                tr_string += embeded_view.setupEmbedActions(embed, record_id);
            }
            
        }
        //check if the numbering is checked
        if(embed.attr('embed-action-click-number') == "true"){
        	tr_string += "<td style='text-align:center'>";
        	var tr_count = (embed.find('tbody').children('tr').length - 1);
        	if(tr_count <= 0){
        		tr_count = 1;
        	}
        	tr_string += '<label id="lblEmbed_' + record_id + '" class="embed_numbering lblEmbed_' + record_id + '">' + tr_count + '</label>';
        	tr_string += "</td>";
        }
        //display the field values
        for(i in display_columns){
            console.log("display_columns", data_json[display_columns[i]]);
            if($.type(data_json[display_columns[i]]) != "undefined"){
            	if($.type(data_json[display_columns[i]]['type']) == 'undefined'){
            	    tr_string += "<td style='text-align:left'>";
            	        tr_string += '<label style="white-space: nowrap;" id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + '">' + data_json[display_columns[i]]['value'] + '</label>';
            	    tr_string += "</td>";
            	}
            	else{
            	    if(data_json[display_columns[i]]['type'] == 'pickList'){
            	    //picklist
            	    	var attribute = "";
            	    	var display_none = "";
            	    	var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		var keys = Object.keys(data_json);
            	    		var formula = EmbedFields.embedFormulaParser(keys, data_json[display_columns[i]]['formula'], 'tr_'+ record_id)['parsed_formula'];
            	    		if(formula != ""){
            	    			attribute = "readonly='readonly'";
            	    		}
        	    			display_none = "display:none;";
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    		display_none = "display:none;";
            	    		border_none = "border:none";
            	    	}
            	    	if(data_json[display_columns[i]]['field_input_type'] == "Currency"){
            	    		tr_string += "<td style='text-align:right'>";
            	    	}
            	    	else{
            	    		tr_string += "<td style='text-align:left'>";
            	    	}
            	            tr_string += "<table style='border:none;' class='" + setDisplay + " picklist-table'>";
            	                tr_string += "<tbody>";
            	                    tr_string += "<tr class='picklist-tr hovereffect'>";
            	                        tr_string += "<td class='picklist-td' style='border:none;'>";
            	                            tr_string += "<input type='text' style='" + border_none + "' data-field-name='" + data_json[display_columns[i]]['name'] + "' style='min-width:30px;display:table-cell;font-size:14px;' data-type='longtext' class='tip form-text embed_getFields inlineEdit_" + record_id + "' id='inlineEdit_" + record_id + "_" + data_json[display_columns[i]]['id'] + "' value='" + val + "' data-placement='bottom'" + attribute + " equivalent-fld-input-type='" + data_json[display_columns[i]]['field_input_type'] + "' obj-type='" + data_json[display_columns[i]]['type'] + "'>";
            	                        tr_string += "</td>";
            	                        tr_string += "<td class='picklist-td' style='border:none;" + display_none + "'>";
            	                            tr_string += "<span style='width:23px;display:table-cell;white-space:nowrap;'>";
            	                                tr_string += "<a class='btn-basicBtn padding_" + record_id + " cursor tip pickListButton' ";
            	                                    tr_string += "picklist-button-id='" + data_json[display_columns[i]]['picklist_button_id'] + "' ";
            	                                    tr_string += "return-field='inlineEdit_" + record_id + "_" + data_json[display_columns[i]]['return_field'] + "' ";
            	                                    tr_string += "picklist-type='" + data_json[display_columns[i]]['picklist_type'] + "' ";
            	                                    tr_string += "form-id='" + data_json[display_columns[i]]['form_id'] + "' ";
            	                                    tr_string += "return-field-name='" + data_json[display_columns[i]]['return_field_name'] + "' ";
            	                                    tr_string += "formname='" + data_json[display_columns[i]]['formname'] + "' ";
            	                                    tr_string += "display_columns='" + data_json[display_columns[i]]['display_columns'] + "' ";
            	                                    tr_string += "selection-type='" + data_json[display_columns[i]]['selection_type'] + "' ";
            	                                    tr_string += "allow-values-not-in-list='" + data_json[display_columns[i]]['allow_values_not_in_list'] + "' ";
            	                                    // tr_string += "display_column_sequence selection-type='" + data_json[display_columns[i]]['selection_type'] + "' condition ";
            	                                    tr_string += "enable-add-entry='" + data_json[display_columns[i]]['enable_add_entry'] + "' ";
            	                                    tr_string += "style='margin-left:5px;'";
            	                                tr_string += ">";
            	                                    tr_string += "<i class='icon-list-alt fa fa-list-alt'></i>";
            	                                tr_string += "</a>";
            	                            tr_string += "</span>";
            	                        tr_string += "</td>";
            	                    tr_string += "</tr>";
            	                tr_string += "</tbody>";
            	            tr_string += "</table>";
            	            tr_string += '<label style="white-space: nowrap;" id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + '  ' + labelDisplay + '">' + data_json[display_columns[i]]['value'] + '</label>';
            	        tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == 'checkbox'){
            	    	var attribute = "";
            	    	var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		var keys = Object.keys(data_json);
            	    		var formula = EmbedFields.embedFormulaParser(keys, data_json[display_columns[i]]['formula'], 'tr_'+ record_id)['parsed_formula'];
            	    		if(formula != ""){
            	    			attribute = "readonly='readonly'";
            	    		}
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    		border_none = "border:none;";
            	    	}
            	        tr_string += "<td style='text-align:left'>";
            	            for(v in data_json[display_columns[i]]['options']){
            	                tr_string += "<label style='white-space: nowrap;' class='" + setDisplay + "'>";
            	                    tr_string += "<input type='checkbox' field-type='" + data_json[display_columns[i]]['type'] + "' data-field-name='" + data_json[display_columns[i]]['name'] + "' value='" + data_json[display_columns[i]]['options'][v] + "' data-type='longtext' data-embed-name='embed_textbox_" + record_id + "' equivalent-header-title='" + data_json[display_columns[i]]['name'] + "' class='tip embed_getFields inlineEdit_" + record_id + "' name='" + data_json[display_columns[i]]['name']  + i + "'";
            	                    if($.inArray(data_json[display_columns[i]]['options'][v], data_json[display_columns[i]]['value']) > -1){
            	                        tr_string += ' checked ';
            	                    }
            	                    tr_string += attribute;
            	                    tr_string += " style='" + border_none + "' ";
            	                    tr_string += " equivalent-fld-input-type='" + data_json[display_columns[i]]['field_input_type'] + "'";
            	                    tr_string += "/>";
            	                    tr_string += "<span class='checkbox-input-label'>" + data_json[display_columns[i]]['options'][v] + "</span>";
            	                tr_string += "</label>";
            	                tr_string += "<span class='rc-prop-alignment rc-align-vertical'></span>";
            	            }
            	            tr_string += "<label style='white-space: nowrap;' id='lblEmbed_" + record_id + "' class='lblEmbed_" + record_id +  " " + labelDisplay + "' equivalent-header-title='" + data_json[display_columns[i]]['name'] + "' equivalent-fld-title='" + data_json[display_columns[i]]['name'] + "'>" + data_json[display_columns[i]]['value'] + "</label>";
            	        tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == 'radioButton'){
            	    	var attribute = "";
            	    	var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		var keys = Object.keys(data_json);
            	    		var formula = EmbedFields.embedFormulaParser(keys, data_json[display_columns[i]]['formula'], 'tr_'+ record_id)['parsed_formula'];
            	    		if(formula != ""){
            	    			attribute = "readonly='readonly'";
            	    		}
        	    			display_none = "display:none;";
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    		border_none = "border:none;";
            	    	}
            	        tr_string += "<td style='text-align:left'>";
            	            for(v in data_json[display_columns[i]]['options']){
            	            	console.log("radio value", data_json[display_columns[i]]['options'][v]);
            	                tr_string += "<label style='white-space: nowrap;'class='" + setDisplay + "'>";
            	                    tr_string += "<input type='radio' field-type='" + data_json[display_columns[i]]['type'] + "' data-field-name='" + data_json[display_columns[i]]['name'] + "' value='" + $.trim(data_json[display_columns[i]]['options'][v]) + "' data-type='longtext' data-embed-name='embed_textbox_" + record_id + "' equivalent-header-title='" + data_json[display_columns[i]]['name'] + "' class='tip embed_getFields inlineEdit_" + record_id + "' name='" + data_json[display_columns[i]]['name']  + record_id + "'";
            	                    if(data_json[display_columns[i]]['options'][v] == data_json[display_columns[i]]['value']){
            	                        tr_string += ' checked ';
            	                    }
            	                    tr_string += attribute;
            	                    tr_string += " style='" + border_none + "' ";
            	                    tr_string += " equivalent-fld-input-type='" + data_json[display_columns[i]]['field_input_type'] + "'";
            	                    tr_string += "/>";
            	                    tr_string += "<span class='radio-input-label'>" + data_json[display_columns[i]]['options'][v] + "</span>";
            	                tr_string += "</label>";
            	                tr_string += "<span class='rc-prop-alignment rc-align-vertical'></span>";
            	                
            	            }
            	            tr_string += "<label style='white-space: nowrap;' id='lblEmbed_" + record_id + "' class='lblEmbed_" + record_id + " " + labelDisplay + "' equivalent-header-title='" + data_json[display_columns[i]]['name'] + "' equivalent-fld-title='" + data_json[display_columns[i]]['name'] + "'>" + data_json[display_columns[i]]['value'] + "</label>";
            	        tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == 'selectMany'){
            	    	var attribute = "";
            	    	var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		var keys = Object.keys(data_json);
            	    		var formula = EmbedFields.embedFormulaParser(keys, data_json[display_columns[i]]['formula'], 'tr_'+ record_id)['parsed_formula'];
            	    		if(formula != ""){
            	    			attribute = "readonly='readonly'";
            	    		}
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    		border_none = "border:none;";
            	    	}
            	        tr_string += "<td style='text-align:left'>";
            	            tr_string += "<select class='form-selectMany " + setDisplay + " tip form-text embed_getFields inlineEdit_" + record_id + "' style='" + border_none +  "' data-field-name='" + data_json[display_columns[i]]['name'] + "' data-embed-name='embed_textbox_" + record_id + "' multiple='multiple' " + attribute + " equivalent-fld-input-type='" + data_json[display_columns[i]]['field_input_type'] + "'>";
            	            for(v in data_json[display_columns[i]]['options']){
            	                tr_string += "<option value='" + data_json[display_columns[i]]['options'][v] + "'";
            	                if($.inArray(data_json[display_columns[i]]['options'][v], data_json[display_columns[i]]['value']) > -1){
            	                    tr_string += " selected ";
            	                }
            	                tr_string += ">" + data_json[display_columns[i]]['options'][v] + "</option>";
            	            }
            	            tr_string += "</select>";
            	            tr_string += "<label style='white-space: nowrap;' id='lblEmbed_" + record_id + "' class='lblEmbed_" + record_id + " " + labelDisplay + "' equivalent-header-title='" + data_json[display_columns[i]]['name'] + "' equivalent-fld-title='" + data_json[display_columns[i]]['name'] + "'>" + data_json[display_columns[i]]['value'].join(", ") + "</label>";
            	        tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == 'dropdown'){
            	    	var attribute = "";
            	    	var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		var keys = Object.keys(data_json);
            	    		var formula = EmbedFields.embedFormulaParser(keys, data_json[display_columns[i]]['formula'], 'tr_'+ record_id)['parsed_formula'];
            	    		if(formula != ""){
            	    			attribute = "readonly='readonly'";
            	    		}
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    		border_none = "border:none;";
            	    	}
            	        tr_string += "<td style='text-align:left'>";
            	            tr_string += "<select class='form-dropdown " + setDisplay + " tip form-text embed_getFields inlineEdit_" + record_id + "' style='" + border_none + "' data-field-name='" + data_json[display_columns[i]]['name'] + "' data-embed-name='embed_textbox_" + record_id + "' value='" + data_json[display_columns[i]]['value'] + "' " + attribute + " equivalent-fld-input-type='" + data_json[display_columns[i]]['field_input_type'] + "'>";
            	            for(v in data_json[display_columns[i]]['options']){
            	                tr_string += "<option value='" + data_json[display_columns[i]]['options'][v] + "'";
            	                if(data_json[display_columns[i]]['options'][v] == data_json[display_columns[i]]['value']){
            	                    tr_string += " selected ";
            	                }
            	                tr_string += ">" + data_json[display_columns[i]]['options'][v] + "</option>";
            	            }
            	            tr_string += "</select>";
            	            tr_string += "<label style='white-space: nowrap;' id='lblEmbed_" + record_id + "' class='lblEmbed_" + record_id + " " + labelDisplay + "' equivalent-header-title='" + data_json[display_columns[i]]['name'] + "' equivalent-fld-title='" + data_json[display_columns[i]]['name'] + "'>" + data_json[display_columns[i]]['value'] + "</label>";
            	        tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == 'dateTime'){
            	    	var attribute = "";
            	    	var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		attribute = "readonly='readonly'";
            	    		border_none = "border:none;";
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    	}
            	        tr_string += "<td style='text-align:left'>";
            	            tr_string += '<input type="text" style="' + border_none + '" data-field-name="' + data_json[display_columns[i]]['name'] + '" class="' + setDisplay + ' tip form-text embed_dateTime embed_getFields inlineEdit_' + record_id + '" data-embed-name="embed_textbox_' + record_id + '" value="' + val + '" ' + attribute + ' equivalent-fld-input-type="' + data_json[display_columns[i]]['field_input_type'] + '">';
            	            tr_string += '<label style="white-space: nowrap;" id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + ' ' + labelDisplay + '">' + data_json[display_columns[i]]['value'] + '</label>';
            	        tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == 'datepicker'){
            	    	var attribute = "";
            	    	var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		var keys = Object.keys(data_json);
            	    		var formula = EmbedFields.embedFormulaParser(keys, data_json[display_columns[i]]['formula'], 'tr_'+ record_id)['parsed_formula'];
            	    		if(formula != ""){
            	    			attribute = "readonly='readonly'";
            	    		}
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    		border_none = "border:none;";
            	    	}
            	        tr_string += "<td style='text-align:left'>";
            	            tr_string += '<input type="text" style="' + border_none + '" data-field-name="' + data_json[display_columns[i]]['name'] + '" class="' + setDisplay + ' tip form-text embed_date embed_getFields inlineEdit_' + record_id + '" data-embed-name="embed_textbox_' + record_id + '" value="' + val + '" ' + attribute + 'equivalent-fld-input-type="' + data_json[display_columns[i]]['field_input_type'] + '">';
            	            tr_string += '<label style="white-space: nowrap;" id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + ' ' + labelDisplay + '">' + data_json[display_columns[i]]['value'] + '</label>';
            	        tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == 'time'){
            	    	var attribute = "";
             	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		var keys = Object.keys(data_json);
            	    		var formula = EmbedFields.embedFormulaParser(keys, data_json[display_columns[i]]['formula'], 'tr_'+ record_id)['parsed_formula'];
            	    		if(formula != ""){
            	    			attribute = "readonly='readonly'";
            	    		}
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    	}
            	        tr_string += "<td style='text-align:left'>";
            	            tr_string += '<input type="text" data-field-name="' + data_json[display_columns[i]]['name'] + '" class="' + setDisplay + ' tip form-text embed_time embed_getFields inlineEdit_' + record_id + '" data-embed-name="embed_textbox_' + record_id + '" value="' + val + '" ' + attribute + ' equivalent-fld-input-type="' + data_json[display_columns[i]]['field_input_type'] + '">';
            	            tr_string += '<label style="white-space: nowrap;" id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + " " + labelDisplay + '">' + data_json[display_columns[i]]['value'] + '</label>';
            	        tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == 'listNames'){
            	    	var attribute = "";
            	    	var display_none = "";
            	    	var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		var keys = Object.keys(data_json);
            	    		var formula = EmbedFields.embedFormulaParser(keys, data_json[display_columns[i]]['formula'], 'tr_'+ record_id)['parsed_formula'];
            	    		if(formula != ""){
            	    			attribute = "readonly='readonly'";
            	    		}
        	    			display_none = "display:none;";
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    		display_none = "display:none;";
            	    		border_none = "border:none;";
            	    	}
            	    	if(data_json[display_columns[i]]['field_input_type'] == "Currency"){
            	    		tr_string += "<td style='text-align:right'>";
            	    	}
            	    	else{
            	    		tr_string += "<td style='text-align:left'>";
            	    	}
            	            tr_string += "<table style='border:none;' class='" + setDisplay + " picklist-table'>";
            	                tr_string += "<tbody>";
            	                    tr_string += "<tr class='picklist-tr hovereffect'>";
            	                        tr_string += "<td class='picklist-td' style='border:none;'>";
            	                            tr_string += "<input type='text' data-field-name='" + data_json[display_columns[i]]['name'] + "' style='min-width:30px;display:table-cell;font-size:14px;" + border_none + "' data-type='longtext' class='tip form-text embed_getFields inlineEdit_" + record_id + "' id='" + data_json[display_columns[i]]['id'] + "_" + record_id + "' value='" + val + "' data-placement='bottom' " + attribute + " equivalent-fld-input-type='" + data_json[display_columns[i]]['field_input_type'] + "'  obj-type='" + data_json[display_columns[i]]['type'] + "'>";
            	                        tr_string += "</td>";
            	                        tr_string += "<td class='picklist-td' style='border:none;'>";
            	                            tr_string += "<span style='width:23px;display:table-cell;white-space:nowrap;'>";
            	                                tr_string += "<a class='btn-basicBtn padding_" + record_id + " cursor tip viewNames' ";
            	                                    // tr_string += "picklist-button-id='" + data_json[display_columns[i]]['picklist_button_id'] + "' ";
            	                                    tr_string += "return-field='" + data_json[display_columns[i]]['return_field'] + "_" + record_id + "' ";
            	                                    // tr_string += "picklist-type='" + data_json[display_columns[i]]['picklist_type'] + "' ";
            	                                    // tr_string += "form-id='" + data_json[display_columns[i]]['form_id'] + "' ";
            	                                    // tr_string += "return-field-name='" + data_json[display_columns[i]]['return_field_name'] + "' ";
            	                                    // tr_string += "formname='" + data_json[display_columns[i]]['formname'] + "' ";
            	                                    // tr_string += "display_columns='" + data_json[display_columns[i]]['display_columns'] + "' ";
            	                                    // tr_string += "display_column_sequence list-type-selection='" + data_json[display_columns[i]]['list_type_selection'] + "' condition ";
            	                                    // tr_string += "enable-add-entry='" + data_json[display_columns[i]]['enable_add_entry'] + "' ";
            	                                    tr_string += "list-type-selection='" + data_json[display_columns[i]]['list_type_selection'] + "' ";
            	                                    tr_string += "data-original-title='" + data_json[display_columns[i]]['data_original_title'] + "' ";
            	                                    tr_string += "style='margin-left:5px;" + display_none + "'";
            	                                tr_string += ">";
            	                                    tr_string += "<i class='icon-list-alt fa fa-list-alt'></i>";
            	                                tr_string += "</a>";
            	                            tr_string += "</span>";
            	                        tr_string += "</td>";
            	                    tr_string += "</tr>";
            	                tr_string += "</tbody>";
            	            tr_string += "</table>";
            	            tr_string += '<label style="white-space: nowrap;" id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + " " + labelDisplay + '">' + data_json[display_columns[i]]['value'] + '</label>';
            	        tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == "textArea" || data_json[display_columns[i]]['type'] == "noteStyleTextarea"){
            	    	var attribute = "";
            	    	var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		attribute = "readonly='readonly'";
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
            	    	else if(data_json[display_columns[i]]['readonly'] == "1"){
            	    		attribute = "readonly='readonly'";
            	    		border_none = "border:none;";
            	    	}
            	    	if(data_json[display_columns[i]]['field_input_type'] == "Currency"){
            	    		tr_string += "<td style='text-align:right'><pre>";
            	    	}
            	    	else{
            	    		tr_string += "<td style='text-align:left'><pre>";
            	    	}
            	    	    tr_string += '<textarea type="text" embed-data-obj-type="' + data_json[display_columns[i]]['type'] + '" style="' + border_none + '" data-field-name="' + data_json[display_columns[i]]['name'] + '" class="form-textarea ' + setDisplay + ' tip form-text embed_getFields inlineEdit_' + record_id + '" data-embed-name="embed_textbox_' + record_id + '" value="' + val + '" ' + attribute + ' equivalent-fld-input-type="' + data_json[display_columns[i]]['field_input_type'] + '">' + data_json[display_columns[i]]['value'] + '</textarea>';
            	    	    tr_string += '<label id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + " " + labelDisplay + '">' + data_json[display_columns[i]]['value'] + '</label>';
            	    	tr_string += "</pre></td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == "attachment_on_request"){
            	    	tr_string += "<td style='text-align:left'>";
	            	    	tr_string += "<form id='embed_on_attach_" + data_json[display_columns[i]]['obj_id'] + "_" + record_id + "' method='post' enctype='multipart/form-data' action='/ajax/request_on_attachment' encoding='multipart/form-data'>";
	            	    		tr_string += "<input type='file' class='embed_file_attachement obj-attachment " + setDisplay + "' data-action-id='" + data_json[display_columns[i]]['obj_id'] + "' name='file_attachment' id='on_attachment_" + data_json[display_columns[i]]['obj_id'] + "' size='24' data-action-type='attachmentForm' field-id='" + record_id + "'/>";
	            	    	tr_string += "</form>";
	            	    	tr_string += "<input type='text' data-single-attachment='true' class='display embed_getFields inlineEdit_" + record_id + "' data-field-name='" + data_json[display_columns[i]]['name'] + "' id='getFields_" + data_json[display_columns[i]]['obj_id'] + "' value='" + data_json[display_columns[i]]['value'] + "'/>"
            	    		if(!inline_insert){
            	    			var view_html = '<a target="_blank" href="' + data_json[display_columns[i]]['value'] + '" style="';
            	    			view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
            	    			var file = "";
            	    			if(data_json[display_columns[i]]['value'].length > 0){
            	    				file = data_json[display_columns[i]]['value'].split("/");
            	    				view_html += '"><u> <i class="fa fa-search"></i> View File </u> ( <strong>' + file[file.length - 1] + '</strong> )</a>';
            	    			}
            	    			else{
            	    				view_html += '">No file attachment.</a>';
            	    			}
            	    			
            	    			tr_string += "<label style='white-space: nowrap;' id='lblEmbed_" + record_id + "' class='lblEmbed_" + record_id + ' ' + labelDisplay + "'>" + view_html + "</label>";
            	    		}
            	    		else{
            	    			tr_string += "<label style='white-space: nowrap;' id='lblEmbed_" + record_id + "' class='lblEmbed_" + record_id + ' ' + labelDisplay + "'>" + data_json[display_columns[i]]['value'] + "</label>";
            	    		}
            	    	tr_string += "</td>";
            	    }
            	    else if(data_json[display_columns[i]]['type'] == "multiple_attachment_on_request"){
            	    	tr_string += "<td style='text-align:left'>";
	            	    	tr_string += "<form id='embed_multiple_on_attach_" + data_json[display_columns[i]]['obj_id'] + "_" + record_id + "' method='post' enctype='multipart/form-data' action='/ajax/multiple_request_on_attachment' encoding='multipart/form-data'>";
	            	    		tr_string += "<input type='file' class='embed_multiple_file_attachement obj-attachment " + setDisplay + "' data-action-id='" + data_json[display_columns[i]]['obj_id'] + "' class='" + setDisplay + "' name='multiple_file_attachment[]' multiple id='multiple_on_attachment_" + data_json[display_columns[i]]['obj_id'] + "' size='24' data-action-type='multiple_attachmentForm'  field-id='" + record_id + "'/>";
	            	    	tr_string += "</form>";
	            	    	tr_string += "<input type='text' data-attachment='attachment_multiple_on_attachment_ " + data_json[display_columns[i]]['obj_id'] + "' class='display embed_getFields inlineEdit_" + record_id + "' data-field-name='" + data_json[display_columns[i]]['name'] + "' name='embed_" + data_json[display_columns[i]]['name'] + "_" + record_id + "' id='getFields_" + data_json[display_columns[i]]['obj_id']+ "' value='" + data_json[display_columns[i]]['value'] + "'/>";
            	    		if(!inline_insert){
	            	    		var parse_file_type = $.parseJSON($.parseJSON(data_json[display_columns[i]]['value']));
	            	    		if (parse_file_type == null) {
	            	    		    var view_html = '<a id="view_files_in_modal" data-body-name="embed_' + data_json[display_columns[i]]['name'] + '_' + record_id + '" style="';
	            	    		    view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
	            	    		    view_html += '">No file attachment.</a>';
	            	    		    tr_string += "<label style='white-space: nowrap;' id='lblEmbed_" + record_id + "' class='lblEmbed_" + record_id + ' ' + labelDisplay + "'>" + view_html + "</label>";	            	    		}
	            	    		else{
	            	    			var location = parse_file_type[0]['location'];
		            	    		var parse_files = $.parseJSON(parse_file_type[0]['file_type']);
		            	    		var blkstr = [];
		            	    		var file_name = parse_files['name'];
		            	    		var file_length = file_name.length;
		            	    		$.each(file_name, function (id, value) {
		            	    		    blkstr.push(value);
		            	    		});
		            	    		var files = blkstr.join(", ");
		            	    		// Save to body data
		            	    		$("body").data('embed_' + data_json[display_columns[i]]['name'] + '_' + record_id, parse_file_type);
		            	    		var view_html = '<a id="view_files_in_modal" data-body-name="embed_' + data_json[display_columns[i]]['name'] + '_' + record_id + '" style="';
		            	    		view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
		            	    		view_html += '"><u> <i class="fa fa-search"></i> Show (' + file_length + ') File`s </u></a>';
		            	    		tr_string += "<label style='white-space: nowrap;' id='lblEmbed_" + record_id + "' class='lblEmbed_" + record_id + ' ' + labelDisplay + "'>" + view_html + "</label>";
	            	    		}
	            	    	}
	            	    	else{
	            	    		tr_string += "<label style='white-space: nowrap;' id='lblEmbed_" + record_id + "' class='lblEmbed_" + record_id + ' ' + labelDisplay + "'>" + data_json[display_columns[i]]['value'] + "</label>";
	            	    	}
            	    	tr_string += "</td>";
            	    }
            	    else{
            	    //textbox
	            	    var attribute = "";
	            	    var border_none = "";
            	    	var val = data_json[display_columns[i]]['value'] || "";
            	    	if(data_json[display_columns[i]]['formula_type'] == "computed"){
            	    		var keys = Object.keys(data_json);
            	    		var formula = EmbedFields.embedFormulaParser(keys, data_json[display_columns[i]]['formula'], 'tr_'+ record_id)['parsed_formula'];
            	    		if(formula != ""){
            	    			attribute = "readonly='readonly'";
            	    		}
        	    			display_none = "display:none;";
            	    	}
            	    	else if(data_json[display_columns[i]]['formula_type'] == "static"){
            	    		if(val == "" && data_json[display_columns[i]]['formula'] != ""){
            	    			val = data_json[display_columns[i]]['formula'];
            	    		}
            	    	}
	            	    if(data_json[display_columns[i]]['readonly'] == "1"){
	            	    	attribute = "readonly='readonly'";
	            	    	border_none = "border:none;";
	            	    }
	            	    console.log("12345", data_json, display_columns[i]);
	            	    // var value = data_json[display_columns[i]]['value'];
	            	    if(data_json[display_columns[i]]['field_input_type'] == "Currency"){
	            	    	tr_string += "<td style='text-align:right'>";
	            	    }else{
	            	    	tr_string += "<td style='text-align:left'>";
	            	    }
        	            tr_string += '<input type="text" style="' + border_none + '" data-field-name="' + data_json[display_columns[i]]['name'] + '" class="' + setDisplay + ' tip form-text embed_getFields inlineEdit_' + record_id + '" data-embed-name="embed_textbox_' + record_id + '" value="' + val + '" ' + attribute + ' equivalent-fld-input-type="' + data_json[display_columns[i]]['field_input_type'] + '">';
        	            tr_string += '<label id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + " " + labelDisplay + '">' + val + '</label>';
            	        tr_string += "</td>";
            	    }
            	}
            }
            else{
            	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
            		var column_field_data = $('body').data('user_form_json_data')['form_json'][""+ embed.closest('.setOBJ').attr('data-object-id')]['column_settings'].filter(function(val, index){
    		       		return val['column_field_name'] == display_columns[i];
    		       	});
    		       	tr_string += "<td style='text-align:left'>";
		            	tr_string += '<label id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + " " + labelDisplay + '">' + (column_field_data['column_field_replacer']||"") + '</label>';
	    	        tr_string += "</td>";
            	}
            }                    
        }
        tr_string += "</tr>";
        // console.log("tr string", tr_string);
        var appendedRow = $(embed).find('.appendNewRecord');
        if(type == "insert"){
            $(tr_string).insertBefore(appendedRow);
            
            if($.type(inline_insert) != "undefined"){
                if(inline_insert == true){
                    if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
                    	embeded_view.bindCreateEvents(embed, embed.find('.tr_' + frm_id + '_' + record_id));
                    	embed.find('.tr_' + record_id).attr('on-edit', '');
                    }
                    else{
                    	embeded_view.bindCreateEvents(embed, embed.find('.tr_' + record_id));
                    	embed.find('.tr_' + record_id).attr('on-edit', '');
                    }
                    
                }
            }
        }
        else{
        	if(embed.find('.tr_' + record_id).length <= 0){
        		$(tr_string).insertBefore(appendedRow);
        	}
        	else{
        		if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
            		embed.find('.tr_' + frm_id + '_'  + record_id).replaceWith(tr_string);
            	}
            	else{
            		embed.find('.tr_' + record_id).replaceWith(tr_string);
            	}
            }
        }
        // if(embed.attr('embed-action-click-number') == "true"){
        // 	var td_string = "<td style='text-align:center'>";
        // 	var tr_count = (embed.find('tbody').find('tr').length - 1);
        // 	if(tr_count <= 0){
        // 		tr_count = 1;
        // 	}
        // 	td_string += '<label id="lblEmbed_' + record_id + '" class="lblEmbed_' + record_id + '">' + tr_count + '</label>';
        // 	td_string += "</td>";
        // 	$('.tr_' + record_id).prepend(td_string);
        // }
        var disabledDays = [];
        $('.embed_dateTime').datetimepicker({
            changeMonth: true,
            dateFormat: 'yy-mm-dd',
            beforeShowDay: function (date) {
                //console.log(this);
                var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
                for (i = 0; i < disabledDays.length; i++) {
                    if ($.inArray((m + 1) + '-' + d + '-' + y, disabledDays) != -1) {
                        return [false, 'red', 'This is Holiday'];
                    }
                }
                return [true];
            }   
        });
        $('.embed_date').datepicker({
            'dateFormat': 'yy-mm-dd',
            "beforeShow": function () {
                // setTimeout(function(){
                $('.ui-datepicker').css('z-index', 100);
                // }, 0);
            },
            changeMonth: true
        });
        $('.embed_time').datetimepicker({
            changeMonth: true,
            dateFormat: '',
            timeFormat: 'HH:mm',
            timeOnly: true
        })

        //fix the embedded view picklist allow not in list values
        var picklists = embed.find('[obj-type="pickList"]');
        picklists.each(function(index){
        	var self = $(this);
        	var parent = $(this).closest('.picklist-table');
        	var picklist_button = parent.find('.pickListButton');
        	if(picklist_button.attr('allow-values-not-in-list') == "No"){
        		self.css('cursor', 'pointer');
        		self.on('focus', function(){
        			$(this).blur();
        		}).on('click', function(e){
		        	e.preventDefault();
		        	picklist_button.trigger('click');
		        })
        	}
        })

        embeded_view.bindEvents(embed.find('.tr_' + record_id).find('.viewTDEmbedded'));
        if(embed.find('.tr_'+ record_id).parents('table').attr('form-events') && $('.tr_'+ record_id).parents('table').attr('form-events') != "" && embed.find('.tr_'+ record_id).parents('table').attr('form-events') != "[]"){
        	console.log("wrong condition...");
	        if(embed.find('.tr_'+ record_id).attr('is-pending') == "true"){
	        	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
	        		EmbedFormEvents.init(embed.find('.tr_'+ frm_id  + '_' +  record_id), frm_id, "0", embed.find('.tr_'+ frm_id + '_' +  record_id).parents('.embed-view-container'), 'onload', true, 'insert');
	        	}
	        	else{
	        		EmbedFormEvents.init(embed.find('.tr_'+ record_id), embed.attr('embed-source-form-val-id'), "0", embed.find('.tr_'+ record_id).parents('.embed-view-container'), 'onload', true, 'insert');
	        	}
			}
			else{
				if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
					EmbedFormEvents.init(embed.find('.tr_'+ frm_id  + '_' +  record_id), frm_id, record_id, embed.find('.tr_'+ frm_id + '_' +  record_id).parents('.embed-view-container'), 'onload', false, 'insert');
				}
				else{
					EmbedFormEvents.init(embed.find('.tr_'+ record_id), embed.attr('embed-source-form-val-id'), record_id, embed.find('.tr_'+ record_id).parents('.embed-view-container'), 'onload', false, 'insert');
				}
			}
        }
        else{
        	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
        		if(embed.find('.tr_'+  frm_id + '_' +  record_id).attr('is-pending') == "true"){
	        		EmbedFields.initV2('.tr_'+ frm_id  + '_' + record_id, frm_id, "0", true, embed.closest('.setOBJ').attr('data-object-id'), embed.find('.tr_'+ frm_id  + '_' + record_id));
	        	}
	        	else{
        			EmbedFields.initV2('.tr_'+ frm_id  + '_' + record_id, frm_id, record_id, true, embed.closest('.setOBJ').attr('data-object-id'), embed.find('.tr_'+ frm_id  + '_' + record_id));
        		}
        	}
        	else{
		        if(embed.find('.tr_'+ record_id).attr('is-pending') == "true"){
					EmbedFields.initV2('.tr_'+ record_id, embed.attr('embed-source-form-val-id'), "0", true, embed.closest('.setOBJ').attr('data-object-id'), embed.find('.tr_'+ record_id));
				}
				else{
					EmbedFields.initV2('.tr_'+ record_id, embed.attr('embed-source-form-val-id'), record_id, true, embed.closest('.setOBJ').attr('data-object-id'), embed.find('.tr_'+ record_id));
				}
        	}
		}
        $('.embedded_wrapper').remove();
        if(embed.attr('embed-action-click-number') == "true"){
	        embeded_view.fixNumbering(embed);
	    }
	    EmbeddedFixHeader.reFixHeader(embed);
	    // if($.type(is_import) != "undefined"){
	    // 	if(is_import != true){
	    // 		$(embed).animate({
	    // 		    scrollTop: embed.find('.tr_'+ record_id).offset().top
	    // 		}, 1000);
	    // 	}
	    // }
	    // else{
	    // 	$(embed).animate({
	    // 	    scrollTop: embed.find('.tr_'+ record_id).offset().top
	    // 	}, 1000);
	    // }
    },
    "bindCreateEvents": function(embed, tr_line){
        $(tr_line).on('click', '.okInsertDEmbedded', function(){
        	var embed = $(this).closest('.embed-view-container');
        	EmbedFields.error_counter = 0;
        	console.log("dease", tr_line);
        	console.log(embed.closest('.setOBJ').attr('data-object-id'));
        	EmbedFields.fieldValueFixes(undefined,tr_line, 0);
		   	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
   	     		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_line.attr('form-id') + '_' + tr_line.attr('record-id')), tr_line.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'), tr_line.attr('form-id'));
   	    	}
   	    	else{
   	    		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_line.attr('record-id')), tr_line.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'));
   	    	}
        	// EmbedFields.embedInputValidation(embed.find('.tr_' + tr_line.attr('record-id')), tr_line.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'));
        	console.log("error counter", EmbedFields.error_counter);
        	if(EmbedFields.error_counter <= 0){

	            if($(this).parents('.embed-view-container').attr('commit-data-row-event') != "parent"){
	            	var self = $(this);
	            	var message = "Are you sure you want to add this record?";
	            	if($(tr_line).attr('on-edit')){
	            		message = "Are you sure you want to edit this record?";
	            	}
	            	var newConfirm = new jConfirm(message, 'Confirmation Dialog', '', '', '', function(r) {
	            		var filter_values = function(field_models, fields_data, tr_line, prefix){
	            			var json_new_keypair = {};
	            			var tr = $(tr_line);

	            			for(i in field_models){
	            				if(i.indexOf(prefix) > -1){
	            					var new_i = i.replace(prefix, '');
	            					console.log("filter value:", field_models[i], "field name", i);
	            					if($.type(field_models[i]) != "undefined"){
	            						if(typeof field_models[i] == "number"){
	            							json_new_keypair[new_i] = field_models[i];
	            						}
	            						else{
	            							if(fields_data[new_i]['field_input_type'] == "Number"){
	            								json_new_keypair[new_i] = Number(field_models[i]);
	            							}
	            							else if(fields_data[new_i]['field_input_type'] == "Currency"){
	            								json_new_keypair[new_i] = field_models[i].replace(/,/g, '');
	            							}
	            							else{
	            								if($.type(field_models[i]) == "null"){
	            									json_new_keypair[new_i] = "";
	            								}
				            					else{
				            						if(field_models[i].indexOf('|^|') > -1){
					            						var multi_values = field_models[i].split('|^|');
					            						json_new_keypair[new_i] = multi_values;
					            					}
					            					else{
					            						json_new_keypair[new_i] = field_models[i];
					            					}
					            				}
				            				}
			            				}
		            				}
		            				else{
		            					if(new_i != "--Select--"){
		            						json_new_keypair[new_i] = (field_models[i]||"");
		            					}
		            				}
	            				}
	            				else if(i == "RequestID"){
	            					var new_i = "ID";
	            					if($.type(field_models[i]) != "undefined" && field_models[i] != ""){
	            						json_new_keypair[new_i] = field_models[i];
	            					}
	            					else{
	            						json_new_keypair[new_i] = '0';
	            					}
	            				}
	            				else{
	            					json_new_keypair[i] = field_models[i];
	            				}
	            				
	            			}
	            			return json_new_keypair;
	            		}
	            		var fields_data_formatter = function(new_data, form_id, reference_id){
	            			var fields_data = $('body').data('embed_row_data')[form_id][reference_id]['fields_data'];
	            			for(i in new_data){
	            				// console.log("field name:", i,"new_data", new_data[i], "field_data", fields_data[i]);
	            				if(i != "undefined"){
	            					if($.type(fields_data[i]) != "undefined"){
	            						if(fields_data[i]['type'] == 'checkbox' || fields_data[i]['type'] == 'selectMany'){
	            							fields_data[i]['value'] = new_data[i].split('|^|');
	            						}
	            						else{
		            						fields_data[i]['value'] = new_data[i];
		            					}
		            				}
	            				}
	            			}
	            			// $('body').data('embed_row_data')[form_id][reference_id]['fields_data'] = fields_data;
	            			return fields_data;
	            		}
	            		if(r == true){
	            			// EmbedFormEvents.EmbedFormulaEventController(self.parents('.embed-view-container'), 'presave', self.parents('tr').attr('record-id'), '.' + self.parents('tr').attr('reference-id'));
	            			var form_id = self.parents('.embed-view-container').attr('embed-source-form-val-id');
	            			var obj_id = self.parents('.setOBJ').attr('data-object-id');
	            			var reference_id = self.parents('tr').attr('reference-id');
	            			var record_id = self.parents('tr').attr('record-id');
	            			var embed_data = $('body').data('embed_row_data')[obj_id][reference_id];
	            			var fields_data = embed_data['fields_data'];
	            			var field_models = self.parents('tr').data('field_models');
	            			console.log("fields_data", fields_data, "field_models", field_models);
	            			var json_data = filter_values(field_models, fields_data, reference_id, 'embed_');
	            			json_data['Mode'] = 'formApproval';
	            			json_data['FormID'] = form_id;
	            			console.log("json_data", json_data);
	            			ui.block();
	            			$.post('/ajax/request', json_data, function(echo){
	            				console.log("echoasdasdasd", echo);
	            				var result = JSON.parse(echo);
	            				var new_record_id = result['id'];
	            				var new_ref = obj_id + "_" + result['id'];
	            				self.parents('tr').removeClass('tr_' + record_id).addClass('tr_' + new_record_id);
	            				self.parents('tr').removeClass(reference_id).addClass(new_ref);
	            				self.parents('tr').attr('record-id', result['id']);
	            				self.parents('tr').attr('reference-id', new_ref);
	            				self.parents('tr').attr('record-trackno', result['trackNo']);
	            				self.parents('tr').removeAttr('is-pending');
	            				self.parents('tr').find('.lblEmbed_' + record_id).attr('id', 'lblEmbed_' + new_record_id).removeClass('lblEmbed_' + record_id).addClass('lblEmbed_' + new_record_id);
	            				self.parents('tr').find('.embed_getFields').removeClass('inlineEdit_' + record_id).addClass('inlineEdit_' + new_record_id);
	            				var get_actions = self.parents('.embed_action');
	            				get_actions.find('#showEditActions_' + record_id).attr('id','showEditActions_' + new_record_id).removeClass('display');
	            				get_actions.find('#hideEditActions_' + record_id).attr('id', 'hideEditActions_' + new_record_id).addClass('display');
	            				get_actions.find('.showpasteActions_' + record_id).removeClass('showpasteActions_' + record_id).addClass('showpasteActions_' + new_record_id).removeClass('display');
	            				get_actions.find('.hidepasteActions_' + record_id).removeClass('hidepasteActions_' + record_id).addClass('hidepasteActions_' + new_record_id).addClass('display');
	            				embed.find('.tr_' + new_record_id).data('field_models')['RequestID'] = result['id'];
	            				var new_field_data = fields_data_formatter(result['data'], obj_id, reference_id);
	            				console.log("new_field_data", new_field_data);
	            				var get_details = $('body').data('embed_row_data')[obj_id][reference_id];
	            				get_details['submitted'] = true;
	            				delete $('body').data('embed_row_data')[obj_id][reference_id];
	            				$('body').data('embed_row_data')[obj_id][new_ref] = get_details;
	            				embeded_view.updateEmbedRow(self.parents('.embed-view-container'), new_field_data, new_ref, new_record_id);

	            				var result_field = self.parents('.embed-view-container').attr('embed-result-field-val');
	            				 $('[name="' + result_field + '"').trigger('change');
	            				// $('body').data('embed_row_data')[obj_id][reference_id]['submitted'] = true;
	            				tr_line.find('.showpasteActions_' + tr_line.attr('record-id')).removeClass('display');
	            				tr_line.find('.viewTDEmbedded').removeClass('display');
	            				tr_line.find('.deleteEmbedded').removeClass('display');
	            				self.parents('.embed-view-container').find('.disable-clickable').removeClass('disable-clickable');
	            				var result_field_name = self.parents('.embed-view-container').attr('embed-result-field-val');
	            				$('.loaded_form_content').find('[name="' + result_field_name + '"]').trigger('change');
	            				EmbeddedFixHeader.reFixHeader(self.parents('.embed-view-container'));
	            				ui.unblock();
	            			});
	            		}
	            	});
	            	newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right" style="color:#f00; font-size:15px;"></i>'});   
	            }
	            else{
	            	ui.blockPortion($(this).parents('.embed-view-container'));
	            	//finding ian
	            	if($('.temp-display').length > 0){
	            		$(this).parents('td').addClass('temp-display');
	            	}
	            	showHideEmbedActions($(this).parents('.embed-view-container'), 'hide');
	            	EmbedFormEvents.EmbedFormulaEventController($(this).parents('.embed-view-container'), 'presave', $(this).parents('tr').attr('record-id'),'.' + $(this).parents('tr').attr('reference-id'));
	            	var tr_reference = $(this).parents('tr').attr('reference-id');
	            	var form_id = $(this).parents('.setOBJ').attr('data-object-id');
	            	var data = $('body').data('embed_row_data');
	            	var diff = function(array, a) {
	            	    return array.filter(function(i) {return a.indexOf(i) < 0;});
	            	}
	            	var get_field_list = $.map(JSON.parse($(this).parents('.embed-view-container').attr('data-embed-sortedcolumn'))['sortedcols'], function(value, index){
	            	    return value['FieldName']
	            	});
	            	var get_displayed_field_list = $.map(JSON.parse($(this).parents('.embed-view-container').attr('embed-column-data')), function(value, index){
	            	    return value['FieldName']
	            	});
	            	var get_invisible_fields = diff(get_field_list, get_displayed_field_list);
	            	console.log("tr_reference", tr_reference, "form_id", form_id, "data", data);
	            	data[form_id][tr_reference]['invisible_fields'] = get_invisible_fields;
	            	$(this).parents('tr').attr('current', '');
	            	console.log("egul dito", data[form_id][tr_reference]);
	            	console.log($(this).parents('tr').data('field_models'));
	            	data[form_id][tr_reference]['field_models'] = $(this).parents('tr').data('field_models');
	            	$('body').data('embed_row_data')[form_id][tr_reference]['submitted'] = false;
	            	embeded_view.createTemporaryData($(embed), "insert", "in-line", tr_reference);
	            	tr_line.find('.showpasteActions_' + tr_line.attr('record-id')).removeClass('display');
	            	tr_line.find('.viewTDEmbedded').removeClass('display');
	            	tr_line.find('.deleteEmbedded').removeClass('display');
	            	EmbeddedFixHeader.reFixHeader($(this).parents('.embed-view-container'));
	            	ui.unblockPortion($(this).parents('.embed-view-container'));
	            }
	            
	            
        	}

        });
        $(tr_line).on('click', '.okeditDEmbedded', function(){
        	EmbedFields.error_counter = 0;
        	var embed = $(this).closest('.embed-view-container');
        	// EmbedFields.fieldValueFixes(undefined,tr_line);
        	ui.blockPortion($(this).parents('.embed-view-container'));
		   	if(typeof embed.attr('source-form-type') != "undefined" && embed.attr('source-form-type') == "multiple"){
   	     		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_line.attr('form-id') + '_' + tr_line.attr('record-id')), tr_line.attr('record-id'), tr_line.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'), tr_line.attr('form-id'));
   	    	}
   	    	else{
   	    		EmbedFields.embedInputValidation(embed.find('.tr_' + tr_line.attr('record-id')), tr_line.attr('record-id'), tr_line.attr('record-id'), embed.closest('.setOBJ').attr('data-object-id'));
   	    	}
        	// EmbedFields.embedInputValidation(embed.find('.tr_' + tr_line.attr('record-id')), tr_line.attr('record-id'),  embed.closest('.setOBJ').attr('data-object-id'));
        	if(EmbedFields.error_counter <= 0){
            	var tr_reference = $(this).parents('tr').attr('reference-id');
            	var form_id = $(this).parents('.embed-view-container').attr('embed-source-form-val-id');
            	var data = $('body').data('embed_row_data');
            	var diff = function(array, a) {
            	    return array.filter(function(i) {return a.indexOf(i) < 0;});
            	}
            	var get_field_list = $.map(JSON.parse($(this).parents('.embed-view-container').attr('data-embed-sortedcolumn'))['sortedcols'], function(value, index){
            	    return value['FieldName']
            	});
            	var get_displayed_field_list = $.map(JSON.parse($(this).parents('.embed-view-container').attr('embed-column-data')), function(value, index){
            	    return value['FieldName']
            	});
            	var get_invisible_fields = diff(get_field_list, get_displayed_field_list);
            	$(this).parents('tr').attr('current', '');
            	console.log("form_id", form_id, "tr_reference", tr_reference);
            	data[form_id][tr_reference]['invisible_fields'] = get_invisible_fields;
            	data[form_id][tr_reference]['field_models'] = $(this).parents('tr').data('field_models');
            	embeded_view.createTemporaryData($(embed), "insert", "in-line", tr_reference);
            	$(this).parents('.embed-view-container').find('.disable-clickable').removeClass('disable-clickable');
            	EmbeddedFixHeader.reFixHeader($(this).parents('.embed-view-container'));
            	ui.unblockPortion($(this).parents('.embed-view-container'));
            }
            else{
            	ui.unblockPortion($(this).parents('.embed-view-container'));
            }
        });
		$(tr_line).on('click', '.deleteEmbedded', function(){
            if($(this).parents('.embed-view-container').attr('commit-data-row-event') == 'parent'){
            	var tr_reference = $(this).parents('tr').attr('reference-id');
            	var form_id = $(this).parents('.setOBJ').attr('data-object-id');
            	var data = $('body').data('embed_row_data');
            	var embed = $(this).parents('.embed-view-container');
            	data[form_id][tr_reference] = undefined;
            	$(this).parents('tr').remove();
            	embeded_view.getSetTotalOfColumn.call(embed, embed.children());
            	embeded_view.fixNumbering(embed);
            	if($('.okInsertDEmbedded:visible, .okeditDEmbedded:visible').length <= 0){
            		$('.embed-edit-mode').removeClass('embed-edit-mode');
            	}
            	EmbeddedFixHeader.reFixHeader($(this).parents('.embed-view-container'));
            }
            else{
            	var trackNo = $(this).parent().parent().parent().attr("record-trackno");
            	var recordID = $(this).parent().parent().parent().attr("record-id");
            	var formID = $(this).parent().parent().parent().parent().parent().parent().attr("embed-source-form-val-id");
            	var action = "embed_deleteRecord";
            	var data = {action:action,trackNo:trackNo,recordID:recordID,formID:formID}
            	var resultFld = $(this).parent().parent().parent().parent().parent().parent().attr("embed-result-field-val");
            	var message = "Are you sure you want to delete this record?";
            	
            	var newConfirm = new jConfirm(message, 'Confirmation Dialog', '', '', '', function(r) {
            	    if (r == true) {
            		//console.log("Trank No: " + trackNo + "\n")
            		//console.log("Record ID: " + recordID + "\n")
            		//console.log("Form ID: " + formID + "\n")
            		
            		$.post("/ajax/embedded_view_functions",data,function(e){
            		    var json_data = JSON.parse(e);
            		    
            		    if (json_data['notification'] == "Successfull") {
            			showNotification({
            			    message: json_data['message'],
            			    type: "success",
            			    autoClose: true,
            			    duration: 3
            			});
            				
            			// Remove Row
            			$('tr[record-trackno="' + trackNo + '"]').remove();
            			
            			$("[name='" + resultFld + "']").trigger("change");
            		    }
            		});
            		
            	    }
            	});
            	
            	newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>'});
            }
        });
        $(tr_line).on('click', '.editCreatedEmbed', function(){
        	var self = $(this);
        	var record_id = $(this).parents('tr').attr('record-id');
        	var tr_reference = $(this).parents('tr').attr('reference-id');
        	var form_id = $(this).parents('.setOBJ').attr('data-object-id');
        	var data = $('body').data('embed_row_data');
        	self.parents('tr').data('old_field_models', self.parents('tr').data('field_models'));
        	$('body').data('embed_row_data')[form_id][tr_reference]['old_fields_data'] = data[form_id][tr_reference]['fields_data'];
        	checkEmbedAttr($(this).parents('tr'));
        	self.parents('.embed_action').find('.viewTDEmbedded').addClass('display');
        	self.parents('.embed_action').find('#deleteTDEmbedded').addClass('display');
        	self.parents('.embed_action').find('#deleteEmbedded').addClass('display');
        	console.log("feid", data[form_id][tr_reference]['fields_data'], "field_models",  $(this).parents('tr').data('field_models'));
        	$(".showpasteActions_" + tr_reference).addClass("display");
        	EmbeddedFixHeader.reFixHeader($(this).parents('.embed-view-container'));
        	
        });
        $(tr_line).on('click', '.cancelInsertDEmbedded', function(){
        	console.log($(this).parents('tr').data('old_field_models'));
        	if($(this).parents('tr').data('old_field_models')){
	            var tr_reference = $(this).parents('tr').attr('reference-id');
	            var form_id = $(this).parents('.setOBJ').attr('data-object-id');
	            var record_id = $(this).parents('tr').attr('record-id');
	            // var data = $('body').data('embed_row_data');
	            var diff = function(array, a) {
	                return array.filter(function(i) {return a.indexOf(i) < 0;});
	            }
	            var get_field_list = $.map(JSON.parse($(this).parents('.embed-view-container').attr('data-embed-sortedcolumn'))['sortedcols'], function(value, index){
	                return value['FieldName']
	            });
	            var get_displayed_field_list = $.map(JSON.parse($(this).parents('.embed-view-container').attr('embed-column-data')), function(value, index){
	                return value['FieldName']
	            });
	            var get_invisible_fields = diff(get_field_list, get_displayed_field_list);
	            console.log("tr_reference", tr_reference, "form_id", form_id, "data", data);
	            $('body').data('embed_row_data')[form_id][tr_reference]['invisible_fields'] = get_invisible_fields;
	            $(this).parents('tr').attr('current', '');
	            $('body').data('embed_row_data')[form_id][tr_reference]['field_models'] = $(this).parents('tr').data('old_field_models');
	            $('body').data('embed_row_data')[form_id][tr_reference]['fields_data'] = $('body').data('embed_row_data')[form_id][tr_reference]['old_fields_data'];
	            $('body').data('embed_row_data')[form_id][tr_reference]['fields_data']['saved'] = true;
	            embeded_view.updateEmbedRow($(embed), $('body').data('embed_row_data')[form_id][tr_reference]['fields_data'], tr_reference, record_id);
	            EmbeddedFixHeader.reFixHeader($(embed));
	        }
	        else{
	        	var embedded = $(this).parents('.embed-view-container');
        	    var get_row_count_id = $(this).parents('tr').attr('reference-id');
        	    var get_form_id = get_row_count_id.split('_')[0];
        	    var data = $('body').data('embed_row_data');
        	    data[get_form_id][get_row_count_id] = undefined;
        	    $(this).parents('tr:eq(0)').remove();
        	    embeded_view.fixNumbering(embed);
        	    EmbeddedFixHeader.reFixHeader(embedded);
	        }
	        if($('.okInsertDEmbedded:visible, .okeditDEmbedded:visible').length <= 0){
	        	$('.embed-edit-mode').removeClass('embed-edit-mode');
	        }
	        EmbeddedFixHeader.reFixHeader($(this).parents('.embed-view-container'));
        });
		// $(tr_line).on('click', '.deleteTDEmbedded', function(){
    },
    "setupEmbedActions": function(embed, record_id, inLineInsert){
        var embed = $(embed);
        // var getLastRecordId = $(embed).children('table').children('tbody').find('tr[record-id]').map(function(){
        //     return $(this).attr('record-id');
        // }).sort(function(a, b){
        //     return b - a;
        // })[0]||0;
        var record_id = record_id;
        var action_str = '';
        var actionDisplay = ''; 
        var visibility_formulas = embed.attr('visible-formula');
        var copy_action="", delete_action="", inline_edit_action="", popup_action="", view_action="";
        var f = new Formula(visibility_formulas);
        var result = f.getEvaluation();
        if($.type(visibility_formulas) != undefined){
        	for(i = 1; i < result.length; i+=2){
        		var formula = result[i-1];
        		var target = result[i];
        		// console.log("target", target, "formula", formula);
        		if(target == 4 && formula == false){
        			//copy action
        			copy_action = "style='display:none;'";
        		}
        		else if(target == 5 && formula == false){
        			//delete action
        			delete_action = "style='display:none;'";
        		}
        		else if(target == 6 && formula == false){
        			//inline action
        			inline_edit_action = "style='display:none;'";
        		}
        		else if(target == 7 && formula == false){
        			//popup action
        			popup_action = "style='display:none;'";
        		}
        		else if(target == 8 && formula == false){
        			//view action
        			view_action = "style='display:none;'";
        		}
        	}
        }
        if(embed.find('thead th').eq(0)[0].style.display == 'none'){
        	actionDisplay = 'style="display: none;"';
        }
        if($.type(inLineInsert) != "undefined"){
            action_str += '<td '+ actionDisplay + '><div class="fl-table-ellip embed_action" style="text-align: center;">';
            if(embed.attr('embed-action-click-view') == "true"){
            	var display = "";
            	if(inLineInsert){
                	display = " display";
                }
                action_str += '<i class="fa fa-search dataTip cursor viewTDEmbedded' + display + '" data-action-attr="view" id="" data-original-title="View Record" ' + view_action + '></i>&nbsp;&nbsp;';
            }
            if(embed.attr('embed-action-click-edit-popup') == "true"){
            	var display = "";
            	if(inLineInsert){
                	display = " display";
                }
                action_str += '<i class="display fa fa-edit dataTip cursor viewTDEmbedded' + display + '" data-action-attr="edit" id="" data-original-title="Popup Edit" ' + popup_action + '></i>&nbsp;&nbsp;';
            }
            if(embed.attr('embed-action-inline-create') == 'true'){
                action_str += '<i id="showEditActions_' + record_id + '" class="display">';
                    action_str += '<i class="fa fa-pencil dataTip cursor editCreatedEmbed" id="editCreatedEmbed" data-original-title="Inline Edit"  ' + inline_edit_action + '></i>&nbsp;&nbsp;';
                action_str += '</i>';
                action_str += '<i id="hideEditActions_' + record_id + '" class="">';
                    action_str += '<i class="fa fa-check dataTip cursor okInsertDEmbedded" id="okInsertDEmbedded" data-original-title="Save Record"></i>&nbsp;&nbsp;';
                    action_str += '<i class="fa fa-times dataTip cursor cancelInsertDEmbedded" id="cancelInsertDEmbedded" data-original-title="Cancel Record"></i>&nbsp;&nbsp;';
                action_str += '</i>';
            }
            if(embed.attr('embed-action-click-copy') == "true"){
            	var display = "";
            	if(inLineInsert){
                	display = " display";
                }
                action_str += '<i class="showpasteActions_' + record_id + display + '">';
                    action_str += '<i class="fa fa-copy dataTip cursor" id="copyTDEmbedded" data-original-title="Copy Record" ' + copy_action + '></i>&nbsp;&nbsp;';
                action_str += '</i>'
                action_str += '<i class="hidepasteActions_' + record_id + ' display">';
                    action_str += '<i class="fa fa-paste dataTip cursor " id="pasteTDEmbedded" data-original-title="Paste Record"></i>&nbsp;&nbsp;';
                    action_str += '<i class="fa fa-times dataTip cursor cancelpasteDEmbedded" data-original-title="Cancel Copy Record"></i>&nbsp;&nbsp;';
                action_str += '</i>';
            }
            if(embed.attr('embed-action-click-delete') == "true"){
                var display = "";
            	if(inLineInsert){
                	display = " display";
                }
            	action_str += '<i class="fa fa-trash-o dataTip cursor deleteEmbedded' + display + '" id="deleteEmbedded" data-original-title="Delete Record" ' + delete_action + '></i>&nbsp;&nbsp;';
            }
        }
        else{
            action_str += '<td '+ actionDisplay + '><div class="fl-table-ellip embed_action" style="text-align: center;">';

            if(embed.attr('embed-action-click-view') == "true"){
                action_str += '<i class="fa fa-search dataTip cursor viewTDEmbedded" data-action-attr="view" id="" data-original-title="View Record" ' + view_action + '></i>&nbsp;&nbsp;';
            }
            if(embed.attr('embed-action-click-edit-popup') == "true"){
                action_str += '<i class="fa fa-edit dataTip cursor viewTDEmbedded" data-action-attr="edit" id="" data-original-title="Popup Edit" ' + popup_action + '></i>&nbsp;&nbsp;';
            }
            if(embed.attr('embed-action-click-edit') == "true"){
                action_str += '<i id="showEditActions_' + record_id + '" class="" ' + inline_edit_action + '>';
                    action_str += '<i class="fa fa-pencil dataTip cursor" id="editDEmbedded" data-original-title="Inline Edit"></i>&nbsp;&nbsp;';
                action_str += '</i>';
                action_str += '<i id="hideEditActions_' + record_id + '" class="display">';
                    action_str += '<i class="fa fa-check dataTip cursor " id="okeditDEmbedded" data-original-title="Save Edit"></i>&nbsp;&nbsp;';
                    action_str += '<i class="fa fa-times dataTip cursor " id="canceleditDEmbedded" data-original-title="Cancel Edit"></i>&nbsp;&nbsp;';
                action_str += '</i>';
            }
            if(embed.attr('embed-action-click-copy') == "true"){
                action_str += '<i class="showpasteActions_' + record_id + '">';
                    action_str += '<i class="fa fa-copy dataTip cursor" id="copyTDEmbedded" data-original-title="Copy Record" ' + copy_action + '></i>&nbsp;&nbsp;';
                action_str += '</i>'
                action_str += '<i class="hidepasteActions_' + record_id + ' display">';
                    action_str += '<i class="fa fa-paste dataTip cursor " id="pasteTDEmbedded" data-original-title="Paste Record"></i>&nbsp;&nbsp;';
                    action_str += '<i class="fa fa-times dataTip cursor cancelpasteDEmbedded" data-original-title="Cancel Copy Record"></i>&nbsp;&nbsp;';
                action_str += '</i>';
            }
            if(embed.attr('embed-action-click-delete') == "true"){
                action_str += '<i class="fa fa-trash-o dataTip cursor deleteEmbedded" id="deleteTDEmbedded" data-original-title="Delete Record" ' + delete_action + '></i>&nbsp;&nbsp;';
            }
            action_str += "</div></td>";
        }
        // console.log("action_str", action_str);
        return action_str;
    },
    "tempoEmbeddedListRefresh": function(embed, is_import){
    	console.log("tempo refreshed.");
        var data = $('body').data('embed_row_data');
        var new_data = {};
        for(form_id in data){
            new_data[form_id] = {};
            for(form_id_row in data[form_id]){
                if($.type(data[form_id][form_id_row]) != "undefined"){
                    new_data[form_id][form_id_row] = data[form_id][form_id_row];
                    var inline_display = data[form_id][form_id_row]['saved'];
                    var isSubmitted = data[form_id][form_id_row]['submitted'];
                    var embedded = $('[data-object-id="' + form_id + '"]').find('.embed-view-container');
                    // if($(embed).attr('commit-data-row-event') == 'parent'){
                    // 	isSubmitted = false;
                    // }
                    // if(isSubmitted == false){
	                    if(inline_display == true){
	                    	if($.type(is_import) != "undefined"){
	                    		embeded_view.addToEmbedList($(embedded), data[form_id][form_id_row]['fields_data'], form_id_row, data[form_id][form_id_row]['query_type'], false, is_import);	
	                    	}
	                    	else{
	                    		embeded_view.addToEmbedList($(embedded), data[form_id][form_id_row]['fields_data'], form_id_row, data[form_id][form_id_row]['query_type']);	
	                    	}
	                    }
	                    else{
	                    	embeded_view.addToEmbedList($(embedded), data[form_id][form_id_row]['fields_data'], form_id_row, data[form_id][form_id_row]['query_type'], true);
	                    }
                    // }
                }
            }
        }
        // $('body').data('embed_row_data', new_data);
    },
    "bindEvents": function(selector){
        $(selector).each(function(){
            $(this).on({
                "click": function () {
                    var dis_ele = $(this);
                    var embed_attr = dis_ele.attr('data-action-attr');
                    var embed_view_cont = dis_ele.parents(".embed-view-container");
                    var formId = $(embed_view_cont).attr('embed-source-form-val-id');
                    var data_object_id = $(embed_view_cont).closest('.setOBJ').attr('data-object-id');
                    var reference_id = $(this).parents('tr').eq(0).attr('reference-id');
                    $('html').data("embed_container", embed_view_cont);
                    var embeded_attr = $(this).attr("data-action-attr");
                    var allow_row_click = $(embed_view_cont).attr("enable-embed-row-click")
                    if(dis_ele.closest('tr').is('[is-locked]')){
                    	return;
                    }
                    if (allow_row_click) {
                        if (allow_row_click != "true") {
                            return;
                        }
                    } else {
                        return;
                    }
                    var dis_row_click_ele = $(this).parents("tr").eq(0);
                    var row_click_data = {
                        "record_trackno": dis_row_click_ele.attr("record-trackno"),
                        "record_id": dis_row_click_ele.attr("record-id")
                    }

                    var ret = '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
                            '<div class="hr"></div>';
                    var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "", function () {
                    });
                    dis_dialog.themeDialog("modal2");
                    var ele_dialog = $('#popup_container');
                    //console.log('/user_view/workspace?view_type=request&formID='+formId+'&requestID='+row_click_data["record_id"]+'&trackNo='+row_click_data["record_trackno"]);
                    if($(dis_row_click_ele).attr('is-pending') != "true" || $(dis_row_click_ele).attr('is-pending') == "" || $.type($(dis_row_click_ele).attr('is-pending')) == "undefined"){
                        
                        if (embeded_attr == "view") {
                            var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
                        } else {
                            var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
                        }
                    }
                    else{
                        if(embed_attr == "view"){
                            var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=0&embed_type=viewEmbedOnly';
                        }
                        else{
                        	if(row_click_data['record_trackno'].indexOf('pending') > -1){
                    			// var fields_data = $('body').data('embed_row_data')[formId][reference_id]['fields_data'];
                    		    var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=0';
                        	}
                        	else{
	                        	// var fields_data = $('body').data('embed_row_data')[formId][reference_id]['fields_data'];
	                        	var view = "&trackNo="+ row_click_data['record_trackno'] + '&embedded_view_object=true';// + "&requestor=" + fields_data['requestor-name']['value'] + "&status=" +  fields_data['status-embed']['value'];
	                            var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + dis_row_click_ele.attr("record-id") + view;
	                        }
                        }
                    }
                    var my_dialog = $(
                            '<div class="embed-iframe" style="height: 100%;">' +
                            '<iframe track_no="' + row_click_data["record_trackno"] + '" record-id="' + row_click_data["record_id"] + '" style="height:100%;width:100%;" src="' + src + '">' +
                            '</iframe>' + '</div>'
                            )

                    ele_dialog.find("#popup_content").html(my_dialog);

                    ele_dialog.find("#popup_content").css({
                        "height": ($(window).outerHeight() - 120) + "px"
                    });
                    ele_dialog.css({
                        "top": "30px"
                    });
                    fieldReference = embed_view_cont.attr("embed-result-field-val");
                    parent_container_embed = embed_view_cont;
                    ele_dialog.data("field_updater", fieldReference);
                    ele_dialog.data("embed_container", parent_container_embed);
                    var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
                    var loading_panel = panel.load(ele_dialog.find("#popup_content"));
                    loading_panel.parent().css("position", "relative");
                    //added by japhet morada

                    //for embed reimplementation...
                    var data = $('body').data('embed_row_data');
                    request_data = data[data_object_id][reference_id]['fields_data']
                    var get_request_type = request_data['TrackNo']['value']||"";
                    //===========================================================
                    iframe_ele_dialog.load(function () {
                        var iframe_document = $(this)[0].contentWindow.$('html');
                        EmbedPopUpDisplayFixes.call(iframe_document);
                        loading_panel.fadeOut();
                        iframe_document.addClass('view-embed-iframe');
                        iframe_document.find('#frmrequest').attr('embedded-view-object', true);
                        embeded_view.distributeFieldsData(reference_id, $(this), parent_container_embed);
                    })
                    if(get_request_type.indexOf('pending') > -1){
                    	if($.type(reference_id) == "undefined"){
                    		embeded_view.createTemporaryData($(parent_container_embed), "insert", "popup");
                    	}
                    	else{
                    		embeded_view.createTemporaryData($(parent_container_embed), "update", "popup", reference_id);
                    	}
                    }
                    else{
                        embeded_view.createTemporaryData($(parent_container_embed), "update", "popup", reference_id);
                    }
                    EmbeddedFixHeader.reFixHeader($(parent_container_embed));
                    // embeded_view.getRequests(parent_container_embed.data("embed_view_reference"), function(data) {
                    //     $(container).html(data);
                    // });

                    // var iframe_ele = my_dialog.find("iframe").eq(0);
                    // iframe_ele.load(function(){
                    //     var iframe_doc = $(iframe_ele).contents()[0];
                    //     console.log("FRAAA",$(iframe_doc))
                    // })
                
                     // Roni Pinili close button modal configuration
                        
                    if($('body').data("user_form_json_data")){
                        
                        var embeddedViewObjId =  $(this).parents('.setOBJ').eq(0).attr('data-object-id');

                        if($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]){ 
                            var embededViewActionVisibilityVal =  $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
                            console.log("VALUE", embededViewActionVisibilityVal);  
                            if (embededViewActionVisibilityVal == "Yes") {
                                var close_dial = setInterval(function(){
                                    $('.fl-closeDialog').hide();
                                    if($('.fl-closeDialog').length >=1){
                                        clearInterval(close_dial);
                                    }
                                },0);
                            }
                        }

                    }
                }
            });
        });
    },
    "distributeFieldsData": function(ref_id, iframe, embed){
        var data = $('body').data('embed_row_data');
        var form_id = ref_id.split('_')[0];
        var request_data = data[form_id][ref_id]['fields_data'];
        var record_id = ref_id.split('_')[1];
        AsyncLoop(request_data, function(index, value){
            if(value['type'] == "radioButton"){
                $(iframe).contents().find('[name="'+ index + '"][value="' + $.trim(value['value']) + '"]').prop('checked', true);
                // console.log("iframe values",  $(iframe).contents().find('[name="'+ index + '"][value="' + value['value'] + '"]'));
            }
            else if(value['type'] == "checkbox"){
                for(i in value['value']){
                    $(iframe).contents().find('[name="'+ index + '[]"][value="' + $.trim(value['value'][i]) + '"]').prop('checked', true);   
                }
            }
            else if(value['type'] == "selectMany"){
                for(i in value['value']){
                    $(iframe).contents().find('[name="'+ index + '[]"]').find('option[value="' + value['value'][i] + '"]').prop('selected', true);   
                }
            }
            else if(value['type'] == 'multiple_attachment_on_request'){
            	var parse_file_type = $.parseJSON(value['value']);
            	if ($.type(parse_file_type) == "string"){
            	    parse_file_type = $.parseJSON(parse_file_type);
            	}
            	var location = parse_file_type[0]['location'];
            	var parse_files = $.parseJSON(parse_file_type[0]['file_type']);
            	console.log("");
            	var blkstr = [];
            	var file_name = parse_files['name'];
            	var file_length = file_name.length;
            	$.each(file_name, function (id, value) {
            	    blkstr.push(value);
            	});
            	var files = blkstr.join(", ");
            	if (parse_file_type == null) {
            	    var view_html = '<a id="view_files_in_modal" data-body-name="embed_' + data_json[i]['name'] + '_' + record_id + '" style="';
            	    view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
            	    view_html += '">No file attachment.</a>';
            	    current_line.find('[data-field-name="' + i + '"]').prev().children().eq(0).addClass('display');
            	    current_line.find('[data-field-name="' + i + '"]').parents('td').find('[id="lblEmbed_'+ getLastRecordId + '"]').html(view_html).removeClass('display');
            	}
            	else{
	            	// Save to body data
	            	$("body").data('embed_' + index + '_' + record_id, parse_file_type);
	            	var view_html = '<a id="view_files_in_modal" data-body-name="embed_' + index + '_' + record_id + '" style="';
	            	view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
	            	view_html += '"><u> <i class="fa fa-search"></i> Show (' + file_length + ') File`s </u></a>';
	            	$(iframe).contents().find('[name="'+ index + '"]').parent().append(view_html);
	            	$(iframe).contents().find('#'+ index).css('display', 'none');
	            	$(iframe).contents().find('[name="'+ index + '"]').val(value['value']);
	            }

            }
            else if(value['type'] == 'attachment_on_request'){
            	var view_html = '<a target="_blank" href="' + value['value'] + '" style="';
            	view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
            	var file = value['value'].split("/");
            	view_html += '"><u> <i class="fa fa-search"></i> View File </u> ( <strong>' + file[file.length - 1] + '</strong> )</a>';
            	$(iframe).contents().find('[name="'+ index + '"]').parent().append(view_html);
            	$(iframe).contents().find('#'+ index).css('display', 'none');
            	$(iframe).contents().find('[name="'+ index + '"]').val(value['value']);
            }
            else{
            	if(value['type'] == 'noteStyleTextarea'){
            		$(iframe).contents().find('[name="'+ index + '"]').parents('.note-style-wrap-contents').find('[contenteditable="true"]').text(value['value']);
            		$(iframe).contents().find('[name="'+ index + '"]').val(value['value']);
            	}
            	else{
	                $(iframe).contents().find('[name="'+ index + '"]').val(value['value']);
	            }
            }
        });
    },
    "displayTemporaryData":function(embedded_view){
    	var embed_row_data = $('body').data('embed_row_data');
    	for(i in embed_row_data){
    		var embed_records = embed_row_data[i];
    		for(j in embed_records){
    			var query = embed_records[j]['query_type'];
    			var origin = embed_records[j]['origin'];
    			embeded_view.createTemporaryData(embedded_view, query, origin, j);
    		}
    	}
    },
    "fixNumbering":function(embed_view){
    	var numbering = 1;
    	var column_number_index = (embed_view).find('.embed-table-numbering').index();
    	$(embed_view).find('tbody').first().children().each(function(index){
    		if(!$(this).hasClass('appendNewRecord')){
    			$(this).children().eq(column_number_index).text((numbering));
    			numbering++;
    		}
    	});
    },
    "distributeDataSending":function(data_send_json, tr_line){
    	for(i in data_send_json){
    		var value;
    		var data_type = $('[name="' + data_send_json[i]['epds_select_my_form_field_side'] + '"]').parents('.setOBJ').attr('data-type');
    		if(data_type == 'checkbox'){
    			value = $('[name="' + data_send_json[i]['epds_select_my_form_field_side'] + '"]:checked').map(function(){ return $(this).val(); });
    		}
    		else if(data_type == 'radioButton'){
    			value = $('[name="' + data_send_json[i]['epds_select_my_form_field_side'] + '"]:checked').val();
    		}
    		else if(data_type == 'selectMany'){
    			value = $('[name="' + data_send_json[i]['epds_select_my_form_field_side'] + '"]').find('option:selected').map(function(){return $(this).val()})
    		}
    		else{
    			value = $('[name="' + data_send_json[i]['epds_select_my_form_field_side'] + '"]').val();
    		}
    		data_send_json[i]['epds_select_my_form_field_side']
    		embeded_view.distributeToDifferentDataTypes(tr_line, data_send_json[i]['epds_select_popup_form_field_side'], data_type, value);
    	}
    },
    "distributeToDifferentDataTypes":function(tr_line, child_field, data_type, value){
    	if(data_type == "checkbox"){
    		var val;
    		if($.type(value) == "object"){
    			for(i in value){
    				tr_line.find('[data-field-name="' + child_field + '"][value="' + value[i] + '"]').prop('checked', true);
    			}
    			val = value.join('|^|');
    		}
    		else{
    			val = value;
    			tr_line.find('[data-field-name="' + child_field + '"][value="' + val + '"]').prop('checked', true);
    		}
    		tr_line.data('field_models')['embed_' + child_field] = val;
    	}
    	else if(data_type == "radioButton"){
    		tr_line.find('[data-field-name="' + child_field + '"][value="' + value + '"]').prop('checked', true);
    	}
    	else if(data_type == "selectMany"){
    		if($.type(value) == "object"){
    			for(i in value){
    				tr_line.find('[data-field-name="' + child_field + '"]').find('option[value="' + value[i] + '"]').attr('selected', true);
    			}
    			val = value.join('|^|');
    		}
    		else{
    			val = value;
    			tr_line.find('[data-field-name="' + child_field + '"][value="' + val + '"]').prop('checked', true);
    		}
    		tr_line.data('field_models')['embed_' + child_field] = val;
    	}
    	else{
    		tr_line.find('[data-field-name="' + child_field + '"]').val(value);
    		tr_line.data('field_models')['embed_' + child_field] = value;
    	}
    },
    "getFieldsData": function(form_id, embedded_view, type, import_data){
        var parameters = {
            "form_id": form_id,
            "columns_needed": ["field_name", "field_type", "formula", "multiple_values", "other_attributes", "formula_type", "field_input_type", "readonly", "object_id"],
            "get_row_count": true,
            "get_request_details": true
        }
        if(embedded_view.attr('commit-data-row-event') == "default"){
        	embedded_view.addClass('embed-edit-mode');
        }
        $.post('/ajax/embed_request', parameters, function(echo_result){
            console.log(echo_result);
            try{
                var result = JSON.parse(echo_result);
                var data = $('body').data();
                if(!data['embed_row_data']){
                    data['embed_row_data'] = {};
                }
                row_id = embedded_view.closest('.setOBJ').attr('data-object-id');//embedded_view.attr('embed-source-form-val-id');
                if(!data['embed_row_data'][row_id]){
                    data['embed_row_data'][row_id] = {};
                }
                var get_count_current_process_row = $('tr[is-pending="true"]').length||0;
                get_count_current_process_row += 1;
                var row_name_count = embedded_view.closest('.setOBJ').attr('data-object-id') + '_' + (Number(result['row_count']) + Number(get_count_current_process_row));
                console.log("row_name", row_name_count);
                if($.type(import_data) != "undefined"){
                	var new_row_name_count = "";
                	AsyncLoop(import_data, function(a, b){
                	// for(d in import_data){
            			if(new_row_name_count == ""){
            				new_row_name_count = row_name_count;
            			}
            			else{
            				new_row_name_count = row_id + "_" + Number(Number(new_row_name_count.split("_")[1]) + 1);	
            			}
        				data['embed_row_data'][row_id][new_row_name_count] = {
        				    "query_type": type,
        				    "origin": 'in-line',
        				    "saved": true,
        				    "form_id": embedded_view.attr('embed-source-form-val-id'),
        				    "fields_data": {}
        				};
        			    for(details in result['other_details']){
        			        if(details == 'buttonStatus'){
        			            var getBtnStatus = JSON.parse(result['other_details'][details]);
        			            data['embed_row_data'][row_id][new_row_name_count]['fields_data']['Node_ID'] = {
        			                "type": undefined, 
        			                "name": details,
        			                "value": getBtnStatus['child_id']
        			            }
        			        }
        			        else{
        			            data['embed_row_data'][row_id][new_row_name_count]['fields_data'][details] = {
        			                "type": undefined,
        			                "name": details,
        			                "value": result['other_details'][details]
        			            }
        			        }
        			    }
        			    for(r in result){
        			    	if(!isNaN(r)){
        			        	if(result[r]['field_type'] == "TrackNo"){
        			        	    data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        	        "type": result[r]['field_type'],
        			        	        "name": result[r]['field_name'],
        			        	        "formula_type": result[r]['formula_type']||"static",
        			        	        "field_input_type": result[r]['field_input_type']||"",
        			        	        "value": 'pending...'
        			        	    }
        			        	}
        			        	else if(result[r]['field_type'] == "pickList"){
        			        		console.log("kulambo", result[r]['other_attributes']);
        			        	    var other_attr = JSON.parse(result[r]['other_attributes']);
        			        	    data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        	        "type": result[r]['field_type'],
        			        	        "name": result[r]['field_name'],
        			        	        "id": other_attr['return-field'],
        			        	        "picklist_button_id": other_attr['picklist-button-id'],
        			        	        "picklist_type": other_attr['picklist-type'],
        			        	        "return_field": other_attr['return-field'],
        			        	        "form_id": other_attr['form-id'],
        			        	        "return_field_name": other_attr['return-field-name'],
        			        	        "formname": other_attr['formname'],
        			        	        "display_columns": other_attr['display_columns'],
        			        	        "display_column_sequence": other_attr['display_column_sequence'],
        			        	        "selection_type": other_attr['selection-type'],
        			        	        "enable_add_entry": other_attr['enable_entry'],
        			        	        "allow_values_not_in_list": other_attr['allow-values-not-in-list'],
        			        	        "show_picklist_view": '',
        			        	        "formula_type": result[r]['formula_type']||"static",
        			        	        "formula": result[r]['formula']||"",
        			        	        "field_input_type": result[r]['field_input_type']||"",
        			        	        "readonly": result[r]['readonly']||0,
        			        	        "condition": result[r]['condition']||"",
        			        	        "value": b[result[r]['field_name']]||''
        			        	    }
        			        	}
        			        	else if(result[r]['field_type'] == "radioButton"){
        			        	    console.log(JSON.parse(result[r]['multiple_values'])[0]);
        			        	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
        			        	    var options = $.map(other_attr, function(value, index){
        			        	        return value;
        			        	    });
        			        	    var get_id = result[r]['field_name'];
        			        	    var get_selected = '';
        			        	    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
        			        	    data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        	        "type": result[r]['field_type'],
        			        	        "name": result[r]['field_name'],
        			        	        "options": options,
        			        	        "formula_type": result[r]['formula_type']||"static",
        			        	        "formula": result[r]['formula']||"",
        			        	        "field_input_type": result[r]['field_input_type']||"",
        			        	        "readonly": result[r]['readonly']||0,
        			        	        "value": b[result[r]['field_name']]||[]
        			        	    
        			        	    }
        			        	}
        			        	else if(result[r]['field_type'] == "checkbox"){
        			        	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
        			        	    var options = $.map(other_attr, function(value, index){
        			        	        return value;
        			        	    });
        			        	    var get_id = result[r]['field_name'];
        			        	    var get_selected = '';
        			        	    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
        			        	    data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        	        "type": result[r]['field_type'],
        			        	        "name": result[r]['field_name'],
        			        	        "options": options,
        			        	        "formula_type": result[r]['formula_type']||"static",
        			        	        "formula": result[r]['formula']||"",
        			        	        "field_input_type": result[r]['field_input_type']||"",
        			        	        "readonly": result[r]['readonly']||0,
        			        	        "value": b[result[r]['field_name']]||[]
        			        	    }
        			        	}
        			        	else if(result[r]['field_type'] == "selectMany"){
        			        	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
        			        	    var options = $.map(other_attr, function(value, index){
        			        	        return value;
        			        	    });
        			        	    var get_id = result[r]['field_name'];
        			        	    var get_selected = '';
        			        	    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
        			        	    data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        	        "type": result[r]['field_type'],
        			        	        "name": result[r]['field_name'],
        			        	        "options": options,
        			        	        "formula_type": result[r]['formula_type']||"static",
        			        	        "formula": result[r]['formula']||"",
        			        	        "field_input_type": result[r]['field_input_type']||"",
        			        	        "readonly": result[r]['readonly']||0,
        			        	        "value": b[result[r]['field_name']].split(",")||[]
        			        	    }
        			        	}
        			        	else if(result[r]['field_type'] == "dropdown"){
        			        	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
        			        	    var options = $.map(other_attr, function(value, index){
        			        	        return value;
        			        	    });
        			        	    data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        	        "type": result[r]['field_type'],
        			        	        "name": result[r]['field_name'],
        			        	        "options": options,
        			        	        "formula_type": result[r]['formula_type']||"static",
        			        	        "formula": result[r]['formula']||"",
        			        	        "field_input_type": result[r]['field_input_type']||"",
        			        	        "readonly": result[r]['readonly']||0,
        			        	        "value": b[result[r]['field_name']]||options[0]
        			        	    }
        			        	}
        			        	else if(result[r]['field_type'] == "listNames"){
        			        	    var other_attr = JSON.parse(result[r]['other_attributes']);
        			        	    data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        	        "type": result[r]['field_type'],
        			        	        "name": result[r]['field_name'],
        			        	        "id": other_attr['return-field'],
        			        	        "return_field": other_attr['return-field'],
        			        	        "list_type_selection": other_attr['list-type-selection'],
        			        	        "data_original_title": '',
        			        	        "formula_type": result[r]['formula_type']||"static",
        			        	        "formula": result[r]['formula']||"",
        			        	        "field_input_type": result[r]['field_input_type']||"",
        			        	        "readonly": result[r]['readonly']||0,
        			        	        "value": b[result[r]['field_name']]||''
        			        	    }

        			        	}
        			        	else if(result[r]['field_type'] == "multiple_attachment_on_request"){
        			        		data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        		    "type": result[r]['field_type'],
        			        		    "name": result[r]['field_name'],
        			        		    "obj_id": result[r]['object_id'],
        			        		    "data_original_title": '',
        			        		    "formula_type": result[r]['formula_type']||"static",
        			        		    "formula": result[r]['formula']||"",
        			        		    "field_input_type": result[r]['field_input_type']||"",
        			        		    "readonly": result[r]['readonly']||0,
        			        		    "value": b[result[r]['field_name']]||''
        			        		}
        			        	}
        			        	else if(result[r]['field_type'] == "attachment_on_request"){
        			        		data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        		    "type": result[r]['field_type'],
        			        		    "name": result[r]['field_name'],
        			        		    "obj_id": result[r]['object_id'],
        			        		    "data_original_title": '',
        			        		    "formula_type": result[r]['formula_type']||"static",
        			        		    "formula": result[r]['formula']||"",
        			        		    "field_input_type": result[r]['field_input_type']||"",
        			        		    "readonly": result[r]['readonly']||0,
        			        		    "value": b[result[r]['field_name']]||''
        			        		}
        			        	}
        			        	else{
        			        	    data['embed_row_data'][row_id][new_row_name_count]['fields_data'][result[r]['field_name']] = {
        			        	        "type": result[r]['field_type'],
        			        	        "name": result[r]['field_name'],
        			        	        "formula_type": result[r]['formula_type']||"static",
        			        	        "formula": result[r]['formula']||"",
        			        	        "field_input_type": result[r]['field_input_type']||"",
        			        	        "readonly": result[r]['readonly']||0,
        			        	        "value": b[result[r]['field_name']]||''
        			        	    }
        			        	}
        			        }
        			    }
        			    data['embed_row_data'][row_id][new_row_name_count]['fields_data']['TrackNo'] = {
        			        "type": undefined,
        			        "name": 'TrackNo',
        			        "value": 'pending...'
        			    }
        			    data['embed_row_data'][row_id][new_row_name_count]['fields_data']['DateCreated'] = {
        			        "type": undefined,
        			        "name": 'DateCreated',
        			        "value": ''
        			    }
        			    data['embed_row_data'][row_id][new_row_name_count]['fields_data']['Processor'] = {
        			        "type": undefined,
        			        "name": 'Processor',
        			        "value": $('[name="Processor"]').val()
        			    }
        			    data['embed_row_data'][row_id][new_row_name_count]['fields_data']['Requestor'] = {
        			        "type": undefined,
        			        "name": 'Requestor',
        			        "value": $('[name="Requestor"]').val()
        			    }
        			    data['embed_row_data'][row_id][new_row_name_count]['fields_data']['CreatedBy'] = {
        			        "type": undefined,
        			        "name": 'CreatedBy',
        			        "value": $('[name="CurrentUser"]').val()
        			    }
        			    data['embed_row_data'][row_id][new_row_name_count]['fields_data']['UpdatedBy'] = {
        			        "type": undefined,
        			        "name": 'UpdatedBy',
        			        "value": $('[name="CurrentUser"]').val()
        			    }
        			    data['embed_row_data'][row_id][new_row_name_count]['fields_data']['requestor-name'] = {
        			        "type": undefined,
        			        "name": 'requestor-name',
        			        "value": $('[name="Requestor"]').attr('requestor-name')
        			    }
        			    data['embed_row_data'][row_id][new_row_name_count]['fields_data']['status-embed'] = {
        			        "type": undefined,
        			        "name": 'status-embed',
        			        "value": 'Draft'
        			    }
            		}, function(){
            			embeded_view.tempoEmbeddedListRefresh($(embedded_view), true);
            			ui.unblockPortion($(embedded_view));
            			$(embedded_view).parent().find('.refresh_embed').html("");
            		});
            		
                }
                else{
            		data['embed_row_data'][row_id][row_name_count] = {
            		    "query_type": type,
            		    "origin": 'in-line',
            		    "saved": false,
            		    "form_id": form_id,
            		    "fields_data": {}
            		};
            	    console.log("resultssss", result['other_details']);
            	    for(details in result['other_details']){
            	        if(details == 'buttonStatus'){
            	            var getBtnStatus = JSON.parse(result['other_details'][details]);
            	            data['embed_row_data'][row_id][row_name_count]['fields_data']['Node_ID'] = {
            	                "type": undefined, 
            	                "name": details,
            	                "value": getBtnStatus['child_id']
            	            }
            	        }
            	        else{
            	            data['embed_row_data'][row_id][row_name_count]['fields_data'][details] = {
            	                "type": undefined,
            	                "name": details,
            	                "value": result['other_details'][details]
            	            }
            	        }
            	    }
            	    for(r in result){
            	    	if(!isNaN(r)){
            	        	if(result[r]['field_type'] == "TrackNo"){
            	        	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        	        "type": result[r]['field_type'],
            	        	        "name": result[r]['field_name'],
            	        	        "formula_type": result[r]['formula_type']||"static",
            	        	        "field_input_type": result[r]['field_input_type']||"",
            	        	        "value": 'pending...'
            	        	    }
            	        	}
            	        	else if(result[r]['field_type'] == "pickList"){
            	        	    var other_attr = JSON.parse(result[r]['other_attributes']);
            	        	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        	        "type": result[r]['field_type'],
            	        	        "name": result[r]['field_name'],
            	        	        "id": other_attr['return-field'],
            	        	        "picklist_button_id": other_attr['picklist-button-id'],
            	        	        "picklist_type": other_attr['picklist-type'],
            	        	        "return_field": other_attr['return-field'],
            	        	        "form_id": other_attr['form-id'],
            	        	        "return_field_name": other_attr['return-field-name'],
            	        	        "formname": other_attr['formname'],
            	        	        "display_columns": other_attr['display_columns'],
            	        	        "display_column_sequence": other_attr['display_column_sequence'],
            	        	        "selection_type": other_attr['selection-type'],
            	        	        "enable_add_entry": other_attr['enable_entry'],
            	        	        "allow_values_not_in_list": other_attr['allow-values-not-in-list'],
            	        	        "show_picklist_view": '',
            	        	        "formula_type": result[r]['formula_type']||"static",
            	        	        "formula": result[r]['formula']||"",
            	        	        "field_input_type": result[r]['field_input_type']||"",
            	        	        "readonly": result[r]['readonly']||0,
            	        	        "condition": result[r]['condition']||"",
            	        	        "value": ''
            	        	    }
            	        	}
            	        	else if(result[r]['field_type'] == "radioButton"){
            	        	    console.log(JSON.parse(result[r]['multiple_values'])[0]);
            	        	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
            	        	    var options = $.map(other_attr, function(value, index){
            	        	        return value;
            	        	    });
            	        	    var get_id = result[r]['field_name'];
            	        	    var get_selected = '';
            	        	    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
            	        	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        	        "type": result[r]['field_type'],
            	        	        "name": result[r]['field_name'],
            	        	        "options": options,
            	        	        "formula_type": result[r]['formula_type']||"static",
            	        	        "formula": result[r]['formula']||"",
            	        	        "field_input_type": result[r]['field_input_type']||"",
            	        	        "readonly": result[r]['readonly']||0,
            	        	        "value": []
            	        	    
            	        	    }
            	        	}
            	        	else if(result[r]['field_type'] == "checkbox"){
            	        	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
            	        	    var options = $.map(other_attr, function(value, index){
            	        	        return value;
            	        	    });
            	        	    var get_id = result[r]['field_name'];
            	        	    var get_selected = '';
            	        	    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
            	        	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        	        "type": result[r]['field_type'],
            	        	        "name": result[r]['field_name'],
            	        	        "options": options,
            	        	        "formula_type": result[r]['formula_type']||"static",
            	        	        "formula": result[r]['formula']||"",
            	        	        "field_input_type": result[r]['field_input_type']||"",
            	        	        "readonly": result[r]['readonly']||0,
            	        	        "value": []
            	        	    }
            	        	}
            	        	else if(result[r]['field_type'] == "selectMany"){
            	        	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
            	        	    var options = $.map(other_attr, function(value, index){
            	        	        return value;
            	        	    });
            	        	    var get_id = result[r]['field_name'];
            	        	    var get_selected = '';
            	        	    // console.log("options", options, "getid", get_id, "get_selected", $('iframe').contents().find('[name="' + get_id + '"]').filter(':checked').val());
            	        	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        	        "type": result[r]['field_type'],
            	        	        "name": result[r]['field_name'],
            	        	        "options": options,
            	        	        "formula_type": result[r]['formula_type']||"static",
            	        	        "formula": result[r]['formula']||"",
            	        	        "field_input_type": result[r]['field_input_type']||"",
            	        	        "readonly": result[r]['readonly']||0,
            	        	        "value": []
            	        	    }
            	        	}
            	        	else if(result[r]['field_type'] == "dropdown"){
            	        	    var other_attr = JSON.parse(result[r]['multiple_values'])[0];
            	        	    var options = $.map(other_attr, function(value, index){
            	        	        return value;
            	        	    });
            	        	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        	        "type": result[r]['field_type'],
            	        	        "name": result[r]['field_name'],
            	        	        "options": options,
            	        	        "formula_type": result[r]['formula_type']||"static",
            	        	        "formula": result[r]['formula']||"",
            	        	        "field_input_type": result[r]['field_input_type']||"",
            	        	        "readonly": result[r]['readonly']||0,
            	        	        "value": options[0]
            	        	    }
            	        	}
            	        	else if(result[r]['field_type'] == "listNames"){
            	        	    var other_attr = JSON.parse(result[r]['other_attributes']);
            	        	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        	        "type": result[r]['field_type'],
            	        	        "name": result[r]['field_name'],
            	        	        "id": other_attr['return-field'],
            	        	        "return_field": other_attr['return-field'],
            	        	        "list_type_selection": other_attr['list-type-selection'],
            	        	        "data_original_title": '',
            	        	        "formula_type": result[r]['formula_type']||"static",
            	        	        "formula": result[r]['formula']||"",
            	        	        "field_input_type": result[r]['field_input_type']||"",
            	        	        "readonly": result[r]['readonly']||0,
            	        	        "value": ''
            	        	    }

            	        	}
            	        	else if(result[r]['field_type'] == "multiple_attachment_on_request"){
            	        		data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        		    "type": result[r]['field_type'],
            	        		    "name": result[r]['field_name'],
            	        		    "obj_id": result[r]['object_id'],
            	        		    "data_original_title": '',
            	        		    "formula_type": result[r]['formula_type']||"static",
            	        		    "formula": result[r]['formula']||"",
            	        		    "field_input_type": result[r]['field_input_type']||"",
            	        		    "readonly": result[r]['readonly']||0,
            	        		    "value": ''
            	        		}
            	        	}
            	        	else if(result[r]['field_type'] == "attachment_on_request"){
            	        		data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        		    "type": result[r]['field_type'],
            	        		    "name": result[r]['field_name'],
            	        		    "obj_id": result[r]['object_id'],
            	        		    "data_original_title": '',
            	        		    "formula_type": result[r]['formula_type']||"static",
            	        		    "formula": result[r]['formula']||"",
            	        		    "field_input_type": result[r]['field_input_type']||"",
            	        		    "readonly": result[r]['readonly']||0,
            	        		    "value": ''
            	        		}
            	        	}
            	        	else{
            	        	    data['embed_row_data'][row_id][row_name_count]['fields_data'][result[r]['field_name']] = {
            	        	        "type": result[r]['field_type'],
            	        	        "name": result[r]['field_name'],
            	        	        "formula_type": result[r]['formula_type']||"static",
            	        	        "formula": result[r]['formula']||"",
            	        	        "field_input_type": result[r]['field_input_type']||"",
            	        	        "readonly": result[r]['readonly']||0,
            	        	        "value": ''
            	        	    }
            	        	}
            	        }
            	    }
            	    data['embed_row_data'][row_id][row_name_count]['fields_data']['TrackNo'] = {
            	        "type": undefined,
            	        "name": 'TrackNo',
            	        "value": 'pending...'
            	    }
            	    data['embed_row_data'][row_id][row_name_count]['fields_data']['DateCreated'] = {
            	        "type": undefined,
            	        "name": 'DateCreated',
            	        "value": ''
            	    }
            	    data['embed_row_data'][row_id][row_name_count]['fields_data']['Processor'] = {
            	        "type": undefined,
            	        "name": 'Processor',
            	        "value": $('[name="Processor"]').val()
            	    }
            	    data['embed_row_data'][row_id][row_name_count]['fields_data']['Requestor'] = {
            	        "type": undefined,
            	        "name": 'Requestor',
            	        "value": $('[name="Requestor"]').val()
            	    }
            	    data['embed_row_data'][row_id][row_name_count]['fields_data']['CreatedBy'] = {
            	        "type": undefined,
            	        "name": 'CreatedBy',
            	        "value": $('[name="CurrentUser"]').val()
            	    }
            	    data['embed_row_data'][row_id][row_name_count]['fields_data']['UpdatedBy'] = {
            	        "type": undefined,
            	        "name": 'UpdatedBy',
            	        "value": $('[name="CurrentUser"]').val()
            	    }
            	    data['embed_row_data'][row_id][row_name_count]['fields_data']['requestor-name'] = {
            	        "type": undefined,
            	        "name": 'requestor-name',
            	        "value": $('[name="Requestor"]').attr('requestor-name')
            	    }
            	    data['embed_row_data'][row_id][row_name_count]['fields_data']['status-embed'] = {
            	        "type": undefined,
            	        "name": 'status-embed',
            	        "value": 'Draft'
            	    }
                	embeded_view.addToEmbedList(embedded_view, data['embed_row_data'][row_id][row_name_count]['fields_data'], row_name_count, type, true);
                	ui.unblockPortion($(embedded_view));
                }
                // EmbeddedUI.unblock(embeded_view);
            }
            catch(e){
                console.error("error", "something is wrong...", e);
            }
        });
    },
    "embedAdditionalDisplayFixes": function(embed){
    	//multiple attachment fixing
    	var multiple_attachment_td = $(embed).find('.embed_multiple_file_attachement').closest('td:not(.picklist-td)');
    	console.log("teedee's", multiple_attachment_td);
    	multiple_attachment_td.each(function(){
    		var name = $(this).find('input[type="text"]').attr('name');
    		var json_value = $.parseJSON($(this).find('input[type="text"]').val());
    		if($.type(json_value) == 'string'){
    			json_value = $.parseJSON(json_value);
    		}
    		$("body").data(name, json_value);
    	})
    },
    "embedHideDeletedRecords": function(embed_object_id){
    	console.log("embed_container_object_id", embed_object_id);
    	if($.type($('body').data('embed_deleted_row')) != "undefined"){
	    	var embed_deleted_data = $('body').data('embed_deleted_row');
	    	var embedded_view = $('.getFields_' + embed_object_id);
	    	var embed_deleted_row = embed_deleted_data[embed_object_id];
	    	for(i in embed_deleted_row['id_list']){
	    		embedded_view.find('[record-id="' + embed_deleted_row['id_list'][i] + '"]').remove();
	    	}
	    	embeded_view.fixNumbering(embedded_view);
	    }
    },
    "embedSetupComputedSearch": function(embed){//find me...
    	var embedded = $(embed);
    	var self = this;
    	embedded.on('embedEventV2.embedSearchComputed', function(){
    		// var self = $(this);
    		var formId = $(this).attr('embed-source-form-val-id');
            var fieldReference = $(this).attr('embed-result-field-val');
            var fieldValue;
            var fieldReferenceObjectName = $(this).attr('search-field-object-name');
            if(fieldReferenceObjectName == 'pickList' || fieldReferenceObjectName == 'checkbox' || fieldReferenceObjectName == 'selectMany'){
            	if(fieldReferenceObjectName == 'pickList'){
            		fieldValue = $('[name="' + fieldReference + '"]').val().split('|');
            	}
            	else if(fieldReferenceObjectName == 'checkbox'){
            		fieldValue = $('[name="' + fieldReference + '[]"]:checked').map(function(a, b){ return $.trim($(this).val()); });
            	}
            	else if(fieldReferenceObjectName == 'selectMany'){
            		fieldValue = $('[name="' + fieldReference + '[]"]>option:selected').map(function(){ return $.trim($(this).val()); });
            	}
            }
            else{
            	fieldValue = $('[name="' + fieldReference + '"]').val();
            }
            
            var fieldFilter = $(this).attr('embed-source-lookup-active-field-val');
            var fieldFilterConditionalOperator = $(this).attr('embed-conditional-operator');
            
            if ($.type(fieldFilterConditionalOperator) == "undefined") {
                fieldFilterConditionalOperator = "=";
            } else if (fieldFilterConditionalOperator == "") {
                fieldFilterConditionalOperator = "=";
            }
            var embedAdditionalFilterFormula = $(this).attr('embed-additional-filter-formula');
            if ($.type(embedAdditionalFilterFormula) == "undefined") {
                embedAdditionalFilterFormula = "";
            }
            var fieldReferenceType = $(this).attr('rfc-choice');
            var enable_embed_row_click = $(this).attr("enable-embed-row-click");
            var embed_action_click_copy = $(this).attr('embed-action-click-copy');
            var embed_action_click_delete = $(this).attr('embed-action-click-delete');
            var embed_action_click_edit = $(this).attr('embed-action-click-edit');
            var embed_action_click_view = $(this).attr('embed-action-click-view');
            var embed_action_click_number = $(this).attr('embed-action-click-number');
            var embed_action_click_edit_popup = $(this).attr('embed-action-click-edit-popup');
            if (fieldReferenceType == 'computed') {
                var thisFormula = $(this).attr('embed-computed-formula');
                var formulaDoc = new Formula(thisFormula);
                fieldValue = formulaDoc.getEvaluation();
            }

            var container = this;
            if (fieldValue !== null && fieldValue !== 'null') {
                var reference = {
                    FormID: formId,
                    FieldReference: fieldReference,
                    FieldValue: fieldValue,
                    FieldFilter: fieldFilter,
                    "field_conditional_operator": fieldFilterConditionalOperator,
                    "embed_additional_filter_formula": self.parseCondition(embedAdditionalFilterFormula),
                    HLData: "",
                    HLType: "row",
                    HLAllow: "false",
                    column_data: "",
                    current_form_fields_data: [],
                    "enable_embed_row_click": enable_embed_row_click,
                    "embed_action_click_copy": embed_action_click_copy,
                    "embed_action_click_delete": embed_action_click_delete,
                    "embed_action_click_edit": embed_action_click_edit,
                    "embed_action_click_view": embed_action_click_view,
                    "embed_action_click_number": embed_action_click_number,
                    "embed_action_click_edit_popup": embed_action_click_edit_popup,
                    "embed_default_sorting": {"column": "ID", "type": "ASC"}

                };
                if ($(container).is('[data-default-sort-col]')) {
                    if ($(container).attr('data-default-sort-col') != '--Selected--') {
                        var sort_type = $(container).attr('data-default-sort-type');
                        var sort_column = $(container).attr('data-default-sort-col');
                        reference['embed_default_sorting']['column'] = sort_column;
                        reference['embed_default_sorting']['type'] = sort_type;
                    }
                }
                if (typeof $(container).attr('embed-hl-data') != "undefined") {
                    reference["HLData"] = $(container).attr('embed-hl-data');
                }
                if (typeof $(container).attr('allow-highlights') != "undefined") {
                    reference["HLAllow"] = $(container).attr('allow-highlights');
                }
                if (typeof $(container).attr('allow-highlights') != "undefined") {
                    reference["HLType"] = $(container).attr('highlight-type');
                }
                if (typeof $(container).attr('embed-column-data') != "undefined") {
                    reference["column_data"] = $(container).attr('embed-column-data');
                }
                if($(this).attr('commit-data-row-event') == "parent"){
                	$(this).addClass('embed-edit-mode');
                }
                reference['search_field_obj_name'] = fieldReferenceObjectName;
                $('#frmrequest').find(".setOBJ").each(function (eqi) {
                    var data_object_id = $(this).attr("data-object-id");
                    var getFIELDS = $(this).find(".getFields_" + data_object_id);
                    var field_name = $(this).find(".getFields_" + data_object_id).eq(0).attr("name");
                    field_name_values = [];
                    getFIELDS.each(function (eqi2) {
                        field_name_values.push($(this).val());
                    })
                    field_name_values = field_name_values.join("|^|");
                    reference["current_form_fields_data"].push({
                        "f_name": field_name,
                        "values": field_name_values
                    });
                });
                reference["current_form_fields_data"] = JSON.stringify(reference["current_form_fields_data"]);
                $(container).html('<center>' + '<div class="spinner load-app-request" style="margin-top:30px;">' +
                        '<div class="bar1"></div>' +
                        '<div class="bar2"></div>' +
                        '<div class="bar3"></div>' +
                        '<div class="bar4"></div>' +
                        '<div class="bar5"></div>' +
                        '<div class="bar6"></div>' +
                        '<div class="bar7"></div>' +
                        '<div class="bar8"></div>' +
                        '<div class="bar9"></div>' +
                        '<div class="bar10"></div>' +
                        '</div>' + '</center>');


                $(container).data("embed_reference", reference);

                self.getRequests(reference, function (data) {
                    var obj_jq = $(data);
                    var parent_container_embed = $(container)
                    parent_container_embed.html(obj_jq);
                    var rows_element = obj_jq.find("tr").filter(":not(:eq(0))").find(".viewTDEmbedded");

                    obj_jq.find("tr").filter(":not(:eq(0))").addClass("hovereffect");

                    if (obj_jq.attr("author-valid")) {

                        if (obj_jq.attr("author-valid") == "true") {

                        } else {
                            parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
                        }
                    } else {
                        parent_container_embed.parents(".setOBJ").eq(0).find(".embed_newRequest").remove();
                    }
                    //console.log("rows_element",rows_element)
                    self.getSetTotalOfColumn.call(parent_container_embed, obj_jq);
                    //self.embedBindHeaderSorting(obj_jq);
                    rows_element.on({
                        "click": function () {

                            var dis_ele = $(this);
                            var embed_view_cont = dis_ele.parent().find(".embed-view-container").eq(0);
                            $('html').data("embed_container", embed_view_cont);
                            var embeded_attr = $(this).attr("data-action-attr");
                            var reference_id = dis_ele.parents('tr').attr('reference-id')||dis_ele.parents('.embed-view-container').attr('embed-source-form-val-id') + '_' + dis_ele.parents('tr').attr('record-id');
                            var allow_row_click = $(container).attr("enable-embed-row-click")
                            if (allow_row_click) {
                                if (allow_row_click != "true") {
                                    return;
                                }
                            } else {
                                return;
                            }
                            var dis_row_click_ele = $(this).parents("tr").eq(0);
                            var row_click_data = {
                                "record_trackno": dis_row_click_ele.attr("record-trackno"),
                                "record_id": dis_row_click_ele.attr("record-id")
                            }


                            var ret =
                                    '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
                                    '<div class="hr"></div>';
                            var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "", function () {
                            });
                            dis_dialog.themeDialog("modal2");
                            var ele_dialog = $('#popup_container');
                            //console.log('/user_view/workspace?view_type=request&formID='+formId+'&requestID='+row_click_data["record_id"]+'&trackNo='+row_click_data["record_trackno"]);
                            if (embeded_attr == "view") {
                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
                            } else {
                                var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
                            }
                            var my_dialog = $(
                                    '<div class="embed-iframe" style="height: 100%;">' +
                                    '<iframe style="height:100%;width:100%;" src="' + src + '">' +
                                    '</iframe>' + '</div>'
                                    )

                            ele_dialog.find("#popup_content").html(my_dialog);

                            ele_dialog.find("#popup_content").css({
                                "height": ($(window).outerHeight() - 120) + "px"
                            });
                            ele_dialog.css({
                                "top": "30px"
                            });
                            ele_dialog.data("field_updater", fieldReference);
                            ele_dialog.data("embed_container", parent_container_embed);
                            var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
                            var loading_panel = panel.load(ele_dialog.find("#popup_content"));
                            loading_panel.parent().css("position", "relative");
                            iframe_ele_dialog.load(function () {
                                var iframe_document = $(this)[0].contentWindow.$('html');
                                EmbedPopUpDisplayFixes.call(iframe_document);
                                loading_panel.fadeOut();
                                iframe_document.addClass('view-embed-iframe');
                                if(embeded_attr == "view"){
                                    iframe_document.find('[data-type="embeded-view"]').css({"z-index":"9999"});
                                }
                            })
                            // if(get_request_type.indexOf('pending') > -1){
                                // embeded_view.createTemporaryData($(parent_container_embed), "insert", "popup");
                            // }
                            // else{
                                embeded_view.createTemporaryData($(parent_container_embed), "update", "popup", reference_id);
                            // }
                            // embeded_view.getRequests(parent_container_embed.data("embed_view_reference"), function(data) {
                            //     $(container).html(data);
                            // });

                            // var iframe_ele = my_dialog.find("iframe").eq(0);
                            // iframe_ele.load(function(){
                            //     var iframe_doc = $(iframe_ele).contents()[0];
                            //     console.log("FRAAA",$(iframe_doc))
                            // })

                            // Roni Pinili close button modal configuration

                            if ($('body').data("user_form_json_data")) {

                                var embeddedViewObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

                                if ($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]) {
                                    var embededViewActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
                                    console.log("VALUE", embededViewActionVisibilityVal);
                                    if (embededViewActionVisibilityVal == "Yes") {
                                        var close_dial = setInterval(function () {
                                            $('.fl-closeDialog').hide();
                                            if ($('.fl-closeDialog').length >= 1) {
                                                clearInterval(close_dial);
                                            }
                                        }, 0);
                                    }
                                }

                            }

                        }
                        
                    })
                    obj_jq.find(".columnHeader").on("click", function () {//carlo
                    	if($(this).closest('.embed-view-container').is('.embed-edit-mode')){
                    		return;
                    	}
                    	if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
                    		return;
                    	}
                    	self.embedrowsSorter.call(this, self);
                    });
                    console.log("HARRTEST")
                    initializePage.exePrintDisplayFixes(parent_container_embed);
                    initializePage.visibilityOnChange2(parent_container_embed);
                    //trigger default sorting
                    // if($(parent_container_embed).is('[data-default-sort-col]')){
                    //     if($(parent_container_embed).attr('data-default-sort-col') != '--Selected--'){
                    //        var sort_type = $(parent_container_embed).attr('data-default-sort-type');
                    //        var sort_column = $(parent_container_embed).attr('data-default-sort-col');
                    //        obj_jq.find(".columnHeader[data-fld-name='"+sort_column+"']").trigger("click");
                    //     }
                    // }
                    if(parent_container_embed.attr('commit-data-row-event') == 'parent'){
                    	embeded_view.tempoEmbeddedListRefresh(parent_container_embed);
                    }
                    embeded_view.embedAdditionalDisplayFixes($(parent_container_embed));
                });
            }
    	});
    },
    "RESERVED_WORDS": ['Requestor','Status','Processor','LastAction','CreatedBy','UpdatedBy','Unread','Node_ID','Workflow_ID','fieldEnabled','fieldRequired','fieldHiddenValues','imported','Repeater_Data','Editor','Viewer','middleware_process','ProcessorType','ProcessorLevel','SaveFormula','CancelFormula']
};

/** 
added by japhet morada 
prototype function for fix header in embedded view
03-14-2016
**/
var EmbeddedFixHeader = {
	"embedded_view": $(),
	"init": function(embedded_view){
		var self = this;
		self.embedded_view = embedded_view;
		self.applyFixHeader(embedded_view);
	},
	"applyFixHeader": function(embedded_view){
		var self = this;
		var clone = $(embedded_view).find('thead').clone();
		clone.addClass('cloned');
		console.log("theads", $(embedded_view).find('thead'));
		$(embedded_view).find('thead').after(clone);
		console.log("cloned theads", $(embedded_view)[0]);
		$(embedded_view).find('thead.cloned').css({'position': 'absolute', 'top': '0px'});
		$(embedded_view).find('thead.cloned th').css('border-radius', '0px');
		$(embedded_view).css('position', 'relative');
		self.maintainCellWidth(embedded_view);
		self.scrollEvent(embedded_view);
		self.applyHeaderEvents(embedded_view);
	},
	"scrollEvent": function(embedded_view){
		var self = this;
		$(embedded_view).on('scroll', function(e,data){
			var embed = $(this);
			if(data == "trigger"){
				$(embedded_view).find('thead.cloned').css({'top':'0px'});
			}else{
				// console.log("embed top",$(embedded_view).position()['top'], "scroll top", $(embedded_view).scrollTop(),"total", ($(embedded_view).position['top'] + $(embedded_view).scrollTop()));
				$(embedded_view).find('thead.cloned').css({'top':(Number($(embedded_view).scrollTop()))});
			}
			
		});
	},
	"DOMEvents": function(){
		var self = this;
		$(self.embedded_view).bind("DOMSubtreeModified",function(e, data){
			if($.type($(self.embedded_view).attr(''))){

			}
			self.maintainCellWidth(embedded_view);
		});
	},
	"maintainCellWidth": function(embedded_view){
		var self = this;
		var thead = $(embedded_view).find('thead.cloned');
		var row = $(embedded_view).find('thead:not(.cloned)').find('tr');

		if($.type(row) != "undefined"){
			// console.log("row found...", $(row));
			// console.log("cell found...", $(row).find('td'));
			thead.find('th').each(function(index){
				var get_width = $(row).find('th').eq(index).find('div').outerWidth();
				$(this).find('div').css('width', get_width + 'px');
			});
		}
	},
	"applyHeaderEvents": function(embedded_view){
		var self = this;
		var thead = $(embedded_view).find('thead.cloned');
		thead.find('div').each(function(index){
			$(this).on('click', function(e){
				console.log($(this));
				$(embedded_view).find('thead:not(.cloned)').find('th div').eq(index).trigger('click');
			});
		});
	},
	"reFixHeader": function(embedded_view){
		console.log("refix the header");
		var self = $(embedded_view);
		var thead;
		var row = $(self).find('thead:not(.cloned)').find('tr');
		if(typeof self.attr('source-form-type') != "undefined" && self.attr('source-form-type') == "multiple"){
			var body_data = $('body').data('user_form_json_data')['form_json'][''+ self.closest('.setOBJ').attr('data-object-id')]||[];
			if($(self).find('thead.cloned').length > 0){
				thead = $(self).find('thead.cloned');
				if($.type(row) != "undefined"){
					thead.find('th').each(function(index){
						if(body_data['allow_row_selection'] == true){
							if(index == 0){
								var get_width = $(row).find('th').eq(index).width();
								$(this).find('div').css('width', get_width + 'px');
							}
							else{
								var get_width = $(row).find('th').eq(index).find('div').outerWidth();
								$(this).find('div').css('width', get_width + 'px');
							}
						}
						else{
							var get_width = $(row).find('th').eq(index).find('div').outerWidth();
							$(this).find('div').css('width', get_width + 'px');
						}
					});
				}
				thead.css({'top': $(self).scrollTop()});	
			}
		}
		else{
			if($(self).find('thead.cloned').length > 0){
				thead = $(self).find('thead.cloned');
				if($.type(row) != "undefined"){
					thead.find('th').each(function(index){
						var get_width = 0;
						if($(row).find('.record_select_all').length > 0){
							get_width = $(row).find('th').eq(index).find('div').parent().width();
						}
						else{
							get_width = $(row).find('th').eq(index).find('div').outerWidth();
						}
						$(this).find('div').css('width', get_width + 'px');
					});
				}
				thead.css({'top': $(self).scrollTop()});	
			}
		}
		// else{
		// 	EmbeddedFixHeader.init(embedded_view);
		// 	EmbeddedFixHeader.maintainCellWidth();
		// }

	}
}
function initializeMultipleEmbedddedView(embedded_view, load_type){
	var do_id = $(embedded_view).parents('.setOBJ[data-type="embeded-view"][data-object-id]:eq(0)').attr('data-object-id');
	var element_data = $('body').data('user_form_json_data')['form_json']['' + do_id];
	var form_id_list = element_data.multiple_forms;
	var column_list = element_data.column_settings;
	var embedded_view_object = $(embedded_view);
	var field_values = {};
	
	for(i in form_id_list){
		var ref_field = $('[name="' + form_id_list[i]['search_field'] + '"]');
		var ref_field_val = "";
		if(ref_field.length > 0){
			if(ref_field.closest('.setOBJ').attr('data-type') == "radioButton"){
				ref_field_val = $('[name="' + form_id_list[i]['search_field'] + '"]:checked').val();
				field_values[form_id_list[i]['lookup_field']] = ref_field_val||"";
			}
			else if(ref_field.closest('.setOBJ').attr('data-type') == "checkbox"){
				ref_field_val = $('[name="' + form_id_list[i]['search_field'] + '[]"]:checked').map(function(a, b){ return $.trim($(this).val()); });
				field_values[form_id_list[i]['lookup_field']] = ref_field_val||[];
			}
			else if(ref_field.closest('.setOBJ').attr('data-type') == "selectMany"){
				ref_field_val = $('[name="' + form_id_list[i]['search_field'] + '[]"]>option:selected').map(function(){ return $.trim($(this).val()); });
				field_values[form_id_list[i]['lookup_field']] = ref_field_val||[];
			}
			else{
				ref_field_val =ref_field.val();
				field_values[form_id_list[i]['lookup_field']] = ref_field_val||"";
			}
		}
		field_values[form_id_list[i]['lookup_field']] = ref_field.val()||"";
		form_id_list[i]['advance_filter_formula'] = embeded_view.parseCondition(form_id_list[i]['advance_filter_formula']);
	}
	//setting up the column_list
	var column_option = embedded_view_object.data('column_sort_option')||{"column": "ID", "type": "ASC"};
	var embed_row_catgory = "";
	if($('body').data('user_form_json_data')['form_json'][""+do_id]['allow_row_category'] == true){
		embed_row_catgory = $('body').data('user_form_json_data')['form_json'][""+do_id]['allow_row_category_column']||"";
	}
	var postdata = {
		"formsourcetype": embedded_view_object.attr('source-form-type'),
		"form_id_list": form_id_list,
		"column_reference_fields": field_values,
		"column_settings": column_list,
		"enable_embed_row_click": embedded_view_object.attr("enable-embed-row-click"),
		"embed_action_click_copy": embedded_view_object.attr('embed-action-click-copy'),
		"embed_action_click_delete": embedded_view_object.attr('embed-action-click-delete'),
		"embed_action_click_edit": embedded_view_object.attr('embed-action-click-edit'),
		"embed_action_click_view": embedded_view_object.attr('embed-action-click-view'),
		"embed_action_click_number": embedded_view_object.attr('embed-action-click-number'),
		"embed_action_click_edit_popup": embedded_view_object.attr('embed-action-click-edit-popup'),
		"embed_default_sorting": column_option,
		"embed_row_category": embed_row_catgory

	}
	if(typeof load_type == "undefined" || load_type == "onload"){
		if(embedded_view_object.attr('embed-action-custom-action') == "true"){
			loadCustomActions(embedded_view_object, do_id);
		}
	}
	var embed_contianer = $('.setOBJ[data-object-id="'+do_id+'"]');
	if( embed_contianer.is(':not(:visible)') ){
		if(embed_contianer.is(':data("visible_refresh")')){
			embed_contianer.removeData("visible_refresh");
		}
		embed_contianer.data("visible_refresh",function(){
			setTimeout(function(){
				if(embed_contianer.is(':visible')){
					embed_contianer.removeData("visible_refresh");
					$.post("/ajax/embedded_view", postdata, function(echo_result){
						var rows_element;
						ui.unblockPortion(embedded_view_object);
						embedded_view_object.html(echo_result);
						applyMultipleSelection(embedded_view_object, do_id);
						rows_element = embedded_view_object.find("tr").filter(":not(:eq(0))").find(".viewTDEmbedded");
						embedded_view_object.find("tr").filter(":not(:eq(0))").addClass("hovereffect");
						// embedded_view_object.find("[is-editable='0']").css("background-color", "rgba(220,220,220,1)");
						//applying checkboxes
						applyColumnSortingEvents(embedded_view_object, embedded_view_object.find('thead > tr > th > .columnHeader').get());
						if(embed_row_catgory != ""){
							categorizeRow(embedded_view_object);
						}

						EmbeddedFixHeader.init(embedded_view_object);
						EmbeddedFixHeader.reFixHeader(embedded_view_object);
						rebindViewEvents(rows_element, embedded_view_object, element_data['multiple_forms']);
						embedded_view_object.parent().find('.custom_action_cancel_edit').trigger('click');
						
					});
				}else{
					embed_contianer.data("visible_refresh")();
				}
			},500);
		});
		embed_contianer.data("visible_refresh")();
	}
	else{
		$.post("/ajax/embedded_view", postdata, function(echo_result){
			var rows_element;
			ui.unblockPortion(embedded_view_object);
			embedded_view_object.html(echo_result);
			applyMultipleSelection(embedded_view_object, do_id);
			rows_element = embedded_view_object.find("tr").filter(":not(:eq(0))").find(".viewTDEmbedded");
			embedded_view_object.find("tr").filter(":not(:eq(0))").addClass("hovereffect");
			// embedded_view_object.find("[is-editable='0']").css("background-color", "rgba(220,220,220,1)");
			//applying checkboxes
			applyColumnSortingEvents(embedded_view_object, embedded_view_object.find('thead > tr > th > .columnHeader').get());
			if(embed_row_catgory != ""){
				categorizeRow(embedded_view_object);
			}
			embedded_view_object.removeData('selected_rows');
			EmbeddedFixHeader.init(embedded_view_object);
			EmbeddedFixHeader.reFixHeader(embedded_view_object);
			rebindViewEvents(rows_element, embedded_view_object, element_data['multiple_forms']);
			embedded_view_object.parent().find('.custom_action_cancel_edit').trigger('click');
			
		});
	}
}

function rebindViewEvents(rows_element, embedded_view_object, element_data){
	rows_element.each(function(){ //FS#7646
		var dis_row_click_ele = $(this).parents("tr").eq(0);
		var row_click_data = {
		    "record_trackno": dis_row_click_ele.attr("record-trackno"),
		    "record_id": dis_row_click_ele.attr("record-id"),
		    "formId": dis_row_click_ele.attr('form-id')
		}
		if ($(this).attr("data-action-attr") == "view") {
		    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
		} else {
		    var src = '/user_view/workspace?view_type=request&formID=' + row_click_data['formId'] + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
		}
		$(this).attr("data-embed-href-link",src); //FS#7646
	});
	rows_element.on({
	    "click": function () {
	    	var parent_container_embed = $(embedded_view_object);
	        var dis_ele = $(this);
	        var embed_view_cont = dis_ele.parents(".embed-view-container").eq(0);
	        $('html').data("embed_container", embed_view_cont);
	        var embeded_attr = $(this).attr("data-action-attr");
	        var reference_id = dis_ele.parents('tr').attr('reference-id')||dis_ele.parents('.embed-view-container').attr('embed-source-form-val-id') + '_' + dis_ele.parents('tr').attr('record-id');
	        var allow_row_click = parent_container_embed.attr("enable-embed-row-click")
	        console.log(parent_container_embed);
	        if(dis_ele.closest('tr').is('[is-locked]')){
	        	return;
	        }

	        if (allow_row_click) {
	            if (allow_row_click != "true") {
	                return;
	            }
	        } else {
	            return;
	        }
	        var dis_row_click_ele = $(this).parents("tr").eq(0);
	        var row_click_data = {
	            "record_trackno": dis_row_click_ele.attr("record-trackno"),
	            "record_id": dis_row_click_ele.attr("record-id")
	        }
	        var embed_dialog_type = embed_view_cont.attr('embed-dialog-type');
	        var formId = dis_ele.closest('tr').attr('form-id');
	        var ret =
	                '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
	                '<div class="hr"></div>';
	        if (embeded_attr == "view") {
	            var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"] + '&embed_type=viewEmbedOnly';
	        } else {
	            var src = '/user_view/workspace?view_type=request&formID=' + formId + '&requestID=' + row_click_data["record_id"] + '&trackNo=' + row_click_data["record_trackno"];
	        }
	        if(embed_dialog_type == "2"){
	        	var existingWindow = false; //FS#7646
            	if(embed_view_cont.is(":data('embedDialog')")){ //FS#7646
            	    if( embed_view_cont.data("embedDialog").closed == false ){
            	        existingWindow = true;
            	        embed_view_cont.data("embedDialog").focus();
            	        // embed_view_cont.data("embedDialog").location.href = str_link;
            	        if( (embed_view_cont.data("embedDialog").location.pathname+embed_view_cont.data("embedDialog").location.search) != src){
        	        		embed_view_cont.data("embedDialog").location.href = src;
            	        }
            	        if(embed_view_cont.data("embedDialog").$('html').is('.view-embed-iframe') == false){ //this means the dialog window has been reloaded
            	        	embed_view_cont.data("embedDialog").$('html').addClass('view-embed-iframe');
            	        	embed_view_cont.data("embedDialog").onload = function ( ) {
				        	    var iframe_document = embed_view_cont.data("embedDialog").$('html');
				        	    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
				        	    embedDialog.onbeforeunload = function (e) {
				        	        if(iframe_document.find('.getFields[name]').filter(function(){ return embedDialog.$(this)[0].value != embedDialog.$(this)[0].defaultValue; }).length >= 1 ) return "You have changes made.";
				        	    };
				        	};
            	        }
            	    }
            	}
            	if(existingWindow == false){ //FS#7646
            		var embedDialog = window.open(src,'_blank','fullscreen=no');
		        	embed_view_cont.data("embedDialog",embedDialog);
		        	embedDialog.setTimeout(function(){
		        	    embedDialog.addingViewEmbedIframe = setInterval(function(){
		        	        embedDialog.document.getElementsByTagName('html')[0].className += " view-embed-iframe";
		        	    },0);
		        	},0);
		        	embedDialog.fromParent = embedDialog;
		        	embedDialog.onload = function ( ) {
		        	    clearInterval(embedDialog.addingViewEmbedIframe);
		        	    var iframe_document = embedDialog.$('html');
		        	    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
		        	    embedDialog.onbeforeunload = function (e) {
		        	        if(iframe_document.find('.getFields[name]').filter(function(){ return embedDialog.$(this)[0].value != embedDialog.$(this)[0].defaultValue; }).length >= 1 ) return "You have changes made.";
		        	    };
		        	};
            	}
	        }else{
	    	    var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "", function () {
	    	    });
	    	    dis_dialog.themeDialog("modal2");
	    	    var ele_dialog = $('#popup_container');
	    	    //console.log('/user_view/workspace?view_type=request&formID='+formId+'&requestID='+row_click_data["record_id"]+'&trackNo='+row_click_data["record_trackno"]);
	    	    
	    	    var my_dialog = $(
	    	            '<div class="embed-iframe" style="height: 100%;">' +
	    	            '<iframe style="height:100%;width:100%;" src="' + src + '">' +
	    	            '</iframe>' + '</div>'
	    	            )

	    	    ele_dialog.find("#popup_content").html(my_dialog);

	    	    ele_dialog.find("#popup_content").css({
	    	        "height": ($(window).outerHeight() - 120) + "px"
	    	    });
	    	    ele_dialog.css({
	    	        "top": "30px"
	    	    });
	    	    var fieldReference = $('[nam="' + element_data.filter(function(val){
	    	    	return val['source_form'] == formId;
	    	    })['search_field'] + '"]');
	    	    ele_dialog.data("field_updater", fieldReference);
	    	    ele_dialog.data("embed_container", parent_container_embed);
	    	    var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
	    	    var loading_panel = panel.load(ele_dialog.find("#popup_content"));
	    	    loading_panel.parent().css("position", "relative");
	    	    iframe_ele_dialog.load(function () {
	    	        var iframe_document = $(this)[0].contentWindow.$('html');
	    	        EmbedPopUpDisplayFixes.call(iframe_document);
	    	        loading_panel.fadeOut();
	    	        iframe_document.addClass('view-embed-iframe');
	    	        if(embeded_attr == "view"){
	    	            iframe_document.find('[data-type="embeded-view"]').css({"z-index":"9999"});
	    	        }
	    	    })
	    	    // if(get_request_type.indexOf('pending') > -1){
	    	        // embeded_view.createTemporaryData($(parent_container_embed), "insert", "popup");
	    	    // }
	    	    // else{
	    		if($(this).closest('.embed-view-container').attr('commit-data-row-event') == "parent"){
	    			embeded_view.createTemporaryData($(parent_container_embed), "update", "popup", reference_id);
	    		}
	    	    // }
	    	    // embeded_view.getRequests(parent_container_embed.data("embed_view_reference"), function(data) {
	    	    //     $(container).html(data);
	    	    // });

	    	    // var iframe_ele = my_dialog.find("iframe").eq(0);
	    	    // iframe_ele.load(function(){
	    	    //     var iframe_doc = $(iframe_ele).contents()[0];
	    	    //     console.log("FRAAA",$(iframe_doc))
	    	    // })

	    	    // Roni Pinili close button modal configuration

	    	    if ($('body').data("user_form_json_data")) {

	    	        var embeddedViewObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

	    	        if ($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]) {
	    	            var embededViewActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
	    	            console.log("VALUE", embededViewActionVisibilityVal);
	    	            if (embededViewActionVisibilityVal == "Yes") {
	    	                var close_dial = setInterval(function () {
	    	                    $('.fl-closeDialog').hide();
	    	                    if ($('.fl-closeDialog').length >= 1) {
	    	                        clearInterval(close_dial);
	    	                    }
	    	                }, 0);
	    	            }
	    	        }

	    	    }
	        }

	    }
	    
	})
}

function applyColumnSortingEvents(embedded_view, columns){
	console.log("columns", columns);
	for(i in columns){
		var column = $(columns[i]);
		column.on('click', function(e){
			var self = $(this);
			column_option = {};
			if(typeof embedded_view.data('column_sort_option') == "undefined"){
				var column_option = {
					"column": self.attr('data-fld-name'),
					"form_id": self.attr('form-id-origin'),
					"type": "ASC"
				}
			}
			else{
				if(embedded_view.data('column_sort_option')['type'] == "ASC"){
					var column_option = {
						"column": self.attr('data-fld-name'),
						"form_id": self.attr('form-id-origin'),
						"type": "DESC"
					}
				}
				else{
					var column_option = {
						"column": self.attr('data-fld-name'),
						"form_id": self.attr('form-id-origin'),
						"type": "ASC"
					}
				}
			}
			embedded_view.data('column_sort_option', column_option);
			ui.blockPortion(embedded_view);
			initializeMultipleEmbedddedView(embedded_view, "onsort");
		})
	}
}

function applyMultipleSelection(embedded_view, obj_id){
	var embed = $(embedded_view);
	if(typeof $('body').data('user_form_json_data')['form_json'][''+obj_id]['allow_row_selection'] == "undefined" || $('body').data('user_form_json_data')['form_json'][''+obj_id]['allow_row_selection'] == false){
		return false;
	}
	var th = '<th><div style="display:inline-block;"><label><input type="checkbox" class="record_select_all css-checkbox" name="record_select_all" id="record_select_all_' + obj_id + '"/><label for="record_select_all_' + obj_id + '" class="css-label"></label></label></div></th>';
	embed.find('tbody > tr:not(.appendNewRecord):not(.picklist-tr)').each(function(index){
		var form_id = $(this).attr('form-id') || embed.attr('embed-source-form-val-id');
		var ret = '';
		if($(this).hasClass('embed_row_category')){
			ret += "<td></td>";
		}
		else{
			ret += '<td><label><input type="checkbox" class="css-checkbox record_selector" name="record_selector" id="record_selector_' + form_id + '_' + (index + 1) + '"/><label for="record_selector_' + form_id + '_' + (index + 1) + '" class="css-label"></label></label></td>';
		}
		$(this).prepend(ret);
	});
	embed.find('thead > tr').prepend(th);
	embed.find('.record_select_all').on('change', function(e){
		var embedded_view = $(this).closest('.embed-view-container');
		if(embedded_view.find('.record_selector:checked').length == embedded_view.find('.record_selector').length){
			embedded_view.find('.record_selector').each(function(index){
				var checkbox = $(this);
				if(checkbox.prop('checked') == true){
					checkbox.prop('checked', false);
				}
			});
		}
		else{
			embedded_view.find('.record_selector').each(function(index){
				var checkbox = $(this);
				if(checkbox.prop('checked') == false){
					checkbox.prop('checked', true);
				}
			});
		}

		embedded_view.data('selected_rows',embedded_view.find('.record_selector:checked').parents('tr').get());
	});
	embed.find('.record_selector').on('change', function(){
		embedded_view.data('selected_rows',embedded_view.find('.record_selector:checked').parents('tr').get());
	});
}

function categorizeRow(embedded_view){
	var embed_object = $(embedded_view);
	var rows = embed_object.find('tbody > tr:not(.appendNewRecord):not(.embed_row_category)');
	embed_object.find('tbody > .embed_row_category').on('click', function(e){
		var self = $(this);
		var category_value = self.attr('category-value');
		if(self.attr('is-display') == "true"){
			self.parent().find('tr:not(.embed_row_category)[category-value="' + category_value + '"]').css('display', 'none');
			self.attr('is-display','false');
		}
		else{
			self.parent().find('tr:not(.embed_row_category)[category-value="' + category_value + '"]').css('display', '');
			self.attr('is-display','true');
		}
		EmbeddedFixHeader.reFixHeader(embed_object);
	});
	var current_elements = {};
	rows.each(function(index){
		var self = $(this);
		if(self.attr('category-field') != ""){
			var category_value = self.attr('category-value');
			var category_row = embed_object.find('tbody > .embed_row_category[category-value="' + category_value + '"]');
			var counter = Number(category_row.find('td:eq(1) > div > .embed_row_category_counter').text()||0) + 1;
			category_row.find('td:eq(1) > div > .embed_row_category_counter').html(counter);
			self.css('display','none');
			if(typeof current_elements[category_value] != "undefined"){
				self.insertAfter(current_elements[category_value]);
				current_elements[category_value] = self;
			}
			else{
				self.insertAfter(category_row);
				current_elements[category_value] = self;
			}
		}
	});
}

function loadCustomActions(embedded_view, obj_id){ 
	var embedCustomActions = {
		data: {},
		initCustomAction: function(embedded_view, action_element, action_type, action_data){
			var self = this;
			action_element.insertBefore(embedded_view);
			switch(action_type){
				case "new":
					action_element.data('action_data', action_data);
					action_element.on('click', self.CustomActionNew);
					break;
				case "edit":
					action_element.data('action_data', action_data);
					action_element.on('click', self.CustomActionEdit);
					break;
				case "delete":
					action_element.data('action_data', action_data);
					action_element.on('click', self.CustomActionDelete);
					break;
				case "lock":
					action_element.data('action_data', action_data);
					action_element.on('click', self.CustomActionLock);
					break;
				case "custom":
					action_element.data('action_data', action_data);
					action_element.on('click', self.CustomActionCustom);
					break;
				case "cancel_edit":
					action_element.data('action_data', action_data);
					action_element.on('click', self.CustomActionCancelEdit);
					break;
			}
		},
		CustomActionNew: function(e){
			/** 
			select a record at least 1 to run this function
			**/
			if(typeof $(this).attr('on-process') == "undefined"){
				$(this).removeAttr('on-process');
				var self = $(this);
				var embed_object = embedded_view;
				$(this).removeAttr('on-process');
			}
			//we have already a function for creating a new request both popup and inline create
			return;

		},
		CustomActionEdit: function(e){
			/** 
			select a record at least 1 to run this function
			**/
			var self = $(this);
			var embed_object = embedded_view;
    		var selected_rows = embed_object.data('selected_rows')||[];
    		var edit_checker = 0;
    		// console.log("selected_rows", selected_rows, embedded_view);
    		if($(this).parent().find('.embedded_view_custom_action[on-process="true"]').length > 0 && self.attr('edit-mode') != "save"){
    			return;
    		}
    		if(self.attr('edit-mode') == "edit"){
	    		if(selected_rows.length > 0){
	    			ui.blockPortion(embed_object);
		    		for(i in selected_rows){
		    			var row = $(selected_rows[i]);
		    			is_editable = row.attr('is-editable')||"1"
		    			if(is_editable == "1" && !row.is('[is-locked]')){
			    			var row_html = row.html();
			    			row.data('clone_element', row_html);
			    			if($.type(row.attr('reference-id')) == 'undefined'){
					    		var form_id = row.parents('.embed-view-container').attr('embed-source-form-val-id');
						    	var tr_reference = row.attr('reference-id')||form_id + "_" + row.attr('record-id');
						    	row.attr('reference-id', tr_reference);
						    	embeded_view.createTemporaryData(row.parents('.embed-view-container'), 'update', 'in-line', tr_reference, true);
						    }
						    edit_checker++;
				    		checkEmbedAttr(row);
				    	}
				    	else{
				    		continue;
				    	}
		    		}
		    	}
		    	if(edit_checker > 0){
			    	self.attr('data-original-title', htmlEntities(self.html()));
			    	self.text("Save");
			    	self.attr('edit-mode', 'save');
			    	self.parent().children('.custom_action_cancel_edit').css('display','inline-block');
			    	self.attr('on-process', 'true');
			    }
			    EmbeddedFixHeader.reFixHeader(embed_object);
    		}
    		else{
    			ui.blockPortion(embed_object);
				if(selected_rows.length > 0){
					var details = [];
					var action = "embed_editMultipleRecord";

					// {:trackNo,:recordID,formID:formID,flds:flds,fieldRequired:required_fields_attr,fieldEnabled:enabled_fields_attr,Workflow_ID:workflow_id,form_parent_id:$('[name="FormID"]').val()}
					for(row_index in selected_rows){
						var row_data = {};
						var tr_element = $(selected_rows[row_index]);
						var recordID = tr_element.attr('record-id');
						var fields = {};
						var fld_ctr = 0;
						if(tr_element.is('[is-locked]')){
							continue;
						}
		    			tr_element.find(".inlineEdit_" + recordID).each(function(){
							//check if required is empty
							var value_selected = $(this).val() /*|| $(this).val().join(",")*/;
							if($(this).is('[type="radio"]:last')){

								if(!$(this).closest('td').find('[type="radio"]:checked').exists()){

									value_selected = "";
								}
							}
							
							if($(this).is('.fieldRequiredClass:visible') && $.trim(value_selected)=="" && !$(this).is('.display')){

								error_ctr++;
								NotifyMe($(this), "Required!");
							}

							if($(this).is('[type="checkbox"]')){
								var collected_value = [];
								$(this).parents('td').find($(".inlineEdit_" + recordID+":checked")).each(function(){
									collected_value.push($(this).val());
								});
								value_selected = collected_value.join(",");
							}

							if($(this).is('[equivalent-fld-input-type="Currency"]')){
								value_selected = value_selected.replace(/,/g,"");
							}
							
							var name = $(this).attr("data-field-name");
							if($(this).attr('old-value') == value_selected){
								console.log("not modified:", value_selected);
								return;
							}
						    var trimmer = $.trim(value_selected);
						    edit_checker++;
							if($(this).is('[type="radio"]:not(:checked)')){
								return;
							}
							fields[name] = trimmer;
							$(this).removeAttr('old-value');
							fld_ctr++;

							
						});
						row_data['trackNo'] = tr_element.attr('record-trackno');
						row_data['recordID'] = tr_element.attr('record-id');
						row_data['formID'] = tr_element.attr('form-id');
						if(fld_ctr > 0){
							row_data['flds'] = fields;
						}
						else{
							row_data['flds'] = 0;
						}
						details.push(row_data);

					}
					if(details.length > 0){
						var data = {
							action:action,
							details:details
						};

						$.post('/ajax/embedded_view_functions', data, function(e){
							var parsed_result = JSON.parse(e);
							console.log(parsed_result);
							initializeMultipleEmbedddedView(embed_object, "onedit");
							if(edit_checker > 0){
								var virtual_selector = $('<div></div>');
								virtual_selector.html(self.attr('data-original-title'));
								self.html(virtual_selector.text()||"");
								self.attr('edit-mode', 'edit');
								self.parent().children('.custom_action_cancel_edit').css('display', 'none');
								self.removeAttr('on-process');
							}
						});
					}
				}
    		}
		},
		CustomActionDelete: function(e){
			/** 
			select a record at least 1 to run this function
			**/
			var self = $(this);
			var embed_object = embedded_view;
			// var trackNo = $(this).parent().parent().parent().attr("record-trackno");
			// var recordID = $(this).parent().parent().parent().attr("record-id");
			// var formID = $(this).parent().parent().parent().parent().parent().parent().attr("embed-source-form-val-id");
			// var action = "embed_deleteRecord";
			// var data = {action:action,trackNo:trackNo,recordID:recordID,formID:formID}
			// var resultFld = $(this).parent().parent().parent().parent().parent().parent().attr("embed-result-field-val");
			// var message = "Are you sure you want to delete this record?";
			// if(){

			// }
			console.log(self.data("selected_rows"));
			var selected_rows = embed_object.data("selected_rows")||[];
			if($(this).parent().find('.embedded_view_custom_action[on-process="true"]').length > 0){
				return;
			}
			if(selected_rows.length > 0){
				var action = "embed_deleMultipleRecord";
				var multiple_data = [];
				var message = "Are you sure you want to delete the selected record?";
				for(i in selected_rows){
					var selected_row = $(selected_rows[i]);
					var row_data = {};
					if(selected_row.attr('is-editable') == "1" && !selected_row.attr('[is-locke]')){
						row_data['trackNo'] = selected_row.attr('record-trackno')||"";
						row_data['recordID'] = selected_row.attr('record-id')||"";
						row_data['formID'] = selected_row.attr('form-id')||"";
						multiple_data.push(row_data);
					}
				}
				console.log(multiple_data);
				if(multiple_data.length > 0){
					if(multiple_data.length > 1){
						message = "Are you sure you want to delete the selected records?";
					}
					var newConfirm = new jConfirm(message, 'Confirmation Dialog', '', '', '', function(r) {
					    if (r == true) {
							//console.log("Trank No: " + trackNo + "\n")
							//console.log("Record ID: " + recordID + "\n")
							//console.log("Form ID: " + formID + "\n")s
							if(embed_object.attr('commit-data-row-event') != 'parent'){
								var data = {action:action,details:multiple_data}
								$.post("/ajax/embedded_view_functions",data,function(e){
								    var json_data = JSON.parse(e);
								    console.log("JASON data", json_data);
								    if (json_data['notification'] == "Successfull") {
									showNotification({
									    message: json_data['message'],
									    type: "success",
									    autoClose: true,
									    duration: 3
									});
									// Remove Row
									// $('tr[record-trackno="' + trackNo + '"]').remove();
									ui.blockPortion(embed_object);
									initializeMultipleEmbedddedView(embed_object, "ondelete");
									// $("[name='" + resultFld + "']").trigger("change");
								    }
								});
							}
							// else{
							// 	var tr_reference = self.parents('tr').attr('reference-id');
							// 	var record_id = self.parents('tr').attr('record-id');
							// 	var obj_id = self.closest('.setOBJ').attr('data-object-id');
							// 	var data = $('body').data('embed_row_data');
							// 	var embed = self.parents('.embed-view-container');
							// 	var form_id = embed.attr('embed-source-form-val-id');
							// 	if($.type(data) == 'undefined'){
							// 		if($.type($('body').data('embed_deleted_row')) != "undefined"){
							// 			var deleted_row = $('body').data('embed_deleted_row');
							// 			if($.type(deleted_row[obj_id]) != "undefined"){
							// 				deleted_row[obj_id]['id_list'].push(record_id);
							// 			}
							// 			else{
							// 				deleted_row[obj_id]['form_id'] = form_id;
							// 				deleted_row[obj_id]['id_list'] = [];
							// 				deleted_row[obj_id]['id_list'].push(record_id);
							// 			}
							// 		}
							// 		else{
							// 			var deleted_row = {};
							// 			deleted_row[obj_id] = {};
							// 			deleted_row[obj_id]['form_id'] = form_id;
							// 			deleted_row[obj_id]['id_list'] = [];
							// 			deleted_row[obj_id]['id_list'].push(record_id);
							// 			$('body').data('embed_deleted_row', deleted_row);
							// 		}
							// 	}
							// 	else{
							// 		data[form_id][tr_reference] = undefined;
							// 		self.parents('tr').remove();
							// 	}
							// 	embeded_view.getSetTotalOfColumn.call(embed, embed.children());
							// 	embeded_view.fixNumbering(embed);
							// 	if($('.okInsertDEmbedded:visible, .okeditDEmbedded:visible').length <= 0){
							// 		$('.embed-edit-mode').removeClass('embed-edit-mode');
							// 	}
							// 	$("[name='" + resultFld + "']").trigger("change");
							// }
					    }
					});
					newConfirm.themeConfirm("confirm2", {'icon':'<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>'}); 
				}
			}
			else{
				showNotification({
					message: "Please select atleast 1 record to be deleted",
					type: "warning",
					autoClose: true,
					duration: 3
				});
			}
		},
		CustomActionLock: function(e){
			/** 
			select a record at least 1 to run this function
			**/
			var self = $(this);
			var embed_object = embedded_view;
			var selected_rows = embed_object.data("selected_rows");
			// alert("lock is clicked: " + embed_object.find('tr[is-locked]').length + ", " + selected_rows.length);
			if($(this).parent().find('.embedded_view_custom_action[on-process="true"]').length > 0){
				return;
			}
			if(embed_object.find('tr[is-locked]').length != selected_rows.length){
				if(selected_rows.length > 0){
					for(i in selected_rows){
						var row = $(selected_rows[i]);
						if(!row.is('[is-locked]')){
							row.attr('is-locked', '');
							row.css('background-color', 'rgba(220,220,200,1)');
						}
					}
				}
			}
			else{
				//this is for unlocking the records
				if(selected_rows.length > 0){
					for(i in selected_rows){
						var row = $(selected_rows[i]);
						if(row.is('[is-locked]')){
							row.removeAttr('is-locked');
							row.css('background-color', '');
						}
					}
				}
			}
		},
		CustomActionCustom: function(e){
			var self = $(this);
			var embed_object = embedded_view;
			var custom_action_data = self.data('action_data');
			var parsed_formula = embeded_view.parseCondition(custom_action_data['custom_action_formula']||"");
			var selected_rows = embed_object.data('selected_rows');
			var row_datas = [];
			var request_data = {};
			
			ui.blockPortion(embed_object);
			if(typeof embed_object.attr('source-form-type') != "undefined" && embed_object.attr('source-form-type') == "multiple"){
				if(typeof selected_rows != "undefined" && selected_rows.length > 0){
					for(i in selected_rows){
						var selected_row = $(selected_rows[i]);
						var data = {
							"request_id": selected_row.attr('record-id'),
							"form_id": selected_row.attr('form-id'),
							"track_no": selected_row.attr('record-trackno')

						}
						row_datas.push(data);
					}
				}
			}
			else{
				var form_id = embed_object.attr('embed-source-form-val-id');
				if(typeof selected_rows != "undefined" && selected_rows.length > 0){
					for(i in selected_rows){
						var selected_row = $(selected_rows[i]);
						var data = {
							"request_id": selected_row.attr('record-id'),
							"form_id": form_id,
							"track_no": selected_row.attr('record-trackno')

						}
						row_datas.push(data);
					}
				}	
			}
			request_data['action'] = "embed_executeCustomActions";
			request_data['source_form_type'] = embed_object.attr('source-form-type')||"Single";
			request_data['row_data'] = row_datas;
			request_data['custom_action_formula'] = parsed_formula;
			console.log(request_data);
			$.post('/ajax/embedded_view_functions', request_data, function(echo){
				console.log(echo);
				ui.unblockPortion(embed_object);
			});
		},
		CustomActionCancelEdit: function(e){
			var self = $(this);
			var embed_object = embedded_view;
			var selected_rows = embed_object.data('selected_rows')||[];
			if(selected_rows.length > 0){
				for(i in selected_rows){
					var row = $(selected_rows[i]);
					var clone_html = row.data('clone_element');
					row.html(clone_html);
					row.find('[name="record_selector"]').prop('checked', true);
					// rebindViewEvents(row.find('.viewTDEmbedded'), embedded_view_element, element_data['multiple_forms']);
				}
				self.css('display','none');
				var button_text = self.parent().children('.custom_action_edit_record').attr('data-original-title')||"";
				var html_text = self.parent().children('.custom_action_edit_record').html(button_text).text();
				self.parent().children('.custom_action_edit_record').html(html_text);
				self.parent().children('.custom_action_edit_record').removeAttr('on-process');
				self.parent().children('.custom_action_edit_record').attr('edit-mode', 'edit');
			}
		}
	}
	var custom_actions_data = $('body').data("user_form_json_data")['form_json'][""+obj_id]['embed_custom_actions']||[];
	console.log("custom actions data", custom_actions_data);
	for(i in custom_actions_data){
		var custom_action_element = $('<a class="embed_newRequest embedded_view_custom_action fl-buttonEffect btn-basicBtn cursor" style="display:inline-block;padding:5px;margin-bottom:5px; margin-left:3px; font-size:12px; color: #fff;text-decoration:none;"></a>');
		switch(custom_actions_data[i]['custom_action_type']){
			case "1":// default action new
				var action_html = "";
				if(typeof custom_actions_data[i]['custom_action_icon_id'] != "undefined" && custom_actions_data[i]['custom_action_icon_id'] != "" && custom_actions_data[i]['custom_action_icon_id'] != "#svg-custom-actions-icon-000"){
					action_html += '<svg style="width:15px; margin-right:5px;" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' + custom_actions_data[i]['custom_action_icon_id'] + '"></use></svg>';
				}
				custom_action_element.html(action_html + custom_actions_data[i]['custom_action_label']);
				
				custom_action_element.addClass('custom_action_new_record');
				embedCustomActions.initCustomAction(embedded_view, custom_action_element, "new", custom_actions_data[i]);
				break;
			case "2"://default action edit
				var action_html = "";
				if(typeof custom_actions_data[i]['custom_action_icon_id'] != "undefined" && custom_actions_data[i]['custom_action_icon_id'] != "" && custom_actions_data[i]['custom_action_icon_id'] != "#svg-custom-actions-icon-000"){
					action_html += '<svg style="width:15px; margin-right:5px;" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' + custom_actions_data[i]['custom_action_icon_id'] + '"></use></svg>';
				}
				custom_action_element.html(action_html + custom_actions_data[i]['custom_action_label']);
				custom_action_element.attr('edit-mode', 'edit');
				custom_action_element.addClass('custom_action_edit_record');
				
				embedCustomActions.initCustomAction(embedded_view, custom_action_element, "edit");
				var cancel_edit = $('<a class="embed_newRequest custom_action_cancel_edit embedded_view_custom_action fl-buttonEffect btn-basicBtn cursor" style="display:none;padding:5px;margin-bottom:5px; margin-left:3px; font-size:12px; color: #fff;text-decoration:none;">Cancel</a>', custom_actions_data[i]);
				embedCustomActions.initCustomAction(embedded_view, cancel_edit, "cancel_edit");
				break;
			case "3"://default action delete
				var action_html = "";
				if(typeof custom_actions_data[i]['custom_action_icon_id'] != "undefined" && custom_actions_data[i]['custom_action_icon_id'] != "" && custom_actions_data[i]['custom_action_icon_id'] != "#svg-custom-actions-icon-000"){
					action_html += '<svg style="width:15px; margin-right:5px;" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' + custom_actions_data[i]['custom_action_icon_id'] + '"></use></svg>';
				}
				custom_action_element.html(action_html + custom_actions_data[i]['custom_action_label']);
				
				custom_action_element.addClass('custom_action_delete_record');
				embedCustomActions.initCustomAction(embedded_view, custom_action_element, "delete", custom_actions_data[i]);
				break;
			case "4"://default action lock
				var action_html = "";
				if(typeof custom_actions_data[i]['custom_action_icon_id'] != "undefined" && custom_actions_data[i]['custom_action_icon_id'] != "" && custom_actions_data[i]['custom_action_icon_id'] != "#svg-custom-actions-icon-000"){
					action_html += '<svg style="width:15px; margin-right:5px;" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' + custom_actions_data[i]['custom_action_icon_id'] + '"></use></svg>';
				}
				custom_action_element.html(action_html + custom_actions_data[i]['custom_action_label']);
				
				custom_action_element.addClass('custom_action_lock_record');
				embedCustomActions.initCustomAction(embedded_view, custom_action_element, "lock", custom_actions_data[i]);
				break;
			case "5"://custom action
				var action_html = "";
				if(typeof custom_actions_data[i]['custom_action_icon_id'] != "undefined" && custom_actions_data[i]['custom_action_icon_id'] != "" && custom_actions_data[i]['custom_action_icon_id'] != "#svg-custom-actions-icon-000"){
					action_html += '<svg style="width:15px; margin-right:5px;" class="icon-svg icon-svg-workspace" viewBox="0 0 50 50"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' + custom_actions_data[i]['custom_action_icon_id'] + '"></use></svg>';
				}
				custom_action_element.html(action_html + custom_actions_data[i]['custom_action_label']);
				
				custom_action_element.addClass('custom_action_custom_record');
				embedCustomActions.initCustomAction(embedded_view, custom_action_element, "custom", custom_actions_data[i]);
				break;
		}
	}
}

//working in multiple forms only
function embedRequestFormSelection(form_list_data, embedded){
	var ret = '<div>';
			ret += '<h3 class="fl-margin-bottom">';
				ret += '' + '<span>Select a Form:</span>'; //'<i class=" '+ icon +' '+ faiconColor +' icon-save"></i> ' + title;
	    	ret += '</h3></div>';
		    ret += '<div class="hr"></div>';
		    //ret +='<div style="margin:2px 0px 0px 2px;font-size:11px;color:red">' + note + '</div>';
		    ret += '<div class="content-dialog" id="related_info_content">';
		    	ret += '<div class="properties_width_container">';
		    		ret += '<div class="fields_below fl-field-style column div_1_of_1">';
		    			ret += '<div class="input_position_below">';
		    				ret += '<span>Select a Form:</span>';
		    				ret += '<select class="form-select form_selection">';
		    					ret += '<option value="0">--Select--</option>';
		    					for(i in form_list_data){
		    						ret += '<option value="' + form_list_data[i]['source_form'] +  '">' + form_list_data[i]['source_form_name'] + '</option>';
		    					}
		    				ret += '</select>';
		    			ret += '</div>';
		    		ret += '</div>';
		    	ret += '</div>';
		    ret += '</div>';
		    ret += '<input type="button" class="btn-blueBtn fl-margin-right setFormID fl-positive-btn" value="OK"/>';
		ret += '</div>'
	var dialog = new jDialog(ret, '', '', '150', '40',  function(){});
	dialog.themeDialog('modal2');
	$('.setFormID').on('click', function(e){
		var self = $(this);
		var container = self.parents('#popup_container');
		return container.find('.form_selection').val();
	});
}

(function( $ ) {
	$.fn.initializeEmbeddedViewActions = function(){

	}
})(jQuery);


//other functions
function showHideEmbedActions(embed, action){
	var embedded_actions_index = $(embed).find('.embed-table-actions').parent().index();
	var rows = $(embed).find('thead tr, tbody tr:not(.picklist-tr):not(.appendNewRecord)');
	if($(rows[0]).children().eq(0)[0].style.display == '' && $('.temp-display').length <= 0){
		return;
	}
	if(action == 'show'){
		rows.each(function(index){
			var self = $(this);
			var td = self.children().eq(embedded_actions_index);
			console.log(td);
			if(td[0].style.display == 'none'){
				td[0].style.display = '';
				td.addClass('temp-display');
				td.children().children().css('display', 'none');
			}
		});
		EmbeddedFixHeader.reFixHeader($(embed));
	}
	else{
		rows.find('.temp-display').each(function(index){
			var self = $(this);
			self.css('display', 'none')
			self.children().children().css('display', '');
			self.removeClass('temp-display');
		});
		EmbeddedFixHeader.reFixHeader($(embed));
	}
	// console.log("rows", rows);

}
// ================================================================