var embed_field_models = {};
var EmbedFields = {
	"init": function(request_type){
		$('body').on('click', '#editDEmbedded', function(){
			return;
			var tr = $(this).closest('tr');
			EmbedFields.embed_server_timer = undefined;
			EmbedFields.ServerFormulas = {};
			EmbedFields.field_models = {};
			EmbedFields.embed_fields = {};
			EmbedFields.loadFormulas(tr.closest('table').closest('div').attr('embed-source-form-val-id'), tr.attr('record-id'), tr);			
		});
		this.fieldValueFixes();
	},
	"initV2": function(tr_line, form_id, record_id, type, embed_object_id, embed_row){
		console.log("initialize embed formula");
		var tr = $(tr_line);
		EmbedFields.embed_server_timer = undefined;
		EmbedFields.ServerFormulas = {};
		EmbedFields.field_models = {};
		EmbedFields.embed_fields = {};
		// EmbedFields.loadFormulas(form_id, record_id, tr);

		EmbedFields.loadFormulas(embed_row, form_id, record_id, tr_line, type, embed_object_id);
		this.fieldValueFixes(embed_row);
	},
	"loadFormulas": function(embed_row, form_id, record_id, tr_line, type, embed_object_id){
		var json_params = {
			"form_id": form_id,
			"record_id": record_id
		}
		console.log("parameter", tr_line);
		$.post('/ajax/get_fields_data', json_params, function(echo){
			console.log("embed_rows123", embed_row);
			var embed_json_model = {};
			var form_json = JSON.parse(echo);
			console.log("return", form_json);
			var tbl_values = form_json['tbl_values'][0];
			form_json = JSON.parse(form_json[0]['form_json']);
			console.log("form_json", form_json);
			real_form_json = form_json['form_json'];
			var real_fields_data = form_json['fields_data'];
			embed_row.data()['field_models'] = {};
			console.log("the fields data", real_fields_data);
			EmbedFields.GetKeywords(tbl_values, embed_row, type);
			var embed_json_model = EmbedFields.getEmbedFieldNames(real_form_json, real_fields_data, tbl_values, embed_row);
			embed_row.data()['embed_fields'] = embed_json_model;
			if(typeof tr_line == "string"){
				EmbedFields.initializeFormulas(embed_json_model, tr_line, embed_object_id);
				EmbedFields.tr_line = tr_line;
				EmbedFields.formulaEventBinding(tr_line, embed_object_id);
			}
			else{
				EmbedFields.initializeFormulas(embed_json_model, '.tr_' + record_id, embed_object_id);
				EmbedFields.tr_line = '.tr_' + record_id;
				EmbedFields.formulaEventBinding('.tr_' + record_id, embed_object_id);
			}
			if(embed_row.parents('.embed-view-container').attr('stringified-json-data-send')){
				embeded_view.distributeDataSending(JSON.parse($('.getFields_' + embed_object_id).attr('stringified-json-data-send')||"[]"), embed_row);
			}
			EmbedFields.triggerFields(tr_line);

		});
	},
	"getEmbedFieldNames": function(form_json, fields_data, tbl_values, tr_line){
		var embed_json_model = {};
		console.log("form object id", obj_id);
		console.log("forme_json", form_json);
		console.log("getEmbedFieldNames", fields_data);
		for(i in fields_data){
			var obj_id = fields_data[i]['object_id'];
			console.log("typeof", typeof i, i);
			if(!isNaN(i)){
				// console.log("fields_data", i, "fields_data", fields_data[fields_data_index]['data_field_visibility_formula']);	
				var default_computed = "";
				var visibility = "";
				var readonly = "";
				var input_validation_formula = "";
				var input_validation_message = "";
				var field_type = "";
				if(fields_data[i]['data_field_default_type'] != "static"){
					if($.type(fields_data[i]['data_field_formula']) != "undefined"){
						default_computed = fields_data[i]['data_field_formula'];
					}
				}
				if($.type(fields_data[i]['data_field_visibility_formula']) != "undefined"){
					visibility = fields_data[i]['data_field_visibility_formula'];
				}
				if($.type(form_json[obj_id]) != "undefined"){
					if($.type(form_json[obj_id]['ReadOnlyFormula']) != "undefined"){
						readonly = form_json[obj_id]['ReadOnlyFormula'];
					}
				}
				if($.type(fields_data[i]['input_validation']) != "undefined"){
					input_validation_formula = JSON.parse(fields_data[i]['input_validation'])['formula'];
					input_validation_message = JSON.parse(fields_data[i]['input_validation'])['message'];
					
				}
				if($.type(fields_data[i]['field_type']) != "undefined"){
					field_type = fields_data[i]['field_type'];
				}

				$(tr_line).data()['field_models']["embed_" + fields_data[i]['data_field_name']] = tbl_values[fields_data[i]['data_field_name']];
				
				embed_json_model[fields_data[i]['data_field_name']] = {
					"value": tbl_values[fields_data[i]['data_field_name']],
					"default_computed": default_computed,
					"input_validation_formula": input_validation_formula,
					"input_validation_message": input_validation_message,
					'field_type': field_type,
					"visibility": visibility,
					"readonly": readonly
				}
			}
		}
		return embed_json_model;
	},
	"formulaEventBinding": function(tr_line, embed_object_id){
		var self = this;
		for(i in EmbedFields['embed_fields']){
			if($(tr_line).find('[data-field-name="' + i + '"]').length > 0){
				$(tr_line).find('[data-field-name="' + i + '"]').on("change.embedFormulaEvent blur.embedFormulaEvent", function(e, data, server_chaining){
					var self = $(this);
					var key = 'embed_' + self.attr('data-field-name')
					var temp_collection_chain = [];
					var temp_data = data||[];
					var trigger_selection_collection = $();
					var non_trigger_selection_collection = {};
					var regex = new RegExp("\\\$\\\(\\\'\.getFields_" + embed_object_id + '\s' + tr_line + "\\\'\\\)\\\.data\\\(\\\)\\\[\\\'field_models\\\'\\\]", "g");
					var tr = '$(\'' + tr_line + '\').data()[\'field_models\']';
					EmbedFields.fieldValueFixes($(this),"",0);
					// console.log('solid checkbox', self.filter(':checked').map(function(a,b){return $(b).val();}).get().join('|^|'));
					if(self.is('input[type="checkbox"]')){
						var group_obj_name = self.attr("name");

						var obj = self.closest("td").find("[name='"+group_obj_name+"']");

						var checkedValues = obj.filter(':checked').map(function(a,b){return $.trim($(b).val());}).get().join('|^|');

						if($.type($(tr_line).data('field_models')[key]) != "array"){
							$(tr_line).data('field_models')[key] = [];
						}

						$(tr_line).data()['field_models'][key] = checkedValues;


						// $(tr_line).data()['field_models'][key] = ($(tr_line).data()['field_models'][key]||[]).push(self.filter(':checked').val());

						// self.filter(':checked').map(function(a,b){return $(b).val();}).get().join('|^|');
						console.log("etp ung checkbox", $(tr_line).data()['field_models'][key]);
					}else if(self.is('select[multiple]')){
						$(tr_line).data()['field_models'][key] = self.children('option:selected').map(function(a,b){return $(b).val();}).get().join('|^|');
					}
					else{
						if(self.attr('equivalent-fld-input-type') == "Currency"){
							$(tr_line).data()['field_models'][key] = self.val().replace(/,/g, '');
						}
						else{
							$(tr_line).data()['field_models'][key] = self.val();
						}
					}

					if(e.hasOwnProperty('originalEvent')){
						console.log("originalEvent yan e");
						EmbedFields.tr_line = tr_line;
						$(tr_line).attr('on-edit','');
					}

					if($.type(server_chaining) == "undefined"){
						EmbedFields.InitServerFormulas(false, tr_line, temp_data);
					}

					if($.type(self.data('formulaProcess')) == "undefined"){
						self.data('formulaProcess', []);
					}
					else{
						for(i in self.data('formulaProcess')){
							clearInterval(self.data('formulaProcess')[i]);
						}
					}

					EmbedFields.tmp_data = temp_data;
					if($.type(self.data('formula_data_binding')) != "undefined"){
						var formula_chaining = self.data('formula_data_binding');
						if($.type(formula_chaining['root_formula_default_computed']) != "undefined"){
							var formula_binding = formula_chaining['root_formula_default_computed'];
							self.data('formulaProcess').push(AsyncLoop(formula_binding, function(a, b){
								console.log("a", a, "b", b);
								var is_server_function = new RegExp(EmbedFields.server_functions.join("|")).test(b);
								// var temp_field = $(tr_line).find('[data-field-name="' + a + '"]')
								if(is_server_function){
									if($.type(EmbedFields.ServerFormulas[a]) != "undefined"){
										var new_b = b.replace(regex, "\$field_models");
										EmbedFields.ServerFormulas[a][a]['default_computed'] = new_b;
										if($(tr_line).find('[data-field-name="' + a + '"]').length < 1){
											if($.type($(tr_line).data('embed_fields')[a]['formula_data_binding']) != "undefined"){
												var get_formula = $(tr_line).data('embed_fields')[a]['formula_data_binding'];
												var f = get_formula['root_formula_default_computed'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_default_computed'] = f;
												f = get_formula['root_formula_readonly'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_readonly'] = f;
												f = get_formula['root_formula_visibility'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_visibility'] = f;
												EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
											}
										}
										else{
											if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
												var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
												var f = get_formula['root_formula_default_computed'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_default_computed'] = f;
												f = get_formula['root_formula_readonly'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_readonly'] = f;
												f = get_formula['root_formula_visibility'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_visibility'] = f;
												EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
											}
										}
									}
									else{
										var field_name = {};
										var sv_b = b.replace(regex, "\$field_models");
										var default_computed = {
											"default_computed": sv_b
										}
										field_name[a] = default_computed;
										EmbedFields.ServerFormulas[a] = field_name;
										if($(tr_line).find('[data-field-name="' + a + '"]').length < 1){
											if($(tr_line).data('embed_fields')[a]['formula_data_binding'] != "undefined"){
												var get_formula = $(tr_line).data('embed_fields')[a]['formula_data_binding'];
												var f = get_formula['root_formula_default_computed'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_default_computed'] = f;
												f = get_formula['root_formula_readonly'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_readonly'] = f;
												f = get_formula['root_formula_visibility'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_visibility'] = f;
												EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
											}
										}
										else{
											if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
												var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
												var f = get_formula['root_formula_default_computed'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_default_computed'] = f;
												f = get_formula['root_formula_readonly'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_readonly'] = f;
												f = get_formula['root_formula_visibility'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_visibility'] = f;
												EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
											}
										}
									}
									if($(tr_line).find('[data-field-name="' + a + '"]').length < 1){
										console.log("embed_fields", $(tr_line).data('embed_fields')[a]);
										non_trigger_selection_collection[a] = $(tr_line).data('embed_fields')[a];
									}
									else{
										if(!is_server_function){
											trigger_selection_collection = trigger_selection_collection.add($(tr_line).find('[data-field-name="' + a + '"]'));
										}
									}
								}
								else{
									// console.log("a", a, "val", b);
									var new_b = b.replace(/\$field_models/g, tr);
									if($(tr_line).find('[data-field-name="' + a + '"]').length < 1){
										console.log("a", a , "b", new_b);
										var chain_key = "embed_" + a;
										$(tr_line).data()['embed_fields'][a]['value'] = eval(new_b);
										$(tr_line).data()['field_models'][chain_key] = eval(new_b);
									}
									else{
										if($(tr_line).find('[data-field-name="' + a + '"]').attr('equivalent-fld-input-type') == "Currency"){
											$(tr_line).find('[data-field-name="' + a + '"]').val((Number(eval(new_b))||0).currencyFormat());
										}
										else{
											$(tr_line).find('[data-field-name="' + a + '"]').val(eval(new_b));
										}
										EmbedFields.fieldValueFixes($(tr_line).find('[data-field-name="' + a + '"]'),"",1);
										var chain_key = "embed_" + a;
										$(tr_line).data()['field_models'][chain_key] = $(tr_line).find('[data-field-name="' + a + '"]').val();
									}
									if($(tr_line).find('[data-field-name="' + a + '"]').length < 1){
										// if(!is_server_function){
											non_trigger_selection_collection[a] = $(tr_line).data('embed_fields')[a];
										// }
									}
									else{
										// if(!is_server_function){
											trigger_selection_collection = trigger_selection_collection.add($(tr_line).find('[data-field-name="' + a + '"]'));
										// }
									}
								}
							}, function(){

								//readonly formulas
								if($.type(self.data('formula_data_binding')) != "undefined"){
									var formula_chaining = self.data('formula_data_binding');
									if($.type(formula_chaining['root_formula_readonly']) != "undefined"){
										var formula_binding = formula_chaining['root_formula_readonly'];
										AsyncLoop(formula_binding, function(a, b){
											var is_server_function = new RegExp(EmbedFields.server_functions.join("|")).test(b);
											// var temp_field = $(tr_line).find('[data-field-name="' + a + '"]')
											if(is_server_function){
												if($.type(EmbedFields.ServerFormulas[a]) != "undefined"){
													var new_b = b.replace(regex, "\$field_models");
													EmbedFields.ServerFormulas[a][a]['readonly'] = new_b;
													if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
														var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
														var f = get_formula['root_formula_readonly'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_readonly'] = f;
														f = get_formula['root_formula_default_computed'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_default_computed'] = f;
														f = get_formula['root_formula_visibility'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_visibility'] = f;
														EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
													}
												}
												else{
													var field_name = {};
													var sv_b = b.replace(regex, "\$field_models");
													var default_computed = {
														"readonly": sv_b
													}
													field_name[a] = default_computed;
													EmbedFields.ServerFormulas[a] = field_name;
													if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
														var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
														var f = get_formula['root_formula_readonly'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_readonly'] = f;
														f = get_formula['root_formula_default_computed'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_default_computed'] = f;
														f = get_formula['root_formula_visibility'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_visibility'] = f;
														get_formula['root_formula_readonly'] = f;
														EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
													}
												}
											}
											else{
												var new_b = b.replace(/\$field_models/g, tr);
												var res = eval(new_b);
												if($.type(res) != "undefined"){
													if($.type(res) == "boolean"){
														if(res == true){
															$(tr_line).find('[data-field-name="' + a + '"]').removeAttr('readonly');
														}
														else{
															$(tr_line).find('[data-field-name="' + a + '"]').attr('readonly', 'readonly');
														}
													}
												}
											}
										
										});
									}
								}

								//visibility formulas
								if($.type(self.data('formula_data_binding')) != "undefined"){
									var formula_chaining = self.data('formula_data_binding');
									if($.type(formula_chaining['root_formula_visibility']) != "undefined"){
										var formula_binding = formula_chaining['root_formula_visibility'];
										self.data('formulaProcess').push(AsyncLoop(formula_binding, function(a, b){
											var is_server_function = new RegExp(EmbedFields.server_functions.join("|")).test(b);
											// var temp_field = $(tr_line).find('[data-field-name="' + a + '"]')
											if(is_server_function){
												if($.type(EmbedFields.ServerFormulas[a]) != "undefined"){
													var new_b = b.replace(/EmbedFields\./g, "$");
													EmbedFields.ServerFormulas[a][a]['visibility'] = new_b;
													if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
														var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
														var f = get_formula['root_formula_visibility'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_visibility'] = f;
														f = get_formula['root_formula_default_computed'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_default_computed'] = f;
														f = get_formula['root_formula_readonly'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_readonly'] = f;
														console.log("get_formula", get_formula);
														EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
													}
													else{
														console.log("boom! baliw a");
													}
												}
												else{
													var field_name = {};
													var sv_b = b.replace(regex, "\$field_models");
													var default_computed = {
														"visibility": sv_b
													}
													field_name[a] = default_computed;
													EmbedFields.ServerFormulas[a] = field_name;
													if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
														var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
														var f = get_formula['root_formula_visibility'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_visibility'] = f;
														f = get_formula['root_formula_default_computed'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_default_computed'] = f;
														f = get_formula['root_formula_readonly'];
														for(i in f){
															f[i] = f[i].replace(regex, "\$field_models");
														}
														get_formula['root_formula_readonly'] = f;
														EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
													}
													else{
														console.log("boom! baliw b");
													}
												}
											}
											else{
												var new_b = b.replace(/\$field_models/g, tr);
												var res = eval(new_b);
												if($.type(res) != "undefined"){
													if($.type(res) == "boolean"){
														if(res == true){
															$(tr_line).find('[data-field-name="' + a + '"]').addClass('display');
														}
														else{
															$(tr_line).find('[data-field-name="' + a + '"]').removeClass('display');
														}
													}
												}
											}
										}));
									}
								}
								//trigger chaining

								var temp_selected_fn_collection_array = [];
								
								if(trigger_selection_collection.length < 1){
									temp_selected_fn_collection_array = Object.keys(non_trigger_selection_collection);
								}
								else{
									temp_selected_fn_collection_array = trigger_selection_collection.map(function(){
										return $(this).attr("data-field-name");
									}).get();
								}
								self.data('formulaProcess').push(AsyncLoop(temp_selected_fn_collection_array, function(a, b){			
									if($.inArray(b, temp_data) < 0){
										temp_data = temp_data.concat(b);
										setTimeout(function(){
											if($(tr_line).find('[data-field-name="' + b + '"]').length < 1){
												console.log("walang forever", b);
												EmbedFields.NonExistingFieldTrigger(tr_line, $(tr_line).data('embed_fields')[b], b, temp_data, server_chaining);
											}
											else{
												$(tr_line).find('[data-field-name="' + b + '"]').trigger('change.embedFormulaEvent', [temp_data, server_chaining]);
											}
										}, 0);
										// }
									}
								}));
							}));
						}
						if($.type(formula_chaining['root_formula_readonly']) != "undefined"){
							var formula_binding = formula_chaining['root_formula_readonly'];
							self.data('formulaProcess').push(AsyncLoop(formula_binding, function(a, b){
								var is_server_function = new RegExp(EmbedFields.server_functions.join("|")).test(b);
								// var temp_field = $(tr_line).find('[data-field-name="' + a + '"]')
								if(is_server_function){
									if($.type(EmbedFields.ServerFormulas[a]) != "undefined"){
										var new_b = b.replace(regex, "$field_models");
										EmbedFields.ServerFormulas[a][a]['readonly'] = new_b;
										if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
											var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
											var f = get_formula['root_formula_readonly'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_readonly'] = f;
											f = get_formula['root_formula_default_computed'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_default_computed'] = f;
											f = get_formula['root_formula_visibility'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_visibility'] = f;
											EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
										}
									}
									else{
										var field_name = {};
										var sv_b = b.replace(regex, "\$field_models");
										var default_computed = {
											"readonly": sv_b
										}
										field_name[a] = default_computed;
										EmbedFields.ServerFormulas[a] = field_name;
										if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
											var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
											var f = get_formula['root_formula_readonly'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_readonly'] = f;
											f = get_formula['root_formula_default_computed'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_default_computed'] = f;
											f = get_formula['root_formula_visibility'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_visibility'] = f;
											console.log("get_formula", get_formula);
											EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
										}
									}
								}
								else{
									var new_b = b.replace(/\$field_models/g, tr);
									var res = eval(new_b);
									if($.type(res) != "undefined"){
										if($.type(res) == "boolean"){
											if(res == true){
												$(tr_line).find('[data-field-name="' + a + '"]').removeAttr('readonly');
											}
											else{
												$(tr_line).find('[data-field-name="' + a + '"]').attr('readonly', 'readonly');
											}
										}
										$(tr_line).data()['field_models']['embed_' + a] = $(tr_line).find('[data-field-name="' + a + '"]').val();
									}
								}
							
							}, function(){
								var formula_binding = formula_chaining['root_formula_visibility'];
								self.data('formulaProcess').push(AsyncLoop(formula_binding, function(a, b){
									var is_server_function = new RegExp(EmbedFields.server_functions.join("|")).test(b);
									// var temp_field = $(tr_line).find('[data-field-name="' + a + '"]')

									if(is_server_function){
										if($.type(EmbedFields.ServerFormulas[a]) != "undefined"){
											var new_b = b.replace(regex, "$field_models");
											EmbedFields.ServerFormulas[a][a]['visibility'] = new_b;
											if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
												var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
												var f = get_formula['root_formula_visibility'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_visibility'] = f;
												f = get_formula['root_formula_default_computed'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_default_computed'] = f;
												f = get_formula['root_formula_readonly'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_readonly'] = f;
												console.log("get_formula", get_formula);
												EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
											}
											else{
												console.log("boom! baliw c");
											}
										}
										else{
											var field_name = {};
											var sv_b = b.replace(regex, "\$field_models");
											var default_computed = {
												"visibility": sv_b
											}
											field_name[a] = default_computed;
											EmbedFields.ServerFormulas[a] = field_name;
											if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
												var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
												var f = get_formula['root_formula_visibility'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_visibility'] = f;
												f = get_formula['root_formula_default_computed'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_default_computed'] = f;
												f = get_formula['root_formula_readonly'];
												for(i in f){
													f[i] = f[i].replace(regex, "\$field_models");
												}
												get_formula['root_formula_readonly'] = f;
												console.log("get_formula", get_formula);
												EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
											}
											else{
												console.log("boom! baliw d");
											}
										}
									}
									else{
										var new_b = b.replace(/\$field_models/g, tr);
										var res = eval(new_b);
										if($.type(res) != "undefined"){
											if($.type(res) == "boolean"){
												if(res == true){
													$(tr_line).find('[data-field-name="' + a + '"]').addClass('display');
												}
												else{
													$(tr_line).find('[data-field-name="' + a + '"]').removeClass('display');
												}
											}
										}
									}
								}));
								//trigger chaining
								var temp_selected_fn_collection_array = [];
								
								if(trigger_selection_collection.length < 1){
									temp_selected_fn_collection_array = Object.keys(non_trigger_selection_collection);
								}
								else{
									temp_selected_fn_collection_array = trigger_selection_collection.map(function(){
										return $(this).attr("data-field-name");
									}).get();
								}
								self.data('formulaProcess').push(AsyncLoop(temp_selected_fn_collection_array, function(a, b){			
									if($.inArray(  b  , temp_data) < 0){
										temp_data = temp_data.concat(b);
										// console.log("TRIGGEREDs", b)
										// if($.type($('[name="'+b+'"]').attr('original-event')) == 'undefined'){
										setTimeout(function(){
											if($(tr_line).find('[data-field-name="' + b + '"]').length < 1){
												EmbedFields.NonExistingFieldTrigger(tr_line, $(tr_line).data('embed_fields')[b], b, temp_data, server_chaining);
											}
											else{
												$(tr_line).find('[data-field-name="' + b + '"]').trigger('change.embedFormulaEvent', [temp_data, server_chaining]);
											}
											
										}, 0);
										// }
									}
								}));
							}));
						}
						if($.type(formula_chaining['root_formula_visibility']) != "undefined"){
							var formula_binding = formula_chaining['root_formula_visibility'];
							self.data('formulaProcess').push(AsyncLoop(formula_binding, function(a, b){
								var is_server_function = new RegExp(EmbedFields.server_functions.join("|")).test(b);
								if(is_server_function){
									if($.type(EmbedFields.ServerFormulas[a]) != "undefined"){
										var new_b = b.replace(/EmbedFields\./g, "$");
										EmbedFields.ServerFormulas[a][a]['visibility'] = new_b;
										if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
											var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
											var f = get_formula['root_formula_visibility'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_visibility'] = f;
											f = get_formula['root_formula_default_computed'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_default_computed'] = f;
											f = get_formula['root_formula_readonly'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_readonly'] = f;
											console.log("get_formula", get_formula);
											EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
										}
										else{
											console.log("boom! baliw e");
										}
									}
									else{
										var field_name = {};
										var sv_b = b.replace(regex, "\$field_models");
										var default_computed = {
											"visibility": sv_b
										}
										field_name[a] = default_computed;
										EmbedFields.ServerFormulas[a] = field_name;
										if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
											var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_visibility'] = f;
											f = get_formula['root_formula_default_computed'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_default_computed'] = f;
											f = get_formula['root_formula_readonly'];
											for(i in f){
												f[i] = f[i].replace(regex, "\$field_models");
											}
											get_formula['root_formula_readonly'] = f;
											console.log("get_formula", get_formula);
											EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
										}
										else{
											console.log("boom! baliw f");
										}
									}
								}
								else{
									var new_b = b.replace(/\$field_models/g, tr);
									var res = eval(new_b);
									if($.type(res) != "undefined"){
										if($.type(res) == "boolean"){
											if(res == true){
												$(tr_line).find('[data-field-name="' + a + '"]').addClass('display');
											}
											else{
												$(tr_line).find('[data-field-name="' + a + '"]').removeClass('display');
											}
										}
									}
								}
							}));
							var temp_selected_fn_collection_array = [];
							
							if(trigger_selection_collection.length < 1){
								temp_selected_fn_collection_array = Object.keys(non_trigger_selection_collection);
							}
							else{
								temp_selected_fn_collection_array = trigger_selection_collection.map(function(){
									return $(this).attr("data-field-name");
								}).get();
							}
							self.data('formulaProcess').push(AsyncLoop(temp_selected_fn_collection_array, function(a, b){			
								if($.inArray(  b  , temp_data) < 0){
									temp_data = temp_data.concat(b);
									setTimeout(function(){
										if($(tr_line).find('[data-field-name="' + b + '"]').length < 1){
											EmbedFields.NonExistingFieldTrigger(tr_line, $(tr_line).data('embed_fields')[b], b, temp_data, server_chaining);
										}
										else{
											$(tr_line).find('[data-field-name="' + b + '"]').trigger('change.embedFormulaEvent', [temp_data, server_chaining]);
										}
									}, 0);
									// }
								}
							}));
						}
					}
				});
			}
		}
	},
	"initializeFormulas": function(embed_fields, tr_line, embed_obj_id){
		var embed_json_model = {};

		embed_fields = EmbedFields.addPrefixToFields(Object.keys(embed_fields), embed_fields, 'embed_');
		var keys = Object.keys(embed_fields);
		keys = keys.map(function(value){ return 'embed_' + value});
		for(i in embed_fields){
			var embed_key = i;
			if($.type(embed_fields[embed_key]['default_computed']) != "undefined"){
				if(embed_fields[embed_key]['default_computed'] != ""){
					var formula = embed_fields[embed_key]['default_computed'];
					var get_parsed_formula = EmbedFields.embedFormulaParser(keys, formula, tr_line, embed_obj_id);
					if($.type(embed_fields[embed_key]['parsedf_default_computed']) != "undefined"){
						embed_fields[embed_key]['parsedf_default_computed'] = get_parsed_formula['parsed_formula'];
					}
					else{
						var get_embed = embed_fields[embed_key];
						get_embed['parsedf_default_computed'] = get_parsed_formula['parsed_formula'];
					}
					var get_involve_fields = get_parsed_formula["involve_fields"];
					for(j in get_involve_fields){
						var original_field = get_involve_fields[j].replace(/embed_/g, "");
						if($.type(embed_fields[original_field]["formula_data_binding"]) == "undefined"){
							embed_fields[original_field]["formula_data_binding"] = {}
							if($.type(embed_fields[original_field]["formula_data_binding"]["root_formula_default_computed"]) != "undefined"){
								embed_fields[original_field]["formula_data_binding"]["root_formula_default_computed"][embed_key] = get_parsed_formula['parsed_formula'];
							}
							else{
								var key_field = original_field;
								embed_fields[original_field]["formula_data_binding"]["root_formula_default_computed"] = {};
								embed_fields[original_field]["formula_data_binding"]["root_formula_default_computed"][embed_key] = get_parsed_formula['parsed_formula']
							}
						}
						else{
							if($.type(embed_fields[original_field]["formula_data_binding"]["root_formula_default_computed"]) != "undefined"){
								embed_fields[original_field]["formula_data_binding"]["root_formula_default_computed"][embed_key] = get_parsed_formula['parsed_formula'];
							}
							else{
								var key_field = original_field;
								embed_fields[original_field]["formula_data_binding"]["root_formula_default_computed"] = {};
								embed_fields[original_field]["formula_data_binding"]["root_formula_default_computed"][embed_key] = get_parsed_formula['parsed_formula']
							}
						}
					}
				}
			}
			if($.type(embed_fields[embed_key]['readonly']) != "undefined"){
				if(embed_fields[embed_key]['readonly'] != ""){
					var formula = embed_fields[embed_key]['readonly'];
					var get_parsed_formula = EmbedFields.embedFormulaParser(keys, formula, tr_line, embed_obj_id);
					if($.type(embed_fields[embed_key]['parsedf_default_computed']) != "undefined"){
						embed_fields[embed_key]['parsedf_readonly'] = get_parsed_formula['parsed_formula'];
					}
					else{
						var get_embed = embed_fields[embed_key];
						get_embed['parsedf_readonly'] = get_parsed_formula['parsed_formula'];
					}
					var get_involve_fields = get_parsed_formula["involve_fields"];
					for(j in get_involve_fields){
						var original_field = get_involve_fields[j].replace(/embed_/g, "");
						if($.type(embed_fields[original_field]["formula_data_binding"]) == "undefined"){
							embed_fields[original_field]["formula_data_binding"] = {}
							if($.type(embed_fields[original_field]["formula_data_binding"]["root_formula_readonly"]) != "undefined"){
								embed_fields[original_field]["formula_data_binding"]["root_formula_readonly"][embed_key] = get_parsed_formula['parsed_formula'];
							}
							else{
								var key_field = original_field;
								embed_fields[original_field]["formula_data_binding"]["root_formula_readonly"] = {};
								embed_fields[original_field]["formula_data_binding"]["root_formula_readonly"][embed_key] = get_parsed_formula['parsed_formula']
							}
						}
						else{
							if($.type(embed_fields[original_field]["formula_data_binding"]["root_formula_readonly"]) != "undefined"){
								embed_fields[original_field]["formula_data_binding"]["root_formula_readonly"][embed_key] = get_parsed_formula['parsed_formula'];
							}
							else{
								var key_field = original_field;
								embed_fields[original_field]["formula_data_binding"]["root_formula_readonly"] = {};
								embed_fields[original_field]["formula_data_binding"]["root_formula_readonly"][embed_key] = get_parsed_formula['parsed_formula']
							}
						}
					}
				}
			}
			if($.type(embed_fields[embed_key]['visibility']) != "undefined"){
				if(embed_fields[embed_key]['visibility'] != ""){
					var formula = embed_fields[embed_key]['visibility'];
					var get_parsed_formula = EmbedFields.embedFormulaParser(keys, formula, tr_line, embed_obj_id);
					if($.type(embed_fields[embed_key]['parsedf_visibility']) != "undefined"){
						embed_fields[embed_key]['parsedf_visibility'] = get_parsed_formula['parsed_formula'];
					}
					else{
						var get_embed = embed_fields[embed_key];
						get_embed['parsedf_visibility'] = get_parsed_formula['parsed_formula'];
					}
					var get_involve_fields = get_parsed_formula["involve_fields"];
					for(j in get_involve_fields){
						var original_field = get_involve_fields[j].replace(/embed_/g, "");
						if($.type(embed_fields[original_field]["formula_data_binding"]) == "undefined"){
							embed_fields[original_field]["formula_data_binding"] = {}
							if($.type(embed_fields[original_field]["formula_data_binding"]["root_formula_visibility"]) != "undefined"){
								console.log("not undefined root_formula_visibility");
								embed_fields[original_field]["formula_data_binding"]["root_formula_visibility"][embed_key] = get_parsed_formula['parsed_formula'];
							}
							else{
								var key_field = original_field;
								embed_fields[original_field]["formula_data_binding"]["root_formula_visibility"] = {};
								embed_fields[original_field]["formula_data_binding"]["root_formula_visibility"][embed_key] = get_parsed_formula['parsed_formula']
							}
						}
						else{
							if($.type(embed_fields[original_field]["formula_data_binding"]["root_formula_visibility"]) != "undefined"){
								console.log("not undefined root_formula_visibility");
								embed_fields[original_field]["formula_data_binding"]["root_formula_visibility"][embed_key] = get_parsed_formula['parsed_formula'];
							}
							else{
								var key_field = original_field;
								embed_fields[original_field]["formula_data_binding"]["root_formula_visibility"] = {};
								embed_fields[original_field]["formula_data_binding"]["root_formula_visibility"][embed_key] = get_parsed_formula['parsed_formula']
								console.log("embed_fields", embed_fields[original_field]["formula_data_binding"]);
							}
						}
					}
				}
			}
		}
		// console.log("embed_fields", embed_fields);
		EmbedFields['embed_fields'] = embed_fields;
		EmbedFields.bindData(tr_line);
	},
	"triggerFields":function(tr_line){
		console.log($(tr_line).data('embed_fields'));
		var get_keys = Object.keys($(tr_line).data('embed_fields'));
		AsyncLoop(get_keys, function(index, value){
			$(tr_line).find('[data-field-name="' + value + '"]').trigger('change');
		});
	},
	"bindData":function(tr_line){
		for(i in EmbedFields['embed_fields']){
			if($(tr_line).find('[data-field-name="' + i + '"]').length > 0){
				$(tr_line).find('[data-field-name="' + i + '"]').data(EmbedFields['embed_fields'][i]);
			}
		}
	},
	"addPrefixToFields": function(field_names, embed_fields, prefix){
		for(i in embed_fields){
			if($.type(embed_fields[i]['default_computed']) != "undefined"){
				if(embed_fields[i]['default_computed'] != ""){
					for(j in field_names){
						if(embed_fields[i]['default_computed'].indexOf(field_names[j]) > -1){
							var reg = new RegExp('@' + field_names[j], 'g');
							embed_fields[i]['default_computed'] = embed_fields[i]['default_computed'].replace(reg, '@' + prefix + field_names[j]);
						}
					}
				}
			}
			if($.type(embed_fields[i]['readonly']) != "undefined"){
				if(embed_fields[i]['readonly'] != ""){
					for(j in field_names){
						if(embed_fields[i]['readonly'].indexOf(field_names[j]) > -1){
							var reg = new RegExp('@' + field_names[j], 'g');
							embed_fields[i]['readonly'] = embed_fields[i]['readonly'].replace(reg, '@' + prefix + field_names[j]);
						}
					}	
				}
			}
			if($.type(embed_fields[i]['visibility']) != "undefined"){
				if(embed_fields[i]['visibility'] != ""){
					for(j in field_names){
						if(embed_fields[i]['visibility'].indexOf(field_names[j]) > -1){
							var reg = new RegExp('@' + field_names[j], 'g');
							embed_fields[i]['visibility'] = embed_fields[i]['visibility'].replace(reg, '@' + prefix + field_names[j]);
						}
					}
				}
			}
			if($.type(embed_fields[i]['input_validation_formula']) != "undefined"){
				if(embed_fields[i]['input_validation_formula'] != ""){
					for(j in field_names){
						if(embed_fields[i]['input_validation_formula'].indexOf(field_names[j]) > -1){
							var reg = new RegExp('@' + field_names[j], 'g');
							embed_fields[i]['input_validation_formula'] = embed_fields[i]['input_validation_formula'].replace(reg, '@' + prefix + field_names[j]);
						}
					}
				}
			}
		}
		console.log('new embed fields', embed_fields);
		return embed_fields;
	},
	"embedFormulaParser": function(field_names, formula_string, tr_line, embed_obj_id){
		var reg_parts_ff = [
		    "@SDev\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
		    "@GetAVG\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
		    "@GetMin\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
		    "@GetMax\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
		    "@Sum\\\(@[a-zA-Z][a-zA-Z0-9_]*\\\)",
		    "@[a-zA-Z][a-zA-Z0-9_]*"
		];
		var remove_comments = new RegExp("\/\/.*", "g");
		formula_string = formula_string.replace(remove_comments, '');
		var keywords = new RegExp(EmbedFields.formula_keywordsV2.join("|"), "g");
		formula_string = formula_string.replace(keywords, function(m1){
			m1 = m1.replace(/@/g, '');
			if($.inArray(m1, EmbedFields.formula_keywords) > -1){
				console.log('$(\'\.getFields_' + embed_obj_id + ' ' + tr_line + '\').data()[\'field_models\'][\'' + m1 + '\']');
				return '$(\'.getFields_' + embed_obj_id + ' ' + tr_line + '\').data()[\'field_models\'][\'' + m1 + '\']';
			}
			else{
				return m1;
			}
		});
		console.log("formula_string", formula_string);
		var reg = new RegExp(reg_parts_ff.join("|"),"g");
		if(formula_string.match(reg)){
			var fields_used = formula_string.match(reg).map(function(a){
				var f_name = a.replace(/@/g, '');
				if($.inArray(f_name, field_names) > -1){
					return f_name;
				}
			});
			fields_used = fields_used.filter(Boolean);
			formula_string = formula_string.replace(reg, function(m1){
				m1 = m1.replace(/@/g, '');
				if($.inArray(m1, field_names) > -1){
					console.log("embed_obj_id", embed_obj_id);
					return '$(\'\.getFields_' + embed_obj_id + ' ' + tr_line + '\').data()[\'field_models\'][\'' + m1 + '\']';
				}
				else{
					return m1;
				}
			});
		}
		//solid
		formula_string = formula_string.replace(/@/g, '');
		return {
			"parsed_formula": formula_string,
			"involve_fields": fields_used
		};
	},
	"NonExistingFieldTrigger": function(tr_line, element_json, element_name, data, server_chaining){
		var self = element_json;
		var key = 'embed_' + element_name;
		var temp_collection_chain = [];
		var temp_data = data||[];
		var trigger_selection_collection = $();
		var regex = new RegExp("\\\$\\\(\\\'" + tr_line + "\\\'\\\)\\\.data\\\(\\\)\\\[\\\'field_models\\\'\\\]", "g");
		var tr = '$(\'' + tr_line + '\').data()[\'field_models\']';
		$(tr_line).data()['field_models'][key] = self['value'];
		if($.type(server_chaining) == "undefined"){
			EmbedFields.InitServerFormulas(false, tr_line, temp_data);
		}
		else{
			console.log("temp_data", EmbedFields.tmp_data);
		}
		if($.type(self['formulaProcess']) == "undefined"){
			self['formulaProcess'] = [];
		}
		else{
			for(i in self['formulaProcess']){
				clearInterval(self['formulaProcess'][i]);
			}
		}
		
		// EmbedFields.tmp_data = temp_data;
		if($.type(self['formula_data_binding']) != "undefined"){
			var formula_chaining = self['formula_data_binding'];
			if($.type(formula_chaining['root_formula_default_computed']) != "undefined"){
				var formula_binding = formula_chaining['root_formula_default_computed'];
				// console.log("formula_binding", formula_binding);
				self['formulaProcess'].push(AsyncLoop(formula_binding, function(a, b){
					console.log("a",a,"b",b);
					var is_server_function = new RegExp(EmbedFields.server_functions.join("|")).test(b);
					// var temp_field = $(tr_line).find('[data-field-name="' + a + '"]')
					if(is_server_function){
						if($.type(EmbedFields.ServerFormulas[a]) != "undefined"){
							var new_b = b.replace(regex, "$field_models");
							EmbedFields.ServerFormulas[a][a]['default_computed'] = new_b;
							if($(tr_line).find('[data-field-name="' + a + '"]').length < 1){
								var get_formula = $(tr_line).data('embed_fields')[a]['formula_data_binding'];
								var f = get_formula['root_formula_default_computed'];
								for(i in f){
									f[i] = f[i].replace(regex, "\$field_models");
								}
								get_formula['root_formula_default_computed'] = f;
								EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
							}
							else{
								if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
									var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
									var f = get_formula['root_formula_default_computed'];
									for(i in f){
										f[i] = f[i].replace(regex, "\$field_models");
									}
									get_formula['root_formula_default_computed'] = f;
									EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
								}
							}
						}
						else{
							var field_name = {};
							var sv_b = b.replace(regex, "\$field_models");
							var default_computed = {
								"default_computed": sv_b
							}
							field_name[a] = default_computed;
							EmbedFields.ServerFormulas[a] = field_name;
							if($(tr_line).find('[data-field-name="' + a + '"]').length < 1){
								if($.type($(tr_line).data('embed_fields')[a]['formula_data_binding']) != "undefined"){
									var get_formula = $(tr_line).data('embed_fields')[a]['formula_data_binding'];
									var f = get_formula['root_formula_default_computed'];
									for(i in f){
										f[i] = f[i].replace(regex, "\$field_models");
									}
									get_formula['root_formula_default_computed'] = f;
									EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
								}
							}
							else{
								if($.type($(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding')) != "undefined"){
									var get_formula = $(tr_line).find('[data-field-name="' + a + '"]').data('formula_data_binding');
									var f = get_formula['root_formula_default_computed'];
									for(i in f){
										f[i] = f[i].replace(regex, "\$field_models");
									}
									get_formula['root_formula_default_computed'] = f;
									EmbedFields.ServerFormulas[a]['invovle_fields'] = get_formula;
								}
							}
						}
						if($(tr_line).find('[data-field-name="' + a + '"]').length < 1){
								trigger_selection_collection = trigger_selection_collection.add($(tr_line).data('embed_fields')[a]);
						}
						else{
							trigger_selection_collection = trigger_selection_collection.add($(tr_line).find('[data-field-name="' + a + '"]'));
						}
					}
					else{
						console.log("change value agad");
						var new_b = b.replace(/\$field_models/g, tr);
						if((tr_line).find('[data-field-name="' + a + '"]').length < 1){
							$(tr_line).data('field_models')[a]['value'] = eval(new_b);
						}
						else{
							$(tr_line).find('[data-field-name="' + a + '"]').val(eval(new_b));
						}
						var chain_key = "embed_" + a;
						$(tr_line).data()['field_models'][chain_key] = $(tr_line).find('[data-field-name="' + a + '"]').val();
						if($(tr_line).find('[data-field-name="' + a + '"]').length < 1){
							trigger_selection_collection = trigger_selection_collection.add($(tr_line).data('embed_fields')[a]);
						}
						else{
							trigger_selection_collection = trigger_selection_collection.add($(tr_line).find('[data-field-name="' + a + '"]'));
						}
					}
				}, function(){
					//trigger chaining
					var temp_selected_fn_collection_array = trigger_selection_collection.map(function(){
						return $(this).attr("data-field-name");
					}).get();
					self['formulaProcess'].push(AsyncLoop(temp_selected_fn_collection_array, function(a, b){			
						if($.inArray(  b  , temp_data) < 0){
							temp_data = temp_data.concat(b);
							// console.log("TRIGGEREDs", b)
							// if($.type($('[name="'+b+'"]').attr('original-event')) == 'undefined'){
							//check muna kung existing find_me
							setTimeout(function(){
								if($(tr_line).find('[data-field-name="' + b + '"]').length < 1){
									EmbedFields.NonExistingFieldTrigger(tr_line, $(tr_line).data('embed_fields')[b], b, temp_data, server_chaining);
								}
								else{
									$(tr_line).find('[data-field-name="' + b + '"]').trigger('change', [temp_data, server_chaining]);
								}
							}, 0);
							// }
						}
					}));
				}));
			}
		}
	},
	"ExecuteServerSideFormula": function(tr_line, temp_data){
		// return;
		var self = this;
		if(Object.keys(EmbedFields.ServerFormulas).length > 0){
			var json_params = {
				"field_models": $(EmbedFields.tr_line).data()['field_models'],
				"lookup_fields":EmbedFields.ServerFormulas,
				"prefix": "embed_"
			}
			console.log("json_params", json_params);
			$.post("/ajax/ajaxformula_prototype",json_params,function(result_echoes){
				// alert("server");r
				try{
					console.log("eho", result_echoes);
					var server_result = JSON.parse(result_echoes);
					var element_trigger_collector = $();
					AsyncLoop(server_result, function(a, b){
						console.log("tr_line", EmbedFields.tr_line);
						if($.type(b['default_computed']) != "undefined"){
							var key = "embed_" + a;
							if($.type(b['default_computed']) == "undefined"){
								b['default_computed'] = "";
							}
							else if($.type(b['default_computed']) == "null"){
								b['default_computed'] = "";
							}
							if($(self.tr_line).find('[data-field-name="' + a + '"]').length < 1){
								$(self.tr_line).data('field_models')[key] = b['default_computed'];
								console.log("embedfields", $(self.tr_line).data('embed_fields')[a], "a",a, "embed_data", $(self.tr_line).data('embed_fields'));
								$(self.tr_line).data('embed_fields')[a]['value'] = b['default_computed'];
							}
							else{
								$(self.tr_line).data('field_models')[key] = b['default_computed'];
								$(self.tr_line).data('embed_fields')[a]['value'] = b['default_computed'];
								$(self.tr_line).find('[data-field-name="' + a + '"]').val(b['default_computed']);
							}
							
						}
						if($.type(b['readonly']) != "undefined"){
							var field = $(self.tr_line).find('[data-field-name="' + a + '"]');
							if(b['readonly'] == true){
								field.attr('readonly', 'readonly');
							}
							else{
								field.removeAttr('readonly');
							}
						}
						if($.type(b['visibility']) != "undefined"){
							var field = $(self.tr_line).find('[data-field-name="' + a + '"]');
							if(b['visibility'] == true){
								field.addClass('display');
							}
							else{
								field.removeClass('display');
							}
						}
						var keys = Object.keys(server_result);
						if($(self.tr_line).find('[data-field-name="' + a + '"]').length < 1){
							var orig_a = a;
							var formula_binding = $(self.tr_line).data('embed_fields')[a]['formula_data_binding'];
							AsyncLoop(formula_binding, function(a, b){
								if(a == "root_formula_default_computed"){
									for(i in b){
										if($.inArray(i, keys) < 0){
											temp_data = temp_data.concat(orig_a);
											EmbedFields.NonExistingFieldTrigger(self.tr_line, $(self.tr_line).data('embed_fields')[orig_a], orig_a, temp_data, server_chaining);
										}
										else{
											temp_data = temp_data.concat(orig_a);
										}
									}
								}
								else if(a == "root_formula_readonly"){
									for(i in b){
										if($.inArray(i, keys) < 0){
											temp_data = temp_data.concat(orig_a);
											EmbedFields.NonExistingFieldTrigger(self.tr_line, $(self.tr_line).data('embed_fields')[orig_a], orig_a, temp_data, server_chaining);
										}
										else{
											temp_data = temp_data.concat(orig_a);
										}
									}
								}
								else{
									for(i in b){
										if($.inArray(i, keys) < 0){
											temp_data = temp_data.concat(orig_a);
											EmbedFields.NonExistingFieldTrigger(self.tr_line, $(self.tr_line).data('embed_fields')[orig_a], orig_a, temp_data, server_chaining);
										}
										else{
											temp_data = temp_data.concat(orig_a);
										}
									}
								}
							});
						}
						else{
							var orig_a = a;
							var formula_binding = $(self.tr_line).data('embed_fields')[a]['formula_data_binding'];
							AsyncLoop(formula_binding, function(a, b){
								if(a == "root_formula_default_computed"){
									for(i in b){
										if($.inArray(i, keys) < 0){
											temp_data = temp_data.concat(orig_a);
											$(self.tr_line).find('[data-field-name="' + orig_a + '"]').trigger('change', [temp_data, true]);
										}
										else{
											temp_data = temp_data.concat(orig_a);
										}
									}
								}
								else if(a == "root_formula_readonly"){
									for(i in b){
										if($.inArray(i, keys) < 0){
											temp_data = temp_data.concat(orig_a);
											$(self.tr_line).find('[data-field-name="' + orig_a + '"]').trigger('change', [temp_data, true]);
										}
										else{
											temp_data = temp_data.concat(orig_a);
										}
									}
								}
								else{
									for(i in b){
										if($.inArray(i, keys) < 0){
											temp_data = temp_data.concat(orig_a);
											$(self.tr_line).find('[data-field-name="' + orig_a + '"]').trigger('change', [temp_data, true]);
										}
										else{
											temp_data = temp_data.concat(orig_a);
										}
									}
								}
							});
						}
					}, function(){
						EmbedFields.ServerFormulas = {};
					});
				}
				catch(e){
					console.log(" parse error", e);
				}
			});
		}
	},
	"embed_server_timer": undefined,
	"InitServerFormulas": function(is_executed, tr_line, temp_data){
		if(is_executed == false){
			if($.type(EmbedFields.embed_server_timer) != "undefined"){
				EmbedFields.embed_server_timer.resetTimeoutV2(500);
			}
		}
		if($.type(EmbedFields.embed_server_timer) == "undefined"){
			EmbedFields.embed_server_timer = new SetMyTimer(function(){
				EmbedFields.ExecuteServerSideFormula(tr_line, temp_data);
			}, 500);
		}
	},
	"GetKeywords": function(tbl_values, tr_line, type){
		for(i in EmbedFields.formula_keywords){
			if(EmbedFields.formula_keywords[i] == 'Requestor'){
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = tbl_values['requestor_keyname']||$('[name="Requestor"]').val();
			}
			else if(EmbedFields.formula_keywords[i] == 'Today'){
				var now = ThisDate($('body').data().getServerTimeData).formatDate('Y-m-d');
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = now.toString("yy-mm-dd");
			}
			else if(EmbedFields.formula_keywords[i] == 'Processor'){
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = tbl_values['processor_keyname']||$('[name="Processor"]').val();
			}
			else if(EmbedFields.formula_keywords[i] == 'CurrentUser'){
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = $('[name="CurrentUser"]').attr("current-user-name")||"";
			}
			else if(EmbedFields.formula_keywords[i] == 'TrackNo'){
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = tbl_values['TrackNo'];
			}
			else if(EmbedFields.formula_keywords[i] == 'Now'){
				var now = ThisDate($('body').data().getServerTimeData).formatDate('Y-m-d H:i');
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = now.toString();
			}
			else if(EmbedFields.formula_keywords[i] == "Department"){
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = $('[data-field-name="Department_Name"]').val()||"";
			}
			else if(EmbedFields.formula_keywords[i] == "RequestID"){
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = tbl_values['ID']||"";
			}
			else if(EmbedFields.formula_keywords[i] == "LastAction"){
				if(tbl_values['LastAction'] == "null"){
					tbl_values['LastAction'] = null;
				}
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = tbl_values['LastAction'];
			}
			else if(EmbedFields.formula_keywords[i] == 'TimeStamp'){
				try{
					var old_date = $('body').data().getServerTimeData,
					dateStr = old_date.getDate() + "-" + old_date.getMonth() + "-" + old_date.getFullYear() + " " + old_date.getHours() + ":" + old_date.getMinutes(),
					dateParts = dateStr.split(' '),
					timeParts = dateParts[1].split(':'),
					date;
					dateParts = dateParts[0].split('-');
					date = new Date(dateParts[2], parseInt(dateParts[1], 10) - 1, dateParts[0], timeParts[0], timeParts[1]);
					$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = date.getTime();
				}
				catch(e){

				}
			}
			else if(EmbedFields.formula_keywords[i] == 'Status'){
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = tbl_values['Status']||"";
			}
			else if(EmbedFields.formula_keywords[i] == 'other_details'){
				for(i in tbl_values['other_details']){
					if(i == "buttonStatus"){
						console.log(tbl_values['other_details'][i]);
						var getBtnStatus = JSON.parse(tbl_values['other_details'][i]);
						for(btn_name in getBtnStatus){
							
							if(type == true){
								$(tr_line).data()['field_models']['Node_ID'] = getBtnStatus[btn_name]['child_id'];
							}
							else{
								$(tr_line).data()['field_models']['Node_ID'] = "Save";
							}
							
						}
						$(tr_line).data()['field_models']['LastAction'] = tbl_values['other_details'][i];
					}
					else{
						// if(i == "fieldEnabled"){
						// 	$(tr_line).data()['field_models'][i] = JSON.parse(tbl_values['other_details'][i]);
						// }
						// else if(i == "fieldRequired"){
						// 	$(tr_line).data()['field_models'][i] = JSON.parse(tbl_values['other_details'][i]);
						// }
						// else if(i == "fieldHIddenValues"){
						// 	$(tr_line).data()['field_models'][i] = JSON.parse(tbl_values['other_details'][i]);
						// }
						// else{
							$(tr_line).data()['field_models'][i] = tbl_values['other_details'][i]
						// }
					}
				}
				if(type != true){
					$(tr_line).data()['field_models']['Node_ID'] = "Save";
				}
			}
			else{
				$(tr_line).data()['field_models'][EmbedFields.formula_keywords[i]] = $('[data-field-name = '+ EmbedFields.formula_keywords[i] + ']').val();
			}
		}
	},
	"fieldValueFixes":function(element, tr_line,formula_trigger){
		if(element){
			var self = $(element);
			// $(tr_line).find('.embed_getFields').each(function(e){
			if($.trim(self.val()) == ""){
				return false;
			}

			if($(element).attr('equivalent-fld-input-type') == "Currency"){
				self.val(self.val().replace(/,/g, ''));
				if(Number(self.val()).currencyFormat().indexOf('NaN') > -1 && formula_trigger==0){
					var notif_field = NotifyMe($(element), "Not a Number!");
					// var top = (($(element).offset()['top'] / 2) - $(element).height());
					// var left = ($(element).offset()['left'] + $(element).width());
					// notif_field.css({"top": top + "px", "left": left + "px"});
					EmbedFields.error_counter++;
					return false;
				}
				else{
					$(element).removeClass('NM-redborder');
				}
				
				// console.log("self val", Number(self.val()).currencyFormat());
				if(Number(self.val()).currencyFormat().indexOf('NaN') > -1){
					console.log("field", self.attr('data-field-name'));
					var notif_field = NotifyMe($(element), "Not a Number!");
					// var top = (($(element).offset()['top'] / 2) - $(element).height());
					// var left = ($(element).offset()['left'] + $(element).width());
					// notif_field.css({"top": top + "px", "left": left + "px"});
					$(element).val("Not a valid computation");
					EmbedFields.error_counter++;
				}
				else{
					if($(element).val() != ""){
						$(element).val(GetRound(Number(self.val()), 2).currencyFormat());
						$(element).removeClass('NM-redborder');
					}
					// 
				}
				// $(element).val(Number(self.val()).currencyFormat());
				// Not a valid computation
			}
			else if($(element).attr('equivalent-fld-input-type') == "Number"){
				if(isNaN(self.val()) == true && formula_trigger==0){
					var notif_field = NotifyMe($(element), "Not a Number!");
					// var top = (($(element).offset()['top'] / 2) - $(element).height());
					// var left = ($(element).offset()['left'] + $(element).width());
					// notif_field.css({"top": top + "px", "left": left + "px"});
					// $(element).val("Not a Number");
					EmbedFields.error_counter++;
					return false;
				}
				else{
					$(element).removeClass('NM-redborder');
				}

				// var self = $(element);
				if(isNaN(self.val()) == true){
					var notif_field = NotifyMe($(element), "Not a Number!");
					// var top = (($(element).offset()['top'] / 2) - $(element).height());
					// var left = ($(element).offset()['left'] + $(element).width());
					// notif_field.css({"top": top + "px", "left": left + "px"});
					// $(element).val("Not a Number");
					EmbedFields.error_counter++;
				}
				else{
					if($(element).val() != ""){
						$(element).val(Number(self.val()));
						$(element).removeClass('NM-redborder');
					}
					// $(element).val(Number(self.val()));
				}
			}
		}
		else{
			$(tr_line).find('.embed_getFields').each(function(e){
			var element = $(this);
			if($(element).attr('equivalent-fld-input-type') == "Currency"){
				var self = $(element);
				// if(Number(self.val()).currencyFormat().indexOf('NaN') > -1 && formula_trigger==0){
				// 	return false;
				// }
				self.val(self.val().replace(/,/g, ''));
				console.log("currency", isNaN(Number(self.val())) == true, "field",  self.attr('data-field-name'));
				if(isNaN(Number(self.val())) == true){
					var notif_field = NotifyMe($(element), "Not a Number!");
					// var top = $(element).position()['top'] + $(element).closest('.setOBJ').position()['top'];//((field.offset()['top'] / 2) - field.height());
					// var left = ($(element).position()['left'] + $(element).width()+20);
					// notif_field.css({"top": top + "px","left": left + "px"});
					// $(element).val("Not a valid computation");
					EmbedFields.error_counter++;
				}
				else{
					// $(element).val(Number(self.val()).currencyFormat());
					if($(element).val() != ""){
						$(element).val(GetRound(Number(self.val()), 2).currencyFormat());
						$(element).removeClass('NM-redborder');
					}
					// $(element).removeClass('NM-redborder');
				}
				// $(element).val(Number(self.val()).currencyFormat());
				// Not a valid computation
			}
			else if($(element).attr('equivalent-fld-input-type') == "Number"){
				var self = $(element);
				// if(isNaN(self.val()) == true && formula_trigger==0){
				// 	return false;
				// }
				// // $(element).val(Number(self.val()));
				// console.log("Number", isNaN(self.val()) == true);
				if(isNaN(self.val()) == true){
					var notif_field = NotifyMe($(element), "Not a Number!");
					// var top = (($(element).offset()['top'] / 2) - $(element).height());
					// var left = ($(element).offset()['left'] + $(element).width());
					// notif_field.css({"top": top + "px", "left": left + "px"});
					// $(element).val("Not a Number");
					EmbedFields.error_counter++;
				}
				else{
					if($(element).val() != ""){
						$(element).val(Number(self.val()));
						$(element).removeClass('NM-redborder');
					}
				}
			}
			});
		}
	},
	"embedInputValidation": function(tr_line, record_id, embed_obj_id, form_id){
		var embed_fields = $(tr_line).data('embed_fields');
		console.log("embed_Fields", embed_fields);
		var keys = Object.keys(embed_fields);
		keys = keys.map(function(value){ return 'embed_' + value});
		for(i in embed_fields){
			if(embed_fields[i]['input_validation_formula'] != "" && $.type(embed_fields[i]['input_validation_formula']) != "undefined"){
				// console.log("input validation:", EmbedFields.embedFormulaParser(keys, embed_fields[i]['input_validation_formula'], '.tr_' + record_id, embed_obj_id));
				console.log("asd embed_obj_id",embed_obj_id);
				var parsed_formula;
				if(typeof form_id != "undefined"){
					form_id
					parsed_formula = EmbedFields.embedFormulaParser(keys, embed_fields[i]['input_validation_formula'], '.tr_' + form_id + '_' + record_id, embed_obj_id)['parsed_formula'];
				}
				else{
					parsed_formula = EmbedFields.embedFormulaParser(keys, embed_fields[i]['input_validation_formula'], '.tr_' + record_id, embed_obj_id)['parsed_formula'];
					
				}
				var input_validation_message = embed_fields[i]['input_validation_message'];
				console.log(parsed_formula);
				console.log("parsed_formula",eval(parsed_formula));
				if(eval(parsed_formula) == false){
					var field = $(tr_line).find('[data-field-name="' + i + '"]');
					if(field.length > 0){
						var notif_field = NotifyMe(field, input_validation_message);
						EmbedFields.error_counter++;
					}
					// console.log(notif_field.position()['top'], field.position()['top']);
					// var top = field.position()['top'] + field.closest('.setOBJ').position()['top'];//((field.offset()['top'] / 2) - field.height());
					// var left = (field.position()['left'] + field.width()+20);
					// notif_field.css({"top": top + "px","left": left + "px"});
				}
			}
			if(embed_fields[i]['field_type'] == "dateTime"){
				var field = $(tr_line).find('[data-field-name="' + i + '"]');
				if(field.length > 0){
					if(field.val() != ""){
						if(ThisDate(field.val()) == "Invalid Date"){
							var notif_field = NotifyMe(field, "The value you entered is not a datetime");
							EmbedFields.error_counter++;
						}
					}
				}
			}
			if(embed_fields[i]['field_type'] == "time"){
				var field = $(tr_line).find('[data-field-name="' + i + '"]');
				if(field.length > 0){
					if(field.val() != ""){
						if(ThisDate(FormatDate(new Date(), "Y-m-d") + ' ' + field.val()) == "Invalid Date"){
							var notif_field = NotifyMe(field, "The value you entered is not a time");
							EmbedFields.error_counter++;
						}
					}
				}
			}
			if(embed_fields[i]['field_type'] == "datepicker"){
				var field = $(tr_line).find('[data-field-name="' + i + '"]');
				if(field.length > 0){
					if(field.val() != ""){
						if(ThisDate(field.val()) == "Invalid Date"){
							var notif_field = NotifyMe(field, "The value you entered is not a date");
							EmbedFields.error_counter++;
						}
					}
				}
			}
		}
		var required_fields = JSON.parse($(tr_line).data('field_models')['fieldRequired']||"[]");
		console.log("required", required_fields);
		for(i in required_fields){
			// console.log("required field...", required_fields[i]);
			if($(tr_line).find('[data-field-name="' + required_fields[i] + '"]').length > 0){
				if($(tr_line).find('[data-field-name="' + required_fields[i] + '"]').is('[type="radio"]') || $(tr_line).find('[data-field-name="' + required_fields[i] + '"]').is('[type="checkbox"]')){
					if($(tr_line).find('[data-field-name="' + required_fields[i] + '"]').filter(':checked').length <= 0){
						var field = $(tr_line).find('[data-field-name="' + required_fields[i] + '"]');
						console.log("field", field);
						var notif_field = NotifyMe(field, "Required!");
						console.log(notif_field.position()['top'], field.position()['top']);
						// var top = field.position()['top'] + field.closest('.setOBJ').position()['top'];//((field.offset()['top'] / 2) - field.height());
						// var left = (field.position()['left'] + field.width()+20);
						// notif_field.css({"top": top + "px","left": left + "px"});
						EmbedFields.error_counter++;
					}
				}
				else if($(tr_line).find('[data-field-name="' + required_fields[i] + '"]').is('select[multiple]')){
					if($('[data-field-name="selectmany"]').children('option:selected').length <= 0){
						var field = $(tr_line).find('[data-field-name="' + required_fields[i] + '"]');
						console.log("field", field);
						var notif_field = NotifyMe(field, "Required!");
						console.log(notif_field.position()['top'], field.position()['top']);
						// var top = field.position()['top'] + field.closest('.setOBJ').position()['top'];//((field.offset()['top'] / 2) - field.height());
						// var left = (field.position()['left'] + field.width()+20);
						// notif_field.css({"top": top + "px","left": left + "px"});
						EmbedFields.error_counter++;
					}
				}
				else{
					if($(tr_line).find('[data-field-name="' + required_fields[i] + '"]').val().length <= 0){
						var field = $(tr_line).find('[data-field-name="' + required_fields[i] + '"]');
						console.log("field", field);
						var notif_field = NotifyMe(field, "Required!");
						console.log(notif_field.position()['top'], field.position()['top']);
						// var top = field.position()['top'] + field.closest('.setOBJ').position()['top'];//((field.offset()['top'] / 2) - field.height());
						// var left = (field.position()['left'] + field.width()+20);
						// notif_field.css({"top": top + "px","left": left + "px"});
						EmbedFields.error_counter++;
					}
				}
				
			}
		}


	},
	"embed_fields": {

	},
	"field_models": {

	},
	"ServerFormulas": {

	},
	"error_counter": 0,
	"tmp_data": [],
	"formula_keywords": ["Status", "Requestor", "RequestID", "CurrentUser", "TrackNo", "Department", "Processor","Today","Now","TimeStamp", "LastAction", "other_details"],
	"formula_keywordsV2": ["@Status", "@Requestor", "@RequestID", "@CurrentUser", "@TrackNo", "@Department", "@Processor","@Today","@Now","@TimeStamp", "@LastAction"],
	"server_functions": ["Head", "AssistantHead", "UserInfo", "Lookup", "RecordCount", "LookupAuthInfo", "LookupWhere", "Total", "LookupGetMax", "LookupGetMin", "LookupSDev", "LookupAVG", "LookupCountIf"],
	"tr_line": ""
}

$(document).ready(function(){
	if(location.pathname.indexOf('workspace') > -1){
		EmbedFields.init();
	}	
});

var EmbedFormEvents = {
	"onload_xhr": null,
	"presave_xhr": null,
	"postsave_xhr": null,
	"onchange_xhr": null,
	"init": function(class_name, form_id, record_id, embedded_view_selector, event_type, type, mode){
		var self = this;
		var json_params = {
			"form_id": form_id,
			"record_id": record_id
		}
		var  index = $(class_name).attr('record-id');
		var embed_obj_id = $(embedded_view_selector).closest('.setOBJ').attr('data-object-id');
		$.post('/ajax/get_fields_data', json_params, function(echo){
			console.log("get_fields_data echo", echo);
			var embed_json_model = {};
			var form_json = JSON.parse(echo);
			// console.log("return", form_json);
			var tbl_values = form_json['tbl_values'][0];
			console.log("tbl_values123", tbl_values);
			form_json = JSON.parse(form_json[0]['form_json']);
			console.log("form_json", form_json);
			real_form_json = form_json['form_json'];
			var real_fields_data = form_json['fields_data'];
			console.log("class_name", $(class_name));
			// if(mode == 'insert'){ 
			$(class_name).data()['field_models'] = {};
			var embed_json_model = EmbedFields.getEmbedFieldNames(real_form_json, real_fields_data, tbl_values, $(class_name));
			var keys = Object.keys(embed_json_model);
			embed_json_model = EmbedFields.addPrefixToFields(keys, embed_json_model, 'embed_');
			$(class_name).data()['embed_fields'] = embed_json_model;
			EmbedFields.GetKeywords(tbl_values, $(class_name), type);
			self.setFieldModels('.tr_' + index, real_fields_data, mode);
			self.addUnsupportedFormula('.tr_' + index, embed_json_model, embedded_view_selector.closest('.setOBJ').attr('data-object-id'));
			var filtered_fields = self.getOnchangeFields(real_form_json, real_fields_data);
			// if($(class_name).parents('.embed-view-container').attr('stringified-json-data-send')){
			// 	embeded_view.distributeDataSending(JSON.parse($(class_name).parents('.embed-view-container').attr('stringified-json-data-send')), $(class_name));
			// }
			EmbedFormEvents.distributeDataSending(JSON.parse($(embedded_view_selector).attr('stringified-json-data-send')), class_name);
			self.EmbedFormulaEventController(embedded_view_selector, event_type, index, class_name);
			self.EmbedFormulaEventController(embedded_view_selector, 'onchange', index, class_name, filtered_fields);
			// self.updateFieldModel(class_name);
			// }
		});
	},
	"EmbedFormulaEventDistribution":function(formula_event_values, row_line){
		var getSequenceData = formula_event_values['sequence_data'];
		for(index in getSequenceData){
			var row = $(row_line);
			var field = row.find('[data-field-name="' + getSequenceData[index]['field_name'] + '"]');
			var distribution_type = getSequenceData[index]['distribution_type'];
			console.log("field", field);
			switch(distribution_type){
				case "computed_value":
					if(field.length > 0){
						if(field.is('select[multiple]') ){
						    var temp_val = getSequenceData[index]['value'];
						    if($.trim(temp_val) != ""){
						    	row.data('field_models')['embed_' + getSequenceData[index]['field_name']] = temp_val;
						        temp_val = temp_val.split("|^|");
						        for( var ctr = 0 ; ctr < temp_val.length ; ctr++ ){
						            field.children('option[value="'+temp_val[ctr]+'"]').prop("checked",true);
						        }
						    }
						}else if(field.is('input[type="checkbox"]') ){
						    var temp_val = getSequenceData[index]['value'];
						    if($.trim(temp_val) != ""){
						    	row.data('field_models')['embed_' + getSequenceData[index]['field_name']] = temp_val;
						        temp_val = temp_val.split("|^|");
						        for( var ctr = 0 ; ctr < temp_val.length ; ctr++ ){
						            field.filter('[value="'+temp_val[ctr]+'"]').prop("checked",true);
						        }
						    }
						}else if(field.is('input[type="radio"]') ){
							row.data('field_models')['embed_' + getSequenceData[index]['field_name']] = getSequenceData[index]['value']||"";
						    field.filter('[value="'+getSequenceData[index]['value']+'"]').prop("checked",true);
						}else{
							if(field.attr('equivalent-fld-input-type') == "Currency"){
								row.data('field_models')['embed_' + getSequenceData[index]['field_name']] = (Number(getSequenceData[index]['value'])||Number(""));
								field.val((Number(getSequenceData[index]['value'])||Number("")).currencyFormat());
							}
							else{
								row.data('field_models')['embed_' + getSequenceData[index]['field_name']] = getSequenceData[index]['value']||"";
								field.val(getSequenceData[index]['value']||"");
							}
						    
						}
					}
					else{
						console.log("field name", 'embed_' + getSequenceData[index]['field_name'], "value", getSequenceData[index]['value']||"");
						row.data('field_models')['embed_' + getSequenceData[index]['field_name']] = getSequenceData[index]['value']||"";
					}
					break;
				case "change_list_computed_value":
					if(field.is('select')){
						var values = getSequenceData[index]['value'];
						if($.type(values) == 'array'){
							var new_select_values = (values||[]).filter(Boolean);
							field.html("");
							for(i in new_select_values){
							    field.append('<option value="' + new_select_values[i] + '">' + new_select_values[i] + '</option>');
							}
						}
					}
					break;
				case "readonly":

					break;
			}
		}
	},
	"EmbedFormulaEventController":function(embedded_view_selector, event_type, index, class_name, filtered_fields){
		var embed = $(embedded_view_selector);
		var form_id = embed.attr('embed-source-form-val-id');
		console.log("embed table", embed.find('table'));
		var form_events = JSON.parse(embed.find('table').attr('form-events'));
		if(event_type == "onload"){
			if($.type(this.onload_xhr) != "null"){
				if($.type(this.onload_xhr.abort) == "function"){
					this.onload_xhr.abort();
				}
			}
			this.onload_xhr = this.EmbedFormulaEventAjax(form_events['onload'], event_type, class_name, "embed_", index);
		}
		else if(event_type == "presave"){
			if($.type(this.onload_xhr) != "null"){
				if($.type(this.onload_xhr.abort) == "function"){
					this.onload_xhr.abort();
				}
			}
			this.onload_xhr = this.EmbedFormulaEventAjax(form_events['presave'], event_type, class_name, "embed_", index);
		}
		else if(event_type == "postsave"){
			if($.type(this.onload_xhr) != "null"){
				if($.type(this.onload_xhr.abort) == "function"){
					this.onload_xhr.abort();
				}
			}
			this.onload_xhr = this.EmbedFormulaEventAjax(form_events['postsave'], event_type, class_name, "embed_", index);
		}
		else if(event_type == "onchange"){
			var self = this;
			if(filtered_fields){
				for(i in filtered_fields){
					console.log("filtered_fields", filtered_fields[i]);
					$(class_name).find('[data-field-name="' + i + '"]').data('onchange_formula_event', filtered_fields[i]['onchange-formula-event']);
					$(class_name).find('[data-field-name="' + i + '"]').on('change.embedAlias', function(){
						var field_name = $(this).attr('data-field-name');
						var onchange_formula = $(this).data('onchange_formula_event');
						if($.type(self.onchange_xhr) != "null"){
							if($.type(self.onchange_xhr.abort) == "function"){
								self.onchange_xhr.abort();
							}
						}
						console.log("find me.....");
						self.onchange_xhr = self.EmbedFormulaEventAjax(onchange_formula, event_type, class_name, "embed_", index, field_name);
					})
				}
			}
		}
	},
	"EmbedFormulaEventAjax": function(formula, event_type, row_line, embed, prefix, index, formula_list_onchange_auto_excluded_fieldname){
		var embed = $(row_line).parents('.embed-view-container');
		var set_formula = formula;
		var field_models = {};
		var user_fields = {};
		var form_events = JSON.parse(embed.find('table').attr('form-events'));
		var field_associated_data = {};
		$(row_line).find('.embed_getFields').each(function(){
			var self = $(this);
			var self_name = $(this).attr('data-field-name');
			field_associated_data[self_name] = {};
			field_associated_data[self_name]["field_object_type"] = embed.find('[data-form-fld-name="' + self_name + '"]').attr("data-field-object-type");
			field_associated_data[self_name]["field_input_type"] = self.attr("equivalent-fld-input-type")||"Text";
			if(self.is('select[multiple]')){
				// user_fields[self_name] = self.children('option:selected').map(function(){ return $(this).val(); }).get();
				// field_models[self_name] = self.children('option:selected').map(function(){ return $(this).val(); }).get();
				$(row_line).data('field_models')['embed_' + self_name] = self.children('option:selected').map(function(){ return $(this).val(); }).get().join('|^|');
			}
			else if(self.is('[type="radio"]')){
				// user_fields[self_name] = self.filter(':checked').val();
				// field_models[self_name] = self.filter(':checked').val();
				var self_name = self.attr('data-field-name');
				// alert($(row_line).find('[data-field-name="' + self_name + '"]:checked').val());
				// self.filter(':checked').val());
				$(row_line).data('field_models')['embed_' + self_name] = $(row_line).find('[data-field-name="' + self_name + '"]:checked').val();
			}
			else if(self.is('[type="checkbox"]')){
				// user_fields[self_name] = self.filter(':checked').map(function(){ return $.trim($(this).val()); }).get().filter(Boolean);
				// field_models[self_name] = self.children(':checked').map(function(){ return $.trim($(this).val()); }).get().filter(Boolean);
				$(row_line).data('field_models')['embed_' + self_name] = $(row_line).find('[data-field-name="' + self.attr('data-field-name') + '"]').filter(':checked').map(function(){ return $.trim($(this).val()); }).get().filter(Boolean).join('|^|');
			}
			else{
				if(self.is('[equivalent-fld-input-type="Currency"]')){
				    // user_fields[self_name] = Number(self.val().replace(/,/g,""));
				    // field_models[self_name] = Number(self.val().replace(/,/g,""));
				    $(row_line).data('field_models')['embed_' + self_name] = self.val().replace(/,/g,"");
				}else{
				    // user_fields[self_name] = self.val();
				    // field_models[self_name] = self.val();
				    $(row_line).data('field_models')['embed_' + self_name] = self.val();
				}
			}
		});
		var f_m = $(row_line).data('field_models');
		var f_d = $('body').data('embed_row_data')[embed.closest('.setOBJ').attr('data-object-id')][embed.closest('.setOBJ').attr('data-object-id') + '_' + $(row_line).attr('record-id')]['fields_data'];
		for(i in f_m){
			var field_name = i;
			var fm_field_name = i;
			if(field_name.indexOf('embed_') > -1){
				field_name = field_name.replace(/embed_/g, '');
			}
			if($.type(f_d[field_name]) != 'undefined'){
				if(f_d[field_name]['type'] == "selectMany"){
					field_models[field_name] = f_m[fm_field_name].split('|^|');
					user_fields[field_name] = f_m[fm_field_name].split('|^|');
				}
				else if(f_d[field_name]['type'] == "checkbox"){
					field_models[field_name] = f_m[fm_field_name].split('|^|');
					user_fields[field_name] = f_m[fm_field_name].split('|^|');
				}
				else{
					if(f_d[field_name]['field_input_type'] == 'Currency'){
						console.log(f_m[fm_field_name], f_m, fm_field_name);
						try{
							field_models[field_name] = f_m[fm_field_name].replace(/,/g, '');
							user_fields[field_name] = f_m[fm_field_name].replace(/,/g, '');
						}
						catch(e){
							field_models[field_name] = f_m[fm_field_name];
							user_fields[field_name] = f_m[fm_field_name];
						}
						

					}
					else{
						field_models[field_name] = f_m[fm_field_name];
						user_fields[field_name] = f_m[fm_field_name];
					}
				}
			}
		}
		var formDataSource = JSON.parse(form_events['form_variables']||"[]");
		var keywords = EmbedFields.formula_keywords;
		var row_field_models = $(row_line).data('field_models');
		var reserve_key = {};
		for(i in row_field_models){
			if($.inArray(i, keywords) > -1){
				reserve_key[i] = row_field_models[i];
				field_models[i] = row_field_models[i];
			}
		}
		field_models["TimeStamp"] = $('body').data("date_page_visited");
		var temp_formula_event = form_events['ComputedFormulaEventList'];
		console.log("field_models", field_models);
		return $.ajax({
			type: "POST",
			url: "/ajax/FormulaEventAjax",
			data: {
			    "field_associated_data":field_associated_data,
			    "formula_list_onchange_auto_excluded_fieldname":formula_list_onchange_auto_excluded_fieldname,
			    "form_variable_data_source":formDataSource,
			    "form_reserve_keys":reserve_key,
			    "form_user_fields":user_fields,
			    "computed_formula_event_list":temp_formula_event,
			    "formula":set_formula,
			    "field_model":JSON.stringify(field_models)
			},
			success: function(data_res){
	            console.log("FORMULA EVENT", data_res);
	            try{
	                var user_implement_given_format = JSON.parse(data_res);
	                // console.log("user_implement_given_format",user_implement_given_format);
	                
	                EmbedFormEvents.EmbedFormulaEventDistribution(user_implement_given_format, row_line);
	            }catch(e){
	                console.log("error");
	            }
	            // ui.unblock();
	        }
		});
	},
	"addUnsupportedFormula":function(row_line, embed_fields, embed_object_key){//this function adds visibility and readonly
		var row = $(row_line);
		var keys = Object.keys(row.data('field_models'));
		for(i in embed_fields){
			console.log("embed_fields", embed_fields[i]);
			// visibility
			{
				if($.type(embed_fields[i]['visibility']) != "undefined" && embed_fields[i]['visibility'] != ""){
					var visibility_formula = embed_fields[i]['visibility'];
					var parsed_formula = EmbedFields.embedFormulaParser(keys, visibility_formula, row_line, embed_object_key);
					if(parsed_formula['parsed_formula'] != ""){
						row.find('[data-field-name="' + i + '"]').data('visibility', parsed_formula['parsed_formula']);
						if(eval(parsed_formula['parsed_formula']) == true){
							row.find('[data-field-name="' + i + '"]').removeClass('display');
						}
						else{
							row.find('[data-field-name="' + i + '"]').addClass('display');
						}
						//setup the event
						$(row_line).find('[data-field-name="' + i + '"]').on('change', function(e){
							if(eval($(this).data('visibility')) == true){
								$(this).removeClass('display');
							}
							else{
								$(this).addClass('display');

							}
						});
					}
				}
			}
			// readonly
			{
				if($.type(embed_fields[i]['readonly']) != "undefined" && embed_fields[i]['readonly'] != ""){
					var formula = embed_fields[i]['readonly'];
					var parsed_formula = EmbedFields.embedFormulaParser(keys, formula, row_line, embed_object_key);
					if(parsed_formula['parsed_formula'] != ""){
						row.find('[data-field-name="' + i + '"]').data('readonly', parsed_formula['parsed_formula']);
						console.log("parsed formula", parsed_formula['parsed_formula']);
						if(eval(parsed_formula['parsed_formula']) == true){
							row.find('[data-field-name="' + i + '"]').attr('readonly', true).css('border', 'none');
						}
						else{
							row.find('[data-field-name="' + i + '"]').attr('readonly', false);
							// if(row.find('[data-field-name="' + i + '"]').css('border') != '1px solid'){
								row.find('[data-field-name="' + i + '"]').css('border', '');
							}
						// }
						//setup the event
						$(row_line).find('[data-field-name="' + i + '"]').on('change', function(e){
							if(eval($(this).data('readonly')) == true){
								$(this).attr('readonly', true);
								$(this).css('border', 'none');
							}
							else{
								$(this).attr('readonly', false);
								// if($(this).css('border') != '1px solid'){
									$(this).css('border', '');
								// }
							}
						});
					}
				}
			}
		}
	},
	"readonlyFormula":function(row_line, embed_fields){
		// var row = $(row_line);
		// var keys = Object.keys(row.data('field_models'));
		// for(i in embed_fields){
		// 	if(embed_fields[i]['ReadOnlyFormula']){
		// 		var formula = embed_fields[i]['ReadOnlyFormula'];
		// 		var parsed_formula = EmbedFields.embedFormulaParser(keys, formula, row_line);
		// 		if(parsed_formula['parsed_formula'] != ""){
		// 			console.log("parsed_formula", eval(parsed_formula['parsed_formula']));
		// 			row.find('[data-field-name="' + i + '"]').data('readonly', parsed_formula['parsed_formula']);
		// 			if(eval(parsed_formula['parsed_formula']) == true){
		// 				row.find('[data-field-name="' + i + '"]').attr('readonly', true);
		// 			}
		// 			else{
		// 				row.find('[data-field-name="' + i + '"]').attr('readonly', false);
		// 			}
		// 			//setup the event
		// 			$(row_line).find('[data-field-name="' + i + '"]').on('change', function(e){
		// 				if(eval($(this).data('readonly')) == true){
		// 					$(this).attr('readonly', true);
		// 				}
		// 				else{
		// 					$(this).attr('readonly', false);

		// 				}
		// 			});
		// 		}
		// 	}
		// }
	},
	"getOnchangeFields":function(form_json, fields_data){
		var return_data = {};
		for(i in fields_data){
			var obj_id = fields_data[i]['object_id'];
			if(form_json[obj_id]['onchange-formula-event']){
				return_data[fields_data[i]['data_field_name']] = form_json[obj_id];
			}
		}
		console.log("onchange_fields",return_data);
		return return_data;
	},
	"setFieldModels":function(row_line, fields_data, mode){
		var row = $(row_line);
		console.log("moooodeeeeeee", mode);
		// if(mode == "insert"){
			for(i in fields_data){
				if(fields_data[i]['data_field_input_type'] == "Currency"){
					if($.type(row.data('field_models')['embed_' + fields_data[i]['data_field_name']]) == "undefined"){
						if(row.find('[data-field-name="' + fields_data[i]['data_field_name'] + '"]')){
							var value = row.find('[data-field-name="' + fields_data[i]['data_field_name'] + '"]').val() || "".replace(/,/g, '');
							row.data('field_models')['embed_' + fields_data[i]['data_field_name']] = Number(value).currencyFormat();
						}
						else{
							row.data('field_models')['embed_' + fields_data[i]['data_field_name']] = Number("").currencyFormat();
						}
						// row.data('field_models')['embed_' + fields_data[i]['data_field_name']] = Number("").currencyFormat();
					}
				}
				else if(fields_data[i]['data_field_input_type'] == "Number"){
					if($.type(row.data('field_models')['embed_' + fields_data[i]['data_field_name']]) == "undefined"){
						if(row.find('[data-field-name="' + fields_data[i]['data_field_name'] + '"]')){
							var value = row.find('[data-field-name="' + fields_data[i]['data_field_name'] + '"]').val() || "".replace(/,/g, '');
							row.data('field_models')['embed_' + fields_data[i]['data_field_name']] = Number(value);
						}
						else{
							row.data('field_models')['embed_' + fields_data[i]['data_field_name']] = Number("");
						}
					}
					// row.data('field_models')['embed_' + fields_data[i]['data_field_name']] = Number("");
				}
				else{
					if($.type(row.data('field_models')['embed_' + fields_data[i]['data_field_name']]) == "undefined"){
						console.log("row.find", row.find('[data-field-name="' + fields_data[i]['data_field_name'] + '"]'));
						if(row.find('[data-field-name="' + fields_data[i]['data_field_name'] + '"]')){
							var value = row.find('[data-field-name="' + fields_data[i]['data_field_name'] + '"]').val() || "";
							row.data('field_models')['embed_' + fields_data[i]['data_field_name']] = value;
						}
						else{
							row.data('field_models')['embed_' + fields_data[i]['data_field_name']] = "";
						}
					}
					// row.data('field_models')['embed_' + fields_data[i]['data_field_name']] = "";
				}
				row.find('[data-field-name="' + fields_data[i]['data_field_name'] + '"]').on('change', function(e){
					// e.preventDefault();
					var self = $(this);
					if(self.is('select[multiple]')){
						var selected = self.children('option:selected').map(function(){ return $(this).val(); }).get().filter(Boolean);
						row.data('field_models')['embed_' + self.attr('data-field-name')] = selected.join('|^|');
					}
					else if(self.is('input[type="checkbox"]')){
						var checked = row.find('[data-field-name="' + self.attr('data-field-name') + '"]').filter(':checked').map(function(){ return $.trim($(this).val()); }).get().filter(Boolean);
						row.data('field_models')['embed_' + self.attr('data-field-name')] = checked.join('|^|');
					}
					else if(self.is('input[type="radio"]')){
						row.data('field_models')['embed_' + self.attr('data-field-name')] = self.filter(':checked').val();
					}
					else{
						if(self.attr('equivalent-fld-input-type') == 'Currency'){
							row.data('field_models')['embed_' + self.attr('data-field-name')] = Number($(this).val()).currencyFormat();
							var current_value = $(this).val().replace(/,/g, '');
							console.log("current_value", current_value);
							$(this).val(Number(current_value).currencyFormat());
						}
						else if(self.attr('equivalent-fld-input-type') == 'Number'){
							row.data('field_models')['embed_' + self.attr('data-field-name')] = Number($(this).val());
						}
						else{
							row.data('field_models')['embed_' + self.attr('data-field-name')] = $(this).val();
						}
					}
				});
			}
		// }
	},
	"distributeDataSending":function(data_sending, row_line){
		var row = $(row_line);
		for(i in data_sending){
			console.log("data_sending o", data_sending[i]);
			var d_sending_object = data_sending[i];
			var parent_form_field = d_sending_object['epds_select_my_form_field_side'];
			var child_form_field = d_sending_object['epds_select_popup_form_field_side'];
			var child_field = $(row.find('[data-field-name="' + child_form_field + '"]'));
			var parent_field_value = EmbedFormEvents.getDataSendingParentFieldValue(parent_form_field);
			console.log("parent value:", parent_field_value, "type", $.type(parent_field_value));
			if(child_field.is('select[multiple]')){
				if($.type(parent_field_value) != "array"){
					child_field.find('option[value="' + parent_field_value + '"]').prop('selected', true);
				}
				else{
					for(i in parent_field_value){
						child_field.find('option[value="' + parent_field_value[i] + '"]').prop('selected', true);
					}
					row.data('field_models')['embed_' + child_form_field] = parent_field_value.join('|^|');
				}
			}
			else if(child_field.is('input[type="checkbox"]')){
				if($.type(parent_field_value) != "array"){
					child_field.filter('[value="' + parent_field_value + '"]').prop('checked', true);

				}
				else{
					for(i in parent_field_value){
						child_field.filter('[value="' + parent_field_value[i] + '"]').prop('checked', true);
					}
					row.data('field_models')['embed_' + child_form_field] = parent_field_value.join('|^|');
				}
			}
			else if(child_field.is('input[type="radio"]')){
				child_field.filter('[value="' + parent_field_value + '"]').prop('checked', true);
				row.data('field_models')['embed_' + child_form_field] = parent_field_value;
			}
			else{
				if(child_field.attr('equivalent-fld-input-type') == "Currency"){
					child_field.val((Number(parent_field_value)||Number("")).currencyFormat());
					row.data('field_models')['embed_' + child_form_field] = (Number(parent_field_value)||Number("")).currencyFormat();
				}
				else{
					child_field.val(parent_field_value);
					row.data('field_models')['embed_' + child_form_field] = parent_field_value;
				}
				
			}
		}
	},
	"getDataSendingParentFieldValue":function(parent_form_field){
		var parent_field = $('[name="' + parent_form_field + '"]')||$('[name="' + parent_form_field + '[]"]');
		var value;
		if(parent_field.is('select[multiple]')){
			value = parent_field.children('option[selected]').map(function(){
				return $(this).val();
			}).get();
		}
		else if(parent_field.is('input[type="checkbox"]')){
			value = parent_field.filter(':checked').map(function(){
				$(this).val();
			}).get();
		}
		else if(parent_field.is('input[type="radio"]')){
			value = parent_field.filter(':checked').val();
		}
		else{
			value = parent_field.val();
		}
		return value;
	},
	"updateFieldModel": function(row_line){
		$(row_line).find('.embed_getFields').each(function(index){
			$(this).on('change', function(e){
				var self = $(this);
				if(self.is('select[multiple]')){
					$(row_line).data('field_models')["embed_" + $(this).attr('data-field-name')] = self.children('option:selected').map(function(){ return $(this).val(); }).get().join('|^|');
				}
				else if(self.is('input[type="checkbox"]')){
					$(row_line).data('field_models')["embed_" + $(this).attr('data-field-name')] = self.filter(':checked').map(function(){ return $.trim($(this).val()); }).get().filter(Boolean).join('|^|');
				}
				else if(self.is('input[type="radio"]')){
					$(row_line).data('field_models')["embed_" + $(this).attr('data-field-name')] = self.filter(':checked').val();
				}
				else{
					if(self.attr('equivalent-fld-input-type') == "Currency"){
						$(row_line).data('field_models')["embed_" + $(this).attr('data-field-name')] = Number(self.val()).currencyFormat();
					}
					else if(self.attr('equivalent-fld-input-type') == "Number"){
						$(row_line).data('field_models')["embed_" + $(this).attr('data-field-name')] = Number(self.val());
					}
					else{
						$(row_line).data('field_models')["embed_" + $(this).attr('data-field-name')] = self.val();
					}
				}
			});
		});
	}
}

// var EmbeddedUI ={
// 	"block": function(embedded_element){
// 		var ui_block = '<div class="embed-ui-block" style="width: 100%; height: 100%; position: absolute; background-color: black; opacity: 0.5;">' + 
// 						    '<div class="spinner load_m" style="position: absolute; top: 40%; left: 45%;">' + 
// 						        '<div class="bar1"></div>' + 
// 						        '<div class="bar2"></div>' + 
// 						        '<div class="bar3"></div>' + 
// 						        '<div class="bar4"></div>' + 
// 						        '<div class="bar5"></div>' + 
// 						        '<div class="bar6"></div>' + 
// 						        '<div class="bar7"></div>' + 
// 						        '<div class="bar8"></div>' + 
// 						        '<div class="bar9"></div>' + 
// 						        '<div class="bar10"></div>' + 
// 						    '</div>' + 
// 						'</div>';
// 		this.embed_element = embedded_element;
// 		embedded_element.prepend(ui_block);
// 	},
// 	"unblock": function(){
// 		this.embed_element.find('.embed-ui-block').fadeOut(function(){
// 			$(this).remove()
// 		});
// 	},
// 	"embed_element": $()
// }