// var pathname = window.location.pathname;
// if (pathname == "/user_view/workspace") { }
var text_tagging = {
    init:function(){
        // this.onloadGetUsers();
        this.load();
    },
    load:function(){
        var self = this;
        $('.tagging-input').each(function(){
            var initialvalue = $(this).data('value')||[];
            initialvalue = initialvalue.map(function(val){
                return {id:val,text:(val||'|').split('|')[1]||val};
            });
            var $s2Element = $(this).select2({
                data: initialvalue,
                multiple: true,
                placeholder: 'Input Email:',
                allowClear: true,
                delay: 1000,
                ajax: {
                    url: '/ajax/get-user?preferenceID=email&preferenceText=email',
                    dataType: 'json',
                    processResults: function (data) {
                        // console.log("console",$('.chat_tabs[data-thread-id="'+threadId+'"]').find('.chat_container'))
                        // $('.chat_tabs[data-thread-id="'+threadId+'"]').find('.chat_container').perfectScrollbar('update');
                        // console.log($('#recipients').attr('data-user-id'));
                        // console.log(this.$element.val());
                        // var addedExclusions = 1;

                        var _this = this.$element;
                        return {
                            results: data.filter(function(val){
                                return (_this.val()||[]).indexOf(val.id) <= -1;
                            })
                        };
                    }
                }
            });
            self.cleanElement($s2Element);
            self.checkForInitialValue($s2Element);
        });
    },
    cleanElement:function(s2Element){
        if(s2Element.next().is('span.select2-container')){
            var temp = s2Element.next().add(s2Element.next().children().children('.select2-selection:eq(0)'));
            temp.css({
                'max-height':s2Element.parent().outerHeight()+'px',
                'height':'100%'
            });
        }
    },
    checkForInitialValue:function(s2Element){
            var temp = s2Element.data('value')||[];
            s2Element.val( temp ).trigger('change');
            s2Element.removeData('value');
    },
    onloadGetUsers:function(){
        var self = this;
        var defer = {};
        defer.then = function(cb){
            this.callBack = cb;
        };
        $.ajax({
            type:'GET',
            url:'/ajax/get-user2',
            dataType:'json',
            success:function(data){
                if($.type(defer.callBack) == "function"){
                    defer.callBack(data);    
                }
            }
        });
        return defer;
    }
};



var computed = {
    init: function () {
        this.load();
    },
    load: function () {
        var self = this;
        var computedFields = [];
        var computedFieldPost = [];
        var computed_getfields_ele = $('[data-type="computed"]').find('.getFields');
        computed_getfields_ele.each(function () {
            computedFields.push({
                FieldName: $(this).attr('name'),
                ProcessorType: $(this).attr('processor-type'),
                Processor: $(this).attr('processor')
            });
            computedFieldPost.push($(this).attr('name'));
        });

        $('#computedFields').val(JSON.stringify(computedFieldPost));

        var reference = {
            fields: JSON.stringify(computedFields)
        };
        // ui.block();
        var loading_content = $(
                '<span class="select-loader-wrapper">' +
                '<div class="getFromsLoader form-select" style="display:inline-block;position:relative;vertical-align:middle;text-align:center;">' +
                '<label style="color:black;position:reltive;font-size: 15px;display:inline-block;">Loading...</label>' +
                '<img src="/images/loader/loader.gif" class="display processorLoad" style="display: inline-block;"/>' +
                '</div>' +
                '<span>'
                );
        computed_getfields_ele.each(function () {
            var self = $(this);
            var self_w = self.width();
            var self_h = self.height();
            var cloned_loading = $(loading_content.clone());
            self.parent().prepend(cloned_loading);
            cloned_loading.css({
                "width": (self_w) + "px",
                "height": (self_h) + "px",
                "display": "inline-block"
            });
            self.hide();

        });
        this.lookup(reference, function (data) {
            computed_getfields_ele.each(function () {
                var self = $(this);
                self.parent().children('.select-loader-wrapper').hide();
                self.show();
            });
            data = JSON.parse(data);
            console.log(data);
            for (var index in data) {
                $('[name="' + data[index].FieldName + '"]').attr('disabled', false);
                $('[name="' + data[index].FieldName + '"]').attr('readonly', true);
                $('[name="' + data[index].FieldName + '"]').val(data[index].Processor);
            }

            // ui.unblock();
            //embeded_view.init();
        });
        //added by Japhet - fix the names tooltip
        $('.viewNames').each(function () {
            if ($(this).attr('data-original-title') != "Select Name") {
                $(this).attr('data-original-title', 'Select Name');
            }
        });
        //================================================================
    },
    lookup: function (reference, callback) {
        $.get('/ajax/request_computed_fields', reference, function (data) {
            callback(data);
        });
    }
};


// var pathname = window.location.pathname;
// if (pathname == "/user_view/workspace") { }
var detail_panel = {
    init: function () {
        this.load();
    },
    load: function () {
        var self_detail_panel = this;
        $('.details-panel-container').each(function () {
            var data_object_id = $(this).closest('.setOBJ').attr("data-object-id");
            if ($.type($('body').data("user_form_json_data")["form_json"][data_object_id]) != "undefined") {
                var detail_panel_json_values = $('body').data("user_form_json_data")["form_json"][data_object_id]["saved_details_panel"];
                if ($.type(detail_panel_json_values) != "undefined") {
                    var dpjv_length = detail_panel_json_values.length;

                    var ret = '<div class="fl-reveal-wrapper fl-reveal-wrapper-workspace trans_duration3 fl-modules-forms-details">';
                    ret += '<div class="fl-widget-head fl-small-widgetv2">';
                    ret += '<div class="widget-icon-wrapper">';
                    ret += '<span class="widget-title"><i class="fa fa-info"></i> Other details panel</span>';
                    ret += '</div>';
                    ret += '</div>';
                    ret += '<div >';
                    for (var ctr in detail_panel_json_values) {

                        var dp_val = detail_panel_json_values[ctr]["container_name"];
                        var dp_title_formula = detail_panel_json_values[ctr]["title_formula_array"];
                        ret += '<div class ="fl-request-widget">';
                        ret += '<div class ="fl-request-widget-header">';
                        ret += '<span> Header : ' + dp_val + '</span>';
                        ret += '</div>';
                        ret += '<ul class="fl-request-widget-data">';
                        for (var ctr2 in dp_title_formula) {
                            var dp_title_val = dp_title_formula[ctr2]["title"];
                            var dp_formula_val = dp_title_formula[ctr2]["formula"];
                            var formulaIsLink = dp_title_formula[ctr2]["link"];
                            var new_formula = new Formula(dp_formula_val);
                            var executed_formula = new_formula.getEvaluation();
                            if ($.type(executed_formula) == "undefined") {
                                executed_formula = "";
                            }
                            if (formulaIsLink == "yes") {
                                executed_formula = '<a href="" style="text-decoration: underline;">' + executed_formula + '</a>';
                            }
                            ret += '<li>' + dp_title_val + " : " + executed_formula + '</li>';
                        }
                        ret += '</ul>';
                        ret += '</div>';
                    }
                    var appended_dp = $(ret)
                    $(this).append(appended_dp);
                    $(appended_dp).perfectScrollbar();
                }


            }


        });

    }
}

// var pathname = window.location.pathname;
// if (pathname == "/user_view/workspace") { }
var qr_code_scanner = {
    "init": function () {
        this.load();
    },
    "load": function () {
        var delay = (function () {
            var timer = 0;
            return function (callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        })();
        $('.setOBJ[data-type="qr-code"]').each(function () {
            var self = $(this);
            var self_data_object_id = self.attr('data-object-id');
            var input_self = self.find('.input_position_below').children('.qr-code-field-container').children('input.qr-code-input');
            var self_input_name = input_self.attr('name');
            var image_self = self.find('.input_position_below').children('.qr-code-field-container').children('div.qr-code-image');
            input_self.on({
                'keyup.QRCodeScannerEvent': function (e) {
                    delay(function () {
                        if(input_self.val() != ""){
                            image_self.html("");
                            var qrcodesvg = new Qrcodesvg(input_self.val(), image_self.attr('id'), 150);
                            qrcodesvg.draw();
                        }else{
                            image_self.html("");
                        }
                    }, 15);
                }
            });
            if(input_self.val() != ""){
                input_self.trigger('keyup.QRCodeScannerEvent');
            }
        });
    }
}

// var pathname = window.location.pathname;
// if (pathname == "/user_view/workspace") { }
var smart_barcode_scan = {
    "init": function () {

        this.load();
    },
    "load": function () {

        $('.setOBJ[data-type="smart_barcode_field"]').each(function () {
            var self = $(this);
            var self_data_object_id = self.attr('data-object-id');
            var input_smart_barcode_ele = self.find('input.smart-barcode-field-input[type="text"]');
            input_smart_barcode_ele.on({//.getFields_'+self_data_object_id+'[name]
                "change": function () {
                    var self = $(this);
                    var sbcf_container = self.parents('.sbcf-container:eq(0)');
                    var collector_field = sbcf_container.prev().find('.smart-barcode-field-input-str-collection[name]');
                    var str_val = $.trim(self.val());
                    // if((str_val).length >= 1 /* && !self.data("value_entered")*/ ) {
                    self.data("value_entered", true);
                    collector_field.val(sbcf_container.find('input.smart-barcode-field-input[type="text"]').map(function () {
                        return $(this).val();
                    }).get().filter(Boolean).join('|^|'))
                    collector_field.trigger("change");
                    if (self.parents('.sbcf-smart-barcode-field-group').eq(0).next().length <= 0) {
                        self.parents('.sbcf-smart-barcode-field-group').eq(0).find('.fa.fa-plus.sbcf-action-icon-add').trigger('click', {"triggered": true});
                    } else {
                        self.parents('.sbcf-smart-barcode-field-group').eq(0).next().find('input.smart-barcode-field-input[type="text"]').trigger('focus');
                    }
                    // }
                },
                "focus": function () {
                    var self = $(this);
                    var str_val = $.trim(self.val());
                    if ((str_val).length >= 1) {
                        self.select();
                    }
                }
                // "keyup":function(e){
                //     // extra feature request
                //     // var self = $(this);
                //     // var str_val = $.trim(self.val());
                //     // var keyCode = (typeof e.which == "number")?e.which:((e.keycode)?e.keycode:e.keyCode);
                //     // if(e.keyCode == 8 && str_val.length <= 0){
                //     //     var prev_group = self.parents('.sbcf-smart-barcode-field-group').eq(0).prev();
                //     //     self.parents('.sbcf-smart-barcode-field-group').eq(0).find('.fa.fa-minus.sbcf-action-icon-delete:visible').trigger('click',{"triggered":true});
                //     //     if(prev_group.length >= 1){
                //     //         prev_group.find('.getFields').trigger("focus",{"triggered":true})
                //     //     }
                //     // }
                // }
            });

            var value_existing = self.find('input.smart-barcode-field-input-str-collection').val(); //DITO na ako
            var value_existing_arr = value_existing.split('|^|').filter(Boolean);
            var add_plus_ele = self.find('.fa.fa-plus.sbcf-action-icon-add').eq(0);
            var temp_clone = $();
            var tr_group = input_smart_barcode_ele.parents('.sbcf-smart-barcode-field-group')
            if (value_existing_arr.length >= 1) {
                input_smart_barcode_ele.val(value_existing_arr[0]);
                for (var ctr = value_existing_arr.length; ctr > 0; ctr--) {
                    if (value_existing_arr[ctr]) {
                        temp_clone = tr_group.clone();
                        tr_group.after(temp_clone);
                        temp_clone.children("td:eq(0)").children("div").children("input").val(value_existing_arr[ctr]);
                    }
                }
            }

            $('.setOBJ[data-type="smart_barcode_field"]').css("height","+=0px");// fix for border of the smart barcode by paul

            if(self.find('.smart-barcode-field-input').attr("disabled")){
                    //self.find('.sbcf-smart-barcode-field-group').remove()
                    self.find('.fa.fa-plus.sbcf-action-icon-add').remove()
                    //alert('1');
            }else{
                MakePlusPlusRowV2({
                    "tableElement": self,
                    "rules": [
                        {
                            "elePlus": ".fa.fa-plus.sbcf-action-icon-add",
                            "elePlusTarget": ".sbcf-smart-barcode-field-group",
                            "elePlusTargetFnBefore":function(e,data){
                                var self = $(this);
                                var target = $(e.target);
                                if(e.originalEvent){
                                    if(target.is('.readonly-true')){
                                        return false;
                                    }
                                }
                            },
                            "elePlusTargetFn": function (e, data) {
                                var self = $(this);
                                var self_field = self.find('input.smart-barcode-field-input[type="text"]');
                                if (data['field_value']) {
                                    self_field.val(data['field_value'])
                                } else {
                                    self_field.val("");

                                }
                                self_field.removeData('value_entered');


                                if ($(data['next_ele']).length >= 0 || $(data['prev_ele']).length >= 0) {
                                    $(data['parent_ele']).find('.fa.fa-minus.sbcf-action-icon-delete:not(:visible)').removeClass('isDisplayNone');
                                }

                                if (data['triggered']) {
                                    self_field.trigger('focus');
                                }

                            },
                            "eleMinus": ".fa.fa-minus.sbcf-action-icon-delete",
                            "eleMinusTarget": ".sbcf-smart-barcode-field-group",
                            "eleMinusTargetFnBefore":function(e,data){
                                var self = $(this);
                                var target = $(e.target);
                                if(e.originalEvent){
                                    if(target.is('.readonly-true')){
                                        return false;
                                    }
                                }
                            },
                            "eleMinusTargetFn": function (e, data) {
                                var self = $(this);
                                var self_field = self.find('input.smart-barcode-field-input[type="text"]');
                                var collector_field = self.find('.smart-barcode-field-input-str-collection[name]');
                                if ($(data['parent_ele']).children('.sbcf-smart-barcode-field-group').length == 1) {
                                    $(data['parent_ele']).find('.fa.fa-minus.sbcf-action-icon-delete:visible').addClass('isDisplayNone');
                                }
                                collector_field.val($(data['parent_ele']).children('tr').children('td').children('div').children('input.smart-barcode-field-input[type="text"]:not([name])').map(function () {
                                    return $(this).val();
                                }).get().filter(Boolean).join('|^|'))
                                collector_field.trigger("change");
                            }
                        }
                    ]
                });
            }
            // var value_existing = self.find('input.smart-barcode-field-input-str-collection').val(); //DITO na ako
            // var value_existing_arr = value_existing.split('|^|').filter(Boolean);
            // var add_plus_ele = self.find('.fa.fa-plus.sbcf-action-icon-add').eq(0);
            // if (value_existing_arr.length >= 1) {
            //     input_smart_barcode_ele.val(value_existing_arr[0])
            //     for (var ctr = value_existing_arr.length; ctr > 0; ctr--) {
            //         add_plus_ele.trigger('click', {"field_value": value_existing_arr[ctr]});
            //     }
            // }
        });
    }
}

window.setVariableInterval = function (callbackFunc, timing) {
    var variableInterval = {
        interval: timing,
        callback: callbackFunc,
        stopped: false,
        runLoop: function () {
            alert("HAHHH: " + variableInterval.stopped)
            if (variableInterval.stopped)
                return;

            var result = variableInterval.callback.call(variableInterval);
            if (typeof result == 'number') {
                if (result === 0)
                    return;

                variableInterval.interval = result;
            }
            variableInterval.loop();
        },
        stop: function () {
            this.stopped = true;
            window.clearTimeout(this.timeout);
        },
        start: function () {
            this.stopped = false;
            return this.loop();
        },
        loop: function () {
            this.timeout = window.setTimeout(this.runLoop, this.interval);
            return this;
        }
    };

    return variableInterval;
};
function getPercent(containerPx, fillerPx) {
    return (fillerPx / containerPx) * 100;
}
function getPX(containerPx, fillerPercent) {
    return (containerPx * fillerPercent) / 100;
}
$.fn.selectRange = function (start, end) {
    return this.each(function () {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};
function getCaretPosition(editableDiv) {

    var element = editableDiv;
    var caretOffset = 0;
    if (typeof window.getSelection != "undefined") {
        var range = window.getSelection().getRangeAt(0);
        var preCaretRange = range.cloneRange();
        preCaretRange.selectNodeContents(element);
        preCaretRange.setEnd(range.endContainer, range.endOffset);
        caretOffset = preCaretRange.toString().length;
    } else if (typeof document.selection != "undefined" && document.selection.type != "Control") {
        var textRange = document.selection.createRange();
        var preCaretTextRange = document.body.createTextRange();
        preCaretTextRange.moveToElementText(element);
        preCaretTextRange.setEndPoint("EndToEnd", textRange);
        caretOffset = preCaretTextRange.text.length;
    }
    // var newLineIndexes = getListREIndexOf($(element), "<div");
    // var sPoint = selection_index["startPoint"];
    // var ePoint = selection_index["endPoint"];
    // for(var iiz in newLineIndexes){
    //     if(newLineIndexes[iiz] > ){
    //         newLineIndexes[iiz];
    //     }
    // }

    // if(typeof range["startOffset"] !== 'undefined'){
    //     if(range["startOffset"] >= 0){
    //         var value_typed = getContentEditableVal($(element));

    //         var index_newline = value_typed.indexOf("\n");
    //         console.log(value_typed,index_newline)
    //         var last_catch = -1;
    //         while(index_newline > 0){

    //             console.log(index_newline, caretOffset)
    //             if(value_typed.indexOf("\n",index_newline + 1) > 0){
    //                 if(value_typed.indexOf("\n",index_newline + 1) > caretOffset && index_newline < caretOffset){
    //                     break;
    //                 }
    //             }else{
    //                 // if(index_newline > caretOffset){
    //                 //     break;
    //                 // }
    //             }

    //             if(value_typed.indexOf("\n",index_newline + 1) < 0){
    //                 last_catch = index_newline;
    //             }
    //             index_newline = value_typed.indexOf("\n",index_newline + 1) ;
    //         }
    //         index_newline = last_catch;

    //     }
    // }
    // index_newline = index_newline -1;
    // if(index_newline < 0){
    //     index_newline = 0;
    // }
    console.log($(element).caret().start, $(element).caret().end)
    return {
        // "index_newline":index_newline,
        "inlineStartPoint": range["startOffset"],
        "startPoint": range["startOffset"],
        "endPoint": caretOffset
    };
}
String.prototype.insertReplaceAt = function (start_index, len, str) {
    if (typeof start_index === "number") {
        // console.log(this, "IRA", start_index, len, str, "-===-", this.substr(0, start_index) + str + this.substr(start_index + (len), this.length));
        // console.log("DETAIL", this.substr(0, start_index), "+", str, "+", this.substr(start_index + (len), this.length))
        return this.substr(0, start_index) + str + this.substr(start_index + (len), this.length);
    }
}
String.prototype.regexIndexOf = function (regex, startpos) {
    var indexOf = this.substring(startpos || 0).search(regex);
    return (indexOf >= 0) ? (indexOf + (startpos || 0)) : indexOf;
}

String.prototype.regexLastIndexOf = function (regex, startpos) {
    regex = (regex.global) ? regex : new RegExp(regex.source, "g" + (regex.ignoreCase ? "i" : "") + (regex.multiLine ? "m" : ""));
    if (typeof (startpos) == "undefined") {
        startpos = this.length;
    } else if (startpos < 0) {
        startpos = 0;
    }
    var stringToWorkWith = this.substring(0, startpos + 1);
    var lastIndexOf = -1;
    var nextStop = 0;
    while ((result = regex.exec(stringToWorkWith)) != null) {
        lastIndexOf = result.index;
        regex.lastIndex = ++nextStop;
    }
    return lastIndexOf;
}
function getListREIndexOf(str, pattern) {
    var collect_indexOf = [];
    i = str.regexIndexOf(new RegExp(pattern, ""));
    collect_indexOf.push(i);
    while (i >= 0) {
        // console.log(i)
        collect_indexOf.push(i);
        i = str.regexIndexOf(new RegExp(pattern, ""), i + 1);
    }
}


AutoCompletePicklist = {
    "init": function (element) {
        return;
        if (typeof element === "undefined") {

            element = '.pickListButton';
        }
        $(element).each(function () {
            var formId = $(this).attr('form-id');
            var returnFieldName = $(this).attr('return-field-name');
            var self = $(this);
            var condition = $(this).attr('condition');
            var returnData = [];
            var picklist_data = {
                formId: formId,
                search: '',
                condition: condition
            };
            if (self.is('auto_complete_data')) {
                data = self.is('auto_complete_data');
                for (var i in data) {
                    returnData.push(data[i]['' + returnFieldName + ''])
                }
                if (returnData.length >= 1) {
                    returnData = returnData.filter(Boolean);
                }
                self.closest('.setOBJ[data-type="pickList"]').find(".getFields").autocomplete({
                    source: returnData,
                    select: function (event, ui) {
                        // console.log(ui,event);
                        $(this).val(ui.item.value);
                        $(this).change();
                        return true;
                    },
                    close: function (event, ui) {
                        event.preventDefault();
                        return false;
                    }
                });
            } else {
                picklist.getRequests(picklist_data, function (data) {
                    self.data("auto_complete_data", data);
                    for (var i in data) {
                        returnData.push(data[i]['' + returnFieldName + ''])
                    }
                    if (returnData.length >= 1) {
                        returnData = returnData.filter(Boolean);
                    }
                    self.closest('.setOBJ[data-type="pickList"]').find(".getFields").autocomplete({
                        source: returnData,
                        select: function (event, ui) {
                            // console.log(ui,event);
                            $(this).val(ui.item.value);
                            $(this).change();
                            return true;
                        },
                        close: function (event, ui) {
                            event.preventDefault();
                            return false;
                        }
                    });
                });
            }
        })
    }
}
function deployArrayValues(arrayValue, callBack) {
    $.each(arrayValue, function (i, v) {
        if (typeof v === "object") {
            deployArrayValues(v, callBack);
        } else {
            callBack(i, v);
        }
    })
}

//added jewel 03192015
function getProcessedFormula(obj_name, formula) {
    var process_formula = formula;
    var getFieldName = formula.match(/@[A-Za-z_][A-Za-z0-9_]*/g);
    var fn_reserve_keywords = ["@Requestor", "@RequestID", "@Status", "@CurrentUser", "@Department", "@TrackNo", "@Processor", "@Today", "@Now", "@TimeStamp"];

    if (getFieldName) {
        getFieldName = getFieldName.filter(Boolean);
        var temp_getFieldName = getFieldName;
        temp_getFieldName = temp_getFieldName.map(function (value, index, whole) {
            return value.replace("@", "");
        })

        var at_str_name = "";
        for (var ctr = 0; ctr < getFieldName.length; ctr++) {
            if ($('[name="' + getFieldName[ctr].replace("@", "") + '"]').length >= 1 && fn_reserve_keywords.indexOf(getFieldName[ctr].replace('[]', '')) <= -1) {
                var fn_ele = $('[name="' + getFieldName[ctr].replace("@", "") + '"]');
                var value_fn = fn_ele.val()
                if (value_fn) {
                    value_fn = value_fn.replace("@", "^@");
                    process_formula = process_formula.replace(getFieldName[ctr], "'" + value_fn + "'");
                } else {
                    process_formula = process_formula.replace(getFieldName[ctr], "'" + "'");
                }
//                if ($.type(fn_ele.data("formula_lookup_names")) == "array") {
//                    fn_ele.data("formula_lookup_names").push(fieldName);
//                    $.unique(fn_ele.data("formula_lookup_names"));
//                } else {
//                    fn_ele.data("formula_lookup_names", []);
//                    fn_ele.data("formula_lookup_names").push(fieldName);
//                }
            } else {
                at_str_name = getFieldName[ctr];
                if (at_str_name == "@Requestor") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    var requestor_name = "";
                    if ($("[name='ID']").val() == "0") {
                        var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                        requestor_name = requestor_ele.attr("requestor-name");
                    } else {
                        var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                        requestor_name = requestor_ele.attr("requestor-name");
                    }
                    process_formula = process_formula.replace(re, "\"" + requestor_name + "\"");
                } else if (at_str_name == "@RequestID") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    var request_id_ele = $('[name="ID"]');
                    var rid_val = request_id_ele.val();
                    process_formula = process_formula.replace(re, rid_val);
                } else if (at_str_name == "@Status") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    var status_ele = $("#frmrequest").find('[name="Status"]').eq(0);
                    if (status_ele.length >= 1) {
                        var getStatusVal = status_ele.val();
                        process_formula = process_formula.replace(re, "\"" + getStatusVal + "\"");
                    }
                } else if (at_str_name == "@CurrentUser") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    var CurrentUserName = "";
                    var element_in_progress = $('[name="CurrentUser"]');
                    if (typeof element_in_progress.attr("current-user-name") != "undefined") {
                        CurrentUserName = element_in_progress.attr("current-user-name");
                    }
                    process_formula = process_formula.replace(re, "\"" + CurrentUserName + "\"");
                } else if (at_str_name == "@Department") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    var getDep_ele = $("[name='Department_Name']");
                    if (getDep_ele.length >= 1) {
                        var getDepName = getDep_ele.val();
                        process_formula = process_formula.replace(re, "\"" + getDepName + "\"");
                    }
                } else if (at_str_name == "@TrackNo") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    var track_no_ele = $("#frmrequest").find('[name="TrackNo"]').eq(0);
                    if (track_no_ele.length >= 1) {
                        var getTrackNoVal = track_no_ele.val();
                        process_formula = process_formula.replace(re, "\"" + getTrackNoVal + "\"");
                    }
                } else if (at_str_name == "@Processor") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    if ($("[name='ID']").val() == "0") {
                        var processor_user_ele = $('[name="Processor"]');
                        var processor_name = processor_user_ele.attr("processor-name");
                        process_formula = process_formula.replace(re, "\"" + processor_name + "\"");
                    } else {
                        var processor_user_ele = $("#processor_display");
                        var processor_name = processor_user_ele.text();
                        process_formula = process_formula.replace(re, "\"" + processor_name + "\"");
                    }
                } else if (at_str_name == "@Today") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    var this_today = new Date(getUpdatedServerTime());
                    this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2);
                    process_formula = process_formula.replace(re, "\"" + cleanStringValue(this_today) + "\"");
                } else if (at_str_name == "@Now") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    var this_today = new Date(getUpdatedServerTime());
                    this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2) + " " + StrRight("0" + this_today.getHours(), 2) + ":" + StrRight("0" + this_today.getMinutes(), 2) + ":" + StrRight("0" + this_today.getSeconds(), 2);
                    process_formula = process_formula.replace(re, "\"" + cleanStringValue(this_today) + "\"");
                } else if (at_str_name == "@TimeStamp") {
                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                    var this_timestamp = $('body').data("date_page_visited");
                    process_formula = process_formula.replace(re, "\"" + this_timestamp + "\"");
                }
            }
        }
    }

    return process_formula;
}

//added by japhet
//update tables, update the datas of single and multiple attachments inside the repeater data
function updateTables() {
    var resultJson = $('[name="Repeater_Data"]').val();
    var rep_data = [];
    try {
        rep_data = JSON.parse(resultJson);
    }
    catch (e) {

    }
    for (index in rep_data) {
        var get_table = $('#' + rep_data[index]['ObjectId']);
        if (rep_data.length > 0) {
            var rows = rep_data[index]['Row']
            for (i in rows) {
                var index = rows[i]['Index'];
                var data = rows[i]['Data'];
                var multiple_current_attachment = get_table.find('[data-attachment]').eq(index);
                var single_current_attachment = get_table.find('[data-single-attachment]').eq(index);
                for (i in data) {
                    if (multiple_current_attachment.attr('rep-original-name') == data[i]['FieldName']) {
                        var file_name_ext = data[i]['FieldName'] + "UID" + index;
                        var parsed_files = JSON.parse(JSON.parse(data[i]['Values']));
                        $('body').data(file_name_ext, parsed_files);
                        file_modal = multiple_current_attachment.parent().find('a');
                        file_modal.attr('data-body-name', file_name_ext);
                        var file_count = parsed_files.length;
                        file_modal.html('<u> <i class="fa fa-search"></i> Show (' + file_count + ') File`s </u>');
                        file_modal.on("click", function () {
                            var lbl_name = $(this).parent().parent().children().eq(0).children().text();
                            var data_body_name = $(this).attr("data-body-name");
                            var data_json = $("body").data(data_body_name);
                            var ret = "";
                            ret += '<h3 class="fl-margin-bottom fl-add-comment-here"><i class="icon-comment fa fa-paperclip"></i> ' + lbl_name + '</h3>';
                            ret += '<div class="hr"></div>';
                            ret += '<div class="fields">'; //style="padding:10px;"
                            ret += '<ol style="padding:10px;list-style-type:decimal;">';
                            for (var i = 0; i < data_json.length; i++) {
                                ret += '<li style="padding:10px;border: 1px solid #ddd;margin-bottom: 5px; "><div style="float: left;height: 15px;width: 280px;  white-space: nowrap;  overflow: hidden;text-overflow: ellipsis;">' + data_json[i]['file_name'] + '</div>';

                                ret += '<div style="float: right;">';
                                ret += '<a href="' + data_json[i]['location'] + "/" + data_json[i]['file_name'] + '" target="_blank">View File</a>';
                                ret += '</div>';

                                ret += '</li>';
                            }
                            ret += '</ol>';
                            ret += '</div>';
                            var newDialog = new jDialog(ret, "", "", "470", "", function () {
                            });
                            newDialog.themeDialog("modal2");
                            $(".tip").tooltip();
                        });
                    }
                    else if (single_current_attachment.attr('rep-original-name') == data[i]['FieldName']) {
                        var file_name_ext = data[i]['FieldName'] + "UID" + index;
                        var parsed_files = data[i]['Values'];
                        $('body').data(file_name_ext, parsed_files);
                        file_modal = single_current_attachment.parent().find('a');
                        file_modal.attr('href', parsed_files);
                        var file = parsed_files.split("/");
                        file_modal.html('<u><i class="fa fa-search"></i> View File </u> ( <strong>' + file[file.length - 1] + ' ) </strong></u>');
                    }

                }
            }
        }
    }
}


function getContentEditableVal_backup(contentEditEle){
    var ce_elem = $(contentEditEle);
    var ce = $("<pre />").html(ce_elem.html());

    if($.browser.webkit){
           // ce.find("div").each(function(eq){
          //   ce.find("div").eq(eq)
          // })
        ce.find("div").find("br").filter(function(index){
            if(index == 0){
                return false;
            }else{
                return true;
            }
        }).replaceWith(function() { return "\n" + this.innerHTML; })
        ce.find("div").replaceWith(function() { return "\n" + this.innerHTML; })

    }
    if($.browser.msie)
      ce.find("p").replaceWith(function() { return this.innerHTML  +  "<br>"; });
    if($.browser.mozilla || $.browser.opera ||$.browser.msie )
      ce.find("br").replaceWith("\n");

    var textWithWhiteSpaceIntact = ce.text();
    return textWithWhiteSpaceIntact;
}