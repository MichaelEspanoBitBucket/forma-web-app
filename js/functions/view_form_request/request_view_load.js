(function InlineJScriptWorkspacePHTML() {
    $(document).ready(function () {
        var path_name = window.location.pathname;
        //if (path_name == "/user_view/workspace" && getParametersName("view_type", pathname) == "update") {
        var requestor_id = $("#request_by").val();
        if ($('.frm-json-request-details').text() != "") {
            $('body').data('request', JSON.parse($('.frm-json-request-details').text()));
            $('.frm-json-request-details').remove();
        }
        //var status = $("#Status").val();
        //var trackNO = $("#TrackNo").val();
        ////var requestor_id = $("#Requestor").attr('requestor-name');
        //$("#requestor_display").html(requestor_id);
        //$("#trackno_display").html(trackNO);
        //$("#status_display").html(status);
        //
        //
        //}
        // alert("tag ON!");
    });
    var pathname = window.location;
    if (getParametersName("view_type", pathname) == "preview" || getParametersName("view_type", pathname) == "report") {

        var pid = window.opener["pid"];
        var parseJson_data = jQuery.parseJSON(pid);
        // Btn Content
        var workspace_btn = parseJson_data.BtnName;
        var get_btn = workspace_btn.split(",");
        for (var a = 0; a < get_btn.length; a++) {
            $(".input-select").append("<option>" + get_btn[a] + "</option>");
        }
        // Form Content   
        var workspace_content = parseJson_data.WorkspaceContent;
        var content = workspace_content.replace(new RegExp("setObject cursor_move", "g"), "setOBJ form_upload_photos").replace(new RegExp('disabled="disabled"', 'g'), "");


        //console.log(content)
        // Append To content preview form
        $(".loaded_form_content").append(content);


        //fix objects in preview
        {
            //tab panel
            $(".setOBJ ul.ui-tabs-nav li span, .setOBJ ul.ui-tabs-nav li .add-new-tab-panel, .setOBJ .ui-tabs-panel .ui-resizable-handle").css("display", "none");
            //accordian
            $(".setOBJ .form-accordion ul.form-accordion-action-icon").css("display", "none");
        }



        $(".formName").hide();
        //$(".fl-content").css("width", "100%");
    }
    $(document).ready(function () {
        var path_name = window.location.pathname;
        if (path_name == "/user_view/workspace" && getParametersName("view_type", pathname) == "update") {
            $(".getFields").each(function () {
                $(this).attr('disabled', true);
            });
        }

        if (path_name == "/user_view/workspace" && getParametersName("view_type", pathname) == "preview") {

            //Custom object css local storage get item
            //Roni Pinili
            var styleTag = $('<style type="text/css" class="object_css"></style>');
            //var styleTag = $('RAWR');
            $(styleTag).appendTo('head');

            $('.setOBJ').each(function () {
                var setOBJId = $(this).attr('data-object-id');
                var styles = localStorage.getItem(setOBJId);
                $(styleTag).append(styles);
                //console.log(localStorage.getItem(setOBJId));
            });

        }

    });
})();



var formulaOnload = false;
var jsonRequestData = {
    form_id: "1",
    search_value: "",
    field: "0",
    start: "0",
    date_field: "",
    date_from: "",
    date_to: "",
    "column-sort": "",
    "column-sort-type": ""
}
var formula_event_ajax_xhr = {
    "onload": null,
    "presave": null,
    "postave": null,
    "onchange": null
};
var default_spectrum_settings = {
    allowEmpty: true,
    showAlpha: true,
    showInput: true,
    className: "full-spectrum",
    showInitial: true,
    showPalette: true,
    showSelectionPalette: true,
    maxPaletteSize: 10,
    preferredFormat: "rgb",
    localStorageKey: "spectrum-text-color",
    // move: function (color) {

    // },
    //  hide: function(color) {
    //      var revert_color = $(this).next().find('.sp-preview-inner').css("background-color");
    //      $("#lbl_" + object_id).css("color", revert_color);
    //      var data_properties_type = $(this).attr("data-properties-type");
    //      var json = $("body").data();
    //      if (json['' + object_id + '']) {
    //          var prop_json = json['' + object_id + ''];
    //      } else {
    //          var prop_json = {};
    //      }
    //      prop_json['' + data_properties_type + ''] = revert_color;
    //      json['' + object_id + ''] = prop_json;
    //      $("body").data(json);
    //  },
    // show: function () {
    //     var self = $(this);
    //     var selfNextTop = $('.sp-container:visible').position().top;
    //     var selfNextLeft = $('.sp-container:visible').position().left;
    //     var docScollTop = $(document).scrollTop();
    //     var docScollLeft = $(document).scrollLeft();
    //     $('.sp-container:visible').css({'position':'fixed', 'top':(selfNextTop-docScollTop)+'px', 'left':(selfNextLeft-docScollLeft)+'px' });

    //  },
    // beforeShow: function () {

    // },
    // hide: function () {

    // },
    // change: function() {
    //     alert(423)
    // },
    palette: [
        ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
            "rgb(204, 204, 204)", "rgb(217, 217, 217)", "rgb(255, 255, 255)"],
        ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
            "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
        ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
            "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
            "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
            "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
            "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
            "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
            "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
            "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
            "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
            "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
    ]
};
function PrintContentEmbedView(windowDoc) {
    var wdocument = $(windowDoc);
    wdocument.find('.display,.isDisplayNone').hide();
    var window_ele = this;
    var ebmeded_obj_data = wdocument.find('.setOBJ[data-type="embeded-view"]').map(function (a, b) {
        var self = $(this);
        var data_info = {
            "ele": self,
            "top": self.position().top,
            "left": self.position().left,
            "height": self.height(),
            "width": self.width(),
            "oheight": self.outerHeight(),
            "owidth": self.outerWidth()
        }
        data_info['bottom'] = data_info['oheight'] + data_info['top'];
        data_info['right'] = data_info['owidth'] + data_info['left'];


        data_info['ele_below'] = self.parent().children('.setOBJ').map(function () {
            var inner_self = $(this);
            var margin_top = Number((inner_self.css("margin-top") || "").replace("px", ""));
            var margin_left = Number((inner_self.css("margin-left") || "").replace("px", ""));
            var ele_top = inner_self.position().top;
            var ele_left = inner_self.position().left;
            if (margin_top >= 1 && ele_top <= 0) {
                inner_self.css("top", margin_top).css("margin-top", "");
            }
            if (margin_left >= 1 && ele_left <= 0) {
                inner_self.css("left", margin_left).css("margin-left", "");
            }
            var data_info_inner = {
                "ele": inner_self,
                "top": inner_self.position().top,
                "left": inner_self.position().left,
                "height": inner_self.height(),
                "width": inner_self.width(),
                "oheight": inner_self.outerHeight(),
                "owidth": inner_self.outerWidth(),
                "distance_from_dis": ""
            };
            data_info_inner['bottom'] = data_info_inner['oheight'] + data_info_inner['top'];
            data_info_inner['right'] = data_info_inner['owidth'] + data_info_inner['left'];
            data_info_inner['distance_from_dis'] = data_info_inner['top'] - data_info['bottom'];
            if (self[0] != inner_self[0]) {
                return data_info_inner;
            }
            return null;
        }).get().filter(Boolean).sort(function (a, b) {
            return a["top"] - b["top"];
        }).map(function (a, b) {
            console.log("LOG LOG", a, b)
            if ((data_info['bottom'] - 10) <= a['top']) {
                return a;
            }
            return null;
        }).filter(Boolean);

        return data_info;
    }).get();


    var sorted_ele = ebmeded_obj_data.sort(function (a, b) {
        return a["top"] - b["top"];
    });

    var additional_form_height = 0;
    for (var ctr = 0; ctr < sorted_ele.length; ctr++) {
        if(sorted_ele[ctr]["ele"].css("display") != 'none' ){
            sorted_ele[ctr]["ele"].css("display", "inline-table");
        }
        sorted_ele[ctr]["ele"].find('.cloned').css("display", "none");
        sorted_ele[ctr]["new_height_diff"] = sorted_ele[ctr]["ele"].outerHeight() - sorted_ele[ctr]["oheight"];
        additional_form_height += sorted_ele[ctr]["new_height_diff"];
        for (var ctr2 = 0; ctr2 < sorted_ele[ctr]['ele_below'].length; ctr2++) {
            sorted_ele[ctr]['ele_below'][ctr2]['ele'].css("top", sorted_ele[ctr]['ele_below'][ctr2]['ele'].position().top + sorted_ele[ctr]["new_height_diff"]);

        }
    }
    wdocument.find('.loaded_form_content').css("height", (wdocument.find('.loaded_form_content').height() + additional_form_height) + "px");
}
function containerWithRequiredFields(element) {
    var container_selector = {
        "TabPanel": {
            "Containment": ".ui-tabs-panel",
            "jqEleTarget": function (self) {
                var eleId = "#" + self.attr('id');
                var eleTarget = self.closest(".tab-panel-parent-container").find('a[href="' + eleId + '"]').closest('li');
                return eleTarget;
            }
        },
        "Accordion": {
            "Containment": ".ui-accordion-content",
            "jqEleTarget": function (self) {
                var eleId = self.attr('id');
                var eleTarget = self.closest(".form-accordion-section-group").find('h3.ui-accordion-header[aria-controls="' + eleId + '"]');
                return eleTarget;
            }
        }
    };
    var element_parent_selector_ele = Object.keys(container_selector).map(function (a, b) {
        return container_selector[a]["Containment"];
    });
    var containment = element.parents(element_parent_selector_ele.join(","));
    containment.each(function () {
        var self = $(this);
        var container_route = null;
        if (self.is(container_selector["TabPanel"]["Containment"])) {
            container_route = container_selector["TabPanel"];
        } else if (self.is(container_selector["Accordion"]["Containment"])) {
            container_route = container_selector["Accordion"];
        }
        if (container_route != null) {
            container_route.jqEleTarget(self).addClass('container-required');
        }
    });
    $('.container-required').on('click', function () {
        $(this).removeClass('container-required');
    });
}
function containerWithRequiredFieldsOLD(element) {
    var containers = element.closest('.setOBJ[data-type="tab-panel"],.setOBJ[data-type="accordion"]') || "";
    var containerType = containers.attr('data-type') || "";
    var eleTarget = "";
    var eleId = "";
    if (containerType === "tab-panel" || containerType == "accordion") {
        console.log("evaaalll", $(element), $(element).find('.input_position_below:eq(0)').find('input').val(), $(element).val() == "")

        var elementsContainer = element.parents(".ui-tabs-panel,.ui-accordion-content");
        elementsContainer.each(function () {
            alert($(this).attr("class"))

            eleId = "#" + $(this).closest(".ui-tabs-panel").attr('id');
            eleTarget = $(this).closest(".tab-panel-parent-container").find('a[href="' + eleId + '"]').closest('li');
            eleTarget.addClass('container-required');
        });

        //ETO UNG MALI ABA
        // var elementsContainer = element.parents(".ui-tabs-panel");
        // elementsContainer.each(function () {

        //     eleId = "#" + $(this).closest(".ui-tabs-pagetnel").attr('id');
        //     eleTarget = $(this).closest(".tab-panel-parent-container").find('a[href="' + eleId + '"]').closest('li');
        //     eleTarget.addClass('container-required');
        //     console.log("XXX", $(this), eleId, eleTarget);
        // });



    }

    $('.container-required').on('click', function () {
        $(this).removeClass('container-required');
    });
}
function RedirectPage(page, landing_page) {
    var data = null;
    if( !( this == window ) ){
        data = this;
    }
    /*page is the url (you can use the user_view,report_list for local pages)*/
    var redirection_landing_page = landing_page;

    if (!landing_page || landing_page == '' || landing_page == 'undefined' || landing_page == undefined) {
        window.location = "/user_view" + this.headerLocation;
    } else {
        /*CheckUrl (note:used for specific URL)validates the url if it is valid it redirects to the URL if not, redirects to local pages*/
        if (CheckUrl(redirection_landing_page)) {
            window.location = redirection_landing_page;
        } else if (redirection_landing_page == "application") {
            window.location = "/user_view" + this.headerLocation;
        } else if (redirection_landing_page == "workspace") {
            this.workspaceLocation = this.workspaceLocation.replace(/&amp;/g, '&');
            window.location = "/user_view" + this.workspaceLocation;
        } else if (redirection_landing_page == "portal") {
            window.location = "/" + redirection_landing_page;
        } else if (redirection_landing_page == "report") {
            var user_level = $("#current-user-user-level").text();
            if (user_level == "2") {
                window.location = "/user_view/report_list";
            } else {
                window.location = "/user_view/reports";
            }
        } else if (redirection_landing_page == "message-app") {
            if ($("#message_app_config").val() == "1") {
                window.location = "/user_view/" + redirection_landing_page;
            } else {
                window.location = "/user_view" + this.headerLocation;
            }
        } else if (redirection_landing_page == "notifications") {
            if ($("#noti_app_config").val() == "1") {
                window.location = "/user_view/" + redirection_landing_page;
            } else {
                window.location = "/user_view" + this.headerLocation;
            }
        } else if (redirection_landing_page == "current-record" && data ){
            window.location = "/user_view/workspace?view_type=request&formID="+data.formId+"&requestID="+data.id+"&trackNo="+data.trackNo+"&printID=0&annoID=";
        } else {
            window.location = "/user_view/" + redirection_landing_page;
        }
    }
}
function blockActionWhileComputing() {
    $('.fl-action-submit').each(function () {
        $(this).addClass('submit-event-blocker');
    });
    var switcher_set_interval = 0;
    var formula_loading = new SetMyTimer(function () {
        if ($('.data-formula-loader-noti').length >= 1) {
            $('.fl-action-submit').each(function () {
                $(this).addClass('submit-event-blocker');
            });
            formula_loading.resetTimeoutV2(1000);
        } else {
            setTimeout(function () {
                if ($('.data-formula-loader-noti').length >= 1) {
                    $('.fl-action-submit').each(function () {
                        $(this).addClass('submit-event-blocker');
                    });
                    formula_loading.resetTimeoutV2(1000);
                } else {
                    $('.fl-action-submit').each(function () {
                        $(this).removeClass('submit-event-blocker');
                    });
                    switcher_set_interval = 0;
                }
            }, 1000);
        }
    }, 1000);

    setInterval(function () {
        if ($('.data-formula-loader-noti').length >= 1 && switcher_set_interval == 0) {
            switcher_set_interval = 1;
            formula_loading.resetTimeoutV2(1000);
        }
    }, 0)
}

function formulaFieldLoader(setOBJ_field) {
    var self = $(setOBJ_field).filter(':visible');
    if (self.length >= 1) {
        var doi = self.attr('data-object-id');
        var append_loading_field_noti = $('<div class="data-formula-loader-noti" style="position: absolute;width: 99%;text-align: center;height: 28px;background-color: rgb(255, 255, 255);margin-top: 1px;margin-left: 1px;border-radius: 3px;"><div style="display:inline-block;"><span style="display:table-cell;vertical-align:middle;width:100%;height:28px;"><i class="fa fa-rotate-right fl-fieldloader"></i> Loading...</span></div></div>');
        var obj_fields_containment = self.find('[id="obj_fields_' + doi + '"]').eq(0);
        append_loading_field_noti.css({
            "height": (obj_fields_containment.find('.getFields[name]').eq(0).outerHeight() - 3) + "px"
        });
        if (obj_fields_containment.filter(':visible').length >= 1 && obj_fields_containment.children('.data-formula-loader-noti').length <= 0) {
            obj_fields_containment.filter(':visible').prepend(append_loading_field_noti);
            return append_loading_field_noti; //panel.load(append_loading_field_noti);
        }
    }
    return $([]);
}



function EmbedPopUpDisplayFixes() {
    var iframe_document = $(this);
    var url_request_id = iframe_document.find('#frmrequest').find('[name="ID"]').val();
    if (url_request_id == "0" || url_request_id == 0) { // kapang new request
        var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
        iframe_document.find('.fl-content').css('top', '0px');
        iframe_document.find('.fl-user-header-wrapper').remove();
        iframe_document.find('.display_form_status').remove();
        iframe_document.find('.form-action-navs').css({"position": "fixed", "top": "0px"});


    } else { //kapag di na new request
        iframe_document.find('.fl-user-header-wrapper').remove();
        iframe_document.find('.form-action-navs').css({"position": "fixed", "top": "0px"});
        //iframe_document.find('.fl-content').css('top', '40px'); // remove for now while developing new workspace
        iframe_document.find('.display_form_status').css('top', '40px');
        //iframe_document.find('.fl-user-navigation-wrapper').css('top', '40px');
    }
    setTimeout(function () {
        iframe_document.find('.submit-overlay').addClass('display');
    }, 100);

    console.log(iframe_document.find('.forPicklistData'));

}
function StrUpper(string_val) {
    if (typeof string_val == "string") {
        return string_val.toUpperCase();
    } else {
        return "StrLower:Not a string";
    }
}

function StrLower(string_val) {
    if (typeof string_val == "string") {
        return string_val.toLowerCase();
    } else {
        return "StrLower:Not a string";
    }
}

function StrLeft(str_val, str_length) {
    var str_to_process = String(str_val);
    return str_to_process.substr(0, str_length);
}

function StrRight(str_val, str_length) {
    var str_to_process = String(str_val);
    var start = (str_to_process.length - (str_length));
    if (start <= -1) {
        start = 0;
    }
    return str_to_process.substr(start, str_length);
}

function StrCount(str_val) {
    if (typeof str_val != "undefined") {
        return String(str_val).length;
    }
}

function GetAuth(str_key_entry) {
    var key_entries = ["FormAuthID", "FormAuthEmail"];
    var get_ele = $('[name="' + str_key_entry + '"]');
    if (key_entries.indexOf(str_key_entry) >= 0) {
        if (get_ele.length >= 1) {
            return get_ele.val();
        } else {
            return "";
        }
    } else {
        return "";
    }
}
var AutoTimer = new (function () {
    var pathname = window.location.pathname;
    if (pathname != "/user_view/workspace") {
        return;
    }
    // Stopwatch element on the page
    var $stopwatch;

    // Timer speed in milliseconds
    var incrementTime = 10;

    // Current timer position in milliseconds
    var currentTime = 0;

    // Start the timer
    $(function () {
        $stopwatch = $('[data-type="autoTimer"] .obj_label label');
        AutoTimer.Timer = $.timer(updateTimer, incrementTime, false);
        $('[data-type="autoTimer"]').draggable();
        $stopwatch.css("cursor", "pointer");

        $('.fl-timer-start').on('click', function () {
            AutoTimer.Timer.play(true);
            $(this).css('cursor', 'not-allowed');
        });

    });

    // Output time and increment
    function updateTimer() {
        var timeString = formatTime(currentTime);
        $stopwatch.html(timeString);
        currentTime += incrementTime;
    }
});
function GetCheckboxValue(field_element, get_type) {

    var sel_f_e = "";
    var collect_value = [];
    if (typeof field_element === "string") {
        sel_f_e = $('[name="' + field_element + '[]"]');
    } else if (typeof field_element === "object") {
        sel_f_e = $(field_element);
    }
    if (sel_f_e.length >= 1) {

        if (sel_f_e.prop("tagName") == "INPUT") {

            if (sel_f_e.attr("type") == "checkbox") {

                //EXTENSION OF DEFAULT COMPUTED EVENT
                if (the_field) { // bind event
                    if (typeof this_ini_variant === "object") {
                        if (typeof this_ini_variant["makeOnChange"] === "function") {
                            // console.log("EXTENSION",sel_f_e,the_field)
                            var skip_binding = false;
                            if (sel_f_e.data("events")) {
                                if (sel_f_e.data("events")["change"]) {
                                    // alert("data events change")
                                    if (sel_f_e.data("events")["change"].length >= 1) {
                                        // alert("data events change length")
                                        for (var index_d in sel_f_e.data("events")["change"]) {
                                            // alert("LOOP"+sel_f_e.data("events")["change"][index_d]["namespace"])
                                            if (sel_f_e.data("events")["change"][index_d]["namespace"] == "startUpDefaultValComputed") {
                                                // alert("namespace")
                                                skip_binding = true;
                                            }
                                        }
                                    }
                                }
                            }
                            if (skip_binding == false) {
                                this_ini_variant["makeOnChange"](sel_f_e, the_field, "startUpDefaultValComputed");
                            }
                        }
                    }
                }

                if (get_type == "checked") {
                    sel_f_e.filter(":checked").each(function () {
                        collect_value.push($(this).val());
                    })
                } else if (get_type == "not checked") {

                    sel_f_e.filter(":not(:checked)").each(function () {
                        collect_value.push($(this).val());
                    })
                }
                return collect_value.join("|^|");
            }
        }
    }

    return "";
}
// function StrConcat() {
//     var args = arguments;//infinite argument
//     var collect_strings = "";
//     for (var ctr = 0; ctr < args.length; ctr++) {
//         collect_strings = collect_strings + args[ctr];
//     }
//     return collect_strings;
// }
function GetRowIndex(element) {
    var dis_ele = $(element);
    var row_index = -1;
    if (dis_ele.hasClass("getFields")) {
        row_index = dis_ele.parents(".setOBJ").eq(0).parents("tr").eq(0).index();
    } else {
        row_index = dis_ele.parents("tr").eq(0).index();
    }
    return row_index;
}
// function AddValueField(field_name, value){
//     if($('[name="'+field_name+'"]').length == 1){
//         var field_ele = $('[name="'+field_name+'"]');
//         var field_container_ele = field_ele.parents(".setOBJ").eq(0);
//         var doi = field_container_ele.attr("data-object-id");
//         var get_fields_ele = field_container_ele.find(".getFields_"+doi);
//         var get_field_type = get_fields_ele.eq(0).attr("data-type");
//         var obj_type_field = field_container_ele.attr("data-type")


//         if(typeof obj_type_field !== "undefined"){
//             get_fields_ele.each(function(eqi){
//                 var self_dis = $(this)
//                 if(typeof value === "string"){
//                     if(obj_type_field == "dropdown"){
//                         self_dis.append('<option value="'+value+'">'+value+'</option>');
//                     }
//                 }else if(typeof value === "object"){
//                     $.each(value,function(){
//                         if(obj_type_field == "dropdown"){
//                             self_dis.append('<option value="'+value+'">'+value+'</option>');
//                         }
//                     });
//                 }
//             });
//         }
//     }else{

//     }
// }
function StrGet(str_val, str_istart, str_char_len) {
    if (typeof str_val == "undefined") {
        return "";
    }
    var string_val = String(str_val);
    var string_start_index = Number(str_istart);
    if (string_start_index <= 0) {
        return "";
    }
    if (typeof str_char_len == "undefined") {
        return String(string_val.charAt((Number(str_istart) - 1)));
    } else {
        return string_val.substr((Number(str_istart) - 1), Number(str_char_len));
    }
}

function printAllExistingFormulas() {
    var collect_in_json = [];
    $('[default-formula-value][default-type="computed"]').each(function () {
        collect_in_json.push({
            "this_ele_formula": $(this).attr("default-formula-value"),
            "this_ele": $(this),
            "object_id": $(this).closest(".setOBJ").attr("data-object-id")
        })
        console.log($(this).attr("default-formula-value"));
    });
    console.log("===========================================");
    console.log("check DOM here below!");
    return collect_in_json;
}


function SDev(fieldname) {
    var fn_str = fieldname;

    var nums = [];
    if ($.type(fn_str) == "string" || $.type(fn_str) == "array") {
        if ($.type(fn_str) == "array") {
            nums = fn_str;
        } else {
            nums.push(Number($('[name="' + fn_str + '"]').val()))

            $('[rep-original-name="' + fn_str + '"]').each(function () {
                nums.push(Number($(this).val()));
            });
        }


        var dataSD = {};
        var mean = 0;
        var mean_average = 0;
        var decimal = 1000000;

        dataSD.totalNumbers = nums.length;

        function getMean(nums) {
            for (var ii in nums) {
                mean = mean + parseFloat(nums[ii]);
            }
            mean = mean / nums.length;
            mean_average = Math.round(mean * decimal) / decimal;
            return Math.round(mean * decimal) / decimal;
        }
        dataSD.meanAverage = getMean(nums);

        var variance = 0;
        var varian = 0;

        function getVariance(nums) {
            for (var ii in nums) {
                variance = variance + Math.pow((parseFloat(nums[ii]) - mean), 2);
            }
            varian = variance / (nums.length - 1);
            varian = (isNaN(varian) == false) ? varian : 0;
            return Math.round(varian * decimal) / decimal;
        }
        dataSD.varianceSD = getVariance(nums);

        var sd = 0;

        function getStandardDeviation() {
            sd = Math.sqrt(varian);
            return Math.round(sd * decimal) / decimal;
        }
        dataSD.standardDeviation = getStandardDeviation();
        var pop = 0;

        function getPopulationSD(nums) {
            pop = variance / nums.length;
            pop_sqrt = Math.sqrt(pop);
            return Math.round(pop_sqrt * decimal) / decimal;
        }
        dataSD.populationSD = getPopulationSD(nums);

        var var_pop = 0;

        function getVariancePSD(nums) {
            var_pop = variance / nums.length;
            return Math.round(var_pop * decimal) / decimal;
        }
        dataSD.variancePSD = getVariancePSD(nums);
        return dataSD["standardDeviation"];

    } else {
        return 0;
    }
}
function Sum(sum_fn) {
    var summation = 0;
    if ($.type(sum_fn) == "string") {
        var fieldname = sum_fn;

        if (fieldname.indexOf("@") >= 0) {
            fieldname = fieldname.replace("@", "");
        }
        summation = summation + Number($('[name="' + fieldname + '"]').val().replace(/,/g, ""));

        $('[rep-original-name="' + fieldname + '"]').each(function (eqi) {
            summation = summation + Number($(this).val().replace(/,/g, ""));
        });
    }
    return summation;
}
function GetAVG(fns) {
    if ($.type(fns) == "string") {
        var fieldname = "@" + fns;
        var collect_values = [];
        var totality = 0;
        var counted_names = 0;
        console.log('asd', fieldname)
        if (fieldname.indexOf("@") >= 0) {
            fieldname = fieldname.replace("@", "");
            totality = totality + Number($('[name="' + fieldname + '"]').val());
            counted_names++;
            $('[rep-original-name="' + fieldname + '"]').each(function (eqi) {
                totality = totality + Number($(this).val());
                counted_names++;
            });
            if (counted_names == 0) {
                return 0;
            } else {
                return (totality / counted_names);
            }
        } else {

        }
    } else {
        return 0;
    }
}
function GetMax(fns) {
    if ($.type(fns) == "string") {
        var fieldname = fns;
        var collect_values = [];
        if (fieldname.indexOf("@") >= 0) {
            fieldname = fieldname.replace("@", "");
        }
        collect_values.push(Number($('[name="' + fieldname + '"]').val()));

        $('[rep-original-name="' + fieldname + '"]').each(function (eqi) {
            collect_values.push(Number($(this).val()));
        });
        if (collect_values.length == 0) {
            return "";
        } else {
            return (collect_values.sort(function (a, b) {
                return b - a
            }))[0];
        }
    } else {
        return "";
    }
}

function GetMin(fns) {
    if ($.type(fns) == "string") {
        var fieldname = fns;
        var collect_values = [];
        if (fieldname.indexOf("@") >= 0) {
            fieldname = fieldname.replace("@", "");
        }
        collect_values.push(Number($('[name="' + fieldname + '"]').val()));

        $('[rep-original-name="' + fieldname + '"]').each(function (eqi) {
            collect_values.push(Number($(this).val()));
        });

        if (collect_values.length == 0) {
            return "";
        } else {
            return (collect_values.sort(function (a, b) {
                return a - b
            }))[0];
        }
    } else {
        return "";
    }
}

//move to Formula.js CountDayIf 0654PM 10222015

function GetTimeInt(date_here) {
    var date_parse = new Date(date_here);
    if (date_parse == "Invalid Date") {
        return date_parse.getTime();
    } else {
        return date_parse.getTime();
    }
}

//functionality event
//{//OLD VERSION SLOW

function rebindFieldFunctionality(appended_row) { // defaultComputed(data_obj_id,the_field_ele,this_element,eventAddressNamespace){
    if (appended_row.find(".hasDatepicker").length >= 1) {
        appended_row.find(".hasDatepicker").each(function () {
            $(this).removeClass("hasDatepicker");
            if ($(this).attr("data-type")) {
                if ($(this).attr("data-type") == "date") {
                    $(this).datepicker({
                        'dateFormat': 'yy-mm-dd'
                    });

                } else if ($(this).attr("data-type") == "dateTime") {
                    $(this).datetimepicker({
                        'dateFormat': 'yy-mm-dd'
                    });
                }
            }
        });
    }
    appended_row.find(".setObject").each(function () {

    });
}
function rebindFieldFunctionalityV2(table) {
    var table_id = table.closest(".setOBJ").attr("data-object-id");
    table.find(".hasDatepicker").each(function (eqi) {
        // console.log($(this).data())
        // console.log(typeof $(this).data().datepicker);
        //DESTROY HERE
        if ($(this).data().datepicker) {
            $(this).datepicker("destroy");
        }
        //REBIND HERE
        if (typeof $(this).data().datepicker == "undefined") {
            $(this).removeClass("hasDatepicker");
            if ($(this).attr("data-type")) {
                if ($(this).attr("data-type") == "date") {
                    $(this).datepicker({
                        'dateFormat': 'yy-mm-dd'
                    });
                } else if ($(this).attr("data-type") == "dateTime") {
                    $(this).datetimepicker({
                        'dateFormat': 'yy-mm-dd'
                    });
                }
            }
        }
    });
    // console.log("STARTING... REBINDING OF COMPUTED DEFAULT VALUE FIELDS LIST THEN");

    //destroy first
    $(".getFields").off("change.repeaterDefaultValueComputed" + table_id);
    $(".getFields").off("keyup.repeaterDefaultValueComputed" + table_id);
    //"startUpDefaultValComputed"
    //makeOnChange(fieldAffected,fieldWithComputation,eventAddressNamespace)
    table.children("tbody").children("tr").each(function (eqi_tr) {
        $(this).find(".setOBJ").each(function (eqi_setOBJ) {
            var obj_id = $(this).attr("data-object-id");
            var this_setOBJFIELD = $(this).find(".getFields_" + obj_id);
            // console.log("REFRESH")
            // console.log(this_setOBJFIELD)
            var this_setOBJFIELD_name = this_setOBJFIELD.eq(0).attr("name");
            if (this_setOBJFIELD.data("events")) {
                if (this_setOBJFIELD.data("events").change) {
                    for (var ctr = 0; ctr < this_setOBJFIELD.data("events").change.length; ctr++) {
                        if (this_setOBJFIELD.data("events").change[ctr].namespace.toLowerCase().indexOf("startup") >= 0) {
                            return true;
                        }
                    }
                }
            }

            if (this_setOBJFIELD.data("events")) {
                if (this_setOBJFIELD.data("events").keyup) {
                    for (var ctr = 0; ctr < this_setOBJFIELD.data("events").keyup.length; ctr++) {
                        if (this_setOBJFIELD.data("events").keyup[ctr].namespace.toLowerCase().indexOf("startup") >= 0) {
                            return true;
                        }
                    }
                }
            }

            if (this_setOBJFIELD.attr("default-type")) { //PA ROW NORMAL lANG
                if (this_setOBJFIELD.attr("default-type") == "computed") {
                    //REBIND DEFAULT VALUES COMPUTED
                    //console.log("REFRESh");
                    //console.log(this_setOBJFIELD);
                    self_ini.defaultComputed(obj_id, this_setOBJFIELD, this, "repeaterDefaultValueComputed" + table_id);
                    // console.log( this_setOBJFIELD.parents(".setOBJ").eq(0) )
                }
            }
            //console.log("arrgh")
            //console.log(this_setOBJFIELD)
            if (typeof this_setOBJFIELD.attr("event-id-from") != "undefined") { //PACOLUMN FORMULA IS AT OUTER @fieldName[sum]
                var ids = this_setOBJFIELD.attr("event-id-from").split(",");
                var idEleWithFormula = "";
                for (var zz = 0; zz < ids.length; zz++) {
                    var idEleWithFormula = $(".getFields_" + ids[zz]);
                    self_ini.makeOnChange(this_setOBJFIELD, idEleWithFormula, "repeaterDefaultValueComputed" + table_id);
                }
                // var idEleWithFormula = $(".getFields_" + this_setOBJFIELD.attr("event-id-from"));
                // console.log("GEH NGA??")
                // console.log(this_setOBJFIELD)
                // makeOnChange(this_setOBJFIELD, idEleWithFormula, "repeaterDefaultValueComputed" + table_id);
            }

            //CURRENCY FORMAT
            if (this_setOBJFIELD.attr("data-input-type") == 'Currency') {
                var has_change_curr_format = false;
                var data_events = this_setOBJFIELD.data("events");
                if (typeof data_events != "undefined") {
                    if (typeof data_events["change"] != "undefined") {
                        $.each(this_setOBJFIELD.data("events")["change"], function (index, value) {
                            if (value["namespace"] == "currency_format") {
                                has_change_curr_format = true;
                                return false;
                            }
                        });
                    }
                }
                if (has_change_curr_format == false) {
                    this_setOBJFIELD.on("change.currency_format", function () {
                        if ($.isNumeric($(this).val())) {
                            var value_number = parseFloat($(this).val());
                            $(this).val(value_number.currencyFormat());
                        } else {
                            var value_nito = $(this).val()
                            var sanitized_val = value_nito.replace(/[\s,]/g, "");
                            if ($.isNumeric(sanitized_val)) {
                                var value_number = parseFloat(sanitized_val);
                                $(this).val(value_number.currencyFormat());
                            }
                        }
                    });
                }
            }


        });
    });
    table.find(".getFields").change();
}



function evRepeaterRemove(repeaterBtnEle) {
    if (repeaterBtnEle.children(".rep-remove-spec-row").length >= 1) {
        repeaterBtnEle.children(".rep-remove-spec-row").on({
            "click": function (e) {
                e.preventDefault();
                if ($(this).attr("disabled")) {

                } else {
                    $(this).closest("tr").children("td").animate({
                        "height": 0
                    }, function () {
                        var this_table = $(this).closest(".form-table");
                        $(this).closest("tr").remove();
                        this_table.find("tbody").eq(0).children("tr").eq(0).find(".getFields").filter(function (index, dom) { //UNANG ROW LANG ANG NAKA .CHANGE
                            var dis_ele_dom = $(this)
                            var ded_data_events = dis_ele_dom.data("events");
                            if (ded_data_events["change"]) {
                                if (ded_data_events["change"].length >= 1) {
                                    return true;
                                }
                            }
                            return false;
                        }).change();
                        // refreshRepeaterTable(this_table);
                        // rebindFieldFunctionalityV2(this_table);
                    });
                }
                if ($(this).parents("[table-response-to]").eq(0).length >= 1) {
                    var clicked_row_index = $(this).parents("tr").eq(0).index();
                    var tables_affected = $(this).parents("[table-response-to]").attr("table-response-to").split(",");
                    var collect_selection = null;
                    for (var ii = 0; ii < tables_affected.length; ii++) {
                        if (collect_selection == null) {
                            collect_selection = $("[table-name='" + tables_affected[ii] + "']")
                        } else {
                            collect_selection = collect_selection.add($("[table-name='" + tables_affected[ii] + "']"));
                        }
                    }
                    collect_selection.each(function (eqi) {
                        if ($(this).find("tbody").children("tr").eq(clicked_row_index).length >= 1) {
                            $(this).find("tbody").children("tr").eq(clicked_row_index).find(".rep-remove-spec-row").eq(0).trigger("click");
                        }
                    })
                }
                return false;
            }
        });
    }
}
function refreshRepeaterTable(tableEle) {
    var field_container = ".setOBJ";
    var table_body = $(tableEle).children("tbody");
    var table_row = table_body.children("tr");
    $(table_row).each(function (eqi_tr_i) {
        var total_field_disabled_in_row = $(this).find(".getFields:disabled").length;
        var total_field_in_row = $(this).find(".getFields").length;
        if (total_field_in_row == total_field_disabled_in_row) {
            $(this).find(".repeater-action").css("opacity", 0);
            $(this).find(".repeater-action").addClass("disable-repeater-action");
            $(".disable-repeater-action").children(".rep-remove-spec-row,.rep-add-spec-row").attr("disabled", true);
            $(".disable-repeater-action").children(".rep-remove-spec-row,.rep-add-spec-row").prop("disabled", true);
        }
        // if ($(this).hasClass("original-row")) {
        //     //
        // } else {
        //     $(this).find(field_container).each(function () {
        //         sobj_index_id = $(this).attr("data-object-id");
        //         $(this).find(".getFields_" + sobj_index_id).each(function () {

        //             if (!$(this).attr("rep-original-id")) {
        //                 $(this).attr("rep-original-id", $(this).attr("id"));
        //                 $(this).attr("id", $(this).attr("rep-original-id") + "_tr" + eqi_tr_i);
        //             } else {
        //                 //
        //                 $(this).attr("id", $(this).attr("rep-original-id") + "_tr" + eqi_tr_i);
        //             }

        //             if (!$(this).attr("rep-original-name")) {
        //                 $(this).attr("rep-original-name", $(this).attr("name"));
        //                 $(this).attr("name", $(this).attr("rep-original-name") + "_tr" + eqi_tr_i);
        //             } else {
        //                 //
        //                 $(this).attr("name", $(this).attr("rep-original-name") + "_tr" + eqi_tr_i);
        //             }

        //             //MAKE AN INDEX TOO ON FORMULAS
        //             // if($(this).attr("default-type")){
        //             //    if($(this).attr("default-type") == "computed"){
        //             //      var computed_default_value = $(this).attr("default-formula-value");
        //             //      var splitted_default_val = computed_default_value.match(/@\(.*?\)|@[a-zA-Z0-9_]*/g);
        //             //      if(splitted_default_val != null){
        //             //            console.log("FIGURE THIS OUT")
        //             //            for(var ctr = 0 ; ctr < splitted_default_val.length ; ctr++){
        //             //                  var formula_key_words = new Array("@Department","@Status","@Requestor","@RequestID","@Now","@Today","@TrackNo","@CurrentUser","@StrRight","@StrLeft","@GivenIf");
        //             //                  var ele_in_formula = $('[name="'+splitted_default_val[ctr].replace("@","")+'"]');
        //             //                  if(ele_in_formula.length >= 1){
        //             //                        console.log(ele_in_formula.data("events"));
        //             //                        var ele_in_f_val = $.trim(ele_in_formula.val() );
        //             //                        if($.isNumeric(ele_in_f_val ) ){
        //             //                             computed_default_value = computed_default_value.replace(splitted_default_val[ctr], ele_in_f_val);
        //             //                        }else{
        //             //                             computed_default_value = computed_default_value.replace(splitted_default_val[ctr], "\""+ele_in_f_val+"\"");
        //             //                        }
        //             //                  }else{
        //             //                         if(formula_key_words.indexOf(splitted_default_val[ctr]) >= 0 ){
        //             //                            if(splitted_default_val[ctr] == "@Requestor"){
        //             //                                 requestor_ele = "<?php echo $auth['display_name']; ?>";
        //             //                                 computed_default_value = computed_default_value.replace(splitted_default_val[ctr],"\""+requestor_ele+"\"" );
        //             //                            }else if(splitted_default_val[ctr] == "@RequestID"){
        //             //                                 var value_reqID = $("#ID").val();
        //             //                                 computed_default_value = computed_default_value.replace(splitted_default_val[ctr], value_reqID);
        //             //                            }else if(splitted_default_val[ctr] == "@Department"){
        //             //                                 var value_reqID = $("[name='Department_Name']").val();
        //             //                                 computed_default_value = computed_default_value.replace(splitted_default_val[ctr], value_reqID);
        //             //                            }else if(splitted_default_val[ctr] == "@RowIndex"){

        //             //                                 if($(this).parents("tr").length >=1){
        //             //                                     var rowIndex_val = $(this).parents("tr").eq(0).index();
        //             //                                     computed_default_value = computed_default_value.replace(splitted_default_val[ctr], rowIndex_val);
        //             //                                 }
        //             //                            }
        //             //                            continue;
        //             //                          }else{
        //             //                            var toBeReplace = splitted_default_val[ctr];
        //             //                            var formula_RegExp = new RegExp("("+toBeReplace+"|"+toBeReplace+"_tr[0-9]*)","g");
        //             //                            computed_default_value = computed_default_value.replace(formula_RegExp, splitted_default_val[ctr].replace(/_tr[0-9]*/g,"")+"_tr"+eqi_tr_i);
        //             //                          }
        //             //                  }
        //             //            }
        //             //      }
        //             //      var status_ok = "false";
        //             //      try{
        //             //         eval(computed_default_value);
        //             //         status_ok = "true";
        //             //      }catch(eerr){

        //             //      }
        //             //      if(status_ok== "true"){
        //             //         $(this).val(eval(computed_default_value));
        //             //      }
        //             //    }
        //             // }
        //         });

        //         $(this).find(".pickListButton").each(function () {
        //             if (!$(this).attr("original-return-field")) {
        //                 $(this).attr("original-return-field", $(this).attr("return-field"));
        //                 $(this).attr("return-field", $(this).attr("original-return-field") + "_tr" + eqi_tr_i);
        //             } else {
        //                 $(this).attr("return-field", $(this).attr("original-return-field") + "_tr" + eqi_tr_i);
        //             }
        //         });
        //     });
        // }
    });
    if (typeof callBack != "undefined") {
        callBack(tableEle);
    }
}
//refreshRepeaterTable(tableElement); // this excute at the startup of repeater table ... so that the rows with no fields 
//}

function removeDuplicates(arrayIn) {
    var arrayOut = [];
    for (var a = 0; a < arrayIn.length; a++) {
        if (arrayOut[arrayOut.length - 1] != arrayIn[a]) {
            arrayOut.push(arrayIn[a]);
        }
    }
    return arrayOut;
}

function collapseByColumn(dis_ele) {//COLUMN BASIS
    var selected_tbody = $(dis_ele).children("tbody").eq(0);
    var selected_trs = selected_tbody.children("tr");
    var total_columns = selected_trs.eq(0).children("td").length;
    for (var ii = 0; ii < total_columns; ii++) {
        var selected_col = $(selected_trs.children("td").eq(ii));
        selected_trs.each(function (eq_trs) {
            if (eq_trs != 0) {
                selected_col = selected_col.add($(this).children("td").eq(ii));
            }
        })


        var col_total_field_hidden = selected_col.find(".setOBJ").filter(function (index, dom_ele) {
            var looped_ele = $(dom_ele);
            var data_object_id = looped_ele.attr("data-object-id");
            var getFields_ele = looped_ele.find("#obj_fields_" + data_object_id);
            if (getFields_ele.length >= 1) {
                if (getFields_ele.find('[name]').length >= 1) {
                    if (looped_ele.css("display") == "none") {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;//wag iselect
            }
        });


        var col_total_field_setOBJ = selected_col.find(".setOBJ").filter(function (index, dom_ele) {
            var looped_ele = $(dom_ele);
            var data_object_id = looped_ele.attr("data-object-id");
            var getFields_ele = looped_ele.find("#obj_fields_" + data_object_id);
            if (getFields_ele.length >= 1) {
                if (getFields_ele.find('[name]').length >= 1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;//wag iselect
            }
        });

        console.log("COLUMN " + ii + "::", selected_col);
        console.log("COLUMN SETOBJ" + ii + "::", selected_col.find(".setOBJ"));
        if (col_total_field_setOBJ.length == col_total_field_hidden.length) {
            selected_col.stop(true, true);
            if (selected_col.css("display") != "none") {
                selected_col.attr("cVisibilityWidthTD", selected_col.filter(function (index, elem_dom) {
                    if ($(elem_dom).css("display") != "none") {
                        return true;
                    } else {
                        return false;
                    }
                }).outerWidth());

                // selected_col.animate({
                //     "width":0
                // },function(){
                //     console.log("NONE",$(this).css("display","none")    )
                // });//animate
                selected_col.css("display", "none");
            }

            // selected_col.stop(true,true);
            // if(selected_col.css("display") != "none"){
            //     $(this).data("rVisibilityHeightTD",$(this).outerHeight());
            // }
            // $(this).children('td').animate({
            //     "height":0
            // },function(){
            //     $(this).css("display","none");
            // })
        } else {
            // alert(selected_col.css("display"))
            // console.log("BABA",selected_col);
            // var showed_ele_td = selected_col.filter(function(index, dom_ele){
            //     if($(this).css("display") != "none" && !$(this).parents("tr").eq(0).data("rVisibilityHeightTD")){
            //         return true;
            //     }else{
            //         return false;
            //     }
            // }).length;


            // if(showed_ele_td == 0 ){

            // }else if(showed_ele_td >= 1){
            //     var load_td_width = selected_col.attr("cVisibilityWidthTD");
            //     selected_col.css("display","table-cell");
            // }
            var showed_ele_td = selected_col.filter(function (index, dom_ele) {
                if ($(this).css("display") != "none" && !$(this).parents("tr").eq(0).data("rVisibilityHeightTD")) {
                    return true;
                } else {
                    return false;
                }
            }).length;
            if (showed_ele_td == 0) {
                var load_td_width = selected_col.attr("cVisibilityWidthTD");
                selected_col.css("display", "table-cell");
            }
        }
    }

    // selected_trs.each(function(eq_trs){

    //     var selected_tds = $(this).children("td");
    // })
    return {};
}
function collapseByRow(dis_ele) {
    var collapse_elements = {
        "elem_to_collapse": null,
        "elem_to_expand": null
    };
    var selected_tbody = $(dis_ele).children("tbody").eq(0);
    var selected_trs = selected_tbody.children("tr");
    selected_trs.each(function (eq_trs) {
        var row_total_field_setOBJ = $(this).find(".setOBJ")
                .filter(function (index, dom_ele) {
                    var looped_ele = $(dom_ele);
                    var data_object_id = looped_ele.attr("data-object-id");
                    var getFields_ele = looped_ele.find("#obj_fields_" + data_object_id);
                    if (getFields_ele.length >= 1) {
                        if (getFields_ele.find('[name]').length >= 1) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;//wag iselect
                    }
                });


        var row_total_field_hidden = $(this).find(".setOBJ")
                .filter(function (index, dom_ele) {
                    var looped_ele = $(dom_ele);
                    var data_object_id = looped_ele.attr("data-object-id");
                    var getFields_ele = looped_ele.find("#obj_fields_" + data_object_id);
                    if (getFields_ele.length >= 1) {
                        if (getFields_ele.find('[name]').length >= 1) {
                            if (looped_ele.css("display") == "none") {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;//wag iselect
                    }
                });
        // console.log(eq_trs,"GRAA",row_total_field_setOBJ," === ",row_total_field_hidden);
        if (row_total_field_setOBJ.length >= 1) {
            if (row_total_field_setOBJ.length == row_total_field_hidden.length) {
                $(this).children('td').stop(true, true);
                if ($(this).children('td').css("display") != "none") {
                    $(this).data("rVisibilityHeightTD", $(this).outerHeight());
                    // $(this).children('td').animate({
                    //     "height":0
                    // },function(){
                    //     $(this).css("display","none");
                    // })//ANIMATE
                    $(this).children('td').css({
                        "display": "none"
                    })
                    if (collapse_elements["elem_to_collapse"] != null) {
                        collapse_elements["elem_to_collapse"] = $(collapse_elements["elem_to_collapse"].add($(this).children('td')));
                    } else {
                        collapse_elements["elem_to_collapse"] = $($(this).children('td'));
                    }
                }
            } else {
                if ($(this).data("rVisibilityHeightTD")) {
                    height_rTD = $(this).data("rVisibilityHeightTD");
                    $(this).children('td').css("display", "table-cell");
                    // $(this).children('td').filter(function(index, ele_dom){if($(ele_dom).attr("cvisibilitywidthtd")){return false;}else{return true;} }).stop(true,true).animate({
                    //     "height":height_rTD
                    // },function(){
                    //     $(this).css("display","table-cell");
                    // }) //ANIMATE
                    $(this).children('td').filter(function (index, ele_dom) {
                        if ($(ele_dom).attr("cvisibilitywidthtd")) {
                            return false;
                        } else {
                            return true;
                        }
                    }).css({
                        "display": "table-cell"
                    }) //STATIC
                    if (collapse_elements["elem_to_expand"] != null) {
                        collapse_elements["elem_to_expand"] = $(collapse_elements["elem_to_expand"].add($(this).children('td').filter(function (index, ele_dom) {
                            if ($(ele_dom).attr("cvisibilitywidthtd")) {
                                return false;
                            } else {
                                return true;
                            }
                        })));
                    } else {
                        collapse_elements["elem_to_expand"] = $($(this).children('td').filter(function (index, ele_dom) {
                            if ($(ele_dom).attr("cvisibilitywidthtd")) {
                                return false;
                            } else {
                                return true;
                            }
                        }));
                    }
                } else {
                    $(this).children('td').filter(function (index, ele_dom) {
                        if ($(ele_dom).attr("cvisibilitywidthtd")) {
                            return false;
                        } else {
                            return true;
                        }
                    }).css("display", "table-cell");

                }
            }
        }
    })
    return collapse_elements;
}
function checkStaticTablesGAP() {

    $(".form-table.thisDynamicTable").each(function (eq_table) {
        var row_elems = collapseByRow($(this));
        // var col_elems = collapseByColumn($(this));


        // if(row_elems["elem_to_collapse"] != null){
        //     row_elems["elem_to_collapse"].each(function(){
        //         $(this).parents("tr").eq(0).attr("rVisibilityHeightTD",$(this).parents("tr").eq(0).outerHeight());
        //     })
        //     row_elems["elem_to_collapse"].animate({
        //         height:0
        //     },function(){
        //         if(row_elems["elem_to_expand"] != null){
        //             row_elems["elem_to_expand"].animate({
        //                 height:62   
        //             })
        //         }
        //     })
        // }
        // if(row_elems["elem_to_expand"] != null){
        //     row_elems["elem_to_expand"].animate({
        //         height:62   
        //     })
        // }
    })
}
//added by japhet morada
//hiding the button if the status is "Cancelled"
//#FS7742
function hideActionButtonsByStatus(status) {
    if (status == 'Cancelled') {
        $('.fl-action-submit').hide();
    }
}
//========================================================

$(document).ready(function () {

    var isSafari = navigator.vendor.indexOf("Apple") == 0 && /\sSafari\//.test(navigator.userAgent); // true or false
    if (isSafari) {
        $('.fl-links-option-category').addClass('safariBrowser');
    }
    ;

    var pathname = window.location.pathname;
    if (pathname == "/user_view/workspace") { //original pathname form_workspace_test for display fixes
        AutoCompletePicklist.init();
        initializePage.init();

        var isInIframe = (window.location != window.parent.location) ? true : false;

        if (isInIframe) {
            $('.fl-user-header-wrapper').hide();
            $('.show-menu-workspace').hide();
            $('.fl-backhistory').hide();
            $('.fl-workspace-fullscreen').hide();
            $('.form-action-navs').css('top', '0');
            $('.display_form_status').css('top', '40px');
            //$('.fl-content').css('top', '40px'); /kapag naka iframe

            if ($('body').hasClass('fl-new-request-form')) {
                $('.fl-generic-control').css('top', '40px'); //fl-submitted-view-request-form
                //$('.fl-content').css('top', '40px'); // remove for now while developing new workspace
            }
            ;

            if ($('body').hasClass('fl-submitted-view-request-form')) {
                $('.fl-generic-control').css('top', '84px');
                $('.fl-content').css('top', '82px');
            }

        }
        /*var windowLocation = window.location;
         if (getParametersName("fromPrintButton", windowLocation) == "true") {
         // initializePage.print_content("", "");
         var commentsFlag = getParametersName("comment", windowLocation);
         var logsFlag = getParametersName("log", windowLocation)
         initializePage.getLogs(function (logs) {
         initializePage.getComments(function (comments) {
         if (commentsFlag == "-1") {
         comments = "";
         }
         if (logsFlag == "-1") {
         logs = "";
         }
         initializePage.print_content(logs, comments);
         
         })
         })
         }*/
    }

})





var pathname = window.location.pathname;
if (pathname == "/user_view/workspace") { //original pathname workspace for display fixes
    //PROGRESS BAR ADDITIONALS
    var loading_progress_added = -1;
    var initializePage = {
        "init": function () {
            console_time.start("start_count");
            if (window.embedWindowDataSending) {
                window.embedWindowDataSending(window);
            }
            var self = this;
            //sequence here

            self.disableAllFields();

            var collect_ajax = [];

            console_time.start('getFormJson');
            collect_ajax.push(this.getFormJson($("#FormID").val()));
            console_time.end('getFormJson');


            // console_time.start('getRequestDetails');
            // collect_ajax.push(this.getRequestDetails());
            // console_time.end('getRequestDetails');


            console_time.start('getServerTime');
            collect_ajax.push(this.getServerTime());
            console_time.end('getServerTime');



            console_time.start('collect_ajax');
            self.pageLoader(collect_ajax, function () {
                console_time.end('collect_ajax');

                console_time.start('getRequestDetails');
                self.getRequestDetails();
                console_time.end('getRequestDetails');

                console_time.start('readonlyIfNotProcessor');
                self.readonlyIfNotProcessor();
                console_time.end('readonlyIfNotProcessor');

                console_time.start('displayFixes');
                self.displayFixes();

                console_time.end('displayFixes');


                console_time.start('loadImage');
                self.loadImage();
                console_time.end('loadImage');

                console_time.start('evtSubmitRequest');
                self.evtSubmitRequest();
                console_time.end('evtSubmitRequest');

                console_time.start('addRefreshEvent');
                self.addRefreshEvent();
                console_time.end('addRefreshEvent');

                // self.visibilityOnChange();
                // self.setFieldsVisibility();

                console_time.start('setDefaultValues');
                self.setDefaultValues();
                console_time.end('setDefaultValues');


                //formula events
                self.FormulaEventController("onload");

                self.FormulaEventController("onchange");

                console_time.start('populateFormula');
                self.populateFormula();
                console_time.end('populateFormula');

                console_time.start('visibilityOnChange2');
                self.visibilityOnChange2();
                console_time.end('visibilityOnChange2');

                console_time.start('hyperLinkFormula');
                self.hyperlinkFormula();
                console_time.end('hyperLinkFormula');

                console_time.start('readOnlyFormula');
                self.readOnlyFormula();
                console_time.end('readOnlyFormula');
                // self.disabledFormula2();
                console_time.start('setData');
                self.setData();
                console_time.end('setData');

                console_time.start('embeded_view');
                embeded_view.init();
                console_time.end('embeded_view');

                console_time.start('smart_barcode_scan');
                smart_barcode_scan.init();
                console_time.end('smart_barcode_scan');

                console_time.start('qr_code_scanner');
                qr_code_scanner.init();
                console_time.end('qr_code_scanner');


                console_time.start('detail_panel');
                detail_panel.init();
                console_time.end('detail_panel');

                console_time.start('text_tagging');
                text_tagging.init();
                console_time.end('text_tagging');

                console_time.start('addCommentRequest');
                self.addCommentRequest();
                console_time.end('addCommentRequest');


                console_time.start('printForm');
                self.printForm(); // Print Form
                console_time.end('printForm');


                console_time.start('removeFiles');
                self.removeFiles(); // download attachment
                console_time.end('removeFiles');


                console_time.start('picklist');
                picklist.init();
                console_time.end('picklist');

                console_time.start('names');
                names.init();
                console_time.end('names');


                console_time.start('preparePrintDisplayFixes');
                self.preparePrintDisplayFixes();
                console_time.end('preparePrintDisplayFixes');
                formulaOnload = true;

                console_time.start('commitChangesDefaultVal');
                self.commitChangesDefaultVal();
                console_time.end('commitChangesDefaultVal');

                //added by japhet morada #FS7742
                hideActionButtonsByStatus($('[name="Status"]').val());
                //======================================================

                blockActionWhileComputing();

                console_time.end("start_count");
            });
            this.addViewer();
            this.getUserViewer();
            this.seeAllUsers_AddViewers();
            if (pathname == '/user_view/workspace') {
                bind_onBeforeOnload(1);
            }
        },
        "readonlyIfNotProcessor": function () {
            var current_user = $('[name="CurrentUser"]').val();

            var editors = String($("#Editor").val()).split(',');
            if (editors.indexOf(current_user) >= 0) {
                return false;
            }
            var processor = $('[name="Processor"]').val().split(',');

            var all_input_on_the_form = $('.loaded_form_content').find(':input.getFields').not('button');
            if (!(processor.indexOf(current_user) >= 0)) {
                all_input_on_the_form.attr("disabled", "disabled");
                $('.input_position_below').find('span a i.fa-list-alt').css('display', 'none');
            }
        },
        "loadImage": function () {
            // var selection = $(".form_upload_photos").find("img.getFields");

            // selection.each(function() {
            //     var value_img = $(this).attr("src");
            //     if (value_img != "/images/avatar/large.png") {
            //         $(this).attr("src", "/" + value_img);
            //     }

            // });
        },
        "pageLoader": function (passArrayOfAjax, PL_Callback) {
            if (typeof PL_Callback != "undefined") {
                //catch undefined
            }

            var self = this;
            var args_ajax = arguments;//infinite argument

            {//aayusin ung form depende kapag create request or view request
                var pathname = window.location;
                if ($('#allow_default_actions').text() !== '0') {
                    if (getParametersName("requestID", pathname) == "0") {//new request
                        //$('body').addClass('fl-new-request-form'); // remove for now for developing new workspace structure
                    } else {
                        //$('body').addClass('fl-submitted-view-request-form'); //remove for now for developing new workspace structure
                    }
                }
            }
            {//aayusin ung form


                $('.display_form_status').css({
                    'top': '80px',
                    'z-index': '9',
                    'position': 'fixed',
                    'width': '100%'

                });

//                  $('.fl-content').css({
//                    'top': '40px'
//                }); //remove for now for developing new workspace structure


                if ($('.display_form_status').is(':visible')) {
                    $('.fl-content').css({
                        'top': '75px'
                    });
                }

                if ($('body').hasClass('fl-new-request-form')) {
                    $('.fl-content').css({
                        'top': '85px'
                    });
                }
                ;

                if ($('body').hasClass('fl-submitted-view-request-form')) {
                    $('.fl-content').css({
                        'top': '120px'
                    });
                }

            }


            if ($(".page-loader-overlay").length >= 1) {
                return $(".page-loader-overlay");
            } else {
                var page_loader =
                        $('<div class="page-loader-overlay">' +
                                '<div class="align-middle">' +
                                '<div class="loader-bar-container align-center" style="text-align:center;">' +
                                '<div class="spinner load_m" style="display:inline-block;">' +
                                '<div class="bar1"></div>' +
                                '<div class="bar2"></div>' +
                                '<div class="bar3"></div>' +
                                '<div class="bar4"></div>' +
                                '<div class="bar5"></div>' +
                                '<div class="bar6"></div>' +
                                '<div class="bar7"></div>' +
                                '<div class="bar8"></div>' +
                                '<div class="bar9"></div>' +
                                '<div class="bar10"></div>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>');

                $("html").prepend(page_loader);
                var ajaxArray = (typeof passArrayOfAjax === "undefined") ? [] : passArrayOfAjax;

                var loader_bar_container = $(".page-loader-overlay").find(".loader-bar-container");

                //SET DEFAULT AJAX HERE
                //SAMPLE: ajaxArray.push(self.getServerTime()); 
                //EVERY TIME this.pageLoader calls, the set ajax here executes

                loading_progress_added = 100 / ajaxArray.length;

                //SET WHEN TO FADEOUT THE PAGE LOADING
                var vi = new SetMyTimer(function () {
                    var loader_bar_container = $(".page-loader-overlay").find(".loader-bar-container");
                    var parent_outerW = loader_bar_container.outerWidth();
                    //EXECUTE ALL AJAX HERE $.when
                    // console.log("ETO NA ANG WHENN",ajaxArray);
                    $.when.apply("args_variable_name", ajaxArray).done(function () {
                        //DO SOMETHING WHEN ALL AJAX IS DONE
                        if (typeof PL_Callback != "undefined") {
                            this.page_loader = page_loader;
                            PL_Callback.call(this);
                        }
                        //LOADING OVERLAY FADEOUT
                        // var loading_containerW = loader_bar_container.outerWidth();
                        // loader_bar_container.find(".label").text("Loading 100%");
                        // loader_bar_container.find(".loader-filler").stop(false, false).animate({
                        //     "width": (parent_outerW)
                        // }, function() {
                        //     page_loader.fadeOut(function() {
                        //         $(this).remove();
                        //     });
                        // })


                        loader_bar_container.closest('.page-loader-overlay').fadeOut(function () {
                            $(this).remove();
                        });
                    });
                    $("html").unbind("DOMSubtreeModified");
                    // this.stop();
                }, 1000);
                var counter_modif = 0;
                $("html").bind("DOMSubtreeModified", function () {
                    if (counter_modif == 0) {
                        // vi.start();
                    } else {
                        vi.resetTimeout(100); //ADD TIME WHEN THE DOM's ARE BEING MODIFIED BY JAVASCRIPT?
                    }
                    counter_modif++;
                    // console.log("LOADING HTML CONTENTS MODIFICATIONS COUNTED: ", counter_modif);
                });

                {
                    // var vi = setVariableInterval(function () {
                    //     var loader_bar_container = $(".page-loader-overlay").find(".loader-bar-container");
                    //     var parent_outerW = loader_bar_container.outerWidth();
                    //     //EXECUTE ALL AJAX HERE $.when
                    //     // console.log("ETO NA ANG WHENN",ajaxArray);
                    //     $.when.apply("args_variable_name", ajaxArray).done(function () {
                    //         //DO SOMETHING WHEN ALL AJAX IS DONE
                    //         if (typeof PL_Callback != "undefined") {
                    //             this.page_loader = page_loader;
                    //             PL_Callback.call(this);
                    //         }
                    //         //LOADING OVERLAY FADEOUT
                    //         // var loading_containerW = loader_bar_container.outerWidth();
                    //         // loader_bar_container.find(".label").text("Loading 100%");
                    //         // loader_bar_container.find(".loader-filler").stop(false, false).animate({
                    //         //     "width": (parent_outerW)
                    //         // }, function() {
                    //         //     page_loader.fadeOut(function() {
                    //         //         $(this).remove();
                    //         //     });
                    //         // })


                    //         loader_bar_container.closest('.page-loader-overlay').fadeOut(function () {
                    //             $(this).remove();
                    //         });
                    //     });
                    //     $("html").unbind("DOMSubtreeModified");
                    //     this.stop();
                    // }, 1000);

                    // var counter_modif = 0;
                    // $("html").bind("DOMSubtreeModified", function () {
                    //     if (counter_modif == 0) {
                    //         vi.start();
                    //     } else {
                    //         vi.interval = 100; //ADD TIME WHEN THE DOM's ARE BEING MODIFIED BY JAVASCRIPT?
                    //     }
                    //     counter_modif++;
                    //     // console.log("LOADING HTML CONTENTS MODIFICATIONS COUNTED: ", counter_modif);
                    // });
                }
                return page_loader;
            }

        },
        "displayFixes": function () {
            var self = this;

            // if needed a input length validation
            // $('.loaded_form_content').find('input[type="text"]').on("change.TooltipValidation",function(){
            //     var self = $(this)

            //     var selfInputFieldWidth = self.width();
            //     var selfInputFieldVal = self.val();
            //     var getInputvalueWidth = $('#getInputvalueWidth');
            //     var selfInputFieldValWidth = getInputvalueWidth.width();
            //     getInputvalueWidth.append(selfInputFieldVal)
            //     consolelog(selfInputFieldValWidth);

            // });

            var form_content = $(".fl-body-content-wrapper");
            $('.setOBJ[data-type="radioButton"]').find("input").css("height", "auto");
            $('.setOBJ[data-type="checkbox"]').find("input").css("height", "auto");
            $('.embed-view-container').css('height', "inherit");
            $('.setOBJ').css('box-shadow', "");
            $('.label_below.obj_label').children('label').css('cursor', '');
            //fixing header contents and actions
            var fixes_location = $("body .fl-maincontainer")

            $("body .bgs-header").css("position", "relative").appendTo(fixes_location);
            //$("body .form-name-title").css("position","relative").appendTo(fixes_location);
            $("body .form-action-navs").appendTo(fixes_location);
            $("body .display_form_status").appendTo(fixes_location);//$("body .display_form_status").css("position", "relative").appendTo(fixes_location);

            //removing unnecessary ui for request client side
            //ui fixes for dynamic table
            if ($(".form-table[border-visible='No']").length >= 1) {
                var ele_style_table = $(
                        "<style>" +
                        ".form-table[border-visible='No']>tbody>tr>td{" +
                        "border: none !important;" +
                        "}" +
                        "</stlyle>"
                        );
                $("body").prepend(ele_style_table);
            }



            //added by roni 04-21-2015
            //revised by michael 7 31 2015 1:22 PM
            //Fixing fields for Workspace view only 
            var form_json_data = $('body').data("user_form_json_data");
            if (form_json_data) {
                form_json_data = form_json_data['form_json'];
            }

            var setOBJ = $('.getFields[disabled="disabled"],.getFields[readonly="readonly"]')
                    .closest('.setOBJ:not([data-type="table"])')
                    .filter(function () {
                        var setOBJID = $(this).attr('data-object-id');
                        if (form_json_data[setOBJID]) {
                            if (form_json_data[setOBJID]["setReadOnlyField"] == "Yes") {
                                return true;
                            }
                        }
                        return false;
                    });
            /*
             For old picklist - aug 5 2015 roni
             */



            //console.log(setOBJ);

            function disabledField(self) {
                self.css({
                    'display': 'none'
                });
            }

            setOBJ.each(function () {

                var setOBJID = $(this).attr('data-object-id');

                var fieldId = $(this).find('.getFields_' + setOBJID);

                var getFields = $('#obj_fields_' + setOBJID).find($('.getFields_' + setOBJID + ':not([type="checkbox"]):not([type="radio"])')); //get fields with set object id inside object fields width id


                var getFieldsAll = $('#obj_fields_' + setOBJID).find($('.getFields_' + setOBJID));
                var dataType = $(this).attr("data-type");

                $(getFields).focus(function () {
                    $(this).blur();
                });

                if (dataType == "imageOnly") {
                    var imageSrc = $(this).find('img.getFields').attr('src');
                    if (imageSrc) {
                        if (imageSrc.indexOf('/') != 0) {
                            var imageArr = imageSrc.split('/');
                            console.log('imageArr', imageArr);
                            delete imageArr[0];
                            delete imageArr[1];
                            delete imageArr[2];
                            imageArr = imageArr.filter(Boolean);
                            var newSrc = imageArr.join('/');
                            newSrc = "/" + newSrc;
                            $(this).find('img.getFields').attr('src', newSrc);

                        }
                    }

                }
                if (dataType == "listNames") {
                    var getFieldsNames = $(this).find('.viewNames');
                    //console.log("setOBJID",setOBJID)
                    //console.log(getFieldsNames);
                    disabledField(getFieldsNames);
                }

                if (dataType == "attachment_on_request" || dataType == "multiple_attachment_on_request") {
                    var getFieldsInputFIle = $(this).find('input[type="file"]');
                    disabledField(getFieldsInputFIle);
                }

                if (dataType == "dropdown") {
                    //getFields.css({'margin-left': '-4px'}); remove bec of object custom css
                }

                if (dataType == "textbox") {
                    // getFields.css({'height': 'auto'}); remove bec of object custom css
                    getFields.attr('title', getFields.val());

                }

                if (dataType == "datepicker") {
                    getFields.attr('title', getFields.val());
                }
                if (dataType == "time") {
                    getFields.attr('title', getFields.val());

                }
                ;

                if (dataType == "dateTime") {
                    getFields.attr('title', getFields.val());
                }

                if (dataType == "qr-code") {
                    getFields.attr('title', getFields.val());
                }

                if (dataType == "barCodeScanner") {
                    getFields.attr('title', getFields.val());
                }

                if (dataType == "textbox_editor_support") {
                    getFields.attr('title', getFields.val());
                }

                if (dataType == "textbox_reader_support") {
                    getFields.attr('title', getFields.val());
                }

                if (dataType == "computed") {
                    getFields.attr('title', getFields.val());
                }

                if (dataType == "pickList") {
                    getFields.attr('style', getFields.attr('style') + ";cursor:auto !important");
                    getFields.attr('title', getFields.val());
                    getFields.siblings('span').remove();
                }

                if (dataType == "listNames") {
                    getFields.attr('title', getFields.val());
                    // getFields.siblings('span').remove(); //japhet issue
                }

                if (dataType == "dropdown") {
                    getFields.attr('title', getFields.val());
                }

                if (dataType == "textArea") {
                    getFields.attr('title', getFields.val());
                }

                if (dataType == "selectMany") {
                    getFields.attr('title', getFields.val());
                }

                if (dataType == "checkbox") {
                    getFieldsAll.attr('style', getFields.attr('style') + ";cursor:auto !important");
                    getFieldsAll.each(function () {
                        $(this).closest("label").find('.checkbox-input-label').css({'color': '#808080'});
                    })
                }
                ;

                if (dataType == "radioButton") {
                    getFieldsAll.attr('style', getFields.attr('style') + ";cursor:auto !important");
                    getFieldsAll.each(function () {
                        $(this).closest("label").find('.radio-input-label').css({'color': '#808080'});
                    })
                }
                ;

                if (dataType == "multiple_attachment_on_request") {
                    //console.log(getFields.parents('.input_position_below').find('a'));  
                    if (getFields.parents('.input_position_below').find('a').length == 0) {


                        var getFieldsCss = getFields.css('font-family')
                        var noFileAttached = $("<div style='font-family:" + getFieldsCss + ";color:#808080;'>No file attached.</div>");
                        noFileAttached.insertAfter(getFields);

                    }
                    ;
                    getFields.parents('.input_position_below').addClass('clearfix');
                }

                if (dataType == "attachment_on_request") {

                    //console.log(getFields.parents('.input_position_below').find('a'));  
                    if (getFields.parents('.input_position_below').find('a').length == 0) {
                        var getFieldsCss = getFields.css('font-family')
                        var noFileAttached = $("<div style='font-family:" + getFieldsCss + ";color:#808080;'>No file attached.</div>");
                        noFileAttached.insertAfter(getFields);
                    }
                    ;
                    getFields.parents('.input_position_below').addClass('clearfix');
                }
                ;

                fieldId.parents('.fields_below').css({
                    'display': 'static'
                });

                getFields.css({
                    // 'padding': '0px',
                    // 'margin-top': '-5px',
                    '-moz-appearance': 'none',
                    '-webkit-appearance': 'none',
                    'appearance': 'none'
                });




            });

            //Custom object css
            //Roni Pinili
            { // version 1
                // var styleTag = $('<style type="text/css" class="object_css"></style>');
                // $(styleTag).appendTo('head');

                // $('.setOBJ').each(function () {

                //     var setOBJId = $(this).attr('data-object-id');

                //     if ($('body').data("user_form_json_data")['form_json'][setOBJId]) {

                //         var obj_objectCssValue = $('body').data("user_form_json_data")['form_json'][setOBJId].obj_objectCss;
                //         $(styleTag).append(obj_objectCssValue)
                //         //console.log("VALUE", obj_objectCssValue);

                //     }

                // });
            }
            { // version 2
                var styleTag = '<style type="text/css" class="object_css">';
                // console_time.start("DISPLAY FIXES TEST 3.1");
                $('.setOBJ').each(function () {
                    var setOBJId = $(this).attr('data-object-id');
                    if ($('body').data("user_form_json_data")['form_json'][setOBJId]) {
                        styleTag += (form_json_data[setOBJId].obj_objectCss||'')+'\n'; //'<style type="text/css" class="object_css object_css_'+setOBJId+'">'+(form_json_data[setOBJId].obj_objectCss||'')+'</style>';
                    }
                });
                styleTag += '</style>';
                // console_time.end("DISPLAY FIXES TEST 3.1");
                // console_time.start("DISPLAY FIXES TEST 3.2");
                $('head').eq(0).append(styleTag);
                // console_time.end("DISPLAY FIXES TEST 3.2");
            }


            if ($('[name="ID"][id="ID"]').val() == 0) {
                $('.workspace_option_tab').find('.viewAuditLogs').addClass("isDisplayNone");
                $('.workspace_option_tab').find('.viewAuditLogs').css('display', 'none');
            }

            if ($('.forma-colorpicker').length >= 1) {
                var temps = $('.forma-colorpicker');
                temps.map(function () {
                    return $(this).next()[0];
                }).remove();
                temps.spectrum(default_spectrum_settings);
            }


            if ($('[field-visible="computed"][visible-formula]').length >= 1) {
                $('[field-visible="computed"][visible-formula]').each(function () {
                    if ($(this).attr("visible-formula") == "" || $(this).attr("visible-formula") == "[,]" || $(this).attr("visible-formula") == "[]") {
                        return true;
                    }
                    if ($(this).is('.embed-view-container[data-action-event-visibility="true"]')) {
                        var se_obj = $(this).parents(".setOBJ");
                        se_obj.find(".embed_newRequest")
                                .add(se_obj.find(".viewTDEmbedded"))
                                .add(se_obj.find(".copyTDEmbedded"))
                                .add(se_obj.find(".pasteTDEmbedded"))
                                .add(se_obj.find(".cancelpasteDEmbedded"))
                                .add(se_obj.find(".deleteTDEmbedded")).hide()
                    } else if ($(this).is('.embed-view-container[data-embed-view-visibility="true"]')) {
                        $(this).parents(".setOBJ").eq(0).hide();
                    }
                });
                //FS#6855
                var visibilities_element = $('[field-visible="computed"][visible-formula]:not([visible-formula=""])').filter(function () {
                    var self = $(this);
                    var visible_formula = "";
                    visible_formula = self.attr("visible-formula") || "";
                    visible_formula = visible_formula.replace(/(\/\/.*\n|\/\/.*$|\/\/.*\r\n|\/\/.*\r)/g, "");
                    visible_formula = $.trim(visible_formula);
                    if (visible_formula.length >= 3) {
                        return true;
                    } else {
                        return false;
                    }
                }).closest(".setOBJ").not('[data-type="embeded-view"]')

                if (visibilities_element.is('[data-type="accordion"]')) {
                    // visibilities_element.hide();
                } else {
                    visibilities_element.each(function () {
                        if ($(this).is('[data-type="attachment_on_request"]')) {
                            $(this).find('form:eq(0)').hide();
                        } else if ($(this).is('[data-type="multiple_attachment_on_request"]')) {
                            $(this).find('form:eq(0)').hide();
                        } else {
                            $(this).hide();
                        }
                    });
                }


            }
            //added by japhet morada=================================
            if (document.getElementsByClassName('dynamic-td-droppable-selected').length >= 1) {

                $('.dynamic-td-droppable-selected').removeClass('dynamic-td-droppable-selected');
            }
            if (document.getElementsByClassName('scroll-resize-padding').length >= 1) {

                $('.scroll-resize-padding').remove();

            }
            if (document.getElementsByClassName('setObject-drag-handle').length >= 1) {
                $(".setObject-drag-handle").remove();
            }
            if (document.getElementsByClassName('setObjects_actions').length >= 1) {
                $(".setObjects_actions").remove();

            }
            if (document.getElementsByClassName('form-builder-ruler').length >= 1) {
                $(".form-builder-ruler").remove();

            }
            if (document.getElementsByClassName('ui-resizable').length >= 1) {

                $(".ui-resizable").removeClass("ui-resizable");
            }
            if (document.getElementsByClassName('ui-droppable').length >= 1) {

                $(".ui-droppable").removeClass("ui-droppable");
            }
            if (document.getElementsByClassName('ui-draggable').length >= 1) {

                $(".ui-draggable").removeClass("ui-draggable");
            }
            if (document.getElementsByClassName('ui-resizable-handle').length >= 1) {

                $(".ui-resizable-handle").remove();
            }
            if (document.getElementsByClassName('component-ancillary-focus').length >= 1) {

                $(".component-ancillary-focus").removeClass("component-ancillary-focus");
            }
            if (document.getElementsByClassName('component-primary-focus').length >= 1) {

                $(".component-primary-focus").removeClass("component-primary-focus");
            }

            // if ($('.dynamic-td-droppable-selected').length >= 1) {
            //     $('.dynamic-td-droppable-selected').removeClass('dynamic-td-droppable-selected');
            // }
            // if ($('.scroll-resize-padding').length >= 1) {
            //     $('.scroll-resize-padding').remove();
            // }
            // if ($(".setObject-drag-handle").length >= 1) {
            //     $(".setObject-drag-handle").remove();
            // }
            // if ($(".setObjects_actions").length >= 1) {
            //     $(".setObjects_actions").remove();
            // }
            // if ($(".form-builder-ruler").length >= 1) {
            //     $(".form-builder-ruler").remove();
            // }
            // if ($(".ui-resizable").length >= 1) {
            //     $(".ui-resizable").removeClass("ui-resizable");
            // }
            // if ($(".ui-droppable").length >= 1) {
            //     $(".ui-droppable").removeClass("ui-droppable");
            // }
            // if ($(".ui-draggable").length >= 1) {
            //     $(".ui-draggable").removeClass("ui-draggable");
            // }
            // if ($(".ui-resizable-handle").length >= 1) {
            //     $(".ui-resizable-handle").remove();
            // }
            // if ($(".component-ancillary-focus").length >= 1) {
            //     $(".component-ancillary-focus").removeClass("component-ancillary-focus");
            // }
            // if ($(".component-primary-focus").length >= 1) {
            //     $(".component-primary-focus").removeClass("component-primary-focus");
            // }
            if ($(".form-tabbable-pane").length >= 1) {
                $(".form-tabbable-pane").each(function () {
                    $(this).children('.ui-tabs-nav[role="tablist"]').find(".ui-icon-close").hide();
                });
            }
            if ($(".icon-list-alt").length >= 1) {
                $(".icon-list-alt").attr("class", $(".icon-list-alt").attr("class").replace("icon-list-alt", "fa fa-list-alt"));
            }

            if ($(".embed-view-container").length >= 1) {
                $(".embed-view-container").each(function () {
                    //

                    var dis_embed_ele = $(this);
                    var form_id = dis_embed_ele.attr("embed-source-form-val-id");
                    var attr_creation = dis_embed_ele.attr("embed-row-click-creation-form");
                    var allow_import = dis_embed_ele.attr('embed-allow-import');//copy this....
                    var fieldReference = dis_embed_ele.attr("embed-result-field-val");
                    if (attr_creation) {
                        if (attr_creation == "true") {
                            var link_caption = "Add Request";
                            if (dis_embed_ele.attr("link-ele-caption")) {
                                if (dis_embed_ele.attr("link-ele-caption") != "") {
                                    link_caption = dis_embed_ele.attr("link-ele-caption");
                                }
                            }
                            var link_ele = $(
                                    "<a class='embed_newRequest fl-buttonEffect btn-basicBtn cursor' style='display:inline-block;padding:5px;margin-bottom:5px; font-size:12px; color: #fff;text-decoration:none;'>" + link_caption + "</a>"
                                    );
                            if ($(this).parents('.setOBJ[data-type="embeded-view"]').eq(0).find('.embeded-button-actions').eq(0).length >= 1) {
                                $(this).parents('.setOBJ[data-type="embeded-view"]').eq(0).find('.embeded-button-actions').eq(0).children('.embed_newRequest').remove(); // FS#8720
                                $(this).parents('.setOBJ[data-type="embeded-view"]').eq(0).find('.embeded-button-actions').eq(0).prepend(link_ele);
                            } else {
                                $(this).parent().children('.embed_newRequest').remove(); // FS#8720
                                $(this).before(link_ele);
                            }
                            link_ele.attr("data-embed-href-link", "/user_view/workspace?view_type=request&formID=" + form_id + "&requestID=0"); //FS#7646
                            $(link_ele).on({
                                "click": function (e) {
                                    e.preventDefault();
                                    if (dis_embed_ele.attr('source-form-type') != "multiple") {
                                        var dis_ele = $(this);
                                        var str_link = "/user_view/workspace?view_type=request&formID=" + form_id + "&requestID=0";
                                        var ret = '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
                                                '<div class="hr"></div>';

                                        // $(window).outerHeight()
                                        var embedded = dis_ele.parent().find('.embed-view-container');
                                        var embed_dialog_type = dis_embed_ele.attr('embed-dialog-type');
                                        var parent_form = $(".fl-body-content-wrapper.preview_content").eq(0);
                                        if (embedded.attr('embed-action-inline-create') == 'true') {
                                            showHideEmbedActions(dis_embed_ele, 'show');//finding ian
                                            console.log("true 2");
                                            ui.blockPortion($(embedded));
                                            embeded_view.getFieldsData(form_id, embedded, 'insert');
                                            return false;
                                        } else if (embed_dialog_type == "2") {
                                            var existingWindow = false; //FS#7646
                                            if (embedded.is(":data('embedDialog')")) { //FS#7646
                                                if (embedded.data("embedDialog").closed == false) {
                                                    existingWindow = true;
                                                    embedded.data("embedDialog").focus();
                                                    // embedded.data("embedDialog").location.href = str_link;
                                                    if ((embedded.data("embedDialog").location.pathname + embedded.data("embedDialog").location.search) != str_link) {
                                                        embedded.data("embedDialog").location.href = str_link;
                                                    }
                                                    if (embedded.data("embedDialog").$('html').is('.view-embed-iframe') == false) {  //this means the dialog window has been reloaded
                                                        embedded.data("embedDialog").$('html').addClass('view-embed-iframe');
                                                        embedded.data("embedDialog").field_updater = function () {
                                                            $('[name="' + fieldReference + '"]').trigger('change');
                                                        };
                                                        EmbedPopUpDisplayFixes.call(embedded.data("embedDialog").$('html'));
                                                        if (embedded.attr("stringified-json-data-send")) {
                                                            var json_data_send = embedded.attr("stringified-json-data-send");
                                                            if (json_data_send != "") {
                                                                var parsed_json_data_send = JSON.parse(json_data_send);
                                                                //console.log("DAADA",parsed_json_data_send)
                                                                var iframe_document = embedded.data("embedDialog").$('html');
                                                                var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
                                                                for (var index_json_data_send in parsed_json_data_send) {
                                                                    if (parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == "true" || parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == true) {


                                                                        var data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '"]');
                                                                        if (data_send_embed_ele_parent.length == 0) {
                                                                            data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '[]"]'); /// for multiples
                                                                        }
                                                                        var data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '"]');
                                                                        if (data_send_embed_ele_iframe.length == 0) {
                                                                            data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '[]"]'); // for multiples
                                                                        }
                                                                        //console.log("QWERT",data_send_embed_ele_iframe,data_send_embed_ele_parent)
                                                                        if (data_send_embed_ele_parent.is('input[type="radio"]')) {
                                                                            if(data_send_embed_ele_iframe.is('input[type="radio"]')){
                                                                                data_send_embed_ele_iframe.removeAttr('checked');
                                                                                data_send_embed_ele_iframe.filter('[value="'+data_send_embed_ele_parent.filter(':checked').val()+'"]').prop('checked',true);
                                                                            }else{
                                                                                data_send_embed_ele_iframe.val(data_send_embed_ele_parent.filter(':checked').val());
                                                                            }
                                                                            data_send_embed_ele_iframe.trigger("change");
                                                                        } else if (data_send_embed_ele_parent.is('input[type="checkbox"]')) {
                                                                            if (data_send_embed_ele_iframe.is('input[type="checkbox"]')) {
                                                                                data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                                    return ($(this).is(":checked")) ? $(this).val() : null;
                                                                                }).get());
                                                                                //console.log("TAAS");
                                                                            } else {
                                                                                data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                                    return ($(this).is(":checked")) ? $(this).val() : null;
                                                                                }).get().join('<--->'));
                                                                                //console.log("BABA");
                                                                            }
                                                                        } else {
                                                                            data_send_embed_ele_iframe[0].defaultValue = data_send_embed_ele_parent.val();
                                                                            data_send_embed_ele_iframe.val(data_send_embed_ele_parent.val());
                                                                            data_send_embed_ele_iframe.trigger("change");
                                                                        }

                                                                    }
                                                                }
                                                            }
                                                        }

                                                        //save draft
                                                        {
                                                            //added by japhet
                                                            //commit data row event
                                                            if (embedded.attr('commit-data-row-event') != "parent") {
                                                                iframe_document.find(".fl-action-submit").removeAttr("status-embed");
                                                            }
                                                            else {
                                                                iframe_document.find(".fl-action-submit").attr("status-embed", "Draft");
                                                            }
                                                            // console.log("asdasdqe",iframe_document_form.find("."))
                                                        }
                                                        if (embedded.attr('commit-data-row-event') != 'default') {
                                                            embeded_view.createTemporaryData(embedded, 'insert', 'popup', undefined, undefined, undefined, embedDialog);
                                                        }

                                                    }
                                                }
                                            }
                                            if (existingWindow == false) { //FS#7646
                                                var embed_window_prop = $('body').data('user_form_json_data')['form_json'][embedded.closest('.setOBJ').attr('data-object-id')];
                                                var other_prop_str = ",width=" + (embed_window_prop['embed-popup-window-width'] || "300") + ",height=" + (embed_window_prop['embed-popup-window-height'] || "300");
                                                // prop_str.substring(0, prop_str.length - 2);
                                                var embedDialog = window.open(str_link, '_blank', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no' + other_prop_str); // FS#7646
                                                console.log("KNOCK OUT", embedded)
                                                embedded.data("embedDialog", embedDialog);
                                                embedDialog.setTimeout(function () {
                                                    embedDialog.addingViewEmbedIframe = setInterval(function () {
                                                        if (embedDialog.document.getElementsByTagName('html').length >= 1) {
                                                            embedDialog.document.getElementsByTagName('html')[0].className += " view-embed-iframe";
                                                        }
                                                    }, 0);
                                                }, 0);
                                                embedDialog.fromParent = true;
                                                embedDialog.field_updater = function () {
                                                    $('[name="' + fieldReference + '"]').trigger('change');
                                                };
                                                embedDialog.onload = function ( ) {
                                                    clearInterval(embedDialog.addingViewEmbedIframe);
                                                    embedDialog.$(embedDialog).on('keydown', function (e) {
                                                        if (((e.ctrlKey || (e.shiftKey && e.ctrlKey)) && (e.keyCode == 82 || e.keyCode == 116)) || e.keyCode == 116) { // FS#9143  - when capitalizing the letter r
                                                            e.preventDefault(); // FS#7646
                                                        }
                                                    });
                                                    var iframe_document = embedDialog.$('html');
                                                    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");

                                                    embedDialog.onbeforeunload = function (e) {
                                                        if (iframe_document.find('.getFields[name]').filter(function () {
                                                            return embedDialog.$(this)[0].value != embedDialog.$(this)[0].defaultValue;
                                                        }).length >= 1)
                                                            return "You have changes made.";
                                                    };
                                                    EmbedPopUpDisplayFixes.call(iframe_document);
                                                    iframe_document.addClass('view-embed-iframe');

                                                    if (embedded.attr("stringified-json-data-send")) {
                                                        var json_data_send = embedded.attr("stringified-json-data-send");
                                                        if (json_data_send != "") {
                                                            var parsed_json_data_send = JSON.parse(json_data_send);
                                                            //console.log("DAADA",parsed_json_data_send)

                                                            for (var index_json_data_send in parsed_json_data_send) {
                                                                if (parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == "true" || parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == true) {


                                                                    var data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '"]');
                                                                    if (data_send_embed_ele_parent.length == 0) {
                                                                        data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '[]"]'); /// for multiples
                                                                    }
                                                                    var data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '"]');
                                                                    if (data_send_embed_ele_iframe.length == 0) {
                                                                        data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '[]"]'); // for multiples
                                                                    }
                                                                    //console.log("QWERT",data_send_embed_ele_iframe,data_send_embed_ele_parent)
                                                                    if (data_send_embed_ele_parent.is('input[type="radio"]')) {
                                                                        // data_send_embed_ele_iframe.val(data_send_embed_ele_parent.filter(':checked').val());
                                                                        data_send_embed_ele_iframe.removeAttr('checked');
                                                                        data_send_embed_ele_iframe.filter('[value="'+data_send_embed_ele_parent.filter(':checked').val()+'"]').prop('checked',true);
                                                                        data_send_embed_ele_iframe.trigger("change");
                                                                    } else if (data_send_embed_ele_parent.is('input[type="checkbox"]')) {
                                                                        if (data_send_embed_ele_iframe.is('input[type="checkbox"]')) {
                                                                            data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                                return ($(this).is(":checked")) ? $(this).val() : null;
                                                                            }).get());
                                                                            //console.log("TAAS");
                                                                        } else {
                                                                            data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                                return ($(this).is(":checked")) ? $(this).val() : null;
                                                                            }).get().join('<--->'));
                                                                            //console.log("BABA");
                                                                        }
                                                                    } else {
                                                                        data_send_embed_ele_iframe[0].defaultValue = data_send_embed_ele_parent.val();
                                                                        data_send_embed_ele_iframe.val(data_send_embed_ele_parent.val());
                                                                        data_send_embed_ele_iframe.trigger("change");
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }

                                                    //save draft
                                                    {
                                                        //added by japhet
                                                        //commit data row event
                                                        if (embedded.attr('commit-data-row-event') != "parent") {
                                                            iframe_document.find(".fl-action-submit").removeAttr("status-embed");
                                                        }
                                                        else {
                                                            iframe_document.find(".fl-action-submit").attr("status-embed", "Draft");
                                                        }
                                                        // console.log("asdasdqe",iframe_document_form.find("."))
                                                    }
                                                    if (embedded.attr('commit-data-row-event') != 'default') {
                                                        embeded_view.createTemporaryData(embedded, 'insert', 'popup', undefined, undefined, undefined, embedDialog);
                                                    }
                                                }
                                            }

                                        } else {

                                            if ($('body').data("user_form_json_data")) {
                                                var embeddedViewObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

                                                //console.log('picklist', picklist_data);
                                                // Roni Pinili close button modal configuration
                                                if ($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]) {
                                                    var embededViewActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
                                                    console.log("VALUE", embededViewActionVisibilityVal);
                                                    if (embededViewActionVisibilityVal == "Yes") {
                                                        var close_dial = setInterval(function () {
                                                            $('.fl-closeDialog').hide();
                                                            if ($('.fl-closeDialog').length >= 1) {
                                                                clearInterval(close_dial);
                                                            }
                                                        }, 0);
                                                    }
                                                }

                                            }

                                            var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "40", function () {
                                            });
                                            dis_dialog.themeDialog("modal2");
                                            $('.fl-closeDialog').addClass('close-confirmation');

                                            var ele_dialog = $('#popup_container');
                                            var my_dialog = $(
                                                    '<div class="embed-iframe" style="height: 100%;">' +
                                                    '<iframe style="height:100%;width:100%;" src="' + str_link + '">' +
                                                    '</iframe>' +
                                                    '</div>'
                                                    )
                                            ele_dialog.find("#popup_content").css({
                                                "height": ($(window).outerHeight() - 120) + "px"
                                            });
                                            ele_dialog.find("#popup_content").html(my_dialog);
                                            ele_dialog.css({
                                                "top": "30px"
                                            });


                                            var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
                                            var embed_view_cont = dis_ele.parent().find(".embed-view-container").eq(0);
                                            $('html').data("embed_container", embed_view_cont);
                                            ele_dialog.data("field_updater", fieldReference);
                                            //added by japhet morada trigger current embedded view
                                            ele_dialog.data("embedded_view", dis_embed_ele.attr('embed-name'));

                                            var loading_panel = panel.load(ele_dialog.find("#popup_content"));
                                            loading_panel.parent().css("position", "relative");

                                            iframe_ele_dialog[0].contentWindow.embedWindowDataSending = (function (win) { //ISSUE ON THE ONLOAD OF IFRAME added execution of embedWindowDataSending on initializePage.init()
                                                var iframe_document = win.$('html');//$($(this).contents()[0]);
                                                var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
                                                EmbedPopUpDisplayFixes.call(iframe_document);
                                                iframe_document.addClass('view-embed-iframe');

                                                if (embed_view_cont.attr("stringified-json-data-send")) {
                                                    var json_data_send = embed_view_cont.attr("stringified-json-data-send");
                                                    if (json_data_send != "") {
                                                        var parsed_json_data_send = JSON.parse(json_data_send);
                                                        //console.log("DAADA",parsed_json_data_send)

                                                        for (var index_json_data_send in parsed_json_data_send) {
                                                            if (parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == "true" || parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == true) {


                                                                var data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '"]');
                                                                if (data_send_embed_ele_parent.length == 0) {
                                                                    data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '[]"]'); /// for multiples
                                                                }
                                                                var data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '"]');
                                                                if (data_send_embed_ele_iframe.length == 0) {
                                                                    data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '[]"]'); // for multiples
                                                                }
                                                                //console.log("QWERT",data_send_embed_ele_iframe,data_send_embed_ele_parent)

                                                                if (data_send_embed_ele_parent.is('input[type="radio"]')) {
                                                                    data_send_embed_ele_iframe.val(data_send_embed_ele_parent.filter(':checked').val());
                                                                    data_send_embed_ele_iframe.trigger("change");
                                                                } else if (data_send_embed_ele_parent.is('input[type="checkbox"]')) {
                                                                    if (data_send_embed_ele_iframe.is('input[type="checkbox"]')) {
                                                                        data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                            return ($(this).is(":checked")) ? $(this).val() : null;
                                                                        }).get());
                                                                        //console.log("TAAS");
                                                                    } else {
                                                                        data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                            return ($(this).is(":checked")) ? $(this).val() : null;
                                                                        }).get().join('<--->'));
                                                                        //console.log("BABA");
                                                                    }
                                                                } else {
                                                                    data_send_embed_ele_iframe.val(data_send_embed_ele_parent.val());
                                                                    data_send_embed_ele_iframe.trigger("change");
                                                                }

                                                            }
                                                        }
                                                    }
                                                }

                                                //save draft
                                                {
                                                    //added by japhet
                                                    //commit data row event
                                                    if (embedded.attr('commit-data-row-event') != "parent") {
                                                        iframe_document.find(".fl-action-submit").removeAttr("status-embed");
                                                    }
                                                    else {
                                                        iframe_document.find(".fl-action-submit").attr("status-embed", "Draft");
                                                    }
                                                    // console.log("asdasdqe",iframe_document_form.find("."))
                                                }

                                                loading_panel.fadeOut();
                                            });
                                            if (embedded.attr('commit-data-row-event') != 'default') {
                                                embeded_view.createTemporaryData(embedded, 'insert', 'popup');
                                            }
                                        }
                                    }
                                    else {
                                        var get_object_data = $('body').data("user_form_json_data")['form_json']["" + dis_embed_ele.parents('.setOBJ:eq(0)').attr('data-object-id')]['multiple_forms'];
                                        var ret = '<div>';
                                        ret += '<h3 class="fl-margin-bottom">';
                                        ret += '' + '<span>Select a Form:</span>'; //'<i class=" '+ icon +' '+ faiconColor +' icon-save"></i> ' + title;
                                        ret += '</h3></div>';
                                        ret += '<div class="hr"></div>';
                                        //ret +='<div style="margin:2px 0px 0px 2px;font-size:11px;color:red">' + note + '</div>';
                                        ret += '<div class="content-dialog" id="related_info_content">';
                                        ret += '<div class="properties_width_container">';
                                        ret += '<div class="fields_below fl-field-style column div_1_of_1">';
                                        ret += '<div class="input_position_below">';
                                        ret += '<span>Select a Form:</span>';
                                        ret += '<select class="form-select form_selection">';
                                        ret += '<option value="0">--Select--</option>';
                                        for (i in get_object_data) {
                                            ret += '<option value="' + get_object_data[i]['source_form'] + '">' + get_object_data[i]['source_form_name'] + '</option>';
                                        }
                                        ret += '</select>';
                                        ret += '</div>';
                                        ret += '</div>';
                                        ret += '</div>';
                                        ret += '</div>';
                                        ret += '<input type="button" class="btn-blueBtn fl-margin-right setFormID fl-positive-btn" value="OK"/>';
                                        ret += '</div>'
                                        var dialog = new jDialog(ret, '', '', '150', '40', function () {
                                        });
                                        dialog.themeDialog('modal2');
                                        $('.setFormID').on('click', function (e) {
                                            var self = $(this);
                                            var container = self.parents('#popup_container');
                                            if (container.find('.form_selection').val() == "0") {
                                                showNotification({
                                                    message: "Please Select a form!",
                                                    type: "error",
                                                    autoClose: true,
                                                    duration: 3
                                                });
                                                return;
                                            }
                                            else {
                                                form_id = container.find('.form_selection').val();
                                            }
                                            var dis_ele = $(this);
                                            var str_link = "/user_view/workspace?view_type=request&formID=" + form_id + "&requestID=0";
                                            var ret = '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Embed popup</h3>' +
                                                    '<div class="hr"></div>';

                                            // $(window).outerHeight()
                                            var embedded = dis_embed_ele;
                                            var embed_dialog_type = dis_embed_ele.attr('embed-dialog-type');
                                            var parent_form = $(".fl-body-content-wrapper.preview_content").eq(0);
                                            if (embedded.attr('embed-action-inline-create') == 'true') {
                                                showHideEmbedActions(dis_embed_ele, 'show');//finding ian
                                                console.log("true 2");
                                                ui.blockPortion($(embedded));
                                                embedded.attr('embed-source-form-val-id', form_id);
                                                embeded_view.getFieldsData(form_id, embedded, 'insert');
                                                self.parents('#popup_container').find('.fl-closeDialog').trigger('click');
                                                return false;
                                            } else if (embed_dialog_type == "2") {
                                                var embed_window_prop = $('body').data('user_form_json_data')['form_json'][embedded.closest('.setOBJ').attr('data-object-id')];
                                                var other_prop_str = ",width=" + (embed_window_prop['embed-popup-window-width'] || "300") + ",height=" + (embed_window_prop['embed-popup-window-height'] || "300");
                                                // prop_str.substring(0, prop_str.length - 2);
                                                var embedDialog = window.open(str_link, '_blank', 'fullscreen=no' + other_prop_str);
                                                embedDialog.setTimeout(function () {
                                                    embedDialog.addingViewEmbedIframe = setInterval(function () {
                                                        if (embedDialog.document.getElementsByTagName('html').length >= 1) {
                                                            embedDialog.document.getElementsByTagName('html')[0].className += " view-embed-iframe";
                                                        }
                                                    }, 0);
                                                }, 0);
                                                embedDialog.fromParent = true;
                                                embedDialog.field_updater = function () {
                                                    if (typeof embedded.attr('source-form-type') != "undefined" && embedded.attr('source-form-type') == "multiple") {
                                                        var field_references = embedded.data('field_reference');
                                                        $('[name="' + field_references[0] + '"]').trigger('change');
                                                    }
                                                    else {
                                                        $('[name="' + fieldReference + '"]').trigger('change');
                                                    }
                                                };
                                                embedDialog.onload = function ( ) {
                                                    clearInterval(embedDialog.addingViewEmbedIframe);
                                                    var iframe_document = embedDialog.$('html');
                                                    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");

                                                    embedDialog.onbeforeunload = function (e) {
                                                        if (iframe_document.find('.getFields[name]').filter(function () {
                                                            return embedDialog.$(this)[0].value != embedDialog.$(this)[0].defaultValue;
                                                        }).length >= 1)
                                                            return "You have changes made.";
                                                    };
                                                    EmbedPopUpDisplayFixes.call(iframe_document);
                                                    iframe_document.addClass('view-embed-iframe');

                                                    if (embedded.attr("stringified-json-data-send")) {
                                                        var json_data_send = embedded.attr("stringified-json-data-send");
                                                        if (json_data_send != "") {
                                                            var parsed_json_data_send = JSON.parse(json_data_send);
                                                            //console.log("DAADA",parsed_json_data_send)

                                                            for (var index_json_data_send in parsed_json_data_send) {
                                                                if (parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == "true" || parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == true) {


                                                                    var data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '"]');
                                                                    if (data_send_embed_ele_parent.length == 0) {
                                                                        data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '[]"]'); /// for multiples
                                                                    }
                                                                    var data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '"]');
                                                                    if (data_send_embed_ele_iframe.length == 0) {
                                                                        data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '[]"]'); // for multiples
                                                                    }
                                                                    //console.log("QWERT",data_send_embed_ele_iframe,data_send_embed_ele_parent)
                                                                    if (data_send_embed_ele_iframe.length > 0) {
                                                                        if (data_send_embed_ele_parent.is('input[type="radio"]')) {
                                                                            data_send_embed_ele_iframe.val(data_send_embed_ele_parent.filter(':checked').val());
                                                                            data_send_embed_ele_iframe.trigger("change");
                                                                        } else if (data_send_embed_ele_parent.is('input[type="checkbox"]')) {
                                                                            if (data_send_embed_ele_iframe.is('input[type="checkbox"]')) {
                                                                                data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                                    return ($(this).is(":checked")) ? $(this).val() : null;
                                                                                }).get());
                                                                                //console.log("TAAS");
                                                                            } else {
                                                                                data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                                    return ($(this).is(":checked")) ? $(this).val() : null;
                                                                                }).get().join('<--->'));
                                                                                //console.log("BABA");
                                                                            }
                                                                        } else {
                                                                            data_send_embed_ele_iframe[0].defaultValue = data_send_embed_ele_parent.val();
                                                                            data_send_embed_ele_iframe.val(data_send_embed_ele_parent.val());
                                                                            data_send_embed_ele_iframe.trigger("change");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    //save draft
                                                    {
                                                        //added by japhet
                                                        //commit data row event
                                                        if (embedded.attr('commit-data-row-event') != "parent") {
                                                            iframe_document.find(".fl-action-submit").removeAttr("status-embed");
                                                        }
                                                        else {
                                                            iframe_document.find(".fl-action-submit").attr("status-embed", "Draft");
                                                        }
                                                        // console.log("asdasdqe",iframe_document_form.find("."))
                                                    }
                                                    if (embedded.attr('commit-data-row-event') != 'default') {
                                                        embeded_view.createTemporaryData(embedded, 'insert', 'popup', undefined, undefined, undefined, embedDialog);
                                                    }
                                                };
                                                self.parents('#popup_container').find('.fl-closeDialog').trigger('click');
                                            } else {
                                                if ($('body').data("user_form_json_data")) {
                                                    var embeddedViewObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

                                                    //console.log('picklist', picklist_data);
                                                    // Roni Pinili close button modal configuration
                                                    if ($('body').data("user_form_json_data")['form_json'][embeddedViewObjId]) {
                                                        var embededViewActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][embeddedViewObjId].obj_hideModalCloseBtn;
                                                        console.log("VALUE", embededViewActionVisibilityVal);
                                                        if (embededViewActionVisibilityVal == "Yes") {
                                                            var close_dial = setInterval(function () {
                                                                $('.fl-closeDialog').hide();
                                                                if ($('.fl-closeDialog').length >= 1) {
                                                                    clearInterval(close_dial);
                                                                }
                                                            }, 0);
                                                        }
                                                    }

                                                }

                                                var dis_dialog = new jDialog(ret, "", $(window).outerWidth() - 100, "", "40", function () {
                                                });
                                                dis_dialog.themeDialog("modal2");
                                                $('.fl-closeDialog').addClass('close-confirmation');

                                                var ele_dialog = $('#popup_container');
                                                var my_dialog = $(
                                                        '<div class="embed-iframe" style="height: 100%;">' +
                                                        '<iframe style="height:100%;width:100%;" src="' + str_link + '">' +
                                                        '</iframe>' +
                                                        '</div>'
                                                        )
                                                ele_dialog.find("#popup_content").css({
                                                    "height": ($(window).outerHeight() - 120) + "px"
                                                });
                                                ele_dialog.find("#popup_content").html(my_dialog);
                                                ele_dialog.css({
                                                    "top": "30px"
                                                });


                                                var iframe_ele_dialog = ele_dialog.find(".embed-iframe").eq(0).children("iframe").eq(0);
                                                var embed_view_cont = embedded;
                                                $('html').data("embed_container", embed_view_cont);
                                                if (typeof embed_view_cont.attr('source-form-type') != "undefined" && embed_view_cont.attr('source-form-type') == "multiple") {

                                                    ele_dialog.data("field_updater", embed_view_cont.data('field_reference'));
                                                }
                                                else {
                                                    ele_dialog.data("field_updater", fieldReference);
                                                }
                                                //added by japhet morada trigger current embedded view
                                                ele_dialog.data("embedded_view", dis_embed_ele.attr('embed-name'));

                                                var loading_panel = panel.load(ele_dialog.find("#popup_content"));
                                                loading_panel.parent().css("position", "relative");

                                                iframe_ele_dialog.load(function () {
                                                    console.log("embed_view_cont", embed_view_cont);
                                                    var iframe_document = $(this)[0].contentWindow.$('html');//$($(this).contents()[0]);
                                                    var iframe_document_form = iframe_document.find(".fl-body-content-wrapper.preview_content");
                                                    EmbedPopUpDisplayFixes.call(iframe_document);
                                                    iframe_document.addClass('view-embed-iframe');

                                                    if (embed_view_cont.attr("stringified-json-data-send")) {
                                                        var json_data_send = embed_view_cont.attr("stringified-json-data-send");
                                                        if (json_data_send != "") {
                                                            var parsed_json_data_send = JSON.parse(json_data_send);
                                                            //console.log("DAADA",parsed_json_data_send)
                                                            for (var index_json_data_send in parsed_json_data_send) {
                                                                if (parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == "true" || parsed_json_data_send[index_json_data_send]["enable_embedPopupDataSending"] == true) {

                                                                    var data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '"]');
                                                                    if (data_send_embed_ele_parent.length == 0) {
                                                                        data_send_embed_ele_parent = parent_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_my_form_field_side"] + '[]"]'); /// for multiples
                                                                    }
                                                                    var data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '"]');
                                                                    if (data_send_embed_ele_iframe.length == 0) {
                                                                        data_send_embed_ele_iframe = iframe_document_form.find('[name="' + parsed_json_data_send[index_json_data_send]["epds_select_popup_form_field_side"] + '[]"]'); // for multiples
                                                                    }
                                                                    // console.log("QWERT",data_send_embed_ele_iframe,data_send_embed_ele_parent)
                                                                    // if(data_send_embed_ele_iframe.length > 0){
                                                                    if (data_send_embed_ele_parent.is('input[type="radio"]')) {
                                                                        data_send_embed_ele_iframe.val(data_send_embed_ele_parent.filter(':checked').val());
                                                                        data_send_embed_ele_iframe.trigger("change");
                                                                    } else if (data_send_embed_ele_parent.is('input[type="checkbox"]')) {
                                                                        if (data_send_embed_ele_iframe.is('input[type="checkbox"]')) {
                                                                            data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                                return ($(this).is(":checked")) ? $(this).val() : null;
                                                                            }).get());
                                                                            //console.log("TAAS");
                                                                        } else {
                                                                            data_send_embed_ele_iframe.val(data_send_embed_ele_parent.map(function () {
                                                                                return ($(this).is(":checked")) ? $(this).val() : null;
                                                                            }).get().join('<--->'));
                                                                            //console.log("BABA");
                                                                        }
                                                                    } else {
                                                                        data_send_embed_ele_iframe.val(data_send_embed_ele_parent.val());
                                                                        data_send_embed_ele_iframe.trigger("change");
                                                                    }
                                                                    // }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    //save draft
                                                    {
                                                        //added by japhet
                                                        //commit data row event
                                                        if (embedded.attr('commit-data-row-event') != "parent") {
                                                            iframe_document.find(".fl-action-submit").removeAttr("status-embed");
                                                        }
                                                        else {
                                                            iframe_document.find(".fl-action-submit").attr("status-embed", "Draft");
                                                        }
                                                        // console.log("asdasdqe",iframe_document_form.find("."))
                                                    }

                                                    loading_panel.fadeOut();
                                                });
                                                if (embedded.attr('commit-data-row-event') != 'default') {
                                                    embeded_view.createTemporaryData(embedded, 'insert', 'popup');
                                                }
                                            }
                                            self.parents('#popup_container').find('.fl-closeDialog').trigger('click');
                                        });
                                    }
                                }
                            });
                        }
                    }
                    //$(dis_embed_ele).perfectScrollbar();
                    if (allow_import) {//copy this....
                        var caption = "Import";
                        var refresh_caption = "Refresh";
                        var embedded_name = dis_embed_ele.attr('embed-name');
                        var active_fields = dis_embed_ele.attr('import-active-fields');
                        var embed_obj_id = dis_embed_ele.closest('.setOBJ').attr('data-object-id');
                        var import_ele = $(
                                "<a class='embed_newRequest import_record fl-buttonEffect btn-basicBtn cursor' embed-import='true' link-type='import' embed-object-id='" + embed_obj_id + "' link-form-id='" + form_id + "' import-active-fields='" + active_fields + "'  style='display:inline-block;padding:5px;margin-bottom:5px; font-size:12px; color: #fff; text-decoration: none; margin-left: 3px;'>" + caption + "</a>"
                                );
                        var refresh_ele = $(
                                "<a class='refresh_embed_view fl-buttonEffect btn-basicBtn cursor display' embed-object-id='" + embed_obj_id + "' style='float:right;padding:5px;margin-bottom:5px; font-size:12px; color: #fff; text-decoration: none; margin-left: 3px;'>" + refresh_caption + "</a>"
                                );
                        var import_refresh_btn = $(
                                "<div class='refresh_embed' style='display:none;'></div>"
                                // <label>Import done... Click </label><a>here</a><label> to refresh.</label>
                                );
                        if ($(this).parents('.setOBJ[data-type="embeded-view"]').eq(0).find('.embeded-button-actions').eq(0).length >= 1) {
                            $(this).parents('.setOBJ[data-type="embeded-view"]').eq(0).find('.embeded-button-actions').eq(0).children('.import_record').remove(); // FS#8720
                            $(this).parents('.setOBJ[data-type="embeded-view"]').eq(0).find('.embeded-button-actions').eq(0).prepend(import_ele);
                        } else {
                            $(this).parent().children('.import_record').remove(); // FS#8720
                            if ($(this).parent().children('.embed_newRequest').length > 0) {
                                $(this).parent().children('.embed_newRequest').after(import_ele);
                                import_ele.after(refresh_ele);
                                import_ele.after(import_refresh_btn);
                            }
                            else {
                                $(this).before(import_ele);
                                import_ele.after(refresh_ele);
                                import_ele.after(import_refresh_btn);
                            }
                        }
                    }
                })
                if ($('.embed-view-container').parent().find('[embed-import="true"]').length > 0) {
                    ImportRequest.generateImportDialog();
                }
            }

            //FIX DISPLAY VALUES
            if ($('.getFields[data-type="double"]').length >= 1) {
                $('.getFields[data-type="double"]').change(function () {
                    var unclean_val = $(this).val();
                    var dis_val = unclean_val.replace((new RegExp(",", "g")), "");
                    var dis_val_get_str_integer = (dis_val.split("."))[0];
                    var dis_val_parse_number = Number(dis_val);
                    var dis_val_to_percision = dis_val_parse_number.toPrecision(15);
                    if (unclean_val != "") {
                        if (!isNaN(dis_val_parse_number)) {
                            if (dis_val_get_str_integer.length >= 1 && dis_val_get_str_integer.length <= 15) {
                                $(this).val(Number(dis_val_to_percision));
                            } else if (dis_val.length == 0) {
                                $(this).val(0);
                            } else if (dis_val_get_str_integer.length >= 16) {
                                {//version 1
                                    // NotifyMe($(this), "The value is too big exponential");
                                    // $(this).val(dis_val_to_percision);
                                }

                                {//version 2
                                    validationNotification($(this), "The value is too big your value will be rounded off to 15 digit");
                                    var rounded = Math.round(dis_val.replace(".", "").insertReplaceAt(15, 0, "."));
                                    $(this).val(rounded);
                                }
                            }
                        } else {
                            validationNotification($(this), "Numbers only");
                            // $(this).val(0);

                        }
                    }
                });
            }

            if ($('input[data-type="dateTime"]').length >= 1) {

                var disabledDays = [];
                $('input[data-type="dateTime"]').datetimepicker({
                    changeYear: true,
                    changeMonth: true,
                    dateFormat: 'yy-mm-dd',
                    beforeShowDay: function (date) {
                        //console.log(this);
                        var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
                        for (i = 0; i < disabledDays.length; i++) {
                            if ($.inArray((m + 1) + '-' + d + '-' + y, disabledDays) != -1) {
                                return [false, 'red', 'This is Holiday'];
                            }
                        }
                        return [true];
                    }
                });
                if ($('input[data-type="dateTime"]').val().length >= 1) {
                    // replace value of date pickers

                    $('input[data-type="dateTime"]').each(function () {
                        if (ThisDate($(this).val()) == "Invalid Date") {

                            $(this).val("");
                        }
                        else {
                            $(this).val(ThisDate($(this).val()).formatDate("Y-m-d H:i"));
                        }

                    });
                }
                $('input[data-type="dateTime"]').each(function (eqi) {
                    var temporary = $(this);
                    if (temporary.is('[readonly][read-only-formula=""]')) {
                        //$(this).datetimepicker("option", "disabled", true);
                        $(this).datetimepicker("destroy");
                    }
                    if (temporary.is('[readonly="readonly"]')) { //FS#8041
                        $(this).datetimepicker("destroy");
                    }
                });
            }
            if ($('input[data-type="date"]').length >= 1) {
                $('input[data-type="date"]').datepicker({
                    'changeYear': true,
                    'dateFormat': 'yy-mm-dd',
                    "beforeShow": function () {
                        // setTimeout(function(){
                        $('.ui-datepicker').css('z-index', 100);
                        // }, 0);
                    },
                    changeMonth: true
                });
                if ($('input[data-type="date"]').val().length >= 1) {
                    // replace value of date pickers
                    $('input[data-type="date"]').each(function () {
                        if (ThisDate($(this).val()) == "Invalid Date") {

                            $(this).val("");
                        }

                        else {
                            $(this).val(ThisDate($(this).val()).formatDate("Y-m-d"));

                        }
                    });
                }
                $('input[data-type="date"]').each(function () {
                    var temporary = $(this);
                    if (temporary.is('[readonly][read-only-formula=""]')) {
                        //$(this).datepicker("option", "disabled", true);
                        $(this).datepicker("destroy");
                    }
                    if (temporary.is('[readonly="readonly"]')) { //FS#8041
                        $(this).datepicker("destroy");
                    }
                })
            }
            if ($('input[data-type="time"]').length >= 1) {
                $('input[data-type="time"]').datetimepicker({
                    changeYear: true,
                    changeMonth: true,
                    dateFormat: '',
                    timeFormat: 'HH:mm',
                    timeOnly: true
                });
                //if ($('input[data-type="time"]').val().length >= 1) {
                //    //replace value of date pickers
                //    $('input[data-type="time"]').each(function() {
                //        if (ThisDate($(this).val()) == "Invalid") {

                //        }
                //        $(this).val(ThisDate($(this).val()).formatDate("Y-m-d"));
                //    });
                //}
                $('input[data-type="time"]').each(function () {
                    var temporary = $(this);
                    if (temporary.is('[readonly][read-only-formula=""]')) {
                        //$(this).datetimepicker("option", "disabled", true);
                        $(this).datetimepicker("destroy");
                    }
                    if (temporary.is('[readonly="readonly"]')) { //FS#8043 FS#8041
                        $(this).datetimepicker("destroy");
                    }
                })
            }
            if ($('textarea.form-textarea.getFields[data-type="longtext"]').length >= 1) {
                $('textarea.form-textarea.getFields[data-type="longtext"]').css({
                    "resize": "none"
                });
            }
            if ($(".form-tabbable-pane").length >= 1) {
                $(".edit-title-tab-panel").remove();
                $(".li-add-new-tab-panel").remove();
                $(".scroll-achor-ul-tab-nav").css('color', 'gray');

                if ($(".form-tabbable-pane").find(".ul-scroll-anchor").length >= 1) {
                    $(".form-tabbable-pane").each(function () {
                        $(this).find(".ul-scroll-anchor").eq(0).remove();
                    });
                }
                $(".form-tabbable-pane").each(function (eqi_ftp) {
                    $(this).find(".ui-state-active").removeClass("ui-state-active");
                    $(this).tabPanel({
                        tabNavSortable: false
                                //resizableContent:true,
                                //resizableContentContainment:".preview_content"
                    });
                });
                $(".edit-title-tab-panel").remove();
                $(".li-add-new-tab-panel").remove();
                $(".scroll-achor-ul-tab-nav").css('color', 'gray');
                //has-scroll="true"
                //ui-tabs-nav-scrollable="true"

                // $(".scroll-achor-ul-tab-nav").on({
                //     "mouseenter":function(){
                //         $(this).stop(false,false).animate({
                //             "opacity":1
                //         });
                //     },
                //     "mouseleave":function(){
                //         $(this).stop(false,false).animate({
                //             "opacity":0
                //         });
                //     }
                // })
                // $('.ui-tabs-nav, .scroll-achor-ul-tab-nav').on({
                //     "mouseenter":function(){
                //         if($(this).hasClass('ui-tabs-nav')){
                //             $(this).parent().find('.ul-scroll-anchor:eq(0) .scroll-achor-ul-tab-nav').stop(false,false).animate({
                //                 "opacity":1
                //             });
                //         }else{
                //             $(this).stop(false,false).animate({
                //                 "opacity":1
                //             });
                //         }
                //     },
                //     "mouseleave":function(){
                //         if($(this).hasClass('ui-tabs-nav')){
                //             $(this).parent().find('.ul-scroll-anchor:eq(0) .scroll-achor-ul-tab-nav').stop(false,false).animate({
                //                 "opacity":0
                //             });
                //         }else{
                //             $(this).stop(false,false).animate({
                //                 "opacity":0
                //             });
                //         }
                //     }
                // })
            }

            if ($(".form-table").length >= 1) {



                $(".form-table").each(function () {
                    if ($(this).closest(".table-wrapper").length >= 1) {
                        $(this).closest(".table-wrapper").find(".table-actions-ra").remove();
                        if ($(this).closest(".table-wrapper").find(".ui-resizable-handle").length >= 1) {
                            $(this).closest(".table-wrapper").find(".ui-resizable-handle").remove();
                        }
                    } else if ($(this).find(".table-actions-ra").length >= 1) {
                        $(this).find(".table-actions-ra").remove();
                        if ($(this).find(".ui-resizable-handle").length >= 1) {
                            $(this).find(".ui-resizable-handle").remove();
                        }
                    }

                    if ($(this).attr("repeater-table")) {
                        if ($(this).attr("repeater-table") == "true") {
                            // $(this).closest(".")//DITO NA AKO
                            self.makeRepeaterTable($(this), function (table_ui) {
                                $(table_ui).children("tbody").children("tr").on({
                                    "mouseenter": function (e) {
                                        $(this).children("td").children(".td-relative").children(".repeater-action").not(".disable-repeater-action").stop(false, false).animate({
                                            "opacity": 1
                                        }, 500);
                                    },
                                    "mouseleave": function (e) {
                                        $(this).children("td").children(".td-relative").children(".repeater-action").not(".disable-repeater-action").stop(false, false).animate({
                                            "opacity": 0
                                        }, 500);
                                    }
                                });
                                $(table_ui).children("tbody").children("tr").find(".disable-repeater-action").css("display", "none");
                            });
                            var object_field_ele = $(this).closest(".setOBJ").find("#obj_fields_" + $(this).closest(".setOBJ").attr("data-object-id"));
                            // object_field_ele.css({
                            //   "width":(object_field_ele.outerWidth())+"px",
                            //   "height":(object_field_ele.outerHeight())+"px",
                            //   "overflow":"auto"
                            // });
                            // object_field_ele.children(".table-wrapper").css({
                            //   "margin-left":"40px"
                            // })
                            object_field_ele.filter(function () {
                                return !$(this).parent().parent().is('.setOBJ[data-type="table"]');
                            }).css({
                                "overflow-y": "auto",
                                "overflow-x": "hidden"
                            });
                        }
                    }

                    $(this).closest(".setOBJ").css({"height": ($(this).closest(".setOBJ").height()) + "px"});
                    if ($(this).closest(".setOBJ").css("display") == "none") {
                    } else {
                        if ($(this).is('[fld-table-container-responsive="true"]')) {
                            $(this).closest(".setOBJ").css({"display": "inline-block"});
                            var topsetobj = $(this).closest(".setOBJ").position().top;
                            var leftsetobj = $(this).closest(".setOBJ").position().left;
                            $(this).closest(".setOBJ").css({
                                "top": (0) + "px",
                                "left": (0) + "px",
                                "margin-left": (leftsetobj) + "px",
                                "margin-top": (topsetobj) + "px"
                            });
                        } else {
                            $(this).closest(".setOBJ").css({"display": "block"});
                        }
                    }
                });
            }
            if ($(".getFields[data-input-type='Currency']").length >= 1) {
                $(".getFields[data-input-type='Currency']").each(function () {
                    $(this).on("change.currency_format", function () {
                        if ($.isNumeric($(this).val())) {
                            var value_number = Number($(this).val());
                            $(this).val(value_number.currencyFormat());
                        } else {
                            var value_nito = $(this).val()
                            var sanitized_val = value_nito.replace(/[\s,]/g, "");
                            if ($.isNumeric(sanitized_val)) {
                                var value_number = Number(sanitized_val);
                                $(this).val(value_number.currencyFormat());
                            }
                        }
                    });
                });
                //$(".getFields[data-input-type='Currency']").trigger("change.currency_format");
            }
            // NOTE STYLE FIELD FUNCTIONALITY
            if ($('.content-editable-field[contenteditable="true"]').length >= 1) {

                var ele_style_cef = $(
                        "<style>" +
                        ".content-editable-field[contentEditable=true]:empty:not(:focus):before {" +
                        "content:attr(placeholder)" +
                        "color:rgb(150, 150, 150)" +
                        "}" +
                        ".content-editable-field[contentEditable=true]:empty:before {" +
                        "content:attr(placeholder)" +
                        "color:rgb(200,200,200)" +
                        "}" +
                        "</stlyle>"
                        );
                $("body").prepend(ele_style_cef);

                $("body").on('keyup.notesChange focusout.notesChange', '.content-editable-field[contenteditable="true"]', function (e) {//keypress.notesChange keydown.notesChange 
                    var disVal = "";
                    if (/*e.type == "keypress" ||*/ e.type == "keydown") {
                        var ele_dis = $(this);
                        var selection_index = /*getCaretPosition(ele_dis[0])*/ $(ele_dis).caret();//startPoint endPoint
                        // console.log(selection_index)
                    }

                    if (e.type == "keypress") {
                        return true;
                        // console.log("keypress", e)
                        // var keyCode = (typeof e.which == "number")?e.which:((e.keycode)?e.keycode:e.keyCode);
                        var charCode = e.charCode;
                        disVal = "" + String.fromCharCode(charCode);
                        var self_ele = $(this);
                        var oid = $(this).parents(".setOBJ").eq(0).attr("data-object-id");
                        var dis_val = getContentEditableVal(self_ele);
                        // console.log("BRAWL",selection_index["start"], selection_index["end"]-selection_index["start"])
                        dis_val = dis_val.insertReplaceAt(selection_index["start"], (selection_index["end"]) - (selection_index["start"]), disVal);
                        // console.log("VALUEPRESSED", dis_val)
                        var getField = self_ele.parents(".note-style-wrap-contents").eq(0).find(".getFields_" + oid);

                        getField.val(dis_val);
                        getField.trigger("change", [{
                                "notes_change_not_allow": true
                            }]);
                    } else if (e.type == "keydown") {
                        return true;
                        // self_ele = $(this);
                        // var oid = self_ele.parents(".setOBJ").eq(0).attr("data-object-id");
                        // var dis_val = getContentEditableVal(self_ele);
                        // var getField = self_ele.parents(".note-style-wrap-contents").eq(0).find(".getFields_" + oid);
                        // var getField_maxlength = Number(getField.attr("maxlength"));
                        // if( (dis_val.length+1) > getField_maxlength){
                        //     self_ele.html(dis_val.substr(0,getField_maxlength));
                        // }

                        // console.log("SELECTION",selection_index["endPoint"],selection_index["startPoint"])
                        // var keyCode = (typeof e.which == "number")?e.which:((e.keycode)?e.keycode:e.keyCode);
                        // var self_ele = $(this);
                        // var oid = $(this).parents(".setOBJ").eq(0).attr("data-object-id");
                        // var dis_val = getContentEditableVal(self_ele);
                        // var getField = self_ele.parents(".note-style-wrap-contents").eq(0).find(".getFields_"+oid);
                        // if(keyCode == 8){

                        //     if( (selection_index["endPoint"]-selection_index["startPoint"]) == 0){
                        //         // console.log(selection_index["endPoint"],selection_index["startPoint"],"BOOOM",selection_index["startPoint"]-1);
                        //         // dis_val = dis_val.insertReplaceAt(selection_index["startPoint"]-1, 1, "");
                        //     }else{
                        //         // console.log(selection_index["endPoint"],selection_index["startPoint"],"BOOOM",selection_index["startPoint"]-1);
                        //         // dis_val = dis_val.insertReplaceAt(selection_index["startPoint"], selection_index["endPoint"]-selection_index["startPoint"], "");
                        //     }
                        // }
                        // if(keyCode == 13){
                        //     dis_val = dis_val.insertReplaceAt(selection_index["startPoint"], selection_index["endPoint"]-selection_index["startPoint"], "\n");
                        // }
                        // getField.val(dis_val);

                        // var self_ele = $(this);
                        // var oid = $(this).parents(".setOBJ").eq(0).attr("data-object-id");
                        // var getField = self_ele.parents(".note-style-wrap-contents").eq(0).find(".getFields_" + oid);
                        // getField.trigger("change", [{
                        //         "notes_change_not_allow": true
                        //     }]);
                    } else if (e.type == "keyup" || e.type == "focusout") {
                        // console.log("UPPP")
                        var self_ele = $(this);
                        var oid = $(this).parents(".setOBJ").eq(0).attr("data-object-id");
                        var dis_val = getContentEditableVal(self_ele);
                        var getField = self_ele.parents(".note-style-wrap-contents").eq(0).find(".getFields_" + oid);
                        var getField_maxlength = Number(getField.attr("maxlength"));
                        // console.log("dis_val",dis_val,getField_maxlength);
                        // if(dis_val.length  > getField_maxlength){
                        //     dis_val = dis_val.substr(0,getField_maxlength);
                        // }
                        // console.log("dis_val",dis_val,getField_maxlength);
                        getField.val(dis_val);
                        if (dis_val.length > getField_maxlength) {
                            getField.val(dis_val.substr(0, getField_maxlength));
                            self_ele.html(dis_val.substr(0, getField_maxlength));
                        }
                        getField.trigger("change", [{
                                "notes_change_not_allow": true
                            }]);
                    }
                });
                $("body").on('change.notesChange', '.noteStyleField.getFields', function (e, data_event) {
                    // console.log("NOTESCHANGE ALLOWS",data_event)
                    if (data_event) {
                        if (data_event["notes_change_not_allow"]) {
                            return false;
                        } else {

                        }
                    } else {

                    }
                    var self = $(this);
                    var display_location = self.parents(".note-style-wrap-contents").eq(0).find(".content-editable-field[contenteditable]").eq(0);
                    var regEks = new RegExp(".*(?=\n|$)", "g"); // change to
                    var val_matches = self.val().match(regEks).filter(Boolean);
                    // console.log("TESTMATCHES::", val_matches)
                    var collect_val = [];
                    for (var ii in val_matches) {
                        collect_val.push("<div>" + val_matches[ii] + "</div>");
                    }
                    display_location.html(collect_val.join(""));
                });
                $("[fluid-container-cell]").each(function () {
                    var dis_ele = $(this);
                    var dis_ele_top = Number(dis_ele.css('top').replace("px", ""));
                    var dis_ele_left = Number(dis_ele.css('left').replace("px", ""));
                    var dis_ele_setOBJ = dis_ele.parents(".setOBJ").eq(0);
                    var dis_doi = dis_ele_setOBJ.attr("data-object-id");

                    if (dis_ele_setOBJ.parent().hasClass("td-relative")) {
                        dis_ele_setOBJ.css("position", "relative");
                    }
                    if (dis_ele_top >= 1) {
                        dis_ele.css({
                            "top": "0px",
                            "margin-top": (dis_ele_top) + "px"
                        });

                    }
                    if (dis_ele_left >= 1) {
                        dis_ele.css({
                            "left": "0px",
                            "margin-left": (dis_ele_left) + "px"
                        });
                    }
                });

                $('[fld-table-container-responsive="true"]').each(function (eqi) {
                    var dis_ele = $(this);
                    var dis_ele_top = Number(dis_ele.css('top').replace("px", ""));
                    var dis_ele_left = Number(dis_ele.css('left').replace("px", ""));
                    var dis_ele_setOBJ = dis_ele.parents(".setOBJ").eq(0);
                    var dis_doi = dis_ele_setOBJ.attr("data-object-id");

                    if (dis_ele_setOBJ.parent().is('.ui-tabs-panel[role="tabpanel"]')) {

                        dis_ele_setOBJ.css({
                            "position": "relative",
                            "top": "",
                            "left": "",
                            "margin-top": (dis_ele_top) + "px",
                            "margin-left": (dis_ele_left) + "px"
                        })
                        dis_ele_setOBJ.parents().filter('.setOBJ[data-type="tab-panel"]').each(function (eqi) {
                            var self_ele = $(this);
                            var self_ele_ow = self_ele.outerWidth();
                            var self_ele_top = self_ele.position().top;
                            var self_ele_left = self_ele.position().left;

                            self_ele.css({
                                "width": "",
                                "min-width": (self_ele_ow) + "px",
                                "margin-top": (self_ele_top) + "px",
                                "margin-left": (self_ele_left) + "px",
                                "top": "",
                                "left": "",
                                "position": "relative"
                            });
                        });
                        dis_ele_setOBJ.parents().filter('.ui-tabs-panel[role="tabpanel"]').each(function (eqi) {
                            var self_ele = $(this);
                            var self_ele_oh = self_ele.outerHeight();
                            self_ele.css({
                                "overflow": 'visible',
                                "width": "",
                                "min-width": "100%",
                                "height": "",
                                "min-height": (self_ele_oh) + "px"
                            });
                        });
                    }
                });
                $(".noteStyleField.getFields").trigger("change");
                $('.setOBJ[data-type="noteStyleTextarea"]').each(function (eq_i_ns) {
                    var dis_ele = $(this);
                    var doi = dis_ele.attr("data-object-id");
                    var dis_getfield = dis_ele.find(".getFields_" + doi);
                    if (dis_getfield.attr("disabled")) {
                        dis_ele.find(".content-editable-field_" + doi).attr("contenteditable", false);

                    }
                    dis_ele.find(".content-editable-field_" + doi).css({
                        "width": "",
                        "height": ""
                    });
                });
            }

            if ($('[data-type="attachment_on_request"]').length >= 1) {
                $('[data-type="attachment_on_request"]').each(function () {
                    var data_obj_id = $(this).attr("data-object-id");
                    if ($(this).find("form").length == 0) {
                        $(this).find('[data-action-type="attachmentForm"]').wrap('<form id="on_attach_' + data_obj_id + '" method="post" enctype="multipart/form-data" action="/ajax/request_on_attachment" />')
                    }
                })
            }

            if ($('[data-type="multiple_attachment_on_request"]').length >= 1) {
                $('[data-type="multiple_attachment_on_request"]').each(function () {
                    var data_obj_id = $(this).attr("data-object-id");
                    if ($(this).find("form").length == 0) {
                        $(this).find('[data-action-type="multiple_attachmentForm"]').wrap('<form id="multiple_on_attach_' + data_obj_id + '" method="post" enctype="multipart/form-data" action="/ajax/multiple_request_on_attachment" />')
                    }
                })
            }
            if ($('.table-form-design-relative').length >= 1) {//if ($('.table-form-design-relative').find('.form-table').eq(0).is('[fld-table-container-responsive="true"]')) {
                // var form_size = {
                //     "height": $('.table-form-design-relative').parents('.workspace.formbuilder_ws').outerHeight(),
                //     "width": $('.table-form-design-relative').parents('.workspace.formbuilder_ws').outerWidth(),
                // };
                // $('.table-form-design-relative').parents('#frmrequest').css({
                //     "text-align": "center"
                // });
                // $('.table-form-design-relative').parents('.loaded_form_content').css({
                //     "height": "auto",
                //     "width": "auto",
                //     "min-height": (form_size["height"]) + "px",
                //     "min-width": (form_size["width"]) + "px",
                //     "display": "inline-block"
                // });


                $('#frmrequest').css({
                    "text-align": "center"
                });
                var tfdr_ele = $('.table-form-design-relative');
                tfdr_ele.css('display', 'inline-table');
                var form_size = {
                    "height": tfdr_ele.outerHeight(),
                    "width": tfdr_ele.outerWidth()
                };
                $('.loaded_form_content').css({
                    'display': 'inline-table',
                    'width': 'auto',
                    'height': 'auto',
                    'text-align': 'left',
                    "min-height": (form_size["height"]) + "px",
                    "min-width": (form_size["width"]) + "px",
                });
                tfdr_ele.find('.form-table').eq(0)
                        .children('tbody')
                        .children('tr')
                        .children('td').css({
                    'border': 'none'
                });
            }
            if ($('[tabindex="1"]').length >= 1) {
                $('[tabindex="1"]').trigger("focus");
            }


            if ($('[data-type="pickList"]').exists()) {

                $('[data-type="pickList"]').each(function () {
                    var is_multiple = $(this).find('.pickListButton').attr('selection-type') || "";
                    var picklistButton = $(this).find('.pickListButton:eq(0)');
                    var allow_val_not_in_list = picklistButton.attr('allow-values-not-in-list') || "";

                    if (allow_val_not_in_list === "No") {
                        $(this).find('input.getFields:eq(0):not([disabled])').on('click.picklistClickField', function () {
                            if (!$(this).is('[readonly]')) {
                                picklistButton.trigger('click');
                            }
                        }).on('focus', function () {
                            $(this).blur();
                        }).css('cursor', 'pointer');
                    }
                    if (picklistButton.is('.hidePicklistAction')) {
                        picklistButton.css('display', "none");
                    }
                    // $(this).find('input.getFields:eq(0)').on('blur',function(){
                    //     var this_val = $(this).val();
                    //     //allow values not in list
                    //      //console.log("eexexexexexe",$(this).closest('.setOBJ').find('.suggestion-data-content tbody').find('[return-value='+this_val+']').exists())
                    //     if(!$(this).closest('.setOBJ').find('.suggestion-data-content tbody').find('[return-value="'+this_val+'"]').exists() && allow_val_not_in_list === "No"){
                    //         $(this).val("");

                    //     }

                    // }); 





                });
            }
            if ($('[data-type="accordion"]').length >= 1) {
                $('.form-accordion-action-icon').empty();
                $(".form-accordion").each(function (eqi_ftp) {
                    $(this).find(".ui-state-active").removeClass("ui-state-active");
                    $(this).accordion(default_accordion_settings);
                });
            }
            if ($('#uniform-fileInput').exists()) {

                $('#uniform-fileInput').addClass('isDisplayNone');
            }
            var b_data = $('body').data();
            $('.setOBJ[data-type="pickList"],.setOBJ[data-type="listNames"]').each(function (eqi) {
                var self = $(this);
                var obj_id = self.attr('data-object-id');
                var elem_w_readonly_field = self.find(".getFields_" + obj_id);
                if (elem_w_readonly_field.is("[readonly]")) {
                    if (b_data) {
                        if (b_data['user_form_json_data']) {
                            if (b_data['user_form_json_data']['form_json']) {
                                if (b_data['user_form_json_data']['form_json'][obj_id]) {
                                    if (b_data['user_form_json_data']['form_json'][obj_id]['setReadOnlyField']) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                    console.log("TRESTING", elem_w_readonly_field);
                    elem_w_readonly_field.removeAttr("readonly");
                }
            });
            if ($('div.input_position_below').length >= 1) {
                var readonly_objects = $("div.input_position_below").find('input[readonly="readonly"],select[readonly="readonly"],textarea[readonly="readonly"]');
                readonly_objects.each(function () {
                    //$(this).addClass('readonly-cursor-display');
                    if ($(this).is('[type="radio"]') || $(this).is('[type="checkbox"]')) {
                        $(this).on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                        });
                    }
                    if ($(this).is('select:not([multiple])')) {
                        $(this).children().addClass('isDisplayNone');
                    }
                    if ($(this).closest('.setOBJ').is('[data-type="listNames"]')) {

                        $(this).closest('.setOBJ').find('a.viewNames').on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                        });
                    }
                    if ($(this).closest('.setOBJ').is('[data-type="pickList"]')) {
                        $(this).closest('.setOBJ').find('a.pickListButton').on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            return false;
                        });
                    }

                    //var the_element = $(this).parents(".setOBJ").find('.input_position_below').eq(0);
                    // if (the_element.children('.readonly-overlay').length <= 0) {
                    //     the_element.prepend("<div class='readonly-overlay' style='position:absolute;width:100%;height:100%;background-color:rgba(0,0,0,0)'></div>");

                    // }
                });
            }
            // FIXES by michael... 8/4/2015
            //fixes dun sa mga button ng embed na nag ttrigger kapag nag enter lang sa mga fields
            $(".embed_newRequest").filter("button").attr("type", "button");
            //added by japhet
            //04-07-2016
            //fix the table object for dynamic row
            $('[repeater-table="true"]').parents('.input_position_below').css('overflow', 'auto');
        },
        "preparePrintDisplayFixes": function () {

            var pathname = window.location;
            if (getParametersName("print_form", pathname) != "true") {
                return false;
            }

            {//embed reveal all scrolls

                var sorted_top_embeded_objs = $('.setOBJ[data-type="embeded-view"]').sort(function (prev, next) {
                    return $(prev).position().top - $(next).position().top; //sort ascending in position top
                });
                //save first all data 
                sorted_top_embeded_objs.each(function () {
                    var dis_self = $(this);
                    var dis_self_doi = dis_self.attr("data-object-id");
                    var dis_self_original_oheight = dis_self.outerHeight();
                    var dis_self_origin_pos_bottom = dis_self.position().top + dis_self_original_oheight;

                    var dis_self_origin_pos_left = dis_self.position().left;
                    var dis_self_origin_pos_right = dis_self.position().left + dis_self.outerWidth();

                    var affected_left = dis_self_origin_pos_left;
                    var affected_right = dis_self_origin_pos_right;

                    //collecting objects embeds parallel position below based on its left and right position
                    var objects_below_dis_self = dis_self.parent().children(".setOBJ").sort(function (prev, next) {
                        return $(prev).position().top - $(next).position().top;
                    }).filter(function () {
                        var dis_below_obj = $(this);
                        var dis_below_obj_doi = dis_below_obj.attr("data-object-id");
                        if (dis_self_doi == dis_below_obj_doi) {
                            return false;
                        }
                        var dis_below_obj_top = dis_below_obj.position().top;
                        var dis_below_obj_left = dis_below_obj.position().left;
                        var dis_below_obj_right = dis_below_obj.position().left + dis_below_obj.outerWidth();

                        if (dis_self_origin_pos_bottom <= dis_below_obj_top) { //filter to get all objects below embed
                            if (affected_left <= dis_below_obj_right && affected_right >= dis_below_obj_left) { //filter to get 
                                if (affected_left > dis_below_obj_left) {
                                    affected_left = dis_below_obj_left;
                                }
                                if (affected_right < dis_below_obj_right) {
                                    affected_right = dis_below_obj_right;
                                }
                                return true;
                            }
                        }
                        return false;
                    });

                    dis_self.data("print_reveal_scroll_ele_affected", {
                        "dis_self_original_oheight": dis_self_original_oheight,
                        "dis_self_origin_pos_bottom": dis_self_origin_pos_bottom,
                        "ele_below": objects_below_dis_self
                    });
                });

            }
        },
        "exePrintDisplayFixes": function (embed) {
            var ele_set_obj = $(embed).parents('.setOBJ[data-type="embeded-view"]').eq(0);
            if (ele_set_obj.is('.setOBJ[data-type="embeded-view"]')) {

                if (ele_set_obj.is(':data("print_reveal_scroll_ele_affected")') == false) {
                    return true;
                }
                var dis_self_embed_set_obj_data = ele_set_obj.data("print_reveal_scroll_ele_affected");
                var dis_self_embed_set_obj_data_affected_below = dis_self_embed_set_obj_data["ele_below"];
                // {
                var dis_self_doi = ele_set_obj.attr("data-object-id");
                var dis_self_embed_obj_view = ele_set_obj.find(".embed-view-container.getFields_" + dis_self_doi);
                var dis_self_embed_obj_view_origin_oheight = dis_self_embed_obj_view.outerHeight();
                ele_set_obj.css("display", "inline-table");
                dis_self_embed_obj_view.css("display", "inline-table");
                var added_embed_height = (dis_self_embed_obj_view.outerHeight() - dis_self_embed_obj_view_origin_oheight);
                dis_self_embed_set_obj_data_affected_below.each(function () {
                    var dis_inner_below_obj = $(this);
                    dis_inner_below_obj.css({
                        "top": (dis_inner_below_obj.position().top + added_embed_height) + "px"
                    });
                });
                var last_affected = dis_self_embed_set_obj_data_affected_below.last();
                var form_container = $('#frmrequest').children('.loaded_form_content').eq(0);
                var form_container_oheight = form_container.outerHeight();
                if (last_affected.length >= 1) {
                    var last_affected_bottom = last_affected.position().top + last_affected.outerHeight();


                    var form_container_oheight_added = last_affected_bottom - form_container_oheight;
                    if (form_container_oheight_added >= 0) {
                        form_container.css({
                            "height": (form_container.outerHeight() + form_container_oheight_added) + "px"
                        });
                    }
                }

                // }

                //if this embed passed the container bottom
                form_container_oheight = form_container.outerHeight();
                var ele_set_obj_bottom = ele_set_obj.position().top + ele_set_obj.outerHeight();
                if (form_container_oheight <= ele_set_obj_bottom) {
                    form_container.css({
                        "height": (ele_set_obj_bottom) + "px"
                    });
                }
                $('a.embed_newRequest').addClass("display2");
            }


        },
        /*search validation*/
        "inputValidation": function (buttonObject) {
            var error_counter = 0;
            var error_outside_container = 0;
            $(".setOBJ").each(function () {
                var dis_elem = $(this);
                var doi = $(dis_elem).attr("data-object-id");
                var get_fields_ele = dis_elem.find(".getFields_" + doi);
                var container = dis_elem.parent();
                if (container.is('.td-relative')) {
                    container = container.closest('.setOBJ[data-type="table"]').parent();
                }


                //if( dis_elem.is('[data-type="multiple_attachment_on_request"]') ){
                //    var reg_exp_attachment_value = new RegExp("\\[.*\\]","g");
                //    var get_valid_attachment_val = (dis_elem.val()||"").match(reg_exp_attachment_value);
                //    var get_origin_attachment_val = (dis_elem.val()||"");

                //    console.log("TESTING ERROR ATTACHMENT: ", get_origin_attachment_val ==  get_valid_attachment_val, get_valid_attachment_val, get_origin_attachment_val)

                //}
                //error_counter++;
                buttonFieldRequired = $(buttonObject).attr("field_required");
//                alert(buttonFieldRequired);
                var isFieldRequired = false;
                if (buttonFieldRequired != "") {
                    var buttonFieldRequiredArr = buttonFieldRequired.split(',');
                    if (buttonFieldRequiredArr.indexOf(get_fields_ele.attr("name")) > -1) {
                        isFieldRequired = true;
                    }
                } else {
                    isFieldRequired = get_fields_ele.attr("required");
                }
                if (isFieldRequired) {
                    if (get_fields_ele.is('input[type="checkbox"]')) {
                        if (get_fields_ele.filter(':checked').length <= 0) {
                            validationNotification(get_fields_ele, "Required!");
                            error_counter++;
                            if (!container.is('.loaded_form_content')) {
                                containerWithRequiredFields(dis_elem);
                            } else {
                                error_outside_container++;
                            }
                        }

                    }
                    if (get_fields_ele.is('input[type="radio"]')) {
                        if (get_fields_ele.filter(':checked').length <= 0 || get_fields_ele.filter(':checked').val() == "none") {
                            validationNotification(get_fields_ele, "Required!");
                            error_counter++;
                            if (!container.is('.loaded_form_content')) {
                                containerWithRequiredFields(dis_elem);
                            } else {
                                error_outside_container++;
                            }
                        }
                    }
                    if (get_fields_ele.is('select[multiple]')) {

                        if (get_fields_ele.children('option:selected').length <= 0) {
                            validationNotification(get_fields_ele, "Required!");
                            error_counter++;
                            if (!container.is('.loaded_form_content')) {
                                containerWithRequiredFields(dis_elem);
                            } else {
                                error_outside_container++;
                            }
                        }
                    }
                    if ($.trim(get_fields_ele.val()) == "" || $.trim(get_fields_ele.val()).toLowerCase() == "--select--") {
                        //validationNotification(get_fields_ele, "Required!");
                        if ($(".table-form-design-relative").length >= 1) {
                            var toolstips = validationNotification(get_fields_ele, "Required!");
                            var tooltip_fixer = Number(dis_elem.css("margin-top").replace("px", ""));
                            var margin_left = dis_elem.css("margin-left");
                            margin_left = Number(margin_left.replace("px", ""));
                            var width_size = dis_elem.outerWidth();
                            var margin_ryt = margin_left + width_size;
                            toolstips.css("top", (tooltip_fixer + 5) + "px");
                            toolstips.css("left", (margin_ryt + 5) + "px");
                        } else if (get_fields_ele.is('textarea') && get_fields_ele.parent().hasClass('content-editable-containter-value')) {

                            var ns_parent_container = get_fields_ele.parents('.setOBJ[data-type="noteStyleTextarea"]').eq(0);
                            if (ns_parent_container.is('[fluid-container-cell="Yes"]')) {
                                //get_fields_ele.show();
                                var displayed_tooltip_element = validationNotification(get_fields_ele, "Required!");
                                var ns_top = ns_parent_container.css('margin-top');
                                var ns_left = ns_parent_container.css('margin-left');
                                var ns_o_width = ns_parent_container.outerWidth();
                                displayed_tooltip_element.css({
                                    'top': ns_top,
                                    'left': (Number(ns_left.replace('px', "")) + ns_o_width) + "px"
                                });
                                //get_fields_ele.hide();
                            } else {

                                validationNotification(get_fields_ele, "Required!");
                            }
                        } else {
                            validationNotification(get_fields_ele, "Required!");
                        }

                        error_counter++;
                        if (!container.is('.loaded_form_content')) {
                            containerWithRequiredFields(dis_elem);
                        } else {
                            error_outside_container++;
                        }
                    }
                }
                if (get_fields_ele.attr("field-formula-validation")) {
                    var field_data_validation_message = "Invalid value!";
                    if (get_fields_ele.attr("field-formula-validation-message")) {
                        if (get_fields_ele.attr("field-formula-validation-message") != "") {
                            field_data_validation_message = "" + get_fields_ele.attr("field-formula-validation-message");
                        }
                    }

                    var field_formula_validation = get_fields_ele.attr("field-formula-validation");
                    if (field_formula_validation != "") {
                        (function InputValidationDynamicTable() {
                            if (get_fields_ele.closest(".setOBJ").parent().is(".td-relative")) {
                                var getting_fieldnames_on_formula = field_formula_validation.match(/@[a-zA-Z0-9_]*/g);
                                var temp_regeks = null;
                                if (getting_fieldnames_on_formula != null) {
                                    getting_fieldnames_on_formula = getting_fieldnames_on_formula.filter(Boolean);
                                    for (var keyi in getting_fieldnames_on_formula) {
                                        if (get_fields_ele.closest("tr").find('[rep-original-name="' + getting_fieldnames_on_formula[keyi].replace("@", "") + '"]').length >= 1) {
                                            // temp_regeks = new RegExp("@"+get_fields_ele.closest("tr").find('[rep-original-name="'+getting_fieldnames_on_formula[keyi].replace("@","")+'"]').attr("name")+"(?![a-zA-Z0-9_])","g");
                                            temp_regeks = new RegExp(getting_fieldnames_on_formula[keyi] + "(?![a-zA-Z0-9_])", "g");
                                            field_formula_validation = field_formula_validation.replace(temp_regeks, "@" + get_fields_ele.closest("tr").find('[rep-original-name="' + getting_fieldnames_on_formula[keyi].replace("@", "") + '"]').attr("name"));
                                        }
                                    }
                                }
                            }
                        })();
                        var formula_parser = new Formula(field_formula_validation);
                        if (formula_parser.getEvaluation() == false) {
                            var temporary = validationNotification(get_fields_ele, field_data_validation_message);
                            var label_inner = get_fields_ele.parents('#setObject_' + doi).children().find('#label_' + doi).children();
                            var label_inner_width = label_inner.width();
                            console.log("AGAS", label_inner, "Width", label_inner_width);
                            console.log("lagyan ng left", temporary);
                            //label_inner.wrapInner('<span/>');
                            //var computed_left = get_fields_ele.outerWidth() + temp2.wrapInner('<span/>').children().width();
                            // console.log("ACOMP", computed_left, "BTEMP",temporary.css('width'))
                            console.log();
                            temporary.css({
                                //'left':( Number(temporary.css('left').replace("px","")) - computed_left )+"px"
                                //'left':label_inner_width
                            });
                            console.log("LAST", get_fields_ele)
                            //console.log("TEERTING ", validationNotification(get_fields_ele, field_data_validation_message));
                            error_counter++;
                            if (!container.is('.loaded_form_content')) {
                                containerWithRequiredFields(dis_elem);
                            } else {
                                error_outside_container++;
                            }
                            //temp2.children().unwrap();
                        }
                    }
                }
                var fieldDataType = get_fields_ele.attr('data-input-type');
                if (fieldDataType) {
                    field_Value = get_fields_ele.val();
                    if (fieldDataType == 'Currency') {
                        var temp = field_Value.replace(/^\s?-/, "") || "";
                        temp = temp.replace(/[0-9\.,]+/g, "") || "";
                        if (temp.length >= 1) {
                            validationNotification(get_fields_ele, "The value you entered is not a number");
                            error_counter++;
                        }
                    }
                }
                if (get_fields_ele.attr('data-type') && get_fields_ele.is(':not([disabled="disabled"])')) {
                    if (get_fields_ele.attr('data-type') == "date" /*&& get_fields_ele.is('[required="required"]')*/) {
                        var field_Value = get_fields_ele.val();
                        var pattern = /^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/;

                        if (ThisDate(field_Value) == "Invalid Date" && field_Value != "0000-00-00") {
                            //validation prompt
                            if (field_Value == "" && get_fields_ele.is(':not([required="required"])')) {

                            } else {
                                validationNotification(get_fields_ele, "The value you entered is not a date");
                                error_counter++;
                                if (!container.is('.loaded_form_content')) {
                                    containerWithRequiredFields(dis_elem);
                                } else {
                                    error_outside_container++;
                                }
                            }
                        } else if (pattern.test(field_Value) == false || pattern.test(field_Value) == "false") {
                            validationNotification(get_fields_ele, "Date format should be yyyy-mm-dd");
                            error_counter++;
                            if (!container.is('.loaded_form_content')) {
                                containerWithRequiredFields(dis_elem);
                            } else {
                                error_outside_container++;
                            }
                        }
                    }
                    if (get_fields_ele.attr('data-type') == "dateTime" /*&& get_fields_ele.is('[required="required"]')*/) {
                        var field_Value = get_fields_ele.val();
                        var pattern = /[0-9]{4}\-[0-9]{2}\-[0-9]{2}\s[0-9]{1,2}:[0-9]{2}(:[0-9]{2})?(\s(AM|PM))?/;

                        if (ThisDate(field_Value) == "Invalid Date") {
                            if (field_Value == "" && get_fields_ele.is(':not([required="required"])')) {

                            } else {
                                validationNotification(get_fields_ele, "The value you entered is not a datetime");
                                error_counter++;
                                if (!container.is('.loaded_form_content')) {
                                    containerWithRequiredFields(dis_elem);
                                } else {
                                    error_outside_container++;
                                }
                            }
                        } else if (pattern.test(field_Value) == false || pattern.test(field_Value) == "false") {
                            validationNotification(get_fields_ele, "Datetime format should be yyyy-mm-dd hh:ii:ss");
                            error_counter++;
                            if (!container.is('.loaded_form_content')) {
                                containerWithRequiredFields(dis_elem);
                            } else {
                                error_outside_container++;
                            }
                        }
                    }
                    if ((get_fields_ele.attr('data-type') == "float" || get_fields_ele.attr('data-type') == "double") /*&& get_fields_ele.is('[required="required"]')*/) {
                        var field_Value = $.trim(get_fields_ele.val());
                        var fieldDataType = get_fields_ele.attr('data-input-type');
                        //alert(field_Value.replace(/[^0-9\.]+/g, ""))
                        if (fieldDataType == 'Currency') {
                            field_Value = Number(field_Value.replace(/,/g, "")); // [^0-9\.]+

                        }

                        if (!$.isNumeric(field_Value) && field_Value != "") {

                            validationNotification(get_fields_ele, "Numbers only");
                            error_counter++;
                            if (!container.is('.loaded_form_content')) {
                                containerWithRequiredFields(dis_elem);
                            } else {
                                error_outside_container++;
                            }
                        }
                    }
                }
                if (dis_elem.is('[data-type="contactNumber"]')) {
                    var temp = get_fields_ele.data('validate.contactField')();
                    if (temp['errors'] >= 1) {
                        error_counter++;
                        // validationNotification( $(temp['error_fields']).map(function(a,b){ return b['element'].get(); }) , temp['message']);
                        $.each(temp['error_fields'], function (a, b) {
                            error_counter++;
                            b['element'].parents('.contact-number-temp-field-container:eq(0)').parent().append(NotifyMe(b['element'], b['error_message'], 'contact-field-validation').css('width', '100px'));
                            // b['element'].parents('.contact-number-temp-field-container:eq(0)').parent().append(NotifyMe(b['element'], b['error_message'], 'contact-field-validation'));
                        });
                    }
                }
            });
            if (error_outside_container >= 1) {
                $('.loaded_form_content').attr("data-error-outside-container", error_outside_container);
            } else {
                $('.loaded_form_content').removeAttr("data-error-outside-container");
            }
            return error_counter;
        },
        "requireCommentDialog": function (btn) {
            var ret = "<div style='font-weight: bold;margin-bottom: 10px;color:black' class='fl-margin-top'>Comment is required. Please write your comment here.</div>";
            ret += '<div style="margin-left:10px">';
            ret += '<textarea class="form-textarea" id="require-comment-txt" placeholder=""></textarea>';
            ret += '</div>';
            return ret;
        },
        "evtSubmitRequest": function () {
            var self = this;


            $('body').on('click', '.close-confirmation', function (e) {
                e.preventDefault();
                e.stopPropagation();
                //$('iframe')[0].contentWindow["jConfirm"]();
                if ($('iframe').contents().find('.fl-action-close:eq(0)').exists()) {
                    $('iframe').contents().find('.fl-action-close:eq(0)').trigger('click');
                } else {
                    $(this).removeClass('close-confirmation').click();
                }

            });

            $('body').on('click', '.fl-action-close', function (e) {
                if (!$('#popup_container').exists()) {
                    var redirectPage = $('#DashboardPage').val();
                    var isInIFrame = (window.location != window.parent.location); //#FS
                    var newConfirm = new jConfirm("Are you sure you want to close this form? ", 'Confirmation Dialog', '', '', '', function (answer) {
                        if (answer == true) {
                            $.post('/modules/data/remove_document_lock', {
                                form_id: $('#FormID').val(),
                                request_id: $('#ID').val()
                            }, function () {
                                window.onbeforeunload = null;
                                if (isInIFrame == true) {
                                    //search confirmation
                                    window.parent.$('iframe').parents('[id="popup_container"]').eq(0).find('.fl-closeDialog').removeClass('close-confirmation').eq(0).trigger('click');
                                } else if (window.fromParent) {
                                    window.close();
                                } else {
                                    window.location = redirectPage;
                                }
                                embedded_view_fs.delete_multiple_records();
                            });

                        }
                    });

                    newConfirm.themeConfirm("confirm2", {
                        'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'
                    })
                }
            });



            $('body').on('click', '.fl-action-menu .fl-action-submit', function () { //#fl-action-option
                var this_fl_submit = $(this);
                if (this_fl_submit.is('[submit-in-progress="true"]')) { // FS#8252 
                    showNotification({
                        message: "Submission in progress. Please wait...",
                        type: "information",
                        autoClose: true,
                        duration: 3
                    });
                    return false; // FS#8252 
                }
                // IAN REQUEST [08-25-2016]
                $(this).addClass('temp-action-button-class-formula-keyword');

                if ($(this).hasClass('submit-event-blocker')) {
                    showNotification({
                        message: "Some fields are still in process. Please wait...",
                        type: "information",
                        autoClose: true,
                        duration: 3
                    });
                    return false;
                }

                if ($('.refresh_embed label').text().indexOf("Importing Records...") > -1) {
                    showNotification({
                        message: "Some fields are still in process. Please wait...",
                        type: "information",
                        autoClose: true,
                        duration: 3
                    });
                    return false;
                }

                var getProcedures = $(this).text();

                if (getProcedures == 'Get Procedures') {
                    var referenceNumber = $('[name="Container_ReferenceNumber"]').val();
                    if (referenceNumber != '') {
                        $.get('/ajax/velco_procedures', {
                            referencenumber: referenceNumber
                        }, function (data) {
                            $('[name="steps"]').val(data);
                        });
                    }
                    return;
                }

                var require_comment = $(this).attr("require_comment");
                var require_comment_html = "";
                if (require_comment == "1") {
                    var require_comment_html = self.requireCommentDialog(this);
                }
                var selected_action_name = this.getAttribute("value")
                console.log(selected_action_name)
                // alert(selected_action_name)
                var pathname = window.location;
                if (getParametersName("view_type", pathname) == "preview") {
                    return false;
                }
                self.getKeywordFields();




                var server_date = (typeof $("body").data().getServerTimeData === "undefined") ? null : $("body").data().getServerTimeData;
                if (server_date == null) {
                    console.log("SERVER DATE DID NOT GET", server_date)
                } else {
                    console.log("SERVER DATE GET", server_date)
                }

                var invalid_datas = 0;
                //set data validation
                {
                    // $(".setOBJ").each(function() {

                    //     var data_obj_id = $(this).attr("data-object-id");
                    //     var the_field = $(this).find(".getFields_" + data_obj_id);
                    //     //console.log("TESTING ATTR SELECTOR JQUERY",the_field);
                    //     if (
                    //             (typeof the_field.attr("required") != "undefined" && the_field.val() != "") ||
                    //             (typeof the_field.attr("required") == "undefined")
                    //             ) {

                    //         var invalid_field_data = 0;
                    //         console.log("BDT", the_field)
                    //         if (the_field.attr("field-formula-validation")) {
                    //             console.log("TEST VALIDTIOn", the_field.attr("field-formula-validation"))
                    //             if (the_field.attr("field-formula-validation") != null || the_field.attr("field-formula-validation") != '') {
                    //                 var field_data_validation_message = "Invalid value";
                    //                 if (typeof the_field.attr("field-formula-validation-message") != "undefined") {
                    //                     field_data_validation_message = "" + the_field.attr("field-formula-validation-message");
                    //                 }

                    //                 var field_data_validation_formula = the_field.attr("field-formula-validation");
                    //                 if (field_data_validation_formula.indexOf("@Date_Only(") >= 0) {
                    //                     field_data_validation_formula = field_data_validation_formula.replace(/@Date_Only\(/g, "Date_Only(");
                    //                 }
                    //                 var FDVF_splitted;
                    //                 var getting_name_re = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*", "g");
                    //                 try {
                    //                     //FDVF_splitted = field_data_validation_formula.split(/<=|>=|==|!=|<|>|Date_Only\(|\(|\)|!/g);

                    //                     FDVF_splitted = field_data_validation_formula.match(getting_name_re);
                    //                     if (typeof FDVF_splitted === "object") {
                    //                         FDVF_splitted = FDVF_splitted.filter(Boolean);
                    //                     }

                    //                     if (FDVF_splitted.length >= 1) {
                    //                         for (var u_i = 0; u_i < FDVF_splitted.length; u_i++) {
                    //                             FDVF_splitted[u_i] = $.trim(FDVF_splitted[u_i]);
                    //                         }
                    //                     }
                    //                 } catch (error) {
                    //                     //
                    //                 }
                    //                 console.log("OHH NOOOO!!")
                    //                 console.log(FDVF_splitted)
                    //                 //check if date or number or other
                    //                 try {
                    //                     var numeric_data_type = 0;
                    //                     var date_data_type = 0;
                    //                     var other_data_type = 0;
                    //                     var data_type_status = "";
                    //                     for (var ctr = 0; ctr < FDVF_splitted.length; ctr++) {
                    //                         if (typeof FDVF_splitted[ctr] == "undefined" || $.trim(FDVF_splitted[ctr]) == "") {
                    //                             continue;
                    //                         }
                    //                         string_field_name = $.trim(FDVF_splitted[ctr]).split("@").join("");
                    //                         field_affected_on_formula = $('[name="' + string_field_name + '"]');
                    //                         if (field_affected_on_formula.length >= 1) {
                    //                             var FAOF_val = field_affected_on_formula.val();
                    //                             if (typeof field_affected_on_formula.attr("data-input-type") != "undefined") {
                    //                                 if (field_affected_on_formula.attr("data-input-type") == "Currency") {
                    //                                     FAOF_val = FAOF_val.replace(/,/g, "");
                    //                                 }
                    //                             }
                    //                             if ($.isNumeric(FAOF_val)) {
                    //                                 numeric_data_type++;
                    //                             } else if (!isNaN(Date.parse(FAOF_val))) {
                    //                                 date_data_type++;
                    //                             } else if (FAOF_val == "") {
                    //                                 other_data_type++;
                    //                                 //return true; //hindi na i rereturn kasi meron pala sa formula kapag ung value ng field walang laman ok lang
                    //                                 // I return so that the required field validation of jewel will not be affected
                    //                             } else {
                    //                                 // alert("NAKO: "+string_field_name);
                    //                                 // alert(FAOF_val);
                    //                                 other_data_type++;
                    //                             }
                    //                         } else {
                    //                             // console.log("dito ung mga value na hindi fieldname")
                    //                             // console.log(string_field_name)
                    //                             if (("@" + string_field_name) == "@Now") {
                    //                                 date_data_type++;
                    //                             } else if (("@" + string_field_name) == "@Today") {
                    //                                 date_data_type++;
                    //                             } else {
                    //                                 if ($.isNumeric(string_field_name)) {
                    //                                     numeric_data_type++;
                    //                                 } else if (!isNaN(Date.parse(string_field_name))) {
                    //                                     date_data_type++;
                    //                                 } else {
                    //                                     // alert("eto:::"+string_field_name)
                    //                                     // alert("NAKO: "+string_field_name);
                    //                                     // alert($('[name="'+string_field_name+'"]').length);
                    //                                     other_data_type++;
                    //                                 }
                    //                             }
                    //                         }
                    //                     }
                    //                     var data_consitency_status = 0;
                    //                     if (numeric_data_type >= 1) {
                    //                         data_type_status = "numeric";
                    //                         data_consitency_status++;
                    //                     }
                    //                     if (date_data_type >= 1) {
                    //                         data_type_status = "date";
                    //                         data_consitency_status++;
                    //                     }
                    //                     if (other_data_type >= 1) {
                    //                         data_type_status = "unknown";
                    //                         //data_consitency_status++;
                    //                         // console.log("ETOHAN unknown");
                    //                         // console.log(the_field)
                    //                         //requiredFields_v2(the_field);
                    //                         // alert(" value: "+the_field.val())
                    //                         //validationNotification(the_field, "Invalid value");
                    //                         //alert(44)
                    //                         //invalid_field_data++;
                    //                     }
                    //                     // console.log(
                    //                     //     "numeric:"+numeric_data_type+
                    //                     //     "\ndate:"+date_data_type+
                    //                     //     "\nother:"+other_data_type
                    //                     // )
                    //                 } catch (err) {

                    //                 }
                    //                 // alert("OK PA ETO TINGIN\n"+field_data_validation_formula)
                    //                 if (invalid_field_data >= 1) {
                    //                     //requiredFields_v2(the_field);
                    //                     // console.log("invalid field data other");
                    //                     // console.log(the_field)

                    //                     // alert("invalid field data")
                    //                     // alert(field_data_validation_formula)
                    //                     console.log("invalid_field_data")
                    //                     validationNotification(the_field, "Invalid value");
                    //                     //alert(33)
                    //                     invalid_datas++;
                    //                     return true;
                    //                     //} else if (data_consitency_status != 1) {
                    //                     //requiredFields_v2(the_field);
                    //                     // console.log("data consistent");
                    //                     // console.log(the_field)
                    //                     // console.log("data_consitency_status")
                    //                     //validationNotification(the_field, "Invalid value");
                    //                     //alert(22)
                    //                     //invalid_datas++;
                    //                     //return true;
                    //                 } else {
                    //                     console.log("wALA EERORe")
                    //                     try {
                    //                         console.log("field_data_validation_formula")
                    //                         console.log(FDVF_splitted)
                    //                         // alert("OK")

                    //                         for (var ctr = 0; ctr < FDVF_splitted.length; ctr++) {
                    //                             if (typeof FDVF_splitted[ctr] == "undefined" || $.trim(FDVF_splitted[ctr]) == "") {
                    //                                 continue;
                    //                             }
                    //                             string_field_name = $.trim(FDVF_splitted[ctr]).split("@").join("");
                    //                             field_affected_on_formula = $('[name="' + string_field_name + '"]');
                    //                             console.log("ituu")
                    //                             console.log(field_affected_on_formula)
                    //                             if (field_affected_on_formula.length >= 1) {
                    //                                 var FAOF_val = field_affected_on_formula.val();
                    //                                 if (typeof field_affected_on_formula.attr("data-input-type") != "undefined") {
                    //                                     if (field_affected_on_formula.attr("data-input-type") == "Currency") {
                    //                                         FAOF_val = FAOF_val.replace(/,/g, "");
                    //                                     }
                    //                                 }
                    //                                 if (data_type_status == "numeric") {
                    //                                     field_data_validation_formula = field_data_validation_formula.replace("@" + string_field_name, FAOF_val);
                    //                                 } else if (data_type_status == "date") {
                    //                                     // console.log("@fieldName "+string_field_name+":"+FAOF_val); //DITO NA AKO
                    //                                     field_data_validation_formula = field_data_validation_formula.replace("@" + string_field_name, Date.parse(FAOF_val));
                    //                                 } else if ($.isNumeric(FAOF_val)) {
                    //                                     field_data_validation_formula = field_data_validation_formula.replace("@" + string_field_name, FAOF_val);
                    //                                 } else {
                    //                                     field_data_validation_formula = field_data_validation_formula.replace("@" + string_field_name, "\"" + FAOF_val + "\"");
                    //                                 }
                    //                             } else {
                    //                                 if (data_type_status == "numeric") {

                    //                                 } else if (data_type_status == "date") {
                    //                                     if (("@" + string_field_name) == "@Now") {
                    //                                         // console.log("@Now server_date: "+server_date);
                    //                                         field_data_validation_formula = field_data_validation_formula.replace("@" + string_field_name, Date.parse(server_date));
                    //                                     } else if (("@" + string_field_name) == "@Today") {
                    //                                         //alert(server_date);
                    //                                         // console.log("server_date: "+server_date)
                    //                                         var date_from_server = new Date(server_date);
                    //                                         var today_formatted_from_server = date_from_server.getFullYear() + "-" + (date_from_server.getMonth() + 1) + "-" + date_from_server.getDate();
                    //                                         // console.log("@Today today_formatted_from_server: "+today_formatted_from_server);
                    //                                         field_data_validation_formula = field_data_validation_formula.replace("@" + string_field_name, Date.parse(today_formatted_from_server));
                    //                                     } else {
                    //                                         // console.log("'yy-mm-dd' string_date: "+string_field_name);
                    //                                         field_data_validation_formula = field_data_validation_formula.replace(string_field_name, Date.parse(string_field_name));
                    //                                     }
                    //                                 }
                    //                             }
                    //                             // alert("11 "+FDVF_splitted[ctr])
                    //                             // alert("ETO TINGIN\n"+field_data_validation_formula+" "+ctr)
                    //                             // console.log(field_affected_on_formula)
                    //                             // console.log(field_data_validation_formula)
                    //                         }

                    //                         // alert(field_data_validation_formula)
                    //                         console.log("TESTING STRINGS");

                    //                         console.log(field_data_validation_formula)
                    //                         field_data_validation_formula = field_data_validation_formula.replace(/@/g, "");
                    //                         // console.log(eval(field_data_validation_formula))
                    //                         // console.log(typeof eval(field_data_validation_formula))
                    //                         //$(".getFname").html(field_data_validation_formula)
                    //                         if (typeof eval(field_data_validation_formula) == "boolean") {
                    //                             console.log("pasok")
                    //                             console.log(eval(field_data_validation_formula))
                    //                             if (eval(field_data_validation_formula) == true) {
                    //                                 //ok
                    //                                 // alert(field_data_validation_formula)
                    //                             } else if (eval(field_data_validation_formula) == false) {
                    //                                 // alert(field_data_validation_formula + "ETO?")
                    //                                 // console.log("nag false");
                    //                                 // console.log(field_data_validation_formula);
                    //                                 //hindi ok
                    //                                 //the_field.attr("data-original-title","Invalid value");
                    //                                 //requiredFields_v2(the_field);
                    //                                 // console.log("LAST");
                    //                                 // console.log(the_field);
                    //                                 validationNotification(the_field, field_data_validation_message);
                    //                                 //alert(55);
                    //                                 invalid_datas++;
                    //                             }
                    //                         } else {
                    //                             console.log("pasok32")
                    //                             validationNotification(the_field, "Invalid result on formula");
                    //                             invalid_datas++;
                    //                         }
                    //                     } catch (err) {
                    //                         console.log("error")
                    //                         //console.log(field_data_validation_formula,eval(field_data_validation_formula))
                    //                         console.log(field_data_validation_formula)
                    //                         invalid_datas++;
                    //                         //invalid_datas
                    //                     }
                    //                 }
                    //             }
                    //         }
                    //         if (the_field.attr("data-type")) {

                    //             if (the_field.attr("data-type") == "longtext") {

                    //             } else if (the_field.attr("data-type") == "float" || the_field.attr("data-type") == "double") {

                    //                 var field_Value = the_field.val();
                    //                 var fieldDataType = the_field.attr('data-input-type');

                    //                 if (fieldDataType == 'Currency') {
                    //                     field_Value = Number(field_Value.replace(/[^0-9\.]+/g, ""));
                    //                 }

                    //                 if (!$.isNumeric(field_Value) && field_Value != "") {
                    //                     //requiredFields_v2(the_field);
                    //                     validationNotification(the_field, "Numbers only");
                    //                     invalid_datas++;
                    //                 }
                    //             } else if(the_field.attr("data-type")=="date"){
                    //                  var field_Value = the_field.val();
                    //                  var pattern =/^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/;

                    //                 if(ThisDate(field_Value) == "Invalid Date"){
                    //                     validationNotification(the_field, "The value you entered is not a date");
                    //                     invalid_datas++;  

                    //                 }
                    //                 else if (pattern.test(field_Value) == false || pattern.test(field_Value) == "false"){
                    //                     validationNotification(the_field, "Date format should be yyyy-mm-dd");
                    //                     invalid_datas++;  

                    //                 }

                    //             }else if(the_field.attr("data-type")=="dateTime"){
                    //                  var field_Value = the_field.val();
                    //                  var pattern =/[0-9]{4}\-[0-9]{2}\-[0-9]{2}\s[0-9]{1,2}:[0-9]{2}(:[0-9]{2})?(\s(AM|PM))?/;
                    //                 //balik ako
                    //                 if(ThisDate(field_Value) == "Invalid Datetime"){
                    //                     validationNotification(the_field, "The value you entered is not a datetime");
                    //                     invalid_datas++;  

                    //                 }
                    //                 else if (pattern.test(field_Value) == false || pattern.test(field_Value) == "false"){
                    //                     validationNotification(the_field, "Datetime format should be yyyy-mm-dd hh:ii:ss");
                    //                     invalid_datas++;  

                    //                 }

                    //             }
                    //         }
                    //     }
                    // });
                }

                var isDraft = $(this).attr('isDraft');
                var action = $(this).attr('action');

                var requestStatus = $('#Status').val();
                var isValidationRequired = (
                        isDraft != 'true' &&
                        (
                                (action == 'Cancel' && requestStatus != 'DRAFT')
                                ||
                                (action == 'Save' && requestStatus != 'DRAFT')
                                ||
                                (action != 'Cancel' && action != 'Save')
                                )
                        );

                if (isValidationRequired)
                {
                    if (action != 'Cancel') {
                        invalid_datas = invalid_datas + self.inputValidation(this);
                    }

                    if (invalid_datas >= 1) {
                        var requiredContainers = [];
                        var errorMessage = "Please correct the field values";
                        var errors_outside_container = Number($('.loaded_form_content').attr("data-error-outside-container") || 0);
                        if (errors_outside_container <= 0) {
                            $('.container-required').each(function () {
                                var containerName = $(this).attr('tab-name') || $(this).text();
                                requiredContainers.push(containerName);
                            });

                            if (requiredContainers.length > 0) {
                                requiredContainers = requiredContainers.join(', ');
                                errorMessage = errorMessage + " in the ff. container(s) - " + requiredContainers;
                            }
                        }

                        showNotification({
                            message: errorMessage,
                            type: "error",
                            autoClose: true,
                            duration: 3
                        });
                    }
                    $('.loaded_form_content').removeAttr("data-error-outside-container");
                }
                if (invalid_datas < 1) {
                    var selectedAction = $(this).attr('value');// $('#actionRequestButton option:selected').val();
                    var selectedNode = $(this).attr('action');// $('#actionRequestButton option:selected').attr('action');
                    var selectedWorkflowID = $(this).attr('data-workflow-id'); //$('#actionRequestButton option:selected').attr('data-workflow-id');
                    var message = $(this).attr('message');
                    var message_success = $(this).attr('message_success');
                    var landing_page = $.trim($(this).attr('landing_page'));

                    if (message != undefined) {
                        message = message.trim();
                    }
                    if (!message || message == '' || message == 'undefined' || message == undefined) {
                        message = "Are you sure you want to proceed?";
                    }

                    if (!message_success || message_success == '' || message_success == 'undefined' || message_success == undefined) {
                        message_success = "Request has been submitted.";
                    }


                    $('#Node_ID').val(selectedNode);
                    $('#Workflow_ID').val(selectedWorkflowID);

                    //collect all formula on submit
                    if ($('[name="onsubmit-fields-wformula"]').length > 0 && $('[default-type="OnSubmit"][default-onsubmit-value]').length > 0) {
                        var onsubmit_formulas = [];
                        var onsubmit_formulas_stringified = [];
                        $('[default-type="OnSubmit"][default-onsubmit-value]:not([default-onsubmit-value=""])').each(function () {
                            var dis_self = $(this);
                            onsubmit_formulas.push({
                                "fname": dis_self.attr("name"),
                                "onsubmit_field_formula": dis_self.attr("default-onsubmit-value")
                            });

                        });
                        onsubmit_formulas_stringified = JSON.stringify(onsubmit_formulas);
                        $('[name="onsubmit-fields-wformula"]').val(onsubmit_formulas_stringified);
                    }

                    //loop embeded here
                    if ($(".embed-view-container").length > 0) {
                        var embedroutedata = [];
                        $(".embed-view-container").each(function () {
                            if ($(this).attr("erf-route-my-form-fields") != "undefined" || $(this).attr("erf-route-my-form-fields") != undefined) {
                                embedroutedata.push({
                                    "embed_parent_field": $(this).attr("erf-route-my-form-fields"),
                                    "embed_child_field": $(this).attr("erf-route-popup-form-fields"),
                                    "embed-source-form-val-id": $(this).attr("embed-source-form-val-id")
                                })
                            }
                        })
                        if (embedroutedata.length > 0) {
                            var embedroutedataStr = JSON.stringify(embedroutedata);
                            $("#prop-emebed").val(embedroutedataStr);
                        }
                    }

                    //if in sub request
                    if ($(this).attr("status-embed") == "Draft") {
                        $('#status-embed').val("Draft");
                    }

                    // if (selectedAction == '0') {
                    //     showNotification({
                    //         message: "Please select action",
                    //         type: "error",
                    //         autoClose: true,
                    //         duration: 3
                    //     });
                    //     return;
                    // }


//                    if (isValidationRequired && action != "Cancel") {
//                        var hasRequiredError = 0;
//                        var button_required_fields = $(this).attr("field_required");
//                        var buttonBlankArr = [];
//                        if (button_required_fields && button_required_fields != "") {
//                            var button_required_fields_arr = button_required_fields.split(',');
//                            for (var btn_ctr = 0; btn_ctr <= button_required_fields_arr.length - 1; btn_ctr++) {
//                                if ($('[name="' + button_required_fields_arr[btn_ctr] + '"]').val() == "") {
//                                    hasRequiredError++;
//                                    buttonBlankArr.push(button_required_fields_arr[btn_ctr]);
//                                }
//                            }
//
//                            if (hasRequiredError > 0) {
//                                showNotification({
//                                    message: "Please Fill out " + buttonBlankArr.join(),
//                                    type: "error",
//                                    autoClose: true,
//                                    duration: 3
//                                });
//                                return;
//                            }
//                        } else {
//                            $(".setOBJ .getFields").each(function () {
//                                if ($(this).attr("required")) {
//                                    //design for required field here
//                                    if ($(this).attr("type") == "checkbox") {
//
//                                    } else {
//                                        if ($(this).val() == "") {
//                                            hasRequiredError++;
//                                        }
//                                    }
//
//                                }
//                            });
//
//                            if (hasRequiredError > 0) {
//                                showNotification({
//                                    message: "Fill out all required fields",
//                                    type: "error",
//                                    autoClose: true,
//                                    duration: 3
//                                });
//                                return;
//                            }
//                        }
//
//
//                    } else {
//                        //require_comment_html = "";
//                    }


                    //ADDED JEWEL GET EDITORS AND READERS FIELD
                    var addEditors = [];
                    var addReaders = [];

                    $('[data-type="textbox_editor_support"] .getFields').each(function () {
                        addEditors.push($(this).val());
                    });

                    $('[data-type="textbox_reader_support"] .getFields').each(function () {
                        addReaders.push($(this).val());
                    });

                    $('#AdditionalEditors').val(addEditors.join("|"));
                    $('#AdditionalReaders').val(addReaders.join("|"));

                    //
                    self.getData();
                    var newConfirm = new jConfirm(message + require_comment_html, '10', '', '', '', function (r) {
                        if (r) {
                            if (require_comment_html != "") {
                                var comment = $.trim($("#require-comment-txt").val());
                                if (comment == "") {
                                    showNotification({
                                        message: "Please enter your comment.",
                                        type: "error",
                                        autoClose: true,
                                        duration: 3
                                    });
                                    return false;
                                }
                                $("#RequireComment").val(comment)
                            }

                            {//ADDED BY MICHAEL AFFECTED BY READONLY FORMULA FOR dropdown, select-multiple, radio checkbox
                                $('[readonly-disabled]').each(function () {
                                    $(this).removeAttr("disabled");
                                });
                            }


                            self.formatCurrency();
                            // $('#frmrequest').submit();
                            // return;
                            //                            //var submit_ajax = [];
                            //submit_ajax.push();
                            //$('[data-input-type="Currency"]').each(function(){
                            //    alert($(this).val());
                            //})

                            ui.block();
                            $.xhrpAbortAll();
                            initializePage.FormulaEventController("presave");
                            if ($('body').data('embed_row_data')) {
                                var json_params = {
                                    "embed_row_data": $('body').data('embed_row_data'),
                                    "prefix": "embed_"
                                };
                                if ($('body').data('embed_deleted_row')) {
                                    json_params['embed_deleted_row'] = $('body').data('embed_deleted_row');
                                }
                                $.post('/ajax/embedded_view_request', json_params, function (result_echoes) {
                                    console.log(result_echoes);
                                })
                            }
                            else if ($('body').data('embed_deleted_row')) {
                                var json_params = {
                                    "embed_row_data": $('body').data('embed_row_data') || {},
                                    "prefix": "embed_"
                                };
                                if ($('body').data('embed_deleted_row')) {
                                    json_params['embed_deleted_row'] = $('body').data('embed_deleted_row');
                                }
                                $.post('/ajax/embedded_view_request', json_params, function (result_echoes) {
                                    console.log(result_echoes);
                                })
                            }
                            // this_fl_submit.attr('submit-in-progress','true'); // FS#8252 


                            $.get('/modules/data/check_session', {}, function (session_data) {
                                if (session_data == '400') {
                                    showNotification({
                                        message: "User not logged-in. Please log-in using other tab to save your changes.",
                                        type: "error",
                                        autoClose: true,
                                        duration: 3
                                    }).css("z-index", 100000000);
                                    ui.unblock();
                                    return;
                                }

                                $('.fl-action-submit').attr('submit-in-progress', 'true'); // FS#8252 
                                $('#frmrequest').ajaxSubmit({
                                    data: {},
                                    success: function (data) {
                                        {//ADDED BY MICHAEL AFFECTED BY READONLY FORMULA FOR dropdown, select-multiple, radio checkbox
                                            $('[readonly-disabled]').each(function () {
                                                $(this).attr("disabled", "true");
                                            });
                                        }

                                        //  Get only the json data
                                        window.onbeforeunload = null;
                                        data = data.substring(data.indexOf('{'), data.length);
                                        console.log(data);

                                        var parsedData = JSON.parse(data);

                                        console.log('parsedData', parsedData, data);

                                        if (parsedData.message == 'Request was already processed by other user.') {

                                            showNotification({
                                                message: parsedData.message,
                                                type: "error",
                                                autoClose: true,
                                                duration: 2
                                            }).css("z-index", 100000000);

                                            setTimeout(function () {


                                                // if (parsedData.number) {
                                                //     var message = "The generated number for this request is " + parsedData.number;
                                                // }

                                                var isInIFrame = (window.location != window.parent.location);
                                                if (window.parent.pathname == "/user_view/home" || window.parent.pathname == "/user_view/gi-dashboard-home") {
                                                    ui.unblock();
                                                    // window.location = "/user_view" + parsedData.headerLocation;
                                                    // window.parent.$('#popup_container').find('.fl-closeDialog').trigger("click");

                                                    window.location.reload(true);

                                                    // if (parsedData.number) {
                                                    //     newjAlert = new jAlert(message, 'Confirmation Dialog', '', '', '', function(r) {});
                                                    //     newjAlert.themeAlert("jAlert2");
                                                    //     $('#popup_ok').on('click', function(){
                                                    //         window.location.reload(true);
                                                    //     });
                                                    // } else {
                                                    //     window.location.reload(true);
                                                    // }
                                                } else {

                                                    if (isInIFrame == true) {
                                                        var field_updater = window.parent.$('#popup_container').data("field_updater");
                                                        console.log("FIELD_UPDATER", field_updater);

                                                        var embed_container = window.parent.$('html').data("embed_container");
                                                        var embedded_container = window.parent.$('#popup_container').data("embed_container");
                                                        var get_embed_doi = embed_container.parents('.setOBJ[data-type="embeded-view"]').attr('data-object-id');
                                                        // embed_container = window.parent.$('.setOBJ[data-type="embeded-view"][data-object-id="' + get_embed_doi + '"]');
                                                        if (window.parent.$('[name="' + field_updater + '"]').length >= 1) {
                                                            window.parent.$('[name="' + field_updater + '"]').trigger("change");
                                                            setTimeout(function () {
                                                                window.parent.$('#popup_container').find('.fl-closeDialog').removeClass('close-confirmation').trigger("click"); //IFRAME FOR EMBED
                                                            }, 0);

                                                        } else if (embedded_container.length >= 1) {
                                                            embedded_container.trigger('embedEventV2.embedSearchComputed');
                                                            setTimeout(function () {
                                                                window.parent.$('#popup_container').find('.fl-closeDialog').removeClass('close-confirmation').trigger("click"); //IFRAME FOR EMBED
                                                            }, 0);
                                                        } else {
                                                            embeded_view.refreshEmbed(embed_container, null, null, function () {
                                                                ui.unblock();
                                                                window.parent.$('#popup_container').find('.fl-closeDialog').removeClass('close-confirmation').trigger("click"); //IFRAME FOR EMBED
                                                            });
                                                        }
                                                        //========================================================================
                                                    } else {
                                                        // ui.unblock();
                                                        window.location = "/user_view" + parsedData.headerLocation;

                                                        // if (parsedData.number) {
                                                        //     newjAlert = new jAlert(message, 'Confirmation Dialog', '', '', '', function(r) {});
                                                        //     newjAlert.themeAlert("jAlert2");
                                                        //     $('#popup_ok').on('click', function(){
                                                        //         window.location = "/user_view" + parsedData.headerLocation;
                                                        //     });
                                                        // } else {
                                                        //     window.location = "/user_view" + parsedData.headerLocation;
                                                        // }
                                                    }
                                                }


                                            }, 2000);
                                            return;
                                        }

                                        $.event.trigger({
                                            type: 'documentCreated',
                                            documentData: parsedData
                                        });

                                        $.event.trigger({
                                            type: 'sendNotification',
                                            sendTo: [parsedData.data.Processor],
                                            data: parsedData.data,
                                            notificationData: {
                                                notificationType: 'REQUEST_ACTION',
                                                requestData: parsedData
                                            }
                                        });

                                        $.event.trigger({
                                            type: 'updateChart',
                                            sendTo: [parsedData.data.Processor],
                                            data: parsedData.data,
                                            notificationData: {
                                                notificationType: 'UPDATE_CHART',
                                                requestData: parsedData
                                            }
                                        });

                                        $.event.trigger({
                                            type: 'updateEmbed',
                                            sendTo: [parsedData.data.Processor],
                                            data: parsedData.data,
                                            notificationData: {
                                                notificationType: 'UPDATE_EMBED',
                                                requestData: parsedData
                                            }
                                        });
                                        {  //AUTO GRAMMAR MESSAGE ACTION SUBMIT // by michael
                                            var temp_message = "Request has been submitted.";
                                            // var spl_selected_action_name = selected_action_name.split("");
                                            // var last_char_sel_action = selected_action_name.split("").pop();
                                            // var temp_message = "";
                                            // if(last_char_sel_action == "e"){
                                            //     temp_message = selected_action_name+"d";
                                            // }else if(selected_action_name.toLowerCase() == "submit"){
                                            //     temp_message = selected_action_name+"ted";
                                            // }else if(last_char_sel_action == "t"){
                                            //     temp_message = selected_action_name+"ed";
                                            // }else if(last_char_sel_action == "y"){
                                            //     spl_selected_action_name.pop();
                                            //     temp_message = spl_selected_action_name.join("")+"ied";
                                            // }else{
                                            //     temp_message = selected_action_name+"ed";
                                            // }
                                        }

                                        submitCreatePDF(parsedData.id, parsedData.formId, parsedData.trackNo, function () {
                                        });
                                        var pathname = window.location;
                                        if (getParametersName("public_forma_page", pathname) == "1" && PublicEmbedSetup) {
                                            PublicEmbedSetup.sendParentMessage({
                                                'event':'redirect'
                                            });
                                            // temp2 = {
                                            //     originProtocol: getParametersName("protocol", window.location),
                                            //     originPort: getParametersName("port", window.location),
                                            //     originName: getParametersName("hostname", window.location)
                                            // };
                                            // var temp = {
                                            //     event: "redirect"
                                            //             // height:$(document).outerHeight(true)+4,
                                            //             // height:($('*').map(function(){ return $(this).outerHeight(true); }).sort(function(a,b){ return b-a})[0]||0)+4,
                                            //             // buttons:$('.workflow_buttons').children(':not(".fl-action-close")').map(function(a,b){
                                            //             //     return {caption:$(this).text(), buttonIndex:$(this).index()};
                                            //             // }).get()
                                            // };
                                            // window.parent.postMessage(JSON.stringify(temp), temp2.originProtocol + '//' + temp2.originName + ':' + temp2.originPort);
                                            // window.parent.postMessage("redirect",temp2.originProtocol+'//'+temp2.originName+':'+temp2.originPort);
                                        }
                                        showNotification({
                                            message: message_success + ", Redirecting...",
                                            type: "success",
                                            autoClose: true,
                                            duration: 2
                                        }).css("z-index", 100000000);

                                        setTimeout(function () {


                                            // if (parsedData.number) {
                                            //     var message = "The generated number for this request is " + parsedData.number;
                                            // }

                                            var isInIFrame = (window.location != window.parent.location);



                                            if (window.parent.pathname == "/user_view/home") {
                                                ui.unblock();
                                                // window.location = "/user_view" + parsedData.headerLocation;
                                                // window.parent.$('#popup_container').find('.fl-closeDialog').trigger("click");

                                                window.location.reload(true);

                                                // if (parsedData.number) {
                                                //     newjAlert = new jAlert(message, 'Confirmation Dialog', '', '', '', function(r) {});
                                                //     newjAlert.themeAlert("jAlert2");
                                                //     $('#popup_ok').on('click', function(){
                                                //         window.location.reload(true);
                                                //     });
                                                // } else {
                                                //     window.location.reload(true);
                                                // }
                                            } else {
                                                if (isInIFrame == true) {

                                                    var field_updater = window.parent.$('#popup_container').data("field_updater");
                                                    if (typeof window.parent.$('html').data("embed_container").attr('source-form-type') != "undefined" && window.parent.$('html').data("embed_container").attr('source-form-type') == "multiple") {
                                                        var field_reference = window.parent.$('html').data("embed_container").data('field_reference')[0];
                                                        field_updater = field_reference;
                                                    }
                                                    // var request_id_picklist = '<div class="picklist-request-id-storage isDisplayNone">'+data+'</div>';
                                                    var request_id_picklist = '<div class="picklist-request-id-storage isDisplayNone">' + parsedData['trackNo'] + '</div>';
                                                    window.parent.$('body').prepend(request_id_picklist);
                                                    if (window.parent.$('body').find('.embed-iframe').exists()) {

                                                        var requestTrackNo = parsedData['trackNo'];
                                                        var requestID = parsedData['id'];
                                                        var formIDEmbed = parsedData['formId'];
                                                        var addedData = {
                                                            trackNo: requestTrackNo,
                                                            id: requestID,
                                                            formId: formIDEmbed
                                                        }
                                                        window.parent["addedRequest"].push(addedData);

                                                    }
                                                    if ($.type(field_updater) != "undefined") {
                                                        console.log("FIELD_UPDATER", field_updater);
                                                        var embed_container = window.parent.$('html').data("embed_container");
                                                        var get_embed_doi = embed_container.parents('.setOBJ[data-type="embeded-view"]').attr('data-object-id');
                                                        // embed_container = window.parent.$('.setOBJ[data-type="embeded-view"][data-object-id="' + get_embed_doi + '"]');
                                                        if (window.parent.$('[name="' + field_updater + '"]').length >= 1) {
                                                            window.parent.$('[name="' + field_updater + '"]').trigger("change");
                                                            var embed = window.parent.$('[embed-source-form-val-id="' + parsedData['formId'] + '"]');
                                                            setTimeout(function () {
                                                                if (embed.attr('commit-data-row-event') == 'parent') {
                                                                    embeded_view.createTemporaryData(embed, 'insert', 'popup', undefined, parsedData);
                                                                    window.parent.$('#popup_container').find('.fl-closeDialog').removeClass('close-confirmation').trigger("click"); //IFRAME FOR EMBED
                                                                }
                                                                else {
                                                                    window.parent.$('#popup_container').find('.fl-closeDialog').removeClass('close-confirmation').trigger("click"); //IFRAME FOR EMBED
                                                                }
                                                            }, 0);
                                                        } else {
                                                            embeded_view.refreshEmbed(embed_container, null, null, function () {
                                                                ui.unblock();
                                                                window.parent.$('#popup_container').find('.fl-closeDialog').removeClass('close-confirmation').trigger("click"); //IFRAME FOR EMBED
                                                            });
                                                        }
                                                    } else {
                                                        console.log("windows", window.parent.$('.picklistoverlay'))
                                                        window.parent.$('[name="' + field_updater + '"]').trigger("change");
                                                        setTimeout(function () {
                                                            window.parent.$('.picklistoverlay').find('.picklistclose').trigger("click"); //IFRAME FOR EMBED
                                                        }, 0);
                                                    }

                                                    if (parent) {
                                                        parent.$.event.trigger({
                                                            type: 'EVENT_DOCUMENT_CREATED_FROM_IFRAME',
                                                            documentData: parsedData
                                                        });
                                                    }

                                                } else {
                                                    ui.unblock();


                                                    var bodyData = $('body').data("user_form_json_data")["form_json"]["saved_landing_page"];
                                                    if (window.fromParent) {
                                                        window.field_updater();
                                                        window.close();
                                                    }
                                                    RedirectPage.call(parsedData, bodyData, landing_page);
                                                    // if (parsedData.number) {
                                                    //     newjAlert = new jAlert(message, 'Confirmation Dialog', '', '', '', function(r) {});
                                                    //     newjAlert.themeAlert("jAlert2");
                                                    //     $('#popup_ok').on('click', function(){
                                                    //         window.location = "/user_view" + parsedData.headerLocation;
                                                    //     });
                                                    // } else {
                                                    //     window.location = "/user_view" + parsedData.headerLocation;
                                                    // }
                                                }
                                            }


                                        }, 2000);
                                        //  redirect to home
                                    },
                                    error: function () {
                                        // this_fl_submit.removeAttr('submit-in-progress'); // FS#8252 
                                        // $('.fl-action-submit').removeAttr('submit-in-progress'); // FS#8252 
                                    }
                                });
                            });

                            // self.pageLoader(submit_ajax, function() {
                            //     //CALLBACK WHEN AJAX IS FINISHED
                            // });

                        }
                    });
                    newConfirm.themeConfirm("confirm2", {
                        'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'
                    });
                    if (require_comment_html != "") {
                        $("#popup_ok").attr("custom-fn", "1");
                    }
                } else if (invalid_datas >= 1) {
                    $('.frequired-red').filter(function () {
                        var self = $(this);
                        var self_setobj_container = self.closest(".setOBJ").parent();
                        if (self_setobj_container.is('.ui-tabs-panel[role="tabpanel"]')) {
                            return true;
                        }
                        return false;
                    }).eq(0).parents('.ui-tabs-panel[role="tabpanel"]').map(function (a, b) {
                        return $('a[href="#' + $(b).attr("id") + '"]')[0];
                    }).trigger("click")
                }

                // IAN REQUEST [08-25-2016]
                $(this).removeClass('temp-action-button-class-formula-keyword');

            }
            );
        },
        //AJAX FUNCTIONS
        "getServerTime": function () {
            var strServerObjectTime = $('#currentDateTime').attr('data-js-time-format');
            var local = new Date();
            var server = new Date();
            console.log("TAPUSIN NA NATIN TO", strServerObjectTime);
            //set up server time to js
            {
                server.setFullYear(Number(strServerObjectTime.year));
                server.setMonth(Number(strServerObjectTime.month));
                server.setDate(Number(strServerObjectTime.day));
                server.setHours(Number(strServerObjectTime.hours));
                server.setMinutes(Number(strServerObjectTime.minutes) - 1);
                server.setSeconds(Number(strServerObjectTime.seconds));
            }

            server.setMilliseconds(server.getMilliseconds() + performance.now());

            // {//add difference of server and local time
            //     if(server.getFullYear() != local.getFullYear()) server.setFullYear( server.getFullYear() + ( server.getFullYear() - local.getFullYear() ) );
            //     if( server.getMonth() != local.getMonth() ) server.setMonth( server.getMonth() + ( server.getMonth() - local.getMonth() ) );
            //     if( server.getDate() != local.getDate() ) server.setDate( server.getDate() + ( server.getDate() - local.getDate() ) );

            //     if( server.getHours() != local.getHours() ) server.setHours( server.getHours() + ( server.getHours() - local.getHours() ) );
            //     if( server.getMinutes() != local.getMinutes() ) server.setMinutes( server.getMinutes() + ( server.getMinutes() - local.getMinutes() ) );
            //     if( server.getSeconds() != local.getSeconds() ) server.setSeconds( server.getSeconds() + ( server.getSeconds() - local.getSeconds() ) );
            // }

            $("body").data("date_page_visited", server.getTime());
            $("body").data("getServerTimeData", server);
            setInterval(function () {
                $("body").data("getServerTimeData").setSeconds($("body").data("getServerTimeData").getSeconds() + 1);
                // console.log($("body").data("getServerTimeData"))
            }, 1000);


        },
        "getServerTimeOld": function () {
            var ajaxTime = (new Date()).getTime();
            // return $.ajax({
            //     "url": "/ajax/getDateToday",
            //     "async": true,
            //     "type": "POST",
            //     "cache": false,
            //     "beforeSend": function(xhr) {
            //         // xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
            //     },
            //     "data": {"access": "get_date"},
            //     "dataType": "html"
            // }).done(function(data) {
            var data = $("#currentDateTime").attr("data-js-time-format") || $("#currentDateTime").val();
            var responseTime = (new Date()).getTime() - ajaxTime;

            var serverOBJTIME = new Date(data);

            serverOBJTIME = serverOBJTIME.setTime(serverOBJTIME.getTime())

            var temp = $("body").data();

            temp.getServerTimeData = serverOBJTIME;
            $("body").data(temp);
            $("body").data("date_page_visited", temp.getServerTimeData);
            setInterval(function () {
                var temp = $("body").data();
                var localTime = new Date();
                var serverTime = new Date(temp.getServerTimeData);
                // var updatedServerTime = new Date(serverTime.setHours(serverTime.getHours() + (localTime.getHours() - serverTime.getHours()))); //ADDING HOURS
                // updatedServerTime = new Date(serverTime.setMinutes(serverTime.getMinutes() + (localTime.getMinutes() - serverTime.getMinutes()))); //ADDING MINUTES
                // updatedServerTime = new Date(serverTime.setSeconds(serverTime.getSeconds() + (localTime.getSeconds() - serverTime.getSeconds()))); //ADDING SECONDS
                var updatedServerTime = new Date(serverTime.setSeconds(serverTime.getSeconds() + 1));
                temp.getServerTimeData = updatedServerTime;
                $("body").data(temp);
            }, 1000)





            //ADD PROGRESS FOR PAGE LOADER
            var loader_bar_container = $(".page-loader-overlay").find(".loader-bar-container");
            var loading_container = loader_bar_container.outerWidth();
            var loading_filler = loader_bar_container.find(".loader-filler").outerWidth();
            loader_bar_container.find(".loader-filler").css({
                "width": (getPercent(loading_container, loading_filler) + loading_progress_added) + "%"
            });
            loading_filler = loader_bar_container.find(".loader-filler").outerWidth();
            loader_bar_container.find(".label").text("Loading " + Math.ceil(getPercent(loading_container, loading_filler)) + "%");

            {
                // loader_bar_container.find(".loader-filler").stop(false,false).animate({
                //     "width":(getPX(loading_container , getPercent(loading_container,loading_filler)+loading_progress_added) )
                // },function(){
                //  var loading_container = loader_bar_container.outerWidth();
                //  var loading_filler = loader_bar_container.find(".loader-filler").outerWidth();
                //  loader_bar_container.find(".label").text("Loading "+Math.ceil( getPercent(loading_container,loading_filler) )+"%");
                // });
            }



            // }).fail(function() {
            //     console.log("SERVER DATE : ERROR ON GETTING AJAX POST METHOD SERVER TIME.");
            // })
        },
        getKeywordFields: function () {
            var keywordFields = [];
            $('.pickListButton').each(function () {
                var inputField = $(this).attr('return-field');
                var inputFieldName = $('#' + inputField).attr('name');
                var inputCode = $('#' + inputField).attr('code');
                keywordFields.push({
                    Field: inputFieldName,
                    Code: inputCode
                });
            });
            console.log(keywordFields);
            $('#KeywordsField').val(JSON.stringify(keywordFields));
        },
        disableAllFields: function () {
            $(".setOBJ .getFields, .setOBJ .file_attachement,.setOBJ .multiple_file_attachement, .setOBJ .smart-barcode-field-input").attr("disabled", "disabled");
            $(".form_upload_photos[data-upload='photo_upload']").removeAttr("data-upload");
            //hide picklist if enabled
            $(".pickListButton").hide();
        },
        "FormulaEventDistribution": function (json_return) {
            var temporary_selector = null;
            for (key1 in json_return) {
                if (!($.type(json_return[key1]) == "object")) {
                    continue;
                }
                for (key2 in json_return[key1]) {
                    if ($.type(json_return[key1]) == "function") {
                        continue;
                    }
                    temporary_selector = $('[name="' + key2 + '"]');
                    if (temporary_selector.length <= 0) {
                        temporary_selector = $('[name="' + key2 + '[]"]');
                    }
                    // if(temporary_selector.is('select')){
                    //     json_return["change_list_computed_value"][key2] = json_return[key1][key2];
                    //     key1 = "change_list_computed_value";
                    // }
                    if (key1 == "visibility") {//this may effect the performance
                        if (key2 == "form-tab-panel") {
                            for (key3 in json_return[key1][key2]) {
                                if (json_return[key1][key2][key3]) {
                                    $('[aria-controls="' + key3 + '"]').show();
                                } else {
                                    $('[aria-controls="' + key3 + '"]').hide();
                                }
                            }
                        } else if (key2 == "embed") {
                            var parent_ele_data = {};
                            var evaluated_formula = "";
                            for (key3 in json_return[key1][key2]) {

                                parent_ele_data["dis_ele"] = $('.embed-view-container.getFields_' + key3);
                                evaluated_formula = json_return[key1][key2][key3];

                                if ($.isArray(evaluated_formula)) {
                                    //embed multi visibility affected

                                    {
                                        if (parent_ele_data["dis_ele"].is(".embed-view-container")) {
                                            if (evaluated_formula.length % 2 == 0) {
                                                for (var ctr_index in evaluated_formula) {
                                                    if (ctr_index % 2 == 0) {
                                                        var formula_parsed = evaluated_formula[ctr_index];
                                                        var target_type = evaluated_formula[Number(ctr_index) + 1];

                                                        // console.log("WHOAAHAA",evaluated_formula[ctr_index], evaluated_formula[Number(ctr_index)+1])
                                                        if (target_type == 1) {//WHOLE EMBED
                                                            if (formula_parsed == true) {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                                                dis_embed_container.css("display", "");
                                                            } else {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                                                dis_embed_container.css("display", "none");
                                                            }
                                                        } else if (target_type == 2) {//Column ACTIONS
                                                            if (formula_parsed == true) {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                var head_actions_column_ele = dis_embed_container.find("thead th:contains('Actions')").eq(0);
                                                                var column_index = head_actions_column_ele.index();
                                                                var whole_column_action = dis_embed_container.find('tbody td').filter(function (arr_i, val) {
                                                                    return $(this).index() == column_index;
                                                                });
                                                                whole_column_action.add(head_actions_column_ele).show();
                                                            } else {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                var head_actions_column_ele = dis_embed_container.find("thead th:contains('Actions')").eq(0);
                                                                var column_index = head_actions_column_ele.index();
                                                                var whole_column_action = dis_embed_container.find('tbody td').filter(function (arr_i, val) {
                                                                    return $(this).index() == column_index;
                                                                });
                                                                whole_column_action.add(head_actions_column_ele).hide();
                                                            }


                                                        } else if (target_type == 3) {//ADD LINK 
                                                            if (formula_parsed == true) {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                dis_embed_container
                                                                        .find(".embed_newRequest").not("[embed-import='true']")
                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                        // .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                        // .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                                                        // .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                                                        // .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                        // .add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                        // .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                        .css("display", "inline-block");
                                                            } else {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                dis_embed_container
                                                                        .find(".embed_newRequest").not("[embed-import='true']")
                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                        // .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                        // .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                                                        // .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                                                        // .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                        // .add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                        // .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                        .css("display", "none");
                                                            }
                                                            //ADDED BY JOSHUA CLIFFORD REYES 09/22/2015
                                                        } else if (target_type == 4) {//COPY ACTION
                                                            if (formula_parsed == true) {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                        .find("[id='copyTDEmbedded']")
                                                                        .css("display", "inline-block");
                                                            } else {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                        .find("[id='copyTDEmbedded']")
                                                                        .css("display", "none");
                                                            }
                                                        } else if (target_type == 5) {//DELETE ACTION
                                                            if (formula_parsed == true) {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.css("display", "inline-block");
                                                                        //.add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                        .find("[id='deleteTDEmbedded']")
                                                                        .css("display", "inline-block");
                                                            } else {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.css("display", "none");
                                                                        //.add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                        .find("[id='deleteTDEmbedded']")
                                                                        .css("display", "none");
                                                            }
                                                        } else if (target_type == 6) {//IN-LINE ACTION
                                                            if (formula_parsed == true) {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                        .find("[id='editDEmbedded']")
                                                                        .css("display", "inline-block");
                                                            } else {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                        .find("[id='editDEmbedded']")
                                                                        .css("display", "none");
                                                            }
                                                        } else if (target_type == 7) {//POP-UP ACTION
                                                            if (formula_parsed == true) {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                        .find('.viewTDEmbedded[data-action-attr="edit"]')
                                                                        .css("display", "inline-block");
                                                            } else {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                        .find('.viewTDEmbedded[data-action-attr="edit"]')
                                                                        .css("display", "none");
                                                            }
                                                        } else if (target_type == 8) {//VIEW ACTION
                                                            if (formula_parsed == true) {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                        .find('.viewTDEmbedded[data-action-attr="view"]')
                                                                        .css("display", "inline-block");
                                                            } else {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                        .find('.viewTDEmbedded[data-action-attr="view"]')
                                                                        .css("display", "none");
                                                            }
                                                        } else if (target_type == 9) {//IMPORT
                                                            if (formula_parsed == true) {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                        .find('.import_record')
                                                                        .css("display", "inline-block");
                                                            } else {
                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                dis_embed_container
                                                                        //.find(".embed_newRequest")
                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                        .find('.import_record')
                                                                        .css("display", "none");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (json_return[key1][key2][key3]) {
                                    $('.setObject[data-object-id="' + key3 + '"]').eq(0).show();
                                } else {
                                    $('.setObject[data-object-id="' + key3 + '"]').eq(0).hide();
                                }
                            }
                        } else {
                            if (json_return[key1][key2]) { // true
                                // temporary_selector.is('[type="radio"]');
                                temporary_selector.closest(".setOBJ").show();
                            } else { //false
                                // temporary_selector.is('[type="radio"]');
                                temporary_selector.closest(".setOBJ").hide();
                            }
                        }
                    } else if (key1 == "computed_value") {
                        if (temporary_selector.is('select[multiple]')) {
                            var temp_val = json_return[key1][key2];
                            if ($.trim(temp_val) != "") {
                                temp_val = temp_val.split("|^|");
                                for (var ctr = 0; ctr < temp_val.length; ctr++) {
                                    temporary_selector.children('option[value="' + temp_val[ctr] + '"]').prop("checked", true);
                                }
                            }
                        } else if (temporary_selector.is('input[type="checkbox"]')) {
                            var temp_val = json_return[key1][key2];
                            if ($.trim(temp_val) != "") {
                                temp_val = temp_val.split("|^|");
                                for (var ctr = 0; ctr < temp_val.length; ctr++) {
                                    temporary_selector.filter('[value="' + temp_val[ctr] + '"]').prop("checked", true);
                                }
                            }
                        } else if (temporary_selector.is('input[type="radio"]')) {
                            temporary_selector.filter('[value="' + json_return[key1][key2] + '"]').prop("checked", true);
                        } else if (temporary_selector.is('.hasDatepicker')) {
                            var temp_val = json_return[key1][key2];
                            if (temp_val) {
                                var converted_js_date = new Date(Number(temp_val));
                                if (converted_js_date != "Invalid Date") {
                                    if (converted_js_date.getFullYear() == "1970") {
                                        converted_js_date = new Date(Number(temp_val) * 1000);
                                    }
                                }
                            }
                            if (temporary_selector.is('[data-type="datepicker"]')) {
                                temporary_selector.val(ThisDate(converted_js_date).formatDate("Y-m-d"));
                            } else if (temporary_selector.is('[data-type="dateTime"]')) {
                                temporary_selector.val(ThisDate(converted_js_date).formatDate("Y-m-d H:i"));
                            } else if (temporary_selector.is('[data-type="time"]')) {
                                temporary_selector.val(ThisDate(converted_js_date).formatDate("H:i"));
                            }
                        } else if (temporary_selector.is('[data-input-type="Currency"]')) {
                            var temp_val = Number(json_return[key1][key2])
                            if (!isNaN(temp_val)) {
                                temp_val = temp_val.currencyFormat();
                            }
                            temporary_selector.val(temp_val);
                        } else {
                            temporary_selector.val(json_return[key1][key2]);
                        }
                    } else if (key1 == "change_list_computed_value") {
                        if (temporary_selector.is("select")) {
                            console.log("KABOOM", json_return[key1][key2], $.type(json_return[key1][key2]))
                            if ($.type(json_return[key1][key2]) == "array") {
                                var select_array_value_opt = (json_return[key1][key2] || []).filter(Boolean);
                                var temp_value = "";
                                temp_value = temporary_selector.val();
                                temporary_selector.html("");
                                for (var ctr = 0; ctr < select_array_value_opt.length; ctr++) {
                                    temporary_selector.append('<option value="' + select_array_value_opt[ctr] + '">' + select_array_value_opt[ctr] + '</option>');
                                }
                                // temporary_selector.children('option[value="'+temp_value+'"]').prop("selected",true);
                            }
                        }
                    } else if (key1 == "readonly") {
                        if (json_return[key1][key2]) {//true
                            if (temporary_selector.is('[type="radio"]')) {
                                temporary_selector.attr("readonly", true)
                                temporary_selector.attr("readonly-disabled", "");
                                temporary_selector.attr("disable", true);
                            } else {
                                temporary_selector.attr("attr", true);
                            }
                        } else {//false
                            if (temporary_selector.is('[type="radio"]')) {
                                if (temporary_selector.is('[readonly-disabled]')) {
                                    temporary_selector.attr("readonly", false)
                                    temporary_selector.removeAttr("readonly-disabled");
                                    temporary_selector.removeAttr("disable");
                                }
                            } else {
                                temporary_selector.attr("attr", true);
                            }
                        }
                    } else if (key1 == "validation") {

                    }
                    if (temporary_selector.is('select')) {
                        key1 = "computed_value";
                    }
                    temporary_selector.trigger('change.exeVisibilityOnChange');
                    temporary_selector.trigger('change.embedSearchReference');
                }
            }
        },
        "FormulaEventDistribution2": function (json_return) {
            console.log("json_return", json_return);
            var collectorElementOnchange = $();
            for (index in json_return['sequence_data']) {
                var field = $('[name="' + json_return['sequence_data'][index]['field_name'] + '"]');
                var container = $('');
                if (field.length <= 0) {
                    field = $('[name="' + json_return['sequence_data'][index]['field_name'] + '[]"]');
                }
                if (field.length <= 0) {
                    container = $('.setOBJ[data-object-id="' + json_return['sequence_data'][index]['data_object_id'] + '"]');
                }
                var distribution_type = json_return['sequence_data'][index]['distribution_type'];
                console.log("sequence:");
                switch (distribution_type) {
                    case "visibility":
                        // for reference only
                        // if(key2 == "form-tab-panel"){
                        //     for(key3 in json_return[key1][key2]){
                        //         if(json_return[key1][key2][key3]){
                        //             $('[aria-controls="'+key3+'"]').show();
                        //         }else{
                        //             $('[aria-controls="'+key3+'"]').hide();
                        //         }
                        //     }
                        // }else if(key2 == "embed"){
                        //     var parent_ele_data = {};
                        //     var evaluated_formula = "";
                        //     for(key3 in json_return[key1][key2]){

                        //         parent_ele_data["dis_ele"] = $('.embed-view-container.getFields_'+key3);
                        //         evaluated_formula = json_return[key1][key2][key3];

                        //         if ($.isArray(evaluated_formula)) {
                        //             //embed multi visibility affected

                        //             {
                        //                 if (parent_ele_data["dis_ele"].is(".embed-view-container")) {
                        //                     if (evaluated_formula.length % 2 == 0) {
                        //                         for (var ctr_index in evaluated_formula) {
                        //                             if (ctr_index % 2 == 0) {
                        //                                 var formula_parsed = evaluated_formula[ctr_index];
                        //                                 var target_type = evaluated_formula[Number(ctr_index) + 1];

                        //                                 // console.log("WHOAAHAA",evaluated_formula[ctr_index], evaluated_formula[Number(ctr_index)+1])
                        //                                 if (target_type == 1) {//WHOLE EMBED
                        //                                     if (formula_parsed == true) {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parents('.setOBJ[data-type="embeded-view"]').eq(0);
                        //                                         dis_embed_container.css("display", "");
                        //                                     } else {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parents('.setOBJ[data-type="embeded-view"]').eq(0);
                        //                                         dis_embed_container.css("display", "none");
                        //                                     }
                        //                                 } else if (target_type == 2) {//Column ACTIONS
                        //                                     if (formula_parsed == true) {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent();
                        //                                         var head_actions_column_ele = dis_embed_container.find("thead th:contains('Actions')").eq(0);
                        //                                         var column_index = head_actions_column_ele.index();
                        //                                         var whole_column_action = dis_embed_container.find('tbody td').filter(function (arr_i, val) {
                        //                                             return $(this).index() == column_index;
                        //                                         });
                        //                                         whole_column_action.add(head_actions_column_ele).show();
                        //                                     } else {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent();
                        //                                         var head_actions_column_ele = dis_embed_container.find("thead th:contains('Actions')").eq(0);
                        //                                         var column_index = head_actions_column_ele.index();
                        //                                         var whole_column_action = dis_embed_container.find('tbody td').filter(function (arr_i, val) {
                        //                                             return $(this).index() == column_index;
                        //                                         });
                        //                                         whole_column_action.add(head_actions_column_ele).hide();
                        //                                     }


                        //                                 } else if (target_type == 3) {//ADD LINK 
                        //                                     if (formula_parsed == true) {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent();
                        //                                         dis_embed_container
                        //                                                 .find(".embed_newRequest")
                        //                                                 //.add(dis_embed_container.find(".viewTDEmbedded"))
                        //                                                 // .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find("[id='editDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                        //                                                 .css("display", "inline-block");
                        //                                     } else {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent()
                        //                                         dis_embed_container
                        //                                                 .find(".embed_newRequest")
                        //                                                 //.add(dis_embed_container.find(".viewTDEmbedded"))
                        //                                                 // .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find("[id='editDEmbedded']"))
                        //                                                 // .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                        //                                                 .css("display", "none");
                        //                                     }
                        //                                 //ADDED BY JOSHUA CLIFFORD REYES 09/22/2015
                        //                                 } else if (target_type == 4) {//COPY ACTION
                        //                                     if (formula_parsed == true) {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent();
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.add(dis_embed_container.find("[id='copyTDEmbedded']"))
                        //                                                  .find("[id='copyTDEmbedded']")
                        //                                                  .css("display", "inline-block");
                        //                                     } else {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent()
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.add(dis_embed_container.find("[id='copyTDEmbedded']"))
                        //                                                  .find("[id='copyTDEmbedded']")
                        //                                                  .css("display", "none");
                        //                                     }
                        //                                 } else if (target_type == 5) {//DELETE ACTION
                        //                                      if (formula_parsed == true) {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent();
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.css("display", "inline-block");
                        //                                                  //.add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                        //                                                  .find("[id='deleteTDEmbedded']")
                        //                                                  .css("display", "inline-block");
                        //                                     } else {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent()
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.css("display", "none");
                        //                                                  //.add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                        //                                                  .find("[id='deleteTDEmbedded']")
                        //                                                  .css("display", "none");
                        //                                     }
                        //                                 } else if (target_type == 6) {//IN-LINE ACTION
                        //                                      if (formula_parsed == true) {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent();
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.add(dis_embed_container.find("[id='editDEmbedded']"))
                        //                                                  .find("[id='editDEmbedded']")
                        //                                                  .css("display", "inline-block");
                        //                                     } else {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent()
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.add(dis_embed_container.find("[id='editDEmbedded']"))
                        //                                                  .find("[id='editDEmbedded']")
                        //                                                  .css("display", "none");
                        //                                     }
                        //                                 } else if (target_type == 7) {//POP-UP ACTION
                        //                                      if (formula_parsed == true) {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent();
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                        //                                                  .find('.viewTDEmbedded[data-action-attr="edit"]')
                        //                                                  .css("display", "inline-block");
                        //                                     } else {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent()
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                        //                                                  .find('.viewTDEmbedded[data-action-attr="edit"]')
                        //                                                  .css("display", "none");
                        //                                     }
                        //                                 } else if (target_type == 8) {//VIEW ACTION
                        //                                      if (formula_parsed == true) {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent();
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.add(dis_embed_container.find(".viewTDEmbedded"))
                        //                                                  .find('.viewTDEmbedded[data-action-attr="view"]')
                        //                                                  .css("display", "inline-block");
                        //                                     } else {
                        //                                         var dis_embed_container = parent_ele_data["dis_ele"].parent()
                        //                                             dis_embed_container
                        //                                                  //.find(".embed_newRequest")
                        //                                                  //.add(dis_embed_container.find(".viewTDEmbedded"))
                        //                                                  .find('.viewTDEmbedded[data-action-attr="view"]')
                        //                                                  .css("display", "none");
                        //                                     }
                        //                                 }
                        //                             }
                        //                         }
                        //                     }
                        //                 }
                        //             }
                        //         }
                        //         if(json_return[key1][key2][key3]){
                        //             $('.setObject[data-object-id="'+key3+'"]').eq(0).show();
                        //         }else{
                        //             $('.setObject[data-object-id="'+key3+'"]').eq(0).hide();
                        //         }
                        //     }
                        // }else{
                        //     if(json_return[key1][key2]){ // true
                        //         // temporary_selector.is('[type="radio"]');
                        //         temporary_selector.closest(".setOBJ").show();
                        //     }else{ //false
                        //         // temporary_selector.is('[type="radio"]');
                        //         temporary_selector.closest(".setOBJ").hide();
                        //     }
                        // }
                        break;
                    case "computed_value":
                        if (field.is('select[multiple]')) {
                            var temp_val = json_return['sequence_data'][index]['value'];
                            field.children(':selected').prop('selected', false);
                            if ($.trim(temp_val) != "") {
                                temp_val = temp_val.split("|^|");
                                for (var ctr = 0; ctr < temp_val.length; ctr++) {
                                    field.children('option[value="' + temp_val[ctr] + '"]').prop("selected", true);
                                }
                            }
                        } else if (field.is('input[type="checkbox"]')) {
                            var temp_val = json_return['sequence_data'][index]['value'];
                            field.filter(':checked').prop('checked', false);
                            if ($.trim(temp_val) != "") {
                                temp_val = temp_val.split("|^|");
                                for (var ctr = 0; ctr < temp_val.length; ctr++) {
                                    field.filter('[value="' + temp_val[ctr] + '"]').prop("checked", true);
                                }
                            }
                        } else if (field.is('input[type="radio"]')) {
                            field.filter('[value="' + json_return['sequence_data'][index]['value'] + '"]').prop("checked", true);
                        } else if (field.is('[data-type="date"],[data-type="time"],[data-type="dateTime"]')) {
                            var converted_js_date = json_return['sequence_data'][index]['value'];
                            var getting_length = converted_js_date + ""; //FS#8286
                            if ($.isNumeric(converted_js_date) && converted_js_date !== 0 && getting_length.length >= 10) {
                                var temp_val = Number(json_return['sequence_data'][index]['value']);
                                if (temp_val) {
                                    var converted_js_date = new Date(Number(temp_val));
                                    if (converted_js_date != "Invalid Date") {
                                        if (converted_js_date.getFullYear() == "1970") {
                                            converted_js_date = new Date(Number(temp_val) * 1000);
                                            if (converted_js_date.getFullYear() == "1970") { //FS#8129
                                                converted_js_date = "Invalid Date";
                                            }
                                        }
                                    }
                                }
                            } else { //FS#8129
                                converted_js_date = new Date(converted_js_date);
                                if (isNaN(converted_js_date)) {
                                    converted_js_date = new Date(ThisDate(new Date()).formatDate('Y-m-d') + " " + json_return['sequence_data'][index]['value']); //FS#8286
                                }
                                if (converted_js_date.getFullYear() == "1970") {
                                    converted_js_date = "Invalid Date";
                                }
                            }
                            if (!json_return['sequence_data'][index]['value'] || getting_length.length <= 1 || json_return['sequence_data'][index]['value'] == "0000-00-00 00:00:00") {
                                field.val("");
                            } else if (field.is('[data-type="date"]') && getting_length.length > 1) {
                                field.val(ThisDate(converted_js_date).formatDate("Y-m-d"));
                            } else if (field.is('[data-type="dateTime"]') && getting_length.length > 1) {
                                field.val(ThisDate(converted_js_date).formatDate("Y-m-d H:i"));
                            } else if (field.is('[data-type="time"]') && getting_length.length > 1) {
                                field.val(ThisDate(converted_js_date).formatDate("H:i"));
                            }
                        } else if (field.is('[data-input-type="Currency"]')) {
                            var temp_val = Number(json_return['sequence_data'][index]['value'])
                            if (!isNaN(temp_val)) {
                                temp_val = temp_val.currencyFormat();
                            }
                            field.val(temp_val);
                        } else {
                            // alert(json_return[key1][key2]);
                            field.val(json_return['sequence_data'][index]['value']);
                        }
                        break;
                    case "change_list_computed_value":
                        console.log("change_list_computed_value");
                        if (field.is('select')) {
                            var values = json_return['sequence_data'][index]['value']
                            // if($.type(json_return[key1][key2]) == "array" ){
                            //     var select_array_value_opt = (json_return[key1][key2]||[]).filter(Boolean);
                            //     var temp_value = "";
                            //     temp_value = temporary_selector.val();
                            //     temporary_selector.html("");
                            //     for(var ctr = 0 ; ctr < select_array_value_opt.length ; ctr++){
                            //         temporary_selector.append('<option value="' + select_array_value_opt[ctr] + '">' + select_array_value_opt[ctr] + '</option>');
                            //     }
                            //     // temporary_selector.children('option[value="'+temp_value+'"]').prop("selected",true);
                            // }
                            if ($.type(values) == "array") {
                                var new_select_values = (values || []).filter(Boolean);
                                field.html("");
                                for (i in new_select_values) {
                                    field.append('<option value="' + new_select_values[i] + '">' + new_select_values[i] + '</option>');
                                }
                            }
                        }
                        break;
                    case "placeholder_formula":
                        //console.log(field);
                        if (field.is('[data-input-type="Currency"]')) {
                            var temp_val = Number(json_return['sequence_data'][index]['value']);
                            if (!isNaN(temp_val)) {
                                temp_val = temp_val.currencyFormat();
                            }
                            field.attr('placeholder', temp_val);
                        } else if (field.is('.smart-barcode-field-input-str-collection')) {
                            field.attr('placeholder', json_return['sequence_data'][index]['value']);
                            field.parent().next().find('.smart-barcode-field-input').attr('placeholder', json_return['sequence_data'][index]['value']);
                            //var place = $('.smart-barcode-field-input-str-collection').attr('placeholder');
                            //$('.smart-barcode-field-input').attr('placeholder', place);

                        } else {
                            //$.find('.smart-barcode-field-input').attr('placeholder', json_return['sequence_data'][index]['value']);
                            field.attr('placeholder', json_return['sequence_data'][index]['value']);

                        }
                        break;
                    case "label_formula":
                        //console.log(field);
                        if (field.is('label')) {
                            //alert('1');
                            field.text(json_return['sequence_data'][index]['value']);
                        } else if (container.length >= 1) {
                            container.find('[id="lbl_' + json_return['sequence_data'][index]['data_object_id'] + '"]:eq(0)').text(json_return['sequence_data'][index]['value']);
                        } else if (field.parent().is('.obj_f-aligned-left-wrap')) {
                            field.parents(".setOBJ[data-object-id='" + json_return['sequence_data'][index]['data_object_id'] + "']:eq(0)")
                                    .find('[id="lbl_' + json_return['sequence_data'][index]['data_object_id'] + '"]:eq(0)')
                                    .text(json_return['sequence_data'][index]['value']);
                        } else {
                            field.parent().prev().children('label').text(json_return['sequence_data'][index]['value']);
                        }
                        break;
                }

                // field
                //         .trigger('change.exeVisibilityOnChange')
                //         .trigger('change.embedSearchReference')
                //         .trigger('change.readOnlyControl')
                //         .trigger('change.ToggleClearButton'); //FS#9012
                collectorElementOnchange = collectorElementOnchange.add(field);
            }
            collectorElementOnchange
                        .trigger('change.exeVisibilityOnChange')
                        .trigger('change.embedSearchReference')
                        .trigger('change.readOnlyControl')
                        .trigger('change.ToggleClearButton'); //FS#9012
        },
        "FormulaEventController": function (event_type) {
            if ($('#app_config_formbuilder_form_events').val() == "0") {
                return "this was disabled on configuration settings";
            }
            var self = this;
            var form_json_variable = $("body").data("user_form_json_data");
            if (event_type == "onload") {

                if (form_json_variable["form_json"]) {
                    if (form_json_variable["form_json"]["form_events"]) {
                        if (form_json_variable["form_json"]["form_events"]["compute_onload"]) {
                            if ($.type(formula_event_ajax_xhr["onload"]) == "object") {
                                if ($.type(formula_event_ajax_xhr["onload"].abort) == "function") {
                                    formula_event_ajax_xhr["onload"].abort();
                                }
                            }

                            formula_event_ajax_xhr["onload"] = self.FormulaEventAjax(form_json_variable["form_json"]["form_events"]["compute_onload"], event_type);
                        }
                        if (form_json_variable["form_json"]["form_events"]["compute_presave"]) {
                            $('[name="forma_presave_formula"]').val(form_json_variable["form_json"]["form_events"]["compute_presave"]);
                        }
                        if (form_json_variable["form_json"]["form_events"]["compute_postsave"]) {
                            $('[name="forma_postsave_formula"]').val(form_json_variable["form_json"]["form_events"]["compute_postsave"]);
                        }
                    }
                }
            } else if (event_type == "presave") {
                if (form_json_variable["form_json"]) {
                    if (form_json_variable["form_json"]["form_events"]) {
                        if (form_json_variable["form_json"]["form_events"]["compute_presave"]) {
                            if ($.type(formula_event_ajax_xhr["presave"]) == "object") {
                                if ($.type(formula_event_ajax_xhr["presave"].abort) == "function") {
                                    formula_event_ajax_xhr["presave"].abort();
                                }
                            }
                            formula_event_ajax_xhr["presave"] = self.FormulaEventAjax(form_json_variable["form_json"]["form_events"]["compute_presave"], event_type);
                        }
                    }
                }
            } else if (event_type == "postsave") {
                if (form_json_variable["form_json"]) {
                    if (form_json_variable["form_json"]["form_events"]) {
                        if (form_json_variable["form_json"]["form_events"]["compute_postsave"]) {
                            if ($.type(formula_event_ajax_xhr["postsave"]) == "object") {
                                if ($.type(formula_event_ajax_xhr["postsave"].abort) == "function") {
                                    formula_event_ajax_xhr["postsave"].abort();
                                }
                            }
                            formula_event_ajax_xhr["postsave"] = self.FormulaEventAjax(form_json_variable["form_json"]["form_events"]["compute_postsave"], event_type);
                        }
                    }
                }
            } else if (event_type == "onchange") {
                var event_trigger_ex = ["SetDefault", "commitChangesDefaultVal", "triggerReadOnlyFormula"]; // remove , "embededTrigger" solution for FS#8012
                $('.setOBJ').each(function () {
                    var here = $(this);
                    var doi = here.attr("data-object-id");
                    //getting formula by body data json
                    if (form_json_variable["form_json"]) {
                        if (form_json_variable["form_json"][doi + ""]) {
                            if (form_json_variable["form_json"][doi + ""]["onchange-formula-event"]) {
                                console.log("RENDER", $('.getFields_' + doi))
                                var temp_name_event = "change.Alias";
                                var temp_get_fields = $('.getFields_' + doi);
                                if (temp_get_fields.is('.embed-view-container[embed-name]')) {
                                    temp_name_event = "embed.FormulaOnchange";
                                }
                                temp_get_fields.on(temp_name_event, function (e, trigger_inject_data) {
                                    if (event_trigger_ex.indexOf(trigger_inject_data) >= 0) {
                                        return true;
                                    }
                                    var in_self = $(this);
                                    var field_name = (in_self.attr('name') || "").replace("[]", "");
                                    var formula_onchange_event = in_self.attr("onchange-formula-event") || form_json_variable["form_json"][doi + ""]["onchange-formula-event"];
                                    if ($.type(formula_event_ajax_xhr["onchange"]) == "object") {
                                        if ($.type(formula_event_ajax_xhr["onchange"].abort) == "function") {
                                            formula_event_ajax_xhr["onchange"].abort();
                                        }
                                    }
                                    formula_event_ajax_xhr["onchange"] = self.FormulaEventAjax(formula_onchange_event, event_type, field_name);
                                });

                            }
                        }
                    }
                });
            }
        },
        "FormulaEventAjax": function (formula, event_type, formula_list_onchange_auto_excluded_fieldname) {

            ui.block();
            var parent_self = this;
            var set_formula = formula;
            var field_associated_data = {};
            // console.log("FormulaEventAjax", $.extend({},{}));
            var current_collected_field_model = {};
            var filters_selectors = [
                '[data-type="createLine"]',
                '[data-type="labelOnly"]',
                '[data-type="accordion"]',
                '[data-type="table"]',
                '[data-type="embeded-view"]',
                '[data-type="imageOnly"]',
                '[data-type="tab-panel"]'
            ];
            // console.log("NUMBERSWAN: ",$('.setOBJ').not(filters_selectors.join(",")) ) 
            var user_fields = {};
            $('.setOBJ').not(filters_selectors.join(",")).map(function () {
                //FormulaEventAjax michael dito ka na
                var self = $(this);
                var doi = self.attr("data-object-id");
                var get_fields_ele = $('.getFields_' + doi);
                var field_name = ($('.getFields_' + doi).attr("name") || "").replace('[]', '');
                field_associated_data[field_name + ""] = {};
                field_associated_data[field_name + ""]["field_object_type"] = self.attr("data-type");
                field_associated_data[field_name + ""]["field_input_type"] = get_fields_ele.attr("data-input-type") || "Text";
                if (get_fields_ele.is('[type="radio"]')) {
                    user_fields[field_name + ""] = get_fields_ele.filter(":checked").val();
                    current_collected_field_model[field_name + ""] = get_fields_ele.filter(":checked").val();
                } else if (get_fields_ele.is('[type="checkbox"]')) {
                    user_fields[field_name + ""] = get_fields_ele.filter(":checked").map(function () {
                        return $(this).val();
                    }).get().filter(Boolean).join("|^|");
                    current_collected_field_model[field_name + ""] = get_fields_ele.filter(":checked").map(function () {
                        return $(this).val();
                    }).get().filter(Boolean).join("|^|");
                } else if (get_fields_ele.is('select[multiple]')) {
                    user_fields[field_name + ""] = get_fields_ele.children("option:selected").map(function () {
                        return $(this).val();
                    }).get().filter(Boolean).join("|^|");
                    current_collected_field_model[field_name + ""] = get_fields_ele.children("option:selected").map(function () {
                        return $(this).val();
                    }).get().filter(Boolean).join("|^|");
                } else {
                    if (get_fields_ele.is('[data-input-type="Currency"]') || get_fields_ele.is('[data-input-type="Number"]')) {
                        user_fields[field_name + ""] = Number(get_fields_ele.val().replace(/,/g, ""));
                        current_collected_field_model[field_name + ""] = Number(get_fields_ele.val().replace(/,/g, ""));
                    } else {
                        user_fields[field_name + ""] = get_fields_ele.val();
                        current_collected_field_model[field_name + ""] = get_fields_ele.val();
                    }
                }
            });

            //FROM jewel data source
            var bodyData = $('body').data();
            var userFormData = bodyData['user_form_json_data'];
            var formJson = userFormData['form_json'];
            var formDataSource = formJson['form_variables'];
            //end FROM jewel data source

            {//Keywords
                var reserve_key = {};
                $('[id="frmrequest"]').children("div.display").eq(0).children("input").each(function () {
                    var self = $(this);
                    var field_name = self.attr("name");
                    var field_value = "";
                    if (field_name == "CurrentUser") {
                        field_value = self.attr("current-user-name");
                    } else if (field_name == "Processor") {
                        field_value = ($("[name='ID']").val() == "0") ? $('[name="Processor"]').attr("processor-name") : $("#processor_display").text();
                    } else {
                        field_value = self.val()
                    }
                    reserve_key[field_name + ""] = field_value;
                    current_collected_field_model[field_name + ""] = field_value;
                });
                delete reserve_key["FormJson"];
                delete current_collected_field_model["FormJson"];

                current_collected_field_model["TimeStamp"] = $('body').data("date_page_visited");
            }


            //console.log("current_collected_field_model HAHA: ",current_collected_field_model);

            var temp_computed_formula_event_list = "";
            if ($('body').data("user_form_json_data")) {
                if ($('body').data("user_form_json_data")["form_json"]) {
                    if ($('body').data("user_form_json_data")["form_json"]["ComputedFormulaEventList"]) {
                        temp_computed_formula_event_list = JSON.stringify($('body').data("user_form_json_data")["form_json"]["ComputedFormulaEventList"]);
                    }
                }

            }
            var default_ajax_setup = {
                type: 'POST',
                url: "/ajax/FormulaEventAjax",
                data: {
                    "field_associated_data": field_associated_data,
                    "formula_list_onchange_auto_excluded_fieldname": formula_list_onchange_auto_excluded_fieldname,
                    "form_variable_data_source": formDataSource,
                    "form_reserve_keys": reserve_key,
                    "form_user_fields": user_fields,
                    "computed_formula_event_list": temp_computed_formula_event_list,
                    "formula": set_formula,
                    "field_model": JSON.stringify(current_collected_field_model)
                },
                success: function (data_res) {
                    console.log("FORMULA EVENT", data_res);
                    try {
                        var user_implement_given_format = JSON.parse(data_res);
                        console.log("user_implement_given_format", user_implement_given_format);
                        parent_self.FormulaEventDistribution2(user_implement_given_format);
                        // parent_self.FormulaEventDistribution(user_implement_given_format);
                    } catch (e) {
                        console.log("error");
                    }
                    if(event_type != 'presave'){
                        ui.unblock();
                    }
                },
                dataType: "text",
                async: false //so this will be synchronous meaning this will execute and be waited
            };
            if (event_type == "onchange") {
                default_ajax_setup["async"] = false; // affected for FS#8012, because embed total has a multiple fields return
            }
            return $.ajax(default_ajax_setup);



            // $.post(,,function(data_res){
            //     console.log("FORMULA EVENT", data_res);
            //     try{
            //         var user_implement_given_format = JSON.parse(data_res);
            //         console.log("user_implement_given_format",user_implement_given_format);
            //         parent_self.FormulaEventDistribution(user_implement_given_format);
            //     }catch(e){
            //         console.log("error")
            //     }
            // });
        },
        getRequestDetails: function () {
            var formId = $('#FormID').val();
            var self = this;
            var requestId = $("#ID").val();
            //add by japhet morada
            var currentUser = $("#CurrentUser").val();
            //=======================================
            var tmpFieldEnabled = $("#fieldEnabled").val();
            //var tmpFieldHidden = $("#ID").val();
            //var tmpFieldRequired = $("#ID").val();
            var location = window.location.pathname;
            if (requestId != '0' && requestId != '') {
                if (location == '/workspace') {
                    if ($('body').children(".submit-overlay").length == 0) {
                        var submit_overlay = $('<div class="submit-overlay" style="background:url(/images/loader/loader.gif);z-index:1002;background-repeat:no-repeat;background-attachment:fixed;background-color:rgba(200,200,200,0.5);background-position:center;position:fixed;width:100%;height:100%;text-align:center;vertical-align:middle;">Loading...</div>');
                        // $('body').prepend(submit_overlay);//this will use if async is true
                    }
                }
                var pathname = window.location;
                if (getParametersName("view_type", pathname) == "update") {
                    var loc = "/ajax/printdetails";
                    var resultJson = $('body').data('request');
                    var objects = Object.keys(resultJson[0]);
                    var i = 0;
                    //added by japhet morada
                    for (i in resultJson[0]) {
                        if (typeof resultJson[0][i] !== 'undefined') {
                            console.log(resultJson[0][i], i);
                            if (resultJson[0][i] == null) {
                                resultJson[0][i] = "";
                            }
                            else if (resultJson[0][i] === "") {
                                resultJson[0][i] = "";
                            }
                            self.distributeValues(resultJson[0], 0, i, formId, self, requestId, currentUser, tmpFieldEnabled);
                        }
                        else {
                            self.distributeValues(resultJson[0], 0, i, formId, self, requestId, currentUser, tmpFieldEnabled);
                        }
                        i++;
                    }
                    //===========================================================
                    // return $.ajax({
                    //     type: 'POST',
                    //     url: loc,
                    //     data: {
                    //         FormID: formId,
                    //         RequestID: requestId
                    //     },
                    //     success: function (result) {
                    //         console.log("TROWs",result)
                    //         try {
                    //             //z var resultJson = JSON.parse(result);
                    //             var resultJson = result;
                    //             self.setFieldValues(resultJson);
                    //             //embeded_view.init();

                    //         } catch (error) {
                    //             console.log("getRequestDetails ERROR AJAX", error)
                    //         }

                    //         if ($("#fl-action-option").children("li").length == 0) {
                    //             $("#fl-action-btn").css("display", "none");
                    //         }
                    //     },
                    //     dataType: "json",
                    //     async: false
                    // }).fail(function () {
                    //     console.log("FAIL REQUEST DETAILS");
                    // });
                } else {
                    //added by japhet morada
                    var resultJson = $('body').data('request');
                    console.log(resultJson, "SAMUEL");
                    if (resultJson != null) {
                        if ($.type(resultJson) == "array") {
                            if (resultJson.length >= 1) {
                                var objects = Object.keys(resultJson[0]);
                                var i = 0;
                                for (i in resultJson[0]) {
                                    if (typeof resultJson[0][i] !== 'undefined') {
                                        if (resultJson[0][i] == null) {
                                            resultJson[0][i] = "";
                                        }
                                        else if (resultJson[0][i] === "") {
                                            resultJson[0][i] = "";
                                        }
                                        self.distributeValues(resultJson[0], 0, i, formId, self, requestId, currentUser, tmpFieldEnabled);
                                    }
                                    else {
                                        self.distributeValues(resultJson[0], 0, i, formId, self, requestId, currentUser, tmpFieldEnabled);
                                    }
                                    i++;
                                }
                            }
                        }
                    }
                    //=============================================
                    // var resultJson = $('body').data('request');
                    self.setFieldValues(resultJson);
                }
            } else {

                // Commented by Roni pinili
                // reason -> dulplicating close button when you request a new form see workspace.php
                //$('#fl-action-option').append('<li class="fl-action-close" value="Close" data-workflow-id="0" action="0" message="" message_success = ""><a href="#" data-original-title="" title="">Close</a></li>');
                //alert("requestId !=");

                self.fieldEnabled();

                self.fieldRequired();
                self.fieldHiddenValues();
                computed.init();

                // $(".viewer_action").append('<button class="btn-basicBtn padding_5 cursor tip addViewer" data-original-title="Add Viewer"><i class="icon-user"></i></button>')
                // $(".tip").tooltip();
                //   self.getButtons();

                if ($("#fl-action-option").children("li").length == 0) {
                    $("#fl-action-btn").css("display", "none");
                }


                return "";
            }
        },
        distributeValues: function (json, index, key, form_id, SELF, request_id, current_user, field_enabled) {
            var formId = form_id;
            var self = SELF;
            var requestId = request_id;
            var currentUser = current_user;
            var tmpFieldEnabled = field_enabled;
            //var tmpFieldHidden = $("#ID").val();
            //var tmpFieldRequired = $("#ID").val();
            var location = window.location.pathname;
            // console.log("key:", key, "value:", json[key]);
            // setTimeout(function(){
            // console_time.start(key);
            var field_2 = $('[name="' + key + '[]"]');
            var field_1 = $('[name="' + key + '"');
            if (field_2.attr("type") == "checkbox") {
                field_2.each(function (index) {
                    var self = $(this);
                    if (json[key]) {
                        var checkbox_value = json[key].split(",");
                        var checkbox_value_new = json[key].split("|^|");
                        if (checkbox_value.indexOf(self.val()) >= 0 || checkbox_value_new.indexOf(self.val()) >= 0) {
                            //added by japhet morada
                            //FS#8036 - fix the popup edit for parent submission of embedded view
                            if (!$('#frmrequest').attr('embedded-view-object')) {
                                self.attr("checked", "checked")
                            }
                        }
                    }
                });
            } else if (field_2.parents('.setOBJ:eq(0)').is('[data-type="text-tagging"]') ) {
                field_2.data("value", (json[key]||'').split('|^|') );
            } else if (field_1.is(".smart-barcode-field-input-str-collection")) {
                //smart barcode 
                var valuestr = json[key];
                field_1.val(valuestr)
                // document.getElementsByName(key)[0].value = valuestr;
            }else if(field_1.is('select')){ // restoring value of the select element object
                var append_new_opt_str = "";
                var select_value = $.trim(json[key]);
                if(field_1.children('[value="' + select_value + '"]').length <= 0){
                    append_new_opt_str += '<option value="' + select_value + '" selected="selected">' + select_value + '</option>';
                    field_1.append(append_new_opt_str);
                }else{
                    field_1.children('[value="' + select_value + '"]').prop('selected',true);
                }
            } else if (field_2.is("select[multiple]")) {
                //added by japhet morada
                //FS#8036 - fix the popup edit for parent submission of embedded view
                if ($('#frmrequest').attr('embedded-view-object')) {
                    return;
                }
                var select_value = $.trim(json[key]);
                if (select_value) {
                    var select_value_arr = select_value.split("|^|");
                    if (field_2.is('[default-type="computed"]')) {
                        if (field_2.attr('default-formula-value') != '') {

                            var append_new_opt_str = "";
                            var append_new_opt = null;
                            for (var ii in select_value_arr) {
                                append_new_opt_str += '<option value="' + select_value_arr[ii] + '" selected="selected">' + select_value_arr[ii] + '</option>';
                            }
                            append_new_opt = $(append_new_opt_str);
                            field_2.append(append_new_opt);
                            append_new_opt.prop("selected", true);

                        } else {
                            field_2.children("option").filter(function () {
                                return select_value_arr.indexOf($(this).attr("value")) >= 0;
                            }).prop("selected", true);
                        }
                    } else {

                        field_2.children("option").filter(function () {
                            return select_value_arr.indexOf($(this).attr("value")) >= 0;
                        }).prop("selected", true);
                    }
                }
            } else if (field_1.attr("type") == "radio") {
                // $('[name="' + key +'"]').each(function(){
                // });
                //added by japhet morada 123123
                //FS#8036 - fix the popup edit for parent submission of embedded view
                if (!$('#frmrequest').attr('embedded-view-object')) {
                    $('[name="' + key + '"][value="' + json[key] + '"]').prop("checked", true);
                }
            } else {
                if (key == "fieldEnabled" || key == "fieldRequired" || key == "fieldHiddenValues") {
                    $("." + key).text(json[key]);
                    if (field_1.prop("tagName") == "TEXTAREA") {
                        field_1.text(json[key]);
                        field_1.val(json[key]);
                    } else {
                        field_1.attr("value", json[key]);
                        field_1.val(json[key]);
                    }
                } else if (key == 'TrackNo') {
                    field_1.val(json[key]);
                    // document.getElementById('trackno_display').innerHTML = document.getElementById('TrackNo').value;
                    $('#trackno_display').text($('#TrackNo').val());
                } else if (key == 'DateCreated') {
                    // document.getElementById('datecreated_display').innerHTML = json[key];
                    // $('[name="' + key + '"]').val(resultJson[index][key]);
                    $('#datecreated_display').text(json[key]);
                } else if (key == 'Requestor_Fullname') {// || key == 'Requestor_Name'
                    //Requestor_Fullname
                    // field_1.attr("value", json[key]);
                    // $('#requestor_display').text(json[key]);
                    field_1.attr("value", json[key]);
                    // document.getElementsByName('Requestor')[0].setAttribute('requestor-name', json[key]);
                    $('[name="' + 'Requestor' + '"]').attr('requestor-name', json[key])
                    // document.getElementById('requestor_display').innerHTML = json[key];
                    $('#requestor_display').text(json[key]);
                } else if (key == 'Processor_Name') {
                    field_1.attr("value", json[key]);
                    if (json[key] == "" || json[key] == "null" || !json[key]) {
                        $('#processor_display').parent().hide();
                    } else {
                        // document.getElementById('processor_display').innerHTML = json[key];
                        document.getElementById('processor_display').innerHTML = json[key];
                        $('#processor_display').attr('title', (json[key]+'').split(",").join("\u000d"));
                        // $('#processor_display').text(json[key]);
                    }
                    // $('#processor_display').text(json[key]);
                } else if (key == 'Status') {
                    field_1.val(json[key]);
                    // document.getElementById('status_display').innerHTML = document.getElementById('Status').value;
                    $('#status_display').text($('#Status').val());
                } else if (key == 'Delegate_Name') {
                    var delegates = json[key];
                    if (delegates && delegates != "") {
                        $('.delegate_display').show();
                        $('.delegate_display').show();
                        var delegatesArr = delegates.split(',');
                        if (delegatesArr.length > 1) {
                            delegatesArr.sort();
//                            var dele = document.getElementById('delegate_display');
//                            dele.innerHTML = delegatesArr[0] + '...';
//                            dele.setAttribute('data-original-title',delegatesArr.join('<br/>'));
//                            dele.style.cursor = 'pointer';
//                            document.getElementById('lbldelegate').innerHTML = "Delegates";
                            $('#delegate_display').text(delegatesArr[0] + '...');
                            $('#delegate_display').attr('data-original-title', delegatesArr.join('<br/>'));
                            $('#delegate_display').css('cursor', 'pointer');
                            $('#lbldelegate').text('Delegates');
                        } else {
//                            document.getElementById('delegate_display').value = delegates;
                            $('#delegate_display').text(delegates);
                        }

                    }


                    if (json[key] == '' || !json[key]) {
                        $('.delegate_display').addClass('display');
                    } // viewer action not showing
                } else if (field_1.attr("data-attachment")) {
                    //added by japhet morada
                    //FS#8036 - fix the popup edit for parent submission of embedded view
                    // if ($('#frmrequest').attr('embedded-view-object')) {
                    //     return;
                    // }
                    console.log(json[key], "san");
                    try {
                        if (json[key]) {
                            // ---------------------- OLD
                            // //code
                            // //var stringify = JSON.stringify(json[key]);
                            // //console.log($.parseJSON($.parseJSON(stringify)))
                            // var parse_file_type = $.parseJSON($.parseJSON(json[key]));
                            // // console.log("parse_file_type",parse_file_type)
                            // if (parse_file_type == null) {
                            //     // continue;
                            // }
                            // var location = parse_file_type[0]['location'];
                            // var parse_files = $.parseJSON(parse_file_type[0]['file_type']);

                            // console.log(parse_file_type)
                            // var blkstr = [];
                            // var file_name = parse_files['name'];
                            // var file_length = file_name.length;
                            // $.each(file_name, function (id, value) {
                            //     blkstr.push(value);
                            // });
                            // var files = blkstr.join(", ");
                            // // Save to body data
                            // $("body").data(key, parse_file_type);

                            // //console.log(parse_file_type);
                            // var file = json[key].split("/");
                            // var view_html = '<a id="view_files_in_modal" data-body-name="' + key + '" style="';
                            // view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
                            // view_html += '"><u> <i class="fa fa-search"></i> Show (' + file_length + ') File`s </u></a>';
                            // if (json[key] != "") {
                            //     if (field_1.attr("data-attachment")) {
                            //         field_1.parent().append(view_html);
                            //         field_1.val(json[key]);
                            //     }
                            // }


                            // ---------------------- NEW
                            //code
                            //var stringify = JSON.stringify(json[key]);
                            // console.log($.parseJSON($.parseJSON(stringify)))
                            // console.log(json[key])
                            console.log($.parseJSON(json[key]))
                            var filesUpload = $.parseJSON(json[key]);
                            var file_length = filesUpload.length;
                            console.log(file_length)
                            //
                            // var parse_file_type = $.parseJSON($.parseJSON(json[key]));
                            // // console.log("parse_file_type",parse_file_type)
                            // if (parse_file_type == null) {
                            //     // continue;
                            // }
                            // var location = parse_file_type[0]['location'];
                            // var parse_files = $.parseJSON(parse_file_type[0]['file_type']);

                            // console.log(parse_file_type)
                            // var blkstr = [];
                            // var file_name = parse_files['name'];
                            // // var file_length = file_name.length;
                            // $.each(file_name, function (id, value) {
                            //     blkstr.push(value);
                            // });
                            // var files = blkstr.join(", ");
                            // Save to body data
                            $("body").data(key, filesUpload);

                            //console.log(parse_file_type);
                            var file = json[key].split("/");
                            var view_html = '<a id="view_files_in_modal" data-body-name="' + key + '" style="';
                            view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
                            view_html += '"><u style="font-family:Arial;"> <i class="fa fa-search"></i> Show (' + file_length + ') File/s </u></a>';
                            if (json[key] != "") {
                                if (field_1.attr("data-attachment")) {
                                    field_1.parent().append(view_html);
                                    field_1.val(json[key]);
                                }
                            }

                        }
                    } catch (e) {
                    }
                } else if (field_1.attr("data-single-attachment")) {
                    //added by japhet morada
                    //FS#8036 - fix the popup edit for parent submission of embedded view
                    if ($('#frmrequest').attr('embedded-view-object')) {
                        return;
                    }
                    if (json[key]) {
                        var view_html = '<a target="_blank" href="' + json[key] + '" style="';
                        view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
                        var file = json[key].split("/");
                        view_html += '"><u> <i class="fa fa-search"></i> View File </u> ( <strong>' + file[file.length - 1] + '</strong> )</a>';
                        if (json[key] != "") {
                            if (json[key].indexOf("/images/on_attachment") != -1) {
                                field_1.parent().append(view_html);
                                field_1.val(json[key]);
                            }
                        }
                    }
                } else {
                    if (field_1.prop("tagName") == "TEXTAREA") {
                        if (json[key]) {
                            //added by japhet morada 123123
                            //FS#8036 - fix the popup edit for parent submission of embedded view
                            if (!$('#frmrequest').attr('embedded-view-object')) {
                                document.getElementsByName(key)[0].value = json[key];
                                // field_1.text(json[key]);
                            }
                        }
                    } else if (field_1.prop("tagName") == "SELECT") {
                        if (field_1.filter('[default-type="computed"][default-formula-value]:not([default-formula-value=""])').length >= 1) {
                            var append_new_opt = $('<option value="' + json[key] + '" selected="selected">' + json[key] + '</option>');
                            field_1.append(append_new_opt);
                            //added by japhet morada 123123
                            //FS#8036 - fix the popup edit for parent submission of embedded view
                            if (!$('#frmrequest').attr('embedded-view-object')) {
                                append_new_opt.prop('selected', true);
                            }
                        } else {
                            //added by japhet morada 123123
                            //FS#8036 - fix the popup edit for parent submission of embedded view
                            if (!$('#frmrequest').attr('embedded-view-object')) {
                                $('[name="' + key + '"] option[value="' + json[key] + '"]').attr("selected", "selected");
                            }
                        }
                    } else {
                        try {
                            if ($(document.getElementsByName(key)[0]).is("[data-type='time']")) {
                                // console.log("test --",$(document.getElementsByName(key)[0]).is("[data-type='time']"));
                                document.getElementsByName(key)[0].value = json[key].substr(0, json[key].length - 3);
                            } else {
                                //added by japhet morada 123123
                                //FS#8036 - fix the popup edit for parent submission of embedded view
                                if (!$('#frmrequest').attr('embedded-view-object')) {
                                    $('[name="' + key + '"]:not(meta)').eq(0).val(json[key]); //added by michael JANUARY_11_2016 3_44PM FOR FS#7770
                                    $('[name="' + key + '"]').eq(0).val(json[key]); //added by michael JANUARY_11_2016 3_44PM FOR FS#7770
                                }
                            }

                        }
                        catch (e) {
                            // console.log("error", e);
                        }
                        // field_1.attr("value", json[key]);
                        // field_1.val(json[key]);
                        //var photos = field_1.parent().next().children().attr("src");

                        //for submt request with photos
                        var status = $("#status_display").text();
                        if (currentUser != json[key] && status.indexOf('Updated') >= 0) {
                            //modified by michael 0335PM 10222015 ... pinapalitan ung data-type picklist
                            // if(!field_1.parent().parent().parent().parent().is("[data-type='pickList']")){


                            // }
                            if (field_1.parent().parent().parent().parent().is("[data-type='imageOnly']")) {
                                field_1.parent().parent().parent().parent().attr("data-type", "imageOnly");
                            }
                        }


                        // else {
                        // field_1.parent().parent().parent().parent().attr("data-type", "");
                        // }


                        if (json[key] != null) {
                            $(".uploader2").hide();
                            //field_1.parent().parent().parent().parent().attr("data-type", "");
                            field_1.parent().find('img').attr("src", json[key]);
                        }
                        if (json[key] == "") {
                            field_1.parent().find('img').attr("src", "/images/avatar/large.png");
                            //field_1.parent().parent().parent().parent().parent().attr("data-type", "");
                        }
                        // console.log("KEY:"+(key)+"  VALUE:"+(json[key]));
                    }
                }
            }

            if ($('.delegate_display').children('[id="delegate_display"]').text() == "") {
                $('.delegate_display').hide();
            }
            // console_time.end(key);   
            // }, 0);
        },
        setFieldValues: function (resultJson) {
            var self = this;
            var currentUser = $("#CurrentUser").val();
            // for (var index in resultJson) {
            //     for (var key in resultJson[index]) {
            //         console_time.start("test " + key);
            //         if ($('[name="' + key + '[]"]').attr("type") == "checkbox") {
            //             $('[name="' + key + '[]"]').each(function () {
            //                 if (resultJson[index][key]) {
            //                     var checkbox_value = resultJson[index][key].split(",");
            //                     var checkbox_value_new = resultJson[index][key].split("|^|");
            //                     if (checkbox_value.indexOf($(this).val()) >= 0 || checkbox_value_new.indexOf($(this).val()) >= 0) {
            //                         $(this).attr("checked", "checked")
            //                     }
            //                 }
            //             });
            //         } else if ($('[name="' + key + '"]').is(".smart-barcode-field-input-str-collection")) {
            //             //smart barcode 
            //             var valuestr = resultJson[index][key];
            //             $('[name="' + key + '"]').val(valuestr)
            //         } else if ($('[name="' + key + '[]"]').is("select[multiple]")) {

            //             var select_value = $.trim(resultJson[index][key]);
            //             if (select_value) {
            //                 var select_value_arr = select_value.split("|^|");
            //                 if ($('[name="' + key + '[]"]').is('[default-type="computed"]')) {
            //                     if ($('[name="' + key + '[]"]').attr('default-formula-value') != '') {

            //                         var append_new_opt_str = "";
            //                         var append_new_opt = null;
            //                         for (var ii in select_value_arr) {
            //                             append_new_opt_str += '<option value="' + select_value_arr[ii] + '" selected="selected">' + select_value_arr[ii] + '</option>';
            //                         }
            //                         append_new_opt = $(append_new_opt_str);
            //                         $('[name="' + key + '[]"]').append(append_new_opt);
            //                         append_new_opt.prop("selected", true);

            //                     } else {
            //                         $('[name="' + key + '[]"]').children("option").filter(function () {
            //                             return select_value_arr.indexOf($(this).attr("value")) >= 0;
            //                         }).prop("selected", true);
            //                     }
            //                 } else {
            //                     $('[name="' + key + '[]"]').children("option").filter(function () {
            //                         return select_value_arr.indexOf($(this).attr("value")) >= 0;
            //                     }).prop("selected", true);
            //                 }
            //             }
            //         } else if ($('[name="' + key + '"]').attr("type") == "radio") {
            //             // $('[name="' + key +'"]').each(function(){
            //             // });
            //             $('[name="' + key + '"][value="' + resultJson[index][key] + '"]').prop("checked", true);
            //         } else {
            //             if (key == "fieldEnabled" || key == "fieldRequired" || key == "fieldHiddenValues") {
            //                 $("." + key).text(resultJson[index][key]);
            //                 if ($('[name="' + key + '"]').prop("tagName") == "TEXTAREA") {
            //                     $('[name="' + key + '"]').text(resultJson[index][key]);
            //                     $('[name="' + key + '"]').val(resultJson[index][key]);
            //                 } else {
            //                     $('[name="' + key + '"]').attr("value", resultJson[index][key]);
            //                     $('[name="' + key + '"]').val(resultJson[index][key]);
            //                 }
            //             } else if (key == 'TrackNo') {
            //                 $('[name="' + key + '"]').val(resultJson[index][key]);
            //                 $('#trackno_display').text($('#TrackNo').val());

            //             } else if (key == 'DateCreated') {
            //                 $('[name="' + key + '"]').val(resultJson[index][key]);
            //                 $('#datecreated_display').text($('#DateCreated').val());
            //             } else if (key == 'Requestor_Fullname') {// || key == 'Requestor_Name'
            //                 //Requestor_Fullname
            //                 // $('[name="' + key + '"]').attr("value", resultJson[index][key]);
            //                 // $('#requestor_display').text(resultJson[index][key]);
            //                 $('[name="' + key + '"]').attr("value", resultJson[index][key]);
            //                 $('[name="' + 'Requestor' + '"]').attr('requestor-name', resultJson[index][key])
            //                 $('#requestor_display').text(resultJson[index][key]);
            //             } else if (key == 'Processor_Name') {
            //                 if (resultJson[index][key]) {
            //                     $('[name="' + key + '"]').attr("value", resultJson[index][key]);
            //                     $('#processor_display').text(resultJson[index][key]);
            //                 } else {
            //                     $('#lblprocessor_display').hide();
            //                 }
            //             } else if (key == 'Status') {
            //                 $('[name="' + key + '"]').val(resultJson[index][key]);
            //                 $('#status_display').text($('#Status').val());
            //             } else if (key == 'Delegate_Name') {
            //                 var delegates = resultJson[index][key];
            //                 if (delegates && delegates != "") {
            //                     var delegatesArr = delegates.split(',');
            //                     if (delegatesArr.length > 1) {
            //                         delegatesArr.sort();
            //                         $('#delegate_display').text(delegatesArr[0] + '...');
            //                         $('#delegate_display').attr('data-original-title', delegatesArr.join('<br/>'));
            //                         $('#delegate_display').css('cursor', 'pointer');
            //                         $('#lbldelegate').text('Delegates');
            //                     } else {
            //                         $('#delegate_display').text(delegates);
            //                     }

            //                 }
            //                 if (resultJson[index][key] == '' || !resultJson[index][key]) {
            //                     $('.delegate_display').addClass('display');
            //                 } // viewer action not showing
            //             } else if ($('[name="' + key + '"]').attr("data-attachment")) {
            //                 if (resultJson[index][key]) {
            //                     //code
            //                     //var stringify = JSON.stringify(resultJson[index][key]);
            //                     //console.log($.parseJSON($.parseJSON(stringify)))
            //                     var parse_file_type = $.parseJSON($.parseJSON(resultJson[index][key]));
            //                     // console.log("parse_file_type",parse_file_type);
            //                     if (parse_file_type == null) {
            //                         continue;
            //                     }
            //                     var location = parse_file_type[0]['location'];
            //                     var parse_files = $.parseJSON(parse_file_type[0]['file_type']);

            //                     console.log(parse_file_type)
            //                     var blkstr = [];
            //                     var file_name = parse_files['name'];
            //                     var file_length = file_name.length;
            //                     $.each(file_name, function (id, value) {
            //                         blkstr.push(value);
            //                     });
            //                     var files = blkstr.join(", ");
            //                     // Save to body data
            //                     $("body").data(key, parse_file_type);

            //                     //console.log(parse_file_type);
            //                     var file = resultJson[index][key].split("/");
            //                     var view_html = '<a id="view_files_in_modal" data-body-name="' + key + '" style="';
            //                     view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
            //                     view_html += '"><u> <i class="fa fa-search"></i> Show (' + file_length + ') File`s </u></a>';
            //                     if (resultJson[index][key] != "") {
            //                         if ($('[name="' + key + '"]').attr("data-attachment")) {
            //                             $('[name="' + key + '"]').parent().append(view_html);
            //                             $('[name="' + key + '"]').val(resultJson[index][key]);
            //                         }
            //                     }
            //                 }
            //             } else if ($('[name="' + key + '"]').attr("data-single-attachment")) {
            //                 if (resultJson[index][key]) {
            //                     var view_html = '<a target="_blank" href="' + resultJson[index][key] + '" style="';
            //                     view_html += 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
            //                     var file = resultJson[index][key].split("/");
            //                     view_html += '"><u> <i class="fa fa-search"></i> View File </u> ( <strong>' + file[file.length - 1] + '</strong> )</a>';
            //                     if (resultJson[index][key] != "") {
            //                         if (resultJson[index][key].indexOf("/images/on_attachment") != -1) {
            //                             $('[name="' + key + '"]').parent().append(view_html);
            //                             $('[name="' + key + '"]').val(resultJson[index][key]);
            //                         }
            //                     }
            //                 }
            //             } else {
            //                 if ($('[name="' + key + '"]').prop("tagName") == "TEXTAREA") {
            //                     if (resultJson[index][key]) {
            //                         $('[name="' + key + '"]').text(resultJson[index][key]);
            //                     }
            //                 } else if ($('[name="' + key + '"]').prop("tagName") == "SELECT") {
            //                     if ($('[name="' + key + '"]').filter('[default-type="computed"][default-formula-value]:not([default-formula-value=""])').length >= 1) {
            //                         var append_new_opt = $('<option value="' + resultJson[index][key] + '" selected="selected">' + resultJson[index][key] + '</option>');
            //                         $('[name="' + key + '"]').append(append_new_opt);
            //                         append_new_opt.prop('selected', true);
            //                     } else {
            //                         $('[name="' + key + '"] option[value="' + resultJson[index][key] + '"]').attr("selected", "selected");
            //                     }
            //                 } else {

            //                     if (key == "Workflow_ID") {
            //                         $('[name="WorkflowId"]').attr("value", resultJson[index][key]);
            //                         $('[name="WorkflowId"]').val(resultJson[index][key]);
            //                     }

            //                     $('[name="' + key + '"]').attr("value", resultJson[index][key]);
            //                     $('[name="' + key + '"]').val(resultJson[index][key]);
            //                     //var photos = $('[name="' + key + '"]').parent().next().children().attr("src");

            //                     //for submt request with photos
            //                     var status = $("#status_display").text();
            //                     if (currentUser != resultJson[index][key] && status.indexOf('Updated') >= 0) {
            //                         $('[name="' + key + '"]').parent().parent().parent().parent().attr("data-type", "imageOnly");
            //                     }
            //                     // else {
            //                     // $('[name="' + key + '"]').parent().parent().parent().parent().attr("data-type", "");
            //                     // }


            //                     if (resultJson[index][key] != null) {
            //                         $(".uploader2").hide();
            //                         //$('[name="' + key + '"]').parent().parent().parent().parent().attr("data-type", "");
            //                         $('[name="' + key + '"]').parent().find('img').attr("src", resultJson[index][key]);
            //                     }
            //                     if (resultJson[index][key] == "") {
            //                         $('[name="' + key + '"]').parent().find('img').attr("src", "/images/avatar/large.png");
            //                         //$('[name="' + key + '"]').parent().parent().parent().parent().parent().attr("data-type", "");
            //                     }
            //                     // console.log("KEY:"+(key)+"  VALUE:"+(resultJson[index][key]));
            //                 }
            //             }
            //         }
            //         console_time.end("test " + key);
            //     }
            // }
            var imported = $("#imported").val();
            // if (imported == "1") {
            // $("[name='fieldEnabled']").val(tmpFieldEnabled);
            // $(".fieldEnabled").html(tmpFieldEnabled);
            // $("#imported").val('');
            // } else {
            self.checkDocumentLock(function (docLockData) {
                docLockData = JSON.parse(docLockData);
                if (docLockData.status == 'warning') {
//                    showNotification({
//                        message: docLockData.message,
//                        type: "warning",
//                        autoClose: true,
//                        duration: 5
//                    });
                    alert(docLockData.message);
                    self.getButtons(false);
                } else {
                    self.getButtons(true);
                    self.fieldEnabled();
                }

                // }
                if ($('#Processor').val() == 'null' || $('#Processor').val() == null || $('#Processor').val() == '') {
                    $('#lblprocessor_display').hide();
                    $('#processor_display').add($('#processor_display').parents("label").eq(0)).hide();
                }

                if ($(".submit-overlay").length >= 1) {
                    $(".submit-overlay").fadeOut(function () {
                        $(this).remove();
                    });
                }

                self.fieldRequired();
                self.fieldHiddenValues();
            });

        },
        checkDocumentLock: function (callback) {
            var docLockEnabled = $('#enable_doc_lock').val();
            if (docLockEnabled == 1) {
                $.get('/modules/data/check_document_lock', {
                    form_id: $('#FormID').val(),
                    request_id: $('#ID').val()
                }, function (data) {
                    console.log('checkDocumentLock', data);
                    callback(data);
                });
            } else {
                var data = {
                    status: "Success"
                };
                data = JSON.stringify(data);
                callback(data);
            }

        },
        fieldEnabled: function () {

            if ($('[name="Status"]').val() == "Cancelled") {
                return false;
            }
            ;

            var fields = ""
            var requestId = $("#ID").val();
            var requestor_id = $("#Requestor").val();
            var currentUser = $("#CurrentUser").val();
            var status = $('#Status').val();
            //if (requestor_id == currentUser || requestId == 0) {
            //    fields = $(".fieldEnabled_default").text();
            //} else {

            if (status == 'DRAFT') {
                fields = $(".fieldEnabled_default").text();
            } else {
                fields = $(".fieldEnabled").text();
            }

            //}
            fields = (fields == "null" ? "" : fields);

            if (fields) {
                fields = JSON.parse(fields);
                console.log(fields)
                $.each(fields, function (key, value) {
                    if ($('[name="' + value + '[]"]').attr("type") == "checkbox") {
                        $('[name="' + value + '[]"]').attr("disabled", false);
                    } else if ($('[name="' + value + '[]"]').is('.smart-barcode-field-input')) {
                        $('[name="' + value + '[]"]').attr("disabled", false);

                    } else if ($('[name="' + value + '"]').is('.smart-barcode-field-input-str-collection')) { //added by aaron (hindi na seselect pag array ung smart barcode)
                        $('[name="' + value + '"]').attr("disabled", false);
                        $('[name="' + value + '"]').closest(".setOBJ").eq(0).find(".smart-barcode-field-input").attr("disabled", false);
                    } else if ($('[name="' + value + '[]"]').is('select[multiple="multiple"]')) {
                        $('[name="' + value + '[]"]').attr("disabled", false);
                    } else if ($('[name="' + value + '"]').attr("data-single-attachment") ||
                            $('[name="' + value + '"]').attr("data-attachment")
                            ) {
                        var object_id = $('[name="' + value + '"]').attr("id");
                        var ob_id = object_id.split("_");
                        var new_ob_id = ob_id[ob_id.length - 1];
                        //$('#on_attachment_' + new_ob_id).each(function(){
                        $('#on_attachment_' + new_ob_id).attr("disabled", false);
                        $('#multiple_on_attachment_' + new_ob_id).attr("disabled", false);
                        $('[name="' + value + '"]').attr("disabled", false);
                        //});
                    } /*else if($('#' + value).attr("type") == "file"){
                     //$('#' + value).removeAttr("disabled");
                     //$('#' + value).each(function(){
                     console.log(value)
                     $('#' + value).removeAttr("disabled");
                     //});
                     }*/
                    else {

                        var myVariable = $('[name="' + value + '"]').val();

                        //if(/^([a-z]([a-z]|\d|\+|-|\.)*):(\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?((\[(|(v[\da-f]{1,}\.(([a-z]|\d|-|\.|_|~)|[!\$&'\(\)\*\+,;=]|:)+))\])|((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=])*)(:\d*)?)(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*|(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)|((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)){0})(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(myVariable)) {
                        $(".resize_image_request").attr("data-upload", "photo_upload");
                        //$('[name="' + value + '"]').parent().parent().parent().attr("data-upload", "photo_upload");
                        //$('[name="' + value + '"]').parent().parent().parent().attr("disabled", false);
                        //    alert(1)
                        //} else {
                        $('[name="' + value + '"]').attr("disabled", false);
                        //}



                        //show picklist if enabled
                        $('[name="' + value + '"]').closest(".setOBJ").find(".pickListButton").show();
                    }
                });
            }
        },
        readOnlyFormula: function () {
            $('.setOBJ').filter(function (eqi, elem) {
                elem = $(this);
                var doi = $(elem).attr("data-object-id");
                if (doi) {
                    var get_flds = $(elem).find('.getFields_' + doi);
                    if (get_flds.is('[read-only-formula]') && $.type(get_flds.attr("read-only-formula")) == "string") {
                        return $.trim(get_flds.attr("read-only-formula")).length >= 1;
                    }
                }
                return false;
            }).each(function (eqi) {
                var keywords = ["Requestor", "RequestID", "Status", "CurrentUser", "Department", "TrackNo", "Processor", "Today", "Now", "TimeStamp"];
                var self = $(this);
                var doi = self.attr("data-object-id");
                var obj_fields = self.find('.getFields_' + doi); // ito ung may formula
                var self_name = obj_fields.attr("name");
                var read_only_formula = obj_fields.attr("read-only-formula");
                var process_read_only_formula = read_only_formula;
                var reg_eks_match_fn = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*", "g");
                var matched_field_name = read_only_formula.match(reg_eks_match_fn);

                var matched_function_names = null;
                if (matched_field_name) { // if not null
                    {//getting the field names and functions matched
                        matched_function_names = matched_field_name.filter(Boolean).filter(function (value, ele_index) {
                            var sanitized_val = value.replace("@", "");
                            var flag_return = false;
                            try {
                                if (eval("typeof " + sanitized_val) == "function") {
                                    flag_return = true;
                                } else {
                                    flag_return = false;
                                }
                            } catch (error) {
                                console.error("readOnlyFormula: matched_function_names ... typeof ", sanitized_val, " is error!");
                            }
                            return flag_return;
                        });

                        matched_field_name = matched_field_name.filter(Boolean).filter(function (value, ele_index) {
                            var sanitized_val = value.replace("@", "");
                            var flag_return = false;
                            try {
                                if (eval("typeof " + sanitized_val) == "function") {
                                    flag_return = false;
                                } else if (keywords.indexOf(sanitized_val) >= 0) {
                                    flag_return = false;
                                } else {
                                    flag_return = true;
                                }
                            } catch (error) {
                                console.error("readOnlyFormula: matched_field_name ... typeof ", sanitized_val, " is error!");
                            }
                            return flag_return;
                        });
                    }




                    var formula_has_fieldname = false;
                    if ($.type(matched_field_name) == "array") { // binding of events of fields
                        if (matched_field_name.length >= 1) {
                            formula_has_fieldname = true;
                            console.log("READONLY FORMULA", matched_field_name);
                            var field_names_sanitized = $.unique(matched_field_name.map(function (val, elem_index) {
                                if (typeof val == "string") {
                                    return val.replace("@", "");
                                } else {
                                    return null;
                                }
                            }));
                            console.log("field_names_sanitized", field_names_sanitized)
                            // var reg_eks_for_replace = "";
                            var event_addressing = "";
                            for (var ctr_index in field_names_sanitized) {
                                var field_object = $('[name="' + field_names_sanitized[ctr_index] + '"]');
                                if (field_object.length <= 0) {
                                    field_object = $('[name="' + field_names_sanitized[ctr_index] + '[]"]');
                                }
                                if (field_object.length >= 1) {
                                    if ($.type(field_object.data("read_only_formula")) == "array") {
                                        field_object.data("read_only_formula").push({
                                            "field_names_sanitized": field_names_sanitized,
                                            "read_only_formula": read_only_formula,
                                            "formula_source_field": obj_fields
                                        });
                                    } else {//HERE IS THE FIRST BIND
                                        field_object.data("read_only_formula", [{
                                                "field_names_sanitized": field_names_sanitized,
                                                "read_only_formula": read_only_formula,
                                                "formula_source_field": obj_fields
                                            }]);
                                        field_object.on("change.readOnlyControl.read_only_formula|" + self_name, function (e) {
                                            var readonly_formula = $(this).data("read_only_formula");
                                            for (var ctr_in in readonly_formula) {
                                                var formulaDoc = new Formula(readonly_formula[ctr_in]["read_only_formula"]);
                                                // alert(formulaDoc.getEvaluation());
                                                var readonly_flagging = formulaDoc.getEvaluation();

                                                var selected_ele_setOBJ = readonly_formula[ctr_in]["formula_source_field"].parents('.setOBJ').eq(0)
                                                var doi = selected_ele_setOBJ.attr('data-object-id');

                                                if (readonly_flagging == true) {
                                                    console.log("eto ung sinave", selected_ele_setOBJ);
                                                    if (selected_ele_setOBJ.is('[data-type="attachment_on_request"]')) {
                                                        selected_ele_setOBJ.find('.file_attachement[data-action-id="' + doi + '"][data-action-type="attachmentForm"]').attr("readonly", true);
                                                    } else if (selected_ele_setOBJ.is('[data-type="multiple_attachment_on_request"]')) {
                                                        selected_ele_setOBJ.find('.multiple_file_attachement[data-action-id="' + doi + '"][data-action-type="multiple_attachmentForm"]').attr("readonly", true);
                                                    } else if (selected_ele_setOBJ.is('[data-type="noteStyleTextarea"]')) {
                                                        selected_ele_setOBJ.find('.content-editable-field_' + doi).attr("contenteditable", false);
                                                    } else if (selected_ele_setOBJ.is('[data-type="pickList"]')) {
                                                        selected_ele_setOBJ.find('.pickListButton[picklist-button-id="picklist_button_' + doi + '"]').attr("readonly", true);
                                                        readonly_formula[ctr_in]["formula_source_field"].attr("readonly", true);
                                                        readonly_formula[ctr_in]["formula_source_field"].next().hide();
                                                    } else if (selected_ele_setOBJ.is('[data-type="listNames"]')) {
                                                        readonly_formula[ctr_in]["formula_source_field"].next().hide();
                                                        readonly_formula[ctr_in]["formula_source_field"].attr("readonly", true);
                                                    } else if (selected_ele_setOBJ.is('[data-type="dropdown"],[data-type="selectMany"],[data-type="radioButton"],[data-type="checkbox"]')) {
                                                        readonly_formula[ctr_in]["formula_source_field"].attr("disabled", true).attr("readonly-disabled", "");
                                                    } else if (selected_ele_setOBJ.is('[data-type="smart_barcode_field"]')) {
                                                        var temp = readonly_formula[ctr_in]["formula_source_field"].parent().next()
                                                        temp.find('.smart-barcode-field-input').attr("readonly", true);
                                                        temp.find('.sbcf-action-icon-add,.sbcf-action-icon-delete').addClass('readonly-true');
                                                    } else {

                                                        readonly_formula[ctr_in]["formula_source_field"].attr("readonly", true);
                                                    }
                                                    if (readonly_formula[ctr_in]["formula_source_field"].is('.hasDatepicker') && readonly_formula[ctr_in]["formula_source_field"].closest('.setOBJ[data-type="datepicker"]').length >= 1) {
                                                        readonly_formula[ctr_in]["formula_source_field"].datepicker('option', 'showOn', 'button');
                                                    } else if (readonly_formula[ctr_in]["formula_source_field"].is('.hasDatepicker') && readonly_formula[ctr_in]["formula_source_field"].closest('.setOBJ[data-type="time"]').length >= 1) {
                                                        var temp = readonly_formula[ctr_in]["formula_source_field"].val();
                                                        readonly_formula[ctr_in]["formula_source_field"].datetimepicker('option', 'showOn', 'button');
                                                        readonly_formula[ctr_in]["formula_source_field"].val(temp);
                                                    } else if (readonly_formula[ctr_in]["formula_source_field"].is('.hasDatepicker') && readonly_formula[ctr_in]["formula_source_field"].closest('.setOBJ[data-type="dateTime"]').length >= 1) {
                                                        var temp = readonly_formula[ctr_in]["formula_source_field"].val();
                                                        readonly_formula[ctr_in]["formula_source_field"].datetimepicker('option', 'showOn', 'button');
                                                        readonly_formula[ctr_in]["formula_source_field"].val(temp);
                                                    }
                                                } else if (readonly_flagging == false) {
                                                    if (selected_ele_setOBJ.is('[data-type="attachment_on_request"]')) {
                                                        selected_ele_setOBJ.find('.file_attachement[data-action-id="' + doi + '"][data-action-type="attachmentForm"]').attr("readonly", false);
                                                    } else if (selected_ele_setOBJ.is('[data-type="multiple_attachment_on_request"]')) {
                                                        selected_ele_setOBJ.find('.multiple_file_attachement[data-action-id="' + doi + '"][data-action-type="multiple_attachmentForm"]').attr("readonly", false);
                                                    } else if (selected_ele_setOBJ.is('[data-type="noteStyleTextarea"]')) {
                                                        selected_ele_setOBJ.find('.content-editable-field_' + doi).attr("contenteditable", true);
                                                    } else if (selected_ele_setOBJ.is('[data-type="pickList"]')) {
                                                        selected_ele_setOBJ.find('.pickListButton[picklist-button-id="picklist_button_' + doi + '"]').attr("readonly", false);
                                                        readonly_formula[ctr_in]["formula_source_field"].attr("readonly", false);
                                                        readonly_formula[ctr_in]["formula_source_field"].next().show();
                                                    } else if (selected_ele_setOBJ.is('[data-type="listNames"]')) {
                                                        readonly_formula[ctr_in]["formula_source_field"].next().show();
                                                        readonly_formula[ctr_in]["formula_source_field"].attr("readonly", false);
                                                    } else if (selected_ele_setOBJ.is('[data-type="dropdown"],[data-type="selectMany"],[data-type="radioButton"],[data-type="checkbox"]')) {
                                                        if (readonly_formula[ctr_in]["formula_source_field"].is('[readonly-disabled]')) {
                                                            readonly_formula[ctr_in]["formula_source_field"].removeAttr("disabled");
                                                            readonly_formula[ctr_in]["formula_source_field"].removeAttr("readonly-disabled");
                                                        }
                                                    } else if (selected_ele_setOBJ.is('[data-type="smart_barcode_field"]')) {
                                                        var temp = readonly_formula[ctr_in]["formula_source_field"].parent().next()
                                                        temp.find('.smart-barcode-field-input').attr("readonly", false);
                                                        temp.find('.sbcf-action-icon-add,.sbcf-action-icon-delete').removeClass('readonly-true');
                                                    } else {
                                                        readonly_formula[ctr_in]["formula_source_field"].attr("readonly", false);
                                                    }
                                                    if (readonly_formula[ctr_in]["formula_source_field"].is('.hasDatepicker') && readonly_formula[ctr_in]["formula_source_field"].closest('.setOBJ[data-type="datepicker"]').length >= 1) {
                                                        readonly_formula[ctr_in]["formula_source_field"].datepicker('option', 'showOn', 'focus');
                                                    } else if (readonly_formula[ctr_in]["formula_source_field"].is('.hasDatepicker') && readonly_formula[ctr_in]["formula_source_field"].closest('.setOBJ[data-type="time"]').length >= 1) {
                                                        var temp = readonly_formula[ctr_in]["formula_source_field"].val();
                                                        readonly_formula[ctr_in]["formula_source_field"].datetimepicker('option', 'showOn', 'focus');
                                                        readonly_formula[ctr_in]["formula_source_field"].val(temp);
                                                    } else if (readonly_formula[ctr_in]["formula_source_field"].is('.hasDatepicker') && readonly_formula[ctr_in]["formula_source_field"].closest('.setOBJ[data-type="dateTime"]').length >= 1) {
                                                        var temp = readonly_formula[ctr_in]["formula_source_field"].val();
                                                        readonly_formula[ctr_in]["formula_source_field"].datetimepicker('option', 'showOn', 'focus');
                                                        readonly_formula[ctr_in]["formula_source_field"].val(temp);
                                                    }
                                                } else {
                                                    readonly_formula[ctr_in]["formula_source_field"].attr("readonly", false);
                                                    if (readonly_formula[ctr_in]["formula_source_field"].is('.hasDatepicker') && readonly_formula[ctr_in]["formula_source_field"].closest('.setOBJ[data-type="datepicker"]').length >= 1) {
                                                        readonly_formula[ctr_in]["formula_source_field"].datepicker('option', 'showOn', 'focus');
                                                    } else if (readonly_formula[ctr_in]["formula_source_field"].is('.hasDatepicker') && readonly_formula[ctr_in]["formula_source_field"].closest('.setOBJ[data-type="time"]').length >= 1) {
                                                        var temp = readonly_formula[ctr_in]["formula_source_field"].val();
                                                        readonly_formula[ctr_in]["formula_source_field"].datetimepicker('option', 'showOn', 'focus');
                                                        readonly_formula[ctr_in]["formula_source_field"].val(temp);
                                                    } else if (readonly_formula[ctr_in]["formula_source_field"].is('.hasDatepicker') && readonly_formula[ctr_in]["formula_source_field"].closest('.setOBJ[data-type="dateTime"]').length >= 1) {
                                                        var temp = readonly_formula[ctr_in]["formula_source_field"].val();
                                                        readonly_formula[ctr_in]["formula_source_field"].datetimepicker('option', 'showOn', 'focus');
                                                        readonly_formula[ctr_in]["formula_source_field"].val(temp);
                                                    } else if (selected_ele_setOBJ.is('[data-type="listNames"]')) {
                                                        readonly_formula[ctr_in]["formula_source_field"].next().show();
                                                    } else if (selected_ele_setOBJ.is('[data-type="pickList"]')) {
                                                        readonly_formula[ctr_in]["formula_source_field"].next().show();
                                                    } else if (selected_ele_setOBJ.is('[data-type="dropdown"],[data-type="selectMany"],[data-type="radioButton"],[data-type="checkbox"]')) {
                                                        if (readonly_formula[ctr_in]["formula_source_field"].is('[readonly-disabled]')) {
                                                            readonly_formula[ctr_in]["formula_source_field"].removeAttr("disabled");
                                                            readonly_formula[ctr_in]["formula_source_field"].removeAttr("readonly-disabled");
                                                        }
                                                    }
                                                }
                                            }
                                            // console.log("formulaDoc.getEvaluation()",formulaDoc.getEvaluation())
                                        });
                                    }
                                }
                            }
                        }
                    }
                    if (formula_has_fieldname == false) {
                        var formulaDoc = new Formula(read_only_formula);
                        var parsed_value = formulaDoc.getEvaluation();
                        if ($.type(parsed_value) === "boolean") {
                            if (parsed_value == true) {
                                if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="pickList"]')) {
                                    obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').attr("readonly", true);
                                    obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').next().hide();
                                    obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]')
                                            .off(".picklistClickField")
                                            .css("cursor", "")
                                            .next().find('i.fa-list-alt').hide();

                                } else if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="listNames"]')) {
                                    obj_fields.next().hide();
                                    obj_fields.attr("readonly", true);
                                } else if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="dropdown"],[data-type="selectMany"],[data-type="radioButton"],[data-type="checkbox"]')) {
                                    obj_fields.attr("disabled", true).attr("readonly-disabled", "");
                                } else {
                                    obj_fields.attr("readonly", true);
                                }
                                if (obj_fields.is('.hasDatepicker') && obj_fields.closest('.setOBJ[data-type="datepicker"]').length >= 1) {
                                    obj_fields.datepicker('option', 'showOn', 'button');
                                } else if (obj_fields.is('.hasDatepicker') && obj_fields.closest('.setOBJ[data-type="time"]').length >= 1) {
                                    var temp = obj_fields.val();
                                    obj_fields.datetimepicker('option', 'showOn', 'button');
                                    obj_fields.val(temp);
                                } else if (obj_fields.is('.hasDatepicker') && obj_fields.closest('.setOBJ[data-type="dateTime"]').length >= 1) {
                                    var temp = obj_fields.val();
                                    obj_fields.datetimepicker('option', 'showOn', 'button');
                                    obj_fields.val(temp);
                                }
                            } else if (parsed_value == false) {
                                if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="pickList"]')) {
                                    obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').attr("readonly", false);
                                    obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').next().show();
                                    obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').on('click.picklistClickField', function () {
                                        $(this).blur();
                                        $(this).next().find('.pickListButton:eq(0)').trigger('click');
                                    }).css('cursor', 'pointer').next().find('i.fa-list-alt').show();
                                } else if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="listNames"]')) {
                                    obj_fields.next().show();
                                    obj_fields.attr("readonly", false);
                                } else if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="dropdown"],[data-type="selectMany"],[data-type="radioButton"],[data-type="checkbox"]')) {
                                    if (obj_fields.is('[readonly-disabled]')) {
                                        obj_fields.removeAttr("disabled");
                                        obj_fields.removeAttr("readonly-disabled");
                                    }
                                } else {
                                    obj_fields.attr("readonly", false);
                                }
                                if (obj_fields.is('.hasDatepicker') && obj_fields.closest('.setOBJ[data-type="datepicker"]').length >= 1) {
                                    obj_fields.datepicker('option', 'showOn', 'focus');
                                } else if (obj_fields.is('.hasDatepicker') && obj_fields.closest('.setOBJ[data-type="time"]').length >= 1) {
                                    var temp = obj_fields.val();
                                    obj_fields.datetimepicker('option', 'showOn', 'focus');
                                    obj_fields.val(temp);
                                } else if (obj_fields.is('.hasDatepicker') && obj_fields.closest('.setOBJ[data-type="dateTime"]').length >= 1) {
                                    var temp = obj_fields.val();
                                    obj_fields.datetimepicker('option', 'showOn', 'focus');
                                    obj_fields.val(temp);
                                }
                            }
                        } else {
                            if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="pickList"]')) {
                                obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').attr("readonly", false);
                                obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').next().show();
                                obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').on('click.picklistClickField', function () {
                                    $(this).blur();
                                    $(this).next().find('.pickListButton:eq(0)').trigger('click');
                                }).css('cursor', 'pointer').next().find('i.fa-list-alt').show();
                            } else if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="listNames"]')) {
                                obj_fields.next().show();
                                obj_fields.attr("readonly", false);
                            } else if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="dropdown"],[data-type="selectMany"],[data-type="radioButton"],[data-type="checkbox"]')) {
                                if (obj_fields.is('[readonly-disabled]')) {
                                    obj_fields.removeAttr("disabled");
                                    obj_fields.removeAttr("readonly-disabled");
                                }
                            } else {
                                obj_fields.attr("readonly", false);
                            }
                            if (obj_fields.is('.hasDatepicker') && obj_fields.closest('.setOBJ[data-type="datepicker"]').length >= 1) {
                                obj_fields.datepicker('option', 'showOn', 'focus');
                            } else if (obj_fields.is('.hasDatepicker') && obj_fields.closest('.setOBJ[data-type="time"]').length >= 1) {
                                var temp = obj_fields.val();
                                obj_fields.datetimepicker('option', 'showOn', 'focus');
                                obj_fields.val(temp);
                            } else if (obj_fields.is('.hasDatepicker') && obj_fields.closest('.setOBJ[data-type="dateTime"]').length >= 1) {
                                var temp = obj_fields.val();
                                obj_fields.datetimepicker('option', 'showOn', 'focus');
                                obj_fields.val(temp);
                            }
                        }

                    }

                }
            });

            $('.getFields').filter(function (index, val_dis) {
                if ($(val_dis).data('events')) {
                    if ($(val_dis).data('events')['change']) {
                        return $(this).data('events')['change'].filter(function (val, index_) {
                            return val['namespace'].indexOf('readOnlyControl') >= 0;
                        }).length >= 1;
                    }
                }
                return false;
            }).trigger('change', ["triggerReadOnlyFormula"]);
        },
        disabledFormula2: function () {
            $('.setOBJ').filter(function (eqi, elem) {
                var doi = $(elem).attr("data-object-id");
                if (doi) {
                    var get_flds = $(elem).find('.getFields_' + doi);
                    if (get_flds.is('[disable-formula]') && $.type(get_flds.attr("disable-formula")) == "string") {
                        return $.trim(get_flds.attr("disable-formula")).length >= 1;
                    }
                }
                return false;
            }).each(function (eqi) {
                var keywords = ["Requestor", "RequestID", "Status", "CurrentUser", "Department", "TrackNo", "Processor", "Today", "Now", "TimeStamp"];
                var self = $(this);
                var doi = self.attr("data-object-id");
                var obj_fields = self.find('.getFields_' + doi); // ito ung may formula
                var self_name = obj_fields.attr("name");
                var disable_formula = obj_fields.attr("disable-formula");
                var process_disable_formula = disable_formula;
                var reg_eks_match_fn = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*", "g");
                var matched_field_name = disable_formula.match(reg_eks_match_fn);

                var matched_function_names = null;
                if (matched_field_name) { // if not null
                    {//getting the field names and functions matched
                        matched_function_names = matched_field_name.filter(Boolean).filter(function (value, ele_index) {
                            var sanitized_val = value.replace("@", "");
                            var flag_return = false;
                            try {
                                if (eval("typeof " + sanitized_val) == "function") {
                                    flag_return = true;
                                } else {
                                    flag_return = false;
                                }
                            } catch (error) {
                                console.error("disabledFormula: matched_function_names ... typeof ", sanitized_val, " is error!");
                            }
                            return flag_return;
                        });

                        matched_field_name = matched_field_name.filter(Boolean).filter(function (value, ele_index) {
                            var sanitized_val = value.replace("@", "");
                            var flag_return = false;
                            try {
                                if (eval("typeof " + sanitized_val) == "function") {
                                    flag_return = false;
                                } else if (keywords.indexOf(sanitized_val) >= 0) {
                                    flag_return = false;
                                } else {
                                    flag_return = true;
                                }
                            } catch (error) {
                                console.error("disabledFormula: matched_field_name ... typeof ", sanitized_val, " is error!");
                            }
                            return flag_return;
                        });
                    }




                    var formula_has_fieldname = false;
                    if ($.type(matched_field_name) == "array") { // binding of events of fields
                        if (matched_field_name.length >= 1) {
                            formula_has_fieldname = true;
                            console.log("DISABLED FORMULA", matched_field_name);
                            var field_names_sanitized = $.unique(matched_field_name.map(function (val, elem_index) {
                                if (typeof val == "string") {
                                    return val.replace("@", "");
                                } else {
                                    return null;
                                }
                            }));
                            console.log("field_names_sanitized", field_names_sanitized)
                            // var reg_eks_for_replace = "";
                            var event_addressing = "";
                            for (var ctr_index in field_names_sanitized) {
                                var field_object = $('[name="' + field_names_sanitized[ctr_index] + '"]');
                                if (field_object.length >= 1) {
                                    if ($.type(field_object.data("disabledFormula")) == "array") {
                                        field_object.data("disabledFormula").push({
                                            "field_names_sanitized": field_names_sanitized,
                                            "disable_formula": disable_formula,
                                            "formula_source_field": obj_fields
                                        })
                                    } else {//HERE IS THE FIRST BIND
                                        field_object.data("disabledFormula", [{
                                                "field_names_sanitized": field_names_sanitized,
                                                "disable_formula": disable_formula,
                                                "formula_source_field": obj_fields
                                            }]);
                                        field_object.on("change.disabledFormulaControl.disabledFormula|" + self_name, function (e) {
                                            var disable_formula_data = $(this).data("disabledFormula");
                                            for (var ctr_in in disable_formula_data) {
                                                var formulaDoc = new Formula(disable_formula_data[ctr_in]["disable_formula"]);
                                                // alert(formulaDoc.getEvaluation());
                                                var disabled_flagging = formulaDoc.getEvaluation();

                                                var selected_ele_setOBJ = disable_formula_data[ctr_in]["formula_source_field"].parents('.setOBJ').eq(0)
                                                var doi = selected_ele_setOBJ.attr('data-object-id');

                                                if (disabled_flagging == true) {
                                                    console.log("eto ung sinave", selected_ele_setOBJ);
                                                    if (selected_ele_setOBJ.is('[data-type="attachment_on_request"]')) {
                                                        selected_ele_setOBJ.find('.file_attachement[data-action-id="' + doi + '"][data-action-type="attachmentForm"]').attr("disabled", true);
                                                    } else if (selected_ele_setOBJ.is('[data-type="multiple_attachment_on_request"]')) {
                                                        selected_ele_setOBJ.find('.multiple_file_attachement[data-action-id="' + doi + '"][data-action-type="multiple_attachmentForm"]').attr("disabled", true);
                                                    } else if (selected_ele_setOBJ.is('[data-type="noteStyleTextarea"]')) {
                                                        selected_ele_setOBJ.find('.content-editable-field_' + doi).attr("contenteditable", false);
                                                    } else if (selected_ele_setOBJ.is('[data-type="pickList"]')) {
                                                        selected_ele_setOBJ.find('.pickListButton[picklist-button-id="picklist_button_' + doi + '"]').attr("disabled", true);
                                                        disable_formula_data[ctr_in]["formula_source_field"].attr("disabled", true);
                                                        //ok
                                                    } else {

                                                        disable_formula_data[ctr_in]["formula_source_field"].attr("disabled", true);
                                                    }
                                                } else if (disabled_flagging == false) {
                                                    if (selected_ele_setOBJ.is('[data-type="attachment_on_request"]')) {
                                                        selected_ele_setOBJ.find('.file_attachement[data-action-id="' + doi + '"][data-action-type="attachmentForm"]').attr("disabled", false);
                                                    } else if (selected_ele_setOBJ.is('[data-type="multiple_attachment_on_request"]')) {
                                                        selected_ele_setOBJ.find('.multiple_file_attachement[data-action-id="' + doi + '"][data-action-type="multiple_attachmentForm"]').attr("disabled", false);
                                                    } else if (selected_ele_setOBJ.is('[data-type="noteStyleTextarea"]')) {
                                                        selected_ele_setOBJ.find('.content-editable-field_' + doi).attr("contenteditable", true);
                                                    } else if (selected_ele_setOBJ.is('[data-type="pickList"]')) {
                                                        selected_ele_setOBJ.find('.pickListButton[picklist-button-id="picklist_button_' + doi + '"]').attr("disabled", false);
                                                        disable_formula_data[ctr_in]["formula_source_field"].attr("disabled", false);
                                                    } else {
                                                        disable_formula_data[ctr_in]["formula_source_field"].attr("disabled", false);
                                                    }
                                                } else {
                                                    disable_formula_data[ctr_in]["formula_source_field"].attr("disabled", false);
                                                }
                                            }
                                            // console.log("formulaDoc.getEvaluation()",formulaDoc.getEvaluation())
                                        });
                                    }
                                }
                            }
                        }
                    }
                    if (formula_has_fieldname == false) {
                        var formulaDoc = new Formula(disable_formula);
                        var parsed_value = formulaDoc.getEvaluation();
                        if ($.type(parsed_value) === "boolean") {
                            if (parsed_value == true) {
                                if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="pickList"]')) {
                                    obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').attr("disabled", true);

                                } else {
                                    obj_fields.attr("disabled", true);
                                }
                            } else if (parsed_value == false) {
                                if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="pickList"]')) {
                                    obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').attr("disabled", false);
                                } else {
                                    obj_fields.attr("disabled", false);
                                }
                            }
                        } else {
                            if (obj_fields.parents('.setOBJ[data-object-id]').eq(0).is('[data-type="pickList"]')) {
                                obj_fields.parent().find('.getFields_' + doi + ',[picklist-button-id="picklist_button_' + doi + '"]').attr("disabled", false);
                            } else {
                                obj_fields.attr("disabled", false);
                            }
                        }

                    }

                }
            });

            $('.getFields').filter(function (index, val_dis) {
                if ($(val_dis).data('events')) {
                    if ($(val_dis).data('events')['change']) {
                        return $(this).data('events')['change'].filter(function (val, index_) {
                            return val['namespace'].indexOf('disabledFormulaControl') >= 0;
                        }).length >= 1;
                    }
                }
                return false;
            }).trigger('change');
        },
        fieldRequired: function () {
            var fields = ""
            var requestId = $("#ID").val();
            var requestor_id = $("#Requestor").val();
            var currentUser = $("#CurrentUser").val();
            var status = $('#Status').val();
            //if (requestor_id == currentUser || requestId == 0) {
            //    fields = $(".fieldRequired_default").text()

            //} else {

            if (status == 'DRAFT') {
                fields = $(".fieldRequired_default").text();
            } else {
                fields = $(".fieldRequired").text();
            }
            //}

            fields = (fields == "null" ? "" : fields);
            if (fields) {
                fields = JSON.parse(fields);
                $.each(fields, function (key, value) {
                    var the_field = $('[name="' + value + '"]');
                    if (the_field.length <= 0) {
                        the_field = $('[name="' + value + '[]"]');
                    }
                    the_field.attr("required", "required");
                    parentObj = the_field.closest(".setOBJ");

                    uniqueID = parentObj.attr("data-object-id");
                    label = parentObj.find("#lbl_" + uniqueID);
                    field_container = parentObj.find("#obj_fields_" + uniqueID);
                    labelText = label.text();
                    labelText = labelText.trim();
                    labelContainer = label.parents('#label_' + uniqueID).eq(0);
                    if (labelContainer.css('display') != "none") {
                        label.html('<font color="red" style="position:absolute;z-index:1;margin-left: -10px;"> *</font>' + labelText);
                        // field_container.prepend('<font color="red" style="position:absolute;z-index:1;margin-left: -10px;"> *</font>');
                    } else {
                        field_container.prepend('<font color="red" style="position:absolute;z-index:1;margin-left: -10px;"> *</font>');
                    }
                })
            }
        },
        fieldHiddenValues: function () {
            var fields = ""
            var requestId = $("#ID").val();
            var requestor_id = $("#Requestor").val();
            var currentUser = $("#CurrentUser").val();
            var status = $('#Status').val();
            //if (requestor_id == currentUser || requestId == 0) {
            //    fields = $(".fieldHiddenValues_default").text()

            //} else {
            // fields = $(".fieldHiddenValues").text()
            //}

            if (status == 'DRAFT') {
                fields = $(".fieldHiddenValues_default").text();
            } else {
                fields = $(".fieldHiddenValues").text();
            }

            fields = (fields == "null" ? "" : fields);
            if (fields) {
                fields = JSON.parse(fields);
                $.each(fields, function (key, value) {
                    if (
                            $('[name="' + value + '[]"]').attr("type") == "checkbox" ||
                            (
                                    $("[name='" + value + "[]']").prop("tagName") == "SELECT" &&
                                    $("[name='" + value + "[]']").attr("multiple") == "multiple"
                                    )
                            )
                    {
                        $('[name="' + value + '[]"]').closest(".setOBJ").addClass("display2");
                    } else {
                        $('[name="' + value + '"]').closest(".setOBJ").addClass("display2");
                    }
                })
            }
        },
        setRepeaterContent: function () {
            var content = JSON.parse($('#Repeater_Data').val());

            console.log(content);
            for (var index = 0; index <= rowCount - 1; index++) {
                var fieldName = content[index].FieldName;
                var fieldContainer = $('[name="' + fieldName + '"]').closest('.input_position_below');

                var containerPanel = $(fieldContainer).closest('.setOBJ');
                var buttonContainer = $(containerPanel).closest('tr');

                $(buttonContainer).find('.rep-add-spec-row').each(function () {
                    if (index != 0) {
                        $(this).click();
                    }
                });
            }

            $('[repeater-table="true"]').find('tr').each(function (row) {
                $(this).find('.getFields ').each(function (column) {
                    var value = JSON.parse(content[column].Values);

                    switch ($(this).attr('type')) {
                        case "checkbox":
                            if ($(this).val() === value[row]) {
                                $(this).prop('checked', true);
                            } else {
                                $(this).prop('checked', false);
                            }
                            break;
                        case "radio":
                            break;
                        default :
                            break;
                    }

                    //$(this).val(value[row]);
                });
            });
        },
        getRepeaterContent: function () {
            var repeaters = [];
            $('[repeater-table="true"]').each(function () {
                $(this).find('tr:first').each(function () {
                    $(this).find('.getFields').each(function () {
                        var thisName = $(this).attr('name');
                        var data = [];
                        var fields = [];

                        $('[rep-original-name="' + thisName + '"], [name="' + thisName + '"]').each(function () {
                            var fieldType = $(this).attr('type');
                            var copyName = $(this).attr('name');
                            if (fields.indexOf(copyName) === -1) {
                                switch (fieldType) {
                                    case "checkbox":
                                        var addedValue = [];
                                        $('[name="' + copyName + '"]').each(function () {
                                            if ($(this).prop('checked')) {
                                                addedValue.push($(this).val());
                                            }
                                        });
                                        break;
                                    case "radio":
                                        var addedValue = [];
                                        $('[name="' + copyName + '"]').each(function () {
                                            if ($(this).prop('checked')) {
                                                addedValue.push($(this).val());
                                            }
                                        });
                                        break;
                                    default :
                                        var addedValue = $(this).val();
                                        break;
                                }

                                var valueGenerated = JSON.stringify(addedValue);

                                fields.push(copyName);
                                data.push(valueGenerated);
                            }
                        });

                        repeaters.push({
                            FieldName: thisName,
                            Data: data
                        });
                    });
                });
            });

            console.log(repeaters);
            $('#Repeater_Data').val(JSON.stringify(repeaters));
        },
        // Print actual Form
        printForm: function () {
            var self = this;
            $("body").on("click", ".printForm", function () {
                var pathname = window.location;
                var request_ID = $.trim($("#ID").val());
                var trackNo = $.trim($("#TrackNo").val())
                var printID = $("#print-form-id").val()//getParametersName("printID", pathname);
                var print_form = getParametersName("print_form", pathname);
                var form_id = $('#FormID').val();
                // if (request_ID == "0" || request_ID == "") {
                //     self.print_content("", "");
                //     return false;
                // }
                $(".getFields").each(function () {
                    if ($(this).attr("type") == "radio") {
                        if ($(this).is(":checked")) {
                            $(this).attr("checked", "checked");
                        }
                    }
                })

                if (getParametersName("view_type", pathname) == "preview") {
                    self.print_content("", "");
                } else {
                    var ret = '<h3 class="fl-margin-bottom"><i class="icon-print fa fa-print"></i> Print settings</h3>';
                    ret += '<div class="hr"></div>';
                    ret += '</div>';
                    ret += '<div id="comment-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 180px;overflow: hidden;" class="ps-container">'
                    ret += '<div class="content-dialog sub-container-comment requestComment" style="width: 95%;height: auto;padding-top: 1px;">';
                    if (print_form == undefined) {
                        ret += '<div class="fields">';
                        ret += '<div class="label_below2">Print Type: </div>';
                        ret += '<div class="input_position">';
                        ret += '<select class="input-select" id="print-type" style="width:100%">';
                        ret += '<option value="1">Default Form</option>';
                        if (printID != 0 && printID) {
                            ret += '<option value="2">Customized Form</option>';
                        }
                        ret += '<option value="3">Page</option>';
                        ret += '</select>';
                        ret += '</div>';
                        ret += '</div>';
                    }
                    ret += '<div class="fields print-setting-inc-container">';
                    ret += '<div class="label_below2">Choose data you want to include in your printing.</div>';
                    ret += '<div class="input_position">';
                    ret += "<label style=''><input type='checkbox' class='print-setting-inc css-checkbox' value='comment' id='print-setting-inc_comment'><label for='print-setting-inc_comment' class='css-label'></label> Comments</label><br /><br />";
                    ret += "<label style=''><input type='checkbox' class='print-setting-inc css-checkbox' value='audit-log' id='print-setting-inc_audit-logs'><label for='print-setting-inc_audit-logs' class='css-label'></label> Audit Logs</label>";
                    ret += '</div>';
                    ret += '</div>';
                    ret += '</div>';
                    ret += '<div class="fields">';
                    ret += '<div class="label_basic"></div>';
                    ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                    ret += ' <input type="button" class="btn-blueBtn fl-margin-right fl-positive-btn" id="print-setup" value="Print" style="margin-left:5px; margin-bottom:5px;">';
                    ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
                    ret += '</div>';
                    ret += '</div>';
                    var newDialog = new jDialog(ret, "", "", "500", "", function () {

                    });
                    newDialog.themeDialog("modal2");
                    $("#print-setup").click(function () {

                        var print_type = $("#print-type").val();
                        var arrChecked = array_store(".print-setting-inc", "checkbox");
                        if (print_type == "1" || print_form == "true") {
                            ui.block();

                            self.getLogs(function (logs) {
                                self.getComments(function (comments) {
                                    if (arrChecked.indexOf("comment") == -1) {
                                        comments = "";
                                    }
                                    if (arrChecked.indexOf("audit-log") == -1) {
                                        logs = "";
                                    }
                                    self.print_content(logs, comments);

                                })
                            })
                        } else if (print_type == "2") {
                            var print_url = "/user_view/workspace?view_type=update&ID=" + printID + "&formID=" + form_id + "&requestID=" + request_ID + "&trackNo=" + trackNo + "&embed_type=viewEmbedOnly&print_form=true&fromPrintButton=true&comment=" + arrChecked.indexOf("comment") + "&log=" + arrChecked.indexOf("audit-log");
                            window.open(print_url)
                        } else if (print_type == "3") {
                            $(".fl-closeDialog").trigger("click");
                            window.print();
                        }
                    })

                    $("#print-type").change(function () {
                        if ($(this).val() == "3" || $(this).val() == "2") {
                            $(".print-setting-inc-container").hide();
                        } else {
                            $(".print-setting-inc-container").show();
                        }
                    })
                }

                // $(".getFields").each(function() {
                //     if ($(this).attr("type") == "radio") {
                //         if ($(this).is(":checked")) {
                //             $(this).attr("checked", "checked");
                //         }
                //     }
                // })
                // var pathname = window.location;
                // if (getParametersName("view_type", pathname) == "preview") {
                //     self.print_content("", "");
                // } else {
                //     self.getLogs(function(logs) {
                //         self.getComments(function(comments) {
                //             self.print_content(logs, comments);
                //         })
                //     })
                // }

            });
        },
        expand_table: function () {
            var dis_body = $(this);
            var rep_tables_ele = dis_body.find('[repeater-table="true"]');
            var repeater_table_datas = rep_tables_ele.map(function () {
                var self_setOBJ = $(this).parents('.setOBJ[data-type="table"]').eq(0);
                var rep_ele_setOBJ_origin_height = self_setOBJ.outerHeight();
                var rep_ele_setOBJ_origin_width = self_setOBJ.outerWidth();
                var rep_ele_setOBJ_origin_top = self_setOBJ.position().top;
                var rep_ele_setOBJ_origin_left = self_setOBJ.position().left;

                var set_OBJ_below_OBJ_datas = self_setOBJ.parent().children(".setOBJ").map(function (a, b) {
                    var self_setOBJ2 = $(b);
                    var rep_ele_bottom = rep_ele_setOBJ_origin_top + rep_ele_setOBJ_origin_height - 50;
                    var ele_setOBJ_origin_height = self_setOBJ2.outerHeight();
                    var ele_setOBJ_origin_width = self_setOBJ2.outerWidth();
                    var ele_setOBJ_origin_top = self_setOBJ2.position().top;
                    var ele_setOBJ_origin_left = self_setOBJ2.position().left;
                    if (self_setOBJ[0] != self_setOBJ2[0]) {
                        if (rep_ele_bottom < ele_setOBJ_origin_top) {
                            return {
                                "ele_setOBJ": self_setOBJ2,
                                "ele_setOBJ_origin_height": ele_setOBJ_origin_height,
                                "ele_setOBJ_origin_width": ele_setOBJ_origin_width,
                                "ele_setOBJ_origin_top": ele_setOBJ_origin_top,
                                "ele_setOBJ_origin_left": ele_setOBJ_origin_left,
                            };
                        }
                    }
                    return null;
                }).get();
                return {
                    "rep_ele_setOBJ": self_setOBJ,
                    "rep_ele_setOBJ_origin_height": rep_ele_setOBJ_origin_height,
                    "rep_ele_setOBJ_origin_width": rep_ele_setOBJ_origin_width,
                    "rep_ele_setOBJ_origin_top": rep_ele_setOBJ_origin_top,
                    "rep_ele_setOBJ_origin_left": rep_ele_setOBJ_origin_left,
                    "set_OBJ_below_OBJ_datas": set_OBJ_below_OBJ_datas
                };
            }).get();

            var rep_table_height_changes = [];
            for (var level1 in repeater_table_datas) { //rep_tables
                $(repeater_table_datas[level1]["rep_ele_setOBJ"]).css("display", "inline-table");
                rep_table_height_changes.push($(repeater_table_datas[level1]["rep_ele_setOBJ"]).outerHeight() - repeater_table_datas[level1]["rep_ele_setOBJ_origin_height"]);
                for (var level2 in repeater_table_datas[level1]["set_OBJ_below_OBJ_datas"]) {//setOBJ below
                    //repeater_table_datas[level1]["set_OBJ_below_OBJ_datas"][level2][""]
                    $(repeater_table_datas[level1]["set_OBJ_below_OBJ_datas"][level2]["ele_setOBJ"]).css("top", ($(repeater_table_datas[level1]["set_OBJ_below_OBJ_datas"][level2]["ele_setOBJ"]).position().top + rep_table_height_changes[rep_table_height_changes.length - 1]) + "px")
                }
            }
            setTimeout(function () {
                for (var vctr in rep_table_height_changes) {
                    dis_body.find('.loaded_form_content').css("height", (dis_body.find('.loaded_form_content').outerHeight() + rep_table_height_changes[vctr]) + "px");
                }
            }, 0);

        },
        print_content: function (logs, comments) {



            var self = this;
            var ret = ""
            var widthForm = $(".preview_content").width();
            var heightForm = $(".preview_content").parent().height();
            var formName = $("#FormName").val();
            ret += '<link rel="stylesheet" type="text/css" href="/css/style.css" />';
            ret += '<link rel="stylesheet" type="text/css" href="/css/font-awesome.css" />';
            ret += '<link rel="stylesheet" type="text/css" href="/css/reset.css" />';
            ret += '<link rel="stylesheet" href="/css/jquery-ui.css" />';
            ret += '<link rel="stylesheet" href="/js/functions/formbuilder.css" />';
            ret += '<link rel="stylesheet" href="/css/global_css.css" />';


            var mywindow = window.open('', formName, 'height=' + heightForm + ',width=' + widthForm);
            mywindow.document.write('<html><head><title>' + formName + '</title>');
            $("link, style").each(function () {
                $(mywindow.document.head).append($(this).clone())
            });

            console.log(mywindow.document);





            mywindow.document.write('</head><body>');
            mywindow.document.write('<div class="submit-overlay" style="z-index:1000000000;background-repeat:no-repeat;background-attachment:fixed;background-color:rgba(200,200,200,0.5);background-position:center;position:fixed;width:100%;height:100%;text-align:center;vertical-align:middle;"><div class="spinner load_m" style="position: absolute; top: 50%; left: 50%;"> <div class="bar1"></div> <div class="bar2"></div> <div class="bar3"></div> <div class="bar4"></div> <div class="bar5"></div> <div class="bar6"></div> <div class="bar7"></div> <div class="bar8"></div> <div class="bar9"></div> <div class="bar10"></div> </div></div>');
            mywindow.document.write('<div id="previewToPrint" style="height:auto">');
            mywindow.document.write('</div>');

            $(".preview_content").find('.getFields[name]').each(function () {

                try {
                    if ($(this).val().length >= 1) {
                        //console.log('valuuuueee',$(this).val())
                        $(this).attr('data-print-value', $(this).val());
                        if ($(this).is("input[type='text']")) {
                            console.log($(this))
                            $(this).attr('value', $(this).attr('value') || $(this).val());
                        }
                    }
                    return true;
                } catch (err) {

                }

            });

            // mywindow.document.write($(".preview_content").clone(true)[0].outerHTML);
            var htmlClone = $(".preview_content").html();
            // $(htmlClone).appendTo(mywindow.document.getElementById("previewToPrint"));

            $(mywindow.document.getElementById("previewToPrint")).append(htmlClone)

            var selectIds = "";
            var select = "";
            var val = "";



            $(mywindow.document.getElementById("previewToPrint")).find("select").each(function () {
                if ($(this).hasClass("getFields")) {
                    selectIds = this.id;
                    val = $(this).attr('data-print-value');

                    select = $(this).clone(true);//.val(val);\
                    if (select.is('[multiple="multiple"]')) {
                        var eleSelected = select.children('option').filter(function () {
                            if (val) {
                                return val.indexOf($(this).attr('value')) > -1;
                            }
                        })


                        eleSelected.prop('selected', true);
                    }

                    $(mywindow.document.getElementById(selectIds)).parent().html(select);
                }
            });

            $(mywindow.document.getElementById("previewToPrint")).find("textarea").each(function () {
                if ($(this).hasClass("getFields")) {
                    selectIds = this.id;
                    val = $(this).attr('data-print-value');

                    $(mywindow.document.getElementById(selectIds)).text(val);
                }
            });
            if (logs != "") {
                // mywindow.document.write();
                $(mywindow.document).find(".loaded_form_content").after("<div class='hr'></div>" + logs);
            }
            if (comments != "") {
                // mywindow.document.write("<div class='hr'></div>");
                // mywindow.document.write(comments);
                $(mywindow.document).find(".loaded_form_content").after("<div class='hr'></div>" + comments);
            }

            $(mywindow.document).find(".loaded_form_content").css("top", "0px")

            mywindow.document.write('</body></html>');

            var dis_body = $("body", mywindow.document);

            self.expand_table.call(dis_body);


            var tbWrapper = mywindow.document.getElementsByClassName("table-wrapper");
            var parentTbWrapper = "";
            for (var i in tbWrapper) {
                if (tbWrapper[i].style) {
                    tbWrapper[i].style.display = "block";
                }
                parentTbWrapper = tbWrapper[i].parentNode;
                if (parentTbWrapper) {
                    parentTbWrapper.style.overflow = "";
                }

            }
            var setObj = $(mywindow.document.getElementsByClassName("setOBJ")).not('[data-type="embeded-view"]').get();
            for (var i in setObj) {
                if (setObj[i].style) {
                    if (setObj[i].style.display != "none") {
                        setObj[i].style.display = "inline-table";
                    }
                }
            }
            var fields_below = mywindow.document.getElementsByClassName("fields_below");
            for (var i in fields_below) {
                if (fields_below[i].style) {
                    if (fields_below[i].style.display == "table-cell") {
                        fields_below[i].style.display = "block";
                    }
                }
            }

            var notestyle_border_upper_right = mywindow.document.getElementsByClassName("cefb-corner-top-left");
            for (var i in notestyle_border_upper_right) {
                if (notestyle_border_upper_right[i].style) {
                    notestyle_border_upper_right[i].style.display = "none";
                }
            }

            var notestyle_border_lower_left = mywindow.document.getElementsByClassName("cefb-corner-bottom-right");
            for (var i in notestyle_border_lower_left) {
                if (notestyle_border_lower_left[i].style) {
                    notestyle_border_lower_left[i].style.display = "none";
                }
            }

            var display_form_status = mywindow.document.getElementsByClassName("display_form_status")
            if (display_form_status[0]) {
                display_form_status[0].style.display = "none";
            }

            //picklist problems FS#8740 fixes by michael
            (function () { // setting up jquery for mywindow ... // mywindow is a closure data for protection inside this anonymous function
                mywindow.jQuery = function (selector, root_contex) { // adding jQuery to mywindow and changing the root element of mywindow.jQuery
                    return jQuery(selector, root_contex || mywindow.document); // the second parameter is the context or the root 
                }
                mywindow.$ = mywindow.jQuery; // passing it to mywindow.$
            })();

            mywindow.$('.picklist-clear-value').remove(); //FS#8740 removing picklist clear value
            {//old version
                // var asyncCallback = function(){
                //     setTimeout(function () {
                //         $(mywindow.document).find(".submit-overlay").remove();
                //         mywindow.print();
                //         mywindow.close();
                //         ui.unblock();
                //     }, 3000);
                // };
                // setTimeout(function(){
                //     PrintContentEmbedView(mywindow.document);
                //     asyncCallback();
                // },0);
            }

            window.asyncCallback = function(){
                setTimeout(function () {
                    $(mywindow.document).find(".submit-overlay").remove();
                    mywindow.print();
                    mywindow.close();
                    ui.unblock();
                }, 3000);
            };
            setTimeout(function(){
                PrintContentEmbedView(mywindow.document);
                // asyncCallback();
                mywindow.$('head').append('<style>'+$('.object_css').html()+'</style>');
                mywindow.$('html').attr('print-form', 'true' );
                mywindow.$('html').attr('print-form-id', $('html').attr('print-form-id') );
                mywindow.$('html').append('<script>window.asyncCallback();</script>');
            },0);



        },
        getLogs: function (callback) {
            var request_id = $('#ID').val();
            var form_id = $('#FormID').val();
            //view of logs



            $.post('/ajax/getRequestLogs', {
                formId: form_id,
                request_Id: request_id
            }, function (data) {
                var result = JSON.parse(data);
                var ret = '<div><h3 class="fl-margin-bottom"><i class="icon-book fa fa-book"></i> Audit Logs</h3>';
                ret += '<div class="hr"></div>';
                ret += '<div id="logs-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: auto;" class="ps-container">'
                ret += '<div class="content-dialog sub-container-logs" style="width: 95%;">'
                for (var index in result) {
                    ret += '<div class="reply fl-padding-bottom " id="reply_80" style="width:100%;">';
                    // audit_table += '<a class="message-img" href="#">' + result[index].created_by.image + '</a>';
                    ret += '<div class="message-body" style="margin-left:0px;">';
                    ret += '<div class="text" id="postText_80" style="width: 94.4%; border: none!important;"><p>' + result[index].details + '</p></div>';
                    ret += '<p class="attribution">by <a href="#non">' + result[index].created_by_display_name + '</a> at <label class="timeago" title="' + result[index].date_created + '">' + result[index].date_created + '</label>';
                    ret += '</p></div>';
                    ret += '</div>';
                }
                ret += '</div>';
                ret += '</div>';
                ret += '</div>';
                callback(ret);
                //$("label.timeago").timeago();
            });
        },
        getComments: function (callback) {
            //view of comment
            $(".load-mr").show();
            var reqID = $("#ID").val();
            var formID = $("#FormID").val();
            $.ajax({
                type: "POST",
                url: "/ajax/post",
                dataType: "json",
                cache: false,
                data: "action=getComment" + "&postID=" + reqID + "&formID=" + formID,
                success: function (result) {
                    var ret = '';
                    if (result) {
                        ret = '<div><h3><i class="icon-comment"></i> Comments / Suggestion</h3>';
                        ret += '<div class="hr"></div>';
                        ret += '<div id="comment-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: auto;" class="ps-container">'
                        ret += '<div class="content-dialog sub-container-comment requestComment" style="width: 95%;height: auto;">';
                        for (var a = 0; a < result.length; a++) {
                            var postDetails = "print";
                            var uploadedDetails = null;
                            var likeDetails = null;
                            var comments = result[a];
                            ret += replyPost(postDetails, uploadedDetails, likeDetails, comments);
                        }
                        ret += '</div>';
                        ret += '</div>';
                        ret += '</div>';
                    }
                    callback(ret)
                }
            });
        },
        /* Remove Files attached on the request
         ================================================== */
        removeFiles: function () {
            $("body").on("click", ".removeFiles", function () {
                var self = $(this);
                var filename = $(this).attr("data-file");
                var file_location = $(this).attr("data-location");
                con = "Are you sure you want to delete this?";
                var newConfirm = new jConfirm(con, 'Confirmation Dialog', '', '', '', function (r) {
                    if (r == true) {
                        $.post("/ajax/files", {action: "deleteFiles", filename: filename, file_location: file_location}, function (e) {

                            if (e == "File was successfully deleted.") {
                                self.parent().parent().parent().remove();
                            }
                        });
                    }
                });
                newConfirm.themeConfirm("confirm2", {'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});
            });
        },
        addViewer: function () {
            var requestId = $("#ID").val();
            var form_id = $('#FormID').val();
            var self = this;
            $("body").on("click", ".addViewer", function () {
                var ele = this;
                //add viwer
                var ret = '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-user"></i> Add Viewer</h3>';
                ret += '<div class="hr"></div>';
                ret += '<div style="margin-bottom: 10px; margin-top:10px;padding:5px;/* font-style: italic; */font-size: 11px;"><span style="';
                ret += 'color: red;';
                ret += 'font-size: 13px;';
                ret += '">Note:</span> Only the users in the category where this form belongs to, are allowed to tag. Click <a id="seeAllUsers_AddViewers" style="cursor:pointer">SEE ALL</a> for allowed users to tag.</div>';
                ret += '</div>';
                ret += '<div id="add-viewer-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">'
                ret += '<div class="content-dialog sub-container-add-viewer" style="width: 95%;height: auto;">'
                // ret += '<center><img src="/images/loader/loader.gif" class="load-mr" style="display: inline;margin-top:20%;"></center>'
                //ret += '<textarea class="mention display" id="mention-name" placeholder="" data-mentions-input="true" style="height: 42px;background-transparent"></textarea>';
                ret += '<div id="userList" style="float: left;border: 1px solid #ddd;min-height: 40px;margin-top: 5px;width: 100%;padding-bottom: 8px;">' +
                        '<input type="text" id="request_viewers" class="input-small msgBoxTo" data-tag-id="0" style="border: none;box-shadow:none; background-color:#fafafa">' +
                        '</div>' +
                        '<div style="float: left;width: 100.2%;">' +
                        '<div id="request_viewers_users"></div>' +
                        '</div>';
                ret += '</div>';
                ret += '</div>';
                ret += '<div class="fields">';
                ret += '<div class="label_basic"></div>';
                ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                ret += ' <input type="button" class="btn-basicBtn fl-margin-right" id="popup_cancel" value="Close" style="margin-bottom:5px;">';
                ret += ' <input type="button" class="btn-basicBtn fl-buttonEffect saveViewer fl-margin-right" value="Save" style="margin-bottom:5px;">';
                ret += '</div>';
                ret += '</div>';
                var newDialog = new jDialog(ret, "", "", "", "70", function () {
                });
                newDialog.themeDialog("modal2");
                // $(".sub-container-add-viewer").perfectScrollbar();

                //         $.post("/ajax/request_viewer",{action:"getUsers"},function(data){
                //             data = JSON.parse(data);
                // $(".sub-container-add-viewer").html('<textarea class="mention" id="mention-name" placeholder="" data-mentions-input="true" style="height: 42px;background-transparent"></textarea>')
                //             $("#mention-name").mention_only({
                //                 json : data

                //             });
                //         })
                if (requestId != 0) {
                    var returnViewer = "";

                    $.post("/ajax/request_viewer", {action: "getRequestViewer", request_id: requestId, form_id: form_id}, function (data) {
                        data = JSON.parse(data);
                        // if(ele.hasAttribute("hasAutoComplete")==false){
                        $("body").off("click.adduser_autocomplete");
                        $("#request_viewers").autoComplete({
                            table_search: "tbuser",
                            search_type: "users",
                            event: "#request_viewers",
                            display: "#request_viewers_users",
                            allow_select: true,
                            allow_delete: true
                        });
                        // $(ele).attr("hasAutoComplete","true");
                        // }


                        // for (var i in data) {
                        //     returnViewer += "<div class='mentionSelected' data-id=" + data[i]['id'] + "><i class='removeTagg_i icon-remove-sign' style='margin-right: 5px;cursor: pointer;'></i>" + data[i]['name'] + "</div>";
                        // }
                        // $(".sub-container-add-viewer").html('<textarea class="mention" id="mention-name" placeholder="" data-mentions-input="true" style="height: 42px;background-transparent;border: transparent !important;"></textarea>')
                        // $("#mention-name").mention_only({
                        //     json: data_name

                        // });
                        var viewersID = "";
                        var viewers = "";
                        var ctr = 0;
                        for (var i in data) {
                            name = data[i]['name'];
                            viewers += Groups.groupsMembers(data[i]['id'], name, "request_viewers");
                            viewersID += data[i]['id'] + ",";
                            ctr++;
                        }
                        $("#request_viewers").before(viewers)
                        if (ctr > 0) {
                            $("#request_viewers").attr("data-tag-id", viewersID.substr(0, viewersID.length - 1))
                        } else {
                            $("#request_viewers").attr("data-tag-id", "0")
                        }
                        $("body").on("click", ".removeTag", function () {
                            $(this).closest(".recipient").remove();
                        })
                        // $(".mention").before(returnViewer);
                    })
                }
                self.saveViewer();
            })
        },
        seeAllUsers_AddViewers: function () {
            var form_id = $('#FormID').val();
            $("body").on("click", "#seeAllUsers_AddViewers", function () {
                var mywindow = window.open("/ajax/request_viewer?seeAllUsersAddViewers=1&form_id=" + form_id + "", "", "width=350,height=500");
                // console.log($(mywindow.document.body).html());
                // $(mywindow.document.head).append('<link rel="stylesheet" type="text/css" href="/css/mention.css">')
            })
        },
        saveViewer: function () {
            var array_search = new Array();
            var requestId = $("#ID").val();
            var form_id = $('#FormID').val();
            $(".saveViewer").click(function () {

                $(".request_viewers").each(function () {
                    array_search.push($(this).attr("data-id"));
                })
                // if (array_search.length == 0) {
                //     showNotification({
                //         message: "Please enter alteast 1 user",
                //         type: "error",
                //         autoClose: true,
                //         duration: 3
                //     });
                //     ui.unblock()
                //     return;
                // }
                if (requestId != 0) {
                    //save to database
                    ui.block()
                    $.post("/ajax/request_viewer", {action: "saveViewer", array_search: array_search, requestId: requestId, form_id: form_id}, function (data) {
                        ui.unblock()
                        showNotification({
                            message: "Viewer Saved",
                            type: "success",
                            autoClose: true,
                            duration: 3
                        });
                        $("#popup_cancel").click();
                        array_search = [];
                    })
                } else {

                }
            })
        },
        getUserViewer: function () {
            var requestId = $("#ID").val();
            var form_id = $('#FormID').val();
            var self = this;
            $.post("/ajax/request_viewer", {action: "getTaggedUsers", requestId: requestId, form_id: form_id}, function (data) {
                if (data == 1) {
                    // $(".viewer_action").append('<button class="btn-basicBtn padding_5 cursor tip removetagRequest" data-original-title="Remove Tag"><i class="icon-power-off"></i></button>')
                    //$(".viewer_action").append('<div class="fl-createViewreq fl-buttonEffect btn-basicBtn padding_5 cursor tip removetagRequest" data-original-title="Remove Tag" data-placement="bottom"><i class="fa fa-power-off"></i></div>');

                    $('<li class="isCursorPointer btn-basicBtn fl-buttonEffect tip" data-original-title="Remove Viewers" style="position:relative; z-index:2;"><a class="removetagRequest"><i class="fa fa-users"></i></a></li>').insertAfter($('.fl-worspace-headerv2 #fl-form-basic-options ul li.showReply'));

                    $(".tip").tooltip();
                    self.removetagRequest();
                }
            })
        },
        removetagRequest: function () {
            var requestId = $("#ID").val();
            var form_id = $('#FormID').val();
            var self = this;
            $(".removetagRequest").click(function () {
                var newConf = new jConfirm('Are you sure you want to remove Tag?', '', '', '', '', function (r) {
                    if (r) {
                        // if ($('body').children(".submit-overlay").length == 0) {
                        //     var submit_overlay = $('<div class="submit-overlay" style="background:url(/images/loader/loader.gif);z-index: 100000;background-repeat:no-repeat;background-attachment:fixed;background-color:rgba(200,200,200,0.5);background-position:center;position:fixed;width:100%;height:100%;text-align:center;vertical-align:middle;">Loading...</div>');
                        //     $('body').prepend(submit_overlay);
                        // }
                        ui.block();
                        $.post("/ajax/request_viewer", {action: "removetagRequest", requestId: requestId, form_id: form_id}, function (data) {
                            showNotification({
                                message: "Tagged Removed",
                                type: "success",
                                autoClose: true,
                                duration: 3
                            });
                            ui.unblock()
                            $("#popup_cancel").click();
                            // $(".removetagRequest").remove();
                            // $(".tooltip").remove();
                            // Gi and Forma
                            var gi = $("gi").val();

                            if (gi != "1-1") {
                                var load = "/user_view/home";
                            } else {
                                var load = "/gi-dashboard-home";
                            }
                            if (data == 0) {
                                setTimeout(function () {
                                    window.location = load;
                                }, 2000)
                            }
                        })
                    }
                });
                newConf.themeConfirm("confirm2", {'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'});
            })
        },
        getButtons: function (hasEditAccess) {
            var buttons = ($('#LastAction').val() === undefined || $('#LastAction').val() === '') ? "" : JSON.parse($('#LastAction').val());
            var workflowId = $('#WorkflowId').val();
            var currentUser = $("#CurrentUser").val();
            var processor = $("#Processor").val();
            var requestId = $("#ID").val();
            var requestor_id = $("#Requestor").val();
            var wButtons = $('.workflow_buttons');
            var location = window.location.pathname;
            //$('#actionRequestButton').html('<option value="0">-------------Select Button-------------</option>');
            $(wButtons).html('');
            if (!processor && ($("#delete_access_forma").val() == "" || $("#enable_deletion_forma").val() == "0")) {
                $(wButtons).append('<li class="fl-action-close" value="Close" data-workflow-id="0" action="0" message="" message_success = ""><a href="#" data-original-title="" title="">Close</a></li>');
                return;
            }

            var isSaveHidden = $('#SaveFormula').val();
            var isCancelHidden = $('#CancelFormula').val();
            processor = processor.split(",");
            processor = processor.filter(function (e) {
                return e
            });

            //modified by joshua reyes 03/07/2016 [For Button Properties]
            var form_id = $('#FormID').val();
            var require_comment = "";
            $.post("/ajax/request_viewer",
                    {
                        action: "getButtonProperties",
                        "form_id": form_id,
                        "button_last_action": buttons
                    },
            function (data) {
                var parse_json = JSON.parse(data);
                var button_validation_properties = parse_json['validate'];
                var button_last_action = parse_json['button_last_action'];

                if (processor.indexOf(currentUser) >= 0 && hasEditAccess) {
                    //$('#actionRequestButton').append('<option value="' + buttons[index].button_name + '" data-workflow-id="' + workflowId + '" action="' + buttons[index].child_id + '">' + buttons[index].button_name + '</option>');
                    //<ul id="fl-action-option" style="z-index:1;"></ul>

                    //========================================================================================================
                    if (button_validation_properties == "not_empty") { //Kapag recorded na yung button name sa tb_button
                        var button_properties = parse_json['button_properties'];
                        $.each(button_properties, function (id, val) {
                            //require_comment = buttons[index].require_comment;
                            require_comment = buttons[val.button_name].require_comment;

                            console.log('buttons[val.button_name]', buttons[val.button_name]);
                            var buttonFieldRequired = buttons[val.button_name].fieldRequired;
                            if (require_comment == true) {
                                require_comment = 1;
                            }
                            var parsed_formula = new Formula(val.visibility_formula);
                            parsed_formula.getProcessedFormula();
                            if (val.visibility_formula != "") { //Kapag hindi empty ung Formula Visibility then Parse the Formula
                                if (parsed_formula.getEvaluation()) {
                                    //$(wButtons).append('<li class="fl-action-submit" value="' + buttons[index].button_name + '" data-workflow-id="' + workflowId + '" action="' + buttons[index].child_id + '" message="' + buttons[index].message + '" message_success = "' + buttons[index].message_success + '" require_comment="' + require_comment + '" landing_page="'+ buttons[index].wf_landing_page +'"><a href="#" data-original-title="" title="">' + buttons[index].button_name + '</a></li>');
                                    $(wButtons).append('<li class="fl-action-submit" value="' + buttons[val.button_name].button_name + '" data-workflow-id="' + workflowId + '" action="' + buttons[val.button_name].child_id + '" message="' + buttons[val.button_name].message + '" message_success = "' + buttons[val.button_name].message_success + '" field_required="' + buttonFieldRequired + '" require_comment="' + require_comment + '" landing_page="' + buttons[val.button_name].wf_landing_page + '"><a href="#" data-original-title="" title="">' + buttons[val.button_name].button_name + '</a></li>');
                                }
                            } else { //Kapag empty proceed lng sa output

                                $(wButtons).append('<li class="fl-action-submit" value="' + buttons[val.button_name].button_name + '" data-workflow-id="' + workflowId + '" action="' + buttons[val.button_name].child_id + '" message="' + buttons[val.button_name].message + '" message_success = "' + buttons[val.button_name].message_success + '" field_required="' + buttonFieldRequired + '" require_comment="' + require_comment + '" landing_page="' + buttons[val.button_name].wf_landing_page + '"><a href="#" data-original-title="" title="">' + buttons[val.button_name].button_name + '</a></li>');
                            }
                        })
                    } else { //Para sa mga Old buttons na hindi recorded sa tb_button, balik sa normal lng ang output
                        if (button_last_action) {
                            $.each(button_last_action, function (id, val) {
                                require_comment = buttons[id].require_comment;
                                if (require_comment == true) {
                                    require_comment = 1;
                                }
                                $(wButtons).append('<li class="fl-action-submit" value="' + buttons[id].button_name + '" data-workflow-id="' + workflowId + '" action="' + buttons[id].child_id + '" message="' + buttons[id].message + '" message_success = "' + buttons[id].message_success + '" require_comment="' + require_comment + '" landing_page="' + buttons[id].wf_landing_page + '"><a href="#" data-original-title="" title="">' + buttons[id].button_name + '</a></li>');
                            });
                        }

                    }
                    //========================================================================================================

                    /* Addded by japhet morada */
                    // if($('#config_default_actions').val() == '1'){
                    //     // console.log("config", $('#config_default_actions').val());
                    //     //alert($('#config_default_actions').val());
                    //     $('#fl-action-option').children().wrap("<ul class='sublist'></ul>");
                    //     $('#fl-action-option').find('ul').wrap('<li class="function-btn" id="Functions"></li>');
                    //     $('.function-btn').prepend('<a>Functions</a>');
                    // }

                    if (requestId != '0' && processor != null && processor != '' && (isSaveHidden == 'false' || isSaveHidden == '')) {
                        //$('#actionRequestButton').append('<option value="Save" data-workflow-id="Save" action="Save">Save</option>');
                        if ($('[action="Save"]').length == 0) {
                            $(wButtons).append('<li class="fl-action-submit" value="Save" data-workflow-id="' + workflowId + '" action="Save" message="Are you sure you want save this request?" message_success = "Request has been saved."><a href="#" data-original-title="" title="">Save</a></li>');
                        }
                        $(".tip").tooltip();
                    }

                    // if (location == '/user_view/workspace') {
                    //     bind_onBeforeOnload(1);
                    // }
                } else {
                    // For Annotation hiding
                    $(".addAnnotation").hide()
                    $("#fl-form-controlsv-li2t").hide()
                    $("#wPaint").hide()
                }

                var editors = String($("#Editor").val()).split(',');
                if (editors.indexOf(currentUser) >= 0 && hasEditAccess) {
                    if (requestId != '0' && processor != null && processor != '' && (isSaveHidden == 'false' || isSaveHidden == '')) {
                        //$('#actionRequestButton').append('<option value="Save" data-workflow-id="Save" action="Save">Save</option>');
                        if ($('[action="Save"]').length == 0) {
                            $(wButtons).append('<li class="fl-action-submit" value="Save" data-workflow-id="' + workflowId + '" action="Save" message="Are you sure you want save this request?" message_success = "Request has been saved."><a href="#" data-original-title="" title="">Save</a></li>');
                        }
                        $(".tip").tooltip();
                        // if (location == '/user_view/workspace') {
                        //     bind_onBeforeOnload(1);
                        // }
                    }
                }

                if (requestId != '0' && processor != null && processor != '' && hasEditAccess) {
                    if (requestor_id == currentUser && (isCancelHidden == 'false' || isCancelHidden == '')) {
                        // $('#actionRequestButton').append('<option value="Cancel" data-workflow-id="Cancel" action="Cancel">Cancel</option>');
                        $(wButtons).append('<li class="fl-action-submit" value="Cancel" data-workflow-id="' + workflowId + '" action="Cancel" message="Are you sure you want cancel this request?" message_success = "Request has been cancelled."><a href="#" data-original-title="" title="">Cancel</a></li>');
                    }
                }
                $(wButtons).append('<li class="fl-action-close" value="Close" data-workflow-id="0" action="0" message="" message_success = ""><a href="#" data-original-title="" title="">Close</a></li>');
                //alert('sa get button')
                //for deletion

                if (parseInt($("#delete_access_forma").val()) > 0 && $("#enable_deletion_forma").val() == "1" && $("#app_config_deletion").val() == "1") {
                    $(wButtons).append('<li id="fl-action-delete" value="Delete(Default)" data-workflow-id="0" action="0" message="" message_success = ""><a href="#" data-original-title="" title="">Delete Data (Default)</a></li>');
                }

                if (processor.indexOf(currentUser) >= 0 || requestor_id == currentUser) {
                    //$(".viewer_action").append('<div class="fl-createViewreq fl-buttonEffect btn-basicBtn padding_5 cursor tip addViewer" data-original-title="Add Viewer" data-placement="bottom"><i class="fa fa-user icon-user"></i></div>')
                    $('<li class="isCursorPointer btn-basicBtn fl-buttonEffect tip" data-original-title="Add Viewers" style="position:relative; z-index:2;"><a class="addViewer"><i class="fa fa-users"></i></a></li>').insertAfter($('.fl-worspace-headerv2 #fl-form-basic-options ul li.showReply'));
                    $(".tip").tooltip();
                    var form_id = $('#FormID').val();
                    // $.post("/ajax/request_viewer", {action: "getRequestUsers", "form_id": form_id}, function (data) {
                    //     // console.log(data)
                    //     data_name = JSON.parse(data);
                    // })
                    // $(".viewer_action").html('<button class="btn-basicBtn padding_5 cursor tip form-readers" data-original-title="Viewers"><i class="icon-book"></i></button>');
                }

                if ($(wButtons).children("li").length == 0) {
                    $("#fl-action-btn").css("display", "none");
                }
                //default action button // added by japhet 02/17/2015
                // $("#fl-action-option").prepend($(".defaultActionButton").html())
            })
        },
        "makeRepeaterTable": function (tableElement, callBack) {
            var self_ini = this;
            try {
                if (typeof callBack == "undefined") {
                    //do nothing just catch errs
                }
            } catch (errs) {
                console.log(errs)
            }

            var repeat_fields = ".setObject";
            var table_body = $(tableElement).children("tbody");
            var table_row = table_body.children("tr");
            //MAG LALAGAY MUNA NG BUTTON ADDING SA BAWAT ROW
            //at initial startup
            //adding buttons

            function evRepeaterAdd(repeaterBtnEle) {
                if (repeaterBtnEle.children(".rep-add-spec-row").length >= 1) {
                    repeaterBtnEle.children(".rep-add-spec-row").on({
                        "click": function (e) {
                            var select_dis_table = $(this).parents("table.form-table");
                            if (select_dis_table.data("added_rows_uid")) {
                                select_dis_table.data("added_rows_uid", (select_dis_table.data("added_rows_uid") + 1));
                            } else {
                                select_dis_table.data("added_rows_uid", 1);
                            }
                            e.preventDefault();
                            var pathname = window.location;

                            if ($(this).closest("tr.original-row").length >= 1) {
                                //repeat clicked row, append content
                                var appended_row = "";
                                $(this).closest("tr.original-row").after(appended_row = $(
                                        '<tr>' +
                                        $(this).closest("tr.original-row").html() +
                                        '</tr>'
                                        ));
                                appended_row.children("td").slideUp(500).stop(true, true);
                                appended_row.children("td").slideDown(500);
                                var rep_btn_container_ar = appended_row.children("td").children(".td-relative").children(".repeater-action");
                                //display the remove row button of the repeated row
                                rep_btn_container_ar.children(".rep-remove-spec-row").css({
                                    "display": ""
                                });
                                rep_btn_container_ar.css({
                                    "left": "0px"
                                });


                                //adding event for original row button

                                evRepeaterAdd(rep_btn_container_ar);
                                evRepeaterRemove(rep_btn_container_ar);
                                // rebindFieldFunctionality(appended_row);
                                rebindFieldFunctionalityV3.call($(this), appended_row);
                            } else {
                                //repeat clicked row, append content
                                var appended_row = "";
                                $(this).closest("tr").after(appended_row = $(
                                        '<tr>' +
                                        $(this).closest("tr").html() +
                                        '</tr>'
                                        ));
                                appended_row.children("td").slideUp(500).stop(true, true);
                                appended_row.children("td").slideDown(500);
                                var rep_btn_container_ar2 = appended_row.children("td").children(".td-relative").children(".repeater-action");
                                //display the remove row button of the repeated row
                                rep_btn_container_ar2.children(".rep-remove-spec-row").css({
                                    "display": ""
                                });
                                rep_btn_container_ar2.css({
                                    // "margin-left":"-"+(rep_btn_container_ar2.css("width")),
                                    "left": "0px"
                                });

                                //adding event for original row button
                                evRepeaterAdd(rep_btn_container_ar2);
                                evRepeaterRemove(rep_btn_container_ar2);
                                // rebindFieldFunctionality(appended_row);
                                rebindFieldFunctionalityV3.call($(this), appended_row);
                            }
                            // refreshRepeaterTable($(this).closest(".form-table"));
                            // rebindFieldFunctionality(appended_row); // MAY PROBLEMS PA SA NAMES BIND
                            // rebindFieldFunctionalityV2($(this).closest(".form-table"));
                            // console.log($(this),"TABLE ADD ??",dis_appended_row,dis_appended_row.index());
                            if ($(this).parents("[table-response-to]").eq(0).length >= 1) {
                                var clicked_row_index = $(this).parents("tr").eq(0).index();
                                var tables_affected = $(this).parents("[table-response-to]").attr("table-response-to").split(",");
                                var collect_selection = null;
                                for (var ii = 0; ii < tables_affected.length; ii++) {
                                    if (ii == 0 && collect_selection == null) {
                                        collect_selection = $("[table-name='" + tables_affected[ii] + "']");
                                    } else {
                                        // console.log("NAKITA",$("[table-name='"+tables_affected[ii]+"']"))
                                        collect_selection = collect_selection.add($("[table-name='" + tables_affected[ii] + "']"));
                                    }
                                }
                                console.log("TRIPPIN", collect_selection)
                                collect_selection.each(function (eqi) {
                                    if ($(this).find("tbody").children("tr").eq(clicked_row_index).length >= 1) {
                                        $(this).find("tbody").children("tr").eq(clicked_row_index).find(".rep-add-spec-row").eq(0).trigger("click");
                                    }
                                })
                            }
                            return false;
                        }
                    });
                }
            }

            function rebindFieldFunctionalityV3(appended_row) {
                var select_tbody = $(appended_row).parents("tbody").eq(0);
                // var count_trs = select_tbody.children("tr").length;
                var count_trs = select_tbody.parents("table.form-table").data("added_rows_uid");
                var dis_appended_row = $(appended_row);
                var table_id = dis_appended_row.parents("table.form-table").eq(0).parents(".setOBJ").eq(0).attr("data-object-id");

                //remove class
                dis_appended_row.find(".hasDatepicker").removeClass("hasDatepicker");

                //remove attr's
                dis_appended_row.find("[value]").not('OPTION,[type="radio"],[type="checkbox"]').removeAttr("value");

                //fixing ids and names
                dis_appended_row.find("[data-object-id]").each(function (eq_name) {//FIX IDS
                    if ($(this).attr("rep-data-doi")) { //REPLACING THE GETFIELD UNIQUE CLASS
                        var doi = $(this).attr("rep-data-doi");


                        var getFields = $(this).find("*").filter(function () {
                            var regu_eks = new RegExp("getFields_" + doi, "g");
                            return regu_eks.test($(this).attr("rep-data-class"));
                        });

                        $(this).attr("data-object-id", $(this).attr("rep-data-doi") + "_UID" + count_trs);

                        if (getFields.attr("rep-data-class")) {
                            var reg_eks = new RegExp("getFields_[0-9]*", "g");
                            var replaced_class = getFields.attr("rep-data-class").replace(reg_eks, "getFields_" + doi + "_UID" + count_trs);
                            getFields.attr("class", replaced_class);
                        }
                    } else { //REPLACING THE UNIQUE GETFIELD CLASS

                        $(this).attr("rep-data-doi", $(this).attr("data-object-id"));
                        $(this).attr("data-object-id", $(this).attr("data-object-id") + "_UID" + count_trs);


                        var new_doi = $(this).attr("rep-data-doi");
                        var getFields = $(this).find(".getFields_" + new_doi);
                        // console.log($(this),"ETO UNG ERROR",getFields,(".getFields_" + new_doi) )
                        if (getFields.length >= 1) {
                            var reg_eks = new RegExp("getFields_[0-9]*", "g");
                            getFields.attr("rep-data-class", getFields.attr("class"));
                            var replaced_class = getFields.attr("class").replace(reg_eks, "getFields_" + new_doi + "_UID" + count_trs);
                            getFields.attr("class", replaced_class);
                        }
                    }
                });
                //fixing ids for request image
                dis_appended_row.find('[data-action-id]').each(function () {
                    var selfed = $(this)
                    if (selfed.attr('rep-data-dai')) {
                        selfed.attr("data-action-id", selfed.attr("rep-data-dai") + "_UID" + count_trs);
                    } else {
                        selfed.attr("rep-data-dai", selfed.attr("data-action-id"));
                        selfed.attr("data-action-id", selfed.attr("data-action-id") + "_UID" + count_trs);
                    }
                })
                // console.log("ImagegetFields_",dis_appended_row.find('.getFields[class*=ImagegetFields_]'))
                dis_appended_row.find('.getFields[class*=ImagegetFields_]').each(function () {
                    var selfed = $(this);
                    if (selfed.attr('src')) {
                        selfed.attr("src", "/images/avatar/large.png");
                    }
                    var get_doi = selfed.attr("class").match(/ImagegetFields_[0-9]*/g);
                    if (get_doi != null) {
                        get_doi = get_doi.filter(Boolean);
                        if (get_doi.length == 1) {
                            get_doi = get_doi[0].match(/[0-9]*/g);
                            if (get_doi != null) {
                                get_doi = get_doi.filter(Boolean);
                                if (get_doi.length == 1) {
                                    get_doi = get_doi[0];
                                }
                            }
                        }
                    }

                    if (get_doi == null) {
                        return true;
                    } else if (get_doi.length == 0) {
                        return true;
                    }

                    if (selfed.attr('rep-data-request-image-class')) {
                        selfed.attr("class", selfed.attr("rep-data-request-image-class").replace(/ImagegetFields_[0-9]*/g, "ImagegetFields_" + get_doi + "_UID" + count_trs));
                    } else {
                        selfed.attr("rep-data-request-image-class", selfed.attr("class"));
                        selfed.attr("class", selfed.attr("class").replace(/ImagegetFields_[0-9]*/g, "ImagegetFields_" + get_doi + "_UID" + count_trs));
                    }
                });

                dis_appended_row.find("[id]").each(function (eq_id) {//FIX IDS
                    if ($(this).attr("rep-data-id")) {
                        $(this).attr("id", $(this).attr("rep-data-id") + "_UID" + count_trs);
                    } else {
                        $(this).attr("rep-data-id", $(this).attr("id"));
                        $(this).attr("id", $(this).attr("id") + "_UID" + count_trs);
                    }
                });
                dis_appended_row.find("[name]").each(function (eq_name) {//FIX IDS
                    if ($(this).attr("rep-original-name")) {
                        if ($(this).attr('type') != "file") {
                            $(this).attr("name", $(this).attr("rep-original-name") + "_UID" + count_trs);
                        }
                    } else {
                        $(this).attr("rep-original-name", $(this).attr("name"));
                        if ($(this).attr('type') != "file") {
                            $(this).attr("name", $(this).attr("name") + "_UID" + count_trs);
                        }
                    }
                });



                //restoring functionality
                {
                    //REQUEST PHOTOS

                    //LISTNAMES BUTTON
                    dis_appended_row.find('.viewNames[return-field]').each(function () {
                        if ($(this).attr("rep-original-return-field")) {
                            $(this).attr("return-field", $(this).attr("rep-original-return-field") + "_UID" + count_trs);
                        } else {
                            $(this).attr("rep-original-return-field", $(this).attr("return-field"));
                            $(this).attr("return-field", $(this).attr("return-field") + "_UID" + count_trs);
                        }
                    });

                    //PICKLIST BUTTON
                    dis_appended_row.find('.pickListButton[return-field]').each(function () {
                        if ($(this).attr("rep-original-return-field")) {
                            $(this).attr("return-field", $(this).attr("rep-original-return-field") + "_UID" + count_trs);
                        } else {
                            $(this).attr("rep-original-return-field", $(this).attr("return-field"));
                            $(this).attr("return-field", $(this).attr("return-field") + "_UID" + count_trs);
                        }

                    });
                    AutoCompletePicklist.init(dis_appended_row.find('.pickListButton[return-field]'));
                    //DATEPICKER
                    dis_appended_row.find('input[data-type="date"]').each(function (eq_d) {
                        $(this).datepicker({
                            'dateFormat': 'yy-mm-dd'
                        });
                        if (ThisDate($(this).val()) != "Invalid Date") {
                            $(this).val(ThisDate($(this).val()).formatDate("Y-m-d"));
                        }
                    })
                    //DATETIMEPICKER
                    dis_appended_row.find('input[data-type="dateTime"]').each(function (eq_d) {
                        $(this).datetimepicker({
                            dateFormat: 'yy-mm-dd'
                        });
                        if (ThisDate($(this).val()) != "Invalid Date") {
                            $(this).val(ThisDate($(this).val()).formatDate("Y-m-d H:i"));
                        }
                    })
                    //TIME
                    dis_appended_row.find('input[data-type="time"]').each(function (eq_d) {
                        $(this).datetimepicker({
                            dateFormat: '',
                            timeFormat: 'HH:mm',
                            timeOnly: true
                        });
                        if (!$(this).val() && $(this).val().length >= 1) {
                            var time_val = ThisDate(ThisDate(new Date()).formatDate("Y-m-d") + " " + $(this).val());

                            if (time_val != "Invalid Date") {
                                $(this).val(time_val.formatDate("H:i"));
                            }
                        }

                    })
                    //CURRENCY
                    dis_appended_row.find(".getFields[data-input-type='Currency']").each(function () {
                        $(this).on("change.currency_format", function () {
                            if ($.isNumeric($(this).val())) {
                                var value_number = parseFloat($(this).val());
                                $(this).val(value_number.currencyFormat());
                            } else {
                                var value_nito = $(this).val()
                                var sanitized_val = value_nito.replace(/[\s,]/g, "");
                                if ($.isNumeric(sanitized_val)) {
                                    var value_number = parseFloat(sanitized_val);
                                    $(this).val(value_number.currencyFormat());
                                }
                            }
                        })
                        $(this).change();
                    });

                    dis_appended_row.find('[data-attachment]').each(function () {
                        var selfed = $(this)
                        if (selfed.attr('rep-data-dai')) {
                            selfed.attr("data-attachment", "attachment_multiple_on_attachment_" + selfed.parents('.setOBJ').attr("rep-data-doi") + "_UID" + count_trs);
                        } else {
                            console.log("walang rep-data-dai");
                            selfed.attr("rep-data-dai", selfed.attr("data-action-id"));
                            selfed.attr("data-attachment", "attachment_multiple_on_attachment_" + selfed.parents('.setOBJ').attr("rep-data-doi") + "_UID" + count_trs);
                            // selfed.attr("rep-original-name", $(selfed).attr('name'));

                        }
                    })
                    dis_appended_row.find('[data-single-attachment]').each(function () {
                        var selfed = $(this)
                        if (selfed.attr('rep-data-dai')) {
                            // selfed.attr("rep-original-name", $(selfed).attr('name'));
                        } else {
                            selfed.attr("rep-data-dai", selfed.attr("data-action-id"));
                            // selfed.attr("rep-original-name", $(selfed).attr('name'));

                        }
                    });
                    //REPEATER ACTION BUTTON DISABLING
                    dis_appended_row.on({
                        "mouseenter": function (e) {
                            if (!$(this).find(".repeater-action").hasClass("disable-repeater-action")) {
                                $(this).find(".repeater-action").css("display", "");
                                $(this).find(".repeater-action").stop(false, false).animate({
                                    "opacity": 1
                                }, function () {
                                    //TEST
                                })
                            }
                        },
                        "mouseleave": function (e) {
                            $(this).find(".repeater-action").stop(false, false).animate({
                                "opacity": 0
                            }, function () {
                                $(this).css("display", "none");
                            })
                        }
                    })
                    {//DISABLING REPEATER ACTION BUTTON BASED OF DISABLED FIELDS
                        var total_field_disabled_in_row = dis_appended_row.find(".getFields:disabled").length;
                        var total_field_in_row = dis_appended_row.find(".getFields").length;
                        if (total_field_disabled_in_row == total_field_in_row) {
                            dis_appended_row.find(".repeater-action").addClass("disable-repeater-action");
                        }
                    }
                    {//AUTO FIX FORMULA NAMES INDEXES IN ROW EXISTENCE
                        dis_appended_row.find('.getFields[default-type="computed"][default-formula-value]').each(function () {
                            var self = $(this);
                            var dis_formula = self.attr('default-formula-value');
                            var dis_formula_original = self.attr('default-formula-value');
                            if (self.attr('original-default-formula-value')) {
                                dis_formula = self.attr('original-default-formula-value');
                                dis_formula_original = self.attr('original-default-formula-value');
                            }
                            var reg_eks_get_name = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*", "g");
                            var name_matches = dis_formula.match(reg_eks_get_name)
                            if (name_matches != null) {
                                name_matches = name_matches.filter(Boolean);
                            }
                            var collect_names = [];
                            var reg_eks_for_replace = null;
                            var function_keyword_names = [
                                '@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
                                '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
                                '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
                                '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
                                '@LookupAVG', '@GetRound', '@LookupCountIf', '@Sum',
                                '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now',
                                '@GetAuth'
                            ];


                            //console.log("name_matches",name_matches);
                            for (var iz in name_matches) {
                                if (function_keyword_names.indexOf(name_matches[iz]) < 0) {
                                    collect_names.push({
                                        "origin_name": name_matches[iz],
                                        "modify_name": name_matches[iz] + "_UID" + count_trs,
                                    });
                                }
                            }
                            for (var iz in collect_names) {
                                //console.log("origin_name",collect_names[iz]['origin_name'],"modify_name",collect_names[iz]['modify_name'], " "+dis_appended_row.find('[name="'+collect_names[iz]['modify_name'].replace("@","")+'"]').length)
                                if (dis_appended_row.find('[name="' + collect_names[iz]['modify_name'].replace("@", "") + '"]').length >= 1) {
                                    reg_eks_for_replace = new RegExp(collect_names[iz]['origin_name'] + "(?![a-zA-Z0-9_])", "g")
                                    dis_formula = dis_formula.replace(reg_eks_for_replace, collect_names[iz]['modify_name']);
                                    //console.log(reg_eks_for_replace,"modified",dis_formula);
                                }
                            }
                            //console.log('dis_formula',dis_formula);
                            self.attr('default-formula-value', dis_formula);
                            self.attr('original-default-formula-value', dis_formula_original);
                        });
                    }
                    //REBIND DEFAULT COMPUTED
                    dis_appended_row.find('.getFields[default-type="computed"][default-formula-value]').each(function () {
                        console.log("ELEMENT REPEATED W FORMULAS " + $(this).attr("name"), $(this));
                        var this_element_setOBJ = $(this).parents(".setOBJ").eq(0);
                        var this_element_field = $(this);
                        var data_obj_id = this_element_setOBJ.attr("data-object-id");

                        self_ini.defaultComputed(data_obj_id, this_element_field, this_element_setOBJ, "repeaterDefaultValueComputed" + table_id);
                    });
                    //REBIND FIELDS FROM COMPUTED FORMULAS
                    dis_appended_row.find('.getFields[event-id-from]').each(function (eqi) {

                        var edoi_from = $(this).attr("event-id-from");
                        var element_in_computation = $(this);
                        var splitted_edoi_from = edoi_from.split(",");

                        for (index_oid in splitted_edoi_from) {

                            var the_fields = $("[data-object-id='" + splitted_edoi_from[index_oid] + "']").find(".getFields_" + splitted_edoi_from[index_oid]);
                            if (the_fields.attr("default-formula-value")) {
                                console.log("ID MAY FORMULA" + splitted_edoi_from[index_oid], the_fields.attr("name"), element_in_computation.attr("name"), "TBL ID::" + table_id)
                                var eventAddressNamespace = "repeaterDefaultValueComputed" + table_id;
                                self_ini.makeOnChange(element_in_computation, the_fields, eventAddressNamespace);
                            }
                        }

                    })
                    //TRIGGER CHANGES OF NEWLY ADDED ROW
                    dis_appended_row.find('.getFields').filter(function (index, element) {
                        var dis_ele = $(element);
                        var dis_ele_ev_data = dis_ele.data("events")
                        if (dis_ele_ev_data) {
                            if (dis_ele_ev_data["change"]) {
                                if (dis_ele_ev_data["change"].length >= 1) {
                                    return true;
                                }
                            }
                        }
                    }).change();


                }

            }


            table_row.each(function (eqi_tr) {
                td_ele = $(this).children("td");
                // relative_content = td_ele.last()/*.eq(0)*/.children(".td-relative");
                relative_content = td_ele.eq(0).children(".td-relative");
                $(this).addClass("original-row"); //declaring original row
                $(this).children("td").attr("row-height", $(this).outerHeight());
                relative_content.append(adding_and_removing_btn = $(
                        '<span class="repeater-action" style="opacity:0;z-index:2;position:absolute;bottom:0px;">' +
                        '<a class="fa fa-plus icon-plus rep-add-spec-row" style="width:auto;height:auto;margin: 0px 5px;"></a>' +
                        '<a class="fa fa-minus icon-minus rep-remove-spec-row" style="width:auto;height:auto;margin: 0px 5px;"></a>' +
                        '</span>'
                        ));
                //don't display remove row on original buttons
                adding_and_removing_btn.children(".rep-remove-spec-row").eq(0).css({
                    "display": "none"
                });
                // adding_and_removing_btn.css({
                //    // "margin-left":"-"+(adding_and_removing_btn.css("width")),
                //    "margin-right":"-"+(adding_and_removing_btn.css("width")),
                //    "position":"absolute",
                //    "right":"0px"
                // });
                adding_and_removing_btn.css({
                    // "margin-left":"-"+(adding_and_removing_btn.css("width")),
                    "left": "0px"
                });
                //adding events for button
                evRepeaterAdd(adding_and_removing_btn);
                evRepeaterRemove(adding_and_removing_btn);

                //adding events for button version 2
                // evRepeaterAddV2(adding_and_removing_btn);
                // evRepeaterRemoveV2(adding_and_removing_btn);
            });





            // {//eyAH NEW VERSION REPEATER TABLE FAST
            //     function evRepeaterAddV2(row_button){
            //         if (row_button.find(".rep-add-spec-row").length >= 1) {
            //             row_button.find(".rep-add-spec-row").eq(0).on("click",function(){
            //                 var tr_element = $(this).parents("tr").eq(0);
            //                 var tr_append_location = tr_element;
            //                 var tr_cloned = tr_element.clone();
            //                 tr_append_location.after(tr_cloned);
            //                 tr_cloned.find(".rep-remove-spec-row").css("display","");
            //             })
            //         }
            //     }
            //     function evRepeaterRemoveV2(row_button){
            //         if (row_button.find(".rep-remove-spec-row").length >= 1) {
            //             row_button.find(".rep-remove-spec-row").eq(0).on("click",function(){
            //                 alert("MAYNUS");
            //             })
            //         }
            //     }
            // }
            refreshRepeaterTable(tableElement);
            if (typeof callBack != "undefined") {
                callBack(tableElement);
            }
        },
        "getUpdatedServerTime": function () {
            var b_data = $("body").data();
            if (b_data) {
                if (b_data.getServerTimeData) {
                    return b_data.getServerTimeData;
                }
            }
            return "";
        },
        "defaultComputed": function (data_obj_id, the_field_ele, this_element, eventAddressNamespace) {
            //alert("defaultComputed")


            var fieldname_keywords = [
                "FormID", "WorkflowId", "ID",
                "Requestor", "CurrentUser", "Status",
                "Processor", "LastAction", "DateCreated",
                "DateUpdated", "CreatedBy", "UpdatedBy",
                "Unread", "Node_ID", "Workflow_ID",
                "Mode", "TrackNo", "imported",
                "computedFields", "fieldEnabled", "KeywordsField",
                "Repeater_Data", "Department_Name", "From",
                "To", "Range", "requestorname", "Auth", "TimeStamp"
            ];
            this_ini_variant = this;
            the_field = the_field_ele;
            this_self_ele = $(this_element);
            var string_default_val = "" + $(this_self_ele).find(".getFields_" + data_obj_id).attr("default-formula-value");

            string_default_val = "" + $(this_self_ele).find(".getFields_" + data_obj_id).attr("default-formula-value");
            if (string_default_val.indexOf("@UserInfo") >= 0) {
                return;
            }
            if (string_default_val.indexOf("@CustomScript") >= 0) {
                return;
            }
            if (string_default_val.indexOf("@Head") >= 0) {
                return;
            }
            if (string_default_val.indexOf("@AssistantHead") >= 0) {
                return;
            }
            if (string_default_val.indexOf("@Lookup") >= 0) {
                return;
            }
            if (string_default_val.indexOf("@RecordCount") >= 0) {
                return;
            }

            if (string_default_val.indexOf("@LookupWhere") >= 0) {
                console.log("HERE TRACK", the_field, this_self_ele)
                return;
            }
            if (string_default_val.indexOf("@LookupSDev") >= 0) {
                return;
            }
            if (string_default_val.indexOf("@LookupAVG") >= 0) {
                return;
            }
            if (string_default_val.indexOf("@LookupAuthInfo") >= 0) {
                return;
            }
            if (string_default_val.indexOf(/^@Total\(*.\)$/) >= 0) {
                return;
            }

            splitted_default_val = string_default_val.match(/@GetAVG\(@[A-Za-z0-9_]*\)|@SDev\(@[A-Za-z0-9_]*\)|@GetMin\(@[A-Za-z0-9_]*\)|@GetMax\(@[A-Za-z0-9_]*\)|@Sum\(@[A-Za-z0-9_]*\)|@(\[[a-zA-Z0-9_\'\"]*\]){3}|@[A-Za-z0-9_]*/g);
            // console.log("HEY",splitted_default_val)

            if (splitted_default_val != null) { //KAPAG NAG NULL UNG MATCH OR SPLITTED VALUES
                var is_number = "true";
                for (ctr = 0; ctr < splitted_default_val.length; ctr++) {
                    replace_param1 = splitted_default_val[ctr].trim();
                    element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');

                    replace_param2 = element_in_computation.val();
                    if (element_in_computation.length >= 1) {
                    } else {
                        continue;
                    }
                    if ($.isNumeric(replace_param2)) {
                    } else {
                        is_number = "false";
                        break;
                    }
                }
                // console.log("TESTING THIS GREAT FORMULA");
                var fieldname_in_formula_has_binding = false;
                var collected_fieldnames_binded = [];
                for (ctr = 0; ctr < splitted_default_val.length; ctr++) {

                    replace_param1 = splitted_default_val[ctr].trim();
                    if (replace_param1.match(/@Sum\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                        replace_param1 = splitted_default_val[ctr].trim().match(/@Sum\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                        replace_param1 = replace_param1[1].replace("@", "");
                    } else if (replace_param1.match(/@GetMax\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                        replace_param1 = splitted_default_val[ctr].trim().match(/@GetMax\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                        replace_param1 = replace_param1[1].replace("@", "");
                    } else if (replace_param1.match(/@SDev\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                        replace_param1 = splitted_default_val[ctr].trim().match(/@SDev\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                        replace_param1 = replace_param1[1].replace("@", "");
                    } else if (replace_param1.match(/@GetMin\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                        replace_param1 = splitted_default_val[ctr].trim().match(/@GetMin\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                        replace_param1 = replace_param1[1].replace("@", "");
                    } else if (replace_param1.match(/@GetAVG\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                        replace_param1 = splitted_default_val[ctr].trim().match(/@GetAVG\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                        replace_param1 = replace_param1[1].replace("@", "");
                    } else {
                        replace_param1 = splitted_default_val[ctr].trim().replace("@", "");
                    }
                    if ($(this_self_ele).parents("tr").length >= 1) { // kapag ung may formula ay nasa loob ng row
                        if ($(this_self_ele).parents("tr").find('[rep-original-name="' + replace_param1 + '"]').length >= 1) {
                            element_in_computation = $(this_self_ele).parents("tr").find('[rep-original-name="' + replace_param1 + '"]');
                        } else if ($(this_self_ele).parents("tr").find('[name="' + replace_param1 + '"]').length >= 1) {
                            element_in_computation = $(this_self_ele).parents("tr").find('[name="' + replace_param1 + '"]');
                        } else {
                            element_in_computation = $('[name="' + replace_param1 + '"]');
                        }
                        /*} else if($("[name='"+replace_param1+"']").parents("tr").length >= 1){// kapag ung fieldname sa formula ay nasa loob ng row
                         element_in_computation = $("[name='"+replace_param1+"']").add('[rep-original-name="' + replace_param1 + '"]');*/
                    } else {
                        element_in_computation = $('[name="' + replace_param1 + '"]');
                        if (splitted_default_val[ctr].trim().match(/@Sum\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {
                            if (splitted_default_val[ctr].trim().match(/\((@[A-Za-z][A-Za-z0-9_]*)\)/)[1].replace("@", "") == replace_param1 && element_in_computation.length >= 1) {
                                if (typeof element_in_computation.attr("event-id-from") != "undefined") {
                                    if (element_in_computation.attr("event-id-from").indexOf(data_obj_id) >= 0) {

                                    } else {
                                        element_in_computation.attr("event-id-from", element_in_computation.attr("event-id-from") + "," + data_obj_id);
                                    }
                                } else {
                                    element_in_computation.attr("event-id-from", data_obj_id);
                                }
                            }
                            var second_fn = "@Sum";
                        }
                        if (splitted_default_val[ctr].trim().match(/@SDev\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {
                            if (splitted_default_val[ctr].trim().match(/\((@[A-Za-z][A-Za-z0-9_]*)\)/)[1].replace("@", "") == replace_param1 && element_in_computation.length >= 1) {
                                if (typeof element_in_computation.attr("event-id-from") != "undefined") {
                                    if (element_in_computation.attr("event-id-from").indexOf(data_obj_id) >= 0) {

                                    } else {
                                        element_in_computation.attr("event-id-from", element_in_computation.attr("event-id-from") + "," + data_obj_id);
                                    }
                                } else {
                                    element_in_computation.attr("event-id-from", data_obj_id);
                                }
                            }
                            var second_fn = "@SDev";
                        }
                        if (splitted_default_val[ctr].trim().match(/@GetMax\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {
                            if (splitted_default_val[ctr].trim().match(/\((@[A-Za-z][A-Za-z0-9_]*)\)/)[1].replace("@", "") == replace_param1 && element_in_computation.length >= 1) {
                                if (typeof element_in_computation.attr("event-id-from") != "undefined") {
                                    if (element_in_computation.attr("event-id-from").indexOf(data_obj_id) >= 0) {

                                    } else {
                                        element_in_computation.attr("event-id-from", element_in_computation.attr("event-id-from") + "," + data_obj_id);
                                    }
                                } else {
                                    element_in_computation.attr("event-id-from", data_obj_id);
                                }
                            }
                            var second_fn = "@GetMax";
                        }
                        if (splitted_default_val[ctr].trim().match(/@GetMin\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {
                            if (splitted_default_val[ctr].trim().match(/\((@[A-Za-z][A-Za-z0-9_]*)\)/)[1].replace("@", "") == replace_param1 && element_in_computation.length >= 1) {
                                if (typeof element_in_computation.attr("event-id-from") != "undefined") {
                                    if (element_in_computation.attr("event-id-from").indexOf(data_obj_id) >= 0) {

                                    } else {
                                        element_in_computation.attr("event-id-from", element_in_computation.attr("event-id-from") + "," + data_obj_id);
                                    }
                                } else {
                                    element_in_computation.attr("event-id-from", data_obj_id);
                                }
                            }
                            var second_fn = "@GetMin";
                        }
                        if (splitted_default_val[ctr].trim().match(/@GetAVG\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {
                            if (splitted_default_val[ctr].trim().match(/\((@[A-Za-z][A-Za-z0-9_]*)\)/)[1].replace("@", "") == replace_param1 && element_in_computation.length >= 1) {
                                if (typeof element_in_computation.attr("event-id-from") != "undefined") {
                                    if (element_in_computation.attr("event-id-from").indexOf(data_obj_id) >= 0) {

                                    } else {
                                        element_in_computation.attr("event-id-from", element_in_computation.attr("event-id-from") + "," + data_obj_id);
                                    }
                                } else {
                                    element_in_computation.attr("event-id-from", data_obj_id);
                                }
                            }
                            var second_fn = "@GetAVG";
                        }
                    }
                    //kapag length == 0 icheck kung select many sya or checkbox
                    if (element_in_computation.length <= 0) {
                        element_in_computation = $('[name="' + replace_param1 + '[]"]');
                    }

                    // console.log($.trim(replace_param1.split(/@|\(|\)/g).join("")));
                    // console.log(element_in_computation.attr("name"));

                    if (replace_param1 == "") {
                        continue;
                    }
                    // alert(splitted_default_val[ctr].replace("@","")+"TESTTT"+splitted_default_val[ctr].indexOf(fieldname_keywords))
                    if (element_in_computation.length >= 1 && fieldname_keywords.indexOf(splitted_default_val[ctr].replace("@", "")) <= -1 && (splitted_default_val[ctr].trim().match(/@GetAVG/) == null && splitted_default_val[ctr].trim().match(/@SDev/) == null && splitted_default_val[ctr].trim().match(/@Sum/) == null && splitted_default_val[ctr].trim().match(/@GetMax/) == null && splitted_default_val[ctr].trim().match(/@GetMin/) == null)) {
                        if (element_in_computation.is('select[multiple]') || element_in_computation.is('input[type="checkbox"]') || element_in_computation.is('input[type="radio"]')) {
                            replace_param2 = element_in_computation.filter(':selected,:checked').map(function (a, b) {
                                return $(b).val()
                            }).get().filter(Boolean).join("|^|") || "";
                        } else {
                            replace_param2 = element_in_computation.val() || "";
                        }

                        // console.log("DC");
                        // console.log(element_in_computation);
                        try {
                            //if(element_in_computation.parents("tr").length >= 1){
                            //  if(the_field.attr("default-formula-value")){
                            //      if(the_field.attr("default-type")){
                            //          if(the_field.attr("default-type") == "computed"){
                            //              if(the_field.attr("default-formula-value").indexOf("@Sum") >= 0 ){
                            //                  element_in_computation = $(element_in_computation.selector+", [rep-original-name='"+$.trim(replace_param1.split(/@|\(|\)/g).join(""))+"']")
                            //                  element_in_computation.each(function(eqi){
                            //                      $(this).attr("event-id-from", the_field.parents(".setOBJ").eq(0).attr("data-object-id"));
                            //                  })
                            //              }
                            //          }
                            //      }
                            //  }
                            //}
                        } catch (errr) {
                        }
                        //if(this_self_ele.attr("name")=="TotalRunBal"){
                        //  console.log("ARRGH2   "+$.trim(replace_param1.split(/@|\(|\)/g).join("")))
                        //  console.log(this_self_ele.attr("default-formula-value"))
                        //  console.log(this_self_ele)
                        //  console.log(this_self_ele.attr("name"))
                        //  console.log(element_in_computation)
                        //  console.log("ARRGH2----")
                        //}
                        //console.log("PALA"+string_default_val)
                        //console.log("MAKEONCHANGE",element_in_computation,this_self_ele)

                        if (collected_fieldnames_binded.indexOf(element_in_computation.attr("name")) <= -1) {
                            this_ini_variant.makeOnChange(element_in_computation, the_field, eventAddressNamespace);
                            collected_fieldnames_binded.push(element_in_computation.attr("name"))
                        }
                        if (is_number == "false") {//
                            if (element_in_computation.attr("data-type") == "float" || element_in_computation.attr("data-type") == "double") {
                                string_default_val = string_default_val.replace(replace_param1, "Number('" + "" + replace_param2.replace(/\"/g, "\\\"").replace(",", "") + "" + "')");
                            } else if (element_in_computation.attr("data-type") == "longtext") {
                                replace_param2 = replace_param2.replace(/\\/g, "\\\\");
                                replace_param2 = replace_param2.replace(/\"/g, "\\\"");
                                string_default_val = string_default_val.replace(replace_param1, "\"" + replace_param2 + "\"");
                            } else {
                                string_default_val = string_default_val.replace(replace_param1, "\"" + replace_param2.replace(/\"/g, "\\\"") + "\"");
                            }
                        } else {
                            string_default_val = string_default_val.replace(replace_param1, replace_param2);
                        }
                    } else if ((element_in_computation.length == 0 || fieldname_keywords.indexOf(splitted_default_val[ctr].replace("@", "")) >= 0) || (string_default_val.match(/@GetAVG/) != null || string_default_val.match(/@SDev/) != null || string_default_val.match(/@Sum/) != null || string_default_val.match(/@GetMax/) != null || string_default_val.match(/@GetMin/) != null)) {
                        var rep_param_w_at = "@" + replace_param1;

                        if (typeof second_fn !== "undefined") {
                            if (second_fn == "@Sum") {
                                rep_param_w_at = "@Sum(" + rep_param_w_at + ")";
                            } else if (second_fn == "@GetMax") {
                                rep_param_w_at = "@GetMax(" + rep_param_w_at + ")";
                            } else if (second_fn == "@SDev") {
                                rep_param_w_at = "@SDev(" + rep_param_w_at + ")";
                            } else if (second_fn == "@GetMin") {
                                rep_param_w_at = "@GetMin(" + rep_param_w_at + ")";
                            } else if (second_fn == "@GetAVG") {
                                rep_param_w_at = "@GetAVG(" + rep_param_w_at + ")";
                            }
                        }

                        if (rep_param_w_at.indexOf("@Today") >= 0) {

                            var this_today = new Date(this_ini_variant.getUpdatedServerTime());
                            this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2);

                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + this_today + "\"");
                        } else if (rep_param_w_at.indexOf("@TimeStamp") >= 0) {
                            var this_timestamp = $('body').data("date_page_visited");
                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + this_timestamp + "\"");
                        } else if (rep_param_w_at.indexOf("@Now") >= 0) {
                            var this_today = new Date(this_ini_variant.getUpdatedServerTime());
                            this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2) + " " + StrRight("0" + this_today.getHours(), 2) + ":" + StrRight("0" + this_today.getMinutes(), 2) + ":" + StrRight("0" + this_today.getSeconds(), 2);
                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + this_today + "\"");
                        } else if (rep_param_w_at.indexOf("@RequestID") >= 0) {
                            var getID = $("#ID").val();
                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + getID + "\"");
                        } else if (rep_param_w_at.indexOf("@CurrentUser") >= 0) {
                            var current_user_ele = $("#frmrequest").find('[name="CurrentUser"]').eq(0);
                            var current_user_name = current_user_ele.attr("current-user-name");
                            // var getCurrentUser = "<?php echo $auth['display_name']; ?>";
                            // string_default_val = string_default_val.replace(rep_param_w_at, "\"" + getCurrentUser + "\"");
                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + current_user_name + "\"");
                        } else if (rep_param_w_at.indexOf("@Processor") >= 0) {

                            if ($("[name='ID']").val() == "0") {
                                var processor_user_ele = $('[name="Processor"]');
                                var processor_name = processor_user_ele.attr("processor-name");
                            } else {
                                var processor_user_ele = $('[name="Processor"]');
                                var processor_name = processor_user_ele.attr("processor-name");
                            }

                            // var getCurrentUser = "<?php echo $auth['display_name']; ?>";
                            // string_default_val = string_default_val.replace(rep_param_w_at, "\"" + getCurrentUser + "\"");
                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + processor_name + "\"");
                        } else if (rep_param_w_at.indexOf("@Requestor") >= 0) {
                            // requestor_ele = "<?php echo $auth['display_name']; ?>";
                            // $(this_self_ele).find(".getFields_" + data_obj_id).val(requestor_ele);
                            // var getRequestorVal = "<?php echo $auth['display_name']; ?>";
                            if ($("[name='ID']").val() == "0") {
                                var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                var requestor_name = requestor_ele.attr("requestor-name");
                            } else {
                                // var requestor_ele = $("#requestor_display");
                                // var requestor_name = requestor_ele.text();
                                var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                var requestor_name = requestor_ele.attr("requestor-name");
                            }

                            // string_default_val = string_default_val.replace(rep_param_w_at, "\"" + getRequestorVal + "\"");
                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + requestor_name + "\"");
                        } else if (rep_param_w_at.indexOf("@TrackNo") >= 0) {


                            // var self_tn = $(this_self_ele);
                            // var gettingValueChangesTrackNo = setInterval(function() {
                            //     track_no_ele_val = track_no_ele.val();
                            //     if (track_no_ele_val == "") {
                            //         self_tn.find(".getFields_" + data_obj_id).val("");
                            //     } else {
                            //         self_tn.find(".getFields_" + data_obj_id).val(track_no_ele_val);
                            //         clearInterval(gettingValueChangesTrackNo);
                            //     }
                            // }, 100);
                            // $(this_self_ele).find(".getFields_" + data_obj_id).val(track_no_ele.val());
                            var track_no_ele = $("#frmrequest").find('[name="TrackNo"]').eq(0)
                            var getTrackNoVal = track_no_ele.val();
                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + getTrackNoVal + "\"");
                        } else if (rep_param_w_at.indexOf("@Status") >= 0) {
                            // var self_s = $(this_self_ele);
                            // var gettingValueChangesStatus = setInterval(function() {
                            //     status_ele_val = status_ele.val();
                            //     if (status_ele_val == "") {
                            //         self_s.find(".getFields_" + data_obj_id).val("New request");
                            //     } else {
                            //         self_s.find(".getFields_" + data_obj_id).val(status_ele_val);
                            //         clearInterval(gettingValueChangesStatus);
                            //     }
                            // }, 100);
                            var status_ele = $("#frmrequest").find('[name="Status"]').eq(0);
                            var getStatusVal = status_ele.val();
                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + getStatusVal + "\"");
                        } else if (rep_param_w_at.indexOf("@Department") >= 0) {
                            var getDepName = $("[name='Department_Name']").val();
                            string_default_val = string_default_val.replace(rep_param_w_at, "\"" + getDepName + "\"");

                            // } else if (rep_param_w_at.match(/\[[A-Za-z]*\]/) != null) {//FOR SUM AVG ETC FORMULA
                            //     alert("QUARANTINe")
                            //     var fieldName = rep_param_w_at.replace(/\[[A-Za-z]*\]|@/g, "");
                            //     var formula_function = rep_param_w_at.match(/\[[A-Za-z]*\]/);
                            //     var get_fields_value = "(";
                            //     if (formula_function[0] == "[sum]") {
                            //         get_fields_value = get_fields_value + "Number('" + $('[name="' + fieldName + '"]').val() + "')";

                            //         this_ini_variant.makeOnChange($('[name="' + fieldName + '"]'), the_field, eventAddressNamespace);
                            //         $('[rep-original-name="' + fieldName + '"]').each(function() {
                            //             //
                            //             get_fields_value = get_fields_value + " + Number('" + $(this).val() + "')";
                            //         });
                            //         get_fields_value = get_fields_value + ")";
                            //         string_default_val = string_default_val.replace(rep_param_w_at, "" + get_fields_value + "");
                            //     }
                            //     if (formula_function[0] == "[Last]") {
                            //         if ($('[rep-original-name="' + fieldName + '"]').length >= 1) {
                            //             var element_in_computation_last = $('[rep-original-name="' + fieldName + '"]').last();
                            //             this_ini_variant.makeOnChange(element_in_computation_last, the_field, eventAddressNamespace);
                            //             if ($.isNumeric(element_in_computation_last.val())) {
                            //                 get_fields_value = get_fields_value + "" + element_in_computation_last.val() + "";
                            //             } else {
                            //                 get_fields_value = get_fields_value + "'" + element_in_computation_last.val() + "'";
                            //             }
                            //             get_fields_value = get_fields_value + ")";
                            //             string_default_val = string_default_val.replace(rep_param_w_at, "" + get_fields_value + "");
                            //         } else if ($('[name="' + fieldName + '"]').length >= 1) {
                            //             var element_in_computation_last = $('[name="' + fieldName + '"]').last();
                            //             this_ini_variant.makeOnChange(element_in_computation_last, the_field, eventAddressNamespace);
                            //             if ($.isNumeric(element_in_computation_last.val())) {
                            //                 get_fields_value = get_fields_value + "" + element_in_computation_last.val() + "";
                            //             } else {
                            //                 get_fields_value = get_fields_value + "'" + element_in_computation_last.val() + "'";
                            //             }
                            //             get_fields_value = get_fields_value + ")";
                            //             string_default_val = string_default_val.replace(rep_param_w_at, "" + get_fields_value + "");
                            //         }
                            //     }
                        } else if (rep_param_w_at.match(/@Sum\(@[A-Za-z0-9_]*\)/g) != null) { //FOR @Sum(@Fieldname)
                            fieldName = rep_param_w_at.match(/\((@[A-Za-z0-9_]*)\)/);
                            var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                            if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                            }
                            this_ini_variant.makeOnChange($('[name="' + fieldName[1].replace("@", "") + '"]'), the_field, eventAddressNamespace);
                        } else if (rep_param_w_at.match(/@GetMax\(@[A-Za-z0-9_]*\)/g) != null) { //

                            fieldName = rep_param_w_at.match(/\((@[A-Za-z0-9_]*)\)/);
                            var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                            if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                            }
                            this_ini_variant.makeOnChange($('[name="' + fieldName[1].replace("@", "") + '"]'), the_field, eventAddressNamespace);
                        } else if (rep_param_w_at.match(/@GetMin\(@[A-Za-z0-9_]*\)/g) != null) { //
                            fieldName = rep_param_w_at.match(/\((@[A-Za-z0-9_]*)\)/);
                            var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                            if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                            }
                            this_ini_variant.makeOnChange($('[name="' + fieldName[1].replace("@", "") + '"]'), the_field, eventAddressNamespace);
                        } else if (rep_param_w_at.match(/@SDev\(@[A-Za-z0-9_]*\)/g) != null) { //
                            fieldName = rep_param_w_at.match(/\((@[A-Za-z0-9_]*)\)/);
                            var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                            if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                            }
                            this_ini_variant.makeOnChange($('[name="' + fieldName[1].replace("@", "") + '"]'), the_field, eventAddressNamespace);
                        } else if (rep_param_w_at.match(/@GetAVG\(@[A-Za-z0-9_]*\)/g) != null) { //
                            fieldName = rep_param_w_at.match(/\((@[A-Za-z0-9_]*)\)/);
                            var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                            if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                            }
                            this_ini_variant.makeOnChange($('[name="' + fieldName[1].replace("@", "") + '"]'), the_field, eventAddressNamespace);
                        } else if (splitted_default_val[ctr].match(/@(\[[a-zA-Z0-9_\'\"]*\]){3}/g) != null) {
                            var reg_exp = new RegExp("\\[[@a-zA-Z0-9_\\\'\\\"]*\\](?!\\s)", 'g');
                            var value_get = "";
                            if (splitted_default_val[ctr].match(reg_exp) != null) {
                                var match_res = splitted_default_val[ctr].match(reg_exp).filter(Boolean);
                                var tbl_name = match_res[0].replace(/\[|\]/g, "");
                                var field_name = match_res[1].replace(/\[|\]/g, "");
                                var fn_index = match_res[2].replace(/\[|\]/g, "");
                                if ($.isNumeric(fn_index)) {
                                    var collect_fn_ele = $('[table-name="' + tbl_name + '"]').find('[name="' + field_name + '"]');
                                    collect_fn_ele = collect_fn_ele.add($('[table-name="' + tbl_name + '"]').find('[rep-original-name="' + field_name + '"]'));
                                    if (collect_fn_ele.eq(fn_index).attr("data-type") == "float" || collect_fn_ele.eq(fn_index).attr("data-type") == "double") {
                                        value_get = "Number(" + collect_fn_ele.eq(fn_index).val() + ")";
                                    } else {
                                        value_get = "\'" + collect_fn_ele.eq(fn_index).val() + "\'";
                                    }

                                    if (collect_fn_ele.eq(0).attr("event-id-from")) {
                                        var eids = collect_fn_ele.eq(0).attr("event-id-from").split(",");
                                        if (eids.indexOf("" + data_obj_id) < 0) {
                                            collect_fn_ele.eq(0).attr("event-id-from", collect_fn_ele.eq(0).attr("event-id-from") + "," + data_obj_id)
                                        }
                                    } else {
                                        collect_fn_ele.eq(0).attr("event-id-from", data_obj_id)
                                    }

                                    this_ini_variant.makeOnChange(collect_fn_ele.eq(fn_index), the_field, eventAddressNamespace);
                                } else if (fn_index == "RootRowIndex") {
                                    fn_index = GetRowIndex(this_self_ele);

                                    var collect_fn_ele = $('[table-name="' + tbl_name + '"]').find('[name="' + field_name + '"]');
                                    collect_fn_ele = collect_fn_ele.add($('[table-name="' + tbl_name + '"]').find('[rep-original-name="' + field_name + '"]'));
                                    if (collect_fn_ele.eq(fn_index).attr("data-type") == "float" || collect_fn_ele.eq(fn_index).attr("data-type") == "double") {
                                        value_get = "Number(" + collect_fn_ele.eq(fn_index).val() + ")";
                                    } else {
                                        value_get = "\'" + collect_fn_ele.eq(fn_index).val() + "\'";
                                    }

                                    if (collect_fn_ele.eq(0).attr("event-id-from")) {
                                        var eids = collect_fn_ele.eq(0).attr("event-id-from").split(",");
                                        if (eids.indexOf("" + data_obj_id) < 0) {
                                            collect_fn_ele.eq(0).attr("event-id-from", collect_fn_ele.eq(0).attr("event-id-from") + "," + data_obj_id)
                                        }
                                    } else {
                                        collect_fn_ele.eq(0).attr("event-id-from", data_obj_id)
                                    }
                                    this_ini_variant.makeOnChange(collect_fn_ele.eq(fn_index), the_field, eventAddressNamespace);
                                } else if (fn_index == "RowIndex") {
                                    // fn_index = GetRowIndex($('[table-name="'+tbl_name+'"]').find('[name="'+field_name+'"]'));

                                    // var collect_fn_ele = $('[table-name="'+tbl_name+'"]').find('[name="'+field_name+'"]');
                                    // collect_fn_ele = collect_fn_ele.add($('[table-name="'+tbl_name+'"]').find('[rep-original-name="'+field_name+'"]'));
                                    // if(collect_fn_ele.eq(fn_index).attr("data-type") == "float"){
                                    //     value_get = "Number("+collect_fn_ele.eq(fn_index).val()+")";
                                    // }else{
                                    //     value_get = "\'"+collect_fn_ele.eq(fn_index).val()+"\'";
                                    // }

                                    // if(collect_fn_ele.eq(0).attr("event-id-from")){
                                    //     var eids = collect_fn_ele.eq(0).attr("event-id-from").split(",");
                                    //     if(eids.indexOf(""+data_obj_id) < 0){
                                    //         collect_fn_ele.eq(0).attr("event-id-from",collect_fn_ele.eq(0).attr("event-id-from")+","+data_obj_id)
                                    //     }
                                    // }else{
                                    //     collect_fn_ele.eq(0).attr("event-id-from",data_obj_id)
                                    // }

                                    // this_ini_variant.makeOnChange(collect_fn_ele.eq(fn_index), the_field, eventAddressNamespace);
                                }

                            }
                            var reg_exp_repl = new RegExp(splitted_default_val[ctr].replace(/\[/g, "\\\[").replace(/\]/g, "\\\]").replace(/\'/g, "\\\'").replace(/\"/g, "\\\""), "g");
                            string_default_val = string_default_val.replace(reg_exp_repl, value_get);

                        } else if (
                                (
                                        $("[name='" + rep_param_w_at + "']").is('input') ||
                                        $("[name='" + rep_param_w_at + "']").is('textarea') ||
                                        $("[name='" + rep_param_w_at + "']").is('select')
                                        ) &&
                                $("[name='" + rep_param_w_at + "']").hasClass('getFields')
                                ) {
                            var at_selected_field_ele = $("[name='" + rep_param_w_at + "']");
                            var at_selected_field_ele_val = '';
                            if (at_selected_field_ele.is('[type="radio"]')) {
                                at_selected_field_ele_val = at_selected_field_ele.filter(':checked').val();
                            } else if (at_selected_field_ele.is('select:not([multiple])')) {
                                at_selected_field_ele_val = at_selected_field_ele.filter(':checked').val();
                            } else {
                                at_selected_field_ele_val = at_selected_field_ele.val();
                            }
                            var reg_exp = new RegExp(rep_param_w_at + "(?![a-zA-Z0-9_])", 'g');
                            string_default_val = string_default_val.replace(reg_exp, "\"" + at_selected_field_ele_val + "\"");
                        }
                    }

                }
                //console.log("end of testing great formula",string_default_val)
                if (string_default_val.indexOf("@") >= 0) {
                    string_default_val = string_default_val.replace(/(\^@|@)/g, function (data) {
                        if (data == "^@") {
                            return "@";
                        }
                        return "";
                    });
                }
                if (string_default_val.indexOf(/\'/g) >= 0) {
                    string_default_val = string_default_val.replace(/\'/g, "\'");
                }
                if (string_default_val.match(/\"/g) != null) {
                    string_default_val = string_default_val.replace(/\"/g, "\"");
                }
                if (string_default_val.match(/\n/g) != null) {
                    //string_default_val = string_default_val.replace(/\n/g, "\\n"); 
                }
                // // console.log(string_default_val)
                // if(string_default_val.match(/\\/g) != null){
                //  string_default_val = string_default_val.replace(/\\/g,"\\\\");
                // }
                // // console.log(string_default_val)
                // if(string_default_val.match(/\"/g) != null){
                //  string_default_val = string_default_val.replace(/\"/g,"\\\"");
                // }
                // console.log("TESTZZZ")
                // console.log(string_default_val)
                // alert("WHOA")

                if (string_default_val.indexOf("Lookup") >= 0) {

                } else {
                    var eval_parse_status = "";
                    try {
                        eval(String("" + string_default_val));
                        eval_parse_status = "ok";
                        console.log(this_self_ele, "ok", string_default_val);
                    } catch (e) {
                        eval_parse_status = "not ok";
                        console.log(this_self_ele, "not", string_default_val, e);
                    }
                    if (eval_parse_status == "ok") {
                        if (eval(String(string_default_val)) == "Infinity") {
                            if ($(this_self_ele).find(".getFields_" + data_obj_id).prop("tagName") == "SELECT") {

                            } else {
                                $(this_self_ele).find(".getFields_" + data_obj_id).val("Cannot divide by zero");
                            }

                        } else if (eval(String(string_default_val)) == "Invalid Date") {
                            //do not place any value
                        } else {

                            var fieldWithComputation = $(this_self_ele).find(".getFields_" + data_obj_id);
                            if (fieldWithComputation.attr("data-type") == "date") {
                                fieldWithComputation.val(eval(string_default_val));
                                if (fieldWithComputation.val() != "") {
                                    fieldWithComputation.val(ThisDate(fieldWithComputation.val()).formatDate("Y-m-d"));
                                }
                            } else if (fieldWithComputation.attr("data-type") == "dateTime") {
                                fieldWithComputation.val(eval(string_default_val));
                                if (fieldWithComputation.val() != "") {
                                    fieldWithComputation.val(ThisDate(fieldWithComputation.val()).formatDate("Y-m-d H:i"));
                                }
                            } else if (fieldWithComputation.attr("data-type") == "time") {
                                var temp = eval(string_default_val);
                                if ($.type(temp) == "date") {
                                    fieldWithComputation.val(ThisDate(temp).formatDate('H:i'));
                                } else {
                                    var temp2 = ThisDate(ThisDate(new Date()).formatDate('Y-m-d') + temp).formatDate('H:i');
                                    if (temp2 == "Invalid Date") {
                                        temp2 = ThisDate(temp).formatDate('H:i');
                                    }
                                    fieldWithComputation.val(temp2);
                                }
                            } else {
                                //THIS IS THE STARTUP VALUE
                                if ($(this_self_ele).find(".getFields_" + data_obj_id).prop("tagName") == "SELECT") {
                                    var eval_val = eval(String("[" + string_default_val + "]"));
                                    var temp_select_val = $(this_self_ele).find(".getFields_" + data_obj_id).children("option:selected").map(function () {
                                        return $(this).val();
                                    }).get();
                                    if (typeof eval_val === "object") {
                                        $(this_self_ele).find(".getFields_" + data_obj_id).html("");
                                        deployArrayValues(eval_val, function (i, v) {
                                            if (typeof v === "string") {
                                                if ($.trim(v) != "") {
                                                    $(this_self_ele).find(".getFields_" + data_obj_id).append('<option value="' + v + '">' + v + '</option>');
                                                }
                                            }
                                        });
                                        $(this_self_ele).find(".getFields_" + data_obj_id).children("option").each(function () {
                                            if (temp_select_val.indexOf($(this).val()) >= 0) {
                                                $(this).prop("selected", true);
                                            }
                                        });
                                    }
                                } else {
                                    // console.log("string_default_val",string_default_val,eval(String(string_default_val)),GetLocation)
                                    $(this_self_ele).find(".getFields_" + data_obj_id).val(eval(String(string_default_val)));
                                    if ($(this_self_ele).find(".getFields_" + data_obj_id).val() == "NaN") {
                                        $(this_self_ele).find(".getFields_" + data_obj_id).val("Not a valid computation")
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // }
        },
        "commitChangesDefaultVal": function () {
            var form_ele = $('.preview_content').find('#frmrequest').eq(0).find('.loaded_form_content').eq(0);
            var skip_ele_field = ['labelOnly', 'tab-panel', 'table', 'createLine', 'imageOnly'];
            form_ele.find('.setOBJ').each(function () {
                var dis_self = $(this);
                var doid = dis_self.attr('data-object-id');
                var setOBJ_data_type = dis_self.attr('data-type');
                var get_fields_ele = dis_self.find('.getFields_' + doid);
                if (!get_fields_ele.is('[default-type="computed"]:not([default-formula-value=""])')) {
                    return true;
                }
                if (skip_ele_field.indexOf(setOBJ_data_type) < 0) {
                    if (formulaOnload == false) {
                        return;
                    }
                    setTimeout(function () {
                        get_fields_ele.trigger('change', ["commitChangesDefaultVal"]);
                    }, 0);
                }
            });
            // var getFields = form_ele.find('.setOBJ').get();
            // AsyncLoop(getFields, function(a, b){
            //     var dis_self = $(b);
            //     var doid = dis_self.attr('data-object-id');
            //     var setOBJ_data_type = dis_self.attr('data-type');
            //     if (skip_ele_field.indexOf(setOBJ_data_type) < 0) {
            //         console.log("trigger now");
            //         if (formulaOnload == false) {
            //             return;
            //         }
            //         setTimeout(function() {
            //             dis_self.find('.getFields_' + doid).trigger('change');
            //         }, 0);
            //     }
            // });
        },
        "setDefaultValues": function () {//to be on progress sooner
            var this_ini_variant = this;
            var form_json_data = $("body").data()["user_form_json_data"];
            if (typeof form_json_data != "undefined" && form_json_data != '') {

                if ($(".setOBJ").length >= 1) {
                    //fields having a defaultfields
                    //set static values
                    $(".setOBJ").each(function (eqi_setOBJ) {
                        var data_obj_id = $(this).attr("data-object-id");
                        var the_field = $(this).find(".getFields_" + data_obj_id);
                        if (the_field.attr("default-type")) {
                            if (the_field.attr("default-type") == "static") {
                                if (typeof the_field.attr("default-static-value") != "undefined") {
                                    if (the_field.attr("default-static-value").length >= 1) {
                                        if (String(the_field.val()).length <= 0) {
                                            the_field.val(the_field.attr("default-static-value"));
                                        }
                                    }
                                }
                            } else if (the_field.attr("default-type") == "computed") {

                            }
                        }
                    });
                    //set default values
                    $(".setOBJ").each(function (eqi_setOBJ) {
                        var data_obj_id = $(this).attr("data-object-id");
                        //try{
                        if (typeof form_json_data.form_json["" + data_obj_id] != "undefined") {
                            var the_field = $(this).find(".getFields_" + data_obj_id);
                            if (
                                    the_field.attr("type") && (
                                    the_field.attr("type") == "radio" ||
                                    the_field.attr("type") == "checkbox"
                                    )
                                    ) {
                                if (typeof form_json_data.form_json["" + data_obj_id].defaultValues != "undefined") {
                                    $.each(form_json_data.form_json["" + data_obj_id].defaultValues, function (index, item) {
                                        var radio_check_strvalue = form_json_data.form_json["" + data_obj_id].defaultValues[index];
                                        if ($(the_field).eq(index).length >= 1 && $(the_field).eq(index).val() == radio_check_strvalue) {
                                            if ($("#getID").val() == "0") {
                                                $(the_field).eq(index).prop("checked", true);
                                            }
                                        }
                                    });
                                }
                            } else if (the_field.prop("tagName") == "SELECT") {
                                // if ($('[name="ID"]').val() == 0 || $('[name="ID"]').val() == "0") {
                                //FIX ISSUE FOR DEFAULT VALUE OF THE SELECT ... SINCE ON THE FORMBUILDER THE OPTION WAS ALREADY SET SELECTED ON HTML percisely
                                // if( (the_field.children('option:eq(0)').attr('value')||"") ==  the_field.val() ){
                                //     if (typeof form_json_data.form_json["" + data_obj_id].defaultValues != "undefined") {
                                //         var temporary = form_json_data.form_json["" + data_obj_id].defaultValues;
                                //         if ($.type(temporary) == "array") {
                                //             form_json_data.form_json["" + data_obj_id].defaultValues = temporary.filter(Boolean);
                                //         }
                                //         $.each(form_json_data.form_json["" + data_obj_id].defaultValues, function (index, item) {
                                //             var select_strval = form_json_data.form_json["" + data_obj_id].defaultValues[index];
                                //             if($(the_field).children("option").is('[value="'+select_strval+'"]')){
                                //                 $(the_field).children('option[value="'+select_strval+'"]').prop("selected", true);
                                //             }
                                //             //old
                                //             // if ($(the_field).children("option").eq(index).length >= 1 && $(the_field).children("option").eq(index).val() == select_strval) {
                                //             //     $(the_field).children("option").eq(index).prop("selected", true);
                                //             // }
                                //         });
                                //     }
                                // }
                                if ($(this).find(".getFields_" + data_obj_id).attr("default-type") == "computed") {
                                    this_ini_variant.defaultComputed(data_obj_id, the_field, this, "startUpDefaultValComputed");
                                }
                                // }
                            } else {
                                if ($(this).find(".getFields_" + data_obj_id).attr("default-type")) {
                                    if ($(this).find(".getFields_" + data_obj_id).attr("default-type") == "static") {
                                        if (typeof $(this).find(".getFields_" + data_obj_id).attr("default-static-value") != "undefined") {
                                            if ($(this).find(".getFields_" + data_obj_id).attr("default-static-value").length >= 1) {
                                                // console.log($(this).find(".getFields_" + data_obj_id).attr("name"))
                                                if (String($(this).find(".getFields_" + data_obj_id).val()).length <= 0) {
                                                    $(this).find(".getFields_" + data_obj_id).val($(this).find(".getFields_" + data_obj_id).attr("default-static-value"));
                                                }
                                            }
                                        }
                                    } else if ($(this).find(".getFields_" + data_obj_id).attr("default-type") == "computed") {
                                        this_ini_variant.defaultComputed(data_obj_id, the_field, this, "startUpDefaultValComputed");
                                    }
                                }
                            }
                        }
                        // }catch(error){
                        //    console.log("ERROR on getting the json datas of this individual field");
                        //    console.log("OR the json variable form_json_data.form_json[''+"+data_obj_id+"] is not existing this id:->["+data_obj_id+"]");
                        //    console.log("OR the values of computed is not well defined");
                        //    console.log(error)
                        // }
                        // console.log($('[name="Date_Hired"]').val())
                        // console.log($(this).find(".getFields_" + data_obj_id).attr("name"))
                        // console.log($(this).find(".getFields_" + data_obj_id).attr("default-computed-value"))
                    });
                    // $('[default-type="computed"][default-formula-value]').change();
                }
                AsyncLoop($('[default-type="static"]'), function () {
                    $(this).trigger("change", ["SetDefault"]);
                });
                // $('[default-type="static"]').change();
            } else {
                console.log("form_json_data is undefined");
            }
        },
        // "makeOnChange": function(fieldAffected, fieldWithComputationParam, eventAddressNamespace) {
        //     var this_ini_variant = this;
        //     var fieldWithComputation = $(fieldWithComputationParam);
        //     var this_ini_variant = this;
        //     // console.log("check affected")
        //     // console.log(fieldAffected)
        //     if (fieldWithComputation.attr("default-formula-value")) {
        //         // if(fieldWithComputation.attr("default-formula-value").indexOf("Lookup") >= 0){
        //         // populateFormula();

        //         // }else{
        //         var event_string = "keyup change";
        //         if (typeof eventAddressNamespace == "undefined") {
        //             //do nothing

        //         } else {
        //             event_string = "keyup." + eventAddressNamespace + " change." + eventAddressNamespace;//namespace added for destroying repeated events
        //         }
        //         // console.log(event_string)
        //         // console.log("eto na ba un")
        //         // console.log(fieldAffected)
        //         if ($(fieldAffected).attr("event-id-from")) {
        //             var ids_event_from = $(fieldAffected).attr("event-id-from");
        //             var array_ids_event_from = ids_event_from.split(",");
        //             if (array_ids_event_from.indexOf(fieldWithComputation.closest(".setOBJ").attr("data-object-id")) >= 0) {

        //             } else {
        //                 $(fieldAffected).attr("event-id-from", $(fieldAffected).attr("event-id-from") + "," + fieldWithComputation.closest(".setOBJ").attr("data-object-id"));
        //             }
        //         } else {
        //             //console.log($(fieldAffected).attr("event-id-from", "" + fieldWithComputation.closest(".setOBJ").attr("data-object-id")));
        //         }
        //         // console.log("HAYS")
        //         // console.log(fieldWithComputation)

        //         $(fieldAffected).on(event_string, {"fieldWithComputation": fieldWithComputation}, function(e) {
        //             // e.stopPropagation();//AYAW GUMANA :<<<
        //             // event.stopPropagation();//AYAW GUMANA :<<<
        //             var fieldWithComputation = e.data.fieldWithComputation;
        //             // try{
        //             // console.log("ETO NGA BA UN?")
        //             // console.log(fieldWithComputation)
        //             var string_default_val = "" + fieldWithComputation.attr("default-formula-value");
        //             // console.log("eto ung formula")
        //             // console.log(string_default_val)
        //             // console.log(fieldWithComputation)
        //             if (string_default_val.indexOf("@Lookup") >= 0) {
        //                 return;
        //             }
        //             if (string_default_val.indexOf("@LookupWhere") >= 0) {
        //                 return;
        //             }
        //             if (string_default_val.indexOf("@LookupGetMin") >= 0) {
        //                 return;
        //             }
        //             if (string_default_val.indexOf("@LookupGetMax") >= 0) {
        //                 return;
        //             }
        //             if (string_default_val.indexOf("@LookupSDev") >= 0) {
        //                 return;
        //             }
        //             if (string_default_val.indexOf("@LookupAVG") >= 0) {
        //                 return;
        //             }
        //             if (string_default_val.indexOf(/^@Total\(*.\)$/) >= 0) {
        //                 return;
        //             }
        //             var string_field_formula = string_default_val;
        //             try {
        //                 splitted_default_val = string_default_val.match(/@GetAVG\(@[A-Za-z0-9_]*\)|@SDev\(@[A-Za-z0-9_]*\)|@GetMin\(@[A-Za-z0-9_]*\)|@GetMax\(@[A-Za-z0-9_]*\)|@Sum\(@[A-Za-z0-9_]*\)|@[a-zA-Z0-9_]*\[[A-Za-z]*\]|@[a-zA-Z0-9_]*/g);
        //             } catch (error) {
        //                 //console.log("the formula value is not defined");
        //             }
        //             var is_number = "true";
        //             for (ctr = 0; ctr < splitted_default_val.length; ctr++) {
        //                 replace_param1 = splitted_default_val[ctr].trim();
        //                 if (replace_param1.match(/@Sum\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
        //                     replace_param1 = splitted_default_val[ctr].trim().match(/@Sum\((@[A-Za-z][A-Za-z0-9_]*)\)/);
        //                     replace_param1 = replace_param1[1].replace("@", "");
        //                 } else if (replace_param1.match(/@GetMax\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
        //                     replace_param1 = splitted_default_val[ctr].trim().match(/@GetMax\((@[A-Za-z][A-Za-z0-9_]*)\)/);
        //                     replace_param1 = replace_param1[1].replace("@", "");
        //                 } else if (replace_param1.match(/@GetMin\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
        //                     replace_param1 = splitted_default_val[ctr].trim().match(/@GetMin\((@[A-Za-z][A-Za-z0-9_]*)\)/);
        //                     replace_param1 = replace_param1[1].replace("@", "");
        //                 } else if (replace_param1.match(/@SDev\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
        //                     replace_param1 = splitted_default_val[ctr].trim().match(/@SDev\((@[A-Za-z][A-Za-z0-9_]*)\)/);
        //                     replace_param1 = replace_param1[1].replace("@", "");
        //                 } else if (replace_param1.match(/@GetAVG\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
        //                     replace_param1 = splitted_default_val[ctr].trim().match(/@GetAVG\((@[A-Za-z][A-Za-z0-9_]*)\)/);
        //                     replace_param1 = replace_param1[1].replace("@", "");
        //                 } else {
        //                     replace_param1 = splitted_default_val[ctr].trim().replace("@", "");
        //                 }
        //                 element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');

        //                 if ($(fieldWithComputation).parents("tr").length >= 1) {
        //                     if ($(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]').length >= 1) {
        //                         element_in_computation = $(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                     } else if ($(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]').length >= 1) {
        //                         element_in_computation = $(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                         // console.log($(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
        //                     } else {
        //                         element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                     }
        //                 } else {
        //                     element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                 }

        //                 if (element_in_computation.length >= 1) {
        //                 } else {
        //                     continue;
        //                 }

        //                 if (element_in_computation.attr("data-input-type") && element_in_computation.attr("data-input-type") == "Currency") {
        //                     replace_param2 = element_in_computation.val().replace(/,/g, "");
        //                 } else {
        //                     replace_param2 = element_in_computation.val();
        //                 }

        //                 if ($.isNumeric($.trim(replace_param2))) {

        //                 } else {
        //                     is_number = "false";
        //                     break;
        //                 }
        //             }
        //             for (ctr = 0; ctr < splitted_default_val.length; ctr++) {
        //                 replace_param1 = splitted_default_val[ctr].trim();
        //                 if (replace_param1.match(/@Sum\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {

        //                     var second_fn = "@Sum";
        //                 }
        //                 if (replace_param1.match(/@GetMax\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {
        //                     var second_fn = "@GetMax";
        //                 }
        //                 if (replace_param1.match(/@GetMin\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {

        //                     var second_fn = "@GetMin";
        //                 }
        //                 if (string_default_val.match(/@SDev\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {

        //                     var second_fn = "@SDev";
        //                 }
        //                 if (string_default_val.match(/@GetAVG\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {

        //                     var second_fn = "@GetAVG";
        //                 }
        //                 if ($(fieldWithComputation).parents("tr").length >= 1) {
        //                     if ($(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]').length >= 1) {
        //                         element_in_computation = $(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                         // console.log($(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
        //                         // }else{
        //                         //     element_in_computation = $(fieldWithComputation).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                         //     console.log($(fieldWithComputation).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
        //                         // }
        //                     } else if ($(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]').length >= 1) {
        //                         element_in_computation = $(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                         // console.log($(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
        //                     } else {
        //                         element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                     }
        //                 } else {
        //                     element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                     // console.log($(fieldWithComputation).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
        //                 }
        //                 // element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
        //                 if (replace_param1 == "") {
        //                     continue;
        //                 }
        //                 if (element_in_computation.length >= 1 && (string_default_val.match(/@GetAVG/) == null && string_default_val.match(/@SDev/) == null && replace_param1.match(/@Sum/) == null && replace_param1.match(/@GetMax/) == null && replace_param1.match(/@GetMin/) == null)) {
        //                     var field_type = "";
        //                     if (typeof element_in_computation.attr("type") !== "undefined") {
        //                         if (element_in_computation.attr("type") == "radio") {
        //                             field_type = "radio";
        //                         }
        //                     }
        //                     if (element_in_computation.attr("data-input-type") && element_in_computation.attr("data-input-type") == "Currency") {
        //                         replace_param2 = element_in_computation.val().replace(/,/g, "");
        //                     } else {
        //                         if (field_type == "radio") {
        //                             replace_param2 = $('[name="' + element_in_computation.attr("name") + '"]:checked').val();
        //                             if (!replace_param2) {
        //                                 replace_param2 = "";
        //                             }
        //                         } else {
        //                             replace_param2 = element_in_computation.val();
        //                         }

        //                     }

        //                     if (is_number == "false") {
        //                         if (element_in_computation.attr("data-type") == "float") {
        //                             string_default_val = string_default_val.replace(replace_param1, "Number('" + "" + replace_param2.replace(/\"/g, "\\\"") + "" + "')");
        //                         } else if (element_in_computation.attr("data-type") == "longtext") {
        //                             replace_param2 = replace_param2.replace(/\\/g, "\\\\");
        //                             replace_param2 = replace_param2.replace(/\"/g, "\\\"");
        //                             // string_default_val = string_default_val.replace(replace_param1,"\""+replace_param2.replace(/\\/g,"\\\\")+"\"");
        //                             string_default_val = string_default_val.replace(replace_param1, "\"" + replace_param2 + "\"");
        //                         } else {
        //                             string_default_val = string_default_val.replace(replace_param1, "\"" + replace_param2.replace(/\"/g, "\\\"") + "\"");
        //                         }
        //                     } else {
        //                         string_default_val = string_default_val.replace(replace_param1, replace_param2);
        //                     }
        //                 } else if (element_in_computation.length == 0 || (string_default_val.match(/@SDev/) != null || replace_param1.match(/@Sum/) != null || replace_param1.match(/@GetMax/) != null || replace_param1.match(/@GetMin/) != null)) {

        //                     if (typeof second_fn !== "undefined") { //di na kailangan nito
        //                         // if(second_fn == "@Sum"){
        //                         //     replace_param1 = "@Sum("+replace_param1+")";
        //                         // }else if(second_fn == "@GetMax"){
        //                         //     replace_param1 = "@GetMax("+replace_param1+")";
        //                         // }else if(second_fn == "@GetMin"){
        //                         //     replace_param1 = "@GetMin("+replace_param1+")";
        //                         // }
        //                     }
        //                     if (replace_param1.indexOf("@Today") >= 0) {
        //                         var this_today = new Date(this_ini_variant.getUpdatedServerTime());
        //                         this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2);
        //                         string_default_val = string_default_val.replace(replace_param1, "\"" + this_today + "\"");
        //                     } else if (replace_param1.indexOf("@Now") >= 0) {
        //                         var this_today = new Date(this_ini_variant.getUpdatedServerTime());
        //                         this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2) + " " + StrRight("0" + this_today.getHours(), 2) + ":" + StrRight("0" + this_today.getMinutes(), 2) + ":" + StrRight("0" + this_today.getSeconds(), 2);
        //                         string_default_val = string_default_val.replace(replace_param1, "\"" + this_today + "\"");
        //                     } else if (replace_param1.indexOf("@RequestID") >= 0) {
        //                         var getID = $("#ID").val();
        //                         string_default_val = string_default_val.replace(replace_param1, "\"" + getID + "\"");
        //                     } else if (replace_param1.indexOf("@Department") >= 0) {
        //                         var getDepName = $("[name='Department_Name']").val();
        //                         string_default_val = string_default_val.replace(replace_param1, "\"" + getDepName + "\"");
        //                     } else if (replace_param1.match(/\[[A-Za-z]*\]/) != null) {//FOR SUM AVG ETC FORMULA
        //                         var fieldName = replace_param1.replace(/\[[A-Za-z]*\]|@/g, "");
        //                         var formula_function = replace_param1.match(/\[[A-Za-z]*\]/);
        //                         var get_fields_value = "(";
        //                         if (formula_function[0] == "[sum]") {
        //                             get_fields_value = get_fields_value + "Number('" + $('[name="' + fieldName + '"]').val() + "')";
        //                             $('[rep-original-name="' + fieldName + '"]').each(function() {
        //                                 get_fields_value = get_fields_value + " + Number('" + $(this).val() + "')";
        //                             });
        //                             get_fields_value = get_fields_value + ")";
        //                             string_default_val = string_default_val.replace(replace_param1, "" + get_fields_value + "");
        //                         }
        //                         if (formula_function[0] == "[Last]") {
        //                             if ($('[rep-original-name="' + fieldName + '"]').length >= 1) {
        //                                 var element_in_computation_last = $('[rep-original-name="' + fieldName + '"]').last();
        //                                 if ($.isNumeric(element_in_computation_last.val())) {
        //                                     get_fields_value = get_fields_value + "" + element_in_computation_last.val() + "";
        //                                 } else {
        //                                     get_fields_value = get_fields_value + "'" + element_in_computation_last.val() + "'";
        //                                 }
        //                                 get_fields_value = get_fields_value + ")";
        //                                 string_default_val = string_default_val.replace(replace_param1, "" + get_fields_value + "");
        //                             } else if ($('[name="' + fieldName + '"]').length >= 1) {
        //                                 var element_in_computation_last = $('[name="' + fieldName + '"]').last();
        //                                 if ($.isNumeric(element_in_computation_last.val())) {
        //                                     get_fields_value = get_fields_value + "" + element_in_computation_last.val() + "";
        //                                 } else {
        //                                     get_fields_value = get_fields_value + "'" + element_in_computation_last.val() + "'";
        //                                 }
        //                                 get_fields_value = get_fields_value + ")";
        //                                 string_default_val = string_default_val.replace(replace_param1, "" + get_fields_value + "");
        //                             }
        //                         }
        //                     } else if (replace_param1.match(/@Sum\(@[A-Za-z0-9_]*\)/g) != null) { //FOR @Sum(@Fieldname)
        //                         fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
        //                         var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
        //                         if (fieldExists("\"" + fieldName[1] + "\"") == true) {
        //                             string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
        //                         }
        //                     } else if (replace_param1.match(/@GetMax\(@[A-Za-z0-9_]*\)/g) != null) { //

        //                         fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
        //                         var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
        //                         if (fieldExists("\"" + fieldName[1] + "\"") == true) {
        //                             string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
        //                         }
        //                     } else if (replace_param1.match(/@GetMin\(@[A-Za-z0-9_]*\)/g) != null) { //
        //                         fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
        //                         var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
        //                         if (fieldExists("\"" + fieldName[1] + "\"") == true) {
        //                             string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
        //                         }
        //                     } else if (replace_param1.match(/@SDev\(@[A-Za-z0-9_]*\)/g) != null) { //
        //                         fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
        //                         var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
        //                         if (fieldExists("\"" + fieldName[1] + "\"") == true) {
        //                             string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
        //                         }
        //                     } else if (replace_param1.match(/@GetAVG\(@[A-Za-z0-9_]*\)/g) != null) { //

        //                         fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
        //                         var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
        //                         if (fieldExists("\"" + fieldName[1] + "\"") == true) {
        //                             string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
        //                         }

        //                     }
        //                 }
        //             }
        //             if (string_default_val.indexOf("@") >= 0) {
        //                 string_default_val = string_default_val.replace(/@/g, "");
        //             }
        //             if (string_default_val.indexOf(/\'/g) >= 0) {
        //                 string_default_val = string_default_val.replace(/\'/g, "\'");
        //             }
        //             // console.log(string_default_val)
        //             // if(string_default_val.match(/\\/g) != null){
        //             //  string_default_val = string_default_val.replace(/\\/g,"\\\\");
        //             // }
        //             // console.log(string_default_val)
        //             if (string_default_val.match(/\"/g) != null) {
        //                 string_default_val = string_default_val.replace(/\"/g, "\"");
        //             }
        //             // console.log("TEST")
        //             // console.log(string_default_val)
        //             var eval_status = "not ok";
        //             try {
        //                 eval(string_default_val);
        //                 eval_status = "ok";
        //                 // console.log("OK",string_default_val,fieldWithComputation)
        //             } catch (error) {
        //                 eval_status = "not ok";
        //                 // console.log("NOT OK",string_default_val,fieldWithComputation,error);
        //             }
        //             // console.log("NAKARATING??")
        //             // console.log(fieldWithComputation)
        //             // console.log($(this))
        //             if (eval_status == "ok") {
        //                 if (string_default_val.indexOf("Lookup") >= 0) {
        //                     //data = [];
        //                     //fieldName = fieldWithComputation.attr("name");
        //                     //formula = string_default_val;
        //                     //data.push({
        //                     //    FieldName: fieldName,
        //                     //    Formula: formula
        //                     //});
        //                     //$.get('/ajax/ajaxformula', {
        //                     //    data: JSON.stringify(data)
        //                     //}, function(data) {
        //                     //    data = JSON.parse(data);
        //                     //    console.log("uung sa onchange");
        //                     //    console.log(data);
        //                     //    $('[name="' + data[0].FieldName + '"]').val(data[0].FieldValue);
        //                     //});
        //                 } else if (string_default_val.indexOf("RecordCount") >= 0) {
        //                     //
        //                 } else {
        //                     if (eval(string_default_val) == "Infinity") {
        //                         fieldWithComputation.val("Cannot divide by zero");
        //                     } else if (eval(string_default_val) == "Invalid Date") {
        //                         //do not place any value
        //                     } else {
        //                         if (fieldWithComputation.hasClass("hasDatepicker") && fieldWithComputation.attr("data-type") == "date") {
        //                             fieldWithComputation.val(eval(string_default_val));
        //                             fieldWithComputation.val(ThisDate(fieldWithComputation.val()).formatDate("Y-m-d"));
        //                         } else if (fieldWithComputation.hasClass("hasDatepicker") && fieldWithComputation.attr("data-type") == "dateTime") {
        //                             fieldWithComputation.val(eval(string_default_val));
        //                             fieldWithComputation.val(ThisDate(fieldWithComputation.val()).formatDate("Y-m-d H:i"));
        //                         } else {
        //                             fieldWithComputation.val(eval(string_default_val));
        //                             if (fieldWithComputation.val() == "NaN") {
        //                                 fieldWithComputation.val("Not a valid computation")
        //                             }
        //                         }
        //                     }
        //                 }
        //             } else {
        //                 //console.log(fieldWithComputation,"EVAL STAT NOT OK");
        //                 //console.log(string_default_val)
        //             }


        //             // }catch(error){
        //             //   alert("nag error??")
        //             // }
        //             // if(fieldWithComputation.attr("default-formula-value").indexOf("GivenIf") >= 0 ){
        //             //   fieldWithComputation.change();
        //             // }
        //             var reg_eks = new RegExp("@" + fieldWithComputation.attr("name") + "(?![a-zA-Z0-9_])", "g");
        //             if (fieldWithComputation.attr("default-formula-value").match(reg_eks) == null) {
        //                 fieldWithComputation.change();
        //             }
        //             //console.log(string_default_val)
        //             //console.log("\n\n\n\n\n\n\n\n\n\n\n\n")
        //         });
        //         //}
        //     }

        // },
        "makeOnChange": function (fieldAffected, fieldWithComputationParam, eventAddressNamespace) {
            var fieldname_keywords = [
                "FormID", "WorkflowId", "ID",
                "Requestor", "CurrentUser", "Status",
                "Processor", "LastAction", "DateCreated",
                "DateUpdated", "CreatedBy", "UpdatedBy",
                "Unread", "Node_ID", "Workflow_ID",
                "Mode", "TrackNo", "imported",
                "computedFields", "fieldEnabled", "KeywordsField",
                "Repeater_Data", "Department_Name", "From",
                "To", "Range", "requestorname", "Auth"
            ];
            var this_ini_variant = this;
            var fieldWithComputation = $(fieldWithComputationParam);
            // console.log("check affected")
            // console.log(fieldAffected)
            if (fieldWithComputation.attr("default-formula-value")) {
                var event_string = "keyup change";
                // var field_affected = $(fieldAffected);
                // var field_typing = field_affected.filter(function(index, elem){
                //     if($(elem).prop("tagName") == "TEXTAREA") {
                //         return true;
                //     }else if($(elem).attr("type")){
                //         if($(elem).attr("type") == "text" && !$(elem).hasClass("hasDatepicker")){
                //             return true;
                //         }
                //     }
                // });
                // if(field_typing.length >= 1 ){
                //     var event_string = "keyup";
                // }else{
                //     var event_string = "change";
                // }

                if (typeof eventAddressNamespace == "undefined") {
                    //do nothing

                } else {
                    event_string = "keyup." + eventAddressNamespace + " change." + eventAddressNamespace;//namespace added for destroying repeated events
                    // var field_affected = $(fieldAffected);
                    // if(
                    //     field_affected.prop("tagName") == "TEXTAREA" ||
                    //     field_affected.attr("type") == "text"
                    // ){
                    //     event_string = "keyup." + eventAddressNamespace;
                    // }else{
                    //     event_string = "change." + eventAddressNamespace;
                    // }
                }
                // console.log(event_string)
                // console.log("eto na ba un")
                // console.log(fieldAffected)
                if ($(fieldAffected).attr("event-id-from")) {
                    var ids_event_from = $(fieldAffected).attr("event-id-from");
                    var array_ids_event_from = ids_event_from.split(",");
                    if (array_ids_event_from.indexOf(fieldWithComputation.closest(".setOBJ").attr("data-object-id")) >= 0) {

                    } else {
                        $(fieldAffected).attr("event-id-from", $(fieldAffected).attr("event-id-from") + "," + fieldWithComputation.closest(".setOBJ").attr("data-object-id"));
                    }
                } else {
                    //console.log($(fieldAffected).attr("event-id-from", "" + fieldWithComputation.closest(".setOBJ").attr("data-object-id")));
                }
                // console.log("HAYS")
                // console.log(fieldWithComputation)

                $(fieldAffected).on(event_string, {"fieldWithComputation": fieldWithComputation}, function (e, data) {

                    if (data) {
                        if (typeof data["name_collection"] === "object") {

                            name_collection = data["name_collection"];
                            // console.log("MOChange NAME COLLECTION","    CHAINING"+name_collection.length,"    etype:",e.type,"     "+$(this).attr("name"),"     collected_name:",data)
                        } else {
                            name_collection = null;
                        }
                    } else {
                        // console.log("MOChange NAME COLLECTION","    STARTPNT","    etype:",e.type,"     "+$(this).attr("name"),"     collected_name:",data)
                        name_collection = null;
                    }
                    if (formulaOnload == false) {
                        return;
                    }
                    var event_type = e.type;
                    var fieldname_keywords = [
                        "FormID", "WorkflowId", "ID",
                        "Requestor", "CurrentUser", "Status",
                        "Processor", "LastAction", "DateCreated",
                        "DateUpdated", "CreatedBy", "UpdatedBy",
                        "Unread", "Node_ID", "Workflow_ID",
                        "Mode", "TrackNo", "imported",
                        "computedFields", "fieldEnabled", "KeywordsField",
                        "Repeater_Data", "Department_Name", "From",
                        "To", "Range", "requestorname", "Auth"
                    ];
                    // alert("KULANGOT")
                    // e.stopPropagation();//AYAW GUMANA :<<<
                    // event.stopPropagation();//AYAW GUMANA :<<<
                    var fieldWithComputation = e.data.fieldWithComputation;
                    // try{
                    // console.log("ETO NGA BA UN?")
                    // console.log(fieldWithComputation)
                    var string_default_val = "" + fieldWithComputation.attr("default-formula-value");
                    // console.log("eto ung formula")
                    // console.log(string_default_val)
                    // console.log(fieldWithComputation)
                    if (string_default_val.indexOf("@Lookup") >= 0) {
                        return;
                    }
                    if (string_default_val.indexOf("@LookupWhere") >= 0) {
                        return;
                    }
                    if (string_default_val.indexOf("@CustomScript") >= 0) {
                        return;
                    }
                    if (string_default_val.indexOf("@LookupGetMin") >= 0) {
                        return;
                    }
                    if (string_default_val.indexOf("@LookupGetMax") >= 0) {
                        return;
                    }
                    if (string_default_val.indexOf("@LookupSDev") >= 0) {
                        return;
                    }
                    if (string_default_val.indexOf("@LookupAVG") >= 0) {
                        return;
                    }
                    if (string_default_val.indexOf(/^@Total\(*.\)$/) >= 0) {
                        return;
                    }
                    var string_field_formula = string_default_val;
                    try {
                        splitted_default_val = string_default_val.match(/@GetAVG\(@[A-Za-z0-9_]*\)|@SDev\(@[A-Za-z0-9_]*\)|@GetMin\(@[A-Za-z0-9_]*\)|@GetMax\(@[A-Za-z0-9_]*\)|@Sum\(@[A-Za-z0-9_]*\)|@(\[[a-zA-Z0-9_\'\"]*\]){3}|@[A-Za-z0-9_]*/g);
                    } catch (error) {
                        //console.log("the formula value is not defined");
                    }
                    var is_number = "true";
                    for (ctr = 0; ctr < splitted_default_val.length; ctr++) {
                        replace_param1 = splitted_default_val[ctr].trim();
                        if (replace_param1.match(/@Sum\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                            replace_param1 = splitted_default_val[ctr].trim().match(/@Sum\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                            replace_param1 = replace_param1[1].replace("@", "");
                        } else if (replace_param1.match(/@GetMax\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                            replace_param1 = splitted_default_val[ctr].trim().match(/@GetMax\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                            replace_param1 = replace_param1[1].replace("@", "");
                        } else if (replace_param1.match(/@GetMin\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                            replace_param1 = splitted_default_val[ctr].trim().match(/@GetMin\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                            replace_param1 = replace_param1[1].replace("@", "");
                        } else if (replace_param1.match(/@SDev\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                            replace_param1 = splitted_default_val[ctr].trim().match(/@SDev\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                            replace_param1 = replace_param1[1].replace("@", "");
                        } else if (replace_param1.match(/@GetAVG\(@[A-Za-z][A-Za-z0-9_]*\)/g) != null) {
                            replace_param1 = splitted_default_val[ctr].trim().match(/@GetAVG\((@[A-Za-z][A-Za-z0-9_]*)\)/);
                            replace_param1 = replace_param1[1].replace("@", "");
                        } else {
                            replace_param1 = splitted_default_val[ctr].trim().replace("@", "");
                        }

                        element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');

                        if ($(fieldWithComputation).parents("tr").length >= 1) {
                            if ($(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]').length >= 1) {
                                element_in_computation = $(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                            } else if ($(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]').length >= 1) {
                                element_in_computation = $(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                                // console.log($(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
                            } else {
                                element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                            }
                        } else {
                            element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                        }

                        if (element_in_computation.length >= 1) {
                        } else {
                            continue;
                        }

                        if (element_in_computation.attr("data-input-type") && element_in_computation.attr("data-input-type") == "Currency") {
                            replace_param2 = element_in_computation.val().replace(/,/g, "");
                        } else {
                            replace_param2 = element_in_computation.val();
                        }

                        if ($.isNumeric($.trim(replace_param2))) {

                        } else {
                            console.log(replace_param2, $.trim(replace_param1.split(/@|\(|\)/g).join("")), $.isNumeric($.trim(replace_param2)))
                            is_number = "false";
                            break;
                        }
                    }
                    for (ctr = 0; ctr < splitted_default_val.length; ctr++) {
                        replace_param1 = splitted_default_val[ctr].trim();
                        if (replace_param1.match(/@Sum\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {

                            var second_fn = "@Sum";
                        }
                        if (replace_param1.match(/@GetMax\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {
                            var second_fn = "@GetMax";
                        }
                        if (replace_param1.match(/@GetMin\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {

                            var second_fn = "@GetMin";
                        }
                        if (string_default_val.match(/@SDev\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {

                            var second_fn = "@SDev";
                        }
                        if (string_default_val.match(/@GetAVG\((@[A-Za-z][A-Za-z0-9_]*)\)/) != null) {

                            var second_fn = "@GetAVG";
                        }
                        if ($(fieldWithComputation).parents("tr").length >= 1) {
                            if ($(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]').length >= 1) {
                                element_in_computation = $(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                                // console.log($(fieldWithComputation).parents("tr").find('[rep-original-name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
                                // }else{
                                //     element_in_computation = $(fieldWithComputation).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                                //     console.log($(fieldWithComputation).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
                                // }
                            } else if ($(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]').length >= 1) {
                                element_in_computation = $(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                                // console.log($(this_self_ele).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
                            } else {
                                element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                            }
                        } else {
                            element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                            // console.log($(fieldWithComputation).parents("tr").find('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]'));
                        }
                        // element_in_computation = $('[name="' + $.trim(replace_param1.split(/@|\(|\)/g).join("")) + '"]');
                        if (replace_param1 == "") {
                            continue;
                        }
                        if (element_in_computation.length <= 0) {
                            element_in_computation = $('[name="' + replace_param1.replace("@", "") + '[]"]');
                            // console.log("element_in_computation",element_in_computation)
                        }
                        //console.log("element_in_computation", element_in_computation, element_in_computation.is("input[type='checkbox']"), element_in_computation.attr('default-formula-value'))
                        // console.log("WORMHOLDE",splitted_default_val[ctr].replace("@", "") , fieldname_keywords, fieldname_keywords.indexOf(splitted_default_val[ctr].replace("@", "")) , fieldname_keywords.indexOf(splitted_default_val[ctr].replace("@", "")) <= -1);
                        if (element_in_computation.length >= 1 && fieldname_keywords.indexOf(splitted_default_val[ctr].replace("@", "")) <= -1 && (string_default_val.match(/@GetAVG/) == null && string_default_val.match(/@SDev/) == null && replace_param1.match(/@Sum/) == null && replace_param1.match(/@GetMax/) == null && replace_param1.match(/@GetMin/) == null)) {
                            var field_type = "";
                            if (typeof element_in_computation.attr("type") !== "undefined") {
                                if (element_in_computation.attr("type") == "radio") {
                                    field_type = "radio";
                                } else if (element_in_computation.is("input[type='checkbox']")) {
                                    field_type = "checkbox";
                                }
                            }
                            else if (element_in_computation.is("select[multiple='multiple']")) {
                                field_type = 'select_many';
                            }


                            if (element_in_computation.attr("data-input-type") && element_in_computation.attr("data-input-type") == "Currency") {
                                replace_param2 = element_in_computation.val().replace(/,/g, "");
                            } else {
                                if (field_type == "radio") {
                                    replace_param2 = element_in_computation.filter(':checked').map(function (a, b) {
                                        return $(b).val()
                                    }).get().filter(Boolean).join("|^|");
                                    if (!replace_param2) {
                                        replace_param2 = "";
                                    }
                                } else if (field_type == "select_many") {
                                    replace_param2 = element_in_computation.children('option:selected').map(function (a, b) {
                                        return $(b).val();
                                    }).get().filter(Boolean).join("|^|") || "";
                                } else if (field_type == "checkbox") {
                                    replace_param2 = element_in_computation.filter(':checked').map(function (a, b) {
                                        return $(b).val();
                                    }).get().filter(Boolean).join("|^|") || "";
                                } else {
                                    replace_param2 = element_in_computation.val();
                                }

                            }

                            if (is_number == "false") {
                                if (element_in_computation.attr("data-type") == "float" || element_in_computation.attr("data-type") == "double") {
                                    string_default_val = string_default_val.replace(replace_param1, "Number('" + "" + replace_param2.replace(/\"/g, "\\\"") + "" + "')");
                                } else if (element_in_computation.attr("data-type") == "longtext") {
                                    if (replace_param2) {
                                        replace_param2 = replace_param2.replace(/\\/g, "\\\\");
                                        replace_param2 = replace_param2.replace(/\"/g, "\\\"");
                                        // string_default_val = string_default_val.replace(replace_param1,"\""+replace_param2.replace(/\\/g,"\\\\")+"\"");
                                        string_default_val = string_default_val.replace(replace_param1, "\"" + replace_param2 + "\"");
                                    } else {
                                        string_default_val = string_default_val.replace(replace_param1, "\"" + replace_param2 + "\"");
                                    }
                                } else {
                                    string_default_val = string_default_val.replace(replace_param1, "\"" + replace_param2.replace(/\"/g, "\\\"") + "\"");
                                }
                            } else {
                                if (element_in_computation.attr("data-input-type") == "Number" || element_in_computation.attr("data-input-type") == "Currency") {
                                    string_default_val = string_default_val.replace(replace_param1, "" + replace_param2.replace(/\"/g, "\\\"").replace(/,/g, "") + "");
                                } else {
                                    string_default_val = string_default_val.replace(replace_param1, "\"" + replace_param2.replace(/\"/g, "\\\"") + "\"");
                                }
                            }
                        } else if (element_in_computation.length == 0 || fieldname_keywords.indexOf(splitted_default_val[ctr].replace("@", "")) >= 0 || (string_default_val.match(/@GetAVG/) != null || string_default_val.match(/@SDev/) != null || replace_param1.match(/@Sum/) != null || replace_param1.match(/@GetMax/) != null || replace_param1.match(/@GetMin/) != null)) {
                            if (typeof second_fn !== "undefined") { //di na kailangan nito
                                // if(second_fn == "@Sum"){
                                //     replace_param1 = "@Sum("+replace_param1+")";
                                // }else if(second_fn == "@GetMax"){
                                //     replace_param1 = "@GetMax("+replace_param1+")";
                                // }else if(second_fn == "@GetMin"){
                                //     replace_param1 = "@GetMin("+replace_param1+")";
                                // }
                            }
                            if (replace_param1.indexOf("@TimeStamp") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                var this_timestamp = $('body').data("date_page_visited");
                                string_default_val = string_default_val.replace(re, "\"" + this_timestamp + "\"");
                            } else if (replace_param1.indexOf("@TrackNo") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", "g");
                                var getTrackNoVal = $("[name='TrackNo']").val();
                                string_default_val = string_default_val.replace(re, "\"" + getTrackNoVal + "\"");
                            } else if (replace_param1.indexOf("@Today") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", "g");
                                var this_today = new Date(this_ini_variant.getUpdatedServerTime());
                                this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2);
                                string_default_val = string_default_val.replace(re, "\"" + this_today + "\"");
                            } else if (replace_param1.indexOf("@Now") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", "g");
                                var this_today = new Date(this_ini_variant.getUpdatedServerTime());
                                this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2) + " " + StrRight("0" + this_today.getHours(), 2) + ":" + StrRight("0" + this_today.getMinutes(), 2) + ":" + StrRight("0" + this_today.getSeconds(), 2);
                                string_default_val = string_default_val.replace(re, "\"" + this_today + "\"");
                            } else if (replace_param1.indexOf("@RequestID") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", "g");
                                var getID = $("#ID").val();
                                string_default_val = string_default_val.replace(re, "\"" + getID + "\"");
                            } else if (replace_param1.indexOf("@Status") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", "g");
                                var stat = $("[name='Status']").val();
                                string_default_val = string_default_val.replace(re, "\"" + stat + "\"");
                            } else if (replace_param1.indexOf("@Requestor") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", "g");
                                var requestor_name = "";
                                if ($("[name='ID']").val() == "0") {
                                    var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                    requestor_name = requestor_ele.attr("requestor-name");
                                } else {
                                    var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                    requestor_name = requestor_ele.attr("requestor-name");
                                }
                                string_default_val = string_default_val.replace(re, "\"" + requestor_name + "\"");
                            } else if (replace_param1.indexOf("@CurrentUser") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", "g");
                                var CurrentUserName = "";
                                var element_in_progress = $('[name="CurrentUser"]');
                                if (typeof element_in_progress.attr("current-user-name") != "undefined") {
                                    CurrentUserName = element_in_progress.attr("current-user-name");
                                }
                                string_default_val = string_default_val.replace(re, "\"" + CurrentUserName + "\"");
                            } else if (replace_param1.indexOf("@Processor") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", "g");
                                if ($("[name='ID']").val() == "0") {
                                    var processor_user_ele = $('[name="Processor"]');
                                    var processor_name = processor_user_ele.attr("processor-name");
                                } else {
                                    var processor_user_ele = $('[name="Processor"]');
                                    var processor_name = processor_user_ele.attr("processor-name");
                                }
                                // var getCurrentUser = "<?php echo $auth['display_name']; ?>";
                                // string_default_val = string_default_val.replace(replace_param1, "\"" + getCurrentUser + "\"");
                                string_default_val = string_default_val.replace(re, "\"" + processor_name + "\"");
                            } else if (replace_param1.indexOf("@Department") >= 0) {
                                var re = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", "g");
                                var getDepName = $("[name='Department_Name']").val();
                                string_default_val = string_default_val.replace(re, "\"" + getDepName + "\"");
                                // } else if (replace_param1.match(/\[[A-Za-z]*\]/) != null) {//FOR SUM AVG ETC FORMULA
                                //     var fieldName = replace_param1.replace(/\[[A-Za-z]*\]|@/g, "");
                                //     var formula_function = replace_param1.match(/\[[A-Za-z]*\]/);
                                //     var get_fields_value = "(";
                                //     if (formula_function[0] == "[sum]") {
                                //         get_fields_value = get_fields_value + "Number('" + $('[name="' + fieldName + '"]').val() + "')";
                                //         $('[rep-original-name="' + fieldName + '"]').each(function() {
                                //             get_fields_value = get_fields_value + " + Number('" + $(this).val() + "')";
                                //         });
                                //         get_fields_value = get_fields_value + ")";
                                //         string_default_val = string_default_val.replace(replace_param1, "" + get_fields_value + "");
                                //     }
                                //     if (formula_function[0] == "[Last]") {
                                //         if ($('[rep-original-name="' + fieldName + '"]').length >= 1) {
                                //             var element_in_computation_last = $('[rep-original-name="' + fieldName + '"]').last();
                                //             if ($.isNumeric(element_in_computation_last.val())) {
                                //                 get_fields_value = get_fields_value + "" + element_in_computation_last.val() + "";
                                //             } else {
                                //                 get_fields_value = get_fields_value + "'" + element_in_computation_last.val() + "'";
                                //             }
                                //             get_fields_value = get_fields_value + ")";
                                //             string_default_val = string_default_val.replace(replace_param1, "" + get_fields_value + "");
                                //         } else if ($('[name="' + fieldName + '"]').length >= 1) {
                                //             var element_in_computation_last = $('[name="' + fieldName + '"]').last();
                                //             if ($.isNumeric(element_in_computation_last.val())) {
                                //                 get_fields_value = get_fields_value + "" + element_in_computation_last.val() + "";
                                //             } else {
                                //                 get_fields_value = get_fields_value + "'" + element_in_computation_last.val() + "'";
                                //             }
                                //             get_fields_value = get_fields_value + ")";
                                //             string_default_val = string_default_val.replace(replace_param1, "" + get_fields_value + "");
                                //         }
                                //     }
                            } else if (replace_param1.match(/@Sum\(@[A-Za-z0-9_]*\)/g) != null) { //FOR @Sum(@Fieldname)
                                fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
                                var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                                if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                    string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                                }

                            } else if (replace_param1.match(/@GetMax\(@[A-Za-z0-9_]*\)/g) != null) { //

                                fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
                                var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                                if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                    string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                                }

                            } else if (replace_param1.match(/@GetMin\(@[A-Za-z0-9_]*\)/g) != null) { //
                                fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
                                var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                                if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                    string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                                }

                            } else if (replace_param1.match(/@SDev\(@[A-Za-z0-9_]*\)/g) != null) { //
                                fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
                                var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                                if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                    string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                                }

                            } else if (replace_param1.match(/@GetAVG\(@[A-Za-z0-9_]*\)/g) != null) { //

                                fieldName = replace_param1.match(/\((@[A-Za-z0-9_]*)\)/);
                                var reg_exp = new RegExp(fieldName[1] + "|" + "'" + fieldName[1] + "'", 'g');
                                if (fieldExists("\"" + fieldName[1] + "\"") == true) {
                                    string_default_val = string_default_val.replace(reg_exp, "'" + fieldName[1] + "'");
                                }

                            } else if (splitted_default_val[ctr].match(/@(\[[a-zA-Z0-9_\'\"]*\]){3}/g) != null) {
                                var reg_exp = new RegExp("\\[[@a-zA-Z0-9_\\\'\\\"]*\\](?!\\s)", 'g');
                                var value_get = "";
                                if (splitted_default_val[ctr].match(reg_exp) != null) {
                                    var match_res = splitted_default_val[ctr].match(reg_exp).filter(Boolean);
                                    var tbl_name = match_res[0].replace(/\[|\]/g, "");
                                    var field_name = match_res[1].replace(/\[|\]/g, "");
                                    var fn_index = match_res[2].replace(/\[|\]/g, "");
                                    if ($.isNumeric(fn_index)) {
                                        var collect_fn_ele = $('[table-name="' + tbl_name + '"]').find('[name="' + field_name + '"]');
                                        collect_fn_ele = collect_fn_ele.add($('[table-name="' + tbl_name + '"]').find('[rep-original-name="' + field_name + '"]'));
                                        if (collect_fn_ele.eq(fn_index).attr("data-type") == "float" || collect_fn_ele.eq(fn_index).attr("data-type") == "double") {
                                            value_get = "Number(" + collect_fn_ele.eq(fn_index).val() + ")";
                                        } else {
                                            value_get = "\'" + collect_fn_ele.eq(fn_index).val() + "\'";
                                        }
                                    } else if (fn_index == "RootRowIndex") {
                                        fn_index = GetRowIndex($(fieldWithComputation.parents(".setOBJ").eq(0)));
                                        console.log("fn_index", fn_index, $(fieldWithComputation.parents(".setOBJ").eq(0)))
                                        var collect_fn_ele = $('[table-name="' + tbl_name + '"]').find('[name="' + field_name + '"]');
                                        collect_fn_ele = collect_fn_ele.add($('[table-name="' + tbl_name + '"]').find('[rep-original-name="' + field_name + '"]'));
                                        if (collect_fn_ele.eq(fn_index).attr("data-type") == "float" || collect_fn_ele.eq(fn_index).attr("data-type") == "double") {
                                            value_get = "Number(" + collect_fn_ele.eq(fn_index).val() + ")";
                                        } else {
                                            value_get = "\'" + collect_fn_ele.eq(fn_index).val() + "\'";
                                        }
                                    } else if (fn_index == "RowIndex") {
                                        // fn_index = GetRowIndex($('[table-name="'+tbl_name+'"]').find('[name="'+field_name+'"]'));

                                        // var collect_fn_ele = $('[table-name="'+tbl_name+'"]').find('[name="'+field_name+'"]');
                                        // collect_fn_ele = collect_fn_ele.add($('[table-name="'+tbl_name+'"]').find('[rep-original-name="'+field_name+'"]'));
                                        // if(collect_fn_ele.eq(fn_index).attr("data-type") == "float"){
                                        //     value_get = "Number("+collect_fn_ele.eq(fn_index).val()+")";
                                        // }else{
                                        //     value_get = "\'"+collect_fn_ele.eq(fn_index).val()+"\'";
                                        // }
                                    }
                                }
                                var reg_exp_repl = new RegExp(splitted_default_val[ctr].replace(/\[/g, "\\\[").replace(/\]/g, "\\\]").replace(/\'/g, "\\\'").replace(/\"/g, "\\\""), "g");
                                string_default_val = string_default_val.replace(reg_exp_repl, value_get);

                            } else if (
                                    (
                                            $("[name='" + replace_param1 + "']").is('input') ||
                                            $("[name='" + replace_param1 + "']").is('textarea') ||
                                            $("[name='" + replace_param1 + "']").is('select')
                                            ) && $("[name='" + replace_param1 + "']").hasClass('getFields')
                                    ) {
                                var at_selected_field_ele = $("[name='" + replace_param1 + "']");
                                var at_selected_field_ele_val = '';
                                if (at_selected_field_ele.is('[type="radio"]')) {
                                    at_selected_field_ele_val = at_selected_field_ele.filter(':checked').val();
                                } else if (at_selected_field_ele.is('select:not([multiple])')) {
                                    at_selected_field_ele_val = at_selected_field_ele.filter(':checked').val();
                                } else {
                                    at_selected_field_ele_val = at_selected_field_ele.val();
                                }
                                var reg_exp = new RegExp(replace_param1 + "(?![a-zA-Z0-9_])", 'g');
                                string_default_val = string_default_val.replace(reg_exp, "\"" + at_selected_field_ele_val + "\"");
                            }
                            //console.log("BADDDKERT", replace_param1);
                        }
                    }
                    if (string_default_val.indexOf("@") >= 0) {
                        string_default_val = string_default_val.replace(/(\^@|@)/g, function (data) {
                            if (data == "^@") {
                                return "@";
                            }
                            return "";
                        });
                    }
                    if (string_default_val.indexOf(/\'/g) >= 0) {
                        string_default_val = string_default_val.replace(/\'/g, "\'");
                    }
                    // console.log(string_default_val)
                    // if(string_default_val.match(/\\/g) != null){
                    //  string_default_val = string_default_val.replace(/\\/g,"\\\\");
                    // }
                    // console.log(string_default_val)
                    if (string_default_val.match(/\"/g) != null) {
                        string_default_val = string_default_val.replace(/\"/g, "\"");
                    }
                    if (string_default_val.match(/\n/g) != null) {
                        // string_default_val = string_default_val.replace(/\n/g, "\\n");
                        string_default_val = string_default_val.replace(/"('|\\\'|\\\"|[a-zA-Z0-9_\s]|[\u000A])*"|'("|\\\'|\\\"|[a-zA-Z0-9_\s]|[\u000A])*'/g, function (a, b) {
                            return a.replace(/\n/g, "\\n");
                        });
                    }
                    // console.log("TEST FORMULA makeOnChange ",string_default_val,"\nDIS",$(this),"\nLOCATION F",fieldWithComputation);
                    var eval_status = "not ok";
                    try {
                        eval(string_default_val);
                        eval_status = "ok";
                        console.log("OK", string_default_val, fieldWithComputation)
                    } catch (error) {
                        eval_status = "not ok";
                        console.log("NOT OK", string_default_val, fieldWithComputation, error);
                    }
                    // console.log("NAKARATING??")
                    // console.log(fieldWithComputation)
                    // console.log($(this))
                    if (eval_status == "ok") {
                        if (string_default_val.indexOf("Lookup") >= 0) {
                            //data = [];
                            //fieldName = fieldWithComputation.attr("name");
                            //formula = string_default_val;
                            //data.push({
                            //    FieldName: fieldName,
                            //    Formula: formula
                            //});
                            //$.get('/ajax/ajaxformula', {
                            //    data: JSON.stringify(data)
                            //}, function(data) {
                            //    data = JSON.parse(data);
                            //    console.log("uung sa onchange");
                            //    console.log(data);
                            //    $('[name="' + data[0].FieldName + '"]').val(data[0].FieldValue);
                            //});
                        } else if (string_default_val.indexOf("RecordCount") >= 0) {
                            //
                        } else {
                            if (eval(string_default_val) == "Infinity") {
                                fieldWithComputation.val("Cannot divide by zero");
                            } else if (eval(string_default_val) == "Invalid Date") {
                                //do not place any value
                            } else {
                                if (fieldWithComputation.attr("data-type") == "date") {
                                    fieldWithComputation.val(eval(string_default_val));
                                    if (fieldWithComputation.val() != "") {
                                        fieldWithComputation.val(ThisDate(fieldWithComputation.val()).formatDate("Y-m-d"));

                                    }
                                } else if (fieldWithComputation.attr("data-type") == "dateTime") {
                                    fieldWithComputation.val(eval(string_default_val));
                                    if (fieldWithComputation.val() != "") {
                                        fieldWithComputation.val(ThisDate(fieldWithComputation.val()).formatDate("Y-m-d H:i"));

                                    }
                                } else if (fieldWithComputation.attr("data-type") == "time") {
                                    var temp = eval(string_default_val);
                                    if ($.type(temp) == "date") {
                                        fieldWithComputation.val(ThisDate(temp).formatDate('H:i'));
                                    } else {
                                        var temp2 = ThisDate(ThisDate(new Date()).formatDate('Y-m-d') + temp).formatDate('H:i');
                                        if (temp2 == "Invalid Date") {
                                            temp2 = ThisDate(temp).formatDate('H:i');
                                        }
                                        fieldWithComputation.val(temp2);
                                    }
                                } else {
                                    if (fieldWithComputation.prop("tagName") == "SELECT") {
                                        if (event_type == "change") {
                                            var eval_val = eval(String("[" + string_default_val + "]"));
                                            console.log("HERE " + eval_val);
                                            if (typeof eval_val === "object") {
                                                fieldWithComputation.html("");
                                                var temp_dis_name = $(this).attr("name");
                                                deployArrayValues(eval_val, function (i, v) {
                                                    if (typeof v === "string") {
                                                        if ($.trim(v) != "") {
                                                            fieldWithComputation.append('<option value="' + v + '">' + v + '</option>');

                                                            {//added 10:51 AM 12/3/2014
                                                                if (name_collection) {
                                                                    name_collection.push(temp_dis_name); //fieldWithComputation.attr("name")
                                                                } else {
                                                                    name_collection = [temp_dis_name]; //fieldWithComputation.attr("name")
                                                                }
                                                                setTimeout(function () {
                                                                    fieldWithComputation.trigger("change", [{"name_collection": name_collection}]);
                                                                }, 0);
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                            // fieldWithComputation.append('<option value="'+eval_val+'">'+eval_val+'</option>');
                                        }
                                    } else {
                                        fieldWithComputation.val(eval(string_default_val));
                                        if (fieldWithComputation.val() == "NaN") {
                                            fieldWithComputation.val("Not a valid computation")
                                        }
                                    }

                                }
                            }
                        }
                    } else {
                        //console.log(fieldWithComputation,"EVAL STAT NOT OK");
                        //console.log(string_default_val)
                    }


                    // }catch(error){
                    //   alert("nag error??")
                    // }
                    var reg_eks = new RegExp("@" + fieldWithComputation.attr("name") + "(?![a-zA-Z0-9_])", "g");
                    //console.log("BB",fieldWithComputation.attr("name"))
                    // if (fieldWithComputation.attr("default-formula-value").match(reg_eks) == null) {
                    if (name_collection) {
                        name_collection.push($(this).attr("name")); //fieldWithComputation.attr("name")
                    } else {
                        name_collection = [$(this).attr("name")]; //fieldWithComputation.attr("name")
                    }
                    $.unique(name_collection);
                    // console.log("BAGO MAG ONCHANGE",name_collection,fieldWithComputation.attr("name"))
                    console.log("FORMULA ONCHANGE CHAINING PATH: " + name_collection.join("->") + "~~>" + fieldWithComputation.attr("name"))
                    if (name_collection.indexOf(fieldWithComputation.attr("name")) <= -1) {
                        setTimeout(function () {
                            fieldWithComputation.trigger("change", [{"name_collection": name_collection}]);
                        }, 0);
                    } else {
                        console.error("ERROR!!!!, YOUR FORMULA LOOPS THIS MAY CAUSE THE PAGE TO HANGUP/STOP,", "    CHAINING PATH BY FIELDNAMES:", name_collection.join("->") + ":::" + fieldWithComputation.attr("name"));
                    }

                    // }
                    //console.log(string_default_val)
                    //console.log("\n\n\n\n\n\n\n\n\n\n\n\n")
                });
                //}
            }

        },
        // getData: function() {
        //     var repeaterData = [];
        //     $('[repeater-table="true"]').each(function() {
        //         var objectId = $(this).closest('.setOBJ').attr('id');
        //         var row = [];

        //         $(this).find('tr').each(function(index) {
        //             var data = [];

        //             $(this).find('.getFields').each(function() {
        //                 var originalName = $(this).attr('rep-original-name');
        //                 var name = $(this).attr('name');
        //                 var type = $(this).attr('type');

        //                 switch (type) {
        //                     case "checkbox":
        //                         var values = [];
        //                         if ($(this).prop('checked')) {
        //                             values.push({
        //                                 Value: $(this).val(),
        //                                 Checked: true
        //                             });
        //                         } else {
        //                             values.push({
        //                                 Value: $(this).val(),
        //                                 Checked: false
        //                             });
        //                         }
        //                         break;
        //                     case "radio":
        //                         var values = [];
        //                         if ($(this).prop('checked')) {
        //                             values.push({
        //                                 Value: $(this).val(),
        //                                 Checked: true
        //                             });
        //                         } else {
        //                             values.push({
        //                                 Value: $(this).val(),
        //                                 Checked: false
        //                             });
        //                         }
        //                         break;
        //                     default :
        //                         if (typeof $(this).attr("data-input-type") != "undefined") {
        //                             if ($(this).attr("data-input-type") == "Currency") {
        //                                 var values = $(this).val().replace(/,/g, "");
        //                             } else {
        //                                 var values = $(this).val();
        //                             }
        //                         } else {
        //                             var values = $(this).val();
        //                         }
        //                         break;
        //                 }
        //                 if (index === 0) {
        //                     data.push({
        //                         FieldName: name,
        //                         Values: values
        //                     });
        //                 } else {
        //                     data.push({
        //                         FieldName: originalName,
        //                         Values: values
        //                     });
        //                 }
        //             });

        //             row.push({
        //                 Index: index,
        //                 Data: data
        //             });
        //         });


        //         repeaterData.push({
        //             ObjectId: objectId,
        //             Row: row
        //         });
        //     });

        //     console.log('REPEATER GET DATA',repeaterData);

        //     $('#Repeater_Data').val(JSON.stringify(repeaterData));
        // },
        // setData: function() {
        //     try {
        //         var content = JSON.parse($('#Repeater_Data').val());


        //         for (var index in content) {
        //             var objectId = content[index].ObjectId;
        //             for (var row in content[index].Row) {
        //                 if (row != 0) {
        //                     $('#' + objectId).find('.rep-add-spec-row:last').click();
        //                     $('#' + objectId).find('.rep-add-spec-row:last').parents("tbody").eq(0).children("tr").last().children("td").stop(true, true);
        //                 }
        //                 for (var data in content[index].Row[row].Data) {

        //                     var fieldName = content[index].Row[row].Data[data].FieldName;
        //                     var type = $('[name="' + fieldName + '"]').attr('type');
        //                     var field;
        //                     var values;

        //                     if (row == 0) {
        //                         field = $('[name="' + fieldName + '"]');
        //                     } else {
        //                         field = $('[name="' + fieldName + '_UID' + row + '"]');
        //                     }

        //                     switch (type) {
        //                         case "checkbox":
        //                             var values = content[index].Row[row].Data[data].Values[0].Value;
        //                             var checked = content[index].Row[row].Data[data].Values[0].Checked;
        //                             $(field).each(function() {
        //                                 var thisValue = $(this).val();

        //                                 if (values == thisValue) {
        //                                     $(this).prop('checked', checked);
        //                                 }

        //                             });
        //                             break;
        //                         case "radio":
        //                             var values = content[index].Row[row].Data[data].Values[0].Value;
        //                             var checked = content[index].Row[row].Data[data].Values[0].Checked;

        //                             $(field).each(function() {
        //                                 var thisValue = $(this).val();
        //                                 if (values == thisValue) {
        //                                     $(this).prop('checked', checked);
        //                                 }
        //                             });
        //                             break;
        //                         default :
        //                             values = content[index].Row[row].Data[data].Values;
        //                             $(field).val(values);
        //                             break;
        //                     }

        //                 }
        //             }
        //         }
        //     } catch (error) {
        //         console.log("error setData: function() { parsing json")
        //     }
        // },
        getData: function () {
            var repeaterData = [];
            $('[repeater-table="true"]').each(function () {
                var objectId = $(this).closest('.setOBJ').attr('id');
                var row = [];

                $(this).find('tr').each(function (index) {
                    var data = [];

                    $(this).find('.getFields').each(function () {
                        var originalName = $(this).attr('rep-original-name');
                        var name = $(this).attr('name');
                        var type = $(this).attr('type');

                        switch (type) {
                            case "checkbox":
                                var values = [];
                                if ($(this).prop('checked')) {
                                    values.push({
                                        Value: $(this).val(),
                                        Checked: true
                                    });
                                } else {
                                    values.push({
                                        Value: $(this).val(),
                                        Checked: false
                                    });
                                }
                                break;
                            case "radio":
                                var values = [];
                                if ($(this).prop('checked')) {
                                    values.push({
                                        Value: $(this).val(),
                                        Checked: true
                                    });
                                } else {
                                    values.push({
                                        Value: $(this).val(),
                                        Checked: false
                                    });
                                }
                                break;
                            default :
                                if (typeof $(this).attr("data-input-type") != "undefined") {
                                    if ($(this).attr("data-input-type") == "Currency") {
                                        var values = $(this).val().replace(/,/g, "");
                                    } else {
                                        var values = $(this).val();
                                    }
                                } else {
                                    var values = $(this).val();
                                }
                                break;
                        }
                        if (index === 0) {
                            data.push({
                                FieldName: name,
                                Values: values
                            });
                        } else {
                            data.push({
                                FieldName: originalName,
                                Values: values
                            });
                        }
                    });

                    row.push({
                        Index: index,
                        Data: data
                    });
                });


                repeaterData.push({
                    ObjectId: objectId,
                    Row: row
                });
            });

            console.log('REPEATER GET DATA', repeaterData);

            $('#Repeater_Data').val(JSON.stringify(repeaterData));
        },
        setData: function () {
            // try {
            var r_data = $('#Repeater_Data').val();
            if ($.type(r_data) != "undefined" && r_data != '') {
                var content = JSON.parse($('#Repeater_Data').val());


                for (var index in content) {
                    var objectId = content[index].ObjectId;
                    for (var row in content[index].Row) {
                        if (row != 0) {
                            $('#' + objectId).find('.rep-add-spec-row:last').click();
                            $('#' + objectId).find('.rep-add-spec-row:last').parents("tbody").eq(0).children("tr").last().children("td").stop(true, true);
                        }
                        for (var data in content[index].Row[row].Data) {

                            var fieldName = content[index].Row[row].Data[data].FieldName;
                            var type = $('[name="' + fieldName + '"]').attr('type');
                            var field;
                            var values;

                            if (row == 0) {
                                field = $('[name="' + fieldName + '"]');
                            } else {
                                field = $('[name="' + fieldName + '_UID' + row + '"]');
                            }

                            switch (type) {
                                case "checkbox":
                                    var values = content[index].Row[row].Data[data].Values[0].Value;
                                    var checked = content[index].Row[row].Data[data].Values[0].Checked;
                                    $(field).each(function () {
                                        var thisValue = $(this).val();

                                        if (values == thisValue) {
                                            $(this).prop('checked', checked);
                                        }

                                    });
                                    break;
                                case "radio":
                                    var values = content[index].Row[row].Data[data].Values[0].Value;
                                    var checked = content[index].Row[row].Data[data].Values[0].Checked;

                                    $(field).each(function () {
                                        var thisValue = $(this).val();
                                        if (values == thisValue) {
                                            $(this).prop('checked', checked);
                                        }
                                    });
                                    break;
                                default :
                                    values = content[index].Row[row].Data[data].Values;
                                    $(field).val(values);
                                    break;
                            }

                        }
                    }
                }
            }
            //added by japhet morada
            //update tables, update the datas of single and multiple attachments inside the repeater data
            updateTables();

            // } catch (error) {
            //     console.log("error setData: function() { parsing json", error)
            // }
        },
        "getFormJson": function (form_id) {
            // // form_id = $("#FormID").val();
            // return $.ajax({
            //     url: '/ajax/getFormData?form_id=' + form_id,
            //     async: false,
            //     type: 'GET',
            //     cache: false,
            //     success: function(data) {
            //         var body_data = $("body").data();
            //         if (data || data != "") {
            //             body_data.user_form_json_data = JSON.parse(data);
            //             console.log(body_data.user_form_json_data);
            //             $("body").data(body_data);
            //             // console.log("WHAT")
            //             // console.log($("body").data())
            //         }
            //     }
            // }).fail(function() {
            //     console.log("ERROR AJAX GET_FORM_JSON");
            // })
            var formjson = $("#FormJsonDiv").text();
            // console.log(formjson)
            $("body").data("user_form_json_data", JSON.parse(formjson));
        },
        "setFieldsVisibility": function () {
            $('.getFields').each(function () {
                var visibility = $(this).attr('field-visible');

                switch (visibility) {
                    case "true":
                        $(this).parents('.setOBJ').eq(0).css('display', 'block');
                        break;
                    case "false":
                        $(this).parents('.setOBJ').eq(0).css('display', 'none');
                        break;
                }
            });

        },
        "computeVisibility": function () {
            if (typeof callBack == "undefined") {

            }
            var data = [];
            $('[field-visible="computed"]').each(function (index) {
                if ($(this).attr('visible-formula')) {
                    var fieldName = $(this).attr('name');
                    if (typeof fieldName == "undefined") {
                        fieldName = $(this).attr('tab-name');
                    }
                    if (typeof fieldName == "undefined") {
                        fieldName = $(this).attr('accordion-name');
                    }
                    var formula = $(this).attr('visible-formula');
                    var process_formula = formula;
                    var getFieldName = formula.match(/@[A-Za-z_][A-Za-z0-9_]*/g);

                    if (getFieldName != null) {
                        getFieldName = getFieldName.filter(Boolean);
                        console.log("getFieldName", getFieldName);
                        for (var ctr = 0; ctr < getFieldName.length; ctr++) {
                            console.log("WHOA", $('[name="' + getFieldName[ctr].replace("@", "") + '"]'))
                            if ($('[name="' + getFieldName[ctr].replace("@", "") + '"]').length >= 1) {
                                var type = $('[name="' + getFieldName[ctr].replace("@", "") + '"]').attr('type');
                                if (type == 'radio') {
                                    var value_fn = $('[name="' + getFieldName[ctr].replace("@", "") + '"]:checked').val();
                                } else {
                                    var value_fn = $('[name="' + getFieldName[ctr].replace("@", "") + '"]').val();
                                }
                                process_formula = process_formula.replace(getFieldName[ctr], "'" + value_fn.replace("'","\\'").replace('"','\\"') + "'");
                            }
                        }
                        data.push({
                            FieldName: fieldName,
                            Formula: process_formula
                        });
                    }

                }
            });
            // console.log("ETOOO BAAz");
            // console.log(data);

            for (var index in data) {
                var returnFieldName = data[index].FieldName;
                console.log(data[index].Formula);
                var returnFieldValue = eval(data[index].Formula);
                var role = "";
                if ($('[tab-name="' + returnFieldName + '"]').attr("role")) {
                    role = $('[tab-name="' + returnFieldName + '"]').attr("role");
                }
                console.log("return field name", returnFieldName);
                if (role.length >= 1 && role == "tab") {
                    var tab_navigation_ele = $('[tab-name="' + returnFieldName + '"]');
                    var content_id_ele = $('[tab-name="' + returnFieldName + '"]').children("a[role='presentation']").attr("href");
                    if (returnFieldValue) {
                        if (tab_navigation_ele.eq(0).css("display") == "none") {
                            tab_navigation_ele.eq(0).show();
                            $(content_id_ele).show();
                            tab_navigation_ele.eq(0).parents(".form-tabbable-pane").eq(0).css("height", "");
                            // tab_navigation_ele.eq(0).parents(".form-tabbable-pane").eq(0).css("width","");
                            tab_navigation_ele.eq(0).children("a").click();
                        }
                    } else {
                        if (tab_navigation_ele.eq(0).css("display") != "none") {
                            tab_navigation_ele.eq(0).hide();
                            $(content_id_ele).hide();
                            if (tab_navigation_ele.eq(0).next().length >= 1 && tab_navigation_ele.eq(0).next().css("display") != "none") {
                                tab_navigation_ele.eq(0).next().children("a").click();
                            } else if (tab_navigation_ele.eq(0).prev().length >= 1 && tab_navigation_ele.eq(0).prev().css("display") != "none") {
                                tab_navigation_ele.eq(0).prev().children("a").click();
                            } else {
                                console.log("WALA NANG NAKA DISPLAY NA TAB")
                                // width_of_tab_panel = tab_navigation_ele.eq(0).parents(".setOBJ").eq(0).outerWidth();
                                height_of_tab = tab_navigation_ele.eq(0).css("height").replace("px", "");
                                height_of_tab_content = $(tab_navigation_ele.eq(0).children("a").eq(0).attr("href")).css("height").replace("px", "");
                                // console.log(tab_navigation_ele.eq(0).parents(".form-tabbable-pane").eq(0))
                                // console.log(width_of_tab_content+"  "+height_of_tab + "  " + height_of_tab_content)
                                tab_navigation_ele.eq(0).parents(".form-tabbable-pane").eq(0).css("height", (Number(height_of_tab) + Number(height_of_tab_content)) + "px")
                                // tab_navigation_ele.eq(0).parents(".form-tabbable-pane").eq(0).css("width",(width_of_tab_panel)+"px")
                                console.log(tab_navigation_ele.eq(0))
                            }
                        }
                    }
                } else {
                    if (returnFieldValue) {
                        // console.log("COMPUTE SPACES toblock", computeGap($('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0),'expand') );
                        if ($('[accordion-name="' + returnFieldName + '"]').length > 0) {
                            console.log("accordion is here");
                            if ($('[accordion-name="' + returnFieldName + '"]').css('display') != 'none') {
                                $('[accordion-name="' + returnFieldName + '"]').css('display', 'none');
                            }
                            else {
                                $('[accordion-name="' + returnFieldName + '"]').css('display', 'block');
                            }
                        }
                        else {
                            if ($('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).attr("compute-visibility-display")) {
                                $('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).css("display", $('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).attr("compute-visibility-display"));
                            } else {
                                if ($('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).css("display") != "none") {
                                    $('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).css('display', $('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).css('display'));
                                } else {
                                    $('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).css('display', 'block');
                                }
                            }
                        }
                    } else {
                        // console.log("COMPUTE SPACES togap", computeGap($('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0),'collapse') );
                        if ($('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).css("display") != "none") {
                            $('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).attr("compute-visibility-display", $('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).css("display"));
                        }
                        $('[name="' + returnFieldName + '"]').parents('.setOBJ').eq(0).css('display', 'none');

                    }
                }
            }
            checkStaticTablesGAP();
        },
        "populateFormula": function (object, callBack) {

            var function_keyword_names_serversides = [
                '@AdjustDate', '@DiffDate', '@Lookup', '@RecordCount', '@LookupWhere',
                '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
                '@LookupAVG', '@LookupCountIf', "@FormatDate",
                "@Head", "@AssistantHead", "@UserInfo", "@DataSource", "@CustomScript"
            ];
            var fn_reserve_keywords = ["@Requestor", "@RequestID", "@Status", "@CurrentUser", "@Department", "@TrackNo", "@Processor", "@Today", "@Now", "@TimeStamp"];

            if (typeof callBack == "undefined") {

            }
            var obj_name = "";
            if (object) {
                obj_name = $(object).attr("name");
            }
            var data = [];
            var start = true;
            if (object) {
                var each_selector = $(object);
                if ($.type(each_selector.data("formula_lookup_names")) == "array") {
                    $.each(each_selector.data("formula_lookup_names"), function (ii, vv) {
                        each_selector = each_selector.add($('[name="' + vv + '"]'));
                    });
                }
            } else {
                var each_selector = $('.getFields');
            }

            //added jewel data source
            var bodyData = $('body').data();
            var userFormData = bodyData['user_form_json_data'];
            var formJson = userFormData['form_json'];
            var formDataSource = formJson['form_variables'];
            var dataSourceArr = [];
            for (var index in formDataSource) {
                console.log('formDataSource', formDataSource[index]);
                var isDynamic = formDataSource[index]['var_compute_type'];
                var name = formDataSource[index]['var_name'];
                var formula = formDataSource[index]['var_formula'];

                if (isDynamic == 'dynamic') {
                    var processedFormula = getProcessedFormula(obj_name, formula);
                    dataSourceArr.push({
                        var_name: name,
                        var_formula: processedFormula
                    });
                }

                // console.log('processedFormula', processedFormula);
            }

            console.log('dataSourceArr', dataSourceArr);
            //end added jewel data source

            each_selector.each(function (index) {

                var temp_getFieldName = null;
                if ($(this).attr('default-type') != "computed" || typeof $(this).attr('default-type') === "undefined") {
                    return true;
                }

                if ($(this).attr('default-formula-value')) {
                    var fieldName = $(this).attr('name');
                    var formula = $(this).attr('default-formula-value');
                    var process_formula = formula;
                    var reg_eks_check_server_formula = new RegExp(function_keyword_names_serversides.join("(?=[^a-zA-Z0-9_])|") + "(?=[^a-zA-Z0-9_])", "g");
                    var matched_server_formula = process_formula.match(reg_eks_check_server_formula);
                    var f_loader = $([]);
                    if (matched_server_formula) {
                        matched_server_formula = matched_server_formula.filter(Boolean);
                        if (matched_server_formula.length >= 0) {
                            f_loader = formulaFieldLoader($(this).parents('.setOBJ').eq(0));
                        }
                    }

                    if ($.type(obj_name) == "string") {
                        if (obj_name == "") {

                        } else if (process_formula.indexOf(obj_name) <= -1) { //eto ung nag fifilter para di lahat ay magtrigger ng onchange
                            f_loader.remove();
                            return true;
                        }
                    }

                    var getFieldName = formula.match(/@[A-Za-z_][A-Za-z0-9_]*/g);

                    if (getFieldName) {
                        getFieldName = getFieldName.filter(Boolean);
                        temp_getFieldName = getFieldName;
                        temp_getFieldName = temp_getFieldName.map(function (value, index, whole) {
                            return value.replace("@", "");
                        })
                        if (temp_getFieldName.indexOf(obj_name) <= -1) {
                            // alert(234)
                            // console.log(temp_getFieldName, obj_name);
                            // return true;
                        }
                        var at_str_name = "";
                        for (var ctr = 0; ctr < getFieldName.length; ctr++) {
                            if ($('[name="' + getFieldName[ctr].replace("@", "") + '"]').length >= 1 && fn_reserve_keywords.indexOf(getFieldName[ctr].replace('[]', '')) <= -1) {
                                var fn_ele = $('[name="' + getFieldName[ctr].replace("@", "") + '"]');

                                var value_fn = (fn_ele.val() || "").replace(/\'/g, "\\'");
                                if (fn_ele.is('input[type="radio"]')) {
                                    value_fn = fn_ele.filter(':checked').val();
                                }
                                if (value_fn) {
                                    value_fn = value_fn.replace("@", "^@");
                                    process_formula = process_formula.replace(getFieldName[ctr], "'" + value_fn.replace("'","\\'").replace('"','\\"') + "'");
                                } else {
                                    process_formula = process_formula.replace(getFieldName[ctr], "'" + "'");
                                }
                                if ($.type(fn_ele.data("formula_lookup_names")) == "array") {
                                    fn_ele.data("formula_lookup_names").push(fieldName);
                                    $.unique(fn_ele.data("formula_lookup_names"));
                                } else {
                                    fn_ele.data("formula_lookup_names", []);
                                    fn_ele.data("formula_lookup_names").push(fieldName);
                                }
                            } else {
                                at_str_name = getFieldName[ctr];
                                if (at_str_name == "@Requestor") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    var requestor_name = "";
                                    if ($("[name='ID']").val() == "0") {
                                        var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                        requestor_name = requestor_ele.attr("requestor-name");
                                    } else {
                                        var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                        requestor_name = requestor_ele.attr("requestor-name");
                                    }
                                    process_formula = process_formula.replace(re, "\"" + requestor_name + "\"");
                                } else if (at_str_name == "@RequestID") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    var request_id_ele = $('[name="ID"]');
                                    var rid_val = request_id_ele.val();
                                    process_formula = process_formula.replace(re, rid_val);
                                } else if (at_str_name == "@Status") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    var status_ele = $("#frmrequest").find('[name="Status"]').eq(0);
                                    if (status_ele.length >= 1) {
                                        var getStatusVal = status_ele.val();
                                        process_formula = process_formula.replace(re, "\"" + getStatusVal + "\"");
                                    }
                                } else if (at_str_name == "@CurrentUser") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    var CurrentUserName = "";
                                    var element_in_progress = $('[name="CurrentUser"]');
                                    if (typeof element_in_progress.attr("current-user-name") != "undefined") {
                                        CurrentUserName = element_in_progress.attr("current-user-name");
                                    }
                                    process_formula = process_formula.replace(re, "\"" + CurrentUserName + "\"");
                                } else if (at_str_name == "@Department") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    var getDep_ele = $("[name='Department_Name']");
                                    if (getDep_ele.length >= 1) {
                                        var getDepName = getDep_ele.val();
                                        process_formula = process_formula.replace(re, "\"" + getDepName + "\"");
                                    }
                                } else if (at_str_name == "@TrackNo") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    var track_no_ele = $("#frmrequest").find('[name="TrackNo"]').eq(0);
                                    if (track_no_ele.length >= 1) {
                                        var getTrackNoVal = track_no_ele.val();
                                        process_formula = process_formula.replace(re, "\"" + getTrackNoVal + "\"");
                                    }
                                } else if (at_str_name == "@Processor") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    if ($("[name='ID']").val() == "0") {
                                        var processor_user_ele = $('[name="Processor"]');
                                        var processor_name = processor_user_ele.attr("processor-name");
                                        process_formula = process_formula.replace(re, "\"" + processor_name + "\"");
                                    } else {
                                        var processor_user_ele = $("#processor_display");
                                        var processor_name = processor_user_ele.text();
                                        process_formula = process_formula.replace(re, "\"" + processor_name + "\"");
                                    }
                                } else if (at_str_name == "@Today") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    var this_today = new Date(getUpdatedServerTime());
                                    this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2);
                                    process_formula = process_formula.replace(re, "\"" + cleanStringValue(this_today) + "\"");
                                } else if (at_str_name == "@Now") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    var this_today = new Date(getUpdatedServerTime());
                                    this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2) + " " + StrRight("0" + this_today.getHours(), 2) + ":" + StrRight("0" + this_today.getMinutes(), 2) + ":" + StrRight("0" + this_today.getSeconds(), 2);
                                    process_formula = process_formula.replace(re, "\"" + cleanStringValue(this_today) + "\"");
                                } else if (at_str_name == "@TimeStamp") {
                                    var re = new RegExp(at_str_name + "(?![A-Za-z][a-zA-Z0-9_])", "g");
                                    var this_timestamp = $('body').data("date_page_visited");
                                    process_formula = process_formula.replace(re, "\"" + this_timestamp + "\"");
                                }
                            }
                        }
                    }

                    // console.log("process_formula", process_formula)
                    var reg_eks_check_server_formula = new RegExp(function_keyword_names_serversides.join("|"), "g");
                    var matched_server_formula = process_formula.match(reg_eks_check_server_formula);
                    console.log("matched_server_formula", matched_server_formula);
                    if (matched_server_formula) {
                        matched_server_formula = matched_server_formula.filter(Boolean);
                        if (matched_server_formula.length >= 0) {
                            data.push({
                                RequestID: $("#ID").val(),
                                FieldName: fieldName,
                                Formula: process_formula
                            });
                            //console.log("OK SERVER FORMULA")
                        }
                    }
                }
            });

            console.log("ajaxFormula", data);
            if (data.length >= 1) {
                $.post('/ajax/ajaxformula', {
                    data: JSON.stringify(data),
                    dataSources: JSON.stringify(dataSourceArr)
                }, function (result) {
                    var result = result;
                    var error_ctr = 0;
                    try {
                        result = JSON.parse(result);
                        if (result == false) {
                            throw "result is not right!...\n";
                        }
                    } catch (e) {
                        error_ctr++;
                        console.error("error on populateFormula /ajax/ajaxformula ...", e, result);
                    }
                    if (error_ctr >= 1) {
                        return "";
                    }
                    for (var index in result) {
                        var returnFieldName = result[index].FieldName;
                        var returnFieldValue = result[index].FieldValue;

                        //console.log(returnFieldName);
                        //console.log($('[name="' + returnFieldName + '"]'))
                        $('[name="' + returnFieldName + '"]').val(returnFieldValue);

                        oid = $('[name="' + returnFieldName + '"]').parents('.setOBJ[data-object-id]').eq(0).attr('data-object-id');

                        // console.log($('[name="' + returnFieldName + '"]').parents('[id="obj_fields_'+oid+'"]'),"YUMMY",$('[name="' + returnFieldName + '"]').parents('[id="obj_fields_'+oid+'"]').eq(0).children('.data-formula-loader-noti'))
                        $('[name="' + returnFieldName + '"]').parents('[id="obj_fields_' + oid + '"]').eq(0).children('.data-formula-loader-noti').remove();


                        // if(obj_name == ''){
                        //     var name_collection = [returnFieldName];
                        // }else{

                        // }
                        var name_collection = [obj_name];
                        if ($.type(callBack) != 'function' && $.type(callBack) != 'undefined') {
                            if ($.type(callBack['name_collection']) != 'undefined') {
                                name_collection = $.unique(callBack['name_collection'].concat(name_collection));
                            }
                        }
                        name_collection = name_collection.filter(Boolean);
                        if (name_collection.indexOf(returnFieldName) <= -1 || obj_name == '') { //obj_name == '' para sa unang load ng page
                            var selector_to_trigger = $('[name="' + returnFieldName + '"]');
                            selector_to_trigger.trigger("change", [{"name_collection": name_collection}]);
                        } else {
                            console.error("POPULATE_F ERROR!!!!, YOUR FORMULA LOOPS THIS MAY CAUSE THE PAGE TO HANGUP/STOP, CHAINING PATH BY FIELDNAMES: " + name_collection.join('->') + ":::RF:" + returnFieldName + "~FF:" + obj_name);
                        }
                    }
                    if ($.type(callBack) == "function") {
                        callBack();
                    }
                });
            } else {
                each_selector.parent().children('.data-formula-loader-noti').remove();
            }
        },
        "addRefreshEvent": function () {
            var self = this;
            $("body").on("change", '[lookup-event-trigger="true"]', function (e, data) {
                //alert("nagchange above picklist");
                if (formulaOnload == false) {
                    return;
                }
                //alert("nagchange below picklist");
                self.populateFormula(this, data);
            });
        },
        "hyperlinkFormula": function () {

            $('[default-hyperlink-type="computed"][default-hyperlink-formula-value]').each(function () {
                var self = $(this);
                var self_attr = self.attr("default-hyperlink-formula-value");
                var parsed_formula = new Formula(self_attr);
                parsed_formula.getProcessedFormula();
                console.log("parsed formula", parsed_formula.getProcessedFormula());
                parsed_formula.getEvaluation();
                console.log("getFieldNames", parsed_formula.getFieldNames());
                parsed_formula.fieldBinderEvent({
                    "callBack": function (data) {
                        console.log("fieldBinderEvent", data, $(this))
                        $(this).on("change.hyperlinkFormula", function (event2, data2) {
                            var link_dom = self;
                            var new_parsed_formula = new Formula(data["formula"]);
                            new_parsed_formula.getEvaluation();
                            var parsed_path = new_parsed_formula.getEvaluation();
                            link_dom.children('a').contents().unwrap();
                            link_dom.wrapInner('<a href="' + parsed_path + '"/>');
                        })
                    },
                    "targetEle": self
                });
            });

        },
        "visibilityOnChange2": function (selector_jq) {
            var function_keyword_names = [
                '@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
                '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
                '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
                '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
                '@LookupAVG', '@GetRound', '@LookupCountIf', '@Sum',
                '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now', '@TimeStamp', '@Processor',
                '@GetAuth', '@StrSplitBy', '@ArraySearchBy', '@StrFind'
            ];
            function_keyword_names = function_keyword_names.concat(['@StrSplitBy', '@ArraySearchBy']);
            var self = this;
            var visibilityonchange_selector = $();

            if (typeof selector_jq === "string" || typeof selector_jq === "object") {
                visibilityonchange_selector = $(selector_jq);
            } else {
                visibilityonchange_selector = $('[field-visible="computed"][visible-formula]');
            }
            // console.log("visibilityonchange_selector",visibilityonchange_selector)
            visibilityonchange_selector.each(function () {
                var dis_ele = $(this);
                var dis_ele_name = dis_ele.attr("name");
                var dis_visible_formula = dis_ele.attr('visible-formula');
                if (dis_visible_formula) {
                    if (dis_visible_formula == "" || dis_visible_formula == "[,]") {
                        return;
                    }
                }
                if (dis_visible_formula == undefined) {
                    return;
                }
                var dis_visible_formula_process = dis_visible_formula;
                var regeks_get_name = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*", "g");
                var names_in_formula = dis_visible_formula.match(regeks_get_name)

                if (names_in_formula != null) {

                    names_in_formula = names_in_formula.filter(Boolean);
                    if (names_in_formula.length >= 1) {
                        names_in_formula = $.unique(names_in_formula);
                        for (var ii in names_in_formula) {

                            var at_str_name = names_in_formula[ii];
                            if (function_keyword_names.indexOf(at_str_name) <= -1) {
                                var str_name = at_str_name.replace(/@/g, "");
                                var field_element = $('[name="' + str_name + '"]'); //single datas
                                if (field_element.length <= 0) {
                                    field_element = $('[name="' + str_name + '[]"]');//for multiple datas if single is not existing
                                }
                                if (field_element.length >= 1) {
                                    var input_ele = field_element;

                                    if (input_ele.is('input[type="radio"]')) {
                                        var ele_val = input_ele.filter(":checked").val() || "";
                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + ele_val + "\"");
                                    } else if (input_ele.is('select:not([multiple])')) {
                                        var ele_val = input_ele.val() || "";
                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + ele_val + "\"");
                                    } else if (input_ele.is('select[multiple]')) {
                                        var ele_val = input_ele.children('option:selected').map(function () {
                                            return $(this).val();
                                        }).get().filter(Boolean).join("|^|") || "";
                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + ele_val + "\"");
                                    } else if (input_ele.is('input[type="checkbox"]')) {
                                        var ele_val = input_ele.filter(":checked").map(function () {
                                            return $(this).val();
                                        }).get().filter(Boolean).join("|^|") || "";
                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + ele_val + "\"");
                                    } else if (input_ele.is('[data-input-type="Currency"]') || input_ele.is('[data-input-type="Number"]')) {
                                        var replace_comma = (new RegExp(",", "g"));
                                        var input_val = input_ele.val().replace(replace_comma, "");
                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, Number(input_val));
                                    } else {
                                        var input_val = input_ele.val().replace(/\"/g, "\\\'");
                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + input_val + "\"");
                                    }
                                    if (input_ele.is(':data("visibility_dis_ele")') == false) {
                                        input_ele.data("visibility_dis_ele", {
                                            "dis_ele": [dis_ele],
                                            "dis_ele_name": dis_ele_name,
                                            "dis_visible_formula": dis_visible_formula
                                        });
                                    } else {

                                        if (
                                                input_ele.filter(':data("visibility_dis_ele")').data("visibility_dis_ele")["dis_ele"].filter(function (val) {
                                            // console.log(val[0],"===", dis_ele[0], "~", val[0] == dis_ele[0])
                                            if (val[0] == dis_ele[0]) {
                                                return true;
                                            } else {
                                                return false;
                                            }
                                        }).length == 0
                                                ) {
                                            input_ele.data("visibility_dis_ele")["dis_ele"].push(dis_ele);
                                        }
                                    }

                                    input_ele.filter(function () {
                                        if ($(this).data("events")) {
                                            if ($(this).data("events")["change"]) {
                                                if ($(this).data("events")["change"]) {
                                                    if (
                                                            $(this).data("events")["change"].filter(function (a) {
                                                        if (a["namespace"] == "exeVisibilityOnChange") {
                                                            return true;
                                                        }
                                                        return false;
                                                    }).length >= 1
                                                            ) {
                                                        return false;
                                                    }
                                                }
                                            }
                                        }
                                        return true;
                                    }).on({
                                        "change.exeVisibilityOnChange": function () {
                                            // console.log("formulaOnload",formulaOnload);
                                            if (formulaOnload == false) {
                                                return;
                                            }
                                            var input_ele_dis = $(this);
                                            var dis_data = input_ele_dis.data("visibility_dis_ele");
                                            if (dis_data["dis_ele"].length >= 1) {
                                                $(dis_data["dis_ele"]).each(function () {

                                                    var parent_ele_data = {
                                                        "dis_ele": $(this),
                                                        "dis_ele_name": $(this).attr("name"),
                                                        "dis_visible_formula": $(this).attr("visible-formula")
                                                    }
                                                    var dis_visible_formula_process = parent_ele_data["dis_visible_formula"];
                                                    var regeks_get_name = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*", "g");
                                                    var names_in_formula = parent_ele_data["dis_visible_formula"].match(regeks_get_name)
                                                    if (names_in_formula != null) {
                                                        names_in_formula = names_in_formula.filter(Boolean);
                                                        if (names_in_formula.length >= 1) {
                                                            names_in_formula = $.unique(names_in_formula);
                                                            for (var ii in names_in_formula) {
                                                                var at_str_name = names_in_formula[ii];
                                                                if (function_keyword_names.indexOf(at_str_name) <= -1) {
                                                                    var str_name = at_str_name.replace(/@/g, "");
                                                                    var field_element = $('[name="' + str_name + '"]'); //single datas
                                                                    if (field_element.length <= 0) {
                                                                        field_element = $('[name="' + str_name + '[]"]');//for multiple datas if single is not existing
                                                                    }
                                                                    if (field_element.length >= 1) {
                                                                        var input_ele = field_element;

                                                                        if (input_ele.is('input[type="radio"]')) {
                                                                            var ele_val = input_ele.filter(":checked").val();
                                                                            var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + ele_val + "\"");
                                                                        } else if (input_ele.is('select:not([multiple])')) {
                                                                            var ele_val = input_ele.val();
                                                                            var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + ele_val + "\"");
                                                                        } else if (input_ele.is('select[multiple]')) {
                                                                            var ele_val = input_ele.children('option:selected').map(function () {
                                                                                return $(this).val();
                                                                            }).get().filter(Boolean).join("|^|")
                                                                            var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + ele_val + "\"");
                                                                        } else if (input_ele.is('input[type="checkbox"]')) {
                                                                            var ele_val = input_ele.filter(":checked").map(function () {
                                                                                return $(this).val();
                                                                            }).get().filter(Boolean).join("|^|");
                                                                            var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + ele_val + "\"");
                                                                        } else if (input_ele.is('[data-input-type="Currency"]') || input_ele.is('[data-input-type="Number"]')) {
                                                                            var replace_comma = (new RegExp(",", "g"));
                                                                            var input_val = input_ele.val().replace(replace_comma, "");
                                                                            var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, Number(input_val));
                                                                        } else {
                                                                            var input_val = input_ele.val().replace(/\"/g, "\\\'");
                                                                            var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + input_val + "\"");
                                                                        }
                                                                    }
                                                                } else if (function_keyword_names.indexOf(at_str_name) >= 0) {
                                                                    if (at_str_name == "@GivenIf") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, at_str_name.replace(/@/g, ""));
                                                                    } else if (at_str_name == "@Requestor") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        var requestor_name = "";
                                                                        if ($("[name='ID']").val() == "0") {
                                                                            var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                                                            requestor_name = requestor_ele.attr("requestor-name");
                                                                        } else {
                                                                            var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                                                            requestor_name = requestor_ele.attr("requestor-name");
                                                                        }
                                                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + requestor_name + "\"");
                                                                    } else if (at_str_name == "@RequestID") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        var request_id_ele = $('[name="ID"]');
                                                                        var rid_val = request_id_ele.val();
                                                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, rid_val);
                                                                    } else if (at_str_name == "@Status") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        var status_ele = $("#frmrequest").find('[name="Status"]').eq(0);
                                                                        if (status_ele.length >= 1) {
                                                                            var getStatusVal = status_ele.val();
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + getStatusVal + "\"");
                                                                        }
                                                                    } else if (at_str_name == "@CurrentUser") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        var CurrentUserName = "";
                                                                        var element_in_progress = $('[name="CurrentUser"]');
                                                                        if (typeof element_in_progress.attr("current-user-name") != "undefined") {
                                                                            CurrentUserName = element_in_progress.attr("current-user-name");
                                                                        }
                                                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + CurrentUserName + "\"");
                                                                    } else if (at_str_name == "@Department") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        var getDep_ele = $("[name='Department_Name']");
                                                                        if (getDep_ele.length >= 1) {
                                                                            var getDepName = getDep_ele.val();
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + getDepName + "\"");
                                                                        }
                                                                    } else if (at_str_name == "@TrackNo") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        var track_no_ele = $("#frmrequest").find('[name="TrackNo"]').eq(0);
                                                                        if (track_no_ele.length >= 1) {
                                                                            var getTrackNoVal = track_no_ele.val();
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + getTrackNoVal + "\"");
                                                                        }
                                                                    } else if (at_str_name == "@Processor") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        if ($("[name='ID']").val() == "0") {
                                                                            var processor_user_ele = $('[name="Processor"]');
                                                                            var processor_name = processor_user_ele.attr("processor-name");
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + processor_name + "\"");
                                                                        } else {
                                                                            var processor_user_ele = $("#processor_display");
                                                                            var processor_name = processor_user_ele.text();
                                                                            dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + processor_name + "\"");
                                                                        }
                                                                    } else if (at_str_name == "@Today") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        var this_today = new Date(getUpdatedServerTime());
                                                                        this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2);
                                                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + cleanStringValue(this_today) + "\"");
                                                                    } else if (at_str_name == "@Now") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        var this_today = new Date(getUpdatedServerTime());
                                                                        this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2) + " " + StrRight("0" + this_today.getHours(), 2) + ":" + StrRight("0" + this_today.getMinutes(), 2) + ":" + StrRight("0" + this_today.getSeconds(), 2);
                                                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + cleanStringValue(this_today) + "\"");
                                                                    } else if (at_str_name == "@TimeStamp") {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        var this_timestamp = $('body').data("date_page_visited");
                                                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + this_timestamp + "\"");
                                                                    } else {
                                                                        var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, at_str_name.replace(/@/g, ""));
                                                                    }
                                                                }
                                                            }

                                                            var formula_process_ok = false;
                                                            var evaluated_formula = false;
                                                            // console.log("dis_visible_formula_process", dis_visible_formula_process)
                                                            try {
                                                                dis_visible_formula_process = dis_visible_formula_process.replace(/\/\/.*\n|\/\/.*$/g, "");
                                                                evaluated_formula = eval(dis_visible_formula_process);
                                                                formula_process_ok = true;
                                                            } catch (err) {
                                                                formula_process_ok = false;
                                                            }
                                                            // console.log("dis_visible_formula_process",dis_visible_formula_process)
                                                            // console.log("EMBESD", evaluated_formula)
                                                            if (formula_process_ok) {
                                                                if ($.isArray(evaluated_formula)) {
                                                                    //embed multi visibility affected
                                                                    {
                                                                        if (parent_ele_data["dis_ele"].is(".embed-view-container")) {
                                                                            if (evaluated_formula.length % 2 == 0) {
                                                                                for (var ctr_index in evaluated_formula) {
                                                                                    if (ctr_index % 2 == 0) {
                                                                                        var formula_parsed = evaluated_formula[ctr_index];
                                                                                        var target_type = evaluated_formula[Number(ctr_index) + 1];

                                                                                        // console.log("WHOAAHAA",evaluated_formula[ctr_index], evaluated_formula[Number(ctr_index)+1])
                                                                                        if (target_type == 1) {//WHOLE EMBED
                                                                                            if (formula_parsed == true) {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                                                                                dis_embed_container.css("display", "");
                                                                                            } else {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                                                                                dis_embed_container.css("display", "none");
                                                                                            }
                                                                                        } else if (target_type == 2) {//Column ACTIONS
                                                                                            if (formula_parsed == true) {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                                                var head_actions_column_ele = dis_embed_container.find("thead th.embed-table-actions").eq(0);
                                                                                                var column_index = head_actions_column_ele.index();
                                                                                                var whole_column_action = dis_embed_container.find('tbody td').filter(function (arr_i, val) {
                                                                                                    return $(this).index() == column_index;
                                                                                                });
                                                                                                whole_column_action.add(head_actions_column_ele).show();
                                                                                            } else {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                                                var head_actions_column_ele = dis_embed_container.find("thead th.embed-table-actions").eq(0);
                                                                                                var column_index = head_actions_column_ele.index();
                                                                                                var whole_column_action = dis_embed_container.find('tbody td').filter(function (arr_i, val) {
                                                                                                    return $(this).index() == column_index;
                                                                                                });
                                                                                                whole_column_action.add(head_actions_column_ele).hide();
                                                                                            }


                                                                                        } else if (target_type == 3) {//ADD LINK 
                                                                                            if (formula_parsed == true) {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                                                dis_embed_container
                                                                                                        .find(".embed_newRequest").not("[embed-import='true']")
                                                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                                                        // .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                                                        .css("display", "inline-block");
                                                                                            } else {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                                                dis_embed_container
                                                                                                        .find(".embed_newRequest").not("[embed-import='true']")
                                                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                                                        // .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                                                        // .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                                                        .css("display", "none");
                                                                                            }
                                                                                            //ADDED BY JOSHUA CLIFFORD REYES 09/22/2015
                                                                                        } else if (target_type == 4) {//COPY ACTION
                                                                                            if (formula_parsed == true) {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                                                        .find("[id='copyTDEmbedded']")
                                                                                                        .css("display", "inline-block");
                                                                                            } else {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                                                        .find("[id='copyTDEmbedded']")
                                                                                                        .css("display", "none");
                                                                                            }
                                                                                        } else if (target_type == 5) {//DELETE ACTION
                                                                                            if (formula_parsed == true) {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.css("display", "inline-block");
                                                                                                        //.add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                                                        .find("[id='deleteTDEmbedded']")
                                                                                                        .css("display", "inline-block");
                                                                                            } else {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.css("display", "none");
                                                                                                        //.add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                                                        .find("[id='deleteTDEmbedded']")
                                                                                                        .css("display", "none");
                                                                                            }
                                                                                        } else if (target_type == 6) {//IN-LINE ACTION
                                                                                            if (formula_parsed == true) {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                                                        .find("[id='editDEmbedded']")
                                                                                                        .css("display", "inline-block");
                                                                                            } else {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                                                        .find("[id='editDEmbedded']")
                                                                                                        .css("display", "none");
                                                                                            }
                                                                                        } else if (target_type == 7) {//POP-UP ACTION
                                                                                            if (formula_parsed == true) {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                                                        .find('.viewTDEmbedded[data-action-attr="edit"]')
                                                                                                        .css("display", "inline-block");
                                                                                            } else {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                                                        .find('.viewTDEmbedded[data-action-attr="edit"]')
                                                                                                        .css("display", "none");
                                                                                            }
                                                                                        } else if (target_type == 8) {//VIEW ACTION
                                                                                            if (formula_parsed == true) {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                                                        .find('.viewTDEmbedded[data-action-attr="view"]')
                                                                                                        .css("display", "inline-block");
                                                                                            } else {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                                                        .find('.viewTDEmbedded[data-action-attr="view"]')
                                                                                                        .css("display", "none");
                                                                                            }
                                                                                        } else if (target_type == 9) {//IMPORT
                                                                                            if (formula_parsed == true) {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent();
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                                                        .find('.import_record')
                                                                                                        .css("display", "inline-block");
                                                                                            } else {
                                                                                                var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                                                dis_embed_container
                                                                                                        //.find(".embed_newRequest")
                                                                                                        //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                                                        .find('.import_record')
                                                                                                        .css("display", "none");
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    // if (parent_ele_data["dis_ele"].is(".embed-view-container")) {
                                                                    //     if (parent_ele_data["dis_ele"].is('.embed-view-container[data-embed-view-visibility="true"]') && evaluated_formula[1] == true) {
                                                                    //         var dis_embed_container = parent_ele_data["dis_ele"].parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                                                    //         dis_embed_container.css("display", "");
                                                                    //     } else if (parent_ele_data["dis_ele"].is('.embed-view-container[data-embed-view-visibility="true"]') && evaluated_formula[1] == false) {
                                                                    //         var dis_embed_container = parent_ele_data["dis_ele"].parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                                                    //         dis_embed_container.css("display", "none");
                                                                    //     }

                                                                    //     if (parent_ele_data["dis_ele"].is('.embed-view-container[data-action-event-visibility="true"]') && evaluated_formula[0] == true) {
                                                                    //         var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                    //         dis_embed_container
                                                                    //                 .find(".embed_newRequest")
                                                                    //                 //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                    //                 .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                    //                 .css("display", "");
                                                                    //     } else if (parent_ele_data["dis_ele"].is('.embed-view-container[data-action-event-visibility="true"]') && evaluated_formula[0] == false) {
                                                                    //         var dis_embed_container = parent_ele_data["dis_ele"].parent()
                                                                    //         dis_embed_container
                                                                    //                 .find(".embed_newRequest")
                                                                    //                 //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                    //                 .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                    //                 .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                    //                 .css("display", "none");
                                                                    //     }
                                                                    // }


                                                                    //add another multiples object target different FUNCTIONALITY for other objjects
                                                                    //<---------------------->

                                                                } else if (evaluated_formula && $.type(evaluated_formula) == "boolean") {

                                                                    if (parent_ele_data["dis_ele"].attr("role")) {
                                                                        if (parent_ele_data["dis_ele"].attr("role").length >= 1 && parent_ele_data["dis_ele"].attr("role") == "tab") {
                                                                            parent_ele_data["dis_ele"].eq(0).show();
                                                                            if( parent_ele_data["dis_ele"].parent().children('li:visible').is(':not(.ui-tabs-active.ui-state-active)') ){ // if all visible tabs has no focus then I must set a focus on the visible tabs
                                                                                parent_ele_data["dis_ele"].parent().children('li:visible').eq(0).find('a[href]').eq(0).trigger('click')
                                                                            }
                                                                            // console.log("ui-tabs-active ui-state-active", parent_ele_data["dis_ele"].eq(0));
                                                                            // var href = parent_ele_data["dis_ele"].eq(0).find('a[href]').eq(0).attr("href");
                                                                            // if (parent_ele_data["dis_ele"].eq(0).find('a[href]').eq(0).is(":visible")) {
                                                                            //     if (parent_ele_data["dis_ele"].eq(0).is('.ui-tabs-active.ui-state-active')) {
                                                                            //         parent_ele_data["dis_ele"].eq(0).find('a[href]').eq(0).trigger('click');
                                                                            //     }else if (parent_ele_data["dis_ele"].parent().children('li:visible').length == 1){
                                                                            //         parent_ele_data["dis_ele"].parent().children('li:visible').find('a[href]').eq(0).trigger('click');
                                                                            //     }
                                                                            //     //parent_ele_data["dis_ele"].eq(0).find('a[href]').eq(0).trigger('click');
                                                                            // }
                                                                            // if (parent_ele_data["dis_ele"].eq(0).is('.ui-tabs-active.ui-state-active')) {
                                                                            //     parent_ele_data["dis_ele"].eq(0).parents('.form-tabbable-pane').eq(0).find(href).show();
                                                                            // }
                                                                            //parent_ele_data["dis_ele"].eq(0).parents('.form-tabbable-pane').eq(0).find(href).show();
                                                                        }

                                                                    } else {
                                                                        if (parent_ele_data["dis_ele"].attr("accordion-name")) {
                                                                            parent_ele_data["dis_ele"].add('[rep-original-name="' + parent_ele_data["dis_ele_name"] + '"]').each(function () {
                                                                                $(this).show()
                                                                            });
                                                                        } else if (parent_ele_data["dis_ele"].parents('.setOBJ:eq(0)').is('[data-type="attachment_on_request"]')) {
                                                                            parent_ele_data["dis_ele"].parent().children('form:eq(0)').show();
                                                                        } else if (parent_ele_data["dis_ele"].parents('.setOBJ:eq(0)').is('[data-type="multiple_attachment_on_request"]')) {
                                                                            parent_ele_data["dis_ele"].parent().children('form:eq(0)').show();
                                                                        } else {
                                                                            var selectors_to_show = parent_ele_data["dis_ele"].add('[rep-original-name="' + parent_ele_data["dis_ele_name"] + '"]');
                                                                            selectors_to_show.each(function () {
                                                                                $(this).parents(".setOBJ").eq(0).show();
                                                                            })
                                                                        }
                                                                    }

                                                                } else if ($.type(evaluated_formula) == "boolean") {

                                                                    // console.log(parent_ele_data["dis_ele_name"],"AMF",parent_ele_data["dis_ele"].add('[rep-original-name="'+parent_ele_data["dis_ele_name"]+'"]'))
                                                                    if (parent_ele_data["dis_ele"].attr("role")) {
                                                                        if (parent_ele_data["dis_ele"].attr("role").length >= 1 && parent_ele_data["dis_ele"].attr("role") == "tab") {
                                                                            parent_ele_data["dis_ele"].eq(0).hide()
                                                                            var href = parent_ele_data["dis_ele"].eq(0).find('a[href]').eq(0).attr("href");
                                                                            parent_ele_data["dis_ele"].eq(0).parents('.form-tabbable-pane').eq(0).find(href).hide();
                                                                            if (parent_ele_data["dis_ele"].hasClass('ui-tabs-active') || parent_ele_data["dis_ele"].hasClass('ui-state-active')) {
                                                                                parent_ele_data["dis_ele"].parent().children('li[role="tab"]:visible').eq(0).children('a[role="presentation"]').eq(0).trigger("click");
                                                                            }
                                                                        }
                                                                    } else {
                                                                        if (parent_ele_data["dis_ele"].attr("accordion-name")) {
                                                                            parent_ele_data["dis_ele"].add('[rep-original-name="' + parent_ele_data["dis_ele_name"] + '"]').each(function () {
                                                                                $(this).hide();
                                                                            })
                                                                        } else if (parent_ele_data["dis_ele"].parents('.setOBJ:eq(0)').is('[data-type="attachment_on_request"]')) {
                                                                            parent_ele_data["dis_ele"].parent().children('form:eq(0)').hide();
                                                                        } else if (parent_ele_data["dis_ele"].parents('.setOBJ:eq(0)').is('[data-type="multiple_attachment_on_request"]')) {
                                                                            parent_ele_data["dis_ele"].parent().children('form:eq(0)').hide();
                                                                        } else {
                                                                            var selectors_to_hide = parent_ele_data["dis_ele"].add('[rep-original-name="' + parent_ele_data["dis_ele_name"] + '"]');
                                                                            selectors_to_hide.each(function () {
                                                                                $(this).parents(".setOBJ").eq(0).hide();
                                                                            })
                                                                        }
                                                                    }
                                                                }

                                                                checkStaticTablesGAP();
                                                            }
                                                        }
                                                    }
                                                })
                                            }
                                        }
                                    })
                                }
                            } else if (function_keyword_names.indexOf(at_str_name) >= 0) {
                                if (at_str_name == "@GivenIf") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    dis_visible_formula_process = dis_visible_formula_process.replace(re, at_str_name.replace(/@/g, ""));
                                } else if (at_str_name == "@Requestor") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    var requestor_name = "";
                                    if ($("[name='ID']").val() == "0") {
                                        var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                        requestor_name = requestor_ele.attr("requestor-name");
                                    } else {
                                        // var requestor_ele = $("#requestor_display");
                                        // requestor_name = requestor_ele.text();
                                        var requestor_ele = $("#frmrequest").find('[name="Requestor"]').eq(0);
                                        requestor_name = requestor_ele.attr("requestor-name");
                                    }
                                    dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + requestor_name + "\"");
                                } else if (at_str_name == "@RequestID") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    var request_id_ele = $('[name="ID"]');
                                    var rid_val = request_id_ele.val();
                                    dis_visible_formula_process = dis_visible_formula_process.replace(re, rid_val);
                                } else if (at_str_name == "@Status") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    var status_ele = $("#frmrequest").find('[name="Status"]').eq(0);
                                    if (status_ele.length >= 1) {
                                        var getStatusVal = status_ele.val();
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + getStatusVal + "\"");
                                    }
                                } else if (at_str_name == "@CurrentUser") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    var CurrentUserName = "";
                                    var element_in_progress = $('[name="CurrentUser"]');
                                    if (typeof element_in_progress.attr("current-user-name") != "undefined") {
                                        CurrentUserName = element_in_progress.attr("current-user-name");
                                    }
                                    dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + CurrentUserName + "\"");
                                } else if (at_str_name == "@Department") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    var getDep_ele = $("[name='Department_Name']");
                                    if (getDep_ele.length >= 1) {
                                        var getDepName = getDep_ele.val();
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + getDepName + "\"");
                                    }
                                } else if (at_str_name == "@TrackNo") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    var track_no_ele = $("#frmrequest").find('[name="TrackNo"]').eq(0);
                                    if (track_no_ele.length >= 1) {
                                        var getTrackNoVal = track_no_ele.val();
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + getTrackNoVal + "\"");
                                    }
                                } else if (at_str_name == "@Processor") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    if ($("[name='ID']").val() == "0") {
                                        var processor_user_ele = $('[name="Processor"]');
                                        var processor_name = processor_user_ele.attr("processor-name");
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + processor_name + "\"");
                                    } else {
                                        var processor_user_ele = $("#processor_display");
                                        var processor_name = processor_user_ele.text();
                                        dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + processor_name + "\"");
                                    }
                                } else if (at_str_name == "@Today") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    var this_today = new Date(getUpdatedServerTime());
                                    this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2);
                                    dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + cleanStringValue(this_today) + "\"");
                                } else if (at_str_name == "@Now") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    var this_today = new Date(getUpdatedServerTime());
                                    this_today = this_today.getFullYear() + "-" + StrRight("0" + (this_today.getMonth() + 1), 2) + "-" + StrRight("0" + this_today.getDate(), 2) + " " + StrRight("0" + this_today.getHours(), 2) + ":" + StrRight("0" + this_today.getMinutes(), 2) + ":" + StrRight("0" + this_today.getSeconds(), 2);
                                    dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + cleanStringValue(this_today) + "\"");
                                } else if (at_str_name == "@TimeStamp") {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    var this_timestamp = $('body').data("date_page_visited");
                                    dis_visible_formula_process = dis_visible_formula_process.replace(re, "\"" + this_timestamp + "\"");
                                } else {
                                    var re = new RegExp(at_str_name + "(?![a-zA-Z0-9_])", "g");
                                    dis_visible_formula_process = dis_visible_formula_process.replace(re, at_str_name.replace(/@/g, ""));
                                }
                            }
                        }

                        console.log("dis_visible_formula_process", dis_visible_formula_process)
                        var formula_process_ok = false;
                        var evaluated_formula = false;
                        try {
                            dis_visible_formula_process = dis_visible_formula_process.replace(/\/\/.*\n|\/\/.*$/g, "");
                            evaluated_formula = eval(dis_visible_formula_process);
                            // console.log("vonchange2_evaluated_formula_OK", evaluated_formula);
                            formula_process_ok = true;
                        } catch (err) {
                            // console.log("vonchange2_evaluated_formula_NOT_OK", evaluated_formula);
                            formula_process_ok = false;
                        }

                        if (formula_process_ok) {
                            if ($.isArray(evaluated_formula)) {
                                //embed multi visibility affected
                                if (dis_ele.is(".embed-view-container")) {
                                    if (evaluated_formula.length % 2 == 0) {
                                        for (var ctr_index in evaluated_formula) {
                                            if (ctr_index % 2 == 0) {
                                                var formula_parsed = evaluated_formula[ctr_index];
                                                var target_type = evaluated_formula[Number(ctr_index) + 1];

                                                // console.log("WHOAAHAA",evaluated_formula[ctr_index], evaluated_formula[Number(ctr_index)+1])
                                                if (target_type == 1) {//WHOLE EMBED
                                                    if (formula_parsed == true) {
                                                        var dis_embed_container = dis_ele.parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                                        dis_embed_container.css("display", "");
                                                    } else {
                                                        var dis_embed_container = dis_ele.parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                                        dis_embed_container.css("display", "none");
                                                    }
                                                } else if (target_type == 2) {//COLUMN ACTION
                                                    if (formula_parsed == true) {
                                                        var dis_embed_container = dis_ele.parent();
                                                        var head_actions_column_ele = dis_embed_container.find("thead th.embed-table-actions").eq(0);
                                                        var column_index = head_actions_column_ele.index();
                                                        var whole_column_action = dis_embed_container.find('tbody td').filter(function (arr_i, val) {
                                                            return $(this).index() == column_index;
                                                        });

                                                        whole_column_action/*.add(dis_embed_container.find('.embed_newRequest').eq(0))*/
                                                                .add(head_actions_column_ele).show();
                                                    } else {
                                                        var dis_embed_container = dis_ele.parent();
                                                        var head_actions_column_ele = dis_embed_container.find("thead th.embed-table-actions").eq(0);
                                                        var column_index = head_actions_column_ele.index();
                                                        var whole_column_action = dis_embed_container.find('tbody td').filter(function (arr_i, val) {
                                                            return $(this).index() == column_index;
                                                        });
                                                        whole_column_action/*.add(dis_embed_container.find('.embed_newRequest').eq(0))*/
                                                                .add(head_actions_column_ele).hide();
                                                    }
                                                } else if (target_type == 3) {//ADD LINK
                                                    if (formula_parsed == true) {
                                                        var dis_embed_container = dis_ele.parent();
                                                        dis_embed_container
                                                                .find(".embed_newRequest").not('[embed-import="true"]')
                                                                //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                // .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                // .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                                                // .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                                                // .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                // .add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                // .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                .css("display", "inline-block");
                                                    } else {
                                                        var dis_embed_container = dis_ele.parent()
                                                        dis_embed_container
                                                                .find(".embed_newRequest").not('[embed-import="true"]')
                                                                //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                // .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                // .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                                                // .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                                                // .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                // .add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                // .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                .css("display", "none");
                                                    }
                                                    //ADDED BY JOSHUA CLIFFORD REYES 09/22/2015
                                                } else if (target_type == 4) {//COPY ACTION
                                                    if (formula_parsed == true) {
                                                        var dis_embed_container = dis_ele.parent();
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                .find("[id='copyTDEmbedded']")
                                                                .css("display", "inline-block");
                                                    } else {
                                                        var dis_embed_container = dis_ele.parent()
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                                                .find("[id='copyTDEmbedded']")
                                                                .css("display", "none");
                                                    }
                                                } else if (target_type == 5) {//DELETE ACTION
                                                    if (formula_parsed == true) {
                                                        var dis_embed_container = dis_ele.parent();
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.css("display", "inline-block");
                                                                //.add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                .find("[id='deleteTDEmbedded']")
                                                                .css("display", "inline-block");
                                                    } else {
                                                        var dis_embed_container = dis_ele.parent()
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.css("display", "none");
                                                                //.add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                                                .find("[id='deleteTDEmbedded']")
                                                                .css("display", "none");
                                                    }
                                                } else if (target_type == 6) {//IN-LINE ACTION
                                                    if (formula_parsed == true) {
                                                        var dis_embed_container = dis_ele.parent();
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                .find("[id='editDEmbedded']")
                                                                .css("display", "inline-block");
                                                    } else {
                                                        var dis_embed_container = dis_ele.parent()
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find("[id='editDEmbedded']"))
                                                                .find("[id='editDEmbedded']")
                                                                .css("display", "none");
                                                    }
                                                } else if (target_type == 7) {//POP-UP ACTION
                                                    if (formula_parsed == true) {
                                                        var dis_embed_container = dis_ele.parent();
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                .find('.viewTDEmbedded[data-action-attr="edit"]')
                                                                .css("display", "inline-block");
                                                    } else {
                                                        var dis_embed_container = dis_ele.parent()
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                                                .find('.viewTDEmbedded[data-action-attr="edit"]')
                                                                .css("display", "none");
                                                    }
                                                } else if (target_type == 8) {//VIEW ACTION
                                                    if (formula_parsed == true) {
                                                        var dis_embed_container = dis_ele.parent();
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                .find('.viewTDEmbedded[data-action-attr="view"]')
                                                                .css("display", "inline-block");
                                                    } else {
                                                        var dis_embed_container = dis_ele.parent()
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                .find('.viewTDEmbedded[data-action-attr="view"]')
                                                                .css("display", "none");
                                                    }
                                                } else if (target_type == 9) {//IMPORT
                                                    if (formula_parsed == true) {
                                                        var dis_embed_container = dis_ele.parent();
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                .find('.import_record')
                                                                .css("display", "inline-block");
                                                    } else {
                                                        var dis_embed_container = dis_ele.parent()
                                                        dis_embed_container
                                                                //.find(".embed_newRequest")
                                                                //.add(dis_embed_container.find(".viewTDEmbedded"))
                                                                .find('.import_record')
                                                                .css("display", "none");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                {
                                    // if (dis_ele.is(".embed-view-container")) {
                                    //     if (dis_ele.is('.embed-view-container[data-embed-view-visibility="true"]') && evaluated_formula[1] == true) {
                                    //         var dis_embed_container = dis_ele.parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                    //         dis_embed_container.css("display", "");
                                    //     } else if (dis_ele.is('.embed-view-container[data-embed-view-visibility="true"]') && evaluated_formula[1] == false) {
                                    //         var dis_embed_container = dis_ele.parents('.setOBJ[data-type="embeded-view"]').eq(0);
                                    //         dis_embed_container.css("display", "none");
                                    //     }

                                    //     if (dis_ele.is('.embed-view-container[data-action-event-visibility="true"]') && evaluated_formula[0] == true) {
                                    //         var dis_embed_container = dis_ele.parent()
                                    //         dis_embed_container
                                    //                 .find(".embed_newRequest")
                                    //                 //.add(dis_embed_container.find(".viewTDEmbedded"))
                                    //                 .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                    //                 .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                    //                 .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                    //                 .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                    //                 .add(dis_embed_container.find("[id='editDEmbedded']"))
                                    //                 .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                    //                 .css("display", "");
                                    //     } else if (dis_ele.is('.embed-view-container[data-action-event-visibility="true"]') && evaluated_formula[0] == false) {
                                    //         var dis_embed_container = dis_ele.parent()
                                    //         dis_embed_container
                                    //                 .find(".embed_newRequest")
                                    //                 //.add(dis_embed_container.find(".viewTDEmbedded"))
                                    //                 .add(dis_embed_container.find("[id='copyTDEmbedded']"))
                                    //                 .add(dis_embed_container.find("[id='pasteTDEmbedded']"))
                                    //                 .add(dis_embed_container.find("[id='cancelpasteDEmbedded']"))
                                    //                 .add(dis_embed_container.find("[id='deleteTDEmbedded']"))
                                    //                 .add(dis_embed_container.find("[id='editDEmbedded']"))
                                    //                 .add(dis_embed_container.find('.viewTDEmbedded[data-action-attr="edit"]'))
                                    //                 .css("display", "none");
                                    //     }
                                    // }
                                }



                                //add another multiples object target different FUNCTIONALITY for other objjects
                                //<---------------------->

                                //locate here
                            } else if (evaluated_formula == true && $.type(evaluated_formula) == "boolean") {
                                console.log("this_ele", dis_ele);
                                if (dis_ele.attr("role")) {
                                    if (dis_ele.attr("role").length >= 1 && dis_ele.attr("role") == "tab") {
                                        dis_ele.eq(0).show();
                                        var href = dis_ele.eq(0).find('a[href]').eq(0).attr("href");
                                        if (dis_ele.eq(0).find('a[href]').eq(0).is(":visible")) {
                                            // dis_ele.eq(0).find('a[href]').eq(0).trigger("click");
                                        }
                                        // dis_ele.eq(0).parents('.form-tabbable-pane').eq(0).find(href).show();
                                    }
                                } else {
                                    if (dis_ele.attr("accordion-name")) {
                                        dis_ele.add('[rep-original-name="' + dis_ele.attr("accordion-name") + '"]').each(function () {
                                            $(this).show()
                                        })
                                    } else if (dis_ele.parents('.setOBJ:eq(0)').is('[data-type="attachment_on_request"]')) {
                                        dis_ele.parent().children('form:eq(0)').show();
                                    } else if (dis_ele.parents('.setOBJ:eq(0)').is('[data-type="multiple_attachment_on_request"]')) {
                                        dis_ele.parent().children('form:eq(0)').show();
                                    } else {
                                        var selectors_to_show = dis_ele.add('[rep-original-name="' + dis_ele.attr("accordion-name") + '"]');
                                        // if(dis_ele.is('[data-type="listNames"]')){
                                        //     selectors_to_show.add()
                                        // }

                                        selectors_to_show.each(function () {
                                            $(this).parents(".setOBJ").eq(0).show()
                                        })
                                    }
                                }

                            } else if ($.type(evaluated_formula) == "boolean") {
                                if (dis_ele.attr("role")) {
                                    if (dis_ele.attr("role").length >= 1 && dis_ele.attr("role") == "tab") {
                                        dis_ele.eq(0).hide();
                                        var href = dis_ele.eq(0).find('a[href]').eq(0).attr("href");
                                        dis_ele.eq(0).parents('.form-tabbable-pane').eq(0).find(href).hide();
                                        if (dis_ele.hasClass('ui-tabs-active') || dis_ele.hasClass('ui-state-active')) {
                                            // console.log("dis_ele.parent()", dis_ele.parent(), dis_ele.parent().children('li[role="tab"]:visible').eq(0))
                                            dis_ele.parent().children('li[role="tab"]:visible').eq(0).children('a[role="presentation"]').eq(0).trigger("click");
                                        }
                                    }

                                } else {
                                    if (dis_ele.is("[accordion-name]")) {

                                        dis_ele.add('[rep-original-name="' + dis_ele.attr("accordion-name") + '"]').each(function () {
                                            $(this).hide();
                                        })
                                    } else if (dis_ele.parents('.setOBJ:eq(0)').is('[data-type="attachment_on_request"]')) {
                                        dis_ele.parent().children('form:eq(0)').hide();
                                    } else if (dis_ele.parents('.setOBJ:eq(0)').is('[data-type="multiple_attachment_on_request"]')) {
                                        dis_ele.parent().children('form:eq(0)').hide();
                                    } else {
                                        var selectors_to_show = dis_ele.add('[rep-original-name="' + dis_ele.attr("accordion-name") + '"]');
                                        // if(dis_ele){

                                        // }
                                        selectors_to_show.each(function () {

                                            $(this).parents(".setOBJ").eq(0).hide()
                                        })
                                    }
                                }


                            }

                        }
                    }
                }
            })
            checkStaticTablesGAP();
        },
        "visibilityOnChange": function () {
            var self = this;
            $('[field-visible="computed"][visible-formula]').each(function () {
                var dis_ele = $(this)
                var dis_visible_formula = dis_ele.attr('visible-formula');
                var regeks_get_name = new RegExp("@[a-zA-Z][a-zA-Z0-9_]*", "g");
                var names_in_formula = dis_visible_formula.match(regeks_get_name)
                if (names_in_formula != null) {
                    names_in_formula = names_in_formula.filter(Boolean);
                }
                var fname_ele = null;
                for (var ii in names_in_formula) {
                    fname_ele = $('[name="' + names_in_formula[ii].replace("@", "") + '"]');
                    if (fname_ele.length >= 1) {
                        self.computeVisibility.call(dis_ele);
                        fname_ele.on("change", function () {
                            var with_formula_ele = dis_ele;
                            self.computeVisibility.call($(this).add(with_formula_ele));
                        });
                    }
                }
            });
        },
        addCommentRequest: function () {
            $("body").on("click", "#reply", function (e) {
                var fID = $(this).attr("data-f-id");
                replyComment(this, fID);
            });
        },
        formatCurrency: function () {
            $('[data-input-type="Currency"]').each(function () {
                var currency = $(this).val();
                if (currency[0] == "-") {
                    var newValue = Number("-" + currency.replace(/[^0-9\.]+/g, ""));
                } else {
                    var newValue = Number(currency.replace(/[^0-9\.]+/g, ""));
                }
                $(this).val(newValue);
            });
        }
    }
}



//Workspace
//create pdf
//create pdf
// @size = a4
// Limitation
// A4 Size Only, No tabs, accordion, embed
// var widthForm = $(".preview_content").width();
//             var heightForm = $(".preview_content").parent().height();
// var 
// htmlPDF = $(".loaded_form_content"),
// formName = $("#FormName").val(),
// //cache_width = formName.width(),
// cache_width="",
// a4  =[heightForm,widthForm]; 

// $('body').scrollTop(0);
// createPDF(formName,htmlPDF,a4,cache_width);
// console.log(1)


// function createPDF(formName,formElement,size,cache_width){

//     getCanvas(formElement,size).then(function(canvas){
//         var 
//         img = canvas.toDataURL("image/png"),
// l = {
// orientation: 'l',
// unit: 'mm',
// compress: true,
// autoSize: false
// };
//         doc = new jsPDF(l);
// //{
//           //unit:'px', 
//           //format:'a4'
//         //}     
//         doc.addImage(img, 'JPEG', 20, 20);
//         doc.save(formName + '.pdf');
//         //formElement.width(cache_width);
//     });
//     console.log(2)
// }

// // create canvas object
// function getCanvas(formElement,size){
//     //formElement.width((size[0]*1.33333) -80).css('max-width','none');
//     return html2canvas(formElement,{
//         imageTimeout:2000,
//         removeContainer:true
//     }); 
//     console.log(3)
// }


function submitCreatePDF(getID, getFormID, getTrackNo, callback) {
    var widthForm = $(".preview_content").width();
    var heightForm = $(".preview_content").parent().height();

    var
            htmlPDF = $(".loaded_form_content"),
            formName = $("#FormName").val(),
//cache_width = formName.width(),

            cache_width = "",
            a4 = [heightForm, widthForm];

    $('body').scrollTop(0);
    createPDF(getID, getFormID, getTrackNo, formName, htmlPDF, a4, cache_width, function (result) {
        console.log('result', result);
        callback(result);
    });
}
function createPDF(getID, getFormID, getTrackNo, formName, formElement, size, cache_width, callback) {
    getCanvas(formElement, size).then(function (canvas) {
        var
                img = canvas.toDataURL("image/png"),
                l = {
                    orientation: 'l',
                    unit: 'mm',
                    compress: true,
                    autoSize: false
                };

        doc = new jsPDF();
        //{
        //unit:'px', 
        //format:'a4'
        //}     

        doc.addImage(img, 'JPEG', 15, 10, 180, 0);
//        doc.save(formName + '.pdf');

        //formElement.width(cache_width);

        // Send data to server side
        var pdf = doc.output('blob');
        console.log('pdf', pdf);
        var data = new FormData();
        data.append('data', pdf);
        data.append('getID', getID);
        data.append('getFormID', getFormID);
        data.append('getTrackNo', getTrackNo);
        data.append('pdfName', formName);
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function (result) {
            if (this.readyState == 4 && this.status == 200) {
                callback(result);
            }
        }

        xhr.open('POST', '/ajax/savePDF', true);
        xhr.send(data);

    });
}

// create canvas object
function getCanvas(formElement, size) {
    //formElement.width((size[0]*1.33333) -80).css('max-width','none');
    return html2canvas(formElement, {
        imageTimeout: 2000,
        removeContainer: true
    });
}