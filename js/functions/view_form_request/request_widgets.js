$(document).ready(function() {
    // $(".viewRequestComment").click(function() {
    //    RequestWidgets.view_comment();
       
    // });


    // $('body').on('click', '.viewAuditLogs', function() {
    //     var request_id = $(this).attr('data-request-id');
    //     var form_id = $(this).attr('data-form-id');
    //     RequestWidgets.view_logs(request_id, form_id);
    // });

    // $(".viewAuditLogs").click(function() {
    //     var request_id = $('#ID').val();
    //     var form_id = $('#FormID').val();
    //     RequestWidgets.view_logs(request_id, form_id);
    // });
    $('body').on('click', '.viewAuditLogs', function() {
        var request_id = $(this).attr('data-request-id');
        var form_id = $(this).attr('data-form-id');
        if(request_id===undefined && form_id===undefined){
            request_id = $('#ID').val();
            form_id = $('#FormID').val();
        }
        
        RequestWidgets.view_logs(request_id, form_id);
    });

})

RequestWidgets = {
    view_comment: function() {
        //view of comment
        var ret = '<h3><i class="icon-comment"></i> Comments / Suggestion</h3>';
        ret += '<div class="hr"></div>';
        ret += '</div>';
        ret += '<div id="comment-container" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">'
        ret += '<div class="content-dialog sub-container-comment requestComment" style="width: 95%;height: auto;">';
        ret += '<center><img src="/images/loader/loader.gif" class="load-mr display" style="margin-top:20%;"></center>';

        ret += '</div>';
        ret += '</div>';
        ret += '<div class="fields" style="border-top:1px solid #ddd;">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
        ret += '</div>';
        ret += '</div>';
        jDialog(ret, "", "", "", "", function() {
        });
        $(".load-mr").show();
        var reqID = $("#ID").val();
        var formID = $("#FormID").val();
        $.ajax({
            type: "POST",
            url: "/ajax/post",
            dataType: "json",
            cache: false,
            data: "action=getComment" + "&postID=" + reqID + "&formID=" + formID,
            success: function(result) {
                if (result) {
                    for (var a = 0; a < result.length; a++) {
                        var postDetails = null;
                        var uploadedDetails = null;
                        var likeDetails = null;
                        var comments = result[a];
                        var showComment = replyPost(postDetails, uploadedDetails, likeDetails, comments);
                        $(".requestComment").append(showComment);
                        $("label.timeago").timeago();  // Time To go
                        $(".load-mr").hide();
                    }
                } else {
                    $(".load-mr").hide();
                }
                $("#comment-container").perfectScrollbar();
            }
        });

    },
    view_logs: function(request_id, form_id) {

        //view of logs
        var ret = '<h3 class="fl-margin-bottom"><i class="icon-book fa fa-book"></i> Audit Logs</h3>';
        ret += '<div class="hr"></div>';
        ret += '</div>';
        ret += '<div class="content-dialog-scroll" id="logs-container" style="position: relative; overflow: hidden; padding:0px !important;">'
        ret += '<div class="content-dialog sub-container-logs">'
        ret += '<center><img src="/images/loader/loader.gif" class="load-mr" style="display: inline;margin-top:20%;"></center>'
        ret += '</div>'
        ret += '</div>'
        ret += '<div class="fields">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close" style="margin-bottom:5px;">';
        ret += '</div>';
        ret += '</div>';
       var newDialog = new jDialog(ret, "", "", "", "40", function() {
        });
        newDialog.themeDialog("modal2");
        
        $.post('/ajax/getRequestLogs', {
            formId: form_id,
            request_Id: request_id
        }, function(data) {
            var result = JSON.parse(data);
            var audit_table = '';
             audit_table += '<div class="content-dialog sub-container-logs">';
            for (var index in result) {
               
                audit_table += '<div class="reply fl-padding-bottom " id="reply_80" style="width:100%;">';
                // audit_table += '<a class="message-img" href="#">' + result[index].created_by.image + '</a>';
                audit_table += '<div class="message-body" style="margin-left:0px;">';
                console.log(result[index]);
                audit_table += '<div class="text" id="postText_80" style="width: 94.4%; border: none!important; padding:5px; background:#FBFBFB;"><p>' + result[index].details + '</p></div>';
                audit_table += '<p class="attribution" style="padding-left:5px;">by <a href="#non">' + result[index].created_by_display_name + '</a> at <label class="timeago" title="' + result[index].date_created + '">' + result[index].date_created + '</label>';
                audit_table += '</p></div>';
                audit_table += '</div>';
                
            }
            audit_table += '</div>';

            $('#logs-container').html(audit_table);
           
            //$("#logs-container").perfectScrollbar();
            //$("label.timeago").timeago();
            $('#logs-container').perfectScrollbar().mouseenter(function(){
                $('#logs-container').perfectScrollbar('destroy');
                $('#logs-container').perfectScrollbar();
            });

        });

 

    }
}