var jsonLatestModifiedWorkflowData = {
    "endlimit": "5",
    "search_value": "",
    "column-sort": "date",
    "column-sort-type": "DESC"

}
var columnSort = [];
$(document).ready(function(){
	$('#workflowHistory').click(function () {
        var ret = "";
        ret += '<h3 class="fl-margin-bottom"><i class="fa fa-history"></i> History</h3>';
        ret += '<div class="hr"></div>';
        ret += '<div class="fields fl-workflow-history-list-wrap">';
        ret += '<div class="fl-list-of-app-record-wrapper">';
        ret += '<div class="fl-table-wrapper"  style="min-height:250px">';
        ret += '<table class="display_data dataTable loadWorkflowHistory"  id="loadWorkflowHistory">';
        ret += '<thead class="fl-header-tbl-wrapper">';
        ret += '<tr>';
        ret += '<th style="width: 70px !important;"><div class=""></div><div class="fl-table-ellip"></div></th>';
        ret += '<th><div class=""></div><div class="fl-table-ellip">Title</div></th>';
        ret += '<th><div class=""></div><div class="fl-table-ellip">Status</div></th>';
        ret += '<th><div class=""></div><div class="fl-table-ellip">Date Created</div></th>';
        ret += '<th><div class=""></div><div class="fl-table-ellip">Creator</div></th>';
        ret += '</tr>';
        ret += '</thead>';
        ret += '</table>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';

        var newDialog = new jDialog(ret, "", "800", "", "", function () {
        });
        newDialog.themeDialog("modal2");
        WorkflowHistory.init(function () {

        });

    });
})
WorkflowHistory = {
    init: function (callback) {
        var oTable = $('#loadWorkflowHistory').dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            "oLanguage": {
                "sProcessing": '<div class="spinner load_m"> ' +
                        '<div class="bar1"></div> ' +
                        '<div class="bar2"></div> ' +
                        '<div class="bar3"></div> ' +
                        '<div class="bar4"></div> ' +
                        '<div class="bar5"></div> ' +
                        '<div class="bar6"></div> ' +
                        '<div class="bar7"></div> ' +
                        '<div class="bar8"></div> ' +
                        '<div class="bar9"></div> ' +
                        '<div class="bar10"></div> ' +
                        '</div>'
            },
            "oColReorder": false,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/search",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({"name": "search_value", "value": $.trim(jsonLatestModifiedWorkflowData['search_value'])});
                aoData.push({"name": "action", "value": "loadWorkflowHistory"});
                aoData.push({"name": "column-sort", "value": jsonLatestModifiedWorkflowData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonLatestModifiedWorkflowData['column-sort-type']});
                aoData.push({"name": "endlimit", "value": jsonLatestModifiedWorkflowData['endlimit']});
                aoData.push({"name": "form_id", "value": $('[relForm="form_id"]').val()});
                aoData.push({"name": "workflow_id", "value": $.trim($("#workflowID").text())});
                console.log(jsonLatestModifiedWorkflowData)
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        // console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function (oSettings, json) {
                var obj = '#loadWorkflowHistory';
                var listOfappContainer = '.fl-list-of-app-record-wrapper';
                dataTable_widget(obj, listOfappContainer);
                $(".content-dialog-scroll").perfectScrollbar("update");
            },
            fnDrawCallback: function () {

                $(".tip").tooltip();
                if (callback) {
                    callback(1);
                }
            }
        });
        return oTable;
    }
}