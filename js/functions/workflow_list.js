var jsonWorkflowData = {
    search_value: "",  
    start: 0,
    limit: 10, 
}
var columnSort = [];
$(document).ready(function() {
    $("body").on("keyup","#txtSearchWorkflowDatatable",function(e){
        if (e.keyCode == "13") {
            $(".dataTable_workflow").dataTable().fnDestroy();
            jsonWorkflowData['search_value'] = $(this).val();
            GetWorkflowDataTable.defaultData();
        }
    })
    $("body").on("click","#btnSearchWorkflowDatatable",function(e){
        $(".dataTable_workflow").dataTable().fnDestroy();
        jsonWorkflowData['search_value'] = $("#txtSearchWorkflowDatatable").val();
        GetWorkflowDataTable.defaultData();
    })
    //load
    GetWorkflowDataTable.defaultData();
    //sort
    $("body").on("click",".dataTable_workflow th",function(){
        var cursor = $(this).css("cursor");
        jsonWorkflowData['column-sort'] = $(this).attr("field_name");
        if($(this).attr("field_name")=="" || $(this).attr("field_name")==undefined || cursor=="col-resize"){
            return;
        }
        var indexcolumnSort = columnSort.indexOf($(this).attr("field_name"));

        $(this).closest(".dataTable_workflow").find(".sortable-image").html("");
        if(indexcolumnSort==-1){
            columnSort.push($(this).attr("field_name"))
            $(this).find(".sortable-image").html("<i class='fa fa-sort-asc'></i>")
            jsonWorkflowData['column-sort-type'] = "ASC";
        }else{
            columnSort.splice(indexcolumnSort,1);
            $(this).find(".sortable-image").html("<i class='fa fa-sort-desc'></i>")
            jsonWorkflowData['column-sort-type'] = "DESC";
        }
        
        $(".dataTable_workflow").dataTable().fnDestroy();
        var self = this;
        GetWorkflowDataTable.defaultData(function(){
             addIndexClassOnSort(self);
        });
        //show entries
    })
    //show entries
    $(".searchWorkflowLimitPerPage").change(function(e) {
        // if (e.keyCode == "13") {
            var val = parseInt($(this).val());
            jsonWorkflowData['limit'] = val;
            $(".dataTable_workflow").dataTable().fnDestroy();
            // var self = this;
            var oTable = GetWorkflowDataTable.defaultData();


        // }
    })
})

GetWorkflowDataTable = {
    "defaultData" : function(callback){
        var oColReorder = {
            allowReorder : false,
            allowResize : true
        };
        var oTable = $(".dataTable_workflow").dataTable({
            "sDom": 'Rlfrtip',
            // "sScrollY": "400px",
            // "oLanguage": {
            //     "sProcessing": '<div class="spinner load_m"> '+
            //            '<div class="bar1"></div> '+
            //            '<div class="bar2"></div> '+
            //            '<div class="bar3"></div> '+
            //            '<div class="bar4"></div> '+
            //            '<div class="bar5"></div> '+
            //            '<div class="bar6"></div> '+
            //           '<div class="bar7"></div> '+
            //           '<div class="bar8"></div> '+
            //           '<div class="bar9"></div> '+
            //           '<div class="bar10"></div> '+
            //      '</div>'
            // },
            "oLanguage": {
                "sProcessing": ''
            },
            "oColReorder" : oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "sPaginationType": "full_numbers",
            "bSort": false,
            "sAjaxSource": "/ajax/search",
            "fnServerData": function(sSource, aoData, fnCallback) {
                aoData.push({"name": "action", "value": "getWorkflowDataTable"});
                aoData.push({"name": "start", "value": "0"});
                aoData.push({"name": "limit", "value": jsonWorkflowData['limit']});
                aoData.push({"name": "search_value", "value": $.trim(jsonWorkflowData['search_value'])});
                aoData.push({"name": "column-sort", "value": jsonWorkflowData['column-sort']});
                aoData.push({"name": "column-sort-type", "value": jsonWorkflowData['column-sort-type']});
                ui.blockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function(data) {
                        // console.log(data)
                        // return;
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function(oSettings, json) {

                var obj = '.dataTable_workflow';
                var listOfappContainer = $('.fl-list-of-app-record-wrapper');
                dataTable_widget(obj, listOfappContainer);
                dataTableSetwidth(this.find('[field_name="date"]'), 130);

                // $(".tip").tooltip();
            },
            fnDrawCallback : function(){
                // $(".tip").tooltip();
                //set_dataTable_th_width([0]);
                setDatatableTooltip(".dataTable_widget");
                if (callback) {
                    callback(1);
                }
                ui.unblockPortion($('.fl-list-of-app-record-wrapper').find(".fl-main-app-view"))
            },
            iDisplayStart : 0,
            iDisplayLength : parseInt(jsonWorkflowData['limit']),
        });
        return oTable;
    }
}