var form_fields = "";
WorkflowTrigger = {
	updateTrigger: function(){
        //multi
        var self = this;
        $("body").on("change",".workflow-trigger-action-class",function(){
            var value = $(this).val();

            if(value=="Create"){
                $(this).closest(".trigger-container-fields").find(".forUpdateTrgger").addClass("display");
                $(this).closest(".trigger-container-fields").find(".forCreateTrgger").removeClass("display");
                $(this).closest(".trigger-container-fields").find(".fields_formula_row").css("display","block");
                $(this).closest(".trigger-container-fields").find(".forUpdateResponse").addClass("display");
                $(this).closest(".trigger-container-fields").find(".fields_formula").removeClass("display");
                $(this).closest(".trigger-container-fields").find(".post_submit_formula").addClass("display");
                $(this).closest(".trigger-container-fields").find(".formSelectionContainer").removeClass("display");
            }else if(value=="Update Response"){
                $(this).closest(".trigger-container-fields").find(".forUpdateTrgger").addClass("display");
                $(this).closest(".trigger-container-fields").find(".forCreateTrgger").removeClass("display");
                $(this).closest(".trigger-container-fields").find(".fields_formula_row").css("display","block");
                $(this).closest(".trigger-container-fields").find(".forUpdateResponse").removeClass("display");
                $(this).closest(".trigger-container-fields").find(".fields_formula").removeClass("display");
                $(this).closest(".trigger-container-fields").find(".post_submit_formula").addClass("display");
                $(this).closest(".trigger-container-fields").find(".formSelectionContainer").removeClass("display");
            }else if(value=="Post Submit"){
                $(this).closest(".trigger-container-fields").find(".forUpdateTrgger").addClass("display");
                $(this).closest(".trigger-container-fields").find(".forCreateTrgger").removeClass("display");
                $(this).closest(".trigger-container-fields").find(".fields_formula_row").css("display","block");
                $(this).closest(".trigger-container-fields").find(".forUpdateResponse").addClass("display");
                $(this).closest(".trigger-container-fields").find(".fields_formula").addClass("display");
                $(this).closest(".trigger-container-fields").find(".post_submit_formula").removeClass("display");
                $(this).closest(".trigger-container-fields").find(".formSelectionContainer").addClass("display");
                console.log("display",$(this).closest(".trigger-container-fields").find(".formSelectionContainer"))
            }else{
                $(this).closest(".trigger-container-fields").find(".forCreateTrgger").addClass("display");
                $(this).closest(".trigger-container-fields").find(".forUpdateTrgger").removeClass("display");
                $(this).closest(".trigger-container-fields").find(".forUpdateResponse").addClass("display");
                $(this).closest(".trigger-container-fields").find(".formSelectionContainer").removeClass("display");
            }
            self.removeCurrentFormSelection($(this).closest(".trigger-container-fields"));
        })
    },
    updateFieldReference: function(){
        // $("#workflow-form-trigger").change(function(){
        //     getFormFields($(this).val(),0);
        // })
        
        //multi
        $("body").on("change",".workflow-form-trigger",function(){
            getFormFields($(this),$(this).val(),0);
        })

        $("body").on("change",".workflow-db-connection",function(){
            getNewConnection($(this),$(this).val(),0,"");  
        })
    },
    setCheckedToJson : function(cont){
        var json = {};
        var departments = [];
        var positions = [];
        var users = [];
        var groups = [];
        //for deoartments
        $(cont).find(".departments").each(function(){
            if($(this).prop("checked")){
                departments.push($(this).val());
            }
        })
        //for positions
        $(cont).find(".positions").each(function(){
            if($(this).prop("checked")){
                positions.push($(this).val());
            }
        })
        //for users
        $(cont).find(".users").each(function(){
            if($(this).prop("checked")){
                users.push($(this).val());
            }
        })

        //for groups
        $(cont).find(".groups").each(function(){
            if($(this).prop("checked")){
                groups.push($(this).val());
            }
        })

        json = {
            "departments" : departments,
            "positions" : positions,
            "users" : users,
            "groups" : groups
        }
        return json;
    },
    trigger_html : function(ctr,data,prefix,node_type){
        prefix = if_undefinded(prefix,"");
        node_type = if_undefinded(node_type,"");
        var forms = $(".company_forms").text();
        forms = JSON.parse(forms);
        var pathname = window.location.pathname;
        if(pathname!="/user_view/rule_settings"){
            form_fields = $(".form_fields").val();
            form_fields = JSON.parse(form_fields);  
        }


        /*Setting up the data*/
        var chk_create = 'checked="checked"';
        var chk_update = "";
        var chk_updateResp = "";
        var chk_postSubmit = "";

        //hiding of fields
        var hd_create = "";
        var hd_update = "display";
        var hd_updateRes = "display";


        var fields_formula_container = "";
        var hd_postSubmit = "display";
        var fmula_postSubmit = "";
        var hideFormSelection = "";
        //create fields
        if(data!=""){
            //for hiding fields(create or update)
            if(data['workflow-trigger-action']=="Create"){
                hd_create = "";
                hd_update = "display";
                hd_updateRes = "display";
                hd_postSubmit = "display";
            }else if(data['workflow-trigger-action']=="Update Response"){
                hd_updateRes = "";
                hd_create = "";
                hd_update = "display";
                hd_postSubmit = "display";
            }else if(data['workflow-trigger-action']=="Post Submit"){
                hd_create = "";
                hd_update = "display";
                hd_updateRes = "display";
                hd_postSubmit = "";
                fields_formula_container = "display";
                fmula_postSubmit = if_undefinded(data['workflow-postSubmit-action'],"");
                hideFormSelection = "display";
            }else{
                hd_create = "display";
                hd_update = ""
                hd_updateRes = "display";
                hd_postSubmit = "display";
            }
            //for trigger action(create or update)
            chk_create = setChecked("Create", data['workflow-trigger-action']);
            chk_update = setChecked("Update", data['workflow-trigger-action']);
            chk_updateResp = setChecked("Update Response", data['workflow-trigger-action']);
            chk_postSubmit = setChecked("Post Submit", data['workflow-trigger-action']);

            //create fields

        }

        var dbConnectRet = "";
        var triggerlabel = "Form Trigger";
        //for database connection
        if(node_type=="workflow_chart-database"){
            var externalConnection = $("#wf-external-connections-list").text();
            var json_data = "";
            externalConnection = JSON.parse(externalConnection);

            dbConnectRet += '<div class="section clearing fl-field-style formSelectionContainer '+ hideFormSelection +'">';
                dbConnectRet += '<div class="column div_1_of_1" id="forDBConnection">';
                    dbConnectRet += '<span class="font-bold">Database Connection: <font color="red">*</font></span>';
                    dbConnectRet += '<div class="input_position_below">';
                        dbConnectRet += '<select class="form-select workflow-db-connection" workflow-db-connection">';
                            dbConnectRet += '<option value="0">--Select Form--</option>';
                                for (i in externalConnection) {
                                    // console.log(form_fields)
                                    dbConnectRet += '<option value="'+ externalConnection[i]['id'] +'" '+ setSelected(externalConnection[i]['id'], data['workflow-db-connection']) +'>'+ externalConnection[i]['connection_name'] +'</option>'; //setSelected(forms[i]['form_id'], data['workflow-form-trigger'])
                                }
                        dbConnectRet += '</select>';
                        dbConnectRet += '<img src="/images/loader/loading.gif" class="pull-left display replDisp processorLoad"/>';
                    dbConnectRet += '</div>';
                dbConnectRet += '</div>';
            dbConnectRet += '</div>';

            //remove form array
            forms = [];

            //replace trigger label
            triggerlabel = "Table Trigger";
        }


        var ret = '<div class="trigger-container-fields trigger-container-fields_'+ctr+'" trigger-index="'+ ctr +'" >';
            ret += '<div style="float: left;border: 1px solid #E2DCDC;height: auto;padding: 7px;width: 92%;margin-bottom:10px">';
                ret += '<div class="section clearing fl-field-style">';
                    ret += '<div class="column div_1_of_1">';
                        ret += '<span class="font-bold" style="font-size:12px !important;">Action: </span>';
                        ret += '<div class="input_position_below">';
                        ret += '<span style="font-size:11px; margin-right:3px;"><label><input type="radio" name="'+ prefix +'workflow-trigger-action_'+ ctr +'" value="Create" '+ chk_create +' class="workflow-trigger-action-class css-checkbox" id="'+ prefix +'1_workflow-trigger-action_'+ ctr +'"><label for="'+ prefix +'1_workflow-trigger-action_'+ ctr +'" class="css-label"></label> <span> Create Response</span></label> </span>';
                        ret += '<span style="font-size:11px; margin-right:3px;"><label><input type="radio" name="'+ prefix +'workflow-trigger-action_'+ ctr +'" value="Update Response" '+ chk_updateResp +' class="workflow-trigger-action-class css-checkbox" id="'+ prefix +'2_workflow-trigger-action_'+ ctr +'"><label for="'+ prefix +'2_workflow-trigger-action_'+ ctr +'" class="css-label"></label> <span> Update Response</span></label> </span>';
                        ret += '<div class="display" style="font-size:11px"><label><input type="radio" name="'+ prefix +'workflow-trigger-action_'+ ctr +'" value="Update" '+ chk_update +' class="workflow-trigger-action-class css-checkbox" id="'+ prefix +'3_workflow-trigger-action_'+ ctr +'"><label for="'+ prefix +'3_workflow-trigger-action_'+ ctr +'" class="css-label"></label> <span> Update Field</span></label> </div>';
                        // Lite
                        var c_lite = $("#go_lite").val();
                        if(c_lite == 0){
                            ret += '<span class="" style="font-size:11px; margin-right:3px;"><label><input type="radio" disabled="disabled" name="" value="Available in Full Version"  class="workflow-trigger-action-class css-checkbox" id=""><label for="" class="css-label"></label> <span> Post Submit</span></label></span>';
                        }else{
                           ret += '<span class="" style="font-size:11px; margin-right:3px;"><label><input type="radio" name="'+ prefix +'workflow-trigger-action_'+ ctr +'" value="Post Submit" '+ chk_postSubmit +' class="workflow-trigger-action-class css-checkbox" id="'+ prefix +'4_workflow-trigger-action_'+ ctr +'"><label for="'+ prefix +'4_workflow-trigger-action_'+ ctr +'" class="css-label"></label> <span> Post Submit</span></label></span>';
                        }
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';

                //FOR UPDATE TRIGGER
                ret += '<div class="forUpdateTrgger '+ hd_update +'">';
                    ret += '<div style="float:left;width:23%;">';
                        ret += '<div class="fields_below" style="width:98%;">';
                            ret += '<div class="label_below2">Field Filter: <font color="red">*</font></div>';
                            ret += '<div class="input_position_below">';
                                ret += '<select class="form-select workflow-trigger-fields-filter" id="" style="margin-top:0px">';
                                for (i in form_fields) {
                                    // console.log(form_fields)
                                    if(data!=""){
                                        ret += '<option '+ setSelected(form_fields[i], data['workflow-trigger-fields-filter']) +'>'+ form_fields[i] +'</option>';
                                    }else{
                                        ret += '<option>'+ form_fields[i] +'</option>';
                                    }
                                }
                                ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float:left;width:23%;">';
                        ret += '<div class="fields_below" style="width:98%;">';
                            ret += '<div class="label_below2">Field Update: <font color="red">*</font></div>';
                            ret += '<div class="input_position_below">';
                                ret += '<select class="form-select workflow-trigger-fields-update" id="" style="margin-top:0px">';
                                for (i in form_fields) {
                                    // console.log(form_fields)
                                    if(data!=""){
                                        ret += '<option '+ setSelected(form_fields[i], data['workflow-trigger-fields-update']) +'>'+ form_fields[i] +'</option>';
                                    }else{
                                        ret += '<option>'+ form_fields[i] +'</option>';
                                    }
                                }
                                ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    ret += '<div style="float:left;width:23%;">';
                        ret += '<div class="fields_below" style="width:98%;">';
                            ret += '<div class="label_below2">Operator: <font color="red">*</font></div>';
                            ret += '<div class="input_position_below">';
                                ret += '<select class="form-select workflow-trigger-operator" id="" style="margin-top:0px">';
                                    if(data!=""){
                                        ret += '<option value="Addition" '+ setSelected("Addition", data['workflow-trigger-operator']) +'>Addition</option>'
                                        ret += '<option value="Subtraction" '+ setSelected("Subtraction", data['workflow-trigger-operator']) +'>Subtraction</option>'
                                        ret += '<option value="Multiplication" '+ setSelected("Multiplication", data['workflow-trigger-operator']) +'>Multiplication</option>'
                                        ret += '<option value="Division" '+ setSelected("Division", data['workflow-trigger-operator']) +'>Division</option>'
                                    }else{
                                        ret += '<option value="Addition">Addition</option>'
                                        ret += '<option value="Subtraction">Subtraction</option>'
                                        ret += '<option value="Multiplication">Multiplication</option>'
                                        ret += '<option value="Division">Division</option>'
                                    }
                                ret += '</select>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';
                //FOR CREATE TRIGGER
                ret += '<div class="forCreateTrgger '+ hd_create +'">';

                    ret += dbConnectRet;

                    ret += '<div class="section clearing fl-field-style formSelectionContainer '+ hideFormSelection +'">';
                        ret += '<div class="column div_1_of_1" id="forFormTrigger">';
                            ret += '<span class="font-bold">'+ triggerlabel +': <font color="red">*</font></span>';
                            ret += '<div class="input_position_below">';
                                ret += '<select class="form-select workflow-form-trigger" id="workflow-form-trigger" node-type="'+ node_type +'">';
                                    ret += '<option value="0">--Select Form--</option>';
                                for (i in forms) {
                                    // console.log(form_fields)
                                    if(data!=""){
                                        ret += '<option value="'+ forms[i]['form_id'] +'" '+ setSelected(forms[i]['form_id'], data['workflow-form-trigger']) +'>'+ forms[i]['form_name'] +'</option>';
                                    }else{
                                        ret += '<option value="'+ forms[i]['form_id'] +'">'+ forms[i]['form_name'] +'</option>';
                                    }
                                }
                                ret += '</select>';
                                ret += '<img src="/images/loader/loading.gif" class="pull-left display replDisp processorLoad"/>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';
                    
                    //for child reference
                    ret += '<div class="section clearing fl-field-style">';
                        ret += '<div class=" column div_1_of_2 fields_below forUpdateResponse '+ hd_updateRes +'" id="" >';
                            ret += '<span class="font-bold">Parent Reference Field: <font color="red">*</font></span>';
                            ret += '<div class="input_position_below">';
                                ret += '<select class="form-select workflow-ref-field-parent" id="workflow-ref-field-parent" style="margin-top:0px">';
                                    ret += '<option value="0">--Select Field--</option>';
                                ret += '</select>';
                                ret += '<img src="/images/loader/loading.gif" class="pull-left display replDisp processorLoad"/>';
                            ret += '</div>';
                        ret += '</div>';
                        //for child reference
                        ret += '<div class=" column div_1_of_2 fields_below forUpdateResponse '+ hd_updateRes +'" id="">';
                            ret += '<span class="font-bold">Child Reference Field: <font color="red">*</font></span>';
                            ret += '<div class="input_position_below">';
                                ret += '<select class="form-select workflow-ref-field" id="workflow-ref-field" style="margin-top:0px">';
                                    for (i in form_fields) {
                                        // console.log(form_fields)
                                        if(data!=""){
                                            ret += '<option '+ setSelected(form_fields[i], data['workflow-ref-field']) +'>'+ form_fields[i] +'</option>';
                                        }else{
                                            ret += '<option>'+ form_fields[i] +'</option>';
                                        }
                                    }
                                ret += '</select>';
                                ret += '<img src="/images/loader/loading.gif" class="pull-left display replDisp processorLoad"/>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';

                    
                    
                    

                    //for create and update response
                    ret += '<div class="section clearing fl-field-style fields_formula '+ fields_formula_container +'">';
                    if(data!=""){
                        var field_formula = data['workflow-field_formula'];
                        if(field_formula!=undefined){
                            for(var i in field_formula){
                                ret +=  this.fields_formula(ctr,field_formula[i],i);
                            }
                        }else{
                            ret +=  this.fields_formula(ctr,"",0);
                        }
                        
                    }else{
                        ret +=  this.fields_formula(ctr,"",0);
                    }
                    ret += '</div>';
                    
                    //for post submit
                    ret += '<div class="post_submit_formula '+ hd_postSubmit +'">';
                        ret += '<div class="section clearing fl-field-style">';
                            ret += '<div class=" column div_1_of_1 fields_below" id="forReferenceField">';
                                ret += '<span class="font-bold">Formula: </span>';
                                ret += '<div class="input_position_below">';
                                    ret += '<textarea class="form-textarea workflow-postSubmit-action" data-ide-properties-type="ide-workflow-postSubmit-action" style="margin-top:0px;resize:none">'+ fmula_postSubmit +'</textarea>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';


                ret += '</div>';
            ret += '</div>';    
            ret += '<div style="float:left;font-size: 15px;position: relative;">';
                ret += '<div style="float:left;width:100%;margin-left:5px;">';
                    ret += '<i class="addtrigger icon-plus-sign fa fa-plus cursor"></i>';
                    if(ctr!=0){
                        ret += '<i class="minusTrigger icon-minus-sign fa fa-minus cursor" style="margin-left:5px;"></i>';
                    }
                ret += '</div>';  
            ret += '</div>';    
        ret += '</div>';   
        return ret;
    },
    fields_formula : function(ctr,data,row_ctr){
        var fmula = ""
        if(data!=""){
            fmula = data['workflow-field-formula'];
        }
        var ret = '<div class="fields_formula_row column div_1_of_1" style="display:none;margin:0px;"><div style="float:left;width: 90%; background-color:#ffffff; border-radius:3px; border: 1px solid #E2DEDE;padding:5px;">';
                        ret += '<div class="fields_below" id="forReferenceField">';
                            ret += '<span class="font-bold">Field: <font color="red">*</font></span>';
                            ret += '<div class="input_position_below">';
                                ret += '<select class="form-select workflow-reference-fields-update" id="workflow-reference-fields-update_'+ctr+'_'+row_ctr+'" style="margin-top:0px">';
                                ret += '<option value="0">--Select Field--</option>';
                                ret += '</select>';
                                ret += '<img src="/images/loader/loading.gif" class="pull-left display replDisp processorLoad"/>';
                            ret += '</div>';
                        ret += '</div>';
                        ret += '<div>';
                            ret += '<div class="fields_below" id="forReferenceField">';
                                ret += '<span class="font-bold" style="margin-top:3px;">Formula: </span>';
                                ret += '<div class="input_position_below">';
                                    ret += '<textarea class="form-textarea workflow-reference-fields-formula" data-ide-properties-type="ide-workflow-reference-fields-formula" style="margin-top:0px;resize:none">'+ fmula +'</textarea>';
                                ret += '</div>';
                            ret += '</div>';
                        ret += '</div>';
                    ret += '</div>';    
                    ret += '<div style="float:left; font-size: 12px;position: relative;">';
                        ret += '<div style="float:left;width:100%;margin-left:5px;">';
                            ret += '<i class="addFieldtrigger icon-plus-sign fa fa-plus cursor"></i>';
                            if(row_ctr!=0){
                                ret += '<i class="minusFieldTrigger icon-minus-sign fa fa-minus cursor" style="margin-left:5px;"></i>';
                            }
                        ret += '</div>';  
                    ret += '</div></div>';
        return ret;
    },
    field_trigger_custom : function(){
        var self = this;
        var ctr_triggerhtml = 0;
        var collect_indexes = [];

        var ctr_fields_formula = 0;

        $("body").on("click",".addtrigger",function(){
            var node_type = if_undefinded($(this).closest(".trigger-container-fields").find(".workflow-form-trigger").attr("node-type"),"");
            var prefix =  $(this).closest(".external_db_triggers").attr("trigger_type")
            $(".trigger-container-fields").each(function(){
                collect_indexes.push(parseInt($(this).attr("trigger-index")))
            })
            ctr_triggerhtml = collect_indexes.sort(function(a,b){return b-a});
            // alert(ctr_triggerhtml)
            ctr_triggerhtml = ctr_triggerhtml[0]+1;
            // ctr_triggerhtml = $(".trigger-container-fields").length;
            var pathname = window.location.pathname;
            if(pathname=="/user_view/rule_settings"){
                form_fields = $(".middlewareFormFields").val().split(",");
                form_fields.unshift("TrackNo")
            }


            var htmlStr = self.trigger_html(ctr_triggerhtml++,"",prefix,node_type);
            
            $(this).closest(".trigger-container-fields").after(htmlStr)
            $(this).closest(".trigger-container-fields").next().find(".fields_formula_row").last().fadeIn();
            if(pathname=="/user_view/rule_settings"){
            	$(".workflow-trigger-action-class[value='Post Submit']").closest("div").remove();
            }
            // $(".fields_formula_row").show();
            $(".content-dialog-scroll").perfectScrollbar("update");
            // self.updateTrigger();
            // self.updateFieldReference();
            // $(".workflow-reference-fields-formula").mention_post({
            //     json : formulaJson, // from local file fomula.json
            //     highlightClass:"",
            //     prefix : "",
            //     countDisplayChoice : "4"
            // });
            self.removeCurrentFormSelection($(".trigger-container-fields"));
        })
        $("body").on("click",".minusTrigger",function(){
            $(this).closest(".trigger-container-fields").remove()
            $(".content-dialog-scroll").perfectScrollbar("update")
        })
        $("body").on("click",".addFieldtrigger",function(){
            var this_ele = this;
            var index_fieldTrigger = $(this).closest(".trigger-container-fields").attr("trigger-index");
            ctr_fields_formula = $(this).closest(".trigger-container-fields").find(".workflow-reference-fields-update").length;
            var htmlStr = self.fields_formula(index_fieldTrigger,"",ctr_fields_formula++);
            $(this).closest(".fields_formula").append(htmlStr);
            $(this).closest(".fields_formula").find(".fields_formula_row").last().fadeIn();
            $(".content-dialog-scroll").perfectScrollbar("update");
            //  $(".workflow-form-trigger").change(function(){
                
            // })
            var workflow_form_trigger = $(this).closest(".trigger-container-fields").find(".workflow-form-trigger");
            getFormFields(workflow_form_trigger,workflow_form_trigger.val(),"null",function(){

                TriggerUniqueFields.init($(this_ele));

                // $(".workflow-reference-fields-formula").mention_post({
                //     json : formulaJson, // from local file fomula.json
                //     highlightClass:"",
                //     prefix : "",
                //     countDisplayChoice : "4"
                // });
            });
        })
        $("body").on("click",".minusFieldTrigger",function(){
            var this_ele = this;
            $(this).closest(".fields_formula_row").fadeOut(function(){
                $(this_ele).closest(".fields_formula_row").find(".workflow-reference-fields-update").val("0").trigger("change");
                // $(this).remove()
                
            });
            $(".content-dialog-scroll").perfectScrollbar("update")
        })
    },
    field_trigger_settings : function(node_data_id){
        var json = $("body").data();
        var json_nodes_var = json['nodes'];
        ui.block()
        var newDialog = new jDialog(object_properties_node("workflow_chart-fields-trigger",node_data_id,"field-trigger"), "","700", "", "20", function(){});
        newDialog.themeDialog("modal2");
        $(".content-dialog-scroll").perfectScrollbar();
        // $(".workflow-reference-fields-formula").mention_post({
        //     json : formulaJson, // from local file fomula.json
        //     highlightClass:"",
        //     prefix : "",
        //     countDisplayChoice : "4"
        // });
        var workflow_trigger = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-trigger'],"");
        var workflow_trigger2 = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-trigger'],"");

        if(workflow_trigger){
            // var k = triggerHtml(workflow_trigger);
            // $(".content-dialog").html(k)
            // for(var i in workflow_trigger){
            //     getFormFields($("#workflow-form-trigger_"+i),workflow_trigger[i]['workflow-form-trigger'],workflow_trigger[i]['workflow-reference-fields-update']);
            // }   
            var ret = "";
            var index = "";
            for(var i in workflow_trigger){
                index = i;
                if(workflow_trigger[i]['trigger-index']){
                    index = workflow_trigger[i]['trigger-index'];
                }
                ret += this.trigger_html(index,workflow_trigger[i]);
            }
            // $(".content-dialog").html(ret); // ETO UNG PROBLEM 
            var temp = $(ret);
            $(".content-dialog").html(temp);
            temp.find('.fields_formula').append(this.fields_formula(++index,"",0));
            ui.unblock()
            var field_formula = "";
            var frm_trigger = "";
            var workflow_reference_fields_update = "";
            for(var i in workflow_trigger){
                frm_trigger = $(".trigger-container-fields_"+[i]).find(".workflow-form-trigger");
                if(workflow_trigger[i]['trigger-index']){
                    frm_trigger = $(".trigger-container-fields_"+workflow_trigger[i]['trigger-index']).find(".workflow-form-trigger");
                }



                getFormFields(frm_trigger,frm_trigger.val(),"",function(){
                    if(i==workflow_trigger.length-1){ //perform at the last loop
                        $(".fields_formula_row").show();
                        for(var l in workflow_trigger2){
                            field_formula = workflow_trigger2[l]['workflow-field_formula'];
                            for(var kk in field_formula){
                                // console.log(field_formula[kk])
                                /*
                                    Updated Aaron Tolentino 02/20/2015
                                    // to fix the issue on deletion of trigger field
                                */
                                workflow_reference_fields_update = $(".trigger-container-fields_"+workflow_trigger2[l]['trigger-index']).find(".workflow-reference-fields-update").eq(kk);
                                if(field_formula[kk]['workflow-field-update']!=0){ 
                                    workflow_reference_fields_update.val(field_formula[kk]['workflow-field-update']);
                                }else{
                                    //hide if deleted // problem in indexing
                                    // $("#"+field_formula[kk]['workflow-field-id']).closest(".fields_formula_row").css("display","none")
                                    workflow_reference_fields_update.closest(".fields_formula_row").css("display","none");
                                }
                            }
                            if(workflow_trigger2[l]['workflow-ref-field-parent']){
                                $(".trigger-container-fields_"+workflow_trigger2[l]['trigger-index']).find(".workflow-ref-field-parent").val(htmlEntities(workflow_trigger2[l]['workflow-ref-field-parent']));
                            }else{
                                $(".trigger-container-fields_"+workflow_trigger2[l]['trigger-index']).find(".workflow-ref-field-parent").val("TrackNo");
                            }
                        }
                        TriggerUniqueFields.init(frm_trigger);
                        ui.unblock()
                    }
                });

                // $("#"+field_formula[j]['workflow-field-id']).val(field_formula[j]['workflow-field-update'])
            }
            $(".content-dialog-scroll").perfectScrollbar("update");

        }else{
            $(".fields_formula_row").show();
            ui.unblock()
        }



        //call function for saving
        workflow_actions.save_node_settings()
        // this.updateTrigger();
        // this.updateFieldReference();
        this.removeCurrentFormSelection($(".trigger-container-fields"));
        
    },
    external_db_data : function(node_data_id){
        var json = $("body").data();
        var json_nodes_var = json['nodes'];
        var workflow_trigger_ext = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-external-trigger'],"");
        var workflow_trigger2_ext = if_undefinded(json_nodes_var[''+ node_data_id +'']['workflow-external-trigger'],"");

        if(workflow_trigger_ext){
            var ret = "";
            var index = 0;var trigger_type = "";
            for(var i in workflow_trigger_ext){
                trigger_type = workflow_trigger_ext[i];
                ret = "";
                index = 0;
                for(var j in trigger_type){
                    index = i;
                    if(trigger_type[j]['trigger-index']){
                        index = trigger_type[j]['trigger-index'];
                    }
                    ret += this.trigger_html(index,trigger_type[j],i,"workflow_chart-database");
                }
                if(i=="upload-ext"){
                    $("#dbtabs-2").html(ret)
                }else if(i=="import-ext"){
                    $("#dbtabs-3").html(ret)
                }
            }

            var ctr_workflow_trigger_ext = 1;
            var form_trigger_value = 0;
            var frm_trigger = "";
            for(var i in workflow_trigger_ext){
                // ctr_workflow_trigger_ext++;
                trigger_type = workflow_trigger_ext[i];
                
                for(var j in trigger_type){
                    // console.log(i)
                    frm_trigger = $("[trigger_type='"+ i +"'] .trigger-container-fields_"+[j]).find(".workflow-form-trigger");
                    frm_connection = $("[trigger_type='"+ i +"'] .trigger-container-fields_"+[j]).find(".workflow-db-connection");
                    if(trigger_type[j]['trigger-index']){
                        frm_trigger = $("[trigger_type='"+ i +"'] .trigger-container-fields_"+trigger_type[j]['trigger-index']).find(".workflow-form-trigger");
                        frm_connection = $("[trigger_type='"+ i +"'] .trigger-container-fields_"+trigger_type[j]['trigger-index']).find(".workflow-db-connection");
                    }

                    // console.log(frm_trigger)
                    getNewConnection(frm_connection,frm_connection.val(),"",
                        {
                            form_trigger_value:trigger_type[j]['workflow-form-trigger'],
                            frm_trigger:frm_trigger
                        },
                        function(data){
                            form_trigger_value = data['form_trigger_value'];//trigger_type[j]['workflow-form-trigger'];
                            frm_trigger = data['frm_trigger'];
                            frm_trigger.val(form_trigger_value);
                            // console.log(frm_trigger)
                            // console.log(form_trigger_value)
                            // ctr_workflow_trigger_ext++;
                            
                            getFormFields(frm_trigger,form_trigger_value,"",function(){
                                    //loop ulet
                                if(j==trigger_type.length-1){ //perform at the last loop //static 2
                                    for(var h in workflow_trigger2_ext){
                                        // console.log(h)
                                        trigger_type2 = workflow_trigger2_ext[h];

                                        $("[trigger_type='"+ h +"'] .fields_formula_row").show();
                                        for(var l in trigger_type2){
                                            field_formula = trigger_type2[l]['workflow-field_formula'];
                                            for(var kk in field_formula){
                                                // console.log(field_formula[kk])
                                                /*
                                                    Updated Aaron Tolentino 02/20/2015
                                                    // to fix the issue on deletion of trigger field
                                                */
                                                workflow_reference_fields_update = $("[trigger_type='"+ h +"'] .trigger-container-fields_"+trigger_type2[l]['trigger-index']).find(".workflow-reference-fields-update").eq(kk);
                                                if(field_formula[kk]['workflow-field-update']!=0){ 
                                                    workflow_reference_fields_update.val(field_formula[kk]['workflow-field-update']);
                                                }else{
                                                    //hide if deleted // problem in indexing
                                                    // $("#"+field_formula[kk]['workflow-field-id']).closest(".fields_formula_row").css("display","none")
                                                    workflow_reference_fields_update.closest(".fields_formula_row").css("display","none");
                                                }
                                            }
                                            if(trigger_type2[l]['workflow-ref-field-parent']){
                                                $("[trigger_type='"+ h +"'] .trigger-container-fields_"+trigger_type2[l]['trigger-index']).find(".workflow-ref-field-parent").val(htmlEntities(trigger_type2[l]['workflow-ref-field-parent']));
                                            }else{
                                                $("[trigger_type='"+ h +"'] .trigger-container-fields_"+trigger_type2[l]['trigger-index']).find(".workflow-ref-field-parent").val("TrackNo");
                                            }
                                        }
                                        TriggerUniqueFields.init(frm_trigger);
                                        // ui.unblock()
                                    }
                                }
                        });
                    })
                    // console.log(frm_trigger);
                    
                }
            }
        }else{
            $(".fields_formula_row").show();
        }
    },
    removeCurrentFormSelection : function(){
        var current_form_id = $("#form_id").val();
        $(".trigger-container-fields").each(function(){
            var form_trigger = $(this).find(".workflow-trigger-action-class:checked").val();
            console.log(form_trigger);
            if(form_trigger=="Create"/* || form_trigger=="Update Response"*/){
                $(this).find(".workflow-form-trigger option[value='"+ current_form_id +"']").css("display","none");
                $(this).find(".workflow-form-trigger option[value='"+ current_form_id +"']").attr("disabled",true);
            }else{
                $(this).find(".workflow-form-trigger option[value='"+ current_form_id +"']").css("display","");
                $(this).find(".workflow-form-trigger option[value='"+ current_form_id +"']").attr("disabled",false);
            }
        })
    },
    
    
    "change_referenceFieldsUpdate" : function(){
        $("body").on("change",".workflow-reference-fields-update",function(){
            TriggerUniqueFields.init($(this))
        })
    },
}

function getFormFields(ele,form_id,selectedVal,callback){
    var action = "getFormFields";
    var extDB_data = {};
    if(ele.attr("node-type")=="workflow_chart-database"){
        //for external table columns
        action = "getFieldsInExternalConnection";
        extDB_data = {
            id:ele.closest(".trigger-container-fields").find(".workflow-db-connection").val(),

        }
    }
    var pathname = window.location.pathname;
    if(pathname=="/user_view/formbuilder"){
        action = "getFieldsInExternalConnection";
        extDB_data = {
            id:ele.closest(".trigger-container-fields").find(".external_database_connection").val(),

        }
    }
    if(form_id==0){
        ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").html("<option value='0'>--Select Field--</option>");
        ele.closest(".trigger-container-fields").find(".workflow-ref-field-parent").html("<option value='0'>--Select Field--</option>");
        return;
    }
    if(selectedVal=="null"){
        ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").last().next().removeClass("display");
        ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").last().addClass("display")
    }else{
        ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").next().removeClass("display");
        ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").addClass("display")
    }
    $.post("/ajax/workflow",{action:action,form_id:form_id,extDB_data:extDB_data},function(data){
        try{
            data = JSON.parse(data)
            var ret = "<option value='0'>--Select Field--</option>";
            var referenceFields = ret;
                referenceFields += "<option value='TrackNo'>TrackNo</option>";
            for(var i in data){
                ret += "<option value='"+ data[i] +"' "+ setSelected(data[i], selectedVal) +">"+ data[i] +"</option>";
                referenceFields += "<option value='"+ data[i] +"' "+ setSelected(data[i], selectedVal) +">"+ data[i] +"</option>";
            }
            if(selectedVal=="null"){
                ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").last().html(ret);
                ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").last().next().addClass("display");
                ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").last().removeClass("display")
            }else{
                ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").html(ret);
                ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").next().addClass("display");
                ele.closest(".trigger-container-fields").find(".workflow-reference-fields-update").removeClass("display")

                //for parent reference field
                ele.closest(".trigger-container-fields").find(".workflow-ref-field-parent").html(referenceFields);
            }
            if(typeof callback=="function"){
                callback(1);
            }
        }catch(err){
            console.log(data);
            console.log(err);
        }
    })
}

function getNewConnection(ele,connection_id,selectedVal,otherData,callback){

    if(selectedVal=="null"){
        ele.closest(".trigger-container-fields").find(".workflow-form-trigger").last().next().removeClass("display");
        ele.closest(".trigger-container-fields").find(".workflow-form-trigger").last().next().addClass("display");
    }else{
        ele.closest(".trigger-container-fields").find(".workflow-form-trigger").next().removeClass("display");
        ele.closest(".trigger-container-fields").find(".workflow-form-trigger").addClass("display")
    }

    
    // $.post("/ajax/workflow",{
    //     action:"getTablesInExternalConnection",
    //     id:connection_id
    // },function(data){
    $.ajax({
        type: 'POST',
        url: '/ajax/workflow',
        data: {
            action:"getTablesInExternalConnection",
            id:connection_id
        },
        // async : false,
        success: function (data) {

            if($.trim(data)==""){
                
                showNotification({
                    message: "Connection Failed",
                    type: "error",
                    autoClose: true,
                    duration: 3
                })

                var ret = "<option value='0'>--Select Field--</option>";
                if(selectedVal=="null"){
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").last().html(ret);
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").last().next().addClass("display");
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").last().removeClass("display")
                }else{
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").html(ret);
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").next().addClass("display");
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").removeClass("display")

                    //for parent reference field
                    ele.closest(".trigger-container-fields").find(".workflow-ref-field-parent").html(ret);
                }
                return false;
            }

            // alert(data)
            try{
                data = JSON.parse(data)
                var ret = "<option value='0'>--Select Field--</option>";
                for(var i in data){
                    ret += "<option value='"+ data[i]['TABLE_NAME'] +"' "+ setSelected(data[i]['TABLE_NAME'], selectedVal) +">"+ data[i]['TABLE_NAME'] +"</option>";
                }

                if(selectedVal=="null"){
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").last().html(ret);
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").last().next().addClass("display");
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").last().removeClass("display")
                }else{
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").html(ret);
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").next().addClass("display");
                    ele.closest(".trigger-container-fields").find(".workflow-form-trigger").removeClass("display")

                    //for parent reference field
                    ele.closest(".trigger-container-fields").find(".workflow-ref-field-parent").html(ret);
                }

                ele.closest(".trigger-container-fields").find(".workflow-form-trigger").trigger("change");
                if(typeof callback=="function"){
                    callback(otherData);
                }
            }catch(err){
                console.log(data)
                console.log(err)
            }
        }
    })
}