//from formbuilder.js  default_accordion_settings
var default_accordion_settings = {
    collapsible: false,
    header: ">div.form-accordion-section-group > h3.form-accordion-section-header",
    heightStyle: "fill",
    // animate: "easeInCubic",
    activate: function (event, ui) {

        $(this).find('.form-accordion').accordion('refresh');
    },
    // create: function (event, ui) { // this is the sample that will not close accordion panels
    // 	setTimeout(function(){
    // 		$(event.target).find('h3.form-accordion-section-header')
    // 		    .off('click')
    // 		.click(function(){
    // 		    $(this).next().toggle('fast');
    // 		});
    // 	},1)
    // }
}
function accordionDialog(doid,actionType,cb){
	var action = "";
	var header_value=""
	var visibility_formula = "";
	var dbd = $('body').data(doid);
	if($.type(dbd)=="undefined"){
		$('body').data(doid,{});
		dbd = $('body').data(doid);
		dbd['section_ctr'] = 2;
		
	}else if( $.type(dbd['section_ctr']) == "undefined" ){
		dbd['section_ctr'] = 2;
	}
	var dialog_title = "Add Section";
	if(actionType=="Add"){
		 action="Add";
	}
	else if(actionType == "Edit"){
		action = "Save";
		dialog_title = "Edit Section";
		header_value = dbd['header_name'];
		visibility_formula = dbd['visibility_formula'];
		visibility_formula = $.trim(visibility_formula);
	}

	var ret = 	'<div class="accordion-actions-container">'+
					'<h3 class="fl-margin-bottom"> '+dialog_title+'</h3><div class="hr"></div>'+
						'<div class="addAccordionDialog" title="Accordion Data">'+
							'<form>'+
								'<div class="fields">'+
									'<div class="fields">'+
										'<div class="label_below2"> Title: </div>'+
										'<div class="input_position">'+
											'<input type="text"  class="accordion-title-value form-text" placeholder="Title" value="'+header_value+'"/>'+
											
										'</div>'+
										'<div class="properties_width_container chk_Visibility">'+
										    '<div class="fields_below">'+
												'<div class="label_below2">'+"Visibility by formula: "+'  <font color="red"></font></div>'+
												'<div class="input_position_below chk_Visibility_below">'+
											    	'<label style="display:none;"><input type="radio" value="yes" class="visibility-choices css-checkbox" name="visibility-choices" id="visibility-choices" /><label for="visibility-choices" class="css-label"></label> Yes </label>'+
											    	'<label style="display:none;"><input type="radio" value="no" class="visibility-choices css-checkbox" name="visibility-choices" id="visibility-choices2" /><label for="visibility-choices2" class="css-label"></label> No </label>'+
											    	'<label style="display:none;"><input type="radio" checked="checked" value="computed" class="visibility-choices css-checkbox" name="visibility-choices" id="visibility-choices3" /><label for="visibility-choices3" class="css-label"></label> Computed </label>'+
											    	'<textarea class="form-textarea visibility-formula accordion-visibility-formula" data-properties-type="visibility-formula" data-ide-properties-type="ide-visibility-formula">' + visibility_formula + '</textarea>'+
												'</div>'+
										    '</div>'+
										'</div>'  +
									'</div>'+
										
								'</div>'+
							'</form>'+
							'<div class="fields"><div class="label_basic"></div><div class="input_position" style="margin-top:5px;text-align:right;"><input type="button" class="btn-blueBtn fl-margin-right action-accordion-header fl-positive-btn" value="'+action+'"/></div></div>'+
						'</div>'+
				'</div>' 
	var accordion_add_dialog = new jDialog($(ret).html(), "", "400", "", "95", function() {

		});

	accordion_add_dialog.themeDialog("modal2");
	$('.accordion-visibility-formula').val(visibility_formula);	
	$('#popup_container').find('.action-accordion-header').on('click',function(){
		
		if($.type(cb)=='function'){
			var header_name = $('.accordion-title-value').val();
			var visibility_formula = $('.visibility-formula').val();
			if($.trim(header_name) == ""){
				
				header_name = "Section "+dbd['section_ctr'];	
			}

			cb(header_name, visibility_formula);
			$('.fl-closeDialog').trigger('click');
		}
	});

    return ret;

}
   
function accordionAction(ele){
	var add = $(ele).find('.form-accordion-action-icon-add:eq(0)');
	var edit = $(ele).find('.form-accordion-action-icon-edit:eq(0)');
	var del = $(ele).find('.form-accordion-action-icon-delete:eq(0)');
	var doid = 	ele.closest('.setObject').attr('data-object-id');
	var dbd = $('body').data(doid);
	if($.type(dbd)=="undefined"){
		$('body').data(doid,{});
		dbd = $('body').data(doid);
		dbd['section_ctr'] = 2;
		
	}
	console.log('droppme',ele, ele.find('.accordion-div-content:eq(0)'));
	accordion_droppable(ele.find('.accordion-div-content:eq(0)'));
	$(add).on('click',function(e){

		e.stopPropagation();
		
		var accordion_group_ele = $(this).parents('.form-accordion-section-group:eq(0)');
		var headerHeight = $(this).closest('.form-accordion-section-header').outerHeight();
		var setObjHeight = accordion_group_ele.closest('.setObject').outerHeight()+1;
		var clone_accordion_group = accordion_group_ele.clone();
		clone_accordion_group.find('.form-accordion-section-header:eq(0)').removeClass('ui-state-active').removeClass('ui-state-hover');
		clone_accordion_group.find('.accordion-div-content').empty();
		doid = accordion_group_ele.closest('.setObject').attr('data-object-id');
		dbd = $('body').data(doid);
		accordionDialog(doid,"Add",function(val, visibility_formula){
			
			clone_accordion_group.attr('visible-formula', visibility_formula);
			clone_accordion_group.attr('field-visible', 'computed');
			accordion_group_ele.after(clone_accordion_group);
			clone_accordion_group.attr('accordion-name','accordion_' + ($(".form-accordion-section-group").index(clone_accordion_group) + 1));
			clone_accordion_group.find('.form-accordion-section-header').find('.accordion-header-name').text(val);
			accordion_group_ele.closest('.setObject').css('min-height',setObjHeight + headerHeight);
			accordion_group_ele.closest('.form-accordion').accordion('refresh');
			accordionAction(clone_accordion_group);
			accordion_group_ele.closest('.form-accordion').find('.form-accordion-section-group').each(function(){
				$(this).find('.cursor-disable').removeClass('cursor-disable');
		
			});
			dbd['section_ctr'] = Number(dbd['section_ctr']||1)+1;
			//accordion_group_ele.closest('.setObject').trigger("resize");
			
		});

		
	});
	$(edit).on('click',function(e){
		e.stopPropagation();
		var element_to_edit = $(this).parents('.form-accordion-section-group:eq(0)');
		doid = element_to_edit.closest('.setObject').attr('data-object-id');
		dbd = $('body').data(doid);
		
		
		
		dbd['header_name'] = element_to_edit.find('.form-accordion-section-header').find('.accordion-header-name').text();
		dbd['visibility_formula'] = element_to_edit.attr('visible-formula');
		element_to_edit.attr('field-visible', 'computed');
		accordionDialog(doid,"Edit",function(val, visibility_formula){
			element_to_edit.attr('accordion-name','accordion_' + ($(".form-accordion-section-group").index(element_to_edit) + 1));
			element_to_edit.find('.form-accordion-section-header').find('.accordion-header-name').text(val);
			element_to_edit.attr('visible-formula', visibility_formula);
		});
	});
	$(del).on('click',function(e){
		e.stopPropagation();
		var element_to_delete = $(this).parents('.form-accordion-section-group:eq(0)');
		

		if($(this).is('.cursor-disable')){
			return false;
		}
		var conf = "Are you sure you want to delete this section?";
		var newConfirm = new jConfirm(conf, 'Confirmation Dialog','', '', '', function(r){
			if(r==true){
				var form_accordion_ele = element_to_delete.closest('.form-accordion');
				var accordionSetObj = form_accordion_ele.closest('.setObject');


				console.log(element_to_delete.find('.setObject').attr("data-object-id"))
				console.log($(this))
				$(element_to_delete).find('.setObject').each(function(){
						var lay_number = $(this).attr("data-object-id");
						$('#layer_'+lay_number).remove();							
						})
				element_to_delete.remove();
				
		  		var number_of_group = form_accordion_ele.find('.form-accordion-section-group').length;
		  		if(number_of_group==1){

					form_accordion_ele.find('.form-accordion-action-icon-delete').addClass('cursor-disable');
				}
				accordionSetObj.css('min-height',accordionSetObj.outerHeight()-23);
				form_accordion_ele.accordion('refresh');
			}
		});
		newConfirm.themeConfirm("confirm2", {
       		 'icon':'<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>' 
  		});


  		
		
	});
}
function accordion_droppable(thisEle){

	$(thisEle).droppable({
		greedy: true,
		drop: function(event,ui){
        var target = event.target;
        var source = ui.draggable[0].parentElement;
        
        if(target === source){ //conditional purpose to prevent dropping off the same place
            //alert('Same droppable container');

    	}else{

    		
    		
    		var pos = ui.draggable.offset();
    		var dpos = $(this).offset();
    		console.log("posdpos",pos,dpos);
    		if((pos.top - dpos.top)<11){
    			pos.top = 10;
    			dpos.top = 0;
    		}
    		if((pos.left-dpos.left)<29){
    			pos.left = 28;
    			dpos.left = 0;
    		}
			$(ui['helper']).css({
				"top": pos.top - dpos.top,
				"left":pos.left - dpos.left
			});

			$('.component-ancillary-focus:not(".component-primary-focus")').map(function(){
				var self = $(this);
				var pos = self.offset();
				self.css({
					"top": pos.top - dpos.top,
					"left":pos.left - dpos.left
				});
				return this;
			}).appendTo($(this));
			ui['helper'].appendTo($(this));
    	}
					
		},
	});
}
function accordion_resizable(thisEle){
	var originalHeight = 0;
	var addedHeight = 0;
	var originalAccordionHeight = 0;

	$(thisEle).resizable({
		
		containment: 'parent',
		handles: 's,e',
		start:function(event,ui){
			if($('body').css("cursor") == "s-resize"){
				$(this).addClass("custom-south-resizing");
			}
			if($('body').css("cursor") == "e-resize"){
				$(this).addClass("custom-east-resizing");
			}
			save_history();
			// originalHeight = $(thisEle).height();
			// originalAccordionHeight = $(this).find('.form-accordion:eq(0)').find('.ui-accordion-content[aria-expanded="true"]').height();
		},
		resize: function(event, ui) {
			// var thisEleHeight = $(this).height();
			// var thisEle_label_height = $(thisEle).find('#label_'+$(thisEle).attr("data-object-id")).outerHeight()+2;
			// var headerCount = $(this).find('.form-accordion-section-header').length;
			// var headerHeight = $(this).find('.form-accordion-section-header').eq(0).outerHeight();
			// var accordionContentMinHeight = $(this).find('.accordion-div-content').css('min-height');
			// accordionContentMinHeight = Number(accordionContentMinHeight.replace('px',''));
			// var accordionMinHeight = headerCount*headerHeight + 27 +thisEle_label_height +accordionContentMinHeight;
			// console.log('minHeightAccordion',thisEle_label_height,headerHeight,accordionMinHeight)
			// $(this).css('min-height',accordionMinHeight+"px");

			// console.log('heheheheiught',thisEleHeight,originalHeight,originalAccordionHeight)
			// var newAccordionHeight = thisEleHeight - originalHeight + originalAccordionHeight;
			// //$(".object_properties").popover('hide');
			// var thisEle_label_height = $(thisEle).find('#label_'+$(thisEle).attr("data-object-id")).outerHeight()+2;
			// //$(this).find('.form-accordion:eq(0)').accordion("refresh");
			// // ui.size.width = ui.size.width - 10;
			// // ui.size.height = ui.size.height - 10;
			// $(this).find('.fields_below').css('height',thisEleHeight);
 		// 	$(this).find('.fields_below').find('.input_position_below').css('height',thisEleHeight - 25 +'px');
			
				// $(this).css({
				// 	"height":($(this).find('.form-accordion:eq(0)').outerHeight()+thisEle_label_height)+"px"
				// });

 				// console.log('newHeight',newAccordionHeight);
 				//$(this).find('.form-accordion:eq(0)').find('.ui-accordion-content[aria-expanded="true"]').css('height',newAccordionHeight);
			// $(this).find('.form-accordion:eq(0)').accordion('refresh');
			
			if($('body').css("cursor") == "s-resize"){
				$(this).find('.form-accordion:eq(0)').accordion('refresh');
				

			}
			// $(this).eq(0).resizable("option", "minHeight", $(this).css('height'));
		},
		stop: function (ee, ui) {
			var self = $(this);
	        // $(container).resizable("option", "minWidth", checkFormMinWidth());
	        // $(container).resizable("option", "minHeight", checkFormMinHeight());
	        self.parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight(self))
	        self.parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth(self))
	        self.parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight(self)) + "px")
	        self.parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth(self)) + "px")
	        if(self.hasClass("custom-south-resizing")){
	        	self.removeClass("custom-south-resizing");
				temp2 = self.find('.form-accordion').eq(0)
	        	temp3 = self;
	        	var test = ( ($(temp3).offset().top + $(temp3).height()) - ($(temp2).offset().top + $(temp2).height()) );
	        	if(test <= 0){
	        		temp3.css("height",( temp3.height() + 1 + (test * -1) )+"px");
	        	}
	        }
        	if(self.hasClass("custom-east-resizing")){
        		self.removeClass("custom-east-resizing");
        	}
	    }
		// stop: function(ee, ui) {
		// 	// var thisEleHeight = $(this).height();
		// 	// console.log('heheheheiught',thisEleHeight,originalHeight,originalAccordionHeight)
		// 	// var newAccordionHeight = thisEleHeight - originalHeight + originalAccordionHeight;
		// 	// var thisEle_label_height = $(thisEle).find('#label_'+$(thisEle).attr("data-object-id")).outerHeight()+2;
		// 	// // $(container).resizable("option", "minWidth", checkFormMinWidth());
		// 	// // $(container).resizable("option", "minHeight", checkFormMinHeight());
		// 	// $(this).parents(".ui-resizable").eq(0).resizable("option", "minHeight", checkParentResizableMinHeight($(this)))
			
		// 	// $(this).parents(".ui-resizable").eq(0).resizable("option", "minWidth", checkParentResizableMinWidth($(this)))
		// 	// $(this).parents(".ui-resizable").eq(0).css("min-height", (checkParentResizableMinHeight($(this))) + "px")
		// 	// $(this).parents(".ui-resizable").eq(0).css("min-width", (checkParentResizableMinWidth($(this))) + "px")
		// 	// if($(this).data('ui-resizable').axis == 's'){

		// 	// 	// $(this).css({
		// 	// 	// 	"height":($(this).find('.form-accordion:eq(0)').outerHeight()+thisEle_label_height)+"px"
		// 	// 	// });
 	// 	// 		//$(this).find('.form-accordion:eq(0)').find('.ui-accordion-content[aria-expanded="true"]').css('height',newAccordionHeight);
		// 	// }
		// 	// $(this).find('.form-accordion').eq(0).accordion("refresh");
			
		// }
	
	});
}