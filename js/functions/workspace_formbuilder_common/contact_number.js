var contact_number_functionality = {
	"init":function(){
		this.events();
	},
	"events":function(elements){
		if(elements){
			elements = $(elements);
		}
		(elements||$('.setOBJ[data-type="contactNumber"],.setObject[data-type="contactNumber"]')).each(function(){
			var self = $(this);
			var doi = self.attr('data-object-id');
			var get_fields = self.find('.getFields_'+doi).eq(0);
			var temp_fields_container = self.find('.contact-number-temp-field-container:eq(0)');
			var temp_fields = get_fields.parent().parent().children('.contact-number-temp-field-container').find('.contact-number-temp-field');

			temp_fields_container.children('.ps-scrollbar-x-rail,.ps-scrollbar-y-rail').remove();
			temp_fields_container.perfectScrollbar();

			ApplyNumbersOnly(temp_fields);
			temp_fields.on('input.contactField',function(e){
				var temp_fields = get_fields.parent().parent().children('.contact-number-temp-field-container').find('.contact-number-temp-field');
				get_fields.val( temp_fields.map(function(){ return $(this).val(); }).get().join(',') );
			});
			get_fields.data('validate.contactField',function(){
				var self = get_fields;
				var temp_fields = self.parent().parent().children('.contact-number-temp-field-container').find('.contact-number-temp-field');
				var error_counts = 0;
				var temp_message = '';
				var error_fields = [];
				temp_fields.each(function(){
					var self = $(this);
					var regexp_cell_no = new RegExp('^0[0-9]{10}$',""); //^(\\+[1-9]{2}[0-9]{10}|0[0-9]{10})(?=$|\\s|,)		//(^\s*?|,\s*?)(\+[1-9]{2}[0-9]{10}|0[0-9]{10})(?=$|\s|,) //COMENT PANG LAHATAN NA REGEXP
					var cell_no_value = self.val()||'';
					if(!regexp_cell_no.test(cell_no_value)){
						error_counts++;
						temp_message += 'Not valid cell number: \"'+cell_no_value+'\"<br/>';
						error_fields.push({
							"element":self,
							"error_message":'Not valid cell number: \"'+cell_no_value+'\"<br/>'
						});
					}else{
						return true;
					}
				});
				return {
					'message':temp_message,
					'errors':error_counts,
					'error_fields':error_fields
				};
			});

    		MakePlusPlusRowV2({
		        "tableElement":temp_fields_container,
		        "rules":[
		            {
		                "eleMinus":">span>div>span>span.minus-cell-contact",
		                "eleMinusTarget":"span:eq(1)",
		                "eleMinusTargetFn":function(e,ui){
		                	if($(this).children('input').is('[disabled]')){
		                		return false;
		                	}
		                	if(ui['parent_ele'].children('span').length == 1){
		                		ui['next_ele'].add(ui['prev_ele']).find('>div>span>span.minus-cell-contact').hide();
		                	}
		                	ui['next_ele'].add(ui['prev_ele']).children('input').eq(0).trigger('input.contactField');
		                	ui['parent_ele'].perfectScrollbar("update");
		                },
		                "eleMinusTargetFnBefore":function(e,ui){
		                	if($(this).children('input').is('[disabled]')){
		                		return false;
		                	}
		                },
		                "eleMinusTargetFnAfter":"",

		                "elePlus":">span>div>span>span.add-cell-contact",
		                "elePlusTarget":"span:eq(1)",
		                "elePlusTargetFn":function(e,ui){
		                	if(ui['parent_ele'].children('span').length >= 2){
		                		ui['parent_ele'].children('span').find('>div>span>span.minus-cell-contact').show();
		                	}
		                	if(ui['parameter'].length >= 1){
		                		$(this).children('input').val(ui['parameter'][0]);
		                	}
		                	$(this).children('input').trigger('input.contactField');
		                	ui['parent_ele'].perfectScrollbar("update");
		                },
		                "elePlusTargetFnBefore":function(e,ui){
		                	if($(this).children('input').is('[disabled]')){
		                		return false;
		                	}
		                },
		                "elePlusTargetFnAfter":""
		            }
		        ]
		    });

			get_fields.data('CustomDomObserveData',{
				'html':get_fields[0].outerHTML||"",
				'REAttr':new RegExp('[0-9A-Za-z_\\-]{1,}\\s*?=\\s*?\\"[0-9A-Za-z_\\-\\s\\:\\;]*\\"|[0-9A-Za-z_\\-]{1,}','g'),
				'targetObserve':{
					'attr':['readonly','placeholder','disabled','style'],
					'data':[],
					'tag':false
				}
			});

			get_fields.data('CustomDomObserve',function(){
				var inner_self = $(this);
				var temp_fields = inner_self.parent().parent().children('.contact-number-temp-field-container').find('.contact-number-temp-field');
				setTimeout(function(){
					if( inner_self[0].outerHTML != inner_self.data('CustomDomObserveData')['html'] ){
						var temp_old = inner_self.data('CustomDomObserveData')['html'].match(inner_self.data('CustomDomObserveData')['REAttr']);
						var temp_new = (inner_self[0].outerHTML||"").match(inner_self.data('CustomDomObserveData')['REAttr']);
						if(temp_new.length >= temp_old.length){
							for(var ctr in temp_new){
								if(temp_old.indexOf(temp_new[ctr]) <= -1 && (new RegExp(inner_self.data('CustomDomObserveData')['targetObserve']['attr'].join('|'),'g')).test(temp_new[ctr]) ){
									temp_fields.attr(temp_new[ctr].split('=')[0],(temp_new[ctr].split('=')[1]||'').replace(/\"/g,''));
								}else continue;
							}
						}else{
							for(var ctr in temp_old){
								if(temp_new.indexOf(temp_old[ctr]) <= -1 && (new RegExp(inner_self.data('CustomDomObserveData')['targetObserve']['attr'].join('|'),'g')).test(temp_old[ctr]) ){
									temp_fields.removeAttr((temp_old[ctr]||"").split('=')[0]);
								}else continue;
							}
						}
						inner_self.data('CustomDomObserveData')['html'] = inner_self[0].outerHTML;
					}
					inner_self.data('CustomDomObserve').call(inner_self);
				},800);
			});
			get_fields.data('CustomDomObserve').call(get_fields);
		});
	},
	"validate":function(setobject_elements){

	}
};

contact_number_functionality.init();