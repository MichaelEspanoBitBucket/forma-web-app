

( function( $ ){
	$.fn.dynamicTable = function(options){
		var settings = $.extend({
			tableBorder:"0",
			tableCellSpacing:"0",
			tableCellWidth:100,
			tableCellHeight:30,
			tableColumn:1,
			tableCellStyle:{},
			tableRow:1,
			tHeadTag:true,
			tBodyTag:true,
			tFootTag:false,
			tCaptionTag:false,
			tCaptionTitle:"Table",
			tCaptionTitleEditable:true,
			tHeadBGColor:"white",
			tHeadFontColor:"black",
			tHeadFontSize:"black",
			tBodyAlterColor:true,
			tBodyAlterColor1:"rgb(200,200,255)",
			tBodyAlterColor2:"rgb(200,200,200)",
			tRowAdding:true,
			tColumnAdding:true,
			tRowRemoving:true,
			tColumnRemoving:true,
			tRepeater:false,
			resizableCell:true,
			droppableCell:true,
			tCellEvent:true,
			tCellEventOn:{
				"click":function(ev){
					var evtarget = (window.event) ? window.event.srcElement /* for IE */ : ev.target;
					var dis_ele_table = $(this).parents('table.form-table');
					if(dis_ele_table.data('highlight_merge_data_row_col_index')){

					}else{
						dis_ele_table.data('highlight_merge_data_row_col_index',{});
					}

					var dis_ele_table_data_rci = dis_ele_table.data('highlight_merge_data_row_col_index');
					var dis_row_index = $(this).parents('tr').eq(0).index();
					var dis_col_index = $(this).index();

					var highlighted_cell = dis_ele_table.children('tbody').children('tr').children('td.dynamic-td-droppable-selected')
					

					if(highlighted_cell.length == 0){
						dis_ele_table.data('highlight_merge_data_row_col_index',{});
					}

					var data_cell_highlight = dis_ele_table.data('highlight_merge_data_row_col_index');

					if($(evtarget).hasClass("td-relative")){
						if(ev.ctrlKey){
							if($(this).hasClass("dynamic-td-droppable-selected")){
								$(this).removeClass("dynamic-td-droppable-selected");
								ev.stopPropagation();

								if($.isArray(data_cell_highlight[""+dis_row_index])){
									delete data_cell_highlight[""+dis_row_index][dis_col_index];
								}else{

								}

							}else{
								//$(".table td").removeClass("dynamicTDDroppable")
								$(this).addClass("dynamic-td-droppable-selected");

								if($.isArray(data_cell_highlight[""+dis_row_index])){
									data_cell_highlight[""+dis_row_index].push(dis_col_index);
								}else{
									data_cell_highlight[""+dis_row_index] = [dis_col_index];
								}
								ev.stopPropagation();
							}
						}else{
							if($(this).hasClass("dynamic-td-droppable-selected")){
								$(this).removeClass("dynamic-td-droppable-selected");
								if($.isArray(data_cell_highlight[""+dis_row_index])){
									delete data_cell_highlight[""+dis_row_index][dis_col_index];
								}else{

								}
								ev.stopPropagation();
							}else{
								$(".dynamic-td-droppable-selected").removeClass("dynamic-td-droppable-selected");
								$(this).addClass("dynamic-td-droppable-selected");

								data_cell_highlight = {};
								data_cell_highlight[""+dis_row_index] = [];
								// console.log(data_cell_highlight)
								if($.isArray(data_cell_highlight[""+dis_row_index])){
									data_cell_highlight[""+dis_row_index].push(dis_col_index);
								}else{
									data_cell_highlight[""+dis_row_index] = [dis_col_index];
								}
								ev.stopPropagation();
							}
						}
					}

					var highlight_data = mergeHighlightIsValidV2();
					if(highlight_data == true){
						if(highlight_data['mergeType'] == "merge"){
							$('.relative-grid-merge').show();
							$('.relative-grid-unmerge').hide();
							$('.relative-grid-merge').data("highlight_data",highlight_data);
						}else if(highlight_data['mergeType'] == "unmerge"){
							$('.relative-grid-unmerge').show();
							$('.relative-grid-merge').hide();
						}
					}else{
						$('.relative-grid-merge').hide();
						$('.relative-grid-unmerge').hide();
						$('.relative-grid-merge').removeData('highlight_data');
					}
				}
			},
			cellDroppable:{
				revert:true,
				greedy:true,
				// drop:function(e,ui){ //I comment for reference
				// 	//evtarget = (window.event) ? window.event.srcElement /* for IE */ : e.target;
			 //        //console.log(e.target)
			 //        //console.log(ui.draggable[0].parentElement);
			 //        if($(ui.helper).find("table[element-type='control']").length == 0){
			 //        	var target = e.target;
				//         var source = ui.draggable[0].parentElement;
				//         if(target === source){ //conditional purpose to prevent dropping off the same place
				//             //alert('Same droppable container');
				//     	}else{
				//     		if($(this).children(".td-relative").eq(0).length == 0){
				//     			$(ui.helper).appendTo(this);
				// 				$(ui.helper).css({
				// 					"top":"0px",
				// 					"left":"0px"
				// 				});
				//     		}else{
				//     			$(ui.helper).appendTo($(this).children(".td-relative").eq(0));
				// 				$(ui.helper).css({
				// 					"top":"0px",
				// 					"left":"0px"
				// 				});
				//     		}
				    		
				//     	}
			 //        }
				// }
			},
			cellResizable:{
				minWidth:65,
				minHeight:26,
				handles:"s,e",
				// alsoResize:".resizing-also-resize",
				start:function(e, ui){
					save_history();
					$(this).children(".td-relative").data("cell_resizable_resized",true);

					var dis_table_ele = $(this).parents('.setObject').eq(0);

					var original_min_width = $(this).resizable("option","minWidth");
					var original_min_height = $(this).resizable("option","minHeight");
					$(this).resizable("option","original_min_width",original_min_width);
					$(this).resizable("option","original_min_height",original_min_height);

					var collecting_min_width_resize = [];
					var collecting_min_height_resize = [];
					var cursor_axis_movement = $('body').css("cursor");
					$('.resizing-also-resize').each(function(){
						try{
							if(cursor_axis_movement == "e-resize"){
								collecting_min_width_resize.push($(this).resizable("option","minWidth"));
							}else if(cursor_axis_movement == "s-resize"){
								collecting_min_height_resize.push($(this).resizable("option","minHeight"));
							}
						}catch(e){
							//
						}
					});
					
					if(cursor_axis_movement == "e-resize"){
						if(collecting_min_width_resize.length >= 1){
							collecting_min_width_resize = collecting_min_width_resize.sort(function(a,b){
								return b-a;
							});
							$(this).resizable("option","minWidth", collecting_min_width_resize[0]);
						}
						$('.resizing-also-resize').not(this).css({
							"width":(ui.size.width)+"px"
						});
						if(dis_table_ele.parent().is('.ui-tabs-panel.ui-droppable') && $('.table-form-design-relative').length >= 1 ){
							var direction = ui['size']['width'] - ui['originalSize']['width'];
							dis_table_ele
							.parents('.setObject[data-type="tab-panel"]')
							.eq(0)
							.data('dis_tab_original_width',dis_table_ele
							.parents('.setObject[data-type="tab-panel"]')
							.eq(0).outerWidth());
							dis_table_ele.data('diff_for_table_width',dis_table_ele.outerWidth() - ui['originalSize']['width']);
						}
					}else if(cursor_axis_movement == "s-resize"){
						if(collecting_min_height_resize.length >= 1){
							collecting_min_height_resize = collecting_min_height_resize.sort(function(a,b){
								return b-a;
							});
							$(this).resizable("option","minHeight",collecting_min_height_resize[0])
						}
						if($(this).is(':not([rowspan])')){
							$('.resizing-also-resize').not(this).filter('[rowspan]').each(function(){
								var dis_added_height_data = $(this).outerHeight()-ui.size.height;
								$(this).data('dis_added_height_data',dis_added_height_data);
								$(this).css({
									"height":(ui.size.height+dis_added_height_data)+"px"
								});
								$(this).children('.td-relative').css({
									"min-height":(ui.size.height+dis_added_height_data)+"px"
								});
							});
						}else if($(this).is('[rowspan]')){
							$('.resizing-also-resize').not(this).each(function(){
								if($(this).is('[rowspan]')){
									var dis_added_height_data = ui.size.height - $(this).outerHeight();
								}else{
									var dis_added_height_data = ui.size.height - $(this).closest('tr').outerHeight();
								}
								
								
								$(this).data('dis_added_height_data',dis_added_height_data);
								$(this).css({
									"height":(ui.size.height-dis_added_height_data)+"px"
								});
								$(this).children('.td-relative').css({
									"min-height":(ui.size.height-dis_added_height_data)+"px"
								});
							});
						}else{
							$('.resizing-also-resize').not(this).css({
								"height":(ui.size.height)+"px"
							});
						}
						
					}
					$(this).data('cursor_axis',cursor_axis_movement);


				},
				resize:function(e,ui){

					var cursor_axis_movement = $('body').css("cursor");
					var dis_table_ele = $(this).parents('.setObject').eq(0);

					// console.log("BADAdsa",dis_table_ele.parents('.setObject[data-type="tab-panel"]').eq(0))
					if(cursor_axis_movement == "e-resize"){
						$(this).children(".td-relative").eq(0).css("min-width",(ui.size.width)+"px");
						$(".resizing-also-resize").children(".td-relative").css("min-width",(ui.size.width)+"px");
						// $('.resizing-also-resize').filter('[colspan]').each(function(){
						// 	var colspan_data = Number($(this).attr('colspan'));
						// 	$(this).children(".td-relative").eq(0).css("min-width",(ui.size.width * colspan_data)+"px");
						// });
						$('.resizing-also-resize').not(this).css({
							"width":(ui.size.width)+"px"
						});


						if(dis_table_ele.parent().is('.ui-tabs-panel.ui-droppable') && $('.table-form-design-relative').length >= 1 ){
							
							var dis_tab_setobj = dis_table_ele
							.parents('.setObject.ui-resizable[data-type="tab-panel"]')
							.eq(0);

							var dis_diff_table_tab = dis_table_ele.data('diff_for_table_width');
							


							dis_tab_setobj.css({
								"width":(ui['size']['width']+dis_diff_table_tab+6)+"px"
							});

							if( hasScrollLeft(dis_tab_setobj.find('.ui-tabs-nav').eq(0)) ){

								dis_tab_setobj.find('.form-tabbable-pane').eq(0).attr('has-scroll',true);
							}else{
								dis_tab_setobj.find('.form-tabbable-pane').eq(0).attr('has-scroll',false);
							}
							
							// var direction = ui['size']['width'] - ui['originalSize']['width'];
							
						}

					}else if(cursor_axis_movement == "s-resize"){
						$(this).children(".td-relative").eq(0).css("min-height",(ui.size.height)+"px");
						$(".resizing-also-resize").children(".td-relative").css("min-height",(ui.size.height)+"px");
						// $('.resizing-also-resize').filter('[rowspan]').each(function(){
						// 	var colspan_data = Number($(this).attr('rowspan'));
						// 	$(this).children(".td-relative").eq(0).css("min-height",(ui.size.height * colspan_data)+"px");
						// });
						if($(this).is(':not([rowspan])')){
							$('.resizing-also-resize').not(this).filter('[rowspan]').each(function(){
								var dis_added_height_data = $(this).data('dis_added_height_data');
								$(this).css({
									"height":(ui.size.height+dis_added_height_data)+"px"
								});
								$(this).children('.td-relative').css({
									"min-height":(ui.size.height+dis_added_height_data)+"px"
								});
							});
							$('.resizing-also-resize').not(this).filter(':not([rowspan])').each(function(){
								$(this).css({
									"height":(ui.size.height)+"px"
								});
							});
						}else if($(this).is('[rowspan]')){
							$('.resizing-also-resize').not(this).each(function(){
								var dis_added_height_data = $(this).data('dis_added_height_data');
								$(this).css({
									"height":(ui.size.height-dis_added_height_data)+"px"
								});
								$(this).children('.td-relative').css({
									"min-height":(ui.size.height-dis_added_height_data)+"px"
								});
							});
						}else{
							$('.resizing-also-resize').not(this).css({
								"height":(ui.size.height)+"px"
							});
						}
					}

				},
				stop:function(e, ui){

					// console.log("TESTING TABLE",e,ui)
					// console.log('$(".resizing-also-resize")',$(".resizing-also-resize"));
					$(this).resizable("option","minWidth",$(this).resizable("option","original_min_width"));
					$(this).resizable("option","minHeight",$(this).resizable("option","original_min_height"));
					

					$(this).children(".td-relative").eq(0).css("min-width",(ui.size.width)+"px");
					$(this).children(".td-relative").eq(0).css("min-height",(ui.size.height)+"px");

					var dis_table_ele = $(this).parents('.setObject').eq(0);
					var dis_table_ele_right = dis_table_ele.position().left + dis_table_ele.outerWidth();
					var dis_table_ele_bottom = dis_table_ele.position().top + dis_table_ele.outerHeight();

					//alert('test')
					if(dis_table_ele.parent().hasClass('formbuilder_ws')){
						//alert(324)
						var parent_td_right = dis_table_ele.parent().outerWidth();
						var parent_td_bottom = dis_table_ele.parent().outerHeight();
					}else{
						var parent_td_right = dis_table_ele.parents('td').eq(0).outerWidth();
						var parent_td_bottom = dis_table_ele.parents('td').eq(0).outerHeight();
					}

					var cursor_axis_movement = $(this).data("cursor_axis");

					if(cursor_axis_movement == "e-resize"){

						$(".resizing-also-resize").children(".td-relative").css("min-width",(ui.size.width)+"px");
						if(dis_table_ele.parent().hasClass('fcp-td-relative') ||dis_table_ele.parent().hasClass('formbuilder_ws')){
							var most_elem_right = dis_table_ele.parent().children('.setObject').get().map(function(elem,index){
								return $(elem).position().left + $(elem).outerWidth();
							}).concat(dis_table_ele_right).sort(function(a,b){
								return b-a;
							});
							if(most_elem_right.length >= 1){

								if(most_elem_right[0] >= parent_td_right){
									if(dis_table_ele.parent().hasClass('formbuilder_ws')){
										
										// alert(most_elem_right[0])
										// dis_table_ele.parent().css('width',(most_elem_right[0])+'px');
										$(window).resize();//trigger change
										// dis_table_ele.parent().css('width','auto');
									}else{
										dis_table_ele.parents('td').eq(0).css('width',(most_elem_right[0])+'px');
										dis_table_ele.parents('td').eq(0).children('.td-relative').eq(0).css('min-width',(most_elem_right[0])+'px');
									}
								}
							}
						}else if(dis_table_ele.parent().is('.ui-tabs-panel.ui-droppable') && $('.table-form-design-relative').length >= 1 ){
							
							var dis_tab_setobj = dis_table_ele
							.parents('.setObject.ui-resizable[data-type="tab-panel"]')
							.eq(0);

							if(dis_tab_setobj.parent().is('.td-relative.ui-droppable')){
								var direction = (dis_tab_setobj.position().left + dis_tab_setobj.outerWidth()) -  dis_tab_setobj.parents('td.ui-resizable').eq(0).outerWidth();
								if( dis_tab_setobj.parents('td.ui-resizable').eq(0).outerWidth() < (dis_tab_setobj.position().left + dis_tab_setobj.outerWidth()) ){
									
									
									console.log(direction,"mamaw",dis_tab_setobj.parents('td.ui-resizable').eq(0))
									// dis_tab_setobj.parents('td.ui-resizable').eq(0).children('.td-relative').children('.ui-resizable-handle').appendTo(dis_tab_setobj.parents('td.ui-resizable').eq(0));
									setTimeout(function(){
										dis_tab_setobj.parents('td.ui-resizable').eq(0).resizable('resizeBy',{
											"width":direction
										});
									},500);
										
									// dis_tab_setobj.parents('td.ui-resizable').eq(0).children('.ui-resizable').appendTo(dis_tab_setobj.parents('td.ui-resizable').eq(0).children('.td-relative'))
								}
							}


							//
							// console.log("LSAD",dis_table_ele.parent().children('.setObject').get());
							// var most_elem_right = dis_table_ele.parent().children('.setObject').get().map(function(elem,index){
							// 	return $(elem).position().left + $(elem).outerWidth();
							// }).concat(dis_table_ele_right).sort(function(a,b){
							// 	return b-a;
							// });
							// console.log("SDASDA",most_elem_right)
							// if(most_elem_right.length >= 1){
								// var direction = ui['size']['width'] - ui['originalSize']['width'];
								// dis_table_ele.parents('.setObject[data-type="tab-panel"]').eq(0).resizable('resizeBy',{
								// 	"width":direction
								// });
								
							// }


							// var distance_width = dis_table_ele.parent().outerWidth() - dis_table_ele.outerWidth();
							// // alert(distance_width)
							// if(	distance_width <= 3){
							// 	dis_table_ele.parents('.setObject[data-type="tab-panel"]').eq(0).css({
							// 		"width":""
							// 	});
							// 	dis_table_ele.parents('.setObject[data-type="tab-panel"]').eq(0).resizable('resizeBy',{
									
							// 	});
							// 	distance_width = dis_table_ele.parent().outerWidth() - dis_table_ele.outerWidth();
							// 	if(distance_width >= 4){
							// 		dis_table_ele.parents('.setObject[data-type="tab-panel"]').eq(0).resizable('resizeBy',{
							// 			"width":-(distance_width)
							// 		});
							// 	}
							// }else{
							// 	// dis_table_ele.parents('.setObject[data-type="tab-panel"]').eq(0).css({
							// 	// 	"width":dis_table_ele.outerWidth()+7
							// 	// });
							// 	dis_table_ele.parents('.setObject[data-type="tab-panel"]').eq(0).resizable('resizeBy',{
							// 		"width":-(distance_width)
							// 	});
							// }
						}


						// $('.resizing-also-resize').filter('[colspan]').each(function(){
						// 	var colspan_data = Number($(this).attr('colspan'));
						// 	$(this).children(".td-relative").eq(0).css("min-width",(ui.size.width * colspan_data)+"px");
						// });
						$('.resizing-also-resize').not(this).css({
							"width":(ui.size.width)+"px"
						});
					}else if(cursor_axis_movement == "s-resize"){

						$(".resizing-also-resize").children(".td-relative").css("min-height",(ui.size.height)+"px");
						if(dis_table_ele.parent().hasClass('fcp-td-relative') || dis_table_ele.parent().hasClass('formbuilder_ws')){
							// console.log("dis_table_ele.parent().children('.setObject').get()",dis_table_ele.parent().children('.setObject').get()	)
							var most_elem_bottom = dis_table_ele.parent().children('.setObject').get().map(function(elem,index){
								return $(elem).position().top + $(elem).outerHeight();
							}).concat(dis_table_ele_bottom).sort(function(a,b){
								return b-a;
							});
							if(most_elem_bottom.length >= 1){
								// console.log("AMP",most_elem_bottom[0] , parent_td_bottom);
								if(most_elem_bottom[0] >= parent_td_bottom){
									if(dis_table_ele.parent().hasClass('formbuilder_ws')){
										//dis_table_ele.parent().css('height',(most_elem_bottom[0])+'px');
										$(window).resize();//trigger change
										//dis_table_ele.parent().css('height','auto');
									}else{
										dis_table_ele.parents('td').eq(0).css('height',(most_elem_bottom[0])+'px');
										dis_table_ele.parents('td').eq(0).children('.td-relative').eq(0).css('min-height',(most_elem_bottom[0])+'px');
									}
									
								}
							}
						}else if(dis_table_ele.parent().is('.ui-tabs-panel.ui-droppable') && $('.table-form-design-relative').length >= 1 ){
							var dis_tab_setobj = dis_table_ele
							.parents('.setObject.ui-resizable[data-type="tab-panel"]')
							.eq(0);

							if(dis_tab_setobj.parent().is('.td-relative.ui-droppable')){
								var direction = (dis_tab_setobj.position().top + dis_tab_setobj.outerHeight()) -  dis_tab_setobj.parents('td.ui-resizable').eq(0).outerHeight();
								console.log("TEST CONDITION",
									dis_tab_setobj.parents('td.ui-resizable').eq(0).outerHeight() ,"<", (dis_tab_setobj.position().top + dis_tab_setobj.outerHeight())
								)
								if( dis_tab_setobj.parents('td.ui-resizable').eq(0).outerHeight() < (dis_tab_setobj.position().top + dis_tab_setobj.outerHeight()) ){
									
									
									// console.log(direction,"mamaw",dis_tab_setobj.parents('td.ui-resizable').eq(0))
									// dis_tab_setobj.parents('td.ui-resizable').eq(0).children('.td-relative').children('.ui-resizable-handle').appendTo(dis_tab_setobj.parents('td.ui-resizable').eq(0));
									setTimeout(function(){
										dis_tab_setobj.parents('td.ui-resizable').eq(0).resizable('resizeBy',{
											"height":direction
										});
									},500)
										
									// dis_tab_setobj.parents('td.ui-resizable').eq(0).children('.ui-resizable').appendTo(dis_tab_setobj.parents('td.ui-resizable').eq(0).children('.td-relative'))
								}
							}
						}

						// $('.resizing-also-resize').filter('[rowspan]').each(function(){
						// 	var colspan_data = Number($(this).attr('rowspan'));
						// 	$(this).children(".td-relative").eq(0).css("min-height",(ui.size.height * colspan_data)+"px");
						// });
						if($(this).is(':not([rowspan])')){
							$('.resizing-also-resize').not(this).filter('[rowspan]').each(function(){
								var dis_added_height_data = $(this).data('dis_added_height_data');
								$(this).css({
									"height":(ui.size.height+dis_added_height_data)+"px"
								});
								$(this).children('.td-relative').css({
									"min-height":(ui.size.height+dis_added_height_data)+"px"
								});
							});
						}else if($(this).is('[rowspan]')){
							$('.resizing-also-resize').not(this).each(function(){
								var dis_added_height_data = $(this).data('dis_added_height_data');
								$(this).css({
									"height":(ui.size.height-dis_added_height_data)+"px"
								});
								$(this).children('.td-relative').css({
									"min-height":(ui.size.height-dis_added_height_data)+"px"
								});
							});
						}else{
							$('.resizing-also-resize').not(this).css({
								"height":(ui.size.height)+"px"
							});
						}
					}

					$(".resizing-also-resize").removeClass("resizing-also-resize");

					$(this).children(".td-relative").removeData("cell_resizable_resized");
					$(this).removeData("cursor_axis");
					var collision = dis_table_ele.testCollision();
					
					if(collision["final_collision_bot"] || collision["final_collision_right"] || collision["final_collision_top"]){
						var newTdWidth = $(this).outerWidth() - collision["final_collide_overflow_right"];
						var newTdHeight = $(this).outerHeight() - collision["final_collide_overflow_bot"];
						var tdIndex = $(this).index();
						var trIndex = $(this).closest('tr').index();
						$(this).closest('tbody').children('tr').each(function(){
							$(this).children().eq(tdIndex).css('width',newTdWidth);
							$(this).children().eq(tdIndex).children().css('min-width',newTdWidth);
							if($(this).index()==trIndex){
								$(this).children().css('height',newTdHeight);
								$(this).children().children().css('min-height',newTdHeight);
							}
						});
					}
					setFormbuilderMinSize();
				}
				
			}
		},options);

		return this.each(function(){
			//applying settings
			$(this).addClass("thisDynamicTable");
			//ui display settings
			if(settings.tCaptionTag){
				if($(this).find(">caption").length == 1){
					$(this).find(">caption").css({
						display:""
					});
				}
			}else{
				if($(this).find(">caption").length == 1){
					$(this).find(">caption").css({
						display:"none"
					});
				}
			}


			if(settings.tHeadTag){
				if($(this).find(">thead").length == 1){
					$(this).find(">thead").css({
						display:""
					});
				}
			}else{
				if($(this).find(">thead").length == 1){
					$(this).find(">thead").css({
						display:"none"
					});
				}
			}


			if(settings.tFootTag){
				if($(this).find(">tfoot").length == 1){
					$(this).find(">tfoot").css({
						display:""
					});
				}
			}else{
				if($(this).find(">tfoot").length == 1){
					$(this).find(">tfoot").css({
						display:"none"
					});
				}
			}

			//check element tags

			if(settings.tCaptionTag && $(this).find(">caption").length == 0 ){
				$(this).prepend("<caption><div contenteditable='true' class='component-label caption'>"+settings.tCaptionTitle+"</div></caption>");
			}else if(settings.tCaptionTag && $(this).find(">caption").length == 1){
				$(this).find(">caption").html("<div contenteditable='true'>"+settings.tCaptionTitle+"</div>");
			}

			if(settings.tHeadTag && $(this).find(">thead").length == 0 ){
				if($(this).find(">caption").length == 1){
					$(this).find(">caption").after("<thead></thead>");
				}else if($(this).find(">caption").length == 0){
					$(this).prepend("<thead></thead>");
				}
			}

			if(settings.tBodyTag && $(this).find(">tbody").length == 0 ){
				if($(this).find(">thead").length == 1){
					$(this).find(">thead").after("<tbody></tbody>");
				}else if($(this).find(">caption").length == 1){
					$(this).find(">caption").after("<tbody></tbody>");
				}else if($(this).find(">thead").length == 0){
					$(this).prepend("<tbody></tbody>");
				}
			}

			if(settings.tBodyTag && $(this).find(">tfoot").length == 0 ){
				if($(this).find(">tbody").length == 1){
					$(this).find(">tbody").after("<tfoot></tfoot>");
				}else if($(this).find(">thead").length == 1){
					$(this).find(">thead").after("<tfoot></tfoot>");
				}else if($(this).find(">caption").length == 1){
					$(this).find(">caption").after("<tfoot></tfoot>");
				}else if($(this).find(">tbody").length == 0){
					$(this).prepend("<tfoot></tfoot>");
				}
			}

			//setting rows and columns, width and height of cells //DITO MAY PROBLEMA SA ROW 
			if(settings.tableRow && settings.tableColumn){
				if(settings.tHeadTag && $(this).find(">thead").length == 1){
					if($(this).find(">thead").find(">tr").length == 0){
						$(this).find(">thead").append("<tr></tr>");
					}
					columns = settings.tableColumn - $(this).find(">thead").find(">tr:first>th").length;
					for(ctr_col = 1 ; ctr_col <= columns ; ctr_col++){
						$(this).find(">thead>tr:first").append("<th style='width:"+settings.tableCellWidth+"px;height:"+settings.tableCellHeight+"px;'><div class='th-relative' style='width:100%;height:100%;position:relative;'></div></th>");
					}
				}

				if(settings.tBodyTag && $(this).find(">tbody").length == 1){
					columns = settings.tableColumn - $(this).find(">tbody").find(">tr:first>td").length;
					if(columns <= 0){
						columns = $(this).find(">tbody").find(">tr:first>td").length;
					}
					rows = settings.tableRow - $(this).find(">tbody").find(">tr").length;
					if(rows >= 1){
						for(ctr_row = 1 ; ctr_row <= rows ; ctr_row++){
							var appendRow = $("<tr></tr>");
							$(this).find(">tbody").append(appendRow);
							if(columns >=1 ){
								for(ctr_col = 1 ; ctr_col <= columns ; ctr_col++){
									append_td = $("<td style='width:"+settings.tableCellWidth+"px;height:"+settings.tableCellHeight+"px;'><div class='td-relative' style='width:100%;height:100%;position:relative;'></div></td>")
									$(appendRow).append(append_td);
									if(settings.tableCellStyle && typeof settings.tableCellStyle == "object"){
										$(append_td).css(settings.tableCellStyle)
									}
								}
							}
						}
					}
				}
				
				if(settings.tFootTag && $(this).find(">tfoot").length == 1){
					if($(this).find(">tfoot").find(">tr").length == 0){
						$(this).find(">tfoot").append("<tr></tr>");
					}
					columns = settings.tableColumn - $(this).find(">tfoot").find(">tr:first>td").length;
					for(ctr_col = 1 ; ctr_col <= columns ; ctr_col++){
						append_td = $("<td style='width:"+settings.tableCellWidth+"px;height:"+settings.tableCellHeight+"px;'><div class='td-relative' style='width:100%;height:100%;position:relative;'></div></td>");
						$(this).find(">tfoot>tr:first").append(append_td);
						if(settings.tableCellStyle && typeof settings.tableCellStyle == "object"){
							$(append_td).css(settings.tableCellStyle)
						}
					}
				}
			}
			if(settings.tFootTag && $(this).find(">tfoot").length == 0 ){
				if($(this).find(">tbody").length == 1){
					$(this).find(">tbody").after("<tfoot></tfoot>");
				}else if($(this).find(">tbody").length == 0){
					$(this).prepend("<tfoot></tfoot>");
				}
			}
			if(settings.tableBorder){
				$(this).attr("border",settings.tableBorder);
			}
			if(settings.tableCellSpacing){
				$(this).attr("cellspacing",settings.tableCellSpacing);
			}
			if(settings.tHeadBGColor){
				$(this).attr("style","background-color:"+settings.tableBorder+";");
			}

			if(settings.tHeadFontColor){
				$(this).attr("style","color:"+settings.tableBorder+";");
			}

			//action buttons and event
			//at initial state LOAD
			if(settings.resizableCell){
				$(this).children("tbody").children("tr").each(function(){
					$(this).children("td").each(function(){
						
						if(settings.resizableCell){
							$(this).resizable(settings.cellResizable);
							$(this).find(">.ui-resizable-handle").mousedown(function(){
								td_mousedown_index = $(this).closest("td").index();
								if(getCursor=="s-resize"){
									$(this).closest("tr").find(">td").addClass("resizing-also-resize");
									// var tr_h = $(this).closest("tr").outerHeight();
									// if($(this).closest("td").is(':not([rowspan])')){
									// 	$(this).closest("tr").children('td[rowspan]').each(function(){
									// 		$(this).data('added_height', ($(this).outerHeight() - tr_h) );
									// 	});
									// }
									
								}else if(getCursor=="e-resize"){
									$(this).closest("tbody").find(">tr").each(function(eqi_tr){
										$(this).find(">td").each(function(eqi_td){
											if(td_mousedown_index == eqi_td){
												$(this).addClass("resizing-also-resize");
											}
										});
									});
								}

							});
							$(this).children(".ui-resizable-handle").mouseup(function(){ //DEPRECATED HERE BECAUSE MAS NAUUNANG TANGGALIN DIITO KESA SA STOP
								if($(this).parent().hasClass("td-relative") && $(this).parent().data("cell_resizable_resized")){

								}else{
									$(".resizing-also-resize").removeClass("resizing-also-resize");
								}
							});
							$(this).children(".ui-resizable-handle").appendTo($(this).find(">.td-relative"));
						}
						
						if($(this).children(".ui-resizable-handle").length >= 1){
							$(this).children(".ui-resizable-handle").appendTo($(this).children(".td-relative") );
						}
						if(settings.tCellEvent){
							$(this).on(settings.tCellEventOn)
						}
						if($(this).children(".td-relative").length >= 1){
							if(settings.droppableCell){
								$(this).children(".td-relative").droppable(settings.cellDroppable);
							}
						}
					});
				});
			}

			if(settings.tColumnRemoving){
				if(settings.tHeadTag){

				}else if(settings.tBodyTag){
					border = 2;
					$(this).children("tbody").children("tr:first").children("td").each(function(eqi_td){
						if(eqi_td == 0){

						}else{
							append_td_action_button = $("<span class='table-actions-ra action-column-remove' style='display:inline-block;'><button class='table-remove-column' style='height:25px;border-top-left-radius:10px;border-top-right-radius:10px;'>Remove</button></span>");
							$(this).prepend(append_td_action_button);
							// console.log("append_td_action_button",append_td_action_button);
							if($(this).children(".td-relative").eq(0).length >= 1){
								$(this).children(append_td_action_button).appendTo($(this).children(".td-relative").eq(0));
								
								append_td_action_button.css({
									//"display":"inline-block",
									"top":-(parseInt(append_td_action_button.children(".table-remove-column").eq(0).css("height").split("px").join(""))+border)+"px",
									"width":"100%",
									"position":"relative"
								});
								append_td_action_button.find(".table-remove-column").css({
									width:"100%"
								});
							}
							$(this).children(append_td_action_button).find(".table-remove-column").on({
								"click":function(e,data){
									save_history();
									e.stopPropagation();
									var self = $(this);
									if(data){
										if(data["auto_remove"]){
											column_index =	self.closest("td").index();
											self.closest("table.thisDynamicTable").children("tbody").children("tr").each(function(eqi_tr){
												$(this).children("td").eq(column_index).remove();
											});
										}
									}else{
										var newConfirm = new jConfirm("Are you sure you want to delete this column?", 'Confirmation Dialog','', '', '', function(r){
											if(r==true){
												column_index =	self.closest("td").index();
												self.closest("table.thisDynamicTable").children("tbody").children("tr").each(function(eqi_tr){
													
													console.log($(this).children("td").eq(column_index).find(".setObject"))
													$(this).children("td").eq(column_index).find(".setObject").each(function(){
														var table_obj_del=$(this).attr("data-object-id");
														$('#layer_'+table_obj_del).remove();
													})
													$(this).children("td").eq(column_index).remove();
												});
											}
									    });
									    newConfirm.themeConfirm("confirm2", {
									    	'icon':'<i class="fa fa-question-circle fl-margin-right" font-size:15px;"></i>' 
	      
									    });
									}
								}
							});
						}
						
					});
				}
			}
			//at INITIAL LOAD
			if(settings.tRowRemoving){
				if(settings.tHeadTag){

				}else if(settings.tBodyTag){
					$(this).children("tbody").children("tr").each(function(eqi_tr){
						append_tr_action_button = $("<span class='table-actions-ra action-row-remove' style='display:inline-block !important;position:absolute;left: 0;'><button class=' table-remove-row' style='width:24px;height:24px;'>-</button></span>");

						if(eqi_tr == 0){

						}else{
							$(this).children("td:first").children(".td-relative").append(append_tr_action_button);
							
							$(append_tr_action_button).css({
								"margin-left":-($(append_tr_action_button).children(".table-remove-row").css("width").split("px").join(""))+"px"
							});
							$(append_tr_action_button).children(".table-remove-row").on({
								"click":function(e,data){
									save_history();
									e.stopPropagation();
									self = $(this);
									if(data){
										if(data["auto_remove"]){
											self.closest("tr").remove();
										}
									}else{
										var newConfirm = new jConfirm("Are you sure you want to delete this row?", 'Confirmation Dialog','', '', '', function(r){
											if(r==true){
												console.log(self.closest("tr").find(".setObject"))
													$(self.closest("tr")).find(".setObject").each(function(){
														var table_obj_del=$(this).attr("data-object-id");
														$('#layer_'+table_obj_del).remove();
													})
												self.closest("tr").remove();
											}
										});
										newConfirm.themeConfirm("confirm2", {
									    	'icon':'<i class="fa fa-question-circle fl-margin-right" font-size:15px;"></i>' 
	      
									    });
									}
								}
							});
						}
					});
				}
			}

			if(settings.tRowAdding){

				table_element = $(this);
				if($(this).parent().filter(".table-wrapper").length == 0){
					$(this).wrap("<span class='table-wrapper' style='display:inline-block;position:relative;' />");
				}
				append_tr_action_button = $("<span class='table-actions-ra action-row-add' style='position:absolute;'><button class=' table-add-row' style='width:25px;height:25px;'>+</button></span>");
				$(this).closest(".table-wrapper").append(append_tr_action_button);


				$(this).closest(".table-wrapper").find(append_tr_action_button).find(".table-add-row").on({
					"click":function(e,auto_event_data){
						save_history();
						e.stopPropagation();
						//adding new row
						var append_tr = $("<tr></tr>");
						var auto_cell_size_width = settings.tableCellWidth;
						var auto_cell_size_height = settings.tableCellHeight;


						if(auto_event_data){
							if(auto_event_data["auto"]){
								if(auto_event_data['cellSize']){
									auto_cell_size_width = auto_event_data['cellSize'][0];
									auto_cell_size_height = auto_event_data['cellSize'][1];
								}
							}
						}
						

						columns = $(this).closest(".table-wrapper").children("table.thisDynamicTable").find(">tbody").find(">tr:first").find(">td").length;
						
						$(this).closest(".table-wrapper").children("table.thisDynamicTable").children("tbody").append(append_tr);
						for(ctr_td = 1 ; ctr_td <= columns ; ctr_td++){
							//adding td's in a row
							var append_td = $("<td style='width:"+auto_cell_size_width+"px;height:"+auto_cell_size_height+"px;'><div class='td-relative' style='width:100%;height:100%;position:relative;'></div></td>");
							$(append_tr).append(append_td);

							//making td to be resizable
							if(settings.resizableCell){
								$(append_td).resizable(settings.cellResizable);
								$(append_td).children(".ui-resizable-handle").mousedown(function(){
									td_mousedown_index = $(this).closest("td").index();
									if(getCursor=="s-resize"){
										$(this).closest("tr").find(">td").addClass("resizing-also-resize");
										// var tr_h = $(this).closest("tr").outerHeight();
										// if($(this).closest("td").is(':not([rowspan])')){
										// 	$(this).closest("tr").children('td[rowspan]').each(function(){
										// 		$(this).data('added_height', ($(this).outerHeight() - tr_h) );
										// 	});
										// }
									}else if(getCursor=="e-resize"){
										$(this).closest("tbody").find(">tr").each(function(eqi_tr){
											$(this).find(">td").each(function(eqi_td){
												if(td_mousedown_index == eqi_td){
													$(this).addClass("resizing-also-resize");
												}
											});
										});
									}
								});
								$(append_td).children(".ui-resizable-handle").mouseup(function(){ //DEPRECATED HERE BECAUSE MAS NAUUNANG TANGGALIN DIITO KESA SA STOP
									if($(this).parent().hasClass("td-relative") && $(this).parent().data("cell_resizable_resized")){

									}else{
										$(".resizing-also-resize").removeClass("resizing-also-resize");
									}
								});
								$(append_td).children(".ui-resizable-handle").appendTo($(append_td).children(".td-relative"));
							}
							if(settings.tCellEvent){
								$(append_td).on(settings.tCellEventOn)
							}
							if(settings.droppableCell){
								$(append_td).children(".td-relative").droppable(settings.cellDroppable)
							}
							if(settings.tableCellStyle && typeof settings.tableCellStyle == "object"){
								$(append_td).css(settings.tableCellStyle)
							}
						}

						//adding action button remove row
						append_tr_action_button = $("<span class='table-actions-ra action-row-remove' style='display:inline-block;position:absolute;left: 0;'><button class=' table-remove-row' style='width:24px;height:24px;'>-</button></span>");
						$(append_tr).find(">td:first").find(".td-relative").append(append_tr_action_button)
						//setting css of action remove row
						$(append_tr_action_button).css({
							"margin-left":-($(append_tr_action_button).width())+"px"
						});
						//removing row event 
						append_tr_action_button.find(".table-remove-row").on({
							"click":function(e,data){
								save_history();
								e.stopPropagation();
								self = $(this);
								if(data){
									if(data["auto_remove"]){
										self.closest("tr").remove();
									}
								}else{
									var newConfirm = new jConfirm("Are you sure you want to delete this row?", 'Confirmation Dialog','', '', '', function(r){
										if(r==true){
											$(self.closest("tr")).find(".setObject").each(function(){
														var table_obj_del=$(this).attr("data-object-id");
														$('#layer_'+table_obj_del).remove();
													})
											self.closest("tr").remove();
										}
								    });
							 		newConfirm.themeConfirm("confirm2", {
								    	'icon':'<i class="fa fa-question-circle  fl-margin-right" font-size:15px;"></i>' 
	  
								    });
								}
								// jConfirm("Are you sure you want to delete this row?", 'Confirmation Dialog','', '', '', function(r){
								// 	if(r==true){
								// 		self.closest("tr").remove();
								// 	}
							    // });
								// newConfirm.themeConfirm("confirm2", {
							 //    	'icon':'<i class="fa fa-exclamation-triangle fl-margin-right" style="color:#f00; font-size:15px;"></i>' 
  
							 //    });
							}
						});
						var table_set_obj = $(this).closest('[data-type="table"]');
						var table_collision = table_set_obj.testCollision();
						
						if(table_collision["final_collision_top"]){
							table_set_obj.css('top',0);
							setFormbuilderMinSize();
						}
						else if(table_collision["final_collision_bot"]){
							table_set_obj.css('top', table_set_obj.position().top-table_collision["final_collide_overflow_bot"]);
							$(".formbuilder_ws").trigger("resize");
						}
					}
				});
			}

			if(settings.tColumnAdding){
				table_element = $(this);
				border = 2;
				if($(this).parent().filter(".table-wrapper").length == 0){
					$(this).wrap("<span class='table-wrapper' style='display:inline-block;' />");
				}
				append_column_action_button = $("<span class='table-actions-ra action-column-add' style='position:absolute;'><button style='width:25px;height:25px;' class=' table-add-column'>+</button></span>");
				$(this).closest(".table-wrapper").prepend(append_column_action_button);
				append_column_action_button.css({
					"right":"0px",
					"margin-right":-( parseInt(append_column_action_button.children(".table-add-column").eq(0).css("width").split("px").join("")) )+"px"
				});
				
				$(this).closest(".table-wrapper").find(append_column_action_button).find(".table-add-column").on({
					"click":function(e,auto_event_data){
						save_history();
						e.stopPropagation();
						//adding td row
						var auto_cell_size_width = settings.tableCellWidth;
						var auto_cell_size_height = settings.tableCellHeight;
						
						

						if(settings.tHeadTag){

							append_th = $("<th style='width:"+auto_cell_size_width+"px;height:"+auto_cell_size_height+"px;'><div class='th-relative' style='width:100%;height:100%;position:relative;'></div></th>");
							$(this).closest(".table-wrapper").find(">table.thisDynamicTable").find(">thead").find(">tr:first").append(append_th);
							//adding an action button remove column
							append_th_action_button = $("<span class='table-actions-ra action-column-remove' style='display:inline-block;'><button class='table-remove-column' style='height:25px;border-top-left-radius:10px;border-top-right-radius:10px;'>Remove</button></span>");
							$(this).closest(".table-wrapper").find(">table.thisDynamicTable").find(">thead").find(">tr:first").find(append_th).find(".th-relative").prepend(append_th_action_button);
							append_th_action_button.css({
								"top":-(append_th_action_button.height()+border)+"px",
								"width":"100%",
								"position":"relative"
							});
							append_th_action_button.find(".table-remove-column").css({
								width:"100%"
							});
							$(this).closest(".table-wrapper").find(">table.thisDynamicTable").find(">thead").find(">tr:first").find(append_th).find(".th-relative").find(append_th_action_button).find(".table-remove-column").on({
								"click":function(e,data){
									save_history();
									e.stopPropagation();
									self = $(this);
									if(data){
										if(data["auto_remove"]){
											column_index =	self.closest("td").index();
											self.closest("table.thisDynamicTable").children("tbody").children("tr").each(function(eqi_tr){
												$(this).children("td").eq(column_index).remove();
											});
										}
									}else{
										var newConfirm = new jConfirm("Are you sure you want to delete this column?", 'Confirmation Dialog','', '', '', function(r){
											if(r==true){
												column_index =	self.closest("th").index();
												self.closest("table.thisDynamicTable").children("tbody").children("tr").each(function(eqi_tr){
													
													console.log($(this).children("td").eq(column_index).find(".setObject"))
													$(this).children("td").eq(column_index).find(".setObject").each(function(){
														var table_obj_del=$(this).attr("data-object-id");
														$('#layer_'+table_obj_del).remove();
													})
													
													$(this).children("td").eq(column_index).remove();
												});
											}
									    });
										newConfirm.themeConfirm("confirm2", {
									    	'icon':'<i class="fa fa-question-circle fl-margin-right" font-size:15px;"></i>'
									    });
									}
								}
							});
							// adding a cell on each row


							$(this).closest(".table-wrapper").find(">table.thisDynamicTable").find(">tbody").find(">tr").each(function(eqi_tr){
								append_td = $("<td style='width:"+auto_cell_size_width+"px;height:"+auto_cell_size_height+"px;'><div class='td-relative' style='width:100%;height:100%;position:relative;'></div></td>");
								$(this).append(append_td);
								//making td to be resizable
								if(settings.resizableCell){
									$(this).find(append_td).resizable(settings.cellResizable);
									$(this).find(append_td).find(">.ui-resizable-handle").mousedown(function(){
										td_mousedown_index = $(this).closest("td").index();
										if(getCursor=="s-resize"){
											$(this).closest("tr").find(">td").addClass("resizing-also-resize");
											// var tr_h = $(this).closest("tr").outerHeight();
											// if($(this).closest("td").is(':not([rowspan])')){
											// 	$(this).closest("tr").children('td[rowspan]').each(function(){
											// 		$(this).data('added_height', ($(this).outerHeight() - tr_h) );
											// 	});
											// }
										}else if(getCursor=="e-resize"){
											$(this).closest("tbody").find(">tr").each(function(eqi_tr){
												$(this).find(">td").each(function(eqi_td){
													if(td_mousedown_index == eqi_td){
														$(this).addClass("resizing-also-resize");
													}
												});
											});
										}
									});
									$(this).children(".ui-resizable-handle").mouseup(function(){ //DEPRECATED HERE BECAUSE MAS NAUUNANG TANGGALIN DIITO KESA SA STOP
										if($(this).parent().hasClass("td-relative") && $(this).parent().data("cell_resizable_resized")){

										}else{
											$(".resizing-also-resize").removeClass("resizing-also-resize");
										}
									});
									$(this).find(append_td).find(">.ui-resizable-handle").appendTo($(this).find(append_td).find(">.td-relative"));
								}
							});
						}else if(settings.tBodyTag){
							$(this).closest(".table-wrapper").children("table.thisDynamicTable").children("tbody").children("tr").each(function(eqi_tr){
								var append_td = $("<td style='width:"+auto_cell_size_width+"px;height:"+auto_cell_size_height+"px;'><div class='td-relative' style='width:100%;height:100%;position:relative;'></div></td>");
								$(this).append(append_td);
								if(settings.resizableCell){
									$(this).find(append_td).resizable(settings.cellResizable);
									$(this).find(append_td).find(">.ui-resizable-handle").mousedown(function(){
										td_mousedown_index = $(this).closest("td").index();
										if(getCursor=="s-resize"){
											$(this).closest("tr").find(">td").addClass("resizing-also-resize");
											// var tr_h = $(this).closest("tr").outerHeight();
											// if($(this).closest("td").is(':not([rowspan])')){
											// 	$(this).closest("tr").children('td[rowspan]').each(function(){
											// 		$(this).data('added_height', ($(this).outerHeight() - tr_h) );
											// 	});
											// }
										}else if(getCursor=="e-resize"){
											$(this).closest("tbody").find(">tr").each(function(eqi_tr){
												$(this).find(">td").each(function(eqi_td){
													if(td_mousedown_index == eqi_td){
														$(this).addClass("resizing-also-resize");
													}
												});
											});
										}
									});
									$(this).children("td").children(".ui-resizable-handle").mouseup(function(){  //DEPRECATED HERE BECAUSE MAS NAUUNANG TANGGALIN DIITO KESA SA STOP
										if($(this).parent().hasClass("td-relative") && $(this).parent().data("cell_resizable_resized")){

										}else{
											$(".resizing-also-resize").removeClass("resizing-also-resize");
										}
									});
									$(this).find(append_td).children(".ui-resizable-handle").appendTo($(this).find(append_td).children(".td-relative"));
								}
								
								if(eqi_tr == 0){
									//adding an action button remove column
									append_td_action_button = $("<span class='table-actions-ra action-column-remove' style='display:inline-block;'><button class='table-remove-column' style='height:25px;border-top-left-radius:10px;border-top-right-radius:10px;'>Remove</button></span>");
									$(this).find(append_td).find(".td-relative").prepend(append_td_action_button);
									append_td_action_button.css({
										"top":-(append_td_action_button.height()+border)+"px",
										"width":"100%",
										"position":"relative"
									});

									$(append_td_action_button).find(".table-remove-column").css({
										width:"100%"
									});
									
									$(this).find(append_td).find(".td-relative").find(append_td_action_button).find(".table-remove-column").on({
										"click":function(e,data){
											save_history();
											e.stopPropagation();
											self = $(this);
											if(data){
												if(data["auto_remove"]){
													column_index =	self.closest("td").index();
													self.closest("table.thisDynamicTable").children("tbody").children("tr").each(function(eqi_tr){
														$(this).children("td").eq(column_index).remove();
													});
												}
											}else{
												var newConfirm = new jConfirm("Are you sure you want to delete this column?", 'Confirmation Dialog','', '', '', function(r){
													if(r==true){
														column_index =	self.closest("td").index();
														self.closest("table.thisDynamicTable").children("tbody").children("tr").each(function(eqi_tr){
															console.log($(this).children("td").eq(column_index).find(".setObject"))
															$(this).children("td").eq(column_index).find(".setObject").each(function(){
															var table_obj_del=$(this).attr("data-object-id");
															$('#layer_'+table_obj_del).remove();
															})
															$(this).children("td").eq(column_index).remove();
														});
													}
											    });
												newConfirm.themeConfirm("confirm2", {
													'icon':'<i class="fa fa-question-circle fl-margin-right" font-size:15px;"></i>' 

												});
											}
										}
									});
								}
								if(settings.tCellEvent){
									$(append_td).on(settings.tCellEventOn)
								}
								if(settings.droppableCell){
									$(append_td).children(".td-relative").droppable(settings.cellDroppable)
								}
								if(settings.tableCellStyle && typeof settings.tableCellStyle == "object"){
									$(append_td).css(settings.tableCellStyle)
								}
							});
						}
						/*added carlo medina 5/8/2015*/
						
						var table_set_obj = $(this).closest('[data-type="table"]');
						var table_collision = table_set_obj.testCollision();
						if(table_collision["final_collision_right"] && table_collision["final_collision_left"]){
							table_set_obj.css('left',0);
							setFormbuilderMinSize();
						}
						else if(table_collision["final_collision_right"]){
							table_set_obj.css('left', table_set_obj.position().left-table_collision["final_collide_overflow_right"]);
							$(".formbuilder_ws").trigger("resize");
						}
						
					}
				});
			}


			//resizable event
			// if($(this).find(".td-relative").length >= 1){
			// 	$(this).find(".td-relative").closest("td").resizable({
			// 		handles:"s,e",
			// 		containment:"#droppable",
			// 		alsoResize:".resizing-also-resize",
			// 		start:function(e, ui){
			// 			// column_index = $(this).index();
			// 			// row_index = $(this).closest("tr").index();
			// 			// if($(".resizing-also-resize").length>=1){
			// 			// 	$(".resizing-also-resize").removeClass("resizing-also-resize");
			// 			// }
			// 			// if(getCursor == "e-resize"){
			// 			// 	$(this).closest("tbody").find(">tr").each(function(){
			// 			// 		$(this).find(">td").eq(column_index).addClass("resizing-also-resize");
			// 			// 	});
			// 			// }else if(getCursor == "s-resize"){
			// 			// 	$(this).closest("tr").find(">td").addClass("resizing-also-resize");
			// 			// }
			// 		},
			// 		stop:function(e, ui){
			// 			$(".resizing-also-resize").removeClass("resizing-also-resize");
			// 		}
			// 	});

			// 	//making a class for a parallel rows or columns resizing
			// 	$(this).find(".td-relative").closest("td").find(">.ui-resizable-handle").mousedown(function(){
			// 		td_mousedown_index = $(this).closest("td").index();
			// 		if(getCursor=="s-resize"){
			// 			$(this).closest("tr").find(">td").addClass("resizing-also-resize");
			// 		}else if(getCursor=="e-resize"){
			// 			$(this).closest("tbody").find(">tr").each(function(eqi_tr){
			// 				$(this).find(">td").each(function(eqi_td){
			// 					if(td_mousedown_index == eqi_td){
			// 						$(this).addClass("resizing-also-resize");
			// 					}
			// 				});
			// 			});
			// 		}
			// 	});

			// 	//getting the handle inside the td-relative
			// 	$(this).find(".td-relative").closest("td").find(".ui-resizable-handle").each(function(){
			// 		$(this).appendTo($(this).closest("td").find(">.td-relative"));
			// 	});
			// }





			if($(this).closest(".table-wrapper").length >= 1){
				$(this).closest(".table-wrapper").find(".table-actions-ra").css({
					"opacity":0
				});

				$(this).closest(".table-wrapper").on({
					"mouseenter":function(e){
						e.stopPropagation();
						// $(this).find(".table-actions-ra").children().eq(0).css('height','25px')
						var self = this;
						var set_obj_ele = $(self).closest('.setObject[data-type="table"]');
						var data_object_id = set_obj_ele.attr('data-object-id');
						set_obj_ele.find('#obj_actions_'+data_object_id).removeClass('display');
						$(this).find(".table-actions-ra").stop().animate({
							"opacity":1
						},200,function(){
							//fully visible
							$(self).find(".table-actions-ra.action-column-remove,.table-actions-ra.action-row-remove").css('display','inline-block');
						});
						// $(this).find('.table-actions-ra.action-column-remove').css({
						// 	"max-height":"",
						// 	"display":"inline-block"
						// });

						
					},
					"mouseleave":function(){
						// $(this).find(".table-actions-ra").children().eq(0).css('height','0px')
						var self = this;
						var set_obj_ele = $(self).closest('.setObject[data-type="table"]');
						var data_object_id = set_obj_ele.attr('data-object-id');
						$(this).find(".table-actions-ra").stop().animate({
							"opacity":0
						},200,function(){
							// $(this).children().eq(0).css('height','0px')
							//set_obj_ele.find('#obj_actions_'+data_object_id).addClass('display');
							$(self).find(".table-actions-ra.action-column-remove,.table-actions-ra.action-row-remove").css('display','none');
						});
						// $(this).find('.table-actions-ra.action-column-remove').css({
						// 	"max-height":"0px",
						// 	"display":"block"
						// });
						
					}
				});

				// if(settings.tableSetProperty){
				// 	action_settings = $(
				// 		"<span class='table-actions-ra action-table-settings' style='position:absolute;'>"+
				// 			"<button class='table-settings'>Settings</button>"+
				// 			"<div class='table-settings-optionpane' style='box-shadow:0px 0px 10px black;z-index:10;background-color:red;position:absolute;width:300px;height:auto;bottom:0;'>"+
				// 				"<div class='table-settings-optionpane-header' style='background-color:blue;'>TABLE SETTINGS</div>"+
				// 				"<div class='table-settings-optionpane-body' style='background-color:green;'>"+
				// 					"<button>TESTING</button><input type='text'/>"+
				// 				"</div>"+
				// 			"</div>"+
				// 		"</span>"
				// 	);
				// 	$(this).closest(".table-wrapper").prepend(action_settings);
				// 	$(action_settings).css({
				// 		"top":0+"px",
				// 		"margin-top":-($(action_settings).height())+"px"
				// 	});
				// 	$(this).closest(".table-wrapper").children(action_settings).children(".table-settings").on({
				// 		"click":function(e){
				// 			e.stopPropagation();
				// 			$(this).closest(".table-wrapper")
				// 		}
				// 	});
				// }
			}
			if(settings.tBodyTag){
				
			}

			
		});
		function makeTdSelectable(the_element){
			if($(the_element).prop("tagName") == "TABLE"){
				$(the_element).children("tbody").children("tr").children("td").on({
					"click":function(ev){
						if(ev.ctrlKey){
							if($(this).hasClass("dynamic-td-droppable-selected")){
								$(this).removeClass("dynamic-td-droppable-selected");
							}else{
								//$(".table td").removeClass("dynamicTDDroppable")
								$(this).addClass("dynamic-td-droppable-selected");
							}
						}else{
							if($(this).hasClass("dynamic-td-droppable-selected")){
								$(this).removeClass("dynamic-td-droppable-selected");
							}else{
								$(".dynamic-td-droppable-selected").removeClass("dynamic-td-droppable-selected");
								$(this).addClass("dynamic-td-droppable-selected");
							}
						}
					}
				});
			}else if($(the_element).prop("tagName") == "TD"){
				$(the_element).on({
					"click":function(ev){
						if(ev.ctrlKey){
							if($(this).hasClass("dynamic-td-droppable-selected")){
								$(this).removeClass("dynamic-td-droppable-selected");
							}else{
								//$(".table td").removeClass("dynamicTDDroppable")
								$(this).addClass("dynamic-td-droppable-selected");
							}
						}else{
							if($(this).hasClass("dynamic-td-droppable-selected")){
								$(this).removeClass("dynamic-td-droppable-selected");
							}else{
								$(".dynamic-td-droppable-selected").removeClass("dynamic-td-droppable-selected");
								$(this).addClass("dynamic-td-droppable-selected");
							}
						}
					}
				});
			}
			
		}
	}
}(jQuery));


var getCursor = "";
$(document).ready(function(){

	$("body").on("mouseover",".ui-resizable-handle",function(){
		getCursor = $(this).css("cursor");
	});

	// $("body").on("click",".thisDynamicTable tbody tr td",function(ev){
	// 	if(ev.ctrlKey){
	// 		if($(this).hasClass("dynamic-td-droppable-selected")){
	// 			$(this).removeClass("dynamic-td-droppable-selected");
	// 		}else{
	// 			//$(".table td").removeClass("dynamicTDDroppable")
	// 			$(this).addClass("dynamic-td-droppable-selected");
	// 		}
	// 	}else{
	// 		if($(this).hasClass("dynamic-td-droppable-selected")){
	// 			$(this).removeClass("dynamic-td-droppable-selected");
	// 		}else{
	// 			$(".dynamic-td-droppable-selected").removeClass("dynamic-td-droppable-selected");
	// 			$(this).addClass("dynamic-td-droppable-selected");
	// 		}
	// 	}
	// });
});


/*
		$(document).ready(function(){
			$("body").on("click","#table-border-top",function(e){
				$("table[element-type='control']").each(function(eqi_tables){
					console.log("collecting...");
					var collect_selected_cell = [];
					$(this).find(".droppableTD").each(function(eqi_dTD){
						collect_selected_cell.push({
							"row_index":$(this).closest("tr").index(),
							"column_index":$(this).closest("td").index(),
							"td_element":$(this).closest("td")
						});
					});
					collect_selected_cell = sortResults(collect_selected_cell, "row_index", 'asc');
					///executing of border css
					for(var i= 0; i<collect_selected_cell.length; i++ ){
						if(collect_selected_cell[i].td_element.css("border-top").split(" ").indexOf("none") == 1){
							css_border_top ="1px solid black";
							collect_selected_cell[i].td_element.css({
								"border-top":css_border_top
							});
							if(collect_selected_cell[i].td_element.closest("tr").prev().length == 1){
								collect_selected_cell[i]
								.td_element.closest("tr")
								.prev()
								.children("td")
								.eq(collect_selected_cell[i].column_index).css({
									"border-bottom":css_border_top
								});
							}
						}else{
							collect_selected_cell[i].td_element.css({
								"border-top":"none"
							});
							if(collect_selected_cell[i].td_element.closest("tr").prev().length == 1){
								collect_selected_cell[i]
								.td_element.closest("tr")
								.prev()
								.children("td")
								.eq(collect_selected_cell[i].column_index).css({
									"border-bottom":"none"
								});
							}
						}
					}
				});
			});


			$("body").on("click","#table-border-bottom",function(e){
				console.log("collecting...");
				var collect_selected_cell = [];
				$("table[element-type='control']").each(function(eqi_tables){
					//
					$(this).find(".droppableTD").each(function(eqi_dTD){
						collect_selected_cell.push({
							"row_index":$(this).closest("tr").index(),
							"column_index":$(this).closest("td").index(),
							"td_element":$(this).closest("td")
						});
					});
					collect_selected_cell = sortResults(collect_selected_cell, "row_index", 'asc');
					for (var i = 0; i < collect_selected_cell.length; i++) {
						if(collect_selected_cell[i].td_element.css("border-bottom").split(" ").indexOf("none") == 1){
							css_border_top ="1px solid black";
							collect_selected_cell[i].td_element.css({
								"border-bottom":css_border_top
							});
							if(collect_selected_cell[i].td_element.closest("tr").next().length == 1){
								collect_selected_cell[i]
								.td_element.closest("tr")
								.next()
								.children("td")
								.eq(collect_selected_cell[i].column_index).css({
									"border-top":css_border_top
								});
							}
						}else{
							collect_selected_cell[i].td_element.css({
								"border-bottom":"none"
							});
							if(collect_selected_cell[i].td_element.closest("tr").next().length == 1){
								collect_selected_cell[i]
								.td_element.closest("tr")
								.next()
								.children("td")
								.eq(collect_selected_cell[i].column_index).css({
									"border-top":"none"
								});
							}
						}
					};
				});
			});



			$("#table-border-right").on({
				"click":function(e){

				}
			});
			$("#table-border-left").on({
				"click":function(e){
					//
				}
			});
			function sortResults(json_v ,prop, asc) {
			    json_v = json_v.sort(function(a, b) {
			        if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
			        else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
			    });
			    return json_v;
			}
		});
*/


// $(document).ready(function(){
// 	alert($(".thisDynamicTable").length)
// 	$("body").on("click",".thisDynamicTable tbody tr td",function(ev){
// 		if(ev.ctrlKey){
// 			if($(this).hasClass("droppableTD dynamicTDDroppable")){
// 				$(this).removeClass("droppableTD dynamicTDDroppable");
// 			}else{
// 				//$(".table td").removeClass("dynamicTDDroppable")
// 				$(this).addClass("droppableTD dynamicTDDroppable");
// 			}
// 		}else{
// 			if($(this).hasClass("droppableTD dynamicTDDroppable")){
// 				$(this).removeClass("droppableTD dynamicTDDroppable");
// 			}else{
// 				$(".table td").removeClass("droppableTD dynamicTDDroppable")
// 				$(this).addClass("droppableTD dynamicTDDroppable");
// 			}
// 		}
// 	});

// });


// $("body").on("click","#droppable",function(){
// 	if(removeTD==0){
// 		$(".table td").removeClass("droppableTD")
// 	}
// });