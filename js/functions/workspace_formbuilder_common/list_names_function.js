//ADDED JEWEL 10142014
var names = {
    init: function () {
        this.addEventListeners();
    },
    selected: [],
    data: {},
    obj_input: "",
    addEventListeners: function () {
        var self = this;
        $('body').on('click', '.viewNames', function () {
            self.obj_input = $(this);
            var returnField = $(this).attr('return-field');
            if (!$('#' + returnField).attr('disabled')) {
                self.loadDialog(this);
            }
        });
        $('.viewNames').css({'position': 'relative', 'z-index': 3, 'top':0})
        $('body').on('click', '.select_name', function (e) {
            if ($(e.target).closest(".suggestion_container").length > 0) {
                $(e.target).closest(".suggestion_container").remove();
            }
            self.selectValue(this);
        });

        $('body').on('click', '.names-selection', function () {
            var checked = $(this).prop('checked');
            var returnValue = $(this).closest('div').attr('return-value');

            if (checked) {
                self.selected.push(returnValue);
            } else {
                remove(self.selected, returnValue);
            }
//            console.log(self.selected);
            self.data.selectedValues = self.selected.join('|');
            $('.selectedNames').html('Currently Selected: [ ' + self.selected.join(', ') + ' ]'); //<b>Currently Selected:</b>
        });

        $('body').on('click', '.namesOkButton', function () {

            var selectedValues = [];
            var returnValue = '';
            var txtReturnField = $(this).attr('return-field');
            var input_obj = ($(self.obj_input).closest("tr").find(".getFields#" + txtReturnField).length >= 1) ? $(self.obj_input).closest("tr").find(".getFields#" + txtReturnField) : $('#' + txtReturnField);
            //                    var returnField = '';
            $('.names-selection:checked').each(function () {
                returnValue = $(this).closest('div').attr('return-value');
                selectedValues.push(returnValue);
            });

            // $('#' + txtReturnField).val(self.data.selectedValues);
            // $('#' + txtReturnField).change();
            $(input_obj).val(self.data.selectedValues);
            $(input_obj).change();
            $('.fl-closeDialog').click();
        });
        var requestAjaxDataPicklist = {};
        $("body").on("keyup", '[data-type="listNames"] input.getFields', function (e, a, b) { //dating keyup Modified by michael 3:34 PM 10/2/2015 //restore sa dating keyup 1:14 PM 11/20/2015
            var keycode_pressed = e.keyCode;
            if(keycode_pressed == 13){
                if(a=="SetDefault"){
                    return false;
                }
                var this_input = this;
                var search = $.trim($(this).val());
                var viewNamesButton = $(this).closest(".input_position_below").find(".viewNames");

                var selectionType = viewNamesButton.attr('list-type-selection');
                var returnField = viewNamesButton.attr('return-field');
                var txtReturnField = viewNamesButton.attr('return-field');
                var shortListType = viewNamesButton.attr('short-list-type');
                var columnsDisplay = [{
                        "FieldLabel": "Account Name"
                    },
                    {
                        "FieldLabel": "Position"
                    }]

                var names_data = {
                    returnField: returnField,
                    shortListType: shortListType,
                    iDisplayLength: 5,
                    search: search,
                    columnsDisplay: JSON.stringify(columnsDisplay)
                };
                if (search != "") {
                    //initialize modal
                    suggestionTableModal(this_input, 'field-container-listnames');
                    try {
                        requestAjaxDataPicklist.abort();
                    } catch (err) {

                    }
                    requestAjaxDataPicklist = $.post("/ajax/names", names_data, function (data) {
                        try {
                            data = JSON.parse(data);
                            suggestionTableData(this_input, data, function () {
                                $('[data-suggestion="' + $(this_input).attr("id") + '"]').find("table tbody tr").addClass('select_name');
                                // $('[data-suggestion="'+ $(this_input).attr("id") +'"]').find(".suggestion-data-content").perfectScrollbar();
                            });
                        } catch (err) {
                            console.log(err)
                        }
                    });
                } else {
                    $('[data-suggestion="' + $(this_input).attr("id") + '"]').remove();
                }
            }
        })

        // $('.field-container-listnames input').attr('readonly', true);
        //FS#8055 - tes april 12 2016
        FieldSelectionClear($('.viewNames'), "view-names");
    },
    namesSuggestionData: function (this_input, data) {

    },
    loadDialog: function (obj) {
        var self = this;
        var selectionType = $(obj).attr('list-type-selection');
        var returnField = $(obj).attr('return-field');
        var txtReturnField = $(obj).attr('return-field');
        var shortListType = $(obj).attr('short-list-type');
        var input_obj = ($(self.obj_input).closest("tr.picklist-tr").find(".getFields").length >= 1) ? $(self.obj_input).closest("tr").find(".getFields") : $('#' + returnField);
        //var selectedValues = $('#' + returnField).val();
        var selectedValues = $(input_obj).val();
        this.selected = [];

        if (selectedValues != '') {
            this.selected = selectedValues.split('|');
        }

        self.data = {
            returnField: returnField,
            selectionType: selectionType,
            selectedValues: this.selected.join('|'),
            shortListType: shortListType
        };


        var result = '<h3><i class="icon-key"></i><span class="popup-picklist-title">Select Account Name</span></h3><div class="hr"></div><div></div>';
        result += "<div class='fl-list-of-app-record-wrapper'>";
        result += '<div class="fl-option-toolbar">';
        result += '<div class="fl-toolbar-left-wrapper"></div>';
        result += '<div class="fl-toolbar-right-wrapper">';
        result += '<div class="fl-search-wrapper">';
        result += '<input type="text" placeholder="Search" class="searchNames">';
        result += '<input type="button" value="" class="search search-names-button icon fl-search-icon-list"/>';
        result += '</div>';
        result += '</div>';
        result += '</div>';

        if (selectionType == 'Multiple') {
            result += '<div class="fl-search-wrapper selectedNames" style="margin-top:10px;margin-bottom:10px;width:100%;font-style:italic"><b>Currently Selected:</b> [ ' + this.selected.join(', ') + ' ]</div>';
        }

        result += "<div class='fl-datatable-wrapper'>";
        result += "<table id='namesData' class='display_data dataTable recordsDatatable' style='cursor:pointer'><thead class='fl-header-tbl-wrapper'>";
        result += '<tr>';
        result += "<th data-type=''>Account Name</th>";
        result += "<th data-type=''>Position</th>"
        result += "</tr>";
        //result += "</table></div><div class='dataTable_widget'></div>";

        if (selectionType == 'Multiple') {
            result += '<input type="button" return-field="' + txtReturnField + '"class="btn-blueBtn namesOkButton fl-margin-right fl-margin-bottom fl-positive-btn fl-buttonEffect" object_type="workflow_chart-fields-trigger" id="" value="OK" node-data-id="node_2" style="">';
        }

        ui.unblock();


        if (window.self !== window.top) { 
            if ($('.ui-portal-body').length >= 1) {
                var saveDialog = new jDialog(result, "", "430px", "300", "", function () {

                });
            }else {
                var saveDialog = new jDialog(result, "", "900", "", "", function () {

                });
            }
            
        }else {
            var saveDialog = new jDialog(result, "", "900", "", "", function () {

            });
        }


        saveDialog.themeDialog('modal2');

        $('.content-dialog-scroll').addClass('fornameslistData').css({'height': '400px', 'max-height': 'none'});

        self.generateDataTable(self.data, function () {

        });
        $('.search-names-button').on('click', function (e) {
            var evs = $.Event('keyup');
            evs.which = 13;
            $('.searchNames').trigger(evs);
        })
        $('.searchNames').on('keyup', function (e) {
            var keycode = e.keyCode || e.which;
            if (keycode === 13) {
                self.data.search = $.trim($(this).val());
                self.generateDataTable(self.data, function () {

                });
            }

        });

        if (selectionType != 'Multiple') {
            $('body').on('click', '.select_name', function () {
                self.selectValue(this);
            });
        }
    },
    generateDataTable: function (arg, callback) {
        var returnTable = $('#namesData').dataTable({
            "oLanguage": {
                "sProcessing": ''
            },
            "bAutoWidth": false,
            "bDestroy": true,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 6,
            "sPaginationType": "full_numbers",
            "bSort": true,
            "sAjaxSource": "/ajax/names",
            "fnServerData": function (sSource, aoData, fnCallback) {
                aoData.push({"name": "search", "value": arg.search});
                aoData.push({"name": "selectionType", "value": arg.selectionType});
                aoData.push({"name": "selectedValues", "value": arg.selectedValues});
                aoData.push({"name": "returnField", "value": arg.returnField});
                aoData.push({"name": "shortListType", "value": arg.shortListType});
                ui.blockPortion($('.ps-container'));
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {
                        console.log(data)
//                                 return;
                        // $(obj).find("tbody tr td:not(td:first-child)").css({
                        //     "cursor":"pointer"
                        // });
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function (oSettings, json) {

                var obj = '#namesData';
                var listOfappContainer = $('.fl-list-of-app-record-wrapper');
                dataTable_widget(obj, listOfappContainer);
                callback(true);
            },
            fnDrawCallback: function () {
                ui.unblockPortion($('.ps-container'));
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (arg.selectionType != 'Multiple') {
                    $(nRow).addClass('select_name');
                }

            }
        });
    },
    getNames: function (data, callback) {
        $.get('/ajax/names', data, function (result) {
            callback(result);
        });
    },
    selectValue: function (obj) {
        var self = this;
        var returnField = $(obj).find('div').attr('return-field');
        var returnValue = $(obj).find('div').attr('return-value');
        /*search names*/
        console.log("neeems", self.obj_input)
        var input_obj = ($(self.obj_input).closest("tr.picklist-tr").find(".getFields").length >= 1) ? $(self.obj_input).closest("tr").find(".getFields") : $('#' + returnField);

        // $('#' + returnField).val(returnValue);
        // $('#' + returnField).change();
        $(input_obj).val(returnValue);
        $(input_obj).change();
        $(input_obj).parent().children('[data-suggestion]').remove();
        $('.fl-closeDialog').click();
    }
}

////this is from formbuilder.js 0722PM 10202015
//$(document).ready(function(){
//    names.init();
//});