//picklistSorter
function picklistSorter() {
    var popup_container_data = $('#popup_container').data();
    var picklist_obj = popup_container_data["clicked_button"].attr("display_column_sequence");
    var pangselect_ng_table = $("#popup_container").find("table.picklist-view-columns tbody.ui-sortable");


    try {
        obj_picklist_attr_parsed = JSON.parse(picklist_obj);
    }
    catch (error) {
        obj_picklist_attr_parsed = null;
    }
    console.log("picklist_obj", picklist_obj)
    if (obj_picklist_attr_parsed != null) {
        var field_picklistname = popup_container_data["clicked_button"].attr("form-id");
        var field_ng_textbox = $("#popup_container").find(".form-select.picklist_form_type").val();
        if (field_picklistname == field_ng_textbox) {
            var tr_elems = $(pangselect_ng_table).find('tr').filter(function () {
                return $(this).find('th').length <= 0;
            });
            console.log(obj_picklist_attr_parsed);

            // obj_picklist_attr_parsed = obj_picklist_attr_parsed.filter(function (val, index_data) { //ISSUE FS#7716
            //     return index_data !== 0;
            // });
            for (var ctr in obj_picklist_attr_parsed) {
                //{//med data swapping
                //    var pick_attr_fieldname = obj_picklist_attr_parsed[ctr]["FieldName"];
                //    var pick_attr_fieldlabel = obj_picklist_attr_parsed[ctr]["FieldLabel"];

                //    $(pangselect_ng_table).find('tr:eq(' + (parseInt(ctr)) + ')').children('td:eq(1)').attr('data-original-title',pick_attr_fieldname+' (Drag up/down)');
                //    $(pangselect_ng_table).find('tr:eq(' + (parseInt(ctr)) + ') td span').text(pick_attr_fieldname);
                //    $(pangselect_ng_table).find('tr:eq(' + (parseInt(ctr)) + ') td input.form-text').val(pick_attr_fieldlabel);
                //  }

                {//DOM swapping
                    tr_elems = $(pangselect_ng_table).find('tr').filter(function () {
                        return $(this).find('th').length <= 0;
                    });
                    var pick_attr_fieldname = obj_picklist_attr_parsed[ctr]["FieldName"];
                    var pick_attr_fieldlabel = obj_picklist_attr_parsed[ctr]["FieldLabel"];
                    var target_destination = tr_elems.eq(ctr);
                    var target_swap = tr_elems.filter(function () {
                        return $(this).children('td:eq(1)').text() == pick_attr_fieldname;
                    });
                    target_destination.before(target_swap);
                    $(target_swap).find('input.form-text[type]').val(pick_attr_fieldlabel);
                }
            }
        }
    }
}
var picklist = {
    init: function () {
        this.addEventListeners();
    },
    selected: [],
    data: {},
    obj_input: "",
    addEventListeners: function () {
        $('.setOBJ[data-type="pickList"] .pickListButton').each(function () {
            var valueSelectionType = $(this).attr('selection-type');
            if (valueSelectionType != 'Multiple') {
                $('body').on('click.picklistFunction', '[click-event="select"]', function () {
                    if (valueSelectionType != 'Multiple') {
                        var returnValue = $(this).find('div[return-value]').attr('return-value');
                        var returnField = $(this).find('div[return-value]').attr('return-field');
                        var input_obj = ($(self.obj_input).closest("tr").find(".getFields#" + returnField).length >= 1) ? $(self.obj_input).closest("tr").find(".getFields#" + returnField) : $('#' + returnField);
                        // $('#' + returnField).val(returnValue);
                        // $('#' + returnField).change();
                        $(input_obj).val(returnValue);
                        $(input_obj).trigger("change", ["from-click-event"]);

                        //ADDED BY JOSHUA REYES 02/03/2016
                        $('.fl-closeDialog').click();
                        //ADDED BY MICHAEL E. FOR FS#8546 ... 04/06/2016 10 34 PM 
                        $(input_obj).parent().children('.suggestion_container').remove();
                        ui.block();
                        setTimeout(function () {
                            $(input_obj).trigger("change", ["from-click-event"]);
                            ui.unblock();
                        }, 1);
                    }
                });
            } else {
                $('body').on('click.picklistFunctionMulti', '.picklistOkButton', function () {

                    var selectedRequests = $('.request-picklist:checked');
                    var selectedValues = [];
                    var returnValue = '';
                    var returnField = '';
                    $('.request-picklist:checked').each(function () {
                        returnValue = $(this).closest('div').attr('return-value');
                        returnField = $(this).closest('div').attr('return-field');
                        selectedValues.push(returnValue);

                    });
                    var input_obj = ($(self.obj_input).closest("tr").find(".getFields#" + returnField).length >= 1) ? $(self.obj_input).closest("tr").find(".getFields#" + returnField) : $('#' + returnField);
                    // $('#' + returnField).val(selectedValues.join('|'));
                    // $('#' + returnField).change();
                    $(input_obj).val(selectedValues.join('|')).change();
                    $(input_obj).change();

                    // if (selectedRequests.length != 0) {
                    //     $('.fl-closeDialog').click();

                    // }else {

                    //     showNotification({
                    //         message: "Please select atleast one on the picklist.",
                    //         type: "error",
                    //         autoClose: true,
                    //         duration: 3
                    //     });
                    // };

                });
            }
        });

        /*Added carlo medina 04/06/2015*/
        $('body').on('click', '.picklist-add-button', function (e) {

            var sourceFormId = $(self.obj_input).attr('form-id') || "";
            addPickListEntry.call(this, sourceFormId);
            embedViewPicklistItemUi();

        });
        $('body').on('click', '.view-picklist', function (e) {
            e.stopPropagation();

            var sourceFormId = $(self.obj_input).attr('form-id') || $(self.obj_input).closest('.input_position_below').find('[form-id]').attr('form-id') || "";
            console.log("sourcessss", "id" + sourceFormId, $(self.obj_input));
            viewPickListSource.call(this, sourceFormId);
            embedViewPicklistItemUi();

        });

        //comment 10202015 PANDA
        // $('body').on('click', '[click-event="select"]', function (e) {
        //     //var input_obj = $(self.obj_input).closest("tr").find(".getFields")||$('#' + returnField);


        //     var returnValue = $(this).find('div[return-value]').attr('return-value');
        //     var returnField = $(this).find('div[return-value]').attr('return-field');

        //     var input_obj = ($(self.obj_input).closest("tr").find(".getFields#" + returnField).length >= 1) ? $(self.obj_input).closest("tr").find(".getFields#" + returnField) : $('#' + returnField);
        //     //console.log("input",input_obj)
        //     // $('#' + returnField).val(returnValue);
        //     // $('#' + returnField).change();
        //     //inline
        //     $(input_obj).val(returnValue);
        //     $(input_obj).change();
        //     if ($(e.target).closest(".suggestion_container").length > 0) {
        //         $(e.target).closest(".suggestion_container").remove();
        //     }
        //     $('.fl-closeDialog').click();
        // });

        $('body').on('click', '.picklistOkButton', function () {

            var selectedRequests = $('.request-picklist:checked');
            var returnField = $(this).attr('return-field');
            var selectedValues = [];
            // var returnValue = '';
            // var returnField = '';
            // $('.request-picklist:checked').each(function() {
            //     returnValue = $(this).closest('div').attr('return-value');
            //     returnField = $(this).closest('div').attr('return-field');
            //     selectedValues.push(returnValue);
            // });
            if (self.data.selectedValuesRestore != "") {

                var input_obj = ($(self.obj_input).closest("tr").find(".getFields#" + returnField).length >= 1) ? $(self.obj_input).closest("tr").find(".getFields#" + returnField) : $('#' + returnField); //FS#8714 move code inside this condition
                $(input_obj).val(self.data.selectedValuesRestore); //FS#8714 move code inside this condition
                $(input_obj).change(); //FS#8714 move code inside this condition
                //ADDED BY JOSHUA REYES 02/03/2016
                $('.fl-closeDialog').click();
                ui.block();
                setTimeout(function () {
                    $(input_obj).trigger("change", ["from-click-event"]);
                    ui.unblock();
                }, 1);
            } else {

                showNotification({
                    message: "Please select at least one valid item.",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
            }
            ;

            // $('#' + returnField).val(self.data.selectedValuesRestore);
            // $('#' + returnField).change();
            //$('.fl-closeDialog').click();
        });

        // $('.field-container-pick-list input').prop('readOnly', true);
        var self = this;
        //sorting of currency columns
        // I Temporary Comment this first by michael espano 10:01 AM 10/8/2015
        // jQuery.extend(jQuery.fn.dataTableExt.oSort, {
        //     "currency-pre": function (a) {
        //         a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
        //         return parseFloat(a);
        //     },
        //     "currency-asc": function (a, b) {
        //         return a - b;
        //     },
        //     "currency-desc": function (a, b) {
        //         return b - a;
        //     }
        // });

        $('body').on('click', '.request-picklist', function () {
            var checked = $(this).prop('checked');
            var returnValue = $(this).closest('[return-value]').attr('return-value');

            if (checked) {
                if (returnValue) {
                    self.selected.push(returnValue);
                }
            } else {
                remove(self.selected, returnValue);
            }

            // console.log(self.selected);
            self.data.selectedValuesRestore = self.selected.join('|');

//            alert(1);
            $('.selectedPickList').html('<b>Currently Selected:</b> [ ' + self.selected.join(', ') + ' ]');
        });
        //DITO
        $('body').on('click', '[data-type="picklist_selection"]', function () {
            var val = $(this).val();
            var data_source = $("#popup_container").data().clicked_button;
            var keywordCategory = data_source.attr('keyword-category');
            var returnField = data_source.attr('return-field');
            var selectionType = data_source.attr('picklist-type');
            var formId = data_source.attr('form-id');
            var returnFieldName = data_source.attr('return-field-name');
            var returnFormName = data_source.attr('formname');
            var displayColumnsFields = data_source.attr('display_columns');
            var selection_Value_Type = data_source.attr('selection-type');
            var displayColumnSequence = $(this).attr('display_column_sequence');
            var displayColumns = '';
            var dis_data = {
                "keywordCategory": keywordCategory,
                "returnField": returnField,
                "selectionType": selectionType,
                "formId": formId,
                "returnFieldName": returnFieldName,
                "returnFormName": returnFormName,
                "displayColumnsFields": displayColumnsFields,
                "selection_Value_Type": selection_Value_Type,
                "displayColumnSequence": displayColumnSequence
            };

            if (displayColumnsFields != '' && displayColumnsFields) {
                displayColumns = displayColumnsFields.split(',');
                dis_data.displayColumns = displayColumns;
            }



            if (val == 'Forms') {
                $('.form_selection').removeClass('display');
                $('.select_form_picklist').removeClass('display');
                $('.keyword_view').addClass('display');
                $('.keyword_search').addClass('display');


                // self.getForms(function(data) {
                getUserFormsV2(function (data) {
                    // console.log("getUserFormsV2", data)
                    try {
                        data = JSON.parse(data);
                        $('.picklist_form_type').html('<option value="0">----------------------------Select----------------------------</option>');
                        data = data.sort(function (a, b) {
                            return a['form_name'].toLowerCase().localeCompare(b['form_name'].toLowerCase());
                        });
                        for (var index in data) {
                            $('.picklist_form_type').append('<option form-alias="'+data[index].form_alias+'" value="' + data[index].wsID + '" fields="' + data[index].active_fields + '">' + data[index].form_name + '</option>');
                        }

                        // $('.display_columns').html('');
                        // $('.picklist_form_field').html('<option value="0">----------------------------Select----------------------------</option>');
                        restoreVals(dis_data);
                    } catch (errz) {
                        console.error("the json data of picklist forms is not valid search: $('body').on('click', '[data-type='picklist_selection']', function() {");
                    }
                });

                // });

            } else {
                $('.form_selection').addClass('display');
                $('.keyword_view').removeClass('display');
                $('.keyword_search').removeClass('display');
                $('.select_form_picklist').addClass('display');
            }
        });

        $('body').on('change', '.picklist_form_type', function () { //DITO NA KO
            var formid = $(this).val();
            var fields = $(this).find('option:selected').attr('fields');
            //var exisiting = fields.indexOf()
            fields = fields.split(',');
            if ($.type(fields) == "array") {
                fields.push("TrackNo");
                fields.push("Requestor");
                fields.push("Processor");
                fields.push("Status");
                fields.push("DateCreated");
                fields = fields.filter(Boolean);
                if (fields.length >= 1) {
                    fields = fields.sort(function (a, b) {
                        return a.toLowerCase().localeCompare(b.toLowerCase());
                    });
                    fields = $.unique(fields);
                }
            }

            // $('.display_columns').html('');
            $('.picklist_form_field').html('<option value="0">----------------------------Select----------------------------</option>');
            for (var ctr = 0; ctr <= fields.length - 1; ctr++) {
                $('.picklist_form_field').append('<option value="' + fields[ctr] + '">' + fields[ctr] + '</option>');
            }
            $('.picklist_form_field').css("display", "").parent().find('.getFromsLoader ').css('display', 'none');
            // $('.display_columns').append('<input name="chk_displayColumns" type="checkbox" value="TrackNo">TrackNo</input><br/>');
            // 
            //     
            //     $('.display_columns').append('<input name="chk_displayColumns" type="checkbox" value="' + fields[ctr] + '">' + fields[ctr] + '</input><br/>');
            // }

            var columnHeaderTable = '<table class="table_data dataTable picklist-view-columns"><tr><th>Field Label</th><th>Field Name</th></tr>';
            var active_fields = fields;
            //console.log(active_fields)
            for (var ctr = 0; ctr < active_fields.length; ctr++) {
                columnHeaderTable += '<tr class="hovereffect">';
                columnHeaderTable += '<td><input type="text" class="form-text" style="background-color: rgba(255,255,255,0.5) !important;" /></td><td class="tip-title" data-original-title="' + active_fields[ctr].replace(/\|\^\|/g, ",") + '  (Drag up/down)" style="cursor:move"><span style="padding:3px">' + active_fields[ctr].replace(/\|\^\|/g, ",") + '</span></td>';
                columnHeaderTable += '</tr>';
            }

            columnHeaderTable += '</table>';
            var columnHeaderTables = $(columnHeaderTable);

            $('.display_columns').html(columnHeaderTables);
            columnHeaderTables.find('.tip-title').tooltip();
            columnHeaderTables.find("tbody").sortable({
                items: "tr:not(:eq(0))",
                placeholder: {
                    element: function (currentItem) {
                        return $("<tr><td style='width:50%;height:40px;'></td><td style='width:50%;height:40px;'></td></tr>")[0];
                    },
                    update: function (container, p) {
                        return;
                    }
                },
                sort: function (ev, ui) {
                    $(ui["helper"]).children("td").css("width", "181px");
                },
                axis: "y"
            });
            //columnHeaderTables.find('tbody').removeClass('ui-sortable');
            //tooltip ni picklist

            //testing onchange
            picklistSorter();
        });

        $('body').on('click', '.select_form_picklist_multiple', function () {
            var returnFields = $(this).attr('return-field');
            var values = [];

            $('[name="chk_selected_values"]:checked').each(function () {
                values.push($(this).val());
            });

            $('#' + returnFields).val(values.join(';'));
            $('#popup_cancel').click();
        });

        $('body').on('click', '#select_form_picklist', function () {
            if ($('.picklist_selection_type').val() == "1") { // FS#7787
                if ($('[id="popup_container"]').find('.processorLoad').is(":visible")) {
                    showNotification({
                        message: "Some fields are still in process. Please wait...",
                        type: "information",
                        autoClose: true,
                        duration: 3
                    });
                    return true;
                }
            }
            var hidePicklistPagination = $('.hide-picklist-pagination-property').is(':checked');
            var pickListButton = $(this).attr('dialog-button-id');
            var pickListAddEntry = $('.enablePicklistAdd').val();
            var pickListAddValNotList = $('.allow-values-not-in-list').val();
            var showPickListView = $('.show-picklist-view').val();
            var returnField = $(this).attr('return-field');
            var picklist_selection_type = $('.picklist_selection_type').val();
            var formId = $('.picklist_form_type').val();
            var formName = $('.picklist_form_type option:selected').text();
            var formAlias = $('.picklist_form_type option:selected').attr('form-alias');
            var fieldName = $('.picklist_form_field').val();
            var condition = $('#pickist_condition').val();
            var displayColumns = [];
            var savedColumnSequence = [];

            /*external database*/

            var external_database_connection = $("#external_database_connection").val()
            var external_database_connection_table = $("#external_database_connection_table").val()
            var external_database_connection_return_field = $("#external_database_connection_return_field").val()

            /*end external database*/


//            savedColumnSequence.push(formId);

            $('.picklist-view-columns tr:not(:eq(0))').each(function (index) {
                var fieldLabel = $(this).children('td').eq(0).children('input').val();
                var fieldName = $(this).children('td').eq(1).children('span').text();
                if (picklist_selection_type == "1") { // for external
                    if (index >= 0 && fieldLabel != '') {
                        displayColumns.push({
                            FieldLabel: fieldLabel,
                            FieldName: fieldName
                        });
                    }
                } else {
                    if (index >= 0 && fieldLabel != '') {
                        displayColumns.push({
                            FieldLabel: fieldLabel,
                            FieldName: fieldName
                        });
                    }
                }
                savedColumnSequence.push({
                    FieldLabel: fieldLabel,
                    FieldName: fieldName
                });
            });

            $('[picklist-button-id="' + pickListButton + '"]').attr('picklist-type', 'Forms');
            $('[picklist-button-id="' + pickListButton + '"]').attr('picklist_selection_type', picklist_selection_type);
            $('[picklist-button-id="' + pickListButton + '"]').attr('form-id', formId);
            $('[picklist-button-id="' + pickListButton + '"]').attr('return-field-name', fieldName);
            $('[picklist-button-id="' + pickListButton + '"]').attr('formname', formName);
            $('[picklist-button-id="' + pickListButton + '"]').attr('form-alias', formAlias);
            $('[picklist-button-id="' + pickListButton + '"]').attr('display_columns', JSON.stringify(displayColumns));
            $('[picklist-button-id="' + pickListButton + '"]').attr('display_column_sequence', JSON.stringify(savedColumnSequence));
            $('[picklist-button-id="' + pickListButton + '"]').attr('condition', condition);
            $('[picklist-button-id="' + pickListButton + '"]').attr('enable-add-entry', pickListAddEntry);
            $('[picklist-button-id="' + pickListButton + '"]').attr('allow-values-not-in-list', pickListAddValNotList);
            $('[picklist-button-id="' + pickListButton + '"]').attr('show-picklist-view', showPickListView);

            /*checkbox properties*/
            $('[picklist-button-id="' + pickListButton + '"]').attr('hide-picklist-pagination', hidePicklistPagination);            
            /*external Database*/

            $('[picklist-button-id="' + pickListButton + '"]').attr('external_database_connection', external_database_connection);
            $('[picklist-button-id="' + pickListButton + '"]').attr('external_database_connection_table', external_database_connection_table);
            $('[picklist-button-id="' + pickListButton + '"]').attr('external_database_connection_return_field', external_database_connection_return_field);

            /*end external database*/


            if (picklist_selection_type == 0) {
                formNamePlaceHolder = formName;
            } else {
                formNamePlaceHolder = external_database_connection_table;
            }
            if ($('#' + returnField).attr('placeholder')) {
                if ($('#' + returnField).attr('placeholder') == "") {
                    $('#' + returnField).attr('placeholder', 'Please select ' + formNamePlaceHolder);
                }
            } else {
                $('#' + returnField).attr('placeholder', 'Please select ' + formNamePlaceHolder);
            }

            $('#popup_cancel').click();
        });
        var requestAjaxDataPicklist = {};
        $("body").on("change", '[data-type="pickList"] input.getFields', function (e, data) { //ito ung dating keyup modified by michael 3:35 PM 10/2/2015
            if (data == "from-click-event" || data == "SetDefault" || data == "commitChangesDefaultVal" || $.type(data) == "object") { // fixes problems on lumalabas kagad ung drop down
                return true;
            }
            if ($(this).is('[readonly]')) {
                return;
            }
            // if (e.keyCode != 13) {
            //     return false;
            // }
            if ($(this).closest('.setOBJ[data-type="table"]').exists()) {
                $(this).closest('.setOBJ[data-type="table"]').addClass('focused-ele');
            }
            self.obj_input = $(this);
            var this_input = this;
            var search = $.trim($(this).val());
            var pickListButton = $(this).closest(".input_position_below").find(".pickListButton");

            var returnField = pickListButton.attr('return-field');
            var selectionType = pickListButton.attr('picklist-type');
            var condition = pickListButton.attr('condition');
            var formId = pickListButton.attr('form-id');
            var returnFieldName = pickListButton.attr('return-field-name');
            var columnsDisplay = pickListButton.attr('display_columns');
            var valueSelectionType = pickListButton.attr('selection-type');
            var selectedValuesRestore = $('#' + returnField).val();

            var picklist_data = {
                formId: formId,
                search: search,
                condition: self.parseCondition(condition),
                columnsDisplay: columnsDisplay,
                returnField: returnField,
                returnFieldName: returnFieldName,
                valueSelectionType: valueSelectionType,
                selectedValuesRestore: selectedValuesRestore,
                iDisplayLength: 5,
                iDisplayStart: 0,
                action_trigger: "click-event='select'"
            };
            if (search != "") {
                //initialize modal

                suggestionTableModal(this_input, 'field-container-pick-list');
                try {
                    requestAjaxDataPicklist.abort();
                } catch (err) {

                }
                requestAjaxDataPicklist = $.post("/ajax/picklist", picklist_data, function (data) {
                    try {
                        data = JSON.parse(data);

                        suggestionTableData(this_input, data, function () {

                            $('[data-suggestion="' + $(this_input).attr("id") + '"]').find("table tbody tr").attr('click-event', 'select');
                            $('.view-picklist').attr("data-original-title", "View").tooltip();

                            // $('[data-suggestion="'+ $(this_input).attr("id") +'"]').find(".suggestion-data-content").perfectScrollbar();
                        });
                        // $('[data-picklist="'+ $(this_input).attr("id") +'"]').find(".processorLoad").remove();//for the meantime
                    } catch (err) {
                        console.log(err)
                    }
                });
            } else {
                $('[data-suggestion="' + $(this_input).attr("id") + '"]').remove();

            }
        })

        //remove picklist container
        $('body').on('click', '.fl-user-wrapper', function (e) {

            var target = $(e.target);
            if (target.closest(".suggestion_container").length == 0 && target.closest('[data-type="pickList"]').length == 0 && target.closest('[data-type="listNames"]').length == 0) {
                $(".suggestion_container").fadeOut(function () {
                    if ($(this).closest('.setOBJ[data-type="table"]').exists()) {
                        $(this).closest('.setOBJ[data-type="table"]').removeClass('focused-ele');
                    }
                    $(this).remove();
                })
            }

        })

        $('body').on('click', '.pickListButton', function (ev) {


            self.obj_input = $(this);
            if ($(this).is('[disabled="disabled"]')) {
                return true;
            }
            // ui.block();
            var picklistButton = this;
            var location = String(window.location.href);
            var keywordCategory = $(this).attr('keyword-category');
            var enable_add_entry = $(this).attr('enable-add-entry');
            var showPickListView = $(this).attr('show-picklist-view');
            var pickListAddValNotList = $(this).attr('allow-values-not-in-list');
            var returnField = $(this).attr('return-field');
            var selectionType = $(this).attr('picklist-type');
            var condition = $(this).attr('condition');
            var formId = $(this).attr('form-id');
            var picklist_selection_type = $(this).attr('picklist_selection_type');
            var returnFieldName = $(this).attr('return-field-name');
            var returnFormName = $(this).attr('formname');
            var returnFormAlias = $(this).attr('form-alias');


            var picklist_selection_type = $(this).attr('picklist_selection_type');
            if (picklist_selection_type == 1) {
                returnFormName = $(this).attr('external_database_connection_table');
            }
            var displayColumnsFields = $(this).attr('display_columns');
            var displayColumnSequence = $(this).attr('display_column_sequence');
            var selection_Value_Type = $(this).attr('selection-type');
            var displayColumns = '';

            var external_database_connection = $(this).attr("external_database_connection");
            var external_database_connection_table = $(this).attr("external_database_connection_table");
            var external_database_connection_return_field = $(this).attr("external_database_connection_return_field");

            var loading_content = $(
                    '<span class="select-loader-wrapper">' +
                    '<div class="getFromsLoader form-select" style="display:inline-block;position:relative;vertical-align:middle;text-align:center;">' +
                    '<label style="color:black;position:reltive;font-size: 15px;display:inline-block;">Loading...</label>' +
                    '<img src="/images/loader/loader.gif" class="display processorLoad" style="display: inline-block;"/>' +
                    '</div>' +
                    '<span>'
                    );

            var dis_data = {
                "showPickListView": showPickListView,
                "enableAddEntry": enable_add_entry,
                "allowValuesNotinList": pickListAddValNotList,
                "keywordCategory": keywordCategory,
                "returnField": returnField,
                "selectionType": selectionType,
                "picklist_selection_type": picklist_selection_type,
                "formId": formId,
                "returnFieldName": returnFieldName,
                "returnFormName": returnFormName,
                "displayColumnsFields": displayColumnsFields,
                "selection_Value_Type": selection_Value_Type,
                "displayColumnSequence": displayColumnSequence,
                "external_database_connection": external_database_connection,
                "external_database_connection_table": external_database_connection_table,
                "external_database_connection_return_field": external_database_connection_return_field


            };

            if (displayColumnsFields != '' && displayColumnsFields) {
                // displayColumns = displayColumnsFields.split(',');
                //ADDED PANDA 08132014
                try {
                    displayColumnsFieldLabel = JSON.parse(displayColumnsFields);
                } catch (errs) {
                    displayColumnsFieldLabel = [];
                }
                displayColumns = displayColumnsFields.split(',');
                dis_data.displayColumns = displayColumns;
            }


            if (location.indexOf('formbuilder') > -1) {
                ui.block();
                mode = "KeywordDialog";
                var picklistMode = {
                    Mode: mode
                }
            } else {
                mode = "KeywordPicklist";
                var picklistMode = {
                    Mode: mode,
                    Code: keywordCategory
                }
            }

            var columnsDisplay = $(this).attr('display_columns');
            var valueSelectionType = $(this).attr('selection-type');

            var input_obj = ($(self.obj_input).closest("tr").find(".getFields#" + returnField).length >= 1) ? $(self.obj_input).closest("tr").find(".getFields#" + returnField) : $('#' + returnField);
            //var selectedValuesRestore = $('#' + returnField).val();

            var selectedValuesRestore = $(input_obj).val();
            if (selectedValuesRestore != '') {
                console.log('self_pick', self, selectedValuesRestore);
                self.selected = selectedValuesRestore.split('|');
            } else {
                self.selected = [];
            }

            var picklist_selection_type = $(this).attr('picklist_selection_type');

            if (picklist_selection_type != 1) {
                var picklist_data = {
                    picklist_selection_type: picklist_selection_type,
                    formId: formId,
                    search: '',
                    condition: self.parseCondition(condition),
                    columnsDisplay: columnsDisplay,
                    returnField: returnField,
                    returnFieldName: returnFieldName,
                    valueSelectionType: valueSelectionType,
                    selectedValuesRestore: selectedValuesRestore,
                    showPickListView: showPickListView
                };
            } else {
                var external_database_connection_id = $(this).attr('external_database_connection');
                var external_database_connection_table = $(this).attr('external_database_connection_table');
                var external_database_connection_return_field = $(this).attr('external_database_connection_return_field');
                columnsDisplay = $(this).attr('display_columns');
                var picklist_data = {
                    picklist_selection_type: picklist_selection_type,
                    external_database_connection_id: external_database_connection_id,
                    external_database_connection_table: external_database_connection_table,
                    search: '',
                    condition: self.parseCondition(condition),
                    columnsDisplay: columnsDisplay,
                    returnField: returnField,
                    returnFieldName: external_database_connection_return_field,
                    valueSelectionType: valueSelectionType,
                    selectedValuesRestore: selectedValuesRestore,
                    showPickListView: showPickListView
                };
            }


            self.data = picklist_data;

            //console.log('picklist', picklist_data);
            // Roni Pinili close button modal configuration
            if ($('body').data("user_form_json_data")) {

                var picklistDataObjId = $(this).parents('.setOBJ').eq(0).attr('data-object-id');

                if ($('body').data("user_form_json_data")['form_json'][picklistDataObjId]) {
                    var picklistActionVisibilityVal = $('body').data("user_form_json_data")['form_json'][picklistDataObjId].obj_hideModalCloseBtn;
                    console.log("VALUE", picklistActionVisibilityVal);
                    if (picklistActionVisibilityVal == "Yes") {
                        var close_dial = setInterval(function () {
                            $('.fl-closeDialog').hide();
                            if ($('.fl-closeDialog').length >= 1) {
                                clearInterval(close_dial);
                            }
                        }, 0);
                    }
                }

            }


            selectionType = 'Forms';
            if (selectionType == 'Forms') {
                if (mode == 'KeywordPicklist') {
                    //<h3><i class="icon-key"></i><span class="popup-picklist-title">Select ' . $formObject->form_name . '</span></h3>
                    var result = '<h3><i class="icon-key"></i><span class="popup-picklist-title">Select ' + ($.trim(returnFormAlias)||returnFormName) + '</span></h3><div class="hr"></div><div></div>';
                    result += "<div class='fl-list-of-app-record-wrapper'>";
                    /*  if (valueSelectionType == 'Multiple') {
                     result += '<div class="fl-search-wrapper selectedPickList" style="margin-top:10px;margin-bottom:10px;width:100%;font-style:italic"><b>Currently Selected:</b> [ ' + self.selected.join(', ') + ' ]</div>';
                     }*/

                    result += '<div class="fl-option-toolbar">';
                    result += '<div class="fl-toolbar-left-wrapper" style="position:relative;">';
                    /*if(enable_add_entry == 'Yes'){
                     result += '<ul class="fl-standard-btn">';
                     result += '<li class="fl-buttonEffect" return-field="' + returnField + '"><a class="primary-btn  picklist-add-button">Add Entry</a></li>';
                     //result += '<input type="button"  class="btn-blueBtnfl-positive-btn fl-buttonEffect" object_type="workflow_chart-fields-trigger" id="" value="Add Entry" node-data-id="node_2" style="">';
                     result += '</ul>';
                     }*/
                    result += '<div class="selectedPickList" style="position: absolute; top: 0px; left: 0px; width: 590px;   height: 31px;; overflow-y: auto; white-space:nowrap;">';
                    if (valueSelectionType == 'Multiple') {
                        // result +=  '<div class="selectedPickList"><b>Currently Selected:</b> [ ' + self.selected.join(', ') + ' ]</div>';
                        result += '<b>Currently Selected:</b> [ ' + self.selected.join(', ') + ' ]';
                    }
                    result += '</div>';

                    result += '</div>';
                    result += '<div class="fl-toolbar-right-wrapper"><div class="fl-search-wrapper"><input type="text" placeholder="Search" class="searchPicklist"><input type="button" value="" class="search search-picklist-button icon fl-search-icon-list"/></div></div></div>';
                    result += "<div class='fl-datatable-wrapper'>";


                    result += "<table id='picklistData' class='display_data dataTable recordsDatatable picklistData' style='cursor:pointer'><thead class='fl-header-tbl-wrapper'>";
                    result += '<tr>';

                    columnsDisplayArr = JSON.parse(columnsDisplay);
                    console.log('columnsDisplayArr', columnsDisplayArr)

                    if (valueSelectionType == 'Multiple' || showPickListView == "Yes") {
                        result += "<th data-type='' not_sortable='true' style = 'width:50px; background:none;'><div class='fl-table-ellip'></div></th>";
                    }
                    for (var index in columnsDisplayArr) {
                        result += "<th data-type='' style = 'width:150px;'><div class='fl-table-ellip'>" + columnsDisplayArr[index].FieldLabel + "</div></th>"
                    }
                    result += "</tr>";
                    result += "</table></div><div class='dataTable_widget'></div>";

                    if (valueSelectionType == 'Multiple') {
                        result += '<input type="button" return-field="' + returnField + '" class="btn-blueBtn picklistOkButton fl-margin-right fl-margin-bottom fl-positive-btn fl-buttonEffect" object_type="workflow_chart-fields-trigger" id="" value="OK" node-data-id="node_2" style="">';


                    }
                    if (enable_add_entry == 'Yes') {
                        result += '<input type="button"  class="btn-blueBtn fl-positive-btn fl-buttonEffect picklist-add-button" object_type="workflow_chart-fields-trigger" id="" value="Add Entry" node-data-id="node_2" style="">';
                    }

                    ui.unblock();

                    if (window.self !== window.top) {
                        if ($('.ui-portal-body').length >= 1) {
                            var saveDialog = new jDialog(result, "", "430px", "300", "", function () {

                            });
                        } else {
                            var saveDialog = new jDialog(result, "", "900", "", "", function () {

                            });
                        }

                    } else {
                        var saveDialog = new jDialog(result, "", "900", "", "", function () {

                        });
                    }


                    console.log($(this));


                    saveDialog.themeDialog('modal2', {
                    });
                    if($(this).is('[hide-picklist-pagination="true"]')){
                        $('[id="popup_content"]').addClass("picklist-hide-pagination");
                    }else{
                        $('[id="popup_content"]').removeClass("picklist-hide-pagination");
                    }
                    $('.content-dialog-scroll').addClass('forPicklistData').css({'height': '390px', 'max-height': 'none'});
                    self.generateDataTable(self.data, function () {
                        $('.ps-container').perfectScrollbar('update');
                        $('.view-picklist').attr("data-original-title", "View").tooltip();
                    });


                    $('.search-picklist-button').on('click', function () {
                        var e = $.Event("keyup");
                        e.which = 13; // # Some key code value
                        $(".searchPicklist").trigger(e);
                    });
                    $('.searchPicklist').on('keyup', function (e) {

                        var keycode = e.keyCode || e.which;
                        if (keycode === 13) {
                            self.data.search = $.trim($(this).val());
                            self.generateDataTable(self.data, function () {
                                if ($('.picklist-request-id-storage').exists()) {
                                    $('.searchPicklist').val("");
                                    var picklist_table = $('#popup_content').find('table.recordsDatatable').children('tbody:eq(0)');

                                    if (picklist_table.find('[selection-type="Multiple"]').exists()) {
                                        picklist_table.find('tr:eq(0)').find('.request-picklist:eq(0)').trigger('click');
                                        var e = $.Event("keyup");
                                        e.which = 13;
                                        $('.searchPicklist').trigger(e);
                                    }
                                    else {
                                        picklist_table.find('[click-event="select"]:eq(0)').trigger('click');
                                    }

                                    $('.picklist-request-id-storage').remove();
                                }

                            });
                        }

                    });



                    if (valueSelectionType != 'Multiple') {
                        $('body').off("click.picklistFunction");

                        $('body').on('click.picklistFunction', '[click-event="select"]', function () {
                            if (valueSelectionType != 'Multiple') {
                                var returnValue = $(this).find('div[return-value]').attr('return-value');
                                var returnField = $(this).find('div[return-value]').attr('return-field');
                                var input_obj = ($(self.obj_input).closest("tr").find(".getFields#" + returnField).length >= 1) ? $(self.obj_input).closest("tr").find(".getFields#" + returnField) : $('#' + returnField);
                                // $('#' + returnField).val(returnValue);
                                // $('#' + returnField).change();
                                $(input_obj).val(returnValue);
                                $(input_obj).trigger("change", ["from-click-event"]);
                                //ADDED BY JOSHUA REYES 02/03/2016
                                $('.fl-closeDialog').click();
                                //ADDED BY MICHAEL E. FOR FS#8546 ... 04/06/2016 10 34 PM 
                                $(input_obj).parent().children('.suggestion_container').remove();
                                ui.block();
                                setTimeout(function () {
                                    $(input_obj).trigger("change", ["from-click-event"]);
                                    ui.unblock();
                                }, 1);
                            }
                        });
                    } else {
                        $('body').off("click.picklistFunctionMulti");
                        $('body').on('click.picklistFunctionMulti', '.picklistOkButton', function () {

                            var selectedRequests = $('.request-picklist:checked');
                            var selectedValues = [];
                            var returnValue = '';
                            var returnField = '';
                            $('.request-picklist:checked').each(function () {
                                returnValue = $(this).closest('div').attr('return-value');
                                returnField = $(this).closest('div').attr('return-field');
                                selectedValues.push(returnValue);

                            });
                            var input_obj = ($(self.obj_input).closest("tr").find(".getFields#" + returnField).length >= 1) ? $(self.obj_input).closest("tr").find(".getFields#" + returnField) : $('#' + returnField);
                            // $('#' + returnField).val(selectedValues.join('|'));
                            // $('#' + returnField).change();
                            $(input_obj).val(selectedValues.join('|')).change();
                            $(input_obj).change();

                            // if (selectedRequests.length != 0) {
                            //     $('.fl-closeDialog').click();

                            // }else {

                            //     showNotification({
                            //         message: "Please select atleast one on the picklist.",
                            //         type: "error",
                            //         autoClose: true,
                            //         duration: 3
                            //     });
                            // };

                        });
                    }


                } else {
                    self.showKeywordDialog(picklistMode, function (data) {
                        result = JSON.parse(data);
                        var ret = '<h3><i class="icon-key"></i> Picklist</h3>';
                        ret += '<div class="hr"></div>';
                        ret += '<div></div>';
                        ret += '<div class="fields selection isDisplayNone"><div class="label_below2"> Selection:</div><div class="input_position"><input type="radio" name="workspace_title" checked="checked" data-type="picklist_selection" style="margin-top:5px;margin-right:5px" value="Forms" class="css-checkbox"  id="picklist_Form"/><label for="picklist_Form" class="css-label"></label> Forms <input type="radio" name="workspace_title" data-type="picklist_selection" style="margin-top:5px;margin-right:5px" value="Keywords" class="css-checkbox" id="picklist_Keyword" />';
                        //ret += '<label for="picklist_Keyword" class="css-label"></label> Keywords';

                        ret += '</div></div>';

                        ret += '<div class="display keyword_search" style="margin-top:7px;"><input type="text" class="form-text input-medium search-keyword" placeholder="Search" row-return-field="' + returnField + '" dialog-button-id="' + $(picklistButton).attr('picklist-button-id') + '" keyword-category = "' + keywordCategory + '"></div>';
                        ret += '<div style="overflow:auto;height:200px" class="display keyword_view">';
                        ret += '<table  class="table_data display_data dataTable keywordTable">';
                        ret += '<tr><th>Code</th> <th>Description</th></tr>';

                        for (var index in result) {
                            ret += '<tr style="cursor:pointer" row-return-field="' + returnField + '" data="' + result[index].Code + ' - ' + result[index].Description + '" mode="' + picklistMode.Mode + '" dialog-button-id="' + $(picklistButton).attr('picklist-button-id') + '" code="' + result[index].Code + '"class="keyword-category hovereffect">';
                            if (mode == 'KeywordPicklist' && selection_Value_Type == 'Multiple') {
                                ret += '<td><label><input type="checkbox" name="chk_selected_values" value="' + result[index].Code + ' - ' + result[index].Description + '">' + result[index].Code + '</input></label></td>';
                            } else {
                                ret += '<td>' + result[index].Code + '</td>';
                            }
                            ret += '<td>' + result[index].Description + '</td>';
                            ret += '</tr>';

                        }
                        ret += '</table>';
                        ret += '</div>';
                        ret += '<div class="fields form_selection">';
                        //ret += '<div class="label_below2">Form:</div>';
                        ret += '<div class="input_position section clearing fl-field-style"><div class="column div_1_of_1"><span>Selection Type:</span><select class="form-select picklist_selection_type" name="picklist_selection_type" id="picklist_selection_type" style="margin-top:5px;"><option value="0">Forms</option>';
                        if ($("#go_lite").val() == 1) {
                            ret += '<option value="1">External Database</option>';
                        }
                        ret += '<select></div></div>';
                        ret += '</div>';

                        /*START FOR EXTERNAL database*/
                        var externalConnection = $("#external-connections-list").text();
                        externalConnection = JSON.parse(externalConnection);
                        ret += '<div class="trigger-container-fields">';

                        ret += '<div class="fields form_selection">';
                        //ret += '<div class="label_below2">Form:</div>';
                        ret += '<div class="input_position section clearing fl-field-style external_database_connection_container" id="">';
                        ret += '<div class="column div_1_of_1">';
                        ret += '<span>Database Connection:</span>';
                        ret += '<select class="form-select external_database_connection" name="external_database_connection" id="external_database_connection" style="margin-top:5px;">';
                        ret += '<option value="0">----------------------------Select----------------------------</option>';
                        for (i in externalConnection) {
                            // console.log(form_fields)
                            ret += '<option value="' + externalConnection[i]['id'] + '" >' + externalConnection[i]['connection_name'] + '</option>'; //setSelected(forms[i]['form_id'], data['workflow-form-trigger'])
                        }
                        ret += '<select>';
                        ret += '</div>';
                        ret += '</div>';
                        ret += '</div>';

                        ret += '<div class="fields form_selection">';
                        ret += '<div class="input_position section clearing fl-field-style external_database_connection_container" id="">';
                        ret += '<div class="column div_1_of_1">';
                        ret += '<span>Table Name:</span>';
                        ret += '<select class="form-select workflow-form-trigger external_database_connection_table" name="external_database_connection_table" id="external_database_connection_table" style="margin-top:5px;">';
                        ret += '<option value="0">----------------------------Select----------------------------</option>';
                        ret += '<select>';
                        ret += '<img src="/images/loader/loading.gif" class="pull-left display replDisp processorLoad"/>';
                        ret += '</div>';
                        ret += '</div>';
                        ret += '</div>';

                        ret += '<div class="fields form_selection">';
                        ret += '<div class="input_position section clearing fl-field-style external_database_connection_container" id="">';
                        ret += '<div class="column div_1_of_1">';
                        ret += '<span>Return Field:</span>';
                        ret += '<select class="form-select workflow-reference-fields-update external_database_connection_return_field" name="external_database_connection_return_field" id="external_database_connection_return_field" style="margin-top:5px;">';
                        ret += '<option value="0">----------------------------Select----------------------------</option>';
                        ret += '<select>';
                        ret += '<img src="/images/loader/loading.gif" class="pull-left display replDisp processorLoad"/>';
                        ret += '</div>';
                        ret += '</div>';
                        ret += '</div>';

                        ret += '</div>';
                        /* END FOR EXTERNAL database*/

                        ret += '<div class="fields form_selection">';
                        //ret += '<div class="label_below2">Form:</div>';
                        ret += '<div class="input_position section clearing fl-field-style"><div class="column div_1_of_1"><span>Form:</span>' + loading_content.html() + '<select style="display:none;" class="form-select picklist_form_type" name="picklist_form_type" id="picklist_form_type" style="margin-top:5px;"><option value="0">----------------------------Select----------------------------</option><select></div></div>';
                        ret += '</div>';
                        ret += '<div class="fields form_selection">';
                        //ret += '<div class="label_below2">Return Field:</div>';
                        ret += '<div class="input_position section clearing fl-field-style""><div class="column div_1_of_1"><span>Return Field:</span>' + loading_content.html() + '<select   class="form-select picklist_form_field" name="picklist_form_field" id="picklist_form_field" style="margin-top:5px;"><option value="0">----------------------------Select----------------------------</option><select></div></div>';
                        ret += '</div>';
                        ret += obj_enablePicklistAdd();
                        ret += obj_allow_values_not_in_list();
                        ret += obj_show_view();
                        ret += picklist_other_properties();
                        ret += '<div class="fields form_selection">';
                        ///ret += '<div class="label_below2">Display Columns:</br><span style="font-size:9px;">*Fill-up the field label of the fields you would like to appear in the column header.</br>*Drag the field name up or down to rearrange.</span></div>';
                        ret += '<div class="input_position section clearing fl-field-style display_columns" style="overflow:auto;padding:5px;border:1px Solid #ccc;height:200px;"><div class="column div_1_of_1"><span>Display Columns:</br><span style="font-size:9px;">*Fill-up the field label of the fields you would like to appear in the column header.</br>*Drag the field name up or down to rearrange.</span></span></div></div>';
                        ret += '</div>';
                        ret += '<div class="fields form_selection">';
                        //ret += '<div class="label_below2">Condition:</div>';
                        ret += '<div class="input_position section clearing fl-field-style"> ' +
                                '<div class="column div_1_of_1">' +
                                '<span>Condition:</span>' +
                                '<textarea id="pickist_condition" class="form-textarea pickist_condition" data-ide-properties-type="picklist-condition" style="resize: none;"></textarea>' +
                                '</div>';
                        '</div>';
                        ret += '</div>';

                        ret += '<div class="fields"><div class="label_below2"></div>';

                        if (mode == 'KeywordPicklist' && selection_Value_Type == 'Multiple') {
                            ret += '<div class="input_position" style="margin-top:5px;text-align:right;"><input type="button" class="btn-blueBtn fl-margin-right select_form_picklist_multiple fl-positive-btn" return-field="' + returnField + '" dialog-button-id="" id="select_form_picklist_multiple" value="OK"><input type="button" style="margin-bottom:10px;" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div>';
                        } else if (mode == 'KeywordPicklist' && selection_Value_Type != 'Multiple') {
                            ret += '<div class="input_position" style="margin-top:5px;text-align:right;"><input type="button" style="margin-bottom:10px;" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div>';
                        } else {
                            ret += '<div class="input_position" style="margin-top:5px;text-align:right;"><img src="/images/loader/load.gif" class="display saveFormLoad" style="margin-right:5px;margin-top:2px;"><input type="button" class="btn-blueBtn fl-margin-right fl-positive-btn select_form_picklist " qwerty return-field="' + returnField + '" dialog-button-id="' + $(picklistButton).attr('picklist-button-id') + '" id="select_form_picklist" value="OK"><input type="button" style="margin-left:5px;" class="btn-basicBtn" id="popup_cancel" value="Cancel"></div>';
                        }

                        ret += '</div>';


                        ui.unblock();

                        var newDialog = new jDialog(ret, "", "", "", "70", function () {
                        });
                        newDialog.themeDialog('modal2');
                        if(self.obj_input.is('[hide-picklist-pagination="true"]')){
                            $('.hide-picklist-pagination-property').prop('checked',true);
                        }
                        PicklistSelectionType.selectionTypeFn(picklist_selection_type);

                        //set External Connection Value

                        PicklistSelectionType.setExternalConnection(dis_data);

                        // self.getForms(function(data) {
                        getUserFormsV2(function (data) {

                            try {
                                data = JSON.parse(data);
                                $('.picklist_form_type').html('<option value="0">----------------------------Select----------------------------</option>');
                                data = data.sort(function (a, b) {
                                    return a['form_name'].toLowerCase().localeCompare(b['form_name'].toLowerCase());
                                });
                                for (var index in data) {
                                    $('.picklist_form_type').append('<option form-alias="'+data[index].form_alias+'" value="' + data[index].wsID + '" fields="' + data[index].active_fields + '">' + data[index].form_name + '</option>');
                                }
                                $('.picklist_form_type').parent().find('.getFromsLoader').eq(0).fadeOut(200, function () {
                                    $('.picklist_form_type').fadeIn();
                                });
                                $("#popup_container").data({
                                    "clicked_button": $(picklistButton)
                                });
                                restoreVals(dis_data, "picklistButton");
                                $('.picklist_form_field').parent().find('.getFromsLoader').eq(0).fadeOut(200, function () {
                                    $('.picklist_form_field').fadeIn();
                                });
                            } catch (er) {
                                console.error("the json on picklist_form_type is not valid search: self.showKeywordDialog(picklistMode, function(data) {");
                            }
                        });

                        // });


                        if (mode == "KeywordPicklist") {
                            $('.keyword_search').removeClass('display');
                            $('.keyword_view').removeClass('display');
                            $('.selection').addClass('display');
                            $('.form_selection').addClass('display');
                        }


                    });
                }
            } else {
                showNotification({
                    message: "Picklist was not set up properly",
                    type: "warning",
                    autoClose: true,
                    duration: 3
                })
                ui.unblock();

            }

            if($('.picklist-hide-pagination').length >= 1){
                $('.picklist-hide-pagination').data('clickedPicklist',$(this));
            }
        });
        function restoreVals(data, eventType) {//RESTORE VALUES
            console.log(":S:", data, eventType, this, ":E:")
            if (eventType == "picklistButton") {
                $('[name="workspace_title"][value="' + data.selectionType + '"]').trigger("click");
            }

            $(".picklist_selection_type option[value='" + data.picklist_selection_type + "']").prop("selected", true);
            $(".picklist_form_type option[value='" + data.formId + "']:contains('" + data.returnFormName + "')").prop("selected", true);
            if (data.picklist_selection_type != "1") {
                $(".picklist_form_type option[value='" + data.formId + "']:contains('" + data.returnFormName + "')").change();
            }
            $(".picklist_form_field").children('[value="' + data.returnFieldName + '"]').prop("selected", true);
            $(".enablePicklistAdd").children('[value="' + data.enableAddEntry + '"]').prop("selected", true);
            $(".show-picklist-view").children('[value="' + data.showPickListView + '"]').prop("selected", true);
            $('.allow-values-not-in-list').children('[value="' + data.allowValuesNotinList + '"]').prop("selected", true);
            $(".picklist_form_field").change();
            // $(".form_selection .display_columns").children("[name='chk_displayColumns']").each(function(eqi) {
            //     for (var ii = 0; ii < data.displayColumns.length; ii++) {
            //         if ($(this).val() == data.displayColumns[ii]) {
            //             $(this).prop("checked", true);
            //         }
            //     }
            // });
            var display_columns_field = null;
            try {
                display_columns_field = JSON.parse(data["displayColumnsFields"]);
            } catch (e) {

            }
            if ($('#popup_container').data()) {
                if ($('#popup_container').data()['clicked_button']) {
                    $('.pickist_condition').val($($('#popup_container').data()['clicked_button']).attr('condition'))
                }
            }

            if ($.type(display_columns_field) == "array") {
                $(".form_selection .display_columns .picklist-view-columns tbody").children('tr:not(:eq(0))').each(function (eqi) {
                    var self_dis = $(this)
                    var input_label_ele = self_dis.find('input').eq(0);
                    if (
                            display_columns_field.filter(function (value, indeks) {
                                if (value['FieldName']) {
                                    return self_dis.find('span:contains("' + value['FieldName'] + '")').length >= 1;
                                }
                                return false;
                            }).length >= 1
                            ) {
                        input_label_ele.val(
                                display_columns_field.filter(function (value, indeks) {
                                    if (value['FieldName']) {
                                        return self_dis.find('span:contains("' + value['FieldName'] + '")').length >= 1;
                                    }
                                    return false;
                                })[0]['FieldLabel']
                                );
                    }

                });
            }
            //Carlo Medina display
            picklistSorter();


            //$(".form_selection . display_columns .picklist-view-columns tbody").


            ////restore columns sorted sequence 
            //if(data["displayColumnSequence"]){
            //    var json_displayColumnSequence = JSON.parse(data["displayColumnSequence"]);
            //    var popup_container = $('#popup_container');
            //    var form_id_selected = json_displayColumnSequence[0];
            //    var column_sequence = json_displayColumnSequence.filter(function(value_ele,ctr_index){
            //        return ctr_index != 0;
            //    });
            //    var display_columns_ele_contain = popup_container.find(".form_selection .display_columns");
            //
            //    if(popup_container.find(".picklist_form_type[name='picklist_form_type']").val() == form_id_selected){
            //        var from_element = null;  //put your ids here
            //        var to_element = null;
            //        var from_element_content = null;  //put your ids here
            //        var to_element_content = null;
            //        for (var ctr_index in column_sequence){
            //            from_element = display_columns_ele_contain.find("span:contains('"+column_sequence[ctr_index]["FieldName"]+"')").parents("tr").eq(0);
            //            to_element = display_columns_ele_contain.find("tr").filter(function (val,ind){ return ind != 0;}).eq(ctr_index);
            //
            //            from_element_content = from_element.clone();
            //            to_element_content = to_element.clone();
            //            
            //            from_element.replaceWith(to_element_content);
            //            to_element.replaceWith(from_element_content);
            //        }
            //    }
            //}
        }



        $('body').on('dblclick', '.keyword-category', function () {
            var picklistID = $(this).attr('dialog-button-id');
            var code = $(this).attr('code');
            var mode = $(this).attr('mode');
            var returnField = $(this).attr('row-return-field');
            var data = $(this).attr('data');
            if (mode === 'KeywordDialog') {
                $('[picklist-button-id="' + picklistID + '"]').attr('picklist-type', 'Keywords');
                $('[picklist-button-id="' + picklistID + '"]').attr('keyword-category', code);
                if ($('#' + returnField).attr('placeholder')) {
                    if ($('#' + returnField).attr('placeholder') == "") {
                        $('#' + returnField).attr('placeholder', data);
                    }
                } else {
                    $('#' + returnField).attr('placeholder', data);
                }
            } else {
                $('#' + returnField).attr('code', code);
                $('#' + returnField).val(data);
                $('#' + returnField).change();
            }
            $('#popup_cancel').click();
        });

        $('body').on('keyup', '.request_search', function (event, data) {
            if (event.keyCode == 13 || data == 13) {
                var returnField = $(this).attr('row-return-field');
                var buttonId = $(this).attr('dialog-button-id');
                var returnFieldName = $(this).attr('field-name');
                var searchValue = $(this).val();
                var formId = $(this).attr('formId');
                var condition = $('[picklist-button-id="' + buttonId + '"]').attr('condition');
                var displayColumnsFields = $(this).attr('display-columns');

                // console.log("displayColumnsFields",displayColumnsFields)

                var displayColumns = '';
                if (displayColumnsFields != '' && displayColumnsFields) {
                    // displayColumns = displayColumnsFields.split(',');
                    displayColumns = JSON.parse(displayColumnsFields);
                }

                var picklist_data = {
                    formId: formId,
                    search: searchValue,
                    condition: self.parseCondition(condition)
                };


                console.log('picklist', picklist_data);
                self.getRequests(picklist_data, function (result) {
                    // console.log("RESULTA",result)
                    var exemptions = ['0', '1', '2', 'request_id', 'first_name', 'last_name', 'middle_name',
                        'form_status', 'requestor_id', 'processors_id', 'processor_lastname'];

                    var ret = '<tr>';

                    for (var key in result[0]) {
                        if (
                                exemptions.indexOf(key) == -1
                                &&
                                displayColumns.filter(function (value, index_arr) {
                                    return value['FieldName'] == key;
                                }).length == 1
                                ) {
                            ret += '<th>' + displayColumns.filter(function (value, index_arr) {
                                return value['FieldName'] == key;
                            })[0]['FieldLabel'] + '</th>';
                            // ret += '<th>' + key + '</th>';
                        }
                        // if (exemptions.indexOf(key) == -1 && displayColumns.indexOf(key) >= 0) {
                        // ret += '<th>' + key + '</th>';
                        // }
                    }
                    ret += '</tr>';


                    // console.log("displayColumns",displayColumns)

                    for (var index in result) {
                        ret += '<tr style="cursor:pointer" row-return-field="' + returnField + '" data="' + result[index][returnFieldName] + '"  dialog-button-id="' + buttonId + '" class="keyword-category hovereffect">';
                        for (var key in result[index]) {
                            // console.log("TRIPPING OBER",displayColumns.filter(function(value,index_arr){return value['FieldName'] == key;}))
                            if (
                                    exemptions.indexOf(key) == -1
                                    &&
                                    displayColumns.filter(function (value, index_arr) {
                                        return value['FieldName'] == key;
                                    }).length == 1
                                    ) {
                                // console.log("DEFINE")
                                if (result[index][key] == null) {
                                    ret += '<td></td>';
                                } else {
                                    ret += '<td>' + result[index][key].replace(/\|\^\|/g, ",") + '</td>';
                                }
                            }
                            // if (exemptions.indexOf(key) == -1 && displayColumns.indexOf(key) >= 0) {
                            //     ret += '<td>' + result[index][key] + '</td>';
                            // }
                        }
                        ret += '</tr>';
                    }

                    $('.requestTable').html(ret);
                });
            }
        });
        $('body').on('keyup', '.search-keyword', function (event) {
            if (event.keyCode == 13) {
                var search = $(this).val();
                var location = String(window.location.href);
                var returnField = $(this).attr('row-return-field');
                var buttonId = $(this).attr('dialog-button-id');
                var keywordCategory = $(this).attr('keyword-category');

                if (location.indexOf('formbuilder') > -1) {
                    mode = "KeywordDialog";
                    var picklistMode = {
                        Mode: mode,
                        Search: search
                    }
                } else {
                    mode = "KeywordPicklist";
                    var picklistMode = {
                        Mode: mode,
                        Code: keywordCategory,
                        Search: search
                    }
                }
                self.showKeywordDialog(picklistMode, function (data) {
                    result = JSON.parse(data);

                    var ret = '';
                    ret += '<tr><th>Code</th> <th>Description</th></tr>';

                    for (var index in result) {
                        ret += '<tr style="cursor:pointer" row-return-field="' + returnField + ' " data="' + result[index].Code + ' - ' + result[index].Description + '" mode="' + picklistMode.Mode + '" dialog-button-id="' + buttonId + '" code="' + result[index].Code + '"class="keyword-category hovereffect">';
                        ret += '<td>' + result[index].Code + '</td>';
                        ret += '<td>' + result[index].Description + '</td>';
                        ret += '</tr>';
                    }
                    ret += '</table>';

                    $('.keywordTable').html(ret);
                });
            }
        });
        //FS#8055 - tes 03-30-2016
        FieldSelectionClear($('[allow-values-not-in-list="No"],[allow-values-not-in-list="Yes"]'), "picklist"); //FS#8718 - added allow [allow-values-not-in-list="Yes"]
    },
    showKeywordDialog: function (picklistMode, callback) {
        $.get('/ajax/keywords_dialog',
                picklistMode
                , function (data) {
                    callback(data);
                });
    },
    generateDataTable: function (arg, callback) {
        var paginationDisplayLength = 6; //default 6
        var oColReorder = {
            allowReorder: false,
            allowResize: true,
        };
        var firstColumn = $('#picklistData thead tr th').eq(0).attr("not_sortable");

        var notSortableColumn = [];

        if (firstColumn == "true") {
            notSortableColumn = [
                {"bSortable": false, "aTargets": [0], "cursor": "auto"}
            ];
        }

        if($('.picklist-hide-pagination').length >= 1){
            paginationDisplayLength = 1000000;
        }

        var returnTable = $('#picklistData').dataTable({
            "sDom": 'Rlfrtip',
            "oLanguage": {
                "sProcessing": ''
            },
            // "bAutoWidth": false,
            "bDestroy": true,
            "oColReorder": oColReorder,
            "bLengthChange": false,
            "bFilter": false,
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": paginationDisplayLength,
            "sPaginationType": "full_numbers",
            "aoColumnDefs": notSortableColumn,
            // "bSort": true,
            "sAjaxSource": "/ajax/picklist",
            "fnServerData": function (sSource, aoData, fnCallback) {

                if (arg.picklist_selection_type != 1) {
                    aoData.push({"name": "picklist_selection_type", "value": arg.picklist_selection_type});
                    aoData.push({"name": "formId", "value": arg.formId});
                    aoData.push({"name": "search", "value": arg.search});
                    aoData.push({"name": "columnsDisplay", "value": arg.columnsDisplay});
                    aoData.push({"name": "returnField", "value": arg.returnField});
                    aoData.push({"name": "returnFieldName", "value": arg.returnFieldName});
                    aoData.push({"name": "valueSelectionType", "value": arg.valueSelectionType});
                    aoData.push({"name": "selectedValuesRestore", "value": arg.selectedValuesRestore});
                    aoData.push({"name": "condition", "value": arg.condition});
                    aoData.push({"name": "showPickListView", "value": arg.showPickListView});
                } else {
                    //external database
                    aoData.push({"name": "picklist_selection_type", "value": arg.picklist_selection_type});
                    aoData.push({"name": "external_database_connection_id", "value": arg.external_database_connection_id});
                    aoData.push({"name": "external_database_connection_table", "value": arg.external_database_connection_table});
                    aoData.push({"name": "search", "value": arg.search});
                    aoData.push({"name": "columnsDisplay", "value": arg.columnsDisplay});
                    aoData.push({"name": "returnField", "value": arg.returnField});
                    aoData.push({"name": "returnFieldName", "value": arg.returnFieldName});
                    aoData.push({"name": "valueSelectionType", "value": arg.valueSelectionType});
                    aoData.push({"name": "selectedValuesRestore", "value": arg.selectedValuesRestore});
                    aoData.push({"name": "condition", "value": arg.condition});
                    aoData.push({"name": "showPickListView", "value": arg.showPickListView});
                }



                //external database
                aoData.push({"name": "showPickListView", "value": arg.showPickListView});
                ui.blockPortion($('.dialog'));

                $.ajax({
                    'cache': true,
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": function (data) {

                        console.log("PL_data", data)
//                                 return;
                        // $(obj).find("tbody tr td:not(td:first-child)").css({
                        //     "cursor":"pointer"
                        // });
                        fnCallback(data);
                    }
                });
            },
            "fnInitComplete": function (oSettings, json) {
                var obj = $('#picklistData');
                var listOfappContainer = $('.fl-list-of-app-record-wrapper');
                dataTable_widget(obj, listOfappContainer);
                callback(true);
            },
            fnDrawCallback: function () {
                $(".tooltip").remove();
                $('.view-picklist').attr("data-original-title", "View").tooltip();
                ui.unblockPortion($('.dialog'));
            },
            "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                if (arg.valueSelectionType != 'Multiple') {
                    $(nRow).attr('click-event', 'select');
                }

            }
        });

        // new $.fn.dataTable.FixedHeader( returnTable );


    },
    getForms: function (callback) {
        $.post('/ajax/search', {
            action: 'getForm'
        }, function (data) {

            data = JSON.parse(data);
            callback(data);
        });
    },
    getRequests: function (selection, callback) {
        // $.post('/ajax/search', {
        //     action: 'getRequest',
        //     id: formId,
        //     search_value: search_value,
        //     field: '0',
        //     start: 0
        // }, function(data) {
        //     data = JSON.parse(data);

        //     callback(data);
        // });

        $.get('/ajax/picklist', selection, function (data) {
            data = JSON.parse(data);

            console.log('picklist data', data);
            if (typeof callback == "function") {
                callback(data);
            }

        });
    },
    parseCondition: function (string_condition) {
        var temp_val = null;
        var reg_eks_temp = null;
        var temp_string = "";

        var condition_formula = string_condition;
        if (!condition_formula) {
            return;
        }
        var condition_formula_temp = condition_formula;
        condition_formula_temp = condition_formula_temp.replace(/(\/\/.*\n|\/\/.*$|\/\/.*\r\n|\/\/.*\r)/g, ""); //pangtanggal ng mga comment
        var get_reg_eks_fns = new RegExp("@CurrentForm\\\[[\\\'\\\"][a-zA-Z][a-zA-Z0-9_]*[\\\'\\\"]\\\]", "g");
        var matches_fns = condition_formula.match(get_reg_eks_fns);
        var get_reg_eks_IN_multi = new RegExp("@CurrentForm\\\[[\\\'\\\"][a-zA-Z][a-zA-Z0-9_]*[\\\'\\\"]\\\]\\s*==|==\\s*@CurrentForm\\\[[\\\'\\\"][a-zA-Z][a-zA-Z0-9_]*[\\\'\\\"]\\\]", "g");
        // var matches_IN_multi = condition_formula.match(get_reg_eks_IN_multi);

        //MULTI VALUE added September 29 2016
        condition_formula.replace(get_reg_eks_IN_multi , function(val){
            reg_eks_temp = new RegExp("\[[\'\"][a-zA-Z][a-zA-Z0-9_]*[\'\"]\]", "g");
            if( $('[name="'+val.replace(reg_eks_temp, '')+'"]').parent().is('.field-container-pick-list') ){
                if( $('[name="'+val.replace(reg_eks_temp, '')+'"]').next().children().eq(0).is('[selection-type="Multiple"]') ){
                    val = val.replace('==','IN');
                }
            }
            return val;
        });
        if ($.isArray(matches_fns)) {
            matches_fns = matches_fns.filter(Boolean);
            if (matches_fns.length >= 1) {
                for (var ctr_ikey in matches_fns) {
                    temp_string = matches_fns[ctr_ikey];
                    reg_eks_temp = new RegExp("\[[\'\"][a-zA-Z][a-zA-Z0-9_]*[\'\"]\]", "g");
                    temp_string = temp_string.match(reg_eks_temp);
                    if ($.isArray(temp_string)) {
                        temp_string = temp_string.filter(Boolean);
                        if (temp_string.length >= 1) {
                            temp_string = temp_string[0].replace(/[\[\]\'\"]/g, "");
                            if ($('[name="' + temp_string + '[]"]').is('select[multiple]')) {
                                temp_val = $('[name="' + temp_string + '[]"]').children('option:selected').map(function () {
                                    return ($(this).val()||"").replace(/\'/g,"\\'");
                                }).get().join("|^|");
                                reg_eks_temp = new RegExp("@CurrentForm\\\[[\\\'\\\"]" + temp_string + "[\\\'\\\"]\\\](?![a-zA-Z0-9_])", "g");
                                if ($('[name="' + temp_string + '[]"]').is('[data-input-type="Number"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else if ($('[name="' + temp_string + '[]"]').is('[data-input-type="Currency"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "'" + temp_val + "'");
                                }
                            } else if ($('[name="' + temp_string + '[]"]').is('input[type="checkbox"]')) {
                                temp_val = $('[name="' + temp_string + '[]"]').filter(':checked').map(function () {
                                    return ($(this).val()||"").replace(/\'/g,"\\'");
                                }).get().join("|^|");
                                reg_eks_temp = new RegExp("@CurrentForm\\\[[\\\'\\\"]" + temp_string + "[\\\'\\\"]\\\](?![a-zA-Z0-9_])", "g");
                                if ($('[name="' + temp_string + '[]"]').is('[data-input-type="Number"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else if ($('[name="' + temp_string + '[]"]').is('[data-input-type="Currency"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "'" + temp_val + "'");
                                }
                            } else if ($('[name="' + temp_string + '"]').parents('.setOBJ:eq(0)').is('[data-type="pickList"]')) {
                                temp_val = $('[name="' + temp_string + '"]').val();
                                temp_val = temp_val.replace(/\'/g,"\\'");
                                reg_eks_temp = new RegExp("@CurrentForm\\\[[\\\'\\\"]" + temp_string + "[\\\'\\\"]\\\](?![a-zA-Z0-9_])", "g");
                                if($('[name="' + temp_string + '"]').parent().children().children('a.pickListButton').is('[selection-type="Multiple"]')){
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "'" + '(\''+(temp_val||'').split('|').join('\',\'')+'\')' + "'");
                                }else{
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "'" + temp_val + "'");
                                }
                            } else if ($('[name="' + temp_string + '"]').length >= 1) {
                                temp_val = $('[name="' + temp_string + '"]').val();
                                temp_val = temp_val.replace(/\'/g,"\\'");
                                reg_eks_temp = new RegExp("@CurrentForm\\\[[\\\'\\\"]" + temp_string + "[\\\'\\\"]\\\](?![a-zA-Z0-9_])", "g");
                                if ($('[name="' + temp_string + '"]').is('[data-input-type="Number"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else if ($('[name="' + temp_string + '"]').is('[data-input-type="Currency"]')) {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, temp_val);
                                } else {
                                    condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "'" + temp_val + "'");
                                }
                            }
                        }
                    }
                }
                // for(var ctr_i in matches_fns){
                //     temp_string = matches_fns[ctr_i];
                //     alert(temp_string)
                //     if($('[name="'+temp_string+'[]"]').is('select[multiple]') ){
                //         temp_val = $('[name="'+temp_string+'[]"]').children('option:selected').map(function(){ return $(this).val(); }).join("|^|");
                //         reg_eks_temp = new RegExp("@CurrentForm\[[\'\"]"+temp_string+"[\'\"]\](?![a-zA-Z0-9_])","g");
                //         condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "\""+temp_val+"\"");
                //     }else if($('[name="'+temp_string+'[]"]').is('input[type="checkbox"]') ){
                //         temp_val = $('[name="'+temp_string+'[]"]').filter(':checked').map(function(){ return $(this).val(); }).join("|^|");
                //         reg_eks_temp = new RegExp("@CurrentForm\[[\'\"]"+temp_string+"[\'\"]\](?![a-zA-Z0-9_])","g");
                //         condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "\""+temp_val+"\"");
                //     }else if($('[name="'+temp_string+'"]').length >= 1 ){
                //         alert(234)
                //         temp_val = $('[name="'+temp_string+'"]').val();
                //         reg_eks_temp = new RegExp("@CurrentForm\[[\'\"]"+temp_string+"[\'\"]\](?![a-zA-Z0-9_])","g");
                //         condition_formula_temp = condition_formula_temp.replace(reg_eks_temp, "\""+temp_val+"\"");
                //     }else{

                //     }
                // }
            }
        }
        return condition_formula_temp;
    },
}

function addPickListEntry(sourceFormId) {
    var form_id = sourceFormId;

    var dis_ele = $(this);
    var str_link = "/user_view/workspace?view_type=request&formID=" + form_id + "&requestID=0";
    console.log('frame', str_link)
    var my_dialog = $(
            '<div class="picklist-iframe" style="height: 100%;">' +
            '<iframe style="height:100%;width:100%;" src="' + str_link + '">' +
            '</iframe>' +
            '</div>'
            );
    var newHeight = $(window).outerHeight() - 50;
    var newWidth = $(window).outerWidth() - 50;

    var ret = "";
    ret += '<div class="picklistoverlay">';
    ret += '<div class="picklistmiddler">';
    ret += '<div id="' + form_id + '" class="picklistpos">';
    ret += '<div class="idehead">';//'+ideID+'

    ret += '<i class="picklistclose fa fa-times"></i>';
    ret += '</div>';
    ret += '<div class="clearfix"></div>';
    ret += '<div class="picklistcontent" style="width:' + newWidth + 'px; height:' + newHeight + 'px;">';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    var ret_elem = $(ret);

    dis_ele.closest('body').prepend(ret_elem);

    $(ret_elem).find('.picklistcontent:eq(0)').html(my_dialog);
    $(ret_elem).on('click', '.picklistclose', function (e) {
        e.stopPropagation();
        $(this).closest('.picklistoverlay').remove();
        var e = $.Event("keyup");
        e.which = 13;
        var auto_search_picklist = $('body').find('.picklist-request-id-storage').text();

        $('.searchPicklist').val(auto_search_picklist);

        $('.searchPicklist').trigger(e);

    });
    my_dialog.find('iframe').eq(0).on("load", function () {
        var innerJQ = $(this)[0].contentWindow.$;
        innerJQ('html').addClass('view-embed-iframe');
    });
}
PicklistSelectionType = {
    picklistData: "",
    init: function () {
        this.selectionTypeEvs();
        this.getExternalTable();
        this.getTableFields();
    },
    selectionTypeEvs: function () {
        var self = this;
        $("body").on("change", "#picklist_selection_type", function () {
            var selectedType = $(this).val();
            self.selectionTypeFn(selectedType);
        })
    },
    selectionTypeFn: function (selectedType) {
        var self = this;
        if (selectedType == "1") {
            //additional fields display for external DB
            $(".external_database_connection_container").removeClass("isDisplayNone");


            /* Default Fields Display */
            //forms and fields
            $('.picklist_form_type').closest(".form_selection").addClass("isDisplayNone");
            $('.picklist_form_field').closest(".form_selection").addClass("isDisplayNone");


            //other settings
            $(".enablePicklistAdd_prop").closest(".properties_width_container").addClass("isDisplayNone");
            $(".allow-values-not-in-list").closest(".properties_width_container").addClass("isDisplayNone");
            $(".show-picklist-view").closest(".properties_width_container").addClass("isDisplayNone");


            // $(".external_database_connection").trigger("change");
            self.setExternalConnection(self.picklistData);
        } else if (selectedType == "0" || typeof selectedType == "undefined") {
            //additional fields display for external DB
            $(".external_database_connection_container").addClass("isDisplayNone");



            /* Default Fields Display */
            //forms and fields
            $('.picklist_form_type').closest(".form_selection").removeClass("isDisplayNone");
            $('.picklist_form_field').closest(".form_selection").removeClass("isDisplayNone");

            //other settings
            $(".enablePicklistAdd_prop").closest(".properties_width_container").removeClass("isDisplayNone");
            $(".allow-values-not-in-list").closest(".properties_width_container").removeClass("isDisplayNone");
            $(".show-picklist-view").closest(".properties_width_container").removeClass("isDisplayNone");

            if ($(".picklist_form_type").val() != "0") {
                $(".picklist_form_type").trigger("change");
            }
        }
    },
    getExternalTable: function () {
        $("body").on("change", ".external_database_connection", function () {
            getNewConnection($(this), $(this).val(), 0, "", function () {

            });
        })
    },
    getTableFields: function () {
        var self = this;
        $("body").on("change", ".external_database_connection_table", function () {
            getFormFields($(this), $(this).val(), 0, function () {
                self.setPicklistColumnView();
            });
        })
    },
    setExternalConnection: function (data) {
        if (data.picklist_selection_type == "1") {
            if (data.external_database_connection && data.external_database_connection != "0") {
                $(".external_database_connection").val(data.external_database_connection);
                getNewConnection($(".external_database_connection"), data.external_database_connection, 0, "", function () {
                    $(".external_database_connection_table").val(data.external_database_connection_table).trigger("change");
                    //return to its sorted position
                    var display_columns_field = null;
                    try {
                        display_columns_field = JSON.parse(data["displayColumnsFields"]);
                    } catch (e) {

                    }

                    if ($('#popup_container').data()) {
                        if ($('#popup_container').data()['clicked_button']) {
                            $('.pickist_condition').val($($('#popup_container').data()['clicked_button']).attr('condition'))
                        }
                    }

                    if ($.type(display_columns_field) == "array") {
                        $(".form_selection .display_columns .picklist-view-columns tbody").children('tr:not(:eq(0))').each(function (eqi) {
                            var self_dis = $(this)
                            var input_label_ele = self_dis.find('input').eq(0);
                            if (
                                    display_columns_field.filter(function (value, indeks) {
                                        if (value['FieldName']) {
                                            return self_dis.find('span:contains("' + value['FieldName'] + '")').length >= 1;
                                        }
                                        return false;
                                    }).length >= 1
                                    ) {
                                input_label_ele.val(
                                        display_columns_field.filter(function (value, indeks) {
                                            if (value['FieldName']) {
                                                return self_dis.find('span:contains("' + value['FieldName'] + '")').length >= 1;
                                            }
                                            return false;
                                        })[0]['FieldLabel']
                                        );
                            }

                        });
                    }

                    picklistSorter();

                    getFormFields($(".external_database_connection_table"), data.external_database_connection_table, "", function () {

                        console.log("data.external_database_connection_return_field", data.external_database_connection_return_field);
                        $(".external_database_connection_return_field").val(data.external_database_connection_return_field);

                    });
                });
            }
        }
        this.picklistData = data;
    },
    setPicklistColumnView: function () {
        var options = $("#external_database_connection_return_field option:not([value='0'])");
        console.log(options)
        // $('.display_columns').html('');
        $('.picklist_form_field').html('<option value="0">----------------------------Select----------------------------</option>');
        // for (var ctr = 0; ctr <= fields.length - 1; ctr++) {
        options.each(function () {
            $('.picklist_form_field').append('<option value="' + $(this).attr("value") + '">' + $(this).attr("value") + '</option>');
        })

        // }
        $('.picklist_form_field').css("display", "").parent().find('.getFromsLoader ').css('display', 'none');
        // $('.display_columns').append('<input name="chk_displayColumns" type="checkbox" value="TrackNo">TrackNo</input><br/>');
        // 
        //     
        //     $('.display_columns').append('<input name="chk_displayColumns" type="checkbox" value="' + fields[ctr] + '">' + fields[ctr] + '</input><br/>');
        // }

        var columnHeaderTable = '<table class="table_data dataTable picklist-view-columns"><tr><th>Field Label</th><th>Field Name</th></tr>';
        // var active_fields = fields;
        //console.log(active_fields)
        // for (var ctr = 0; ctr < active_fields.length; ctr++) {
        options.each(function () {
            columnHeaderTable += '<tr class="hovereffect">';
            columnHeaderTable += '<td><input type="text" class="form-text" style="background-color: rgba(255,255,255,0.5) !important;" /></td><td class="tip-title" data-original-title="' + $(this).attr("value").replace(/\|\^\|/g, ",") + '  (Drag up/down)" style="cursor:move"><span style="padding:3px">' + $(this).attr("value").replace(/\|\^\|/g, ",") + '</span></td>';
            columnHeaderTable += '</tr>';
        })

        columnHeaderTable += '</table>';
        var columnHeaderTables = $(columnHeaderTable);

        $('.display_columns').html(columnHeaderTables);
        columnHeaderTables.find('.tip-title').tooltip();
        columnHeaderTables.find("tbody").sortable({
            items: "tr:not(:eq(0))",
            placeholder: {
                element: function (currentItem) {
                    return $("<tr><td style='width:50%;height:40px;'></td><td style='width:50%;height:40px;'></td></tr>")[0];
                },
                update: function (container, p) {
                    return;
                }
            },
            sort: function (ev, ui) {
                $(ui["helper"]).children("td").css("width", "181px");
            },
            axis: "y"
        });
        //columnHeaderTables.find('tbody').removeClass('ui-sortable');
        //tooltip ni picklist

        //testing onchange
        picklistSorter();
    }
}

function viewPickListSource(sourceFormId) {
    var form_id = sourceFormId;

    //e.preventDefault();
    var dis_ele = $(this);
    var requestId = dis_ele.closest('td').find('[data-form-source-id]').attr('data-form-source-id');
    var trackNo = dis_ele.closest('td').find('[data-trackno]').attr('data-trackno');
    var src = '/user_view/workspace?view_type=request&formID=' + form_id + '&requestID=' + requestId + '&trackNo=' + trackNo + '&embed_type=viewEmbedOnly';
    //var str_link = "/user_view/workspace?view_type=request&formID=" + form_id + "&requestID=0";
    var newHeight = $(window).outerHeight() - 50;
    var newWidth = $(window).outerWidth() - 50;
    var ret = "";
    ret += '<div class="picklistoverlay">';
    ret += '<div class="picklistmiddler">';
    ret += '<div id="' + form_id + '" class="picklistpos">';
    ret += '<div class="idehead">';//'+ideID+'

    ret += '<i class="picklistclose fa fa-times"></i>';
    ret += '</div>';
    ret += '<div class="clearfix"></div>';
    ret += '<div class="picklistcontent" style="width:' + newWidth + 'px; height:' + newHeight + 'px;">';
    ret += '</div>';

    //ret += '<div class="idefoot">';
    //ret += '<button class="idesave">Save</button>';
    //ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    ret += '</div>';
    var ret_elem = $(ret);
    dis_ele.closest('body').prepend(ret_elem);
    var my_dialog = $(
            '<div class="picklist-iframe" style="height: 100%;">' +
            '<iframe style="height:100%;width:100%;" src="' + src + '">' +
            '</iframe>' + '</div>'
            )
    $(ret_elem).find('.picklistcontent:eq(0)').html(my_dialog);
    my_dialog.children('iframe').eq(0).load(function () {
        iframe_jquery = $(this)[0].contentWindow.$;
        iframe_jquery('html').addClass('view-embed-iframe');
    });
    $(ret_elem).on('click', '.picklistclose', function (e) {
        e.stopPropagation();
        $(this).closest('.picklistoverlay').remove();
        var e = $.Event("keyup");
        e.which = 13;

        $('.searchPicklist').trigger(e);
    });
}