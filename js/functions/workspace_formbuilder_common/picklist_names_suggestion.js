/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function suggestionTableModal(obj, container) {
    var hide_class = "";
    var is_multiple = $(obj).siblings('span').children('a.pickListButton').attr('selection-type') || $();
    var data_id = $(obj).attr("id");
    $(".suggestion_container").remove();
    $(".setOBJ").css("z-index", "");
    $(obj).closest(".setOBJ").css("z-index", "3");
    if (is_multiple === "Multiple") {
        hide_class = "isDisplayNone";
    }
    $(obj).closest("." + container).append("<div data-suggestion='" + data_id + "' class='suggestion_container " + hide_class + "' " +
            "style='background-color:#FFF;z-index: 1;position:absolute;border:1px solid #B3ACAC;width:500px;overflow:auto;" +
            "height:260px;top:55px;left:0px;'>" +
            "<div class='suggestion-data-content' style='height:100%'>" +
            "<div style='display:table;width:100%;height:100%'>" +
            "<div style='vertical-align:middle;width:100%;height:100%;text-align:center;display: table-cell;'>" +
            "<img src='/images/loader/loader.gif' class='processorLoad' style='display:inline-block'>" +
            "</div>" +
            "</div>" +
            //append content here
            "</div>" +
            "</div>");
}

function suggestionTableData(obj, data, callback) {
    console.log("sTD", obj.closest('.setOBJ'), data)
    var obj_type = $(obj).closest('.setOBJ').attr('data-type');
    // var picklistType = obj_type.find('.pickListButton').attr('selection-type');
    var obj_id = $(obj).attr("id");
    var obj_data_container = $("[data-suggestion='" + obj_id + "']").find(".suggestion-data-content");
    var obj_data = data['aaData'];
    var obj_data_content = "<div class='fl-list-of-app-record-wrapper'>";
    obj_data_content += "<table class='display_data dataTable' style='cursor:pointer' style='width:auto;height:auto'>"
    obj_data_content += "<thead class='fl-header-tbl-wrapper'>";
    obj_data_content += '<tr>';

    var columnsDisplayArr = JSON.parse(data['columnsDisplay']);
    // if (obj_type === "pickList") {
    //     obj_data_content += "<th class='fl-table-ellip' style='width:30px' data-type=''></th>";
    // }

    for (var index in columnsDisplayArr) {
        obj_data_content += "<th class='fl-table-ellip' style='width:150px'>" + columnsDisplayArr[index].FieldLabel + "</th>"
    }
    obj_data_content += "</tr>";
    obj_data_content += "<tbody>";
    if (obj_data.length == 0) {
        obj_data_content += "<tr><td colspan='" + columnsDisplayArr.length + "'>No Record Found</td></tr>";
    } else {
        for (var index in obj_data) {
            obj_data_content += "<tr>";
            for (var i in obj_data[index]) {
                obj_data_content += "<td>" + obj_data[index][i] + "</td>";
            }
            obj_data_content += "</tr>";
        }
    }
    obj_data_content += "</tbody>";
    obj_data_content += "</table>";
    obj_data_content += "</div>";
    obj_data_container.html(obj_data_content);
    callback(1);
}
