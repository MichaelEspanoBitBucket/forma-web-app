(function($){ 
	$.fn.tabPanel = function(options){
		var settings = $.extend({
			tabNavSortable:true,
			tabsNavAnchorScroll:true,
			paneWidth:"100%",
			paneHeight:"100px",
			setContentDroppable:{
				// accepts:".setObject",
				accepts: function(ele){
                    return ele.is(".setObject") && ele.parents('.public-embed-unused-object-list,.public-embed-object-list').length <= 0;
                },
				revert:true,
				greedy:true,
				drop:function(e,ui){
					if(
						false
						//$(ui.helper).find(".form-tabbable-pane").length >= 1
						//$(ui.helper).find(".form-table").length >= 1
					){
						//do nothing
					}else{
						//evtarget = (window.event) ? window.event.srcElement /* for IE */ : e.target;
				        console.log(ui)
				        console.log(ui.helper)
				        console.log(e.target)
				        // console.log(ui.draggable[0].parentElement);
				        var target = e.target;
				        var source = ui.draggable[0].parentElement;
				        if(target === source){ //conditional purpose to prevent dropping off the same place
				            //alert('Same droppable container');
				    	}else{
				    		
				    		if($(ui.helper).hasClass("ui-draggable")){
				    			$(ui.helper).draggable("option","containment","parent");
				    		}
				    		var pos = ui.draggable.offset();
				    		var dpos = $(this).offset();
				    		console.log($(this).attr('id'));
				    		console.log("posdpos",pos,dpos);
				    		if((pos.top - dpos.top)<11){
				    			pos.top = 10;
				    			dpos.top = 0;
				    		}
				    		if((pos.left-dpos.left)<29){
				    			pos.left = 28;
				    			dpos.left = 0;
				    		}
							$(ui['helper']).css({
								"top": pos.top - dpos.top,
								"left":pos.left - dpos.left
							});
							$(ui.helper).appendTo(this);
				    	}
				    	$(ui.helper).snapToGrid();
					}
				}
			},
			resizableContent:false,
			resizableContentContainment:null,
		},options);
		// console.log(settings.setContentDroppable)
		return this.each(function(eqi_tabPanel){
			if($(this).closest(".tab-panel-parent-container").length == 0){
				$(this).wrap("<div class='tab-panel-parent-container' style='position:relative;'/>");
			}else{
				
			}
			
			if($(this).find(">ul").length >= 1){
				if($(this).find(">ul").find(">li.li-add-new-tab-panel").length == 0){
					$(this).find(">ul").append(addNewTabPanel = $("<li class='li-add-new-tab-panel' style='outline: 1px solid lightgrey;'><a class='add-new-tab-panel fa fa-plus' href='#blank-new-tab-panel'></a></li>") );
				}
			}

			$(".ui-widget-content[role='tabpanel']") //ETO PO
			// console.log($(this))
			// console.log("ul")
			// console.log($(this).find(">ul").eq(0));
			// console.log("li")
			// console.log($(this).find(">ul").eq(0).find(">.li-add-new-tab-panel").eq(0))


			$(this).find(">ul").eq(0).find(">.li-add-new-tab-panel").eq(0).on({
				"mousedown":function(e){
					tabPanelElement = $(this).closest(".tab-panel-parent-container");

					e.stopPropagation();
					addTabDialog = $('<div class="tab-panel-actions-container">'+
										'<h3 class="fl-margin-bottom"><i class="icon-save"></i> Add Tab</h3><div class="hr"></div>'+
												'<div class="addTabDialog" title="Tab data">'+
													'<form>'+
														'<div class="fields">'+
															'<div class="fields"><div class="input_position section clearing fl-field-style"><div class="column div_1_of_1"><span>Tab Title</span><input type="text"  class="tab-title-value form-text" placeholder="Title" value=""/><input type="text"  class="tab-content-value" style="display:none;" placeholder="Content" value=""/></div></div></div>'+
															// '<div class="label">Content</div>'+
															// '<div class="input_position2">'+
															// 	'<textarea name="tab_content" id="tab_content" class="ui-widget-content ui-corner-all input-x-medium"></textarea>'+
															// '</div>'+
														'</div>'+

														// '<div class="fields">'+
														// 	'<div class="fields">'+
														// 		'<div class="label_basic"> Visibility Formula: </div>'+
														// 		'<div class="input_position">'+
														// 			'<textarea class="form-textarea visibility-formula" style="width:100%;"></textarea>'+
														// 		'</div>'+
														// 	'</div>'+
														// '</div>'+

													'</form>'+
													'<div class="fields"><div class="label_basic"></div><div class="input_position" style="margin-top:5px;text-align:right;"><input type="button" class="btn-blueBtn fl-margin-right add-new-tab-panel fl-positive-btn" value="Add"/></div></div>'+
											'</div>'+
										'</div>');

					var newDialog = new jDialog(addTabDialog.html(), '',"500", "", "", function(){});
					newDialog.themeDialog("modal2");
					// if($(".addTabDialog").find(".visibility-formula").length >= 1 ){
					// 	var title_editing = $(this).closest('li[role="tab"]').children("a.ui-tabs-anchor");
					// 	var visible_formula_tab_val = title_editing.parents("li").eq(0).attr("visible-formula");
					// 	$(".addTabDialog").find(".visibility-formula").val(visible_formula_tab_val);

					// 	$(".addTabDialog").find(".visibility-formula").on({
					// 		"keyup":function(ev){
					// 			var v_formula_value = $(this).val();
					// 			title_editing.parents("li").eq(0).attr("visible-formula",v_formula_value);
					// 			// if (typeof $(".chk_Visibility") != "undefined") {
				 //    //                 $(".chk_Visibility").find("[name='visibility-choices'][value='" + $(".getFields_" + object_id).attr("field-visible") + "']").prop("checked", true);
				 //    //                 if ($(".getFields_" + object_id).attr("field-visible") == "computed") {
				 //    //                     $(".chk_Visibility").find(".visibility-formula").val($(".getFields_" + object_id).attr("visible-formula"));
				 //    //                 }
				 //    //                 title_editing.parents("li").eq(0).attr("visible-formula",title_value);
				 //    //             }
					// 		}
					// 	})
					// }
					
					$(".add-new-tab-panel").on({
						"click":function(){
							save_history();
							//alert($(this).closest(".addTabDialog").find(".tab-title-value").val());
							// console.log("FDJHFSI")
							// console.log(tabPanelElement)
							var datass = {};
							datass.containment = settings.resizableContentContainment;
							datass.paneWidth = settings.paneWidth;
							datass.paneHeight = settings.paneHeight;
							datass.tabTitle = $(this).closest("#popup_container").find(".tab-title-value");
							datass.tabContent = $(this).closest("#popup_container").find(".tab-content-value");
							datass.tabTemplate = "<li><a href='#{href}'>#{label}</a><span class='ui-icon ui-icon-close' role='presentation' style='cursor:pointer;'>Remove Tab</span><span class='edit-title-tab-panel ui-icon ui-icon-edit' style='margin:0px;cursor:pointer;'>Edit tab</span></li>";
							datass.tabCounter = parseInt($(tabPanelElement).children(".form-tabbable-pane").attr("tabcounter"));
							datass.tabCounter++;
							$(tabPanelElement).find(">.ui-tabs").attr("tabCounter",datass.tabCounter);
							$(this).data("added_tab_element",addTab($(tabPanelElement),datass));
							if($(this).parents('.ui-sortable').eq(0).length >= 1){
								$(this).parents('.ui-sortable').eq(0).sortable( "refresh" );
							}
							if($('.table-form-design-relative').length <= 0){
								$('.fl-closeDialog').click();
							}
							// var setobj_tabpanel_ele = $(tabPanelElement).parents('.setObject[data-type="tab-panel"]')
							// var setobj_parent_ele = setobj_tabpanel_ele.parent();
							// if(setobj_parent_ele.is('.td-relative') && (setobj_tabpanel_ele.outerWidth() > setobj_parent_ele.outerWidth() ) ){
							// 	var overrun_width = setobj_tabpanel_ele.outerWidth() - setobj_parent_ele.outerWidth();
							// 	setobj_parent_ele.parents('td.ui-resizable').resizable('resizeBy',{
							// 		"width":overrun_width
							// 	});
							// }
							// if(setobj_parent_ele.is('.td-relative') && (setobj_tabpanel_ele.outerHeight() > setobj_parent_ele.outerHeight() ) ){
							// 	alert(2345)
							// 	var overrun_height = setobj_tabpanel_ele.outerHeight() - setobj_parent_ele.outerHeight();
							// 	setTimeout(function(){
							// 		setobj_parent_ele.parents('td.ui-resizable').resizable('resizeBy',{
							// 			"height":overrun_height
							// 		});
							// 	},500);
							// }
							
						}
					});
					$(".cancel-new-tab-panel").on({
						"click":function(){
							$(this).closest("#popup_container").fadeOut(500,function(){
								$(this).remove();
							});
							$("#popup_overlay").fadeOut(500,function(){
								$(this).remove();
							});
						}
					})
					e.preventDefault();
					return false;
				}
			});
			
			tabPanelElement_tabs = $(this).tabs({
				"activate":function(e,ui){
					// console.log("ACTIVATED TAB TESTING", $(this),e,ui);
					ui["newPanel"].find('.setOBJ[data-type="accordion"]>.fields_below_accordion>.input_position_below_accordion>.form-accordion').accordion("refresh");
				}
			});
			if(typeof $(this).attr("tabcounter") == "undefined"){
				$(this).attr("tabcounter",$(this).find(">.ui-tabs-nav").find(">li[role='tab']").not('.li-add-new-tab-panel').length);
			}

			if(tabPanelElement_tabs.find("ul.ui-tabs-nav").eq(0).length >= 1){
				tabPanelElement_tabs.find(".ui-tabs-nav").eq(0).on({
					"mouseenter":function(e){
						$(this).scrollTop(0);
					},
					"mouseleave":function(e){
						$(this).scrollTop(0);
					}
				});
			}

			if(settings.resizableContent){
				if(settings.resizableContent == true){
					tabPanelElement_tabs.find(">.ui-widget-content[role='tabpanel']").each(function(){
						dataz = {};
						dataz.handles = 's';
						dataz.minWidth = 100;
						dataz.minHeight = 100;
						dataz.containment = settings.resizableContentContainment;
						if(settings.resizableContent == true){
							makeResizableContent($(this), dataz);
						}
					});
				}
			}
			if(settings.setContentDroppable){
				tabPanelElement_tabs.find(">.ui-widget-content[role='tabpanel']").each(function(eqi_tabPanelContent){
					dataz = {};
					dataz.accepts = settings.setContentDroppable.accepts;
					dataz.revert = settings.setContentDroppable.revert;
					dataz.greedy = settings.setContentDroppable.greedy;
					dataz.drop = settings.setContentDroppable.drop;
					makeDroppable($(this), dataz);
				});
			}

			
			//ui-tabs-nav
			// tabPanelElement_tabs.find(".ui-tabs-nav").css({
			//	"display":"inline-block"
			// });
			// close icon: removing the tab on click
			tabPanelElement_tabs.delegate( "ul:eq(0) > li > span.ui-icon-close", "click", function() {
				var indis_self = $( this );
				var ui_tabs_nav_ele = indis_self.parents(".ui-tabs-nav").eq(0);
				var conf = "Are you sure you want to delete this tab?";
				var newConfirm = new jConfirm(conf, 'Confirmation Dialog','', '', '', function(r){
					if(r==true){
						var panelId = indis_self.closest( "li" ).attr( "aria-controls" );
						//added by pao slide layer menu remove list
						$('#'+panelId).children('.setObject').each(function(){
						var lay_number = $(this).attr("data-object-id");
						$('#layer_'+lay_number).remove();							
						})
						//
						indis_self.closest(".ui-tabs").find( "#" + panelId ).remove();
						indis_self.closest( "li" ).remove();
						tabPanelElement_tabs.tabs( "refresh" );

						console.log($('#'+panelId))
						
						if(hasScrollLeft(ui_tabs_nav_ele) == true) {
							ui_tabs_nav_ele.attr("has-scroll","true");
							ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).attr("has-scroll","true");
						}else{
							ui_tabs_nav_ele.removeAttr("has-scroll","true");
							ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).removeAttr("has-scroll","true");
						}
					}
			    });
			    newConfirm.themeConfirm("confirm2", {
               		 'icon':'<i class="fa fa-question-circle fl-margin-right" style="font-size:15px;"></i>' 
          		});

  				
			});
			tabPanelElement_tabs.delegate( "span.edit-title-tab-panel", "click", function() {
				// console.log("TESTINGTESTING")
				// console.log($(this));
				var dis_self = $(this);
				title_editing = $(this).closest('li[role="tab"]').children("a.ui-tabs-anchor");
				addTabDialog = $('<div class="tab-panel-actions-container">'+
									'<h3 class="fl-margin-bottom"><i class="icon-save"></i> Edit Tab Title</h3><div class="hr"></div>'+
										'<div class="editTabDialog" title="Tab data">'+
											'<form>'+
												'<div class="fields">'+
													'<div class="fields">'+
														//'<div class="label_below2"> Title: </div>'+
														'<div class="input_position section clearing fl-field-style">'+
															'<div class="column div_1_of_1">'+
																'<span>Tab Title:</span>'+
																'<input value="'+title_editing.text()+'" type="text"  class="tab-title-value form-text"" placeholder="Title" value=""/>'+
																'<input type="text"  class="tab-content-value" style="display:none;" placeholder="Content" value=""/>'+
															'</div>'+
														'</div>'+
													'</div>'+
													// '<div class="label">Content</div>'+
													// '<div class="input_position2">'+
													// 	'<textarea name="tab_content" id="tab_content" class="ui-widget-content ui-corner-all input-x-medium"></textarea>'+
													// '</div>'+
												'</div>'+

												'<div class="fields">'+
													'<div class="fields">'+
														//'<div class="label_below2"> Visibility Formula: </div>'+
														'<div class="input_position section clearing fl-field-style">'+
															'<div class="column div_1_of_1">'+
																'<span>Visibility Formula: </span>'+
																'<textarea class="form-textarea visibility-formula" data-ide-properties-type="ide-visibility-formula" style="width:100%;margin-top:0px;"></textarea>'+
															'</div>'+
														'</div>'+
													'</div>'+
												'</div>'+

											'</form>'+
											'<div class="fields"><div class="label_basic"></div><div class="input_position fl-margin-top" style="text-align:right;"><input type="button" class="btn-blueBtn fl-margin-right edit-tab-title-panel fl-positive-btn" value="OK"/></div></div>'+
										'</div>'+
									'</div>');

				var newDialog = new jDialog(addTabDialog.html(), '',"500", "", "", function(){});
				newDialog.themeDialog("modal2");
				if($(".editTabDialog").find(".visibility-formula").length >= 1 ){
					
					var visible_formula_tab_val = title_editing.parents("li").eq(0).attr("visible-formula");
					$(".editTabDialog").find(".visibility-formula").val(visible_formula_tab_val);

					$(".editTabDialog").find(".visibility-formula").on({
						"keyup":function(ev){
							var v_formula_value = ""+$(this).val();
							title_editing.parents("li").eq(0).attr("visible-formula",v_formula_value);
							title_editing.parents("li").eq(0).attr("field-visible","computed");
							// if(v_formula_value.length >= 1){
							// 	alert(132456)
							// 	title_editing.parents("li").eq(0).attr("lookup-event-trigger",true);
							// }else{
							// 	title_editing.parents("li").eq(0).attr("lookup-event-trigger",false);
							// }






							// if (typeof $(".chk_Visibility") != "undefined") {
			                    // $(".chk_Visibility").find("[name='visibility-choices'][value='" + $(".getFields_" + object_id).attr("field-visible") + "']").prop("checked", true);
			                    // if ($(".getFields_" + object_id).attr("field-visible") == "computed") {
			                    //     $(".chk_Visibility").find(".visibility-formula").val($(".getFields_" + object_id).attr("visible-formula"));
			                    // }
			                    // title_editing.parents("li").eq(0).attr("visible-formula",title_value);
			                // }
						}
					})
				}

				$(".cancel-new-tab-panel").on({
					"click":function(){
						$(this).closest("#popup_container").remove();
						$("#popup_overlay").remove();
					}
				});

				$(".edit-tab-title-panel").on({
					"click":function(){
						save_history();
						var self = $( this );
						title_value = $(this).closest("#popup_container").find(".tab-title-value").val();
						title_editing.html(title_value);
						title_editing.parents("li").eq(0).attr("tab-name",title_value);
						$(".fl-closeDialog").click();

						var ui_tabs_nav_ele = dis_self.parents(".ui-tabs-nav").eq(0);
						if(hasScrollLeft(ui_tabs_nav_ele) == true) {
							ui_tabs_nav_ele.attr("has-scroll","true");
							ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).attr("has-scroll","true");
						}else{
							ui_tabs_nav_ele.removeAttr("has-scroll","true");
							ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).removeAttr("has-scroll","true");
						}
					}
				});

			});
			tabPanelElement_tabs.bind( "keyup", function( event ) {
				if ( event.altKey && event.keyCode === $.ui.keyCode.BACKSPACE ) {
					var panelId = tabPanelElement_tabs.find( ".ui-tabs-active" ).remove().attr( "aria-controls" );
					tabPanelElement_tabs.find( "#" + panelId ).remove();
					tabPanelElement_tabs.tabs( "refresh" );
				}
			});

			if(settings.tabsNavAnchorScroll){
				
				if($(this).parents(".setObject").length >= 1){

					$(this).parents(".setObject").eq(0).attr("ui-tabs-nav-scrollable","true");

				}else if($(this).parents(".setOBJ").length >= 1){

					$(this).parents(".setOBJ").eq(0).attr("ui-tabs-nav-scrollable","true");

				}
				
				
				if(tabPanelElement_tabs.find(".scroll-achor-ul-tab-nav").length >=1){

				}else{
					var ul_scroll_anchor = $(
						'<div class="ul-scroll-anchor">'+
							// '<div class="ul-scroll-anchor-bg">'+
							// 	'<div class="scroll-achor-ul-tab-nav scroll-achor-left"><i style="font-size: 40px; color:white !important;" class="fa fa-chevron-circle-left"></i></div>'+
							// 	'<div class="scroll-achor-ul-tab-nav scroll-achor-right"><i style="font-size: 40px; color:white !important;" class="fa fa-chevron-circle-right"></i></div>'+
							// '</div>'+
							'<div class="ul-scroll-anchor-bg">'+
								'<div class="scroll-achor-ul-tab-nav scroll-achor-left"><i  class="fa fa-chevron-left"></i></div>'+
								'<div class="scroll-achor-ul-tab-nav scroll-achor-right"><i  class="fa fa-chevron-right"></i></div>'+
							'</div>'+
						'</div>'
					);
					tabPanelElement_tabs.prepend(ul_scroll_anchor)
					var ui_tabs_nav_ele = tabPanelElement_tabs.find('.ui-tabs-nav').eq(0);
					if(hasScrollLeft(ui_tabs_nav_ele) == true) {
						ui_tabs_nav_ele.attr("has-scroll","true");
						ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).attr("has-scroll","true");
					}else{
						ui_tabs_nav_ele.removeAttr("has-scroll","true");
						ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).removeAttr("has-scroll","true");
					}


					ul_scroll_anchor.find('.scroll-achor-left').eq(0).data('mousedown_runs',null)
					ul_scroll_anchor.find('.scroll-achor-left').eq(0).data('mousedown_fn',function(e){
						if($(this).data('mouseenter_runs')){
							console.log($(this).data("scroll-accel"))
							if(typeof $(this).data("scroll-accel") === "undefined"){
								$(this).data("scroll-accel", 1);
							}else{
								$(this).data("scroll-accel", Number($(this).data("scroll-accel"))+1);
							}
							sLeft = $(this).parents(".form-tabbable-pane").eq(0).find('.ui-tabs-nav').eq(0).scrollLeft();
							$(this).parents(".form-tabbable-pane").eq(0).find('.ui-tabs-nav').eq(0).scrollLeft(sLeft - Number($(this).data("scroll-accel")))
							var enself = $(this);
							$(this).data('mousedown_runs',setTimeout(function(){
								enself.data('mousedown_fn').call(enself,e);
							},1));
						}
						e.stopPropagation();
					});

					ul_scroll_anchor.find('.scroll-achor-left').eq(0).on({
						"mousedown":ul_scroll_anchor.find('.scroll-achor-left').eq(0).data('mousedown_fn'),
						"mouseup":function(){
							$(this).data("scroll-accel", 1);
							clearTimeout($(this).data('mousedown_runs'));
						},
						mouseenter:function(e){
							$(this).data('mouseenter_runs',true);
						},
						mouseleave:function(e){
							$(this).data("scroll-accel", 3);
							$(this).data('mouseenter_runs',false);
						}
					});




					ul_scroll_anchor.find('.scroll-achor-right').eq(0).data('mousedown_runs',null)
					ul_scroll_anchor.find('.scroll-achor-right').eq(0).data('mousedown_fn',function(e){
						if($(this).data('mouseenter_runs')){
							console.log(e,"sa"+$(this).data("scroll-accel"))
							if(typeof $(this).data("scroll-accel") === "undefined"){
								$(this).data("scroll-accel", 3);
							}else{
								$(this).data("scroll-accel", Number($(this).data("scroll-accel"))+1);
							}
							sLeft = $(this).parents(".form-tabbable-pane").eq(0).find('.ui-tabs-nav').eq(0).scrollLeft();
							console.log("sLeft",sLeft)
							$(this).parents(".form-tabbable-pane").eq(0).find('.ui-tabs-nav').eq(0).scrollLeft(sLeft + Number($(this).data("scroll-accel")) );

							var enself = $(this);
							$(this).data('mousedown_runs',setTimeout(function(){
								enself.data('mousedown_fn').call(enself,e);
							},1));
						}
						e.stopPropagation();
					});

					ul_scroll_anchor.find('.scroll-achor-right').eq(0).on({
						"mousedown":ul_scroll_anchor.find('.scroll-achor-right').eq(0).data('mousedown_fn'),
						"mouseup":function(){
							$(this).data("scroll-accel", 3);
							clearTimeout($(this).data('mousedown_runs'));
						},
						mouseenter:function(e){
							$(this).data('mouseenter_runs',true);
						},
						mouseleave:function(e){
							$(this).data("scroll-accel", 3);
							$(this).data('mouseenter_runs',false);
						}
					});
				}
			}

			if(settings.tabNavSortable){
				// console.log("tabPanelElement_tabs",tabPanelElement_tabs);
				tabPanelElement_tabs.find('.ui-tabs-nav').eq(0).sortable({
					items : 'li:not(.li-add-new-tab-panel)',
					axis:'x',
					start:function(e, ui){
						
						$(this).scrollTop(0)
						// if($('.temp-style').length >= 1){
						// 	$('.temp-style').remove();
						// }
						// var style_tag = $(
						// 	'<style class="temp-style">'+
						// 		'.tab-navs-placeholder-active{'+
						// 			'height:'+ui.helper.outerHeight()+'px;'+
						// 			'width:'+ui.helper.outerWidth()+'px;'+
						// 		'}'+
						// 	'</style>'
						// );
						// $('body').prepend(style_tag);
						
						ui.placeholder.css({
							"height":ui.helper.height()+"px",
							"width":Number(ui.helper.width()+1)+"px"
						});
						$(this).sortable('refresh');
						$(this).sortable( "refreshPositions" );
					},
					sort:function(e, ui){
						
						$(this).scrollTop(0);
						$(this).sortable('refresh');
						$(this).sortable( "refreshPositions" );
					},
					stop:function(e ,ui){
						$(ui.item).css('top', '');
						$(this).scrollTop(0);
						$(this).sortable('refresh');
						$(this).sortable( "refreshPositions" );
					},
					placeholder:"sortable-placeholder"
				}).disableSelection();
			}
		});
		function addTab(tabPanelElement, datass) {
			var label = datass.tabTitle.val() || "Tab " + datass.tabCounter,
			id = "tabs-" + datass.tabCounter+"_"+tabPanelElement.children(".form-tabbable-pane").attr("id"),
			li = $( datass.tabTemplate.replace( /#\{href\}/g, "#" + id ).replace( /#\{label\}/g, label ) ),
			tabContentHtml = datass.tabContent.val() || ""/*"Tab " + datass.tabCounter + " content."*/;
			li.attr("tab-name",label);

			tabPanelElement.find(">.ui-tabs").find(">.ui-tabs-nav").find(tabPanelElement.context).before( li );

			tabPanelElement.find(">.ui-tabs").append(DRThis = $("<div style='padding:0px;overflow:auto;position:relative;width:"+datass.paneWidth+";height:"+datass.paneHeight+";' id='" + id + "'><p>" + tabContentHtml + "</p></div>") );
			tabPanelElement.find(">.ui-tabs").tabs( "refresh" );

			var same_height = $(DRThis).closest(".form-tabbable-pane").children(".ui-tabs-panel.ui-resizable").not(DRThis).eq(0).outerHeight();
			//console.log("WHAT SAME HEIGHT",same_height)
			if(typeof same_height === "number"){
				$(DRThis).css({
					"height":(same_height)+"px"
				})
			}
			
			dataz = {};
			dataz.accepts = settings.setContentDroppable.accepts;
			dataz.revert = settings.setContentDroppable.revert;
			dataz.greedy = settings.setContentDroppable.greedy;
			dataz.drop = settings.setContentDroppable.drop;
			makeDroppable($(DRThis), dataz);

			dataz = {};
			dataz.handles = 's';
			dataz.minWidth = 100;
			dataz.minHeight = 100;
			dataz.containment = datass.containment;
			if(settings.resizableContent){
				if(settings.resizableContent == true){
					makeResizableContent($(DRThis), dataz);
				}
			}

			var ui_tabs_nav_ele = tabPanelElement.find(">.ui-tabs").find(">.ui-tabs-nav");
			if(hasScrollLeft(ui_tabs_nav_ele) == true) {
				ui_tabs_nav_ele.attr("has-scroll","true");
				ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).attr("has-scroll","true");
			}else{
				ui_tabs_nav_ele.removeAttr("has-scroll","true");
				ui_tabs_nav_ele.parents('.form-tabbable-pane').eq(0).removeAttr("has-scroll","true");
			}
			li.children("a").eq(0).trigger("click");
			return $(DRThis).add(li)
		}
		function makeDroppable(thisElement, datass){
			$(thisElement).droppable({
				accept:datass.accepts,
				revert:datass.revert,
				greedy:datass.greedy,
				drop:datass.drop
			});
		}
		function makeResizableContent(thisElement, datass){
			// console.log("make resizabvle")
			// console.log(thisElement.closest(".tabbingPanel[element-type='control']"))
			// console.log("i console log mo dyan",thisElement);
			// console.log(thisElement.closest(".tabbingPanel[element-type='control']").find(">.ui-tabs-nav").eq(0));
			$(thisElement).resizable({
				handles:datass.handles,
				minWidth:datass.minWidth,
				minHeight:datass.minHeight,
				start:function(e, ui){
					save_history();
					parentWidths = 0;

					$(this).closest(".tabbingPanel[element-type='control']").find(">.ui-tabs-nav").eq(0).find(">li").each(function(){
						parentWidths = parentWidths + $(this).width();
					});
					
					$(ui.helper).resizable("option","minWidth",parentWidths);
				},
				resize:function(e, ui){
					containment = datass.containment;
					//droppable_offset_top = $(containment).offset().top
					//droppable_offset_left = $(containment).offset().left
					if($(containment).length >= 1){
						droppable_width = $(containment).width();
						droppable_height = $(containment).height();

						this_ele_top = $(this).closest(".setObject").position().top;
						this_ele_left = $(this).closest(".setObject").position().left;

						this_ele_bottom = $(this).closest(".setObject").position().top + $(this).closest(".setObject").height();
						this_ele_right = $(this).closest(".setObject").position().left + $(this).closest(".setObject").width();
						/*console.log(this_ele_bottom+" >= "+droppable_height);*/
						if(	this_ele_bottom >= droppable_height ){
							$(this).css({
								height: ($(this).height() - Math.abs(this_ele_bottom-droppable_height))+"px"
							});
							// $(this).closest(".element").find(".blockings").css({
							// 	height: ($(this).closest(".element").find(".blockings").height() - Math.abs(this_ele_bottom-droppable_height))+"px"
							// });
							//refreshBlockings($(ui.helper));
						}
					}
					// var resizableAxis = $(this).data('ui-resizable').axis;
					// resizeableTooltip($(this).children('.ui-resizable-'+resizableAxis));
				},
				stop:function(e, ui){
					var thisElement = $(this);
					var thisElement_height = thisElement.outerHeight();
					var thisElement_width = thisElement.outerWidth();
					$(thisElement).closest(".form-tabbable-pane").children(".ui-tabs-panel.ui-resizable").not(thisElement).css({
						"height":(thisElement_height)+"px"
					})
				}
			})
		}
	}
}(jQuery));