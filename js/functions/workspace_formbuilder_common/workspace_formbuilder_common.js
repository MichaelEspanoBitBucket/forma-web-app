/*
    Functions that needs to  be initialize to formbuilder and request view
    add here
 */
function FieldSelectionClear(selection, psel){//FS#8055, FS#8740 removing picklist clear value
    var prefix_selector = psel;
    var pathname = window.location.pathname;
    if (pathname == "/user_view/formbuilder") {
        return;
    }
    if(!prefix_selector){
        prefix_selector = "clear-field";
    }
    var selector = $(selection);
    //FS#8055 - tes 03-30-2016
    var ClearValue = function(e,data){
        var self = $(this);
        var field = self.parent().prev();
        field.val("");
        field.trigger('change'); //FIX ISSUE WHEN THE VALUE WAS CLEARED, THE CHANGE MUST BE APPLIED/TRIGGER
        self.parent().children('.'+prefix_selector+'-clear-value').hide();
    };
    var ToggleClearButton = function(e,data){
        var self =$(this);
        if( self.val() && !( self.is('[readonly]') || self.is('[disabled]') ) ){ // FS#8006
            self.next().children('.'+prefix_selector+'-clear-value').show();
        }else{
            self.next().children('.'+prefix_selector+'-clear-value').hide();
        }
    }
    selector.each(function(){
        var self = $(this);
        var field = self.parent().prev();
        var clear_button = $('<span data-original-title="Delete" class="'+prefix_selector+'-clear-value" style="cursor: pointer; background-color: white; display:none;position: absolute; margin-left: -20px; font-size: 15px; margin-top: -2px;"><i class="fa fa-times"></i></span>');
        if( field.is('[readonly]') || field.is('[disabled]') ){
            // return true; //FIX ISSUE WHEN THE FIELD IS formula by readonly ISSUED BY: ALVIN
        }
        self.parent().children('.'+prefix_selector+'-clear-value').remove();
        self.parent().prepend(clear_button);
        clear_button.on("click",ClearValue);
        field.on("change.ToggleClearButton",ToggleClearButton);
        // field.data("ToggleClearButton",setInterval(function(){ // FS#8006 FS#9012 you can find this on request view load js FormulaEventDistribution and readonlyformula
        //     ToggleClearButton.call(field);
        // },1000));
        if(field.val()){
            clear_button.show();
        }
        clear_button.tooltip();
    });
}

function objectCSSProperties(objID, setObj, label, field, elements){
    console.log("OBJID", objID, "Parent =>", setObj, "Label=>", label, "Field=>", field);
    var element = elements;
    var dbd = $('body').data(objID);
    var styleTag = $('<style type="text/css" class="object_css setObj_style_'+objID+'"></style>');
   //console.log("from object css prop",objID);


   $(element).each(function(){

        objID = $(this).attr('data-object-id');
        dbd = $('body').data(objID);
        
        if ($.type(dbd) == "undefined") {
            
            dbd = {};
            dbd['obj_objectCss'] = "/*Custom styling for your field container.*/ <br/> #setObject_"+objID+'{}'+ '/*Custom styling for your field label.*/#label_'+objID+'{}'+ '/*Custom styling for your field.*/#setObject_'+objID+' #getFields_'+objID+'{}';
        
        }else{
            if (!dbd['obj_objectCss']) {
                dbd['obj_objectCss'] = "/*Custom styling for your field container.*/#setObject_"+objID+'{}'+ '/*Custom styling for your field label.*/#label_'+objID+'{}'+ '/*Custom styling for your field.*/#setObject_'+objID+' #getFields_'+objID+'{}';

            }

            if ($(element).length >= 1) {
                
                dbd = $('body').data(objID);
                styleTag = $('<style type="text/css" class="object_css setObj_style_'+objID+'"></style>');
                // console.log(dbd['obj_objectCss']);
                //console.log("ITO DAPAT", $(element));

                if ($(".setObj_style_"+objID+"").length == 0) {
                    $(styleTag).appendTo('head');
                    $(".setObj_style_"+objID+"").append(dbd['obj_objectCss']);
                }else {
                    $(".setObj_style_"+objID+"").text("");
                    $(".setObj_style_"+objID+"").append(dbd['obj_objectCss']);
                }
  
            };
            console.log("DBD LAST", dbd)
        };

    });
    
    if($(".setObj_style_"+objID+"").length >= 1) {
        $(".setObj_style_"+objID+"").text("");
        $(".setObj_style_"+objID+"").append(dbd['obj_objectCss']);
    }

    var test = $('[contenteditable="true"]').text();
    console.log("contenteditable ",test)
    for(var str_index = 0 ; str_index < test.length ; str_index++ ){
        console.log(test[str_index] == "}");
    }
 
}

