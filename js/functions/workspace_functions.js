

workspace_functions = {
    view_save: function(elements) {
        $("body").on("click", elements, function() {

            var form_type = $(this).attr("data-form-type");
            var save_form_type = $(this).attr("data-form-save-type");
            var modal_title = $(this).attr("data-workspace");
            //jDialog(message, value,width, height, top, function(){});
            var save_string = save_workspace(form_type, modal_title,save_form_type);

           var newDialog = new jDialog(save_string, "", "", "", "70", function() {});
           newDialog.themeDialog("modal2");
            disableFormSettings();
             
        });

    },
    preview_form: function(elements) {
        $("body").on("click", elements, function() {

            var form_type = $(this).attr("data-form-type");
            var workspace_width = $(".workspacev2").outerWidth();
            var workspace_height = $(".workspacev2").outerHeight(); //formbuilder_ws
            var pathname = window.location.pathname;
            // Get Button
            var btnName = $(".setObject_btn").map(function() {
                return $(this).children().val();
            }).get().join();
            var user_view = $("#user_url_view").val();
            switch (form_type) {
                case "organizational_chart":
                    var workspace_content = $(".orgchart_ws").html();

                    var workspace = {};
                    workspace["WorkspaceHeight"] = workspace_height;
                    workspace["WorkspaceWidth"] = workspace_width;
                    workspace['WorkspaceContent'] = workspace_content;
                    workspace['BtnName'] = btnName;
                    var json_encode = JSON.stringify(workspace);
                    pid = json_encode;
                    workspace_width = parseInt(workspace_width) + 15;
                    //$.post('/preview', {json_encode:json_encode }, function() {
                    // var win = window.open('/workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height + '&type=orgchart', '_blank');
                    if (pathname == user_view + "organizational_chart") {
                        var win = window.open(user_view + 'workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height + '&type=orgchart', '_blank');
                    } else {
                        var win = window.open('/workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height + '&type=orgchart', '_blank');

                    }
                    win.focus();
                    break;

                case "formbuilder":
                    var workspace_content = $(".formbuilder_ws").html();

                    var workspace = {};
                    workspace["WorkspaceHeight"] = workspace_height;
                    workspace["WorkspaceWidth"] = workspace_width;
                    workspace['WorkspaceContent'] = workspace_content;
                    workspace['BtnName'] = btnName;
                    var json_encode = JSON.stringify(workspace);
                    pid = json_encode;
                    
                    // alert(user_view)
                    //$.post('/preview', {json_encode:json_encode }, function() {
                    if (pathname == user_view + "formbuilder") {
                        var win = window.open(user_view + 'workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height, '_blank');
                    } else {
                        var win = window.open('/workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height, '_blank');

                    }
                    win.focus();

                    //});
                    break;

                case "workflow":
                    var workspace_content = $(".workflow_ws").html();

                    var workspace = {};
                    workspace["WorkspaceHeight"] = workspace_height;
                    workspace["WorkspaceWidth"] = workspace_width;
                    workspace['WorkspaceContent'] = workspace_content;
                    workspace['BtnName'] = btnName;
                    var json_encode = JSON.stringify(workspace);
                    pid = json_encode;
                    workspace_width = parseInt(workspace_width) + 15;
                    //$.post('/preview', {json_encode:json_encode }, function() {
                    // var win = window.open('/workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height + '&type=flowchart', '_blank');
                    if (pathname == user_view + "workflow") {
                        var win = window.open(user_view + 'workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height + '&type=flowchart', '_blank');
                    } else {
                        var win = window.open('/workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height + '&type=flowchart', '_blank');

                    }
                    win.focus();
                    break;
                case "generate":
                    var workspace_content = $(".formbuilder_ws").html();
                    var formID = $("#FormId").val();
                    var requestID = $("#requestID").val();
                    var trackNo = $("#trackNo").val();
                    var workspace = {};
                    workspace["WorkspaceHeight"] = workspace_height;
                    workspace["WorkspaceWidth"] = workspace_width;
                    workspace['WorkspaceContent'] = workspace_content;
                    workspace['WorkspaceFormID'] = formID
                    workspace['WorkspaceRequestID'] = requestID
                    workspace['WorkspaceTrackNo'] = trackNo
                    workspace['BtnName'] = btnName;
                    var json_encode = JSON.stringify(workspace);
                    pid = json_encode;
                    // var win = window.open('/workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height + '&formID=' + formID, '_blank');
                    if (pathname == user_view + "generate") {
                        var win = window.open(user_view + 'workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height + '&formID=' + formID, '_blank');
                    } else {
                        var win = window.open('/workspace?view_type=preview&formWidth=' + workspace_width + '&formHeight=' + workspace_height + '&formID=' + formID, '_blank');

                    }
                    //$.post('/preview', {json_encode:json_encode }, function() {
                    win.focus();
                    break;
            }

            //Custom object css local storage set item
            //Roni Pinili
            $('.setObject').each(function(){
                var setOBJId = $(this).attr('data-object-id');
                var dbd = $('body').data(setOBJId);
               // var arr = [dbd['obj_objectCss']];
                //console.log("body data",dbd['obj_objectCss']);
                localStorage.setItem(setOBJId, dbd['obj_objectCss']);
            });
            
        });
    },
    save_form_workspace: function(elements) {
        $("body").on("click", elements, function() {



            var form_type = $(this).attr("data-form-type");
            // Settings
            var workspace_title = $(".workspace_title").val();
            var workspace_displayName = $(".workspace_displayName").val();
            var workspace_aliasName = $(".workspace_aliasName").val();
            var workspace_aliasNameformu = $(".workspace_aliasNameformu").val();
            var workspace_prefix = $(".workspace_prefix").val();
            var workspace_type = $(".workspace_type option:selected").val();
            var workspace_width = $(".form_size_width").val();
            var workspace_height = $(".form_size_height").val();
             
            

        });
    }
}

// Open / Close Settings on Workspace
$(function($) {
    $.fn.extend({
        formbuilder: function(options) {

            var settings = {
                type: "open", // Default action for panel settings
                auto_open: "true" // Auto Open

            };

            options = $.extend(settings, options);

            return this.each(function() {
                var o = options;
                var obj = $(this);

                // Open Close Properties   
                if (o.type == "open") {

                    $("body").on("click", ".form_settings_open", function() {
                        // Functions for open panel settings
                        var pos = {
                            pos_top_action: "-212",
                            pos_top_prop_action: "0"
                        }

                        form_settings(this, ".form_properties", ".form_properties_actions", "form_settings_open", "form_settings_close", pos);

                    });
                } else {
                    $("body").on("click", ".form_settings_close", function() {

                        // Functions for close panel settings
                        var pos = {
                            pos_top_action: "0",
                            pos_top_prop_action: "211"
                        }
                        form_settings(this, ".form_properties", ".form_properties_actions", "form_settings_close", "form_settings_open", pos);

                    });
                    //}

                }

            });
        }
    });


});
/*---------------------------------------- Functions for Form Builder ---------------------------------*/

/*
 * Functions for open / close panel
 *
 */

function form_settings(elements, panel, panel_settings, panel_actions_remove, panel_action, pos) {

    $(panel).animate({right: pos.pos_top_action}, "slow");
    $(panel_settings).animate({right: pos.pos_top_prop_action}, "slow");
    $(elements).removeClass(panel_actions_remove);
    $(elements).addClass(panel_action);

}

/*
 * Ruler
 *
 */

function dragShowHorizontalRuler(passedElement, mouseX, mouseY, container) {
    var container_of_passed_ele = $(passedElement).closest(container);
    var doi = $(passedElement).attr("data-object-id");
    if($(passedElement).find(".getFields_"+doi).length==0){
        addMY = $(passedElement).offset().top - (mouseY+1);
    }else{
        addMY = $(passedElement).find(".getFields_"+doi).eq(0).offset().top - (mouseY+1);
        
    }
    if ($(".ws-ruler-horizontal-row-guide").length >= 1) {

        $(".ws-ruler-horizontal-row-guide").css({
            top: /*passedElement.offset().top+1*/(mouseY + addMY) - container_of_passed_ele.offset().top
        });
    } else {

        tempAppendShowRulerHorizontalRowGuide = $('<span class="draggable ui-draggable ws-ruler-horizontal-row-guide"></span>');

        container_of_passed_ele.append(tempAppendShowRulerHorizontalRowGuide);

        tempAppendShowRulerHorizontalRowGuide.css({
            top: /*passedElement.offset().top+1*/(mouseY + addMY) - container_of_passed_ele.offset().top,
            left: 0
        });
    }
    ruler_h_top = $(".ws-ruler-horizontal-row-guide").offset().top - container_of_passed_ele.offset().top;
    if (ruler_h_top != (passedElement.offset().top - container_of_passed_ele.offset().top)) {
        $(".ws-ruler-horizontal-row-guide").css({
            top: /*passedElement.offset().top*/(mouseY + addMY) - container_of_passed_ele.offset().top
        });
    }
}
function dragShowVerticalRuler(passedElement, mouseX, mouseY, container) {
    var container_of_passed_ele = $(passedElement).closest(container);
    // console.log("VERTICAL RULER",passedElement,container_of_passed_ele.attr("data-object-id"))
    var doi = $(passedElement).attr("data-object-id");
    if($(passedElement).find(".getFields_"+doi).length==0){
        addMX = $(passedElement).offset().left - mouseX;
    }else{
        addMX = $(passedElement).find(".getFields_"+doi).eq(0).offset().left - mouseX;
        
    }
    // console.log("WAAARR",addMX)
    container_of_passed_ele = $(passedElement).closest(container);
    if ($(".ws-ruler-vertical-column-guide").length >= 1) {
        $(".ws-ruler-vertical-column-guide").css({
            left: /*passedElement.offset().left*/(mouseX + addMX) - container_of_passed_ele.offset().left
        });
    } else {

        tempAppendShowRulerVerticalColumnGuide = $('<span class="draggable ui-draggable ws-ruler-vertical-column-guide"></span>');

        container_of_passed_ele.append(tempAppendShowRulerVerticalColumnGuide);

        tempAppendShowRulerVerticalColumnGuide.css({
            left: (mouseX + addMX) - (container_of_passed_ele.offset().left + 0 ),
            top: 0
        });
    }
    ruler_v_left = $(".ws-ruler-vertical-column-guide").offset().left - container_of_passed_ele.offset().left;
    if (ruler_v_left != (passedElement.offset().left - container_of_passed_ele.offset().left)) {
        $(".ws-ruler-vertical-column-guide").css({
            left: /*passedElement.offset().left*/(mouseX + addMX ) - (container_of_passed_ele.offset().left + 0)
        });
    }
}
function removeRuler() {
    $(".ws-ruler-horizontal-row-guide").remove();
    $(".ws-ruler-vertical-column-guide").remove();
}



/*
 * Call Save Functions Design
 *      Via Ctrl + S
 *
 */
function validationNotification(element_affected, message) {
    if (typeof message == "undefined") {
        //catch errr
        message = '';
    }
    var this_element = $(element_affected);
    var this_element_name = $(element_affected).attr("name");
    var element_mga_naka_invi = this_element.parents(":not(:visible)");
    element_mga_naka_invi.each(function(){
        $(this).data("temporary_display", $(this).css("display"));
        $(this).show();
    });

    var element_container = this_element.parent();
    var element_container_doi = this_element.parents('.setOBJ').attr('data-object-id');
    var this_label = element_container.parents('#setObject_'+element_container_doi).children().children('#label_'+element_container_doi).find('#lbl_'+element_container_doi);
    var this_label_span = this_label.wrapInner('<span/>').find('span');
    var this_label_span_width = this_label_span.width();
    var element_ptop = this_element.parents(".setOBJ").eq(0).position().top;
    var element_pleft = this_element.parents(".setOBJ").eq(0).position().left;
    var element_pright = this_element.parents(".setOBJ").eq(0).position().left + this_element.parents(".setOBJ").eq(0).outerWidth();
    var element_pbottom = this_element.parents(".setOBJ").eq(0).position().top + this_element.parents(".setOBJ").eq(0).outerHeight();
    var noti_ele = $(
            '<div class="custom-notification" unique-attr="' + this_element_name + '" style="padding:5px;z-index:100;max-width:200px;"><div class="arrow-left"></div>' + message + '</div>'
            )
    console.log("ASDASD",this_label_span_width); 
    if(this_element.is("[type='radio']") || this_element.is("[type='checkbox']")){
        this_element.closest('.input_position_below').addClass('frequired-red');
    }
    //added by japhet morada validation for attachment files
    if(this_element.is("[data-attachment]") || this_element.is("[data-single-attachment]")){
        this_element.closest('.input_position_below').find('[type="file"]').addClass('frequired-red');
    }
    if(this_element.hasClass('smart-barcode-field-input-str-collection')){
        this_element.parents('.input_position_below').addClass('frequired-red');
    }
    //===========================
    this_element.addClass("frequired-red");
    if (element_container.children(".custom-notification[unique-attr='" + this_element_name + "']").length == 0) {
        // element_container.append(noti_ele);
        element_affected.parents(".setOBJ").eq(0).after(noti_ele);
        console.log(element_container_doi)
        var notif_position_top= $("#setObject_"+element_container_doi).css("top");
        var notif_position_left= $("#setObject_"+element_container_doi).css("left");
        var notif_object_width = $("#setObject_"+element_container_doi).css("width");
        var notif_position = parseFloat(notif_position_left) + parseFloat(notif_object_width);
        // console.log(notif_position_top)
        // console.log(notif_position_left)
        // console.log(notif_object_width)
        console.log(parseFloat(notif_position_left) + parseFloat(notif_object_width));
        $(noti_ele).css({
            // "top": (element_ptop) + "px",
            /*"left": (element_pright + 5) + "px",*/
            // "left":(this_label_span_width + 20)+'px',
            "padding": "8px",
            "color": "#ffffff",
            "text-align": "center",
            "text-decoration": "none",
            "position": "absolute",
            "-webkit-border-radius": "4px",
            "-moz-border-radius": "4px",
            "border-radius": "4px",
            "background-color": "#000000",
            "left": notif_position+'px',
            "top": notif_position_top
        });
        $(noti_ele).children(".arrow-left").eq(0).css({
            "width": "0px",
            "height": "0px",
            "border-top": "10px solid transparent",
            "border-right": "10px solid #000000",
            "position": "absolute",
            "top": "5px",
            "left": "0px",
            "margin-left": "-5px"
        });
        /**added by japhet morada
            notification inside the containers
        **/
        var parents = $(noti_ele).parents('.setOBJ').first();
        var container_names = ["table", "tab-panel", "accordion"];//list of containers if the fields is inside a container
        console.log("element:", this_element);
        if($.inArray(parents.attr('data-type'), container_names) > -1){
            if(parents.is('[data-type="table"]')){
                // console.log("element size + position", this_element.parents('td').position()['left'] + this_element.position()['left'] + $(noti_ele).position()['left'] ,"noti width", $(noti_ele).outerWidth(), "parent width", parents.width());
                if((this_element.parents('td').position()['left'] + this_element.position()['left'] + $(noti_ele).position()['left'] +  $(noti_ele).outerWidth())> parents.width()){
                    // console.log("yeahboy", (this_element.parents('.setOBJ').position()['left'] - $(noti_ele).outerWidth()));
                    $(noti_ele).css('left', (this_element.parents('.setOBJ').position()['left'] - $(noti_ele).outerWidth()) + 'px');
                    $(noti_ele).find('.arrow-left')[0].style.transform = 'rotate(90deg)';
                    $(noti_ele).find('.arrow-left').css('left', $(noti_ele).outerWidth() + 'px');
                    if(this_element.parents('.align-left-wrapper').length > 0){
                        if((this_element.parents('.setOBJ').position()['left'] + label_width - $(noti_ele).outerWidth()) <= 0){
                            $(noti_ele).css('left', '0px');//responsive width need to reposition
                            $(noti_ele).css('top', (this_element.parents('.setOBJ').position()['top']  - $(noti_ele).height())+ 'px');
                            $(noti_ele).find('.arrow-left').css('left', ($(noti_ele).outerWidth() / 2) + 'px');
                            $(noti_ele).find('.arrow-left')[0].style.transform = 'rotate(135deg)';
                            $(noti_ele).find('.arrow-left').css('top', ($(noti_ele).outerHeight() - 5) + 'px');
                        }
                        else{
                            $(noti_ele).css('left', (this_element.parents('.setOBJ').position()['left'] + label_width - $(noti_ele).outerWidth()) + 'px');//responsive width need to reposition
                        }
                    }
                    else{
                        if((this_element.parents('.setOBJ').position()['left'] - $(noti_ele).outerWidth()) <= 0){
                            $(noti_ele).css('left', '0px');//responsive width need to reposition
                            $(noti_ele).css('top', (this_element.parents('.setOBJ').position()['top']  - $(noti_ele).height())+ 'px');
                            $(noti_ele).find('.arrow-left').css('left', ($(noti_ele).outerWidth() / 2) + 'px');
                            $(noti_ele).find('.arrow-left')[0].style.transform = 'rotate(135deg)';
                            $(noti_ele).find('.arrow-left').css('top', ($(noti_ele).outerHeight() - 5) + 'px');
                        }
                        else{
                            $(noti_ele).css('left', (this_element.parents('.setOBJ').position()['left'] - $(noti_ele).outerWidth()) + 'px');//responsive width need to reposition
                        }
                    }
                }
                $('.loaded_form_content').prepend($(noti_ele));
                var top = parents.position()['top'] + parents.find('.input_position_below').position()['top'] +  this_element.parents('td').not('.picklist-td').position()['top'] +  this_element.position()['top'];
                var left = parents.position()['left'] + this_element.parents('td').not('.picklist-td').position()['left'] + this_element.position()['left'] + this_element.outerWidth();
                $(noti_ele).css({'top': top + 'px', 'left': left + 'px'});
            }
            else{
                // console.log("element size + position", ($(noti_ele).position()['left'] + $(noti_ele).outerWidth()),"noti width", $(noti_ele).outerWidth(), "parent width", parents.width());        
                if(($(noti_ele).position()['left'] + $(noti_ele).outerWidth()) > parents.width()){
                    var getOldWidth = ($(noti_ele).outerWidth());
                    var label_width = this_element.parents('.input_position_below').eq(0).prev().width();
                    $(noti_ele).css('left', (this_element.parents('.setOBJ').position()['left']) + 'px');
                    $(noti_ele).find('.arrow-left')[0].style.transform = 'rotate(90deg)';
                    $(noti_ele).find('.arrow-left').css('left', $(noti_ele).outerWidth() + 'px');
                    if(this_element.parents('.align-left-wrapper').length > 0){
                        if((this_element.parents('.setOBJ').position()['left'] + label_width - $(noti_ele).outerWidth()) <= 0){
                            $(noti_ele).css('left', '0px');//responsive width need to reposition
                            $(noti_ele).css('top', (this_element.parents('.setOBJ').position()['top']  - $(noti_ele).height())+ 'px');
                            $(noti_ele).find('.arrow-left').css('left', ($(noti_ele).outerWidth() / 2) + 'px');
                            $(noti_ele).find('.arrow-left')[0].style.transform = 'rotate(135deg)';
                            $(noti_ele).find('.arrow-left').css('top', ($(noti_ele).outerHeight() - 5) + 'px');
                        }
                        else{
                            $(noti_ele).css('left', (this_element.parents('.setOBJ').position()['left'] + label_width - $(noti_ele).outerWidth()) + 'px');//responsive width need to reposition
                        }
                    }
                    else{
                        if((this_element.parents('.setOBJ').position()['left'] - $(noti_ele).outerWidth()) <= 0){
                            $(noti_ele).css('left', '0px');//responsive width need to reposition
                            $(noti_ele).css('top', (this_element.parents('.setOBJ').position()['top']  - $(noti_ele).height())+ 'px');
                            $(noti_ele).find('.arrow-left').css('left', ($(noti_ele).outerWidth() / 2) + 'px');
                            $(noti_ele).find('.arrow-left')[0].style.transform = 'rotate(135deg)';
                            $(noti_ele).find('.arrow-left').css('top', ($(noti_ele).outerHeight() - 5) + 'px');
                        }
                        else{
                            $(noti_ele).css('left', (this_element.parents('.setOBJ').position()['left'] - $(noti_ele).outerWidth()) + 'px');//responsive width need to reposition
                        }
                    }
                }
            }
        }
        //
        var mouse_entered = "not yet";
        noti_ele.hide();
        noti_ele.on({
            "mouseenter": function() {
                if (mouse_entered == "not yet") {
                    noti_ele.fadeOut(2500, function() {
                        $(this).remove();
                    });
                    mouse_entered = "yes";
                }
            }
        });
        //added by japhet morada validation for attachment files
        if(this_element.is("[data-attachment]") || this_element.is("[data-single-attachment]")){
            this_element.closest('.input_position_below').find('[type="file"]').on({
                "mouseenter": function() {
                    $(this).removeClass("frequired-red");
                    $(this).closest('.input_position_below').removeClass('frequired-red');
                    if (noti_ele.length >= 1) {
                        noti_ele.fadeIn(1000, function() {
                            noti_ele.fadeOut(5000,function(){
                                $(this).remove();
                            })
                        });
                    }
                },
            });
        }
        if(this_element.hasClass('smart-barcode-field-input-str-collection')){
            this_element.closest('.input_position_below').on({
                "mouseenter": function() {
                    $(this).removeClass("frequired-red");
                    $(this).closest('.input_position_below').removeClass('frequired-red');
                    if (noti_ele.length >= 1) {
                        noti_ele.fadeIn(1000, function() {
                            noti_ele.fadeOut(5000,function(){
                                $(this).remove();
                            })
                        });
                    }
                },
            });
        }
        this_element.on({
            "mouseenter": function() {
                $(this).removeClass("frequired-red");
                $(this).closest('.input_position_below').removeClass('frequired-red');
                if (noti_ele.length >= 1) {
                    noti_ele.fadeIn(1000, function() {
                        noti_ele.fadeOut(5000,function(){
                            $(this).remove();
                        })
                    });
                }
            },
            
        });
        
    }
    element_mga_naka_invi.each(function(){
        $(this).css("display", $(this).data("temporary_display") );
    });
    return $(noti_ele);
}
function control_save_workspace(elements) {
    //handles ctrl-s
    $(document).keydown(function(event) {

        var pathname = window.location.pathname;
        var user_view = $("#user_url_view").val();
        if (pathname.indexOf("organizational_chart") >= 0 || pathname.indexOf("formbuilder") >= 0 || pathname.indexOf("workflow") >= 0 || pathname.indexOf("report") >= 0 || pathname.indexOf("generate") >= 0
                || pathname == user_view + "create_forms" || pathname.indexOf("rule_settings") >= 0) {
            var currKey = 0, e = e || event;
            currKey = e.keyCode || e.which || e.charCode;  //do this handle FF and IE
            if (!(String.fromCharCode(event.which).toLowerCase() == 's' && event.ctrlKey) && !(event.which == 19))
                return true;
            event.preventDefault();

            var form_type = $(elements).attr("data-form-type");
            var modal_title = $(elements).attr("data-workspace");

            if (!modal_title) {
                if($('.idecontent').length >=1){
                    $('.idesave').trigger('click');
                }
                else{
                       if($('#popup_overlay').exists()){
                            return false;
                        }
                    var save_string = save_workspace(form_type, modal_title);
                    //jDialog(message, value,width, height, top, function(){});
                    var newDialog = new  jDialog(save_string, "", "", "", "70", function() {
                    });

                    newDialog.themeDialog("modal2");

                    disableFormSettings();
                }
                return ;
            }
            /* Save Functions Design */
            if($('.idecontent').length >=1){
                $('.idesave').trigger('click');
            }
            else{
                   if($('#popup_overlay').exists()){
                        return false;
                    }
                var save_string = save_workspace(form_type, modal_title);
                //jDialog(message, value,width, height, top, function(){});
                var newDialog = new  jDialog(save_string, "", "", "", "70", function() {
                });

                newDialog.themeDialog("modal2");

                disableFormSettings();
            }
           
            /*ide caller*/
           
            return false;
        }


    });
}


/*
 * Save Functions Design
 *      Containing (Organizational Chart, Formbuilder, Workflow);
 *      @form_type = validation of save functions details
 *      @modal_title = set the title of save properties modal
 *
 */

function save_workspace(form_type, modal_title,save_type) {
    var json = $("body").data();
    var user_view = $("#user_url_view").val();

    var ret = '';
    var disabled = "";
    var formTitleSuffix = "";
    var pathname = window.location.pathname;
    if (pathname == "/generate" || pathname != user_view + "formbuilder") {
        disabled = "disabled=disabled";
    }
    if (pathname == user_view + "organizational_chart" || pathname == "/organizational_chart") {
        disabled = "";
    }
    if (pathname == user_view + "workflow" || pathname == "/workflow") {
        disabled = "";
    }
    if (pathname == "/formbuilder") {
        disabled = "";
    }
    if (pathname == user_view + "report" || pathname == "/report") {
        disabled = "";
    }
    if (pathname == user_view + "generate" || pathname == "/generate") {
        disabled = "disabled=disabled";
    }
    ret += '<h3 class="fl-margin-bottom">';
    ret += '<i class="fa fa-save"></i> ' + modal_title;
    ret += '</h3>';
    ret += '<div class="hr"></div>';
    ret += '<div class="fields">';
    ret += '<div class="label_below2 display"> Details: </div>';
    if (form_type == "formbuilder") {
        //for save as draft of form
        if(save_type=="2"){
            formTitleSuffix = " Copy";
        }
        var form_id = $("#FormId").val();
        var form_logo = $("body").data("form_logo");
        var logo = '/images/formLogo/' + MD5(MD5(form_id)) + '/small.png';
        //disabled logo
        ret += '<center class="display"><img data-placement="bottom" data-original-title="" src="' + logo + '" width="30" height="30" class="avatar userAvatar form_logo" onerror="this.src='+ $('#broken_image').text() +'"></center>';
    }
    //    if (form_type=="formbuilder") {
    //    ret += '<div class="input_position" style="text-align:center;">';
    //  ret += '<img data-original-title="" src="http://eforms.gs3.com.ph:80/images/avatar/small.png" width="44" height="44" class="avatar userAvatar">';
    //    ret += '</div>';
    //    }
    ret += '</div>';

    ret += '<div class="fields">';
    ret += '<div class="input_position section clearing fl-field-style">';
    ret += '<div class="column div_1_of_1">';
    ret += '<span> Title:<font color="red">*</font> </span>';
    if (pathname == user_view + "generate" || pathname == "/generate") {
         // For Generate Report
        var form_name = $(".form-workspace-form-name").html();
        ret += '<input type="text" name="workspace_title" ' + disabled + ' id="workspace_title" class="workspace_title form-text fl-margin-bottom" style="margin-top:5px;" value="' + $.trim(form_name) + '">';
    }else{
        console.log(form_type)
        if(form_type == "formbuilder"){
            if(getURLParameter("formID") == 0){
                ret += '<input type="text" name="workspace_title" ' + disabled + ' id="workspace_title" class="workspace_title form-text fl-margin-bottom" style="margin-top:5px;" value="' + $.trim(if_undefinded(json.workspace_title+formTitleSuffix, "")) + '">';            
            }
            else if(save_type=="2"){
                ret += '<input type="text" name="workspace_title" ' + disabled + ' id="workspace_title" class="workspace_title form-text fl-margin-bottom" style="margin-top:5px;" value="' + $.trim(if_undefinded(json.workspace_title+formTitleSuffix, "")) + '">';
            }else{
                ret += '<input type="text" hidden name="workspace_title" ' + disabled + ' id="workspace_title" class="workspace_title form-text fl-margin-bottom" style="margin-top:5px;" value="' + $.trim(if_undefinded(json.workspace_title+formTitleSuffix, "")) + '">';
                ret += '<br><br><label type="text" name="workspace_title" ' + disabled + ' class="workspace_title form-text fl-margin-bottom" style="margin-top:5px;margin-left:7px;background-color:#EFEFEF;">' + $.trim(if_undefinded(json.workspace_title+formTitleSuffix, "")) + '</label>';
            }
        }else{
            ret += '<input type="text" name="workspace_title" ' + disabled + ' id="workspace_title" class="workspace_title form-text fl-margin-bottom" style="margin-top:5px;" value="' + $.trim(if_undefinded(json.workspace_title+formTitleSuffix, "")) + '">';
        }
    }
        ret += '</div>';
    ret += '</div>';
    ret += '</div>';

    // For Organizatioanl Chart And Workflow Description
    if (form_type != "formbuilder") {
        ret += '<div class="fields">';
        ret += '<div class="input_position section clearing fl-field-style">';
            ret += '<div class="column div_1_of_1">'
                ret += '<span> Description: </span>';
        var workspace_display_name = json['form_description'];
        if(form_type=="workflow"){
            workspace_display_name = json['workspace_description'];
        }
        ret += '<textarea class=" form-textarea form-textarea_save workspace_description fl-margin-bottom" name="workspace_description" id="workspace_description" style="resize:none">' + $.trim(if_undefinded(workspace_display_name, "")) + '</textarea>';
            ret += '</div>';
        ret += '</div>';
        ret += '</div>';
    }

    if (form_type == "formbuilder" || form_type == "generate") {

        /*
         * Workspace formbuilder
         */
         if (form_type != "generate") {
        ret += '<div class="fields">';
        
        ret += '<div class="input_position clearing section fl-field-style">';
            ret += '<div class="column div_1_of_1">';
            ret += '<span> Form Alias: </span>';
         // For Generate Report
            //var form_name = $(".form-workspace-form-name").html();
            var workspace_aliasName = json.workspace_aliasName;
            
            ret += '<input type="text" name="workspace_aliasName" id="workspace_aliasName" class="fl-margin-bottom workspace_aliasName form-text class" placeholder="(Optional)" style="margin-top:5px;" value="' + $.trim(if_undefinded(workspace_aliasName,"")) + '">';
        }else{
            //ret += '<input type="text" name="workspace_displayName" id="workspace_displayName" class="fl-margin-bottom workspace_displayName form-text class" style="margin-top:5px;" value="' + if_undefinded(json.workspace_displayName, "") + '">';
        }
            ret += '</div>';
        ret += '</div>';
        ret += '</div>';
                                    //   var workspace_enableformu = json.Enableformu; 
                                    //    if(workspace_enableformu == "1"){
                                    //         ret += '<div class="section clearing fl-field-style">';
                                    //         ret += '<div class="column div_1_of_1">';
                                    //             //ret += '<label><input type="checkbox" name="enableFilters" '+ setChecked("1",enableFilters) +' class="css-checkbox" value="1" id="enableFilters"><label for="enableFilters" class="css-label"></label> Enable Advanced Filters</label>'; 
                                    //             ret += '<label><input type="checkbox" name="enableFormula"  class="css-checkbox" value="1" checked="checked"  id="enableFormula"><label for="enableFormula" class="css-label"></label> Enable Alias Formula</label>'; 
                                    //             ret += '<label><input type="checkbox" name="enableField"  class="css-checkbox" value="1" checked="checked"  id="enableField"><label for="enableField" class="css-label"></label> Field Formula Enabled</label>'; 
                                    //         ret += '</div>';
                                    //     ret += '</div>';
                                    // }else{
                                    //     ret += '<div class="section clearing fl-field-style">';
                                    //         ret += '<div class="column div_1_of_1">';
                                    //             //ret += '<label><input type="checkbox" name="enableFilters" '+ setChecked("1",enableFilters) +' class="css-checkbox" value="1" id="enableFilters"><label for="enableFilters" class="css-label"></label> Enable Advanced Filters</label>'; 
                                    //             ret += '<label><input type="checkbox" name="enableFormula"  class="css-checkbox" value="1" checked="checked" id="enableFormula"><label for="enableFormula" class="css-label"></label> Enable Alias Formula</label>';
                                    //             ret += '<label><input type="checkbox" name="enableField"  class="css-checkbox" value="1"  id="enableField"><label for="enableField" class="css-label"></label> Field Formula Enabled</label>';  
                                    //         ret += '</div>';
                                    //     ret += '</div>';
                                    // }
                                    //==================================================================================== 
                                        console.log(json)
                                        var workspace_enableformu = json.Enableformu;
                                        var workspace_enablefield = json.Enablefield;
                                        if(workspace_enableformu == "1" && workspace_enablefield == "1"){
                                            ret += '<div class="section clearing fl-field-style">';
                                            ret += '<div class="column div_1_of_1">';
                                                //ret += '<label><input type="checkbox" name="enableFilters" '+ setChecked("1",enableFilters) +' class="css-checkbox" value="1" id="enableFilters"><label for="enableFilters" class="css-label"></label> Enable Advanced Filters</label>'; 
                                                ret += '<label><input type="checkbox" name="enableFormula"  class="css-checkbox" value="1" checked="checked"  id="enableFormula"><label for="enableFormula" class="css-label"></label> Enable Alias Formula</label>'; 
                                                ret += '<label><input type="checkbox" name="enableField"  class="css-checkbox" value="1" checked="checked"  id="enableField"><label for="enableField" class="css-label"></label> Field Formula Enabled</label>'; 
                                            ret += '</div>';
                                        ret += '</div>';
                                    }else if(workspace_enableformu == "1" && workspace_enablefield == "0"){
                                        ret += '<div class="section clearing fl-field-style">';
                                            ret += '<div class="column div_1_of_1">';
                                                //ret += '<label><input type="checkbox" name="enableFilters" '+ setChecked("1",enableFilters) +' class="css-checkbox" value="1" id="enableFilters"><label for="enableFilters" class="css-label"></label> Enable Advanced Filters</label>'; 
                                                ret += '<label><input type="checkbox" name="enableFormula"  class="css-checkbox" value="1" checked="checked" id="enableFormula"><label for="enableFormula" class="css-label"></label> Enable Alias Formula</label>';
                                                ret += '<label><input type="checkbox" name="enableField"  class="css-checkbox" value="1"  id="enableField"><label for="enableField" class="css-label"></label> Field Formula Enabled</label>';  
                                            ret += '</div>';
                                        ret += '</div>';
                                    }else if(workspace_enableformu == "0" && workspace_enablefield == "1"){
                                        ret += '<div class="section clearing fl-field-style">';
                                            ret += '<div class="column div_1_of_1">';
                                                //ret += '<label><input type="checkbox" name="enableFilters" '+ setChecked("1",enableFilters) +' class="css-checkbox" value="1" id="enableFilters"><label for="enableFilters" class="css-label"></label> Enable Advanced Filters</label>'; 
                                                ret += '<label><input type="checkbox" name="enableFormula"  class="css-checkbox" value="1"  id="enableFormula"><label for="enableFormula" class="css-label"></label> Enable Alias Formula</label>';
                                                ret += '<label><input type="checkbox" name="enableField"  class="css-checkbox" value="1" checked="checked" id="enableField"><label for="enableField" class="css-label"></label> Field Formula Enabled</label>';  
                                            ret += '</div>';
                                        ret += '</div>';
                                    }else{
                                        ret += '<div class="section clearing fl-field-style">';
                                            ret += '<div class="column div_1_of_1">';
                                                //ret += '<label><input type="checkbox" name="enableFilters" '+ setChecked("1",enableFilters) +' class="css-checkbox" value="1" id="enableFilters"><label for="enableFilters" class="css-label"></label> Enable Advanced Filters</label>'; 
                                                ret += '<label><input type="checkbox" name="enableFormula"  class="css-checkbox" value="1"  id="enableFormula"><label for="enableFormula" class="css-label"></label> Enable Alias Formula</label>';
                                                ret += '<label><input type="checkbox" name="enableField"  class="css-checkbox" value="1" id="enableField"><label for="enableField" class="css-label"></label> Field Formula Enabled</label>';  
                                            ret += '</div>';
                                        ret += '</div>';
                                    }
                                        


        if (form_type != "generate") {  
        ret += '<div class="fields">';
        
        ret += '<div class="input_position clearing section fl-field-style">';
            ret += '<div class="column div_1_of_1">';
            ret += '<span> Form Alias Formula: </span>';
         // For Generate Report
            //var form_name = $(".form-workspace-form-name").html();
            var workspace_aliasNameformu = json.Aliasformu;
                        ret += '<textarea name="workspace_aliasNameformu" id="workspace_aliasNameformu" class="fl-margin-bottom workspace_aliasNameformu form-textarea class ide-hover" data-ide-properties-type="form-alias-formula" placeholder="Alias Formula must be Enabled" style="margin-top:5px;">' + $.trim(if_undefinded(workspace_aliasNameformu,"")) + '</textarea>';
        }else{
            //ret += '<input type="text" name="workspace_displayName" id="workspace_displayName" class="fl-margin-bottom workspace_displayName form-text class" style="margin-top:5px;" value="' + if_undefinded(json.workspace_displayName, "") + '">';
        }
         ret += '</div>';
        ret += '</div>';
        ret += '</div>';

        if (form_type != "generate") {
        ret += '<div class="fields">';
        
        ret += '<div class="input_position clearing section fl-field-style">';
            ret += '<div class="column div_1_of_1">';
            ret += '<span> Description:<font color="red">*</font> </span>';
         // For Generate Report
            //var form_name = $(".form-workspace-form-name").html();
            var workspace_display_name = json.workspace_displayName;
            ret += '<input type="text" name="workspace_displayName" id="workspace_displayName" class="fl-margin-bottom workspace_displayName form-text class" style="margin-top:5px;" value="' + $.trim(if_undefinded(workspace_display_name,"")) + '">';
        }else{
            //ret += '<input type="text" name="workspace_displayName" id="workspace_displayName" class="fl-margin-bottom workspace_displayName form-text class" style="margin-top:5px;" value="' + if_undefinded(json.workspace_displayName, "") + '">';
        }
            ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        if (form_type != "generate") {
        ret += '<div class="fields">';
        ret += '<div class="input_position section clearing fl-field-style">';
            ret += '<div class="column div_1_of_1">';
                ret += '<span> Button Name:<font color="red">*</font> </span>';
                    var bttn_name = json.workspace_buttonName;
                    if(bttn_name == undefined || bttn_name == ""){
                         bttn_name = "Create Request";
                    }else{
                         bttn_name = $.trim(json.workspace_buttonName);
                    }
                    // ' + $.trim(if_undefinded(json.workspace_buttonName), "Create Request") + '
                ret += '<input type="text" name="button_name" id="button_name" class="fl-margin-bottom button_name form-text class" style="margin-top:5px;" value="'+ bttn_name +'">';
            ret += '</div>';
            ret += '</div>';
        console.log("BART", json);
        ret += '</div>';
        }
        if (form_type != "generate") {
            ret += '<div class="fields  display">';
            ret += '<div class="label_below2"> Form Category: </div>';
            ret += '<div class="input_position">';
            ret += '<select class="form-select form_category fl-margin-bottom" name="form_category" id="form_category" style="margin-top:5px; margin-bottom:5px;">';
            ret += getFormCategory();
            ret += '</select>';
            ret += '</div>';
            ret += '</div>';
            ret += '<div class="fields display">';
            ret += '<div class="label"> Reference Format:</div>';
            ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="input_position section clearing fl-field-style">';
                ret += '<div class="column div_1_of_1">';
                    ret += '<span> Prefix:<font color="red">*</font></span>';
                    ret += '<input type="text" name="workspace_prefix" value="' + $.trim(if_undefinded(json.workspace_prefix, "")) + '" id="workspace_prefix" class="workspace_prefix form-text fl-margin-bottom" style="margin-top:5px;">';
                ret += '</div>';
            ret += '</div>';
            ret += '</div>';
            ret += '<div class="fields display">';
                var dos_display_type = if_undefinded(json['fm-type-dos'],0);
                var dos_formula = if_undefinded(json['form_display-dynamic'],"");
                var dos_dynamic = "";var dos_static = "";
                if(dos_display_type==0){
                    dos_dynamic = "display:none;";
                }else{
                    dos_static = "display:none;";
                }
                // dos_dynamic = "display:none;";
                ret += '<div class="formulaContainer" style="width:100%;">';
                    ret += '<div class="fields_below">';
                        ret += '<div class="clearfix"></div>';
                        ret += '<div class="" style="float:right;margin: 5px 0px;font-size: 10px;font-weight: bold;">';
                        ret += '    <label>Static: <input type="radio" name="fm-type" '+ setChecked("0",dos_display_type) +' class="fm-type-dos formulaType css-checkbox" value="0" id="fm-type0-dos"><label for="fm-type0-dos" class="css-label"></label></label>';
                        ret += '    <label>Dynamic: <input type="radio" name="fm-type" '+ setChecked("1",dos_display_type) +' class="fm-type-dos formulaType css-checkbox" value="1" id="fm-type1-dos"><label for="fm-type1-dos" class="css-label"></label></label>';
                        ret += '</div>';
                        ret += '<div class="label_below2">Display On Side Bar: </div>';
                        ret += '<div class="clearfix"></div>';
                        ret += '<div class="input_position_below dynamic-dos" style="text-align: right;'+ dos_dynamic +'">';
                            ret += '<textarea name="" class="form-text tip wfSettings" title="" id="form_display-dynamic" data-ide-properties-type="form-display-dynamic" placeholder="">'+ dos_formula +'</textarea>';
                            // ret += '<div style="float:left;font-size:11px;color:red">Note: If no notification message is defined, the system will send the default message: (Request for process: Tracking No. XXX)</div>';
                        ret += '</div>';
                        ret += '<div class="input_position static-dos" style="text-align: right;'+ dos_static +'">';
                            ret += '<select class="form-select form_display_type fl-margin-bottom static-dos" name="form_display_type" id="form_display_type" style="margin-top:5px;">';
                                ret += '<option ' + setSelected("1", if_undefinded(json.form_display_type, "")) + ' value="1">Yes</option>';
                                ret += '<option ' + setSelected("0", if_undefinded(json.form_display_type, "")) + ' value="0">No</option>';
                            ret += '</select>';
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="input_position section clearing fl-field-style">';
                ret += '<div class="column div_1_of_1">';
                    ret += '<span> Display Import Button: <font color="red">*</font> </span>';
                    ret += '<select class="form-select display_import fl-margin-bottom" name="display_import" id="display_import" style="margin-top:5px;">';
                    ret += '<option ' + setSelected("1", if_undefinded(json.display_import, "")) + ' value="1">Yes</option>';
                    ret += '<option ' + setSelected("0", if_undefinded(json.display_import, "")) + ' value="0">No</option>';
                    ret += '</select>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="input_position section clearing fl-field-style">';
                ret += '<div class="column div_1_of_1">';
                    ret += '<span> Tracking Number Type: <font color="red">*</font> </span>';
                    ret += '<select class="form-select workspace_type fl-margin-bottom" name="workspace_type" id="workspace_type" style="margin-top:5px;">';
                    ret += '<option ' + setSelected("Sequential", if_undefinded(json.workspace_type, "")) + '>Sequential</option>';
                    ret += '<option ' + setSelected("DateTime", if_undefinded(json.workspace_type, "")) + '>DateTime</option>';
                    ret += '</select>';
                ret += '</div>';
            ret += '</div>';
//            var hideWorkspaceStatus = "";
            var enable_staging = $("#enable_staging").text();
            var workspace_version = if_undefinded(json.workspace_version, "1");
            if(save_type=="2" && enable_staging=="1"){
                
                ret += '<div class="fields">';
                ret += '<div class="input_position section clearing fl-field-style">';
                    ret += '<div class="column div_1_of_1">';
                        ret += '<span> Version: <font color="red">*</font> </span>';
                        ret += '<select class="form-select workspace_version fl-margin-bottom" name="workspace_version" id="workspace_version" style="margin-top:5px;">';
                        ret += '<option ' + setSelected("1", workspace_version) + ' value="1">Production</option>';
                        ret += '<option ' + setSelected("2", workspace_version) + ' value="2">Staging</option>';
                        ret += '</select>';
                    ret += '</div>';
                ret += '</div>';
            }
//            if(workspace_version=="2"){
//                hideWorkspaceStatus = "display:none;";
//            }
//            ret += '<div class="fields workspace_status_container" style="'+ hideWorkspaceStatus +'">';
//            ret += '<div class="label_below2"> Form Type: <font color="red">*</font> </div>';
//            ret += '<div class="input_position">';
//            ret += '<select class="form-select workspace_status fl-margin-bottom" name="workspace_status" id="workspace_status" style="margin-top:5px;">';
//            ret += '<option ' + setSelected("1", if_undefinded(json.workspace_status, "")) + ' value="1">Published</option>';
//            ret += '<option ' + setSelected("2", if_undefinded(json.workspace_status, "")) + ' value="2">Template</option>';
//            ret += '</select>';
//            ret += '</div>';
//            ret += '</div>';
            /* my request visibility */
            var my_request_custom_view = $("#my_request_custom_view").text();
            if(my_request_custom_view=="1"){
                ret += '<div class="fields">';
                ret += '<div class="input_position section clearing fl-field-style">';
                    ret += '<div class="column div_1_of_1">';
                        ret += '<span> Display in My Request: <font color="red">*</font> </span>';
                        ret += '<select class="form-select myrequest_visibility fl-margin-bottom" name="myrequest_visibility" id="myrequest_visibility" style="margin-top:5px;">';
                            ret += '<option ' + setSelected("0", if_undefinded(json.myrequest_visibility, "")) + ' value="0">No</option>';
                            ret += '<option ' + setSelected("1", if_undefinded(json.myrequest_visibility, "")) + ' value="1">Yes</option>';
                        ret += '</select>';
                    ret += '</div>';
                ret += '</div>';
            }
        }

        //ret += '<input type="text" name="" id="" class=" form-text" style="margin-top:5px;">';
        ret += '</div>';
        ret += '</div>';
    } else {
        if (form_type == "organizational_chart" || form_type == "workflow") {
            /*
             * Workspace Organizational Chart
             */
            ret += '<div class="fields">';
            ret += '<div class="label_below2"> Status: <font color="red">*</font></div>';
            ret += '<div class="input_position fl-margin-bottom">';
            ret += '<select class="form-text workspace_status" name="workspace_status" id="workspace_status" style="margin-top:5px;">';
            // ret += '<option value="null">----------------------------Select----------------------------</option>';
            ret += '<option value="1">Active</option>';
            ret += '<option value="0">Inactive</option>';
            if(form_type=="workflow"){
                ret += '<option value="2">Draft</option>';
            }
            ret += '</select>';
            ret += '</div>';
            ret += '</div>';
        }
    }

    /*
     * Modal Action Button
     * Save / Cancel
     *
     */

    ret += '<div class="fields">';
    ret += '<div class="label_below2"></div>';
    ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
    ret += '<img src="/images/loader/load.gif" class="display saveFormLoad" style="margin-right:5px;margin-top:2px;"/>';
    ret += '<input type="button" class="btn-blueBtn fl-margin-right save_form_workspace" data-form-type="' + form_type + '" data-form-save-type="' + save_type + '" id="save_form_workspace" value="Save">     ';
    ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
    ret += '</div>';
    ret += '</div>';
    
    brokenImage(); // Change broken image into white image
    return ret;
}
function disableFormSettings() {
    // var pathname = window.location;
    // if(getParametersName("view_type",pathname)=="edit"){
    //  $("#workspace_title").attr("disabled","disabled");
    //  $("#workspace_displayName").attr("disabled","disabled");
    //  $("#workspace_prefix").attr("disabled","disabled");
    //  $("#workspace_type").attr("disabled","disabled");
    // }
}

// function getFormCategory() {
//     var json = $("body").data();
//     // console.log(json)
//     var ret = ""
//     ret += '<option value="0">----------------------------Select----------------------------</option>';
//     $.ajax({
//         type: "POST",
//         url: "/ajax/formbuilder",
//         data: {action: "getCategory"},
//         // dataType: "json",
//         success: function(data) {
            
//             if(data && data!="null"){
//                 data = JSON.parse(data);
//                 $.each(data, function(id, val) {
//                     ret += '<option value="' + data[id].category_id + '" ' + setSelected(json['form_category'], data[id].category_id) + '>' + data[id].category_name + '</option>';
//                 });
//             }
//          $(".form_category").append(ret);
//         }
//     });
// }
function getFormCategory() {
    var json = $("body").data();
    var categoryies = $(".form_categories").text();
    var ret = ""
    ret += '<option value="0">----------------------------Select----------------------------</option>';
    if(categoryies=="" || categoryies=="null"){
        return;
    }
    var data = JSON.parse(categoryies);
    $.each(data, function(id, val) {
        ret += '<option value="' + data[id].category_id + '" ' + setSelected(json['form_category'], data[id].category_id) + '>' + data[id].category_name + '</option>';
    });
    return ret;
}

RequestPivacy = {
    "authors": function() {
        ui.block()
        var authors = $(".form-user-authors_original").text();
        try{
            UserAccessFlagCheckBox.originalChecked = JSON.parse(authors);
        }catch(err){
            console.log(authors);
            console.log(err)
        }
        //alert("authors")
        var newDialog = new jDialog(this.setDialog("Form Authors", "form-authors", "", "", "faiconColor", "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-form-authors'></use></svg>"), "", "600", "", "40", function() {
        });
        newDialog.themeDialog("modal2");
        this.otherAttributes();
        ui.unblock();
    },
    "users" : function(){
        ui.block()
        var users = $(".form-user-accessor_original").text();
        try{
            UserAccessFlagCheckBox.originalChecked = JSON.parse(users);
        }catch(err){
            console.log(users);
            console.log(err)
        }

        var newDialog = new jDialog(this.setDialog("Form Users", "form-users", "", "", "faiconColor", "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-form-users'></use></svg>"), "", "600", "", "40", function() {
        });
        newDialog.themeDialog("modal2");
        this.otherAttributes();
        ui.unblock();
    },
    "readers": function() {
        ui.block()
        //alert("readers")
        var viewers = $(".form-user-viewers_original").text();
        try{
            UserAccessFlagCheckBox.originalChecked = JSON.parse(viewers);
        }catch(err){
            console.log(viewers);
            console.log(err)
        }

        var note = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Note: Requestor and Current Processor can automatically view the request.";
        var newDialog = new jDialog(this.setDialog("Request Viewer", "form-readers", note, "", "faiconColor", "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-form-viewers'></use></svg>"), "", "600", "", "40", function() {
        });
         newDialog.themeDialog("modal2");
        this.otherAttributes();
        ui.unblock();
    },
    "admin": function() {
        ui.block()
        var admin = $(".form-user-admin_original").text();
        try{
            UserAccessFlagCheckBox.originalChecked = JSON.parse(admin);
        }catch(err){
            console.log(admin);
            console.log(err)
        }

        var note = "Note: You are not in the list because you are automatically an admin of this form.";
        var newDialog = new jDialog(this.setDialog("Form Admin", "form-admin", note, "", "faiconColor", "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-form-admin'></use></svg>"), "", "600", "", "40", function() {
        });
        newDialog.themeDialog("modal2");
        this.otherAttributes();
        ui.unblock();
    },
    "customizedPrint": function() {
        ui.block()
        var customizedPrint = $(".form-custom-print-users_original").text();
        try{
            UserAccessFlagCheckBox.originalChecked = JSON.parse(customizedPrint);
        }catch(err){
            console.log(customizedPrint);
            console.log(err)
        }

        var newDialog = new jDialog(this.setDialog("Customized Print Author", "customized-print", "", "", "faiconColor", "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-customized-print'></use></svg>"), "", "600", "", "40", function() {
        });
        newDialog.themeDialog("modal2");
        this.otherAttributes();
        ui.unblock();
    },
    "reportUsers": function() {
        ui.block()
        var reportUsers = $(".form-report-users").text();
        try{
            // json['report-users'] = JSON.parse(reportUsers);
            UserAccessFlagCheckBox.originalChecked = JSON.parse(reportUsers);
            // UserAccessFlagCheckBox.
        }catch(err){
            console.log(reportUsers);
            console.log(err)
        }
        var newDialog = new jDialog(this.setDialog("Report Users", "report-users", "", "fa fa-user", "faiconColor", "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-report-setreportuser'></use></svg>"), "", "600", "", "40", function() {
        });
        newDialog.themeDialog("modal2");
        this.otherAttributes();
        ui.unblock();
    },
    "navViewers": function() {
        // var navViewers = $(".form-nav-access").text();
        // try{
        //     UserAccessFlagCheckBox.originalChecked = JSON.parse(navViewers);
        // }catch(err){
        //     console.log(navViewers);
        //     console.log(err)
        // }
        // var newDialog = new jDialog(this.setDialog("Navigation Viewers", "nav-viewers", "", "fa fa-users", "faiconColor"), "", "600", "", "40", function() {
        // });
        // newDialog.themeDialog("modal2");
        // this.otherAttributes();
    },
    "deleteAccess": function() {
        ui.block()
        var json = $("body").data();
        var deleteAccess = $(".form-delete-access-json_original").text();
        try{
            UserAccessFlagCheckBox.originalChecked = JSON.parse(deleteAccess);
        }catch(err){
            console.log(deleteAccess);
            console.log(err)
        }
        var newDialog = new jDialog(this.setDialog("User Delete Access", "delete-access", "", "", "faiconColor", "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-delete-access'></use></svg>"), "", "600", "", "40", function() {
        });
        newDialog.themeDialog("modal2");
        this.otherAttributes();
        var ret = "<div>";
            ret += '<div class="fields section clearing fl-field-style">';
                ret += '<div class="input_position column div_1_of_1">';
                    ret += '<span class="font-bold"><label><input type="checkbox" name="deletion_control" class="css-checkbox" value="1" id="del_control"><label for="del_control" class="css-label"></label> Enable Deletion</label></span>';
                ret += '</div>';
            ret += '</div>';
            ret += '<div class="fields section clearing fl-field-style">';
                ret += '<div class="input_position column div_1_of_1">';
                    ret += '<span class="font-bold"> Deletion Type:</span><br/>';
                    ret += '<label><input type="radio" name="deletion_type" checked="checked" class="deletion_type css-checkbox" value="1" id="del-soft"><label for="del-soft" class="css-label"></label> Soft Delete</label>';
                    ret += ' <label><input type="radio" name="deletion_type" class="deletion_type css-checkbox" value="2" id="del-permanent"><label for="del-permanent" class="css-label"></label> Permanent Delete<label>';
                ret += '</div>';
               
            ret += '</div>';

        ret+= "</div>";
        $(".content-dialog").prepend(ret);
        $(".content-dialog-scroll").perfectScrollbar("update");

        {
            if(json['delete_enable']==undefined || json['delete_enable']=="0"){
                //disable all fields below enable deletion here
            }
            //retrieve values
            if(json['delete_enable']){
                if(json['delete_enable']=="1"){
                    $("#del_control").prop("checked",true);
                }
            }

            if(json['delete_type']){
                $(".deletion_type").attr("checked",false);
                $(".deletion_type[value='"+ json['delete_type'] +"']").prop("checked",true);
            }
        }
        ui.unblock();
    },
    "shareURL": function() {
        var newDialog = new jDialog(this.urlShare("Share Request URL", "shareURL", "", "", "faiconColor", "<svg class='icon-svg icon-svg-workspace' viewBox='0 0 90 90'><use xmlns:xlink='http://www.w3.org/1999/xlink' xlink:href='#svg-icon-workspace-control-request-url'></use></svg>"), "", "600", "", "", function() {
        });
        newDialog.themeDialog("modal2",{"height":"100"});
        this.otherAttributes();
    },

    "urlShare": function(title, type, note, icon, faiconColor, svgIcon){
        var location = window.location;
        var sub_folder_loc = $("#user_url_view").val();
        var formID = $("#FormId").val();
        var company_id = $("#current-user-company-id").text();
            ret = '<div><h3 class="fl-margin-bottom">';
                ret += svgIcon + '<span>'+title+'</span>'; //'<i class=" '+ icon +' '+ faiconColor +' icon-save"></i> ' + title;
            ret += '</h3></div>';
        ret += '<div class="hr"></div>';
        ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red">' + note + '</div>';
        //ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">';

        ret += '<div class="content-dialog" style="height: auto;">';

        ret += '<div class="fp-container CSID-search-container" style="">';
        ret += '<div class="input_position" style="width:100%">';

        ret += '<div class="clearfix"></div>';
        ret += '<div style="">';
        
        
        var get_autologged = $("#autologged").val();
        if(get_autologged == "1"){
            var autologged = "yes";
        }else{
            var autologged = "no";
        }
        ret += '<div class="fields section clearing fl-field-style">';
            //ret += '<div class="label_below2"> Title:<font color="red">*</font> </div>';
            ret += '<div class="input_position column div_1_of_1">';
                ret += '<input type="text" readonly="readonly" name="" id="shareURL" class=" form-text fl-margin-bottom" style="margin-top:5px;" value="' + location.protocol + "//" + location.host + sub_folder_loc + 'workspace?view_type=request&formID=' + formID + '&requestID=0&authentication=login&type=guest&log_type=user&autologged='+autologged+'&company_id=' + company_id + '">';
                //ret += '<input type="button" class="btn-blueBtn fl-margin-right copy_share_url" data-type="' + type + '" value="Copy">     ';
            ret += '</div>';
        ret += '</div>';
        //ret += '<div class="fields">';
        //    ret += '<div class="input_position">';
        //        //ret += '<input type="text" disabled=disabled name="" id="shareURL" class=" form-text fl-margin-bottom" style="margin-top:5px;" value="' + location.protocol + "//" + location.host + sub_folder_loc + 'workspace?view_type=request&formID=' + formID + '&requestID=0&authentication=login&type=guest&log_type=user&company_id=' + company_id + '">';
        //        ret += '<input type="button" class="btn-blueBtn fl-margin-right copy_share_url" data-type="' + type + '" value="Copy">     ';
        //    ret += '</div>'
        //ret += '</div>';
        //ret += '<div class="fields">';

        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
        //ret += '<input type="button" class="btn-blueBtn fl-margin-right save_form_privacy" data-type="' + type + '" value="Save">     ';
        ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
        ret += '</div>';
        ret += '</div>';
        
        return ret;
    },
 
    "otherAttributes": function() {
        this.setCheckAllChecked(".fp-user");
        this.bindOnClickCheckBox();
        limitText(".limit-text-ws", 20);
        $(".tip").tooltip();
        $('#content-dialog-scroll').perfectScrollbar();
    },
    "setDialog": function(title, type, note, icon, faiconColor, svgIcon) {
        var self = this;
        var json = $("body").data();
        console.log(json[''+ type +'_checked'])

        if(json[''+ type +'_checked']){
             UserAccessFlagCheckBox.checkedEle = json[''+ type +'_checked']
        }else{
             UserAccessFlagCheckBox.checkedEle = {}
        }
        
        if(json[''+ type +'_unChecked']){
             UserAccessFlagCheckBox.unCheckedEle = json[''+ type +'_unChecked']
        }else{
             UserAccessFlagCheckBox.unCheckedEle = {}
        }

        // UserAccessFlagCheckBox.checkedEle = {};//if_undefinded(json[''+ type +'_checked'],{});
        // UserAccessFlagCheckBox.unCheckedEle = {};//if_undefinded(json[''+ type +'_unChecked'],{});

        console.log(UserAccessFlagCheckBox.checkedEle)
        ret = '<div style="float:left;width:50%;">';
            ret += '<h3 class="fl-margin-bottom">';
                ret +=  svgIcon + '<span>'+title+'</span>' //'<i class=" '+ icon +' '+ faiconColor +' icon-save"></i> ' + ;
            ret += '</h3>';
        ret += '</div>';
        ret += '<div class="hr"></div>';
        ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:gray">' + note + '</div>';
        //ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">';

        ret += '<div class="content-dialog" style="height: auto;">';

            ret += self.setUsersDialog(type,json);
        
        ret += '</div>';
        ret += '<div class="fields">';
            ret += '<div class="label_basic"></div>';
            ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                ret += '<input type="button" class="btn-blueBtn fl-margin-right save_form_privacy fl-positive-btn" data-type="' + type + '" value="OK">     ';
                ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Cancel">';
            ret += '</div>';
        ret += '</div>';
        return ret;
    },
    "setUsersDialog" : function(type,json){
        var self = this;
        
        var formPrivacy = if_undefinded(json['' + type + ''], "");
        var form_users = if_undefinded(json['form-users'], "");
        var departments_form_users;
        var positions_form_users;
        var users_form_users;
        var groups_form_users;
        var departments_arr;
        var positions_arr;
        var users_arr;
        var groups_arr;
        var basedOnFormUsers = ["form-authors","form-readers","nav-viewers","report-users"];
        if($("#category_id").val()!="0"){
            basedOnFormUsers.push("customized-print");
            basedOnFormUsers.push("delete-access");
        }
        if (formPrivacy && formPrivacy!="null") {
            if(typeof formPrivacy=="string"){
                formPrivacy = JSON.parse(formPrivacy);
            }
            departments_arr = formPrivacy['departments'];
            positions_arr = formPrivacy['positions'];
            users_arr = formPrivacy['users'];
            groups_arr = formPrivacy['groups'];

        }
        if(form_users){
            departments_form_users = form_users['departments'];
            positions_form_users = form_users['positions'];
            users_form_users = form_users['users'];
            groups_form_users = form_users['groups'];
        }

        var departments = $(".departments").text();
        var positions = $(".positions").text();
        var users = $(".users").text();
        var groups = $(".form_group").text();
        var ret = '';
        ret += '<div class="fp-container CSID-search-container '+ type +'_container" style="min-height: 265px;">';
        ret += '<div class="input_position" style="width:100%">';
        ret += '<div>';
        ret += '<div class="section clearing fl-field-style">';
            ret += '<div class="column div_1_of_1" style="display:table;">';
                ret += '<span class="font-bold" style="display:table-cell;vertical-align:middle;"><label><input type="checkbox" class="fp-allUsers css-checkbox" value="All Users" id="Allusers"/><label for="Allusers" class="css-label"></label> All Users</label></span>';
                ret += '<div style="float:right;"><div class="fl-search-wrapper" style="width:250px;"><input type="text" placeholder="Search" id="" class="CSID-search-user-txt" style="float:right;"> <input type="button" value="" class="CSID-search-user-btn search icon fl-search-icon-list " style="top: auto;right: -213px;bottom: auto;left: auto;position: relative;width: 36px;display: block;"></div></div>';
            ret += '</div>';
        ret += '</div>';
        ret += '<div>';
        //if (type == "shareURL") {
        //    ret += '<div class="fields">';
        //        ret += '<div class="label_below2"> Title:<font color="red">*</font> </div>';
        //        ret += '<div class="input_position">';
        //            ret += '<input type="text" name="workspace_title" id="workspace_title" class="workspace_title form-text fl-margin-bottom" style="margin-top:5px;" value="Usana Form">';
        //        ret += '</div>';
        //    ret += '</div>';
        //}else{
            if (type != "form-admin") {
                // for department
                ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="departments">';
                    ret += '<div class="column div_1_of_1">';
                        ret += '<div class="formUser" rel="getDepartmentFF" ><label class="icon-minus fa fa-minus"></label> <label class="font-bold">Department</label></div>';
                        ret += '<div class="getDepartmentFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';
            
                        var ctr_dept = 0;
                        if (departments) {
                            departments = JSON.parse(departments);
                            $.each(departments, function(key, val) {
                                if(basedOnFormUsers.indexOf(type)>=0){
                                    if(self.setFormUsersShortlist(val.user, departments_form_users)==0){
                                        return true;
                                    }
                                }
                                ret += '<div style="width:30%;float:left;margin-bottom:2px" class="CSID-search-data"> ';
                                //setCheckedArray(value, arr)
                                ret += '<label class="tip" title="' + htmlEntities(val.department) + '"><input type="checkbox" class="fp-user departments css-checkbox" ' + self.setCheckedArray(val.user, departments_arr) + ' value="' + val.user + '" id="dept_' + val.user + '"/><label for="dept_' + val.user + '" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">' + htmlEntities(val.department) + '</span></label>';
                                ret += '</div>';
                                ctr_dept++
                            })
                        }
                        var displayEmptyDept = "display";
                        if(ctr_dept==0){
                            displayEmptyDept = ""
                        }
                        ret += '<div class="empty-users '+ displayEmptyDept +'" style="color:red">No Department</div>';
                        ret += '</div>';
                    ret += '</div>';    
                ret += '</div>';

                // for groups
                ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="groups">';
                    ret += '<div class="column div_1_of_1">';
                        ret += '<div class="formUser" rel="getPositionFF" ><label class="icon-minus fa fa-minus"></label> <label class="font-bold">Groups</label></div>';
                        ret += '<div class="getPositionFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';
            
                        var ctr_groups = 0;
                        if (groups) {
                            groups = JSON.parse(groups);
                            $.each(groups, function(key, val) {
                                if (val.group_name != "") {
                                    if(basedOnFormUsers.indexOf(type)>=0){
                                        if(self.setFormUsersShortlist(val.id, groups_form_users)==0){
                                            return true;
                                        }
                                    }
                                    ret += '<div style="width:30%;float:left;margin-bottom:2px" class="CSID-search-data"> ';
                                    ret += '<label class="tip" title="' + htmlEntities(val.group_name) + '"><input type="checkbox" class="fp-user groups css-checkbox" ' + self.setCheckedArray(val.id, groups_arr) + ' value="' + val.id + '" id="groups_' + val.id + '"/><label for="groups_' + val.id + '" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">' + htmlEntities(val.group_name) + '</span></label>';
                                    ret += '</div>';
                                    ctr_groups++;
                                }
                            })
            
                        }
                        var displayEmptyGroups = "display";
                        if(ctr_groups==0){
                            displayEmptyGroups = ""
                        }
                        ret += '<div class="empty-users '+ displayEmptyGroups +'" style="color:red">No Groups</div>';
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';


                // for position
                ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="positions">';
                    ret += '<div class="column div_1_of_1">';
                        ret += '<div class="formUser" rel="getPositionFF" ><label class="icon-minus fa fa-minus"></label> <label class="font-bold">Position</label></div>';
                        ret += '<div class="getPositionFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';

                        var ctr_pos = 0;
                        if (positions) {
                            positions = JSON.parse(positions);
                            $.each(positions, function(key, val) {
                                if (htmlEntities(val.position) != "") {
                                    if(basedOnFormUsers.indexOf(type)>=0){
                                        if(self.setFormUsersShortlist(val.id, positions_form_users)==0){
                                            return true;
                                        }
                                    }
                                    ret += '<div style="width:30%;float:left;margin-bottom:2px" class="CSID-search-data"> ';
                                    ret += '<label class="tip" title="' + htmlEntities(val.position) + '"><input type="checkbox" class="fp-user positions css-checkbox" ' + self.setCheckedArray(val.id, positions_arr) + ' value="' + val.id + '" id="position_' + val.id + '"/><label for="position_' + val.id + '" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">' + htmlEntities(val.position) + '</span></label>';
                                    ret += '</div>';
                                    ctr_pos++;
                                }
                            })
            
                        }
                        var displayEmptyPos = "display";
                        if(ctr_pos==0){
                            displayEmptyPos = ""
                        }
                        ret += '<div class="empty-users '+ displayEmptyPos +'" style="color:red">No Positions</div>';
                        ret += '</div>';
                    ret += '</div>';
                ret += '</div>';

                 
            }else {
                users = $(".users-admin").text();
            }
        //}
        

        //for users
        ret += '<div class="container-formUser section clearing fl-field-style" CSID-search-type="users">';
        ret += '<div class="formUser column div_1_of_1" rel="getUsersFF" ><label class=" icon-minus fa fa-minus"></label> <label class="font-bold">Users</label></div>';
        ret += '<div class="getUsersFF" style="display:inline-block;width:100%;margin-left:10px;margin-top:5px;">';

        var ctr_usr = 0
        if (users) {
            users = JSON.parse(users);
            var user_displayname = "";
            $.each(users, function(key, val) {
                if(basedOnFormUsers.indexOf(type)>=0){
                    if(self.setFormUsersShortlist(val.id, users_form_users)==0){
                        return true;
                    }
                }
                user_displayname = val.display_name;
                if(user_displayname==undefined){
                    user_displayname = val.first_name + ' ' + val.last_name;
                }

                user_displayname = htmlEntities(user_displayname);

                ret += '<div style="width:30%;float:left; padding:2px;" class="CSID-search-data"> ';
                ret += '<label class="tip" title="' + user_displayname + '"><input type="checkbox" class="fp-user users css-checkbox" ' + self.setCheckedArray(val.id, users_arr) + ' value="' + val.id + '" id="users_' + val.id + '"/><label for="users_' + val.id + '" class="css-label"></label> <span class="limit-text-ws CSID-search-data-text">' + user_displayname + '</span></label>';
                ret += '</div>';
                ctr_usr++
            })

        }
        var displayEmptyUser = "display";
            if(ctr_usr==0){
                displayEmptyUser = ""
            }
            ret += '<div class="empty-users '+ displayEmptyUser +'" style="color:red">No Users</div>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        ret += '</div>';
        return ret;
    },
    saveFormPrivacy: function(obj) {
        var data_type = $(obj).attr("data-type");
        var json = $("body").data();
        var json_privacy = {};
        var departments = new Array();
        var positions = new Array();
        var users = new Array();
        var groups = new Array();

        //for deoartments
        $(".departments").each(function() {
            if ($(this).prop("checked")) {
                departments.push($(this).val());
            }
        })
        //for positions
        $(".positions").each(function() {
            if ($(this).prop("checked")) {
                positions.push($(this).val());
            }
        })
        //for users
        $(".users").each(function() {
            if ($(this).prop("checked")) {
                users.push($(this).val());
            }
        })

        //for users
        $(".groups").each(function() {
            if ($(this).prop("checked")) {
                groups.push($(this).val());
            }
        })

        //
        json_privacy['departments'] = departments;
        json_privacy['positions'] = positions;
        json_privacy['users'] = users;
        json_privacy['groups'] = groups;


        //check if form user is checked(based on form users)
        if(data_type=="form-users"){
            // console.log(json['form-authors'])
            /*Form Users*/
            if (json['form-authors']) {
                // workspace['form_authors'] = JSON.stringify(json_a['form-authors']);
                json['form-authors'] = JSON.stringify(checkIfFormUser(json['form-authors'],json_privacy));
            }
            if(json['form-authors_checked']){
                json['form-authors_checked'] = checkIfFormUser(json['form-authors_checked'],json_privacy);
            }

            if(json['form-authors_unChecked']){
                var originalAuthors = $(".form-user-authors_original").text();
                try{
                    originalAuthors = JSON.parse(originalAuthors);
                }catch(err){
                    console.log(originalAuthors);
                    console.log(err)
                }
                json['form-authors_unChecked'] = mergeFormUser(json['form-authors_unChecked'],UserAccessFlagCheckBox.unCheckedEle,originalAuthors);
            }


            /*Form Readers*/
            if (json['form-readers']) {
                // workspace['form_viewers'] = JSON.stringify(json_a['form-readers']);
                json['form-readers'] = JSON.stringify(checkIfFormUser(json['form-readers'],json_privacy));
            }
            if (json['form-readers_checked']) {
                // workspace['form_viewers'] = JSON.stringify(json_a['form-readers']);
                json['form-readers_checked'] = checkIfFormUser(json['form-readers_checked'],json_privacy);
            }
            if (json['form-readers_unChecked']) {
                var originalReader = $(".form-user-viewers_original").text();
                try{
                    originalReader = JSON.parse(originalReader);
                }catch(err){
                    console.log(originalReader);
                    console.log(err)
                }
                json['form-readers_unChecked'] = mergeFormUser(json['form-readers_unChecked'],UserAccessFlagCheckBox.unCheckedEle,originalReader);
            }

            if($("#category_id").val()!="0"){
                //Custom Print
                if (json['customized-print']) {
                    // workspace['form_viewers'] = JSON.stringify(json_a['form-readers']);
                    json['customized-print'] = JSON.stringify(checkIfFormUser(json['customized-print'],json_privacy));
                }
                if (json['customized-print_checked']) {
                    // workspace['form_viewers'] = JSON.stringify(json_a['form-readers']);
                    json['customized-print_checked'] = checkIfFormUser(json['customized-print_checked'],json_privacy);
                }
                if (json['customized-print_unChecked']) {
                    var originalCustomPrint = $(".form-custom-print-users_original").text();
                    try{
                        originalCustomPrint = JSON.parse(originalCustomPrint);
                    }catch(err){
                        console.log(originalCustomPrint);
                        console.log(err)
                    }
                    json['customized-print_unChecked'] = mergeFormUser(json['customized-print_unChecked'],UserAccessFlagCheckBox.unCheckedEle,originalCustomPrint);
                }

                //Delete Access
                if (json['delete-access']) {
                    // workspace['form_viewers'] = JSON.stringify(json_a['form-readers']);
                    json['delete-access'] = JSON.stringify(checkIfFormUser(json['delete-access'],json_privacy));
                }
                if (json['delete-access_checked']) {
                    // workspace['form_viewers'] = JSON.stringify(json_a['form-readers']);
                    json['delete-access_checked'] = checkIfFormUser(json['delete-access_checked'],json_privacy);
                }
                if (json['delete-access_unChecked']) {
                    var originalDeleteAccess = $(".form-delete-access-json_original").text();
                    try{
                        originalDeleteAccess = JSON.parse(originalDeleteAccess);
                    }catch(err){
                        console.log(originalDeleteAccess);
                        console.log(err)
                    }
                    json['delete-access_unChecked'] = mergeFormUser(json['delete-access_unChecked'],UserAccessFlagCheckBox.unCheckedEle,originalDeleteAccess);
                }
            }

        }

        //
        json['' + data_type + ''] = json_privacy;


        {
            //delete access other value
            if(data_type=="delete-access"){
                json['delete_enable'] = if_undefinded($("#del_control:checked").val(),"0");
                json['delete_type'] = $(".deletion_type:checked").val();
            }
        }


        //save check unchecked
        //
        json[''+ data_type +'_checked'] = UserAccessFlagCheckBox.checkedEle;

        if(data_type=="form-authors"){
            var originalAuthors = $(".form-user-authors_original").text();
            json[''+ data_type +'_unChecked'] = mergeFormUser(json[''+ data_type +'_unChecked'],UserAccessFlagCheckBox.unCheckedEle,originalAuthors);
        }else if(data_type=="form-readers"){
            var originalReader = $(".form-user-viewers_original").text();
            json[''+ data_type +'_unChecked'] = mergeFormUser(json[''+ data_type +'_unChecked'],UserAccessFlagCheckBox.unCheckedEle,originalReader);
        }else if(data_type=="customized-print"){
            var originalCustomPrint = $(".form-custom-print-users_original").text();
            json[''+ data_type +'_unChecked'] = mergeFormUser(json[''+ data_type +'_unChecked'],UserAccessFlagCheckBox.unCheckedEle,originalCustomPrint);
        }else if(data_type=="delete-access"){
            var originalDeleteAccess = $(".form-delete-access-json_original").text();
            json[''+ data_type +'_unChecked'] = mergeFormUser(json[''+ data_type +'_unChecked'],UserAccessFlagCheckBox.unCheckedEle,originalDeleteAccess);
        }else{
            json[''+ data_type +'_unChecked'] = UserAccessFlagCheckBox.unCheckedEle;
        }
        


        UserAccessFlagCheckBox.checkedEle = {};
        UserAccessFlagCheckBox.unCheckedEle = {};
        //close the window
        //$("#popup_cancel").click();
        $('.fl-closeDialog').click();

    },
    "checkAllUsers": function(obj) {
        if ($(obj).prop("checked")) {
            $(obj).closest(".fp-container").find(".fp-user").prop("checked", true);
            $(obj).closest(".fp-container").find(".fp-allPositions").prop("checked", true);
            $(obj).closest(".fp-container").find(".fp-allGroups").prop("checked", true);
            $(obj).closest(".fp-container").find(".fp-allDepartments").prop("checked", true);
        } else {
            $(obj).closest(".fp-container").find(".fp-user").prop("checked", false);
            $(obj).closest(".fp-container").find(".fp-allPositions").prop("checked", false);
            $(obj).closest(".fp-container").find(".fp-allGroups").prop("checked", false);
            $(obj).closest(".fp-container").find(".fp-allDepartments").prop("checked", false);
        }
    },
    "checkAllDepts": function(obj) {
        if($(obj).closest(".fp-container").find('.fp-allUsers').prop('checked')){
        }else{
            if ($(obj).prop("checked")) {
                $(obj).closest(".fp-container").find('[csid-search-type="departments"]').find(".fp-user").prop("checked", true);
            } else {
                $(obj).closest(".fp-container").find('[csid-search-type="departments"]').find(".fp-user").prop("checked", false);
            }
        }
    },
    "checkAllGroups": function(obj) {
        if($(obj).closest(".fp-container").find('.fp-allUsers').prop('checked')){
        }else{
            if ($(obj).prop("checked")) {
                $(obj).closest(".fp-container").find('[csid-search-type="groups"]').find(".fp-user").prop("checked", true);
            } else {
                $(obj).closest(".fp-container").find('[csid-search-type="groups"]').find(".fp-user").prop("checked", false);
            }
        }
    },
    "checkAllPositions": function(obj) {
        if($(obj).closest(".fp-container").find('.fp-allUsers').prop('checked')){
        }else{
            if ($(obj).prop("checked")) {
                $(obj).closest(".fp-container").find('[csid-search-type="positions"]').find(".fp-user").prop("checked", true);
            } else {
                $(obj).closest(".fp-container").find('[csid-search-type="positions"]').find(".fp-user").prop("checked", false);
            }
        }
    },
    "setCheckedArray": function(value, arr) {
        // if(value.indexOf(arr)>=0){
        //  return "checked='checked'";
        // }
        for (var i in arr) {
            if (value == arr[i]) {
                return "checked='checked'";
                break;
            }
        }
    },
    "setFormUsersShortlist": function(value, arr,callback) {
        // if(value.indexOf(arr)>=0){
        //  return "checked='checked'";
        // }
        var ctr = 0;
        for (var i in arr) {
            if (value == arr[i]) {
                ctr++;
                return ctr;        
            }
        }
        return ctr;
    },
    "resetUsers": function() {
        var json = $("body").data();
        var json_privacy_admin = {};
        var users_arr_admin = new Array();


        var users_admin = $(".users-admin").text();
        if (users_admin) {
            users_admin = JSON.parse(users_admin);
            $.each(users_admin, function(key, val) {
                users_arr_admin.push(val.id);
            })
        }

        json_privacy_admin['departments'] = "";
        json_privacy_admin['positions'] = "";
        json_privacy_admin['users'] = users_arr_admin;
        //
        json['form-authors'] = {};
        json['form-users'] = {};

        json['form-admin'] = json_privacy_admin;
        //reader access
        json['form-readers'] = {};
        //custom print
        json['customized-print'] = {};
        //delete access
        json['delete-access'] = {};

        $(".form-category").attr("resetUsersFlag","1");

        //orignals
        $(".form-user-authors_original").text(JSON.stringify({})); // default checked all
        $(".form-user-accessor_original").text(JSON.stringify({})); // default checked all
        $(".form-user-viewers_original").text(JSON.stringify({})); // default no checked
        $(".form-user-admin_original").text(JSON.stringify(json_privacy_admin)); // default checked all admin
        $(".form-custom-print-users_original").text(JSON.stringify({})); // default no checked
        $(".form-delete-access-json_original").text(JSON.stringify({})); // default no checked

        //remove all checked unchecked
        FormUserAccess.removeTempCheckedUncheckedUsers();
    },
    "defaultCheckedNewForm": function() {
        var json = $("body").data();
        var json_privacy = {};
        var json_privacy_admin = {};
        var departments_arr = new Array();
        var positions_arr = new Array();
        var users_arr = new Array();
        var groups_arr = new Array();
        var users_arr_admin = new Array();

        var departments = $(".departments").text();
        if (departments) {
            departments = JSON.parse(departments);
            $.each(departments, function(key, val) {
                departments_arr.push(val.user);
            })
        }
        //for positions
        var positions = $(".positions").text();
        if (positions) {
            positions = JSON.parse(positions);
            $.each(positions, function(key, val) {
                positions_arr.push(val.id);
            })
        }
        //for users
        var users = $(".users").text();
        if (users) {
            users = JSON.parse(users);
            $.each(users, function(key, val) {
                users_arr.push(val.id);
            })
        }
        //for users
        var groups = $(".form_group").text();
        if (groups) {
            groups = JSON.parse(groups);
            $.each(groups, function(key, val) {
                groups_arr.push(val.id);
            })
        }

        var users_admin = $(".users-admin").text();
        if (users_admin) {
            users_admin = JSON.parse(users_admin);
            $.each(users_admin, function(key, val) {
                users_arr_admin.push(val.id);
            })
        }
        //
        json_privacy['departments'] = departments_arr;
        json_privacy['positions'] = positions_arr;
        json_privacy['users'] = users_arr;
        json_privacy['groups'] = groups_arr;
        json_privacy_admin['departments'] = "";
        json_privacy_admin['positions'] = "";
        json_privacy_admin['users'] = users_arr_admin;
        //
        json['form-authors'] = json_privacy;
        json['form-users'] = json_privacy;
        json['nav-viewers'] = json_privacy;
        json['form-admin'] = json_privacy_admin;
        //reader access
        json['form-readers'] = {};
        //custom print
        json['customized-print'] = {};
        //delete access
        json['delete-access'] = {};


        //orignals
        $(".form-user-authors_original").text(JSON.stringify(json_privacy)); // default checked all
        $(".form-user-accessor_original").text(JSON.stringify(json_privacy)); // default checked all
        $(".form-user-viewers_original").text(JSON.stringify({})); // default no checked
        $(".form-user-admin_original").text(JSON.stringify(json_privacy_admin)); // default checked all admin
        $(".form-custom-print-users_original").text(JSON.stringify({})); // default no checked
        $(".form-delete-access-json_original").text(JSON.stringify({})); // default no checked

        //remove all checked unchecked
        FormUserAccess.removeTempCheckedUncheckedUsers();
    },
    setCheckAllChecked: function(clickedEle) {
        dialogContainer = $(clickedEle).closest(".fp-container");
        totalCheckBox = dialogContainer.find(".fp-user").length;
        totalCheckBoxChecked = dialogContainer.find(".fp-user:checked").length;
        deptCheckBox = dialogContainer.find(".departments").length;
        deptCheckBoxChecked = dialogContainer.find(".departments:checked").length;
        grpCheckBox = dialogContainer.find(".groups").length;
        grpCheckBoxChecked = dialogContainer.find(".groups:checked").length;
        posCheckBox = dialogContainer.find(".positions").length;
        posCheckBoxChecked = dialogContainer.find(".positions:checked").length;

        // All Users
        if (totalCheckBox == totalCheckBoxChecked) {
            dialogContainer.find(".fp-allUsers").attr("checked", true);
            dialogContainer.find(".fp-allDepartments").attr("checked", true);
            dialogContainer.find(".fp-allGroups").attr("checked", true);
            dialogContainer.find(".fp-allPositions").attr("checked", true);
        } else {
            dialogContainer.find(".fp-allUsers").attr("checked", false);
            dialogContainer.find(".fp-allDepartments").attr("checked", false);
            dialogContainer.find(".fp-allGroups").attr("checked", false);
            dialogContainer.find(".fp-allPositions").attr("checked", false);
        }
        // All Departments
        if (deptCheckBox == deptCheckBoxChecked) {
            dialogContainer.find(".fp-allDepartments").attr("checked" , true);
        } else {
            dialogContainer.find(".fp-allDepartments").attr("checked", false);
        }
        //  All Groups
        if (grpCheckBox == grpCheckBoxChecked) {
            dialogContainer.find(".fp-allGroups").attr("checked", true);
        } else {
            dialogContainer.find(".fp-allGroups").attr("checked", false);
        }
        // All Positions
        if (posCheckBox == posCheckBoxChecked){
            dialogContainer.find(".fp-allPositions").attr("checked", true);
        } else {
            dialogContainer.find(".fp-allPositions").attr("checked", false);
        }
},
    bindOnClickCheckBox: function() {
        self = this;
        dialogContainer = $("#popup_container");
        dialogContainer.find(".fp-user").on({
            "click": function(ee) {
                self.setCheckAllChecked(this);
            }
        })
    }
}

$(document).ready(function() {
    //toggle of formula on display on sidebar
    {
        var obj = ".fm-type-dos";
        var data_1 = {
                "value":"0",
                "display":".static-dos"
            };
        var data_2 = {
                "value":"1",
                "display":".dynamic-dos"
            }
        ToggleWithFormula.toggleEvent(obj,data_1,data_2);
    }
    
    //$(".form_settings_open").formbuilder({auto_open:"true"});
    $(".form_settings_open").formbuilder({type: "open"}).formbuilder({type: "close"});


    //check all fields(enabled,hidden,required)
    $("body").on("click", ".checkAllFields", function() {
        CheckProperties.checkAll(this, ".container-fields");
    })



    //set form authors

    $(".form-authors").click(function() {
        RequestPivacy.authors();
    })
    UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".form-authors_container");
    //set form readers

    // $(".form-readers").click(function(){
    //  RequestPivacy.readers();
    // })
    $("body").on("click", ".form-readers", function() {
        RequestPivacy.readers();
    })
    UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".form-readers_container");


    $("body").on("click", ".form-users", function() {
        RequestPivacy.users();
    })
    UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".form-users_container");


    $("body").on("click", ".form-admin", function() {
        RequestPivacy.admin();
    })
    UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".form-admin_container");


    $("body").on("click", ".form-customized", function() {
        // alert(123123)
        RequestPivacy.customizedPrint();
    })
    UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".customized-print_container");


    // $("body").on("click", ".form-navViewers", function() {
        // alert(123123)
        // RequestPivacy.navViewers();
    // })
    // UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".nav-viewers_container");
    
    $("body").on("click", ".form-reportUsers", function() {
        // alert(123123)
        RequestPivacy.reportUsers();
    })
    UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".report-users_container");

    

    $("body").on("click", ".form-delete-access", function() {
        // alert(123123)
        RequestPivacy.deleteAccess();
    })
    UserAccessFlagCheckBox.init(".fp-user",".fp-allUsers",".delete-access_container");



    $("body").on("click", ".form-url", function() {
        // alert(123123)
        RequestPivacy.shareURL();
        $("#shareURL").select();
    })
    $("body").on("click","#copy_share_url",function(e){
        $("#shareURL").select();
        //$("#copy_share_url").keydown(function (e) {
           if (e.ctrlKey && (e.keyCode == 88 || e.keyCode == 67 || e.keyCode == 86)) {
            alert(1)
                 return false;
             }
        //});
    })
    //check all
    $("body").on("click", ".fp-allUsers", function() {
        RequestPivacy.checkAllUsers(this);
    })
    //check all departments
    $("body").on("click", ".fp-allDepartments", function() {
        RequestPivacy.checkAllDepts(this);
    })
    //check all Groups
    $("body").on("click", ".fp-allGroups", function() {
        RequestPivacy.checkAllGroups(this);
    })
    //check all Positions
    $("body").on("click", ".fp-allPositions", function() {
        RequestPivacy.checkAllPositions(this);
    })

    $("body").on("click", ".save_form_privacy", function() {
        RequestPivacy.saveFormPrivacy(this);
    })

    FieldSequence.init();
    
    WorkspaceVersion.init();

    FormVariables.init();
})

Form_Category2 = {
    setFormCategoryModal : function(){
        var ret = '<h3><i class="icon-comment"></i> Form Category</h3>';
            ret += '<div class="hr"></div>';
            ret += '</div>';
            ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">';
            ret += '<div class="content-dialog" style="height: auto;margin-bottom:0x">';
                ret += '<div class="fields" style="border-top:1px solid #ddd;">';
                    ret += '<div class="label_below2">Form Category:</div>';
                    ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                        ret += '<select class="form-select form_category fl-margin-bottom" name="form_category" id="form_category" style="margin-top:5px; margin-bottom:5px;">';
                            ret += getFormCategory();
                        ret += '</select>';
                    ret += '</div>';
                ret += '</div>';
            ret += '</div>';
            ret += '</div>';
            ret += '<div class="fields" style="border-top:1px solid #ddd;">';
                ret += '<div class="label_basic"></div>';
                ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
                    ret += ' <input type="button" class="btn-basicBtn saveFormCategory" value="Save">';
                    ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
                ret += '</div>';
            ret += '</div>';

        var newDialog = new jDialog(ret, "", "", "", "100px", function() {});
        newDialog.themeDialog("modal2");
        $("#content-dialog-scroll").perfectScrollbar();
        $("#content-dialog-scroll").css("height","100px");
        // alert(123123)
    }
}


function hasScrollLeft(selector){
    {//version 1 getting has scroll
        // var dis_ele = $(selector);
        // var totalScrollWidth = dis_ele.get(0).scrollWidth;
        // var dis_ele_width = dis_ele.outerWidth();
        // if( dis_ele_width >= totalScrollWidth ){
        //  //
        // }else{
        //  //has scroll
        // }
    }

    {//version 2 getting has scroll
        var dis_ele = $(selector);
        var sLeft = dis_ele.scrollLeft();
        var parents_displayed_none = $();
        
        if(dis_ele.is(":visible") == false){
            parents_displayed_none = dis_ele.parents().filter(function(index, element){
                if($(element).css("display") == "none"){
                    return true;
                }else{
                    return false;
                }
            });
            parents_displayed_none.each(function(){
                $(this).css("display","block");
            })
        }
        

        if(sLeft == 0){
            dis_ele.scrollLeft(3);
        }
        if(dis_ele.scrollLeft() >= 1){
            parents_displayed_none.each(function(){
                $(this).css("display","none");
            })
            //has scroll
            return true
        }else if(dis_ele.scrollLeft() <= 0){
            parents_displayed_none.each(function(){
                $(this).css("display","none");
            })
            //no scroll
            return false;
        }
    }
}

function hasScrollTop(selector){
    {//version 1 getting has scroll
        // var dis_ele = $(selector);
        // var totalScrollWidth = dis_ele.get(0).scrollWidth;
        // var dis_ele_width = dis_ele.outerWidth();
        // if( dis_ele_width >= totalScrollWidth ){
        //  //
        // }else{
        //  //has scroll
        // }
    }

    {//version 2 getting has scroll
        var dis_ele = $(selector);
        var sTop = dis_ele.scrollTop();
        var parents_displayed_none = $();
        
        if(dis_ele.is(":visible") == false){
            parents_displayed_none = dis_ele.parents().filter(function(index, element){
                if($(element).css("display") == "none"){
                    return true;
                }else{
                    return false;
                }
            });
            parents_displayed_none.each(function(){
                $(this).css("display","block");
            })
        }

        if(sTop == 0){
            dis_ele.scrollTop(3);
        }
        if(dis_ele.is(":visible")){
            if(dis_ele.scrollTop() >= 1){
                parents_displayed_none.each(function(){
                    $(this).css("display","none");
                })
                //has scroll
                return true
            }else if(dis_ele.scrollTop() <= 0){
                parents_displayed_none.each(function(){
                    $(this).css("display","none");
                })
                //no scroll
                return false;
            }
        }else{
            dis_ele
        }
        
    }
}

FieldSequence = {
    "init" : function(){
        this.createModal();
        this.saveSequence();
    },
    "createModal" : function(){
        $("body").on("click",".field-process-sequence",function(){
            var temp_saved_seq_field_array = $('body').data('saved_seq_field_array');
            var hasMiddleware = 0;
            var ret = '<div><h3><svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-middleware-sequence"></use></svg><span>Middleware Process Sequence</span></h3></div>';
            ret += '<div class="hr"></div>';
            ret += '</div>';
            ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">';
            ret += '<div class="content-dialog" style="height: auto;margin-bottom:0x">';
            ret += '<ul class="sequence-sort">';
            
                if(temp_saved_seq_field_array){
                    var temp_fname = "";
                    var temp_ele_field = null;
                    for(var keyin in temp_saved_seq_field_array){
                        temp_fname = temp_saved_seq_field_array[keyin];
                        temp_ele_field = $('[name="'+temp_fname+'"]');
                        if(temp_ele_field.length >= 1){
                            if(temp_ele_field.attr("default-type") == "middleware"){
                                ret += '<li><div class="sequence-field" style="width:100%;height:20px;background-color: #e0e0e0;cursor:move;margin-bottom:2px;font-size: 15px;padding: 5px;border-radius: 3px;" field-name="'+ temp_ele_field.attr("name") +'">'+ temp_ele_field.parents(".setObject").eq(0).find(".obj_label").eq(0).text() +' ('+temp_ele_field.attr("name")+')</div></li>';
                            }
                        }
                    }
                }

                $(".setObject").each(function(eqi_setObj) {
                    if (
                            $(this).attr("data-type") == "labelOnly" ||
                            $(this).attr("data-type") == "createLine" ||
                            $(this).attr("data-type") == "table" ||
                            $(this).attr("data-type") == "tab-panel" ||
                            $(this).attr("data-type") == "listNames" ||
                            $(this).attr("data-type") == "imageOnly" ||
                            $(this).attr("data-type") == "button" ||
                            $(this).attr("data-type") == "embeded-view"
                            ) {
                        return true; //preventing to insert it to the database
                    }
                    counted_id = $(this).attr("data-object-id");
                    if ($(this).find(".getFields_" + counted_id).attr("name") && $(this).find(".getFields_" + counted_id).attr("default-type") == "middleware") {
                        hasMiddleware++;
                        if(temp_saved_seq_field_array){
                            if(temp_saved_seq_field_array.indexOf( $(this).find(".getFields_" + counted_id).attr("name") ) >= 0){
                                return true;
                            }
                        }
                        ret += '<li><div class="sequence-field" style="width:100%;height:20px;background-color: #e0e0e0;cursor:move;margin-bottom:2px;font-size: 15px;padding: 5px;border-radius: 3px;" field-name="'+ $(this).find(".getFields_" + counted_id).attr("name") +'">'+ $(this).find(".obj_label").eq(0).text() +' ('+$(this).find(".getFields_" + counted_id).attr("name")+')</div></li>';
                    }
                });
            ret += '</ul>';

            ret += '</div>';
            ret += '</div>';
            ret += '<div class="fields" style="border-top:1px solid #ddd;">';
            ret += '<div class="label_basic"></div>';
            ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
            ret += ' <input type="button" class="fl-positive-btn fl-buttonEffect btn-basicBtn saveSequence" value="OK">';
            ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
            ret += '</div>';
            ret += '</div>';

            if(hasMiddleware>=2){
                var newDialog = new jDialog(ret, "", "", "", "30", function() {});
                newDialog.themeDialog("modal2");
                $(".sequence-sort").sortable({
                    "axis":"y",
                });
                //$("#content-dialog-scroll").perfectScrollbar();
            }else{
                showNotification({
                    message: "Not enough middleware fields",
                    type: "error",
                    autoClose: true,
                    duration: 3
                });
            }
        });
        $("body").on("click",".landing-page",function(){
            //$files = upload::getAllfiles_fromDirectory(APPLICATION_PATH . '/modules/user_view/' . 'controller/');
            
            var obj_ajax = $.post("/ajax/settings",{"action":"load_landing_page"},function(data){
                   
                   var ajax_array = JSON.parse(data);
                   
                   
                   ajax_array['others'] = "others";
                   
                    
                   //ajax_array = ajax_array.concat(array_addition);
                   console.log('Landing Page JSON',ajax_array)
                   var ret = '<h3 class="fl-margin-bottom">';
                   ret += '<i class="fa fa-flag"></i> Landing Page';
                   ret += '</h3>';
                   ret += '<div class="hr"></div>';
                   ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
                   ret += '<div class="content-dialog landing-page-dialog" style="height: auto;">';
                       // your content here
                       ret += '<select class="form-select" data-properties-type="landing-page" data-object-id="" id="">';
                           // for(var option_ctr = 0; option_ctr <= ajax_array.length-1; option_ctr++){
                           //      ret += '<option value = "'+ajax_array[option_ctr]+'">'+ajax_array[option_ctr]+'</option>';
                           // }

                           for(var option_ctr in ajax_array){
                                
                                ret += '<option value = "'+ajax_array[option_ctr]+'">'+option_ctr+'</option>';

                           }
                       ret += '</select>';

                       ret += '<div class="fields" style="">';
                           ret += '<div class="label_basic"></div>';
                           ret += '<div class="input_position" style="margin-top:5px;">';
                           ret += ' <input type="button" class="btn-blueBtn fl-margin-right saveLandingPage" id="buttonID" value="Save">';
                           ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
                       ret += '</div>';
                   ret+= '</div>';
                   var newDialog = new jDialog(ret, "", "", "500", "", function() {   
                   });
                   newDialog.themeDialog("modal2");
                   var lps_val = "";

                   var landing_page_selector = $('[data-properties-type="landing-page"]');
                   
                   landing_page_selector.on("change",function(){

                        lps_val = $(this).val();
                        
                        if(lps_val=="others"){
                             
                             var appended_ele_input = '<div class="label_below2 landing-page-label">Url: <font color="red">*</font></div>';
                                appended_ele_input += '<input class="form-text" placeholder="http://www.gs3.com.ph/" data-properties-type="landing-page-others"/>';
                             $('[data-properties-type="landing-page"]').after(appended_ele_input);
                        }
                        else{
                           
                            if($(this).parents('#popup_container').find('[data-properties-type="landing-page-others"]').length>=1){
                                $(this).parents('#popup_container').find('[data-properties-type="landing-page-others"]:eq(0)').siblings('.landing-page-label').remove();
                                $(this).parents('#popup_container').find('[data-properties-type="landing-page-others"]:eq(0)').remove();
                            }
                        }
                   });
                   landing_page_selector.trigger("change");
                   if($.type($('body').data('saved_landing_page')) != "undefined"){
                       var bodyData = $('body').data("saved_landing_page");
                       var selector =  $('[data-properties-type="landing-page"]');
                       if(CheckUrl(bodyData)){
                           selector.val("others");
                           selector.trigger("change");
                           selector.parents('#popup_container').find('[data-properties-type="landing-page-others"]:eq(0)').val(bodyData);
                       }
                       else{
                           selector.val(bodyData);
                           selector.trigger("change");
                       }

                   }
            });

            
          
        });
        $("body").on("click",".details-panel",function(){
          
            var ret = '<div><h3><svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-detailspanel"></use></svg><span>Details Panel</span></h3></div>';
            ret += '<div class="hr"></div>';
            ret += '</div>';
            //ret += '<div id="content-dialog-scroll" style="position: relative; margin: 0px auto; padding: 0px; width: 100%; height: 300px;overflow: hidden;" class="ps-container">';
                ret += '<div class="content-dialog" style="height: auto;margin-bottom:0x">';
                    ret += '<div class="section clearing fl-field-style">';
                        ret += '<div class="column div_1_of_1">';
                            ret += '<label><input type="checkbox" value="yes" class="css-checkbox detail-default-view" id="defaultApplicationView" /><label for="defaultApplicationView" class="css-label"></label> Show details on load</label>'
                        ret += '</div>';
                    ret += '</div>';
                    ret += detailPanelDesign();
                    // ret += '<div class= "detP-master-container">';
                    //     ret += '<div class = "detP-slave-container"><div><span class="detP-add-minus-row"><i class="fa fa-plus detP-add-row"></i> <i class="fa fa-minus detP-minus-row"></i></span></div>';
                    //         ret += '<input type="text" class="form-text" data-properties-type = "details-panel-name" />';
                    //         ret += '</br>';
                    //         ret += '</br>';

                    //             ret += '<table class="detP-table-master">';
                    //                 ret += '<thead>';
                    //                     ret += '<tr>';
                    //                         ret += '<th>Title</th>';
                    //                         ret += '<th>Formula</th>';
                    //                     ret += '</tr>';
                    //                 ret += '</thead>';
                    //                 ret += '<table class="detP-table-slave">';
                    //                     ret += '<tbody class="detP-tr-group">';
                    //                         ret += '<tr>'; 
                    //                             ret += '<td colspan="2"><div><span class="detP-add-minus-row"><i class="fa fa-plus detP-add-row-inner"></i> <i class="fa fa-minus detP-minus-row-inner"></i></span></div></td>';
                    //                         ret += '</tr>';
                    //                         ret += '<tr>';
                    //                             ret += '<td class="details-panel-title-td"><input type="text" data-properties-type="details-panel-title" class="form-text"/></td>';
                    //                             ret += '<td colspan="2"><textarea class="detP-formula form-textarea" data-properties-type="details-panel-formula" data-ide-properties-type="detP-formula"></textarea><label><input type="checkbox" value="yes" class="css-checkbox linkmakerClass" id="linkmaker0" /><label for="linkmaker0" class="linkmakerLabel css-label"></label> Create Link</label>';
                                                    
                    //                             ret += '</td>';
                                                
                    //                         ret += '</tr>';
                    //                         ret += '<tr class="link-form-select">';
                    //                             ret += '<td colspan="2">';
                    //                                 ret += '<select class="form-select" data-properties-type="linkInputAdder-field" data-object-id="" id="">';
                    //                                 ret += '</select>';
                    //                             ret += '</td>';
                    //                         ret += '</tr>';
                                            
                    //                         ret += '<tr class="linkInputAdder">';
                    //                             ret += '<td colspan="2">';
                    //                                 ret += '<table class="linkInputAdder-table">';
                    //                                     ret += '<tbody class="linkInputAdder-table-tbody">';

                    //                                         ret += '<tr>'; 
                    //                                             ret += '<td colspan="3"><div><span class="detP-add-minus-row"><i class="fa fa-plus linkInputAdder-add-row"></i> <i class="fa fa-minus linkInputAdder-minus-row"></i></span></div></td>';
                    //                                         ret += '</tr>';
                    //                                         ret += '<tr>';
                    //                                             ret += '<td><select class="form-select" data-properties-type="linkInputAdder-field" data-object-id="" id="">';
                    //                                             ret += '</select></td>';
                    //                                             ret += '<td><select class="form-select" data-properties-type="linkInputAdder-operator" data-object-id="" id="">';
                    //                                             ret += '</select></td>';
                    //                                             ret += '<td><input type="text" data-properties-type="linkInputAdder-value" class="form-text"/></td>';
                    //                                         ret += '</tr>';
                    //                                     ret += '</tbody>';
                    //                                 ret += '</table>';
                    //                             ret += '</td>';
                                                
                    //                         ret += '</tr>';
                    //                     ret += '</tbody>';
                    //                 ret += '</table>';
                    //             ret += '</table>';
                    //     ret +='</div>';
                    // ret += '</div>';
                ret += '</div>';
            //ret += '</div>';
            ret += '<div class="fields">';
            ret += '<div class="label_basic"></div>';
            ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
            ret += ' <input type="button" class="btn-basicBtn saveDetailsPanel" value="Save">';
            ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
            ret += '</div>';
            ret += '</div>';

            var newDialog = new jDialog(ret, "", "800", "", "", function() {});
            newDialog.themeDialog("modal2");

            getUserFormsV2(function(data) {
                try {
                    data = JSON.parse(data);
                    $('.details-condition-form').html('<option value="0">--Select Form--</option>');
                    data = data.sort(function(a, b) {
                        return a['form_name'].toLowerCase().localeCompare(b['form_name'].toLowerCase());
                    });
                    for (var index in data) {
                        $('.details-condition-form').append('<option value="' + data[index].wsID + '" fields="' + data[index].active_fields + '">' + data[index].form_name + '</option>');
                    }

                    // $('.display_columns').html('');
                    // $('.picklist_form_field').html('<option value="0">----------------------------Select----------------------------</option>');
                } catch (errz) {
                    console.error("the json data of picklist forms is not valid search: $('body').on('click', '[data-type='picklist_selection']', function() {");
                }
            });

            detailPanelPlusMinus();

            // $('.linkInputAdder').hide();
            // $('.link-form-select').hide();
            // $('.linkmakerClass').each(function(){
                
                
            //     $(this).on('click',function(){

            //         if($(this).attr("checked")){
            //             $(this).closest('tbody.detP-tr-group').find('.linkInputAdder').show();
            //             $(this).closest('tbody.detP-tr-group').find('.link-form-select').show()
            //         }
            //         else{
            //             $(this).closest('tbody.detP-tr-group').find('.linkInputAdder').hide();
            //             $(this).closest('tbody.detP-tr-group').find('.link-form-select').hide();
            //         }
            //     });
            // });
            // MakePlusPlusRowV2({
            //     "tableElement":".detP-master-container",
            //     "rules":[
            //         {
            //             "eleMinus":".linkInputAdder-minus-row",
            //             "eleMinusTarget":"tbody.linkInputAdder-table-tbody",
            //             "elePlus":".linkInputAdder-add-row",
            //             "elePlusTarget":"tbody.linkInputAdder-table-tbody",
            //             "elePlusTargetFn":function(){
            //                 var self_ele = $(this);
            //                 self_ele.css({
            //                     "margin-top":"15px",
            //                 });
            //                 if(self_ele.closest('.linkInputAdder-table').find('.linkInputAdder-table-tbody').length>1){
            //                     self_ele.find('[data-properties-type="linkInputAdder-field"]').val("");
            //                     self_ele.find('[data-properties-type="linkInputAdder-operator"]').val("");
            //                     self_ele.find('[data-properties-type="linkInputAdder-value"]').val("");
            //                     self_ele.closest('.linkInputAdder-table').find('.linkInputAdder-table-tbody').each(function(){
            //                         $(this).find('.fa.fa-minus:eq(0)').show();
            //                     });
            //                 }
            //             },
            //             "eleMinusTargetFnBefore":function(){
            //                 var self_ele = $(this);
                            
            //                 if(self_ele.closest('.linkInputAdder-table').find('tbody.linkInputAdder-table-tbody').length<=2){

            //                     self_ele.closest('.linkInputAdder-table').find('tbody.linkInputAdder-table-tbody').each(function(){
            //                         $(this).find('.fa.fa-minus:eq(0)').hide();
            //                     });
                                
            //                 }
            //             }
            //         },
            //         {
            //             "eleMinus":".detP-minus-row",
            //             "eleMinusTarget":".detP-slave-container",
            //             "elePlus":".detP-add-row",
            //             "elePlusTarget":".detP-slave-container",
            //             "elePlusTargetFn":function(){
            //                 linkmaker_ctr++;
                            
            //                 var self_ele = $(this);
            //                 if(self_ele.closest('.detP-master-container').find('.detP-slave-container').length>=1){
            //                     self_ele.closest('.detP-master-container').find('.detP-slave-container').each(function(){
            //                         $(this).find('.fa.fa-minus:eq(0)').show();
            //                         // self_ele.find('.fa.fa-minus:eq(0)').show();
            //                     });
            //                 }
                            
            //                 self_ele.css({
            //                     "margin-top":"15px",
            //                 });
            //                 self_ele.find('[data-properties-type="linkInputAdder-field"]').val("");
            //                 self_ele.find('[data-properties-type="linkInputAdder-operator"]').val("");
            //                 self_ele.find('[data-properties-type="linkInputAdder-value"]').val("");
            //                 self_ele.find('input[data-properties-type="details-panel-name"]:eq(0)').val("");
            //                 self_ele.find('input[data-properties-type="details-panel-title"]').val("");
            //                 self_ele.find('tbody.linkInputAdder-table-tbody:not(:eq(0))').each(function(){
            //                     $(this).find('.fa.fa-minus:eq(0)').trigger('click');
            //                 });
            //                 self_ele.find('tbody.detP-tr-group:not(:eq(0))').each(function(){
            //                     $(this).find('.fa.fa-minus:eq(0)').trigger('click');
            //                 });

            //                  self_ele.find('.linkmakerClass').prop("checked",true).trigger("click");   
            //                 self_ele.find('.linkmakerClass').attr("id","linkmaker"+linkmaker_ctr);
            //                 self_ele.find('.linkmakerLabel').attr("for","linkmaker"+linkmaker_ctr);
            //             },
            //             "eleMinusTargetFnBefore":function(){
                           
            //                 if($('.detP-slave-container').length<=2){
            //                     $('.detP-slave-container').each(function(){
            //                         $(this).find('.fa.fa-minus:eq(0)').hide();
            //                     });
            //                 }
            //             }
            //         },
            //         {
            //             "eleMinus":".detP-minus-row-inner",
            //             "eleMinusTarget":"tbody.detP-tr-group",
            //             "elePlus":".detP-add-row-inner",
            //             "elePlusTarget":"tbody.detP-tr-group",
            //             "elePlusTargetFn":function(){
            //                 linkmaker_ctr++;
            //                 var self_ele = $(this);
            //                 self_ele.find('.fa.fa-minus:eq(0)').show();
            //                 self_ele.find('[data-properties-type="linkInputAdder-field"]').val("");
            //                 self_ele.find('[data-properties-type="linkInputAdder-operator"]').val("");
            //                 self_ele.find('[data-properties-type="linkInputAdder-value"]').val("");
            //                 self_ele.find('input[data-properties-type="details-panel-title"]').val("");
            //                 self_ele.closest('.detP-slave-container').find('tbody.detP-tr-group').each(function(){
            //                     $(this).find('.fa.fa-minus:eq(0)').show();
            //                 });
            //                 self_ele.find('tbody.linkInputAdder-table-tbody:not(:eq(0))').each(function(){
            //                     $(this).find('.fa.fa-minus:eq(0)').trigger('click');
            //                 });
            //                 self_ele.find('.linkmakerClass').prop("checked",true).trigger("click");
            //                 self_ele.find('.linkmakerClass').attr("id","linkmaker"+linkmaker_ctr);
            //                 self_ele.find('.linkmakerLabel').attr("for","linkmaker"+linkmaker_ctr);
                            

            //             },
            //             "eleMinusTargetFnBefore":function(){
            //                 var self_ele = $(this);
            //                 if(self_ele.closest('.detP-slave-container').find('tbody.detP-tr-group').length<=2){
            //                     self_ele.closest('.detP-slave-container').find('tbody.detP-tr-group').each(function(){
            //                         $(this).find('.fa.fa-minus:eq(0)').hide();
            //                     });
            //                 }
            //             }
            //         }
            //     ]
            // });
            restoreData($('body').data());
            // if($.type($('body').data('saved_details_panel')) != "undefined"){
            //     var default_details_view = $('body').data('default_detail_view');
            //     if($.type(default_details_view)!="undefined"){
            //         if(default_details_view=="yes"){
            //             $('.detail-default-view').prop('checked',true);
            //         }
            //         else{
            //             $('.detail-default-view').prop('checked',false);
            //         }
            //     }
            //     var saved_details_panel = $('body').data('saved_details_panel');
            //     var sdp_length = saved_details_panel.length;
            //     var slave_container_ctr = 0;
            //     if (sdp_length >= 1){
            //         for(var ctr in saved_details_panel){
            //             if(ctr != 0){
            //                 $(".detP-add-row:eq(0)").trigger("click");
            //             }

            //         }
                   
            //         $('.detP-slave-container').each(function(){
            //             var self = $(this);

            //             self.find('input[data-properties-type="details-panel-name"]:eq(0)').val(saved_details_panel[slave_container_ctr]['container_name']);
            //             for(var ctr in saved_details_panel[slave_container_ctr]['title_formula_array']){
                             
            //                  var title_formula_array_title_val = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['title'];
            //                  var title_formula_array_formula_val = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['formula'];
            //                 if(ctr != 0){
                                
            //                     self.find('tbody.detP-tr-group:eq('+(Number(ctr)-1)+')').find('.detP-add-row-inner:eq(0)').trigger("click"); 
            //                 }
            //                 var self_children_group_link_box = self.find('tbody.detP-tr-group:eq('+ctr+')').find('tr.linkInputAdder:eq(0)');
            //                 self.find('input[data-properties-type="details-panel-title"]:eq('+ctr+')').val(title_formula_array_title_val);
            //                 self.find('[data-properties-type="details-panel-formula"]:eq('+ctr+')').val(title_formula_array_formula_val);
                            
            //                 if(saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link'] == "yes"){
                                
            //                     self.find('.linkmakerClass:eq('+ctr+')').prop("checked",false).trigger("click");
            //                 }
            //                 else{

            //                      self.find('.linkmakerClass:eq('+ctr+')').prop("checked",true).trigger("click");
            //                 }
            //                 for(var ctr2 in saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link_val']){
                                
            //                     var link_value = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link_val'][ctr2]["link_value"];
            //                     var link_field = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link_val'][ctr2]["form_id"];
            //                     var link_operator = saved_details_panel[slave_container_ctr]['title_formula_array'][ctr]['link_val'][ctr2]["link_operator"]
            //                     if(ctr2 != 0){    
            //                         self_children_group_link_box.find('.linkInputAdder-add-row:eq('+Number(ctr2-1)+')').trigger("click");
            //                     }
            //                     var linkInputAdder_fields = self_children_group_link_box.find('tbody.linkInputAdder-table-tbody:eq('+ctr2+')');
            //                     linkInputAdder_fields.find('[data-properties-type="linkInputAdder-field"]').val(link_field);
            //                     linkInputAdder_fields.find('[data-properties-type="linkInputAdder-operator"]').val(link_operator);
            //                     linkInputAdder_fields.find('[data-properties-type="linkInputAdder-value"]').val(link_value);

            //                 }
            //             }

            //             slave_container_ctr++;
                        
            //         }); 
                    
            //     }

            // }
        });
        $('body').on('change', '.details-condition-form', function() { //DITO NA KO
            var formid = $(this).val();
            var fields = $(this).find('option:selected').attr('fields');
            try{
                //var exisiting = fields.indexOf()
                fields = fields.split(',');
                if ($.type(fields) == "array") {
                    fields.push("TrackNo");
                    fields.push("Requestor");
                    fields.push("Processor");
                    fields.push("Status");
                    fields.push("DateCreated");
                    fields = fields.filter(Boolean);
                    if (fields.length >= 1) {
                        fields = fields.sort(function(a, b) {
                            return a.toLowerCase().localeCompare(b.toLowerCase());
                        });
                        fields = $.unique(fields);
                    }
                }

                // $('.display_columns').html('');
                $(this).closest(".detP-tr-group").find('.details-condition-fields').html('<option value="0">--Select Fields--</option>');
                for (var ctr = 0; ctr <= fields.length - 1; ctr++) {
                    $(this).closest(".detP-tr-group").find('.details-condition-fields').append('<option value="' + fields[ctr] + '">' + fields[ctr] + '</option>');
                }
            }catch(err){
                $(this).closest(".detP-tr-group").find('.details-condition-fields').html('<option value="0">--Select Fields--</option>');
            }
        });
    
        $("body").on("click", "#fl-save-form-icon", function(){           
            var selectedIcon = $(".fl-selected-box-border").find("use").attr("xlink:href");            
            $("#allow-portal").attr("selected-icon", selectedIcon);
            $('#popup_cancel').click();
        });
    
        $("body").on("click", ".custom_icon_wrapper", function() {
            $(".custom_icon_wrapper").removeClass("fl-selected-box-border");
            $(this).addClass("fl-selected-box-border");            
        });
    
        $("body").on("click", ".choose_app_icon", function(){
            var ret = $("#fl-icon-selection-dialog-template").html();
            ret = ret.replace(/\n/g, "");

            var newDialog = new jDialog(ret, "", "800", "", "", function() {});            
            newDialog.themeDialog("modal2");

            var appSelected = $("#allow-portal").attr('selected-icon');
            var replacetoStrinsplittedSvgIcon = appSelected.replace(/[,#]/g, "");
            $('.custom_icon_wrapper.'+replacetoStrinsplittedSvgIcon).addClass('fl-selected-box-border');

        });

    },
    "saveSequence" : function(){
        $("body").on("click",".saveSequence",function(){
            var fieldArray = [];
            $(".sequence-field").each(function(){
                fieldArray.push($(this).attr("field-name"));
            });
            

            // $('.field-process-sequence').data("saved_seq_field_array",fieldArray);
            $('body').data("saved_seq_field_array",fieldArray);
            $(".fl-closeDialog").click();
        });
        $("body").on("click",".saveDetailsPanel",function(){
            saveDetailsPanelData();
            // var detP_array = [];
            // var isShow = $('.detail-default-view:eq(0):checked').val();
            // if($.type(isShow)=="undefined"){
            //     isShow = "no"
            // }
            // $(".detP-slave-container").each(function(){
            //     var container_name = $(this).find('input[data-properties-type="details-panel-name"]:eq(0)').val();
                
            //     detP_array.push({"container_name":container_name,
            //         "title_formula_array":[],
            //         "default_view": isShow,
            //     });
            //     $(this).find(".detP-tr-group").each(function(){
            //         var dp_title = $(this).find('input[data-properties-type="details-panel-title"]').val();
            //         var dp_formula = $(this).find('[data-properties-type="details-panel-formula"]').val();
            //         var islink = $(this).find('.linkmakerClass:checked').val();
            //         var link_form = $(this).find('tr.link-form-select').find('[data-properties-type="linkInputAdder-field"]').val();
            //         var link_val_array = [];
            //         if($.type(islink) == "undefined"){
            //             islink = "no";
            //         }
            //         $(this).find('.linkInputAdder-table').find('.linkInputAdder-table-tbody').each(function(){
            //             var form_id = $(this).find('[data-properties-type="linkInputAdder-field"]').val();
            //             var operator = $(this).find('[data-properties-type="linkInputAdder-operator"]').val();
            //             var link_val = $(this).find('[data-properties-type="linkInputAdder-value"]').val();

            //             link_val_array.push({
            //                 "form_id":form_id,
            //                 "link_operator":operator,
            //                 "link_value":link_val
            //             });
            //         });

            //         detP_array[detP_array.length-1]["title_formula_array"].push({"title":dp_title,
            //             "formula":dp_formula,
            //             "link": islink,
            //             "link_val":link_val_array,
            //             "link_form":link_form
                        
            //         });

            //     });
                
            // });
            // $('body').data("saved_details_panel",detP_array);
            // $('body').data("default_detail_view",isShow);
            // $(".fl-closeDialog").click();
        });
        $("body").on("click",".saveLandingPage",function(){
           var page_val =  $(this).closest('#popup_container').find('select[data-properties-type="landing-page"]').val();
            if(page_val=="others"){
                page_val = $(this).closest('#popup_container').find('input[data-properties-type="landing-page-others"]').val();
                if($.trim(page_val) == ""){
                   var tooltip_position = $('[data-properties-type="landing-page-others"]');
                   var tooltip_top = NotifyMe('input[data-properties-type="landing-page-others"]',"invalid input","landing-page-blank-notif");

                   var top_tooltip = tooltip_position.position().top;
                   var left_tooltip = tooltip_position.position().left;
                    
                   tooltip_top.css("top",(top_tooltip-31)+"px");
                   tooltip_top.css("left",(left_tooltip+39)+"px"); 
                }
                else if(!CheckUrl(page_val)){
                    NotifyMe('input[data-properties-type="landing-page-others"]',"invalid input","landing-page-blank-notif");
                }
                else{
                    $('body').data("saved_landing_page",page_val);
                    $(".fl-closeDialog").click();
                }
            }
            else{
                $('body').data("saved_landing_page",page_val);
                $(".fl-closeDialog").click();
            }
        //

        
            
           

        });

    },
    "restoreSequence":function(){

    }
}

function checkIfFormUser(json,formUsers){
    var jsonReturn = {};
    if(typeof json=="string"){
        json = JSON.parse(json);
    }
    if(json!=undefined){
        
        // console.log(formUsers);
        // console.log(json)
        $.each(json,function(key, value){
            jsonReturn[key] = [];
            for(var i in value){
                // console.log(value[i]+" = "+formUsers[key])
                if(formUsers[key]!=undefined){
                    // console.log(value[i]+" = "+formUsers[key]+" -> "+formUsers[key].indexOf(value[i]))
                    if(formUsers[key].indexOf(value[i])>=0){

                        jsonReturn[key].push(value[i]);
                    }
                }
            }
        })
        
    }
    return jsonReturn;
}
function mergeFormUser(defaults,options,originalChecked){
    var ret = {};
    $.each(options, function(key, value){
        
        if(typeof originalChecked=="string"){
            try{
                originalChecked = JSON.parse(originalChecked);
            }catch(err){
                console.log(err)
            }
        }
        //check if undefined ung key nung original
        if(originalChecked[''+ key +'']!=undefined){
            // console.log(originalChecked[''+ key +''])

            for(var i in value){
                console.log(value[i])
                //check if exist in original(ibig sabihin dating naka check)
                //check if wala pa sa unchecked ng element
                if(originalChecked[''+ key +''].indexOf(value[i])>=0){
                    if(defaults[key]!=undefined){
                        if(defaults[key].indexOf(value[i])<0){
                            defaults[key].push(value[i]);
                        }
                    }else{
                        defaults[key] = [value[i]];
                    }
                }
            }
        }
    })
    return defaults;
}
ToggleWithFormula = {
    toggleEvent : function(obj,data_1,data_2){
        var self = this;
        $("body").on("change",".fm-type-dos",function(){
            self.toggleFunction(this,data_1,data_2);
        })
    },
    toggleFunction : function(obj,data_1,data_2){
        // console.log("data_1",data_1)
        // console.log("data_2",data_2)
        // alert($(obj).val()+" == "+data_1['value'])
        if($(obj).val()==data_1['value']){
            $(data_1['display']).show();
            $(data_2['display']).hide();
        }else{
            $(data_1['display']).hide();
            $(data_2['display']).show();
        }
    }
}

WorkspaceVersion = {
    "init" : function(){
        $("body").on("change",".workspace_version",function(){
            if($(this).val()=="2"){
                $(".workspace_status").closest(".workspace_status_container").hide();
            }else{
                $(".workspace_status").closest(".workspace_status_container").show();
            }
        })
    }
}

FormVariables = {
    "init":function(){
        var self = this;
        this.load();
    },
    "load":function(){
        var self = this;
        $('.form-data-variable').on('click',function(e){
            var newDialog = new jDialog(self["dialogDesign2"], "", "", "", "70", function() {});
            newDialog.themeDialog("modal2");
            
            $('#popup_container').css('max-width','750px');
            $('#popup_container').css('width','750px')
            $('#popup_container').data('fv_data_ctr',0);
            var cds = $('#popup_container').find('.content-dialog-scroll');
            cds.perfectScrollbar();
            MakePlusPlusRowV2({
                "tableElement":".fv-tbody-container",
                "rules":[
                    {
                        "eleMinus":".fv-remove-data-variable",
                        "eleMinusTarget":".fv-tr-container",
                        "elePlus":".fv-add-data-variable",
                        "elePlusTarget":".fv-tr-container",
                        "elePlusTargetFnBefore":function(){
                            var self_ele = $(this);
                            var ctr = $('#popup_container').data('fv_data_ctr');
                            var radio_btn = self_ele.find(".fv-compute-type");
                            ctr++;
                            
                            radio_btn.attr('name',"fv-compute-type"+ctr);
                            radio_btn.filter('[value="static"]').attr('id', 'fv-compute-type-static'+ctr).next().attr('for','fv-compute-type-static'+ctr);
                            radio_btn.filter('[value="dynamic"]').attr('id', 'fv-compute-type-dynamic'+ctr).next().attr('for','fv-compute-type-dynamic'+ctr);

                            $('#popup_container').data('fv_data_ctr',ctr);

                        },
                        "elePlusTargetFn":function(e,data){

                            data = data['parameter'][0]||{};
                            var self_ele = $(this);
                            var trs = self_ele.parent().children(".fv-tr-container"); 
                            var remove_data_var_ele = trs.children().children().children().children('.fv-remove-data-variable');
                            if( trs.length > 1 ){
                                remove_data_var_ele.removeClass('isDisplayNone');
                            }

                            if(data['var_compute_type']){
                                self_ele.find('.fv-compute-type[value="'+data['var_compute_type']+'"]').prop('checked',true);
                            }else{
                                self_ele.find('.fv-compute-type[value="'+"static"+'"]').prop('checked',true);
                            }

                            if(data['var_formula']){
                                self_ele.find('.fv-data-formula').val(data['var_formula']);
                            }else{
                                self_ele.find('.fv-data-formula').val("");
                            }

                            if(data['var_name']){
                                self_ele.find('.fv-data-name').val(data['var_name']);
                            }else{
                                self_ele.find('.fv-data-name').val("");
                            }
                            cds.perfectScrollbar();
                            $('#popup_container').find('.content-dialog-scroll.ps-container').perfectScrollbar("update");
                        },
                        "eleMinusTargetFnBefore":function(){
                            var self_ele = $(this);
                            var trs = self_ele.parent().children(".fv-tr-container");
                            var remove_data_var_ele = trs.children().children().children().children('.fv-remove-data-variable');
                            console.log("remove_data_var_ele VOOM",remove_data_var_ele);
                            if( trs.length == 2 ){
                                remove_data_var_ele.addClass('isDisplayNone');
                            }
                            $('#popup_container').find('.content-dialog-scroll.ps-container').perfectScrollbar("update");
                        }
                    }
                ]
            });
            (function(){ // data restoration
                var load_form_variables = $('body').data('form_variables')||[];
                var variable_container_elem = $('.fv-tbody-container');
                var first_fv_tr_container = variable_container_elem.children(".fv-tr-container:eq(0)");
                var fv_add_ele = first_fv_tr_container.find('.fv-add-data-variable');
                if( load_form_variables.length >= 1 ){
                    first_fv_tr_container.find('.fv-data-name').val(load_form_variables[0]["var_name"]);
                    first_fv_tr_container.find('.fv-data-formula').val(load_form_variables[0]["var_formula"]);
                    first_fv_tr_container.find('.fv-compute-type[value="'+load_form_variables[0]["var_compute_type"]+'"]').prop("checked",true);   
                    var ctr_z = load_form_variables.length-1;
                    for(var ctr in load_form_variables){
                        if(ctr_z != 0){
                            fv_add_ele.trigger('click',[ load_form_variables[ctr_z] ]);
                        }
                        ctr_z--;
                    }
                }
            })();

            $('#popup_container').find('.fv-save-data').on('click',function(){
                var variable_container_elem = $('.fv-tbody-container');
                var collector_saver = [];
                var error_occured = 0;
                variable_container_elem.children(".fv-tr-container").each(function(){
                    var self = $(this);
                    var var_name_ele = self.find('.fv-data-name');
                    var var_name = $.trim(var_name_ele.val());
                    var var_formula = $.trim(self.find('.fv-data-formula').val());
                    var var_compute_type_ele = self.find('.fv-compute-type:checked');
                    var var_not_allowed_names = new RegExp("[^a-zA-Z0-9]*","g");
                    var matches_var_not_allowed_names = var_name.match(var_not_allowed_names)||[];
                    matches_var_not_allowed_names = matches_var_not_allowed_names.filter(Boolean);
                    if(var_name == ""){
                        NotifyMe(var_name_ele, "Empty!", "fv-validation-empty");
                        error_occured++;
                    }else if(matches_var_not_allowed_names.length >= 1){
                        var notif_ele = NotifyMe(var_name_ele, "Alphanumeric only", "fv-validation");
                        notif_ele.css({
                            "left":"50px",
                            "top":"-10px"
                        });
                        error_occured++;
                    }else{
                        collector_saver.push({
                            "var_name":var_name,
                            "var_formula":var_formula,
                            "var_compute_type":var_compute_type_ele.val()
                        });
                    }
                });

                console.log("collector_saver", collector_saver)

                if( error_occured == 0 ){
                    $('body').data("form_variables",collector_saver);
                    $('.fl-closeDialog').trigger('click');
                }else{
                    return true;
                }
            });
        });
    },
   /* "dialogDesign":
        '<h3 class="fl-margin-bottom">'+
        '<i class="icon-save"></i> Data Sources'+
        '</h3>'+
        '<div class="hr"></div>'+
        '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>'+
        '<div class="content-dialog-scroll">'+
            '<div class="content-dialog" style="height: auto;border:none;">'+
                '<div class="fields" style="">'+
                    '<div class="label_basic"></div>'+
                    '<div class="input_position_below" style="margin-top:5px;">'+
                        '<table style="width:100%;display:inline-table;">'+
                            '<thead>'+
                                '<tr class="fv-th-container">'+
                                    '<th style="padding-bottom:10px;padding-top:10px;font-weight:bold">Name:</th>'+
                                    '<th style="padding-bottom:10px;padding-top:10px;font-weight:bold" colspan="2">Formula:</th>'+
                                '</tr>'+
                            '</thead>'+
                            '<tbody class="fv-tbody-container">'+
                                '<tr class="fv-tr-container">'+
                                    '<td style="vertical-align: text-top;padding-right: 5px;"><input class="fv-data-name form-text"/></td>'+
                                    '<td style="padding-right: 5px;padding-bottom:5px"><textarea data-ide-properties-type="ide-data-source-value" placeholder="Click here to edit formula." readonly="readonly" class="fv-data-formula form-textarea" style="height:60px;margin:0;width:380px"></textarea></td>'+
                                    '<td style="vertical-align: text-top;padding-right: 5px;">'+
                                        '<input checked="checked" type="radio" id="fv-compute-type-static" class="css-checkbox fv-compute-type" value="static" name="fv-compute-type"/><label for="fv-compute-type-static" class="css-label" style="font-size:12px;">Compute on page load</label>'+
                                        '<input type="radio" id="fv-compute-type-dynamic" class="css-checkbox fv-compute-type" value="dynamic" name="fv-compute-type"/><label for="fv-compute-type-dynamic" class="css-label" style="font-size:12px;">Compute dynamically</label>'+
                                    '</td>'+
                                    '<td><span style="margin-right:3px;" class="fv-add-data-variable"><i class="fa fa-plus"></i></span><span class="isDisplayNone fv-remove-data-variable"><i class="fa fa-minus"></i></span></td>'+
                                '</tr>'+
                            '</tbody>'+
                        '<table>'+
                    '</div>'+
                    '<input type="button" class="btn-blueBtn fl-margin-right fv-save-data fl-positive-btn"  value="OK">'+ //id="buttonID"
                    '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">'+
                '</div>'+
            '</div>'+
        '</div>'    */
        "dialogDesign2":
            '<div><h3 class="fl-margin-bottom">'+
            '<svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-data-source"></use></svg> <span>Data Sources</span>'+
            '</h3></div>'+
            '<div class="hr"></div>'+
            '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>'+
            '<div class="content-dialog fv-tbody-container" style="height: auto;">'+
                // your content here
                '<div class="fields_below fv-tr-container">'+
                    '<div class="section clearing fl-field-style">'+
                        '<div class="column div_1_of_1">'+
                            '<span>Option: </span>'+
                            ' <input checked="checked" type="radio" id="fv-compute-type-static" class="css-checkbox fv-compute-type" value="static" name="fv-compute-type"/><label for="fv-compute-type-static" class="css-label" style="font-size:12px;">Compute on page load</label>'+
                            ' <input type="radio" id="fv-compute-type-dynamic" class="css-checkbox fv-compute-type" value="dynamic" name="fv-compute-type"/><label for="fv-compute-type-dynamic" class="css-label" style="font-size:12px;">Compute dynamically</label>'+
                            '<div class="fR">'+
                                '<div class="fv-add-data-variable isDisplayInlineBlock" style="margin-right:10px;"><i class="fa fa-plus"></i></div>'+
                                '<div class="fv-remove-data-variable isDisplayInlineBlock isDisplayNone"><i class="fa fa-minus"></i></div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="section clearing fl-field-style">'+
                        '<div class="column div_1_of_1" style="position:relative;">'+
                            '<span>Name:</span>'+
                            '<input type="text" class="fv-data-name form-text">'+
                        '</div>'+
                    '</div>'+
                    '<div class="section clearing fl-field-style">'+
                        '<div class="column div_1_of_1">'+
                            '<span>Formula:</span>'+
                            '<textarea class="form-textarea fv-data-formula" data-ide-properties-type="form-variables-formula"></textarea>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="fields" style="">'+
                    '<div class="label_basic"></div>'+
                    '<div class="input_position" style="margin-top:5px;">'+
                        '<input type="button" class="btn-blueBtn fl-margin-right fl-positive-btn fv-save-data" id="buttonID" value="OK">'+
                        '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">'+
                    '</div>'+
                '</div>'+
            '</div>'
}

// var newDialog = new jDialog(ret, "", "", "300", "", function() {   
        // });
        // newDialog.themeDialog("modal2"); 