$(document).ready(function () {

    ws_annotation.add_annotation();
    ws_annotation.cancel_annotation();
    ws_annotation.save_annotation();
    ws_annotation.actions_tools();
    ws_annotation.inactive_annotation();
    ws_annotation.alone_checker();
    activeBtn('.fl-annotation-tools');
     // to inactive annotation
    //wPaint-menu-icon-name-save").trigger("click")
    
    //$("#wPaint").samuelpulta
    $("body").on("click",".samuelpulta",function(){
        $('#wPaint').wPaint('setFillStyle');
    console.log($('#wPaint').wPaint('setFillStyle'));
    })
    

    // $("body").on("click",".tabAnnotation",function(){
    //     $(".addAnnotation").trigger("click")

    // })

    
    var requestID = $("#ID").val();

    // Hide Annotation if not current processor
    var processor_id = $("#ProcessorID").val();
    var current_user_id = $("#current-user-id").text()
    var split_processor = processor_id.split(",");

    if(requestID == 0){
            // $(".addAnnotation").hide()
            // $("#fl-form-controlsv-li2t").hide()
            // $("#wPaint").hide()
    }

    // if(jQuery.inArray(current_user_id, split_processor) !== -1){
    //     console.log(current_user_id + "--" + processor_id)
    // }else{
    //     console.log("samuel pulta")
    //     $(".addAnnotation").hide()
    //     $("#fl-form-controlsv-li2t").hide()
    //     $("#wPaint").hide()
    // }
    $(".cancelAnnotation").trigger("click")
    // if(current_user_id != processor_id){
        
    // }
    // console.log(processor_id +"=="+current_user_id)
});


var images = [];

function saveImg(image) {
    var _this = this;
    var FormName = $("#FormName").val();
    var TrackNo = $("#TrackNo").val();
    var FormID = $("#FormID").val();
    var ID = $("#ID").val();

    ui.block();

    $.ajax({
        type: 'POST',
        url: '/ajax/annotation',
        data: {image: image, action: "draft_annotation", FormName: FormName, TrackNo: TrackNo, FormID: FormID, ID: ID},
        success: function (resp) {
            //var resp = jQuery.parseJSON(resp);
            //console.log(resp)


            ui.unblock();

            //// internal function for displaying status messages in the canvas
            //_this._displayStatus('Image saved successfully');
            //
            //// doesn't have to be json, can be anything
            //// returned from server after upload as long
            //// as it contains the path to the image url
            //// or a base64 encoded png, either will work
            ////resp = $.parseJSON(resp);
            //
            //// update images array / object or whatever
            //// is being used to keep track of the images
            //// can store path or base64 here (but path is better since it's much smaller)
            ////images.push(resp.img);
            //
            //// do something with the image
            ////$('#wPaint-img').attr('src', image);
        }
    });
}

function loadImgBg() {

    // internal function for displaying background images modal
    // where images is an array of images (base64 or url path)
    // NOTE: that if you can't see the bg image changing it's probably
    // becasue the foregroud image is not transparent.
    this._showFileModal('bg', images);
}

function loadImgFg() {

    // internal function for displaying foreground images modal
    // where images is an array of images (base64 or url path)
    this._showFileModal('fg', images);
}

// var count_annotation = "0";
ws_annotation = {
    //load_annotation : function(){
    //  getParametersName(name, pathname)
    //},

    add_annotation: function () {

        $("body").on("click", ".addAnnotation", function () {
        
            var img = $("#current_annotation").val();
            var wp_length = $("#wPaint").length;
            console.log(wp_length,"TEST ME SAM ANNOTAION")
            if (wp_length == "0") {
                var ret = '<div id="wPaint" class="annotation-bg"></div>';
                $(".loaded_form_content").append(ret);
                var img = $("#current_annotation").val();
                console.log(img,"loob")
                $(".annotation-bg").css("background-image","url('"+img+"')")
            }else{
                var img = $("#current_annotation").val();
                console.log(img,"loob")
                $(".annotation-bg").css("background-image","url('"+img+"')")
            }
            console.log(img,"labas")
            //console.log(count)
            //count++;

            // Show
            $(".hideAnnotation").removeClass("isDisplayNone").addClass('isDisplayBlock'); // orig class is .display
            //$(this).addClass("display"); // for tabs only dont hide

            // init wPaint
            //'.loaded_form_content',
            $('#wPaint').wPaint({
                menuOffsetLeft: -35,
                menuOffsetTop: -50,
                saveImg: saveImg,
                loadImgBg: loadImgBg,
                loadImgFg: loadImgFg
            });

            wPaint_on(); //display block wpaint
            // Hide Other Tools
            $(".wPaint-menu-icon-name-save,.wPaint-menu-icon-name-loadBg,.wPaint-menu").css("display", "none");

            // Hide frm submit actions
            $(".frm-submit-ws-req").addClass('display'); // hide old submit action
            $('.fl-action-menu').addClass('isDisplayNone'); // display none fl action menu

            var addAnno = $(this).parents('.fl-workspace-control-container').find('button.addAnnotation');

            //console.log(addAnno)
            if (addAnno.attr('cancel') === 'false') {
                $('#wPaint').addClass('isDisplayNone'); //display none wpaint
                fl_action_menu_on(); // display block  fl action menu
            }
        });


    },
    cancel_annotation: function () {

        $("body").on("click", ".cancelAnnotation", function () {
            // Show
            $(".addAnnotation").removeClass("display");
            $(".hideAnnotation").addClass("display");

            $(".frm-submit-ws-req").removeClass('display');
            //$("#wPaint").unbind();
            var ctrl = $("#formCtrl").val();
            //var count = "0";

            if ($(this).attr('cancel') === 'false') {

                wPaint_off(); //display none wpaint
                $(this).children('svg').children('use').attr('xlink:href', '#svg-icon-workspace-control-requestview-addannotation');
                $(this).children('span').text('Add/View Annotation')
                if(ctrl == "classic"){
                    $(this).html('<i class="fa fa-plus"></i> Add/View Annotation')
                }
                $(this).removeClass('cancelAnnotation').addClass('addAnnotation');
               
                fl_action_menu_on(); // display block  fl action menu
              

                var addAnno = $(this).parent().find('.addAnnotation');

                trigger_tools_off(); // cursor not allowed trigger tools opacity 0.3
                saveDraftAnnotation_off(); // cursor not allowed saveDraftAnnotation opacity 0.3

                $(addAnno).on('click', function () {

                    $(addAnno).attr('cancel', true);
                    trigger_tools_on(); // cursor pointer trigger tools opacity normal 
                    saveDraftAnnotation_on(); // cursor pointer saveDraftAnnotation opacity normal 


                    $(addAnno).children('svg').children('use').attr('xlink:href', '#svg-icon-workspace-control-requestview-cancelannotation');
                    $(addAnno).children('span').text('Cancel Annotation')
                    if(ctrl == "classic"){
                        $(addAnno).html('<i class="fa fa-times"></i> Cancel Annotation')
                    }
                    $(addAnno).removeClass('addAnnotation').addClass('cancelAnnotation');

                    if ($(this).attr('cancel') === 'true') {
                        fl_action_menu_off(); // display none  fl action menu
                        wPaint_on(); //display block wpaint
                        $(this).on('click', function () {
                            $(this).attr('cancel', 'false');

                        });
                    }

                });

            }

        });



    },
    save_annotation: function () {
        $("body").on("click", ".saveDraftAnnotation", function () {
            // Show
            $(".addAnnotation").removeClass("display");
            $(".hideAnnotation").addClass("display");
            $(".wPaint-menu-icon-name-save").trigger("click");
            $(".frm-submit-ws-req").removeClass('display');
            var ctrl = $("#formCtrl").val();
            //$("#wPaint").unbind();
            var cancelAnno = $(this).parent().find('button.cancelAnnotation');

            wPaint_off(); //display none wpaint
            //$(cancelAnno).html('<i class="fa fa-pencil-square-o"></i> Add/View Annotation').removeClass('cancelAnnotation').addClass('addAnnotation').attr('cancel', 'false'); // toggling the cancel button
            $(cancelAnno).children('svg').children('use').attr('xlink:href', '#svg-icon-workspace-control-requestview-addannotation');
            $(cancelAnno).children('span').text('Add/View Annotation')
            if(ctrl == "classic"){
                $(cancelAnno).html('<i class="fa fa-plus"></i> Add/View Annotation')
            }
            
            $(cancelAnno).removeClass('cancelAnnotation').addClass('addAnnotation'); //.attr('cancel', 'false')
            $(cancelAnno).attr('cancel','false');
            trigger_tools_off(); // cursor not allowed trigger tools opacity 0.3 
            saveDraftAnnotation_off(); // cursor not allowed saveDraftAnnotation opacity 0.3 
            fl_action_menu_on(); // display block  fl action menu

            if ($(cancelAnno).attr('cancel') === 'false') {

                wPaint_off();
                //$('#wPaint').removeClass('isDisplayBlock').addClass('isDisplayNone'); //display none wpaint
                //$(cancelAnno).html('<i class="fa fa-pencil-square-o"></i> Add/View Annotation').removeClass('cancelAnnotation'); // toggling the cancel button
                fl_action_menu_on(); // display block  fl action menu
                $(cancelAnno).children('svg').children('use').attr('xlink:href', '#svg-icon-workspace-control-requestview-addannotation');
                $(cancelAnno).children('span').text('Add/View Annotation')
                if(ctrl == "classic"){
                    $(cancelAnno).html('<i class="fa fa-plus"></i> Add/View Annotation')
                }
                $(cancelAnno).removeClass('cancelAnnotation').addClass('addAnnotation');

                var addAnno = $(this).parent().find('.addAnnotation');
                $(addAnno).trigger('click');
                trigger_tools_off(); // cursor not allowed trigger tools opacity 0.3 
                saveDraftAnnotation_off(); // cursor not allowed saveDraftAnnotation opacity 0.3

                $(addAnno).on('click', function () {
                   
                    $(addAnno).attr('cancel', true);
                    trigger_tools_on();  // cursor pointer trigger tools opacity normal 
                    saveDraftAnnotation_on(); // cursor pointer saveDraftAnnotation opacity normal 

                    $(addAnno).children('svg').children('use').attr('xlink:href', '#svg-icon-workspace-control-requestview-cancelannotation');
                    $(addAnno).children('span').text('Cancel Annotation')
                    if(ctrl == "classic"){
                        $(addAnno).html('<i class="fa fa-times"></i> Cancel Annotation')
                    }
                    $(addAnno).removeClass('addAnnotation').addClass('cancelAnnotation');

                    if ($(this).attr('cancel') === 'true') {
                        fl_action_menu_off(); // display none  fl action menu
                        wPaint_on();  //display block wpaint
                        $(this).on('click', function () {
                            $(this).attr('cancel', 'false');

                        });
                    }

                });

            }
        });

    },
    actions_tools: function () {
        $("body").on("click", ".trigger_tools", function () {
            var dataType = $(this).attr("data-type");
            $(".wPaint-menu-icon-name-" + dataType).trigger("click");
        });
    },
    inactive_annotation: function () {
        $('body').on('click', '.inactive_annotation', function () {

            fl_action_menu_on();  // display block  fl action menu
            $('#wPaint').addClass('isDisplayNone'); //display none wpaint

        });
    },

    alone_checker: function(){
        check_if_alone();
       
    }

};


function trigger_tools_on() {
    $('.trigger_tools').removeClass('isCursorNotAllowed').addClass('isCursorPointer').css('opacity', ''); // cursor pointer trigger tools opacity normal 

}

function  trigger_tools_off() {
    $('.trigger_tools').removeClass('isCursorPointer').addClass('isCursorNotAllowed').css('opacity', '0.3'); // cursor not allowed trigger tools opacity 0.3
}

function wPaint_on() {
    $('#wPaint').removeClass("isDisplayNone").addClass('isDisplayBlock');  //display block wpaint
}

function wPaint_off() {
    $('#wPaint').removeClass('isDisplayBlock').addClass('isDisplayNone'); //display none wpaint
}

function saveDraftAnnotation_on() {
    $('.saveDraftAnnotation').removeClass('isCursorNotAllowed').addClass('isCursorPointer').css('opacity', ''); // cursor pointer saveDraftAnnotation opacity normal
}

function saveDraftAnnotation_off() {
    $('.saveDraftAnnotation').removeClass('isCursorPointer').addClass('isCursorNotAllowed').css('opacity', '0.3'); // cursor not allowed saveDraftAnnotation opacity 0.3
}

function fl_action_menu_on() {
    $('.fl-action-menu').removeClass('isDisplayNone').addClass(' isDisplayBlock');  // display block  fl action menu
}

function fl_action_menu_off() {
    $('.fl-action-menu').removeClass('isDisplayBlock').addClass('isDisplayNone');  // display none  fl action menu
}

function check_if_alone(){

    if ($('#fl-tabs-formcontrolsv2 li:visible').length == 1) {

       
        $('.addAnnotation').trigger('click');

        $('button.addAnnotation').on('click', function(){



           var wp_length = $("#wPaint").length;
            if (wp_length == "0") {
                var ret = '<div id="wPaint" class="annotation-bg"></div>';
                $(".loaded_form_content").append(ret);
            }
            //console.log(count)
            //count++;

            // Show
            $(".hideAnnotation").removeClass("isDisplayNone").addClass('isDisplayBlock'); // orig class is .display
            //$(this).addClass("display"); // for tabs only dont hide
            // init wPaint
            //'.loaded_form_content',
            $('#wPaint').wPaint({
                menuOffsetLeft: -35,
                menuOffsetTop: -50,
                saveImg: saveImg,
                loadImgBg: loadImgBg,
                loadImgFg: loadImgFg
            });

            //wPaint_on(); //display block wpaint
            // Hide Other Tools
            $(".wPaint-menu-icon-name-save,.wPaint-menu-icon-name-loadBg,.wPaint-menu").css("display", "none");

            // Hide frm submit actions
            $(".frm-submit-ws-req").addClass('display'); // hide old submit action
            $('.fl-action-menu').addClass('isDisplayNone'); // display none fl action menu

            var addAnno = $(this).parents('.fl-workspace-control-container').find('button.addAnnotation');

            //console.log(addAnno)
            if (addAnno.attr('cancel') === 'false') {
                $('#wPaint').addClass('isDisplayNone'); //display none wpaint
                fl_action_menu_on(); // display block  fl action menu
            }
        // ws_annotation.add_annotation();
       })
       
    };
}


function activeBtn(parent){
    
    var annoTools = $(parent).find('button');
    //console.log($('.fl-annotation-tools').find('button'));


    $(annoTools).attr('anno-active-link', 'false');

    annoTools.each(function(){

       $(this).on({

            'click':function(){

                var data_type = $(this).attr('data-type');

               if ($(this).attr('anno-active-link') == 'false') {

                    $(this).attr('anno-active-link', 'true');

                    $(this).addClass('fl-active-link-color');

                    $(this).siblings().attr('anno-active-link', 'false');   
                    

                    if (data_type == 'clear') {

                        $(this).attr('anno-active-link', 'false');

                        $(this).removeClass('fl-active-link-color');
                       
                    
                    }else{
                        $(this).siblings().removeClass('fl-active-link-color');
                    }

               }

            }
       
       });



    });

}