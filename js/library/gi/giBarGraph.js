(function($){

Math.getPlaceValue = function(number){
	if(isNaN(number)){
		throw "Not a number.";
	}

	var numStr = number.toString();
	var output = "";
	var hasNonZero = false;
	for(var i=0; i<numStr.length; i++){
		if(numStr[i] === "-"){
			output += "-";
		}else if(numStr[i] === "0"){
			output += "0";
		}else if(numStr[i] === "."){
			output += ".";
		}else if(hasNonZero){
			output += "0";
		}else{
			output += "1";
			hasNonZero = true;
		}
	}

	return parseFloat(output);
}

Math.roundOff = function(number, placeValue){
	return placeValue == 0 || number == 0 ? 0 : Math.round((number)/placeValue)*placeValue;
}

Math.ceilBy = function(number, placeValue){
	return placeValue == 0 || number == 0 ? 0 :  Math.ceil((number)/placeValue)*placeValue;
}

Math.floorBy = function(number, placeValue){
	return placeValue == 0 || number == 0 ? 0 :  Math.floor((number)/placeValue)*placeValue;
}

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

function rgbHexToIntegers(color) {
	var match, quadruplet;

	// Match #aabbcc
	if (match = /#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(color)) {
		quadruplet = [parseInt(match[1], 16), parseInt(match[2], 16), parseInt(match[3], 16), 1];

		// Match #abc
	} else if (match = /#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(color)) {
		quadruplet = [parseInt(match[1], 16) * 17, parseInt(match[2], 16) * 17, parseInt(match[3], 16) * 17, 1];

		// Match rgb(n, n, n)
	} else if (match = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color)) {
		quadruplet = [parseInt(match[1]), parseInt(match[2]), parseInt(match[3]), 1];

	} else if (match = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9\.]*)\s*\)/.exec(color)) {
		quadruplet = [parseInt(match[1], 10), parseInt(match[2], 10), parseInt(match[3], 10),parseFloat(match[4])];

		// No browser returns rgb(n%, n%, n%), so little reason to support this format.
	} else {
		//quadruplet = colors[color];
		//console.log("error parsing color: " + color);
	}
	return quadruplet;
}

function decimalToHex(d, padding) {
    var hex = Number(d).toString(16);
    padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;

    while (hex.length < padding) {
        hex = "0" + hex;
    }

    return hex;
}


var GiChartColors = function(mixColor){
	var colors =
		["6699FF", "FF66FF","66FF66", "FF6666" , "FFFF66", "00FFFF",
		"9966FF", "FF3399","00FF99", "FF9933", "99FF33", "0099FF",
		"666699", "990099", "990000", "999966", "339933", "3366CC"];
		colors = ["7a98bf","ffdc7a","cccccc","95d9f0","f17979"];
	//["0099FF","FF66FF","66FF66","FFCC66","006699","CC6699","339966","FF6666"];
	var _index = 0;
	var index = function(newIndex){
		if(newIndex === undefined){
			return _index;
		}
		_index = newIndex;
	};

	this.next = function(){
		if(index() >= colors.length){
			index(0);
		}
		var color = colors[index()];
		if(mixColor !== undefined){
			var colorDec = rgbHexToIntegers(color);
			var mixColorDec = rgbHexToIntegers(mixColor);
			color = decimalToHex(Math.abs(colorDec[0] - mixColorDec[0])) +
			decimalToHex(Math.abs(colorDec[1] - mixColorDec[1])) +
			decimalToHex(Math.abs(colorDec[2] - mixColorDec[2]));
		}
		
		index(index()+1);
		return color;
	}
}

var giChartComponents = new (function(){
	
	this.generateFillOutFormButton = function(params){
		var buttonId = "gi-temp-"+(new Date().getTime());
		
		var $button = $("<button id='"+buttonId+"' class='button' title='New Request'><span class='fa fa-plus'></span></button>");
		
		var chartForms = params["chartForms"];
	
		var menuItems = {};
		for(var j=0; j<chartForms.length; j++){
			for(var i=0; i<chartForms[j].length; i++){
			
				menuItems[chartForms[j][i]["id"]] = {"name": chartForms[j][i]["name"]};
			}
		}
		
		//menuItems = { "1": {"name": "Under maintenance"}};
		
		
		$.contextMenu({
			selector: "#"+buttonId, 
			trigger: 'left',
			callback: function(key, options) {
				 window.open("/user_view/workspace?view_type=request&formID="+key+"&requestID=0", '_blank').focus();
			},
			items: menuItems
		});
		
		
		return $button;
	};
	
	this.generateFilterDialog = function(params){
		var filters = params.filters;
		var fields = params.fields;
		var $objEl = params.objEl;
		
		var $dialog = $("#giChartFilterDialog");
		if($dialog.size() == 0){
			$dialog = $("<div>");
			$dialog.attr("id", "giChartFilterDialog");
			
			$dialog.appendTo("body");
			$dialog.dialog({ modal: true, hide: true, width: 500, height: 300 });
			
			$dialog.on("click", ".removeFilter", function(e){
				$(e.currentTarget).closest("tr").remove();
			});
			
			$dialog.on("change", ".fieldList", function(e){
				var $fieldList = $(e.currentTarget);
				var type = $fieldList.find("option:selected").attr("data-type");
				var $valueInput = $fieldList.closest("tr").find(".valueInput");
				if(type == "Numeric"){
					$valueInput.spinner();
				}else if(type == "Date"){
					$valueInput.datepicker();
				}else if(type == "Text"){
					$valueInput.datepicker("destroy");
					$valueInput.spinner("destroy");
					$valueInput.autocomplete({ source: [] });
				}
			});
		}else{
			$dialog.empty();
		}
		
		$dialog.append("<p>Note: This filter is temporary and will not be saved.</p>");
		
		$dialog.dialog({
			buttons: [
				{
					text: "Apply",
					click: function(e) {
						var $filterRows = $(this).find(".filterTable tr.data");
						
						var filters = [];
						$filterRows.each(function(i, el){
							var $el = $(el);
							var filter = { 
								dataType: $el.find(".fieldList option:selected").attr("data-type"),
								field: $el.find(".fieldList").val(), 
								comparator: $el.find(".comparatorList").val(), 
								value: $el.find(".valueInput").val(), 
								logicalRelation: $el.find(".logicalList").val() 
							};
							
							if(filter.value !== null && filter.value !== ""){
								filters.push(filter);
							}
						});
						
						$objEl.data("giSessionData")["Filters"] = [filters];
						$objEl[$objEl.data("giIdentifierName")]("render");
						
						$(this).dialog("close");
						//$objEl.data("giSessionData");
						
						//DELETE
						//console.log();
					}
				}
			]
		});
		
		var $filterTable = $("<table class='filterTable'>");
		$filterTable.appendTo($dialog);
		
		//create filter options
		var fieldOptions = "";
		for(var foi=0; foi<fields.length; foi++){
			fieldOptions += "<option data-type='"+ fields[foi]["type"] +"'>"+ fields[foi]["name"] +"</option>";
		}
		
		//create comparator options
		var comparatorOptions = 
			"<option value='=='>=</option>"+
			"<option value='<'><</option>"+
			"<option value='<='><=</option>"+
			"<option value='>'>></option>"+
			"<option value='>='>>=</option>"+
			"<option value='!='><></option>";
			
		//create logical options
		var logicalOptions = 
			"<option value='&&'>and</option>"+
			"<option value='||'>or</option>";
		
		
		$filterTable.append("<tr>" +
			"	<th>Field</th>" +
			"	<th>Comparator</th>" +
			"	<th>Value</th>" +
			"	<th></th>" +
			"	<th></th>" +
			"</tr>");
			
		$filterTable.find("th").css({ fontWeight: "bold" });
		
		for(var fi=0; fi<filters.length; fi++){
			/*
			dataType: $el.find(".fieldList option:selected").attr("data-type"),
				field: $el.find(".fieldList").val(), 
				comparator: $el.find(".comparatorList").val(), 
				value: $el.find(".valueInput").val(), 
				logicalRelation: $el.find(".logicalList").val() 
			*/
			var filter = filters[fi];
			
			var $row = $("<tr class='data'>");
			$row.appendTo($filterTable);
			
			var $fieldCell = $("<td>");
			$fieldCell.appendTo($row);
			
			var $fieldList = $("<select class='fieldList'>");
			$fieldList.appendTo($fieldCell);
			$fieldList.append(fieldOptions);
			$fieldList.val(filter["field"]);
			
			var $comparisonCell = $("<td>");
			$comparisonCell.appendTo($row);
			
			var $comparatorList = $("<select class='comparatorList'>");
			$comparatorList.appendTo($comparisonCell);
			$comparatorList.append(comparatorOptions);
			$comparatorList.val(filter["comparator"]);
			
			var $valueCell = $("<td>");
			$valueCell.appendTo($row);
			
			var $valueInput = $("<input class='valueInput'>");
			$valueInput.appendTo($valueCell);
			$valueInput.val(filter["value"]);
			if(filter["type"] == "Numeric"){
				$valueInput.spinner();
			}else if(filter["type"] == "Date"){
				$valueInput.datepicker();
			}else if(filter["type"] == "Text"){
				$valueInput.datepicker("destroy");
				$valueInput.spinner("destroy");
				$valueInput.autocomplete({ source: [] });
			}
			
			var $logicalCell = $("<td>");
			$logicalCell.appendTo($row);
			
			var $logicalList = $("<select class='logicalList'>");
			$logicalList.appendTo($logicalCell);
			$logicalList.append(logicalOptions);
			$logicalList.val(filter["logicalRelation"]);
			
			var $deleteCell = $("<td>");
			$deleteCell.appendTo($row);
			var $deleteButton = $("<button title='Delete filter' class='removeFilter'><span class='fa fa-remove'>");
			$deleteCell.append($deleteButton);
		}
		
		var $addFilter = $("<button title='Add filter'><span class='fa fa-plus'>");
		$addFilter.appendTo($dialog);
		
		$addFilter.on("click", function(e){
			
			var $row = $("<tr class='data'>");
			$row.appendTo($filterTable);
			
			var $fieldCell = $("<td>");
			$fieldCell.appendTo($row);
			
			var $fieldList = $("<select class='fieldList'>");
			$fieldList.appendTo($fieldCell);
			$fieldList.append(fieldOptions);
			
			var $comparisonCell = $("<td>");
			$comparisonCell.appendTo($row);
			
			var $comparatorList = $("<select class='comparatorList'>");
			$comparatorList.appendTo($comparisonCell);
			$comparatorList.append(comparatorOptions);
			
			var $valueCell = $("<td>");
			$valueCell.appendTo($row);
			
			var $valueInput = $("<input class='valueInput'>");
			$valueInput.appendTo($valueCell);
			
			var $logicalCell = $("<td>");
			$logicalCell.appendTo($row);
			
			var $logicalList = $("<select class='logicalList'>");
			$logicalList.appendTo($logicalCell);
			$logicalList.append(logicalOptions);
			
			var $deleteCell = $("<td>");
			$deleteCell.appendTo($row);
			var $deleteButton = $("<button title='Delete filter' class='removeFilter'><span class='fa fa-remove'>");
			$deleteCell.append($deleteButton);
			
			
		});
		
		return $dialog;
	};
	
	
})();

$.fn.giBarGraph = function(methodName, params){

	var toUpdateData = false;
	var toCheckUpdate = false;
	var createCheckUpdateTimer = false;
	
	var cancelRender = false;
	var affected = [];
	var initialized = false;
	
	

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": "expand", "Height": "auto" };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giBarGraph")){
				
				initialized = true;
				toCheckUpdate = true;
				createCheckUpdateTimer = true;
				
				$objEl.addClass("giObject giChart giBarGraph");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giBarGraph",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Object Type": "Bar Graph",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "auto",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "#000",
					"Corner Radius": 8,
					"Padding": 8,
					"Margin": 4,
					"Background Color": "#fff",
					"New Record Button": "show",
					"Legends Position": "bottom",
					"Data Update Interval": 300,
					"Auto Update Data": true,
					"Query": [],
					"Show Session Filters": true,
					"Data URL": "",
					"On Data Updated": null
				});
				
				$objEl.data("giSessionData", {
					"Chart Data": undefined,
					"Data Drill Fields": [[]],
					"Data Slice Fields": [[]],
					"Filters": [[]],
					"Update XHR": null
				});
			

			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};

		this["get Query"] = function(){ return props["Query"]; };
		this["set Query"] = function(val){ 
			if(true){
				$objEl.data("giSessionData")["Data Drill Fields"] = [[]];
				$objEl.data("giSessionData")["Data Slice Fields"] = [[]];
				$objEl.data("giSessionData")["Chart Data"] = undefined;
				props["Query"] = val; 
				toUpdateData = true;
				
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
			
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? val : parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Padding"] = function(){ return props["Padding"]; }
		this["set Padding"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Padding"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Margin"] = function(){ return props["Margin"]; }
		this["set Margin"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Margin"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Background Color"] = function(){ return props["Background Color"]; }
		this["set Background Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Background Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
		this["set Corner Radius"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Corner Radius"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};			
		
		this["get Legends Position"] = function(){ return props["Legends Position"]; };
		this["set Legends Position"] = function(val){ 
			if(["top","left","bottom","right"].indexOf(val) > -1){
				props["Legends Position"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Data URL"] = function(){ return props["Data URL"]; };
		this["set Data URL"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Data URL"] = val;
				toUpdateData = true;				
			}else{
				return "Invalid value.";
			}
		};
		
		this["get New Record Button"] = function(){ return props["New Record Button"]; };
		this["set New Record Button"] = function(val){ 
			if(val === "show" || val === "hide"){
				props["New Record Button"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Auto Update Data"] = function(){ return props["Auto Update Data"]; }
		this["set Auto Update Data"] = function(val){
			var bools = { "true": true, "false": false };
			if(bools.hasOwnProperty(val.toString())){
				createCheckUpdateTimer = true;
				props["Auto Update Data"] = bools[val.toString()]; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Show Session Filters"] = function(){ return props["Show Session Filters"]; }
		this["set Show Session Filters"] = function(val){
			var bools = { "true": true, "false": false };
			if(bools.hasOwnProperty(val.toString())){
				props["Show Session Filters"] = bools[val.toString()]; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Data Update Interval"] = function(){ return props["Data Update Interval"]; }
		this["set Data Update Interval"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Data Update Interval"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get On Data Updated"] = function(){ return props["On Data Updated"]; }
		this["set On Data Updated"] = function(func){
			var getType = {};
			if(func === null || getType.toString.call(func) === '[object Function]'){
				props["On Data Updated"] = func; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			//clean updater
			var updaterInterval = $objEl.data("giDataUpdater");
			if(updaterInterval !== undefined){
				clearInterval(updaterInterval);
			}
			$objEl.remove();
		};
		
		var context = this;
		this.createCheckUpdateTimer = function(){
			
			var updaterInterval = $objEl.data("giDataUpdater");
			
			if(props["Auto Update Data"] && (updaterInterval === undefined || updaterInterval === null)){
				updaterInterval = setInterval(context.checkUpdate, 5000);
				$objEl.data("giDataUpdater", updaterInterval);
			}else if(!props["Auto Update Data"]){
				clearInterval(updaterInterval);
				$objEl.data("giDataUpdater", null);
			}
			

		};
		
		this.checkUpdate = function(){
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			var updateInterval = $objEl.data("giProperties")["Data Update Interval"];
			
			//check if no data or if data is outdated
			if(chartData === undefined || chartData === null ||
				((new Date() - new Date(chartData.timeGenerated * 1000)) / 1000 > updateInterval)){
				context.updateData();
			}
		};
		
		this.updateData = function(){
			var updateXhr = $objEl.data("giSessionData")["Update XHR"];
			
			if(toUpdateData && updateXhr !== undefined && updateXhr !== null){
				updateXhr.abort();
				updateXhr = null;
			}
			
			if(updateXhr === undefined || updateXhr === null || updateXhr.readyState === 4) {
				
				var newUpdateXhr = $.ajax({
					type: "POST",
					url: props["Data URL"] === ""? "/modules/gi_data_opt/gi-get-chart-data.php" : props["Data URL"],
					dataType: "json",
					data: {query: props["Query"]
					},
					success: function(result){
						$objEl.data("giSessionData")["Chart Data"] = result;
						
						if(props["On Data Updated"] !== null){		
							var dataTable = $objEl.data("giSessionData")["Chart Data"]["dataTable"][0];

							var query1 = props["Data URL"] == "" ? props["Query"][0] : $objEl.data("giSessionData")["Chart Data"]["giqs"][0];
							
							//get categories
							var cats = [];
							for(var j=0; j<query1["groupings"].length; j++){
								cats.push(query1["groupings"][j]["alias"]);
							}
							for(var j=0; j<query1["optionalGroupings"].length; j++){
								cats.push(query1["optionalGroupings"][j]["alias"]);
							}
							
							//get row where all categories are null (grand total)
							var grandTotalRow = null;
							for(var i=(dataTable.length-1); i>=0; i--){
								var row = dataTable[i];
								var allNull = true;
								for(var j=0; j<cats.length; j++){
									if(row[cats[j]] !== null){
										allNull = false;
										break;
									}
								}
								if(allNull){
									grandTotalRow = row;
									break;
								}
							}
					
							props["On Data Updated"]({ "Grand Total": grandTotalRow });
						}
						
						$objEl[props["Identifier Name"]]("render");
					}
				});	
				
				$objEl.data("giSessionData")["Update XHR"] = newUpdateXhr;
			}
			
			
		};
		
		this.render = function(){
		
			//var $scrollParent = $objEl.scrollParent();
			//var scrollParentScrollTop = $scrollParent.scrollTop();
		
			var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css({ 
				"display": (props["Width"] == "expand" ? "block": "inline-block"),
				"width": (props["Width"] == "expand" ? "auto": props["Width"]),
				"height": props["Height"],
				"min-height": (props["Height"] === "auto" ? 20: ""),
				"position": (props["Positioning"] === "auto" ? "relative": "absolute"),
				"top": (props["Positioning"] == "auto" ? "auto": props["Top"]),
				"left": (props["Positioning"] == "auto" ? "auto": props["Left"]),
				"z-index": props["Z-index"]});
				
			if(props["Positioning"] === "auto"){ $objEl.css("margin", props["Margin"]); }
			else{ $objEl.css("margin", 0); }
			
			var $border = $objEl.children("div.border");
			if($border.size() === 0){
				$border = $("<div>");
				$border.appendTo($objEl);
			}
			
			$border.html("");
			$border.removeAttr("style");
			$border.addClass("border");
			$border.css("border-style", "solid");
			$border.css("background-color", props["Background Color"]);
			$border.css("border-width", props["Border Thickness"]);
			$border.css("border-color", props["Border Color"]);
			$border.css("padding", props["Padding"]);
			$border.css("border-radius", props["Corner Radius"]);
			var outerThickness = props["Border Thickness"] + props["Padding"];
			if(props["Height"] !== "auto"){
				$border.css({  "height": $objEl.height() - outerThickness * 2 });
			}
			
			var $toolBar = $border.children(".toolBar"); 
			if($toolBar.size() == 0){
				$toolBar = $("<div>");
				$toolBar.addClass("toolBar giNoPrint");
				$border.append($toolBar);
			}
			$toolBar.html("");
			$toolBar.css("margin-bottom", "8px");

			//console.log("rendering...");
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			
			if(chartData !== undefined && chartData !== null && chartData["dataTable"][0].length > 0){
				//console.log("has chart data");
				//get slicers info 
				var query1 = props["Data URL"] == "" ? props["Query"][0] : chartData["giqs"][0];
					
				var cats = 
					$.Enumerable
					.From(query1["groupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var optCats = 
					$.Enumerable
					.From(query1["optionalGroupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var drilledFields = $objEl.data("giSessionData")["Data Drill Fields"][0];
				
				var allDrilledFields = cats.concat(drilledFields);
								
				var newDrillFields = 
					$.Enumerable
					.From(optCats)
					.Where(function(i){ return allDrilledFields.indexOf(i) === -1; })
					.ToArray();
				
				var sliceFields = $objEl.data("giSessionData")["Data Slice Fields"][0];
				
				
				//prepare session filters
				var sessFilters = $objEl.data("giSessionData")["Filters"][0];
				var sessFilterConditions = "";
				for(var sfi=0, l=sessFilters.length; sfi<l; sfi++){
					
					var sessFilter = sessFilters[sfi];
					var val = "";
					
					if(sessFilter["dataType"] == "Numeric"){
						val = sessFilter["value"];
					}else if(sessFilter["dataType"] == "Text"){
						val = "'" + sessFilter["value"] + "'";
					}else if(sessFilter["dataType"] == "Date"){
						val = "new Date('" + sessFilter["value"] + "')";
					}
					
					var condition = "(row['"+ sessFilter["field"] +"']"+ 
					sessFilter["comparator"] +
					val +
					"||row['"+ sessFilter["field"] +"']==null)";
					
					if(sfi<l-1){
						condition += sessFilter["logicalRelation"];
					}
					sessFilterConditions += condition;
				}
				
				var sessFilterFunc = null;
				if(sessFilterConditions !== ""){
					sessFilterConditions = "(function(row){ return (" + sessFilterConditions + "); })";
					sessFilterFunc = eval(sessFilterConditions);
				}
				
				
				//display navigation buttons
				if(drilledFields.length > 0){
					var $rollUp = $("<button>");
					$rollUp.addClass("rollUpButton, button");
					$rollUp.text("Back");
					$toolBar.append($rollUp);
					$rollUp.on("click", function(e){
						drilledFields.pop();
						allDrilledFields.pop();
						if(allDrilledFields.length > 0){
							//console.log(allDrilledFields[allDrilledFields.length-1]);
							var removeSlice = allDrilledFields[allDrilledFields.length-1];
							for(var dsi=0; dsi<sliceFields.length; dsi++){
								//console.log(sliceFields[dsi].Field);
								if(sliceFields[dsi].Field === removeSlice){
									sliceFields.splice(dsi, 1);
								}
							}
						}
						$objEl[$objEl.data("giIdentifierName")]("render");
					});

				}
				
				
				if(allDrilledFields.length == 0 && newDrillFields.length > 0){
					var $drillDown = $("<button>");
					$drillDown.addClass("drillDownButton, button");
					$drillDown.text("Drill-down");
					$toolBar.append($drillDown);
					$drillDown.on("click", function(e){
						var dataDrillFields = $objEl.data("giSessionData")["Data Drill Fields"][0];
						dataDrillFields.push(newDrillFields[0]);
						$objEl[$objEl.data("giIdentifierName")]("render");
					});
				}
				
				//additional filters
				if(props["Show Session Filters"]){
					var $filter = $("<button><span class='fa fa-filter'></span>");
					$filter.attr("title", "Filter data.");
					$filter.addClass("button");
					$toolBar.append($filter);
					$filter.on("click", function(e){ 
						var $filterDialog = giChartComponents.generateFilterDialog
							({ objEl: $objEl, 
							filters: $objEl.data("giSessionData")["Filters"][0], 
							fields: chartData["fieldsInfo"][0] });
						$filterDialog.dialog("open");
					});
				}
				
				//manual check of updated data
				if(!props["Auto Update Data"]){
					var $checkUpdate = $("<button><span class='fa fa-refresh'></span>");
					$checkUpdate.attr("title", "Check for updated data.");
					$checkUpdate.addClass("button");
					$toolBar.append($checkUpdate);
					$checkUpdate.on("click", function(e){
						context.updateData();
					});
				}
				
				//fill out form button
				if(props["New Record Button"] === "show" && query1["entities"].length > 0){
					var chartForms = [];
					for(var qei=0; qei<query1["entities"].length; qei++){
						var entity = query1["entities"][qei];
						//console.log(entity.formName + " " + entity.formId);
						if(entity["formId"] !== undefined && !isNaN(entity["formId"])){
							chartForms.push({ "name": entity.formName, "id":entity.formId});
						}
					}
					
					var $fillOutFormBtn = giChartComponents.generateFillOutFormButton({
						"chartForms": [chartForms]
					});
				
					$toolBar.append($fillOutFormBtn);
				}
				
				
				//get data table
				var dataTable = [];
				var origDataTable = chartData["dataTable"][0];

				var startExecTime = Date.now();
				for(var odti=0; odti<origDataTable.length; odti++){
					var row = origDataTable[odti];
					//should pass all slice
					var slicePassed = true;
					for(var si=0; si<sliceFields.length; si++){
						var sliceField = sliceFields[si];
						if(sliceField.Values.indexOf(row[sliceField.Field]) == -1){
							slicePassed = false;
						}
					}
					
					//all drilled fields should not be null
					var allDrillFieldsNotNull = true;
					for(var di=0; di<allDrilledFields.length; di++){
						var drilledField = allDrilledFields[di];
						if(row[drilledField] === null){
							allDrillFieldsNotNull = false;
						}
					}
					
					//all non drilled fields should be null
					var allNonDrillFieldsNull = true;
					for(var ndi=0; ndi<newDrillFields.length; ndi++){
						var newDrillField = newDrillFields[ndi];
						if(row[newDrillField] !== null){
							allNonDrillFieldsNull = false;
						}
					}
					
					var sessFilterPassed = true;
					if(sessFilterFunc !== null){
						var sessFilterPassed = sessFilterFunc(row);
					}
					
					if(slicePassed && allDrillFieldsNotNull && allNonDrillFieldsNull && sessFilterPassed){
						dataTable.push(row);
					}
				}
				
				if(dataTable.length == 0){
					return;
				}
				
				//display graph
				var allDrilledFieldsCount = allDrilledFields.length;
				var newDrillFieldsCount = newDrillFields.length;
				
				var $dividerTable = $border.find(".dividerTable");
				if($dividerTable.size() === 0){
					$dividerTable = $("<table>");
					$border.append($dividerTable);
				}				
				$dividerTable.addClass("dividerTable");
				$dividerTable.removeAttr("style");
				$dividerTable.css({ "width": "100%" });
				$dividerTable.html("");
				
				var $chartSection = null;
				var $legendsSection = null;
				if(["top", "bottom"].indexOf(props["Legends Position"]) > -1){
					var $row1 = $("<tr>"); 
					$dividerTable.append($row1);
					
					var $cell1 = $("<td>"); 
					$row1.append($cell1); 
					$cell1.addClass(props["Legends Position"] === "top" ? 
						"legendsSection": "chartSection");
					
					var $row2 = $("<tr>"); 
					$dividerTable.append($row2);
					
					var $cell2 = $("<td>"); 
					$row2.append($cell2); 
					
					$cell2.addClass(props["Legends Position"] === "top" ? 
						"chartSection": "legendsSection");
					
					$legendsSection = props["Legends Position"] === "top" ? $cell1 : $cell2;
					$chartSection = props["Legends Position"] === "top" ? $cell2 : $cell1;
					
				}else if(["left", "right"].indexOf(props["Legends Position"]) > -1){
					var $row = $("<tr>");
					$dividerTable.append($row);
					
					var $cell1 = $("<td>");
					$cell1.addClass(props["Legends Position"] === "left" ? 
						"legendsSection": "chartSection");
					$row.append($cell1);
					
					var $cell2 = $("<td>");
					$cell2.addClass(props["Legends Position"] === "left" ? 
						"chartSection": "legendsSection");
					$row.append($cell2);
					
					$legendsSection = props["Legends Position"] === "left" ? $cell1 : $cell2;
					$chartSection = props["Legends Position"] === "left" ? $cell2 : $cell1;
				}
				
				$legendsSection.css({ "vertical-align": "middle" });
				
				//get legends
				var $legends = $legendsSection.find(".legends");
				if($legends.size() === 0){
					$legends = $("<div>");
					$legendsSection.append($legends);
				}
				$legends.removeAttr("style");
				$legends.addClass("legends");
				$legends.css({ "line-height": "1.2em" });
				$legends.html("");
				
				//display legends
				var colorGen = new GiChartColors();
				var legendColors = [];
				var legendWidth = 0;
				for(var ai=0; ai<query1["aggregates"].length; ai++){
					var aggAlias = query1["aggregates"][ai]["alias"];
					var $legend = $("<div>");
					$legend.addClass("legend");
					$legend.css({ "display": "inline-block", "margin-right": 8 });
					
					legendColors.push(colorGen.next());
					var $legendColor = $("<span>");
					$legendColor.css({ "display": "inline-block", 
						"width": "1em", "height": "1em",
						"background-color": "#" + legendColors[ai],
						"margin-right": 2
					});
					$legend.append($legendColor);
					$legend.append(aggAlias);
						
					$legends.append($legend);
					legendWidth = legendWidth < $legend.outerWidth(true) ? $legend.outerWidth(true) : legendWidth;
				}
				
				$legends.children(".legend").css("width", legendWidth);
				
				//adjust sections' dimension
				if(["top", "bottom"].indexOf(props["Legends Position"]) > -1){
					$chartSection.append("<span class='cellFiller'>");
					$legendsSection.css("height", 1);
					$legendsSection.css("height", $legendsSection.height()); //get/set the computed
										
					if(props["Height"] === "auto"){
						$chartSection.css("height", "200");
					}else{
						var objElRect = $objEl[0].getBoundingClientRect();
						var dividerTableRect = $dividerTable[0].getBoundingClientRect();
						var dividerTableHeight = props["Height"] - (dividerTableRect.top - objElRect.top) - props["Padding"];
						$dividerTable.css("height", dividerTableHeight);
					}
				}else if(["left", "right"].indexOf(props["Legends Position"]) > -1){
					$chartSection.append("<span class='cellFiller'>");
					$chartSection.append("&nbsp;");
					$legendsSection.css("width", 1);
					$legendsSection.css("width", $legendsSection.width()); //get/set the computed
					
					if(props["Height"] === "auto"){
						$chartSection.css("height", "200");
					}else{
						var objElRect = $objEl[0].getBoundingClientRect();
						var dividerTableRect = $dividerTable[0].getBoundingClientRect();
						var dividerTableHeight = props["Height"] - (dividerTableRect.top - objElRect.top) - props["Padding"];
						var legendsHeight = $legends.outerHeight(true) + 4;
						$chartSection.css("height", dividerTableHeight);
						$legends.css("height", legendsHeight > dividerTableHeight ? dividerTableHeight : legendsHeight);
						$legends.css("overflow-y", "auto");
						$legends.css("overflow-x", "hidden");
					}
				}
				
				$chartSection.find(".cellFiller").remove();
				
				//add some space between sections
				if(props["Legends Position"] === "top"){
					$legendsSection.css("padding-bottom", 8);
				}else if(props["Legends Position"] === "bottom"){
					$legendsSection.css("padding-top", 8);
				}else if(props["Legends Position"] === "left"){
					$legendsSection.css("padding-right", 8);
				}else if(props["Legends Position"] === "right"){
					$legendsSection.css("padding-left", 8);
				}
				
				//sections for values and categories
				var $chartSubSections = $chartSection.children(".chartSubSections");
				
				if($chartSubSections.size() === 0){
					$chartSubSections = $("<table>");
					$chartSubSections.addClass("chartSubSections");
					$chartSubSections.appendTo($chartSection);
					
					var $row1 = $("<tr>");
					$row1.appendTo($chartSubSections);
					
					var $row2 = $("<tr>");
					$row2.appendTo($chartSubSections);
					
					var $vals = $("<td>");
					
					$vals.addClass("categoriesSection");
					$vals.appendTo($row1);
					//$vals.css("background", "blue");
					
					var $grid = $("<td>");
					$grid.addClass("gridSection");
					$grid.appendTo($row1);
					//$grid.css("background", "yellow");
					
					var $cats = $("<td>")
					$cats.addClass("valuesSection");
					$cats.attr("colspan", "2");
					$cats.appendTo($row2);
					//$cats.css("background", "orange");
				}
				
				$chartSubSections.css({ width: $chartSection.width(), height: $chartSection.height() });
				
				//prepare values
				var maxAggVal = 0;
				var minAggVal = 0;
				for(var ri=0; ri<dataTable.length; ri++){
					var row = dataTable[ri];
					for(var ai=0; ai<query1["aggregates"].length; ai++){
						var aggAlias = query1["aggregates"][ai]["alias"];
						if(maxAggVal < Number(row[aggAlias])){
							maxAggVal = Number(row[aggAlias]);
						}
						if(minAggVal > Number(row[aggAlias])){
							minAggVal = Number(row[aggAlias]);
						}
					}
				}

				//Rowi
				//set Minimum value of max aggregate if value = 0 to prevent infinite loop.
				maxAggVal = (maxAggVal==0?5:maxAggVal);
				//End
				
				var maxAbsAggVal = maxAggVal > Math.abs(minAggVal) ? maxAggVal : Math.abs(minAggVal);
				
				var linesInterval = (function(maxAbsAggVal){  
					var placeVal = Math.getPlaceValue(maxAbsAggVal);
					maxAbsAggVal = Math.roundOff(maxAbsAggVal, placeVal);
					var str = maxAbsAggVal.toString().replace(".", "");
					var firstNonZeroDigit = null;
					for(var i=0; i<str.length; i++){
						if(str[i] !== "0"){
							firstNonZeroDigit = parseInt(str[i]);
							break;
						}
					}
					
					var divisor = null;
					if(firstNonZeroDigit === 1){
						divisor = 0.2;
					}else if(firstNonZeroDigit === 2 || firstNonZeroDigit === 3){
						divisor = 0.5;
					}else{
						divisor = 1;
					}
					
					return divisor * Math.getPlaceValue(maxAbsAggVal);
				})(maxAbsAggVal);
				
				var lineValues = [0];
				var lineVal = 0;
				while(lineVal > minAggVal){
					lineVal -= linesInterval;
					lineValues.push(lineVal);
				}
				
				lineVal = 0;
				lineValues.reverse();
				while(lineVal <= maxAggVal){
					lineVal += linesInterval;
					lineValues.push(lineVal);
				}
				
				
				//display categories
				
				var $categoriesSection = $chartSubSections.find(".categoriesSection");
				$categoriesSection.html("");
				
				var $categoriesTable = $("<table>");
				$categoriesTable.appendTo($categoriesSection);
				
				var previousCatCells = [];
				
				for(var ri=0; ri<dataTable.length; ri++){
					var $catRow = $("<tr>");
					$catRow.appendTo($categoriesTable);

					for(var ci=0; ci<allDrilledFields.length; ci++){
						var fieldName = allDrilledFields[ci];	
						
						var cat = dataTable[ri][fieldName];
						
						if(previousCatCells[ci] === undefined){
							previousCatCells.push({ cat: undefined, cell: undefined });
						}
						
						if(cat !== previousCatCells[ci]["cat"]){
							var $cell = $("<td>");
							$cell.append(cat === "" ? "&nbsp;" : cat);
							$cell.attr("rowspan", 1);
							$cell.appendTo($catRow);
							$cell.css({ "border-bottom": "solid 1px #C0C0C0",
								"border-top": "solid 1px #C0C0C0",
								"padding": 3,
								"min-height": "1em",
								"white-space": "nowrap" });
							
							previousCatCells[ci]["cat"] = cat;
							previousCatCells[ci]["cell"] = $cell;
						}else{
							var prevRowSpan =  parseInt(previousCatCells[ci]["cell"].attr("rowspan"));
							previousCatCells[ci]["cell"].attr("rowspan", prevRowSpan + 1);
						}
					}
				}
				
				$categoriesSection.css({ width: "1px" });
				
				var $gridSection = $chartSubSections.find(".gridSection");
				$gridSection.append("<span class='cellFiller'>");
				
				var gridSectionWidth = $gridSection.width();
				$gridSection.width(gridSectionWidth);
				$gridSection.css("position", "relative");
				
				/*
				var catWidthPerCol = $categoriesSection.width() / dataTable.length;
				$categoriesTable.find("td").each(function(i){
					$(this).css({
						width: parseInt($(this).attr("colspan")) * catWidthPerCol
					});
				});
				*/
				
				//display values 
				var $valuesSection = $chartSubSections.find(".valuesSection");
				$valuesSection.css({ "height": 1, "text-align": "right" });
				$valuesSection.html("");
				var $valuesTable = $("<table>");
				
				$valuesTable.appendTo($valuesSection);
				var $valRow = $("<tr>");
					$valRow.appendTo($valuesTable);
				
				var valCellWidth = gridSectionWidth / (lineValues.length - 1);
				var valMaxCellWidth = 0;
				for(var vi=0; vi<lineValues.length; vi++){
					var $valCell = $("<td>");
					$valCell.css({ "text-align": "right", "width": valCellWidth, "position": "relative" });
					$valCell.appendTo($valRow);
					
					var $val = $("<span>");
					$val.css({ "display":"inline-block", "position": "absolute", "right": "0.5em" });
					
					$val.text(numeral(lineValues[vi]).format('0.[0]a'));//lineValues[vi].toLocaleString()
					$val.appendTo($valCell);
					
					$valCell.append("&nbsp;");
				}
				
				$valuesTable.css({ "position": "relative", "top": 0, "left": $categoriesSection.outerWidth() - valCellWidth });
				
				var valCellPxPerEm = $valuesTable.height();
				var allValCellWidth = gridSectionWidth + valCellWidth;
				var valRotation = 0; //-45
				
				var rotateVals = true;
				var $vals = $valuesTable.find("span");
				do{
					var maxValRectWidth = 0;
					var maxValRectHeight = 0;
					//rotate 
					$vals.each(function(i){ 
						var widthEm = $(this).width() / valCellPxPerEm;
						$(this).css({
							"transform": "rotate(" + valRotation + "deg)",
							"transform-origin":  ($(this).width() - widthEm)  + "px 0.5em"
						});
						
						if($(this)[0].getBoundingClientRect().width > maxValRectWidth){
							maxValRectWidth = $(this)[0].getBoundingClientRect().width;
						}
						
						if($(this)[0].getBoundingClientRect().height > maxValRectHeight){
							maxValRectHeight = $(this)[0].getBoundingClientRect().height;
						}
					});
					
					valRotation -= 15;
					
					if(valRotation <= -90 || maxValRectWidth <= valCellWidth){
						$valuesTable.find("td").css("height", maxValRectHeight);
						rotateVals = false;
					}
				}while(rotateVals);
				
				$vals.css("top", 0);
				
				//adjust cats table
				$categoriesTable.css("height", $categoriesSection.height());
				
				
				//display grid
				//var $gridSection = $chartSubSections.find(".gridSection");
				
				var svgNs = "http://www.w3.org/2000/svg";
 				var gridSvg = document.createElementNS(svgNs, "svg");
				
				var gridHeight = $gridSection.height();
				var gridWidth = $gridSection.width();
				
				gridSvg.setAttribute
					("style", 
						"position: relative;" +
						"width:" + gridWidth + ";" + 
						"height:" + gridHeight);
				
				$gridSection.append(gridSvg);
				
				var lineGroup = document.createElementNS(svgNs, "g");	
				lineGroup.setAttribute("transform", "translate(0.5,0.5)");
				gridSvg.appendChild(lineGroup);
				
				var intervalLength = (gridWidth - 2) / (lineValues.length-1);
				
				for(var li=0; li<lineValues.length; li++){
					var x = Math.round(li * intervalLength);
					var linePath = document.createElementNS(svgNs, "path");

					linePath.setAttribute("d", "M " + x + " 0 L " + x + " " + gridWidth);
					linePath.setAttribute("fill", "none");
					linePath.setAttribute("stroke", "#C0C0C0");
					linePath.setAttribute("stroke-width", 1);
					linePath.setAttribute("opacity", 1);
					
					lineGroup.appendChild(linePath);
				}
			
				//display bars
				var lengthPerValUnit = gridWidth / (lineValues[lineValues.length-1] - lineValues[0]);	
				
				var catSectionPadding = 8;
				var maxBarThickness = 60;
				
				var aggsCount = query1["aggregates"].length;
				var catThickness = gridHeight / dataTable.length;
				
				var barThickness = maxBarThickness;
				
				
				if((maxBarThickness * aggsCount) + (catSectionPadding * 2) > catThickness){
					barThickness = (catThickness - (catSectionPadding * 2)) / aggsCount;
				}else{
					catSectionPadding = (catThickness - (barThickness * aggsCount)) / 2;
				}
					
				var allAggBarThickness = barThickness * aggsCount;	
				
				var zeroX = lengthPerValUnit * Math.abs(lineValues[0]);
				
				var currentBarY = 0;
				
				for(var ri=0; ri<dataTable.length; ri++){
					var row = dataTable[ri];
						
					currentBarY += catSectionPadding;
					
					for(var ai=0; ai<query1["aggregates"].length; ai++){
						
						var aggField = query1["aggregates"][ai]["alias"];
						var val = row[aggField];
						
						var length = Math.round(val * lengthPerValUnit);
						
						var barRect = document.createElementNS(svgNs, "rect");
						barRect.setAttribute("width", Math.abs(length));
						barRect.setAttribute("height", barThickness);
						barRect.setAttribute("y", currentBarY);
						barRect.setAttribute("x", zeroX);
						barRect.setAttribute("fill", "#" + legendColors[ai]);
						
						var barId = "gi" + ((new Date()).getTime()) + "" + parseInt(Math.random()*1000);
						
						//bar labels
						var $label = $("<span>");
						$label.html(numeral(val).format("0.[0]a"));
						$label.css({ "position": "absolute", 
							"pointer-events": "none", 
							"background": "#fff" });
						$gridSection.append($label);
						
						var labelLeft = zeroX + length + (length < 0 ? -($label.outerWidth() + 2) : 2 ); 
						
						$label.css({ top: currentBarY + (barThickness/2) - ($label.outerHeight()/2), left: labelLeft });
						
						//tooltip
						(function(barId, val, color, aggField){
							$(barRect).on("mouseenter", function(e){
								var $toolTip = $("<div>");
								$toolTip.attr("id", barId + "tt");
								$toolTip.appendTo("body");
								$toolTip.append("<span style='font-weight:bold'>" + aggField + "</span>: ");
								$toolTip.append(Number(val).toLocaleString());
								$toolTip.addClass("giChartToolTip");
								$toolTip.css
									({ "position": "absolute", 
									"padding": 8,
									"background" : "#fff",
									"border-color": "#"+color,
									"border-style": "solid",
									"border-width": 1,
									"border-radius": 4,
									"z-index": 9999999 });
								$toolTip.css
									({ "top": e.clientY+$("body").scrollTop()-$toolTip.outerHeight()-8, 
									"left": e.clientX-($toolTip.outerWidth()/2) });
							});
							
							$(barRect).on("mousemove", function(e){
								var $toolTip = $("#" + barId + "tt");
								$toolTip.css
									({ "top": e.clientY+$("body").scrollTop()-$toolTip.outerHeight()-8, 
									"left": e.clientX-($toolTip.outerWidth()/2) });
							});
							
							$(barRect).on("mouseleave", function(e){
								var $toolTip = $("#" + barId + "tt");
								$toolTip.remove();
							});
							
							$(barRect).on("remove", function(e){
								var $toolTip = $("#" + barId + "tt");
								$toolTip.remove();
							});
						})(barId, val, legendColors[ai], aggField);
						
						
						if(newDrillFieldsCount > 0 && 
							allDrilledFieldsCount > 0){
									
							(function(colName, colValue, newDrillField){
								$(barRect).on("click", function(e){
									
									$objEl.data("giSessionData")["Data Slice Fields"][0].push({ Field: colName, Values: [colValue] });
									
									$objEl.data("giSessionData")["Data Drill Fields"][0].push(newDrillField);
									
									$objEl[$objEl.data("giIdentifierName")]("render");
								});
							})(allDrilledFields[allDrilledFields.length-1], 
								row[allDrilledFields[allDrilledFields.length-1]], 
								newDrillFields[0]);
						}
						
						gridSvg.appendChild(barRect);
						
						currentBarY += barThickness;
						
					}
					
					
					
					currentBarY += catSectionPadding;
					
				}
				
				//adjust values, grid and categories section
				/*
				var $valuesCells = $valuesTable.find("td");
				$valuesCells.css("height", Math.floor(intervalLength));
				$valuesCells.last().css("height", 1);
				
				$valuesTable.find("td span").css({ "position": "relative", "top": "-0.5em" });
				*/
				

			}
			
			//$scrollParent.scrollTop(scrollParentScrollTop);
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined && methodName !== "render"){
		methodResult = objCore[methodName](params);
	}
	
	if(toUpdateData){
		objCore.updateData();
	}
	
	if(toCheckUpdate){
		objCore.checkUpdate();
	}

	if(createCheckUpdateTimer){
		objCore.createCheckUpdateTimer();
	}
	
	if(!cancelRender){
		objCore.render(); 
	}
	
	return { "methodResult": methodResult, "affected": affected };
};

})(jQuery);