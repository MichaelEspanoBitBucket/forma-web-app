(function ($) {

	$.fn.giChartTable = function (operation, key, value) {	
	
		//make the div a chart
		if(!$(this).hasClass("gi-table-chart")){
			$(this).addClass("gi-table-chart");
			$(this).data("dataSets", []);
			$(this).data("width", 100);
			$(this).data("height", 100);		
		}

		if(operation === "option"){
		
			//if getting property, else, set
			if(value === null){
				return $(this).data(key);
			}else{
				$(this).data(key, value);
			}
		}else if(typeof operation === "string"){
			//must be a function call, we'll try! ;)
			switch(operation){
				case "render": 
					$(this.text("Rendered: " + new Date().toTime()));
					break;
			}
		}else if(operation !== null){
			//could be an object that contains properties
			for(var k in operation){
				if($(this).get(0).hasOwnProperty("data-" + k)){
					$(this).data(k, operation[k]);
				}
			}
		}
	}
	
})(jQuery);
