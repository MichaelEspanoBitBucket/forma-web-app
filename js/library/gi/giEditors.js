(function($){


$.fn.giDraggable = function(methodName, params){

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var draggableProp = {
			appendTo:'#content',
			zIndex:100,
			helper:function(){
				var cloned = $(this).clone();
				cloned.find('.ui-fLine').animate({opacity: 0}, 200);
				cloned.find('.ui-resizable-handle').remove();
				//console.log(cloned)
				return cloned.css({width:$(this).width()}).animate({opacity: 0.8}, 900);;
			},
			
			start:function(event, ui){
				$(this).css({'opacity':'0'})
				$(ui.helper).css({'cursor':'move'})
				$objEl.giResize('destroy')
			//	$(ui.helper).clone().appendTo($('#content'))
				
			},
			stop:function(event, ui){
				$(this).css({'opacity':'1'})
				$objEl.giResize()

			},
			drag:function(event, ui){
			//	var doc = document.elementFromPoint(event.clientX, event.clientY);

				//console.log($(doc).closest('.giObject'))
				//console.log(document.elementFromPoint(event.clientX, event.clientY))
			}

		}
		
		/* Common methods */
		this.initializeElement = function(){			
			props = $objEl.data("giProperties");
		};
		
		this.isHelper = function(){
			if (props!=undefined) {
				return false;
			}else{
				return true
			}
		}
		
		this.destroy = function(){
			if ($objEl.hasClass('ui-draggable')){ //&& $objEl.parent()[0].id!=='content') {
				$objEl.draggable('destroy');	
			}
			return $objEl;
		}
		
		this.render = function(){
			if($('.ui-draggable-dragging').length == 0){
				this.destroy();
				if (methodName!='destroy') {
					if(props){
						if (props['Identifier Name'] != 'giPage' && props['Positioning'] == 'manual') {
							//if(props['Identifier Name'] == "giText"){
								draggableProp.cancel = ".text-content[contenteditable='true'], input, .ui-tab";
							//}
							$objEl.draggable(draggableProp);
						}
					}
					
				}
			}
		};
	})($(this));
	
	var methodResult = undefined;
	objCore.initializeElement();
	
	//If obj is not a helper
	if (!objCore.isHelper()) {
		if(methodName !== undefined){
			methodResult = objCore[methodName](params);
		}
		objCore.render();
	}
	
	return methodResult;
};

$.fn.giFocus = function(methodName, params){

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var state = true;
		var bordersTemplate = "<div class='ui-fLine hBorderTop'></div><div class='ui-fLine hBorderBottom'></div><div class='ui-fLine vBorderRight'></div><div class='ui-fLine vBorderLeft'></div>";
		var defaultProps = {}
		
		/* Common methods */
		this.initializeElement = function(){
			
			props = $objEl.data("giProperties");
		};
				
		this.state = function(val){
			if (val) {
				state = true;
			}else{
				state = false;
			}
			
		};

		this.render = function(){			
			if (state) {
				if (!$objEl.hasClass('focused')) {
						if($(".giObject input:focus").length > 0 ){
			            	$(".giObject input:focus").blur();
			        	}

					$objEl.addClass('focused');
					if (props['Identifier Name'] != 'giPage' && props['Identifier Name'] != 'giLineShape') {
						
						$objEl.prepend(bordersTemplate);
						$objEl.giResize();
						
					}

					if(props['Identifier Name'] == 'giLineShape'){
						$objEl.giResize();
					}	

				}
				
				$objEl.giPropertyWindow();
			}else{
				if ($objEl.hasClass('focused')) {
					$objEl.removeClass('focused');
					$objEl.giResize('destroy');
					$objEl.find('.hBorderTop').remove();
					$objEl.find('.hBorderBottom').remove();
					$objEl.find('.vBorderRight').remove();
					$objEl.find('.vBorderLeft').remove();

					if(props['Identifier Name'] == 'giTabContainer'){
						// $objEl.giTabEditor('destroy')
					}

				}
			}
		};
		
	})($(this));

	objCore.initializeElement();
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	objCore.render();
	
	
	
	
	return methodResult;
};

$.fn.giResize = function(methodName, params){
	var cancelRender = false;
	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var state = true;

		var defaultProps = {
				
				//ghost:true,
				helper:'resize-helper',
				minHeight:1,
				minWidth:1,
				handles: 'nw,ne,sw,se,n,e,s,w',
				}
			
		/* Common methods */
		this.initializeElement = function(){
			
			props = $objEl.data("giProperties");
		};
		
		this.handles = function(ui){
			if(ui.element!=undefined){
				if($(ui.element).hasClass("giLineShape")){

				}else{
					$(ui.element).find('.ui-resizable-w').css("left","-" + (ui.size.width) + "px")
					$(ui.element).find('.ui-resizable-e').css("right","-" + (ui.size.width) + "px")
					$(ui.element).find('.ui-resizable-s').css("bottom","-" + (ui.size.height) + "px")
					$(ui.element).find('.ui-resizable-n').css("top","-" + (ui.size.height) + "px")
				}
			
		
			}
			cancelRender = true;
		}
		
		this.destroy = function(){
			//console.log('resize destroyed')
			if ($objEl.hasClass('ui-resizable')) {
				$objEl.resizable('destroy');
			}else if($objEl.hasClass('giLineShape')){
				$('.ui-line-handle').remove();
			}
			cancelRender = true;
		}
		
		this.resizable = function(){
			var context = this;	
			defaultProps.resize = function(){
				//console.log('resize')
			}
			defaultProps.create = function(event, ui){
					var ui = {
						size:{
							width:$objEl.width(),
							height:$objEl.height()
						},
						element:$objEl
					}
				$objEl.find('.ui-resizable-handle').removeClass('ui-icon').removeClass('ui-icon-gripsmall-diagonal-se')
				context.handles(ui);
			}

			if (props.Positioning == 'auto') {
				defaultProps.stop = function(event, ui){
					var actionList = []
					if (ui.originalSize.width != ui.size.width) {
						actionList.push($(this)[$(this).data('giIdentifierName')]('setProperties',{"Width":ui.size.width}));
					}
					if (ui.originalSize.height != ui.size.height) {
						actionList.push($(this)[$(this).data('giIdentifierName')]('setProperties',{"Height":ui.size.height}));
					}
					giHistory.addEvent("Resized " + props["Object Type"],'modified', actionList);

					context.handles(ui)
					$objEl.giFocus();
				}				
			}
			
			if (props.Positioning == 'manual') {
				defaultProps.stop = function(event, ui){
					var parentOffset = $(ui.element).parent().offset();
					var actionList = [];
					var margin = 1;

					actionList.push($(this)[$(this).data('giIdentifierName')]('setProperties',{
						'Height':ui.size.height,
						'Width':ui.size.width,
						'Top' : ui.helper.position().top - (parentOffset.top + margin),
						'Left' :ui.helper.position().left - (parentOffset.left + margin)
					}));
					giHistory.addEvent("Resized " + props["Object Type"],'modified',actionList);

					context.handles(ui)
					$objEl.giFocus();
				}
			}
			
			if (props['Maintain Aspect Ratio']) {
				defaultProps.aspectRatio = true;
			}

			if (props['Identifier Name'] == 'giText') {
				defaultProps.handles = 'w,e';
			}
			
			if (props['Identifier Name'] != 'giPage' && !$objEl.hasClass('ui-resizable')) {
				$objEl.resizable(defaultProps)
				$objEl.children('.ui-resizable-handle').on('mousedown', function(){
					if( $(".giObject input:focus").length > 0 ){
			        	$(".giObject input:focus").blur();
			        }
				})
			}	
		}

		this.lineEditor = function(){

    		$("#content").append('<div class="ui-line-handle h-start" data-pair-id="'+props["UID"]+'"></div>'+
				'<div class="ui-line-handle h-end" data-pair-id="'+props["UID"]+'"></div>');

    		$("#content").find('.ui-line-handle.h-start').css({
    			left:props["Left"] + 20 ,
    			top:props["Top"] + 20
    		})

    		$("#content").find('.ui-line-handle.h-end').css({
    			left:props["Left 2"] + 20,
    			top:props["Top 2"] + 20
    		})

			$('.ui-line-handle').draggable({
		    start:function(){
		        $('.ui-line-handle').animate({opacity:0},300)
		    },
		    stop:function(){
		    	$('.ui-line-handle').animate({opacity:1},300);
		    },
			drag:function(event, ui){
		       	var pairID = $(ui.helper).data('pair-id');
		        var $pHandle;
		        
		        
		        if($(ui.helper).hasClass('h-start')){
		            $pHandle = $('.ui-line-handle.h-end[data-pair-id="'+pairID+'"]');
		        }else{
		        	$pHandle = $('.ui-line-handle.h-start[data-pair-id="'+pairID+'"]');
		        }
		  
		       $objEl[props["Identifier Name"]]('setProperties',{
		       		"Left" : ui.position.left - 20,
		       		"Top" : ui.position.top - 20,
		       		"Left 2" : $pHandle.position().left- 20,
		       		"Top 2" : $pHandle.position().top - 20
		       	})
		       
		    	}
			})
		}
				this.render = function(){	
					var context = this;	
					if(props['Identifier Name'] == "giLineShape"){
						context.lineEditor();
					}else{
						context.resizable();
					}
					
				};
	})($(this));
	
	objCore.initializeElement();
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	if(!cancelRender){
		objCore.render();
	}
	
	return methodResult;
};

$.fn.giContainer = function(methodName, params){

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var state = true;
		var allContainers = [];
		var defaultProps = {
			accept: ".ui-obj-item, .giObject",
			//activeClass:"available-containers",
			greedy:true,
			tolerance:'pointer',
			hoverClass:"active-container",
			activate:function(event){
				$('#workspace').data('isTopContainer',true);
				
			},
			drop:function(event, ui){
				
				//Fix for the drop to top most element.
				var droppable = $(this);
		    	ui.helper.css({pointerEvents: 'none'});
		        var onto = document.elementFromPoint(event.clientX, event.clientY);
		        ui.helper.css({pointerEvents: ''});
		        if(!droppable.is(onto) && droppable.has(onto).length === 0) {
		        	//console.log('here');
		            return;
		        }



					var $draggedEl = $(ui.draggable);
					var props = $draggedEl.data('giProperties')
					var identifierName = $draggedEl.data('giIdentifierName');
					var $parent = $(this);
					

					x = ui.helper.offset().left
					y = ui.helper.offset().top
					
					x-=$parent.offset().left;
					y-=$parent.offset().top;
					x-=1;
					y-=1;
					console.log({top:y, left:x})
					// if(props != undefined){
					// 	if(props["Margin"] != undefined){
					// 		x-=props["Margin"];
					// 		y-=props["Margin"];
					// 	}
						
					// }
					
					if ($(ui.draggable).hasClass('ui-obj-item')) {


						var $newObject = $('<div></div>')
						var createdObject = [];			
						var hIndex = droppable.children('.giObject:not(".giObjectWrapper")').getMaxZ()
						if($(ui.draggable).hasClass('shared-object')){
							var props = $(ui.draggable).data('giProperties');
							//console.log(props)
							ChangeAllUID(props)
							props['Container ID'] = this.id;
							props['Top'] = y;
							props['Left'] = x;
							if(identifierName == "giLineShape"){
								props['Top 2'] = y + 200;
								props['Left 2'] = x + 400;
							}
							props['Z-index'] = (hIndex == 0 || hIndex == '-Infinity'? 1 : ++hIndex);
							createdObject.push(
								$newObject[identifierName]('setProperties',props)
							);
						}else{
							var newProps = {
								'UID': "uid-" + identifierName + "-" + (new Date()).getTime(),
								'Container ID' : this.id,
								'Top': y,
								'Left' : x,
								'Z-index': (hIndex == 0 || hIndex == '-Infinity'? 1 : ++hIndex)
							}
							if(identifierName == "giLineShape"){
								newProps['Top 2'] = y + 200;
								newProps['Left 2'] = x + 200;
							}
							createdObject.push(
								$newObject[identifierName]('setProperties',newProps)
							);
						}
						
						$newObject.giContainer();
						
						$('.giObject.focused').each(function(){
							$(this).giFocus('state',false);
						})
						$newObject.giFocus();
						giHistory.addEvent('Added new ' + $newObject.data('giProperties')["Object Type"],'created',createdObject);
						//Existing Object.. from same or different container
					}else{
						//Same container
						var notToLog = false;
						var modifiedObjects = [];
						if (props['Container ID'] == this.id) {
							if(props['Positioning'] == 'manual'){
								var newProps = {Left: x, Top: y};
								if(identifierName == "giLineShape"){
									newProps['Top 2'] = y + (props['Top 2'] - props['Top']);
									newProps['Left 2'] = x + (props['Left 2'] - props['Left']);
								}
								console.log(newProps)
								modifiedObjects.push($draggedEl[identifierName]('setProperties',newProps));
							}else{
								notToLog = true
							}
							
							
						//Different Container
						}else{
							var newProps = {Left: x, Top: y, 'Container ID':this.id};
							if(identifierName == "giLineShape"){
								newProps['Top 2'] = y + (props['Top 2'] - props['Top']);
								newProps['Left 2'] = x + (props['Left 2'] - props['Left']);
							}
							modifiedObjects.push($draggedEl[identifierName]('setProperties',newProps));
							
						}
						if (!notToLog) {
							giHistory.addEvent('Moved ' + props['Object Type'],'modified',modifiedObjects);
						}
						$draggedEl.giFocus();
					}
				//}	
				
			}
			
		}
		
		/* Common methods */
		this.initializeElement = function(){

			//:not('.ui-droppablex')
			props = $objEl.data("giProperties");
			allContainers = $objEl.find('.giObjectContainer:not(".ui-droppable")');
			if (allContainers.length == 0) {
				if ($objEl.hasClass('giObjectContainer')) {
					allContainers.push($objEl);
				}
				
			}
		};
		
		
		
		this.makeDroppable = function($container){
			if (!$container.hasClass('ui-droppable')) {
				$container.droppable(defaultProps)
			}
			
		}
		//http://stackoverflow.com/questions/4092817/dealing-with-overlapping-jquery-sortable-lists
		this.makeSortable = function($container){
			if (!$container.hasClass('ui-sortable')) {
				$container.fixedSortable({
					connectWith:'.giObjectContainer',
					//placeholder:'sortable-placeholder'
					start:function(event, ui){
						$(ui.item).giResize('destroy')
					},
					stop:function(event, ui){
						$(ui.item).giResize()
					},
					helper:function(e, elem){
						var cloned = $(elem).clone();
					//	console.log(cloned)
						cloned.find('.ui-fLine').animate({opacity: 0}, 200);
						cloned.find('.ui-resizable-handle').remove();
						return cloned.css({width:$(elem).width(),resize:"none",overflow:"hidden"}).animate({opacity: 0.8}, 900);;;
					},
					//placeholder:'sortable-placeholder',
					//tolerance:'intersect',
					appendTo:'#content',
					cancel:".text-content[contenteditable='true'], input, .ui-tab",
					update:function(event, ui){
			
					}
				});
			}
		}
		
		this.destroy = function(){
			$.each(allContainers, function(index, container){
				var $container = $(container);
				$container.droppable( "destroy" );
			});
			
		}
		
		this.render = function(){
			var context = this;
			
			$.each(allContainers, function(index, container){
				var $container = $(container);
				context.makeDroppable($container);
				context.makeSortable($container);
			});
		}
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
		
	objCore.render();
	
	
	
	
	return methodResult;
};

$.fn.giContextMenu = function(methodName, params){

	var objCore = new (function($objEl){
		
		
		var $objEl = $objEl;
		var props = undefined;
		var state = true;
		var allContainers = [];
		var defaultProps = {}
		var event; 
		
		/* Common methods */
		this.initializeElement = function(){
			props = $objEl.data("giProperties");
		};
		
		this.setEvent = function($event){
			event = $event;
		}

		this.render = function(){
			var context = this;
			var items = defaultProps;
			
			if (props['Identifier Name']!='giPage') {
				
				if(props['Identifier Name'] == 'giTableChart' || props['Identifier Name'] == 'giPieChart' || 
					props['Identifier Name'] == 'giLineGraph' || props['Identifier Name'] == 'giAreaGraph' || 
					props['Identifier Name'] == 'giBarGraph' || props['Identifier Name'] == 'giColumnGraph' ||
					props['Identifier Name'] == 'giKpiTable' || props['Identifier Name'] == 'giNumericKpi' || props['Identifier Name'] == 'giGaugeChart'){

					items.Edit = {name:'Query', icon:"edit",callback:function(){
						$objEl.giQueryWindow("open");
					
					}}

					items.Type = {name:'Copy to', icon:"none","items":{
						"Bar": {name:"Bar", icon:"Bar", callback:function(){
							var actionList = []
							var idfName = props['Identifier Name'];
							var Props = $.extend(true, {}, props);
							var $newObj = $("<div></div>")

							ChangeAllUID(Props);
							actionList.push($newObj["giBarGraph"]('setProperties',Props));
							
							giHistory.addEvent(Props["Object Type"] + ' copied to Bar Graph','modified',actionList);
						}},
						"Line": {name:"Line", icon:"Line", callback:function(){
							var actionList = []
							var idfName = props['Identifier Name'];
							var Props = $.extend(true, {}, props);
							var $newObj = $("<div></div>")

							ChangeAllUID(Props);
							actionList.push($newObj["giLineGraph"]('setProperties',Props));
					
							giHistory.addEvent(Props["Object Type"] + ' copied to Line Graph','modified',actionList);
						}},
						"Column": {name:"Column", icon:"Column", callback:function(){
							var actionList = []
							var idfName = props['Identifier Name'];
							var Props = $.extend(true, {}, props);
							var $newObj = $("<div></div>")

							ChangeAllUID(Props);
							actionList.push($newObj["giColumnGraph"]('setProperties',Props));
							
							giHistory.addEvent(Props["Object Type"] + ' copied to Column Graph','modified',actionList);
						}},
						"Area": {name:"Area", icon:"Area", callback:function(){
							var actionList = []
							var idfName = props['Identifier Name'];
							var Props = $.extend(true, {}, props);
							var $newObj = $("<div></div>")

							ChangeAllUID(Props);
							actionList.push($newObj["giAreaGraph"]('setProperties',Props));
														
							giHistory.addEvent(Props["Object Type"] + ' copied to Area Graph','modified',actionList);
						}},
						"Pie": {name:"Pie", icon:"Pie", callback:function(){
							var actionList = []
							var idfName = props['Identifier Name'];
							var Props = $.extend(true, {}, props);
							var $newObj = $("<div></div>")

							ChangeAllUID(Props);
							actionList.push($newObj["giPieChart"]('setProperties',Props));
					
							
							giHistory.addEvent(Props["Object Type"] + ' copied to Pie Graph','modified',actionList);
						}},
						"Table": {name:"Table", icon:"Table", callback:function(){
							var actionList = []
							var idfName = props['Identifier Name'];
							var Props = $.extend(true, {}, props);
							var $newObj = $("<div></div>")

							ChangeAllUID(Props);
							actionList.push($newObj["giTableChart"]('setProperties',Props));
							
							giHistory.addEvent(Props["Object Type"] + ' copied to Table Chart','modified',actionList);
						}},
					}}

					items.sep4 = "-"
				}

				if(props['Identifier Name'] == 'giText'){

					items.Edit = {name:'Edit', icon:"edit",callback:function(){
						giEditors.giTextEditor($objEl);
					
					}}
					items.sep4 = "-"
				}


					if (props['Identifier Name']=='giLayoutTable') {
				
				items.AddColumn = {name:"Add column", icon:"edit", 
					callback:function(){
						$objEl.giLayoutTable("addColumn");
					}
				}
				items.AddRow = {name:"Add row", icon:"edit", 
					callback:function(){
						$objEl.giLayoutTable("addRow");	
					}
				}
				items.DeleteColumn = {name:"Delete column", icon:"delete", 
					callback:function(){
						//console.log($objEl.find(".focused-container:first").parent().index());
						var actionList = []
						actionList.push($objEl.giLayoutTable("deleteColumn",$objEl.find(".focused-container:first").parent().index()));
						giHistory.addEvent('Deleted column','modified',actionList);
					}
				}
				items.DeleteRow = {name:"Delete row", icon:"delete", 
					callback:function(){
						//console.log($objEl.find(".focused-container:first").parent().parent().index());
						var actionList = []
						actionList.push($objEl.giLayoutTable("deleteRow",$objEl.find(".focused-container:first").parent().parent().index()));	
						giHistory.addEvent('Deleted row','modified',actionList);
					}
				}


				items.sep1 = "-------------"
			}


				items.Cut = {name:'Cut', icon:"cut", callback:function(){
					giEditors.Cut($objEl)
				}}
				items.Copy = {name:'Copy', icon:"copy", callback:function(){
					giEditors.Copy($objEl)
				}}
			}
			

			items.Paste = {name:'Paste', disabled:(giEditors.ClipBoardData().length == 0), icon:"paste",callback:function(){
				giEditors.Paste(event)
			}}

				
			if (props['Identifier Name']!='giPage') {

				items.DeleteItem = {name:"Delete", icon:"delete", callback:function(){
					//$objEl.remove();
					var deletedItems = [];
					deletedItems.push($objEl[props['Identifier Name']]('delete'));
					$("#gi-content").giPropertyWindow();
					giHistory.addEvent('Deleted ' + props["Object Type"],'deleted',deletedItems);
				}}
				items.sep3 = "-------------"

				

				items.BringToFront = {name:'Bring to Front', icon:"bringtofront",callback:function(){
					giEditors.BringToFront($objEl);
				}}
				items.BringForward = {name:'Bring Forward', icon:"bringforward",callback:function(){
					giEditors.BringForward($objEl);
				}}
				items.SendToBack = {name:'Send to Back', icon:"sendtoback", callback:function(){
					giEditors.SendToBack($objEl);
				}}
				items.SendBackward = {name:'Send Backward', icon:"sendbackward",callback:function(){
					giEditors.SendBackward($objEl);
				}}
				
				/* TODO: [start] temporary, delete this */
				items.Positioning = {"name": "Positioning", icon:"none", "items": {
					"Auto": {name:"Auto", icon:"none", disabled:props["Positioning"] == "auto" , callback:function(){
						var actionList = []
						actionList.push($objEl[props["Identifier Name"]]("setProperties",{"Positioning":"auto"}));
					
						$(this).giFocus('state',false);
						$(this).giFocus();
						
						giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);

						
					}},
					"Manual": {name:"Manual", icon:"none", disabled:props["Positioning"] == "manual" , callback:function(){
						var actionList = []
						
						actionList.push($objEl[props["Identifier Name"]]("setProperties",{"Positioning":"manual"}));
					
						$(this).giFocus('state',false);
						$(this).giFocus();
						
						giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
					}}
				}}
			
				
			}

			items.AutoHeight = {name:"Auto height", icon:"none", callback:function(){
				var actionList = []
				actionList.push($objEl[props["Identifier Name"]]("setProperties",{"Height":"auto"}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				
			}}
			
			
			items.ExpandWidth = {name:"Expand width", icon:"none", disabled:(props["Positioning"] != "auto" && props["Identifier Name"] != "giPage"), callback:function(){
				var actionList = []
				actionList.push($objEl[props["Identifier Name"]]("setProperties",{"Width":"expand"}));
				
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			}}
			
			


			if (props['Identifier Name']=='giTabContainer') {
				items.sep5 = "-------------"
				items.AddTab = {name:"Add Tab", icon:"edit", 
					callback:function(){
						var actionList = []
					   	actionList.push($objEl.giTabContainer('addTab'));
					    giHistory.addEvent("Added Tab", 'modified', actionList)
						
						$objEl.giContainer();
						// $objEl.giFocus('state',false);
						// $objEl.giFocus();
					}}
				items.RemoveTab = {name:"Remove Tab", icon:"edit", 
				callback:function(){
					var actionList = []
				   	actionList.push($objEl.giTabContainer('deleteTab', $(this).parent().index()));
				    giHistory.addEvent("Deleted Tab", 'delete', actionList)
				}}
			}
			if (props['Identifier Name']!='giPage') {
				items.sep6 = "-------------"
				
				items.CopyToClipBoard = {name:"Copy to Shared Objects", icon:"copy", callback:function(){
					var actionList = [];
					addSharedObject(props);

				}}
			}

			return items;	
		}})($(this));
	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	methodResult = objCore.render();	
	return methodResult;
};


$.fn.giPropertyWindow = function(methodName, params){
	var cancelRender = false;
	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var state = true;
		var defaultProps = {
				
    	}
    

		var spectrumProps = {
			replacerClassName: 'ui-property-spectrum',
			containerClassName: 'ui-property-spectrum-container',
			preferredFormat: "hex",
			showInput: true,
			allowEmpty:true,
    	}
		var $propertyWindow;
		
		/* Common methods */
		this.initializeElement = function(){
			props = $objEl.data("giProperties");
			$propertyWindow = $(".gi-property-window");
		};
		
		this.CreatePorperty = function(name){
			var $editorTemp = $('<div class="ui-property">'+
				'<div class="ui-property-title" title="'+ name +'">'+ name +'</div>'+
				'<div class="ui-property-editor"></div>');
			return $editorTemp;
		}

		this["New Record Button Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Allow Create Record");
			$propEditor.find('.ui-property-editor').append('<select><option value="show">True</option><option value="hide">False</option></select>')
			this.appendToPropertyWindow('Chart Action',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor select').first().val(props["New Record Button"]);
			$propEditor.find('.ui-property-editor select').on('change',function(){
				var actionList = [];
				
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"New Record Button":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				$objEl.giFocus();
			})
		}

		this["Positioning Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Positioning");
			$propEditor.find('.ui-property-editor').append('<select><option value="auto">auto</option><option value="manual">manual</option></select>')
			this.appendToPropertyWindow('Position',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor select').first().val(props["Positioning"]);
			$propEditor.find('.ui-property-editor select').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Positioning":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				$objEl.giFocus();
			})
		}

		this["Super Aggregates Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Totals");
			$propEditor.find('.ui-property-editor').append('<select><option value="show">Show</option><option value="hide">Hide</option></select>')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor select').first().val(props["Super Aggregates"]);
			$propEditor.find('.ui-property-editor select').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Super Aggregates":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				$objEl.giFocus();
			})
		}



		this["Records Per Page Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Records Per Page");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="number" value="'+ props["Records Per Page"] +'">')
			this.appendToPropertyWindow('Data',$propEditor)
			//$propertyWindow.append($propEditor);

			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Records Per Page":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}

		this["Legends Position Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Legend Position");
			$propEditor.find('.ui-property-editor').append('<select><option value="left">left</option><option value="right">right</option><option value="top">top</option><option value="bottom">bottom</option></select>')
			this.appendToPropertyWindow('Chart Elements',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor select').first().val(props["Legends Position"]);
			$propEditor.find('.ui-property-editor select').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Legends Position":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				$objEl.giFocus();
			})
		}

		this["Border Thickness Editor"] = function(){
			var $propEditor = this.CreatePorperty("Border Thickness");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="number" value="'+ props["Border Thickness"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);

			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Border Thickness":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}

		this["Thickness Editor"] = function(){
			var $propEditor = this.CreatePorperty("Thickness");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="number" value="'+ props["Thickness"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);

			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Thickness":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}
		
		this["Border Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("Border Color");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" class="ui-colorpicker"  type="text" value="'+ props["Border Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.appendTo = $propertyWindow.parent(); 
			spectrumProps.change = function(color){    				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Border Color":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}

		this["Tab Border Thickness Editor"] = function(){
			var $propEditor = this.CreatePorperty("Tab Border Thickness");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="number" value="'+ props["Tab Border Thickness"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);

			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Tab Border Thickness":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}
		
		this["Tab Border Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("Tab Border Color");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" class="ui-colorpicker"  type="text" value="'+ props["Tab Border Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.appendTo = $propertyWindow.parent(); 
			spectrumProps.change = function(color){    				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Tab Border Color":(color==null?'transparent':color.toHexString())}));
				
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps);
		}

		this["Fill Editor"] = function(){
			var $propEditor = this.CreatePorperty("Fill");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["Fill"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);

			spectrumProps.change = function(color){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Fill":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);	
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}

		this["Z-index Editor"] = function(){
			var $propEditor = this.CreatePorperty("Z-index");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="number" value="'+ props["Z-index"] +'">')
			this.appendToPropertyWindow('Position',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				//actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Z-index":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',$objEl.setZindex($(this).val()));	
			})
		}

		this["Background Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("Background Color");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["Background Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.change = function(color){    				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Background Color":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);	
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}

		this["Corner Radius Editor"] = function(){
			var $propEditor = this.CreatePorperty("Corner Radius");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="number" value="'+ props["Corner Radius"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Corner Radius":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);	
			})
		}

		this["Width Editor"] = function(){
			var $propEditor = this.CreatePorperty("Width");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["Width"] +'">')
			this.appendToPropertyWindow('Size',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Width":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				//wrapFocusedObjects();
			})
		}

		this["Height Editor"] = function(){
			if(props['Identifier Name'] != 'giText'){
				var $propEditor = this.CreatePorperty("Height");
				$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["Height"] +'">')
				this.appendToPropertyWindow('Size',$propEditor)
			//$propertyWindow.append($propEditor);
				$propEditor.find('.ui-property-editor input').on('change',function(){
					var actionList = [];
					actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Height":$(this).val()}));
					giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
					//wrapFocusedObjects();
				})
			}
		}

		this["Top Editor"] = function(){
			var $propEditor = this.CreatePorperty("Top");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="number" '+ (props["Positioning"]=="auto"?"disabled":"") +' value="'+ props["Top"] +'">')
			this.appendToPropertyWindow('Position',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Top":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}

		this["Left Editor"] = function(){
			var $propEditor = this.CreatePorperty("Left");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="number" '+ (props["Positioning"]=="auto"?"disabled":"") +' value="'+ props["Left"] +'">')
			this.appendToPropertyWindow('Position',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
			
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Left":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}

		this["Padding Editor"] = function(){
			var $propEditor = this.CreatePorperty("Padding");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="number" value="'+ props["Padding"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Padding":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}

		this["Default Tab Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Default Active Tab");
			$propEditor.find('.ui-property-editor').append('<select></select>');
			var $selector = $propEditor.find('.ui-property-editor select');
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			
			$.each(props["Tabs"], function(index, val){
				$selector.append("<option value="+index+">"+ index + " - " +val.Title+" </option>");
			})
			$selector.first().val(props["Default Tab"]);
			$selector.on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Default Tab":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				$objEl.giFocus();
			})
		}

		this["Header Background Editor"] = function(){
			var $propEditor = this.CreatePorperty("Header Background");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="text" value="'+ props["Header Background"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.change = function(color){    				
				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Header Background":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}

		this["Header Text Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("Header Text Color");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="text" value="'+ props["Header Text Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.change = function(color){    				
				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Header Text Color":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}

		// this["Row Background Editor"] = function(){
		// 	var $propEditor = this.CreatePorperty("Row Background");
		// 	$propEditor.find('.ui-property-editor').append('<input class="form-control" type="text" value="'+ props["Row Background"] +'">')
		// 	$propertyWindow.append($propEditor);
		// 	spectrumProps.change = function(color){    				
				
		// 		var actionList = [];
		// 		actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Row Background":(color==null?'transparent':color.toHexString())}));
		// 		giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
  //   		}	
		// 	$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		// }

		// this["Row Alternating Background Editor"] = function(){
		// 	var $propEditor = this.CreatePorperty("Row Alternating Background");
		// 	$propEditor.find('.ui-property-editor').append('<input class="form-control" type="text" value="'+ props["Row Alternating Background"] +'">')
		// 	$propertyWindow.append($propEditor);
		// 	spectrumProps.change = function(color){    				
		// 		var actionList = [];
		// 		actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Row Alternating Background":(color==null?'transparent':color.toHexString())}));
		// 		giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
  //   		}	
		// 	$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		// }

		this["Query Editor"] = function(){
			var $propEditor = this.CreatePorperty("Query");
			$propEditor.find('.ui-property-editor').append('<input class="btn btn-default query-edit-button" value="Edit" type="button">');
			//$propertyWindow.append($propEditor);
			this.appendToPropertyWindow('Data',$propEditor)
			//this.appendToPropertyWindow('Appearance',$propEditor)
			$propEditor.find('.ui-property-editor input').on('click',function(){
				$objEl.giQueryWindow("open");
			})
		}
		
		// this["Tabs Editor"] = function(){
		// 	var $propEditor = this.CreatePorperty("Add Tab");
		// 	$propEditor.find('.ui-property-editor').append('<input class="btn xbtn-default query-edit-button" value="Add" type="button">');
		// 	$propertyWindow.append($propEditor);
		// 	$propEditor.find('.ui-property-editor input').on('click',function(){
		// 		$objEl.giTabContainer('addTab');
		// 		$objEl.giContainer();
		// 	})
		// }
		
		this["Name Editor"] = function(){
			if(props["Identifier Name"] == "giReport"
				|| props["Identifier Name"] == "giPieChart"
				|| props["Identifier Name"] == "giBarGraph"
				|| props["Identifier Name"] == "giColumnGraph"	
				|| props["Identifier Name"] == "giLineGraph"
				|| props["Identifier Name"] == "giAreaGraph"
				|| props["Identifier Name"] == "giTableChart"){
				var $propEditor = this.CreatePorperty("Name");
				$propEditor.find('.ui-property-editor').append('<input class="form-control property-report-name" type="text" value="'+ props["Name"] +'">')
				$propertyWindow.append($propEditor);
				$propEditor.find('.ui-property-editor input').on('change',function(){
					if(props["Identifier Name"] == "giReport"){
						$('#gi-content').data('giProperties').Name = $(this).val();
					$('#report_name').html($(this).val());
					}else{
						var actionList = [];
						actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Name":$(this).val()}));
						giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
					}
					
				})
				$propEditor.find('.ui-property-editor input').on('blur',function(){
					if(props["Identifier Name"] == "giReport"){
						$('#gi-content').data('giProperties').Name = $(this).val();
						$('#report_name').html($(this).val())
					}else{
						var actionList = [];
						actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Name":$(this).val()}));
						giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
					}
					
				})
			}
		}

		// this["Dashboard Published Editor"] = function(){
		// 	var context = this;
		// 	var $propEditor = this.CreatePorperty("Dashboard");

		// 	$propEditor.find('.ui-property-editor').append('<input class="form-control" type="checkbox" value="Publish"> <span>Publish</span>')
		// 	$propertyWindow.append($propEditor);
		// 	var $input = $propEditor.find('.ui-property-editor input');
		// 	$input.prop('checked',props["Dashboard Published"] == 1 ? true : false)
		// 	$input.on('change',function(){
		// 		$('#gi-content').data('giProperties')["Dashboard Published"] = $input.prop('checked') == true ? 1 : 0;
		// 	})
		// }

		this["Dashboard Published Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Published");

			$propEditor.find('.ui-property-editor').append('<select><option value=1>True</option><option value=0>False</option></select>')
			var $selector = $propEditor.find('.ui-property-editor select');
			$propertyWindow.append($propEditor);
			$selector.val(props["Dashboard Published"]);
			//$input.prop('checked',props["Dashboard Published"] == 1 ? true : false)
			$selector.on('change',function(){
				$('#gi-content').data('giProperties')["Dashboard Published"] = $(this).val();
			})
		}

		this["Data Update Interval Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Update Interval");
			$propEditor.find('.ui-property-editor').append('<select></select>');
			var $selector = $propEditor.find('.ui-property-editor select');
			this.appendToPropertyWindow('Data',$propEditor)
			//$propertyWindow.append($propEditor);

			// $selector.append("<option value=10>10 Seconds</option>");
			// $selector.append("<option value=30>30 Seconds</option>");
			$selector.append("<option value=5>5 Seconds</option>" + 
				"<option value=30>30 Seconds</option>" + 
				"<option value=300>5 Minutes</option>" + 
				"<option value=900>15 Minutes</option>" + 
				"<option value=1800>30 Minutes</option>" + 
				"<option value=3600>1 Hour</option>" + 
				"<option value=86400>1 Day</option>" + 
				"<option value=604800>1 Week</option>" + 
				"<option value=2592000>1 Month</option>" + 
				"<option value=31104000>1 Year</option>");


			$selector.val(props["Data Update Interval"]);
			$selector.on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Data Update Interval":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}
		// Assign to Dashboard
		this["Dashboard Department IDs Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Dashboard");
			$propEditor.find('.ui-property-editor').append('<input  type="button" class="btn btn-default query-edit-button" value="Assign"/>');
			var $editor = $propEditor.find('.ui-property-editor');
			$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('click',function(){
				$("#gi-content").giDashboard("open");
			});
		}

		// Dashboard Preview
		// this["Dashboard Position IDs Editor"] = function(){
		// 	if(props['ID']!=undefined){
		// 		var context = this;
		// 		var $propEditor = this.CreatePorperty("Dashboard");
		// 		$propEditor.find('.ui-property-editor').append('<input  type="button" class="btn btn-default query-edit-button" value="Preview"/>');
		// 		var $editor = $propEditor.find('.ui-property-editor');
		// 		$propertyWindow.append($propEditor);
		// 		$propEditor.find('.ui-property-editor input').on('click',function(){
		// 			window.open("/Insights/gi-dashboard-viewer?id=" + props["ID"]);
		// 		});
		// 	}
		// }

		this["URL Editor"] = function(){
			var $propEditor = this.CreatePorperty("URL");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="text" value="'+ props["URL"] +'">')
			this.appendToPropertyWindow('Data',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"URL":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}

		this["Data URL Editor"] = function(){
			if($("#dev_mode").val() == 1){
				var $propEditor = this.CreatePorperty("Data URL");
				$propEditor.find('.ui-property-editor').append('<input class="form-control" type="text" value="'+ props["Data URL"] +'">')
				this.appendToPropertyWindow('Data',$propEditor)
				//$propertyWindow.append($propEditor);
				$propEditor.find('.ui-property-editor input').on('change',function(){
					var actionList = [];
					actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Data URL":$(this).val()}));
					giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				})
			}
		}

		this["Auto Update Data Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Auto Update Data");
			$propEditor.find('.ui-property-editor').append('<select><option value=true>True</option><option value=false>False</option></select>')
			this.appendToPropertyWindow('Data',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor select').first().val(props["Auto Update Data"]?"true":"false");
			$propEditor.find('.ui-property-editor select').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Auto Update Data":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				$objEl.giFocus();
			})
		}

		this["Allow Export Editor"] = function(){
			var context = this;
			var $propEditor = this.CreatePorperty("Allow Export");
			$propEditor.find('.ui-property-editor').append('<select><option value=true>True</option><option value=false>False</option></select>')
			this.appendToPropertyWindow('Chart Action',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor select').first().val(props["Allow Export"]?"true":"false");
			$propEditor.find('.ui-property-editor select').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Allow Export":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
				$objEl.giFocus();
			})
		}

		this["Margin Editor"] = function(){
			var $propEditor = this.CreatePorperty("Outer Margin");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="number" value="'+ props["Margin"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Margin":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}

		
		this["Target Value Editor"] = function(){
			var $propEditor = this.CreatePorperty("Target Value");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="number" value="'+ props["Target Value"] +'">')
			this.appendToPropertyWindow('Data',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Target Value":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}
		
		this["Low Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("Low Color");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["Low Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.change = function(color){    				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Low Color":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);	
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}
		
		this["Medium Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("Medium Color");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["Medium Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.change = function(color){    				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Medium Color":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);	
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}
		
		this["High Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("High Color");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["High Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.change = function(color){    				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"High Color":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);	
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}
		
		this["Decreased Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("Decreased Color");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["Decreased Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.change = function(color){    				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Decreased Color":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);	
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}
		
		this["Not Changed Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("Not Changed Color");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["Not Changed Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.change = function(color){    				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Not Changed Color":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);	
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}
		
		this["Increased Color Editor"] = function(){
			var $propEditor = this.CreatePorperty("Increased Color");
			$propEditor.find('.ui-property-editor').append('<input  class="form-control" type="text" value="'+ props["Increased Color"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			spectrumProps.change = function(color){    				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Increased Color":(color==null?'transparent':color.toHexString())}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);	
    		}	
			$propEditor.find('.ui-property-editor input').spectrum(spectrumProps)
		}
		
		this["Low Threshold Percentage Editor"] = function(){
			var $propEditor = this.CreatePorperty("Low Threshold Percentage");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="number" value="'+ props["Low Threshold Percentage"] +'">')
			this.appendToPropertyWindow('Data',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Low Threshold Percentage":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}
		
		this["High Threshold Percentage Editor"] = function(){
			var $propEditor = this.CreatePorperty("High Threshold Percentage");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="number" value="'+ props["High Threshold Percentage"] +'">')
			this.appendToPropertyWindow('Data',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"High Threshold Percentage":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}
		
		this["Font Size Editor"] = function(){
			var $propEditor = this.CreatePorperty("Font Size");
			$propEditor.find('.ui-property-editor').append('<input class="form-control" type="number" value="'+ props["Font Size"] +'">')
			this.appendToPropertyWindow('Appearance',$propEditor)
			//$propertyWindow.append($propEditor);
			$propEditor.find('.ui-property-editor input').on('change',function(){
				
				var actionList = [];
				actionList.push($objEl[props["Identifier Name"]]('setProperties', {"Font Size":$(this).val()}));
				giHistory.addEvent('Modified ' + props["Object Type"],'modified',actionList);
			})
		}
		
		this.appendToPropertyWindow = function(category, $item){
			var $categoryHead = $propertyWindow.find('.ui-obj-category[data-category="' + category + '"]');
			var $categoryContent = $propertyWindow.find('.ui-obj-list[data-category="' + category + '"]');
			
			if($categoryHead.size()==0){
				$categoryHead = $('<div class="ui-obj-category" data-category="' + category + '">' + category + '<span class="pull-right" fl-show-opt="true"><i class="fa fa-caret-down"></i></span></div>').appendTo($propertyWindow);
				$categoryContent = $('<div class="ui-obj-list" data-category="' + category + '"></div>')
				

				$categoryHead.appendTo($propertyWindow)
				$categoryContent.appendTo($propertyWindow)
			}

			$categoryContent.append($item);
		}
		
		this.render = function(){
			var context = this;
			
			$propertyWindow.find('.ui-property').remove();
			$propertyWindow.find('.ui-obj-category, .ui-obj-list').remove();
			$propertyWindow.parent().find('.sp-container').remove();
			$propertyWindow.siblings('.panel-head').children('.panel-title').html((props["Object Type"]==undefined? "Object" : props["Object Type"]) + ' Properties');
		
	
			$.each(props, function(propName, propValue){
				if(typeof(context[propName + " Editor"]) == "function"){
					context[propName + " Editor"]();
				}
				
			})

			$('.gi-property-window.gi-scrollable').perfectScrollbar("destroy")
			$('.gi-property-window.gi-scrollable').perfectScrollbar({
				wheelSpeed: 1,
				wheelPropagation: true,
				suppressScrollX: false,
				includePadding:true
			});

			
			if(props["UID"] != $propertyWindow.data("obj-id")){
				// $propertyWindow.animate({
		  //           scrollTop: 0
		  //       }, 400);
				$propertyWindow.scrollTop(0);
			}
			// console.log(props["UID"])
			$propertyWindow.data("obj-id",props["UID"]);

			
		};
		
	})($(this));

	objCore.initializeElement();
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	if (!cancelRender) {
		objCore.render();
	}
	
	
	
	
	
	return methodResult;
};

$.fn.giTabEditor = function(methodName, params){
	var cancelRender = false;
	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		
		/* Common methods */
		this.initializeElement = function(){
			props = $objEl.data("giProperties");
		};
		
		this.destroy = function(){
			$objEl.find('.gi-tab-editor-tool').remove();
			$objEl.find('.ui-tab-title-input').blur();
			$objEl.off('click', '.ui-add-tab');
			$objEl.off('dblclick', '.ui-tab-title');
			$objEl.off('blur', '.ui-tab-title-input');
			$objEl.off('keypress', '.ui-tab-title-input');
			$objEl.off('click', '.ui-tab-remove');
			$objEl.off('hover', '.ui-tab');
			if($('.ui-sortable-helper').size() == 0){
				$objEl.find('.ui-tab-nav:first').sortable('destroy');
			}
			
			// $objEl.off('DOMNodeInserted', '.ui-tab-nav:first');

			cancelRender = true
		}

		this.render = function(){
			if(!$objEl.hasClass('giTabEditor')){
				var context = this;
				$objEl.addClass('giTabEditor')
				$objEl.find('.ui-tab-header:first').append("<a class='ui-add-tab gi-tab-editor-tool fa fa-plus' title='Add Tab'></a>");
			
				$objEl.off('hover', '.ui-tab')
				$objEl.on('hover', '.ui-tab:not(.ui-sortable-helper)', function(event){
					if(event.type == "mouseenter"){
						if($(this).find('.gi-tab-editor-tool').size() == 0){
							$(this).append("<span class='ui-tab-remove gi-tab-editor-tool fa fa-remove'></span>");
						}
					}else if(event.type == "mouseleave"){
						$(this).find('.gi-tab-editor-tool').remove();
					}					
				})

				$objEl.off('click', '.ui-add-tab');
				$objEl.on('click', '.ui-add-tab', function(e){
					var actionList = []
					actionList.push($objEl.giTabContainer('addTab'));
					$objEl.giContainer();
					giHistory.addEvent("Added Tab", 'modified', actionList)
					e.stopPropagation();
				})

				$objEl.off('dblclick', '.ui-tab-title');
				$objEl.on('dblclick', '.ui-tab-title', function () {
					var $sibs = $(this).siblings('.ui-tab-title-input');
					$sibs.css('display', 'inline-block')
					$sibs.focus();
					$sibs.val($(this).text())
					$(this).css('display', 'none');
				})

				$objEl.off('blur', '.ui-tab-title-input')
				$objEl.on('blur', '.ui-tab-title-input', function () {
					var index = $(this).parent().index();
					var title = $(this).val()
					$(this).siblings('.ui-tab-title').css('display', 'inline-block')
						$(this).css('display', 'none');
					if(title.trim() != ""){
						
						if($(this).siblings('.ui-tab-title').text() != $(this).val()){
							var actionList = []
							actionList.push($(this).closest('.giTabContainer').giTabContainer('setProperties', {Title:{Index:index, Title:title}}));
							$(this).siblings('.ui-tab-title').text($(this).val());
							giHistory.addEvent("Modified tab title", 'modified', actionList)
						}
					}
					
				})

				$objEl.off('keypress', '.ui-tab-title-input')
				$objEl.on('keypress', '.ui-tab-title-input',function(e){
				    if(e.which == 13){
				        $(this).blur();    
				    }
				});

				$objEl.off('click', '.ui-tab-remove');
				$objEl.on('click', '.ui-tab-remove',function(e){
				   var actionList = []
				   	actionList.push($(this).closest('.giTabContainer').giTabContainer('deleteTab', $(this).parent().index()));
				    giHistory.addEvent("Deleted Tab", 'delete', actionList)
				});

				$objEl.find('.ui-tab-nav:first').sortable({
					axis:'x',
					helper:function(e, elem){
						var cloned = $(elem).clone();
						cloned.find('.gi-tab-editor-tool').remove();
						// console.log($(elem).width())
						return cloned.css({width:'auto'});
					},
					update:function( event, ui ){
						var actionList = []
						actionList.push($objEl['giTabContainer']('updateTabs'))
						giHistory.addEvent("Modified Tab", 'modified', actionList)
					}
				})
			}
			
		};
		
	})($(this));

	objCore.initializeElement();
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	if (!cancelRender) {
		objCore.render();
	}
	
	
	
	
	
	return methodResult;
};

$.fn.giDashboard = function(methodName, params){
	var cancelRender = false;
	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var state = true;
		var defaultProps = {}
		var $template;
		var giExpTree1;
		var dbDepartmentIDs;
		var dbDepartmentPosLvls;
		var repData = undefined;
		
		/* Common methods */
		this.initializeElement = function(){
			props = $objEl.data("giProperties");
			repData = $('#workspace').data();
			$template = $('#dashboard-window');
			var context = this;
			var $depBox = $template.find("#db-department-column");
			var $posBox = $template.find("#db-position-column");
			var $grpBox = $template.find("#db-group-column");

			if(!$template.hasClass('isDialog')){

				$depBox.on('change','.select_all_department', function(){
					if($(this).prop("checked")){
						$depBox.find('input[type="checkbox"]:not(.select_all_department)').prop("checked",true)
					}else{
						$depBox.find('input[type="checkbox"]:not(.select_all_department)').prop("checked",false)
					}
				})
				
				$template.on('keyup',"#search_department",function(){
					context.populateDepartments($(repData["dbDepartmentIDs"]).SearchJsonArray('department',$(this).val()));
				})


				$posBox.on('change','.select_all_position', function(){
					if($(this).prop("checked")){
						$posBox.find('input[type="checkbox"]:not(.select_all_position)').prop("checked",true)
					}else{
						$posBox.find('input[type="checkbox"]:not(.select_all_position)').prop("checked",false)
					}
				})
				$template.on('keyup',"#search_position",function(){
					context.populatePositions($(repData["dbPositionIDs"]).SearchJsonArray('position',$(this).val()));
				})


				$grpBox.on('change','.select_all_group', function(){
					if($(this).prop("checked")){
						$grpBox.find('input[type="checkbox"]:not(.select_all_group)').prop("checked",true)
					}else{
						$grpBox.find('input[type="checkbox"]:not(.select_all_group)').prop("checked",false)
					}
				})
				$template.on('keyup',"#search_group",function(){
					context.populateGroups($(repData["dbGroupIDs"]).SearchJsonArray('group_name',$(this).val()));
				})
				//cancelRender = true;
				$template.addClass('isDialog')
			}else{
				$template.find("input.panel-head-input").val("")
				$template.find('input[type="checkbox"]').prop("checked",false);
			}

			// dbDepartmentIDs = $('#workspace').data('dbDepartmentIDs');
			// dbDepartmentPosLvls = $('#workspace').data('dbDepartmentPosLevels');
		};

		this.populateDepartments = function(deps){
			var $depBox = $template.find("#db-department-column");
			var $ulDep = $depBox.find('ul.ui-departments');
			var items = "";
			if($ulDep.size() == 0){
				$ulDep = $("<ul class='ui-departments'></ul>");
				$depBox.append($ulDep);
			}
			$ulDep.empty();

			$.each(deps, function(index, dep){
				items = items + "<li><input type='checkbox' " + (props["Dashboard Department IDs"].indexOf(dep.department_code) >= 0? "checked" : "") + " value='" + dep.department_code +"'> " + dep.department+"</li>";
				
			})
			$ulDep.append(items);
			if(repData["dbDepartmentIDs"].length == props["Dashboard Department IDs"].length){
				$depBox.find('input[type="checkbox"].select_all_department').prop("checked",true)
			}
		}

		this.populatePositions = function(pos){
			var $posBox = $template.find("#db-position-column");
			var $ulDep = $posBox.find('ul.ui-positions');
			var items = "";
			if($ulDep.size() == 0){
				$ulDep = $("<ul class='ui-positions'></ul>");
				$posBox.append($ulDep);
				
			}
			$ulDep.empty();
			$.each(pos, function(index, pos){
				items = items + "<li><input type='checkbox' " + (props["Dashboard Position IDs"].indexOf(pos.id) >= 0? "checked" : "") + " value='"+ pos.id +"'> "+ pos.position+"</li>";
			})
			$ulDep.append(items);
			if(repData["dbPositionIDs"].length == props["Dashboard Position IDs"].length){
				$posBox.find('input[type="checkbox"].select_all_position').prop("checked",true)
			}
		}

		this.populateGroups = function(grps){
			var $grpBox = $template.find("#db-group-column");
			var $ulDep = $grpBox.find('ul.ui-groups');
			var items = "";
			if($ulDep.size() == 0){
				$ulDep = $("<ul class='ui-groups'></ul>");
				$grpBox.append($ulDep);
				
			}
			$ulDep.empty();
			$.each(grps, function(index, grp){
				items = items + "<li><input type='checkbox' " + (props["Dashboard Group IDs"].indexOf(grp.id) >= 0? "checked" : "") + " value='"+ grp.id +"'> "+ grp.group_name+"</li>";
			})
			$ulDep.append(items);
			if(repData["dbGroupIDs"].length == props["Dashboard Group IDs"].length){
				$grpBox.find('input[type="checkbox"].select_all_group').prop("checked",true)
			}
		}

		this.saveDashboard = function(){
			var $depBox = $template.find("#db-department-column");
			dashDep = $depBox.find('input[type="checkbox"]:checked:not(.select_all_department)').map(function(){
				return $(this).val();
			}).get();
			props["Dashboard Department IDs"] = dashDep;

			var $posBox = $template.find("#db-position-column");
			dashDep = $posBox.find('input[type="checkbox"]:checked:not(.select_all_position)').map(function(){
				return $(this).val();
			}).get();
			props["Dashboard Position IDs"] = dashDep;

			var $groupBox = $template.find("#db-group-column");
			dashDep = $groupBox.find('input[type="checkbox"]:checked:not(.select_all_group)').map(function(){
				return $(this).val();
			}).get();
			props["Dashboard Group IDs"] = dashDep;
		}

		this.open = function(){
			
			var context = this;
			$template.dialog({
					//appendTo:'body',
					dialogClass:"ui-query-editor, gi-ui-dialog",
					appendTo:'#gi-content',
					resizable:false,
					modal:true,
					draggable:false,
					minHeight:540,
					title:"Assign Dashboard To",
					minWidth:990,
					resizable:'s',
					close: function( event, ui ) {
						$(this).dialog( "destroy" );
					},
					buttons:[
						{
						text: "Apply Changes",
						click:function(){
							context.saveDashboard();
							$(this).dialog( "close" );
						}}
					]
				});
		}

		this.render = function(){
			//this.setEventListener();		
			var context = this;
			var $depBox = $template.find("#db-department-column");
			var $posBox = $template.find("#db-position-column");
			var $grpBox = $template.find("#db-group-column");
			
			
	
			$depBox.append('<div class="loading" style="text-align: center;height: 100%;vertical-align: middle;">' + 
				'<i class="fa fa-refresh fa-spin" style="position: relative;top: 200px;font-size: 28px;"></i>' + 
				'</div>')
			if(repData["dbDepartmentIDs"] != undefined){
				$depBox.find('.loading').remove();
				context.populateDepartments(repData["dbDepartmentIDs"]);
			}else{
				
				giGetData.departments(function(r){
					repData["dbDepartmentIDs"] = r;
					//console.log(r)
					giGetData.departmentPositionLevel(function(lvl){
						repData["dbDepartmentPosLevels"] = lvl;
						$depBox.find('.loading').remove();
						context.populateDepartments(r);

					})
				})
			}

			$posBox.append('<div class="loading" style="text-align: center;height: 100%;vertical-align: middle;">' + 
				'<i class="fa fa-refresh fa-spin" style="position: relative;top: 200px;font-size: 28px;"></i>' + 
				'</div>')
			if(repData["dbPositionIDs"] != undefined){
				$posBox.find('.loading').remove();
				context.populatePositions(repData["dbPositionIDs"]);
			}else{
				
				giGetData.positions(function(r){
					repData["dbPositionIDs"] = r;
					$posBox.find('.loading').remove();
					context.populatePositions(r);
				})
			}



			$grpBox.append('<div class="loading" style="text-align: center;height: 100%;vertical-align: middle;">' + 
				'<i class="fa fa-refresh fa-spin" style="position: relative;top: 200px;font-size: 28px;"></i>' + 
				'</div>')
			if(repData["dbGroupIDs"] != undefined){
				$grpBox.find('.loading').remove();
				context.populateGroups(repData["dbGroupIDs"]);
			}else{
				
				giGetData.groups(function(r){
					repData["dbGroupIDs"] = r;
					$grpBox.find('.loading').remove();
					context.populateGroups(repData["dbGroupIDs"]);
				})
			}




			// $template.find('.gi-scrollable').perfectScrollbar("destroy");
			// $template.find('.gi-scrollable').perfectScrollbar({
			// 	wheelSpeed: 20,
			// 	wheelPropagation: false,
			// 	suppressScrollX: false,
			// 	includePadding:true
			// });
		};
		
	})($(this));

	objCore.initializeElement();
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	if (!cancelRender) {
		objCore.render();
	}
	
	
	
	
	
	return methodResult;
};

$.fn.giWebPageEditor = function(methodName, params){
	var cancelRender = false;
	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		
		/* Common methods */
		this.initializeElement = function(){
			props = $objEl.data("giProperties");
		};
		
		this.destroy = function(){
			var $handler = $objEl.children("div.handler");
			if($handler.size() != 0){
				$handler.remove();
			}

			cancelRender = true
		}

		this.render = function(){
			
			var $handler = $objEl.children("div.handler");
			if($handler.size() === 0){
				$handler = $("<div class='handler'>");
				$handler.appendTo($objEl);
			}
			$handler.css({width: $objEl.width(), height: $objEl.height(), "position":"absolute", "z-index":1});

		};
		
	})($(this));

	objCore.initializeElement();
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	if (!cancelRender) {
		objCore.render();
	}
	
	
	
	
	
	return methodResult;
};





})(jQuery);

var giEventEditors = {
	init:function(){
		this.addEventListener();
		this.giTextEventListener();
		this.giTabEditorEventListener();
	},
	addEventListener:function(){
			var moved_id = 0;
		var down_count = 0


		$('body').on('hover','.giObject:not(.giPage)', function( event ) {
			if( event.type === 'mouseenter' ) {
				$(this).giDraggable();
				if($(this).data('giIdentifierName') == "giTabContainer"){
					$(this).giTabEditor();
				}else if($(this).data('giIdentifierName') == "giWebPage"){
					$(this).giWebPageEditor()
				}
				

			}else if( event.type == 'mouseleave'){
				
				if($(this).data('giIdentifierName') == "giTabContainer"){
					//$(this).giTabEditor('destroy');
				}else if($(this).data('giIdentifierName') == "giWebPage"){
					$(this).giWebPageEditor('destroy')
				}
			}
		})
//1,594,851	
		// Double Click Editor
		$("#workspace").on('dblclick', '.giObject', function(event){
				
				if($(this).data('giIdentifierName') == "giText"){
					giEditors.giTextEditor(this);
				}else if($(this).data('giIdentifierName') == "giTableChart"){
					$(this).giQueryWindow("open");
				}else if($(this).data('giIdentifierName') == "giPieChart"){
					$(this).giQueryWindow("open");
				}else if($(this).data('giIdentifierName') == "giLineGraph"){
					$(this).giQueryWindow("open");
				}else if($(this).data('giIdentifierName') == "giAreaGraph"){
					$(this).giQueryWindow("open");
				}else if($(this).data('giIdentifierName') == "giColumnGraph"){
					$(this).giQueryWindow("open");
				}else if($(this).data('giIdentifierName') == "giBarGraph"){
					$(this).giQueryWindow("open");
				}else if($(this).data('giIdentifierName') == "giRecordsTable"){
					$(this).giQueryWindow("open");	
				}else if($(this).data('giIdentifierName') == "giKpiTable"){
					$(this).giQueryWindow("open");	
				}else if($(this).data('giIdentifierName') == "giNumericKpi"){
					$(this).giQueryWindow("open");	
				}else if($(this).data('giIdentifierName') == "giGaugeChart"){
					$(this).giQueryWindow("open");	
				}
		})

		$("#gi-content").on('dblclick', '#report_name', function(){

		})

		$("#save_report_button").on('click',function(){
				giEditors.SaveReport();
		});


		$("#dashboard_report_properties").on('click',function(){
				$("#gi-content").giPropertyWindow();
		});
		

		$("#dashboard-assign-button").on('click',function(){
			$("#gi-content").giDashboard("open");
		});

		$("#dashboard-preview-button").on('click',function(){
			var id = $("#gi-content").data("giProperties").ID;
			if(id!=null){
			

				if($("#workspace").data('lastSavedHistoryID') != (giHistory.getCurrEvent().event==undefined?null:giHistory.getCurrEvent().event.id)){
					giEditors.SaveReport(function(){
					window.open("/Insights/gi-dashboard-viewer?id=" + id);
				})
				}else{
					window.open("/Insights/gi-dashboard-viewer?id=" + id);
					

				}
			}else{
				showNotification({
					message: "Please save the report first.",
					type: "error",
					autoClose: true,
					duration: 3
				});
			}
		});

		$("#report_list_button").on('click',function(){
			window.location.href = "/Insights/gi-report-designer-list"
		});

		$("#new_report_button").on('click',function(){
			window.location.href = "/Insights/gi-report-designer"
		});

		$("#shared_object_button").on('click',function(){
			sharedObjects();
		});


		$("#report_print_button").on('click',function(){
			giEditors.Print()
		});

		$('body').on('mousedown', '.hBorderTop, .hBorderBottom, .vBorderRight, .vBorderLeft', function(){
			if( $(".giObject input:focus").length > 0 ){
		   		$(".giObject input:focus").blur();
			}
		})



		$(document).on('keydown',function(event){
			var $focusedElem  = $('.focused');
			

			//gi-cut-object
			if($(document.activeElement).get(0).nodeName == "BODY"){
				if(event.which == 27){
					var $cutElem  = $('.gi-cut-object');
					if($cutElem.size() != 0){
						$cutElem.removeClass('gi-cut-object');
						$('#workspace').data('clipboard',[]);
					}
					
				}
			}
			

			if ($focusedElem.length!=0) {
				var props = $focusedElem.data('giProperties');
				if (props['Identifier Name']!='giPage') {
					
					//Movements
					if (event.which == 37 || event.which == 38 || event.which == 39 || event.which == 40) {
						if (props['Positioning'] == 'manual') {
							//Down
							if($(document.activeElement).get(0).nodeName == "BODY"){
								if(event.which == 40){
									
									$.each($focusedElem, function(index, elem){
										var $elem = $(elem)
										var elemProps = $elem.data('giProperties')
										$elem[elemProps['Identifier Name']]('set Top',elemProps['Top'] + 1)
										event.preventDefault();
									})
    								event.preventDefault();
    					
								}
								//Up
								if(event.which == 38){
									
									$.each($focusedElem, function(index, elem){
										var $elem = $(elem)
										var elemProps = $elem.data('giProperties')
										$elem[elemProps['Identifier Name']]('set Top',elemProps['Top'] - 1)
										event.preventDefault();
									})
									//$focusedElem[props['Identifier Name']]('set Top',props['Top'] - 1)
								}
								//Left
								if(event.which == 37){
									
									$.each($focusedElem, function(index, elem){
										var $elem = $(elem)
										var elemProps = $elem.data('giProperties')
										$elem[elemProps['Identifier Name']]('set Left',elemProps['Left'] - 1)
										event.preventDefault();
									})
									//$focusedElem[props['Identifier Name']]('set Left',props['Left'] - 1)	
								}
								//Right
								if(event.which == 39){
									$.each($focusedElem, function(index, elem){
										var $elem = $(elem)
										var elemProps = $elem.data('giProperties')
										$elem[elemProps['Identifier Name']]('set Left',elemProps['Left'] + 1)
										event.preventDefault();
									})
									//$focusedElem[props['Identifier Name']]('set Left',props['Left'] + 1)
								}

								
							}
						}

					}
					if($(document.activeElement).get(0).nodeName == "BODY"){
					//Objects with Ctrl key

						if (event.ctrlKey) {
							//Ctrl + C = Copy
							if (event.which == 67) {
								console.log($focusedElem)
								giEditors.Copy($focusedElem);
								event.preventDefault();
							}
							//Ctrl + X = Cut
							if (event.which == 88) {
								giEditors.Cut($focusedElem);
								event.preventDefault();
								
							}


						}

						//Ctrl + P = Print
							if(event.which == 80){
								console.log("Print");
								giEditors.Print();
								event.preventDefault();
							}	
						//Delete
						if(event.which == 46) {
							var deletedItems = []
						
							$.each($focusedElem, function(index, elem){
								var $elem = $(elem)
								var elemProps = $elem.data('giProperties')
								deletedItems.push($elem[elemProps['Identifier Name']]('delete'));
							})	
							$("#gi-content").giPropertyWindow();
							giHistory.addEvent('Deleted','deleted',deletedItems);
							event.preventDefault();
						}
						
					}
				}
			}
			
    		
			//Undo-Redo
			if (event.ctrlKey) {
				var clipboard = $('#workspace').data('clipboard');
				//Paste
				if (event.which == 86) {
					if($(document.activeElement).get(0).nodeName == "BODY"){
						giEditors.Paste();	
						event.preventDefault()
					}
											
				}
				//Undo
				if (event.which == 90) {
					
					if($(document.activeElement).get(0).nodeName == "BODY"){
						giHistory.Undo()
						
						event.preventDefault()
					}
				}
				//Redo
				if (event.which == 89) {
					if($(document.activeElement).get(0).nodeName == "BODY"){
						giHistory.Redo()
						
						event.preventDefault()
					}
					
				}	
				//Save
				if (event.which == 83) {
					giEditors.SaveReport()
					event.preventDefault()				
				}

				//Print
				if(event.which == 80){
					console.log("Print");
					giEditors.Print();
					event.preventDefault();
				}
			}
		})

	},
	giTextEventListener:function(){
		$("#workspace").on('mousedown', '.wysiwyg-editor', function(event){
			if(event.which==3){
				event.stopPropagation();
			}
		})
		$("#workspace").on('mousedown', '.wysiwyg-popup', function(event){
			if(event.which==3){
				event.stopPropagation();
			}
		})
	},
	giTabEditorEventListener:function(){

	},
}
var isSaving = false;
var giEditors = {
	
	ClipBoardData:function(){
		return $('#workspace').data('clipboard');
	},
	Paste:function(event){
		var clipboard = this.ClipBoardData()
		TempUids = [];
		counter = 0;
		var actionList = []
		$.each(clipboard, function(index, item){
			
			var $element = $('<div></div>');
			var newProps = $.extend(true, {}, item.giObject);
			delete newProps['Identifier Name'];
			delete newProps['Maintain Aspect Ratio'];
			delete newProps['Cell Padding'];
			
			
			if (item.method == 'copy') {
				//newProps['UID'] = 'uid-' + item.giObject['Identifier Name'] + '-' +(new Date()).getTime();
				
				ChangeAllUID(newProps);
				
				
				$.each(TempUids, function(ind, idtemp){
					if (idtemp.currentUID == item.giObject['Container ID']) {
						newProps['Container ID'] = idtemp.newUID;
					}
				})
				
				
				if (item.level == 'parent') {
					var newContainerID = $('.giObjectContainer.focused-container:first').attr('id');
					if (newContainerID != newProps['Container ID']) {
						newProps['Left'] = 0;
						newProps['Top'] = 0;
					}
					newProps['Container ID'] = newContainerID;
				}

				if(event!= undefined){
					newProps['Left'] = (event.pageX - $(event.target).offset().left)
					newProps['Top']= (event.pageY - $(event.target).offset().top)
				}else{
					if(item.used!=undefined){
						newProps['Left'] += (6 * item.used);
						newProps['Top'] += (6 * item.used);
						item.used += 1;
					}
				}
				
				
				newProps['Z-index'] = $("#" + newContainerID).children().getMaxZ() + 1
				actionList.push($element[item.giObject['Identifier Name']]('setProperties',newProps))
				$element.giContainer();
				
			}
			
			if (item.method == 'cut') {
				if (item.level == 'parent') {
					var cutProps = {}
					$('#' + newProps['UID']).removeClass('gi-cut-object');
					var newContainerID = $('.giObjectContainer.focused-container:first').attr('id');
					
					if (newContainerID != newProps['Container ID']) {
						cutProps.Top = 0;
						cutProps.Left = 0;	
					}


					if(event!= undefined){
						cutProps['Left'] = (event.pageX - $(event.target).offset().left)
						cutProps['Top']= (event.pageY - $(event.target).offset().top)
					}

					cutProps['Container ID'] = newContainerID;
					actionList.push($('#' + newProps['UID'])[item.giObject['Identifier Name']]('setProperties', cutProps));
					
				}
			}
			
		})
		
		if (clipboard.length!=0) {
			if (clipboard[0].method == 'cut') {
				giHistory.addEvent("Cut", 'cut', actionList)
			}else{
				giHistory.addEvent("Paste", 'copied', actionList)
			}
		}
		
		
		if (clipboard.length > 0) {
			if (clipboard[0].method == 'cut') {
				while (clipboard.length > 0){
			clipboard.pop();
		}
			}
		}
	},
	Copy:function($elements){
		console.log('Copy')
		var clipboard = this.ClipBoardData()
		
		clearClipboard();
		
		var newProps = null
		$.each($elements,function(index, elem){

			var $elem = $(elem)
			var props = $elem.data('giProperties');
			newProps = $.extend(true, {}, props); //to Retain last config
			var newEntry = {method:"copy", giObject:newProps, level:'parent', used:1}
			
			clipboard.push(newEntry);
			
			$.each($elem.find('.giObject'),function(cIndex, child){
				var $child = $(child)
				var cProps = $child.data('giProperties');
				var newCProps = $.extend(true, {}, cProps); //to Retain last config
				var newCEntry = {method:"copy", giObject:newCProps, level:'child'}
				clipboard.push(newCEntry)
			
			});
			
		})
		
		
		
		
		//Focus container where the object is
		$('.giObjectContainer.focused-container').each(function(){
			$(this).removeClass('focused-container');
		})
		$('#' + newProps['Container ID']).addClass('focused-container');
	},
	Cut:function($elements){
		console.log('Cut')
		var clipboard = this.ClipBoardData()
		
		clearClipboard();

		var newProps = null
		$.each($elements,function(index, elem){
			var $elem = $(elem)
			var props = $elem.data('giProperties');
			newProps = $.extend(true, {}, props); //to Retain last config
			var newEntry = {method:"cut", giObject:newProps, level:'parent'}
			
			clipboard.push(newEntry);
			
			$.each($elem.find('.giObject'),function(cIndex, child){
				var $child = $(child)
				var cProps = $child.data('giProperties');
				var newCProps = $.extend(true, {}, cProps); //to Retain last config
				var newCEntry = {method:"cut", giObject:newCProps, level:'child'}
				clipboard.push(newCEntry)
			
			})
			//elem.remove();
			$elem.addClass('gi-cut-object');
		})
		
		
		//var newProps = $.extend(true, {}, props); //to Retain last config
		//$('#' + props['UID']).remove();
		//clipboard.push({method:"cut", giObject:newProps});
		
		$('.giObjectContainer.focused-container').each(function(){
			$(this).removeClass('focused-container');
		})
		$('#' + newProps['Container ID']).addClass('focused-container');
	},
	setZindex:function(){
	},
	BringToFront:function($objEl){
		var hIndex = $objEl.siblings().getMaxZ();
		giHistory.addEvent('Modified ' + $objEl.data('giProperties')["Object Type"],'modified',$objEl.setZindex(++hIndex));		
	},
	BringForward:function($objEl){
		var zIndex = $objEl[$objEl.data('giIdentifierName')]('get Z-index').methodResult
		var nIndex = $objEl.parent().children().getNextZ(zIndex);
		if(nIndex!=undefined){
			giHistory.addEvent('Modified ' + $objEl.data('giProperties')["Object Type"],'modified',$objEl.setZindex(++nIndex));
		}
	},
	SendToBack:function($objEl){

		giHistory.addEvent('Modified ' + $objEl.data('giProperties')["Object Type"],'modified',$objEl.setZindex(1));
	},
	SendBackward:function($objEl){
		var zIndex = $objEl[$objEl.data('giIdentifierName')]('get Z-index').methodResult
		var pIndex = $objEl.parent().children().getPrevZ(zIndex);
		if(pIndex!=undefined){
			giHistory.addEvent('Modified ' + $objEl.data('giProperties')["Object Type"],'modified',$objEl.setZindex(pIndex));		
		}
	},
	giTextEditor:function(object){
		var context = object;
		var $wysiwyg = $(object).find('.wysiwyg-container');
		var $textContent = $(object).find('.text-content:first');

		if($wysiwyg.size() == 0){
			
			$textContent
				.wysiwyg({
	            toolbar: 'selection',
	            buttons: {
	                insertlink: {
	                    title: 'Insert link',
	                    image: '\uf08e', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                     showselection: false
	                },
	                // Fontname plugin
	                fontname: {
	                    title: 'Font',
	                    image: '\uf031', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    popup: function( $popup, $button ) {
	                            var list_fontnames = {
	                                    // Name : Font
	                                    'Arial, Helvetica' : 'Arial,Helvetica',
	                                    'Verdana'          : 'Verdana,Geneva',
	                                    'Georgia'          : 'Georgia',
	                                    'Courier New'      : 'Courier New,Courier',
	                                    'Times New Roman'  : 'Times New Roman,Times'
	                                };
	                            var $list = $('<div/>').addClass('wysiwyg-plugin-list')
	                                                   .attr('unselectable','on');
	                            $.each( list_fontnames, function( name, font ) {
	                                var $link = $('<a/>').attr('href','#')
	                                                    .css( 'font-family', font )
	                                                    .html( name )
	                                                    .click(function(event) {
	                                                        $textContent.wysiwyg('shell').fontName(font).closePopup();
	                                                        // prevent link-href-#
	                                                        event.stopPropagation();
	                                                        event.preventDefault();
	                                                        return false;
	                                                    });
	                                $list.append( $link );
	                            });
	                            $popup.append( $list );
	                           },
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: true    // wanted on selection
	                },
	                // Fontsize plugin
	                fontsize: {
	                    title: 'Size',
	                    image: '\uf034', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    popup: function( $popup, $button ) {
	                            // Hack: http://stackoverflow.com/questions/5868295/document-execcommand-fontsize-in-pixels/5870603#5870603
	                            var list_fontsizes = [];
	                            for( var i=8; i <= 11; ++i )
	                                list_fontsizes.push(i+'px');
	                            for( var i=12; i <= 28; i+=2 )
	                                list_fontsizes.push(i+'px');
	                            list_fontsizes.push('36px');
	                            list_fontsizes.push('48px');
	                            list_fontsizes.push('72px');
	                            
	                            var $list = $('<div/>').addClass('wysiwyg-plugin-list')
	                                                   .attr('unselectable','on');
	                            $.each( list_fontsizes, function( index, size ) {
	                                var $link = $('<a/>').attr('href','#')
	                                                    .html( size )
	                                                    .click(function(event) {
	                                                        $textContent.wysiwyg('shell').fontSize(7).closePopup();
	                                                        $textContent.wysiwyg('container')
	                                                                .find('font[size=7]')
	                                                                .removeAttr("size")
	                                                                .css("font-size", size);
	                                                        // prevent link-href-#
	                                                        event.stopPropagation();
	                                                        event.preventDefault();
	                                                        return false;
	                                                    });
	                                $list.append( $link );
	                            });
	                            $popup.append( $list );
	                           }
	                    //showstatic: true,    // wanted on the toolbar
	                    //showselection: true    // wanted on selection
	                },
	                // Header plugin
	                header: {
	                    title: 'Header',
	                    image: '\uf1dc', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    popup: function( $popup, $button ) {
	                            var list_headers = {
	                                    // Name : Font
	                                    'Header 1' : '<h1>',
	                                    'Header 2' : '<h2>',
	                                    'Header 3' : '<h3>',
	                                    'Header 4' : '<h4>',
	                                    'Header 5' : '<h5>',
	                                    'Header 6' : '<h6>',
	                                    'Code'     : '<pre>'
	                                };
	                            var $list = $('<div/>').addClass('wysiwyg-plugin-list')
	                                                   .attr('unselectable','on');
	                            $.each( list_headers, function( name, format ) {
	                                var $link = $('<a/>').attr('href','#')
	                                                     .css( 'font-family', format )
	                                                     .html( name )
	                                                     .click(function(event) {
	                                                        $textContent.wysiwyg('shell').format(format).closePopup();
	                                                        // prevent link-href-#
	                                                        event.stopPropagation();
	                                                        event.preventDefault();
	                                                        return false;
	                                                    });
	                                $list.append( $link );
	                            });
	                            $popup.append( $list );
	                           },
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: false    // wanted on selection
	                },
	                bold: {
	                    title: 'Bold (Ctrl+B)',
	                    image: '\uf032', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    hotkey: 'b'
	                },
	                italic: {
	                    title: 'Italic (Ctrl+I)',
	                    image: '\uf033', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    hotkey: 'i'
	                },
	                underline: {
	                    title: 'Underline (Ctrl+U)',
	                    image: '\uf0cd', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    hotkey: 'u'
	                },
	                strikethrough: {
	                    title: 'Strikethrough (Ctrl+S)',
	                    image: '\uf0cc', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    hotkey: 's',
	                    showselection: false 
	                },
	                forecolor: {
	                    title: 'Text color',
	                    image: '\uf1fc' // <img src="path/to/image.png" width="16" height="16" alt="" />
	                },
	                highlight: {
	                    title: 'Background color',
	                    image: '\uf043' // <img src="path/to/image.png" width="16" height="16" alt="" />
	                },
	                alignleft: {
	                    title: 'Left',
	                    image: '\uf036', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: true    // wanted on selection
	                },
	                aligncenter: {
	                    title: 'Center',
	                    image: '\uf037', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: true    // wanted on selection
	                },
	                alignright:{
	                    title: 'Right',
	                    image: '\uf038', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: true    // wanted on selection
	                },
	                alignjustify:{
	                    title: 'Justify',
	                    image: '\uf039', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: true    // wanted on selection
	                },
	                subscript:{
	                    title: 'Subscript',
	                    image: '\uf12c', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: true    // wanted on selection
	                },
	                superscript:{
	                    title: 'Superscript',
	                    image: '\uf12b', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: true    // wanted on selection
	                },
	                indent:{
	                    title: 'Indent',
	                    image: '\uf03c', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: false    // wanted on selection
	                },
	                outdent:{
	                    title: 'Outdent',
	                    image: '\uf03b', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: false    // wanted on selection
	                },
	                orderedList:{
	                    title: 'Ordered list',
	                    image: '\uf0cb', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: false    // wanted on selection
	                },
	                unorderedList:{
	                    title: 'Unordered list',
	                    image: '\uf0ca', // <img src="path/to/image.png" width="16" height="16" alt="" />
	                    //showstatic: true,    // wanted on the toolbar
	                    showselection: false    // wanted on selection
	                },
	                removeformat: {
	                    title: 'Remove format',
	                    image: '\uf12d' // <img src="path/to/image.png" width="16" height="16" alt="" />
	                }
	            },
	            // Submit-Button
	            submit: {
	                title: 'Submit',
	                image: '\uf00c' // <img src="path/to/image.png" width="16" height="16" alt="" />
	            },
	            // Other properties

	            placeholderUrl: 'www.example.com',
		        })
	        .blur(function() {

				if( typeof console != 'undefined' ){
					var actionList = []
					$textContent.attr('contenteditable',false);
					actionList.push($(context).giText('setProperties', {Content:$textContent.html()}));
					//If text is edited
					if(actionList.length > 0){
					 	if(actionList[0].affected[0].propAfter.Content != actionList[0].affected[0].propBefore.Content){
					 		giHistory.addEvent("Edited Text", 'modified', actionList)
					 	}		
					}

					//Destroy wysiwyg
					$textContent.removeClass('wysiwyg-editor')
					var $cloned = $textContent.clone()
					$(context).find('.wysiwyg-container').remove();
					$(context).append($cloned)
				}
			});
		}
		$textContent.focus();
	},
	SaveReport:function(callback){
		var $giContent = $("#gi-content");
		if(!isSaving){
			
			if($.trim($giContent.data('giProperties').Name) == ""){
				showNotification({
					message: "Report Name is required.",
					type: "error",
					autoClose: true,
					duration: 3
				});
				$giContent.giPropertyWindow();
				$('.property-report-name').focus()

			}else{
				isSaving = true;	
				ui.block()
				giReport.Save(function(result){
					if($('#report_id').val()=="" || $('#report_id').val()== undefined){
						window.history.pushState("", "Title", "/Insights/gi-report-designer?id=" + result.ReportId);
					}
					ui.unblock();
					$("#workspace").data('lastSavedHistoryID', (giHistory.getCurrEvent().event==undefined?null:giHistory.getCurrEvent().event.id));
					modifiedNotifier()
					showNotification({
						message: "Report has been " +(result.Status == "New"? "saved" : "updated")+ ".",
						type: "success",
						autoClose: true,
						duration: 3
					});
					isSaving = false;
					if(typeof(callback) == 'function'){
						callback(result);
					}
					
				})

			}
			
		}
		
	},
	Print:function(){
		var context = this;
		//context.SaveReport()

		if($("#workspace").data('lastSavedHistoryID') != (giHistory.getCurrEvent().event==undefined?null:giHistory.getCurrEvent().event.id)){
			context.SaveReport(function(){
				printReport();
			})
		}else{
			printReport();
		}
		//$('#printForm')
		
	}
}

var TempUids = [];
var counter = 0


function printReport(){
	if($('#giDialogs').hasClass('ui-dialog-content') && $('#giDialogs').dialog('isOpen')){
		$('#giDialogs').dialog('close')
	}else{
		$("#giDialogs").html('<i class="fa fa-print" style="font-size: 50px;display: inline-block;margin: 20px;"></i>' + 
		'<a style="position: relative;top: -25px;display: inline-block;">Done Printing.</a>')
	}


	var iframe = document.getElementById('printForm');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	$(innerDoc).find("#content").html($('#content').html());
	window.frames["printForm"].focus();
	window.frames["printForm"].print();



	$("#giDialogs").dialog({
		appendTo:'#gi-content',
		dialogClass:'gi-ui-dialog',
		width:280,
		height:130,
		title:"Print",
		resizable:false});

	setTimeout(function() {
		$('#giDialogs').dialog('close');
	}, 2000);
}

function changPosition($obj){
	var identifierName = $obj.data('giIdentifierName')
	var response;
	if($obj[identifierName]('get Positioning').methodResult == "auto"){
		response = $objEl[props["Identifier Name"]]("setProperties",{"Positioning":"manual"})

	}else{
		response = $objEl[props["Identifier Name"]]("setProperties",{"Positioning":"auto"})	
	}

}

function clearClipboard(){
	var clipboard = $('#workspace').data('clipboard');

	while (clipboard.length > 0){
		clipboard.pop();
	}
}

function ChangeAllUID(prop) {
	$.each(prop,function(i,val){
		if (i=='UID') {
			var newUID = val.substring(0,val.lastIndexOf("-")) + "-" + (new Date()).getTime() + counter ;
			TempUids.push({currentUID:val, newUID:newUID});
			prop[i] = newUID;
			counter++			
		}
		if (typeof val == 'object' && !(val == null || val == undefined)) {
			ChangeAllUID(val);
		};
		
	})
	
}

function modifiedNotifier() {
	if($("#workspace").data('lastSavedHistoryID') != (giHistory.getCurrEvent().event==undefined?null:giHistory.getCurrEvent().event.id)){
		$("#report_mod_notifier").css("display","inline-block");
	}else{
		$("#report_mod_notifier").css("display","none");
	}
}

function sharedObjects(){
	// console.log($('#giSharedObject').dialog("isOpen"))
	// if($('#giSharedObject').dialog("isOpen")){
	// 	return
	// }
	var $gsO = $('#giSharedObject');
	$('#shared-object-list').html('<div class="loading" style="text-align: center;height: 100%;vertical-align: middle;"><i class="fa fa-refresh fa-spin" style="' +
								   'position: relative;top: 200px;font-size: 28px;"></i></div>')
	
	giGetData.sharedObjects(function(res){
		$('#shared-object-list').html("");
		$.each(res, function(index, val){
			//console.log(val)

			var $objects = $('<li class="ui-obj-item shared-object" data-id="'+ val.ID + '" data-gi-identifier-name="' + val["Identifier Name"] + '">' +
				'<div class="ui-obj-icon"></div>' +
				'<div class="ui-obj-title">'+ val["Name"] + '</div>' +
				'<div class="ui-obj-shared-date">'+ val["Date Added"] + '</div>' +
			'</li>').data('giProperties',val.giProperties);
			$('#shared-object-list').append($objects)
		});
		//
		$('.ui-obj-item.shared-object').draggable({
					revert:false,
					helper: function(){
						var a = $('<div></div>')
						return a.css({border:'black dashed 1px', height:'200px', width:'200px'})
					}, 
					cursorAt: {left:0,top:(-1)},
					opacity: 0.5,
					zIndex:10000,
					containment: '#workspace-content',
					appendTo: '#content',
					cursor:'pointer',
				})
	})
	
	
	var sharedObjectD = $('#giSharedObject').dialog({
		appendTo:'#gi-content',
		dialogClass:'gi-ui-dialog',
		width:242,
		height:500,
		title:"Shared Objects",
		resizable:'s'
	})
}

function addSharedObject(props){
	$("#shared-object-name-input").val("");
	var giAddSharedObject = $('#giAddSharedObject').dialog({
		appendTo:'#gi-content',
		dialogClass:'gi-ui-dialog',
		width:280,
		height:130,
		title:"Add Shared Objects",
		resizable:false,
		buttons:[
		{
			text:"Save",
			click:function(){
				var obj = {};
				var name = $("#shared-object-name-input").val();
				obj['Report ID'] = $('#gi-content').data('giProperties').ID;
				obj['giProperties'] = props;
				if(name != "" && name.toString().trim() != ""){
					obj.Name = name;
					giPostData.addSharedObject(obj, function(result){
						showNotification({
							message: "Object has been saved.",
							type: "success",
							autoClose: true,
							duration: 10
						});
					})
					
					$(this).dialog( "close" );
				}else{
					showNotification({
						message: "Invalid name.",
						type: "error",
						autoClose: true,
						duration: 10
					});
				}
				
			}
		}
		]
	})
}

var giHistory = {
	giWorkspaceData:function(){
		return $('#workspace').data();
	},
	getCurrEvent:function(){
		var workspace = this.giWorkspaceData();
		//return workspace.history[workspace.historyCurrentPosition];
		var i = workspace.historyCurrentPosition;
		return {event:workspace.history[i],
			position:i}
	},
	getNextEvent:function(){
		var workspace = this.giWorkspaceData();
		//return workspace.history[++workspace.historyCurrentPosition]
		var i = workspace.historyCurrentPosition;
		++i
		return {event:workspace.history[i],
			position:i}
	},
	getPreviousEvent:function(){
		var workspace = this.giWorkspaceData();
		//return workspace.history[--workspace.historyCurrentPosition]
		var i = workspace.historyCurrentPosition;
		--i
		return {event:workspace.history[i],
			position:i}
	},
	setEventPosition:function(position){
		var workspace = this.giWorkspaceData();
		
		workspace.historyCurrentPosition = position;
		
		
		
		$('.gi-report-history .gi-action-history').remove();
		
		$.each(workspace.history,function(i, val){
			$('.gi-report-history').append('<div class="ui-obj-history gi-action-history" style="' + (i>position?"background-color:whitesmoke":"") + '" data-history-position="' + i + '"><i style="margin-right:10px" class="fa fa-history"></i> ' +val.name+ '<span class="pull-right" fl-show-opt="true"><i class="fa ' + (i==position?"fa-caret-left":"") + '"></i></span></div>');
		})
		modifiedNotifier()
		
	},
	addEvent:function(name, type, actionList){
		/*
		Types of events
		 - Paste
		 - Deleted
		 - Created
		 - Modified
		*/
		var workspaceData = this.giWorkspaceData()
		var context = this;
		
		var length = workspaceData.history.length
		var curpos = context.getCurrEvent().position
		
		if ((length-1)!=curpos) {
			var numberOf = (length-1) - curpos
			workspaceData.history.splice((curpos + 1), numberOf)
		}
		
		workspaceData.history.push({id:(new Date()).getTime() + Math.random(), name:name, type:type, actionList:actionList});
		length = workspaceData.history.length
		var historyLimit = workspaceData.historyLimit;
		if (length > historyLimit) {
			workspaceData.history.splice(0,(length - historyLimit));
		}
		length = workspaceData.history.length
		
		this.setEventPosition((length - 1))
	},
	Undo:function(){
		var workspaceData = this.giWorkspaceData()
		var context = this;
		
		if (context.getCurrEvent().event != undefined) {
			$.each(context.getCurrEvent().event.actionList,function(i, val){
				$.each(val.affected,function(index, item){
			
					if (item.type == "created") {
						$("#" + item.propAfter['UID'])[item.propAfter['Identifier Name']]('delete');
					}else{
						var newProps = $.extend(true, {}, item.propBefore);
						var $elem = $("#" + item.propBefore['UID'])
						if ($elem.length == 0) {
							$elem = $('<div></div>')
						}
						//console.log(newProps)
						$elem[item.propBefore['Identifier Name']]('setProperties', newProps)
						$elem.giContainer();
					}
					
					
				})
			})
			
		}
		if (context.getPreviousEvent().event == undefined) {
			context.setEventPosition(-1)
		}else{
			context.setEventPosition(context.getPreviousEvent().position)
		}
		
		//wrapFocusedObjects();

		
	},
	Redo:function(){
		var workspaceData = this.giWorkspaceData()
		var context = this;
		if (context.getNextEvent().event != undefined) {
			$.each(context.getNextEvent().event.actionList,function(i, val){
					$.each(val.affected,function(index, item){
						if (item.type == "deleted") {
							$("#" + item.propBefore['UID'])[item.propBefore['Identifier Name']]('delete')
						}else{
							var newProps = $.extend(true, {}, item.propAfter);
							var $elem = $("#" + item.propAfter['UID'])
							if ($elem.length == 0) {
								$elem = $('<div></div>')
							}
							$elem[item.propAfter['Identifier Name']]('setProperties', newProps);
							$elem.giContainer();
						}
					})
				})
		}
		
		if (context.getNextEvent().event != undefined) {
			context.setEventPosition(context.getNextEvent().position)
		}

		//wrapFocusedObjects();
		
	}
}

$.fn.getMaxZ = function(){
    return Math.max.apply(null, jQuery(this).map(function(){
		var z;
		return isNaN(z = parseInt(jQuery(this).css("z-index"), 10)) ? 0 : z;
    }));
}
$.fn.getNextZ = function(index){
    var arrayOfObj = jQuery(this).map(function(){
		var z;
		return isNaN(z = parseInt(jQuery(this).css("z-index"), 10)) ? 0 : z;
    }).sort();
    var i = $.inArray(index,arrayOfObj);
    var originalI =  $.inArray(index,arrayOfObj);
    var repeated = 0;
    while(index >= arrayOfObj[i]){
    	if(index == arrayOfObj[i]){
    		repeated++
    	}
    	i++;
    }

    if(repeated>1){
 		return index;	
    }else{
    	return arrayOfObj[i];	
    }
}
$.fn.getPrevZ = function(index){
    var arrayOfObj = jQuery(this).map(function(){
		var z;
		return isNaN(z = parseInt(jQuery(this).css("z-index"), 10)) ? 0 : z;
    }).sort();
    var i = $.inArray(index,arrayOfObj);
    while(index <= arrayOfObj[i]){
    	i--;
    }
    if (index <= 1) {
    	return 1;
    }else{
    	return arrayOfObj[i];
    }
}
$.fn.setZindex = function(index){
	var actionList = []
	var $this = $(this)		
	
	var i = index;
	$.each($this.siblings().SortObjectsByZindex(), function(objIndex, obj){
		var $objEl = $(obj);
		var zIndex = $objEl[$objEl.data('giIdentifierName')]('get Z-index').methodResult;
		
		if(zIndex >= index){
			i++
			actionList.push($objEl[$objEl.data('giIdentifierName')]('setProperties',{'Z-index': i}));
		}
	})
	actionList.push($this[$this.data('giIdentifierName')]('setProperties',{'Z-index': index}));

	return actionList;
}
$.fn.SortObjectsByZindex = function(){
	this.sort(function(a, b){
	    var a1= $(a).data('giProperties')['Z-index'], b1= $(b).data('giProperties')['Z-index'];
	    if(a1== b1) return 0;
	    return a1> b1? 1: -1;
	});
	return this;
}


$.fn.SortObjectsByIndex = function(index, order){
	return this.sort(function(a, b){
	    var a1= a[index], b1= b[index];
	    if(a1== b1) return 0;

	    if(order === "desc"){
	    	 return a1< b1? 1: -1;
	    }else{
	    	 return a1> b1? 1: -1;
	    }
	   
	}).get();
	 // this;
}


$.fn.SearchJsonArray = function(index, search){
	var $result = this.map(function(){
		if(this[index].toLowerCase().indexOf(search.toLowerCase())>-1){
			return this;
		}else{
			return null
		}
		
	})
	return $result;
}

function sortObject(o) {
    var sorted = {},
    key, a = [];

    for (key in o) {
        if (o.hasOwnProperty(key)) {
            a.push(key);
        }
    }

    a.sort();

    for (key = 0; key < a.length; key++) {
        sorted[a[key]] = o[a[key]];
    }
    return sorted;
}

function doneMoving(actionList){
	giHistory.addEvent('Moved', 'modified', actionList);
}


// var test = new jConfirm();
// console.log(test.themeConfirm("confirm2", {'icon': '<i class="fa fa-question-circle fl-margin-right fl-message-confirmation"></i>'}));

// var ret = "";
//                     ret += '<h3 class="fl-margin-bottom"><i class="icon-user"></i> ' + btn_val + ' Action </h3>';
//                     ret += '<div class="hr"></div>';
//                     ret += '<div class="fields" style="margin-bottom: -15px;">';
//                     ret += '<div class="label_below2"> ' + trackNo_process + ' </div>';
//                     //ret += '<div class="input_position"  style="width: 100%;">';
//                     //ret += trackNo_process;
//                     //ret += '</div>';
//                     ret += '</div>';
//                     ret += '<div class="input_position" style="margin-top:5px;text-align:right;">';
//                     ret += '<input type="button" class="fl-margin-right btn-blueBtn fl-positive-btn" value="&nbsp;Ok&nbsp;" id="popup_cancel">';
//                     //ret += '<input type="button" class="btn-basicBtn" id="popup_cancel" value="Ok" style="margin-bottom:5px;">';
//                     ret += '</div>';
//                     var newDialog = new jDialog(ret, "", "", "", "", function() {
//                     });
//                     newDialog.themeDialog("modal2");
window.wrapperProps = {Top:0,Left:0,TopHeight:0,LeftWidth:0}
window.wrapperPropsBefor = {Top:0,Left:0,TopHeight:0,LeftWidth:0}

// var wrapObjects = function(){

// 	//console.log($('.focused:not(.giPage)').data('giIdentifierName'))


// 	var $properties = $('.focused:not(.giPage)').map(function(){
// 			var props = $(this).data('giProperties');
// 			if(props!==undefined){
// 				props.LeftWidth = (props['Left']===undefined?0:props['Left']) + (props['Width']===undefined?0:props['Width']);
// 				props.TopHeight = (props['Top']===undefined?0:props['Top']) + (props['Height']===undefined?0:props['Height']);
// 			}
// 			return (props===undefined?null:props);
// 	}).get();
// 	//console.log($properties)
// 	if($properties.length > 0){
// 		var top = $($properties).SortObjectsByIndex('Top','asc')[0].Top 
// 		var left = $($properties).SortObjectsByIndex('Left','asc')[0].Left
// 		var height = ($($properties).SortObjectsByIndex('TopHeight','desc')[0].TopHeight ) - top;
// 		var width = ($($properties).SortObjectsByIndex('LeftWidth','desc')[0].LeftWidth ) - left;

// 		// var ctop = top > window.wrapperPropsBefor.Top ? window.wrapperPropsBefor.Top += 10:window.wrapperPropsBefor.Top -=10;
// 		// var cleft =left  > window.wrapperPropsBefor.Left ? window.wrapperPropsBefor.Left += 5:window.wrapperPropsBefor.Left -=5;
// 		// var cheight =  height > window.wrapperPropsBefor.TopHeight ? window.wrapperPropsBefor.TopHeight += 5:window.wrapperPropsBefor.TopHeight -=5;
// 		// var cwidth = width > window.wrapperPropsBefor.LeftWidth ? window.wrapperPropsBefor.LeftWidth += 5:window.wrapperPropsBefor.LeftWidth -=5;




// 		if($('.giPage .giObjectWrapper').size() === 0){
// 			var $wrapper = $('<div class="giObjectWrapper"></div>').draggable({
// 				helper:'resize-helper',
// 				minHeight:1,
// 				minWidth:1,
// 				handles: 'nw,ne,sw,se,n,e,s,w',

// 				start:function(event, ui){
// 					window.wrapperProps.Top = ui.position.top;
// 					window.wrapperProps.Left = ui.position.left;
// 					window.wrapperProps.TopHeight = height;
// 					window.wrapperProps.LeftWidth = width;
// 					//console.log(ui.position.top)
// 				},
// 				stop:function(event, ui){


// 					var ctop = ui.position.top - window.wrapperProps.Top 
// 					var cleft = ui.position.left - window.wrapperProps.Left
// 					//console.log("After : " + ui.position.top + " - Befor : " + window.wrapperProps.Top + " = " + ctop)
// 	 				// var cheight = 
// 					// var cwidth = 
// 					// console.log(ctop)
// 					// console.log(cleft)
// 					// console.log(window.wrapperProps.el)
// 					$.each(window.wrapperProps.el, function(){
// 						console.log($(this).data('giIdentifierName'))
// 						var props = $(this).data('giProperties')
// 						$(this)[$(this).data('giIdentifierName')]('setProperties',{'Top':(props['Top']+ ctop),'Left':props['Left'] + cleft})
// 					})
// 				}
// 			});;
// 			$('.giPage').append($wrapper);
// 		}
// 		$('.giObjectWrapper').css({
// 				'border':'1px blue dashed',
// 				'top':top + 'px',
// 				'left':left + 'px',
// 				// 'transform':"translate3D("+ ((left - cleft) <= 10 ? left : cleft) +"px, "+ ((top - ctop) <= 10 ? top : ctop) +"px, 0)",
// 				'height':height + 'px',
// 				'width':width + 'px',
// 				'position':'absolute',
// 				'z-index':30
// 			});

// 		// console.log(top)
// 		// console.log(left)
// 		// console.log(height)
// 		// console.log(width)
// 	}
// 	window.requestAnimationFrame(wrapObjects);
	

// }

var wrapFocusedObjects = function(){

	$('.giObject.focused').each(function(index, val){
		$(val).giResize("handles",{'size':
			{
				width:$(val).width(),
				height:$(val).height()
			},
			element:$(val)})
		}
	)
	window.requestAnimationFrame(wrapFocusedObjects);

}
wrapFocusedObjects();

// $('.giObject.focused').each(function(in, val){$(val).giResize("handles",{'size':{
// 		width:$(this).width(),
// 		height:$(this).height},
// 	ui:$(this)}
// 	)}
// )

//window.requestAnimationFrame(wrapObjects);
 $.widget("ui.fixedSortable", $.ui.sortable, {
                _init: function () {
                    this.element.data("sortable", this.element.data("fixedSortable"));
                    return $.ui.sortable.prototype._init.apply(this, arguments);
                },
                _create:function() {
                    var result = $.ui.sortable.prototype._create.apply(this, arguments);
                    this.containerCache.sortable = this;
                    return result;
                },
                _intersectsWithPointer: function (item) {
                	//console.log(item.instance.element)
//This line.... item.instance.element.hasClass("main-list") && 

                    if (item.instance.element.css('position') == 'absolute') { 
                        return false;
                    }
                    return $.ui.sortable.prototype._intersectsWithPointer.apply(this, arguments);
                },
                _intersectsWith: function(containerCache) {
//Also this line.... containerCache.sortable.element.hasClass("main-list") && 
                    if (containerCache.sortable.element.css('position') == 'absolute') {
                        return false;
                    }
                    return $.ui.sortable.prototype._intersectsWith.apply(this, arguments);
                }
            });