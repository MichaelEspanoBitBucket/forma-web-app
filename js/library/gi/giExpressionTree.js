
$.fn.getPrevNextElementsFromXy = function(x, y){
	var $parent = this;
	var prntRect = $parent[0].getBoundingClientRect();
	
	var prev = undefined;
	var next = undefined;
	$parent.children().each(function(i, v){
		var $child = $(v);
		
		var childRect = $child[0].getBoundingClientRect();
		var cInfo = { 
			element: $child,
			left: childRect.left - prntRect.left,
			top: childRect.top - prntRect.top,
			width: $child.width(),
			height: $child.height()
		};
		if(cInfo.left + cInfo.width < x){
			//before
			prev = prev === undefined ? cInfo: (prev.left > cInfo.left ? prev: cInfo);
		}else if(x < cInfo.left){
			//after
			next = next === undefined ? cInfo: (next.left < cInfo.left ? next: cInfo);
		}
	});
	
	return { 
		prev: prev === undefined ? undefined : prev.element, 
		next: next === undefined ? undefined: next.element
	};
};

$.fn.getCursorPosition = function() {
	var element = this.get(0);
	var caretOffset = 0;
	if (typeof window.getSelection != "undefined") {
		var range = window.getSelection().getRangeAt(0);
		var preCaretRange = range.cloneRange();
		preCaretRange.selectNodeContents(element);
		preCaretRange.setEnd(range.endContainer, range.endOffset);
		caretOffset = preCaretRange.toString().length;
	} else if (typeof document.selection != "undefined" && document.selection.type != "Control") {
		var textRange = document.selection.createRange();
		var preCaretTextRange = document.body.createTextRange();
		preCaretTextRange.moveToElementText(element);
		preCaretTextRange.setEndPoint("EndToEnd", textRange);
		caretOffset = preCaretTextRange.text.length;
	}
	return caretOffset;
}

$.fn.getElementCursorPosition = function() {
	var element = this.get(0);
	var caretOffset = 0;
	
		var range = window.getSelection().getRangeAt(0);// document.createRange();
		//var preCaretTextRange = document.body.createTextRange();
		var $caretNode = $("<caret>x</caret>");
		
		var $startNode = $(range.startContainer);
		console.log($startNode[0].nodeValue);
		if($startNode[0].nodeValue === ""){
			//at the beginning there's no node
			$(element).prepend($caretNode);
		}else{
			//a node exist 
			$caretNode.insertAfter($startNode.parent());
		}
		caretOffset = $(element).children().index("caret");
		//$(caretNode).remove();
	
	return caretOffset;
}

$.fn.setCursorPosition = function(start, end) {
	if(!end) end = start; 
	return this.each(function() {
		var el = $(this).get(0);
		el.focus();
		if (typeof el.setSelectionRange != "undefined") {
			el.setSelectionRange(start, end);
		}else if (typeof document.createRange != "undefined"){
			var range = document.createRange();
			range.setStart(el.firstChild, start);
			//range.setEnd(el, end);
			var sel = window.getSelection();
			sel.removeAllRanges();
			sel.addRange(range);
		}
	});
};

$.fn.setCursorAtEnd = function(){	
	return this.each(function() {
		var el = $(this).get(0);
		el.focus();
		var range = document.createRange();
		range.setStart(el, 1);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	});
}

$.fn.setCursorAtStart = function(){
	return this.each(function() {
		var el = $(this).get(0);
		el.focus();
		var range = document.createRange();
		range.setStart(el, 0);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	});
}

GiExpressionTree = function(params){
	
	var giExpTree = this;
	
	this.htmlContext = params.htmlContext === undefined ? $("body") : params.htmlContext;
	
	this.initialize = function(){
		/* [start] expression box behavior */
		giExpTree.htmlContext.on("click", ".giExp", function(e){
			var $giExp = $(e.currentTarget);
			var expRect = $giExp[0].getBoundingClientRect();
			var prevNext = $(e.currentTarget).getPrevNextElementsFromXy(e.clientX - expRect.left, e.clientY - expRect.top);
			
			var $in = giExpTree.getInput();
			
			if(prevNext.prev === undefined){
				$giExp.prepend($in);
			}else{
				$in.insertAfter(prevNext.prev);
			}
			$in.focus();
			
			e.stopPropagation();
		});
		/* [end] expression box behavior */
		
		/* [start] expression node behavior */
		giExpTree.htmlContext.on("click", ".giExpNode", function(e){
			e.stopPropagation();
		});
		 
		giExpTree.htmlContext.on("mouseover", ".giExpNode.literal, .giExpNode.unknown, .giExpNode.operator, .giExpNode.object, .giExpNode.null", function(e){
			var $literalNode = $(e.currentTarget);
			$literalNode.attr("contenteditable", "true");
			$literalNode.addClass("hover");
			e.stopPropagation();
		});
		
		giExpTree.htmlContext.on("mouseout", ".giExpNode.literal, .giExpNode.unknown, .giExpNode.operator, .giExpNode.object, .giExpNode.null", function(e){
			var $literalNode = $(e.currentTarget);
			$literalNode.removeAttr("contenteditable");
			$literalNode.removeClass("hover");
			e.stopPropagation();
		});
		
		giExpTree.htmlContext.on("mouseover", ".giExpNode", function(e){
			//var $literalNode = $(e.currentTarget);
			//$literalNode.addClass("hover");
			e.stopPropagation();
		});
		
		giExpTree.htmlContext.on("mouseout", ".giExpNode", function(e){
			//var $literalNode = $(e.currentTarget);
			//$literalNode.removeClass("hover");
			e.stopPropagation();
		});
		
		giExpTree.htmlContext.on("click", ".giExpNode.literal, .giExpNode.unknown, .giExpNode.operator, .giExpNode.object, .giExpNode.null", function(e){
			var $literalNode = $(e.currentTarget);
			var cursorPos = $literalNode.getCursorPosition();
			var $expIn = giExpTree.getInput();

			$expIn.blur();
			
			$expIn.text($literalNode.text());
			$literalNode.replaceWith($expIn);
			$expIn.setCursorPosition(cursorPos);
		});
		/* [end] expression node behavior */
		
		/* [start] expression input behavior */
		giExpTree.htmlContext.on("mouseover", ".giExpIn", function(e){
			e.stopPropagation();
		});
		
		giExpTree.htmlContext.on("click", ".giExpIn", function(e){
			e.stopPropagation();
		});
		
		giExpTree.htmlContext.on("focus", ".giExpIn", function(e){
			
			var $in = $(e.currentTarget);
			var text = $in.text();
			var type = giExpTree.getTextNodeType(text);
			$in.attr("data-type", type);
		});	
		
		giExpTree.htmlContext.on("focusout", ".giExpIn", function(e){
			
			var $in = $(e.currentTarget);
			
			if($in.data("cancelBlur")){
				e.preventDefault();
				return false;
			}
			
			var $newNode = giExpTree.textToHtml($in.text());
			$in.replaceWith($newNode);
			giExpTree.htmlContext.find(".giInMatchList").remove();
		});
		
		giExpTree.htmlContext.on("keydown", ".giExpIn", function(e){
			
			var $in = $(e.currentTarget);
			$in.data("cancelKeyUp", false);
			var text = $in.text();
			
			var keyChar = String.fromCharCode(e.keyCode);
			
			var cursorPos = $in.getCursorPosition();
			//recompute cursor position based on arrow keys pressed
			cursorPos = e.keyCode === 37 ? cursorPos-1: cursorPos;
			cursorPos = e.keyCode === 39 ? cursorPos+1: cursorPos;
			
			var type = giExpTree.getTextNodeType(text);
			
			//arrow key - exp input <-'abc' or 'abc'->
			
			if(e.keyCode === 13){
				//enter
				var $list = giExpTree.getMatchList();
				if($list !== null && $list.size() > 0 && $list.find(".active").size() > 0){
					var $active = $list.find(".giInMatchItem.active");
					var activeText = $active.text();
					
					var $temp = $("<temp/>");
					$temp.insertAfter($in);
					$in.text(activeText);
					$in.blur();
					$in = giExpTree.getInput();
					var $newNode = $temp.prev(".giExpNode");
					$temp.remove();
					if($newNode.hasClass("function") || $newNode.hasClass("grouping")){
						$newNode.find(".giExp").first().append($in);
						$in.focus();
					}else{
						$in.insertAfter($newNode);
						$in.focus();
					}
				}
				e.preventDefault();
				return false;
			}else if(e.keyCode === 38){
				//up arrow key
				var $list = giExpTree.getMatchList();
				if($list !== null && $list.size() > 0 && $list.find(".giInMatchItem").size() > 0){
					var $active = $list.find(".giInMatchItem.active");
					var $prev = $active.prev(".giInMatchItem");
					if($prev.size() > 0){
						$prev.addClass("active");
						$active.removeClass("active");
					}
				}
				
				$in.data("cancelKeyUp", true);
				e.preventDefault();
				return false;
			}else if(e.keyCode === 40){
				//down arrow key
				var $list = giExpTree.getMatchList();
				if($list !== null && $list.size() > 0 && $list.find(".giInMatchItem").size() > 0){
					var $active = $list.find(".giInMatchItem.active");
					var $next = $active.next(".giInMatchItem");
					if($next.size() > 0){
						$next.addClass("active");
						$active.removeClass("active");
					}
				}
			
				$in.data("cancelKeyUp", true);
				e.preventDefault();
				return false;
			}else if(e.keyCode === 8 && text === ""){
				//backspace 
				var $prevNode = $in.prev(".giExpNode");
				var $parent = $in.closest(".giExp");
				var $parentPrevExp = $parent.prev(".giExp");
				
				if($prevNode.size() > 0){
					//has previous node
					if($prevNode.hasClass("literal") || 
						$prevNode.hasClass("unknown") || 
						$prevNode.hasClass("operator") || 
						$prevNode.hasClass("object") ||
						$prevNode.hasClass("null")){
							
						$in.blur();
						$in = giExpTree.getInput();
						
						$in.text($prevNode.text());
						$prevNode.replaceWith($in);
						$in.setCursorAtEnd();
						e.preventDefault();
						
					}else if($prevNode.hasClass("function") || $prevNode.hasClass("grouping")){
						$prevNode.remove();
					}
					
				}else if($parentPrevExp.size() > 0){
					//prev parent exp has no node, but it exist
					//console.log("parent has prev exp");
					$in.blur();
					$in = giExpTree.getInput();
					$parentPrevExp.append($in);
					$in.focus();		
				}
				$in.data("cancelKeyUp", true);
			}else if(e.keyCode === 46 && text === ""){
				//delete key
				var $nextNode = $in.next(".giExpNode");
				var $parent = $in.closest(".giExp");
				var $parentNextExp = $parent.next(".giExp");
				
				
				if($nextNode.size() > 0){
					//has previous node
					if($nextNode.hasClass("literal") || 
						$nextNode.hasClass("unknown") || 
						$nextNode.hasClass("operator") ||
						$nextNode.hasClass("object") || 
						$nextNode.hasClass("null")){
							
						$in.blur();
						$in = giExpTree.getInput();
						
						$in.text($nextNode.text());
						$nextNode.replaceWith($in);
						$in.setCursorAtStart();
						e.preventDefault();
						
					}else if($nextNode.hasClass("function") || $nextNode.hasClass("grouping")){
						$nextNode.remove();
					}
					
				}else if($parentNextExp.size() > 0){
					//next parent exp has no node, but it exist
					//console.log("parent has prev exp");
					$in.blur();
					$in = giExpTree.getInput();
					$parentNextExp.prepend($in);
					$in.focus();		
				}
				$in.data("cancelKeyUp", true);
			}else if(e.keyCode === 37 && cursorPos < 0){
				//left key
				var $prevNode = $in.prev(".giExpNode");
				var $parentNode = $in.closest(".giExpNode");
				var $parent = $in.closest(".giExp");
				var $parentPrevExp = $parent.prev(".giExp");
				
				if(type === null){
					if($prevNode.size() > 0){
						//has previous node
						if($prevNode.hasClass("literal") || 
							$prevNode.hasClass("unknown") || 
							$prevNode.hasClass("operator") ||
							$prevNode.hasClass("object") ||
							$prevNode.hasClass("null")){
								
							$in.blur();
							$in = giExpTree.getInput();
							
							$in.text($prevNode.text());
							$prevNode.replaceWith($in);
							$in.setCursorAtEnd();
							e.preventDefault();
							
						}else if($prevNode.hasClass("function") || $prevNode.hasClass("grouping")){
							$in.blur();
							$in = giExpTree.getInput();
							$prevNode.children(".giExp").last().append($in);
							$in.focus();
						}
						
					}else if($parentPrevExp.size() > 0){
						//prev parent exp has no node, but it exist
						//console.log("parent has prev exp");
						$in.blur();
						$in = giExpTree.getInput();
						$parentPrevExp.append($in);
						$in.focus();		
					}else if($parentNode.size() > 0){
						//console.log("parent exp has no prev exp, has parent node");
						$in.blur();
						$in = giExpTree.getInput();
						$in.insertBefore($parentNode);
						$in.focus();
					}else{
						//console.log("no prev node, parent exp has no prev exp, no parent node");
						$in.blur();
						$in = giExpTree.getInput();
						$parent.prepend($in);
						$in.focus();
					}
				}else{
					var $temp = $("<temp />");
					$temp.insertBefore($in);
					$in.blur();
					$in = giExpTree.getInput();
					$temp.replaceWith($in);
					$in.focus();
				}
				$in.data("cancelKeyUp", true);
			}else if(e.keyCode === 39 && cursorPos > text.length){
				//right key
				var $nextNode = $in.next(".giExpNode");
				var $parentNode = $in.closest(".giExpNode");
				var $parent = $in.closest(".giExp");
				var $parentNextExp = $parent.next(".giExp");
				
				if(type === null){
					if($nextNode.size() > 0){
						//has previous node
						if($nextNode.hasClass("literal") || 
							$nextNode.hasClass("unknown") || 
							$nextNode.hasClass("operator") ||
							$nextNode.hasClass("object") ||
							$nextNode.hasClass("null")){
								
							$in.blur();
							$in = giExpTree.getInput();
							
							$in.text($nextNode.text());
							$nextNode.replaceWith($in);
							$in.setCursorAtStart();
							e.preventDefault();
							
						}else if($nextNode.hasClass("function") || $nextNode.hasClass("grouping")){
							$in.blur();
							$in = giExpTree.getInput();
							$nextNode.children(".giExp").first().prepend($in);
							$in.focus();
						}
						
					}else if($parentNextExp.size() > 0){
						//prev parent exp has no node, but it exist
						//console.log("parent has prev exp");
						$in.blur();
						$in = giExpTree.getInput();
						$parentNextExp.prepend($in);
						$in.focus();		
					}else if($parentNode.size() > 0){
						//console.log("parent exp has no prev exp, has parent node");
						$in.blur();
						$in = giExpTree.getInput();
						$in.insertAfter($parentNode);
						$in.focus();
					}else{
						//console.log("no prev node, parent exp has no prev exp, no parent node");
						$in.blur();
						$in = giExpTree.getInput();
						$parent.append($in);
						$in.focus();
					}
				}else{
					var $temp = $("<temp />");
					$temp.insertAfter($in);
					$in.blur();
					$in = giExpTree.getInput();
					$temp.replaceWith($in);
					$in.focus();
				}
				$in.data("cancelKeyUp", true);
				
			}else if(type === "number" && e.keyCode === 32 && cursorPos === text.length){
				var $temp = $("<temp />");
				$temp.insertAfter($in);
				$in.blur();
				$in = giExpTree.getInput();
				$temp.replaceWith($in);
				$in.focus();
				
				e.preventDefault();
				return false;
			}else if(e.keyCode === 222 && cursorPos === text.length && giExpTree.getTextNodeType(text + "'") === "string"){
				var $temp = $("<temp />");
				$temp.insertAfter($in);
				$in.text($in.text() + "'");
				$in.blur();
				$in = giExpTree.getInput();
				$temp.replaceWith($in);
				$in.focus();
				
				e.preventDefault();
				return false;
			}
		
			
		});
		
		giExpTree.htmlContext.on("keyup", ".giExpIn", function(e){
			var $in = $(e.currentTarget);
			if($in.data("cancelKeyUp")){
				e.preventDefault();
				return false;
			}
			console.log("keyup");
			var text = $in.text();
			
			//set type for code recognition
			var type = giExpTree.getTextNodeType(text);
			$in.attr("data-type", type);
			
			//code suggestion
			var matches = [];
			
			if(type !== null){
				matches = giExpTree.getMatches(text);
			}
			
			//auto-complete (with some optimization)
			if(matches.length === 1 && matches[0].displayName === text){
				var cursorPos = $in.getCursorPosition();
				if(cursorPos === text.length){
					var $temp = $("<temp/>");
					$temp.insertAfter($in);
					$in.text(text);
					$in.blur();
					$in = giExpTree.getInput();
					var $newNode = $temp.prev(".giExpNode");
					$temp.remove();
					if($newNode.hasClass("function") || $newNode.hasClass("grouping")){
						$newNode.find(".giExp").first().append($in);
						$in.focus();
					}else{
						$in.insertAfter($newNode);
						$in.focus();
					}
				}
				
			}
			//end auto-complete		
			
			giExpTree.htmlContext.find(".giInMatchList").remove();
			
			var $matchList = $("<ul>");
			$matchList.addClass("giInMatchList");
			
			for(var i=0; i<matches.length;i++){
				var $matchItem = $("<li>");
				$matchItem.text(matches[i]["displayName"]);
				$matchItem.attr("title", matches[i]["description"]);
				$matchItem.addClass("giInMatchItem");
				if(i===0){ $matchItem.addClass("active"); }
				$matchList.append($matchItem);
			}
			
			if(matches.length === 0 && type === "unknown"){
				$matchList.append($("<li>No match found.<li>"));
			}
			
			giExpTree.htmlContext.append($matchList);
			
			var bodyRect = $("body")[0].getBoundingClientRect();
			var ctxRect = giExpTree.htmlContext[0].getBoundingClientRect();
			var inRect = $in[0].getBoundingClientRect();
			
			$matchList.css
				({ "top": inRect.top - ctxRect.top + $in.height() + 1, 
				"left": inRect.left - ctxRect.left,
				"z-index": parseInt(giExpTree.htmlContext.css("z-index")) + 999 });
			//end code suggestion
			
			
		});
		/* [end] expression input behavior */
		
		/* [start] code suggestion behavior */
		giExpTree.htmlContext.on("mouseover", ".giInMatchItem", function(e){
			var $item = $(e.currentTarget);
			$item.closest("ul").find("li").removeClass("active");
			$item.addClass("active");
		});
		
		giExpTree.htmlContext.on("mousedown", ".giInMatchItem", function(e){
			var $in = giExpTree.getInput();
			$in.data("cancelBlur", true);
		});
		
		giExpTree.htmlContext.on("click", ".giInMatchItem", function(e){
			var $item = $(e.currentTarget);
			var $in = giExpTree.getInput();
			var $newNode = giExpTree.textToHtml($item.text());
			
			$item.closest(".giInMatchList").remove();
			
			$in.replaceWith($newNode);
			$in = giExpTree.getInput();
			if($newNode.hasClass("function") || $newNode.hasClass("grouping")){
				$newNode.find(".giExp").first().append($in);
			}else{
				$in.insertAfter($newNode);
			}
			$in.focus();
		});
		/* [start] code suggestion behavior */
	};
	
	this.objects = params.objects === undefined ? [] : params.objects;
	
	this.operators = ['+', '-', '/', '*', '%', '=', '>', '>=', '<', '<=', '<>', 'or', 'and', 'is', 'not'];
	this.groupings = ['('];
	
	this.functions = 
		[{name:'if', paramCount:3},
		{name:'ifnull', paramCount:2},
		
		{name:'sum', paramCount:1},
		{name:'count', paramCount:1},
		{name:'average', paramCount:1},
		
		{name:'date', paramCount:1},	
		{name:'now', paramCount:1},
		{name:'date_format', paramCount:2},
		
		{name:'trim', paramCount:1},
		{name:'concat', paramCount:2},
		{name:'round', paramCount:2}
		];
	
	this.getInput = function(createNew){
		var $expIn = giExpTree.htmlContext.find(".giExpIn");
		
		$expIn = $expIn.size() === 0 || createNew ? $("<div class='giExpIn' contenteditable spellcheck='false' ></div>") : $expIn;
		
		return $expIn;
	};
	
	this.getMatchList = function(){
		var $list = giExpTree.htmlContext.find(".giInMatchList");
		return $list.size() > 0 ? $list : null; 
	}
	
	this.getTemp = function(){
		return giExpTree.htmlContext.find("temp");
	};
	
	this.isString = function(text){ 
		return text.length > 1 && text[0] == "'" && text[text.length-1] == "'";
	};
	
	this.isNumber = function(text){
		return text !== undefined &&
			text !== "" &&
			text !== null &&
			!isNaN(text);
	};
	
	this.isOperator = function(text){
		return giExpTree.operators.indexOf(text) > -1;
	};
	
	this.isGrouping = function(text){
		return giExpTree.groupings.indexOf(text) > -1;
	};
	
	this.isNull = function(text){
		return text === "null";
	};
	
	this.isFunction = function(text, outExtra){
		for(var i=0; i<giExpTree.functions.length; i++){
			if(giExpTree.functions[i].name === text){
				if(outExtra !== undefined){
					outExtra["paramCount"] = giExpTree.functions[i].paramCount;
				}
				return true;
			}
		}
		return false;
	};
	
	this.isObject = function(text, outExtra){
		for(var i=0; i<giExpTree.objects.length; i++){
			if(giExpTree.objects[i].displayText === text){
				if(outExtra !== undefined){
					outExtra["namespaces"] = giExpTree.objects[i].namespaces;
					outExtra["objectName"] = giExpTree.objects[i].objectName;
				}
				return true;
			}
		}
		return false;
	};
	
	this.getMatches = function(text){
		var matches = [];
		if(text !== undefined && text !== null && text.trim() !== ""){
			//operators
			for(var i=0; i<giExpTree.operators.length; i++){
				var op = giExpTree.operators[i];
				if(op.indexOf(text) > -1){
					matches.push({ displayName: op, type: "operator", description:"" });
				}
			}
			//groupings
			for(var i=0; i<giExpTree.groupings.length; i++){
				var grps = giExpTree.groupings[i];
				if(grps.indexOf(text) > -1){
					matches.push({ displayName: grps, type: "grouping", description:"" });
				}
			}
			
			//functions
			for(var i=0; i<giExpTree.functions.length; i++){
				var fn = giExpTree.functions[i];
				if(fn.name.indexOf(text) > -1){
					matches.push({ displayName: fn.name, type: "function", description:"" });
				}
			}
			
			//objects
			for(var i=0; i<giExpTree.objects.length; i++){
				var obj = giExpTree.objects[i];
				if(obj.displayText.indexOf(text) > -1){
					matches.push({ displayName: obj.displayText, type: "object", description:"" });
				}
			}
			
			//null
			if(("null").indexOf(text) > -1){
				matches.push({ displayName: "null", type: "null", description:"" });
			}
		}
		return matches;
	};
	
	this.getTextNodeType = function(text){
		var type = "unknown";
		if(text === undefined || text === null || text.trim() === ""){ type = null; }
		if(giExpTree.isString(text)){ type = "string"; }
		if(giExpTree.isNumber(text)){ type = "number"; }
		if(giExpTree.isOperator(text)){ type = "operator"; }
		if(giExpTree.isGrouping(text)){ type = "grouping"; }
		if(giExpTree.isFunction(text)){ type = "function";  }
		if(giExpTree.isObject(text)){ type = "object";  }
		if(giExpTree.isNull(text)){ type = "null";  }
		return type;
	}
	
	this.htmlToJson = function($htmlExp){
		var exp = [];
		var $htmlNodes = $htmlExp.children(".giExpNode");
		
		for(var i=0; i<$htmlNodes.size(); i++){
			var $hNode = $htmlNodes.eq(i);
			var node = {};
			
			
			if($hNode.data("giExpNodeType") === "operator"){
				node.type = "operator";
				node.displayText = $hNode.text();
			}else if($hNode.data("giExpNodeType") === "grouping"){
				node.type = "grouping";
				node.content = giExpTree.htmlToJson($hNode.children(".giExp"));
			}else if($hNode.data("giExpNodeType") === "string"){
				node.type = "string";
				node.value = $hNode.text();
			}else if($hNode.data("giExpNodeType") === "number"){
				node.type = "number";
				node.value = $hNode.text();
			}else if($hNode.data("giExpNodeType") === "function"){
				node.type = "function";
				node.displayText = $hNode.children(".keyword").text();
				node.parameters = [];
				var $paramExps = $hNode.children(".giExp");
				for(var j=0; j<$paramExps.size(); j++){
					node.parameters.push(giExpTree.htmlToJson($paramExps.eq(j)));
				}
			}else if($hNode.data("giExpNodeType") === "object"){
				node.type = "object";
				node.displayText = $hNode.text();
				node.namespaces = $hNode.data("giExpNamespaces");
				node.objectName = $hNode.data("giExpObjectName");
			}else if($hNode.data("giExpNodeType") === "null"){
				node.type = "null";
				node.displayText = $hNode.text();
			}else if($hNode.data("giExpNodeType") === "unknown"){
				node.type = "number";
				node.value = $hNode.text();
			}
			
			exp.push(node);
		}
		
		return exp;
	};

	this.jsonToHtml = function(expTree){
		
		var $html = $("<div class='giExp'></div>");
		for(var i=0; i<expTree.length; i++){
			var node = expTree[i];
			var $node = $("<div class='giExpNode'></div>");
			$html.append($node);
			if(node["type"] === "operator"){
				$node.addClass("operator");
				$node.text(node["displayText"]);
				$node.data("giExpNodeType", "operator");
				$node.attr("data-giExpNodeType", "operator");
			}else if(node.type === "grouping"){
				$node.addClass("grouping");
				$node.data("giExpNodeType", "grouping");
				$node.attr("data-giExpNodeType", "grouping");
				$node.append("(").append(this.jsonToHtml(node.content)).append(")");
			}else if(node.type === "string"){
				$node.addClass("string literal");
				$node.append(node["value"]);
				$node.data("giExpNodeType", "string");
				$node.attr("data-giExpNodeType", "string");
			}else if(node.type === "number"){
				$node.addClass("number literal");
				$node.html(node["value"]);
				$node.data("giExpNodeType", "number");
				$node.attr("data-giExpNodeType", "number");
			}else if(node.type === "function"){
				$node.addClass("function");
				$node.data("giExpNodeType", "function");
				$node.attr("data-giExpNodeType", "function");
				var paramCount = node.parameters.length
				$node.append("<span class='keyword'>" + node["displayText"] + "</span>(");
				for(var j=0; j<paramCount; j++){
					$node.append(this.jsonToHtml(node.parameters[j]));
					if(paramCount-1 != j){
						$node.append(",");
					}
				}
				$node.append(")");
			}else if(node.type === "object"){
				$node.addClass("object");
				$node.data("giExpNodeType", "object");
				$node.data("giExpNamespaces", node.namespaces);
				$node.data("giExpObjectName", node.objectName);
				$node.attr("data-giExpNodeType", "object");
				$node.html(node.displayText);
			}else if(node.type === "null"){
				$node.addClass("null");
				$node.data("giExpNodeType", "null");
				$node.attr("data-giExpNodeType", "null");
				$node.html(node.displayText);
			}else if(node.type === "unknown"){
				$node.addClass("unknown");
				$node.attr("title", "Unrecognized code.");
				$node.data("giExpNodeType", "unknown");
				$node.attr("data-giExpNodeType", "unknown");
				$node.text(node.value);
			}
		}
		return $html;
	};

	this.textToJson = function(text){
		var outExtra = {};
		if(giExpTree.isString(text)){
			return { type:"string", value:text };
		}else if(giExpTree.isNumber(text)){
			return { type:"number", value:text };
		}else if(giExpTree.isOperator(text)){
			return { type:"operator", displayText:text };
		}else if(giExpTree.isGrouping(text)){
			return { type:"grouping", content: [] };
		}else if(giExpTree.isFunction(text, outExtra)){
			var params = [];
			for(i=0;i<outExtra.paramCount;i++){
				params.push([]);
			}
			return { type:"function", displayText:text, parameters: params };	
		}else if(giExpTree.isObject(text, outExtra)){
			return { type:"object", displayText:text, namespaces: outExtra.namespaces, objectName: outExtra.objectName };
		}else if(giExpTree.isNull(text)){
			return { type:"null", displayText:text };
		}else{
			return { type:"unknown", value:text };
		}
	};

	this.textToHtml = function(text){
		//returns a single html node from a string.
		//console.log(giExpTree.textToJson(text));
		return text.trim() === "" ? undefined: giExpTree.jsonToHtml([giExpTree.textToJson(text)]).children();
	};

};

