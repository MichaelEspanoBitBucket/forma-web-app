(function($){

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

var giChartComponents = new (function(){
	
	this.generateFillOutFormButton = function(params){
		var buttonId = "gi-temp-"+(new Date().getTime());
		
		var $button = $("<button id='"+buttonId+"' class='button' title='New Request'><span class='fa fa-plus'></span></button>");
		
		var chartForms = params["chartForms"];
	
		var menuItems = {};
		for(var j=0; j<chartForms.length; j++){
			for(var i=0; i<chartForms[j].length; i++){
			
				menuItems[chartForms[j][i]["id"]] = {"name": chartForms[j][i]["name"]};
			}
		}
		
		//menuItems = { "1": {"name": "Under maintenance"}};
		
		$.contextMenu({
			selector: "#"+buttonId, 
			trigger: 'left',
			callback: function(key, options) {
				 window.open("/user_view/workspace?view_type=request&formID="+key+"&requestID=0", '_blank').focus();
			},
			items: menuItems
		});
		
		
		return $button;
	};
	
	
})();

$.fn.giGaugeChart = function(methodName, params){

	var toUpdateData = false;
	var toCheckUpdate = false;
	var createCheckUpdateTimer = false;
	
	var cancelRender = false;
	var affected = [];
	var initialized = false;

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": "expand", "Height": "auto" };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giGaugeChart")){
				
				initialized = true;
				toCheckUpdate = true;
				createCheckUpdateTimer = true;
				
				$objEl.addClass("giObject giChart giGaugeChart");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giGaugeChart",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Object Type": "Gauge Chart",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "auto",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "#000",
					"Corner Radius": 8,
					"Padding": 8,
					"Margin": 4,
					"Background Color": "#fff",
					"Header Background": "#66CCFF",
					"Header Text Color": "#fff",
					"Row Background": "#fff",
					"Row Alternating Background": "#F0FAFF",
					"New Record Button": "show",
					"Auto Update Data": true,
					"Data Update Interval": 300,
					"Query": [],
					"Target Value": 100,
					"Low Threshold Percentage": 50,
					"High Threshold Percentage": 80,
					"Low Color": "#ff0000",
					"Medium Color": "#ffbb00",
					"High Color": "#00ff55",
					"Data URL": "",
					"On Data Updated": null
				});
				
				$objEl.data("giSessionData", {
					"Chart Data": undefined
				});
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};

		this["get Query"] = function(){ return props["Query"]; };
		this["set Query"] = function(val){ 
			if(true){
				$objEl.data("giSessionData")["Chart Data"] = undefined;
				props["Query"] = val; 
				toUpdateData = true;
				
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Target Value"] = function(){ return props["Target Value"]; };
		this["set Target Value"] = function(val){ 
			
			if(parseInt(val) > 0){
				props["Target Value"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Low Threshold Percentage"] = function(){ return props["Low Threshold Percentage"]; };
		this["set Low Threshold Percentage"] = function(val){ 
			if(parseInt(val) > 0 && parseInt(val) < parseInt(props["High Threshold Percentage"])){
				props["Low Threshold Percentage"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get High Threshold Percentage"] = function(){ return props["High Threshold Percentage"]; };
		this["set High Threshold Percentage"] = function(val){ 
			if(parseInt(val) > 0 && 
				parseInt(val) < 100 && 
				parseInt(val) > parseInt(props["Low Threshold Percentage"]))
			{
				props["High Threshold Percentage"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
			
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? val : parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Padding"] = function(){ return props["Padding"]; }
		this["set Padding"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Padding"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Margin"] = function(){ return props["Margin"]; }
		this["set Margin"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Margin"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Background Color"] = function(){ return props["Background Color"]; }
		this["set Background Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Background Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Header Background"] = function(){ return props["Header Background"]; }
		this["set Header Background"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Header Background"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Header Text Color"] = function(){ return props["Header Text Color"]; }
		this["set Header Text Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Header Text Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Row Background"] = function(){ return props["Row Background"]; }
		this["set Row Background"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Row Background"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Row Alternating Background"] = function(){ return props["Row Alternating Background"]; }
		this["set Row Alternating Background"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Row Alternating Background"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
		this["set Corner Radius"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Corner Radius"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};			
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Low Color"] = function(){ return props["Low Color"]; }
		this["set Low Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Low Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Medium Color"] = function(){ return props["Medium Color"]; }
		this["set Medium Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Medium Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get High Color"] = function(){ return props["High Color"]; }
		this["set High Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["High Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Data URL"] = function(){ return props["Data URL"]; };
		this["set Data URL"] = function(val){ 
			if(val !== undefined && val !== null){
				toUpdateData = true;
				props["Data URL"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get New Record Button"] = function(){ return props["New Record Button"]; };
		this["set New Record Button"] = function(val){ 
			if(val === "show" || val === "hide"){
				props["New Record Button"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		
		this["get Auto Update Data"] = function(){ return props["Auto Update Data"]; }
		this["set Auto Update Data"] = function(val){
			var bools = { "true": true, "false": false };
			if(bools.hasOwnProperty(val.toString())){
				createCheckUpdateTimer = true;
				props["Auto Update Data"] = bools[val.toString()]; 
			}else{
				return "Invalid value.";
			}
		};	
		
		
		this["get Data Update Interval"] = function(){ return props["Data Update Interval"]; }
		this["set Data Update Interval"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Data Update Interval"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			//clean updater
			var updaterInterval = $objEl.data("giDataUpdater");
			if(updaterInterval !== undefined){
				clearInterval(updaterInterval);
			}
			$objEl.remove();
		};
		
		this["get On Data Updated"] = function(){ return props["On Data Updated"]; }
		this["set On Data Updated"] = function(func){
			var getType = {};
			if(func === null || getType.toString.call(func) === '[object Function]'){
				props["On Data Updated"] = func; 
			}else{
				return "Invalid value.";
			}
		};	
		
		var context = this;
		this.createCheckUpdateTimer = function(){
			
			var updaterInterval = $objEl.data("giDataUpdater");
			
			if(props["Auto Update Data"] && (updaterInterval === undefined || updaterInterval === null)){
				updaterInterval = setInterval(context.checkUpdate, 5000);
				$objEl.data("giDataUpdater", updaterInterval);
			}else if(!props["Auto Update Data"]){
				clearInterval(updaterInterval);
				$objEl.data("giDataUpdater", null);
			}
			

		};
		
		this.checkUpdate = function(){
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			var updateInterval = $objEl.data("giProperties")["Data Update Interval"];
			
			//check if no data or if data is outdated
			if(chartData === undefined || chartData === null ||
				((new Date() - new Date(chartData.timeGenerated * 1000)) / 1000 > updateInterval)){
				context.updateData();
			}
		};
		
		this.updateData = function(){
			var updateXhr = $objEl.data("giSessionData")["Update XHR"];
			
			if(toUpdateData && updateXhr !== undefined && updateXhr !== null){
				updateXhr.abort();
				updateXhr = null;
			}
			
			if(updateXhr === undefined || updateXhr === null || updateXhr.readyState === 4) {
				
				
				var newUpdateXhr = $.ajax({
					type: "POST",
					url: props["Data URL"] === ""? "/ajax/gi-get-chart-data" : props["Data URL"],
					dataType: "json",
					data: {query: props["Query"]
					},
					success: function(result){
						$objEl.data("giSessionData")["Chart Data"] = result;
						
						if(props["On Data Updated"] !== null){		
							var dataTable = $objEl.data("giSessionData")["Chart Data"]["dataTable"][0];

							var query1 = props["Data URL"] == "" ? props["Query"][0] : $objEl.data("giSessionData")["Chart Data"]["giqs"][0];
							
							//get categories
							var cats = [];
							for(var j=0; j<query1["groupings"].length; j++){
								cats.push(query1["groupings"][j]["alias"]);
							}
							for(var j=0; j<query1["optionalGroupings"].length; j++){
								cats.push(query1["optionalGroupings"][j]["alias"]);
							}
							
							//get row where all categories are null (grand total)
							var grandTotalRow = null;
							for(var i=(dataTable.length-1); i>=0; i--){
								var row = dataTable[i];
								var allNull = true;
								for(var j=0; j<cats.length; j++){
									if(row[cats[j]] !== null){
										allNull = false;
										break;
									}
								}
								if(allNull){
									grandTotalRow = row;
									break;
								}
							}
					
							props["On Data Updated"]({ "Grand Total": grandTotalRow });
						}
						
						$objEl[props["Identifier Name"]]("render");
					}
				});	
				
				$objEl.data("giSessionData")["Update XHR"] = newUpdateXhr;
			}
			
			
		};
		
		this.render = function(){
		
			var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", props["Width"] == "expand" ? "auto": props["Width"]);
			$objEl.css("height", props["Height"]);
			$objEl.css("min-height", props["Height"] === "auto" ? 20: "");
			$objEl.css("position", props["Positioning"] === "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);
			if(props["Positioning"] === "auto"){ $objEl.css("margin", props["Margin"]); }
			else{ $objEl.css("margin", 0); }
			
			var $border = $objEl.children("div.border");
			if($border.size() === 0){
				$border = $("<div>");
				$border.appendTo($objEl);
			}
			
			$border.html("");
			$border.addClass("border");
			$border.css("border-style", "solid");
			$border.css("background-color", props["Background Color"]);
			$border.css("border-width", props["Border Thickness"]);
			$border.css("border-color", props["Border Color"]);
			$border.css("padding", props["Padding"]);
			$border.css("border-radius", props["Corner Radius"]);
			$border.css("position", props["Height"] === "auto" ? "static" : "absolute");
			$border.css({"top": 0, "left": 0, "right": 0, "bottom": 0 });
			
			var $toolBar = $border.children(".toolBar"); 
			if($toolBar.size() == 0){
				$toolBar = $("<div>");
				$toolBar.addClass("toolBar giNoPrint");
				$border.append($toolBar);
			}
			$toolBar.html("");
			//$toolBar.css("margin-bottom", "8px");
			
			//console.log("rendering...");
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			
			if(chartData !== undefined && chartData !== null && chartData["dataTable"][0].length > 0){
				//console.log("has chart data");
				//get slicers info 
				var query1 = props["Data URL"] == "" ? props["Query"][0] : chartData["giqs"][0];
					
				var cats = 
					$.Enumerable
					.From(query1["groupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var optCats = 
					$.Enumerable
					.From(query1["optionalGroupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				
				var allDrilledFields = cats.concat([]);
				
				var allCats = cats.concat([]);
				
				var aggFields = 
					$.Enumerable
					.From(query1["aggregates"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				
				var dataTable = chartData["dataTable"][0];

				//manual check of updated data
				if(!props["Auto Update Data"]){
					var $checkUpdate = $("<button><span class='fa fa-refresh'></span>");
					$checkUpdate.attr("title", "Check for updated data.");
					$checkUpdate.addClass("button");
					$toolBar.append($checkUpdate);
					$checkUpdate.on("click", function(e){
						context.updateData();
					});
				}
				
				if(dataTable.length == 0){
					return;
				}
				
				//fill out form button
				if(props["New Record Button"] === "show" && query1["entities"].length > 0){
					var chartForms = [];
					for(var qei=0; qei<query1["entities"].length; qei++){
						var entity = query1["entities"][qei];
						//console.log(entity.formName + " " + entity.formId);
						if(entity["formId"] !== undefined && !isNaN(entity["formId"])){
							chartForms.push({ "name": entity.formName, "id":entity.formId});
						}
					}
					
					var $fillOutFormBtn = giChartComponents.generateFillOutFormButton({
						"chartForms": [chartForms]
					});
				
					$toolBar.append($fillOutFormBtn);
				}
				
				var $dividerTable = $border.find(".dividerTable");
				if($dividerTable.size() === 0){
					$dividerTable = $("<table>");
					$border.append($dividerTable);
				}				
				$dividerTable.addClass("dividerTable");
				$dividerTable.removeAttr("style");
				$dividerTable.css({ "width": "100%" });
				$dividerTable.html("");
				
				var $chartSection = null;
				var $legendsSection = null;
				
				//legends section
				var $row1 = $("<tr>"); 
				$dividerTable.append($row1);
				
				var $cell1 = $("<td>"); 
				$row1.append($cell1); 
				$cell1.addClass(props["Legends Position"] === "top" ? 
					"legendsSection": "chartSection");
				
				var $row2 = $("<tr>"); 
				$dividerTable.append($row2);
				
				var $cell2 = $("<td>"); 
				$row2.append($cell2); 
				
				$cell2.addClass(props["Legends Position"] === "top" ? 
					"chartSection": "legendsSection");
				
				$legendsSection = props["Legends Position"] === "top" ? $cell1 : $cell2;
				$chartSection = props["Legends Position"] === "top" ? $cell2 : $cell1;
				
				$legendsSection.css({ "vertical-align": "middle" });
				
				//get legends box, create if doesnt exist
				var $legends = $legendsSection.find(".legends");
				if($legends.size() === 0){
					$legends = $("<div>");
					$legendsSection.append($legends);
				}
				$legends.removeAttr("style");
				$legends.addClass("legends");
				$legends.css({ "line-height": "1.2em" });
				$legends.html("");
				
				//display legends
				var legendColors = [];
				var legendWidth = 0;
				
				var statusInfo = [
					{ colorPropName: "Low Color", label: "Low (&le;" + props["Low Threshold Percentage"] + "%)" }, 
					{ colorPropName: "Medium Color", label: "Medium (&gt;" + props["Low Threshold Percentage"] + "% and &lt;" + props["High Threshold Percentage"] + "%)" },
					{ colorPropName: "High Color", label: "High (&ge;" + props["High Threshold Percentage"] + "%)" }
				];
				
				for(var ai=0; ai<statusInfo.length; ai++){
					
					var $legend = $("<div>");
					$legend.addClass("legend");
					$legend.css({ "display": "inline-block", "margin-right": 8 });
					
					var $legendColor = $("<span>");
					$legendColor.css({ "display": "inline-block", 
						"width": "1em", "height": "1em",
						"background-color": props[statusInfo[ai].colorPropName],
						"margin-right": 2
					});
					$legend.append($legendColor);
					$legend.append(statusInfo[ai].label);
						
					$legends.append($legend);
					legendWidth = legendWidth < $legend.outerWidth(true) ? $legend.outerWidth(true) : legendWidth;
				}
				
				//$legends.children(".legend").css("width", legendWidth);
				
				//adjust sections' dimension
				$chartSection.append("<span class='cellFiller'>");
				$legendsSection.css("height", 1);
				$legendsSection.css("height", $legendsSection.height()); //get/set the computed
									
				if(props["Height"] === "auto"){
					$chartSection.css("height", "200");
				}else{
					var objElRect = $objEl[0].getBoundingClientRect();
					var dividerTableRect = $dividerTable[0].getBoundingClientRect();
					var dividerTableHeight = props["Height"] - (dividerTableRect.top - objElRect.top) - props["Padding"];
					$dividerTable.css("height", dividerTableHeight);
				}
			
				$chartSection.find(".cellFiller").remove();
				
				//add some space between sections
				if(props["Legends Position"] === "top"){
					$legendsSection.css("padding-bottom", 8);
				}else if(props["Legends Position"] === "bottom"){
					$legendsSection.css("padding-top", 8);
				}else if(props["Legends Position"] === "left"){
					$legendsSection.css("padding-right", 8);
				}else if(props["Legends Position"] === "right"){
					$legendsSection.css("padding-left", 8);
				}
				
				//display chart
				var row = dataTable[0];
				var aggField = aggFields[0];
				
				//determine the status
				var targetVpp = parseFloat(props["Target Value"]) / 100;
						
				var lowThresVal = parseFloat(targetVpp) * parseFloat(props["Low Threshold Percentage"]);
				var highThresVal = parseFloat(targetVpp) * parseFloat(props["High Threshold Percentage"]);
				
				var baseValStat = null;
				if(parseFloat(row[aggField]) <= lowThresVal){
					baseValStat = "Low";
				}else if(parseFloat(row[aggField]) >= highThresVal){
					baseValStat = "High";
				}else{
					baseValStat = "Medium";
				}
				
				//create a relative or absolute positioned element to wrap canvas and gauge info
				var chartSize = { Width: (props["Width"] != "auto" ? $chartSection.width(): 200), Height: (props["Height"] != "auto" ? $chartSection.height(): 200) };
				
				var $chartSectInner = $("<div>");
				$chartSectInner.css({ 
					"position": "relative",  
					"width": chartSize.Width + "px",
					"height": chartSize.Height + "px"			
				});
				
				$chartSection.append($chartSectInner);
				
				//we need an id so FlexGauge knows where to put the chart...
				var chartSectInnerId = "gi" + ((new Date()).getTime()) + "" + parseInt(Math.random()*1000);
				$chartSectInner.attr("id", chartSectInnerId);
				
				//display gauge itself
				
				//compute gauge arc thickness and size based on shortest edge of canvas
				var canvasShortSide = chartSize.Width < chartSize.Height ? chartSize.Width : chartSize.Height;
				var gaugeStrokeSize = canvasShortSide * 0.15;
				var gaugeRadius = canvasShortSide * 0.425;
				
				var gauge = new FlexGauge({
					appendTo: '#' + chartSectInnerId,
					animateEasing: false,
					arcFillInt: (props["Target Value"] < row[aggField] ? props["Target Value"] : row[aggField]),
					arcFillTotal: props["Target Value"],
					colorArcFg: props[baseValStat + " Color"],
					arcBgColorLight: 200,
					arcBgColorSat: 40,
					arcStrokeFg: gaugeStrokeSize,
					arcStrokeBg: gaugeStrokeSize,
					arcSize: gaugeRadius,
					elementWidth: chartSize.Width,
					elementHeight: chartSize.Height
				});
				
				//display gauge result info
				var $gaugeInfoWrapper = $("<div>"  + Math.roundOff((100 / props["Target Value"] * row[aggField]), 0.1).toLocaleString() + "%" + "</div>");
				$gaugeInfoWrapper.css({ 
					"position": "absolute",  
					"width": chartSize.Width + "px",
					"vertical-align": "middle",
					"text-align": "center",
					"font-size": "21px",
					"top": (chartSize.Height / 2)
				});
				
				var $targetLabel = $("<span>of "+ props["Target Value"].toLocaleString() +"</span>")
				$gaugeInfoWrapper.append("<br/>");
				$gaugeInfoWrapper.append($targetLabel);
				
				$chartSectInner.append($gaugeInfoWrapper);
				
			}else{
				$border.html("");
			}
			
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined && methodName !== "render"){
		methodResult = objCore[methodName](params);
	}
	
	if(toUpdateData){
		objCore.updateData();
	}
	
	if(toCheckUpdate){
		objCore.checkUpdate();
	}

	if(createCheckUpdateTimer){
		objCore.createCheckUpdateTimer();
	}
	
	if(!cancelRender){ 
		objCore.render(); 
	}
	
	return { "methodResult": methodResult, "affected": affected };
};




})(jQuery);