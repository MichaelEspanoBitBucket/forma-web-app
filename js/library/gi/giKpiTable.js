(function($){

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

var giChartComponents = new (function(){
	
	this.generateFillOutFormButton = function(params){
		var buttonId = "gi-temp-"+(new Date().getTime());
		
		var $button = $("<button id='"+buttonId+"' class='button' title='New Request'><span class='fa fa-plus'></span></button>");
		
		var chartForms = params["chartForms"];
	
		var menuItems = {};
		for(var j=0; j<chartForms.length; j++){
			for(var i=0; i<chartForms[j].length; i++){
			
				menuItems[chartForms[j][i]["id"]] = {"name": chartForms[j][i]["name"]};
			}
		}
		
		//menuItems = { "1": {"name": "Under maintenance"}};
		
		$.contextMenu({
			selector: "#"+buttonId, 
			trigger: 'left',
			callback: function(key, options) {
				 window.open("/user_view/workspace?view_type=request&formID="+key+"&requestID=0", '_blank').focus();
			},
			items: menuItems
		});
		
		
		return $button;
	};
	
	
})();

$.fn.giKpiTable = function(methodName, params){

	var toUpdateData = false;
	var toCheckUpdate = false;
	var createCheckUpdateTimer = false;
	
	var cancelRender = false;
	var affected = [];
	var initialized = false;

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": "expand", "Height": "auto" };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giKpiTable")){
				
				initialized = true;
				toCheckUpdate = true;
				createCheckUpdateTimer = true;
				
				$objEl.addClass("giObject giChart giKpiTable");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giKpiTable",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Object Type": "KPI Table",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "auto",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "#000",
					"Corner Radius": 8,
					"Padding": 8,
					"Margin": 4,
					"Background Color": "#fff",
					"Header Background": "#66CCFF",
					"Header Text Color": "#fff",
					"Row Background": "#fff",
					"Row Alternating Background": "#F0FAFF",
					"Super Aggregates": "show",
					"New Record Button": "show",
					"Auto Update Data": true,
					"Data Update Interval": 300,
					"Query": [],
					"Target Value": 100,
					"Low Threshold Percentage": 50,
					"High Threshold Percentage": 80,
					"Low Color": "#ff0000",
					"Medium Color": "#ffbb00",
					"High Color": "#00ff55",
					"Data URL": "",
					"On Data Updated": null
				});
				
				$objEl.data("giSessionData", {
					"Chart Data": undefined
				});
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};

		this["get Query"] = function(){ return props["Query"]; };
		this["set Query"] = function(val){ 
			if(true){
				$objEl.data("giSessionData")["Chart Data"] = undefined;
				props["Query"] = val; 
				toUpdateData = true;
				
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Target Value"] = function(){ return props["Target Value"]; };
		this["set Target Value"] = function(val){ 
			
			if(parseInt(val) > 0){
				props["Target Value"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Low Threshold Percentage"] = function(){ return props["Low Threshold Percentage"]; };
		this["set Low Threshold Percentage"] = function(val){ 
			if(parseInt(val) > 0 && parseInt(val) < parseInt(props["High Threshold Percentage"])){
				props["Low Threshold Percentage"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get High Threshold Percentage"] = function(){ return props["High Threshold Percentage"]; };
		this["set High Threshold Percentage"] = function(val){ 
			if(parseInt(val) > 0 && 
				parseInt(val) < 100 && 
				parseInt(val) > parseInt(props["Low Threshold Percentage"]))
			{
				props["High Threshold Percentage"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
			
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? val : parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Padding"] = function(){ return props["Padding"]; }
		this["set Padding"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Padding"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Margin"] = function(){ return props["Margin"]; }
		this["set Margin"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Margin"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Background Color"] = function(){ return props["Background Color"]; }
		this["set Background Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Background Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Header Background"] = function(){ return props["Header Background"]; }
		this["set Header Background"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Header Background"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Header Text Color"] = function(){ return props["Header Text Color"]; }
		this["set Header Text Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Header Text Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Row Background"] = function(){ return props["Row Background"]; }
		this["set Row Background"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Row Background"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Row Alternating Background"] = function(){ return props["Row Alternating Background"]; }
		this["set Row Alternating Background"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Row Alternating Background"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
		this["set Corner Radius"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Corner Radius"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};			
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Low Color"] = function(){ return props["Low Color"]; }
		this["set Low Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Low Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Medium Color"] = function(){ return props["Medium Color"]; }
		this["set Medium Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Medium Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get High Color"] = function(){ return props["High Color"]; }
		this["set High Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["High Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Data URL"] = function(){ return props["Data URL"]; };
		this["set Data URL"] = function(val){ 
			if(val !== undefined && val !== null){
				toUpdateData = true;
				props["Data URL"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get New Record Button"] = function(){ return props["New Record Button"]; };
		this["set New Record Button"] = function(val){ 
			if(val === "show" || val === "hide"){
				props["New Record Button"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Super Aggregates"] = function(){ return props["Super Aggregates"]; };
		this["set Super Aggregates"] = function(val){ 
			if(val === "show" || val === "hide"){
				props["Super Aggregates"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Auto Update Data"] = function(){ return props["Auto Update Data"]; }
		this["set Auto Update Data"] = function(val){
			var bools = { "true": true, "false": false };
			if(bools.hasOwnProperty(val.toString())){
				createCheckUpdateTimer = true;
				props["Auto Update Data"] = bools[val.toString()]; 
			}else{
				return "Invalid value.";
			}
		};	
		
		
		this["get Data Update Interval"] = function(){ return props["Data Update Interval"]; }
		this["set Data Update Interval"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Data Update Interval"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			//clean updater
			var updaterInterval = $objEl.data("giDataUpdater");
			if(updaterInterval !== undefined){
				clearInterval(updaterInterval);
			}
			$objEl.remove();
		};
		
		this["get On Data Updated"] = function(){ return props["On Data Updated"]; }
		this["set On Data Updated"] = function(func){
			var getType = {};
			if(func === null || getType.toString.call(func) === '[object Function]'){
				props["On Data Updated"] = func; 
			}else{
				return "Invalid value.";
			}
		};	
		
		var context = this;
		this.createCheckUpdateTimer = function(){
			
			var updaterInterval = $objEl.data("giDataUpdater");
			
			if(props["Auto Update Data"] && (updaterInterval === undefined || updaterInterval === null)){
				updaterInterval = setInterval(context.checkUpdate, 5000);
				$objEl.data("giDataUpdater", updaterInterval);
			}else if(!props["Auto Update Data"]){
				clearInterval(updaterInterval);
				$objEl.data("giDataUpdater", null);
			}
			

		};
		
		this.checkUpdate = function(){
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			var updateInterval = $objEl.data("giProperties")["Data Update Interval"];
			
			//check if no data or if data is outdated
			if(chartData === undefined || chartData === null ||
				((new Date() - new Date(chartData.timeGenerated * 1000)) / 1000 > updateInterval)){
				context.updateData();
			}
		};
		
		this.updateData = function(){
			var updateXhr = $objEl.data("giSessionData")["Update XHR"];
			
			if(toUpdateData && updateXhr !== undefined && updateXhr !== null){
				updateXhr.abort();
				updateXhr = null;
			}
			
			if(updateXhr === undefined || updateXhr === null || updateXhr.readyState === 4) {
				
				
				var newUpdateXhr = $.ajax({
					type: "POST",
					url: props["Data URL"] === ""? "/modules/gi_data_opt/gi-get-chart-data.php" : props["Data URL"],
					dataType: "json",
					data: {query: props["Query"]
					},
					success: function(result){
						$objEl.data("giSessionData")["Chart Data"] = result;
						
						if(props["On Data Updated"] !== null){		
							var dataTable = $objEl.data("giSessionData")["Chart Data"]["dataTable"][0];

							var query1 = props["Data URL"] == "" ? props["Query"][0] : $objEl.data("giSessionData")["Chart Data"]["giqs"][0];
							
							//get categories
							var cats = [];
							for(var j=0; j<query1["groupings"].length; j++){
								cats.push(query1["groupings"][j]["alias"]);
							}
							for(var j=0; j<query1["optionalGroupings"].length; j++){
								cats.push(query1["optionalGroupings"][j]["alias"]);
							}
							
							//get row where all categories are null (grand total)
							var grandTotalRow = null;
							for(var i=(dataTable.length-1); i>=0; i--){
								var row = dataTable[i];
								var allNull = true;
								for(var j=0; j<cats.length; j++){
									if(row[cats[j]] !== null){
										allNull = false;
										break;
									}
								}
								if(allNull){
									grandTotalRow = row;
									break;
								}
							}
					
							props["On Data Updated"]({ "Grand Total": grandTotalRow });
						}
						
						$objEl[props["Identifier Name"]]("render");
					}
				});	
				
				$objEl.data("giSessionData")["Update XHR"] = newUpdateXhr;
			}
			
			
		};
		
		this.render = function(){
		
			var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", props["Width"] == "expand" ? "auto": props["Width"]);
			$objEl.css("height", props["Height"]);
			$objEl.css("min-height", props["Height"] === "auto" ? 20: "");
			$objEl.css("position", props["Positioning"] === "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);
			if(props["Positioning"] === "auto"){ $objEl.css("margin", props["Margin"]); }
			else{ $objEl.css("margin", 0); }
			
			var $border = $objEl.children("div.border");
			if($border.size() === 0){
				$border = $("<div>");
				$border.appendTo($objEl);
			}
			
			$border.html("");
			$border.addClass("border");
			$border.css("border-style", "solid");
			$border.css("background-color", props["Background Color"]);
			$border.css("border-width", props["Border Thickness"]);
			$border.css("border-color", props["Border Color"]);
			$border.css("padding", props["Padding"]);
			$border.css("border-radius", props["Corner Radius"]);
			$border.css("position", props["Height"] === "auto" ? "static" : "absolute");
			$border.css({"top": 0, "left": 0, "right": 0, "bottom": 0 });
			
			var $toolBar = $border.children(".toolBar"); 
			if($toolBar.size() == 0){
				$toolBar = $("<div>");
				$toolBar.addClass("toolBar giNoPrint");
				$border.append($toolBar);
			}
			$toolBar.html("");
			//$toolBar.css("margin-bottom", "8px");
			
			//console.log("rendering...");
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			
			if(chartData !== undefined && chartData !== null && chartData["dataTable"][0].length > 0){
				//console.log("has chart data");
				//get slicers info 
				var query1 = props["Data URL"] == "" ? props["Query"][0] : chartData["giqs"][0];
					
				var cats = 
					$.Enumerable
					.From(query1["groupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var optCats = 
					$.Enumerable
					.From(query1["optionalGroupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				
				var allDrilledFields = cats.concat([]);
				
				var allCats = cats.concat([]);
				
				
				var aggFields = 
					$.Enumerable
					.From(query1["aggregates"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				
				
				var dataTable = chartData["dataTable"][0];

				//manual check of updated data
				if(!props["Auto Update Data"]){
					var $checkUpdate = $("<button><span class='fa fa-refresh'></span>");
					$checkUpdate.attr("title", "Check for updated data.");
					$checkUpdate.addClass("button");
					$toolBar.append($checkUpdate);
					$checkUpdate.on("click", function(e){
						context.updateData();
					});
				}
				
				if(dataTable.length == 0){
					return;
				}
				
				//fill out form button
				if(props["New Record Button"] === "show" && query1["entities"].length > 0){
					var chartForms = [];
					for(var qei=0; qei<query1["entities"].length; qei++){
						var entity = query1["entities"][qei];
						//console.log(entity.formName + " " + entity.formId);
						if(entity["formId"] !== undefined && !isNaN(entity["formId"])){
							chartForms.push({ "name": entity.formName, "id":entity.formId});
						}
					}
					
					var $fillOutFormBtn = giChartComponents.generateFillOutFormButton({
						"chartForms": [chartForms]
					});
				
					$toolBar.append($fillOutFormBtn);
				}
				
				//legend
				var $legend = $('<span style="display:inline-block; font-size: 12px; float:right; margin-top:4px;"><i class="fa fa-exclamation-circle" aria-hidden="true" style="color:'+props["Low Color"]+'"></i> Low (&le;' + props["Low Threshold Percentage"] + '%)&nbsp;' + 
					'<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:'+props["Medium Color"]+'"></i> Medium (&gt;' + props["Low Threshold Percentage"] + '% and &lt;'+ props["High Threshold Percentage"] +'%)&nbsp' + 
					'<i class="fa fa-check" aria-hidden="true" style="color:'+props["High Color"]+'"></i> High (&ge;' + props["High Threshold Percentage"] + '%)</span>');
				$toolBar.append($legend);
				
				//display rows
				var allDrilledFieldsCount = allDrilledFields.length;
				
				var $tblScroller = $border.children("div.scroller");
				if($tblScroller.size() == 0){
					$tblScroller = $("<div>");
					$tblScroller.addClass("scroller");
					$tblScroller.appendTo($border);
				}
				$tblScroller.removeAttr("style");
				$tblScroller.css({"position": "relative" });
				
				if(props["Height"] !== "auto"){
					$tblScroller.css("overflow-y", "auto");
					$tblScroller.css("overflow-x", "hidden");
					var objElRect = $objEl[0].getBoundingClientRect();
					var scrollerRect = $tblScroller[0].getBoundingClientRect();
					//console.log("scrollerRect.top - objElRect.top = " + (scrollerRect.top - objElRect.top));
					var scrollerHeight = props["Height"] - (scrollerRect.top - objElRect.top) - props["Padding"];
					$tblScroller.css("height", scrollerHeight);
				}else{
					$tblScroller.css("overflow-y", "");
					$tblScroller.css("height", "auto");
				}
				
				var $tbl = $tblScroller.children("table.dataTable"); 
				if($tbl.size() == 0){
					$tbl = $("<table>");
					$tbl.appendTo($tblScroller);
				}
				$tbl.addClass("dataTable");
				$tbl.html("");
				$tbl.removeAttr("style");
				
				var previousCatCells = [];
				
				var $row = $("<tr>");
				$row.css({"background": props["Header Background"], 
					"color": props["Header Text Color"],
					"border": "solid 1px " + props["Header Background"] });
				
				//add the header cells: categories and aggregates
				for(var colName in dataTable[0]){
					$row.append("<th style='word-break: break-all;'>" + colName + "</th>");
				}
				
				//add the header for status, target and variance
				$row.append("<th style='word-break: break-all;'>Target</th>");
				$row.append("<th style='word-break: break-all;'>Variance</th>");
				$row.append("<th style='word-break: break-all;'>Achieved</th>");
				$row.append("<th style='word-break: break-all;'>Status</th>");
				
				//append the header row to table
				$tbl.append($row);
				
				//display data rows and cells
				//console.log(dataTable);
				for(var dti=0; dti<dataTable.length-1; dti++){
					var row = dataTable[dti];
										
					var $row = $("<tr>");
					//$row.css("background", dti%2===0 ? props["Row Background"] :  props["Row Alternating Background"]);
					$tbl.append($row);
					
					//show categories' cells
					var catsMatchedFromPrev = true;
					
					for(var ci=0; ci<allDrilledFields.length; ci++){
						var fieldName = allDrilledFields[ci];	
						
						var cat = dataTable[dti][fieldName];
						
						if(previousCatCells[ci] === undefined){
							previousCatCells.push({ cat: undefined, cell: undefined });
						}
					
						if(previousCatCells[ci]["cat"] !== dataTable[dti][fieldName]){
							catsMatchedFromPrev = false;
						}
						
						if(catsMatchedFromPrev){ 
							var prevRowSpan =  parseInt(previousCatCells[ci]["cell"].attr("rowspan"));
							previousCatCells[ci]["cell"].attr("rowspan", prevRowSpan + 1);
						}else{
							var $cell = $("<td>");
							$cell.append(cat === "" ? "&nbsp;" : cat);
							$cell.attr("rowspan", 1);
							$cell.appendTo($row);	
							$cell.attr("title", cat);
							$cell.css({ "border": "solid 1px #C0C0C0", "word-break": "break-all" });
							
							previousCatCells[ci]["cat"] = cat;
							previousCatCells[ci]["cell"] = $cell;
						}
					}
				
					//show aggregates' cells
					for(var ai=0; ai<1; ai++){
						
						//base value
						var $cell = $("<td></td>");
						
						var aggField = aggFields[ai];
						
						$cell.css({ "border": "solid 1px #C0C0C0" });
						
						$cell.appendTo($row);
					
						$cell.text(row[aggField].toLocaleString());
						$cell.css("text-align", "right");
						
						//target cell
						$cell = $("<td></td>");
						$cell.css({ "border": "solid 1px #C0C0C0", "text-align": "right" });
						
						$cell.text(props["Target Value"].toLocaleString());
						
						$cell.appendTo($row);
						
						//variance cell
						$cell = $("<td></td>");
						$cell.css({ "border": "solid 1px #C0C0C0", "text-align": "right" });
						
						$cell.text((row[aggField] - props["Target Value"]).toLocaleString());
						
						$cell.appendTo($row);
						
						//compute status
						
						//value per percentage of target, value of low threshold and high threshold
						var targetVpp = parseFloat(props["Target Value"]) / 100;
						
						var lowThresVal = parseFloat(targetVpp) * parseFloat(props["Low Threshold Percentage"]);
						var highThresVal = parseFloat(targetVpp) * parseFloat(props["High Threshold Percentage"]);
						
						var baseValStat = null;
						if(parseFloat(row[aggField]) <= lowThresVal){
							baseValStat = "Low";
						}else if(parseFloat(row[aggField]) >= highThresVal){
							baseValStat = "High";
						}else{
							baseValStat = "Medium";
						}
						
						//achieved cell
						
						$cell = $("<td></td>");
						$cell.css({ "border": "solid 1px #C0C0C0", "text-align": "right", "color": props[baseValStat + " Color"] });
						
						$cell.text(Math.roundOff((row[aggField] / targetVpp), 0.1).toLocaleString() + "%");
						
						$cell.appendTo($row);

						//status cell
						$cell = $("<td></td>");
						$cell.css({ "border": "solid 1px #C0C0C0", "text-align": "center" });
						
						//console.log(targetVpp + " " + lowThresVal + " " + highThresVal);
						if(baseValStat === "Low"){
							$cell.append('<i class="fa fa-exclamation-circle" aria-hidden="true" style="color:'+props["Low Color"]+'"></i>');
						}else if(baseValStat === "High"){
							$cell.append('<i class="fa fa-check" aria-hidden="true" style="color:'+props["High Color"]+'"></i>');	
						}else{
							$cell.append('<i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:'+props["Medium Color"]+'"></i>');
						}
						
						$cell.appendTo($row);
					}
					
				}
				
				
				$tbl.width($tbl.width());
				$tbl.css("z-index", 2)
				$tbl.find("th").each(function(i){
					$(this).width($(this).width());
					$(this).height($(this).height());
				});
				
				$border.children("table.headerTable").remove(); 
				
				var $headerTable = $($tbl[0].cloneNode(false));
				$headerTable.attr("class", "headerTable");
				$headerTable.insertBefore($tblScroller);
				var tblScrollerPos = $tblScroller.position();
				$headerTable.css({ "position": "absolute", "top":tblScrollerPos.top, "left": tblScrollerPos.left, "z-index": 3, "height": 1 });
				
				var $frozenHeader = $tbl.find("tr:first").clone();
				$headerTable.append($frozenHeader);
				

			}else{
				$border.html("");
			}
			
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined && methodName !== "render"){
		methodResult = objCore[methodName](params);
	}
	
	if(toUpdateData){
		objCore.updateData();
	}
	
	if(toCheckUpdate){
		objCore.checkUpdate();
	}

	if(createCheckUpdateTimer){
		objCore.createCheckUpdateTimer();
	}
	
	if(!cancelRender){ 
		objCore.render(); 
	}
	
	return { "methodResult": methodResult, "affected": affected };
};




})(jQuery);