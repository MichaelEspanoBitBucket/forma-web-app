(function($){

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

$.fn.giLayoutTable = function(methodName, params){

	var renderChildren = false; 
	var cancelRender = false;
	var affected = [];
	var initialized = false;
	
	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": "expand", "Height": 200 };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giLayoutTable")){
				initialized = true;
				$objEl.addClass("giObject giLayout giLayoutTable");
				
				var cellsUids = [];
				var currDateTime = new Date().getTime();
				for(var i=0; i<9; i++){
					cellsUids.push("ui-giLayoutTableCell-" + currDateTime);
					currDateTime++;
				}
				
				var rowsUids = [];
				for(var i=0;i<3; i++){
					rowsUids.push("ui-giLayoutTableRow-" + currDateTime);
					currDateTime++;
				}
				
				$objEl.data("giProperties", {
					"Identifier Name": "giLayoutTable",
					"Object Type": "Layout Table",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Ordinal": 0,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "auto",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "transparent",
					"Cell Padding": 8,
					"Rows": 
						[
							{ 	"UID": rowsUids[0],
								"Cells": [
									{ "UID": cellsUids[0] },
									{ "UID": cellsUids[1] },
									{ "UID": cellsUids[2] }] 
							},
							{ 	"UID": rowsUids[1],
								"Cells": [
									{ "UID": cellsUids[3] },
									{ "UID": cellsUids[4] },
									{ "UID": cellsUids[5] }] 
							},
							{ 	"UID": rowsUids[2],
								"Cells": [
									{ "UID": cellsUids[6] },
									{ "UID": cellsUids[7] },
									{ "UID": cellsUids[8] }] 
							}
						] 
				});
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val);
				renderChildren = true;
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? val : parseInt(val); 
				renderChildren = true;
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
				renderChildren = true;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
				renderChildren = true;
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Cell Padding"] = function(){ return props["Cell Padding"]; }
		this["set Cell Padding"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Cell Padding"] = parseInt(val); 
				renderChildren = true;
			}else{
				return "Invalid value.";
			}
		};
		
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val) || val == "transparent"){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Fill"] = function(){ return props["Fill"]; }
		this["set Fill"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Fill"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Rows"] = function(){ return props["Rows"]; }
		this["set Rows"] = function(val){
			renderChildren = true;
			props["Rows"] = val;
		}
		
		this["addColumn"] = function(){
			var rows = props["Rows"];
			var currDateTime = new Date().getTime();
			for(var i=0; i<rows.length; i++){
				rows[i]["Cells"].push({ "UID": "ui-giLayoutTableCell-" + (++currDateTime) });
			}
			renderChildren = true;
		};
		
		this["deleteColumn"] = function(index){
			affected.push({ "type": "modified", "propBefore": $.extend(true, {}, props) });
			
			var $cols = $objEl.children("table")
				.children("tbody")
				.children("tr")
				.children("td:eq(" + index + ")");
				
			$cols.children(".giObjectContainer")
				.children(".giObject").each(function(i, v){
				var $obj = $(v);
				var delResult = $obj[$obj.data("giIdentifierName")]("delete");
				
				affected = affected.concat(delResult["affected"]);
			});
			
			for(var ri=0; ri<props["Rows"].length; ri++){
				props["Rows"][ri]["Cells"].splice(index, 1);
			}
		}
				
		this["addRow"] = function(){
			var rows = props["Rows"];
			var cellCount = 1;
			if(rows.length > 0){
				cellCount = rows[0]["Cells"].length;
			}
			
			var currDateTime = new Date().getTime();

			var newRow = {"UID": "ui-giLayoutTableRow-" + (currDateTime++), "Cells": []};
			rows.push(newRow);
			
			for(var i=0; i<cellCount; i++){
				newRow["Cells"].push({ "UID": "ui-giLayoutTableCell-" + (currDateTime++) });
			}
			renderChildren = true;
		};
		
		this["deleteRow"] = function(index){
			affected.push({ "type": "modified", "propBefore": $.extend(true, {}, props) });
			remove
			var $row = $objEl.children("table")
				.children("tbody")
				.children("tr:eq(" + index + ")");
				
			$row.children("td")
				.children(".giObjectContainer")
				.children(".giObject").each(function(i, v){
				var $obj = $(v);
				var delResult = $obj[$obj.data("giIdentifierName")]("delete");
				
				affected = affected.concat(delResult["affected"]);
			});
			
			props["Rows"].splice(index, 1);
		}
		
		this["delete"] = function(){
			affected.push({ "type": "deleted", "propBefore": $.extend(true, {}, props) });
			
			$objEl.children("table")
				.children("tbody")
				.children("tr")
				.children("td")
				.children(".giObjectContainer")
				.children(".giObject").each(function(i, v){
				var $obj = $(v);
				var delResult = $obj[$obj.data("giIdentifierName")]("delete");
				
				affected = affected.concat(delResult["affected"]);
			});
			
			cancelRender = true;
			$objEl.remove();
		};
		
		
		this.render = function(){
			var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			/* setup div */
			
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.addClass("giLayoutTable");
			$objEl.attr("id", props["UID"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", props["Width"] == "expand" ? "auto": props["Width"]);
			$objEl.css("height", props["Height"]);
			$objEl.css("position", props["Positioning"] == "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);
			
			/* setup table */
			
			var computedWidth = props["Width"] == "expand" ? computedWidth = $objEl[0].offsetWidth :props["Width"];
			
			var $tbl = $objEl.find("table");
			if($tbl.size() == 0){
				$tbl = $("<table>");
				$objEl.append($tbl);
			}
			
			$tbl.css("width", computedWidth);
			$tbl.css("height", props["Height"]);
			$tbl.css("border-collapse", "collapse");
			$tbl.css("cellspacing", 0);
			
			//remove rows that don't belong anymore, probably deleted
			var rowIds = $.Enumerable.From(props["Rows"])
				.Select(function(r){ return r["UID"]; })
				.ToArray();
			var $currentRows = $objEl.children("table")
					.children("tbody")
					.children("tr");
			for(var ri=0; ri<$currentRows.size(); ri++){
				var $row = $currentRows.eq(ri);
				if(rowIds.indexOf($row.attr("id")) === -1){
					$row.remove();
				}
			}
			
			//remove cells that don't belong anymore, probably deleted
			var cellIds = [];
			for(var ri=0; ri<props["Rows"].length; ri++){
				var row = props["Rows"][ri];
				for(var ci=0; ci<row["Cells"].length; ci++){
					cellIds.push(row["Cells"][ci]["UID"]);
				}
			}
			var $currentCellContainers = $objEl.children("table")
					.children("tbody")
					.children("tr")
					.children("td")
					.children(".giObjectContainer");
			for(var ci=0; ci<$currentCellContainers.size(); ci++){
				var $cellContainer = $currentCellContainers.eq(ci);
				if(cellIds.indexOf($cellContainer.attr("id")) === -1){
					$cellContainer.parent().remove();
				}
			}
			
			for(var ri=0; ri<props["Rows"].length; ri++){
				/* setup rows */
				var $row = $tbl.find("#" + props["Rows"][ri]["UID"]);
				if($row.size() == 0){
					$row = $("<tr>");
					$row.attr("id", props["Rows"][ri]["UID"]);
					$tbl.append($row);
				}
				
				/* setup cells */
				for(var ci=0; ci<props["Rows"][ri]["Cells"].length; ci++){
					
					var $cellContainer = $row.find("#" + props["Rows"][ri]["Cells"][ci]["UID"]);
					var $cell = $cellContainer.closest("td");
					
					if($cellContainer.size() == 0){
						$cell = $("<td>");
						$row.append($cell);
						
						$cellContainer = $("<div>");
						$cell.append($cellContainer);
					}
					
					$cell.css("position", "relative");
					$cell.css("border-style", "solid");
					$cell.css("border-width", props["Border Thickness"]);
					$cell.css("border-color", props["Border Color"]);
					$cell.css("padding", props["Cell Padding"]);
					
					$cellContainer.addClass("giObjectContainer");
					$cellContainer.attr("id", props["Rows"][ri]["Cells"][ci]["UID"]);
					$cellContainer.css("position", "absolute");
					$cellContainer.css("top", 0);
					$cellContainer.css("left", 0);
					$cellContainer.css("right", 0);
					$cellContainer.css("bottom", 0);
					$cellContainer.css("overflow", props["Height"] === "auto" ? "visible" : "hidden");
				}
			}
			
			if(renderChildren){
				$objEl.children("table")
					.children("tbody")
					.children("tr")
					.children("td")
					.children(".giObjectContainer")
					.children(".giObject").each(function(k, v){
					$v = $(v);
					$v[$v.data("giIdentifierName")]("render");
				});
			}
		};
		
		
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}

	if(!cancelRender){ objCore.render(); }
	//console.log(methodResult);
	return { "methodResult": methodResult, "affected": affected };

};

})(jQuery);
