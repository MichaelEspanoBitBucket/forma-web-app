(function($){

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

$.fn.giLineShape = function(methodName, params){

	var toUpdateData = false;
	var cancelRender = false;
	var affected = [];
	var initialized = false;
	var index = 2
	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": 200, "Height": 200 };
		
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giLineShape")){
				
				initialized = true;
				$objEl.addClass("giObject giOther giLineShape");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giLineShape",
					"Object Type": "Line",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Positioning": "manual",
					"Top": 0,
					"Left": 0,
					"Top 2":10,
					"Left 2":10,
					//"Point 1":{"Top":0,"Left":0},
				//	"Point 2":{"Top":0,"Left":0},
					"Z-index": "auto",
					"Thickness":1,
					"Border Thickness": 1,
					"Border Color": "transparent",
					"Background Color": "#000000",
					// "Corner Radius": 0,
					//"Content":"Text Here"
				});
				
				
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				//console.log(name)
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};

		this["get Content"] = function(){ return props["Content"]; };
		this["set Content"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Content"] = val; 
			//	cancelRender = true;
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};

		this["get Query"] = function(){ return props["Query"]; };
		this["set Query"] = function(val){ 
			if(true){
				props["Query"] = val; 
				toUpdateData = true;
				
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
			
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};

		this["get Top 2"] = function(){ return props["Top 2"]; };
		this["set Top 2"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top 2"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left 2"] = function(){ return props["Left 2"]; };
		this["set Left 2"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left 2"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};

		this["get Thickness"] = function(){ return props["Thickness"]; }
		this["set Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val) || val == 'transparent'){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Background Color"] = function(){ return props["Background Color"]; }
		this["set Background Color"] = function(val){
			if(hexColorRegEx.test(val) || val == 'transparent'){
				props["Background Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
		this["set Corner Radius"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Corner Radius"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};			
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
	

		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			$objEl.remove();
		};


		
		
		this.render = function(){
		
			//var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}

			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
 			$objEl.css({
 				"position":"absolute",
 				"z-index":props["Z-index"]
 			})
			var $svg = $objEl.find(".svg-line"); 
			if($svg.size() == 0){
				$svg = $('<svg class="svg-line" version = "1.1">'+
					'<line class="line-border" x1="40" x2="120" y1="100" y2="100" stroke="green" stroke-width="20" stroke-linecap="round"/>'+
    				'<line class="line" x1="40" y1 = "100" x2 = "120" y2 = "100" stroke = "black" stroke-width = "10px" />' +
					'</svg>');
				$svg.appendTo($objEl);
			}
			var $line = $svg.find('.line');
			var $lineBorder = $svg.find('.line-border')
			var $lines = $svg.find('line');
			var bordersize = props["Thickness"] + props["Border Thickness"];
			var extendSize = (bordersize * 2) + 10
			var positions = {
		        t1:props["Top"] <= props["Top 2"] ? props["Top"] : props["Top 2"],
		        t2:props["Top"] >= props["Top 2"] ? props["Top"] : props["Top 2"],
		        l1:props["Left"] <= props["Left 2"] ? props["Left"] : props["Left 2"],
		        l2:props["Left"] >= props["Left 2"] ? props["Left"] : props["Left 2"]
        	}
        	console.log(positions)
        	$line.attr({"stroke-width":props["Thickness"], "stroke":props["Background Color"]});
        	$lineBorder.attr({"stroke-width":bordersize, "stroke":props["Border Color"]});
			
			$objEl.css({
	       		top:positions.t1 - bordersize, // top1 - bordersize
	           	left:positions.l1 - bordersize, // left1 - bordersize
	           	width:(positions.l2 - positions.l1) + extendSize, //(left1-left2)+(bordersize*2)+10
	           	height:(positions.t2 - positions.t1) + extendSize
       		})
       
	        $svg.css({
	           	width:(positions.l2 - positions.l1) + extendSize, //(left1-left2)+(bordersize*2)+10
	           	height:(positions.t2 - positions.t1) + extendSize
	       	})
	  		
	       //Point A
	       $lines.attr('x1',(props["Left"] + bordersize + 5) - positions.l1)
	       $lines.attr('y1',(props["Top"] + bordersize + 5) - positions.t1)
	            
	       //Point B
	       $lines.attr('x2',(props["Left 2"] + bordersize + 5) - positions.l1)
	       $lines.attr('y2',(props["Top 2"] + bordersize + 5) - positions.t1)

			
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined && methodName !== "render"){
		methodResult = objCore[methodName](params);
	}
	
	if(toUpdateData){
		objCore.updateData({ quick:true });
	}
	
	if(!cancelRender){ 
		objCore.render(); 
	}
	
	return { "methodResult": methodResult, "affected": affected };
};

})(jQuery);