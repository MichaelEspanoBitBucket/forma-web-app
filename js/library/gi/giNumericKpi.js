(function($){

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

var giChartComponents = new (function(){
	
	this.generateFillOutFormButton = function(params){
		var buttonId = "gi-temp-"+(new Date().getTime());
		
		var $button = $("<button id='"+buttonId+"' class='button' title='New Request'><span class='fa fa-plus'></span></button>");
		
		var chartForms = params["chartForms"];
	
		var menuItems = {};
		for(var j=0; j<chartForms.length; j++){
			for(var i=0; i<chartForms[j].length; i++){
			
				menuItems[chartForms[j][i]["id"]] = {"name": chartForms[j][i]["name"]};
			}
		}
		
		//menuItems = { "1": {"name": "Under maintenance"}};
		
		$.contextMenu({
			selector: "#"+buttonId, 
			trigger: 'left',
			callback: function(key, options) {
				 window.open("/user_view/workspace?view_type=request&formID="+key+"&requestID=0", '_blank').focus();
			},
			items: menuItems
		});
		
		
		return $button;
	};
	
	
})();

$.fn.giNumericKpi = function(methodName, params){

	var toUpdateData = false;
	var toCheckUpdate = false;
	var createCheckUpdateTimer = false;
	
	var cancelRender = false;
	var affected = [];
	var initialized = false;

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": "auto", "Height": "auto" };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giNumericKpi")){
				
				initialized = true;
				toCheckUpdate = true;
				createCheckUpdateTimer = true;
				
				$objEl.addClass("giObject giChart giNumericKpi");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giNumericKpi",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Object Type": "Numeric KPI",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "auto",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "#000",
					"Corner Radius": 8,
					"Padding": 8,
					"Margin": 4,
					"Font Size": 18,
					"Background Color": "#fff",
					"Decreased Color": "#ff0000",
					"Not Changed Color": "#ffbb00",
					"Increased Color": "#00ff55",
					"Auto Update Data": true,
					"Data Update Interval": 300,
					"Query": [],
					"Data URL": "",
					"Target Value": 100,
					"On Data Updated": null
				});
				
				$objEl.data("giSessionData", {
					"Chart Data": undefined
				});
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};

		this["get Query"] = function(){ return props["Query"]; };
		this["set Query"] = function(val){ 
			if(true){
				$objEl.data("giSessionData")["Chart Data"] = undefined;
				props["Query"] = val; 
				toUpdateData = true;
				
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Target Value"] = function(){ return props["Target Value"]; };
		this["set Target Value"] = function(val){ 
			
			if(parseInt(val) > 0){
				props["Target Value"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
			
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || val === "auto" || parseInt(val) > 0){
				if(isNaN(val)){
					props["Width"] = val;
				}else{
					props["Width"] = parseInt(val);
				}
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? val : parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Font Size"] = function(){ return props["Font Size"]; };
		this["set Font Size"] = function(val){ 
			
			if(!isNaN(parseInt(val)) && parseInt(val) > 7){
				props["Font Size"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Padding"] = function(){ return props["Padding"]; }
		this["set Padding"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Padding"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Margin"] = function(){ return props["Margin"]; }
		this["set Margin"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Margin"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Background Color"] = function(){ return props["Background Color"]; }
		this["set Background Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Background Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
		this["set Corner Radius"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Corner Radius"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};			
		
		this["get Decreased Color"] = function(){ return props["Decreased Color"]; }
		this["set Decreased Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Decreased Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Not Changed Color"] = function(){ return props["Not Changed Color"]; }
		this["set Not Changed Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Not Changed Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Increased Color"] = function(){ return props["Increased Color"]; }
		this["set Increased Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Increased Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Data URL"] = function(){ return props["Data URL"]; };
		this["set Data URL"] = function(val){ 
			if(val !== undefined && val !== null){
				toUpdateData = true;
				props["Data URL"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Auto Update Data"] = function(){ return props["Auto Update Data"]; }
		this["set Auto Update Data"] = function(val){
			var bools = { "true": true, "false": false };
			if(bools.hasOwnProperty(val.toString())){
				createCheckUpdateTimer = true;
				props["Auto Update Data"] = bools[val.toString()]; 
			}else{
				return "Invalid value.";
			}
		};	
		
		
		this["get Data Update Interval"] = function(){ return props["Data Update Interval"]; }
		this["set Data Update Interval"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Data Update Interval"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			//clean updater
			var updaterInterval = $objEl.data("giDataUpdater");
			if(updaterInterval !== undefined){
				clearInterval(updaterInterval);
			}
			$objEl.remove();
		};
		
		this["get On Data Updated"] = function(){ return props["On Data Updated"]; }
		this["set On Data Updated"] = function(func){
			var getType = {};
			if(func === null || getType.toString.call(func) === '[object Function]'){
				props["On Data Updated"] = func; 
			}else{
				return "Invalid value.";
			}
		};	
		
		var context = this;
		this.createCheckUpdateTimer = function(){
			
			var updaterInterval = $objEl.data("giDataUpdater");
			
			if(props["Auto Update Data"] && (updaterInterval === undefined || updaterInterval === null)){
				updaterInterval = setInterval(context.checkUpdate, 5000);
				$objEl.data("giDataUpdater", updaterInterval);
			}else if(!props["Auto Update Data"]){
				clearInterval(updaterInterval);
				$objEl.data("giDataUpdater", null);
			}
			

		};
		
		this.checkUpdate = function(){
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			var updateInterval = $objEl.data("giProperties")["Data Update Interval"];
			
			//check if no data or if data is outdated
			if(chartData === undefined || chartData === null ||
				((new Date() - new Date(chartData.timeGenerated * 1000)) / 1000 > updateInterval)){
				context.updateData();
			}
		};
		
		this.updateData = function(){
			var updateXhr = $objEl.data("giSessionData")["Update XHR"];
			
			if(toUpdateData && updateXhr !== undefined && updateXhr !== null){
				updateXhr.abort();
				updateXhr = null;
			}
			
			if(updateXhr === undefined || updateXhr === null || updateXhr.readyState === 4) {
				
				
				var newUpdateXhr = $.ajax({
					type: "POST",
					url: props["Data URL"] === ""? "/modules/gi_data_opt/gi-get-chart-data.php" : props["Data URL"],
					dataType: "json",
					data: {query: props["Query"]
					},
					success: function(result){
						$objEl.data("giSessionData")["Chart Data"] = result;
						
						if(props["On Data Updated"] !== null){		
							var dataTable = $objEl.data("giSessionData")["Chart Data"]["dataTable"][0];

							var query1 = props["Data URL"] == "" ? props["Query"][0] : $objEl.data("giSessionData")["Chart Data"]["giqs"][0];
							
							//get categories
							var cats = [];
							for(var j=0; j<query1["groupings"].length; j++){
								cats.push(query1["groupings"][j]["alias"]);
							}
							for(var j=0; j<query1["optionalGroupings"].length; j++){
								cats.push(query1["optionalGroupings"][j]["alias"]);
							}
							
							//get row where all categories are null (grand total)
							var grandTotalRow = null;
							for(var i=(dataTable.length-1); i>=0; i--){
								var row = dataTable[i];
								var allNull = true;
								for(var j=0; j<cats.length; j++){
									if(row[cats[j]] !== null){
										allNull = false;
										break;
									}
								}
								if(allNull){
									grandTotalRow = row;
									break;
								}
							}
					
							props["On Data Updated"]({ "Grand Total": grandTotalRow });
						}
						
						$objEl[props["Identifier Name"]]("render");
					}
				});	
				
				$objEl.data("giSessionData")["Update XHR"] = newUpdateXhr;
			}
			
			
		};
		
		this.render = function(){
		
			var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", isNaN(props["Width"]) ? "auto": props["Width"]);
			$objEl.css("height", props["Height"]);
			$objEl.css("min-height", props["Height"] === "auto" ? 20: "");
			$objEl.css("position", props["Positioning"] === "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);
			if(props["Positioning"] === "auto"){ $objEl.css("margin", props["Margin"]); }
			else{ $objEl.css("margin", 0); }
			
			var $border = $objEl.children("div.border");
			if($border.size() === 0){
				$border = $("<div>");
				$border.appendTo($objEl);
			}
			
			$border.html("");
			$border.addClass("border");
			$border.css("border-style", "solid");
			$border.css("background-color", props["Background Color"]);
			$border.css("border-width", props["Border Thickness"]);
			$border.css("border-color", props["Border Color"]);
			$border.css("padding", props["Padding"]);
			$border.css("border-radius", props["Corner Radius"]);
			$border.css("position", props["Height"] === "auto" ? "static" : "absolute");
			$border.css({"top": 0, "left": 0, "right": 0, "bottom": 0 });
			
			var $toolBar = $border.children(".toolBar"); 
			if($toolBar.size() == 0){
				$toolBar = $("<div>");
				$toolBar.addClass("toolBar giNoPrint");
				$border.append($toolBar);
			}
			$toolBar.html("");
			//$toolBar.css("margin-bottom", "8px");
			
			//console.log("rendering...");
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			
			if(chartData !== undefined && chartData !== null && chartData["dataTable"][0].length > 0){
				//console.log("has chart data");
				//get slicers info 
				var query1 = props["Data URL"] == "" ? props["Query"][0] : chartData["giqs"][0];
					
				var cats = 
					$.Enumerable
					.From(query1["groupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var optCats = 
					$.Enumerable
					.From(query1["optionalGroupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				
				var allDrilledFields = cats.concat([]);
				
				var allCats = cats.concat([]);
				
				
				var aggFields = 
					$.Enumerable
					.From(query1["aggregates"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				
				
				var dataTable = chartData["dataTable"][0];

				//manual check of updated data
				
				if(dataTable.length == 0){
					return;
				}
				
				$border.html("");
				var row = dataTable[0];
				
				var aggField = aggFields[0];
				//compute status
						
				//value per percentage of target
				//var targetVpp = parseFloat(props["Target Value"]) / 100;
				
				var baseValStat = null;
				if(parseFloat(row[aggField]) < parseFloat(props["Target Value"])){
					baseValStat = "Decreased";
				}else if(parseFloat(row[aggField]) > parseFloat(props["Target Value"])){
					baseValStat = "Increased";
				}else if(parseFloat(row[aggField]) == parseFloat(props["Target Value"])){
					baseValStat = "Not Changed";
				}
				
				var changeIndicatorData = {
					"Decreased": { "Icon": "fa-chevron-down", "Operator": "-" },
					"Not Changed": { "Icon": "fa-pause", "Operator": "+" },
					"Increased": { "Icon": "fa-chevron-up" , "Operator": "+"}
				};
				
				//change indicator icon
				var $changeIndicator = $('<i class="fa" aria-hidden="true" style="position:relative;margin-right:0.5em;font-size:' + props["Font Size"] + 'px; color:'+props[baseValStat+" Color"]+'"></i>');
				$border.append($changeIndicator);
				
				$changeIndicator.addClass(changeIndicatorData[baseValStat]["Icon"]);
				
				//base value
				var $valEl = $("<span style='position:relative;font-size:" + props["Font Size"] + "px;'>"+ row[aggField].toLocaleString() +"</span>");
				$border.append($valEl);
				
				//changes in percentage
				var percentChange = 100 / props["Target Value"] * row[aggField] - 100;
				
				var $percentChange = $('<span style="font-size:12px; margin-left:0.5em; color:'+props[baseValStat+" Color"]+'">'+ 
					(percentChange < 0 ? "-" : "+") + 
					Math.roundOff(Math.abs(percentChange), 0.1).toLocaleString() + '%</span>');
				
				$border.append($percentChange)
				
				$border.append("<br/>");
				$border.append("compared to: ");
				
				//target value
				$border.css("text-align", "center");
				
				var $targetValue = $('<span style="font-size:12px;">' + props["Target Value"].toLocaleString() + '</span>');
				$border.append($targetValue);
			}else{
				$border.html("");
			}
			
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined && methodName !== "render"){
		methodResult = objCore[methodName](params);
	}
	
	if(toUpdateData){
		objCore.updateData();
	}
	
	if(toCheckUpdate){
		objCore.checkUpdate();
	}

	if(createCheckUpdateTimer){
		objCore.createCheckUpdateTimer();
	}
	
	if(!cancelRender){ 
		objCore.render(); 
	}
	
	return { "methodResult": methodResult, "affected": affected };
};




})(jQuery);