(function($){
 
var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;
 
$.fn.giPage = function(methodName, params){

	var affected = [];
	var cancelRender = false;
	var initialized = false;

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = null;
		var defaultProps = { "Width": 816, "Height": 1056, "Padding": 8 };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giPage")){
				initialized = true;
				$objEl.addClass("giObject giPage");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giPage",
					"Object Type": "Page",
					"UID" : "",
					"Container ID": "content",
					"Name" : "",
					"Ordinal" : 0,
					"Page Size ID": 0,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Padding": defaultProps["Padding"]
				});
				
				$(window).resize(function(){
					if($('.ui-resizable-resizing').length == 0){
						$objEl.giPage("render")
					}; 
				});
				// this.request();
			}


			
			props = $objEl.data("giProperties");
			
		};
		
		// this.request = function(){
		// 	$objEl.giPage("render")
		// 	window.requestAnimationFrame(this.request);
		// }
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){
			return $objEl.data("giProperties");
		};
		
		/* Property getters and setters */
		
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value for UID.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val === undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value for Name.";
			}
		};
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val === undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value for Container ID.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value for Ordinal.";
			}
		};
		
		this['get Page Size ID'] = function(){
			return props["Page Size ID"];
		}
		this['get Page Size ID'] = function(val){
			if(parseInt(val) > -1){
				props["Page Size ID"];
			}else{
				return "Invalid value for Page Size ID.";
			}
		}
				
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? val:  parseInt(val); 
			}else{
				return "Invalid value for Height.";
			}
		};
		
		this["get Padding"] = function(){ return props["Padding"]; };
		this["set Padding"] = function(val){ 
			if(val === "auto" || parseInt(val) > -1){
				props["Padding"] = val === "auto" ? val:  parseInt(val); 
			}else{
				return "Invalid value for Padding.";
			}
		};
		
		this["delete"] = function(){
			affected.push({ "type": "deleted", "propBefore": $.extend(true, {}, props) });
			
			$objEl.find(".giObject").each(function(i, v){
				var $obj = $(v);
				affected = affected.concat($obj[$obj.data("giIdentifierName")]("delete")["affected"]);
			});
			
			cancelRender = true;
			$objEl.remove();
		};
		
		this.render = function(){	
			$objEl.addClass("giObjectContainer");

			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			$objEl.attr("id", props["UID"]);
			
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css({
				"width": props["Width"] == "expand"? "auto": props["Width"] - props["Padding"] * 2,
				"min-height": props["Height"] == "auto" ? 200 : "",
				"height": props["Height"] == "auto"? "auto": props["Height"] - props["Padding"] * 2,
				"padding": props["Padding"],
				"position":"relative",
				"overflow":"hidden"
			});
			
			$objEl.children(".giObject").each(function(k, v){
				$v = $(v);
				$v[$v.data("giIdentifierName")]("render");
				
			});
		};
		
		
		
	})($(this));
	
	var methodResult = undefined;
	
	objCore.initializeElement();
	
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	
	objCore.render();
	//console.log(methodResult);
	return methodResult;
};

})(jQuery);