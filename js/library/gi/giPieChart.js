(function($){

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

function rgbHexToIntegers(color) {
	var match, quadruplet;

	// Match #aabbcc
	if (match = /#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(color)) {
		quadruplet = [parseInt(match[1], 16), parseInt(match[2], 16), parseInt(match[3], 16), 1];

		// Match #abc
	} else if (match = /#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(color)) {
		quadruplet = [parseInt(match[1], 16) * 17, parseInt(match[2], 16) * 17, parseInt(match[3], 16) * 17, 1];

		// Match rgb(n, n, n)
	} else if (match = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color)) {
		quadruplet = [parseInt(match[1]), parseInt(match[2]), parseInt(match[3]), 1];

	} else if (match = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9\.]*)\s*\)/.exec(color)) {
		quadruplet = [parseInt(match[1], 10), parseInt(match[2], 10), parseInt(match[3], 10),parseFloat(match[4])];

		// No browser returns rgb(n%, n%, n%), so little reason to support this format.
	} else {
		//quadruplet = colors[color];
		//console.log("error parsing color: " + color);
	}
	return quadruplet;
}

function decimalToHex(d, padding) {
    var hex = Number(d).toString(16);
    padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;

    while (hex.length < padding) {
        hex = "0" + hex;
    }

    return hex;
}


var GiChartColors = function(mixColor){
	var colors =
		["6699FF", "FF66FF","66FF66", "FF6666" , "FFFF66", "00FFFF",
		"9966FF", "FF3399","00FF99", "FF9933", "99FF33", "0099FF",
		"666699", "990099", "990000", "999966", "339933", "3366CC"];
	//["0099FF","FF66FF","66FF66","FFCC66","006699","CC6699","339966","FF6666"];


		colors = ["65a8ff","90baff","b4d0ff","d8e7ff"];
		colors = ["f17979","ffdc7a","cccccc","95d9f0","7a98bf"];
		



	
	var _index = 0;
	var index = function(newIndex){
		if(newIndex === undefined){
			return _index;
		}
		_index = newIndex;
	};

	this.next = function(){
		if(index() >= colors.length){
			index(0);
		}
		var color = colors[index()];
		if(mixColor !== undefined){
			var colorDec = rgbHexToIntegers(color);
			var mixColorDec = rgbHexToIntegers(mixColor);
			color = decimalToHex(Math.abs(colorDec[0] - mixColorDec[0])) +
			decimalToHex(Math.abs(colorDec[1] - mixColorDec[1])) +
			decimalToHex(Math.abs(colorDec[2] - mixColorDec[2]));
		}
		
		index(index()+1);
		return color;
	}
}

var giChartComponents = new (function(){
	
	this.generateFillOutFormButton = function(params){
		var buttonId = "gi-temp-"+(new Date().getTime());
		
		var $button = $("<button id='"+buttonId+"' class='button' title='New Request'><span class='fa fa-plus'></span></button>");
		
		var chartForms = params["chartForms"];
	
		var menuItems = {};
		for(var j=0; j<chartForms.length; j++){
			for(var i=0; i<chartForms[j].length; i++){
			
				menuItems[chartForms[j][i]["id"]] = {"name": chartForms[j][i]["name"]};
			}
		}
		
		//menuItems = { "1": {"name": "Under maintenance"}};
		
		
		$.contextMenu({
			selector: "#"+buttonId, 
			trigger: 'left',
			callback: function(key, options) {
				 window.open("/user_view/workspace?view_type=request&formID="+key+"&requestID=0", '_blank').focus();
			},
			items: menuItems
		});
		
		
		return $button;
	};
	
	this.generateFilterDialog = function(params){
		var filters = params.filters;
		var fields = params.fields;
		var $objEl = params.objEl;
		
		var $dialog = $("#giChartFilterDialog");
		if($dialog.size() == 0){
			$dialog = $("<div>");
			$dialog.attr("id", "giChartFilterDialog");
			
			$dialog.appendTo("body");
			$dialog.dialog({ modal: true, hide: true, width: 500, height: 300 });
			
			$dialog.on("click", ".removeFilter", function(e){
				$(e.currentTarget).closest("tr").remove();
			});
			
			$dialog.on("change", ".fieldList", function(e){
				var $fieldList = $(e.currentTarget);
				var type = $fieldList.find("option:selected").attr("data-type");
				var $valueInput = $fieldList.closest("tr").find(".valueInput");
				if(type == "Numeric"){
					$valueInput.spinner();
				}else if(type == "Date"){
					$valueInput.datepicker();
				}else if(type == "Text"){
					$valueInput.datepicker("destroy");
					$valueInput.spinner("destroy");
					$valueInput.autocomplete({ source: [] });
				}
			});
		}else{
			$dialog.empty();
		}
		
		$dialog.append("<p>Note: This filter is temporary and will not be saved.</p>");
		
		$dialog.dialog({
			buttons: [
				{
					text: "Apply",
					click: function(e) {
						var $filterRows = $(this).find(".filterTable tr.data");
						
						var filters = [];
						$filterRows.each(function(i, el){
							var $el = $(el);
							var filter = { 
								dataType: $el.find(".fieldList option:selected").attr("data-type"),
								field: $el.find(".fieldList").val(), 
								comparator: $el.find(".comparatorList").val(), 
								value: $el.find(".valueInput").val(), 
								logicalRelation: $el.find(".logicalList").val() 
							};
							
							if(filter.value !== null && filter.value !== ""){
								filters.push(filter);
							}
						});
						
						$objEl.data("giSessionData")["Filters"] = [filters];
						$objEl[$objEl.data("giIdentifierName")]("render");
						
						$(this).dialog("close");
						//$objEl.data("giSessionData");
						
						//DELETE
						//console.log();
					}
				}
			]
		});
		
		var $filterTable = $("<table class='filterTable'>");
		$filterTable.appendTo($dialog);
		
		//create filter options
		var fieldOptions = "";
		for(var foi=0; foi<fields.length; foi++){
			fieldOptions += "<option data-type='"+ fields[foi]["type"] +"'>"+ fields[foi]["name"] +"</option>";
		}
		
		//create comparator options
		var comparatorOptions = 
			"<option value='=='>=</option>"+
			"<option value='<'><</option>"+
			"<option value='<='><=</option>"+
			"<option value='>'>></option>"+
			"<option value='>='>>=</option>"+
			"<option value='!='><></option>";
			
		//create logical options
		var logicalOptions = 
			"<option value='&&'>and</option>"+
			"<option value='||'>or</option>";
		
		
		$filterTable.append("<tr>" +
			"	<th>Field</th>" +
			"	<th>Comparator</th>" +
			"	<th>Value</th>" +
			"	<th></th>" +
			"	<th></th>" +
			"</tr>");
			
		$filterTable.find("th").css({ fontWeight: "bold" });
		
		for(var fi=0; fi<filters.length; fi++){
			/*
			dataType: $el.find(".fieldList option:selected").attr("data-type"),
				field: $el.find(".fieldList").val(), 
				comparator: $el.find(".comparatorList").val(), 
				value: $el.find(".valueInput").val(), 
				logicalRelation: $el.find(".logicalList").val() 
			*/
			var filter = filters[fi];
			
			var $row = $("<tr class='data'>");
			$row.appendTo($filterTable);
			
			var $fieldCell = $("<td>");
			$fieldCell.appendTo($row);
			
			var $fieldList = $("<select class='fieldList'>");
			$fieldList.appendTo($fieldCell);
			$fieldList.append(fieldOptions);
			$fieldList.val(filter["field"]);
			
			var $comparisonCell = $("<td>");
			$comparisonCell.appendTo($row);
			
			var $comparatorList = $("<select class='comparatorList'>");
			$comparatorList.appendTo($comparisonCell);
			$comparatorList.append(comparatorOptions);
			$comparatorList.val(filter["comparator"]);
			
			var $valueCell = $("<td>");
			$valueCell.appendTo($row);
			
			var $valueInput = $("<input class='valueInput'>");
			$valueInput.appendTo($valueCell);
			$valueInput.val(filter["value"]);
			if(filter["type"] == "Numeric"){
				$valueInput.spinner();
			}else if(filter["type"] == "Date"){
				$valueInput.datepicker();
			}else if(filter["type"] == "Text"){
				$valueInput.datepicker("destroy");
				$valueInput.spinner("destroy");
				$valueInput.autocomplete({ source: [] });
			}
			
			var $logicalCell = $("<td>");
			$logicalCell.appendTo($row);
			
			var $logicalList = $("<select class='logicalList'>");
			$logicalList.appendTo($logicalCell);
			$logicalList.append(logicalOptions);
			$logicalList.val(filter["logicalRelation"]);
			
			var $deleteCell = $("<td>");
			$deleteCell.appendTo($row);
			var $deleteButton = $("<button title='Delete filter' class='removeFilter'><span class='fa fa-remove'>");
			$deleteCell.append($deleteButton);
		}
		
		var $addFilter = $("<button title='Add filter'><span class='fa fa-plus'>");
		$addFilter.appendTo($dialog);
		
		$addFilter.on("click", function(e){
			
			var $row = $("<tr class='data'>");
			$row.appendTo($filterTable);
			
			var $fieldCell = $("<td>");
			$fieldCell.appendTo($row);
			
			var $fieldList = $("<select class='fieldList'>");
			$fieldList.appendTo($fieldCell);
			$fieldList.append(fieldOptions);
			
			var $comparisonCell = $("<td>");
			$comparisonCell.appendTo($row);
			
			var $comparatorList = $("<select class='comparatorList'>");
			$comparatorList.appendTo($comparisonCell);
			$comparatorList.append(comparatorOptions);
			
			var $valueCell = $("<td>");
			$valueCell.appendTo($row);
			
			var $valueInput = $("<input class='valueInput'>");
			$valueInput.appendTo($valueCell);
			
			var $logicalCell = $("<td>");
			$logicalCell.appendTo($row);
			
			var $logicalList = $("<select class='logicalList'>");
			$logicalList.appendTo($logicalCell);
			$logicalList.append(logicalOptions);
			
			var $deleteCell = $("<td>");
			$deleteCell.appendTo($row);
			var $deleteButton = $("<button title='Delete filter' class='removeFilter'><span class='fa fa-remove'>");
			$deleteCell.append($deleteButton);
			
			
		});
		
		return $dialog;
	};
	
	
})();

$.fn.giPieChart = function(methodName, params){

	var toUpdateData = false;
	var toCheckUpdate = false;
	var createCheckUpdateTimer = false;
	
	var cancelRender = false;
	var affected = [];
	var initialized = false;

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": "expand", "Height": "auto" };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giPieChart")){
				
				initialized = true;
				toCheckUpdate = true;
				createCheckUpdateTimer = true;				
				
				$objEl.addClass("giObject giChart giPieChart");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giPieChart",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Object Type": "Pie Chart",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "auto",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "#000",
					"Corner Radius": 8,
					"Padding": 8,
					"Margin": 4,
					"Background Color": "#fff",
					"New Record Button": "show",
					"Legends Position": "bottom",
					"Auto Update Data": true,
					"Data Update Interval": 300,
					"Query": [],
					"Show Session Filters": true,
					"Data URL": "",
					"On Data Updated": null
				});
				
				$objEl.data("giSessionData", {
					"Chart Data": undefined,
					"Data Drill Fields": [[]],
					"Data Slice Fields": [[]],
					"Filters": [[]]
				});
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};

		this["get Query"] = function(){ return props["Query"]; };
		this["set Query"] = function(val){ 
			if(true){
				$objEl.data("giSessionData")["Data Drill Fields"] = [[]];
				$objEl.data("giSessionData")["Data Slice Fields"] = [[]];
				$objEl.data("giSessionData")["Chart Data"] = undefined;
				props["Query"] = val; 
				toUpdateData = true;
				
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
			
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? val : parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Padding"] = function(){ return props["Padding"]; }
		this["set Padding"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Padding"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Margin"] = function(){ return props["Margin"]; }
		this["set Margin"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Margin"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Background Color"] = function(){ return props["Background Color"]; }
		this["set Background Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Background Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
		this["set Corner Radius"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Corner Radius"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};			
		
		this["get Legends Position"] = function(){ return props["Legends Position"]; };
		this["set Legends Position"] = function(val){ 
			if(["top","left","bottom","right"].indexOf(val) > -1){
				props["Legends Position"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Data URL"] = function(){ return props["Data URL"]; };
		this["set Data URL"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Data URL"] = val; 
				toUpdateData = true;
			}else{
				return "Invalid value.";
			}
		};
		
		this["get New Record Button"] = function(){ return props["New Record Button"]; };
		this["set New Record Button"] = function(val){ 
			if(val === "show" || val === "hide"){
				props["New Record Button"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Auto Update Data"] = function(){ return props["Auto Update Data"]; }
		this["set Auto Update Data"] = function(val){
			var bools = { "true": true, "false": false };
			if(bools.hasOwnProperty(val.toString())){
				createCheckUpdateTimer = true;
				props["Auto Update Data"] = bools[val.toString()]; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Show Session Filters"] = function(){ return props["Show Session Filters"]; }
		this["set Show Session Filters"] = function(val){
			var bools = { "true": true, "false": false };
			if(bools.hasOwnProperty(val.toString())){
				props["Show Session Filters"] = bools[val.toString()]; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Data Update Interval"] = function(){ return props["Data Update Interval"]; }
		this["set Data Update Interval"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Data Update Interval"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			//clean updater
			var updaterInterval = $objEl.data("giDataUpdater");
			if(updaterInterval !== undefined){
				clearInterval(updaterInterval);
			}
			$objEl.remove();
		};
		
		this["get On Data Updated"] = function(){ return props["On Data Updated"]; }
		this["set On Data Updated"] = function(func){
			var getType = {};
			if(func === null || getType.toString.call(func) === '[object Function]'){
				props["On Data Updated"] = func; 
			}else{
				return "Invalid value.";
			}
		};	
		
		var context = this;
		this.createCheckUpdateTimer = function(){
			
			var updaterInterval = $objEl.data("giDataUpdater");
			
			if(props["Auto Update Data"] && (updaterInterval === undefined || updaterInterval === null)){
				updaterInterval = setInterval(context.checkUpdate, 5000);
				$objEl.data("giDataUpdater", updaterInterval);
			}else if(!props["Auto Update Data"]){
				clearInterval(updaterInterval);
				$objEl.data("giDataUpdater", null);
			}
			

		};
		
		this.checkUpdate = function(){
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			var updateInterval = $objEl.data("giProperties")["Data Update Interval"];
			
			//check if no data or if data is outdated
			if(chartData === undefined || chartData === null ||
				((new Date() - new Date(chartData.timeGenerated * 1000)) / 1000 > updateInterval)){
				context.updateData();
			}
		};
		
		this.updateData = function(){
			var updateXhr = $objEl.data("giSessionData")["Update XHR"];
			
			if(toUpdateData && updateXhr !== undefined && updateXhr !== null){
				updateXhr.abort();
				updateXhr = null;
			}
			
			if(updateXhr === undefined || updateXhr === null || updateXhr.readyState === 4) {
				
				var newUpdateXhr = $.ajax({
					type: "POST",
					url: props["Data URL"] === ""? "/modules/gi_data_opt/gi-get-chart-data.php" : props["Data URL"],
					dataType: "json",
					data: {query: props["Query"]
					},
					success: function(result){
						$objEl.data("giSessionData")["Chart Data"] = result;
						
						
						if(props["On Data Updated"] !== null){		
							var dataTable = $objEl.data("giSessionData")["Chart Data"]["dataTable"][0];

							var query1 = props["Data URL"] == "" ? props["Query"][0] : $objEl.data("giSessionData")["Chart Data"]["giqs"][0];
							
							//get categories
							var cats = [];
							for(var j=0; j<query1["groupings"].length; j++){
								cats.push(query1["groupings"][j]["alias"]);
							}
							for(var j=0; j<query1["optionalGroupings"].length; j++){
								cats.push(query1["optionalGroupings"][j]["alias"]);
							}
							
							//get row where all categories are null (grand total)
							var grandTotalRow = null;
							for(var i=(dataTable.length-1); i>=0; i--){
								var row = dataTable[i];
								var allNull = true;
								for(var j=0; j<cats.length; j++){
									if(row[cats[j]] !== null){
										allNull = false;
										break;
									}
								}
								if(allNull){
									grandTotalRow = row;
									break;
								}
							}
					
							props["On Data Updated"]({ "Grand Total": grandTotalRow });
						}
						
						$objEl[props["Identifier Name"]]("render");
					}
				});	
				
				$objEl.data("giSessionData")["Update XHR"] = newUpdateXhr;
			}
			
			
		};
		
		this.render = function(){
		
			var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", props["Width"] == "expand" ? "auto": props["Width"]);
			$objEl.css("height", props["Height"]);
			$objEl.css("min-height", props["Height"] === "auto" ? 20: "");
			$objEl.css("position", props["Positioning"] === "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);
			if(props["Positioning"] === "auto"){ $objEl.css("margin", props["Margin"]); }
			else{ $objEl.css("margin", 0); }
			
			var $border = $objEl.children("div.border");
			if($border.size() === 0){
				$border = $("<div>");
				$border.appendTo($objEl);
			}
			
			$border.html("");
			$border.removeAttr("style");
			$border.addClass("border");
			$border.css("border-style", "solid");
			$border.css("background-color", props["Background Color"]);
			$border.css("border-width", props["Border Thickness"]);
			$border.css("border-color", props["Border Color"]);
			$border.css("padding", props["Padding"]);
			$border.css("border-radius", props["Corner Radius"]);
			var outerThickness = props["Border Thickness"] + props["Padding"];
			if(props["Height"] !== "auto"){
				$border.css({ "height": $objEl.height() - outerThickness * 2 });
			}
			
			var $toolBar = $border.children(".toolBar"); 
			if($toolBar.size() == 0){
				$toolBar = $("<div>");
				$toolBar.addClass("toolBar giNoPrint");
				$border.append($toolBar);
			}
			$toolBar.html("");
			//$toolBar.css("margin-bottom", "8px");
			
			//console.log("rendering...");
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			
			if(chartData !== undefined && chartData !== null && chartData["dataTable"][0].length > 0){
				//console.log("has chart data");
				//get slicers info 
				var query1 = props["Data URL"] == "" ? props["Query"][0] : chartData["giqs"][0];
					
				var cats = 
					$.Enumerable
					.From(query1["groupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var optCats = 
					$.Enumerable
					.From(query1["optionalGroupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var drilledFields = $objEl.data("giSessionData")["Data Drill Fields"][0];
				
				var allDrilledFields = cats.concat(drilledFields);
								
				var newDrillFields = 
					$.Enumerable
					.From(optCats)
					.Where(function(i){ return allDrilledFields.indexOf(i) === -1; })
					.ToArray();
				
				var sliceFields = $objEl.data("giSessionData")["Data Slice Fields"][0];
				
				//prepare session filters
				var sessFilters = $objEl.data("giSessionData")["Filters"][0];
				var sessFilterConditions = "";
				for(var sfi=0, l=sessFilters.length; sfi<l; sfi++){
					
					var sessFilter = sessFilters[sfi];
					var val = "";
					
					if(sessFilter["dataType"] == "Numeric"){
						val = sessFilter["value"];
					}else if(sessFilter["dataType"] == "Text"){
						val = "'" + sessFilter["value"] + "'";
					}else if(sessFilter["dataType"] == "Date"){
						val = "new Date('" + sessFilter["value"] + "')";
					}
					
					var condition = "(row['"+ sessFilter["field"] +"']"+ 
					sessFilter["comparator"] +
					val +
					"||row['"+ sessFilter["field"] +"']==null)";
					
					if(sfi<l-1){
						condition += sessFilter["logicalRelation"];
					}
					sessFilterConditions += condition;
				}
				
				var sessFilterFunc = null;
				if(sessFilterConditions !== ""){
					sessFilterConditions = "(function(row){ return (" + sessFilterConditions + "); })";
					sessFilterFunc = eval(sessFilterConditions);
				}
				
				
				//display navigation buttons
				if(drilledFields.length > 0){
					var $rollUp = $("<button>");
					$rollUp.addClass("rollUpButton, button");
					$rollUp.text("Back");
					$toolBar.append($rollUp);
					$rollUp.on("click", function(e){
						drilledFields.pop();
						allDrilledFields.pop();
						if(allDrilledFields.length > 0){
							//console.log(allDrilledFields[allDrilledFields.length-1]);
							var removeSlice = allDrilledFields[allDrilledFields.length-1];
							for(var dsi=0; dsi<sliceFields.length; dsi++){
								//console.log(sliceFields[dsi].Field);
								if(sliceFields[dsi].Field === removeSlice){
									sliceFields.splice(dsi, 1);
								}
							}
						}
						$objEl[$objEl.data("giIdentifierName")]("render");
					});

				}
				
				
				if(allDrilledFields.length == 0 && newDrillFields.length > 0){
					var $drillDown = $("<button>");
					$drillDown.addClass("drillDownButton, button");
					$drillDown.text("Drill-down");
					$toolBar.append($drillDown);
					$drillDown.on("click", function(e){
						var dataDrillFields = $objEl.data("giSessionData")["Data Drill Fields"][0];
						dataDrillFields.push(newDrillFields[0]);
						$objEl[$objEl.data("giIdentifierName")]("render");
					});
				}
				
				//additional filters
				if(props["Show Session Filters"]){
					var $filter = $("<button><span class='fa fa-filter'></span>");
					$filter.attr("title", "Filter data.");
					$filter.addClass("button");
					$toolBar.append($filter);
					$filter.on("click", function(e){ 
						var $filterDialog = giChartComponents.generateFilterDialog
							({ objEl: $objEl, 
							filters: $objEl.data("giSessionData")["Filters"][0], 
							fields: chartData["fieldsInfo"][0] });
						$filterDialog.dialog("open");
					});
				}
				
				//manual check of updated data
				if(!props["Auto Update Data"]){
					var $checkUpdate = $("<button><span class='fa fa-refresh'></span>");
					$checkUpdate.attr("title", "Check for updated data.");
					$checkUpdate.addClass("button");
					$toolBar.append($checkUpdate);
					$checkUpdate.on("click", function(e){
						context.updateData();
					});
				}
				
				//fill out form button
				if(props["New Record Button"] === "show" && query1["entities"].length > 0){
					var chartForms = [];
					for(var qei=0; qei<query1["entities"].length; qei++){
						var entity = query1["entities"][qei];
						//console.log(entity.formName + " " + entity.formId);
						if(entity["formId"] !== undefined && !isNaN(entity["formId"])){
							chartForms.push({ "name": entity.formName, "id":entity.formId});
						}
					}
					
					var $fillOutFormBtn = giChartComponents.generateFillOutFormButton({
						"chartForms": [chartForms]
					});
				
					$toolBar.append($fillOutFormBtn);
				}
				
				var dataTable = [];
				var origDataTable = chartData["dataTable"][0];
				var startExecTime = Date.now();
				for(var odti=0; odti<origDataTable.length; odti++){
					var row = origDataTable[odti];
					//should pass all slice
					var slicePassed = true;
					for(var si=0; si<sliceFields.length; si++){
						var sliceField = sliceFields[si];
						if(sliceField.Values.indexOf(row[sliceField.Field]) == -1){
							slicePassed = false;
						}
					}
					
					//all drilled fields should not be null
					var allDrillFieldsNotNull = true;
					for(var di=0; di<allDrilledFields.length; di++){
						var drilledField = allDrilledFields[di];
						if(row[drilledField] === null){
							allDrillFieldsNotNull = false;
						}
					}
					
					//all non drilled fields should be null
					var allNonDrillFieldsNull = true;
					for(var ndi=0; ndi<newDrillFields.length; ndi++){
						var newDrillField = newDrillFields[ndi];
						if(row[newDrillField] !== null){
							allNonDrillFieldsNull = false;
						}
					}
					
					var sessFilterPassed = true;
					if(sessFilterFunc !== null){
						var sessFilterPassed = sessFilterFunc(row);
					}
					
					if(slicePassed && allDrillFieldsNotNull && allNonDrillFieldsNull && sessFilterPassed){
						dataTable.push(row);
					}
				}
				
				if(dataTable.length == 0){
					return;
				}
				
				//display pie
				var allDrilledFieldsCount = allDrilledFields.length;
				var newDrillFieldsCount = newDrillFields.length;
				
				var $dividerTable = $border.find(".dividerTable");
				if($dividerTable.size() === 0){
					$dividerTable = $("<table>");
					$border.append($dividerTable);
				}				
				$dividerTable.addClass("dividerTable");
				$dividerTable.attr("style", "");
				$dividerTable.css({ "width": "100%" });
				$dividerTable.html("");
				
				var $chartSection = null;
				var $legendsSection = null;
				if(["top", "bottom"].indexOf(props["Legends Position"]) > -1){
					var $row1 = $("<tr>"); 
					$dividerTable.append($row1);
					
					var $cell1 = $("<td>"); 
					$row1.append($cell1); 
					$cell1.addClass(props["Legends Position"] === "top" ? 
						"legendsSection": "chartSection");
					
					var $row2 = $("<tr>"); 
					$dividerTable.append($row2);
					
					var $cell2 = $("<td>"); 
					$row2.append($cell2); 
					
					$cell2.addClass(props["Legends Position"] === "top" ? 
						"chartSection": "legendsSection");
					
					$legendsSection = props["Legends Position"] === "top" ? $cell1 : $cell2;
					$chartSection = props["Legends Position"] === "top" ? $cell2 : $cell1;
					
				}else if(["left", "right"].indexOf(props["Legends Position"]) > -1){
					var $row = $("<tr>");
					$dividerTable.append($row);
					
					var $cell1 = $("<td>");
					$cell1.addClass(props["Legends Position"] === "left" ? 
						"legendsSection": "chartSection");
					$row.append($cell1);
					
					var $cell2 = $("<td>");
					$cell2.addClass(props["Legends Position"] === "left" ? 
						"chartSection": "legendsSection");
					$row.append($cell2);
					
					$legendsSection = props["Legends Position"] === "left" ? $cell1 : $cell2;
					$chartSection = props["Legends Position"] === "left" ? $cell2 : $cell1;
				}
				
				$legendsSection.css({ "vertical-align": "middle" });
				
				//get legends
				var $legends = $legendsSection.find(".legends");
				if($legends.size() === 0){
					$legends = $("<div>");
					$legendsSection.append($legends);
				}
				$legends.addClass("legends");
				$legends.css({ "line-height": "1.2em" });
				$legends.html("");
				
				//get pie categories, aggregate value and grand total
				var pieSlices = [];
				var grandTotal = 0;
				
				var firstAggAlias = query1["aggregates"][0]["alias"];
				var colorGen = new GiChartColors();
				
				for(var ri=0; ri<dataTable.length; ri++){
					var dRow = dataTable[ri];
					grandTotal += dRow[firstAggAlias];
					var pieSlice = { row: dataTable[ri], categories: [], value: dRow[firstAggAlias], color: colorGen.next() };
				
					for(var ci=0; ci<allDrilledFields.length; ci++){
						pieSlice["categories"].push(dRow[allDrilledFields[ci]]);
					}
					pieSlices.push(pieSlice);
				}
				
				//get percentage of each row
				for(var ri=0; ri<pieSlices.length; ri++){
					var pieSlice = pieSlices[ri];
					pieSlice["percentage"] = (100 / grandTotal) * pieSlice["value"];
				}
				
				//display legends
				var legendWidth = 0;
				for(var ri=0; ri<pieSlices.length; ri++){
					var pieSlice = pieSlices[ri];
					var $legend = $("<div>");
					$legend.addClass("legend");
					$legend.css({ "display": "inline-block", "margin-right": 8 });
					
					var $legendColor = $("<span>");
					$legendColor.css({ "display": "inline-block", 
						"width": "1em", "height": "1em",
						"background-color": "#" + pieSlice.color,
						"margin-right": 2
					});
					$legend.append($legendColor);
					
					$legend.append
						(pieSlice["categories"].length > 0 ? pieSlice["categories"][pieSlice["categories"].length-1] : "All");
						
						
					$legends.append($legend);
					legendWidth = legendWidth < $legend.outerWidth(true) ? $legend.outerWidth(true) : legendWidth;
				}
				
				$legends.children(".legend").css({ "width": legendWidth });
				
				//adjust sections' dimension
				if(["top", "bottom"].indexOf(props["Legends Position"]) > -1){
					$chartSection.append("<span class='cellFiller'>");
					$legendsSection.css("height", 1);
					$legendsSection.css("height", $legendsSection.height()); //get/set the computed
										
					if(props["Height"] === "auto"){
						$chartSection.css("height", "230");
					}else{
						var objElRect = $objEl[0].getBoundingClientRect();
						var dividerTableRect = $dividerTable[0].getBoundingClientRect();
						var dividerTableHeight = props["Height"] - (dividerTableRect.top - objElRect.top) - props["Padding"];
						$dividerTable.css("height", dividerTableHeight);
					}
				}else if(["left", "right"].indexOf(props["Legends Position"]) > -1){
					$chartSection.append("<span class='cellFiller'>");
					$legendsSection.css("width", 1);
					$legendsSection.css("width", $legendsSection.width()); //get/set the computed
					
					if(props["Height"] === "auto"){
						$chartSection.css("height", "230");
					}else{
						var objElRect = $objEl[0].getBoundingClientRect();
						var dividerTableRect = $dividerTable[0].getBoundingClientRect();
						var dividerTableHeight = props["Height"] - (dividerTableRect.top - objElRect.top) - props["Padding"];
						var legendsHeight = $legends.outerHeight(true) + 4;
						$chartSection.css("height", dividerTableHeight);
						$legends.css("height", legendsHeight > dividerTableHeight ? dividerTableHeight : legendsHeight);
						$legends.css("overflow-y", "auto");
						$legends.css("overflow-x", "hidden");
					}
				}
				$chartSection.find(".cellFiller").remove();
				
				//add some space between sections
				if(props["Legends Position"] === "top"){
					$legendsSection.css("padding-bottom", 8);
				}else if(props["Legends Position"] === "bottom"){
					$legendsSection.css("padding-top", 8);
				}else if(props["Legends Position"] === "left"){
					$legendsSection.css("padding-right", 8);
				}else if(props["Legends Position"] === "right"){
					$legendsSection.css("padding-left", 8);
				}
				
				//display chart itself
				var svgNs = "http://www.w3.org/2000/svg";
				var $chartCanvas = $("<div class='chartCanvas'>");
				
				$chartSection.html("");
				$chartSection.append($chartCanvas);
				$chartCanvas.css
					({ "position": "relative",
					"width" : $chartSection.width(), 
					"height": $chartSection.height() });
				
				var getPieSliceUiInfo = function(angle, radius){

					angle = angle >= 360? 359.9: angle;
					var pieSliceInfo = { };

					var radian = Math.PI / 180;

					/* Step 1: get the b of the first triangle */
					var t1A = 180-90-(angle/2);
					var t1b = radius * (Math.cos(t1A * radian));
					var t1B = (180-angle)/2;

					/* Step 2: get the a and b of the second triangle which is also the x and y of the arc path */
					var t2c = t1b * 2;

					var t2B = 90-t1B;
					var t2A = 180-t2B-90;

					var t2a = t2c * (Math.sin(t2A * radian));
					var t2b = t2c * (Math.cos(t2A * radian));

					pieSliceInfo.x = t2a;
					pieSliceInfo.y = t2b;
					
					pieSliceInfo.arcSweepFlags = angle < 180 ? "0 1": "1 1";
					pieSliceInfo.line = angle < 360 ? 'L ' + (radius) + ' ' + (radius) : "";
					
					return pieSliceInfo;
				};
				
				var diameter = ($chartCanvas[0].offsetHeight < $chartCanvas[0].offsetWidth ? 
					$chartCanvas[0].offsetHeight - 30 : $chartCanvas[0].offsetWidth - 30) ;
				var radius = diameter / 2;
				
				var nextPieSliceRotation = 0;
				
				for(var pi=0; pi<pieSlices.length; pi++){
					var pieSlice = pieSlices[pi];
					
					var piePieceAngle = (360 / 100) * pieSlice["percentage"];
					piePieceAngle = piePieceAngle > 359 ? 359.95 : piePieceAngle;
					//console.log(piePieceAngle);
					
					var piePieceSvg = document.createElementNS(svgNs, "svg");
					var pieLeft = (($chartCanvas[0].offsetWidth / 2) - radius);
					piePieceSvg.setAttribute
						("style", "position:absolute;" + 
						"top:15px;" + 
						"left:" + pieLeft + ";" +
						"width:" + diameter + ";" + 
						"height:" + diameter + ";" + 
						"transform: rotate("+ nextPieSliceRotation + "deg);" + 
						"-webkit-transform: rotate("+ nextPieSliceRotation + "deg);" + 
						"-ms-transform: rotate("+ nextPieSliceRotation + "deg);" + 
						"pointer-events: none;");
					
					
					var pieSliceId = "gi" + ((new Date()).getTime()) + "" + parseInt(Math.random()*1000);
					
					var piePiecePath = document.createElementNS(svgNs, "path");
					piePiecePath.setAttribute("id", pieSliceId);
					piePiecePath.setAttribute("class", "hoverShadow");
					
					piePiecePath.setAttribute("style", "pointer-events: visible;");
										
					if(newDrillFieldsCount > 0 && 
					allDrilledFieldsCount > 0){
								
						(function(colName, colValue, newDrillField){
							$(piePiecePath).on("click", function(e){
								
								$objEl.data("giSessionData")["Data Slice Fields"][0].push({ Field: colName, Values: [colValue] });
								
								$objEl.data("giSessionData")["Data Drill Fields"][0].push(newDrillField);
								
								$objEl[$objEl.data("giIdentifierName")]("render");
							});
						})(allDrilledFields[allDrilledFields.length-1], 
							pieSlice["row"][allDrilledFields[allDrilledFields.length-1]], 
							newDrillFields[0]);
					}
					
					//slice labels
					var labelPos = (function(startAngle, angle, radius, ext){
						var lineAngle = startAngle + (angle / 2);
						var hyp = radius + ext;
						var radian = Math.PI / 180;
						
						var labelPos = { x: 0, y: 0, xOffset: 0, yOffset: 0 };
						if(lineAngle == 0 || lineAngle == 360){
							labelPos.x = radius;
							labelPos.y = -ext;
							labelPos.xOffset = -1;
							labelPos.yOffset = -2;
						}else if(lineAngle == 90){
							labelPos.x = (radius * 2) + ext;
							labelPos.y = radius;
							labelPos.xOffset = 0;
							labelPos.yOffset = -1;
						}else if(lineAngle == 180){
							labelPos.x = radius;
							labelPos.y = (radius * 2) + ext;
							labelPos.xOffset = -1;
							labelPos.yOffset = 0;
						}else if(lineAngle == 270){
							labelPos.x = -ext;
							labelPos.y = radius;
							labelPos.xOffset = -2;
							labelPos.yOffset = -1;
						}else if(lineAngle > 0 && lineAngle < 90){
							var rtAngle = 90 - lineAngle;
							labelPos.x = radius + (Math.cos(rtAngle * radian) * hyp);
							labelPos.y = radius - (Math.sin(rtAngle * radian) * hyp);
							labelPos.xOffset = 0;
							labelPos.yOffset = -2;
						}else if(lineAngle > 90 && lineAngle < 180){
							var rtAngle = lineAngle - 90;
							labelPos.x = (Math.cos(rtAngle * radian) * hyp) + radius;
							labelPos.y = (Math.sin(rtAngle * radian) * hyp) + radius;
							labelPos.xOffset = 0;
							
						}else if(lineAngle > 180 && lineAngle < 270){
							var rtAngle = 270 - lineAngle;
							labelPos.x = radius - (Math.cos(rtAngle * radian) * hyp);
							labelPos.y = radius + (Math.sin(rtAngle * radian) * hyp);
							labelPos.xOffset = -2;
							labelPos.yOffset = 0;
						}else if(lineAngle > 270 && lineAngle < 360){
							var rtAngle = lineAngle - 270;
							labelPos.x = radius - (Math.cos(rtAngle * radian) * hyp);
							labelPos.y = radius - (Math.sin(rtAngle * radian) * hyp);
							labelPos.xOffset = -2;
							labelPos.yOffset = -2;
						}
						
						return labelPos;
					})(nextPieSliceRotation, piePieceAngle, radius, 2);
					//console.log(labelPos);
					
					if(piePieceAngle >= 10){
						var $label = $("<span>");
						$label.text(pieSlice["percentage"].toLocaleString('en-IN', { maximumSignificantDigits: 2 }) + "%");
						$chartCanvas.append($label);
						
						$label.css({
							"position": "absolute",
							"top": 15 + labelPos.y + (labelPos.yOffset * ($label.outerHeight() / 2)),
							"left": pieLeft + labelPos.x + (labelPos.xOffset * ($label.outerWidth() / 2)),
							"z-index": 9999,
							"text-shadow": "-1px 0 white, 0 1px white, 1px 0 white, 0 -1px white", 
							"pointer-events": "none"
						});
					}
					
					//tooltip
					(function(pieSliceId, val, color, cat){
						$(piePiecePath).on("mouseenter", function(e){
							var $toolTip = $("<div>");
							$toolTip.attr("id", pieSliceId + "tt");
							$toolTip.appendTo("body");
							
							$toolTip.addClass("giChartToolTip");
							$toolTip.css
								({ "position": "absolute", 
								"padding": 8,
								"background" : "#fff",
								"border-color": "#"+color,
								"border-style": "solid",
								"border-width": 1,
								"border-radius": 4,
								"z-index": 9999999 });
							
							$toolTip.append("<span style='font-weight:bold'>" + cat + "</span>: ");
							$toolTip.append(Number(val).toLocaleString());
							
								
							//set coordinates after determining outer width and height
							$toolTip.css
								({ "top": e.clientY+$("body").scrollTop()-$toolTip.outerHeight()-8, 
								"left": e.clientX-($toolTip.outerWidth()/2) });
						});
						
						$(piePiecePath).on("mousemove", function(e){
							var $toolTip = $("#" + pieSliceId + "tt");
							$toolTip.css
								({ "top": e.clientY+$("body").scrollTop()-$toolTip.outerHeight()-8, 
								"left": e.clientX-($toolTip.outerWidth()/2) });
						});
						
						$(piePiecePath).on("mouseleave", function(e){
							var $toolTip = $("#" + pieSliceId + "tt");
							$toolTip.remove();
						});
						
						$(piePiecePath).on("remove", function(e){
							var $toolTip = $("#" + pieSliceId + "tt");
							$toolTip.remove();
						});
					})(pieSliceId, pieSlice["value"], pieSlice.color, pieSlice["categories"].length > 0 ? pieSlice["categories"][pieSlice["categories"].length-1] : "All");
					
					//Rowi
					//set piePieceAnge = 0 if value is NaN
					piePieceAngle = (isNaN(piePieceAngle)?0:piePieceAngle)
					//End

					var pieSliceUiInfo = getPieSliceUiInfo(piePieceAngle, radius);
					console.log(pieSliceUiInfo)
					piePiecePath.setAttribute("d", 
						'M ' + radius + ' ' + 0 + ' ' + 
						'a ' + radius + ' ' + radius + ' 0 ' + 
						pieSliceUiInfo.arcSweepFlags + ' ' + pieSliceUiInfo.x + ' ' + pieSliceUiInfo.y + ' ' +
						pieSliceUiInfo.line +  ' z');
					piePiecePath.setAttribute("fill", "#" + pieSlice.color);
						
					piePieceSvg.appendChild(piePiecePath);
					$chartCanvas[0].appendChild(piePieceSvg);
					
					nextPieSliceRotation += piePieceAngle;
				}
				
			}
			
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined && methodName !== "render"){
		methodResult = objCore[methodName](params);
	}
	
	if(toUpdateData){
		objCore.updateData();
	}
	
	if(toCheckUpdate){
		objCore.checkUpdate();
	}

	if(createCheckUpdateTimer){
		objCore.createCheckUpdateTimer();
	}
	
	if(!cancelRender){ 
		objCore.render(); 
	}
	
	return { "methodResult": methodResult, "affected": affected };
};




})(jQuery);