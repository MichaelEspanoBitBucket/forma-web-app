(function($){

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

var giChartComponents = new (function(){
	
	this.generateFillOutFormButton = function(params){
		var buttonId = "gi-temp-"+(new Date().getTime());
		
		var $button = $("<button id='"+buttonId+"' class='button' title='New Request'><span class='fa fa-plus'></span></button>");
		
		var chartForms = params["chartForms"];
	
		var menuItems = {};
		for(var j=0; j<chartForms.length; j++){
			for(var i=0; i<chartForms[j].length; i++){
			
				menuItems[chartForms[j][i]["id"]] = {"name": chartForms[j][i]["name"]};
			}
		}
		
		//menuItems = { "1": {"name": "Under maintenance"}};
		
		$.contextMenu({
			selector: "#"+buttonId, 
			trigger: 'left',
			callback: function(key, options) {
				 window.open("/user_view/workspace?view_type=request&formID="+key+"&requestID=0", '_blank').focus();
			},
			items: menuItems
		});
		
		
		return $button;
	};
	
	this.generateFilterDialog = function(params){
		var filters = params.filters;
		var fields = params.fields;
		var $objEl = params.objEl;
		
		var $dialog = $("#giChartFilterDialog");
		if($dialog.size() == 0){
			$dialog = $("<div>");
			$dialog.attr("id", "giChartFilterDialog");
			
			$dialog.appendTo("body");
			$dialog.dialog({ modal: true, hide: true, width: 500, height: 300 });
			
			$dialog.on("click", ".removeFilter", function(e){
				$(e.currentTarget).closest("tr").remove();
			});
			
			$dialog.on("change", ".fieldList", function(e){
				var $fieldList = $(e.currentTarget);
				var type = $fieldList.find("option:selected").attr("data-type");
				var $valueInput = $fieldList.closest("tr").find(".valueInput");
				if(type == "Numeric"){
					//$valueInput.spinner();
				}else if(type == "Date"){
					//$valueInput.datepicker();
				}else if(type == "Text"){
					//$valueInput.datepicker("destroy");
					//$valueInput.spinner("destroy");
					//$valueInput.autocomplete({ source: [] });
				}
			});
		}else{
			$dialog.empty();
		}
		
		$dialog.append("<p>Note: This filter is temporary and will not be saved.</p>");
		
		$dialog.dialog({
			buttons: [
				{
					text: "Apply",
					click: function(e) {
						var $filterRows = $(this).find(".filterTable tr.data");
						
						var filters = [];
						$filterRows.each(function(i, el){
							var $el = $(el);
							var filter = { 
								dataType: $el.find(".fieldList option:selected").attr("data-type"),
								field: $el.find(".fieldList").val(), 
								comparator: $el.find(".comparatorList").val(), 
								value: $el.find(".valueInput").val(), 
								logicalRelation: $el.find(".logicalList").val() 
							};
							
							if(filter.value !== null && filter.value !== ""){
								filters.push(filter);
							}
						});
						
						$objEl.data("giSessionData")["Filters"] = [filters];
						$objEl[$objEl.data("giIdentifierName")]("render");
						
						$(this).dialog("close");
						//$objEl.data("giSessionData");
						
						//DELETE
						//console.log();
					}
				}
			]
		});
		
		var $filterTable = $("<table class='filterTable'>");
		$filterTable.appendTo($dialog);
		
		//create filter options
		var fieldOptions = "";
		for(var foi=0; foi<fields.length; foi++){
			fieldOptions += "<option data-type='"+ fields[foi]["type"] +"'>"+ fields[foi]["name"] +"</option>";
		}
		
		//create comparator options
		var comparatorOptions = 
			"<option value='=='>=</option>"+
			"<option value='<'><</option>"+
			"<option value='<='><=</option>"+
			"<option value='>'>></option>"+
			"<option value='>='>>=</option>"+
			"<option value='!='><></option>";
			
		//create logical options
		var logicalOptions = 
			"<option value='&&'>and</option>"+
			"<option value='||'>or</option>";
		
		
		$filterTable.append("<tr>" +
			"	<th>Field</th>" +
			"	<th>Comparator</th>" +
			"	<th>Value</th>" +
			"	<th></th>" +
			"	<th></th>" +
			"</tr>");
			
		$filterTable.find("th").css({ fontWeight: "bold" });
		
		for(var fi=0; fi<filters.length; fi++){
			/*
			dataType: $el.find(".fieldList option:selected").attr("data-type"),
				field: $el.find(".fieldList").val(), 
				comparator: $el.find(".comparatorList").val(), 
				value: $el.find(".valueInput").val(), 
				logicalRelation: $el.find(".logicalList").val() 
			*/
			var filter = filters[fi];
			
			var $row = $("<tr class='data'>");
			$row.appendTo($filterTable);
			
			var $fieldCell = $("<td>");
			$fieldCell.appendTo($row);
			
			var $fieldList = $("<select class='fieldList'>");
			$fieldList.appendTo($fieldCell);
			$fieldList.append(fieldOptions);
			$fieldList.val(filter["field"]);
			
			var $comparisonCell = $("<td>");
			$comparisonCell.appendTo($row);
			
			var $comparatorList = $("<select class='comparatorList'>");
			$comparatorList.appendTo($comparisonCell);
			$comparatorList.append(comparatorOptions);
			$comparatorList.val(filter["comparator"]);
			
			var $valueCell = $("<td>");
			$valueCell.appendTo($row);
			
			var $valueInput = $("<input class='valueInput'>");
			$valueInput.appendTo($valueCell);
			$valueInput.val(filter["value"]);
			if(filter["type"] == "Numeric"){
				//$valueInput.spinner();
			}else if(filter["type"] == "Date"){
				//$valueInput.datepicker();
			}else if(filter["type"] == "Text"){
				//$valueInput.datepicker("destroy");
				//$valueInput.spinner("destroy");
				//$valueInput.autocomplete({ source: [] });
			}
			
			var $logicalCell = $("<td>");
			$logicalCell.appendTo($row);
			
			var $logicalList = $("<select class='logicalList'>");
			$logicalList.appendTo($logicalCell);
			$logicalList.append(logicalOptions);
			$logicalList.val(filter["logicalRelation"]);
			
			var $deleteCell = $("<td>");
			$deleteCell.appendTo($row);
			var $deleteButton = $("<button title='Delete filter' class='removeFilter'><span class='fa fa-remove'>");
			$deleteCell.append($deleteButton);
		}
		
		var $addFilter = $("<button title='Add filter'><span class='fa fa-plus'>");
		$addFilter.appendTo($dialog);
		
		$addFilter.on("click", function(e){
			
			var $row = $("<tr class='data'>");
			$row.appendTo($filterTable);
			
			var $fieldCell = $("<td>");
			$fieldCell.appendTo($row);
			
			var $fieldList = $("<select class='fieldList'>");
			$fieldList.appendTo($fieldCell);
			$fieldList.append(fieldOptions);
			
			var $comparisonCell = $("<td>");
			$comparisonCell.appendTo($row);
			
			var $comparatorList = $("<select class='comparatorList'>");
			$comparatorList.appendTo($comparisonCell);
			$comparatorList.append(comparatorOptions);
			
			var $valueCell = $("<td>");
			$valueCell.appendTo($row);
			
			var $valueInput = $("<input class='valueInput'>");
			$valueInput.appendTo($valueCell);
			
			var $logicalCell = $("<td>");
			$logicalCell.appendTo($row);
			
			var $logicalList = $("<select class='logicalList'>");
			$logicalList.appendTo($logicalCell);
			$logicalList.append(logicalOptions);
			
			var $deleteCell = $("<td>");
			$deleteCell.appendTo($row);
			var $deleteButton = $("<button title='Delete filter' class='removeFilter'><span class='fa fa-remove'>");
			$deleteCell.append($deleteButton);
			
			
		});
		
		return $dialog;
	}
	
})();

$.fn.giRecordsTable = function(methodName, params){

	var toUpdateData = false;
	var toCheckUpdate = false;
	var createCheckUpdateTimer = false;
	
	var cancelRender = false;
	var affected = [];
	var initialized = false;

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": "expand", "Height": "auto" };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giRecordsTable")){
				
				initialized = true;
				toCheckUpdate = true;
				createCheckUpdateTimer = true;
				
				$objEl.addClass("giObject giChart giRecordsTable");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giRecordsTable",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Object Type": "Table Chart",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "auto",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "#000",
					"Corner Radius": 8,
					"Padding": 8,
					"Margin": 4,
					"Background Color": "#fff",
					"Header Background": "#66CCFF",
					"Header Text Color": "#fff",
					"Row Background": "#fff",
					"Row Alternating Background": "#F0FAFF",
					"Super Aggregates": "show",
					"Allow Export": false,
					"New Record Button": "show",
					"Auto Update Data": true,
					"Data Update Interval": 300,
					"Query": [],
					"Data URL": "",
					"On Data Updated": null
				});
				
				$objEl.data("giSessionData", {
					"Chart Data": undefined,
					"Data Drill Fields": [[]],
					"Data Slice Fields": [[]],
					"Filters": [[]]
				});
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};

		this["get Query"] = function(){ return props["Query"]; };
		this["set Query"] = function(val){ 
			if(true){
				$objEl.data("giSessionData")["Data Drill Fields"] = [[]];
				$objEl.data("giSessionData")["Data Slice Fields"] = [[]];
				$objEl.data("giSessionData")["Chart Data"] = undefined;
				props["Query"] = val; 
				toUpdateData = true;
				
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
			
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? val : parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Padding"] = function(){ return props["Padding"]; }
		this["set Padding"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Padding"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Margin"] = function(){ return props["Margin"]; }
		this["set Margin"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Margin"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Background Color"] = function(){ return props["Background Color"]; }
		this["set Background Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Background Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Header Background"] = function(){ return props["Header Background"]; }
		this["set Header Background"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Header Background"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Header Text Color"] = function(){ return props["Header Text Color"]; }
		this["set Header Text Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Header Text Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Row Background"] = function(){ return props["Row Background"]; }
		this["set Row Background"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Row Background"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Row Alternating Background"] = function(){ return props["Row Alternating Background"]; }
		this["set Row Alternating Background"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Row Alternating Background"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
		this["set Corner Radius"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Corner Radius"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};			
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Data URL"] = function(){ return props["Data URL"]; };
		this["set Data URL"] = function(val){ 
			if(val !== undefined && val !== null){
				toUpdateData = true;
				props["Data URL"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Allow Export"] = function(){ return props["Allow Export"]; }
		this["set Allow Export"] = function(val){
			var bools = { "true": true, "false": false };
			if(bools.hasOwnProperty(val.toString())){
				createCheckUpdateTimer = true;
				props["Allow Export"] = bools[val.toString()]; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get New Record Button"] = function(){ return props["New Record Button"]; };
		this["set New Record Button"] = function(val){ 
			if(val === "show" || val === "hide"){
				props["New Record Button"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Super Aggregates"] = function(){ return props["Super Aggregates"]; };
		this["set Super Aggregates"] = function(val){ 
			if(val === "show" || val === "hide"){
				props["Super Aggregates"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Auto Update Data"] = function(){ return props["Auto Update Data"]; }
		this["set Auto Update Data"] = function(val){
			var bools = { "true": true, "false": false };
			if(bools.hasOwnProperty(val.toString())){
				createCheckUpdateTimer = true;
				props["Auto Update Data"] = bools[val.toString()]; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Data Update Interval"] = function(){ return props["Data Update Interval"]; }
		this["set Data Update Interval"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Data Update Interval"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			//clean updater
			var updaterInterval = $objEl.data("giDataUpdater");
			if(updaterInterval !== undefined){
				clearInterval(updaterInterval);
			}
			$objEl.remove();
		};
		
		this["get On Data Updated"] = function(){ return props["On Data Updated"]; }
		this["set On Data Updated"] = function(func){
			var getType = {};
			if(func === null || getType.toString.call(func) === '[object Function]'){
				props["On Data Updated"] = func; 
			}else{
				return "Invalid value.";
			}
		};	
		
		var context = this;
		this.createCheckUpdateTimer = function(){
			
			var updaterInterval = $objEl.data("giDataUpdater");
			
			if(props["Auto Update Data"] && (updaterInterval === undefined || updaterInterval === null)){
				updaterInterval = setInterval(context.checkUpdate, 5000);
				$objEl.data("giDataUpdater", updaterInterval);
			}else if(!props["Auto Update Data"]){
				clearInterval(updaterInterval);
				$objEl.data("giDataUpdater", null);
			}
			

		};
		
		this.checkUpdate = function(){
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			var updateInterval = $objEl.data("giProperties")["Data Update Interval"];
			
			//check if no data or if data is outdated
			if(chartData === undefined || chartData === null ||
				((new Date() - new Date(chartData.timeGenerated * 1000)) / 1000 > updateInterval)){
				context.updateData();
			}
		};
		
		this.updateData = function(){
			var updateXhr = $objEl.data("giSessionData")["Update XHR"];
			
			if(toUpdateData && updateXhr !== undefined && updateXhr !== null){
				updateXhr.abort();
				updateXhr = null;
			}
			
			if(updateXhr === undefined || updateXhr === null || updateXhr.readyState === 4) {
				
				
				//queryWithAddedFilters[0][]
				
				var newUpdateXhr = $.ajax({
					type: "POST",
					url: props["Data URL"] === ""? "/ajax/gi-get-chart-data" : props["Data URL"],
					dataType: "json",
					data: {query: props["Query"]
					},
					success: function(result){
						$objEl.data("giSessionData")["Chart Data"] = result;
						
						if(props["On Data Updated"] !== null){		
							var dataTable = $objEl.data("giSessionData")["Chart Data"]["dataTable"][0];

							var query1 = props["Data URL"] == "" ? props["Query"][0] : $objEl.data("giSessionData")["Chart Data"]["giqs"][0];
							
							//get categories
							var cats = [];
							for(var j=0; j<query1["groupings"].length; j++){
								cats.push(query1["groupings"][j]["alias"]);
							}
							for(var j=0; j<query1["optionalGroupings"].length; j++){
								cats.push(query1["optionalGroupings"][j]["alias"]);
							}
							
							//get row where all categories are null (grand total)
							var grandTotalRow = null;
							for(var i=(dataTable.length-1); i>=0; i--){
								var row = dataTable[i];
								var allNull = true;
								for(var j=0; j<cats.length; j++){
									if(row[cats[j]] !== null){
										allNull = false;
										break;
									}
								}
								if(allNull){
									grandTotalRow = row;
									break;
								}
							}
					
							props["On Data Updated"]({ "Grand Total": grandTotalRow });
						}
						
						$objEl[props["Identifier Name"]]("render");
					}
				});	
				
				$objEl.data("giSessionData")["Update XHR"] = newUpdateXhr;
			}
			
			
		};
		
		this.render = function(){
		
			var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", props["Width"] == "expand" ? "auto": props["Width"]);
			$objEl.css("height", props["Height"]);
			$objEl.css("min-height", props["Height"] === "auto" ? 20: "");
			$objEl.css("position", props["Positioning"] === "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);
			if(props["Positioning"] === "auto"){ $objEl.css("margin", props["Margin"]); }
			else{ $objEl.css("margin", 0); }
			
			var $border = $objEl.children("div.border");
			if($border.size() === 0){
				$border = $("<div>");
				$border.appendTo($objEl);
			}
			
			$border.html("");
			$border.addClass("border");
			$border.css("border-style", "solid");
			$border.css("background-color", props["Background Color"]);
			$border.css("border-width", props["Border Thickness"]);
			$border.css("border-color", props["Border Color"]);
			$border.css("padding", props["Padding"]);
			$border.css("border-radius", props["Corner Radius"]);
			$border.css("position", props["Height"] === "auto" ? "static" : "absolute");
			$border.css({"top": 0, "left": 0, "right": 0, "bottom": 0 });
			
			var $toolBar = $border.children(".toolBar"); 
			if($toolBar.size() == 0){
				$toolBar = $("<div>");
				$toolBar.addClass("toolBar giNoPrint");
				$border.append($toolBar);
			}
			$toolBar.html("");
			//$toolBar.css("margin-bottom", "8px");
			
			//console.log("rendering...");
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			
			if(chartData !== undefined && chartData !== null && chartData["dataTable"][0].length > 0){
				//console.log("has chart data");
				//get slicers info 
				var query1 = props["Data URL"] == "" ? props["Query"][0] : chartData["giqs"][0];
					
				var cats = 
					$.Enumerable
					.From(query1["groupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var optCats = 
					$.Enumerable
					.From(query1["optionalGroupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var drilledFields = $objEl.data("giSessionData")["Data Drill Fields"][0];
				
				var allDrilledFields = cats.concat(drilledFields);
				
				var allCats = cats.concat(optCats);
				
				var newDrillFields = 
					$.Enumerable
					.From(optCats)
					.Where(function(i){ return allDrilledFields.indexOf(i) === -1; })
					.ToArray();
				
				var sliceFields = $objEl.data("giSessionData")["Data Slice Fields"][0];
				
				var aggFields = 
					$.Enumerable
					.From(query1["aggregates"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				

				//prepare session filters
				var sessFilters = $objEl.data("giSessionData")["Filters"][0];
				var sessFilterConditions = "";
				for(var sfi=0, l=sessFilters.length; sfi<l; sfi++){
					
					var sessFilter = sessFilters[sfi];
					var val = "";
					
					if(sessFilter["dataType"] == "Numeric"){
						val = sessFilter["value"];
					}else if(sessFilter["dataType"] == "Text"){
						val = "'" + sessFilter["value"] + "'";
					}else if(sessFilter["dataType"] == "Date"){
						val = "new Date('" + sessFilter["value"] + "')";
					}
					
					var condition = "(row['"+ sessFilter["field"] +"']"+ 
					sessFilter["comparator"] +
					val +
					"||row['"+ sessFilter["field"] +"']==null)";
					
					if(sfi<l-1){
						condition += sessFilter["logicalRelation"];
					}
					sessFilterConditions += condition;
				}
				
				var sessFilterFunc = null;
				if(sessFilterConditions !== ""){
					sessFilterConditions = "(function(row){ return (" + sessFilterConditions + "); })";
					sessFilterFunc = eval(sessFilterConditions);
				}
				
				var superAgg = null;
				
				var dataTable = [];
				var origDataTable = chartData["dataTable"][0];
				var startExecTime = Date.now();
				for(var odti=0; odti<origDataTable.length; odti++){
					
					var row = origDataTable[odti];
					
					//search for super aggregate row
					var isSuperAgg = true;
					for(var ci=(allCats.length-1); ci>=0; ci--){
						if(row[allCats[ci]] !== null){
							isSupAgg = false;
						}
					}
					
					if(isSuperAgg){
						superAgg = row;
					}
					
					//search for rows
					
					//should pass all slice
					var slicePassed = true;
					for(var si=0; si<sliceFields.length; si++){
						var sliceField = sliceFields[si];
						if(sliceField.Values.indexOf(row[sliceField.Field]) == -1){
							slicePassed = false;
						}
					}
					
					//all drilled fields should not be null
					var allDrillFieldsNotNull = true;
					for(var di=0; di<allDrilledFields.length; di++){
						var drilledField = allDrilledFields[di];
						if(row[drilledField] === null){
							allDrillFieldsNotNull = false;
						}
					}
					
					//all non drilled fields should be null
					var allNonDrillFieldsNull = true;
					for(var ndi=0; ndi<newDrillFields.length; ndi++){
						var newDrillField = newDrillFields[ndi];
						if(row[newDrillField] !== null){
							allNonDrillFieldsNull = false;
						}
					}
					
					var sessFilterPassed = true;
					if(sessFilterFunc !== null){
						var sessFilterPassed = sessFilterFunc(row);
					}
					
					if(slicePassed && allDrillFieldsNotNull && allNonDrillFieldsNull && sessFilterPassed){
						dataTable.push(row);
					}
					
					if(slicePassed && isSupAgg){
						superAgg = row;
					}
				}
				
				//console.log((Date.now() - startExecTime) * 0.001);
				if(dataTable.length == 0){
					return;
				}
				
				//display export
				if(props["Allow Export"]){
					var exportText = "";
					var regexBr = /<br\s*[\/]?>/gi;
					for(var i=0; i<dataTable.length; i++){
						var row = dataTable[i];
						if(i==0){
							//var colCount = Object.keys(obj).length;
							for(var k in row){
								exportText += k + ",";
							}
							exportText += "\n";
						}
						
						for(var k in row){
							exportText += '"' + row[k].toString().replace(regexBr, "\n") + '",';
						}
						exportText += "\n";
					}
					/*
					if(props["Super Aggregates"] === "show"){
						for(var k in row){
							exportText += (superAgg[k] === null ? "": superAgg[k])  + ",";
						}
					}*/
					
					var $exportData = $("<a>");
					$exportData.addClass("button");
					$exportData.text("Export");
					var exportFile = new Blob([exportText], {type: "text/plain"});
					$exportData.attr("href", URL.createObjectURL(exportFile));
					$exportData[0].download = "test.csv";
					
					$exportData.appendTo($toolBar);
				}
				
				//display navigation buttons
				if(drilledFields.length > 0){
					var $rollUp = $("<button>");
					$rollUp.addClass("rollUpButton, button");
					$rollUp.text("Back");
					$toolBar.append($rollUp);
					$rollUp.on("click", function(e){
						drilledFields.pop();
						allDrilledFields.pop();
						if(allDrilledFields.length > 0){
							//console.log(allDrilledFields[allDrilledFields.length-1]);
							var removeSlice = allDrilledFields[allDrilledFields.length-1];
							for(var dsi=0; dsi<sliceFields.length; dsi++){
								//console.log(sliceFields[dsi].Field);
								if(sliceFields[dsi].Field === removeSlice){
									sliceFields.splice(dsi, 1);
								}
							}
						}
						$objEl[$objEl.data("giIdentifierName")]("render");
					});

				}
				
				if(newDrillFields.length > 0){
					var $drillDown = $("<button>");
					$drillDown.addClass("drillDownButton, button");
					$drillDown.text("Drill-down");
					$toolBar.append($drillDown);
					$drillDown.on("click", function(e){
						var dataDrillFields = $objEl.data("giSessionData")["Data Drill Fields"][0];
						dataDrillFields.push(newDrillFields[0]);
						$objEl[$objEl.data("giIdentifierName")]("render");
					});
				}
				
				//additional filters
				var $filter = $("<button><span class='fa fa-filter'></span>");
				$filter.attr("title", "Filter data.");
				$filter.addClass("button");
				$toolBar.append($filter);
				$filter.on("click", function(e){ 
					var $filterDialog = giChartComponents.generateFilterDialog
						({ objEl: $objEl, 
						filters: $objEl.data("giSessionData")["Filters"][0], 
						fields: chartData["fieldsInfo"][0] });
					$filterDialog.dialog("open");
				});
				
				//manual check of updated data
				if(!props["Auto Update Data"]){
					var $checkUpdate = $("<button><span class='fa fa-refresh'></span>");
					$checkUpdate.attr("title", "Check for updated data.");
					$checkUpdate.addClass("button");
					$toolBar.append($checkUpdate);
					$checkUpdate.on("click", function(e){
						context.updateData();
					});
				}
				
				//fill out form button
				if(props["New Record Button"] === "show" && query1["entities"].length > 0){
					var chartForms = [];
					for(var qei=0; qei<query1["entities"].length; qei++){
						var entity = query1["entities"][qei];
						//console.log(entity.formName + " " + entity.formId);
						if(entity["formId"] !== undefined && !isNaN(entity["formId"])){
							chartForms.push({ "name": entity.formName, "id":entity.formId});
						}
					}
					
					var $fillOutFormBtn = giChartComponents.generateFillOutFormButton({
						"chartForms": [chartForms]
					});
				
					$toolBar.append($fillOutFormBtn);
				}
				
				//display rows
				var allDrilledFieldsCount = allDrilledFields.length;
				var newDrillFieldsCount = newDrillFields.length;
				
				var $tblScroller = $border.children("div.scroller");
				if($tblScroller.size() == 0){
					$tblScroller = $("<div>");
					$tblScroller.addClass("scroller");
					$tblScroller.appendTo($border);
				}
				$tblScroller.removeAttr("style");
				$tblScroller.css({"position": "relative" });
				
				if(props["Height"] !== "auto"){
					$tblScroller.css("overflow-y", "auto");
					$tblScroller.css("overflow-x", "hidden");
					var objElRect = $objEl[0].getBoundingClientRect();
					var scrollerRect = $tblScroller[0].getBoundingClientRect();
					//console.log("scrollerRect.top - objElRect.top = " + (scrollerRect.top - objElRect.top));
					var scrollerHeight = props["Height"] - (scrollerRect.top - objElRect.top) - props["Padding"];
					$tblScroller.css("height", scrollerHeight);
				}else{
					$tblScroller.css("overflow-y", "");
					$tblScroller.css("height", "auto");
				}
				
				var $tbl = $tblScroller.children("table.dataTable"); 
				if($tbl.size() == 0){
					$tbl = $("<table>");
					$tbl.appendTo($tblScroller);
				}
				$tbl.addClass("dataTable");
				$tbl.html("");
				$tbl.removeAttr("style");
				
				var previousCatCells = [];
				
				var $row = $("<tr>");
				$row.css({"background": props["Header Background"], 
					"color": props["Header Text Color"],
					"border": "solid 1px " + props["Header Background"] });
				for(var colName in row){
					if(newDrillFields.indexOf(colName) == -1){
						$row.append("<th style='word-break: break-all;'>" + colName + "</th>");
						$tbl.append($row);
					}
				}
				
				for(var dti=0; dti<dataTable.length; dti++){
					var row = dataTable[dti];
										
					var $row = $("<tr>");
					//$row.css("background", dti%2===0 ? props["Row Background"] :  props["Row Alternating Background"]);
					$tbl.append($row);
					
					var catsMatchedFromPrev = true;
					
					for(var ci=0; ci<allDrilledFields.length; ci++){
						var fieldName = allDrilledFields[ci];	
						
						var cat = dataTable[dti][fieldName];
						
						if(previousCatCells[ci] === undefined){
							previousCatCells.push({ cat: undefined, cell: undefined });
						}
					
						if(previousCatCells[ci]["cat"] !== dataTable[dti][fieldName]){
							catsMatchedFromPrev = false;
						}
						
						if(catsMatchedFromPrev){ 
							var prevRowSpan =  parseInt(previousCatCells[ci]["cell"].attr("rowspan"));
							previousCatCells[ci]["cell"].attr("rowspan", prevRowSpan + 1);
						}else{
							var $cell = $("<td>");
							$cell.append(cat === "" ? "&nbsp;" : cat);
							$cell.attr("rowspan", 1);
							$cell.appendTo($row);	
							$cell.attr("title", cat);
							$cell.css({ "border": "solid 1px #C0C0C0", "word-break": "break-all" });
							
							previousCatCells[ci]["cat"] = cat;
							previousCatCells[ci]["cell"] = $cell;
						}
					}
				
					for(var ai=0; ai<aggFields.length; ai++){
						var aggField = aggFields[ai];
						var $cell = $("<td></td>");
						$cell.css({ "border": "solid 1px #C0C0C0" });
						
						$cell.appendTo($row);
					
						$cell.text(row[aggField].toLocaleString());
						$cell.css("text-align", "right");
					}
					
				}
				
				if(superAgg !== null && props["Super Aggregates"] === "show"){
					
					var $row = $("<tr>");
					$tbl.append($row);
					
					if(allDrilledFields.length > 0){
						var $cell = $("<td>"+ (allDrilledFields.length > 1? "Totals": "Total") +"</td>");
						$cell.attr("colspan", allDrilledFields.length);
						$cell.css({ "border":"solid 1px #C0C0C0", "font-weight":"bold" });
						
						$cell.appendTo($row);
					}
					
					for(var ai=0; ai<aggFields.length; ai++){
						var aggField = aggFields[ai];
						var $cell = $("<td></td>");
						$cell.css({ "border":"solid 1px #C0C0C0", "font-weight":"bold" });
						
						$cell.appendTo($row);
					
						$cell.text(superAgg[aggField].toLocaleString());
						$cell.css("text-align", "right");
					}
				}
				
				/*
				if(superAggregate.row != null && (cats.length > 0 || optCats.length > 0)){
					var $row = $("<tr>");
					$tbl.append($row);
					for(var colName in row){
						if(newDrillFields.indexOf(colName) == -1){	
							var $cell = $("<td></td>");
							if(aggFields.indexOf(colName) > -1){
								$cell.text(superAggregate.row[colName].toLocaleString());
								$cell.css({ "text-align": "right", "font-weight": "bold"});
							}
							
							$row.append($cell);
						}
					}
				}*/
				
				$tbl.width($tbl.width());
				$tbl.find("th").each(function(i){
					$(this).width($(this).width());
					$(this).height($(this).height());
				});
				
				$border.children("table.headerTable").remove(); 
				
				var $headerTable = $($tbl[0].cloneNode(false));
				$headerTable.attr("class", "headerTable");
				$headerTable.insertBefore($tblScroller);
				$headerTable.css({ "position": "absolute", "top": $tblScroller.css("top"), "left": $tblScroller.css("left"), "z-index": 1, "height": 1 });
				
				var $frozenHeader = $tbl.find("tr:first").clone();
				$headerTable.append($frozenHeader);
				

			}else{
				$border.html("");
			}
			
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined && methodName !== "render"){
		methodResult = objCore[methodName](params);
	}
	
	if(toUpdateData){
		objCore.updateData();
	}
	
	if(toCheckUpdate){
		objCore.checkUpdate();
	}

	if(createCheckUpdateTimer){
		objCore.createCheckUpdateTimer();
	}
	
	if(!cancelRender){ 
		objCore.render(); 
	}
	
	return { "methodResult": methodResult, "affected": affected };
};




})(jQuery);