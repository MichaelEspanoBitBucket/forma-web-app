(function($){

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

$.fn.giEllipseShape = function(methodName, params){
	
	var cancelRender = false;
	var affected = [];
	var initialized = false;
	
	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": 200, "Height": 200 };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giEllipseShape")){
				
				initialized = true
				
				$objEl.addClass("giObject giShape giEllipseShape");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giEllipseShape",
					"Object Type": "Ellipse",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "manual",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "#000",
					"Fill": "#fff"
				});
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type": (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value for UID.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value for Name.";
			}
		};
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value for ID.";
			}
		};
			
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value for Ordinal.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
		
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? "auto" : parseInt(val); 
			}else{
				return "Invalid value for Height.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value for Positioning.";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value for Top.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value for Left.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value for Z-index.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value Border Thickness.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value for Border Color.";
			}
		};		
		
		this["get Fill"] = function(){ return props["Fill"]; }
		this["set Fill"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Fill"] = val; 
			}else{
				return "Invalid value for Fill.";
			}
		};	
		
		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			cancelRender = true;
			$objEl.remove();
			
			
			
		};
		
		this.render = function(){
			
			var svgNs = "http://www.w3.org/2000/svg";
			var props = $objEl.data("giProperties");
			
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", props["Width"] == "expand" ? "auto": props["Width"]);
			$objEl.css("height", props["Height"] === "auto" ? defaultProps["Height"] : props["Height"]);
			$objEl.css("position", props["Positioning"] == "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);
			
			var computedWidth = props["Width"] == "expand" ? $objEl[0].offsetWidth : props["Width"];
			var computedHeight = props["Height"] == "auto" ? defaultProps["Height"] : props["Height"];
			
			$objEl.find("svg").remove();
			var svg = document.createElementNS(svgNs, "svg");
			svg.setAttribute("style", "position: relative;");
			
			svg.setAttribute("width", computedWidth);	
			svg.setAttribute("height", computedHeight);	
			$objEl[0].appendChild(svg);
			
			var ellipse = document.createElementNS(svgNs, "ellipse");
			ellipse.setAttribute("cx", computedWidth / 2);
			ellipse.setAttribute("cy", computedHeight / 2);
			ellipse.setAttribute("rx", (computedWidth - props["Border Thickness"]) / 2 );
			ellipse.setAttribute("ry", (computedHeight - props["Border Thickness"]) / 2);
			ellipse.setAttribute("stroke", $objEl.data("giProperties")["Border Color"]);
			ellipse.setAttribute("fill", $objEl.data("giProperties")["Fill"]);
			ellipse.setAttribute("stroke-width", $objEl.data("giProperties")["Border Thickness"]);
			
			svg.appendChild(ellipse);
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	
	if(!cancelRender){ objCore.render(); }
	//console.log(methodResult);
	return { "methodResult": methodResult, "affected": affected };
};

$.fn.giRectangleShape = function(methodName, params){

	var cancelRender = false;
	var affected = [];
	var initialized = false;

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": 200, "Height": 200 };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giRectangleShape")){
				
				initialized = true;
				$objEl.addClass("giObject giShape giRectangleShape");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giRectangleShape",
					"Object Type": "Rectangle",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "manual",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "#000",
					"Fill": "#fff",
					"Corner Radius": 0
				});
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
			
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val == "auto" ? val : parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Fill"] = function(){ return props["Fill"]; }
		this["set Fill"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Fill"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
		this["set Corner Radius"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Corner Radius"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};			
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			$objEl.remove();
		};
		
		this.render = function(){
			
			var svgNs = "http://www.w3.org/2000/svg";
			var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			var computedHeight = props["Height"] == "auto" ? defaultProps["Height"] : props["Height"];
			console.log(computedHeight + " c h" );
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", props["Width"] == "expand" ? "auto": props["Width"]);
			$objEl.css("height", computedHeight);
			$objEl.css("position", props["Positioning"] == "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);
			
			var computedWidth = props["Width"] == "expand" ? $objEl[0].offsetWidth : props["Width"];
			
			$objEl.find("svg").remove();
			var svg = document.createElementNS(svgNs, "svg");
			svg.setAttribute("style", "position: relative;");
			
			svg.setAttribute("width", computedWidth);	
			svg.setAttribute("height", computedHeight);	
			$objEl[0].appendChild(svg);
			
			var rect = document.createElementNS(svgNs, "rect");
			rect.setAttribute("x", props["Border Thickness"] / 2);
			rect.setAttribute("y", props["Border Thickness"] / 2);
			rect.setAttribute("width", computedWidth - props["Border Thickness"]);
			rect.setAttribute("height", computedHeight - props["Border Thickness"]);
			rect.setAttribute("rx", props["Corner Radius"]);
			rect.setAttribute("ry", props["Corner Radius"]);
			rect.setAttribute("stroke", $objEl.data("giProperties")["Border Color"]);
			rect.setAttribute("fill", $objEl.data("giProperties")["Fill"]);
			rect.setAttribute("stroke-width", $objEl.data("giProperties")["Border Thickness"]);
			
			svg.appendChild(rect);
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined){
		methodResult = objCore[methodName](params);
	}
	
	if(!cancelRender){ objCore.render(); }
	//console.log(methodResult);
	return { "methodResult": methodResult, "affected": affected };
};


})(jQuery);