(function($){

var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

function rgbHexToIntegers(color) {
	var match, quadruplet;

	// Match #aabbcc
	if (match = /#([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})/.exec(color)) {
		quadruplet = [parseInt(match[1], 16), parseInt(match[2], 16), parseInt(match[3], 16), 1];

		// Match #abc
	} else if (match = /#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/.exec(color)) {
		quadruplet = [parseInt(match[1], 16) * 17, parseInt(match[2], 16) * 17, parseInt(match[3], 16) * 17, 1];

		// Match rgb(n, n, n)
	} else if (match = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color)) {
		quadruplet = [parseInt(match[1]), parseInt(match[2]), parseInt(match[3]), 1];

	} else if (match = /rgba\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9\.]*)\s*\)/.exec(color)) {
		quadruplet = [parseInt(match[1], 10), parseInt(match[2], 10), parseInt(match[3], 10),parseFloat(match[4])];

		// No browser returns rgb(n%, n%, n%), so little reason to support this format.
	} else {
		//quadruplet = colors[color];
		//console.log("error parsing color: " + color);
	}
	return quadruplet;
}

function decimalToHex(d, padding) {
    var hex = Number(d).toString(16);
    padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;

    while (hex.length < padding) {
        hex = "0" + hex;
    }

    return hex;
}


var GiChartColors = function(mixColor){
	var colors =
		["6699FF", "FF66FF","66FF66", "FF6666" , "FFFF66", "00FFFF",
		"9966FF", "FF3399","00FF99", "FF9933", "99FF33", "0099FF",
		"666699", "990099", "990000", "999966", "339933", "3366CC"];
	//["0099FF","FF66FF","66FF66","FFCC66","006699","CC6699","339966","FF6666"];
	var _index = 0;
	var index = function(newIndex){
		if(newIndex === undefined){
			return _index;
		}
		_index = newIndex;
	};

	this.next = function(){
		if(index() >= colors.length){
			index(0);
		}
		var color = colors[index()];
		if(mixColor !== undefined){
			var colorDec = rgbHexToIntegers(color);
			var mixColorDec = rgbHexToIntegers(mixColor);
			color = decimalToHex(Math.abs(colorDec[0] - mixColorDec[0])) +
			decimalToHex(Math.abs(colorDec[1] - mixColorDec[1])) +
			decimalToHex(Math.abs(colorDec[2] - mixColorDec[2]));
		}
		
		index(index()+1);
		return color;
	}
}

var giChartComponents = new (function(){
	
	this.generateFillOutFormButton = function(params){
		var buttonId = "gi-temp-"+(new Date().getTime());
		
		var $button = $("<button id='"+buttonId+"' class='button' title='New Request'><span class='fa fa-plus'></span></button>");
		
		var chartForms = params["chartForms"];
	
		var menuItems = {};
		for(var j=0; j<chartForms.length; j++){
			for(var i=0; i<chartForms[j].length; i++){
			
				menuItems[chartForms[j][i]["id"]] = {"name": chartForms[j][i]["name"]};
			}
		}
		
		//menuItems = { "1": {"name": "Under maintenance"}};
		
		
		$.contextMenu({
			selector: "#"+buttonId, 
			trigger: 'left',
			callback: function(key, options) {
				 window.open("/user_view/workspace?view_type=request&formID="+key+"&requestID=0", '_blank').focus();
			},
			items: menuItems
		});
		
		
		return $button;
	};
	
})();

$.fn.giPieChart = function(methodName, params){

	var toUpdateData = false;
	var cancelRender = false;
	var affected = [];
	var initialized = false;

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": "expand", "Height": "auto" };
		
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giPieChart")){
				
				initialized = true;
				$objEl.addClass("giObject giChart giPieChart");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giPieChart",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Object Type": "Pie Chart",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "auto",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					"Border Thickness": 1,
					"Border Color": "#fff",
					"Corner Radius": 8,
					"Padding": 8,
					"Background Color": "#fff",
					"New Record Button": "show",
					"Legends Position": "bottom",
					"Data Update Interval": 600,
					"After Update": null,
					"Query": [],
					"Data URL": "",
					"Survey ID": null
				});
				
				$objEl.data("giSessionData", {
					"Chart Data": undefined,
					"Data Drill Fields": [[]],
					"Data Slice Fields": [[]]
				});
			}
			
			props = $objEl.data("giProperties");
		};
		
		this.setProperties = function(newProps){
			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};

		this["get Query"] = function(){ return props["Query"]; };
		this["set Query"] = function(val){ 
			if(true){
				$objEl.data("giSessionData")["Data Drill Fields"] = [[]];
				$objEl.data("giSessionData")["Data Slice Fields"] = [[]];
				$objEl.data("giSessionData")["Chart Data"] = undefined;
				props["Query"] = val; 
				toUpdateData = true;
				
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
			
		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? val : parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		
		
		this["get Padding"] = function(){ return props["Padding"]; }
		this["set Padding"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Padding"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Background Color"] = function(){ return props["Background Color"]; }
		this["set Background Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Background Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
		this["set Corner Radius"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Corner Radius"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};			
		
		this["get Legends Position"] = function(){ return props["Legends Position"]; };
		this["set Legends Position"] = function(val){ 
			if(["top","left","bottom","right"].indexOf(val) > -1){
				props["Legends Position"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
		this["set Container ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Container ID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Data URL"] = function(){ return props["Data URL"]; };
		this["set Data URL"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Data URL"] = val; 
				toUpdateData = true;
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Survey ID"] = function(){ return props["Data URL"]; };
		this["set Survey ID"] = function(val){ 
			if(val !== undefined && val !== null){
				props["Survey ID"] = parseInt(val); 
				toUpdateData = true;
			}else{
				return "Invalid value.";
			}
		};
		
		this["get New Record Button"] = function(){ return props["New Record Button"]; };
		this["set New Record Button"] = function(val){ 
			if(val === "show" || val === "hide"){
				props["New Record Button"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Data Update Interval"] = function(){ return props["Data Update Interval"]; }
		this["set Data Update Interval"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Data Update Interval"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["get After Update"] = function(){ return props["Data Update Interval"]; }
		this["set After Update"] = function(func){
			var getType = {};
			if(func === null || getType.toString.call(func) === '[object Function]'){
				props["After Update"] = func; 
			}else{
				return "Invalid value.";
			}
		};	
		
		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			//clean updater
			var updaterInterval = $objEl.data("giDataUpdater");
			if(updaterInterval !== undefined){
				clearInterval(updaterInterval);
			}
			$objEl.remove();
		};
		
		this.updateData = function(params){
			
			var updaterFunc = function(params){
				
				cancelRender = true;
				var chartData = $objEl.data("giSessionData")["Chart Data"];
				var props = $objEl.data("giProperties");
				
				var updateInterval = props["Data Update Interval"];
				
				var forceUpdate = params !== undefined && params["forceUpdate"] !== undefined ? params.forceUpdate : false;
				
				if(forceUpdate ||
					chartData === undefined ||
					(chartData !== undefined && chartData !== null && (new Date() - new Date(chartData.timeGenerated*1000)) / 1000 > updateInterval)){
						
					$.ajax({
						type: "GET",
						url: "/ajax/gi-get-survey-chart-data",
						dataType: "json",
						data: { "surveyId": props["Survey ID"] },
						success: function(result){
							$objEl.data("giSessionData")["Chart Data"] = result;
							//$objEl.data("giSessionData")["Data Drill Fields"] = [[]];
							//$objEl.data("giSessionData")["Data Slice Fields"] = [[]];
							
							
							if(props["After Update"] !== null){
								
								
								var firstAggAlias = "Count";
								var data = $objEl.data("giSessionData")["Chart Data"]["data"][0];
								
								var grandTotal = data.item[firstAggAlias];

								props["After Update"]({ "Total": grandTotal });
							}
							$objEl[props["Identifier Name"]]("render");
							
						}
					});	
				}
			};
			
			var updaterInterval = $objEl.data("giDataUpdater");
			if(updaterInterval !== undefined){
				clearInterval(updaterInterval);
			}
			//console.log(params);
			if(params.quick !== undefined && params.quick === true){
			//console.log("quick");
				cancelRender = true;
				updaterFunc({ isNewQuery: true });
			}
			updaterInterval = setInterval(updaterFunc, 5000);
			$objEl.data("giDataUpdater", updaterInterval);
		};
		
		this.render = function(){
		
			var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}
			
			
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", props["Width"] == "expand" ? "auto": props["Width"]);
			$objEl.css("height", props["Height"]);
			$objEl.css("min-height", props["Height"] === "auto" ? 20: "");
			$objEl.css("position", props["Positioning"] === "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);
			
			var $border = $objEl.children("div.border");
			if($border.size() === 0){
				$border = $("<div>");
				$border.appendTo($objEl);
			}
			
			$border.html("");
			$border.removeAttr("style");
			$border.addClass("border");
			$border.css("border-style", "solid");
			$border.css("background-color", props["Background Color"]);
			$border.css("border-width", props["Border Thickness"]);
			$border.css("border-color", props["Border Color"]);
			$border.css("padding", props["Padding"]);
			$border.css("border-radius", props["Corner Radius"]);
			var outerThickness = props["Border Thickness"] + props["Padding"];
			if(props["Height"] !== "auto"){
				$border.css({ "height": $objEl.height() - outerThickness * 2 });
			}
			
			var $toolBar = $border.children(".toolBar"); 
			if($toolBar.size() == 0){
				$toolBar = $("<div>");
				$toolBar.addClass("toolBar");
				$border.append($toolBar);
			}
			$toolBar.html("");
			$toolBar.css("margin-bottom", "8px");
			
			//console.log("rendering...");
			var chartData = $objEl.data("giSessionData")["Chart Data"];
			
			if(chartData !== undefined && chartData !== null){
				//console.log("has chart data");
				//get slicers info 
				var query1 = chartData["giqs"][0];
					
				var cats = 
					$.Enumerable
					.From(query1["groupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var optCats = 
					$.Enumerable
					.From(query1["optionalGroupings"])
					.Select(function(i){ return i["alias"] })
					.ToArray();
				var drilledFields = $objEl.data("giSessionData")["Data Drill Fields"][0];
				
				var allDrilledFields = cats.concat(drilledFields);
								
				var newDrillFields = 
					$.Enumerable
					.From(optCats)
					.Where(function(i){ return allDrilledFields.indexOf(i) === -1; })
					.ToArray();
				
				var sliceFields = $objEl.data("giSessionData")["Data Slice Fields"][0];
				
				//display navigation buttons
				if(drilledFields.length > 0){
					var $rollUp = $("<button>");
					$rollUp.addClass("rollUpButton, button");
					$rollUp.text("Back");
					$toolBar.append($rollUp);
					$rollUp.on("click", function(e){
						drilledFields.pop();
						allDrilledFields.pop();
						if(allDrilledFields.length > 0){
							//console.log(allDrilledFields[allDrilledFields.length-1]);
							var removeSlice = allDrilledFields[allDrilledFields.length-1];
							for(var dsi=0; dsi<sliceFields.length; dsi++){
								//console.log(sliceFields[dsi].Field);
								if(sliceFields[dsi].Field === removeSlice){
									sliceFields.splice(dsi, 1);
								}
							}
						}
						$objEl[$objEl.data("giIdentifierName")]("render");
					});

				}
				
				
				if(allDrilledFields.length == 0 && newDrillFields.length > 0){
					var $drillDown = $("<button>");
					$drillDown.addClass("drillDownButton, button");
					$drillDown.text("Drill-down");
					$toolBar.append($drillDown);
					$drillDown.on("click", function(e){
						var dataDrillFields = $objEl.data("giSessionData")["Data Drill Fields"][0];
						dataDrillFields.push(newDrillFields[0]);
						$objEl[$objEl.data("giIdentifierName")]("render");
					});
				}
				
				//fill out form button
				/*
				if(props["New Record Button"] === "show"){
					var chartForms = [];
					for(var qei=0; qei<query1["entities"].length; qei++){
						var entity = query1["entities"][qei];
						//console.log(entity.formName + " " + entity.formId);
						if(entity["formId"] !== undefined && !isNaN(entity["formId"])){
							chartForms.push({ "name": entity.formName, "id":entity.formId});
						}
					}
					
					var $fillOutFormBtn = giChartComponents.generateFillOutFormButton({
						"chartForms": [chartForms]
					});
				
					$toolBar.append($fillOutFormBtn);
				}*/
				
				//traverse grouping tree and get the table
				var getRows = function(group, drillFields, sliceFields){
					var rows = [];
					
					if(drillFields.length > 0){
						var subGroupDrillFields = drillFields.slice();
						subGroupDrillFields.shift();
						for(var si=0; si<group.subGroups.length; si++){
							var subGroup = group.subGroups[si];
							rows = rows.concat(getRows(subGroup, subGroupDrillFields, sliceFields));
						}
					}else{
						var slicePassed = true;
						for(var sfi=0; sfi<sliceFields.length; sfi++){
							var sliceField = sliceFields[sfi];
							if(sliceField.Values.indexOf(group.item[sliceField.Field]) === -1){
								slicePassed = false;
							}
						}
						if(slicePassed){
							rows.push(group.item);
						}
					}
				
					return rows;
				};
				
				var group = chartData.data[0];
				var dataTable = getRows(group, allDrilledFields, sliceFields);
				
				//display pie
				var allDrilledFieldsCount = allDrilledFields.length;
				var newDrillFieldsCount = newDrillFields.length;
				
				var $dividerTable = $border.find(".dividerTable");
				if($dividerTable.size() === 0){
					$dividerTable = $("<table>");
					$border.append($dividerTable);
				}				
				$dividerTable.addClass("dividerTable");
				$dividerTable.attr("style", "");
				$dividerTable.css({ "width": "100%" });
				$dividerTable.html("");
				
				var $chartSection = null;
				var $legendsSection = null;
				if(["top", "bottom"].indexOf(props["Legends Position"]) > -1){
					var $row1 = $("<tr>"); 
					$dividerTable.append($row1);
					
					var $cell1 = $("<td>"); 
					$row1.append($cell1); 
					$cell1.addClass(props["Legends Position"] === "top" ? 
						"legendsSection": "chartSection");
					
					var $row2 = $("<tr>"); 
					$dividerTable.append($row2);
					
					var $cell2 = $("<td>"); 
					$row2.append($cell2); 
					
					$cell2.addClass(props["Legends Position"] === "top" ? 
						"chartSection": "legendsSection");
					
					$legendsSection = props["Legends Position"] === "top" ? $cell1 : $cell2;
					$chartSection = props["Legends Position"] === "top" ? $cell2 : $cell1;
					
				}else if(["left", "right"].indexOf(props["Legends Position"]) > -1){
					var $row = $("<tr>");
					$dividerTable.append($row);
					
					var $cell1 = $("<td>");
					$cell1.addClass(props["Legends Position"] === "left" ? 
						"legendsSection": "chartSection");
					$row.append($cell1);
					
					var $cell2 = $("<td>");
					$cell2.addClass(props["Legends Position"] === "left" ? 
						"chartSection": "legendsSection");
					$row.append($cell2);
					
					$legendsSection = props["Legends Position"] === "left" ? $cell1 : $cell2;
					$chartSection = props["Legends Position"] === "left" ? $cell2 : $cell1;
				}
				
				$legendsSection.css({ "vertical-align": "middle" });
				
				//get legends
				var $legends = $legendsSection.find(".legends");
				if($legends.size() === 0){
					$legends = $("<div>");
					$legendsSection.append($legends);
				}
				$legends.addClass("legends");
				$legends.css({ "line-height": "1.2em" });
				$legends.html("");
				
				//get pie categories, aggregate value and grand total
				var pieSlices = [];
				var grandTotal = 0;
				
				var firstAggAlias = query1["aggregates"][0]["alias"];
				var colorGen = new GiChartColors();
				for(var ri=0; ri<dataTable.length; ri++){
					var dRow = dataTable[ri];
					grandTotal += dRow[firstAggAlias];
					var pieSlice = { row: dataTable[ri], categories: [], value: dRow[firstAggAlias], color: colorGen.next() };
				
					
					for(var ci=0; ci<allDrilledFields.length; ci++){
						pieSlice["categories"].push(dRow[allDrilledFields[ci]]);
					}
					pieSlices.push(pieSlice);
				}
				
				//get percentage of each row
				for(var ri=0; ri<pieSlices.length; ri++){
					var pieSlice = pieSlices[ri];
					pieSlice["percentage"] = (100 / grandTotal) * pieSlice["value"];
				}
				
				//display legends
				var legendWidth = 0;
				for(var ri=0; ri<pieSlices.length; ri++){
					var pieSlice = pieSlices[ri];
					var $legend = $("<div>");
					$legend.addClass("legend");
					$legend.css({ "display": "inline-block", "margin-right": 8 });
					
					var $legendColor = $("<span>");
					$legendColor.css({ "display": "inline-block", 
						"width": "1em", "height": "1em",
						"background-color": "#" + pieSlice.color,
						"margin-right": 2
					});
					$legend.append($legendColor);
					
					$legend.append
						(pieSlice["categories"].join(" > ") + ": " + pieSlice["percentage"].toLocaleString('en-IN', { maximumSignificantDigits: 2 }) + "%");
						
					$legends.append($legend);
					legendWidth = legendWidth < $legend.outerWidth(true) ? $legend.outerWidth(true) : legendWidth;
				}
				
				$legends.children(".legend").css({ "width": legendWidth });
				
				//adjust sections' dimension
				if(["top", "bottom"].indexOf(props["Legends Position"]) > -1){
					$chartSection.append("<span class='cellFiller'>");
					$legendsSection.css("height", 1);
					$legendsSection.css("height", $legendsSection.height()); //get/set the computed
										
					if(props["Height"] === "auto"){
						$chartSection.css("height", "200");
					}else{
						var objElRect = $objEl[0].getBoundingClientRect();
						var dividerTableRect = $dividerTable[0].getBoundingClientRect();
						var dividerTableHeight = props["Height"] - (dividerTableRect.top - objElRect.top) - props["Padding"];
						$dividerTable.css("height", dividerTableHeight);
					}
				}else if(["left", "right"].indexOf(props["Legends Position"]) > -1){
					$chartSection.append("<span class='cellFiller'>");
					$legendsSection.css("width", 1);
					$legendsSection.css("width", $legendsSection.width()); //get/set the computed
					
					if(props["Height"] === "auto"){
						$chartSection.css("height", "200");
					}else{
						var objElRect = $objEl[0].getBoundingClientRect();
						var dividerTableRect = $dividerTable[0].getBoundingClientRect();
						var dividerTableHeight = props["Height"] - (dividerTableRect.top - objElRect.top) - props["Padding"];
						var legendsHeight = $legends.outerHeight(true) + 4;
						$chartSection.css("height", dividerTableHeight);
						$legends.css("height", legendsHeight > dividerTableHeight ? dividerTableHeight : legendsHeight);
						$legends.css("overflow-y", "auto");
						$legends.css("overflow-x", "hidden");
					}
				}
				$chartSection.find(".cellFiller").remove();
				
				//add some space between sections
				if(props["Legends Position"] === "top"){
					$legendsSection.css("padding-bottom", 8);
				}else if(props["Legends Position"] === "bottom"){
					$legendsSection.css("padding-top", 8);
				}else if(props["Legends Position"] === "left"){
					$legendsSection.css("padding-right", 8);
				}else if(props["Legends Position"] === "right"){
					$legendsSection.css("padding-left", 8);
				}
				
				//display chart itself
				var svgNs = "http://www.w3.org/2000/svg";
				var $chartCanvas = $("<div>");
	
				
				$chartSection.html("");
				$chartSection.append($chartCanvas);
				$chartCanvas.css
					({ "position": "relative",
					"width" : $chartSection.width(), 
					"height": $chartSection.height()});
					
				if(grandTotal === undefined || grandTotal === null || grandTotal === 0){
					$chartCanvas
					var $noDataDisplay = $("<div>There are no responses to this survey yet.</div>");
					$noDataDisplay.css({ "text-align": "center", "height": $chartSection.height(), "vertical-align":"middle" });
					$chartCanvas.append($noDataDisplay);
				}
					
				var getPieSliceUiInfo = function(angle, radius){

					angle = angle >= 360? 359.9: angle;
					var pieSliceInfo = { };

					var radian = Math.PI / 180;

					/* Step 1: get the b of the first triangle */
					var t1A = 180-90-(angle/2);
					var t1b = radius * (Math.cos(t1A * radian));
					var t1B = (180-angle)/2;

					/* Step 2: get the a and b of the second triangle which is also the x and y of the arc path */
					var t2c = t1b * 2;

					var t2B = 90-t1B;
					var t2A = 180-t2B-90;

					var t2a = t2c * (Math.sin(t2A * radian));
					var t2b = t2c * (Math.cos(t2A * radian));

					pieSliceInfo.x = t2a;
					pieSliceInfo.y = t2b;
					
					pieSliceInfo.arcSweepFlags = angle < 180 ? "0 1": "1 1";
					pieSliceInfo.line = angle < 360 ? 'L ' + (radius) + ' ' + (radius) : "";
					
					return pieSliceInfo;
				};
				
				var diameter = ($chartCanvas[0].offsetHeight < $chartCanvas[0].offsetWidth ? 
					$chartCanvas[0].offsetHeight : $chartCanvas[0].offsetWidth) ;
				var radius = diameter / 2;
				
				var nextPieSliceRotation = 0;
				
				for(var pi=0; pi<pieSlices.length; pi++){
					var pieSlice = pieSlices[pi];
					
					var piePieceAngle = (360 / 100) * pieSlice["percentage"];
					piePieceAngle = piePieceAngle > 359 ? 359.95 : piePieceAngle;
					//console.log(piePieceAngle);
					
					var piePieceSvg = document.createElementNS(svgNs, "svg");
					piePieceSvg.setAttribute
						("style", "position:absolute;" + 
						"left:" + (($chartCanvas[0].offsetWidth / 2) - radius) + ";" +
						"width:" + diameter + ";" + 
						"height:" + diameter + ";" + 
						"transform: rotate("+ nextPieSliceRotation + "deg);" + 
						"-webkit-transform: rotate("+ nextPieSliceRotation + "deg);" + 
						"-ms-transform: rotate("+ nextPieSliceRotation + "deg);" + 
						"pointer-events: none;");
					
					
					var pieSliceId = "gi" + ((new Date()).getTime()) + "" + parseInt(Math.random()*1000);
					
					var piePiecePath = document.createElementNS(svgNs, "path");
					piePiecePath.setAttribute("id", pieSliceId);
					piePiecePath.setAttribute("class", "hoverShadow");
					piePiecePath.setAttribute("style", "pointer-events: visible;");
										
					if(newDrillFieldsCount > 0 && 
					allDrilledFieldsCount > 0){
								
						(function(colName, colValue, newDrillField){
							$(piePiecePath).on("click", function(e){
								
								$objEl.data("giSessionData")["Data Slice Fields"][0].push({ Field: colName, Values: [colValue] });
								
								$objEl.data("giSessionData")["Data Drill Fields"][0].push(newDrillField);
								
								$objEl[$objEl.data("giIdentifierName")]("render");
							});
						})(allDrilledFields[allDrilledFields.length-1], 
							pieSlice["row"][allDrilledFields[allDrilledFields.length-1]], 
							newDrillFields[0]);
					}
					
					//tooltip
					(function(pieSliceId, val, color){
						$(piePiecePath).on("mouseenter", function(e){
							var $toolTip = $("<div>");
							$toolTip.attr("id", pieSliceId + "tt");
							$toolTip.appendTo("body");
							$toolTip.html(Number(val).toLocaleString());
							$toolTip.addClass("giChartToolTip");
							$toolTip.css
								({ "position": "absolute", 
								"padding": 8,
								"background" : "#fff",
								"border-color": "#"+color,
								"border-style": "solid",
								"border-width": 1,
								"border-radius": 4,
								"z-index": 9999999 });
							$toolTip.css
								({ "top": e.clientY+$("body").scrollTop()-$toolTip.outerHeight()-8, 
								"left": e.clientX-($toolTip.outerWidth()/2) });
						});
						
						$(piePiecePath).on("mousemove", function(e){
							var $toolTip = $("#" + pieSliceId + "tt");
							$toolTip.css
								({ "top": e.clientY+$("body").scrollTop()-$toolTip.outerHeight()-8, 
								"left": e.clientX-($toolTip.outerWidth()/2) });
						});
						
						$(piePiecePath).on("mouseleave", function(e){
							var $toolTip = $("#" + pieSliceId + "tt");
							$toolTip.remove();
						});
						
						$(piePiecePath).on("remove", function(e){
							var $toolTip = $("#" + pieSliceId + "tt");
							$toolTip.remove();
						});
					})(pieSliceId, pieSlice["value"], pieSlice.color);
					
					var pieSliceUiInfo = getPieSliceUiInfo(piePieceAngle, radius);
					piePiecePath.setAttribute("d", 
						'M ' + radius + ' ' + 0 + ' ' + 
						'a ' + radius + ' ' + radius + ' 0 ' + 
						pieSliceUiInfo.arcSweepFlags + ' ' + pieSliceUiInfo.x + ' ' + pieSliceUiInfo.y + ' ' +
						pieSliceUiInfo.line +  ' z');
					piePiecePath.setAttribute("fill", "#" + pieSlice.color);
						
					piePieceSvg.appendChild(piePiecePath);
					$chartCanvas[0].appendChild(piePieceSvg);
					
					nextPieSliceRotation += piePieceAngle;
				}
				
			}
			
		};
		
	})($(this));

	objCore.initializeElement();
	
	var methodResult = undefined;
	if(methodName !== undefined && methodName !== "render"){
		methodResult = objCore[methodName](params);
	}

	if(toUpdateData){
		objCore.updateData({ quick:true, forceUpdate:true });
	}
	
	if(!cancelRender){ 
		objCore.render(); 
	}
	
	return { "methodResult": methodResult, "affected": affected };
};




})(jQuery);