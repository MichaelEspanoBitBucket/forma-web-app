	(function($){

	var hexColorRegEx = /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i;

	$.fn.giTabContainer = function(methodName, params){

		var cancelRender = false;
		var affected = [];
		var initialized = false;
		
		var objCore = new (function($objEl){
			
			var $objEl = $objEl;
			var props = undefined;
			var defaultProps = { "Width": "expand", "Height": 200 };
			
			/* Common methods */
			this.initializeElement = function(){
				var content = this;
				if(!$objEl.hasClass("giTabContainer")){
					initialized = true;
					$objEl.addClass("giObject giContainer giTabContainer");
					
					var tabUids = [];
					var currDateTime = new Date().getTime();
					for(var i=0; i<3; i++){
						tabUids.push("ui-giTabContainerTab-" + currDateTime);
						currDateTime++;
					}
					

					$objEl.data("giProperties", {
						"Identifier Name": "giTabContainer",
						"Object Type": "Tab",
						"UID": "",
						"Container ID": "",
						"Name": "",
						"Ordinal": 0,
						"Width": defaultProps["Width"],
						"Height": defaultProps["Height"],
						"Positioning": "auto",
						"Top": 0,
						"Left": 0,
						"Z-index": "auto",
						"Corner Radius": 8,
						"Padding": 8,
						"Default Tab":0,
						"Background Color": "#fff",
						"Border Thickness": 1,
						"Border Color": "#DFDFDF",
						"Tab Border Thickness": 1,
						"Tab Border Color": "#DFDFDF",
						"Tabs": 
							[
								{ 	
									"UID": tabUids[0],
									"Title" : "Tab 1"
								},
								{ 	
									"UID": tabUids[1],
									"Title" : "Tab 2"
								},
								{ 	
									"UID": tabUids[2],
									"Title" : "Tab 3"
								}
							] 
					});
				}
				
				props = $objEl.data("giProperties");
			};
			
			this.setProperties = function(newProps){

				var setResults = [];
				var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
				affected.push(thisAffected);
				
				for(var name in newProps){
					if(this["set "+ name] !== undefined){
						setResults.push(this["set "+ name](newProps[name]));
					}
				}
				
				thisAffected["propAfter"] = $.extend(true, {}, props);
				
				return setResults;
			};
			
			this.getProperties = function(){ return $objEl.data("giProperties"); };
			
			/* Property getters and setters */
			this["get UID"] = function(){ return props["UID"]; };
			this["set UID"] = function(val){ 
				if(val !== undefined && val !== null && val.toString().trim() !== ""){
					props["UID"] = val; 
				}else{
					return "Invalid value.";
				}
			};
			
			this["get Name"] = function(){ return props["Name"]; };
			this["set Name"] = function(val){ 
				if(val !== undefined && val !== null && val.toString().trim() !== ""){
					props["Name"] = val; 
				}else{
					return "Invalid value.";
				}
			};

			this["get Tabs"] = function(){ return props["Tabs"]; };
			this["set Tabs"] = function(val){ 
				if(val !== undefined && val !== null && val.toString().trim() !== ""){
					props["Tabs"] = val; 
				}else{
					return "Invalid value.";
				}
			};

			this["get Title"] = function(val){ 
				if(props["Tabs"].length <= val || val < 0){
					return "Invalid index";
				}else{
					return props["Tabs"][val].Title;
				}
				
			};
			this["set Title"] = function(val){ 
					props["Tabs"][val.Index].Title = val.Title; 
			};
			
			this["get Container ID"] = function(){ return props["Container ID"]; };
			this["set Container ID"] = function(val){ 
				if(val !== undefined && val !== null){
					props["Container ID"] = val; 
				}else{
					return "Invalid value.";
				}
			};
			
			this["get Ordinal"] = function(){ return props["Ordinal"]; };
			this["set Ordinal"] = function(val){ 
				if(!isNaN(parseInt(val)) && parseInt(val) > -1){
					props["Ordinal"] = parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};

			this["get Default Tab"] = function(){ return props["Default Tab"]; };
			this["set Default Tab"] = function(val){ 
				if(!isNaN(parseInt(val)) && parseInt(val) > -1 && parseInt(val) < props["Tabs"].length){
					props["Default Tab"] = parseInt(val); 
				}else{
					return "Default Tab Invalid value.";
				}
			};
			
			this["get Width"] = function(){ return props["Width"]; };
			this["set Width"] = function(val){ 
				if(val === "expand" || parseInt(val) > 0){
					props["Width"] = val === "expand" ? val:  parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};
			
			this["get Height"] = function(){ return props["Height"]; };
			this["set Height"] = function(val){ 
				if(val === "auto" || parseInt(val) > 0){
					props["Height"] = val === "auto" ? "auto" : parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};
			
			this["get Positioning"] = function(){ return props["Positioning"]; }
			this["set Positioning"] = function(val){
				if(val === "auto" || val === "manual"){
					props["Positioning"] = val;
				}else{
					return "Invalid value";
				}
			}
			
			this["get Top"] = function(){ return props["Top"]; };
			this["set Top"] = function(val){ 
				if(val === "auto" || !isNaN(parseInt(val))){
					props["Top"] = val === "auto" ? val: parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};
			
			this["get Left"] = function(){ return props["Left"]; };
			this["set Left"] = function(val){ 
				if(val === "auto" || !isNaN(parseInt(val))){
					props["Left"] = val === "auto" ? val: parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};
			
			this["get Z-index"] = function(){ return props["Z-index"]; };
			this["set Z-index"] = function(val){ 
				if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
					props["Z-index"] = val === "auto" ? val: parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};
			
			this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
			this["set Border Thickness"] = function(val){
				if(!isNaN(parseInt(val)) && parseInt(val) > -1){
					props["Border Thickness"] = parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};

			this["get Tab Border Thickness"] = function(){ return props["Tab Border Thickness"]; }
			this["set Tab Border Thickness"] = function(val){
				if(!isNaN(parseInt(val)) && parseInt(val) > -1){
					props["Tab Border Thickness"] = parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};
			
			this["get Cell Padding"] = function(){ return props["Cell Padding"]; }
			this["set Cell Padding"] = function(val){
				if(!isNaN(parseInt(val)) && parseInt(val) > -1){
					props["Cell Padding"] = parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};
			
			
			this["get Border Color"] = function(){ return props["Border Color"]; }
			this["set Border Color"] = function(val){
				if(hexColorRegEx.test(val) || val == "transparent"){
					props["Border Color"] = val; 
				}else{
					return "Invalid value.";
				}
			};		

			this["get Tab Border Color"] = function(){ return props["Tab Border Color"]; }
			this["set Tab Border Color"] = function(val){
				if(hexColorRegEx.test(val) || val == "transparent"){
					props["Tab Border Color"] = val; 
				}else{
					return "Invalid value.";
				}
			};		
			
			this["get Fill"] = function(){ return props["Fill"]; }
			this["set Fill"] = function(val){
				if(hexColorRegEx.test(val)){
					props["Fill"] = val; 
				}else{
					return "Invalid value.";
				}
			};	
			

			this["get Corner Radius"] = function(){ return props["Corner Radius"]; }
			this["set Corner Radius"] = function(val){
				if(!isNaN(parseInt(val)) && parseInt(val) > -1){
					props["Corner Radius"] = parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};			

			this["get Background Color"] = function(){ return props["Background Color"]; }
			this["set Background Color"] = function(val){
				if(hexColorRegEx.test(val) || val == 'transparent'){
					props["Background Color"] = val; 
				}else{
					return "Invalid value.";
				}
			};	

			this["get Padding"] = function(){ return props["Padding"]; }
			this["set Padding"] = function(val){
				if(!isNaN(parseInt(val)) && parseInt(val) > -1){
					props["Padding"] = parseInt(val); 
				}else{
					return "Invalid value.";
				}
			};
			
			
			this["addTab"] = function(){
				var thisAffected = { "type": "modified", "propBefore": $.extend(true, {}, props) };
				affected.push(thisAffected);
				var currDateTime = new Date().getTime();
				props["Tabs"].push({ "UID": "ui-giTabContainerTab-" + (++currDateTime), Title:"New Tab" });
				thisAffected["propAfter"] = $.extend(true, {}, props);
			};

			this["deleteTab"] = function(val){

				var context = this;
				var thisAffected = { "type": "modified", "propBefore": $.extend(true, {}, props) };
				affected.push(thisAffected);

				var tabUID = props["Tabs"][val].UID
				var $nav = $objEl.find('[data-tab-ref-uid="' + tabUID + '"]');
				
				
				if(props["Default Tab"] == $nav.index()){
					props["Default Tab"] = 0;
				}
				
				$objEl.find("table:first")
					.children("tbody")
					.children("tr")
					.children("td")
					.find("#" + tabUID + ".giObjectContainer")
					.children(".giObject").each(function(i, v){
					var $obj = $(v);
					var delResult = $obj[$obj.data("giIdentifierName")]("delete");
					//console.log(delResult)
					affected = affected.concat(delResult["affected"]);
				});
				
				cancelRender = true;
				$nav.remove();
				$("#" + tabUID).remove();
				props["Tabs"].splice(val,1);

				thisAffected["propAfter"] = $.extend(true, {}, props);		
			};
			
			this["delete"] = function(){
				affected.push({ "type": "deleted", "propBefore": $.extend(true, {}, props) });
				
				$objEl.find("table:first")
					.children("tbody")
					.children("tr")
					.children("td")
					.children(".ui-tab-content")
					.children(".giObjectContainer")
					.children(".giObject").each(function(i, v){
					var $obj = $(v);
					var delResult = $obj[$obj.data("giIdentifierName")]("delete");
					//console.log(delResult);
					affected = affected.concat(delResult["affected"]);
				});
				
				cancelRender = true;
				$objEl.remove();
			};

			this["active"] = function(val){
				//var $nav = ;
				if(!isNaN(parseInt(val)) && props["Tabs"].length > parseInt(val)){
					var $tab = $($objEl.find('.ui-tab-nav:first > .ui-tab').get(val));
					//console.log($objEl)
					if (!$tab.hasClass('ui-tab-active')) {
					        var $tabPanel = $('#' + $tab.data('tabRefUid'));
					   		var $tabPanelSibs = $objEl.find('.ui-tab-content:first > .ui-tab-panel:not(#'+ $tab.data('tabRefUid') +')');
					   		var $siblings = $objEl.find('.ui-tab-nav:first > .ui-tab:not([data-tab-ref-uid="' + $tab.data('tabRefUid') + '"])');
					      	
					      	$tabPanelSibs.css('position','absolute');
					      	//$tabPanelSibs.css('visibility','hidden');
					        $siblings.removeClass('ui-tab-active');
					        $siblings.css({
					        	'border-bottom':"solid " + props["Tab Border Thickness"] + "px " + props["Tab Border Color"],
					        	'height':'auto',
					        	'background-color':'white'
					        })
					        
					       	//SELECTED Tab
					       	//$tabPanel.css('visibility','visible');
					        $tab.addClass('ui-tab-active')
					        $tab.css({
					        	"border-style": "solid",
					        	"border-width": props["Tab Border Thickness"],
					        	"border-color": props["Tab Border Color"],
					        	'border-bottom':'white solid ' + props["Tab Border Thickness"] + "px",
					        	'background-color':'white',
					        	'border-bottom':'none',
					        	'height':28 + props["Tab Border Thickness"] + "px",
					        })
					       $tabPanel.css('position','relative');
					        
					     
					        if ($tabPanel.length > 0 && !$tabPanel.hasClass('ui-active-tab-panel')) {
					            $tabPanelSibs.removeClass('ui-active-tab-panel');
					            $tabPanel.addClass('ui-active-tab-panel');
					          	$tab.parent().find('.ui-tab-title-input').blur();

					        }

					    }
				}else{
					return "Invalid value.";
				}
				cancelRender = true;
			};
			
			this["updateTabs"] = function(){
				var thisAffected = { "type": "modified", "propBefore": $.extend(true, {}, props) };
				affected.push(thisAffected);
				var tabs = [];
				$.each($objEl.find('.ui-tab-nav:first .ui-tab'), function(){
					tabs.push({"UID":$(this).data('tab-ref-uid'),"Title":$(this).find('.ui-tab-title-input:first').val()})
				})
				props["Tabs"] = tabs;

				thisAffected["propAfter"] = $.extend(true, {}, props);
				cancelRender = true;
			}
			

			this.render = function(){
				var context = this;
				var props = $objEl.data("giProperties");
				if($objEl.parent().attr("id") != props["Container ID"]){
					$objEl.appendTo("#" + props["Container ID"]);
				}
								
				/* setup div */
				
				$objEl.data("giIdentifierName", props["Identifier Name"]);
				$objEl.addClass("giTabContainer");
				$objEl.attr("id", props["UID"]);
				$objEl.css({
					"display" :props["Width"] == "expand" ? "block": "inline-block",
					"width": props["Width"] == "expand" ? "auto": props["Width"],
					"height": props["Height"],
					"position": props["Positioning"] == "auto" ? "relative": "absolute",
					"top": props["Positioning"] == "auto" ? "auto": props["Top"],
					"left": props["Positioning"] == "auto" ? "auto": props["Left"],
					"z-index": props["Z-index"]
				})
			
				var $border = $objEl.children("div.border");
				if($border.size() === 0){
					$border = $("<div>");
					$border.appendTo($objEl);
				}
				
				$border.addClass("border");
				$border.css({
					"background-color": props["Background Color"],
					"border": props["Border Color"]  + " solid " + props['Border Thickness'] + "px",
					"padding": props["Padding"],
					"border-radius": props["Corner Radius"],
					"position": "relative",
				});
			
				/* setup table */
				
			
				var $tbl = $objEl.find("table:first");
				if($tbl.size() == 0){
					$tbl = $("<table>" + 
						"<tr>"+
				        "  <td class='ui-tab-header'>" + 
				        "        <ul class='ui-tab-nav'>" + 
				        "        </ul>" + 
				        "    </td>" + 
				        "</tr>" + 
				        "<tr>" + 
				        "   <td >" + 
				        "		<div class='ui-tab-content'></div>"+
				        "   </td>" + 
				        "</tr>" +
				        "</table>");
				
					$border.append($tbl);
					$objEl.on('click', '.ui-tab', function (e) {
						//console.log($(this).index())
						context.active($(this).index())
						e.stopPropagation();
					});
				}
				var $nav = $tbl.find(".ui-tab-nav:first");
				var $content = $tbl.find(".ui-tab-content:first");
				var currTab = $nav.find('.ui-tab-active').index()
				$nav.html("");
				var $tempNav = "";
				$.each(props["Tabs"], function(tabIndex, tab){
					var $tab = $tbl.find("#" + tab.UID);
					$tempNav += "<li data-tab-ref-uid='" + tab.UID + "' class='ui-tab'><input class='ui-tab-title-input' style='display:none' type='text' value='"+ tab.Title +"'><span class='ui-tab-title'>" + tab.Title + "</span></li>";
					if($tab.size() == 0){
						$content.append("<div id='" + tab.UID + "' class='ui-tab-panel giObjectContainer'></div>");
					}
				})

				$nav.append($tempNav);

				//var computedTabHeight = props["Width"] == "expand" ? $objEl[0].offsetWidth : props["Width"];
				var $tabContent = $border.find('.ui-tab-content:first');
				//Tab Content 
				$tabContent.css({
					"border-style": "solid",
					"border-width": props["Tab Border Thickness"],
					"border-color": props["Tab Border Color"],
					"border-top": '0px',
					"width": ($objEl[0].offsetWidth) - (((props["Padding"] + props['Border Thickness']) + props['Tab Border Thickness']) * 2),
					'position':'relative'
				})
			

				$tabContent.children('.ui-tab-panel').css({
					//'position':'relative',
					'height': props['Height'] === "auto" ? "auto" : (props["Height"] - ((props["Tab Border Thickness"] * 3 ) + ((props["Border Thickness"]  + props["Padding"])  * 2))) - 36
				})
				
				//Active Panel
				//$tabContent.find('.ui-active-tab-panel:first').css('position','relative');

				var $tabNav = $border.find('.ui-tab-nav:first');
				$tabNav.css({
					"border-style": "solid",
					"border-width": props["Tab Border Thickness"],
					"border-color": props["Tab Border Color"],
					"border-top": '0px',
					"border-left": '0px',
					"border-right": '0px'
				})

				$tabNav.find('.ui-tab').css({
					"border-width": props["Tab Border Thickness"],
					"border-color": props["Tab Border Color"]
				})
		
				// $tabNav.find('.ui-tab-active').css({
				// 	'border-bottom':"none",
				// 	'height':28 + props["Tab Border Thickness"] + "px"
				// })
			
				
				
				

			
				if(!initialized){
					//console.log($border.find('.ui-tab-active').index())
					context.active(currTab)
					var allTabID = jQuery(props["Tabs"]).map(function(){
						var z;
						return this.UID
					})

					$.each($tabContent.children('.ui-tab-panel'), function(elIndex, tabEl){
						if($.inArray(this.id, allTabID) < 0 ){
							var $nav = $objEl.find('[data-tab-ref-uid="' + this.id + '"]');					
							$nav.remove();
							$(this).remove();
						}
					})
					
				}else{
					context.active(props["Default Tab"])
				}
				

				
				$objEl.find(".giObjectContainer > .giObject").each(function(k, v){
					$v = $(v);
					$v[$v.data("giIdentifierName")]("render");
				});
			};
		})($(this));

		objCore.initializeElement();
		
		var methodResult = undefined;
		if(methodName !== undefined){
			methodResult = objCore[methodName](params);
		}

		if(!cancelRender){ objCore.render(); }
		//console.log(methodResult);
		return { "methodResult": methodResult, "affected": affected };

	};

	})(jQuery);
