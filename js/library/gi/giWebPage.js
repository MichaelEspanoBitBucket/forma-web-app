(function($){

$.fn.giWebPage = function(methodName, params){
	var toLoadFrame = false;
	var cancelRender = false;
	var affected = [];
	var initialized = false;
	

	var objCore = new (function($objEl){
		
		var $objEl = $objEl;
		var props = undefined;
		var defaultProps = { "Width": 200, "Height": 200 };
				
		/* Common methods */
		this.initializeElement = function(){
			
			if(!$objEl.hasClass("giWebPage")){
				$objEl.addClass("giObject giWebPage");
				
				$objEl.data("giProperties", {
					"Identifier Name": "giWebPage",
					"Object Type": "Web Page",
					"UID": "",
					"Container ID": "",
					"Name": "",
					"Ordinal" : 0,
					"Maintain Aspect Ratio": false,
					// "Display": "block",
					"Width": defaultProps["Width"],
					"Height": defaultProps["Height"],
					"Positioning": "auto",
					"Top": 0,
					"Left": 0,
					"Z-index": "auto",
					// "Border Thickness": 1,
					// "Border Color": "#000",
					"Editing":true,
					"URL":""
				});
				toLoadFrame = true
			}
			
			props = $objEl.data("giProperties");
		};

		this.setProperties = function(newProps){

			var setResults = [];
			var thisAffected = { "type":  (initialized ? "created": "modified"), "propBefore": $.extend(true, {}, props) };
			affected.push(thisAffected);
			
			for(var name in newProps){
				if(this["set "+ name] !== undefined){
					setResults.push(this["set "+ name](newProps[name]));
				}
			}
			
			thisAffected["propAfter"] = $.extend(true, {}, props);
			
			return setResults;
		};
		
		// this.setProperties = function(props){
		// 	var setResults = [];

		// 	for(var name in props){
		// 		console.log(name)
		// 		setResults.push(this["set "+ name](props[name]));
		// 	}
		// 	return setResults;
		// };
		this.getProperties = function(){ return $objEl.data("giProperties"); };
		
		/* Property getters and setters */
		this["get UID"] = function(){ return props["UID"]; };
		this["set UID"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["UID"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get URL"] = function(){ return props["URL"]; };
		this["set URL"] = function(val){ 
			/*var regex = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);*/
		
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["URL"] = val; 
				toLoadFrame = true;
			}else{
				return "Invalid value.";
			}
		};

		this["get Editing"] = function(){ return props["Editing"]; };
		this["set Editing"] = function(val){ 
			/*var regex = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);*/
		
			if(val == true || val == false){
				props["Editing"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Name"] = function(){ return props["Name"]; };
		this["set Name"] = function(val){ 
			if(val !== undefined && val !== null && val.toString().trim() !== ""){
				props["Name"] = val; 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Ordinal"] = function(){ return props["Ordinal"]; };
		this["set Ordinal"] = function(val){ 
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Ordinal"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Maintain Aspect Ratio"] = function(){ return props["Maintain Aspect Ratio"]; };
		
		this["get Display"] = function(){ return props["Display"]; };
		this["set Display"] = function(val){
			if(val === "block" || val === "inline"){
				props["Display"] = val;
			}else{
				return "Invalid value.";
			}
		}
		
		this["get Container ID"] = function(){ return props["Container ID"]; };
			this["set Container ID"] = function(val){ 
				if(val !== undefined && val !== null){
					props["Container ID"] = val; 
				}else{
					return "Invalid value.";
				}
			};

		this["get Width"] = function(){ return props["Width"]; };
		this["set Width"] = function(val){ 
			if(val === "expand" || parseInt(val) > 0){
				props["Width"] = val === "expand" ? val:  parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Height"] = function(){ return props["Height"]; };
		this["set Height"] = function(val){ 
			if(val === "auto" || parseInt(val) > 0){
				props["Height"] = val === "auto" ? "auto" : parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Positioning"] = function(){ return props["Positioning"]; }
		this["set Positioning"] = function(val){
			if(val === "auto" || val === "manual"){
				props["Positioning"] = val;
			}else{
				return "Invalid value";
			}
		}
		
		this["get Top"] = function(){ return props["Top"]; };
		this["set Top"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Top"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Left"] = function(){ return props["Left"]; };
		this["set Left"] = function(val){ 
			if(val === "auto" || !isNaN(parseInt(val))){
				props["Left"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
	
		this["get Z-index"] = function(){ return props["Z-index"]; };
		this["set Z-index"] = function(val){ 
			if(val === "auto" || (!isNaN(parseInt(val)) && parseInt(val) > 0)){
				props["Z-index"] = val === "auto" ? val: parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Thickness"] = function(){ return props["Border Thickness"]; }
		this["set Border Thickness"] = function(val){
			if(!isNaN(parseInt(val)) && parseInt(val) > -1){
				props["Border Thickness"] = parseInt(val); 
			}else{
				return "Invalid value.";
			}
		};
		
		this["get Border Color"] = function(){ return props["Border Color"]; }
		this["set Border Color"] = function(val){
			if(hexColorRegEx.test(val)){
				props["Border Color"] = val; 
			}else{
				return "Invalid value.";
			}
		};		

		this["delete"] = function(){
			var thisAffected = { "type": "deleted", "propBefore": $.extend(true, {}, props) };
			
			affected.push(thisAffected);
			cancelRender = true;
			//clean updater
			// var updaterInterval = $objEl.data("giDataUpdater");
			// if(updaterInterval !== undefined){
			// 	clearInterval(updaterInterval);
			// }
			$objEl.remove();
		};
		
		this.LoadFrame = function(){
			var $frame = $objEl.children("iframe.pageFrame");
			$frame.attr("src", props["URL"]);
		}


		
		this.render = function(){
			var context = this;
			//	var props = $objEl.data("giProperties");
			if($objEl.parent().attr("id") != props["Container ID"]){
				$objEl.appendTo("#" + props["Container ID"]);
			}

			var svgNs = "http://www.w3.org/2000/svg";
			///var props = $objEl.data("giProperties");
			
			$objEl.attr("id", props["UID"]);
			$objEl.data("giIdentifierName", props["Identifier Name"]);

			$objEl.attr("id", props["UID"]);
			$objEl.css("display", props["Width"] == "expand" ? "block": "inline-block");
			$objEl.css("width", props["Width"] == "expand" ? "auto": props["Width"]);
			$objEl.css("height", props["Height"]);
			$objEl.css("position", props["Positioning"] == "auto" ? "relative": "absolute");
			$objEl.css("top", props["Positioning"] == "auto" ? "auto": props["Top"]);
			$objEl.css("left", props["Positioning"] == "auto" ? "auto": props["Left"]);
			$objEl.css("z-index", props["Z-index"]);

		
			var $frame = $objEl.children("iframe.pageFrame");
			if($frame.size() === 0){
				$frame = $("<iframe class='pageFrame'>");
				$frame.appendTo($objEl);
			}

			$frame.css({width: $objEl.width(), height: $objEl.height(), "position":"absolute", "z-index":0});			
		};
		
	})($(this));


	objCore.initializeElement();
		
		var methodResult = undefined;
		if(methodName !== undefined){
			methodResult = objCore[methodName](params);
		}

		if(!cancelRender){ objCore.render(); }
		
		if(toLoadFrame){
			objCore.LoadFrame()
		}
		return { "methodResult": methodResult, "affected": affected };
};


})(jQuery);