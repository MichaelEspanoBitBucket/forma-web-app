

$(document).ready(function(){

	$('.scrollReg').on('click', function(event){
			event.preventDefault();
			var regLinkId = $(this).children('a').attr('href');
			$('html, body').animate({
				scrollTop: regLinkId.offset().top
			}, 'slow');
	});
	
	// CSS BASED ANIMATION

	var bannerlogoAni = $('#logo-banner-wrapper');

	setTimeout(function(){
		bannerlogoAni.addClass('bounceIn').css('opacity', 1);		
	}, 500 );

	var laptopAni = $('#laptop-wrapper');
	
	setTimeout(function() {
		laptopAni.addClass('bounceInRight').css('opacity', 1);
	}, 1000);

	var laptopAni2 = $('.login-banner-img');
	
	setTimeout(function() {
		laptopAni2.addClass('bounceInRight').css('opacity', 1);
	}, 1000);

	setTimeout(function(){
		
		$('#forms-wrapper').addClass('fadeInRight').css('opacity', 1);
		$('#workflows-wrapper').addClass('fadeInRight').css('opacity', 1);
		$('#request-wrapper').addClass('fadeInLeft').css('opacity', 1);

	}, 2000);

	$('#banner-content-right > h4').addClass('compNameAni');
	setTimeout(function(){
	$('.compNameAni').addClass('fadeIn').css('opacity', 1);
	},3500);

	$('#banner-content-right > p').addClass('textInfo');
	$('.textInfo').css({
		'position': 'relative',
		'bottom': -8
	});

		setTimeout(function(){
		$('#bg-radial').animate({
			opacity:1		
		}, 'slow');
	}, 3600);

	setTimeout(function(){
		$('.textInfo').animate({
			opacity: 1,
		bottom: 0
	});
	}, 4500);

	setTimeout(function(){
		$('#gs3display').addClass('bounceInLeft').css('opacity', 1);
	}, 5000);

	setTimeout(function(){
		$('#fl-ui-login-banner-gs3-display').addClass('bounceInLeft').css('opacity', 1);
	}, 2000);

	if ($(window).width() === 960) {
		$('#gs3display').removeClass('bounceInLeft');
	};	

});		