jQuery.fn.extend({ 

	overlapsX: function($el, $includeMargin){
		return $(this).position().left < $el.position().left + $el.outerWidth($includeMargin) && 
			$(this).position().left + $(this).outerWidth($includeMargin) > $el.position().left;
	},
	
	overlapsY: function($el, $includeMargin){
		return $(this).position().top < $el.position().top + $el.outerHeight($includeMargin) && 
			$(this).position().top + $(this).outerHeight($includeMargin) > $el.position().top;
	},
	
	overlaps: function($el, $includeMargin){
		return this.overlapsX($el, $includeMargin) && this.overlapsY($el, $includeMargin);
	}
});


