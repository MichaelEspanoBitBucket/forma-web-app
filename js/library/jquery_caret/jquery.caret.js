{// REVISED
    function getContentEditableVal(contentEditEle){
        var ce_elem = $(contentEditEle);
        var ce = ce_elem.clone();

        if($.browser.webkit){
          var append_n = "\n";
          ce.find("div").filter(function(){
            $(this).addClass("checker-first-div");
            // console.log("test note",$(this).parent().contents().eq(0))
            if($(this).parent().contents().eq(0).hasClass("checker-first-div")){
              $(this).removeClass("checker-first-div");
              return false;
            }else if(typeof $(this).parent().contents().eq(0).hasClass("checker-first-div") === "undefined"){
              $(this).removeClass("checker-first-div");
              return false;
            }else{
              $(this).removeClass("checker-first-div");
              return true;
            }

          }).prepend("\n");/*.replaceWith(function() { return "\n" + this.innerHTML; });*/
        }
        if($.browser.msie)
          ce.find("p").replaceWith(function() { return this.innerHTML  +  "<br>"; });
        if($.browser.mozilla || $.browser.opera ||$.browser.msie )
          ce.find("br").replaceWith("\n");

        var textWithWhiteSpaceIntact = ce.text();
        return textWithWhiteSpaceIntact;
    }

    (function(k,e,i,j){
        k.fn.caret=function(start_caret,end_caret){
            var a,c,f=this[0],d=k.browser.msie,c_text = "";
            
            if(typeof start_caret==="object"&&typeof start_caret.start==="number"&&typeof start_caret.end==="number"){
              a=start_caret.start;c=start_caret.end
            }else if(typeof start_caret==="number"&&typeof end_caret==="number"){
              a=start_caret;c=end_caret
            }else if(typeof start_caret==="string")
              if((a=f.value.indexOf(start_caret))>-1)
                c=a+start_caret[e];
              else
                a=null;
            else if(Object.prototype.toString.call(start_caret)==="[object RegExp]"){
              start_caret=start_caret.exec(f.value);
              if(start_caret!=null){
                a=start_caret.index;c=a+start_caret[0][e]
              }
            }

            if(typeof a!="undefined"){
              if(d){
                d=this[0].createTextRange();
                d.collapse(true);
                d.moveStart("character",a);
                d.moveEnd("character",c-a);
                d.select()
              }else{
                this[0].selectionStart=a;
                this[0].selectionEnd=c
              }

              this[0].focus();
              return this;
            }else{
              
              if(d){
                c=document.selection;
                if(this[0].tagName.toLowerCase()!="textarea"  ){
                    d=this.val();
                    a=c[i]()[j]();
                    a.moveEnd("character",d[e]);
                    
                    var g=a.text==""?d[e]:d.lastIndexOf(a.text);

                    a=c[i]()[j]();
                    a.moveStart("character",-d[e]);

                    var h=a.text[e];
                }else{
                  a=c[i]();
                  c=a[j]();
                  c.moveToElementText(this[0]);
                  c.setEndPoint("EndToEnd",a);
                  g=c.text[e]-a.text[e];
                  h=g+a.text[e]
                }
              }else{
                if( this[0].tagName.toLowerCase() == "div" && this[0].getAttribute("contenteditable") != null ){
                    c_text = getContentEditableVal($(f));
                    containerEl = f;
                    var w_sel = window.getSelection();
                    var range_custom = window.getSelection().getRangeAt(0);
                    // console.log("BADTERPPS",range_custom)
                    var preSelectionRange_custom = range_custom.cloneRange();
                    preSelectionRange_custom.selectNodeContents(f);
                    preSelectionRange_custom.setEnd(range_custom.startContainer, range_custom.startOffset);
                    var start_custom = preSelectionRange_custom.toString().length;
                    // console.log("GRABEE",preSelectionRange_custom.toString())
                    G1 = start_custom;
                    H1 = start_custom + range_custom.toString().length;


                    var w_sel = window.getSelection();
                    if(w_sel.type == "Range"){
                      var w_sel_string = w_sel.toString();
                      var contenteditable_value = c_text;
                      var cev_indexof_start = contenteditable_value.indexOf(w_sel_string);
                      var cev_indexof_end = cev_indexof_start + w_sel_string.length;
                      while(cev_indexof_start >=  0 && cev_indexof_start <= contenteditable_value.length){
                        // console.log("cev_indexof_start",cev_indexof_start)
                        // console.log("QWE",contenteditable_value.substr(0,cev_indexof_end));
                        if(contenteditable_value.indexOf(w_sel_string, cev_indexof_start+1) < 0){
                          break;
                        }
                        test_index_accuracy = contenteditable_value.substr(0,cev_indexof_end).replace(/\n/g,"");
                        // console.log("test_index_accuracy",H1,test_index_accuracy.length)
                        if(H1 == test_index_accuracy.length){
                          break;
                        }
                        cev_indexof_start = contenteditable_value.indexOf(w_sel_string, cev_indexof_start+1);
                        cev_indexof_end = cev_indexof_start + w_sel_string.length;
                        
                      }

                      // g = cev_indexof_start - w_sel_string.length;
                      // h = 0;
                      g = cev_indexof_start;
                      h = cev_indexof_end;
                    }else if (w_sel.type == "Caret"){
                      c_text = getContentEditableVal($(f));
                      containerEl = f;
                      var w_sel = window.getSelection();
                      var w_sel_string = w_sel.toString();
                      var range_custom = window.getSelection().getRangeAt(0);
                      var preSelectionRange_custom = range_custom.cloneRange();
                      preSelectionRange_custom.selectNodeContents(f);
                      preSelectionRange_custom.setEnd(range_custom.startContainer, range_custom.startOffset);
                      var start_custom = preSelectionRange_custom.toString().length;
                      g = start_custom;
                      h = start_custom + range_custom.toString().length;
                      


                      if((g-1) >=0 ){
                        string_basis = c_text.replace(/\n/g,"").substr(g-1,1);
                        var cev_indexof_start = c_text.indexOf(string_basis);
                        while(cev_indexof_start >= 0 && cev_indexof_start <= c_text.length){
                          s_c1 = c_text.replace(/\n/g,"").substr(0, g);
                          s_c2 = c_text.substr(0, cev_indexof_start+1).replace(/\n/g,"");
                          // console.log("SC = ",s_c1 , s_c2)
                          if(s_c1 == s_c2){
                            break;
                          }
                          cev_indexof_start = c_text.indexOf(string_basis,cev_indexof_start+1);
                        }
                        g = cev_indexof_start+1;
                        h = cev_indexof_start+1;
                      }else{
                        g = 0;h=0;
                      }
                    }
                }else{
                  g=f.selectionStart;
                  h=f.selectionEnd;
                }
              }

              if( this[0].tagName.toLowerCase() == "div" && this[0].getAttribute("contenteditable") != null ){
                a=c_text.substring(g,h);
              }else{
                a=f.value.substring(g,h);
              }
              
              return {
                start:g,
                end:h,
                text:a,
                container_text:c_text,
                replace:function(m){
                  if( this[0].tagName.toLowerCase() == "div" && this[0].getAttribute("contenteditable") != null ){
                    return c_text.substring(0,g)+m+c_text.substring(h,c_text[e]);
                  }else{
                    return f.value.substring(0,g)+m+f.value.substring(h,f.value[e]);
                  }
                }
              }
            }
        }
    })(jQuery,"length","createRange","duplicate");
}
{ //OLD
  // (function($) {
  //   $.fn.caret = function(pos) {
  //     var target = this[0];
  //  var isContentEditable = target.contentEditable === 'true';
  //     //get
  //     if (arguments.length == 0) {
  //       //HTML5
  //       if (window.getSelection) {
  //         //contenteditable
  //         if (isContentEditable) {
  //           target.focus();
  //           var range1 = window.getSelection().getRangeAt(0),
  //               range2 = range1.cloneRange();
  //           range2.selectNodeContents(target);
  //           range2.setEnd(range1.endContainer, range1.endOffset);
  //           return range2.toString().length;
  //         }
  //         //textarea
  //         return target.selectionStart;
  //       }
  //       //IE<9
  //       if (document.selection) {
  //         target.focus();
  //         //contenteditable
  //         if (isContentEditable) {
  //             var range1 = document.selection.createRange(),
  //                 range2 = document.body.createTextRange();
  //             range2.moveToElementText(target);
  //             range2.setEndPoint('EndToEnd', range1);
  //             return range2.text.length;
  //         }
  //         //textarea
  //         var pos = 0,
  //             range = target.createTextRange(),
  //             range2 = document.selection.createRange().duplicate(),
  //             bookmark = range2.getBookmark();
  //         range.moveToBookmark(bookmark);
  //         while (range.moveStart('character', -1) !== 0) pos++;
  //         return pos;
  //       }
  //       //not supported
  //       return 0;
  //     }
  //     //set
  //     if (pos == -1)
  //       pos = this[isContentEditable? 'text' : 'val']().length;
  //     //HTML5
  //     if (window.getSelection) {
  //       //contenteditable
  //       if (isContentEditable) {
  //         target.focus();
  //         window.getSelection().collapse(target.firstChild, pos);
  //       }
  //       //textarea
  //       else
  //         target.setSelectionRange(pos, pos);
  //     }
  //     //IE<9
  //     else if (document.body.createTextRange) {
  //       var range = document.body.createTextRange();
  //       range.moveToElementText(target);
  //       range.moveStart('character', pos);
  //       range.collapse(true);
  //       range.select();
  //     }
  //     if (!isContentEditable)
  //       target.focus();
  //     return pos;
  //   }
  // })(jQuery)

}
