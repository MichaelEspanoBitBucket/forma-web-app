Math.getPlaceValueOld = function(number){
	if(isNaN(number)){
		throw "Not a number.";
	}
	
	var parts = number.toString().split(".");

	if(parts[0] === "0" && parts[1] !== undefined){
		//mantissa place value
		var len = parts[1].length;
		var placeValueStr = "";
		for(var i=0; i<len; i++){
			if(parts[1][i]!=="0"){
				placeValueStr += "1";
				break;
			}
			placeValueStr += "0";
		}
		return parseFloat("0." + placeValueStr);
	}else{
		//integer place value
		var len = parts[0].length;
		var placeValueStr = "1";
		for(var i=1; i<len; i++){
			placeValueStr += "0";
		}
		return parseInt(placeValueStr);
	}
}

Math.getPlaceValue = function(number){
	if(isNaN(number)){
		throw "Not a number.";
	}

	var numStr = number.toString();
	var output = "";
	var hasNonZero = false;
	for(var i=0; i<numStr.length; i++){
		if(numStr[i] === "-"){
			output += "-";
		}else if(numStr[i] === "0"){
			output += "0";
		}else if(numStr[i] === "."){
			output += ".";
		}else if(hasNonZero){
			output += "0";
		}else{
			output += "1";
			hasNonZero = true;
		}
	}

	return parseFloat(output);
}


Math.roundOff = function(number, placeValue){
	return placeValue == 0 || number == 0 ? 0 : Math.round((number)/placeValue)*placeValue;
}

Math.ceilBy = function(number, placeValue){
	return placeValue == 0 || number == 0 ? 0 :  Math.ceil((number)/placeValue)*placeValue;
}

Math.floorBy = function(number, placeValue){
	return placeValue == 0 || number == 0 ? 0 :  Math.floor((number)/placeValue)*placeValue;
}
