// jQuery Alert Dialogs Plugin
//
// Usage:
//		jAlert( message, [title,width, height, top, callback] )
//		jConfirm( message, [title,width, height, top, callback] )
//		jPrompt( message, [value, title,width, height, top, callback] )
// 		jDialog(message, value,width, height, top, callback) Dynamic

(function($) {
	
	$.alerts = {
		
		// These properties can be read/written by accessing $.alerts.propertyName from your scripts at any time
		
		verticalOffset: -75,                // vertical offset of the dialog from center screen, in pixels
		horizontalOffset: 0,                // horizontal offset of the dialog from center screen, in pixels/
		repositionOnResize: false,           // re-centers the dialog on window resize
		overlayOpacity: 0.4,                // transparency level of overlay
		overlayColor: '#000',               // base color of overlay
		draggable: true,                    // make the dialogs draggable (requires UI Draggables plugin)
		okButton: '&nbsp;Yes&nbsp;',         // text for the OK button
		cancelButton: '&nbsp;No&nbsp;', // text for the Cancel button
		dialogClass: null,                  // if specified, this class will be applied to all dialogs
		
		// Public methods
		alert: function(message, title, width, height, top,  callback) {
			var cb=callback;
			if( title == null ) title = 'Alert';
			$.alerts._show(title, message, null, 'alert', width, height, top,  function(result) {
				if( cb ) cb(result);
			});
		},
		confirm: function(message, title, width, height, top,  callback) {
			if( title == null ) title = 'Confirm';
			$.alerts._show(title, message, null, 'confirm', width, height, top, function(result) {
				if( callback ) callback(result);
			});
		},
		prompt: function(message, value, title, width, height, top,  callback) {
			if( title == null ) title = 'Prompt';
			$.alerts._show(title, message, value, 'prompt', width, height, top, function(result) {
				if( callback ) callback(result);
			});
		},
		dialog: function(message, value, width, height, top,  callback) {
			$.alerts._show("", message, value, "dialog", width, height, top,   function(result) {
				if( callback ) callback(result); 
			});
		},
		// Private methods
		_show: function(title, msg, value, type, width, height, top,  callback) {
			$.alerts._hide();
			$.alerts._overlay('show');
			$('<div id="popup_container" style="width:'+width+'px;  height:'+height+'px; margin: '+top+'px 0px 0px;">' +
			    '<h1 id="popup_title"></h1>' +
			    '<div id="popup_content">' +
				'<div id="popup_message"></div>' +
				'</div>'+
			  '</div>').appendTo("BODY").fadeIn(1000);
			
			if( $.alerts.dialogClass ) $("#popup_container").addClass($.alerts.dialogClass); 
			
			// IE6 Fix
			//var pos = ($.browser.msie && parseInt($.browser.version) <= 6 ) ? 'absolute' : 'fixed'; 
			var pos = "fixed";
			$("#popup_container").css({
				position: pos,
				zIndex: 1000000,
				padding: 0,
			});
			
			$("#popup_title").text(title);
			$("#popup_content").addClass(type);
			$("#popup_message").text(msg);
			$("#popup_message").html( $("#popup_message").text().replace(/\n/g, '<br />') );			
			
			$("#popup_container").css({
				minWidth: $("#popup_container").outerWidth(),
				maxWidth: $("#popup_container").outerWidth()
			});
			
			$.alerts._reposition();
			$.alerts._maintainPosition(true);
			
			switch( type ) {
				case 'alert':
					$("#popup_message").after('<input type="button" class="fl-margin-right  btn-blueBtn" value="OK" id="popup_ok" />'); //<div id="popup_panel"></div> excess space problem
					$("#popup_ok").click( function() {
						$.alerts._hide();
						callback(true);
					});
					/*
					$("#popup_ok").keypress( function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});*/
					$(document).on("keyup",function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});
				break;
				case 'confirm':
					$("#popup_message").after('<div id="popup_panel"><input type="button"  class="fl-margin-right  btn-blueBtn" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" class="fl-margin-right btn-blueBtn" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
					$("#popup_ok").click( function() {
						if ($(this).attr("custom-fn")) {
							//return;
						}else{
							$.alerts._hide();
						}
						
						if( callback ) callback(true);
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback(false);
					});
					$("#popup_ok").focus();
					$("#popup_prompt, #popup_ok, #popup_cancel").on("keyup",function(e) {
						if( e.keyCode == 13 ) {$("#popup_ok").trigger('click');};
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
						
					});
				break;
				case 'prompt':
					$("#popup_message").append('<br /><input type="text" size="30" id="popup_prompt" />').after('<div id="popup_panel"><input type="button" class="fl-margin-right fl-margin-top fl-margin-bottom  btn-blueBtn" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" class="fl-margin-right fl-margin-top fl-margin-bottom  btn-basicBtn"  value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
					$("#popup_prompt").width( $("#popup_message").width() );
					$("#popup_ok").click( function() {
						var val = $("#popup_prompt").val();
						$.alerts._hide();
						if( callback ) callback( val );
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( null );
					});
					/*
					$("#popup_prompt, #popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});*/
					$(document).on("keyup",function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
						
						
					});
					if( value ) $("#popup_prompt").val(value);
					$("#popup_prompt").focus().select();
				break;
				case 'dialog':
					$("#popup_message").append(value);
					$("#popup_prompt").width( $("#popup_message").width() );
					$("#popup_ok").click( function() {
						var val = $("#popup_prompt").val();
						$.alerts._hide();
						if( callback ) callback( val );
					});
					$("body").on("click","#popup_cancel", function() {
						$.alerts._hide();
						if( callback ) callback( null );
					});
					$(document).on("keyup",function(e) {
						
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if (e.keyCode == 27) {
							if($(".idecontent").length>=1){
								$('.ideclose').trigger("click");

							}
							else if ($(".fields-reset.fl-closeDialog").length>=1) {
								$(":focus").trigger("change");
								$(":focus").blur();
								$(".fields-reset.fl-closeDialog").trigger("click");
								
								
							}
							else{
								$("#popup_cancel").trigger('click');
								
							}
						}
						
					});
					if( value ) $("#popup_prompt").val(value);
					$("#popup_prompt").focus().select();
					
				break;
			}
			
			// Make draggable
			if( $.alerts.draggable ) {
				try {
					$("#popup_container").draggable({ handle: $("#popup_title") });
					$("#popup_title").css({ cursor: 'move' });
				} catch(e) { /* requires jQuery UI draggables */ }
			}
			
		},
		
		_hide: function() {
			if ($('[jconfirmattr="true"]').length == 1) {
				$('[jconfirmattr="true"]')
				$('[jconfirmattr="true"]').stop(false,false).animate({
					opacity:1
				}, function(){
					$(this).remove();	
				});
			}else {
				// $("#popup_container").remove();
				removeFadeout.call($('#popup_container'));
            	removeFadeout.call($('#popup_overlay'));
			}
			$.alerts._overlay('hide');
			$.alerts._maintainPosition(false);
		},
		
		_overlay: function(status) {
			switch( status ) {
				case 'show':
					
					$.alerts._overlay('hide');
					$("BODY").append('<div id="popup_overlay"></div>');
					$("#popup_overlay").css({
						position: 'fixed',
						zIndex: 100000,
						top: '0px',
						left: '0px',
						width: '100%',
						height: $(document).height(),
						background: $.alerts.overlayColor,
						opacity: $.alerts.overlayOpacity,
						opacity:0.3,
						display:'none'
					})
					$("#popup_overlay").fadeIn(1000);
					
				break;
				case 'hide':
					$("#popup_overlay").remove();
				break;
			}
		},
		
		_reposition: function() {
			var top = (($(window).height() / 2) - ($("#popup_container").outerHeight() / 2)) + $.alerts.verticalOffset;
			var left = (($(window).width() / 2) - ($("#popup_container").outerWidth() / 2)) + $.alerts.horizontalOffset;
			if( top < 0 ) top = 0;
			if( left < 0 ) left = 0;
			
			// IE6 fix
			//if( $.browser.msie && parseInt($.browser.version) <= 6 )top = top + $(window).scrollTop();
			var top = top;
			$("#popup_container").css({
				top: top + 'px',
				left: left + 'px'
			});
			$("#popup_overlay").height( $(document).height() );
		},
		
		_maintainPosition: function(status) {
			if( $.alerts.repositionOnResize ) {
				switch(status) {
					case true:
						$(window).bind('resize', $.alerts._reposition);
					break;
					case false:
						$(window).unbind('resize', $.alerts._reposition);
					break;
				}
			}
		}	
	}
	// Shortuct functions
	jAlert = function(message, title, width, height, top, callback) {
		$.alerts.alert(message, title, width, height, "0",  callback);
		if(typeof PublicEmbedSetup != "undefined"){
			PublicEmbedSetup.dialogChecker("alert");
		}
	}
	jConfirm = function(message, title, width, height, top,  callback) {
		$.alerts.confirm(message, title, width, height, "0",  callback);
		if(typeof PublicEmbedSetup != "undefined"){
			PublicEmbedSetup.dialogChecker("confirm");
		}
	};	
	jPrompt = function(message, value, title, width, height, top,  callback) {
		$.alerts.prompt(message, value, title, width, height, "0",  callback);
		if(typeof PublicEmbedSetup != "undefined"){
			PublicEmbedSetup.dialogChecker("prompt");
		}
	};
	jDialog = function(message, value, width, height, top,  callback) {
		$.alerts.dialog(message, value, width, height, top,  callback);
		if(typeof PublicEmbedSetup != "undefined"){
			PublicEmbedSetup.dialogChecker("custom");
		}
	};
})(jQuery)

/* use this for modal design
//Roni Pinili Modal UI
var ret = '<h3 class="fl-margin-bottom">';
ret += '<i class="icon-save"></i> Title';
ret += '</h3>';
ret += '<div class="hr"></div>';
ret += '<div style="margin:2px 0px 0px 2px;font-size:11px;color:red"></div>';
ret += '<div class="content-dialog" style="height: auto;">';
	// your content here
	ret += 'Hello World';
	ret += '<div class="fields" style="">';
        ret += '<div class="label_basic"></div>';
        ret += '<div class="input_position" style="margin-top:5px;">';
            ret += ' <input type="button" class="btn-blueBtn fl-margin-right" id="buttonID" value="Save">';
            ret += ' <input type="button" class="btn-basicBtn" id="popup_cancel" value="Close">';
        ret += '</div>';
    ret += '</div>';
ret+= '</div>';
var newDialog = new jDialog(ret, "", "", "300", "", function() {   
});
newDialog.themeDialog("modal2"); 
 */

 jDialog.prototype.themeDialog = function(themeVersion, options) {
    if (themeVersion == "modal2") {
        //alert($('#popup_container').length);

        var popCont = $('#popup_container');
        var closeDialog = $('<div class="fl-closeDialog"><i class="fa fa-times"></i></div>');
        var clearBoth = $('<div class="clearfix"></div>');
        var footDialog = $('<div class="fl-footDialog"></div>');
        var dialogHeader = $('<div class="fl-dialogHeader"></div>');
        popCont.addClass('fadeInUp');
        popCont.addClass('fl-modal2');
        popCont.css('overflow', 'visible');
        closeDialog.on('click', function(){
            if($(this).hasClass('close-confirmation')){

               
            }
            else{
               removeFadeout.call($(this).parents('#popup_container,.popup_container_display_table'));
               removeFadeout.call($('#popup_overlay')); 
            }
            
        });

        popCont.append(closeDialog);
        popCont.append(footDialog);

        alignDialogToMiddle(popCont);

        $('#save_prop').addClass('fl-positive-btn');
        $('input[type="button"][value="Save"]').addClass('fl-positive-btn');
        //$('.add-new-tab-panel').addClass('fl-default-btn');
        // $('#select_form_picklist').addClass('fl-default-btn');
        //$('.getFormForWorkflow').addClass('fl-default-btn');
        $('input[type="button"].settings-to-json').addClass('fl-positive-btn');
        $('input[type="button"][object_type="organizational_chart"]').addClass('fl-positive-btn');
        $('input[type="button"].updateEditUser').addClass('fl-positive-btn');
        //$('input[type="button"]#advanceFilter').addClass('fl-default-btn');
        //$('input[type="button"]#clearValue').addClass('fl-default-btn');
        //$('input[type="button"][value="Print"]').addClass('fl-default-btn');
        $('.reportColumnOK').addClass('fl-positive-btn');
        $('.reportPlotBandOK').addClass('fl-positive-btn');
        
        $('#popup_message').css('padding', '0px');
        $('#popup_message').children().filter(function(index){
           
            if (index == 0 || index == 1) {
                return true;

            }else {
                return false;
            }
        }).wrapAll(dialogHeader);

        $('#popup_message').children().filter(function(index){
            if ($(this).attr('style') == 'margin-top:-5px;font-size:11px;color:#222222') {
                return true;

            }else{
                return false;
            };
        }).css('margin', '5px 5px 0px');

        if (popCont.find('.content-dialog').length <= 0) {
             var wrapAllcontent = $('<div class="content-dialog"></div>')   
           popCont.find('#popup_message').children().not('.fl-dialogHeader').wrapAll(wrapAllcontent);
           popCont.find('.content-dialog').css('margin-bottom', '0px');
         };

        if (popCont.find('.fl-importBtn').length == 1) {
            var wrapperContentScroll = $('<div class="content-dialog-scroll"></div>');
             $('.content-dialog').wrap(wrapperContentScroll);
             $('.fl-importBtn').appendTo('#popup_message').not('h3');
             $('.fl-importBtn').css('margin', '0px 15px 15px 15px');
             $('div.uploader').css({
                'width':'auto', 
                'position':'static'
             });
         };


         if (popCont.find('.content-dialog-scroll').length <= 0) {
            var wrapperContentScroll = $('<div class="content-dialog-scroll"></div>');
            $('.content-dialog').wrap(wrapperContentScroll);
            $('.content-dialog-scroll').perfectScrollbar({
                  wheelSpeed: 1,
                  wheelPropagation: false,
                  suppressScrollX: true,
                  minScrollbarLength:50

            });         
         };  

         if (popCont.find('.addTabDialog').length == 1) {
                $('.content-dialog-scroll').css('height', 'auto');
         };
         if (popCont.find('.editTabDialog').length == 1) {
                 $('.content-dialog-scroll').css('height', 'auto');
         };


         if (popCont.find('.fl-get-workspace-popup').length == 1) {
             $('.content-dialog-scroll').css('height', 'auto');
             $('.fl-closeDialog').remove();  
         };

         if (popCont.find('.fl-end-workflow').length == 1) {
              $('.content-dialog-scroll').css('height', 'auto');
              $('.object_properties_wrapper').css('height', 'auto');
         };

         if (popCont.find('.middleware-settings-dialog').length == 1) {
            $('.middleware-settings-dialog').appendTo('#popup_message');
            dialogHeader.append('h3');
         };

        if (popCont.find('.trigger-container-fields').length == 1) {
            $('.trigger-container-fields').css('margin-top', '10px');
        };

        if (popCont.find('#clearValue').length == 1) {
            $('#clearValue').addClass('fl-buttonEffect');
            $('.content-dialog').css('margin-bottom', '0px');
        };

        if (popCont.find('.fl-filter-title').length == 1) {
            $('.content-dialog-scroll').css('height', 'auto');
            $('.content-dialog').css('margin-bottom', '0px');
        };

        if (popCont.find('.fl-remove-filter').length == 1) {
        };

        if (popCont.find('.forgot_submit ').length == 1) {
            $('.content-dialog-scroll').css('height', 'auto');
        };

        if (popCont.find('.fl-suggestion-input').length == 1 ) {
            $('.content-dialog-scroll').css('height', 'auto');
        };
        if(popCont.find('.fl-form-details-wrap').length == 1){
            $('.content-dialog-scroll').css('height', 'auto');
            popCont.find('.fl-form-details').css({
                'padding': '10px',
                'border-bottom': '1px solid #ccc'
            });
        };

        if (popCont.find('#fl-button-send-chat-attachments').length == 1) {
           $('.content-dialog-scroll').css('height', 'auto');
          
        };

        var Okbtn = $('.fl-dialogHeader').children().eq(1).children();
        Okbtn.addClass('fl-buttonEffect');
        footDialog.append(Okbtn);
        $('.fl-dialogHeader, .content-dialog').append(clearBoth);
        
        
        if ($('.jdiag-header-info-container').length == 1) {
            popCont.find('#popup_message').children().eq(0).append('<div class="header-info-header"><h3 class="fl-margin-bottom"><svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-view-settings"></use></svg><span>View Settings</span></h3></div>');  
            popCont.find('.jdiag-header-info-container').children('.header-info-header').remove();
            popCont.find('.jdiag-header-info-container').appendTo('#popup_message').next();
        };
        
     
        popCont.find('.hr').remove();
        popCont.find('h3').css('margin-bottom', '0px');
        popCont.find('#popup_cancel, .cancel-header-info').hide();
        popCont.find('.btn-blueBtn, .btn-basicBtn').appendTo(footDialog); 
        // $(window).trigger('resize');   

         if (popCont.find('.fl-add-comment-here').length == 1) {
           popCont.find('i').addClass('faiconColor');
            if ($('#comment-container').length == 1) {
                $('#comment-container').children('.content-dialog-scroll').children('*').unwrap();
                popCont.find('#comment-container').css('top', '0px');
                popCont.find('.fl-above-addfiles').before(popCont.find('.fl-adding-comment, .btn-basicBtn').wrapAll('<div class="fl-addcom-addfile-wrapper fl-floatRight"></div>').parent());
            };
        };
        if (popCont.find('.fl-get-report').length == 1) {
            $('.content-dialog-scroll').css('height', 'auto');
            $('.content-dialog').css('margin-bottom', '0px');
        };

        if (popCont.find('#comment-container').length == 1) {
            $('.content-dialog-scroll').children().unwrap();
        };

        if (popCont.find('#fl-button-send-chat-attachments').length == 1) {
            $('.content-dialog-scroll').css('height', 'auto');
            $('.content-dialog').css('margin-bottom', '0px');
            popCont.find('.newMsg').remove();
        };

        if (popCont.find('.fl-inProgress-workflow-list-wrap').length == 1) {
            $('.content-dialog').css('margin-top', '10px');
            $('.fl-header-tbl-wrapper').css({
                 'border-top': '1px solid #CFDBE2'
            });

            $('.fl-table-ellip').css('margin-left', '0px');
        };

        if (popCont.find('input[type="button"]').length >= 1) {
                popCont.find('input[type="button"]').on('click', function(){
                    setTimeout(function(){
                     if ($('.error_bg').length >= 1) {
                            popCont.removeClass('fadeInUp');
                            popCont.addClass('tada');
                            setTimeout(function(){
                                popCont.removeClass('tada');
                            }, 500);
                    }
                 }, 500);
            });
        };

        if(options){
            if(options["height"]){
                // popCont.find(".content-dialog-scroll").eq(0).attr("style",popCont.attr("style")+"; height:"+options["height"]+"px !important");
                popCont.find(".content-dialog-scroll").eq(0).css("height",options["height"]+"px");
            };
        };

        

        /*if (popCont.find('.fl-requestImg').length == 1) {
            $('.content-dialog-scroll').css('height', 'auto');
            $('.content-dialog').css('margin-bottom', '0px');
        };*/

        //PANG UNWRAP PARA NAKA FIXED SYA NA WALANG BACKGROUND NA NAG HAHANDLE
        // var pos_top = popCont.offset().top;
        // var pos_left = popCont.offset().left;

        // popCont.css({
        //     "position":"fixed",
        //     "top":(pos_top)+"px",
        //     "left":(pos_left)+"px"
        // }).removeClass("isDisplayInlineBlock")
        // .unwrap()
        // .unwrap();
    };
};

jConfirm.prototype.themeConfirm = function(themeVersion, options){
    if (themeVersion == 'confirm2') {
         var popCont = $('#popup_container');
         var footDialog = $('<div class="fl-footDialog"></div>');
         var pop_panel = $('#popup_panel'); 
         var pop_message = $('#popup_message');
         popCont.css('overflow', 'visible');
         popCont.addClass('bounceInUp');
         pop_message.prepend(options['icon']);
         pop_message.css({
            'padding': 10,
            'color':'#222222'
         });

         popCont.attr("jconfirmattr", "true");

         pop_panel.css({
            'padding-left': 0,
            'background-color':'transparent', 
            'padding': 0
         });

         popCont.append(footDialog);
         footDialog.append(pop_panel);
         
         alignDialogToMiddle(popCont);

         if ($('#popup_ok').length == 1 || $('#popup_cancel').length == 1) {

                $('#popup_ok').addClass('fl-positive-btn');
                $('#popup_cancel').addClass('fl-default-btn');              
                setTimeout( function(){
                        popCont.removeClass('bounceInUp');
                    },1000);
                //popCont.find('#popup_ok').on('click', function(){
                //    popCont.addClass('bounceOutUp');
                //});
                popCont.find('#popup_cancel').on('click', function(){
                    popCont.addClass('bounceOutUp');
                });
         };

         // if ($('.setObject_btn').length == 0) {
         // 		$('#popup_cancel').remove();
         // 		$('#popup_ok').val("OK")   
         // };

    };

};

jAlert.prototype.themeAlert = function(themeVersion, options){
    if (themeVersion == 'jAlert2') {

         var popCont = $('#popup_container');
         var footDialog = $('<div class="fl-footDialog"></div>');
         var pop_panel = $('#popup_panel'); 
         var pop_message = $('#popup_message');
         popCont.css('overflow', 'visible');
         popCont.addClass('bounceInUp');
         pop_panel.css('background', 'transparent');
         popCont.append(footDialog);
         pop_message.prepend(options['icon']);
         footDialog.append($('#popup_ok'));

         alignDialogToMiddle(popCont);
    };
} 

function alignDialogToMiddle(popCont){
    popCont
    .wrap('<div class="isDisplayTable popup_container_display_table" style="position:fixed;z-index:100000;top:0px;width:100%;height:100%;"/>')
    .wrap('<div class="isDisplayTableCell taC" style="vertical-align:middle;display:table-cell;"/>');
    
    popCont.css({
        "position":"relative",
        "top":"0px",
        "left":"0px",
        "margin":""
    }).addClass("isDisplayInlineBlock");

    var remove_popup_container_display_table = setInterval(function(){
        if($('#popup_container').length <= 0){
            $('.popup_container_display_table').remove();
            clearInterval(remove_popup_container_display_table);
        }
    },0);
}