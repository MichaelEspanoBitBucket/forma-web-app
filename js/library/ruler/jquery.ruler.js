/**
 * jQuery.Ruler v1.1
 * Add Photoshop-like rulers and mouse position to a container element using jQuery.
 * http://ruler.hilliuse.com
 * 
 * Dual licensed under the MIT and GPL licenses.
 * Copyright 2013 Hillius Ettinoffe http://hilliuse.com
 */
;(function( $ ){

	//optimized version by mikel e.
	$.fn.ruler = function(options) {
	
		var defaults = {
			vRuleSize: 18,
			hRuleSize: 18,
			showCrosshair : false,
			showMousePos: false
		};//defaults
		var settings = $.extend({},defaults,options);
		
		var hRule = '<div class="ruler hRule"></div>',
				vRule = '<div class="ruler vRule"></div>',
				corner = '<div class="ruler corner"></div>',
				vMouse = '<div class="vMouse"></div>',
				hMouse = '<div class="hMouse"></div>',
				mousePosBox = '<div class="mousePosBox">x: 50%, y: 50%</div>';
		
		if (!Modernizr.touch) {
			// Mouse crosshair
			if (settings.showCrosshair) {
				$('.fl-set-ruler').append(vMouse, hMouse);
			}
			// Mouse position
			if (settings.showMousePos) {
				$('.fl-set-ruler').append(mousePosBox);
			}
			// If either, then track mouse position
			if (settings.showCrosshair || settings.showMousePos) {
				$(window).mousemove(function(e) {
					if (settings.showCrosshair) {
						$('.vMouse').css("top",e.pageY-($(document).scrollTop())+1);
						$('.hMouse').css("left",e.pageX+1);
						//-($(window).scrollTop())
					}
					if (settings.showMousePos) {
						$('.mousePosBox').html("x:" + (e.pageX-settings.vRuleSize) + ", y:" + (e.pageY-settings.hRuleSize) ).css({
							top: e.pageY-($(document).scrollTop()) + 16,
							left: e.pageX + 12
						});
					}
				});
			}
		}
		
		//resize
		$(window).resize(function(e){
			var $hRule = $('.hRule');
			var $vRule = $('.vRule');
			var $hRuleChildren = $hRule.children("[data-pos-id-hrule]");
			var $vRuleChildren = $vRule.children("[data-pos-id-vrule]");
			// $hRule.empty();
			// $vRule.empty().height(0).outerHeight($vRule.parent().outerHeight());
			$vRule.height(0).outerHeight($vRule.parent().outerHeight());

			// Horizontal ruler ticks
			var tickLabelPos = settings.vRuleSize;
			if($hRule.children("[data-pos-id-hrule]").length >= 1){
				tickLabelPos = $hRuleChildren.last().position().left;
			}
			var newTickLabel = "";
			while ( tickLabelPos <= $hRule.width() ) {
				if ((( tickLabelPos - settings.vRuleSize ) %50 ) == 0 ) {
					newTickLabel = "<div data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "' class='tickLabel'>" + ( tickLabelPos - settings.vRuleSize ) + "</div>";
					if($hRuleChildren.filter("[data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "left", tickLabelPos+"px" ).appendTo($hRule);
					}
				} else if ((( tickLabelPos - settings.vRuleSize ) %10 ) == 0 ) {
					newTickLabel = "<div data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "' class='tickMajor'></div>";
					if($hRuleChildren.filter("[data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "']").length <= 0){
						$(newTickLabel).css("left",tickLabelPos+"px").appendTo($hRule);
					}
				} else if ((( tickLabelPos - settings.vRuleSize ) %5 ) == 0 ) {
					newTickLabel = "<div data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "' class='tickMinor'></div>";
					if($hRuleChildren.filter("[data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "left", tickLabelPos+"px" ).appendTo($hRule);
					}
				}
				tickLabelPos = (tickLabelPos + 5);				
			}//hz ticks
			
			// Vertical ruler ticks
			tickLabelPos = settings.hRuleSize;
			if($vRule.children("[data-pos-id-vrule]").length >= 1){
				tickLabelPos = $vRuleChildren.last().position().top;
			}
			newTickLabel = "";
			while (tickLabelPos <= $vRule.height()) {
				if ((( tickLabelPos - settings.hRuleSize ) %50 ) == 0) {
					newTickLabel = "<div data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "' class='tickLabel'><span>" + ( tickLabelPos - settings.hRuleSize ) + "</span></div>";
					if($vRuleChildren.filter("[data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
					}
					
				} else if (((tickLabelPos - settings.hRuleSize)%10) == 0) {
					newTickLabel = "<div data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "' class='tickMajor'></div>";
					if($vRuleChildren.filter("[data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
					}
				} else if (((tickLabelPos - settings.hRuleSize)%5) == 0) {
					newTickLabel = "<div data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "' class='tickMinor'></div>";
					if($vRuleChildren.filter("[data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
					}
				}
				tickLabelPos = ( tickLabelPos + 5 );				
			}//vert ticks	

			// $hRuleChildren.filter(function(){
			// 	return $(this).position().left > $hRule.width();
			// }).remove();
			// $vRuleChildren.filter(function(){
			// 	return $(this).position().top > $vRule.height();
			// }).remove();
		});//resize
		
		return this.each(function() {
			var $this = $(this);
			
			// Attach rulers
			
			// Should not need 1 min padding-top of 1px but it does
			// will figure it out some other time
			//back this padding below if needed
			//$this.css("padding-top", settings.hRuleSize + 1 + "px");
			if (settings.hRuleSize > 0) {				
				$(hRule).height(settings.hRuleSize).prependTo($this);
			}
			
			if (settings.vRuleSize > 0) {
				var oldWidth = $this.outerWidth();
				//back this padding below if needed
				//$this.css("padding-left", settings.vRuleSize + 1 + "px").outerWidth(oldWidth);
				$(vRule).width(settings.vRuleSize).height($this.outerHeight()).prependTo($this);
			}
			
			if ( (settings.vRuleSize > 0) && (settings.hRuleSize > 0) ) {
				$(corner).css({
					width: settings.vRuleSize,
					height: settings.hRuleSize
				}).prependTo($this);
			}
			
			
			var $hRule = $this.children('.hRule');
			var $vRule = $this.children('.vRule');
		
			// Horizontal ruler ticks
			var tickLabelPos = settings.vRuleSize;
			if($hRule.children("[data-pos-id-hrule]").length >= 1){
				tickLabelPos = $hRule.children("[data-pos-id-hrule]").last().position().left;
			}
			var newTickLabel = "";
			while ( tickLabelPos <= $hRule.width() ) {
				if ((( tickLabelPos - settings.vRuleSize ) %50 ) == 0 ) {
					newTickLabel = "<div data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "' class='tickLabel'>" + ( tickLabelPos - settings.vRuleSize ) + "</div>";
					if($hRule.children("[data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "left", tickLabelPos+"px" ).appendTo($hRule);
					}
				} else if ((( tickLabelPos - settings.vRuleSize ) %10 ) == 0 ) {
					newTickLabel = "<div data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "' class='tickMajor'></div>";
					if($hRule.children("[data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "']").length <= 0){
						$(newTickLabel).css("left",tickLabelPos+"px").appendTo($hRule);
					}
				} else if ((( tickLabelPos - settings.vRuleSize ) %5 ) == 0 ) {
					newTickLabel = "<div data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "' class='tickMinor'></div>";
					if($hRule.children("[data-pos-id-hrule='" + ( tickLabelPos - settings.vRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "left", tickLabelPos+"px" ).appendTo($hRule);
					}
				}
				tickLabelPos = (tickLabelPos + 5);				
			}//hz ticks
			
			// Vertical ruler ticks
			tickLabelPos = settings.hRuleSize;
			if($vRule.children("[data-pos-id-vrule]").length >= 1){
				tickLabelPos = $vRule.children("[data-pos-id-vrule]").last().position().top;
			}
			newTickLabel = "";
			while (tickLabelPos <= $vRule.height()) {
				if ((( tickLabelPos - settings.hRuleSize ) %50 ) == 0) {
					newTickLabel = "<div data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "' class='tickLabel'><span>" + ( tickLabelPos - settings.hRuleSize ) + "</span></div>";
					if($vRule.children("[data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
					}
					
				} else if (((tickLabelPos - settings.hRuleSize)%10) == 0) {
					newTickLabel = "<div data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "' class='tickMajor'></div>";
					if($vRule.children("[data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
					}
				} else if (((tickLabelPos - settings.hRuleSize)%5) == 0) {
					newTickLabel = "<div data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "' class='tickMinor'></div>";
					if($vRule.children("[data-pos-id-vrule='" + ( tickLabelPos - settings.hRuleSize ) + "']").length <= 0){
						$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
					}
				}
				tickLabelPos = ( tickLabelPos + 5 );				
			}//vert ticks			
			
		});//each		
		
	};//ruler

	//original copy
	$.fn.rulerBackUp = function(options) {
		return false;
		var defaults = {
			vRuleSize: 18,
			hRuleSize: 18,
			showCrosshair : false,
			showMousePos: false
		};//defaults
		var settings = $.extend({},defaults,options);
		
		var hRule = '<div class="ruler hRule"></div>',
				vRule = '<div class="ruler vRule"></div>',
				corner = '<div class="ruler corner"></div>',
				vMouse = '<div class="vMouse"></div>',
				hMouse = '<div class="hMouse"></div>',
				mousePosBox = '<div class="mousePosBox">x: 50%, y: 50%</div>';
		
		if (!Modernizr.touch) {
			// Mouse crosshair
			if (settings.showCrosshair) {
				$('.fl-set-ruler').append(vMouse, hMouse);
			}
			// Mouse position
			if (settings.showMousePos) {
				$('.fl-set-ruler').append(mousePosBox);
			}
			// If either, then track mouse position
			if (settings.showCrosshair || settings.showMousePos) {
				$(window).mousemove(function(e) {
					if (settings.showCrosshair) {
						$('.vMouse').css("top",e.pageY-($(document).scrollTop())+1);
						$('.hMouse').css("left",e.pageX+1);
						//-($(window).scrollTop())
					}
					if (settings.showMousePos) {
						$('.mousePosBox').html("x:" + (e.pageX-settings.vRuleSize) + ", y:" + (e.pageY-settings.hRuleSize) ).css({
							top: e.pageY-($(document).scrollTop()) + 16,
							left: e.pageX + 12
						});
					}
				});
			}
		}
		
		//resize
		$(window).resize(function(e){
			var $hRule = $('.hRule');
			var $vRule = $('.vRule');
			$hRule.empty();
			$vRule.empty().height(0).outerHeight($vRule.parent().outerHeight());
			
			// Horizontal ruler ticks
			var tickLabelPos = settings.vRuleSize;
			var newTickLabel = "";
			while ( tickLabelPos <= $hRule.width() ) {
				if ((( tickLabelPos - settings.vRuleSize ) %50 ) == 0 ) {
					newTickLabel = "<div class='tickLabel'>" + ( tickLabelPos - settings.vRuleSize ) + "</div>";
					$(newTickLabel).css( "left", tickLabelPos+"px" ).appendTo($hRule);
				} else if ((( tickLabelPos - settings.vRuleSize ) %10 ) == 0 ) {
					newTickLabel = "<div class='tickMajor'></div>";
					$(newTickLabel).css("left",tickLabelPos+"px").appendTo($hRule);
				} else if ((( tickLabelPos - settings.vRuleSize ) %5 ) == 0 ) {
					newTickLabel = "<div class='tickMinor'></div>";
					$(newTickLabel).css( "left", tickLabelPos+"px" ).appendTo($hRule);
				}
				tickLabelPos = (tickLabelPos + 5);				
			}//hz ticks
			
			// Vertical ruler ticks
			tickLabelPos = settings.hRuleSize;
			newTickLabel = "";
			while (tickLabelPos <= $vRule.height()) {
				if ((( tickLabelPos - settings.hRuleSize ) %50 ) == 0) {
					newTickLabel = "<div class='tickLabel'><span>" + ( tickLabelPos - settings.hRuleSize ) + "</span></div>";
					$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
				} else if (((tickLabelPos - settings.hRuleSize)%10) == 0) {
					newTickLabel = "<div class='tickMajor'></div>";
					$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
				} else if (((tickLabelPos - settings.hRuleSize)%5) == 0) {
					newTickLabel = "<div class='tickMinor'></div>";
					$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
				}
				tickLabelPos = ( tickLabelPos + 5 );				
			}//vert ticks
		});//resize
		
		return this.each(function() {
			var $this = $(this);
			
			// Attach rulers
			
			// Should not need 1 min padding-top of 1px but it does
			// will figure it out some other time
			//back this padding below if needed
			//$this.css("padding-top", settings.hRuleSize + 1 + "px");
			if (settings.hRuleSize > 0) {				
				$(hRule).height(settings.hRuleSize).prependTo($this);
			}
			
			if (settings.vRuleSize > 0) {
				var oldWidth = $this.outerWidth();
				//back this padding below if needed
				//$this.css("padding-left", settings.vRuleSize + 1 + "px").outerWidth(oldWidth);
				$(vRule).width(settings.vRuleSize).height($this.outerHeight()).prependTo($this);
			}
			
			if ( (settings.vRuleSize > 0) && (settings.hRuleSize > 0) ) {
				$(corner).css({
					width: settings.vRuleSize,
					height: settings.hRuleSize
				}).prependTo($this);
			}
			
			
			var $hRule = $this.children('.hRule');
			var $vRule = $this.children('.vRule');
		
			// Horizontal ruler ticks
			var tickLabelPos = settings.vRuleSize;
			var newTickLabel = "";
			while ( tickLabelPos <= $hRule.width() ) {
				if ((( tickLabelPos - settings.vRuleSize ) %50 ) == 0 ) {
					newTickLabel = "<div class='tickLabel'>" + ( tickLabelPos - settings.vRuleSize ) + "</div>";
					$(newTickLabel).css( "left", tickLabelPos+"px" ).appendTo($hRule);
				} else if ((( tickLabelPos - settings.vRuleSize ) %10 ) == 0 ) {
					newTickLabel = "<div class='tickMajor'></div>";
					$(newTickLabel).css("left",tickLabelPos+"px").appendTo($hRule);
				} else if ((( tickLabelPos - settings.vRuleSize ) %5 ) == 0 ) {
					newTickLabel = "<div class='tickMinor'></div>";
					$(newTickLabel).css( "left", tickLabelPos+"px" ).appendTo($hRule);
				}
				tickLabelPos = (tickLabelPos + 5);				
			}//hz ticks
			
			// Vertical ruler ticks
			tickLabelPos = settings.hRuleSize;
			newTickLabel = "";
			while (tickLabelPos <= $vRule.height()) {
				if ((( tickLabelPos - settings.hRuleSize ) %50 ) == 0) {
					newTickLabel = "<div class='tickLabel'><span>" + ( tickLabelPos - settings.hRuleSize ) + "</span></div>";
					$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
				} else if (((tickLabelPos - settings.hRuleSize)%10) == 0) {
					newTickLabel = "<div class='tickMajor'></div>";
					$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
				} else if (((tickLabelPos - settings.hRuleSize)%5) == 0) {
					newTickLabel = "<div class='tickMinor'></div>";
					$(newTickLabel).css( "top", tickLabelPos+"px" ).appendTo($vRule);
				}
				tickLabelPos = ( tickLabelPos + 5 );				
			}//vert ticks			
			
		});//each		
		
	};//ruler
})( jQuery );