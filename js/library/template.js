/**
 * 
 * @author Ervinne Sodusta
 */

/**
 * 
 * @param {type} data
 * @returns {String.prototype@call;replace}
 */
String.prototype.renderTemplate = function(data) {

    return this.replace(/{=(\w+)}/g, function(match, key) {
//    return this.replace(/{=*}/g, function(match, key) {        
        return typeof data[key] !== 'undefined'
                ? data[key]
                : match
                ;
    });

};