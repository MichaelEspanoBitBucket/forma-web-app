<?php
$mod = unserialize(ENABLE_COMPANY_MOD);
include "layout/main-nav-svg.phtml";
?>

<div class="fl-admindashboard-icons-wrapper">
   <ul>
      <?php if($mod['administer']['user_settings'] != "0"){ ?>
      <li><a href="/user_view/user_settings">
           <div align="center"><!-- <i class="fa fa-user"></i> -->
               <svg class="icon-svg adminbar-svg" viewBox="-5 -5 100.264 100.597">
                  <use xlink:href="#sample-2-layers"></use>
              </svg> 
           </div>
           <div class="fl-admin-icon-name">Users</div>
         </a>
      </li>
      <?php } ?>
      <?php if($mod['organizational_charts']['create_organizational_chart'] == "1"){ ?>
      <li><a href="/user_view/organizational_chart">
         <div align="center"><!-- <i class="fa fa-sitemap"></i> -->
            <svg class="icon-svg adminbar-svg" viewBox="-5 -5 100.264 100.597">
                  <use xlink:href="#svg-icon-adminnav-crearte-org-chart"></use>
              </svg>
         </div>
         <div class="fl-admin-icon-name">Create Organizational Chart</div>
         </a>
      </li>
      <?php } ?>
      <?php if($mod['forms']['create_forms'] == "1"){ ?>
      <li><a href="/user_view/formbuilder?formID=0">
         <div align="center"><!-- <i class="fa fa-list-alt"></i> -->
            <svg class="icon-svg adminbar-svg" viewBox="-5 -5 100.264 100.597">
                  <use xlink:href="#svg-icon-adminnav-create-forms"></use>
              </svg>
         </div>
         <div class="fl-admin-icon-name">Create Form</div>
         </a>  
      </li>
      <?php } ?>
      <?php if($mod['workflows']['create_workflow'] == "1"){ ?>
      <li><a href="/user_view/workflow">
         <div align="center"><!-- <i class="fa fa-retweet"></i> -->
            <svg class="icon-svg adminbar-svg" viewBox="-5 -5 100.264 100.597">
                  <use xlink:href="#svg-icon-adminnav-create-workflow"></use>
              </svg>
         </div>
         <div class="fl-admin-icon-name">Create Workflow</div>
         </a>
      </li>
      <?php } ?>
      <?php if($mod['reports']['create_reports'] == "1"){ ?>
      <li><a href="/user_view/report">
         <div align="center"><!-- <i class="fa fa-line-chart"></i> -->
            <svg class="icon-svg adminbar-svg" viewBox="-5 -5 100.264 100.597">
                  <use xlink:href="#svg-icon-adminnav-create-report"></use>
              </svg>
         </div>
         <div class="fl-admin-icon-name">Create Report</div>
         </a>  
      </li>
      <?php } ?>
      <div class="clearfix"></div> 
   </ul> 
</div>