<?php
$mod = unserialize(ENABLE_COMPANY_MOD);
?>
<div class="fl-admindashboard-icons-wrapper admindashboard-icons-wrapper2">
   <ul>
      <?php if($mod['administer']['user_settings'] != "0"){ ?>
      <li><a href="/user_view/user_settings">
           <div style="float: right; margin:10px 10px 0px 0px; padding: 5px; box-sizing: border-box; color:#222; text-align:center;"><h1 style="font-size:30px;">99+</h1>Users</div>
           <i class="fa fa-user"></i>
           <div class="fl-admin-icon-name">Users</div>
         </a>
      </li>
      <?php } ?>
      <?php if($mod['organizational_charts']['create_organizational_chart'] == "1"){ ?>
      <li><a href="/user_view/organizational_chart">
         <div style="float: right; margin:10px 10px 0px 0px; padding: 5px; box-sizing: border-box; color:#222; text-align:center;"><h1 style="font-size:30px;">20</h1>Charts</div>
         <i class="fa fa-sitemap"></i>
         <div class="fl-admin-icon-name">Create Organizational Chart</div>
         </a>
      </li>
      <?php } ?>
      <?php if($mod['forms']['create_forms'] == "1"){ ?>
      <li><a href="/user_view/formbuilder?formID=0">
         <div style="float: right; margin:10px 10px 0px 0px; padding: 5px; box-sizing: border-box; color:#222; text-align:center;"><h1 style="font-size:30px;">45</h1>Forms</div>
         <i class="fa fa-list-alt"></i>
         <div class="fl-admin-icon-name">Create Forms</div>
         </a>  
      </li>
      <?php } ?>
      <?php if($mod['workflows']['create_workflow'] == "1"){ ?>
      <li><a href="/user_view/workflow">
         <div style="float: right; margin:10px 10px 0px 0px; padding: 5px; box-sizing: border-box; color:#222; text-align:center;"><h1 style="font-size:30px;">13</h1>Workflows</div>
         <i class="fa fa-retweet"></i>
         <div class="fl-admin-icon-name">Create Workflows</div>
         </a>
      </li>
      <?php } ?>
      <?php if($mod['reports']['create_reports'] == "1"){ ?>
      <li><a href="/user_view/report">
         <div style="float: right; margin:10px 10px 0px 0px; padding: 5px; box-sizing: border-box; color:#222; text-align:center;"><h1 style="font-size:30px;">4</h1>Reports</div>
         <i class="fa fa-bar-chart-o"></i>
         <div class="fl-admin-icon-name">Create Reports</div>
         </a>  
      </li>
      <?php } ?>
      <div class="clearfix"></div> 
   </ul> 
</div>.