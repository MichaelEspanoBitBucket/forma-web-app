<div class="fl-workspace-control-container" style="<?php echo $displayPreviewStyle; ?>">
    <?php include 'layout/workspace_request_action_bar.php'; ?> 
    <div class="fl-controlsv2-main-warpper workspace_option_tab fl-controlsv2-main-warpper-default <?= $workspace_control_var; ?>" style="padding-top:<?php echo $topdefinedMenuPadding; ?>;">
        <div class="fl-controlsv2-wrapper">
            <div class="clearfloat">
                <ul id="fl-tabs-formcontrolsv2">
                    <li id="fl-form-controlsv-li1" class="inactive_annotation isDisplayNone"><i class="fa fa-gear"></i> Workspace Options</li>
                    <?php if (ALLOW_EXTERNAL_LINKS == "1") { ?>   
                        <?php if (count($form_json['form_json']['external_link']) > 0) { ?>
                            <?php if ($requestID != 0) { ?>
                                <li id="fl-form-controlsv-li4" attr="active" class="inactive_annotation"><i class="fa fa-file-text-o"></i> Related Information</li>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                    <?php if (ALLOW_FORM_ANNOTATION == "1") { ?>
                        <li id="fl-form-controlsv-li2" class="tabAnnotation addAnnotation"><i class="fa fa-pencil-square-o"></i> Annotations</li>
                    <?php } ?>
                    <?php if (ALLOW_DEFAULT_ACTIONS == "1") { ?>    
                        <li id="fl-form-controlsv-li3" class="inactive_annotation checkDefaultActions isDisplayNone"><i class="fa fa-hand-o-up"></i> Default Actions</li>
                    <?php } ?>
                </ul>
                <?php include 'layout/workspace_headerv2.php'; 
                    if($request_view_datas["form_alias"] != ""){
                    $show_name = $request_view_datas["form_alias"];
                    }else{
                    $show_name = $request_view_datas["form_name"];
                    }
                ?>
                <div class="fl-form-name-wrapper" title="<?php echo  Config::get_page_alias(); ?>"  style="left:5px !important;"><?php echo  Config::get_page_alias(); ?>
                </div>
            </div>
            <?php
            if (ALLOW_EXTERNAL_LINKS == "1") {
                if (count($form_json['form_json']['external_link']) == 0) {
                    $workpsaceHeight = "height:0px";
                }

                if ($requestID == 0) {
                    $workpsaceHeight = "height:0px";
                }
            }

            if (ALLOW_FORM_ANNOTATION == "1") {
                $workpsaceHeight = "height:65px !important;";
            }
            ?>
            <div id="fl-workspace-controls" style="<?php echo $workpsaceHeight; ?>" class="fl-workspace-control-default <?= $workspace_control_var; ?>">
                <div class="tab-wrapper fl-tab-wrapper-headFull isDisplayNone" id="fl-form-controlsv-li1t">
                    <div class="fl-workspace-control-wrapper">
                        <div class="fl-form-contr-hd"><span>BASIC OPTIONS</span></div>
                        <div class="fl-workspace-button-wrapper">

                            <button class="isCursorPointer fl-backhistory"><i class="fa fa-reply"></i> Go Back</button>

                            <?php
                            if (ALLOW_PROCESSOR_ATTACHMENT != "1") {
                                if ($request_details['Requestor'] == $auth['id'] && $_GET['requestID'] != "0") {
                                    $display = "";
                                } elseif ($request_details['Requestor'] != $auth['id'] && $_GET['requestID'] == "0") {
                                    $display = "";
                                } else {
                                    $display = "display";
                                }
                            } else {
                                $display = "";
                            }
                            /* ========== Get Attachment files ========== */
                            $path = "images/attachment";
                            if (isset($_GET['trackNo'])) {
                                $trackNo = $_GET['trackNo'];
                            } else {
                                $trackNo = $request_details['TrackNo'];
                            }

                            if ($_GET['view_type'] != "update") {

                                if ($_GET['requestID'] != 0) {
                                    echo '<button class="isCursorPointer showReply" data-type="1" data-f-id="' . $_GET['formID'] . '" data-id="' . $requestID . '" active-comment="false"><i class="fa fa-comment"></i> Add / View Comment</button>';
                                }
                                echo '<button class="isCursorPointer viewAuditLogs" style="' . $displayPreviewStyle . '"><i class="fa fa-book"></i> View Logs</button>';
                                echo '<button class="isCursorPointer printForm" style="' . $displayPreviewStyle . ' position:relative; z-index:2;"><i class="fa fa-print"></i> Print Form</button>';

                                if (ALLOW_DEFAULT_ACTIONS == "1") {
                                    echo functions::getSingleDefaultAction($form_json['form_json'], $requestID, $getID, "2", "Notes");
                                }
                                $mod = unserialize(ENABLE_COMPANY_MOD);
                                $gi_manager = $mod["gs3_insights"]["gi-report-designer"];
                                $gi_designer = $mod["gs3_insights"]["gi-report-designer-list"];
                                $allow_pin = ($gi_manager == 0 && $gi_designer == 0);

                                if (!functions::getPinnedRequest($getID, $requestID)
                                ) {
                                    if ($allow_pin == true) {
                                        echo '<button class=" isCursorPointer request_dashboard_pin" fl-data-application-id="' . $application_id . '" style="position:relative; z-index:2;"><i class="fa fa-thumb-tack"></i> Pin To Dashboard</button>';
                                    }
                                }


                                if (ALLOW_GUEST == "1") {
                                    if ($_GET['requestID'] != 0) {
                                        if ($auth['user_level_id'] != "4") {
                                            echo '<button class="isCursorPointer addGuest" data-action="guest_selected_form" style="position:relative; z-index:2;"><i class="fa fa-user"></i> Add Guest</button>';
                                        }
                                    }
                                }
                            }

                            if ($_GET['view_type'] != "update") {
                                //echo '<span class="viewer_action"></span>'; //add viewer button
                                //echo '<button class="isCursorPointer uploadButtonRequest"><i class="fa fa-paperclip"></i> Add Files</button>';
                                echo '<button class="' . $display . 'uploadButtonRequest isCursorPointer" style="position: relative;">';
                                echo '<i class="fa fa-paperclip"></i>';
                                echo '<span> Add Files</span>';
                                echo '<form id="fille_attach" method="post" enctype="multipart/form-data" action="/ajax/request_attachment">';
                                echo '<input type="file" data-action-id="2" value="upload" name="file" id="file" size="24" data-action-type="attachFile" style="width: 144px; left:-77px; opacity: 0;position:absolute; height:14px; top:-2px; font-size:11px;" class="cursor upfile uploadFileWorkspace">';
                                echo '<input type="text" name="getFormID" id="getFormID" value="' . $_GET['formID'] . '" data-type="longtext" class="display getFields" />';
                                echo '<input type="text" name="getID" id="getID" value="' . $_GET['requestID'] . '" data-type="longtext" class="display getFields" />';
                                echo '<input type="text" name="getTrackNo" id="getTrackNo" value="' . $trackNo . '" data-type="longtext" class="display getFields" />';
                                echo '</form></button> ';
                                echo '<img src="/images/loader/load.gif" class="display attachFiles"/>';
                            } else {
                                echo '<button class="isCursorPointer printForm" style="position:relative; z-index:2;"><i class="fa fa-print"></i> Print Form</button> ';
                            }
                            ?> 

                        </div>
                    </div>
                </div>
                <?php if (ALLOW_FORM_ANNOTATION == "1") { ?>
                    <div class="tab-wrapper" id="fl-form-controlsv-li2t">
                        <div class="fl-tab-bluebar"></div>
                        <div id="fl-workspace-tab-reqestview" class="fl-workspace-control-content-wrapper">
                            <div class="fl-workspace-control-wrapper  fl-fb-wrapper fl-annotation-actions">
                                <div class="fl-form-contr-hd"><span>ACTIONS</span></div>
                                <div class="fl-workspace-button-wrapper section clearing">
                                    <div class="div_1_of_1 column">
                                        <button class="isCursorPointer saveDraftAnnotation">
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-saveannotation"></use>
                                            </svg>
                                            <span>Save Annotation</span>
                                        </button>
                                        <button class="isCursorPointer cancelAnnotation" cancel="false">
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-cancelannotation"></use>
                                            </svg>
                                            <span>Cancel Annotation</span>
                                        </button>
                                        <button class="isCursorPointer trigger_tools" data-type="undo">
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-undo"></use>
                                            </svg>
                                            <span>Undo Annotation</span>
                                        </button>
                                        <button class="isCursorPointer trigger_tools" data-type="redo">
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-redo"></use>
                                            </svg>
                                            <span>Redo Annotation</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="fl-workspace-control-wrapper  fl-fb-wrapper fl-annotation-tools">
                                <div class="fl-form-contr-hd"><span>TOOLS</span></div>
                                <div class="fl-workspace-button-wrapper  section clearing">
                                    <div class="column div_1_of_1">
                                        <button class="isCursorPointer trigger_tools" data-type="clear">
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-clearannotation"></use>
                                            </svg>
                                            <span>Clear Annotation</span>
                                        </button>
                                        <button class="isCursorPointer trigger_tools" data-type="rectangle" >
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-rectangle"></use>
                                            </svg>
                                            <span>Rectangle</span>
                                        </button>
                                        <button class="isCursorPointer trigger_tools" data-type="ellipse">
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-circle"></use>
                                            </svg>
                                            <span>Circle</span>
                                        </button>
                                        <button class="isCursorPointer trigger_tools" data-type="line">
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-line"></use>
                                            </svg>
                                            <span>Line</span>
                                        </button>
                                        <button class="isCursorPointer trigger_tools" data-type="pencil" >
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-pencil"></use>
                                            </svg>
                                            <span>Pencil</span>
                                        </button>
                                        <button class="isCursorPointer trigger_tools" data-type="text" >
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-text"></use>
                                            </svg>
                                            <span>Text</span>
                                        </button>
                                        <button class="isCursorPointer trigger_tools" data-type="eraser" >
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-eraser"></use>
                                            </svg>
                                            <span>Eraser</span>
                                        </button>
                                        <!-- <button class="isCursorPointer trigger_tools" data-type="bucket">
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-requestview-bucket"></use>
                                            </svg>
                                            <span>Bucket</span>
                                        </button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>



                <?php if (ALLOW_DEFAULT_ACTIONS == "1") { ?>   
                    <div class="tab-wrapper fl-tab-wrapper-headFull isDisplayNone" id="fl-form-controlsv-li3t">
                        <div class="fl-workspace-control-wrapper">
                            <div class="fl-form-contr-hd">Default actions</div>
                            <div class="fl-workspace-button-wrapper">
                                <ul id="fl-default-action-inner">
                                    <button class="isCursorPointer fl-backhistory"><i class="fa fa-reply"></i> Go Back</button>
                                    <?php
                                    echo functions::getDefaultActionButtons($form_json['form_json'], $requestID, $getID, "2", "Notes");
                                    ?>
                                </ul>
                            </div>
                        </div>
                        </duv>
                    </div>
                <?php } ?>
                <?php if (ALLOW_EXTERNAL_LINKS == "1") { ?>
                    <?php
                    if (count($form_json['form_json']['external_link']) > 0) {
                        if ($requestID != 0) {
                            ?>
                            <div class="tab-wrapper fl-tab-wrapper-headFull" id="fl-form-controlsv-li4t">
                                <div class="fl-workspace-control-wrapper">
                                    <div class="fl-form-contr-hd isDisplayNone">Related Forms</div>
                                    <div class="fl-workspace-button-wrapper-relatedforms">
                                        <ul id="fl-links-option-inner">
                                            <?php
                                            foreach ($getModules as $key_category => $value_category) {
                                                ?>

                                                <?php
                                                ?>
                                                <?= "<div class='fl-links-option-category'><ul>"; ?>
                                                <?php
                                                foreach ($value_category as $key => $value) {
                                                    $get_form_id = functions::base_encode_decode("encrypt", $value['form_id']);
                                                    if($value['form_alias'] != ""){
                                                        $my_request_title = $value['form_alias'];
                                                    }else{
                                                        $my_request_title = $value['form_name'];
                                                    }

                                                    ?>
                                                    <?= "<li class='ext-links dudes' title='". $my_request_title ."' form-id='" . $value['form_id'] . "'><a href='/user_view/application?id=" . $get_form_id . "' target='_blank'><i class='fa fa-file-text-o'></i> "  . $my_request_title . "</a></li>"; ?>
                                                    <?php
                                                }
                                                ?>
                                                <?= "</ul></div>"; ?>
                                                <?php
                                                ?>
                                                <?= "</li>"; ?>
                                                <?php
                                            }
                                            ?>
                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>
                                </div>
                                </duv>
                            </div>
                            <?php
                        }
                    }
                    ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="attached_file_container pr-nodes isDisplayNone fl-uploadfile-workspace">

        <?php
        $id_encrypt = md5(md5($trackNo));
        $dir = $path . "/" . $id_encrypt;

        /* ========== Get all files from the directory ========== */
        $getAttachment = $upload->getAllfiles_fromDirectory($dir);

        /* ========== List of files ========== */
        foreach ($getAttachment as $files) {
            $get_extension = explode(".", $files);
            $extension = $get_extension[count($get_extension) - 1];
            /* ========== Determine if files is an image or not ========== */
            $formats = array("jpg", "png", "JPG", "PNG", "GIF", "gif");
            if (!in_array($extension, $formats)) {
                echo '<div style="border-radius:3px;padding:4px;margin-left:3px;min-height:40px; box-shadow: 1px 1px 2px -1px rgba(122,115,122,1); min-height:40px;">';
                echo '<form class="pull-left" method="POST" >';
                echo '<div style="text-align:center">';
                echo '<input type="submit" class="cursor tip" data-original-title="Download Attachment"/><i class="fa fa-download"></i> ';
                echo '<i class="' . $display . ' fa fa-trash-o cursor tip removeFiles" data-original-title="Remove Attachment" data-file="' . $files . '" data-location="' . $dir . '/' . $files . '"></i>';
                echo '</div>';
                echo '<a class="download_attachment">';
                echo '<div data-name="' . $files . '" class="AFiles_only avatar AFiles tip" data-placement="bottom" data-original-title="' . $files . '"  title="' . $files . '" style="text-align:center;"><i class="fa fa-file" style="margin-left:0px; margin-top:4px;"></i></div>'; ////
                echo '</a>';
                //echo '<input type="submit"/>';
                echo '<input type="hidden" value="' . $files . '" name="attachment_filename"/>';
                echo '<input type="hidden" value="' . $dir . '/' . $files . '" name="attachment_location"/>';
                echo '</form>';
                echo '</div>';
            } else {
                echo '<div style="float:left;background-color:#fff;border:1px solid #D5D5D5;border-radius:3px;padding:3px;margin-left:3px;min-height:40px; box-shadow: 1px 1px 2px -1px rgba(122,115,122,1);">';
                echo '<form class="pull-left" method="POST">';
                echo '<div style="text-align: center;">';
                echo '<input type="submit" style="opacity: 0;position: absolute;width: 10px;" class="cursor tip" data-original-title="Download Attachment"/><i class="fa fa-download"></i> ';
                echo '<i class="fa fa-trash-o cursor tip removeFiles" data-original-title="Remove Attachment" data-file="' . $files . '" data-location="' . $dir . '/' . $files . '"></i>';
                echo '</div>';
                echo '<a class="download_attachment">';
                echo '<img data-name="' . $files . '" src="/' . $dir . '/' . $files . '" width="25" height="25" class="AFiles imagetoPost avatar userPhoto personalPhoto userAvatar tip" onerror="this.src='. BROKEN_IMAGE .'"  data-placement="bottom"  data-original-title="' . $files . '" title="' . $files . '">'; //
                echo '</a>';
                //echo '<input type="submit"/>';
                echo '<input type="hidden" value="' . $files . '" name="attachment_filename"/>';
                echo '<input type="hidden" value="' . $dir . '/' . $files . '" name="attachment_location"/>';
                echo '</form>';
                echo '</div>';
            }
        }
        ?>
    </div>
</div>    
