<?php
if (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "organizational_chart")/* for organizational chart */ {
    ?>
    <div class="fl-controlsv2-main-warpper fl-controlsv2-main-warpper-classic">
        <div class="fl-controlsv2-wrapper">
            <div class="clearfloat">
                <ul id="fl-tabs-formcontrolsv2" class="">
                    <li id="fl-form-controlsv-li1" attr="active"><i class="fa fa-plus-square-o"></i> Controls</li>
                    <li id="fl-form-controlsv-li2"><i class="fa fa-newspaper-o"></i> Page Layout</li>
                    <!-- <li id="fl-form-controlsv-li2"><i class="fa fa-hand-o-up"></i> Actions</li> -->  
                    <div class="clearfix"></div>
                </ul>
                <?php include 'layout/workspace_headerv2.php' ?>
            </div>
            <div id="fl-workspace-controls">
                <div class="tab-wrapper fl-tab-wrapper-headFull" id="fl-form-controlsv-li1t">
                    <div class="fl-controls-controlDept">
                        <div class="fl-form-contr-hd"><span>BASIC</span></div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <button class="btn-basicBtn padding_5 cursor orgchart_node tip" data-original-title="Add Node" data-object-type="processNode" data-drop="workspace"><i class="fa fa-users"></i> Department</button>
                            </div>
                        </div>

                    </div>
                </div> 
                <div class="tab-wrapper fl-tab-wrapper-headFull" id="fl-form-controlsv-li2t">
                    <div class="fl-tab-bluebar"></div>
                    <div class="fl-controls-controlPagelayout">
                        <div class="fl-form-contr-hd"><span>SIZE</span></div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <label>Orgchart Size : </label>     
                                <select class="form-select form_size fl-input-form-settings fl-orgchart-size">
                                    <option class="fl-opt_custom_size" value="custom_size">Custom Size</option>
                                    <option value="8.5x11">Letter(8.5in x 11in)</option> 
                                    <option value="8.5x14">Legal(8.5in x 14in)</option> 
                                    <option value="7.25x10.5">Executive(7.25in x 10.5in)</option> 
                                    <option value="8.27x11.69">A4(8.27in x 11.69in)</option> 
                                    <option value="33.11x46.61">A0(33.11in x 46.61in)</option> 
                                    <option value="23.39x33.11">A1(23.39in x 33.11in)</option> 
                                    <option value="16.53x23.39">A2(16.53in x 23.39in)</option> 
                                    <option value="11.69x16.53">A3(11.69in x 16.53in)</option>
                                </select>
                                <br/><Br/>  
                                <label class="fl-custom-size">Custom Size : </label>
                                <input type="text" name="" class="form-text form_set_size form_size_width fl-input-form-settings" style="width:34.5%" placeholder="Width" value="">
                                <input type="text" name="" class="form-text form_set_size form_size_height fl-input-form-settings" style="width: 34.5%" placeholder="Height" value="">
                            </div>
                        </div>

                    </div>
                    <div class="fl-controls-controlRuler">
                        <div class="fl-form-contr-hd">&nbsp;&nbsp;<span>RULER</span></div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <label>Show Ruler : </label>
                                <label>
                                    Yes: 
                                    <input type="radio" class="f-show-ruler css-checkbox" id="Fbyes" value="yes" name="show-form-ruler-select"/>
                                    <label for="Fbyes" class="css-label"></label>
                                </label>
                                <label>
                                    No: 
                                    <input type="radio" class="f-show-ruler css-checkbox" id="Fbno" checked="checked" value="no" name="show-form-ruler-select"/>
                                    <label for="Fbno" class="css-label"></label>
                                </label>
                            </div>
                        </div>

                    </div>	

                </div>
            </div><!-- end Workspace control for org chart -->
        </div>
    </div>    
    <?php
} elseif (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "formbuilder")/* for formbuilder */ {
    ?>
    <div class="fl-controlsv2-main-warpper fl-controlsv2-main-warpper-classic">
        <div class="fl-controlsv2-wrapper">
            <div class="clearfloat">
                <ul id="fl-tabs-formcontrolsv2" class="">
                    <li id="fl-form-controlsv-li1" attr="active"><i class="fa fa-plus-square-o"></i> Controls</li>
                    <li id="fl-form-controlsv-li2"><i class="fa fa-cogs"></i> Properties</li>
                    <li id="fl-form-controlsv-li3"><i class="fa fa-newspaper-o"></i> Page Layout</li>
                    <div class="clearfix"></div>
                </ul>
                <?php include 'layout/workspace_headerv2.php' ?>
            </div>
            <div id="fl-workspace-controls" class="fl-workspace-control-classic">
                <div class="tab-wrapper" id="fl-form-controlsv-li1t">
                    
                    <div class="fl-tab-bluebar"></div>
                    <div id="fl-workspace-tab-controls">
                        <div class="fl-controls-controlBasic fl-fb-wrapper">
                            <div class="fl-form-contr-hd"><span>INPUT FIELDS</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_5 fl-input-fields_a" >
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Button" data-object-type="button" data-drop="formbuilder_page_btn"><i class="fa fa-square fl-fa-background"></i> Button</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Dropdown" data-object-type="dropdown" data-drop="workspace"><i class="fa fa-caret-square-o-down fl-fa-background"></i> Dropdown List</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Select Many" data-object-type="selectMany" data-drop="workspace"><i class="fa fa-list-ul fl-fa-background"></i> Select Many</button>

                                </div>
                                <div class="column div_1_of_5 fl-input-fields_b">
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Checkbox" data-object-type="checkbox" data-drop="workspace"><i class="fa fa-check-square-o fl-fa-background"></i> Checkbox</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Names" data-object-type="listNames" data-drop="workspace"><i class="fa fa-th-list fl-fa-background"></i> Names</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="TextArea" data-object-type="textarea" data-drop="workspace"><i class="fa fa-square-o fl-fa-background"></i> Text Area</button>


                                </div>
                                <div class="column div_1_of_5 fl-input-fields_c">
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Date Picker" data-object-type="datepicker" data-drop="workspace"><i class="fa fa-calendar fl-fa-background"></i> Date Picker</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Pick List" data-object-type="pickList" data-drop="workspace"><i class="fa fa-calendar fl-fa-background"></i> Pick List</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="TextBox"  data-object-type="textbox" data-drop="workspace"><i class="fa fa-square-o fl-fa-background"></i> Text Box</button>
                                </div>
                                <div class="column div_1_of_5 fl-input-fields_d">
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Date Time" data-object-type="dateTime" data-drop="workspace"><i class="fa fa-clock-o fl-fa-background"></i> Date Time Picker</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Radio Button" data-object-type="radioButton" data-drop="workspace"><i class="fa fa-dot-circle-o fl-fa-background"></i> Radio Button</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Time" data-object-type="time" data-drop="workspace"><i class="fa fa-clock-o fl-fa-background"></i> Time Picker</button>
                                </div>
                                <div class="column div_1_of_5 fl-input-fields_e">
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip isDisplayNone" data-original-title="Note Style Field" data-object-type="noteStyleField" data-drop="workspace" style=""><i class="fa fa-align-justify fl-fa-background" ></i> Notes Style</button>
                                </div>

                            </div>

                        </div>
                        <div class="fl-controls-controlUserInterface fl-fb-wrapper" style="width:354px;">
                            <div class="fl-form-contr-hd"><span>ALIGNMENTS & GRAPHICS</span></div>
                            <div class="section clearing">

                                <div class="column div_1_of_3 alignments-graphics_a" style="width:95px;">
                                    <?php
                                    if (ENABLE_ACCORDION == "1") {
                                        ?>
                                        <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Accordion" data-object-type="accordion" data-drop="workspace"><i class="fa fa-stack-overflow fl-fa-background"></i> Accordion</button>
                                        <?php
                                    }
                                    ?>
                                     <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Label"  data-object-type="labelOnly" data-drop="workspace"><i class="fa fa-font fl-fa-background"></i>Label</button>
                                     <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Table" data-object-type="table" data-drop="workspace"><i class="fa fa-table fl-fa-background"></i> Table</button>   
                                    
                                </div>
                                <div class="column div_1_of_3 alignments-graphics_b">
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Embedded View" data-object-type="embedView" data-drop="workspace"><i class="fa fa-eye fl-fa-background"></i> Embedded View</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Line" data-object-type="line" data-drop="workspace"><i class="fa fa-ellipsis-h fl-fa-background"></i>Line</button>        
                                    
                                   
                                    
                                </div>
                                <div class="column div_1_of_3 alignments-graphics_c" style="width:120px;">
                                     <div class="btn-basicBtn padding_5 cursor getObjects tip fl-formImage" data-original-title="Image" data-object-type="image" data-drop="workspace" style="position:relative;">
                                        <i class="fa fa-image fl-fa-background"></i> Image
                                        <form id="getformImage" method="post" enctype="multipart/form-data" action="/ajax/formUpload">
                                            <input type="file" accept="image/*" data-action-id="2" value="upload" name="image" id="formImage" size="24" data-action-type="postImageForm">
                                        </form>
                                    </div>
                                     <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Tabbed Panel" data-object-type="tabPanel" data-drop="workspace" ><i class="fa fa-share-square fl-fa-background"></i> Tabbed Panel</button>
                                </div>
                            </div>    
                        </div>
                        <div class="fl-controls-controlFiles fl-fb-wrapper">
                            <div class="fl-form-contr-hd"><span>ATTACHMENTS</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip " data-original-title="Multiple Request Import" data-object-type="multiplerequestImport" data-drop="workspace"><i class="fa fa-align-justify fl-fa-background"></i> Multiple Attachment</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Request Image" data-object-type="requestImage" data-drop="workspace"><i class="fa fa-picture-o fl-fa-background"></i> Request Image</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip " data-original-title="Single Attachment" data-object-type="requestImport" data-drop="workspace"><i class="fa fa-align-justify fl-fa-background"></i> Single Attachment</button>
                                </div>
                            </div>
                        </div>
                        <div class="fl-controls-controlContainer  isDisplayNone">
                            <div class="fl-form-contr-hd"><span>CONTAINER</span></div>
                            <?php
                            if (ENABLE_DETAILS_PANEL == "1") {
                                ?>
                                <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Details Panel" data-object-type="detailsPanel" data-drop="workspace"><i class="fa fa-child fl-fa-background"></i> Details Panel</button>
                                <?php
                            }
                            ?>



                        </div>
                        <div class="fl-controls-controlFieldAccess  fl-fb-wrapper">
                            <div class="fl-form-contr-hd"><span>&nbsp;&nbsp;FIELD ACCESS</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Computed" data-object-type="computed" data-drop="workspace"><i class="fa fa-file-text fl-fa-background"></i> Computed</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Editor support" data-object-type="textbox_editor_support" data-drop="workspace"><i class="fa fa-square-o fl-fa-background"></i> Editor Support</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Reader support" data-object-type="textbox_reader_support" data-drop="workspace"><i class="fa fa-square-o fl-fa-background"></i> Reader Support</button>
                                </div>
                            </div>
                        </div>
                        <div class="fl-controls-controlScanAndIdentities fl-fb-wrapper">
                            <div class="fl-form-contr-hd"><span>&nbsp;&nbsp;SCAN & IDENTIFY</span></div>
                            <div class="section clearing">
                                <div class="column did_1_of_1 fl-scan-and-identity_a">
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="QR Code" data-object-type="qrCodeScanner" data-drop="workspace"><i class="fa fa-qrcode fl-fa-background"></i> QR Code</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip" data-original-title="Bar Code" data-object-type="barCodeScanner" data-drop="workspace"><i class="fa fa-qrcode fl-fa-background"></i> Bar Code</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip isDisplayNone" data-original-title="Smart Bar Code" data-object-type="smartBarCode" data-drop="workspace"><i class="fa fa-qrcode fl-fa-background"></i>Smart Bar Code</button>
                                </div>
                            </div>        
                    </div>
                </div>
                </div><!-- end of controls -->
                <div class="tab-wrapper" id="fl-form-controlsv-li2t">
                    <div class="fl-tab-bluebar"></div>
                    <div id="fl-workspace-tab-properties">
                        <div class="fl-controls-controlAccess fl-fb-wrapper">
                            <div class="fl-form-contr-hd"><span>ACCESS</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_3 controlAccess_a">
                                    <button class="btn-basicBtn padding_5 cursor form-category tip" data-original-title="Form Category" data-form-type=""><i class="fa fa-folder"></i> Form Category</button>
                                    <button class="btn-basicBtn padding_5 cursor form-admin tip" data-original-title="Form Admin" data-form-type=""><i class="fa fa-user-md"></i> Form Admin</button>
                                    <button class="btn-basicBtn padding_5 cursor form-authors tip" data-original-title="Set Authors" data-form-type=""><i class="fa fa-pencil-square"></i> Authors</button>

                                </div>
                                <div class="column div_1_of_3 controlAccess_b">
                                    <button class="btn-basicBtn padding_5 cursor form-users tip" data-original-title="Form Users" data-form-type=""><i class="fa fa-user"></i> Form Users</button>
                                    <button class="btn-basicBtn padding_5 cursor form-readers tip" data-original-title="Set Viewers" data-form-type=""><i class="fa fa-folder-open"></i> Viewers</button>
                                    <button class="btn-basicBtn padding_5 cursor form-customized tip" data-original-title="Set Customized Author" data-form-type=""><i class="fa fa-print"></i> Customize Print</button>
                                </div>
                                <div class="column div_1_of_3 controlsAccess_c">  
                                    <?php
                                    if (ENABLE_DELETION == "1") {
                                        ?>
                                        <button class="btn-basicBtn padding_5 cursor form-delete-access tip" data-original-title="Delete Access" data-form-type=""><i class="fa fa-trash"></i> Delete Access</button>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div><!-- end of Access -->
                       
                        <div class="fl-controls-controlTools fl-fb-wrapper">
                            <div class="fl-form-contr-hd"><span>&nbsp;&nbsp;TOOLS</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_3 controlTools_a">
                                    <button class="btn-basicBtn padding_5 cursor form-header-info tip" data-original-title="View Settings" data-form-type=""><i class="fa fa-info-circle"></i> View Settings</button>
                                    <button class="btn-basicBtn padding_5 cursor field-process-sequence tip" data-original-title="Field Sequence" data-form-type=""><i class="fa fa-sort"></i> Middleware Sequence</button>
                                    <button class="btn-basicBtn padding_5 cursor form-url tip" data-original-title="Share Request URL" data-form-type="share_request_url"><i class="fa fa-link"></i> Request URL</button>
                                </div>
                                <div class="column div_1_of_3 controlTools_b">
                                    <button class="btn-basicBtn padding_5 cursor calendar-view tip" data-original-title="Calendar View" data-form-type=""><i class="fa fa-calendar"></i> Calendar View</button>
                                    <button class="btn-basicBtn padding_5 cursor form-data-variable" data-original-title="Data Sources" data-form-type=""><i class="fa fa-list-alt"></i> Data Sources</button>
                                    <?php
                                    if (ENABLE_DETAILS_PANEL == "1") {
                                        ?>
                                        <button class="btn-basicBtn padding_5 cursor details-panel tip" data-original-title="Details Panel" data-form-type="detailsPanel"><i class="fa fa-child"></i> Details Panel</button>
                                        <?php
                                    }
                                    ?>
                                    <!--added by japhet -->
                                    <button class="btn-basicBtn padding_5 cursor tabindex-panel tip" data-original-title="Field Tab Index" data-form-type=""><i class="fa fa-sort-amount-asc"></i><span> Field Tab Index</span></button>
                                    <!--====================================================================-->
                                </div>
                                <div class="column div_1_of_3 controlTools_c">
                                    <?php
                                    if (ENABLE_LANDING_PAGE == "1") {
                                        ?>
                                        <button class="btn-basicBtn padding_5 cursor landing-page getObjects tip isDisplayNone" data-original-title="Landing Page" data-object-type="landingPage" data-drop="workspace"><i class="fa fa-flag fl-fa-background"></i> Landing Page</button>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (ALLOW_EXTERNAL_LINKS == "1") {
                                        ?>
                                        <button class="btn-basicBtn padding_5 cursor share-link tip" data-original-title="Related Info" data-form-type=""><i class="fa fa-link"></i> Related Info</button>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (ALLOW_DEFAULT_ACTIONS == "1") {
                                        ?>
                                        <button class="btn-basicBtn padding_5 cursor default-action tip" data-original-title="Default Actions" data-form-type=""><i class="fa fa-hand-o-up"></i> Default Actions</button>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>

                        </div><!-- end of tools -->
                        <div class="fl-controls-genProperties multiple-field-property  fl-fb-wrapper">
                            <div class="fl-form-contr-hd"><span>&nbsp;&nbsp;GENERAL PROPERTIES</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <div class="fl-textfield-type-opt"> 
                                        <input type="hidden" name="test"/>
                                        <label>
                                            Label :
                                            <select  class="form-select show-form-design-select change_label_font_family label-gen-prop fl-input-form-settings">
                                                <?php
                                                $fontfam = array(
                                                    // "Arial"=>"Arial",
                                                    // "Times New Roman"=>"'Times New Roman'", 
                                                    "Droid Sans" => "droid_sansregular", 
                                                    // "Droid Sans Bold"=>"droid_sansregular_bold",

                                                    "Open Sans"=>"open_sanslight",
                                                    // "Open Sans Bold" => "open_sanslight_bold",
                                                    
                                                    /*"Protestant" => "protestant_regular",*/ // no bold fonts
                                                    
                                                    "Dustismo" => "dustismo_regular",
                                                    // "Dustismo Bold"=>"dustismo_regular_bold",
                                                    //"Dustismo Roman Regular"=>"dustismo_romanregular",
                                                    //"Dustismo Italic"=>"dustismo_italic",
                                                    //"Dustismo Bold Italic"=>"dustismobold_italic",
                                                    
                                                    //"Perspective Sans Black Italic"=>"perspective_sans_blackitalic",
                                                    //"Perspective Sans Bold Italic"=>"perspective_sansbold_italic",
                                                    //"Perspective Sans Italic"=>"perspective_sansitalic",
                                                    "Perspective" => "perspective_sansregular",
                                                    /*"Perspective Sans Bold" => "perspective_sansregular_bold",*/
                                                    
                                                    "Sansumi" => "sansumiregular",
                                                    /*"Sansumi Bold"=>"sansumiregular_bold",*/
                                                    //"Sansumi Demi Bold"=>"sansumi_demibold",
                                                    //"Sansumi Ultra Light Regular"=>"sansumi_ultralightregular",
                                                    // "Tradition Sans" => "traditionsans_xlightregular",
                                                    "Vera Humana" => "vera_humana_95regular",
                                                    /*"Vera Humana Bold"=>"vera_humana_95regular_bold",*/
                                                    //"Vera Humana 95 Bold Italic"=>"vera_humana_95bolditalic",
                                                    //"Vera Humana 95 Italic"=>"vera_humana_95italic",
                                                    "Webly Sleek" => "weblysleeklight",
                                                    /*"Webly Bold" => "weblysleeklight_bold",*/

                                                    // "Weezer" => "weezerfontregular",

                                                );
                                                ksort($fontfam);
                                                
                                                foreach ($fontfam as $key => $value) {

                                                    $key = htmlspecialchars($key);
                                                    echo '<option value="' . $value . '" attrfontbold="" attrfontNormal="">' . $key . '</option>';
                                                }
                                                ?>

                                            </select>
                                        </label>
                                    </div>
                                    <div class="fl-textfield-type-opt">
                                        <label>

                                            <select class="form-select show-form-design-select label-gen-prop change_label_font_size fl-input-form-settings">

                                                <option value="6px">6px</option>
                                                <option value="8px">8px</option>
                                                <option value="9px">9px</option>
                                                <option value="10px">10px</option>
                                                <option value="11px">11px</option>
                                                <option value="12px">12px</option>
                                                <option selected="selected" value="14px">14px</option>
                                                <option value="18px">18px</option>
                                                <option value="24px">24px</option>
                                                <option value="30px">30px</option>
                                                <option value="36px">36px</option>
                                                <option value="48px">48px</option>
                                                <option value="60px">60px</option>
                                                <option value="72px">72px</option>
                                            </select>
                                        </label> 
                                    </div>
                                    <!-- <div class="fl-textfield-type-opt">
                                            <label>

                                               <select class="form-select show-form-design-select change_label_font_weight fl-input-form-settings">

                                                            <option selected="selected" value="normal">Normal</option>
                                                            <option value ="bold">Bold</option>

                                               </select>
                                            </label>
                                    </div> -->
                                    <div class="fl-textfield-type-opt"> 
                                        <label>

                                            <select class="form-select show-form-design-select label-gen-prop change_label_text_alignment fl-input-form-settings">

                                                <option selected="selected" value="Left">Left</option>
                                                <option value="Right">Right</option>
                                                <option value="Center">Center</option>
                                                <option value="Justify">Justify</option>   
                                            </select>
                                        </label>
                                    </div>
                                    <div class="fl-textfield-type-opt">

                                        <span class="spanbutton  "><input class="textdecor fl-input-form-settings label-gen-prop change_label_font_weight" has-bold="droid_sansregular" has-normal="droid_sansregular" type="checkbox" value="bold" id="c1"><label for="c1"><i class="fa fa-bold textpos"></i></label></span>
                                        <span class="spanbutton"><input class="textdecor fl-input-form-settings label-gen-prop change_label_font_style" type="checkbox" value="italic" id="c2"><label for="c2"><i class="fa fa-italic textpos"></i></label></span>
                                        <span class="spanbutton"><input class="textdecor fl-input-form-settings label-gen-prop change_label_text_decoration" type="checkbox" value="underline" id="c3"><label for="c3"><i class="fa fa-underline textpos"></i></label></span>
                                        <input type="text" value="" class="change_label_color label-gen-prop spectrumclass fl-input-form-settings" data-type="button"/>


                                    </div>
                                    <!-- <div class="fl-textfield-type-opt" style="display:inline-block">   
                                            <input type="text" value="" class="change_label_color spectrumclass fl-input-form-settings" data-type="button"/>

                                    </div>   -->

                                    <br/>

                                    <div class="fl-textfield-type-opt">
                                        <label>
                                            Field :
                                            <select class="form-select show-form-design-select field-gen-prop change_field_font_family fl-input-form-settings">
                                                <?php
                                                ksort($fontfam);
                                                foreach ($fontfam as $key => $value) {
                                                    $key = htmlspecialchars($key);
                                                    echo '<option value="' . $value . '" attrfontbold="" attrfontNormal="">' . $key . '</option>';
                                                }
                                                ?>

                                            </select>
                                        </label>
                                    </div>
                                    <div class="fl-textfield-type-opt">
                                        <label>

                                            <select class="form-select show-form-design-select field-gen-prop change_field_font_size fl-input-form-settings">

                                                <option value="6px">6px</option>
                                                <option value="8px">8px</option>
                                                <option value="9px">9px</option>
                                                <option value="10px">10px</option>
                                                <option value="11px">11px</option>
                                                <option value="12px">12px</option>
                                                <option selected="selected" value="14px">14px</option>
                                                <option value="18px">18px</option>
                                                <option value="24px">24px</option>
                                                <option value="30px">30px</option>
                                                <option value="36px">36px</option>
                                                <option value="48px">48px</option>
                                                <option value="60px">60px</option>
                                                <option value="72px">72px</option>
                                            </select>
                                        </label>
                                    </div> 
                                    <!-- <div class="fl-textfield-type-opt">
                                            <label>
                                               <select class="form-select show-form-design-select change_field_font_weight fl-input-form-settings">

                                                            <option selected="selected" value="normal">Normal</option>
                                                            <option value ="bold">Bold</option>

                                               </select>
                                            </label>
                                    </div> -->
                                    <div class="fl-textfield-type-opt"> 
                                        <label>
                                            <select class="form-select show-form-design-select field-gen-prop change_field_text_alignment fl-input-form-settings">

                                                <option selected="selected" value="Left">Left</option>
                                                <option value="Right">Right</option>
                                                <option value="Center">Center</option>
                                                <option value="Justify">Justify</option>  
                                            </select>
                                        </label>
                                    </div>
                                    <div class="fl-textfield-type-opt">
                                        <div>
                                            <span class="spanbutton "><input class="textdecor fl-input-form-settings field-gen-prop change_field_font_weight" type="checkbox" has-bold="droid_sansregular" has-normal="droid_sansregular" value="bold" id="d1"><label for="d1"><i class="fa fa-bold textpos"></i></label></span>
                                            <span class="spanbutton"><input class="textdecor fl-input-form-settings field-gen-prop change_field_font_style" type="checkbox" value="italic" id="d2"><label for="d2"><i class="fa fa-italic textpos"></i></label></span>
                                            <span class="spanbutton"><input class="textdecor fl-input-form-settings field-gen-prop change_field_text_decoration" type="checkbox" value="underline" id="d3"><label for="d3"><i class="fa fa-underline textpos"></i></label></span>
                                            <input type="text" value="" class="field-gen-prop change_field_color spectrumclass fl-input-form-settings" data-type="button"/>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div><!-- end of general properties -->
                        <div class="fl-controls-controlformEvents fl-fb-wrapper <?php echo (defined("ENABLE_FORMBUILDER_FORM_EVENTS") != null  && ENABLE_FORMBUILDER_FORM_EVENTS == "1")?"":"display2"; ?>"  style="width:220px;">
                            <div class="fl-form-contr-hd">&nbsp;&nbsp;<span>FORM EVENTS</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip " data-original-title="Onload" btn-formula-event="onload" data-ide-properties-type="ide-onload"><i class="fa fa-refresh fl-fa-background"></i> On-load</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip " data-original-title="Pre Save" btn-formula-event="ide-pre-save" data-ide-properties-type="ide-pre-save"><i class="fa fa-arrow-circle-down fl-fa-background"></i> Pre-save</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip " data-original-title="Post Save" btn-formula-event="ide-post-save" data-ide-properties-type="ide-post-save"><i class="fa fa-arrow-circle-up fl-fa-background"></i> Post-save</button>

                                    <button class="btn-basicBtn padding_5 cursor getObjects tip " data-original-title="Formula List" btn-formula-event="formula-list-dialog"><i class="fa fa-refresh fl-fa-background"></i> Formula List</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip isDisplayNone" data-original-title="clear-existing-formula" data-drop="workspace" ><i class="fa fa-arrow-circle-up fl-fa-background"></i> Clear my formulas for custom srcript</button>
                                    <button class="btn-basicBtn padding_5 cursor getObjects tip isDisplayNone" data-original-title="generate-custom-script" data-drop="workspace" ><i class="fa fa-arrow-circle-up fl-fa-background"></i> Generate Custom Scripting</button>
                                </div>
                            </div>         
                        </div>
                        <div class="fl-fb-wrapper"  style="width:220px;">
                            <div class="fl-form-contr-hd">&nbsp;&nbsp;<span>OTHER SETTINGS</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <label>
                                        <input type="checkbox" class="show-form-design-select css-checkbox" id="form-allow-sending-pdf" value="1" name="form-allow-sending-pdf" selected-icon=""><label for="form-allow-sending-pdf" class="css-label"></label> Allow sending PDF
                                        <button class="btn-basicBtn padding_5 cursor getObjects tip public-embed-setup" data-original-title="Public Iframe" data-placement="bottom" style="padding-right: 0px;">
                                            <svg class="icon-svg icon-svg-workspace" viewBox="0 0 90 90">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#svg-icon-workspace-control-form-events-formulalist"></use>
                                            </svg>
                                            <span>Public Page</span>
                                        </button>
                                    </label>
                                </div>
                            </div>         
                        </div>
                    </div> 
                </div>
                <div class="tab-wrapper" id="fl-form-controlsv-li3t">
                    <div class="fl-tab-bluebar"></div>
                    <div id="fl-workspace-tab-layout">
                        <div class="fl-controls-controlPagelayout fl-fb-wrapper">
                            <div class="fl-form-contr-hd"><span>SIZE</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <div id="fl-default-set-size">
                                        <label>Form Size : </label>
                                        <select class="form-select form_size fl-input-form-settings">
                                            <option class="fl-opt_custom_size" value="custom_size">Custom Size</option> 
                                            <option value="8.5x11">Letter(8.5in x 11in)</option> 
                                            <option value="8.5x14">Legal(8.5in x 14in)</option> 
                                            <option value="7.25x10.5">Executive(7.25in x 10.5in)</option> 
                                            <option value="8.27x11.69">A4(8.27in x 11.69in)</option> 
                                            <option value="33.11x46.61">A0(33.11in x 46.61in)</option> 
                                            <option value="23.39x33.11">A1(23.39in x 33.11in)</option> 
                                            <option value="16.53x23.39">A2(16.53in x 23.39in)</option> 
                                            <option value="11.69x16.53">A3(11.69in x 16.53in)</option>
                                            <option class="allow_portal_size" value="5.59375x10.5">Portal Size(5.59in x 10.5in)</option>
                                        </select>
                                        <br/><br/> 
                                        <label class="fl-custom-size">Custom Size : </label>
                                        <input type="text" name="" class="form-text form_set_size form_size_width fl-input-form-settings" style="width: 34.5%;" placeholder="Width" value="">
                                        <input type="text" name="" class="form-text form_set_size form_size_height fl-input-form-settings" style="width: 34.5%;" placeholder="Height" value="">
                                    </div>
                                    <div id="fl-grid-setting" class="isDisplayNone">
                                        <!-- grid cell number -->
                                        <label>Form Cell Grid Pane : </label>
                                        <input type="text" name="" class="form-text fl-input-form-settings fcp_cell_length fcp-cell-pane-columns" style="width: 20.5% !important; min-width: 28%;"  placeholder="Column No" value="4">
                                        <input type="text" name="" class="form-text fl-input-form-settings fcp_cell_length fcp-cell-pane-rows" style="width: 20.5% !important; min-width: 28%;" placeholder="Row No" value="6">
                                        <br/><br/>
                                        <!-- grid cell size -->
                                        <label>All Cell Grid Size : </label>
                                        <input type="text" name="" class="form-text fl-input-form-settings fcp-cell-size fcp-cell-width-size" style="width: 20.5% !important; min-width: 28%;" placeholder="Cell Width" value="255">
                                        <input type="text" name="" class="form-text fl-input-form-settings fcp-cell-size fcp-cell-height-size" style="width: 20.5% !important; min-width: 28%;"  placeholder="Cell Height" value="150">
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div class="fl-controls-controlRuler fl-fb-wrapper">
                            <div class="fl-form-contr-hd"><span>RULER</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <label>Show Ruler : </label>
                                    <label>
                                        Yes: 
                                        <input type="radio" class="f-show-ruler css-checkbox" id="Fbyes" value="yes" name="show-form-ruler-select"/>
                                        <label for="Fbyes" class="css-label"></label>
                                    </label>
                                    <label>
                                        No: 
                                        <input type="radio" class="f-show-ruler css-checkbox" id="Fbno" checked="checked" value="no" name="show-form-ruler-select"/>
                                        <label for="Fbno" class="css-label"></label>
                                    </label>
                                </div>
                            </div>

                        </div>
                        <div class="fl-controls-controlformDesign fl-fb-wrapper">
                            <div class="fl-form-contr-hd">&nbsp;&nbsp;<span>FORM DESIGN</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <label>
                                        Absolute (default) :
                                        <input type="radio" class="show-form-design-select css-checkbox" id="FdAbsolute" checked="checked" value="absolute" name="show-form-design-select"/> 
                                        <label for="FdAbsolute" class="css-label"></label>
                                    </label>
                                    <label>
                                        Grid :
                                        <input type="radio" class="show-form-design-select css-checkbox" id="FdGrid" value="relative" name="show-form-design-select"/>
                                        <label for="FdGrid" class="css-label"></label>
                                    </label>
                                </div>
                            </div>		
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <label class="isCursorPointer">
                                        <input type="checkbox" class="show-form-design-select css-checkbox" id="allow-portal" value="1" name="allow-portal"/> 
                                        <label for="allow-portal" class="css-label"></label>
                                        <span>Allow in Portal</span>
                                    </label>
                                    <button class="btn-basicBtn padding_5 cursor tip isDisplayNone choose_app_icon" data-original-title="Choose app icon" data-object-type="Choose_app_icon"><i class="fa fa-th fl-fa-background"></i> Choose Icon</button>
                                </div>
                            </div>         
                        </div>
                    </div>
                </div><!-- end of Page Layout -->
            </div><!-- end Workspace control for formbuilder -->
        </div>
    </div>    
    <?php
} elseif (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "workflow")/* for workflow */ {
    ?>
    <div class="fl-controlsv2-main-warpper fl-controlsv2-main-warpper-classic">
        <div class="fl-controlsv2-wrapper">
            <div class="clearfloat">
                <ul id="fl-tabs-formcontrolsv2" class="">
                    <li id="fl-form-controlsv-li1" attr="active"><i class="fa fa-plus-square-o"></i> Controls</li>
                    <li id="fl-form-controlsv-li2"><i class="fa fa-newspaper-o"></i> Page Layout</li>
                    <div class="clearfix"></div>
                </ul>
                <?php include 'layout/workspace_headerv2.php' ?>
            </div>
            <div id="fl-workspace-controls">
                <div class="tab-wrapper fl-tab-wrapper-headFull" id="fl-form-controlsv-li1t">
                    <div class="fl-tab-bluebar"></div>
                    <div class="fl-controls-controlBasic fl-fb-wrapper">
                        <div class="fl-form-contr-hd"><span>BASIC</span></div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <button class="btn-basicBtn padding_5 cursor start_node tip" data-original-title="Add Start Node" data-object-type="startNode" data-drop="workspace"><i class="fa fa-play-circle-o"></i> Start Node</button>
                                <button class="btn-basicBtn padding_5 cursor end_node tip" data-original-title="Add End Node" data-object-type="endNode" data-drop="workspace"><i class="fa fa-stop"></i> End Node</button>
                                <button class="btn-basicBtn padding_5 cursor process_node tip" data-original-title="Add Process Node" data-object-type="processNode" data-drop="workspace"><i class="fa fa-user"></i> Process Node</button>
                                <button class="btn-basicBtn padding_5 cursor  conditional_node tip" data-original-title="Add Conditional Node" data-object-type="conditionalNode" data-drop="workspace"><i class="fa fa-question-circle"></i> Conditional</button>
                                <button class="btn-basicBtn padding_5 cursor database_node tip" data-original-title="Add Database Node" data-object-type="databaseNoe" data-drop="workspace"><i class="fa fa-database"></i> Database Node</button>
                            </div>
                        </div>
                    </div>
                </div><!-- end of controls -->
                <div class="tab-wrapper fl-tab-wrapper-headFull" id="fl-form-controlsv-li2t">
                    <div class="fl-tab-bluebar"></div>
                    <div class="fl-controls-controlPagelayout fl-fb-wrapper">
                        <div class="fl-form-contr-hd"><span>SIZE</span></div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <label>Workflow Size : </label>     
                                <select class="form-select form_size fl-input-form-settings fl-workflow-size">
                                    <option class="fl-opt_custom_size" value="custom_size">Custom Size</option>
                                    <option value="8.5x11">Letter(8.5in x 11in)</option> 
                                    <option value="8.5x14">Legal(8.5in x 14in)</option> 
                                    <option value="7.25x10.5">Executive(7.25in x 10.5in)</option> 
                                    <option value="8.27x11.69">A4(8.27in x 11.69in)</option> 
                                    <option value="33.11x46.61">A0(33.11in x 46.61in)</option> 
                                    <option value="23.39x33.11">A1(23.39in x 33.11in)</option> 
                                    <option value="16.53x23.39">A2(16.53in x 23.39in)</option> 
                                    <option value="11.69x16.53">A3(11.69in x 16.53in)</option>
                                </select>
                                <br/><br/>
                                <label class="fl-custom-size">Custom Size : </label>
                                <input type="text" name="" class="form-text form_set_size form_size_width fl-input-form-settings" style="width: 34.5%;" placeholder="Width" value="">
                                <input type="text" name="" class="form-text form_set_size form_size_height fl-input-form-settings" style="width: 34.5%;" placeholder="Height" value="">
                            </div>
                        </div>


                    </div>
                    <div class="fl-controls-controlRuler">
                        <div class="fl-form-contr-hd">&nbsp;&nbsp;<span>RULER</span></div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <label>Show Ruler</label>
                                <label>
                                    Yes: 
                                    <input type="radio" class="f-show-ruler css-checkbox" id="Ogyes" value="yes" name="show-form-ruler-select"/>
                                    <label for="Ogyes" class="css-label"></label>
                                </label>
                                <label>
                                    No: 
                                    <input type="radio" class="f-show-ruler css-checkbox" id="Ogno" checked="checked" value="no" name="show-form-ruler-select"/>
                                    <label for="Ogno" class="css-label"></label>
                                </label>
                            </div>
                        </div>


                    </div>	
                </div><!-- end of Page Layout -->
            </div><!-- end Workspace control for workflow -->
        </div>
    </div>    
    <?php
} elseif (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "report")/* for report */ {
    ?>
    <div class="fl-controlsv2-main-warpper fl-controlsv2-main-warpper-classic">
        <div class="fl-controlsv2-wrapper">
            <div class="clearfloat">
                <ul id="fl-tabs-formcontrolsv2" class="">
                    <li id="fl-form-controlsv-li2" attr="active"><i class="fa fa-plus-square-o"></i> Controls</li>
                    <div class="clearfix"></div>
                </ul>
                <?php include 'layout/workspace_headerv2.php' ?>
            </div>
            <div id="fl-workspace-controls">
                <div class="tab-wrapper" id="fl-form-controlsv-li2t">
                    <div class="fl-tab-bluebar"></div>
                    <div class="fl-controls-report-basic fl-fb-wrapper">
                        <div class="fl-form-contr-hd">BASIC</div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <button class="btn-basicBtn  cursor padding_5 tip" action="append_parameter" data-original-title="Add Parameter"><i class="fa fa-sliders"></i> Parameter</button> 
                                <button  class="btn-basicBtn cursor padding_5 tip" action="add_column" data-original-title="Add Column"><i class="fa fa-columns"></i> Column</button>
                                <button  class="btn-basicBtn cursor padding_5 tip" action="add_plot_bond" data-original-title="Add Plot Bond"><i class="fa fa-area-chart"></i> Plot Bond</button> 
                            </div>
                        </div>

                    </div>
                    <div class="fl-controls-control-reportAccess fl-fb-wrapper">
                        <div class="fl-form-contr-hd">&nbsp;&nbsp;<span>ACCESS</span></div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <button class="btn-basicBtn padding_5 cursor form-reportUsers tip" data-original-title="Set Report Users" data-form-type=""><i class="fa fa-user"></i> Report Users</button>
                            </div>
                        </div>

                    </div> 
                </div><!-- end of Actions -->               
            </div><!-- end Workspace control for report  -->
        </div>
    </div>    
    <?php
} elseif (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "generate")/* for generate */ {
    ?>
    <div class="fl-controlsv2-main-warpper fl-controlsv2-main-warpper-classic">
        <div class="fl-controlsv2-wrapper">
            <div class="clearfloat">
                <ul id="fl-tabs-formcontrolsv2" class="">
                    <li id="fl-form-controlsv-li1" attr="active"><i class="fa fa-plus-square-o"></i> Objects</li>
                    <li id="fl-form-controlsv-li2"><i class="fa fa-code"></i> Html Value</li>
                    <li id="fl-form-controlsv-li3"><i class="fa fa-newspaper-o"></i> Page Layout</li>
                    <div class="clearfix"></div>
                </ul>
                <?php include 'layout/workspace_headerv2.php' ?>	
            </div>
            <div id="fl-workspace-controls">
                <div class="tab-wrapper" id="fl-form-controlsv-li1t">
                    <div class="fl-tab-bluebar"></div>
                    <div id="fl-workspace-tab-controls-generate-object" class="fl-workspace-control-content-wrapper">
                        <div class="fl-controls-controlGenerateBasic">
                            <div class="fl-form-contr-hd">BASIC</div>
                            <div class="section clearing">
                                <div class="column div_1_of_1">
                                    <?php
                                    $random = substr(md5(rand()), 0, 7);
                                    ?>
                                    <div class="generate_objects fl-scroll-overflow fl-workspace-button-wrapper">
                                        <button class="btn-basicBtn padding_5 cursor getObjects tip" data-count="<?php echo $random; ?>" data-original-title="Label Only" data-object-type="labelOnly" data-drop="workspace"><i class="icon-font fa fa-font"></i> Label Only</button>                          
                                        <button class="btn-basicBtn padding_5 cursor getObjects tip" data-count="<?php echo $random; ?>" data-original-title="Add Line" data-object-type="line" data-drop="workspace"><i class="icon-ellipsis-horizontal fa fa-ellipsis-h"></i> Add Line </button>
                                        <button class="btn-basicBtn padding_5 cursor getObjects tip" data-count="<?php echo $random; ?>" data-original-title="Image" data-object-type="image" data-drop="workspace">
                                            <form id="getformImage" method="post" enctype="multipart/form-data" action="/ajax/formUpload">
                                                <input type="file" accept="image/*" data-action-id="2" value="upload" name="image" id="formImage" size="24" data-action-type="postImageForm" style="opacity: 0;position: absolute;    width: 50px !important;cursor: pointer;" class="">
                                                <i class="icon-picture fa fa-picture-o"></i> Image
                                            </form>
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="fl-controls-controlGenerateaddedObjects fl-fb-wrapper">
                            <div class="fl-form-contr-hd" style="position:relative;"><span>APPENDED OBJECTS</span></div>
                            <div class="section clearing">
                                <div class="column div_1_of_1" style="width: 82%;margin-top: 0px;">
                                    <div class="appendObjectsHere fl-scroll-overflow">
                                        
                                    </div>
                                </div>      
                            </div>   
                        </div>
                    </div>
                </div><!-- end of object -->
                <div class="tab-wrapper fl-tab-wrapper-headFull" id="fl-form-controlsv-li2t">
                    <div class="fl-controls-controlHtmlValues">
                        <div class="fl-form-contr-hd">FORM HTML VALUES</div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <div class="tab_objects fl-scroll-overflow"></div>
                            </div>
                        </div>

                    </div>	
                </div><!-- end of html values -->
                <div class="tab-wrapper fl-tab-wrapper-headFull" id="fl-form-controlsv-li3t">
                    <div class="fl-tab-bluebar"></div>
                    <div class="fl-controls-controlPagelayout">
                        <div class="fl-form-contr-hd"><span>SIZE</span></div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <label>Form Size : </label>     
                                <select class="form-select form_size fl-input-form-settings">
                                    <option class="fl-opt_custom_size" value="custom_size">Custom Size</option>
                                    <option value="8.5x11">Letter(8.5in x 11in)</option> 
                                    <option value="8.5x14">Legal(8.5in x 14in)</option> 
                                    <option value="7.25x10.5">Executive(7.25in x 10.5in)</option> 
                                    <option value="8.27x11.69">A4(8.27in x 11.69in)</option> 
                                    <option value="33.11x46.61">A0(33.11in x 46.61in)</option> 
                                    <option value="23.39x33.11">A1(23.39in x 33.11in)</option> 
                                    <option value="16.53x23.39">A2(16.53in x 23.39in)</option> 
                                    <option value="11.69x16.53">A3(11.69in x 16.53in)</option>
                                </select>
                                <br/><br/>
                                <label class="fl-custom-size">Custom Size : </label>
                                <input type="text" name="" class="form-text form_set_size form_size_width fl-input-form-settings" style="width: 34.5%;" placeholder="Width" value="">
                                <input type="text" name="" class="form-text form_set_size form_size_height fl-input-form-settings" style="width: 34.5%;" placeholder="Height" value="">
                            </div>
                        </div>



                    </div>
                    <div class="fl-controls-controlRuler">
                        <div class="fl-form-contr-hd">&nbsp;&nbsp;<span>RULER</span></div>
                        <div class="section clearing">
                            <div class="column div_1_of_1">
                                <label>Show Ruler</label>
                                <label>
                                    Yes: 
                                    <input type="radio" class="f-show-ruler css-checkbox" id="Ogyes" value="yes" name="show-form-ruler-select"/>
                                    <label for="Ogyes" class="css-label"></label>
                                </label>
                                <label>
                                    No: 
                                    <input type="radio" class="f-show-ruler css-checkbox" id="Ogno" checked="checked" value="no" name="show-form-ruler-select"/>
                                    <label for="Ogno" class="css-label"></label>
                                </label>
                            </div>
                        </div>

                    </div>			
                </div><!-- end of Page Layout -->
            </div><!-- end Workspace control for generate -->
        </div>
    </div>
    <?php
}
?>	