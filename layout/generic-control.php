<div class="fl-generic-control">
    <div id="fl-default-action" class="fl-generic-control-innner">
        <ul id="fl-default-action-inner">
            <?php
            echo functions::getDefaultActionButtons($form_json['form_json'], $requestID, $getID, "2");
            ?>
        </ul>
    </div>
    <div id="" class="fl-generic-control-innner">
   
        <ul id="fl-links-option-inner">
            <?php
            foreach ($getModules as $key_category => $value_category) {
                ?>
                
                <?php
                ?>
                <?= "<ul class=''>"; ?>
                <?php
                foreach ($value_category as $key => $value) {
                    $get_form_id = functions::base_encode_decode("encrypt", $value['form_id']);
                    ?>
                    <?= "<li class='ext-links' form-id='" . $value['form_id'] . "'><i class='fa fa-file-text-o'></i> <a href='/user_view/application?id=" . $get_form_id . "'> " . $key_category." - ".$value['form_name'] . "</a></li>"; ?>
                    <?php
                }
                ?>
                <?= "</ul>"; ?>
                <?php
                ?>
                <?= "</li>"; ?>
                <?php
            }
            ?>
        </ul>

    </div>
</div>