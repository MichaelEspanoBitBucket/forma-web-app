
<!-- GI - Start of Left side toolbox-->
			<div class="fl-form-properties-wrapper fl-form-properties-wrapper-left">
			<!--<div class="fl-form-listener" fl-widget-pos='left'></div>
				<div class="fl-widget-head" fl-widget-pos='left'>
					<span class="widget-title"><i class="fa fa-cogs"></i> properties</span>
					<div class="fl-form-properties-toggle">
						<i class="fa fa-chevron-left"></i>
					</div>
				</div>-->
				
				<div class="fl-props-container gi-scrollable">
					<!--<div class="">-->
					<!--PageSetup
						<div class="fl-subtoolbox-wrapper toolbox-form-setting">
							<div class="fl-widget-head">
								<span class="widget-title">Page Settings</span>
								<span class="fl-min-max-subtoolbox" fl-show-opt="true"><i class="fa fa-caret-down"></i></span>
							</div>
							<div class="fl-form-sub-toolbox-content" style="max-height:240px;height:240px">
								<div class="fl-option-form-wrapper">
									<label>Page Size</label>
									<select class="form-select paper_size">
									
									</select>	
								</div>
								<div class="fl-option-form-wrapper">
									<label>Ruler Unit</label>
									<select class="form-select ruler_unit">
									</select>	
								</div>
								<div class="fl-option-form-wrapper">
									<label>Show Grid</label>
									<input type='checkbox' checked class='show_page_grid'></input>
								</div>
								<div class="fl-option-form-wrapper">
									<label>Show Ruler</label>
									<input type='checkbox' checked class='show_ruler'></input>
								</div>
								<div class="fl-option-form-wrapper">
									<label>Show Mouse Crosshair</label>
									<input type='checkbox' class='show_mouse_crosshair'></input>
								</div>
							
							</div>
						</div>
					PageSetup--> 
						<!--<div class="fl-form-actions-wrapper fl-subtoolbox-wrapper toolbox-chart-types">
							<div class="fl-widget-head">
								<span class="widget-title">Charts</span>
								<span class="fl-min-max-subtoolbox" fl-show-opt="true"><i class="fa fa-caret-down"></i></span>
							</div>
							<div class="fl-form-sub-toolbox-content charts-content" style="max-height:230px;height:190px">
								<div class="list_controls">	
									<div class='right_controls'>
										<div class='control_view' view-type='list'>
											<span class='fa fa-list'></span>
										</div>
										<div class='control_view selected-chart-view' view-type='grid'>
											<span class='fa fa-th-large'></span>
										</div>
									</div>
								</div>
								<div class="ui-obj-list chart_list" view-type="grid">

								</div>
							</div>
						</div>
						-->
						<div class="fl-form-objects-wrapper fl-subtoolbox-wrapper">
							<div class="fl-widget-head">
								<span class="widget-title">Objects</span>
								<span class="fl-min-max-subtoolbox" fl-show-opt="true"><i class="fa fa-caret-down"></i></span>
							</div>
							<div class="generate_objects  fl-form-sub-toolbox-content" style="max-height:800px;height:650px">
								<!-- HEREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE-->
								<div class="list_controls">	
									<div class='right_controls'>
										<div class='control_view' view-type='list'>
											<span class='fa fa-list'></span>
										</div>
										<div class='control_view selected-chart-view' view-type='grid'>
											<span class='fa fa-th-large'></span>
										</div>
									</div>
								</div>
								<div class="ui-obj-list report-tool-list" view-type="grid">

								</div>
							</div>
						</div>

						<div class="fl-form-htmlvalue-wrapper fl-subtoolbox-wrapper">
							<div class="fl-widget-head">
								<span class="widget-title">Dashboard</span>
								<span class="fl-min-max-subtoolbox" fl-show-opt="true"><i class="fa fa-caret-down"></i></span>
							</div>
							<div class="tab_objects fl-form-sub-toolbox-content">
								<div class="fl-option-form-wrapper">
									<label>Assign To</label>
									<select class="form-select assign-to-department">
										<option value=''>Select a department</option>>
									</select>	
								</div>
								<a class="fl-command-btn cursor" target="_blank" href="/user_view/gi-dashboard-viewer?id=<?php echo $_GET['id'];?>" style="width: 49%;padding: 3px;"> Preview <i class="fa fa-search"></i></a>
								
								<div class="fl-option-form-wrapper">
									<label>Published</label> &nbsp
									<input type='checkbox' class='dashboard_published'></input>
								</div>
								
								
							</div>
						</div>
					<!--</div>-->
				</div>
			</div>
		<!--End of Left side toolbox-->
			 
		<!--Start of Right side toolbox-->
			<div class="fl-form-properties-wrapper fl-form-properties-wrapper-right">
			<!--<div class="fl-form-listener" fl-widget-pos='right'></div>
				<div class="fl-widget-head" fl-widget-pos='right'>
						<span class="widget-title"><i class="fa fa-cogs"></i> properties</span>
						<div class="fl-form-properties-toggle">
						<i class="fa fa-chevron-right"></i>
					</div>
				</div>
				-->
				<div class="fl-props-container gi-scrollable">
					
				<!--
					<div class="fl-form-setting-wrapper">
						<div class="fl-widget-head">
						    <span class="widget-title">Query Window</span>
						    <span class="fl-min-max-subtoolbox" fl-show-opt="true"><i class="fa fa-caret-down"></i></span>
						</div>
						<div class="fl-form-sub-toolbox-content query-window-content" style="max-height:460px;height:460px">
							
						</div>
					</div>
				-->
					
					<div class="fl-form-objects-wrapper fl-subtoolbox-wrapper">
						<div class="fl-widget-head">
							<span class="widget-title">Properties</span>
							<span class="fl-min-max-subtoolbox" fl-show-opt="true"><i class="fa fa-caret-down"></i></span>
						</div>
						<div class="fl-form-sub-toolbox-content" id="gi-object-properties-wrapper" style="max-height:460px;height:260px">
							<table id="gi-object-properties-table"></table>   
							<datalist id="auto">
							  <option value="auto">
							</datalist>
						</div>
					</div>
				
					<div class="fl-subtoolbox-wrapper toolbox-form-setting">
							<div class="fl-widget-head">
								<span class="widget-title">Page Settings</span>
								<span class="fl-min-max-subtoolbox" fl-show-opt="true"><i class="fa fa-caret-down"></i></span>
							</div>
							<div class="fl-form-sub-toolbox-content" style="max-height:240px;height:240px">
							<!--
								<div class="fl-option-form-wrapper">
								<label>Custom Form Size</label>
								
									<div class="fl-option-form-wrapper">
										<input type="text" name="" class="form-text form_set_size form_size_width" style="width: 49%;" placeholder="Width" value="1344">
										<input type="text" name="" class="form-text form_set_size form_size_height" style="width: 49%;" placeholder="Height" value="816">
										<div class="clearfix"></div>
									</div>
								</div>
							-->
								<div class="fl-option-form-wrapper">
									<label>Page Size</label>
									<select class="form-select paper_size">
									<!--
										<option value="8.5x11">Letter(8.5in x 11in)</option> 
										<option value="8.5x13">PH Legal(8.5in x 13in)</option>
										<option value="8.5x14">US Legal(8.5in x 14in)</option> 
										<option value="7.25x10.5">Executive(7.25in x 10.5in)</option> 
										<option value="33.11x46.61">A0(33.11in x 46.61in)</option> 
										<option value="23.39x33.11">A1(23.39in x 33.11in)</option> 
										<option value="16.53x23.39">A2(16.53in x 23.39in)</option> 
										<option value="11.69x16.53">A3(11.69in x 16.53in)</option> 
										<option value="8.27x11.69">A4(8.27in x 11.69in)</option>
										-->
									</select>	
								</div>
								<div class="fl-option-form-wrapper">
									<label>Ruler Unit</label>
									<select class="form-select ruler_unit">
									</select>	
								</div>
								<div class="fl-option-form-wrapper">
									<label>Show Grid</label>
									<input type='checkbox' checked class='show_page_grid'></input>
								</div>
								<div class="fl-option-form-wrapper">
									<label>Show Ruler</label>
									<input type='checkbox' checked class='show_ruler'></input>
								</div>
								<div class="fl-option-form-wrapper">
									<label>Show Mouse Crosshair</label>
									<input type='checkbox' class='show_mouse_crosshair'></input>
								</div>
								<!--
								<button class="fl-command-btn cursor more_paper_size" style="width: 49%;"><i class="fa fa-file-o"></i> Paper Size</button>
								-->
							<!--
								<div class="fl-option-form-wrapper">
									<label>Show Ruler</label><Br/>
									<label>Yes: <input type="radio" class="f-show-ruler" value="yes" name="show-form-ruler-select"/></label>
									<label>No: <input type="radio" class="f-show-ruler" checked="checked" value="no" name="show-form-ruler-select"/></label>
								</div>
							-->
							</div>
						</div>	
						
					<div class="fl-form-actions-wrapper fl-subtoolbox-wrapper">
						<div class="fl-widget-head">
							<span class="widget-title">Information</span>
							<span class="fl-min-max-subtoolbox" fl-show-opt="true"><i class="fa fa-caret-down"></i></span>
						</div>
						<div class="fl-form-sub-toolbox-content information-content">
							
						</div>
					</div>

				</div>
			</div>
	<!--End of Right side toolbox-->


