<?php if (ENABLE_DETAILS_PANEL == "1") { ?>
    
    <div class="fl-reveal-wrapper isDisplayNone trans_duration3 fl-modules-forms-details">
        <div class="fl-widget-head fl-small-widgetv2">
            <div class="widget-icon-wrapper">
                <span class="widget-title"><i class="fa fa-info"></i> Other details panel</span>
            </div> 
        </div>
        <div class="fl-widget-wrapper-scroll fl-for-scroll">
            
        <?php
        //print_r(json_encode($saved_details_panel,JSON_PRETTY_PRINT));
        $detailpaneltemp = "";
        $tab_temporary_formula = "";

        foreach ($saved_details_panel as $key => $value) {
           
                $detailpaneltemp .= '<div class ="fl-request-widget">';
                      $detailpaneltemp .=  '<div class ="fl-request-widget-header">';
                           $detailpaneltemp .= '<span> Header : '. $value['container_name'] . '</span>' ;
                        $detailpaneltemp .= '</div>';

                            $detailpaneltemp .= '<ul class="fl-request-widget-data">';
                            foreach ($value['title_formula_array'] as $value2) {
                                $tab_temporary_formula = $value2['formula'];
                                $formula_doc = new Formula($tab_temporary_formula);
                                $formula_doc_eval = $formula_doc->evaluate();
                                if($formula_doc_eval == "skipped"){
                                    $formula_doc_eval = "";
                                }
                                $form_id = $value2['link_form'];
                                $link_strs = "/user_view/application?id=".$fs->base_encode_decode("encrypt", $form_id)."&return_fields=";
                                $arr_condition = array();
                                foreach ($value2['link_val'] as $link) {
                                    // echo "Condition: ".$link['field_name']." ".$link['link_operator']." ".$link['link_value']."<br />";
                                    array_push($arr_condition,array("field_name"=>$link['field_name'],"value"=>$link['link_value'],"operator"=>$link['link_operator']));
                                }
                                $json_condition = json_encode($arr_condition);
                                $link_strs.=htmlentities($json_condition);
                                if($value2['link'] == "yes"){
                                    $executed_formula = '<a href="'. $link_strs .'" style="text-decoration: underline;">'. $formula_doc_eval .'</a>';
                                }else{
                                    $executed_formula = $formula_doc_eval;
                                }
                                
                                $detailpaneltemp .= '<li>' . $value2['title'] . " : " . $executed_formula . '</li>';
                             }

                            $detailpaneltemp .=  '</ul>';

                $detailpaneltemp .=  '</div>';
           echo  $detailpaneltemp;
           $detailpaneltemp = "";
           
        }
        
        
        ?>
            </div>
    </div>
<?php } ?>