<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$server = $_SERVER['REQUEST_URI'];
$form_id_encrypted = $_GET['id'];
$fs = new functions();
$module_name = explode("/", $fs->curPageURL("?"))[4];
 //echo $module_name;
if ($module_name == 'application'):
    ?>

    <?php if ($include_calendar == "1") { ?>
        <div class="fl-toolbar-icon-wrap">
            <i id="switchingAppView" class="fa fa-toggle-off tip switchingAppView" data-original-title="Switch to Calendar View" data-placement="top" view="table-view" default-view="<?php echo $default_view; ?>"></i>
        </div> 
    <?php
    }

    $mod = unserialize(ENABLE_COMPANY_MOD);
    $gi_manager = $mod["gs3_insights"]["gi-report-designer"];
    $gi_designer = $mod["gs3_insights"]["gi-report-designer-list"];
    $allow_pin = ($gi_manager == 0 && $gi_designer == 0);
    
    if ($allow_pin == true) {
    ?>
    <div class="fl-toolbar-icon-wrap">
            <i class="fa fa-thumb-tack dashboard_pin tip" fl-data-application-id="<?php echo $application_id; ?>" fl-data-type="request" fl-data-object-id="<?php echo $form_id; ?>" data-placement="top" data-original-title="Pin this to Dashboard"></i>
    </div> 
    <?php
    }
    if (ENABLE_DETAILS_PANEL == "1") { ?>
        <div class="fl-toolbar-icon-wrap">
            <i class="fa fa-info fl-revealWrapperTrigger tip"  data-placement="top" data-original-title="View details "></i>
        </div>
    <?php } ?>   

<?php endif ?>


<?php if ($dashboard_object_doc["object_type"] == 'widget'): ?>

    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-arrow-circle-up fl-toggleUpDown dataTip tip" data-placement="top" data-original-title="Hide This"></i>
    </div>

    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-arrows dataTip handle_dashboard tip" data-placement="top" data-original-title="Drag Here"></i>
    </div>

<?php endif ?>


<?php if ($dashboard_object_doc["object_type"] == 'request_form'): ?>

    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-thumb-tack unpin tip" fl-data-application-id="1" fl-data-type="widget" fl-data-object-id="notif" data-placement="top" data-original-title="Unpin this from Dashboard"></i>
    </div>
    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-arrow-circle-up fl-toggleUpDown dataTip" data-placement="top" data-original-title="Hide This"></i>
    </div>

    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-arrows dataTip handle_dashboard" data-placement="top" data-original-title="Drag Here"></i>
    </div>

<?php endif ?>


<?php if ($dashboard_object_doc["object_type"] == 'request'): ?>

    <?php if ($include_calendar == "1") {
        ?>  
        <div class="fl-toolbar-icon-wrap">
            <i id="switchingAppView" class="fa fa-toggle-off tip switchingAppView" data-original-title="Switch to Calendar View" data-placement="top" view="table-view" default-view="<?php echo $default_view; ?>"></i>
        </div>
    <?php }
    ?>

    <?php if (ENABLE_DETAILS_PANEL == "1") { ?>
        <div class="fl-toolbar-icon-wrap">
            <i class="fa fa-info fl-revealWrapperTrigger tip"  data-placement="top" data-original-title="View details"></i>
        </div> 
    <?php } ?>

    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-thumb-tack unpin tip" fl-data-application-id="1" fl-data-type="widget" fl-data-object-id="notif" data-placement="top" data-original-title="Unpin this from Dashboard"></i>
    </div>
    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-arrow-circle-up fl-toggleUpDown dataTip tip" data-placement="top" data-original-title="Hide This"></i>
    </div> 

    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-arrows dataTip handle_dashboard tip" data-placement="top" data-original-title="Drag Here"></i>
    </div> 

<?php endif ?>

<?php if ($dashboard_object_doc["object_type"] == 'report'): ?>

    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-thumb-tack unpin tip" fl-data-application-id="1" fl-data-type="widget" fl-data-object-id="notif" data-placement="top" data-original-title="Unpin this from Dashboard"></i>
    </div>
    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-arrow-circle-up fl-toggleUpDown dataTip tip" data-placement="top" data-original-title="Hide This"></i>
    </div>

    <div class="fl-toolbar-icon-wrap">
        <i class="fa fa-arrows dataTip handle_dashboard tip" data-placement="top" data-original-title="Drag Here"></i>
    </div>

<?php endif ?>    

