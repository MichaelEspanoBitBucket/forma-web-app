<?php
$display = false;
$icon = "";
$name = "";
$json = array();
$search = new Search();
$fs = new functions;
$db = new Database();
$auth = Auth::getAuth('current_user');
// var_dump($request_details);
$workspace_version = "0";
if (strpos($_SERVER['REQUEST_URI'], "organizational_chart")) {
    if (isset($_GET['id'])) {
        $icon = "fa-sitemap";
        $display = true;
        $obj = array("condition" => " AND id = " . $_GET['id'] . " ");
        $obj = json_decode(json_encode($obj), true);
        $result = $search->getOrgchart("", 0, $obj);
        $name = $result[0]['title'];
        $description = $result[0]['description'];
        $date = $result[0]['date'];
        $created_by = new Person($db, $result[0]['created_by']);

        $json = array("Title" => $name,
            "Description" => $description,
            "Created Date" => $date,
            "Created By" => $created_by->display_name);
    }
} else if (strpos($_SERVER['REQUEST_URI'], "formbuilder")) {
    if ($_GET['formID'] != 0) {
        $icon = "fa-list-alt";
        $display = true;
        $getFormsByAdmin = $search->getFormsByAdmin(" AND tbw.id = " . $_GET['formID']);
        $workspace_version = $getFormsByAdmin[0]['workspace_version'];
        $name = $getFormsByAdmin[0]['form_name'];
        $description = $getFormsByAdmin[0]['form_description'];
        $created_by_name = $getFormsByAdmin[0]['created_by_name'];
        $form_date_created = $getFormsByAdmin[0]['form_date_created'];
        $update_by_name = $getFormsByAdmin[0]['update_by_name'];
        $form_date_updated = $getFormsByAdmin[0]['form_date_updated'];
        $status = $fs->getStatus($getFormsByAdmin[0]['form_active']);
        $falias = $getFormsByAdmin[0]['form_alias'];
        
        $json = array("Title" => $name,
            "Alias" => $falias,
            "Description" => $description,
            "Workspace Status" => $status,
            "Workspace Version" => $fs->getFormVersion($workspace_version),
            "Created By" => $created_by_name,
            "Created Date" => $form_date_created,
            "Updated By" => $update_by_name,
            "Updated Date" => $form_date_updated);
    }
} else if (strpos($_SERVER['REQUEST_URI'], "workflow")) {
    if (isset($_GET['id'])) {
        $icon = "fa-retweet";
        $display = true;
        $obj = array("condition" => " AND wf.id = " . $_GET['id'] . " ");
        $obj = json_decode(json_encode($obj), true);
        $result = $search->getWorkflows("", 0, $obj);
        $name = $result[0]['wf_title'];
        $wf_date = $result[0]['wf_date'];
        $wf_description = $result[0]['wf_description'];
        $workflow_creator = $result[0]['creator'];
        $status = $fs->getStatus($result[0]['Is_Active']);
        $json = array("Title" => $name,
            "Description" => $wf_description,
            "Supported Form" => $result[0]['form_name'],
            "Created By" => $workflow_creator,
            "Created Date" => $wf_date,
            "Status" => $status);
    }
} else if (strpos($_SERVER['REQUEST_URI'], "report")) {
    if (isset($_GET['id'])) {
        $icon = "fa-line-chart";
        $display_header = "display2";
        $display = true;
        $result = functions::getReports("WHERE id = '" . $_GET['id'] . "'");
        $result = json_decode(json_encode($result), true);
        $name = $result[0]['title'];
        $wf_date = $result[0]['date_created'];
        // $creator = $result[0]['created_by']['first_name'] . " " . $result[0]['created_by']['last_name'];
        $creator = $result[0]['created_by']['display_name'];
        $wf_description = $result[0]['description'];
        $form_name = $result[0]['form']['form_name'];
        $date_updated = "";
        if ($result[0]["date_updated"]) {
            $date_updated = $result[0]["date_updated"];
        }

        $json = array("Title" => $name,
            "Description" => $wf_description,
            "Supported Form" => $form_name,
            "Created Date" => $wf_date,
            "Created By" => $creator,
            "Updated Date" => $date_updated,
            // "Updated By" => $result[0]['updated_by']['first_name'] . " " . $result[0]['updated_by']['last_name']);
            "Updated By" => $result[0]['updated_by']['display_name']);
    }
} else if (strpos($_SERVER['REQUEST_URI'], "generate")) {
    $form_id = $_GET['formID'];
    $icon = "fa-print";
    $display = true;

    if ($_GET['view_type'] == "edit") {
        $form = $db->query("SELECT * FROM tb_workspace 
                  WHERE id={$db->escape($form_id)}", "row");
    } else {
        $form = $db->query("SELECT * FROM tb_generate 
                  WHERE id={$db->escape($form_id)}", "row");
    }
    $updated_by = new Person($db, $form['updated_by']);
    $created_by = new Person($db, $form['created_by']);
    $date_created = $form['date_created'];
    $date_updated = $form['date_updated'];

    $name = $form['form_name'];

    if ($date_created != $date_updated) {
        $json = array("Title" => $form['form_name'],
            "Description" => $form['form_description'],
            "Created By" => $created_by->display_name,
            "Created Date" => $date_created,
            "Updated By" => $updated_by->display_name,
            "Updated Date" => $date_updated);
    } else {
        $json = array("Title" => $form['form_name'],
            "Description" => $form['form_description'],
            "Created By" => $created_by->display_name,
            "Created Date" => $date_created);
    }
}

if (count($json) > 0) {
    $json = htmlentities(json_encode($json));
}
?>

<div class="fl-worspace-headerv2">
    <div id="fl-form-basic-options">
        <ul>
            <!-- ORGCHART CONDITION -->
            <?php if (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "organizational_chart"): ?>

                <li class="btn-basicBtn fl-buttonEffect  save_workspace tip" data-original-title="Save Organizational Chart" data-workspace="Organizational Chart" data-form-type="organizational_chart"><a href="#"><i class="fa fa-floppy-o"></i></a></li>
                <li class="btn-basicBtn fl-buttonEffect clear_all_nodes tip" data-original-title="Clear Workspace"><a href="#"><i class="fa fa-times"></i></a></li>
                <li class="btn-basicBtn fl-buttonEffect preview_form tip" data-original-title="Preview Organizational Chart" data-form-type="organizational_chart"><a href="#"><i class="fa fa-search"></i></a></li>
                <!-- END OF ORGCHART CONDITION -->
                <!-- FORM BUILDER CONDITION -->	
            <?php elseif (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "formbuilder"): ?>
                <li  class="btn-basicBtn fl-buttonEffect  save_workspace tip" data-original-title="Save Form" data-workspace="Formbuilder (Save)" data-form-save-type="1" data-form-type="formbuilder"><a href="#"><i class="fa fa-save"></i></a></li>
                <?php
                if ($_GET['formID'] > 0 && $workspace_version == "1") {
                    ?>
                    <li class="btn-basicBtn fl-buttonEffect saveas_workspace tip" data-original-title="Save As Form" data-workspace="Formbuilder (Save As)" data-form-save-type="2" data-form-type="formbuilder"><a href="#"><i class="fa fa-file-text"></i></a></li>
                    <?php
                }
                ?>

                <?php
                $getWorkflow = $db->query("SELECT * FROM tbworkflow WHERE form_id = {$db->escape($_GET['formID'])} AND is_active=1 AND is_delete = 0 ", "array");
                // if(count($getWorkflow)==1){
                if ($_GET['formID'] > 0 && count($getWorkflow) == 1) {
                    $wfPath = "workflow?action=getActiveWorkflow&view_type=edit&form_id=" . $_GET['formID'];
                    ?>
                    <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="Active Workflow" data-placement="top" id="" data-form-id="1" style=""><a href="<?php echo $wfPath; ?>"><i class="fa fa-retweet"></i></a></li>
                    <?php
                } else {
                    $wfPath = "";
                    $formId = $_GET['formID'];
                    if ($formId != 0) {
                        $wfPath = '&form_id=' . $formId;
                    }
                    ?>
                    <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="Create Workflow" data-placement="top" id="" data-form-id="1" style=""><a href="/user_view/workflow?view_type=create<?php echo $wfPath; ?>"><i class="fa fa-retweet"></i></a></li>
                    <?php
                }
                ?>
    <!-- <li class="btn-basicBtn fl-buttonEffect fl-inProgress-workflow tip" data-original-title="In Progress Workflows"><a href="#"><i class="fa fa-spinner"></i></a></li> -->
                <?php
                if ($_GET['formID'] != 0) {
                    ?>
                    <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="History of Workflow"><a href="#" id="workflowHistory"><i class="fa fa-history"></i></a></li>
                    <?php
                }else{
                    $hide_mobility = "display2";
                }
                ?>
                <li class="btn-basicBtn fl-buttonEffect clear_all_formbuilder_workspace tip" data-original-title="Clear Workspace"><a href="#"><i class="fa fa-times"></i></a></li>
                <li class="btn-basicBtn fl-buttonEffect  preview_form tip" data-original-title="Preview Form" data-form-type="formbuilder"><a><i class="fa fa-search"></i> </a></li>
                <li class="btn-basicBtn fl-buttonEffect mobility-setup tip <?php echo $hide_mobility;?>" data-original-title="Mobility" data-form-type=""><a href="#"><i class="fa fa-mobile"></i></a></li>          
                <li class="tip btn-basicBtn undo-me undo-disable" data-original-title="Undo (Ctrl+Z)"><a><i class="fa fa-rotate-left"></i></a></li>
                <li class="tip btn-basicBtn redo-me redo-disable" data-original-title="Redo (Ctrl+Y)"><a><i class="fa fa-rotate-right"></i></a></li>	
                <!-- END OF FORM BUILDER CONDITION -->
                <!-- WORKFLOW CONDITION -->
            <?php elseif (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "workflow"): ?>
                <li class="btn-basicBtn fl-buttonEffect save_workspace tip " data-original-title="Save Workflow" data-workspace="Workflow Chart" data-form-type="workflow"><a href="#"><i class="fa fa-floppy-o"></i></a></li>
                <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="Related Form" data-placement="top" id="" data-form-id="1" style=""><a href="/user_view/formbuilder?formID=<?php echo $_GET['form_id']; ?>&view_type=edit"><i class="fa fa-list-alt"></i></a></li>
                <li class="btn-basicBtn fl-buttonEffect clear_all_nodes tip" data-original-title="Clear Workspace"><a href="#"><i class="fa fa-times"></i></a></li>
                <li  class="btn-basicBtn fl-buttonEffect preview_form tip " data-original-title="Preview Workflow" data-form-type="workflow"><a href="#"><i class="fa fa-search"></i></a></li>

                <!-- END OF WORKFLOW CONDITION -->
                <!-- REPORT CONDITION -->
            <?php elseif (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "report"): ?>
                <li class="btn-basicBtn fl-buttonEffect save_workspace tip" data-original-title="Save Report" data-workspace="Report" data-form-type="report" ><a href="#"><i class="fa fa-save"></i></a></li>

                <!-- END OF REPORT CONDITION -->
                <!-- GENERATE CONDITION -->
            <?php elseif (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "generate"): ?>
                <li class="btn-basicBtn fl-buttonEffect save_workspace tip"  data-original-title="Save" data-workspace="Customized Print" data-form-type="generate"><a><i class="fa fa-save icon-save"></i></a></li>
                <li class="btn-basicBtn fl-buttonEffect clear_all_formbuilder_workspace tip" data-original-title="Clear Workspace"><a href="#"><i class="fa fa-times icon-remove"></i></a></li>
                <li class="btn-basicBtn fl-buttonEffect preview_form tip" data-form-type="generate" data-original-title="Preview Form"><a href="#"><i class="fa fa-search icon-search"></i></a></li>	

                <!-- END OF GENERATE CONDITION -->

                <!-- WORKSPACE CONDITION -->
            <?php elseif (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "workspace"): ?>
                <li class="btn-basicBtn fl-buttonEffect fl-backhistory tip" data-original-title="Back" style="position:relative; z-index:2;"><a><i class="fa fa-reply"></i></a></li>
                <?php
                // echo $request_details[0]['Requestor'] ."==". $auth['id'] ."&&". $_GET['requestID'] ."!=". "0";
                if (ALLOW_PROCESSOR_ATTACHMENT != "1") {
                    if ($request_details[0]['Requestor'] == $auth['id'] && $_GET['requestID'] != "0") {
                        $display = "display";
                    } elseif ($request_details[0]['Requestor'] != $auth['id'] && $_GET['requestID'] == "0") {
                        $display = "";
                    } else {
                        $display = "display";
                    }
                } else {
                    $display = "";
                }
                /* ========== Get Attachment files ========== */
                $path = "images/attachment";
                if (isset($_GET['trackNo'])) {
                    $trackNo = $_GET['trackNo'];
                } else {
                    $trackNo = $request_details[0]['TrackNo'];
                }


                if ($_GET['view_type'] != "update") {

                    if ($_GET['requestID'] != 0) {
                        echo '<li class="isCursorPointer showReply btn-basicBtn fl-buttonEffect tip" data-original-title="Add / View Comment" data-type="1" data-f-id="' . $_GET['formID'] . '" data-id="' . $requestID . '" style=" margin-right: 8px;" active-comment="false"><a><i class="fa fa-comment"></i></a></li> ';
                    }
                    if (ALLOW_GUEST == "1") {
                        if ($_GET['requestID'] != 0) {
                            if ($auth['user_level_id'] != "4") {
                                echo '<li class="isCursorPointer addGuest btn-basicBtn fl-buttonEffect tip" data-original-title="Add Guest" data-action="guest_selected_form" style="position:relative; z-index:2;"><a><i class="fa fa-user"></i></a></li> ';
                            }
                        }
                    }

                    echo '<li class="isCursorPointer viewAuditLogs btn-basicBtn fl-buttonEffect tip" data-original-title="View Logs" style="' . $displayPreviewStyle . ' position:relative;z-index:2;"><a><i class="fa fa-book"></i></a></li> ';
                    echo '<li class="isCursorPointer printForm btn-basicBtn fl-buttonEffect tip" data-original-title="Print" style="' . $displayPreviewStyle . ' position:relative; z-index:2;"><a><i class="fa fa-print"></i></a></li> ';

                    if (!functions::getPinnedRequest($getID, $requestID)) {
                        $mod = unserialize(ENABLE_COMPANY_MOD);
                        $gi_manager = $mod["gs3_insights"]["gi-report-designer"];
                        $gi_designer = $mod["gs3_insights"]["gi-report-designer-list"];
                        $allow_pin = ($gi_manager == 0 && $gi_designer == 0);

                        if ($allow_pin == true) {
                            echo '<li class=" isCursorPointer request_dashboard_pin btn-basicBtn fl-buttonEffect tip" data-original-title="Pin To Dashboard" fl-data-application-id="' . $application_id . '" style="position:relative; z-index:2;"><a><i class="fa fa-thumb-tack"></i></a></li> ';
                        }
                    }
                }

                echo '<li class=" isCursorPointer btn-basicBtn fl-buttonEffect tip" data-original-title="Geo Location" fl-data-application-id="' . $application_id . '" style="position:relative; z-index:2;"><a><i class="fa fa-map-marker"></i></a></li> ';

                if ($_GET['view_type'] != "update") {
                    //echo '<span class="viewer_action"></span>'; //add viewer button
                    //echo '<button class="isCursorPointer uploadButtonRequest"><i class="fa fa-paperclip"></i> Add Files</button>';
                    if($display != "display"){
                        echo '<li class="' . $display . ' uploadButtonRequest isCursorPointer btn-basicBtn fl-buttonEffect tip" data-original-title="Add Files" style="position: relative;">';
                        
                        echo '<a><i class="fa fa-paperclip"></i></a>';
                        //echo '<span> Add Files</span>';
                        echo '<form id="fille_attach" method="post" enctype="multipart/form-data" action="/ajax/request_attachment" style="position: absolute; width: 100%; left: 0px; height: 100%; top: 0px; overflow: hidden;">';
                        echo '<input type="file" data-action-id="2" value="upload" name="file" id="file" size="24" data-action-type="attachFile" style="width: 30px; left:0px; opacity: 0;position:absolute; height:30px; top:-2px; font-size:11px;" class="cursor upfile uploadFileWorkspace">';
                        echo '<input type="text" name="getFormID" id="getFormID" value="' . $_GET['formID'] . '" data-type="longtext" class="display getFields" />';
                        echo '<input type="text" name="getID" id="getID" value="' . $_GET['requestID'] . '" data-type="longtext" class="display getFields" />';
                        echo '<input type="text" name="getTrackNo" id="getTrackNo" value="' . $trackNo . '" data-type="longtext" class="display getFields" />';
                        echo '</form></li> ';
                        echo '<img src="/images/loader/load.gif" class="display attachFiles"/>';
                    }
                } else {
                    echo '<li class="isCursorPointer printForm btn-basicBtn fl-buttonEffect tip" data-original-title="Print" style="position:relative; z-index:2;"><a><i class="fa fa-print"></i></a></li> ';
                }

                if (ALLOW_DEFAULT_ACTIONS == '1') {
                    echo functions::getDefaultActionButtons($form_json['form_json'], $requestID, $getID, "2", "Notes");
                }
                ?>


                <!-- END OF WORKSPACE CONDITION -->

            <?php endif; ?>
            <li class="btn-basicBtn fl-buttonEffect tip fl-workspace-fullscreen" isFullScreen="exitFullScreen" data-original-title="Go Fullscreen" style="position:relative; z-index:2;">
                <a href="#"><i class=" fa fa-expand"></i></a>
            </li>
            <?php if (functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "formbuilder" || functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "workflow" || functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "organizational_chart" || functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "report"): ?>
                <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="Home">
                    <a href="/portal"><i class="fa fa-home"></i></a>
                </li>
            <?php endif; ?>
            <!-- if statement for list icon -->
            <?php if (strpos($_SERVER['REQUEST_URI'], "organizational_chart")): ?>
                <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="List of Organizational Charts"><a href="/user_view/organizational_chart_list"><i class="fa fa-sitemap"></i></a></li>
            <?php elseif (strpos($_SERVER['REQUEST_URI'], "formbuilder")): ?>
                <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="List of Forms"><a href="/user_view/form_list"><i class="fa fa-list-alt"></i></a></li>
            <?php elseif (strpos($_SERVER['REQUEST_URI'], "workflow")): ?>
                <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="List of Workflows"><a href="/user_view/workflow_list"><i class="fa fa-retweet"></i></a></li>
                <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="History"><a href="#" id="workflowHistory"><i class="fa fa-history"></i></a></li>
            <?php elseif (strpos($_SERVER['REQUEST_URI'], "report")): ?>
                <li class="tip btn-basicBtn fl-buttonEffect" data-original-title="List of Reports"><a href="/user_view/report_list"><i class="fa fa-line-chart"></i></a></li>
                    <?php endif; ?>
            <!-- if statement for list icon -->


        </ul>		
    </div>
</div>

<?php

if($falias != ""){
$show_alias_name = $falias;
$show_title = 'Form Title: '. $name;
}else{
$show_alias_name = $name;
$show_title = 'Click for more Details';
}
if ($display === true) {
    echo '<div class="fl-form-name-wrapper"><a href="#" class="fl-type-creation getDetailFn Dipstick" title="'. $show_alias_name .'" json="' . $json . '"><i class="fa ' . $icon . '" style="color:#ffffff !important; font-size:22px; position:relative; top:2px; left:4px;"></i>&nbsp;&nbsp; ' . $show_alias_name . '</a></div>';
}
?>


