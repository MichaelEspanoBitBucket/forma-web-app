 <div class="fl-widget-head workspace_option_bar" >
        <?php if ($_GET['view_type'] != "preview") { ?>
            <?php if (!isset($_GET['embed_type']) && $_GET['embed_type'] != "viewEmbedOnly") { ?>
                <?php
                if ($_GET['view_type'] != "update") {
                    $optionButtons = "horizontal-sub-menu";
                    if (ALLOW_DEFAULT_ACTIONS == "0") {
                        $optionButtons = "fl-action-option";
                    }
                    ?>
                    <?php if( (gettype($form_json['form_json']['form_action_panel']) == "array"?$form_json['form_json']['form_action_panel']['panel_design_type']:"0") == "0"){ ?>
                     <!-- if condition for navigation ui -->
                    <ul class="horizontal-menu fl-action-menu dropdown-menu  clearfix">
                        <?php
                        if (ALLOW_DEFAULT_ACTIONS == "1") {
                            if (strlen(functions::getDefaultActionButtons($form_json['form_json'], $requestID, $getID, "2", "Notes"))) {
                                ?>
                                <li><a href="#"><i class="fa fa-file-text-o"></i> File</a>
                                    <ul id="<?php echo $optionButtons; ?>" class="horizontal-sub-menu">
                                        <?php
                                        $button_list = '';
                                        if (ALLOW_DEFAULT_ACTIONS == "1") {
                                            if (count($request_view_datas["action_buttons"]) > 0) {
                                                $button_list .= "<li><ul id='fl-action-option' class='horizontal-sub-menu'>";
                                                $button_list .= $request_view_datas["action_buttons"];
                                                $button_list .= "</ul></li>";
                                            }
                                            echo $button_list;
                                        } else {
                                            echo $request_view_datas["action_buttons"];
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                }
                            }
                            ?>
                        </li>
                        <li><a href="#"><i class="fa fa-hand-o-up"></i> Actions</a>
                            <ul id="<?php echo $optionButtons; ?>" class="horizontal-sub-menu workflow_buttons">
                                <?php
                                echo $request_view_datas["action_buttons"];
                                ?>
                            </ul>
                        </li>
                        <?php
                        if (ALLOW_EXTERNAL_LINKS == "1") {
                            if ($requestID != 0) {
                                if (count($form_json['form_json']['external_link']) > 0) {
                                    ?>
                                    <li><a href="#"><i class="fa fa-file-text-o"></i> Related Information</a>
                                        <?php
                                        if (ALLOW_EXTERNAL_LINKS == "1") {
                                            if ($requestID != 0) {
                                                ?>
                                                <ul id="" class="horizontal-sub-menu">
                                                    <?php
                                                    foreach ($getModules as $key_category => $value_category) {
                                                        ?>
                                                        <?= "<li class='ext-btn' title='" . $key_category . "'><a>&nbsp;" . $key_category . "&nbsp;</a> <strong class='fl-nav-down-icon'><i class='fa fa-chevron-right'></i></strong>"; ?>
                                                        <?php
                                                        ?>
                                                        <?= "<ul class='horizontal-sub-menu'>"; ?>
                                                        <?php
                                                        foreach ($value_category as $key => $value) {
                                                            $get_form_id = functions::base_encode_decode("encrypt", $value['form_id']);
                                                            if($value['form_alias'] != ""){
                                                                $my_request_title = $value['form_alias'];
                                                            }else{
                                                                $my_request_title = $value['form_name'];
                                                            }
                                                            ?>
                                                            <?= "<li class='ext-links hellyeah' title='" . $my_request_title . "' form-id='" . $value['form_id'] . "'><a href='/user_view/application?id=" . $get_form_id . "' target='_blank'> " . $my_request_title . "</a></li>"; ?>
                                                            <?php
                                                        }
                                                        ?>
                                                        <?= "</ul>"; ?>
                                                        <?php
                                                        ?>
                                                        <?= "</li>"; ?>
                                                    <?php } ?>
                                                </ul> 
                                            <?php } ?>
                                        <?php } ?>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                    <?php } else { ?>
                    <div id="parent-ul-menu-request-view" class="clearfix">
                        <ul class="horizontal-menu fl-action-menu extended-menu clearfix" >
                            <?php
                            if (ALLOW_DEFAULT_ACTIONS == "1") {
                                if (strlen(functions::getDefaultActionButtons($form_json['form_json'], $requestID, $getID, "2", "Notes"))) {
                                    ?>
                                    <li><a href="#"><i class="fa fa-file-text-o"></i> File</a>
                                        <?php
                                            $button_list = '';
                                            if (ALLOW_DEFAULT_ACTIONS == "1") {
                                                if (count($request_view_datas["action_buttons"]) > 0) {
                                                    $button_list .= "<li><ul id='fl-action-option' class='horizontal-sub-menu'>";
                                                    $button_list .= $request_view_datas["action_buttons"];
                                                    $button_list .= "</ul></li>";
                                                }
                                                echo $button_list;
                                            } else {
                                                echo $request_view_datas["action_buttons"];
                                            }
                                            ?>
                                        <?php
                                    }
                                } 
                                ?>
                            </li>    

                            <li>
                                <ul id="<?php echo $optionButtons; ?>" class="horizontal-sub-menu workflow_buttons">
                                    <?php
                                    echo $request_view_datas["action_buttons"];
                                    ?>
                                </ul>
                            </li>
                            <li><?php echo $request_view_datas["action_buttons"]; ?></li>
                            <?php
                            if (ALLOW_EXTERNAL_LINKS == "1") {
                                if ($requestID != 0) {
                                    if (count($form_json['form_json']['external_link']) > 0) {
                                        ?>
                                        <li>
                                            <?php
                                            if (ALLOW_EXTERNAL_LINKS == "1") {
                                                if ($requestID != 0) {
                                                    ?>
                                                   
                                                        <?php
                                                        foreach ($getModules as $key_category => $value_category) {
                                                            ?>
                                                            <?= "<li class='ext-btn' title='" . $key_category . "'><a>&nbsp;" . $key_category . "&nbsp;</a> <strong class='fl-nav-down-icon'><i class='fa fa-chevron-right'></i></strong>"; ?>
                                                            <?php
                                                            ?>
                                                           
                                                            <?php
                                                            foreach ($value_category as $key => $value) {
                                                                $get_form_id = functions::base_encode_decode("encrypt", $value['form_id']);
                                                                if($value['form_alias'] != ""){
                                                                    $my_request_title = $value['form_alias'];
                                                                }else{
                                                                    $my_request_title = $value['form_name'];
                                                                }
                                                                ?>
                                                                <?= "<li class='ext-links hellyeah' title='" . $my_request_title . "' form-id='" . $value['form_id'] . "'><a href='/user_view/application?id=" . $get_form_id . "' target='_blank'> " . $my_request_title . "</a></li>"; ?>
                                                                <?php
                                                            }
                                                            ?>
                                                           
                                                            <?php
                                                            ?>
                                                            <?= "</li>"; ?>
                                                        <?php } ?>
                                                 
                                                <?php } ?>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                               
                        </ul>
                    </div>
                    <?php } ?>
                <?php } ?>      
            <?php } ?>
        <?php } ?>
        <div class="fl-toolbar-right-wrapper">
            <?php if ($_GET['requestID'] != "0") { ?>
                <div class="fl-form-workspace-details">
                    <span> Tracking Number: <span class="droidsansbold" id="trackno_display"></span></span>
                    <span> Date Created: <span class="droidsansbold" id="datecreated_display"></span></span>
                    <span> Requestor: <span class="droidsansbold" id="requestor_display"></span></span>
                    <span id="lblprocessor_display"> Processor: <span class="droidsansbold" id="processor_display"></span></span>
                    <span class="delegate_display"> <label id="lbldelegate">Delegate:</label> 
                        <span class="tip droidsansbold" data-html="true" data-original-title="" data-placement="bottom"
                              id="delegate_display" style="padding: 4px;"></span></span>
                    <span> Status: <span class="droidsansbold" id="status_display"></span></span>
                </div>
            <?php } ?>
        </div>
    </div>