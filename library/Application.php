<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Application
 *
 * @author Jewel Tolentino
 */
class Application extends Formalistics {

    //put your code here

    public $category_name;
    public $company;
    public $is_active;

    public function __construct($db, $id) {
        $this->tblname = 'tbform_category';
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();

        if ($id) {
            $result = $redis_client->query("form_category_object_" . $id, "SELECT * FROM tbform_category WHERE id = {$this->db->escape($id)} AND is_delete = 0");

            $this->id = $result['id'];
            $this->category_name = $result['category_name'];
            $this->company = new Company($this->db, $result['company_id']);
            $this->is_active = $result['is_active'];
        }
    }

}
