<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Audit Logs
 *
 * @author Samuel Pulta
 */
class Audit_Logs extends Formalistics {

    //put your code here

    public $user_id;
    public $audit_action;
    public $table_name;
    public $record_id;
    public $ip;
    public $date;
    public $is_active;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $this->tblname = "tbaudit_logs";
        
        if ($id) {
            $result = $this->db->query("SELECT * FROM " . $this->tblname . " WHERE user_id = {$this->db->escape($id)}
                                       AND is_active={$this->db->escape(1)}", "row");
            
            $this->id = $result['id'];
            $this->user_id = $result['user_id'];
            $this->audit_action = $result['audit_action'];
            $this->table_name = $result['table_name'];
            $this->record_id = $result['record_id'];
            $this->ip = $result['ip'];
            
            $this->date = $result['date'];
            $this->is_active = $result['is_active'];
            
        }
    }
    
    public function query_array(){
        $result = $this->db->query("SELECT * FROM " . $this->tblname . " WHERE id = {$this->db->escape($this->id)} AND is_active={$this->db->escape(1)}", "array");
        return  $result;
    }
    
    public function query_numrows(){
        $result = $this->db->query("SELECT * FROM " . $this->tblname, "numrows");
        return $result;
    }

    public function save() {
        $insert_array = array(
            "user_id"               =>    $this->user_id,
            "audit_action"          =>    $this->audit_action,
            "table_name"            =>    $this->table_name,
            "record_id"             =>    $this->record_id,
            "ip"                    =>    $this->ip,
            "date"                  =>    $this->date,
            "is_active"             =>    $this->is_active,
            "action_description"    =>    $this->action_description
        );
        // var_dump(insert_array)
        $insert_id = $this->db->insert($this->tblname, $insert_array);
    
        $this->id = $insert_id;
    }
    //
    public function update() {
        $update_array = array(
            "user_id"               =>    $this->user_id,
            "audit_action"          =>    $this->audit_action,
            "table_name"            =>    $this->table_name,
            "record_id"             =>    $this->record_id,
            "ip"                    =>    $this->ip,
            "date"                  =>    $this->date,
            "is_active"             =>    $this->is_active
        );
    
        $condition_array = array("id" => $this->id);
    
        $this->db->update($this->tblname, $update_array, $condition_array);
    }

}

?>
