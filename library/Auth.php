<?php
class Auth extends Database
{
    public function login($username,$password,$table_username,$table_password,$table)
    {
        $user_password = functions::encrypt_decrypt("encrypt",$password);
        //$db = new Database();
        $login = $this->query("SELECT *
                                FROM {$table}
                                WHERE {$table_username}={$this->escape($username)} AND
                                {$table_password}={$this->escape($user_password)} AND is_active = 1
								OR
								username={$this->escape($username)} AND
                                {$table_password}={$this->escape($user_password)} AND is_active = 1","row");
        if(ALLOW_ACTIVE_DIRECTORY == "1"){
            $ldapMan = LDAPManager::getInstance();
            $ldapMan->setHost(AD_IP_ADDRESS);
            $results = $ldapMan->login($username, $password);
        }
        
        if($login){
            $this->setAuth('current_user',$login);
            setcookie('USERAUTH',serialize($login),0,'/',COOKIE_URL);
            setcookie('application', "false",0,'/','' . COOKIE_URL);
            return 'true';
        }
        if(ALLOW_ACTIVE_DIRECTORY == "1"){
            //  added by ervinne
            if ($results['error'] == null) {
                return 'need register';
                //  register user here
            } else{
                return 'error';
            }
        }
        return false;
    }

    public function auto_login($username,$password,$table_username,$table_password,$table,$others)
    {
        $user_password = $password;
        //$db = new Database();
        $login = $this->query("SELECT *
                                FROM {$table}
                                WHERE {$table_username}={$this->escape($username)} AND
                                {$table_password}={$this->escape($user_password)} AND is_active = 1","row");
        
        if($login){
            $this->setAuth('current_user',$login);
            setcookie('USERAUTH',serialize($login),0,'/',COOKIE_URL);
            setcookie('application', "false",0,'/','' . COOKIE_URL);
            $this->setAuth('Guest_AUTO_LOGGED',1);
            $this->setAuth('Guest_URL',$others['url']);
            return 'true';
        }
       
        return false;
    }
    
    public function hasAuth($name)
    {
        if(!isset($_SESSION))
        {
            @session_start();
        }
        if(isset($_SESSION[$name]))
        {
            return true;
        }
        return false;
    }
    
    public function setAuth($name,$value)
    {
        if(!isset($_SESSION))
        {
            @session_start();
        }
        $_SESSION[$name] = $value;
        return $this;
    }
    
    public function getAuth($name)
    {
        if(!isset($_SESSION))
        {
            @session_start();
        }
        if(isset($_SESSION[$name]))
        {
            return $_SESSION[$name];
        }
        return false;
    }
    
    public function destroyAuth($name)
    {
        if(!isset($_SESSION))
        {
            @session_start();
        }
        if(isset($_SESSION[$name]))
        {
            unset($_SESSION[$name]);
            return true;
        }
        return false;
    }
    

}



