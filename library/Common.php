<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Common
 *
 * @author Jewel Tolentino
 */
class Common {

    //put your code here

    public static final function getDatabaseConnection() {
        try {
            $dbh = new PDO("mysql:host=" . DB_HOSTNAME . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);
        } catch (PDOException $ex) {
            die($ex->getMessage());
        }

        return $dbh;
    }

    public static final function getCurrentDateTime($format = "Y-m-d H:i:s") {
        date_default_timezone_set("Asia/Manila");

        return date($format);
    }

}
