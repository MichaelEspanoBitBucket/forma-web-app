<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Company
 *
 * @author Jewel Tolentino
 */
class Company extends Formalistics {

    //put your code here

    public $code;
    public $name;
    public $contact_number;
    public $primary_contact;
    public $email;
    public $extension;
    public $date_registered;
    public $limit_users;
    public $num_available;
    public $is_active;
    public $l_code;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();
        if ($id) {
            $result = $redis_client->query("company_object_" . $id, "SELECT * FROM tbcompany WHERE id = {$this->db->escape($id)}");

            $this->id = $result['id'];
            $this->code = $result['code'];
            $this->name = $result['name'];
            $this->contact_number = $result['contact_number'];
            $this->primary_contact = $result['primary_contact'];
            $this->email = $result['email'];
            $this->extension = $result['extension'];
            $this->date_registered = $result['date_registered'];
            $this->limit_users = $result['limit_users'];
            $this->num_available = $result['num_available'];
            $this->is_active = $result['is_active'];
            $this->l_code = $result['l_code'];
        }
    }

}

?>
