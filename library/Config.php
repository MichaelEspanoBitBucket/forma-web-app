<?php

class Config extends Functions{
    #code
    public $page_author;
    public $page_title;
    public $page_keyword;
    public $page_description;
    
    public function __construct($value){
        $this->page_author = $value;
        $this->page_title = $value;
    }
    
    public function page_author(){
        return $this->page_author = "Samuel G. Pulta";
    }
    
    public function page_title(){
        return $this->page_title = "Formalistics";
    }
    
    public function page_keyword(){
        return $this->page_keyword = "Formalistics, Formbuilder, Forms, Workflow, Organizationl Chart";
    }
    
    public function page_description(){
        return $this->page_description = "FORMALISTICS is a software platform that makes possible creation of software systems based on electronic forms and process / approval flows.";
    }
    
    public function get_page(){
        $page_name = Layout::getParams();
        $count_param = count($page_name);
        if($count_param=="0"){
            return self::page_title();
        }elseif($page_name[1] == "home" || $page_name[0] == ""){
            return self::page_title();
        }elseif($page_name[1] == "application"){
            $db = new Database();
            $fs = new functions;
            $form_id = $fs->base_encode_decode("decrypt", $_GET['id']);
            $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($form_id)} AND is_delete = 0", "row");
            return ucfirst($forms['form_name']);
        }elseif($page_name[1] == "workspace"){
            /*Modified by Aaron Tolentino 10-26-2015*/
            $db = new Database();
            $form_id = $_GET['formID'];
            $form = new Form($db,$form_id);
            // $request_view_datas = createRequestView($_GET);
            return ucfirst($form->form_name);//$request_view_datas["form_name"];
        }elseif($page_name[1] == "generate"){
            return "Print Form";
        }else{
            $name = $page_name[$count_param-1];
            $str_replace_page_name = str_replace('_',' ',$name);
            $str_name = str_replace('-',' ',$str_replace_page_name);
            $web_page_name = ucwords($str_name);
            return $web_page_name;
        }
    }
    public function get_page_alias(){
        $page_name_alias = Layout::getParams();
        $count_param_alias = count($page_name_alias);
        if($count_param_alias=="0"){
            return self::page_title();
        }elseif($page_name_alias[1] == "home" || $page_name_alias[0] == ""){
            return self::page_title();
        }elseif($page_name_alias[1] == "application"){
            $db = new Database();
            $fs = new functions;
            $form_id = $fs->base_encode_decode("decrypt", $_GET['id']);
            $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($form_id)} AND is_delete = 0", "row");
            if($forms['form_alias'] == ""){
                return ucfirst($forms['form_name']);
            }else{
                return ucfirst($forms['form_alias']);
            }

            
        }elseif($page_name_alias[1] == "workspace"){
            /*Modified by Aaron Tolentino 10-26-2015*/
            $db = new Database();
            $form_id = $_GET['formID'];
            $form_track = $_GET['trackNo'];
            $form = new Form($db,$form_id);
            $test = new Formula($form->form_name_formula);
            $fname = $form->form_name;
            //$forms = $db->query("SELECT * FROM tbworkflow WHERE form_id = '$form_id'", "row");
            //$trackno = $db->query("SELECT * FROM tbworkflow WHERE form_id = '$form'");
            //$filter = $forms['id'];
            // $request_view_datas = createRequestView($_GET);
            // echo "<div id='asdasd'>" . $form->form_alias . "</div>";
            if($form->form_alias == ""){

                        if($form->enable_formula == "1" && $form->formula_is_field == "0"){
                            return ucfirst($test->evaluate());
                        }else if($form->enable_formula == "1" && $form->formula_is_field == "1"){
                            //var_dump($filter);
                            //echo $form_track;
                            //print_r($filter);
                            $test->setSourceForm($fname, "*", array(
                                            array("FieldName" => "TrackNo",
                                            "Operator" => "=",
                                            "Value" =>$form_track)
                                            
                                        )
                                    );
                            return ucfirst($test->evaluate());

                        }else{
                            return ucfirst($form->form_name);
                        }
                
            }else if($form->enable_formula == "1" && $form->formula_is_field == "0" && $form->form_alias != ""){
                    
                    return ucfirst($test->evaluate());

            }else if($form->enable_formula == "1" && $form->formula_is_field == "1" && $form->form_alias != ""){
                    //var_dump($filter);
                    //echo $form_track;
                    //print_r($filter);
                    $test->setSourceForm($fname, "*", array(
                                    array("FieldName" => "TrackNo",
                                    "Operator" => "=",
                                    "Value" =>$form_track)
                                    
                                )
                            );
                    return ucfirst($test->evaluate());

            }else{
                return ucfirst($form->form_alias); 
            }
        }elseif($page_name_alias[1] == "generate"){
            return "Print Form";
        }else{
            $name_alias = $page_name_alias[$count_param_alias-1];
            $str_replace_page_name_alias = str_replace('_',' ',$name_alias);
            $str_name_alias = str_replace('-',' ',$str_replace_page_name_alias);
            $web_page_name_alias = ucwords($str_name_alias);
            return $web_page_name_alias;

        }
    }
}




?>