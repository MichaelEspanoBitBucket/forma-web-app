<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard
 *
 * @author Jewel Tolentino
 */
class Dashboard extends Formalistics {

    //put your code here

    public $user;
    public $application;
    public $content;
    public $date_created;
    public $is_active;
    public $date_updated;
    public $creator;
    public $modifier;

    public function __construct($db, $id) {
        $this->tblname = 'tbdashboard';
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();
        if ($id) {
            $result = $redis_client->query("dashboard_object_" . $id, "SELECT * FROM tbdashboard WHERE id = {$this->db->escape($id)}");

            $this->id = $result['ID'];
            $this->user = new Person($this->db, $result['UserID']);
            $this->application = new Application($this->db, $result['ApplicationID']);
            $this->content = $result['Content'];
            $this->date_created = $result['DateCreated'];
            $this->date_updated = $result['DateUpdated'];
            $this->creator = new Person($this->db, $result['CreatedBy']);
            $this->modifier = new Person($this->db, $result['UpdatedBy']);
        }
    }

    public function save() {
        $insert_array = array(
            "UserID" => $this->user->id,
            "ApplicationID" => $this->application->id,
            "Content" => $this->content,
            "DateCreated" => $this->date_created,
            "DateUpdated" => $this->date_updated,
            "CreatedBy" => $this->creator->id,
            "UpdatedBy" => $this->modifier->id
        );

//        print_r($insert_array);
        $insert_id = $this->db->insert($this->tblname, $insert_array);

        $this->id = $insert_id;
    }

    public function update() {
        $update_array = array(
            "UserID" => $this->user->id,
            "ApplicationID" => $this->application->id,
            "Content" => $this->content,
            "DateCreated" => $this->date_created,
            "DateUpdated" => $this->date_updated,
            "CreatedBy" => $this->creator->id,
            "UpdatedBy" => $this->modifier->id
        );

        $condition_array = array("id" => $this->id);
        $this->db->update($this->tblname, $update_array, $condition_array);
    }

}
