<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard_Object
 *
 * @author Jewel Tolentino
 */
class Dashboard_Object extends Formalistics {

    //put your code here

    public $dashboard;
    public $type;
    public $object_id;
    public $date_created;
    public $date_updated;
    public $creator;
    public $modifier;
    public $is_active;

    public function __construct($db, $id) {
        $this->tblname = 'tbdashboard_objects';
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();
        if ($id) {
            $result = $redis_client->query("dashboard_objects_object_" . $id, "SELECT * FROM tbdashboard_objects WHERE id = {$this->db->escape($id)}");

            $this->id = $result['ID'];
            $this->dashboard = new Dashboard($this->db, $result['DashboardID']);
            $this->type = $result['Type'];
            $this->object_id = $result['ObjectID'];
            $this->date_created = $result['DateCreated'];
            $this->date_updated = $result['DateUpdated'];
            $this->creator = new Person($this->db, $result['CreatedBy']);
            $this->modifier = new Person($this->db, $result['UpdatedBy']);
            $this->is_active = $result['Is_Active'];
        }
    }

    public function save() {
        $insert_array = array(
            "DashboardID" => $this->dashboard->id,
            "Type" => $this->type,
            "ObjectID" => $this->object_id,
            "DateCreated" => $this->date_created,
            "DateUpdated" => $this->date_updated,
            "CreatedBy" => $this->creator->id,
            "UpdatedBy" => $this->modifier->id,
            "Is_Active" => $this->is_active
        );

        $insert_id = $this->db->insert($this->tblname, $insert_array);

        $this->id = $insert_id;
    }

    public function update() {
        $update_array = array(
            "DashboardID" => $this->dashboard->id,
            "Type" => $this->type,
            "ObjectID" => $this->object_id,
            "DateCreated" => $this->date_created,
            "DateUpdated" => $this->date_updated,
            "CreatedBy" => $this->creator->id,
            "UpdatedBy" => $this->modifier->id,
            "Is_Active" => $this->is_active
        );

        $condition_array = array("id" => $this->id);
        $this->db->update($this->tblname, $update_array, $condition_array);
    }

}
