<?php

class Database extends functions {

    protected $allowed_mysql_functions = array(
        "NOW()", "CURDATE()", "CURTIME()"
    );
    protected $host;
    protected $username;
    protected $password;
    protected $db;
    protected $last_error_message;

    public function __construct($host = null, $username = null, $password = null, $db = null) {
        if (isset($host)) {
            $this->host = $host;
        } else {
            $this->host = DB_HOSTNAME;
        }
        if (isset($username)) {
            $this->username = $username;
        } else {
            $this->username = DB_USERNAME;
        }
        if (isset($password)) {
            $this->password = $password;
        } else {
            $this->password = DB_PASSWORD;
        }
        if (isset($db)) {
            $this->db = $db;
        } else {
            $this->db = DB_NAME;
        }
    }

    private function connect() {
        $con = mysql_connect($this->host, $this->username, $this->password) or die("Cannot connect to Host: " . mysql_error());
        mysql_select_db($this->db) or die("Cannot connect to DB Name");
        return $con;
    }

    public function escape($str) {
        return "'" . addslashes($str) . "'";
    }

    public function addslash_escape($str) {
        $str = str_replace("_", "\\_", $str);
        $str = str_replace("%", "\\%", $str);

        return $str;
    }

    public function create($sql) {
        $con = $this->connect();
        $query = mysql_query($sql);
        mysql_close($con);
        return $query;
    }

    public function query($sql, $fetch_type = "array") {
        $con = $this->connect();
        mysql_query('SET NAMES utf8');
        $query = mysql_query($sql);

        $this->last_error_message = null;
        if (mysql_errno()) { //	if there was an error		
            $this->last_error_message = "MySQL error " . mysql_errno() . ": " . mysql_error() . "\n<br>When executing:<br>\n$sql\n<br>";
            //echo $this->last_error_message;
        }

        $data = array();
        if ($fetch_type === "array") {
            while ($row = mysql_fetch_assoc($query)) {
                $data[] = $row;
            }
        } else if ($fetch_type === "row") {
            $data = mysql_fetch_assoc($query);
        } else if ($fetch_type === "update") {
            $data = mysql_affected_rows($con);
        } else if ($fetch_type === "insert") {
            $data = mysql_insert_id($con);
        } else if ($fetch_type === "numrows") {
            $data = mysql_num_rows($query);
        } else {
            return false;
        }

        mysql_close($con);
        return $data;
    }

    public function update($table, $fields = array(), $conditions = array()) {
        $field_string = "";
        $cond_string = "";
        //foreach ($fields as $field => $value) {
        //    $value = is_numeric($value) ? $value : $this->escape($value);
        //    $field_string .= $field . "=" . $value . ",";
        //}
        //foreach ($conditions as $condition => $value) {
        //    $value = is_numeric($value) ? $value : $this->escape($value);
        //    $cond_string .= $condition . "=" . $value . " AND ";
        //}
        foreach ($fields as $field => $value) {
            $value = $this->escape($value); //FS#6076
            $field_string .= "`" . $field . "` = " . $value . ",";
        }
        foreach ($conditions as $condition => $value) {
            $value = $this->escape($value);
            $cond_string .= "`" . $condition . "` = " . $value . " AND ";
        }
        $field_string = substr($field_string, 0, strlen($field_string) - 1);
        $cond_string = substr($cond_string, 0, strlen($cond_string) - 4);
        // echo "UPDATE {$table} SET {$field_string} WHERE {$cond_string}</br>";
        createQueryLog("my_sql : UPDATE {$table} SET {$field_string} WHERE {$cond_string}");
        $query = $this->query("UPDATE {$table} SET {$field_string} WHERE {$cond_string}", "update");
        return $query;
    }

    public function insert($table, $fields = array()) {
        $field_string = "";
        foreach ($fields as $field => $value) {
            // if (!in_array($value, $this->allowed_mysql_functions) && !is_numeric($value)) {
            //     $value = $this->escape($value);
            // }
            if (!in_array($value, $this->allowed_mysql_functions)) {
                $value = $this->escape($value); //FS#6076
            }

            $field_string .= "`" . $field . "`" . "=" . $value . ",";
        }
        $field_string = substr($field_string, 0, strlen($field_string) - 1);

        // echo "INSERT INTO {$table} SET {$field_string}</br>";
        createQueryLog("my_sql : INSERT INTO {$table} SET {$field_string}");
        $query = $this->query("INSERT INTO {$table} SET {$field_string}", "insert");
//        echo "INSERT INTO {$table} SET {$field_string}";
        return $query;
    }

    public function multipleInsert($table_name, $data = array()) {

        //  get the keys of the first entry of the multiple insert  
        $columns = array();
        foreach ($data[0] AS $key => $value) {
            array_push($columns, "`" . $key . "`");
        }

        $column_string = implode(",", $columns);
        $values_string = array();

        foreach ($data as $insert_item) {

            $values = array_values($insert_item);
            $quoted_values = array();

            for ($i = 0; $i < count($values); $i ++) {
                if (in_array($values[$i], $this->allowed_mysql_functions) || is_numeric($values[$i])) {
                    array_push($quoted_values, $values[$i]);
                } else {
                    //  prevent sql injection by quoting each values
                    array_push($quoted_values, $this->escape($values[$i]));
                }
            }

            $insert_item_values_string = implode(",", array_values($quoted_values));
            array_push($values_string, "({$insert_item_values_string})");
        }

        $values_clause = implode(",", $values_string);

        $query = "INSERT INTO {$table_name} ({$column_string}) VALUES {$values_clause}";

//        echo $query;
        createQueryLog("my_sql : " . $query);
        return $this->query($query, "insert");
    }

    public function delete($table, $conditions = array()) {
        $cond_string = "";
        foreach ($conditions as $condition => $value) {
            $value = is_numeric($value) ? $value : $this->escape($value);
            $cond_string .= $condition . "=" . $value . " AND ";
        }
        $cond_string = substr($cond_string, 0, strlen($cond_string) - 4);
        createQueryLog("my_sql : DELETE FROM {$table} WHERE {$cond_string}");
        $query = $this->query("DELETE FROM {$table} WHERE {$cond_string}", "update");
        return $query;
    }

    public function getLastErrorMessage() {
        return $this->last_error_message;
    }

}
