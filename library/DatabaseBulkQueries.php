<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DatabaseBulkQueries
 *
 * @author Joshua Reyes
 */
class DatabaseBulkQueries {
	//put your code here

	/**
	 * Executes multiple queries in a 'bulk' to achieve better
	 * performance and integrity.
	 *
	 * @param array  $data   An array of queries. Except for loaddata methods. Those require a 2 dimensional array.
	 * @param string $table
	 * @param string $method
	 * @param array  $options
	 *
	 */

	protected $data;
	protected $table;
	protected $method;
	protected $options;

	protected $sql;
	protected $start_time;
	protected $end_time;

	# Option List
	# 1. query_handler = query type
	# 2. trigger_errors = show/hide error handler
	# 3. clean_memory = data array cleanup
	# 4. link_identifier = database connection setup
	# 5. action = load data infile action type to use if import(csv) produced by user or write(txt) produced using array
	# define in config, operating system supported [windows, linux]
	public function init($data, $table, $method = 'LOAD_DATA_INFILE', $options = array()) {
		$this->data = $data;
		$this->table = $table;
		$this->method = $method;
		$this->options = $options;
		
		$this->options['query_handler'] = !isset($this->options['query_handler']) ? 'mysql_query' : $this->options['query_handler'];
		$this->options['trigger_errors'] = !isset($this->options['trigger_errors']) ? true 	: $this->options['trigger_errors'];
		$this->options['clean_memory'] 	= !isset($this->options['clean_memory']) ? false : $this->options['clean_memory'];
		$this->options['link_identifier'] = !isset($this->options['link_identifier']) ? null : $this->options['link_identifier'];
		
		$this->options['action'] = !isset($this->options['action']) && $method == 'LOAD_DATA_INFILE' ? 'write_data' : $this->options['action'];

		$this->validation();
		$this->start_time = microtime(true);
	}

	public function load() {
		switch ($this->method) {
			case 'LOAD_DATA_INFILE' :
				$this->loadDataInFile();
				break;
			
			case 'TRANSACTION' :
				// $this->options['trigger_errors'] ? trigger_error('Underconstruction: '.$this->method, E_USER_ERROR) : '';
				break;
			
			case 'INSERT_DELAYED':
				// $this->options['trigger_errors'] ? trigger_error('Underconstruction: '.$this->method, E_USER_ERROR) : '';
				break;
			
			default :
				$this->options['trigger_errors'] ? trigger_error('Unknown bulk method: '.$this->method, E_USER_ERROR) : '';
				break;
		}
	}

	public function execute() {
		try {
	        $query = $this->options['link_identifier'] === null ? call_user_func($this->options['query_handler'], $this->sql) : call_user_func($this->options['query_handler'], $this->sql, $this->options['link_identifier']);
	        $this->validateQuery($query);
		} catch (Exception $e) {
			echo $e->getMessage();
		}
		
        $this->end_time = microtime(true) - $this->start_time;
        return $query;
	}

	private function validation() {
		empty($this->data) ? trigger_error('No source data detected', E_USER_ERROR) : '';
		!is_array($this->data) ? trigger_error('First argument "queries" must be an array', E_USER_ERROR) : '';
		empty($this->table) ? trigger_error('No insert table specified', E_USER_ERROR) : '';
	}

	private function validateQuery($query) {
		if (!$query) {
			throw new Exception(
				trigger_error(
                	sprintf(
	                    'Query failed. %s [sql: %s]',
	                    mysql_error($this->options['link_identifier']),
	                    $this->sql
                	), 
                E_USER_ERROR)
          	);
		}
	}

	private function loadDataInFile() {
		$this->options['in_file_path'] = LOAD_DATA_INFILE_PATH.'/infile_'.$this->uniqueFileCode().'.txt';
		$load_data_infile_action = $this->loadDataInFileAction();
		$eol = $this->endOfLineUsedByOperatingSystem();
		
		$buffer_string = '';
		foreach ($this->data as $key => $value) {
			$buffer_string .= implode($load_data_infile_action['field_terminated'] . ',', $value) . $load_data_infile_action['lines_terminated'] . $eol;
		}
		// $fields = implode(', ', array_keys($value));
		$fields = '`'.implode('`, `', array_keys($value)).'`'; // fix issue by troy, picklist data source is not working when there is a keyword existing from mysql defined as column or field of the form
		if (!@file_put_contents($this->options['in_file_path'], $buffer_string)) {
			trigger_error('Cant write to buffer file: ' . $this->options['in_file_path'], E_USER_ERROR);
			return false;
		}

		$in_file_path = str_replace('\\', '/', $this->options['in_file_path']);
		$this->sql = "LOAD DATA INFILE '{$in_file_path}' 
					INTO TABLE `{$this->table}` 
					FIELDS TERMINATED BY '{$load_data_infile_action['field_terminated']},' 
					ENCLOSED BY '\"' 
					LINES TERMINATED BY '{$load_data_infile_action['lines_terminated']}{$eol}' 
					({$fields})";
	}

	private function uniqueFileCode() {
		$access = uniqid();
		$timestamp = $_SERVER['REQUEST_TIME'];
		return (md5(microtime(true)) . '_' . $access . '_' . $timestamp);
	}

	private function loadDataInFileAction() {
		$terminator = array();
		switch ($this->options['action']) {
			case 'write_data' :
				$terminator['field_terminated'] = ';;;:::';
				$terminator['lines_terminated'] = '^^^';
				break;
			case 'import_data' :
				$terminator['field_terminated'] = '';
				$terminator['lines_terminated'] = '';
				break;
			default :
				break;
		}
		return $terminator;
	}

	private function endOfLineUsedByOperatingSystem() {
		$eol = '';
		switch (LOAD_DATA_INFILE_OS_USED) {
			case 'windows':
				$eol = "\r\n";
				break;
			case 'apple':
				$eol = "\r";
				break;
			case 'ubuntu':
				$eol = "\n";
				break;
			case 'linux':
				$eol = "\n";
				break;
			default :
				break;
		}
		return $eol;
	}

	# Free memory in $data array, InFile.txt in data path, and last value of auto_increment in the table
	public function loadDataInFileCleanUp() {
		if ($this->options['clean_memory']) {
			unset($this->data);
		}
		if ($this->options['in_file_path']) {
			@unlink($this->options['in_file_path']);
		}
		
		$this->loadDataInFileResetIncrement();
	}
	
	public function loadDataInFileResetIncrement() {
		//removed by joshua reyes [08-23-2016] deprecated solution in autoincrement hole we now used [mysqld] innodb_autoinc_lock_mode = 0 native algo to avoid incremental hole in InnoDB

		// $sql = "alter table `{$this->table}` auto_increment=0";

		// if ($this->options['link_identifier'] == null) {
		// 	call_user_func($this->options['query_handler'], $sql);
		// } else {
		// 	call_user_func($this->options['query_handler'], $sql, $this->options['link_identifier']);
		// }
	}

	# Benchmarking Query Per Second
	# Note: must declare after of execute function, and also benchmark will not load if clean_memory is set true
	public function benchMarkQPS() {
		if ($this->options['clean_memory'] === false) {
			$total_num_query = count($this->data);
			$qps = round($total_num_query / $this->end_time, 2);
			return 'All went well @' . $qps . ' queries per second(s) | Total Queries: ' . $total_num_query;
		} else {
			return 'Memory cleanup is enable, data source was cleared before benchmark read!';
		}
	}
}
