<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DatabaseConnection
 *
 * @author Jewel Tolentino
 */
class DatabaseConnection {

    private $handle;
    public $is_connected = false;
    public $type;
    public $lastInsertId;

    public function connect($host, $db_name, $user_name, $password) {
        switch ($this->type) {
            case "ms_sql";
                $conn = $this->getMSSQLConnection($host, $db_name, $user_name, $password);
                break;
            case "oracle":
                $conn = $this->getOracleConnection($host, $db_name, $user_name, $password);
                break;
            default :
                $conn = $this->getMySQLConnection($host, $db_name, $user_name, $password);
                break;
        }

        if ($conn) {
            $this->handle = $conn;
            $this->is_connected = true;

            unset($conn);
        }
    }

    private function getMSSQLConnection($host, $db_name, $user_name, $password) {
        /*
          ===============================================
          make sure to enable php_pdo_odbc.dll on php.ini
          ===============================================
         */
        try {
            $conn = new PDO("odbc:Driver={SQL Server};Server={$host};Database={$db_name}; Uid={$user_name};Pwd={$password};Regional=No;");
            return $conn;
        } catch (Exception $ex) {
            createQueryLog($this->type . " : odbc:Driver={SQL Server};Server={$host};Database={$db_name}; Uid={$user_name};Pwd={$password};Regional=No;");
            createQueryLog($this->type . " : " . $ex->getMessage());
        }
    }

    private function getOracleConnection($host, $db_name, $user_name, $password) {
        /*
          ===============================================
          make sure to enable php_oci8.dll and php_pdo_oci.dll on php.ini
          ===============================================
         */
        try {
            $conn = new PDO("oci:dbname=" . $host . "/" . $db_name, $user_name, $password);
            return $conn;
        } catch (Exception $ex) {
            createQueryLog($this->type . " : oci:dbname=" . $host . "/" . $db_name . ";" . $user_name . ";" . $password);
            createQueryLog($this->type . " : " . $ex->getMessage());
        }
    }

    private function getMySQLConnection($host, $db_name, $user_name, $password) {
        /*
          ===============================================
          make sure to enable php_pdo_mysql.dll on php.ini
          ===============================================
         */
        try {
            $conn = new PDO("mysql:host=" . $host . ";dbname=" . $db_name, $user_name, $password);
            return $conn;
        } catch (Exception $ex) {
            createQueryLog($this->type . " : host=" . $host . ";dbname=" . $db_name . ";" . $user_name . ";" . $password);
            createQueryLog($this->type . " : " . $ex->getMessage());
        }
    }

    public function query($clause, $fetch_type = "array") {
        $stmt = $this->handle->prepare($clause);
        $stmt->execute();
        switch ($fetch_type) {
            case "row":
                $result = $stmt->fetch(PDO::FETCH_ASSOC);
                break;
            case "numrows":
                $result = count($stmt->fetchAll());
                break;
            default:
                $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                break;
        }

        createQueryLog($this->type . " : " . $clause);
        if (!$result) {
            createQueryLog($this->type . " : " . $stmt->errorInfo()[2]);
        }

        unset($stmt);
        return $result;
    }

    public function insert($tbl_name, $fields = array()) {
        $str_sql = "INSERT INTO {$tbl_name} (";
        $field_names = "";
        $field_values = "";

        foreach ($fields as $key => $value) {
            $field_names.= $key . ",";
            $field_data = $this->getTableColumns($tbl_name, $key);
            $field_type = $field_data[0]["DATA_TYPE"];
            $field_values.= "{$this->escape($value, $field_type)},";
        }

        $field_names_str = substr($field_names, 0, strlen($field_names) - 1);
        $field_values_str = substr($field_values, 0, strlen($field_values) - 1);


        $str_sql .= $field_names_str . ") VALUES (" . $field_values_str . ")";
        $stmt = $this->handle->prepare($str_sql);
        $insert_result = $stmt->execute();

        if ($this->type == "my_sql") {
            $insert_result = $this->handle->lastInsertId();
        }

        createQueryLog($this->type . " : " . $str_sql);
        if (!$insert_result) {
            createQueryLog($this->type . " : " . $stmt->errorInfo()[2]);
        }


        return $insert_result;
    }

    public function update($tbl_name, $fields = array(), $condition = array()) {
        $str_sql = "UPDATE {$tbl_name} SET ";
        $fields_set = "";
        foreach ($fields as $key => $value) {
            $field_data = $this->getTableColumns($tbl_name, $key);
            $field_type = $field_data[0]["DATA_TYPE"];
            $fields_set .= $key . "={$this->escape($value, $field_type)}, ";
        }

        $fields_set_str = substr($fields_set, 0, strlen($fields_set) - 2);

        $condition_str = " WHERE ";
        foreach ($condition as $key => $value) {
            $field_data = $this->getTableColumns($tbl_name, $key);
            $field_type = $field_data[0]["DATA_TYPE"];
            $condition_str .= $key . "={$this->escape($value, $field_type)} AND ";
        }

        $condition_set_str = substr($condition_str, 0, strlen($condition_str) - 4);

        $str_sql .= $fields_set_str . $condition_set_str;
        $stmt = $this->handle->prepare($str_sql);

        $update_result = $stmt->execute();

        createQueryLog($this->type . " : " . $str_sql);
        if (!$update_result) {
            createQueryLog($this->type . " : " . $stmt->errorInfo()[2]);
        }

        return $update_result;
    }

    public function delete($tbl_name, $condition = array()) {
        $str_sql = "DELETE FROM {$tbl_name} ";

        $condition_str = " WHERE ";
        foreach ($condition as $key => $value) {
            $field_data = $this->getTableColumns($tbl_name, $key);
            $field_type = $field_data[0]["DATA_TYPE"];
            $condition_str .= $key . "={$this->escape($value, $field_type)} AND ";
        }

        $condition_set_str = substr($condition_str, 0, strlen($condition_str) - 4);
        $str_sql.=$condition_set_str;
        $stmt = $this->handle->prepare($str_sql);

        $delete_result = $stmt->execute();

        createQueryLog($this->type . " : " . $str_sql);
        if (!$delete_result) {
            createQueryLog($this->type . " : " . $stmt->errorInfo()[2]);
        }

        return $delete_result;
    }

    public function disConnect() {
        $this->handle = null;
    }

    public function escape($clause, $input_type = "string") {
        switch ($this->type) {
            case "ms_sql":
                $return_value = "'" . $this->ms_escape_string($clause) . "'";
                break;
            case "oracle":
                if (strtolower($input_type) == "date") {
                    $return_value = "to_date('{$this->oracle_escape_string($clause)}','YYYY-MM-DD HH24:MI:SS')";
                } elseif (strtolower($input_type) == "timestamp(6)") {
                    $return_value = "to_timestamp('{$this->oracle_escape_string($clause)}','HH24:MI:SS')";
                } else {
                    $return_value = "'" . $this->oracle_escape_string($clause) . "'";
                }
                break;
            default:
                $return_value = "'" . addslashes($clause) . "'";
        }

        return $return_value;
    }

    private function ms_escape_string($data) {
        if (!isset($data) or empty($data))
            return '';

        $non_displayables = array(
            '/%0[0-8bcef]/', // url encoded 00-08, 11, 12, 14, 15
            '/%1[0-9a-f]/', // url encoded 16-31
            '/[\x00-\x08]/', // 00-08
            '/\x0b/', // 11
            '/\x0c/', // 12
            '/[\x0e-\x1f]/'             // 14-31
        );
        foreach ($non_displayables as $regex) {
            $data = preg_replace($regex, '', $data);
        }
        $data = str_replace("'", "''", $data);
        return $data;
    }

    private function oracle_escape_string($data) {
        $data = str_replace("'", "''", $data);
        return $data;
    }

    public function beginTransaction() {
        $this->handle->beginTransaction();
    }

    public function rollBack() {
        $this->handle->rollBack();
    }

    public function commit() {
        $this->handle->commit();
    }

    public function getTableColumns($tbl_name, $field_name = "") {
        switch ($this->type) {
            case "ms_sql":
                if ($field_name != "") {
                    $field_name_clause = "AND column_name = {$this->escape($field_name)}";
                }
                $data_type_result = $this->query("SELECT 
                                    column_name AS COLUMN_NAME, 
                                    data_type AS DATA_TYPE,
                                    character_maximum_length AS DATA_LENGTH 
                                    FROM information_schema.COLUMNS WHERE table_name = {$this->escape($tbl_name)} {$field_name_clause}
                                    ORDER BY ordinal_position");
                break;
            case "oracle":
                if ($field_name != "") {
                    $field_name_clause = "AND column_name = {$this->escape($field_name)}";
                }
                $data_type_result = $this->query("select column_name, data_type, data_length
                    from all_tab_columns
                    where table_name = {$this->escape($tbl_name)} {$field_name_clause}");
                break;
            default:
                $data_type_result["DATA_TYPE"] = "string";
        }

        return $data_type_result;
    }

}
