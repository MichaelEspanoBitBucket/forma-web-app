<?php


class Delegate extends Database{
	var $auth;

	public function __construct(){
		parent::__construct();
		$this->auth = Auth::getAuth('current_user');
	}
	
	public function getDelegate($search, $start, $json, $type){
		$auth = $this->auth;
        $company_id = $auth['company_id'];
        $userID = $auth['id'];

        $limit = "";
        $end = "10";
        if($json['endLimit']!=""){
            $end = $json['endLimit'];
        }
        if ($start != "") {
            $limit = " LIMIT $start , $end";
        }
        $orderBy = " ORDER BY tbu_delegate.display_name ASC, tbd.is_active ";
        if ($json['column-sort'] != "") {
            $columnSort = $json['column-sort'];
            if($json['column-sort']=="delegate_user"){
                $columnSort = "tbu_delegate.display_name";
            }else if($json['column-sort']=="delegate_to_user"){
                $columnSort = "tbu_to_delegate.display_name";
            }
            $orderBy = " ORDER BY " . $json['column-sort'] . " " . $json['column-sort-type'];
        }

        if ($type == "numrows") {
            $limit = "";
        }
        $condition = "";
        if($search!=""){
        	$condition = " AND (tbu_delegate.display_name LIKE '%". $search ."%' 
                || tbu_to_delegate.display_name LIKE '%". $search ."%'
        		|| tbd.remarks LIKE '%". $search ."%' 
        		|| tbd.date_created LIKE '%". $search ."%' 
        		|| tbd.date_updated LIKE '%". $search ."%' 
        		|| tbd.start_date LIKE '%". $search ."%' 
        		|| tbd.end_date LIKE '%". $search ."%') ";
        }
        if($json['condition']!=""){
        	$condition.=$json['condition'];
        }

        $queryStr = "SELECT tbd.*, 
        tbu_delegate.display_name as delegate_user,
        tbu_to_delegate.display_name as delegate_to_user,

        tbu_created_by.display_name as created_by_user,
        tbu_updated_by.display_name as updated_by_user


        FROM tbdelegate tbd 
        LEFT JOIN tbuser tbu_delegate 
        ON tbd.user_id = tbu_delegate.id 
        LEFT JOIN tbuser tbu_to_delegate 
        ON tbd.user_delegate_id = tbu_to_delegate.id 

        LEFT JOIN tbuser tbu_created_by 
        ON tbd.created_by = tbu_created_by.id 
        LEFT JOIN tbuser tbu_updated_by 
        ON tbd.updated_by = tbu_updated_by.id 

        WHERE tbu_delegate.company_id = '". $company_id ."' ".$condition.$orderBy.$limit;
        return $this->query($queryStr,$type);
	}
}

?>