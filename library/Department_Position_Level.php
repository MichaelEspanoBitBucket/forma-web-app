<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Deparment_Position_Level
 *
 * @author Jewel Tolentino
 */
class Department_Position_Level extends Formalistics {

    //put your code here

    public $position;
    public $is_active;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();

        if ($id) {
            $result = $redis_client->query("department_position_level_object_" . $id, "SELECT * FROM tbuser_position_level WHERE id = {$this->db->escape($id)}");

            $this->id = $result['id'];
            $this->position = $result['position'];
            $this->is_active = $result['is_active'];
        }
    }

}

?>
