<?php

/**
 * Description of ExtensionPostSave
 *
 * @author ervinne
 */
class Extensions {

    public static $PRE_SAVE = 1;
    public static $POST_SAVE = 2;
    public static $PRE_MODIFY = 3;
    public static $POST_MODIFY = 4;
    public static $POST_PROCESS_WORKFLOW = 5;
    public static $ON_CHILD_CREATED = 6;
    
    //  TODO: define all your executable extension scripts (defined in Extensions folder)
    //  below:
    private $postProcessWFExecutables = array();
//    private $preSaveExecutables = ['NAV_Injection/NAV_Injection_Manager'];
    private $preSaveExecutables = array();
    private $postSaveExecutables = array();
//    private $preModifyExecutables = ['NAV_Injection/NAV_Injection_Manager'];
    private $preModifyExecutables = array();
    private $postModifyExecutables = array();
    private $onChildCreatedExecutables = array();

    public function executeExtension(Request $request, $event) {

        switch ($event) {
            case Extensions::$PRE_SAVE : $executables = $this->preSaveExecutables;
                break;
            case Extensions::$POST_SAVE : $executables = $this->postSaveExecutables;
                break;
            case Extensions::$PRE_MODIFY : $executables = $this->preModifyExecutables;
                break;
            case Extensions::$POST_MODIFY : $executables = $this->postModifyExecutables;
                break;
            case Extensions::$POST_PROCESS_WORKFLOW : $executables = $this->postProcessWFExecutables;
                break;
            case Extensions::$ON_CHILD_CREATED : $executables = $this->onChildCreatedExecutables;
                break;
            default : $executables = array();
        }

        $extensionResponses = array();
        foreach ($executables as $moduleName) {
            if (!empty($moduleName)) {
                $response = $this->executeModule($moduleName, $request, $event);
                array_push($extensionResponses, $response);
            }
        }

        return $extensionResponses;
    }

    public function executeModule($moduleName, $request, $event) {
        
        $modulePath = $moduleName;
        
        if (-1 < strpos($moduleName, '/')) {
            //  the module that will be executed has a path in it
            $modulePathSplitted = explode('/', $moduleName);
            $lastIndex = count($modulePathSplitted) - 1;            
                        
            $moduleName = $modulePathSplitted[$lastIndex];
        }
        
        include_once APPLICATION_PATH . '/Extensions/' . $modulePath . '.php';

        /*  @var $executable Executable */
        $executable = new $moduleName;
        return $executable->execute($request, $event);
    }

}
