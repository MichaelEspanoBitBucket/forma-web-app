<?php
class ExternalDatabase extends Formalistics {

    //put your code here
    public function __construct($db) {
        $this->db = $db;
    }

    public function getExternalDBDatatable($json,$company_id,$type) {

        $limit = "";
        $end = "10";
        $start = $json['start'];
        $search_value = $json['search_value'];
        if($json['endLimit']!=""){
            $end = $json['endLimit'];
        }
        if ($start != "" && $type!="numrows") {
            $limit = " LIMIT $start , $end";
        }

        $orderBy = " ORDER BY tbe_d.connection_name ASC ";
        if ($json['column-sort'] != "") {
            $columnSort = $json['column-sort'];
            if($json['column-sort']=="delegate_user"){
                $columnSort = "tbu_delegate.display_name";
            }else if($json['column-sort']=="delegate_to_user"){
                $columnSort = "tbu_to_delegate.display_name";
            }
            $sort = $json['column-sort'];
            if($sort=='db_type_name'){
                $sort = "tbe_d_t.type";
            }
            $orderBy = " ORDER BY " . $sort . " " . $json['column-sort-type'];
        }
        if($search_value!=""){
            $condition = " AND (tbe_d.connection_name LIKE '%$search_value%' OR tbe_d_t.type LIKE '%$search_value%') ";
        }
        // if($json['condition']!=""){
        //     $condition.=$json['condition'];
        // }
        return $this->getExternalDB($condition,$limit,$orderBy,$company_id,$type);
    }

    public function getExternalDB($condition,$limit,$order,$company_id,$type="array"){

        if($order==""){
            $order = " ORDER BY tbe_d.connection_name ASC ";
        }
        $getExternalDBSQL = "SELECT tbe_d.*, tbe_d_t.type as db_type_name,
                        tbu_c.display_name as created_displayname,
                        tbu_u.display_name as updated_displayname
                         FROM 
                        tbexternal_database tbe_d
                        LEFT JOIN 
                        tbexternal_database_type tbe_d_t
                        ON tbe_d.db_type = tbe_d_t.id

                        LEFT JOIN 
                        tbuser tbu_c
                        ON tbe_d.created_by = tbu_c.id

                        LEFT JOIN 
                        tbuser tbu_u
                        ON tbe_d.updated_by = tbu_u.id

                        WHERE tbe_d.company_id = {$this->db->escape($company_id)} AND tbe_d.is_delete = 0 $condition $order $limit";
        // return $getExternalDBSQL;
        return $result = $this->db->query($getExternalDBSQL, $type);
    }


}
?>
