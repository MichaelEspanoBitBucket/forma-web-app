<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Field
 *
 * @author Jewel Tolentino
 */
class Field extends Formalistics {

    //put your code here
    public $name;
    public $form;
    public $type;
    public $input_type;
    public $formula_type;
    public $formula;
    public $visibility_formula;
    public $label;

    public function __construct($db, $id) {
        $this->tblname = 'tbfields';
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();
        if ($id) {
            $result = $redis_client->query("field_object_" . $id, "SELECT * FROM {$this->tblname} WHERE id = {$this->db->escape($id)}");

            $this->id = $result['ID'];
            $this->name = $result["field_name"];
//            $this->form = new Form($this->db, $result["form_id"]);
            $this->type = $result["field_type"];
            $this->input_type = $result["field_input_type"];
            $this->formula_type = $result["formula_type"];
            $this->formula = $result["formula"];
            $this->visibility_formula = $result["visibility_formula"];
            $this->label = $result["data_field_label"];
        }
    }

}
