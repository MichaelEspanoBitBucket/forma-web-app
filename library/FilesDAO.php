<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FilesDAO
 *
 * @author Ervinne
 */
class FilesDAO {

    /** @var upload */
    protected $upload;

    /** @var ImageFilter */
    protected $image_filter;

    public function getProcessedImageURL($user_id, $image_extension) {

        $table      = "tbuser";
        $id_encrypt = md5(md5($user_id));
        $size       = "small";

        return '/images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $image_extension;
    }

    public function moveAttachments($id, $user_id, $files_to_move, $folder_name) {

        //  lazyload upload object instance
        if (!$this->upload) {
            $this->upload = new upload();
        }

        $parent_path     = "images/" . $folder_name;
        $encrypted_id    = md5(md5($id));
        $attachment_path = $parent_path . "/" . $encrypted_id;

        if (count($files_to_move) != 0) {
//            echo $attachment_path;
            $this->upload->createFolder($attachment_path);
        }

        //  compute the temporary folder path of the file(s) to move
        $encrypted_user_id         = md5(md5($user_id));
        $temporary_attachment_path = "images/" . $folder_name . "/temporary_files/" . $encrypted_user_id;

        $attachments = array();

        // Move each file to the proper directory
        foreach ($files_to_move as $file) {

            if ($file && trim($file) !== "") {                
                $destination_path_decoded = htmlentities($attachment_path . '/' . $file, ENT_QUOTES, "UTF-8");
                rename($temporary_attachment_path . '/' . $file, $destination_path_decoded);

                //  generate each file's URL
                array_push($attachments, array(
                    "url" => "images/" . $folder_name . "/" . $encrypted_id . "/" . $file,
                    "path" => $destination_path_decoded
                ));
            }
        }

        //  cleanup
        $this->upload->unlinkRecursive($temporary_attachment_path, "");

        $residual_temporary_files = glob("{$temporary_attachment_path}/*"); // get all file names
        foreach ($residual_temporary_files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        return $attachments;
    }

}
