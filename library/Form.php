<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form
 *
 * @author Jewel Tolentino
 */
class Form extends Formalistics {

    //put your code here
    public $form_alias;
    public $form_name;
    public $form_table_name;
    public $form_description;
    public $form_content;
    public $form_buttons;
    public $creator;
    public $modifier;
    public $company;
    public $button_content;
    public $reference_prefix;
    public $reference_type;
    public $version;
    public $form_json;
    public $date_created;
    public $is_active;
    public $active_fields;
    public $date_updated;
    public $mobile_json_data;
    public $mobile_content;
    public $form_authors;
    public $display_on_sidebar;
    public $enable_deletion;
    public $deletion_type;
    public $category_id;
    public $form_name_formula;
    public $enable_formula;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();
        if ($id) {
            $result = $redis_client->query("form_object_" . $id, "SELECT * FROM tb_workspace WHERE id = {$this->db->escape($id)} AND is_delete = 0");

            $this->id = $result['id'];
            $this->form_name = $result['form_name'];
            $this->form_alias = $result['form_alias'];
            $this->form_table_name = $result['form_table_name'];
            $this->form_description = $result['form_description'];
            $this->form_content = $result['form_content'];
            $this->form_buttons = $result['form_buttons'];
            $this->creator = new Person($this->db, $result['created_by']);
            $this->modifier = new Person($this->db, $result['updated_by']);
            $this->company = new Company($this->db, $result['company_id']);
            $this->button_content = $result['button_content'];
            $this->reference_prefix = $result['reference_prefix'];
            $this->reference_type = $result['reference_type'];
            $this->version = $result['version'];
            $this->form_json = $result['form_json'];
            $this->date_created = $result['date_created'];
            $this->is_active = $result['is_active'];
            $this->active_fields = $result['active_fields'];
            $this->date_updated = $result['date_updated'];
            $this->mobile_json_data = $result['MobileJsonData'];
            $this->mobile_content = $result['MobileContent'];
            $this->form_authors = $result['form_authors'];
            $this->display_on_sidebar = $result['form_display_type'];
            $this->enable_deletion = $result['enable_deletion'];
            $this->deletion_type = $result['deletion_type'];
            $this->category_id = $result['category_id'];
            //added by paul del rosario 3.4.16
            $this->enable_formula = $result['enable_formula'];
            $this->form_name_formula = $result['form_name_formula'];
            $this->formula_is_field = $result['formula_is_field'];
        }
    }

}

?>
