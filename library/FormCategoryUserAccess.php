<?php

class FormCategoryUserAccess{
    var $category_id;
    var $db;


    public function __construct($db,$category_id){
        $this->category_id = $category_id;
        $this->db = $db;
    }

    public function insertUser($jsonChecked,$access_type){
        // $jsonChecked = json_decode($jsonChecked,true);
        foreach ($jsonChecked as $key => $valueChecked) {
            $user_type = functions::setUserType($key);

            //insert
            foreach ($valueChecked as $key => $user) {
                $insert = array("form_category_id" => $this->category_id,
                    "user" => $user,
                    "user_type" => $user_type,
                    "access_type" => $access_type,
                );
                echo $this->db->insert("tbform_category_users",$insert);
            }
        }
    }
    public function deleteUser($jsonUnChecked,$access_type){
        // $jsonUnChecked = json_decode($jsonUnChecked,true);

        foreach ($jsonUnChecked as $key => $valueUnChecked) {
            $user_type = functions::setUserType($key);

            //insert
            foreach ($valueUnChecked as $key => $user) {
                $insert = array("form_category_id" => $this->category_id,
                    "user" => $user,
                    "user_type" => $user_type,
                    "access_type" => $access_type
                );
                $this->db->delete("tbform_category_users",$insert);
            }
        }
    }
}
?>