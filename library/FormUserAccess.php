<?php
class FormUserAccess{
    var $form_id;
    var $db;


    public function __construct($db,$form_id){
        $this->form_id = $form_id;
        $this->db = $db;
    }

    public function insertUser($jsonChecked,$action_type){
        // $jsonChecked = json_decode($jsonChecked,true);
        foreach ($jsonChecked as $key => $valueChecked) {
            $user_type = functions::setUserType($key);

            //insert
            foreach ($valueChecked as $key => $user) {
                $insert = array("form_id" => $this->form_id,
                    "user" => $user,
                    "user_type" => $user_type,
                    "action_type" => $action_type,
                );
                $this->db->insert("tbform_users",$insert);
            }
        }
    }
    public function deleteUser($jsonUnChecked,$action_type){
        // $jsonUnChecked = json_decode($jsonUnChecked,true);

        foreach ($jsonUnChecked as $key => $valueUnChecked) {
            $user_type = functions::setUserType($key);

            //delete
            foreach ($valueUnChecked as $key => $user) {
                $delete = array("form_id" => $this->form_id,
                    "user" => $user,
                    "user_type" => $user_type,
                    "action_type" => $action_type
                );
                $this->db->delete("tbform_users",$delete);
                if($action_type==3){
                    //delete report
                    $deleteReportUser = "DELETE tbru FROM 
                    tbreport tbr 
                    LEFT JOIN 
                    tbreport_users tbru 
                    ON 
                    tbr.id = tbru.report_id 
                    WHERE 
                    form_id = ". $this->form_id ." and tbru.user = '". $user ."' AND tbru.user_type = ". $user_type ."";
                    $this->db->create($deleteReportUser);
                }
            }
        }
    }
}
?>