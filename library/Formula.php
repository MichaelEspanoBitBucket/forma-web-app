<?php

function fatal_handler() {
    // $temp = array();
    // array_push($temp, error_get_last());
    // array_push($temp, $GLOBALS['formula_executed_data_collector']);
    // print_r($temp);
}

$GLOBALS['formula_executed_data_collector'] = array(
    "collected_form_id" => array(),
    "track_formula_list" => array(),
    "tbfields_query" => array()
);

register_shutdown_function("fatal_handler");


$timezone = "Asia/Manila";

if (function_exists('date_default_timezone_set'))
    date_default_timezone_set($timezone);

include("FormulaCustomScript.php");

function GetParam($param, $index) {
    return $_GET[$param][$index];
}

function getFormulaAuth() {
    if (Auth::hasAuth('current_user')) {
        $auth = Auth::getAuth('current_user');
    } else if($GLOBALS["auth"]) {
        $auth = $GLOBALS["auth"];
    } else {
        $db = new Database();
        $options = getopt('f:r:u:');
        $form_id = $options['f'];
        $request_id = $options['r'];
        $user_id = $options['u'];

        $query = "SELECT * FROM tbuser WHERE id = {$db->escape($user_id)}";
        $auth = $db->query($query, 'row');
    } 
    return $auth;
}

function getFormulaFormDetails($FormName) {
    $db = new Database();
    $redis_cache = getRedisConnection();

    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";

    if ($redis_cache) {
        $cache_formula_form_details = json_decode($redis_cache->get("formula_form_details_" . $FormName . "::" . $auth['company_id']), true);
    }

    if ($cache_formula_form_details) {
        $this_get_table_form = $cache_formula_form_details;
    } else {
        $q_get_table_form_name = "SELECT `id`, `form_table_name` FROM `tb_workspace` WHERE is_delete = 0 AND `form_name` LIKE '" . $FormName . "' " . $companyFilter;
        $this_get_table_form = $db->query($q_get_table_form_name, "row");

        if ($redis_cache) {
            $redis_cache->set("formula_form_details_" . $FormName . "::" . $auth['company_id'], json_encode($this_get_table_form));
        }
    }
    $this_get_table_form['_db'] = $db;
    return $this_get_table_form;
}

function NumToWordMichael($num, $format = "", $currency = "", $decimal = -1) {
    echo $num;
    echo "<br/>";
    $decimal_position = array(
        'tenths', 'hundredths', 'thousandths', 'ten-thousandths', 'hundred-thousandths', 'millionths', 'ten-millionths', 'hundred-millionths', 'billionths', 'ten-billionths', 'hundred-billionths', 'trillionths', 'ten-trillionths', 'hundred-trillionths', 'quadrillionths', 'ten-quadrillionths', 'hundred-quadrillionths'
    );



    $convert_number_to_words_tool = function ($number, $convert_number_to_words_tool) {
        $hyphen = '-';
        $conjunction = ' ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' and ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'forty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );
        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (float) $number < 0) || (float) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                    '$convert_number_to_words_tool only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . $convert_number_to_words_tool(abs($number), $convert_number_to_words_tool);
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens =  GetRound((float) ($number / 10), 0, "down") * 10;
                $units = fmod($number , 10);
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = fmod($number , 100);
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $convert_number_to_words_tool($remainder, $convert_number_to_words_tool);
                }
                break;
            default:

                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = GetRound((float) ($number / $baseUnit), 0, "down");
                $remainder = fmod($number, $baseUnit);
                $string = $convert_number_to_words_tool($numBaseUnits, $convert_number_to_words_tool) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $convert_number_to_words_tool($remainder, $convert_number_to_words_tool);
                }

                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {

            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    };

    
    
    $result_word = "";
    if ($num) {
        if( $decimal == -1 && $format == "currency" ){
            $num_chunks = explode(".", strval( $num ) );
        }else if($format == "check"){
            $decimal = 2;
            $num_chunks = explode(".", strval( round($num , $decimal, PHP_ROUND_HALF_UP) ) );
        }else if($decimal != -1){
            $num_chunks = explode(".", strval( round($num , $decimal, PHP_ROUND_HALF_UP) ) );
        }else{
            $num_chunks = explode(".", strval( $num ) );
        }
        $result_word = ucfirst($convert_number_to_words_tool((float) $num_chunks[0], $convert_number_to_words_tool));
        if($result_word == "Too Large"){
            return $result_word;
        }
        else if (count($num_chunks) == 2) {
            if ($format == "currency") {
                $result_word .= " " . $currency . " and " . $convert_number_to_words_tool((float) $num_chunks[1], $convert_number_to_words_tool) . " cents" ;
            }else if($format == "check"){
                $result_word .= " and " . substr($num_chunks[1]."00",0,2) . "/100 " . $currency ;
            }else{
                $result_word .= " and " . $convert_number_to_words_tool((float) $num_chunks[1], $convert_number_to_words_tool);
                $result_word .= " " . $decimal_position[strlen($num_chunks[1]) - 1];
            }
            // $result_word .= " and " . $convert_number_to_words_tool((float) $num_chunks[1], $convert_number_to_words_tool);
            // $result_word .= " " . $decimal_position[strlen($num_chunks[1]) - 1];
        }else{
            if ($format == "currency") {
                $result_word .= " " . $currency;
            }else if($format == "check"){
                $result_word .= " and 00/100 " . $currency ;
            }else{
                $result_word .= " " . $decimal_position[strlen($num_chunks[1]) - 1];
            }
        }


    }

    return $result_word;
}
function NumToWord($num, $format = "", $currency = "", $decimal = -1) {
    $decimal_position = array(
        'tenths', 'hundredths', 'thousandths', 'ten-thousandths', 'hundred-thousandths', 'millionths', 'ten-millionths', 'hundred-millionths', 'billionths', 'ten-billionths', 'hundred-billionths', 'trillionths', 'ten-trillionths', 'hundred-trillionths', 'quadrillionths', 'ten-quadrillionths', 'hundred-quadrillionths'
    );


    $convert_number_to_words_tool = function ($number, $convert_number_to_words_tool) {
        $hyphen = '-';
        $conjunction = ' ';
        $separator = ', ';
        $negative = 'negative ';
        $decimal = ' and ';
        $dictionary = array(
            0 => 'zero',
            1 => 'one',
            2 => 'two',
            3 => 'three',
            4 => 'four',
            5 => 'five',
            6 => 'six',
            7 => 'seven',
            8 => 'eight',
            9 => 'nine',
            10 => 'ten',
            11 => 'eleven',
            12 => 'twelve',
            13 => 'thirteen',
            14 => 'fourteen',
            15 => 'fifteen',
            16 => 'sixteen',
            17 => 'seventeen',
            18 => 'eighteen',
            19 => 'nineteen',
            20 => 'twenty',
            30 => 'thirty',
            40 => 'forty',
            50 => 'fifty',
            60 => 'sixty',
            70 => 'seventy',
            80 => 'eighty',
            90 => 'ninety',
            100 => 'hundred',
            1000 => 'thousand',
            1000000 => 'million',
            1000000000 => 'billion',
            1000000000000 => 'trillion',
            1000000000000000 => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );
        if (!is_numeric($number)) {
            return false;
        }

        
        if( -PHP_INT_MAX >= -2147483647 && PHP_INT_MAX <= 2147483647 ){
            if( (float) $number > (float) "2000000000" || (float) $number < (float) "-2000000000"){
                return "Too Large";
            }
        }else {
            if ((int) $number >= PHP_INT_MAX || (int) $number <= -PHP_INT_MAX) {
                // overflow
                return "Too Large";
                // trigger_error(
                //         '$convert_number_to_words_tool only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
                // );
                // return false;
            }
        }

        if ($number < 0) {
            return $negative . $convert_number_to_words_tool(abs($number), $convert_number_to_words_tool);
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens = ((int) ($number / 10)) * 10;
                $units = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . $convert_number_to_words_tool($remainder, $convert_number_to_words_tool);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = $convert_number_to_words_tool($numBaseUnits, $convert_number_to_words_tool) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= $convert_number_to_words_tool($remainder, $convert_number_to_words_tool);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    };



    $result_word = "";
    if ($num) {
        if( $decimal == -1 && $format == "currency" ){
            $num_chunks = explode(".", strval( $num ) );
        }else if($format == "check"){
            $decimal = 2;
            $num_chunks = explode(".", strval( round($num , $decimal, PHP_ROUND_HALF_UP) ) );
        }else if($decimal != -1){
            $num_chunks = explode(".", strval( round($num , $decimal, PHP_ROUND_HALF_UP) ) );
        }else{
            $num_chunks = explode(".", strval( $num ) );
        }
        $result_word = ucfirst($convert_number_to_words_tool((int) $num_chunks[0], $convert_number_to_words_tool));
        $temp = $num_chunks[0] . ($num_chunks[1] ? ".".$num_chunks[1] : "" );
        if( (float) $temp > 2000000000 ){
            return "Too Large";
        }else if($result_word == "Too Large"){
            return $result_word;
        }
        if (count($num_chunks) == 2) {
            if ($format == "currency") {
                $result_word .= " " . $currency . " and " . $convert_number_to_words_tool( StrLeft(((int) $num_chunks[1]).'00',2), $convert_number_to_words_tool) . " cents" ;
            }else if($format == "check"){
                $result_word .= " and " . substr($num_chunks[1]."00",0,2) . "/100 " . $currency ;
            }else{
                $result_word .= " and " . $convert_number_to_words_tool((int) $num_chunks[1], $convert_number_to_words_tool);
                $result_word .= " " . $decimal_position[strlen($num_chunks[1]) - 1];
            }
            // $result_word .= " and " . $convert_number_to_words_tool((int) $num_chunks[1], $convert_number_to_words_tool);
            // $result_word .= " " . $decimal_position[strlen($num_chunks[1]) - 1];
        }else{
            if ($format == "currency") {
                $result_word .= " " . $currency;
            }else if($format == "check"){
                $result_word .= " and 00/100 " . $currency ;
            }else{
                $result_word .= " " . $decimal_position[strlen($num_chunks[1]) - 1];
            }
        }


    }

    return $result_word;
}

function FormulaList($selected_fields, $excluded_field) {
	$excluded_field = $excluded_field?:array();
    $selected_fields = $selected_fields?:array();
    
	$GLOBALS['formula_executed_data_collector']['track_formula_list'] = array();
    if (isset($_POST['_formula_list_data']) && isset($_POST['_form_reserve_keys']) && isset($_POST['_form_user_fields'])) {
        $formatted_data = array(
            "visibility" => array(),
            "computed_value" => array(),
            "change_list_computed_value" => array(),
            "placeholder_formula" => array(),
            "readonly" => array(),
            "validation" => array(),
            "label_formula" => array()
        );

        $post_data = $_POST;
        $post_data = array_merge($post_data,$post_data["_form_reserve_keys"]);
        $post_data = array_merge($post_data,$post_data["_form_user_fields"]);



        $reserve_words = array_keys($post_data["_form_reserve_keys"]);
        $user_fields = array_keys($post_data["_form_user_fields"]);

        if ($post_data["_formula_list_onchange_auto_excluded_fieldname"])
            array_push($excluded_field, $post_data["_formula_list_onchange_auto_excluded_fieldname"]);


        array_push($reserve_words, "_formula_list_data");
        array_push($reserve_words, "_form_reserve_keys");
        array_push($reserve_words, "_form_user_fields");
        array_push($reserve_words, "_formula_list_onchange_auto_excluded_fieldname");

        $exclude_distribution = $reserve_words;

        if (gettype($selected_fields) == "array") {
            if (count($selected_fields) >= 1) {
                $exclude_distribution = array_values(array_unique(array_merge($exclude_distribution, $user_fields))); //unset lahat
                $exclude_distribution = array_values(array_unique(array_diff($exclude_distribution, $selected_fields))); //ung mga selected field dapat di ma unset so //tatanggalin sa excluded_field ung mga selected_fields
            }else if(gettype($excluded_field) == "array"){ //dinag dag ko lang 2016-07-14 2 49 PM
               if(count($selected_fields) <= 0 && count($excluded_field) <= 0){
                   $exclude_distribution = array_values(array_unique(array_diff($exclude_distribution, $user_fields))); //ung mga selected field dapat di ma unset so //tatanggalin sa excluded_field ung mga selected_fields
               } 
            }
        }

        if (gettype($excluded_field) == "array") { //exclusion must  be a priority than the inclusion if both data has the same fieldname
            if (count($excluded_field) >= 1) {
                $exclude_distribution = array_values(array_unique(array_merge($exclude_distribution, $excluded_field)));
            }
        }

        // print_r($exclude_distribution);
        //exclusion of field distributions
        // print_r($formatted_data['computed_value']);
        // print_r($exclude_distribution);
        // foreach ($exclude_distribution as $key => $value) {
        //     unset($formatted_data['computed_value'][$value]);
        // }
        $formatted_data['__debug_'] = array(
            "visibility" => array(),
            "computed_value" => array(),
            "change_list_computed_value" => array(),
            "placeholder_formula" => array(),
            "readonly" => array(),
            "validation" => array()
        );
        $sequence_collector = array();
        $formulaDoc = new Formula();
        foreach ($post_data['_formula_list_data'] as $key => $value) {
            if (array_search($value['field_name'], $exclude_distribution) === false) {
                $formulaDoc->DataFormSource[0] = $post_data;
                //all trimmer here para mawala ung mga new lines lang ang laman
                $value['existing_formula'] = preg_replace("/\\/\\/.*\\n|\\/\\/.*$/", "", trim($value['eq_formula']) );
                $value['eq_formula'] = preg_replace("/\\/\\/.*\\n|\\/\\/.*$/", "", trim($value['eq_formula']) );
                $value['change_list_formula'] = trim($value['change_list_formula']);
                $value['placeholder_formula'] = trim($value['placeholder_formula']);
                //$ProcessedFormula = preg_replace("/\\/\\/.*\\n|\\/\\/.*$/", "", $ProcessedFormula);//pangtanggal ng comment sa formula
                if ($value['use_exist'] == false) {

                    if ($value['eq_formula']) {

                        $formulaDoc->MyFormula = $value['eq_formula'];
                        $formatted_data['__debug_']['computed_value']["" . $value['field_name']] = $formulaDoc->replaceFormulaFieldNames();
                        $GLOBALS['formula_executed_data_collector']['track_formula_list']["" . $value['field_name']] = $formatted_data['__debug_']["" . $value['field_name']];
                        $post_data["" . $value['field_name']] = $formulaDoc->evaluate();
                        $formatted_data['computed_value']["" . $value['field_name']] = $post_data["" . $value['field_name']];
                        array_push($sequence_collector, array(
                            "data_object_id" => $value['data_object_id'],
                            "field_name" => $value['field_name'],
                            "distribution_type" => "computed_value",
                            "value" => $post_data["" . $value['field_name']]
                        ));
                    }
                } else if ($value['use_exist']) {
                    if ($value['existing_formula']) {
                        $formulaDoc->MyFormula = $value['existing_formula'];
                        // array_push($GLOBALS['formula_executed_data_collector']["track_formula_list"], $formulaDoc->getProccessedFormula());
                        $formatted_data['__debug_']['computed_value']["" . $value['field_name']] = $formulaDoc->replaceFormulaFieldNames();
                        $GLOBALS['formula_executed_data_collector']['track_formula_list']["" . $value['field_name']] = $formatted_data['__debug_']["" . $value['field_name']];
                        $post_data["" . $value['field_name']] = $formulaDoc->evaluate();
                        $formatted_data['computed_value']["" . $value['field_name']] = $post_data["" . $value['field_name']];
                        array_push($sequence_collector, array(
                            "data_object_id" => $value['data_object_id'],
                            "field_name" => $value['field_name'],
                            "distribution_type" => "change_list_computed_value",
                            "value" => $post_data["" . $value['field_name']]
                        ));
                    }
                }
                if ($value['change_list_formula']) {
                    $temp = null;
                    $formulaDoc->MyFormula = $value['change_list_formula'];
                    $formatted_data['__debug_']['change_list_computed_value']["" . $value['field_name']] = $formulaDoc->replaceFormulaFieldNames();
                    $temp = $formulaDoc->evaluate();
                    $formatted_data['change_list_computed_value']["" . $value['field_name']] = $temp;
                    array_push($sequence_collector, array(
                        "data_object_id" => $value['data_object_id'],
                        "field_name" => $value['field_name'],
                        "distribution_type" => "change_list_computed_value",
                        "value" => $temp
                    ));
                }
                if ($value['placeholder_formula']) {
                    $temp = null;
                    $formulaDoc->MyFormula = $value['placeholder_formula'];
                    $formatted_data['__debug_']['placeholder_formula']["" . $value['field_name']] = $formulaDoc->replaceFormulaFieldNames();
                    $temp = $formulaDoc->evaluate();
                    $formatted_data['placeholder_formula']["" . $value['field_name']] = $temp;
                    array_push($sequence_collector, array(
                        "data_object_id" => $value['data_object_id'],
                        "field_name" => $value['field_name'],
                        "distribution_type" => "placeholder_formula",
                        "value" => $temp
                    ));
                }
                if ($value['label_formula']) {
                    $temp = null;
                    $formulaDoc->MyFormula = $value['label_formula'];
                    $formatted_data['__debug_']['label_formula']["" . $value['field_name']] = $formulaDoc->replaceFormulaFieldNames();
                    $temp = $formulaDoc->evaluate();
                    $formatted_data['label_formula']["" . $value['field_name']] = $temp;
                    array_push($sequence_collector, array(
                        "data_object_id" => $value['data_object_id'],
                        "field_name" => $value['field_name'],
                        "distribution_type" => "label_formula",
                        "value" => $temp
                    ));
                }
            }
        }
        $formatted_data['sequence_data'] = $sequence_collector;
        return $formatted_data;
    } else {
        return "FormulaList is for form events only";
    }
}

function ArraySearchBy($given_array, $search, $flag = 'single') {
    if ($flag == 'single') {
        if (array_search($search, $given_array) === false) {
            return -1;
        } else {
            return array_search($search, $given_array);
        }
    } else {
        if (gettype($search) == 'array') {
            $res = array();
            foreach ($search as $key => $value) {
                if (array_search($value, $given_array) > -1) {
                    array_push($res, array_search($value, $given_array));
                }
            }
            if (count($res) <= 0) {
                $res = -1;
            }
            return $res;
        } else {
            return array_search($search, $given_array);
        }
    }
}

function GetFileName($pathname, $extension) {
    if ($pathname) {
        $path_str = $pathname;
        //regexp = new RegExp("[a-z\\(\\)A-Z0-9_][a-z\\(\\)A-Z0-9\\s_\\.]*\\.[a-zA-Z]{3}[a-zA-Z]?(?![a-zA-Z0-9_]|\\.|\\(|\\))" , "g");
        // $matches_str = $pathname.match(regexp);
        $matches_str = explode("/", $pathname);
        if ($matches_str) {
            if (count($matches_str) >= 1) {
                if ($extension) {
                    if ($extension == "*") {
                        return $matches_str[$matches_str . length - 1];
                    } else {
                        preg_match_all("/^[a-zA-Z0-9_]*\\." . $extension . "(?=([^a-zA-Z0-9_]|$))/", $matches_str[count($matches_str) - 1], $extension_matches);
                        if ($extension_matches) {
                            return $extension_matches[0][0];
                        } else {
                            return "";
                        }
                    }
                } else {
                    return $matches_str[count($matches_str) - 1];
                }
            } else {
                return "";
            }
        } else {
            return "";
        }
    } else {
        return "";
    }
}

function DataSource($var_name, $field_name, $index) {
    $param_args = func_get_args();
    $param_length = count($param_args);


    if ($param_length == 1) {
        return $_SESSION["DataSource_" . $var_name];
    } else if ($param_length == 2) {
        if (is_array($_SESSION["DataSource_" . $var_name])) {

            $temporary = $_SESSION["DataSource_" . $var_name][$field_name];
            if (!$temporary) {
                if (!$index) {
                    $collector = array();
                    foreach ($variable as $key => $value) {
                        array_push($collector, $_SESSION["DataSource_" . $var_name][$key]);
                    }
                    return $collector;
                } else {
                    $temporary = $_SESSION["DataSource_" . $var_name][$index][$field_name];
                }
            }
            return $temporary;
        } else {
            return "";
        }
    } else if ($param_length == 3) {
        if (is_array($_SESSION["DataSource_" . $var_name])) {
            return $_SESSION["DataSource_" . $var_name][$index][$field_name];
        } else {
            return "";
        }
    } else if ($param_length == 4) {
        $index = $param_args[3];
        if (is_array($_SESSION["DataSource_" . $var_name])) {
            return $_SESSION["DataSource_" . $var_name][$index][$field_name];
        } else {
            return "";
        }
    }
}

function DataSource2($var_name, $selected_field) { // experimental //ginagawa pa lang
    $param_args = func_get_args();
    $field_key = "";
    $operator = "";
    $value = "";

    if (count($param_args) >= 3 && is_array($_SESSION["DataSource_" . $var_name])) {
        $collect_return = array();
        $temp = 1;
        $valid_param_check = count($param_args) - 2;

        if ($valid_param_check % 3 == 0) {
            $found_indexes = array();
            for ($ii = 0; $ii < count($_SESSION["DataSource_" . $var_name]); $ii++) {
                for ($i = 2; $i < count($param_args); $i+=3) {
                    if (in_array($i, $found_indexes)) {
                        continue;
                    } else {
                        $field_key = $param_args[$i];
                        $operator = $param_args[$i + 1];
                        $value = $param_args[$i + 2];


                        if ($_SESSION["DataSource_" . $var_name][$ii][$field_key] == $value) {
                            array_push($collect_return, $_SESSION["DataSource_" . $var_name][$ii][$selected_field]);
                            array_push($found_indexes, $i);
                        }
                        //ETO UNG MAY OPERATOR KASO PAANO UNG MAY MGA AND AT OR
                        // if($operator == "="){
                        //     if( $_SESSION["DataSource_".$var_name][$ii][$field_key] == $value ){
                        //         array_push($collect_return, $_SESSION["DataSource_".$var_name][$ii][$selected_field] );
                        //         array_push($found_indexes, $i);
                        //     }
                        // }else if($operator == ">="){
                        //     if( $_SESSION["DataSource_".$var_name][$ii][$field_key] == $value ){
                        //         array_push($collect_return, $_SESSION["DataSource_".$var_name][$ii][$selected_field] );
                        //         array_push($found_indexes, $i);
                        //     }
                        // }else if($operator == "<="){
                        //     if( $_SESSION["DataSource_".$var_name][$ii][$field_key] == $value ){
                        //         array_push($collect_return, $_SESSION["DataSource_".$var_name][$ii][$selected_field] );
                        //         array_push($found_indexes, $i);
                        //     }
                        // }else if($operator == "!="){
                        //     if( $_SESSION["DataSource_".$var_name][$ii][$field_key] == $value ){
                        //         array_push($collect_return, $_SESSION["DataSource_".$var_name][$ii][$selected_field] );
                        //         array_push($found_indexes, $i);
                        //     }
                        // }else if($operator == ">"){
                        //     if( $_SESSION["DataSource_".$var_name][$ii][$field_key] == $value ){
                        //         array_push($collect_return, $_SESSION["DataSource_".$var_name][$ii][$selected_field] );
                        //         array_push($found_indexes, $i);
                        //     }
                        // }else if($operator == "<"){
                        //     if( $_SESSION["DataSource_".$var_name][$ii][$field_key] == $value ){
                        //         array_push($collect_return, $_SESSION["DataSource_".$var_name][$ii][$selected_field] );
                        //         array_push($found_indexes, $i);
                        //     }
                        // }
                    }
                }
            }
        }
        return implode("<--->", $collect_return)? : "";
    } else {
        if ($selected_field && is_array($_SESSION["DataSource_" . $var_name])) {
            return $_SESSION["DataSource_" . $var_name][0][$selected_field];
        } else if (is_array($_SESSION["DataSource_" . $var_name])) {
            return $_SESSION["DataSource_" . $var_name][0];
        }
    }

    return $_SESSION["DataSource_" . $var_name]? : "";
}

function ArrayJoinBy($array_val, $glue) {
    if (gettype($array_val) == "string") {
        return "";
    }

    $array_value = [];

    $array_value = array_merge($array_value, $array_val);
    $str_val = implode($glue, $array_value);


    return $str_val;
}

function StrSplitBy($str_val, $delimiter, $index = "all") {
    $str_val_array = [];
    $str_val_array = explode($delimiter, $str_val);
    $str_val_array_index = $index;
    if ($index >= count($str_val_array)) {
        return "";
    }
    if ($index == "first") {
        $str_val_array_index = 0;
        return $str_val_array[$str_val_array_index];
    } else if ($index == "last") {
        $str_val_array_index = count($str_val_array) - 1;
        return $str_val_array[$str_val_array_index];
    } else if ($index == "all") {
        return $str_val_array;
    }
    $str_val_array_index = (int) $str_val_array_index;
    return $str_val_array[$str_val_array_index];
}

function StrFind($string_val = "", $string_search = "*", $return_val) { //compatible in php 5.4 +
    if (!$return_val) {
        $return_val = "first_index";
    }
    $escape_characters = array(
        "\\.",
        "\\^",
        "\\$",
        "\\*",
        "\\+",
        "\\-",
        "\\?",
        "\\(",
        "\\)",
        "\\[",
        "\\]",
        "\\{",
        "\\}",
        "\\\\",
        "\\|"
    );
    $given_strReg = "/" . implode($escape_characters, "|") . "/";
    $string_search = preg_replace_callback($given_strReg, function($a2) {
        return "\\" . $a2[0];
    }, $string_search);
    $string_search2 = "/" . $string_search . "/";
    $string_val2 = $string_val;
    preg_match_all($string_search2, $string_val, $stringArray, PREG_OFFSET_CAPTURE);

    //print_r($stringArray);
    //echo "\n\n";
    //print_r(json_encode($stringArray,JSON_PRETTY_PRINT));
    if ($return_val === "count") {
        return count($stringArray[0]);
    }
    if ($return_val === "first_index") {
        return $stringArray[0][0][1];
    }
    if ($return_val === "last_index") {
        return $stringArray[0][count($stringArray[0]) - 1][1];
    }
    if ($return_val === "all_index") {
        //print_r($stringArray[0]);
        return array_map(function($value) {
            return $value[1];
        }, $stringArray[0]);
    }
}

function StrReplace($string_val, $old_string, $new_string, $string_flags) {

    $string_flags_array = [];
    if (gettype($string_flags) == "string") {
        $string_flags_array = array_merge($string_flags_array, [$string_flags]);
    } else {
        $string_flags_array = array_merge($string_flags_array, $string_flags);
    }

    $old_string_array = [];
    //
    $escape_characters = [
        "\\.",
        "\\^",
        "\\$",
        "\\*",
        "\\+",
        "\\-",
        "\\?",
        "\\(",
        "\\)",
        "\\[",
        "\\]",
        "\\{",
        "\\}",
        "\\\\",
        "\\|"
    ];
    if (gettype($old_string) == "string") {
        $given_strReg = "/" . implode($escape_characters, "|") . "/";

        $old_string = preg_replace_callback($given_strReg, function($a2) {
            return "\\" . $a2[0];
        }, $old_string);
    } else {
        $old_string = array_map(function($a) use($escape_characters) {
            $given_strReg = "/" . implode($escape_characters, "|") . "/";
            return preg_replace_callback($given_strReg, function($a2) {
                return "\\" . $a2[0];
            }, $a);
        }, $old_string);
    }
    if (gettype($old_string) == "string") {
        $old_string_array = array_merge($old_string_array, [$old_string]);
    } else {
        $old_string_array = array_merge($old_string_array, $old_string);
    }

    $string_modifier = "";
    $regExString_array = [];

    if (in_array("ignore_case", $string_flags_array)) {
        $string_modifier .= "i";
    }
    $old_string_array = array_filter($old_string_array);

    for ($osa_ctr = 0; count($old_string_array) - 1 >= $osa_ctr; $osa_ctr++) {
        $temp_string = $old_string_array[$osa_ctr];

        if (in_array("whole_word", $string_flags_array)) {
            //StringReg = new RegExp("(^| )"+temp_string+"(?![0-9a-zA-Z_])",string_modifier);
            array_push($regExString_array, "(^| )" . $temp_string . "(?![0-9a-zA-Z_])");
            // new_word = string_val.replace(StringReg," "+new_string);
        } else {
            array_push($regExString_array, $temp_string);
        }
    }
    if (in_array("global", $string_flags_array)) {
        $NewRegexString = "/" . implode("|", $regExString_array) . "/";
        $new_word = preg_replace_callback($NewRegexString, function($a) use($new_string) {
            return $new_string;
        }, $string_val);
    } else {
        $NewRegexString = "/" . implode("|", $regExString_array) . "/" . $string_modifier;
        $new_word = preg_replace_callback($NewRegexString, function($a) use($new_string) {
            return $new_string;
        }, $string_val, 1);
    }

    $new_word = trim($new_word);

    return $new_word;
}

function StrTrim($string_val) {
    $trimmed_string = trim($string_val);
    return $trimmed_string;
}

function StrUpper($param_str) {
    $str_val = $param_str;
    if (gettype($str_val)) {
        $str_val = (string) $param_str;
        $str_val = strtoupper($str_val);
    }
    return $str_val;
}

function StrLower($param_str) {
    $str_val = $param_str;
    if (gettype($str_val)) {
        $str_val = (string) $param_str;
        $str_val = strtolower($str_val);
    }
    return $str_val;
}

function LoopIn() {//compatible in php 5.4 +
    $param_args = func_get_args();
    $client_variable_collector = array();
    $loop_sys_variable_collector = array();
    $temporary_cb_result = '';

    if (count($param_args) == 3) {//given_arr_val loop_content_val
        $variable_initialization = $param_args[0];
        $loopcontent = $param_args[1];
        $array_data_val = $param_args[2];
        if (is_callable($loopcontent)) {//if loop content is existing and a function
            foreach ($variable_initialization as $data_value) {//[0] is key [1] is value as variable declaration
                if (count($data_value) == 2) {
                    $client_variable_collector[$data_value[0]] = $data_value[1];
                } else {
                    $client_variable_collector[$data_value[0]] = "";
                }
            }
            $temp_counter_loop = 1;
            $loop_sys_variable_collector['_loop_len'] = count($array_data_val);
            foreach ($array_data_val as $key_index => $data_value) {
                $loop_sys_variable_collector['_index'] = $key_index;
                $loop_sys_variable_collector['_value'] = $data_value;
                $loop_sys_variable_collector['_loop_ctr'] = $temp_counter_loop;
                $temporary_cb_result = $loopcontent($loop_sys_variable_collector, $client_variable_collector);
                if (isset($temporary_cb_result['_break'])) {
                    if ($temporary_cb_result['_break'] == true) {
                        $client_variable_collector = $temporary_cb_result;
                        $temporary_cb_result['_break'] = false;
                        break;
                    }
                }
                if (gettype($temporary_cb_result) != "boolean" and gettype($temporary_cb_result) != 'NULL') {
                    $client_variable_collector = $temporary_cb_result;
                } else if (gettype($temporary_cb_result) == "boolean" and $temporary_cb_result == false) {
                    break;
                }
                $temp_counter_loop++;
            }
        }
    } else if (count($param_args) == 4) {// variable declaration, loop content, (start, end)
        $variable_initialization = $param_args[0];
        $loopcontent = $param_args[1];
        $start_val = $param_args[2];
        $end_val = $param_args[3];
        if (is_callable($loopcontent)) {//if loop content is existing and a function
            foreach ($variable_initialization as $data_value) {//[0] is key [1] is value
                if (count($data_value) == 2) {
                    $client_variable_collector[$data_value[0]] = $data_value[1];
                } else {
                    $client_variable_collector[$data_value[0]] = "";
                }
            }


            if (is_date_str($start_val) and is_date_str($end_val)) {
                $start_val = ThisDate($start_val);
                $end_val = ThisDate($end_val);
                if ($start_val <= $end_val) { //ascending loop and equal in date
                    $date_difference_end_loop = DiffDate('day', $end_val, $start_val);
                    $temporary_date_computation = "";
                    $temporary_date_computation = $start_val;
                    $temp_counter_loop = 1;
                    $loop_sys_variable_collector['_loop_len'] = $date_difference_end_loop + 1;
                    for ($ctr = 0; $ctr <= $date_difference_end_loop; $ctr++) {
                        $loop_sys_variable_collector['_index'] = $ctr;
                        $loop_sys_variable_collector['_value'] = $temporary_date_computation;
                        $loop_sys_variable_collector['_loop_ctr'] = $temp_counter_loop;
                        $temporary_cb_result = $loopcontent($loop_sys_variable_collector, $client_variable_collector);
                        if (isset($temporary_cb_result['_break'])) {
                            if ($temporary_cb_result['_break'] == true) {
                                $client_variable_collector = $temporary_cb_result;
                                $temporary_cb_result['_break'] = false;
                                break;
                            }
                        }
                        if (gettype($temporary_cb_result) != "boolean" and gettype($temporary_cb_result) != 'NULL') {
                            $client_variable_collector = $temporary_cb_result;
                        } else if (gettype($temporary_cb_result) == "boolean" and $temporary_cb_result == false) {
                            break;
                        }
                        $temporary_date_computation = ThisDate(AdjustDate('day', $temporary_date_computation, +1));
                        $temp_counter_loop++;
                    }
                } else if ($start_val > $end_val) {//descending loop in date
                    $date_difference_end_loop = DiffDate('day', $start_val, $end_val);
                    $temporary_date_computation = "";
                    $temporary_date_computation = $start_val;
                    $temp_counter_loop = $date_difference_end_loop + 1;
                    $loop_sys_variable_collector['_loop_len'] = $date_difference_end_loop + 1;
                    for ($ctr = $date_difference_end_loop; $ctr >= 0; $ctr--) {
                        $loop_sys_variable_collector['_index'] = $ctr;
                        $loop_sys_variable_collector['_value'] = $temporary_date_computation;
                        $loop_sys_variable_collector['_loop_ctr'] = $temp_counter_loop;
                        $temporary_cb_result = $loopcontent($loop_sys_variable_collector, $client_variable_collector);
                        if (isset($temporary_cb_result['_break'])) {
                            if ($temporary_cb_result['_break'] == true) {
                                $client_variable_collector = $temporary_cb_result;
                                $temporary_cb_result['_break'] = false;
                                break;
                            }
                        }
                        if (gettype($temporary_cb_result) != "boolean" and gettype($temporary_cb_result) != 'NULL') {
                            $client_variable_collector = $temporary_cb_result;
                        } else if (gettype($temporary_cb_result) == "boolean" and $temporary_cb_result == false) {
                            break;
                        }
                        $temporary_date_computation = ThisDate(AdjustDate('day', $temporary_date_computation, -1));
                        $temp_counter_loop--;
                    }
                }
            } else if (//if start_val and end_val is a number
                    (
                    (gettype($start_val) == 'integer' or gettype($start_val) == 'double') and ( gettype($end_val) == "integer" or gettype($end_val) == "double" )
                    ) or (
                    is_numeric($start_val) and is_numeric($end_val)
                    )
            ) {
                if ($start_val <= $end_val) {//ascending loop and equal
                    $temp_index_ctr = 0;
                    $temp_counter_loop = 1;
                    $loop_sys_variable_collector['_loop_len'] = ($end_val - $start_val) + 1;
                    for ($ctr = $start_val; $ctr <= $end_val; $ctr++) {
                        $loop_sys_variable_collector['_index'] = $temp_index_ctr;
                        $loop_sys_variable_collector['_value'] = $ctr;
                        $loop_sys_variable_collector['_loop_ctr'] = $temp_counter_loop;
                        $temporary_cb_result = $loopcontent($loop_sys_variable_collector, $client_variable_collector);
                        if (isset($temporary_cb_result['_break'])) {
                            if ($temporary_cb_result['_break'] == true) {
                                $client_variable_collector = $temporary_cb_result;
                                $temporary_cb_result['_break'] = false;
                                break;
                            }
                        }
                        if (gettype($temporary_cb_result) != "boolean" and gettype($temporary_cb_result) != 'NULL') {
                            $client_variable_collector = $temporary_cb_result;
                        } else if (gettype($temporary_cb_result) == "boolean" and $temporary_cb_result == false) {
                            break;
                        }
                        $temp_index_ctr++;
                        $temp_counter_loop++;
                    }
                } else if ($start_val > $end_val) {//descending loop
                    $temp_index_ctr = 0;
                    $temp_counter_loop = ( $start_val - $end_val ) + 1;
                    $loop_sys_variable_collector['_loop_len'] = ($start_val - $end_val) + 1;
                    for ($ctr = $start_val; $ctr >= $end_val; $ctr--) {
                        $loop_sys_variable_collector['_index'] = $temp_index_ctr;
                        $loop_sys_variable_collector['_value'] = $ctr;
                        $loop_sys_variable_collector['_loop_ctr'] = $temp_counter_loop;
                        $temporary_cb_result = $loopcontent($loop_sys_variable_collector, $client_variable_collector);
                        if (isset($temporary_cb_result['_break'])) {
                            if ($temporary_cb_result['_break'] == true) {
                                $client_variable_collector = $temporary_cb_result;
                                $temporary_cb_result['_break'] = false;
                                break;
                            }
                        }
                        if (gettype($temporary_cb_result) != "boolean" and gettype($temporary_cb_result) != 'NULL') {
                            $client_variable_collector = $temporary_cb_result;
                        } else if (gettype($temporary_cb_result) == "boolean" and $temporary_cb_result == false) {
                            break;
                        }
                        $temp_index_ctr++;
                        $temp_counter_loop--;
                    }
                }
            }
        }
    } else { //too many parameters
    }
    return $client_variable_collector;
}

//LOOP STRUCTURE
//LoopIn( variable declaration ,  loop content , [1,3,5,3,5] )
//OR
//LoopIn( variable declaration ,  loop content , start , end )
// LoopIn([
// 	//variable declaration
// 	['total', 0],
// 	['temp_divisor',0]
// ],function($data,$coll){
// 	//content
// 	$coll['total'] = $coll['total'] + $data['_value'];
// 	//return collected results
// 	return $coll;
// },[7,3,5,6])//values or start and end
// ['total']; //getting the total result

function UserInfo($return_field, $reference_value) {

    $db = new Database();
    $auth = getFormulaAuth();
    $redis_cache = getRedisConnection();

    $filter = "";

    if ($reference_value) {
        $reference_flag = 1;
        $filter = " WHERE user.display_name = {$db->escape($reference_value)} AND user.company_id ={$auth["company_id"]}";
    } else {
        $reference_flag = 0;
        $filter = " WHERE user.display_name = {$db->escape($auth["display_name"])} AND user.company_id ={$auth["company_id"]}";
    }

    $query = "SELECT 
		user . *,
		pos.position as position_name,
		(SELECT getUsersDepartment(user.id, user.company_id)) AS user_department,
                (SELECT getUsersGroup(user.id)) as user_group
	FROM
		tbuser user
			LEFT JOIN
		tbpositions pos ON pos.id = user.position " . $filter;

    if ($redis_cache) {
        if ($reference_flag == 1) {
            $cache_user_info = json_decode($redis_cache->get("formula_user_info_" . $reference_value . "::" . $auth["company_id"]), true);
        } else {
            $cache_user_info = json_decode($redis_cache->get("formula_user_info_" . $auth["id"]), true);
        }
    }

    if ($cache_user_info) {
        $result = $cache_user_info;
    } else {
        $result = $db->query($query, "row");
        if ($redis_cache) {
            if ($reference_flag == 1) {
                $redis_cache->set("formula_user_info_" . $reference_value . "::" . $auth["company_id"], json_encode($result));
            } else {
                $redis_cache->set("formula_user_info_" . $auth["id"], json_encode($result));
            }
        }
    }
    return $result[$return_field];
}

function is_date_str($date_pass) {
    $count_date_error = 0;
    $date_struct = date_parse($date_pass);
    if (empty($date_struct['year']) or strlen((string) $date_struct['year']) != 4) {
        $count_date_error++;
    }
    if (empty($date_struct['month'])) {
        $count_date_error++;
    }
    if (empty($date_struct['day'])) {
        $count_date_error++;
    }



    if (!empty($date_struct['warning_count'])) {
        if ($date_struct['warning_count'] >= 1) {
            $count_date_error++;
        }
    }
    if (!empty($date_struct['warnings'])) {
        if (count($date_struct['warnings']) >= 1) {
            $count_date_error++;
        }
    }
    if (!empty($date_struct['error_count'])) {
        if ($date_struct['error_count'] >= 1) {
            $count_date_error++;
        }
    }
    if (!empty($date_struct['errors'])) {
        if (count($date_struct['errors']) >= 1) {
            $count_date_error++;
        }
    }

    if ($count_date_error >= 1) {
        return false;
    } else {
        return true;
    }
}

function LookupGeoLocation($FormName = null, $GeoType = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    $GeoType = strtolower($GeoType);
    $this_get_table_form = getFormulaFormDetails($FormName);
    $q_string = 'SELECT `GeoLocationDetails` FROM `'.$this_get_table_form['form_table_name'].'` ';
    $args_where = func_get_args();
    unset($args_where[0]);
    unset($args_where[1]);
    $args_where = array_values($args_where);
    $count_where = count($args_where);

    $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $this_get_table_form['_db']);

    $field_name = "";
    $operator = "";
    $field_value = "";

    if($count_where % 3 == 0){
        $where = "WHERE ";
        for ($ctr = 0 ; $ctr < $count_where ; $ctr += 3) {
            $field_name = $args_where[$ctr];
            $operator = $args_where[$ctr+1];
            if ($tbfields_data[ $field_name ]["field_input_type"] == "Number" || $tbfields_data[ $field_name ]["field_input_type"] == "Currency") {
                $field_value = $args_where[$ctr+2];
            } else {
                $field_value = '\''.$args_where[$ctr+2].'\'';
            }
            $where .= "{$field_name} {$operator} {$field_value} AND ";
        }   
        $where = substr($where, 0, -5);
    }
    $this_record = Formula::getLookupValue("formula_lookup_geo_location", $this_get_table_form, $q_string.$where, $cache_key);
    $result = array();
    $result2 = '';
    foreach ($this_record as $key => $value) {
        $result = json_decode($value['GeoLocationDetails'],true);
        $result2 .= $result[$GeoType].'<--->';
    }
    $result2 = substr($result2, 0, -5);
    return $result2?:'';
}

/*
  Lookup external DB
  - created by Aaron Tolentino
 */
function LookupExternalWhere($ConnName, $FormName = null, $ReturnField = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    if (!$ReturnField || !$FormName) {
        return "";
    }
    $db = new Database();
    $auth = getFormulaAuth();

    $condition = " AND connection_name = {$db->escape($ConnName)}";
    $limit = " LIMIT 0, 1";
    $company_id = $auth['company_id'];

    $externalDatabase = new ExternalDatabase($db);
    $getConnection = $externalDatabase->getExternalDB($condition, $limit, "", $company_id, "array");

    if (count($getConnection) == 1) {
        $getConnection = $getConnection[0];

        $db_type = $getConnection['db_type'];

        $db_host = $getConnection['db_hostname'];
        $db_name = $getConnection['db_name'];
        $db_username = $getConnection['db_username'];
        $password = $getConnection['db_password'];

        if ($db_type == "1") { // MySQL
            echo "wala pa";
        } else if ($db_type == "2") { // MSSQL
            /*
              sample usage
              $test = LookupExternalWhere("MS SQL test connection", "test_table1", "*");

             */
            $dbMSSQL = new MSSQLDatabase($db_host, $db_name, $db_username, $password);
            if ($dbMSSQL->is_connected) {

                $rf_select = "";
                if (gettype($ReturnField) == "array") {
                    $rf_select = implode(" , ", $ReturnField);
                } else {
                    $rf_select = $ReturnField;
                }

                $getRecordStrSql = "SELECT $rf_select FROM $FormName";
                $arguments = func_get_args();
                $valid_param_check = count($arguments) - 3;
                $q_string_where .= "";
                $cache_key = $rf_select;
                if ($valid_param_check >= 1 && $valid_param_check % 3 == 0) {
                    $found_indexes = array();
                    $q_string_where = " WHERE ";
                    for ($i = 3; $i < count($arguments); $i+=3) {
                        $field_key = $arguments[$i];
                        $operator = $arguments[$i + 1];
                        $value = $arguments[$i + 2];
                        if ($i != 3) {
                            $q_string_where .=" AND ";
                        }
                        $q_string_where .= " " . $field_key . " " . $operator;

                        if (is_numeric($value) || strtoupper($operator) == "IN") {
                            $q_string_where .= " " . $value . " ";
                        } else {
                            $q_string_where .= " '" . $value . "' ";
                        }

                        $cache_key.="::" . $field_key . "::" . $operator . "::" . $value;
                    }
                }
                $getRecordStrSql .= $q_string_where . ";";
                // echo "MSSQL: ".$getRecordStrSql;

                $getRecord = Formula::getLookupValue("formula_lookup_external_" . $db_type . "_", $FormName, $getRecordStrSql, $cache_key);
//                $getRecord = $dbMSSQL->query($getRecordStrSql);
                // print_r($getRecord);
                $dbMSSQL->disconnect();
            } else {
                echo "Failed MS SQL";
            }
            //sample get tables
            // $getTables = $dbMSSQL->query("SELECT * FROM information_schema.tables","array");
            //sample get columns
            // $getColumns = $dbMSSQL->query("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='test_table1'");
            // print_r($getColumns);
        } else if ($db_type == "3") { // ORACLE
            $dbOracle = new OracleDatabase($db_host, $db_name, $db_username, $password);
            if ($dbOracle->is_connected) {
                /*
                  sample usage
                  $test = LookupExternalWhere("ORACLE test connection", "test_table_1", "*");

                 */
                $rf_select = "";
                if (gettype($ReturnField) == "array") {
                    $rf_select = "\"" . strtoupper(implode("\" , \"", $ReturnField)) . "\"";
                } else if ($ReturnField != "*") {
                    $rf_select = "\"" . strtoupper($ReturnField) . "\"";
                } else {
                    $rf_select = strtoupper($ReturnField);
                }

                $getRecordStrSql = "SELECT $rf_select FROM \"$FormName\"";
                $arguments = func_get_args();
                $valid_param_check = count($arguments) - 3;
                $q_string_where .= "";
                $cache_key = $rf_select;
                if ($valid_param_check >= 1 && $valid_param_check % 3 == 0) {
                    $found_indexes = array();
                    $q_string_where = " WHERE ";
                    for ($i = 3; $i < count($arguments); $i+=3) {
                        $field_key = $arguments[$i];
                        $operator = $arguments[$i + 1];
                        $value = $arguments[$i + 2];

                        if ($i != 3) {
                            $q_string_where .=" AND ";
                        }

                        $q_string_where .= " \"" . strtoupper($field_key) . "\" " . $operator;
                        if (is_numeric($value) || strtoupper($operator) == "IN") {
                            $q_string_where .= " " . $value . " ";
                        } else {
                            $q_string_where .= " '" . $value . "' ";
                        }

                        $cache_key.="::" . $field_key . "::" . $operator . "::" . $value;
                    }
                }

                $getRecord = Formula::getLookupValue("formula_lookup_external_" . $db_type . "_", $FormName, $getRecordStrSql, $cache_key);
//                $getRecordStrSql .= $q_string_where;
                // echo "ORACLE: ".$getRecordStrSql;
                $getRecord = $dbOracle->query($getRecordStrSql);
                // print_r($getRecord);
                $dbOracle->disConnect();
            } else {
                print_r($e);
                echo "Failed Oracle";
            }
            //sample get table
            // $getTables = $dbOracle->query("SELECT TABLE_NAME FROM user_tables ORDER BY table_name DESC");
            //sample get columns
            // $getColumns = $dbOracle->query("SELECT column_name FROM USER_TAB_COLUMNS WHERE table_name = 'test_table_1'");
            // print_r($getColumns);
        }
    } else {
        echo "failed";
    }

//    var_dump($getRecord);
    $ressss2 = array_values(array_map(function($a) use($ReturnField) { //NEW
                // var_dump($a);
                // echo "<br/>===========================<br/>";
                // var_dump($ReturnField);
                if (gettype($ReturnField) == "array") {
                    $array_collector = array();
                    foreach ($ReturnField as $key => $value) {
                        $array_collector[$value] = $a[$value];
                    }
                    return $array_collector;
                } else if ($ReturnField == "*") {
                    return $a;
                } else {
                    return $a[$ReturnField];
                }
            }, $getRecord));
    return $ressss2? : array();
}

class GetDataInnerSystemArrayAccess implements ArrayAccess {

    private $container = array();

    public function __construct() {
        // $this->container = array(
        //     "one"   => 1,
        //     "two"   => 2,
        //     "three" => 3,
        // );
    }

    public function __toString() {
        return ""; //"[ Inline Access ]"
    }

    public function __invoke() {
        return ""; //"[ Execution Access ]"
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset) {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

}

function GetData($field_name = "", $selection_scope = "") {
    //a function that is like a javascript function that returns a valueOf primitive value
    //in javascript primitive value is the term in php magic method is the term
    //$existing_arguments = func_get_args ();
    $obj = new GetDataInnerSystemArrayAccess;

    //var_dump(isset($obj["two"]));
    //var_dump($obj["two"]);
    //unset($obj["two"]);
    //var_dump(isset($obj["two"]));
    //$obj["two"] = "A value";//push key value pair
    //var_dump($obj["two"]);
    //$obj[] = 'Append 1'; //push1
    //$obj[] = 'Append 2'; //push2
    //$obj[] = 'Append 3'; //push3


    $obj["Average"] = "test AVERAGE";
    $obj["Total"] = "test TOTAL";
    $obj["Count"] = "test COUNT";
    $obj["Array"] = "test ARRAY";

    return $obj;
}

//experimental
function array2() {
    $existing_arguments = func_get_args();
    $obj = new GetDataInnerSystemArrayAccess;
    foreach ($existing_arguments as $outer_val) {
        if (gettype($outer_val) == "array") {
            foreach ($outer_val as $each_value => $inner_val) {
                $obj[$each_value] = $inner_val;
            }
        } else {
            $obj[] = $outer_val;
        }
    }
    return $obj;
}

function Head($string_user) {
    if ($string_user != "") {
        $db = new Database();
        $auth = getFormulaAuth();
//        print_r($GLOBALS["auth"]);
        //multiple value field
        $field_value_arr = explode("|", $string_user);
        $return_value = array();

        foreach ($field_value_arr as $string_user_value) {
            $referenceUsers = functions::getUsers("WHERE display_name={$db->escape($string_user_value)} AND company_id={$auth["company_id"]}")[0];
            $retun_val = functions::getDepartmentHeadAssistantHead(
                            array(
                                "user_department_position_level" => $referenceUsers->department_position_level->id,
                                "processorType" => "1",
                                "processor_department" => $referenceUsers->department_id)
            );

            array_push($return_value, $retun_val);
        }

        return implode("|", $return_value);
    } else {
        return "";
    }
}

function AssistantHead($string_user) {
    if ($string_user != "") {
        $db = new Database();
        $auth = getFormulaAuth();
        //multiple value field
        $field_value_arr = explode("|", $string_user);
        $return_value = array();

        foreach ($field_value_arr as $string_user_value) {
            $referenceUsers = functions::getUsers("WHERE display_name={$db->escape($string_user_value)} AND company_id={$auth["company_id"]}")[0];
            $retun_val = functions::getDepartmentHeadAssistantHead(
                            array(
                                "user_department_position_level" => $referenceUsers->department_position_level->id,
                                "processorType" => "2",
                                "processor_department" => $referenceUsers->department_id)
            );

            array_push($return_value, $retun_val);
        }

        return implode("|", $return_value);
    } else {
        return "";
    }
}

function AdjustDate($type, $date, $added_value = '') {
    if (!is_date_str($date)) {
        $date = date('Y-m-d H:i:s', $date);
    }
    $value_add = (float) $added_value;
    switch ($type) {
        case 'day':
            $date = date("Y-m-d H:i", strtotime(date("Y-m-d H:i", strtotime($date)) . " " . $value_add . " day"));
            break;
        case 'month':
            $date = date("Y-m-d H:i", strtotime(date("Y-m-d H:i", strtotime($date)) . " " . $value_add . " month"));
            break;
        case 'year':
            $date = date("Y-m-d H:i", strtotime(date("Y-m-d H:i", strtotime($date)) . " " . $value_add . " year"));
            break;
        case 'minute':
            $date = date("Y-m-d H:i", strtotime(date("Y-m-d H:i", strtotime($date)) . " " . $value_add . " minute"));
            break;
        case 'hour':
            $date = date("Y-m-d H:i", strtotime(date("Y-m-d H:i", strtotime($date)) . " " . $value_add . " hour"));
            break;
        case 'week':
            $date = date("Y-m-d H:i", strtotime(date("Y-m-d H:i", strtotime($date)) . " " . $value_add . " week"));
            break;
    }
    return $date;
}

function CountDayIf($start_date, $end_date) {
    if ($start_date == "" || $end_date == "") {
        return false;
    }
    $args = func_get_args();
    if (!is_date_str($start_date)) {
        $start_date = date('Y-m-d', $start_date);
    }
    if (!is_date_str($end_date)) {
        $end_date = date('Y-m-d', $end_date);
    }
    $counted_if_days = 0;


    if (isset($args[2])) {
        $days_difference = DiffDate('day', $end_date, $start_date);
        for ($diff_ctr = 0; $diff_ctr <= $days_difference; $diff_ctr++) {
            if ($diff_ctr == 0) {
                $adjusted_day = FormatDate(AdjustDate('day', $start_date, 0), 'l');
                $start_date = FormatDate(AdjustDate('day', $start_date, 0), 'Y-m-d');
            } else {
                $adjusted_day = FormatDate(AdjustDate('day', $start_date, 1), 'l');
                $start_date = FormatDate(AdjustDate('day', $start_date, 1), 'Y-m-d');
            }

            for ($day_names_ctr = 0; $day_names_ctr < count($args); $day_names_ctr++) {
                $param_day = $args[$day_names_ctr];
                if ($adjusted_day == $param_day && $day_names_ctr >= 2) {
                    $counted_if_days++;
                }
            }
        }
        return $counted_if_days;
    } else {
        return $counted_if_days;
    }
}

function GetMonth($str_date) {
    $given_date_ms_time = strtotime($str_date);
    $given_date_str = $str_date;
    if (is_date_str($given_date_str) == false) {
        return "";
    } else {
        return (int) (FormatDate($given_date_str, 'm'));
    }
}

function FormatDate($passed_date, $format = 'Y-m-d H:i') {
    if (!is_date_str($passed_date)) {
        $passed_date = date('Y-m-d H:i:s', $passed_date);
    }
    return date($format, strtotime($passed_date));
}

function DiffDate($type, $date1, $date2) {
    if (!is_date_str($date1)) {
        $date1 = date('Y-m-d H:i:s', $date1);
    }
    if (!is_date_str($date2)) {
        $date2 = date('Y-m-d H:i:s', $date2);
    }
    switch ($type) {
        case 'day':
            $now = strtotime($date1); // or your date as well
            $your_date = strtotime($date2);
            $datediff = $now - $your_date;
            $ret_val = floor($datediff / (60 * 60 * 24));
            break;
        case 'month':

            $date1 = strtotime($date1); // or your date as well
            $date2 = strtotime($date2);
            $d1Y = (int) date('Y', $date1);
            $d2Y = (int) date('Y', $date2);
            $d1M = (int) date('m', $date1);
            $d2M = (int) date('m', $date2);

            $ret_val = (($d1M + 12 * $d1Y) - ($d2M + 12 * $d2Y));

            //$datediff = $now - $your_date;
            //$years = floor($datediff / (365 * 60 * 60 * 24));
            //$months = floor(($datediff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
            //$days = floor(($datediff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));
            //$ret_val = $months;
            break;
        case 'year':
            $now = strtotime($date1); // or your date as well
            $your_date = strtotime($date2);
            $datediff = $now - $your_date;

            $years = floor($datediff / (365 * 60 * 60 * 24));
            $months = floor(($datediff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
            $days = floor(($datediff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

            $ret_val = $years;
            break;
        case 'minute':
            $to_time = strtotime($date1);
            $from_time = strtotime($date2);
            $ret_val = round(($to_time - $from_time) / 60, 2);
            break;
        case 'hour':
            $hourdiff = round((strtotime($date1) - strtotime($date2)) / 3600, 7);
            $ret_val = $hourdiff;
            break;
        case 'week':

            break;
    }
    return $ret_val;
}

function LookupGetMax($FormName = null, $ReturnField = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";

    $arguments = func_get_args();
//if (empty($WhereFilterName)) {
//} else {
    $db = new Database();
    $this_get_table_form = getFormulaFormDetails($FormName);

// print_r($this_get_table_form);
    $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);
    $q_string = "SELECT MAX(`$ReturnField`) AS MaxVal FROM `" . $this_get_table_form["form_table_name"] . "`";
// echo $q_string;
    $Field_Name_Counter = 2;
    $Field_Operator_Counter = 3;
    $Filter_Value_Counter = 4;
    $q_string_where = "";
    $cache_key = $ReturnField;
    if (isset($arguments[2])) {
        $q_string_where .= " WHERE ";
        foreach ($arguments as $key => $value) {
// echo "BT".$key;
            $cache_key.="::" . $arguments[$key];
            if ($key >= 2 and $key % $Field_Name_Counter == 0) {
                $q_string_where .= " `" . $arguments[$key] . "` ";
                $Field_Name_Counter = $Field_Name_Counter + 3;
            } else if ($key >= 3 and $key % $Field_Operator_Counter == 0) {
                $q_string_where .= " " . $arguments[$key] . " ";
                $Field_Operator_Counter = $Field_Operator_Counter + 3;
            } else if ($key >= 4 and $key % $Filter_Value_Counter == 0) {
                if ($tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Number" || $tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Currency") {
                    $q_string_where .= " " . $arguments[$key] . " ";
                } else {
                    $q_string_where .= " '" . $arguments[$key] . "' ";
                }
                $Filter_Value_Counter = $Filter_Value_Counter + 3;
                if ($key == (count($arguments) - 1)) {
                    $q_string_where .= " ; ";
                } else {
                    $q_string_where .= " AND ";
                }
            }
        }
        $q_string .= $q_string_where;
    }

// echo $q_string;
    $this_record = Formula::getLookupValue("formula_lookup_get_max", $this_get_table_form, $q_string, $cache_key);

// print_r($this_record);
    array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
        "form_id" => $this_get_table_form['id'],
        "where" => $q_string_where,
        "function_name" => "LookupGetMax",
        "whole_query" => $q_string
    ));
    return $this_record[0]["MaxVal"];
// return $q_string;
//}
}

function LookupGetMin($FormName = null, $ReturnField = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";
    $arguments = func_get_args();
//if (empty($WhereFilterName)) {
//} else {
    $db = new Database();
    $this_get_table_form = getFormulaFormDetails($FormName);

// print_r($this_get_table_form);
    $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);
    $q_string = "SELECT MIN(`$ReturnField`) AS MinVal FROM `" . $this_get_table_form["form_table_name"] . "`";
// echo $q_string;
    $Field_Name_Counter = 2;
    $Field_Operator_Counter = 3;
    $Filter_Value_Counter = 4;
    $q_string_where = "";
    $cache_key = $ReturnField;
    if (isset($arguments[2])) {
        $q_string_where .= " WHERE ";
        foreach ($arguments as $key => $value) {
// echo "BT".$key;
            $cache_key.="::" . $arguments[$key];
            if ($key >= 2 and $key % $Field_Name_Counter == 0) {
                $q_string_where .= " `" . $arguments[$key] . "` ";
                $Field_Name_Counter = $Field_Name_Counter + 3;
            } else if ($key >= 3 and $key % $Field_Operator_Counter == 0) {
                $q_string_where .= " " . $arguments[$key] . " ";
                $Field_Operator_Counter = $Field_Operator_Counter + 3;
            } else if ($key >= 4 and $key % $Filter_Value_Counter == 0) {
                if ($tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Currency" || $tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Number") {
                    $q_string_where .= " " . $arguments[$key] . " ";
                } else {
                    $q_string_where .= " '" . $arguments[$key] . "' ";
                }
                $Filter_Value_Counter = $Filter_Value_Counter + 3;
                if ($key == (count($arguments) - 1)) {
                    $q_string_where .= " ; ";
                } else {
                    $q_string_where .= " AND ";
                }
            }
        }
        $q_string .= $q_string_where;
    }

// echo $q_string;
    $this_record = Formula::getLookupValue("formula_lookup_get_min", $this_get_table_form, $q_string, $cache_key);

// print_r($this_record);
    array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
        "form_id" => $this_get_table_form['id'],
        "where" => $q_string_where,
        "function_name" => "LookupGetMin",
        "whole_query" => $q_string
    ));
    return $this_record[0]["MinVal"];
// return $q_string;
//}
}

function LookupSDev($FormName = null, $ReturnField = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {//or operator
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";
    $arguments = func_get_args();
    //if (empty($WhereFilterName)) {
    //} else {
    $db = new Database();
    $this_get_table_form = getFormulaFormDetails($FormName);
//    $q_get_table_form_name = "SELECT `id`, `form_table_name` FROM `tb_workspace` WHERE is_delete = 0 AND `form_name` LIKE '" . $FormName . "' " . $companyFilter;
// print_r($q_get_table_form_name);
//    $this_get_table_form = $db->query($q_get_table_form_name, "row");
    // print_r($this_get_table_form);
    $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);
    $q_string = "SELECT STDDEV_SAMP(`$ReturnField`) AS SDVal FROM `" . $this_get_table_form["form_table_name"] . "`";
    // echo $q_string;
    $Field_Name_Counter = 2;
    $Field_Operator_Counter = 3;
    $Filter_Value_Counter = 4;
    $q_string_where = "";
    $cache_key = $ReturnField;
    if (isset($arguments[2])) {
        $q_string_where .= " WHERE ";
        foreach ($arguments as $key => $value) {
            // echo "BT".$key;
            $cache_key .= "::" . $arguments[$key];
            if ($key >= 2 and $key % $Field_Name_Counter == 0) {
                $q_string_where .= " `" . $arguments[$key] . "` ";
                $Field_Name_Counter = $Field_Name_Counter + 3;
            } else if ($key >= 3 and $key % $Field_Operator_Counter == 0) {
                $q_string_where .= " " . $arguments[$key] . " ";
                $Field_Operator_Counter = $Field_Operator_Counter + 3;
            } else if ($key >= 4 and $key % $Filter_Value_Counter == 0) {
                if ($tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Currency" || $tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Number") {
                    $q_string_where .= " " . $arguments[$key] . " ";
                } else {
                    $q_string_where .= " '" . $arguments[$key] . "' ";
                }
                $Filter_Value_Counter = $Filter_Value_Counter + 3;
                if ($key == (count($arguments) - 1)) {
                    $q_string_where .= " ; ";
                } else {
                    $q_string_where .= " AND ";
                }
            }
        }
        $q_string .= $q_string_where;
    }

    // echo $q_string;
//    $this_record = $db->query($q_string, "array");
    // print_r($this_record);

    $this_record = Formula::getLookupValue("formula_lookup_sdev", $this_get_table_form, $q_string, $cache_key);

    array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
        "form_id" => $this_get_table_form['id'],
        "where" => $q_string_where,
        "function_name" => "LookupSDev",
        "whole_query" => $q_string
    ));
    return $this_record[0]["SDVal"];
    // return $q_string;
    //}
}

function LookupSDev2($FormName = null, $ReturnField = null, $logical_operator = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";
    $arguments = func_get_args();
    //if (empty($WhereFilterName)) {
    //} else {
    $db = new Database();
    $this_get_table_form = getFormulaFormDetails($FormName);
    $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);
    $q_string = "SELECT STDDEV_SAMP(`$ReturnField`) AS SDVal FROM `" . $this_get_table_form["form_table_name"] . "`";
    // echo $q_string;
    $Field_Name_Counter = 3;
    $Field_Operator_Counter = 4;
    $Filter_Value_Counter = 5;
    $cache_key = $ReturnField;
    if (isset($arguments[3])) {
        $q_string .= " WHERE ";
        foreach ($arguments as $key => $value) {
            // echo "BT".$key;
            $cache_key.="::" . $arguments[$key];
            if ($key >= 3 and $key % $Field_Name_Counter == 0) {
                $q_string .= " `" . $arguments[$key] . "` ";
                $Field_Name_Counter = $Field_Name_Counter + 3;
            } else if ($key >= 4 and $key % $Field_Operator_Counter == 0) {
                $q_string .= " " . $arguments[$key] . " ";
                $Field_Operator_Counter = $Field_Operator_Counter + 3;
            } else if ($key >= 5 and $key % $Filter_Value_Counter == 0) {
                if ($tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Number" || $tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Currency") {
                    $q_string_where .= " " . $arguments[$key] . " ";
                } else {
                    $q_string_where .= " '" . $arguments[$key] . "' ";
                }
                $Filter_Value_Counter = $Filter_Value_Counter + 3;
                if ($key == (count($arguments) - 1)) {
                    $q_string .= " ; ";
                } else {
                    $q_string .= "  " . $logical_operator . " ";
                }
            }
        }
    }

    $this_record = Formula::getLookupValue("formula_lookup_sdev2", $this_get_table_form, $q_string, $cache_key);
    // print_r($this_record);
    return $this_record[0]["SDVal"];
    // return $q_string;
    //}
}

function LookupAVG($FormName = null, $ReturnField = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";
    $arguments = func_get_args();
    // if (empty($WhereFilterName)) {
    // } else {
    $db = new Database();
    $this_get_table_form = getFormulaFormDetails($FormName);
    $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);
    $q_string = "SELECT AVG(`$ReturnField`) AS Average FROM `" . $this_get_table_form["form_table_name"] . "`";
    // echo $q_string;
    $Field_Name_Counter = 2;
    $Field_Operator_Counter = 3;
    $Filter_Value_Counter = 4;
    $q_string_where = "";
    $cache_key = $ReturnField;

    if (isset($arguments[2])) {
        $q_string_where .= " WHERE ";
        foreach ($arguments as $key => $value) {
            // echo "BT".$key;
            $cache_key.="::" . $arguments[$key];
            if ($key >= 2 and $key % $Field_Name_Counter == 0) {
                $q_string_where .= " `" . $arguments[$key] . "` ";
                $Field_Name_Counter = $Field_Name_Counter + 3;
            } else if ($key >= 3 and $key % $Field_Operator_Counter == 0) {
                $q_string_where .= " " . $arguments[$key] . " ";
                $Field_Operator_Counter = $Field_Operator_Counter + 3;
            } else if ($key >= 4 and $key % $Filter_Value_Counter == 0) {
                if ($tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Currency" || $tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Number") {
                    $q_string_where .= " " . $arguments[$key] . " ";
                } else {
                    $q_string_where .= " '" . $arguments[$key] . "' ";
                }
                $Filter_Value_Counter = $Filter_Value_Counter + 3;
                if ($key == (count($arguments) - 1)) {
                    $q_string_where .= " ; ";
                } else {
                    $q_string_where .= " AND ";
                }
            }
        }
        $q_string .= $q_string_where;
    }

    $this_record = Formula::getLookupValue("formula_lookup_avg", $this_get_table_form, $q_string, $cache_key);

    array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
        "form_id" => $this_get_table_form['id'],
        "where" => $q_string_where,
        "function_name" => "LookupAVG",
        "whole_query" => $q_string
    ));
    return $this_record[0]["Average"];
    // }
}
function LookupCountIf($FormName = null, $ReturnField = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";

    $arguments = func_get_args();
    // if (empty($WhereFilterName)) {
    // } else {
    $db = new Database();
    $this_get_table_form = getFormulaFormDetails($FormName);
    $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);
    $q_string = "SELECT COUNT(`$ReturnField`) AS Counted FROM `" . $this_get_table_form["form_table_name"] . "`";
    // echo $q_string;
    $Field_Name_Counter = 2;
    $Field_Operator_Counter = 3;
    $Filter_Value_Counter = 4;
    $q_string_where = "";
    $cache_key = $ReturnField;
    if (isset($arguments[2])) {
        $q_string_where .= " WHERE ";
        foreach ($arguments as $key => $value) {
            // echo "BT".$key;
            $cache_key.="::" . $arguments[$key];
            if ($key >= 2 and $key % $Field_Name_Counter == 0) {
                $q_string_where .= " `" . $arguments[$key] . "` ";
                $Field_Name_Counter = $Field_Name_Counter + 3;
            } else if ($key >= 3 and $key % $Field_Operator_Counter == 0) {
                $q_string_where .= " " . $arguments[$key] . " ";
                $Field_Operator_Counter = $Field_Operator_Counter + 3;
            } else if ($key >= 4 and $key % $Filter_Value_Counter == 0) {
                if ($tbfields_data["" . $arguments[$key]]["field_input_type"] == "Number" || $tbfields_data["" . $arguments[$key]]["field_input_type"] == "Currency") {
                    $q_string_where .= " " . $arguments[$key] . " ";
                } else {
                    $q_string_where .= " '" . $arguments[$key] . "' ";
                }
                $Filter_Value_Counter = $Filter_Value_Counter + 3;
                if ($key == (count($arguments) - 1)) {
                    $q_string_where .= " ; ";
                } else {
                    $q_string_where .= " AND ";
                }
            }
        }
        $q_string .= $q_string_where;
    }

    $this_record = Formula::getLookupValue("formula_lookup_count_if", $this_get_table_form, $q_string, $cache_key);
    array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
        "form_id" => $this_get_table_form['id'],
        "where" => $q_string_where,
        "function_name" => "LookupCountIf",
        "whole_query" => $q_string
    ));
    return $this_record[0]["Counted"];
    // }
}

function LookupAuthInfo($ReturnField = null, $WhereFilterName = null, $WhereSearchValue = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";
    if ($ReturnField == null || $WhereFilterName == null) {
        return "";
    } else {
        $db = new Database();
        $redis_cache = getRedisConnection();

        if ($redis_cache) {
            $cache_auth_info = json_decode($redis_cache->get("lookup_auth_info_" . $ReturnField . "::" . $WhereFilterName . "::" . $WhereSearchValue . "::" . $auth["company_id"]), true);
        }

        if ($cache_auth_info) {
            $this_get_result = $cache_auth_info;
        } else {
            $q_str_get_result = "SELECT `" . $ReturnField . "` FROM `tbuser` WHERE `" . $WhereFilterName . "` = '" . $WhereSearchValue . "' " . $companyFilter;
            $this_get_result = $db->query($q_str_get_result, "row");

            if ($redis_cache) {
                $redis_cache->set("lookup_auth_info_" . $ReturnField . "::" . $WhereFilterName . "::" . $WhereSearchValue . "::" . $auth["company_id"], json_encode($this_get_result));
            }
        }

        return $this_get_result[$ReturnField];
    }


    // // print_r($q_get_table_form_name);
    // $this_get_table_form = $db->query($q_get_table_form_name, "row");
    // // print_r($this_get_table_form);
    // $q_string = "SELECT * FROM `" . $this_get_table_form["form_table_name"] . "`";
    // // echo $q_string;
    // $Field_Name_Counter = 2;
    // $Field_Operator_Counter = 3;
    // $Filter_Value_Counter = 4;
    // if(isset($arguments[2])){
    //     $q_string .= " WHERE ";
    //     foreach ($arguments as $key => $value) {
    //         // echo "BT".$key;
    //         if($key >= 2 and $key % $Field_Name_Counter == 0){
    //             $q_string .= " `".$arguments[$key]."` ";
    //             $Field_Name_Counter = $Field_Name_Counter + 3;
    //         }else if($key >= 3 and $key % $Field_Operator_Counter == 0){
    //             $q_string .= " ".$arguments[$key]." ";
    //             $Field_Operator_Counter = $Field_Operator_Counter + 3;
    //         }else if($key >= 4 and $key % $Filter_Value_Counter == 0){
    //             if(is_numeric($arguments[$key])){
    //                 $q_string .= " ".$arguments[$key]." ";
    //             }else{
    //                 $q_string .= " '".$arguments[$key]."' ";
    //             }
    //             $Filter_Value_Counter = $Filter_Value_Counter + 3;
    //             if($key == (count($arguments)-1) ){
    //                 $q_string .= " ; ";
    //             }else{
    //                 $q_string .= " AND ";
    //             }
    //         }
    //     }
    // }
    // // echo $q_string;
    // $this_record = $db->query($q_string, "array");
    // // print_r($this_record);
    // return $this_record[0][$ReturnField];
}

function GetAuth($key_entry) {
    $key_entries = array("FormAuthID", "FormAuthEmail", "CurrentUser");
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";

    if (array_search($key_entry, $key_entries) >= 0) {
        if ($key_entry == "FormAuthID") {
            return $auth['id'];
        } else if ($key_entry == "FormAuthEmail") {
            return $auth['email'];
        } else if ($key_entry == "CurrentUser") {
            return StrConcat($auth['display_name']/* $auth['first_name'], " ", $auth['last_name'] */);
        } else {
            return "";
        }
        // if(array_key_exists($key_entry, $auth)){
        //     return $auth[$key_entry];
        // }else{
        //     return "";
        // }
    } else {
        return "";
    }
}

function Total($FormName = null, $ReturnField = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";

    $arguments = func_get_args();
    //if (empty($WhereFilterName)) {
    //} else {
    $db = new Database();
    $this_get_table_form = getFormulaFormDetails($FormName);
    $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);
    $q_string = "SELECT SUM(`$ReturnField`) AS Total FROM `" . $this_get_table_form["form_table_name"] . "`";
    // echo $q_string;
    $Field_Name_Counter = 2;
    $Field_Operator_Counter = 3;
    $Filter_Value_Counter = 4;
    $q_string_where = "";
    $cache_key = $ReturnField;

    if (isset($arguments[2])) {
        $q_string_where .= " WHERE ";
        foreach ($arguments as $key => $value) {
            // echo "BT".$key;
            $cache_key .= "::" . $arguments[$key];
            if ($key >= 2 and $key % $Field_Name_Counter == 0) {
                $q_string_where .= " `" . $arguments[$key] . "` ";
                $Field_Name_Counter = $Field_Name_Counter + 3;
            } else if ($key >= 3 and $key % $Field_Operator_Counter == 0) {
                $q_string_where .= " " . $arguments[$key] . " ";
                $Field_Operator_Counter = $Field_Operator_Counter + 3;
            } else if ($key >= 4 and $key % $Filter_Value_Counter == 0) {
                if ($tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Currency" || $tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Number" || $arguments[$key-1] == "IN") {
                    $q_string_where .= " " . $arguments[$key] . " ";
                } else {
                    $q_string_where .= " '" . $arguments[$key] . "' ";
                }
                $Filter_Value_Counter = $Filter_Value_Counter + 3;
                if ($key == (count($arguments) - 1)) {
                    $q_string_where .= " ; ";
                } else {
                    $q_string_where .= " AND ";
                }
            }
        }
        $q_string .= $q_string_where;
    }

    //echo '</br>\n'.$q_string;
    $this_record = Formula::getLookupValue("formula_lookup_total", $this_get_table_form, $q_string, $cache_key);
    // print_r($this_record);
    array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
        "form_id" => $this_get_table_form['id'],
        "where" => $q_string_where,
        "function_name" => "Total",
        "whole_query" => $q_string
    ));
    return (float) $this_record[0]["Total"];
    //}
}

function ArrayConcat() {
    $parameters = func_get_args();
    $collector = array();
    foreach ($parameters as $key => $value) {
        $collector = array_merge($collector, $value);
    }
    return $collector;
}

function LookupWhere($FormName = null, $ReturnField = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";

    $arguments = func_get_args();
    if (empty($WhereFilterName)) {
        
    } else {
        $db = new Database();
        $this_get_table_form = getFormulaFormDetails($FormName);
        $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);
        $q_string = "SELECT * FROM `" . $this_get_table_form["form_table_name"] . "`";
        // echo $q_string;
        $Field_Name_Counter = 2;
        $Field_Operator_Counter = 3;
        $Filter_Value_Counter = 4;
        $q_string_where = "";
        $cache_key = "*";
        if (isset($arguments[2])) {
            $q_string_where .= " WHERE ";
            $search = " (NOT EXISTS(SELECT * FROM tbtrash_bin WHERE record_id = " . $this_get_table_form["form_table_name"] . ".id 
                                            AND form_id = " . $this_get_table_form["id"] . " 
                                            AND table_name='" . $this_get_table_form["form_table_name"] . "')) AND ";
            $q_string_where .= $search;
            foreach ($arguments as $key => $value) {
                // echo "BT".$key;
                $cache_key .= "::" . $arguments[$key];
                if ($key >= 2 and $key % $Field_Name_Counter == 0) {
                    $q_string_where .= " `" . $arguments[$key] . "` ";
                    $Field_Name_Counter = $Field_Name_Counter + 3;
                } else if ($key >= 3 and $key % $Field_Operator_Counter == 0) {
                    $q_string_where .= " " . $arguments[$key] . " ";
                    $Field_Operator_Counter = $Field_Operator_Counter + 3;
                } else if ($key >= 4 and $key % $Filter_Value_Counter == 0) {
                    if ($tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Number" || $tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Currency") {
                        $q_string_where .= " " . (!is_numeric($arguments[$key])?0:$arguments[$key]) . " ";
                    } else {
                        $q_string_where .= " '" . mysql_escape_string($arguments[$key]) . "' "; // mysql_real_escape_string needs connection to db , if no connection the return of the function is blank or false
                    }
                    $Filter_Value_Counter = $Filter_Value_Counter + 3;
                    if ($key == (count($arguments) - 1)) {
                        $q_string_where .= " ; ";
                    } else {
                        $q_string_where .= " AND ";
                    }
                }
            }
        }

        $q_string .= $q_string_where;

        $this_record = Formula::getLookupValue("formula_lookup_where", $this_get_table_form, $q_string, $cache_key);

        array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
            "form_id" => $this_get_table_form['id'],
            "where" => $q_string_where,
            "function_name" => "LookupWhere",
            "whole_query" => $q_string
        ));
        return $this_record[0][$ReturnField];
    }
}

function LookupWhereArray($FormName = null, $ReturnField = null, $WhereFilterName = null, $Operator = null, $WhereSearchName = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";


    $arguments = func_get_args();
    if ($WhereFilterName_xxx) {
        
    } else {
        $db = new Database();
        $this_get_table_form = getFormulaFormDetails($FormName);
        $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);

        $q_string = "SELECT * FROM `" . $this_get_table_form["form_table_name"] . "`";
        // echo $q_string;
        $valid_param_check = count($arguments) - 2;
        $cache_key = "*";
        if ($valid_param_check >= 1 && $valid_param_check % 3 == 0) {
            $found_indexes = array();
            $q_string_where = " WHERE ";
            $search = " (NOT EXISTS(SELECT * FROM tbtrash_bin WHERE record_id = " . $this_get_table_form["form_table_name"] . ".id 
                                            AND form_id = " . $this_get_table_form["id"] . " 
                                            AND table_name='" . $this_get_table_form["form_table_name"] . "')) AND ";
            $q_string_where .= $search;
            for ($i = 2; $i < count($arguments); $i+=3) {
                $field_key = $arguments[$i];
                $operator = $arguments[$i + 1];
                $value = $arguments[$i + 2];

                if ($i > 2) {
                    $q_string_where .= " AND ";
                }
                $q_string_where .= " `" . $field_key . "` " . $operator;
                if (($tbfields_data["" . $field_key]["field_input_type"] == "Number" || $tbfields_data["" . $field_key]["field_input_type"] == "Currency") || strtoupper($operator) == "IN") {
                    $q_string_where .= " " . $value . " ";
                } else {
                    $q_string_where .= " '" . $value . "' ";
                }

                $cache_key.="::" . $field_key . "::" . $operator . "::" . $value;
            }
            $q_string .= $q_string_where;
        }
        $this_record = Formula::getLookupValue("formula_lookup_where_array", $this_get_table_form, $q_string, $cache_key);
        array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
            "form_id" => $this_get_table_form['id'],
            "where" => $q_string_where,
            "function_name" => "LookupWhereArray",
            "whole_query" => $q_string
        ));
        // $ressss = array_values(array_map(function($a) use($ReturnField) { //OLD
        // // var_dump($a);
        // // echo "<br/>===========================<br/>";
        // // var_dump($ReturnField);
        // if (gettype($ReturnField) == "array") {
        // return ( array_flip(
        // array_intersect(array_flip($a), $ReturnField)
        // ) );
        // } else if ($ReturnField == "*") {
        // return $a;
        // } else {
        // return $a[$ReturnField];
        // }
        // }, $this_record));
        $ressss2 = array_values(array_map(function($a) use($ReturnField) { //NEW
                    // var_dump($a);
                    // echo "<br/>===========================<br/>";
                    // var_dump($ReturnField);
                    if (gettype($ReturnField) == "array") {
                        $array_collector = array();
                        foreach ($ReturnField as $key => $value) {
                            $array_collector[$value] = $a[$value];
                        }
                        return $array_collector;
                    } else if ($ReturnField == "*") {
                        return $a;
                    } else {
                        return $a[$ReturnField];
                    }
                }, $this_record));
        return $ressss2;
        //return array_merge(array("BROOMSKI1"=>$this_record, "BROOMSKI2"=>$ReturnField, "RESULTA"=>json_encode($ressss2,JSON_PRETTY_PRINT) , "RESULTA2"=>json_encode($ressss,JSON_PRETTY_PRINT)),$ressss2);
    }
}

function Lookup($FormName = null, $ReturnField = null, $FieldName = null, $Filter = null) {
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";
    if (empty($Filter) && gettype($Filter) == "NULL") {
        
    } else {
        $db = new Database();
        $this_get_table_form = getFormulaFormDetails($FormName);

// print_r($this_get_table_form);
        if (!empty($ReturnField)) {
            $ReturnField = "`" . $ReturnField . "`";
        } else if (empty($ReturnField) or trim($ReturnField) == "*") {
            $ReturnField = "*";
        }

        $cache_key = $ReturnField . "::";
        $q_string = "SELECT $ReturnField FROM `" . $this_get_table_form["form_table_name"] . "`";
        $search = " (NOT EXISTS(SELECT * FROM tbtrash_bin WHERE record_id = " . $this_get_table_form["form_table_name"] . ".id 
                                        AND form_id = " . $this_get_table_form["id"] . " 
                                        AND table_name='" . $this_get_table_form["form_table_name"] . "')) ";
        if (!empty($FieldName) and ( !empty($Filter) || $Filter === '0' ||  $Filter === '') ) {
            $q_string_where = " WHERE " . $search . " AND `$FieldName` = '$Filter'";
            $q_string .= $q_string_where;

            $cache_key.=$FieldName . "::" . $Filter;
        } else {
            $q_string_where = " WHERE " . $search;
            $q_string .= $q_string_where;
        }

        $this_record = Formula::getLookupValue("formula_lookup", $this_get_table_form, $q_string, $cache_key);
        $ReturnField = preg_replace("/`/", "", $ReturnField);
        $collect_rec = array();
        for ($ctr = 0; $ctr < count($this_record); $ctr++) {
            array_push($collect_rec, $this_record[$ctr][$ReturnField]);
        }
//return $GLOBALS['company_id'];
        array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
            "form_id" => $this_get_table_form['id'],
            "where" => $q_string_where,
            "function_name" => "Lookup",
            "whole_query" => $q_string
        ));
        return join($collect_rec, "<--->");
    }
}

function ThisDate($this_date) {
    if (is_date_str($this_date)) {
        return strtotime($this_date);
    } else if (is_numeric($this_date)) {
        return date('Y-m-d H:i:s', $this_date);
    } else {
        return "Invalid Date";
    }


    // $this_new_date = date("Y-m-d H:i",strtotime(this_date));
    // $this_sub_function = new stdClass();
    // $this_sub_function->{"formatDate"} = function (){
    // $new_here = date("Y-m-d H:i",strtotime($this_new_date));
    // return $this_new_date;
    // }
    // return $this_sub_function;
}

function StrLeft($passed_str, $number_of_chars) {
    $NOC = $number_of_chars;
    return substr($passed_str, 0, $NOC);
}

function StrConcat() {
    $args = func_get_args();
    $return_value = '';

    foreach ($args as $arg) {
        $return_value.=$arg;
    }

    return $return_value;
}

function StrRight($passed_str, $number_of_chars) {
    $NOC = $number_of_chars;
    $counted_str = StrCount($passed_str);
    $start_index = $counted_str - $NOC;
    return substr($passed_str, $start_index, $NOC);
}

function StrGet($passed_str, $start_index_char = 0, $number_of_chars = null) {
    $SIC = 0;
    $NOC = $number_of_chars;
    if (is_numeric($SIC)) {
        if ($SIC <= -1) {
            $SIC = 0;
        }
    }
    if (is_numeric($number_of_chars)) {
        $SIC = $start_index_char - 1;
        if ($NOC <= -1) {
            $NOC = 0;
        }
    }
    if (is_null($number_of_chars)) {
        return $passed_str{$SIC};
    } else {
        return substr($passed_str, $SIC, $NOC);
    }
}

function StrCount($passed_str) {
    return strlen($passed_str);
}

function GivenIf($condition, $val_true, $val_false) {
    if ($condition) {
        return $val_true;
    } else {
        return $val_false;
    }
}

function GivenIf2(){

    //REPLACED FORMULA STRING
    //SAMPLE: @GivenIf2<<::true::;;@StrConcat('TRUE BOOM')::;;@StrConcat('FALSE BOOM')::>>"
    // .replace(/@GivenIf2[\s]*?<<::/g,"@GivenIf2 ( function( ) { return ")
    // .replace(/::;;/g,"; }, function ( ) { return ")
    // .replace(/::>>/g,"; } )")

}

//validateDate('2012-02-28 12:12:12')
function validateDate($date, $format = 'Y-m-d H:i:s') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function RecordCount($FormName) {
    $arguments = func_get_args();
    $db = new Database();
    $companyFilter = "";
    $auth = getFormulaAuth();
    $companyFilter = "AND company_id = {$auth['company_id']} ";

    $this_get_table_form = getFormulaFormDetails($FormName);
    $tbfields_data = Formula::getTBFields($this_get_table_form["id"], $db);
    $q_string = "SELECT count(*) as RecordCount FROM `" . $this_get_table_form['form_table_name'] . "`";

    $Field_Name_Counter = 1;
    $Field_Operator_Counter = 2;
    $Filter_Value_Counter = 3;
    $q_string_where = "";
    $cache_key = "*";
    if (isset($arguments[1])) {
        $q_string_where .= " WHERE ";

        foreach ($arguments as $key => $value) {
            // echo "BT".$key;
            $cache_key .= "::" . $arguments[$key];
            if ($key >= 1 and $key % $Field_Name_Counter == 0) {
                $q_string_where .= " `" . $arguments[$key] . "` ";
                $Field_Name_Counter = $Field_Name_Counter + 3;
            } else if ($key >= 2 and $key % $Field_Operator_Counter == 0) {
                $q_string_where .= " " . $arguments[$key] . " ";
                $Field_Operator_Counter = $Field_Operator_Counter + 3;
            } else if ($key >= 3 and $key % $Filter_Value_Counter == 0) {
                if ($tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Currency" || $tbfields_data["" . $arguments[$key - 2]]["field_input_type"] == "Number") {
                    $q_string_where .= " " . $arguments[$key] . " ";
                } else {
                    $q_string_where .= " '" . $arguments[$key] . "' ";
                }
                $Filter_Value_Counter = $Filter_Value_Counter + 3;
                if ($key == (count($arguments) - 1)) {
                    $q_string_where .= " ; ";
                } else {
                    $q_string_where .= " AND ";
                }
            }
        }
    }
    $q_string .= $q_string_where;
    // echo $q_string;

    $count = Formula::getLookupValue("formula_lookup_record_count", $this_get_table_form, $q_string, $cache_key);


    array_push($GLOBALS['formula_executed_data_collector']['collected_form_id'], array(
        "form_id" => $this_get_table_form['id'],
        "where" => $q_string_where,
        "function_name" => "RecordCount",
        "whole_query" => $q_string
    ));
    return $count[0]['RecordCount'];
}

function GetRound($given_num, $decimal_digit, $type) {
    if ($type == "up") {
        $result = floatval($given_num) * pow(10, $decimal_digit);
        $result = ceil($result) / pow(10, $decimal_digit);
        return $result;
    } else if ($type == "down") {
        $result = floatval($given_num) * pow(10, $decimal_digit);
        $result = floor($result) / pow(10, $decimal_digit);
        return $result;
    } else {
        return round(floatval($given_num), $decimal_digit);
    }
}

function sanitizeStringValues($str_val) {
    $sanitize_str = $str_val;

    $json_decode_stat = false;
    $json_decode_result1 = "";
    $json_decode_result2 = "";
    $json_decode_type = "";


    try {
        $json_decode_result1 = json_decode($sanitize_str, true);

        if (gettype($json_decode_result1) == "string") {

            $json_decode_result2 = json_decode($json_decode_result1, true);

            if (gettype($json_decode_result2) == "array") {

                $sanitize_str = $json_decode_result1;
                // $sanitize_str = addslashes($sanitize_str);
                $sanitize_str = preg_replace("/\\\\(?=\\\")/", "\\\\\\\\\\\\", $sanitize_str);
                $sanitize_str = preg_replace("/\\\"/", "\\\\\\\"", $sanitize_str);
            } else {
                $sanitize_str = preg_replace("/\"/", "\\\"", $sanitize_str);
            }
        } else {
            $sanitize_str = preg_replace("/\"/", "\\\"", $sanitize_str);
        }
    } catch (Exception $error) {
        $sanitize_str = "3";
    }

    // $sanitizeStringValuese_str = preg_replace("/\\\'/", "\\\\\\\'", $sanitize_str);
    // $sanitizeStringValuese_str = preg_replace("/\\\\/", "", $sanitize_str);
    // $sanitize_str = preg_replace("/(^\"|\"$)/", "\\\"", $sanitize_str);
    // $sanitize_str = preg_replace("/\\\\/", "\\\\\\\\", $sanitize_str);
    // $sanitize_str = preg_replace("/\-/", "\\\\\\\-", $sanitize_str);
    // $sanitize_str = preg_quote($sanitize_str);
    // $sanitize_str = preg_replace("/\\\"/", "\\\\\\\"", $sanitize_str);
    // $sanitize_str = preg_replace("/\\\'/", "\\\\\\\'", $sanitize_str);
    // $sanitize_str = preg_replace("/\\\\/", "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\", $sanitize_str);
    return $sanitize_str;
}

Class Formula extends Database {

    public $MyFormula;
    protected $ProcessedFormula;
    private $field_model_import = array();
    public $DataFormSource;
    public $DataFormFieldsProp;
    public $AddedDataFormSource = array();
    protected $DBase;
    public $SelFormID = null;
    protected $FormDataProp = null;
    private $FormulaExecutionData = array();
    public $keywords = [
        '@StrRight', '@StrLeft', '@StrCount', '@StrConcat',
        '@StrGet', '@ThisDate', '@AdjustDate', '@DiffDate',
        '@GivenIf', '@Lookup', '@RecordCount', '@LookupWhere',
        '@Total', '@LookupGetMax', '@LookupGetMin', '@LookupSDev',
        '@LookupAVG', '@GetRound', '@LookupCountIf', '@Sum',
        '@Requestor', '@RequestID', '@Status', '@CurrentUser', '@Department', '@TrackNo', '@Today', '@Now',
        '@GetAuth', "@FormatDate", "@NumToWord",
        "@Head", "@AssistantHead", "@UserInfo", "@GetData",
        "@LookupAuthInfo",
        "@GetAVG", "@GetMin", "@SDev", "@GetMax",
        "@StrSplitBy","@CustomScript","@LoopIn","@TimeStamp"
    ];

    public function __construct($user_defined_formula = null) {
        $this->DBase = new Database();
        $this->MyFormula = $user_defined_formula;
        functions::currentDateTime();
    }

    // private function getPropertyViaFieldName($data){
    //     return ($data)
    // }
    public function getEvaluatedRawData() {
        return $this->FormulaExecutionData;
    }

    public static final function getLookupValue($key, $this_get_table_form, $q_string, $cache_key) {
        $db = new Database();
        $redis_cache = getRedisConnection();

        if ($redis_cache) {
            $cache_lookup = json_decode($redis_cache->get($key), true);
        }

        if ($cache_lookup[$this_get_table_form["id"]][$cache_key]) {
            $this_record = $cache_lookup[$this_get_table_form["id"]][$cache_key];
        } else {
            $this_record = $db->query($q_string, "array");

            if ($redis_cache) {
                if (!$cache_lookup) {
                    $cache_lookup = array();
                }
                $cache_lookup[$this_get_table_form["id"]][$cache_key] = $this_record;
                $redis_cache->set($key, json_encode($cache_lookup));
            }
        }
        return $this_record;
    }

    public function getTBFields($form_id, $db) {

        $redis_cache = getRedisConnection();

        if ($redis_cache) {
            $cache_form_fields = json_decode($redis_cache->get("formula_form_fields_" . $form_id), true);
        }

        if ($cache_form_fields) {
            $this_get_tbfields = $cache_form_fields;
        } else {
            $q_string_data_prop = "SELECT * FROM `tbfields` WHERE form_id = " . $form_id;
            array_push($GLOBALS['formula_executed_data_collector']['tbfields_query'], $q_string_data_prop);
            if ($this) {
                $this_get_tbfields = $this->query($q_string_data_prop, "array");
            } else {
                $this_get_tbfields = $db->query($q_string_data_prop, "array");
            }

            if ($redis_cache) {
                $redis_cache->set("formula_form_fields_" . $form_id, json_encode($this_get_tbfields));
            }
        }


        $collector = array();
        foreach ($this_get_tbfields as $value) {
            $collector["" . $value["field_name"]] = array(
                "field_input_type" => $value["field_input_type"]? : "Text"
            );
        }
        return $collector;
    }

    public function getEvaluatedRawDataFormID($include_whole_query = false) {
        if (gettype($this->FormulaExecutionData['collected_form_id']) == "array") {
            if ($include_whole_query) {
                return $this->FormulaExecutionData['collected_form_id'];
            } else {
                return array_map(function($a) {
                    $a['whole_query'] = "";
                    return $a;
                }, $this->FormulaExecutionData['collected_form_id']);
            }
        }
    }

    public function addFormSourceData($value) {
        array_push($this->AddedDataFormSource, $value);
    }

    public function updateDataFormSource($key_name, $value) {
        $this->DataFormSource[0][$key_name] = $value;
    }

    public function setSourceForm($FormName, $GetField = null, $ObjectFilter = null) {
    
        $auth = '';
        if (Auth::hasAuth('current_user')) {
            $auth = Auth::getAuth('current_user');
        } else if ($GLOBALS['auth']) {
            $auth = $GLOBALS['auth'];
        } else {
            $options = getopt('f:r:u');
            $form_id = $options['f'];
            $request_id = $options['r'];
            $user_id = $options['u'];
            
            $query = "SELECT * FROM tbuser WHERE id = '".$user_id."'";
            $auth = $this->DBase->query($query, 'row');
        }
    
        if ($auth) {
            
            $q_get_table_form_name = "SELECT `id`, `form_table_name` FROM `tb_workspace` WHERE is_delete = 0 AND `form_name` LIKE '" . $FormName . "' AND company_id = {$auth['company_id']} ";
        } else {
            $q_get_table_form_name = "SELECT `id`, `form_table_name` FROM `tb_workspace` WHERE is_delete = 0 AND `form_name` LIKE '" . $FormName . "' ";
        }

        $table_name = $this->DBase->query($q_get_table_form_name, "row");

        $fieldNameSelected = $GetField;
        if (!empty($fieldNameSelected) and $fieldNameSelected != "*") {
            $fieldNameSelected = explode(",", $fieldNameSelected);
            for ($ctr = 0; $ctr < count($fieldNameSelected); $ctr++) {
                $fieldNameSelected[$ctr] = "`" . $fieldNameSelected[$ctr] . "`";
            }
            $fieldNameSelected = implode(",", $fieldNameSelected);
        } else {
            $fieldNameSelected = "*";
        }
        // echo "TABLE NAME: ".$table_name["form_table_name"];
        $this->SelFormID = $table_name["id"];
        $q_string_data_prop = "SELECT * FROM `tbfields` WHERE form_id = " . $table_name["id"];
        // echo $q_string_data_prop;
        $property_data = $this->DBase->query($q_string_data_prop, "array");
        $this->DataFormFieldsProp = $property_data;
        // print_r($property_data);


        $q_string = "SELECT $fieldNameSelected FROM `" . $table_name["form_table_name"] . "`";
        $whereClause = ' WHERE ';

        foreach ($ObjectFilter as $key) {
            $whereClause.= $key['FieldName'] . ' ' . $key['Operator'] . ' ' . $this->DBase->escape($key['Value']) . ' AND ';
        }

        $whereClause = substr($whereClause, 0, count($whereClause) - 5);
        $q_string .= $whereClause;

        $source_data = $this->DBase->query($q_string, "array");

        $this->DataFormSource = $source_data;
    }

    public function getResult() {
        // print_r($this->db);
        //DITO EVAL
        eval("\$formula_result = " . $this->replaceFormulaFieldNames() . ";");
        return $formula_result;
    }

    public function replaceFormulaFieldNames() {//FIXES APPLIED FOR FS#7678, FS#8390
        $KeyWords = array();// array("GivenIf", "StrLeft", "StrRight", "StrGet", "StrCount", "GetAuth");
        $ProcessedFormula = $this->MyFormula;
        $ProcessedFormula = preg_replace("/\\/\\/.*\\n|\\/\\/.*$/", "", $ProcessedFormula);//pangtanggal ng comment sa formula
        $FC = $this->getFieldNames();

        for ($outerCtr = 0; $outerCtr < count($FC); $outerCtr++) {
            for ($ctr = 0; $ctr < count($FC[$outerCtr]); $ctr++) {
                if (in_array($FC[$outerCtr][$ctr], $KeyWords)) {
                    continue;
                }
                // skip the keyword dont replace
                // if (isset($_POST[$FC[$outerCtr][$ctr]])) { // if the submitted fieldname is existing
                //     print_r('test');
                //     $fn_value = $_POST[$FC[$outerCtr][$ctr]];
                //     //SANITIZING USER INPUT VALUES FOR SECURITY
                //     // $fn_value = mysqli_real_escape_string($fn_value);
                //     $field_name = $FC[$outerCtr][$ctr];
                //     $ProcessedFormula = preg_replace("/@" . $field_name . "/", $fn_value, $ProcessedFormula);
                // } else 
                if (isset($this->DataFormSource[0][$FC[$outerCtr][$ctr]])) {
                    //if the set source DB formname
                    $collect_value = array();
                    $string_date_stat = "false";
                    $field_name = $FC[$outerCtr][$ctr];

                    for ($ctr_data_source = 0; $ctr_data_source < count($this->DataFormSource); $ctr_data_source++) {
                        //dito na mag condition ng data type
                        // print_r("SABOG");
                        // print_r($this->DataFormFieldsProp);
                        // print_r("TESTSABOG");
                        if (count($this->DataFormFieldsProp) >= 1) {
                            // print_r("TEST RATEDK");
                            // print_r(array_filter($this->DataFormFieldsProp,function($array_val) use ($field_name){
                            //     return $array_val["field_name"] == $field_name;
                            // })["field_input_type"]);
                            $field_property = array_values(array_filter($this->DataFormFieldsProp, function($array_val) use ($field_name) {
                                                return $array_val["field_name"] == $field_name;
                                            }))[0];
                            // echo "FIELD INPUT TYPE: ".$field_property["field_input_type"];
                            if ($field_property["field_input_type"] == "Currency") {
                                array_push($collect_value, $this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]] ? : 0 );
                            } else if ($field_property["field_input_type"] == "Number") {
                                array_push($collect_value, $this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]] ? : 0 );
                            } else {
                                try {
                                    $this_date = new DateTime($this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]]);
                                    $string_date_stat = "true";
                                } catch (Exception $e) {
                                    $string_date_stat = "false";
                                }
                                if ($string_date_stat == "true") {
                                    array_push($collect_value, "\"" . $this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]] . "\"");
                                } else if ($field_property['field_type'] == 'multiple_attachment_on_request') {
                                    array_push($collect_value, $this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]]);
                                } else {
                                    array_push($collect_value, "\"" . str_replace("@", "^@", sanitizeStringValues($this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]])
                                                    // preg_replace("/^\"|\"$/", "",
                                                    //     str_replace("'","\\'",
                                                    //         str_replace("-","\\-",$this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]])
                                                    //     )
                                                    // )
                                            ) . "\""
                                    );
                                }
                            }
                            $string_date_stat = "false";
                        } else {
                            try {
                                $this_date = new DateTime($this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]]);
                                $string_date_stat = "true";
                            } catch (Exception $e) {
                                $string_date_stat = "false";
                            }


                            if ($string_date_stat == "true") {
                                array_push($collect_value, "\"" . $this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]] . "\"");
                            } else if ( ( $field_name == 'Requestor' || $field_name == 'Processor' ) && is_numeric($this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]])) {
                                array_push($collect_value, $this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]]);
                            } else {
                                // echo "<br/>".sanitizeStringValues($this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]])."<br/>";
                                array_push($collect_value, "\"" . str_replace("@", "^@", sanitizeStringValues($this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]])
                                                // preg_replace("/^\"|\"$/", "",
                                                //     str_replace("'","\\'",
                                                //         str_replace("-","\\-",$this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]])
                                                //     )
                                                // )
                                        ) . "\""
                                );
                                //"/(^\"|\"$)/"
                                // array_push($collect_value, "'".str_replace("@","^@",preg_replace("/\"/","\\\\\\\"",$this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]]))."'" );
                                // array_push($collect_value, "'".str_replace("@","^@",preg_replace("/(^\"|\"$)/","",$this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]]))."'" );
                            }
                            $string_date_stat = "false";
                        }
                    }

                    $row_value = implode("<-->", $collect_value);

                    if ($field_property['field_type'] == 'attachment_on_request') {
                        $fn_value = "(" . $row_value . ")";
                        $temp_arr = explode("/", $fn_value);
                        $ProcessedFormula = preg_replace("/@" . $field_name . "(?![a-zA-Z0-9_])/", $temp_arr[count($temp_arr) - 1], $ProcessedFormula);
                        // print_r($ProcessedFormula);
                        if ($ProcessedFormula != '("")') {
                            $ProcessedFormula = '("' . $ProcessedFormula;
                        }
                        // $ProcessedFormula = '("' . $ProcessedFormula;
                        // print_r($ProcessedFormula);
                    } else if ($field_property['field_type'] == 'multiple_attachment_on_request') {
                        $fn_value = $row_value;
                        $temp_arr = json_decode(json_decode($fn_value, true), true);
                        if (gettype($temp_arr) == "array") {
                            $fn_value = implode(", ", array_map(function($a) {
                                        return $a['file_name'];
                                    }, $temp_arr));
                        } else {
                            $fn_value = "";
                        }
                        $ProcessedFormula = preg_replace("/@" . $field_name . "(?![a-zA-Z0-9_])/", "\"" . $fn_value . "\"", $ProcessedFormula);
                    } else if ($field_name == 'Requestor' || $field_name == 'Processor') {
                        if (is_numeric($row_value)) {
                            $fn_value = "(" . $row_value . ")";
                            $row_value = str_replace("\"", "", $row_value);

                            if ($_POST["ProcessorID"] && $field_name == "Processor") {
                                $row_value = $_POST["ProcessorID"];
                            }

                            $requestorDoc = new Person($this->DBase, (int) $row_value);
                            $fn_value = "(\"" . StrConcat($requestorDoc->display_name/* $requestorDoc->first_name, " ", $requestorDoc->last_name */) . "\")";
                        } else {
                            $fn_value = "(" . $row_value . ")";
                        }
                        
                        $ProcessedFormula = preg_replace("/@" . $field_name . "(?![a-zA-Z0-9_])/", $fn_value, $ProcessedFormula);
                    } else {
                        $fn_value = "(" . $row_value . ")";
                        $ProcessedFormula = preg_replace("/@" . $field_name . "(?![a-zA-Z0-9_])/", $fn_value, $ProcessedFormula);
                    }
                } else {//non existingfieldname so it would be functions/functionality keywords
                    if ($FC[$outerCtr][$ctr] == "RequestID") {
                        // $fn_value = "";
                        // $functionality_name = $FC[$outerCtr][$ctr];
                        // $ProcessedFormula = preg_replace("/@".$functionality_name."/", $fn_value, $ProcessedFormula);
                        $collect_value = array();
                        if (count($this->AddedDataFormSource) >= 1) {
                            foreach ($this->AddedDataFormSource as $added_value) {
                                array_push($collect_value, $added_value["RequestID"]);
                            }
                        } else {
                            array_push($collect_value, $this->DataFormSource[0]["ID"]);
                        }
                        // for ($ctr_data_source = 0; $ctr_data_source < count($this->DataFormSource); $ctr_data_source++) {
                        //     array_push($collect_value, $this->DataFormSource[$ctr_data_source]["ID"]);
                        // }

                        $fn_value = "(" . implode("<-->", $collect_value) . ")";
                        $functionality_name = $FC[$outerCtr][$ctr];
                        $ProcessedFormula = preg_replace("/@" . $functionality_name . "(?![a-zA-Z0-9_])/", $fn_value, $ProcessedFormula);
                    } else if ($FC[$outerCtr][$ctr] == "InsertID") {
                        $collect_value = array();
                        if (count($this->AddedDataFormSource) >= 1) {
                            foreach ($this->AddedDataFormSource as $added_value) {
                                array_push($collect_value, $added_value["InsertID"]);
                            }
                        } else {
                            array_push($collect_value, $this->DataFormSource[0]["InsertID"]);
                        }
                        $fn_value = "(" . implode("<-->", $collect_value) . ")";
                        $functionality_name = $FC[$outerCtr][$ctr];
                        $ProcessedFormula = preg_replace("/@" . $functionality_name . "(?![a-zA-Z0-9_])/", $fn_value, $ProcessedFormula);
                    } else if ($FC[$outerCtr][$ctr] == "Status") {
                        // $fn_value = "STATUS HERE";
                        // $functionality_name = $FC[$outerCtr][$ctr];
                        // $ProcessedFormula = preg_replace("/@".$functionality_name."/", $fn_value, $ProcessedFormula);

                        $collect_value = array();
                        for ($ctr_data_source = 0; $ctr_data_source < count($this->DataFormSource); $ctr_data_source++) {
                            array_push($collect_value, $this->DataFormSource[$ctr_data_source][$FC[$outerCtr][$ctr]]);
                        }
                        $fn_value = "(\"" . implode("<-->", $collect_value) . "\")";
                        $functionality_name = $FC[$outerCtr][$ctr];
                        $ProcessedFormula = preg_replace("/@" . $functionality_name . "(?![a-zA-Z0-9_])/", "" . $fn_value . "", $ProcessedFormula);
                    } else if ($FC[$outerCtr][$ctr] == "TimeStamp") {
                        $functionality_name = $FC[$outerCtr][$ctr];
                        $default_precision_configuration = ini_get('precision');
                        ini_set('precision',13);
                        $fn_value = round(microtime(true) * 1000);
                        // $fn_value = date("F d, Y H:i:s");
                        // $functionality_name = $FC[$outerCtr][$ctr];
                        $ProcessedFormula = preg_replace("/@" . $functionality_name . "(?![a-zA-Z0-9_])/", "" . $fn_value . "", $ProcessedFormula);
                        ini_set('precision',$default_precision_configuration);
                    } else if ($FC[$outerCtr][$ctr] == "Now") {
                        $fn_value = date("F d, Y H:i:s");
                        $functionality_name = $FC[$outerCtr][$ctr];
                        $ProcessedFormula = preg_replace("/@" . $functionality_name . "(?![a-zA-Z0-9_])/", "\"" . $fn_value . "\"", $ProcessedFormula);
                    } else if ($FC[$outerCtr][$ctr] == "Today") {
                        $fn_value = date("Y-m-d"); //date("F d, Y");
                        $functionality_name = $FC[$outerCtr][$ctr];
                        $ProcessedFormula = preg_replace("/@" . $functionality_name . "(?![a-zA-Z0-9_])/", "\"" . $fn_value . "\"", $ProcessedFormula);
                    } else if ($FC[$outerCtr][$ctr] == "Department") {//WALA NA TO @UserInfo na ang papalit
                        //$user_id = $this->DataFormSource[$ctr_data_source]["Processor"];
                        $ProcessedFormula = preg_replace("/@" . "Department" . "(?![a-zA-Z0-9_])/", "\"\"", $ProcessedFormula);
                    } else if ($FC[$outerCtr][$ctr] == "CurrentUser") {

                        $ProcessedFormula = preg_replace("/@" . "CurrentUser" . "(?![a-zA-Z0-9_])/", "\"" . GetAuth("CurrentUser") . "\"", $ProcessedFormula);

                        //}else if($FC[$outerCtr][$ctr] == "Processor"){//existing fieldname ito
                        //    
                        //    $ProcessedFormula = preg_replace("/@" . "Processor" . "(?![a-zA-Z0-9_])/", "\"\"", $ProcessedFormula);
                    } else if (preg_match("/^Department(?!=0-9a-zA-Z_)/", $FC[$outerCtr][$ctr]) != 0) {//$FC[$outerCtr][$ctr] == "Department"
                        preg_match_all("/\[.*?\]/", $FC[$outerCtr][$ctr], $attr_val);

                        // $fn_value = "my Department";
                        // $functionality_name = $FC[$outerCtr][$ctr];
                        // $ProcessedFormula = preg_replace("/@".$functionality_name."/", $fn_value, $ProcessedFormula);
                        // print_r(preg_replace("/\[|\]/", "", $attr_val[0][0]));
                        // echo "<br/>";

                        $collect_value = array();
                        for ($ctr_data_source = 0; $ctr_data_source < count($this->DataFormSource); $ctr_data_source++) {
                            $zid = $this->DataFormSource[$ctr_data_source][preg_replace("/\[|\]/", "", $attr_val[0][0])];

                            $zpersonDoc = new Person($this->DBase, $zid); //$zpersonDoc = new Person($db, $auth['id']);
                            $zuserDepartmentName = $zpersonDoc->department->name;

                            // echo "<br/>".$zuserDepartmentName;

                            array_push($collect_value, $zuserDepartmentName);
                        }
                        $fn_value = "(" . implode("<-->", $collect_value) . ")";
                        $functionality_name = $FC[$outerCtr][$ctr];

                        // echo "<br/>s".$functionality_name."s<br/>";
                        if (count($collect_value) <= 0) {
                            $fn_value = "\"\"";
                        }

                        $ProcessedFormula = preg_replace("/@" . $functionality_name . "(?![a-zA-Z0-9_])/", $fn_value, $ProcessedFormula);
                    } else {
                        $functionality_name = $FC[$outerCtr][$ctr];
                        if (!function_exists($functionality_name)) {
                            $ProcessedFormula = preg_replace("/@" . $functionality_name . "(?![a-zA-Z0-9_])/", "(\"\")", $ProcessedFormula);
                        }else{
                            $ProcessedFormula = preg_replace("/@" . $functionality_name . "(?![a-zA-Z0-9_])/", $functionality_name, $ProcessedFormula);
                        }
                    }
                }
            }
        }
        // $ProcessedFormula = preg_replace_callback("/".implode("\s*?\(|", $this->keywords)."\s*?\(/", function($word_match){ //para hindi mareplace ung mga @gmail sa loob ng string na dinefine nila as string
        //     return str_replace("@", "", $word_match[0]);
        // }, $ProcessedFormula);
        // $ProcessedFormula = preg_replace_callback("/\^@[A-Za-z_][A-Za-z0-9_]*/", function($word_match){ //replace ^@sstring
        //     return str_replace("^@", "@", $word_match[0]);
        // }, $ProcessedFormula);
        $ProcessedFormula = preg_replace_callback("/\"(\\\\\\\"|[^\"])*\"|\"(\\\\\\\"|[^\"])*|\'(\\\\\\\'|[^\'])*\'|\'(\\\\\\\'|[^\'])*/", function($word_match){ //replace ^@sstring
            return str_replace("^@", "@", $word_match[0]);
        }, $ProcessedFormula);
        return $ProcessedFormula;
    }

    private function getFieldNames() { //FIX FOR FS#8390
        // preg_match_all("/\^@[A-Za-z_][A-Za-z0-9_]*|@[A-Za-z_][A-Za-z0-9_]*\[[A-Za-z_][A-Za-z0-9_]*\]*|@[A-Za-z_][A-Za-z0-9_]*/", $this->MyFormula, $FieldNameCollection); // luma
        $pattern_preg = [
            "\^@[A-Za-z_][A-Za-z0-9_]*(?=\s)",
            "(\s|^)@[A-Za-z_][A-Za-z0-9_]*\[[A-Za-z_][A-Za-z0-9_]*\]*(?=\s)",
            "([^a-zA-Z0-9_]|^)@[A-Za-z_][A-Za-z0-9_]*(?=([^a-zA-Z0-9_]|$))"
        ];
        $myformula = $this->MyFormula; //" @testfield test@gmail.com @yeah (@horri"
        preg_match_all("/" . implode($pattern_preg, "|") . "/", $myformula, $FieldNameCollectionTemp);
        $FieldNameCollection = array(array_values(array_filter(array_map(function($a) {
                        if(substr($a, 0, 2) == "^@"){
                            return null;
                        }
                        if (preg_match('/^[^@]/i', $a)) {
                            $a = substr($a, 1, strlen($a));
                        }
                        return trim($a);
                    }, $FieldNameCollectionTemp[0]))));
        for ($outerCtr = 0; $outerCtr < count($FieldNameCollection); $outerCtr++) {
            $FieldNameCollection[$outerCtr] = array_filter($FieldNameCollection[$outerCtr], function($value) {
                if (is_array($value) || is_object($value)) {
                    return true;
                } else if (strrpos($value, "^@") === false) {
                    return true;
                } else {
                    return false;
                }
            });
        }

        for ($outerCtr = 0; $outerCtr < count($FieldNameCollection); $outerCtr++) {
            for ($ctr = 0; $ctr < count($FieldNameCollection[$outerCtr]); $ctr++) {
                $FieldNameCollection[$outerCtr][$ctr] = str_replace("@", "", $FieldNameCollection[$outerCtr][$ctr]);
            }
        }

        return $FieldNameCollection;
    }

    private function getAtFieldNames() {
        preg_match_all("/\^@[A-Za-z_][A-Za-z0-9_]*|@[A-Za-z_][A-Za-z0-9_]*\[[A-Za-z_][A-Za-z0-9_]*\]*|@[A-Za-z_][A-Za-z0-9_]*/", $this->MyFormula, $FieldNameCollection);
        for ($outerCtr = 0; $outerCtr < count($FieldNameCollection); $outerCtr++) {
            $FieldNameCollection[$outerCtr] = array_filter($FieldNameCollection[$outerCtr], function($value) {
                if (is_array($value) || is_object($value)) {
                    return true;
                } else if (strrpos($value, "^@") === false) {
                    return true;
                } else {
                    return false;
                }
            });
        }
        for ($outerCtr = 0; $outerCtr < count($FieldNameCollection); $outerCtr++) {
            for ($ctr = 0; $ctr < count($FieldNameCollection[$outerCtr]); $ctr++) {
                $FieldNameCollection[$outerCtr][$ctr] = $FieldNameCollection[$outerCtr][$ctr];
            }
        }
        return $FieldNameCollection;
    }

    public function getAllKeyNames() {
        preg_match_all("/\^@[A-Za-z_][A-Za-z0-9_]*|@[A-Za-z_][A-Za-z0-9_]*\[[A-Za-z_][A-Za-z0-9_]*\]*|@[A-Za-z_][A-Za-z0-9_]*/", $this->MyFormula, $FieldNameCollection);
        for ($outerCtr = 0; $outerCtr < count($FieldNameCollection); $outerCtr++) {
            $FieldNameCollection[$outerCtr] = array_filter($FieldNameCollection[$outerCtr], function($value) {
                if (is_array($value) || is_object($value)) {
                    return true;
                } else if (strrpos($value, "^@") === false) {
                    return true;
                } else {
                    return false;
                }
            });
        }
        for ($outerCtr = 0; $outerCtr < count($FieldNameCollection); $outerCtr++) {
            for ($ctr = 0; $ctr < count($FieldNameCollection[$outerCtr]); $ctr++) {
                $FieldNameCollection[$outerCtr][$ctr] = $FieldNameCollection[$outerCtr][$ctr];
            }
        }
        return $FieldNameCollection;
    }

    public function getProccessedFormula() {
        return $this->replaceFormulaFieldNames();
    }

    public function evaluate() {
        $returnValue = "skipped";
        //$GLOBALS['formula_executed_data_collector']['collected_form_id'] //pagcollect ng form_id and where areas nito
        try {
            $processed_formula = $this->getProccessedFormula();
//            var_dump($processed_formula);
            if ($processed_formula == "") {
                $returnValue = "";
            } else if ($processed_formula) {
                $parseChecker = eval("\$returnValue = " . $this->getProccessedFormula() . ';');
                if($parseChecker === false){
                    $find = array(
                        'unexpected \';\''
                    );
                    $replace = array(
                        'unexpected end of expression'
                    );
                    $temp = array(
                        "error_message"=>str_replace($find, $replace, error_get_last()['message']) ,
                        "formula"=>$this->getProccessedFormula()
                    );
                    $returnValue = print_r($temp,true);
                }
            }
        } catch (Exception $e) {
            if(isset($e->xdebug_message)){
                $e->xdebug_message = "<table>".str_replace("\n", '', $e->xdebug_message)."</table>";
            }
            $returnValue = "Formula not valid! \n".$e->xdebug_message;
        }
        $this->FormulaExecutionData = $GLOBALS['formula_executed_data_collector'];
        return $returnValue;
    }

    public function evaluateV2() {
        $returnValue = "skipped";
        $GLOBALS['formula_executed_data_collector'] = array(
            "collected_form_id" => array()
        );
        $additional_eval_str = "";
        foreach ($this->field_model_import as $key => $value) {
            try {
                $additional_eval_str .= "\$" . $key . " = json_decode('" . json_encode($value) . "',true);";
            } catch (Exception $e) {
                $additional_eval_str .= "\$" . $key . " = array();";
            }
        }
        // print_r("additional eval str: " . $additional_eval_str);
        //$GLOBALS['formula_executed_data_collector']['collected_form_id'] //pagcollect ng form_id and where areas nito
        try {
            // print_r("return value: " . $returnValue . "\n");
            // if(strlen($additional_eval_str)  >= 1){
            //     eval($additional_eval_str);
            // }
            // ." echo 'TESTF';print_r(\$field_models);";
            if ($this->getProccessedFormula()) {
                eval($additional_eval_str . "\$returnValue = " . $this->getProccessedFormula() . ";");
                // print_r("return value: " . $returnValue . "\n");
            }
            // print_r("return value: " . $returnValue . "\n");
        } catch (Exception $e) {
            $returnValue = "Formula not valid!";
        }
        $this->FormulaExecutionData = $GLOBALS['formula_executed_data_collector'];
        return $returnValue;
    }

    public function setFieldModel($field_model_name, $field_model_value) {
        $this->field_model_import[$field_model_name] = $field_model_value;
    }

}

?>
<?php

function SDevDataArray() {

    $args = func_get_args();
    //Don Knuth is the $deity of algorithms
    //http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#III._On-line_algorithm
    $n = 0;
    $mean = 0;
    $M2 = 0;
    $collector_array = array();
    foreach ($args as $key => $value) {
        if (gettype($value) == "array") {
            $collector_array = array_merge($collector_array, $value);
        } else {
            array_push($collector_array, $value);
        }
    }

    foreach ($collector_array as $x) {
        $n++;
        $delta = $x - $mean;
        $mean = $mean + $delta / $n;
        $M2 = $M2 + $delta * ($x - $mean);
    }
    $variance = $M2 / ($n - 1);
    return sqrt($variance);
}

// function SDev($data_array) {
// 	$d_a = $data_array;
// 	$nums = array();
//     if(gettype($d_a) == "array"){
//         $nums = $d_a;
//         $dataSD = array();
// 		$mean = 0;
// 		$mean_average = 0;
// 		$decimal = 1000000;
// 		$dataSD['totalNumbers'] = count($nums);
// 		$division = function ($a, $b) {         
// 		    if($b === 0)
// 		      return null;
// 		    return $a/$b;
// 		};
// 		$getMeanAverage = (function ($nums) use($mean,$decimal,$mean_average) {
// 			foreach($nums as $ii => $ii_value){
// 				$mean = $mean + (int)$ii_value;
// 			}
// 			$mean = $mean / count($nums);
// 			$mean_average = round($mean * $decimal) / $decimal;
// 			return round($mean * $decimal) / $decimal;
// 		});
// 		$dataSD['meanAverage'] = $getMeanAverage($nums);
// 		$variance = 0;
// 		$varian = 0;
// 		$getVariance = (function($nums) use($mean,$varian,$variance,$decimal,$division){
// 			foreach($nums as $ii => $ii_value){
// 				$variance = $variance + pow((parseFloat($ii_value) - $mean), 2);
// 			}
// 			$varian = $division($variance, (count($nums) - 1) );
// 			$varian = ($varian != NULL) ? $varian : 0;
// 			return round($varian * $decimal) / $decimal;
// 		});
// 		$dataSD['varianceSD'] = $getVariance($nums);
// 		$sd = 0;
// 		$getStandardDeviation = (function() use($varian, $decimal,$sd) {
// 			$sd = sqrt($varian);
// 			return round($sd * $decimal) / $decimal;
// 		});
// 		$dataSD['standardDeviation'] = $getStandardDeviation();
// 		$pop = 0;
// 		$getPopulationSD = (function($nums) use ($variance,$pop,$pop_sqrt,$decimal) {
// 			$pop = $variance / count($nums);
// 			$pop_sqrt = sqrt($pop);
// 			return round($pop_sqrt * $decimal) / $decimal;
// 		});
// 		$dataSD['populationSD'] = $getPopulationSD($nums);
// 		$var_pop = 0;
// 		$getVariancePSD = (function($nums) use($var_pop, $variance, $decimal) {
// 			$var_pop = $variance / count($nums);
// 			return round($var_pop * $decimal) / $decimal;
// 		});
// 		$dataSD['variancePSD'] = $getVariancePSD($nums);
// 		return $dataSD["standardDeviation"];
//     }
//     return 0;
// }
// $fromula_doc = new Formula('@LookupWhere("TEST_CHILD_EMBED", "TrackNo", "Requestor", "=", "240")');
// var_dump($fromula_doc->evaluate());
// echo "<br/><br/>";
// echo json_encode($fromula_doc->getEvaluatedRawDataFormID(true),JSON_PRETTY_PRINT);
?>