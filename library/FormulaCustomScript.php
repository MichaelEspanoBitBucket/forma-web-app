<?php
include("client_script/UPPC/LeaveRequest.php");
include("client_script/STA_CLARA/LeaveRequest.php");
include("client_script/STA_CLARA/PersonnelInformation201.php");
include("custom-script-api/CustomScriptFunctionList.php");

function CustomScript($function_name) {//No @ symbol for function name // this method runs only on php version that supports functions in variable as callback tried at php5.6.5
    $db = new Database();
    $params = func_get_args();
    $fetch_function_name = $params[0];
    unset($params[0]);
    $params = array_values($params);

    $formatted_data = array(
        "visibility"=>array(),
        "computed_value"=>array(),
        "change_list_computed_value"=>array(),
        "readonly"=>array(),
        "validation"=>array()
    );



    //ADD FUNCTIONS HERE
    //data source will be the $_POST data
    $custom_function = array(
        "FormulaOnLoadLeave" => function($selected_fields,$excluded_fields) use($formatted_data, $db) {
            if(function_exists("UPPC_LeaveRequest")){
                return UPPC_LeaveRequest($selected_fields,$excluded_fields,$formatted_data,$db);
            }else{
                return $formatted_data;
            }
        },
        "FormulaOnLoadPersonalInfo" => function($selected_fields,$excluded_fields) use($formatted_data, $db) {
            if(function_exists("STA_CLARA_PersonnelInformation201")){
                return STA_CLARA_PersonnelInformation201($selected_fields,$excluded_fields,$formatted_data,$db);
            }else{
                return $formatted_data;
            }
        },
        "FormulaOnLoadLeaveStaClara" => function($selected_fields,$excluded_fields) use($formatted_data, $db) {
            if(function_exists("STA_CLARA_LeaveRequest")){
                return STA_CLARA_LeaveRequest($selected_fields,$excluded_fields,$formatted_data,$db);
            }else{
                return $formatted_data;
            }
        },
        "AaronCustom" => function($selected_fields,$excluded_fields) use($formatted_data, $db) {
            
            $formatted_data['computed_value']['textbox_3'] = $_POST['textbox_1'] + $_POST['textbox_2'];

            return $formatted_data;
        },
    );
	
	// modified by joshua reyes
    $cs_functionlist = new CustomScriptFunctionList();
    $custom_function = array_merge($custom_function, $cs_functionlist->scriptList());
    $GLOBALS['custom_function'] = $custom_function; //added for GivenIf/Recursive algo

    if (isset($custom_function[$fetch_function_name])) { //kapag existing ung custom function name
        return call_user_func_array($custom_function[$fetch_function_name], $params);
    } else {
        return "Function not found!";
    }
}

?>