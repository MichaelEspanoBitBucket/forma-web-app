<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Guest_Account
 *
 * @author Samuel Pulta
 */
class Guest_Account{

    public function save_new_guest($db,$auth,$json){
        $fs = new functions();
        $mail = new Mail_Notification();
        $insert_new_guest = array("email"           => $json['email'],
                                  "company_id"      => $auth['company_id'],
                                  "first_name"      => $json['first_name'],
                                  "last_name"       => $json['last_name'],
                                  "display_name"    =>  $json['display_name'],
                                  "user_level_id"   => "4",
                                  "date_registered" => $fs->currentDateTime(),
                                  "password"        => $json['password'],
                                  "email_activate"  => "0",
                                  "timeout"         => "0",
                                  "is_active"       => "0");
        $guest_id = $db->insert("tbuser",$insert_new_guest);
        
        $encrypted_id = $fs->base_encode_decode("encrypt",$guest_id);
        if($json['type'] != "register_guest"){
           $mail->add_guest(array($json['email']=>$json['first_name'] . ' ' . $json['last_name']),"",$auth['company_id'],$auth,$encrypted_id);
        }else{
            $mail->reg_guest(array($json['email']=>$json['first_name'] . ' ' . $json['last_name']),"",$auth['company_id'],$auth,$encrypted_id,$auth['display_name'],$json['reg_type']);
        }
        
        return array("id"   =>  $guest_id);
        //$person = new Person($db,$auth['id']);
        //
        //$person->email = $json['email'];
        //$person->company_id = $auth['company_id'];
        //$person->user_level_id = "4";
        //$person->date_registered = $fs->currentDateTime();
        //$person->email_activate = "0";
        //$person->is_available = "0";
        //$person->timeout = "0";
        //$person->is_active = "0";
        //$person->save();
        
    }
    
}

?>
