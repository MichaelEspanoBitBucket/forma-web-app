<?php 

class Header_MainController_Include
{

    public function guest_css(){
        // $auth = new Auth();
        //return Auth::hasAuth('current_user');
        if(!Auth::hasAuth('current_user')){ 
            $fs = new functions(); 
            if($fs->get_module($_SERVER['REQUEST_URI'],$_SERVER['QUERY_STRING']) != "activation"){ 
            
            $fs = new functions();
            if(ALLOW_GUEST == "1"){
                
                $url = $fs->encode_decode_url($fs->curPageURL("module_parameter"),"decode");
                
                $query_str = parse_url($url, PHP_URL_QUERY);
                parse_str($query_str, $query_params);
                //print_r($query_params);
                if(isset($query_params) && isset($query_params['authentication']) && isset($query_params['type']) && isset($query_params['log_type'])){
                if($query_params['authentication'] == "login" && $query_params['type'] == "guest" && $query_params['log_type'] == "user" ){
                    $display = '<link rel="stylesheet" href="/css/customloginandsignup.css" class=""/>';
                }else{
                    if(HAS_THEME == 0){
                        $display = '<link rel="stylesheet" class="oLayout" href="/css/customlogin_resdefault.css" />';
                    }
                }
                }else{
                    if(HAS_THEME == 0){
                        $display = '<link rel="stylesheet" class="oLayout" href="/css/customlogin_resdefault.css" />';
                    }
                }
                
            }else{
                if(HAS_THEME == 0){
                    $display = '<link rel="stylesheet" class="oLayout" href="/css/customlogin_resdefault.css" />';
                }
                
            }
                echo $display;
            
                return $display;
            } 
        }
            
    }

    public function main_controller(){
        $layout = new Layout();
        // Allow User View
        $include_css = "";
        $main_controller = "";
        $guest_structure = "";

        if(ALLOW_USER_ADMIN != "1"){
            if(ALLOW_USER_VIEW=="1"){
                $auth = Auth::getAuth('current_user');
                $user_level_id = $auth['user_level_id'];
                if($user_level_id == "2" || $user_level_id == "1"  || !Auth::hasAuth('current_user') || $user_level_id == "0"){
                    $include_css = $layout->setInclude("admin_css");
                    $main_controller = "";
                }else{
                    //Users CSS
                    //Responsive and mobile friendly stuff
                    $include_css = $layout->setInclude("user_css");
                    $main_controller = '<script type="text/javascript" src="/js/functions/user_view/maincontroller.js"></script>';
                }
            }else{
                $include_css = $layout->setInclude("admin_css");
                $main_controller = "";
            }
        }else{
            if(ALLOW_USER_VIEW=="1"){
               
            //if($user_level_id == "4"){
                $guest_structure =  '<link rel="stylesheet" href="/css/gueststructure.css" />';
            //}else{
                //Users CSS
                //Responsive and mobile friendly stuff
                $include_css = $layout->setInclude("user_css");
                $main_controller = '<script type="text/javascript" src="/js/functions/user_view/maincontroller.js"></script>';
            //}
            }else{
            //if($user_level_id == "4"){
                $guest_structure = '<link rel="stylesheet" href="/css/gueststructure.css" />';
            //}else{
                $include_css = $layout->setInclude("admin_css");
                $main_controller = "";
            //}
            }
        }

        return json_encode(array(  "main_controller"       =>  $main_controller,
                                    "include_css"           =>  $include_css,
                                    "guest_structure"       =>  $guest_structure));
    }


    public function header_include(){
        $auth = Auth::getAuth('current_user');
        $user_level_id = $auth['user_level_id'];
        $fs = new functions();
        $layout = new Layout();
        // Allow User View
        if(!Auth::hasAuth('current_user') && $user_level_id != "4"){
                        if(MAINTENANCE == "1"){
                                return $layout->setInclude("maintenance");
                        }else{
                                if($fs->get_module($_SERVER['REQUEST_URI'],$_SERVER['QUERY_STRING']) != "activation"
                                        &&
                                        $fs->get_module($_SERVER['REQUEST_URI'],$_SERVER['QUERY_STRING']) != "ad_registration"
                                        &&
                                        $fs->get_module($_SERVER['REQUEST_URI'],$_SERVER['QUERY_STRING']) != "activate_account"
                                                                                &&
                                        $fs->get_module($_SERVER['REQUEST_URI'],$_SERVER['QUERY_STRING']) != "reset_password"
                                        && 
                                        $fs->get_module($_SERVER['REQUEST_URI'],$_SERVER['QUERY_STRING']) != "browser"
                                        ){
                                         if(COMPANY_STYLES == null ||  COMPANY_STYLES == ""){
                                                         return $layout->setInclude("main",null,null,null);
                                                 } else {
                                                         return $layout->setInclude("custommain_" . COMPANY_STYLES ,null,null,null);
                                                 }
                                         
                                 }
                        }
                        
        //}elseif( Auth::hasAuth('current_user') && $user_level_id == "4"){
        //$this->setInclude("guest_header",null,null,null);
        }else{
                if(MAINTENANCE == "1"){
                        return $layout->setInclude("maintenance");
                }else{
                        return $layout->setInclude("user_header",null,null,null);
                }
        }
    }
}
?>