<?php


class JobTaskDAO {
    public function __construct() {
        $this->database = new Database();
    }
    protected $database;
	protected $selectable_columns = array(
							"TrackNo",
							"job_task_no",
							"estimated_start",
							"estimated_end",
							"actual_start",
							"actual_end",
							"progress",
							"parent_job_task",
							"job_task_line_dependency",
							"description",
							"job_task_status",
							"reader_support",
							"JobNo",
							"Processor",
							"Reader_Support_ID",
							"Task_Color"
	);
	public function getalljobs($user_id,$user_name){
		
		// get all positions of the user

		$raw_query = " SELECT position FROM `tbuser` WHERE id = '{$user_id}' and is_active=1";
		$positions = $this->database->query($raw_query);

		// get all the department codes
		$raw_query = " SELECT department_code FROM `tbdepartment_users` WHERE user_id = '{$user_id}' and is_active = 1";
		$departments = $this->database->query($raw_query);

		// get all groups
		$raw_query = " SELECT group_id FROM `gs3_erp.tbform_groups_users` where user_id = '{$user_id}' and is_active=1";
		$groups = $this->database->query($raw_query);

		$raw_query = " SELECT form_authors,form_viewers,form_admin FROM `gs3_erp.tb_workspace` where id = 309";
		$workspace = $this->database->query($raw_query);
		$authors = json_decode($workspace['form_authors']);
		$viewers = json_decode($workspace['form_viewers']);
		$admin = json_decode($workspace['form_admin']);

		//check if user_id is in form_authors user list

		$hasAccess = false;

		if (in_array($user_id,$authors['users']))
		{
			$hasAccess = true;
		}
		if (in_array($user_id,$viewers['users']))
		{
			$hasAccess = true;
		}
		if (in_array($user_id,$admin['users']))
		{
			$hasAccess = true;
		}

		foreach ($positions as $value) {
			if (in_array($value,$authors['positions']) || in_array($value,$viewers['positions']) || in_array($value,$admin['positions'])){
				$hasAccess = true;
				break;
			}	
		}

		foreach ($departments as $value) {
			if (in_array($value['department_code'],$authors['departments']) || in_array($value['department_code'],$viewers['departments']) || in_array($value['department_code'],$admin['departments'])){
				$hasAccess = true;
				break;
			}	
		}

			foreach ($groups as $value) {
			if (in_array($value,$authors['groups']) || in_array($value,$viewers['groups']) || in_array($value,$admin['groups'])){
				$hasAccess = true;
				break;
			}	
		}
			


		if ($hasAccess){
			$raw_query = "	SELECT
							 No as job_no,
							 description as description
						FROM `20_tbl_jobs`";
		}
		else{

			$raw_query = "	SELECT
							 No as job_no,
							 description as description
						FROM `20_tbl_jobs`
						WHERE
							(Processor = '{$user_id}' 
							OR
							reader_support_id = '{$user_id}'
							OR
							person_responsible = '{$user_name}')
						";

		}

		return $this->database->query($raw_query);
	}

	public function ConnectJobsJobTask ($user_id,$user_name){
		$raw_query = "	SELECT
							-- * 
							JoBT.TrackNo as track_no,
							JoBT.job_task_no as job_task_no,
							JoBT.estimated_start as estimated_start,
							JoBT.estimated_end as estimated_end,
							JoBT.actual_start as actual_start,
							JoBT.actual_end as actual_end,
							JoBT.progress as progress,
							JoBT.parent_job_task as parent_job_task,
							JoBT.job_task_line_dependency as job_task_line_dependency,
							JoBT.description as description,
							JoBT.job_task_status as job_task_status,
							JoBT.reader_support as reader_support,
							JoBT.JobNo as job_no,
							JoBT.Processor as processor
						FROM 
							20_tbl_jobtasklines as JoBT 
						RIGHT JOIN 
							20_tbl_jobs as JoB
						ON
							JoBT.JobNo = Job.No
						WHERE
							JoBT.Processor = '{$user_id}' 
							OR
							JoBT.reader_support_id = '{$user_id}'
							OR
							JoBT.Person_Responsible= '{$user_name}'
					";
		return $this->database->query($raw_query);
	}

	public function getAllJobTask ($user_id,$user_name){
		$raw_query = "	SELECT 
							TrackNo as track_no,
							job_task_no as job_task_no,
							estimated_start as estimated_start,
							estimated_end as estimated_end,
							actual_start as actual_start,
							actual_end as actual_end,
							progress as progress,
							parent_job_task as parent_job_task,
							job_task_line_dependency as job_task_line_dependency,
							description as description,
							job_task_status as job_task_status,
							reader_support as reader_support,
							JobNo as job_no,
							Processor as processor,
							Task_Color as task_color,
							person_responsible as person_responsible
						FROM 
							20_tbl_jobtasklines 
						WHERE
							(Processor = '{$user_id}' 
							OR
							reader_support_id = '{$user_id}'
							OR
							person_responsible = '{$user_name}')
					";
		return $this->database->query($raw_query);
	}

	public function getJobTaskBy ($user_id,$user_name,$field,$field_value){
		$raw_query = "	SELECT 
							TrackNo as track_no,
							job_task_no as job_task_no,
							estimated_start as estimated_start,
							estimated_end as estimated_end,
							actual_start as actual_start,
							actual_end as actual_end,
							progress as progress,
							parent_job_task as parent_job_task,
							job_task_line_dependency as job_task_line_dependency,
							description as description,
							job_task_status as job_task_status,
							reader_support as reader_support,
							JobNo as job_no,
							Processor as processor,
							Task_Color as task_color,
							person_responsible as person_responsible
						FROM 
							20_tbl_jobtasklines 
						WHERE
							-- (Processor = '{$user_id}' 
							-- OR
							-- reader_support_id = '{$user_id}'
							-- OR
							-- person_responsible = '{$user_name}')
							-- AND
							{$field} = '{$field_value}'";
		return $this->database->query($raw_query);
	}

	public function getAllJobTaskByAdmin(){
		$raw_query = "	SELECT 
							TrackNo as track_no,
							job_task_no as job_task_no,
							estimated_start as estimated_start,
							estimated_end as estimated_end,
							actual_start as actual_start,
							actual_end as actual_end,
							progress as progress,
							parent_job_task as parent_job_task,
							job_task_line_dependency as job_task_line_dependency,
							description as description,
							job_task_status as job_task_status,
							reader_support as reader_support,
							JobNo as job_no,
							Processor as processor
						FROM 
							20_tbl_jobtasklines";
		return $this->database->query($raw_query);
	}

	public function getJobTaskByAdminBy($param1,$param2){
		$raw_query = "	SELECT 
							TrackNo as track_no,
							job_task_no as job_task_no,
							estimated_start as estimated_start,
							estimated_end as estimated_end,
							actual_start as actual_start,
							actual_end as actual_end,
							progress as progress,
							parent_job_task as parent_job_task,
							job_task_line_dependency as job_task_line_dependency,
							description as description,
							job_task_status as job_task_status,
							reader_support as reader_support,
							JobNo as job_no,
							Processor as processor
						FROM 
							20_tbl_jobtasklines
						WHERE
							{$param1} = '{$param2}'";
		return $this->database->query($raw_query);
	}
}   