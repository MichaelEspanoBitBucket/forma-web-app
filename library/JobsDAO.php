<?php


class JobsDAO {
	protected $database;
    public function __construct() {
        $this->database = new Database();
    }
    
	public function getAllJobs() {
	 	$raw_query = '	SELECT 
	 						* 
	 					FROM 
	 						`20_tbl_jobs`';
	 	return $this->database->query($raw_query);
	}

	public function getJobsByTrackNo($trackno){
		$raw_querty = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs` 
							WHERE 
							`TrackNo` = '{$trackno}'";
		return $this->database->query($raw_query);
	}

	public function getJobsByRequestor($reqtor){
		$raw_query = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs` 
						WHERE 
							`Requestor` = '{$reqtor}'";
		return $this->database->query($raw_query);
	}
	
	public function getJobsByProcessor($processor){
		$raw_query = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs` 
						WHERE 
							`Processor` = '{$processor}'";
		return $this->database->query($raw_query);
	}

	public function getJobsByStatus($stats){
		$raw_query = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs` 
						WHERE 
							`Status` = '{$stats}'";
		return $this->database->query($raw_query);
	}

	public function getJobsByNo($number){
		$raw_query = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs`
						WHERE 
							`No` = '{$number}'";
		return $this->database->query($raw_query);
	}

	public function getJobsByJobType($Job_Type){
		$raw_query = "	SELECT 
							*
						FROM 
							`20_tbl_jobs`
						WHERE 
							`Job_Type` = '{$Job_Type}'";
		return $this->database->query($raw_query);
	}

	public function getJobsByCustCode($CustCode){
		$raw_query = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs` 
						WHERE 
							`Customer` = '{$CustCode}'";
		return $this->database->query($raw_query);
	}
	
	public function getJobsByCustCode($CustCode){
		$raw_query = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs` 
						WHERE 
							`Customer` = '{$CustCode}'";
		return $this->database->query($raw_query);
	}
	
	public function getJobsBySalesOrder ($SalesOrder) {
		$raw_query = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs` 
						WHERE 
							`Sales_Order` = '{$SalesOrder}'";
		return $this->database->query($raw_query);		
	}

	public function getJobsByEstiCost ($GEsticost1,$LEsticost2) {
		$raw_query = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs` 
						WHERE 
							`Estimated_Cost` <= '{$LEsticost2}' 
						AND
							`Estimated_Cost` >= '{$GEsticost1}'";
		return $this->database->query($raw_query);		
	}

	public function getJobsByEstiDates ($GEsticost1,$LEsticost2) {
		$raw_query = "	SELECT 
							* 
						FROM 
							`20_tbl_jobs` 
						WHERE 
							`Starting_Date` <= '{$LEsticost2}' 
						AND
							`Ending_Date` >= '{$GEsticost1}'";
		return $this->database->query($raw_query);		
	}

}
