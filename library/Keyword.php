<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Keyword
 *
 * @author Jewel Tolentino
 */
class Keyword extends Formalistics {

    //put your code here

    public $code;
    public $description;
    public $value_code;
    public $value_description;
    public $quantity;
    public $company;
    public $date_created;
    public $date_updated;
    public $created_by;
    public $updated_by;
    public $is_active;

    public function __construct($db, $id) {
        $this->tblname = 'tbkeyword';
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();
        if ($id) {
            $result = $redis_client->query("keyword_object_" . $id, "SELECT * FROM $this->tblname WHERE id = {$this->db->escape($id)}");

            $this->id = $result['id'];
            $this->code = $result['code'];
            $this->description = $result['description'];
            $this->value_code = $result['valuecode'];
            $this->value_description = $result['valuedescription'];
            $this->quantity = $result['quantity'];
            $this->company = new Company($this->db, $result['company_id']);
            $this->date_created = $result['date_created'];
            $this->date_updated = $result['date_updated'];
            $this->created_by = new Person($this->db, $result['created_by']);
            $this->updated_by = new Person($this->db, $result['updated_by']);
            $this->is_active = $result['is_active'];
        }
    }

    public function save() {
        $insert_array = array(
            "code" => $this->code,
            "description" => $this->description,
            "valuecode" => $this->value_code,
            "valuedescription" => $this->value_description,
            "quantity" => $this->quantity,
            "company_id" => $this->company->id,
            "date_created" => $this->date_created,
            "created_by" => $this->created_by->id,
            "is_active" => $this->is_active
        );

        $insert_id = $this->db->insert($this->tblname, $insert_array);

        $this->id = $insert_id;
    }

    public function update() {
        $update_array = array(
            "code" => $this->code,
            "description" => $this->description,
            "valuecode" => $this->value_code,
            "valuedescription" => $this->value_description,
            "quantity" => $this->quantity,
            "company_id" => $this->company->id,
            "date_updated" => $this->date_updated,
            "updated_by" => $this->updated_by->id,
            "is_active" => $this->is_active
        );

        $condition_array = array("id" => $this->id);
        $this->db->update($this->tblname, $update_array, $condition_array);
    }

}
