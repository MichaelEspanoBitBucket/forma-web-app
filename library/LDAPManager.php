<?php

/**
 * Singleton manager for access to LDAP. Please use the LDAPManager::getInstance() 
 * for getting an instance of LDAP manager
 *
 * @author ervinne
 */
class LDAPManager {

    private $host;
    private $activeConnection;
    private static $instance = NULL;

    /** @return LDAPManager */
    public static function getInstance() {
        if (LDAPManager::$instance === NULL) {
            LDAPManager::$instance = new LDAPManager();
        }
        return LDAPManager::$instance;
    }

    private function __construct() {
        //  explicit definition of private constructor to restrict access as singleton
        //  also, setup the instance if host and base dn is defined in the settings
        if (defined('LDAP_HOST')) {
            $this->setHost(LDAP_HOST);
        }
    }

    /**
     * 
     * @param type $host the host of ldap server to connect to.
     * @param type $basedn your basedn formatted domain, 
     *  ex gs3.com.ph will be formatted to DC=gs3,DC=com,DC=ph
     */
    public function setHost($host) {
        $this->host = $host;
    }

	public function getActiveConnection() {
		return $this->activeConnection;
	}
	
    public function login($username, $password) {
        $results = array(
            'error' => null,
            'error_message' => null,
            'results' => null
        );

        if (!function_exists('ldap_connect')) {
            $results['error'] = 'Incomplete Setup';
            $results['error_message'] = 'ldap_connect function missing';
            return $results;
        }
        
        if (!$this->host) {
            $results['error'] = 'Incomplete Setup';
            $results['error_message'] = 'LDAP host is not yet defined';
            return $results;
        }

        if (!$this->activeConnection = ldap_connect($this->host)) {
            $results['error'] = 'LDAP Error';
            $results['error_message'] = 'Failed to connect to the LDAP server: ' . $this->host;
        }
        
	ldap_set_option($this->activeConnection, LDAP_OPT_REFERRALS, 0);
        if (!ldap_set_option($this->activeConnection, LDAP_OPT_PROTOCOL_VERSION, 3)) {
            $results['error'] = 'LDAP Error';
            $results['error_message'] = 'Unable to set LDAP protocol version';
        }

        $bindResults = ldap_bind($this->activeConnection, $username, $password);
        
        if ($bindResults) {
            $results['results'] = 'Login Success';
        } else {
            if ('Invalid credentials' == ldap_error($this->activeConnection)) {
                $results['error'] = 'Authentication Error';
                $results['error_message'] = 'Invalid Username or Password';
            } else {
                $results['error'] = 'LDAP Error';
                $results['error_message'] = ldap_error($this->activeConnection);
            }
        }

        return $results;
    }

	public function searchUsers($ldap_base_dn, $search_filter) {
		return ldap_search($$this->activeConnection, $ldap_base_dn, $search_filter);
	}
	
    public function disconnect() {
        if ($this->activeConnection) {
            ldap_unbind($this->activeConnection);
        }
    }

}
