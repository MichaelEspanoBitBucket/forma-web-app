<?php

class Layout extends Auth {

    protected $_view_file = "";

    protected $loaded_as_partial_view = true;

    protected $partial_view_data = null;

    public static function getParams() {
        $params     = explode("/", $_SERVER["REQUEST_URI"]);
        $new_params = array();
        foreach ($params as $key => $param) {
            if (!empty($param)) {
                //allow GET params
                $pos = strpos($param, "?");
                if ($pos)
                    $param = substr($param, 0, $pos);
                $new_params[] = $param;
            }
        }
        return $new_params;
    }

    public function renderContent() {
        $uri = self::getParams();
        if (!empty($uri)) {
            $controller_path = realpath(dirname(__FILE__) . "/../modules/" . $uri[0] . "/controller/");
            $view_path = realpath(dirname(__FILE__) . "/../modules/" . $uri[0] . "/view/");
            if (file_exists($controller_path) && file_exists($view_path)) {
                if (isset($uri[1])) {
                    $db = new Database();
                    $user = explode("/", $_SERVER["REQUEST_URI"]);
                    //$search_user = explode("_",$user[2]);
                    $name = str_replace("_", " ",$user[2]);
                    //$lname = str_replace("%20"," ",$search_user[0]);
                    //$fname = str_replace("%20"," ",$search_user[1]);
                    $u = $db->query("SELECT * FROM tbuser WHERE display_name={$db->escape($name)}","row");
                    $personDoc = new Person($db, $u['id']);
                    $auth = $this->getAuth('current_user');
                    $authCompanyID = $auth['company_id'];
                    if ($personDoc->id != "") {
                        $c = realpath($controller_path . "/profile.php");
                        $v = realpath($view_path . "/profile.phtml");
                    } else {
                        $c = realpath($controller_path . "/" . $uri[1] . ".php");
                        $v = realpath($view_path . "/" . $uri[1] . ".phtml");
                    }


                    if (file_exists($c) && file_exists($v)) {
                        $this->_view_file = $v;
                        include_once($c);
                    } else {
                        //Error Page
                        $this->_view_file = realpath(dirname(__FILE__) . "/../modules/error/view/index.phtml");
                        include_once(realpath(dirname(__FILE__) . "/../modules/error/controller/index.php"));
                    }
                } else {
                    //Automatic detection of index
                    $this->_view_file = realpath("{$view_path}/index.phtml");
                    include_once(realpath("{$controller_path}/index.php"));
                }
            } else {
                $db = new Database();
                $user = explode("/", $_SERVER["REQUEST_URI"]);
                //$search_user = explode("_",$user[2]);
                $name = str_replace("_", " ",$user[2]);
                //$lname = str_replace("%20"," ",$search_user[0]);
                //$fname = str_replace("%20"," ",$search_user[1]);
                $u = $db->query("SELECT * FROM tbuser WHERE display_name={$db->escape($name)}","row");
                $personDoc = new Person($db, $u['id']);
                $auth = $this->getAuth('current_user');
                $authCompanyID = $auth['company_id'];
                if ($u['company_id'] != $authCompanyID) {
                    //Error Page
                    $this->_view_file = realpath(dirname(__FILE__) . "/../" . "/modules/error/view/index.phtml");
                    include_once(realpath(dirname(__FILE__) . "/../modules/error/controller/index.php"));
                } elseif ($personDoc->id != "") {
                    $this->_view_file = realpath(dirname(__FILE__) . "/../modules/profile/view/index.phtml");
                    include_once(realpath(dirname(__FILE__) . "/../modules/profile/controller/index.php"));
                } else {
                    //Error Page
                    $this->_view_file = realpath(dirname(__FILE__) . "/../" . "/modules/error/view/index.phtml");
                    include_once(realpath(dirname(__FILE__) . "/../modules/error/controller/index.php"));
                }
            }
        } else {
            //Home Page
            $this->_view_file = realpath(dirname(__FILE__) . "/../" . "/modules/index/view/index.phtml");
            include_once(realpath(dirname(__FILE__) . "/../modules/index/controller/index.php"));
        }
    }

    public function detectLoginModuleExist() {
        $uri = self::getParams();
        if (!empty($uri)) {
            $controller_path = realpath(dirname(__FILE__) . "/../modules/" . $uri[0] . "/controller/");
            $view_path = realpath(dirname(__FILE__) . "/../modules/" . $uri[0] . "/view/");
            if (!file_exists($controller_path) && !file_exists($view_path)) {
                //Error Page
                header('Location: /');
            }
        }
    }

    private function content() {
        include_once($this->_view_file);
    }

    private function header($header = "header") {
        $layout = realpath(dirname(__FILE__) . "/../layout/{$header}.phtml");
        if (file_exists($layout)) {
            include_once($layout);
        }
    }

    private function footer($footer = "footer") {
        $layout = realpath(dirname(__FILE__) . "/../layout/{$footer}.phtml");
        if (file_exists($layout)) {
            include_once($layout);
        }
    }

    private function setLayout($layout = "") {
        $auth = Auth::getAuth("current_user");
        $captcha_auth = Auth::getAuth("captcha_code_proceed");
        if (empty($layout))
            if(ALLOW_LOGIN_CAPTCHA == 1){
                if($auth['user_level_id'] == 3 && $captcha_auth != "1"){
                    include_once(realpath(dirname(__FILE__) . "/../" . "/layout/captcha.phtml"));
                }else{
                    include_once(realpath(dirname(__FILE__) . "/../" . "/layout/default.phtml"));
                }
            }else{
                include_once(realpath(dirname(__FILE__) . "/../" . "/layout/default.phtml"));
            }
            
        else
            include_once(realpath(dirname(__FILE__) . "/../" . "/layout/{$layout}.phtml"));
    }

    public function setInclude($call) {
        include_once(realpath(dirname(__FILE__) . "/../" . "/layout/{$call}.phtml"));
    }

    public function includeContent($call) {
        include_once(realpath(dirname(__FILE__) . "/../" . "/modules/ajax/controller/{$call}.php"));
    }

    public function renderAsPartialView($path, $data = array()) {
        $this->loaded_as_partial_view = true;
        $this->partial_view_data = $data;        
        include(APPLICATION_PATH . $path);
    }  

}
