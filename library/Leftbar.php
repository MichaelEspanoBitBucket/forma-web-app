<?php

class Leftbar {

    public function leftbar_navigation($auth, $type,$other) {
        // $type = $other['icon-type']; // Dashboard or Current
        $session = new Auth();
        $ret = "";

        $mod = unserialize(ENABLE_COMPANY_MOD);
        $lite = unserialize(LITE_VERSION);
        $properties = unserialize(PARENT_MODULE_PROPERTIES);


        if ($auth['user_level_id'] == "1" || $auth['user_level_id'] == "2") {
            $links_info = unserialize(GET_COMPANY_URL_LINKS);
            $level = "admin";
        } else if ($auth['user_level_id'] == "3") {
            $links_info = unserialize(GET_COMPANY_URL_LINKS_ORDINARY);
            $level = "ordinary";
        } else {
            $links_info = unserialize(GET_COMPANY_URL_LINKS_GUEST);
            $level = "guest";
        }

        $arrs = $mod;
        $array_sub = array();
        // var_dump($arrs);
        $ret .= '<ul id="fl-infinite-nav" class="">';
        foreach ($arrs as $arr_first => $arr_vals) {

            if (!is_array($arr_vals)) {
                if ($arr_vals == "1" && $arr_first != "navigation") {
                    if ($links_info[$arr_first]['type'] != "click") {

                        if ($links_info[$arr_first]['root_user_view'] == "0") {
                            $url = 'href="/user_view/' . $links_info[$arr_first]['url'] . '" ';
                        } else {
                            // echo $links_info[$arr_first]['root_user_view'];
                            $url = 'href="/' . $links_info[$arr_first]['url'] . '" ';
                        }
                    } else {
                        $url = "";
                    }
                    if ($type == "icon_label") {
                        if ($links_info[$arr_first]['name'] != "") {
                            /* $ret .= '<li>';
                              $ret .= '<svg class="icon-svg-main-nav" viewBox="0 0 100.264 100.597">';
                              $ret .= '<use xlink:href="'.$links_info[$arr_first]['navigation']['parent_icon'].'"></use>';
                              $ret .= '</svg>';
                              $ret .= '<a '.$url.' class="'.$links_info[$arr_first]['class'].'"> '.ucwords(str_replace("_"," ", $links_info[$arr_first]['name'])).'</a>';
                              $ret .= '</li>'; */

                            $ret .= '<li>';
                            /*    $ret .= '<svg class="icon-svg-main-nav" viewBox="0 0 100.264 100.597">';
                              $ret .= '<use xlink:href="'.$links_info[$arr_first]['navigation']['parent_icon'].'"></use>';0 0 100.264 100.597
                              $ret .= '</svg>'; */
                            $ret .= '<a ' . $url . ' class="' . $links_info[$arr_first]['class'] . '"> <svg class="icon-svg-main-nav" viewBox="'.$links_info[$arr_first]['navigation']['view_box'].'"><use xlink:href="' . $links_info[$arr_first]['navigation']['parent_icon'] . '"></use></svg> ' . ucwords(str_replace("_", " ", $links_info[$arr_first]['name'])) . '</a>';
                            $ret .= '</li>';
                        }
                    } else {
                        // SVG
                        if ($links_info[$arr_first]['name'] != "") {
                            $ret .= '<li>';
                            $ret .= '<a ' . $url . ' custom-data-tooltip="' . ucwords(str_replace("_", " ", $links_info[$arr_first]['name'])) . '" data-placement="right" class="' . $links_info[$arr_first]['class'] . ' fl-admin-leftbar3-style">';
                            $ret .= '<svg class="icon-svg-main-nav-icon-only" viewBox="'.$links_info[$arr_first]['navigation']['view_box'].'">';
                            $ret .= '<use xlink:href="' . $links_info[$arr_first]['navigation']['parent_icon'] . '"></use>';
                            $ret .= '</svg>';
                            $ret .= '</a>';

                            $ret .= '</li>';
                        }
                    }
                }
            }



            $modules = "<ul class='fl-infinite-nav-sub loading_navigation'>";
            $modules .= '<li>';
            $modules .= '<a href="#"><div align="center"><img src="/images/icon/loading.gif">';
            $modules .= '<p style="font-size:10px;">Loading Content...</p></div>';
            $modules .= '</a>';
            $modules .= '</li>';
            $modules .= '</ul>';

            //
            $load = 'true';
            $redis_cache = getRedisConnection();
            if ($redis_cache) {
                $cachedFormatNav = $auth['id'];
                $cache_nav = json_decode($redis_cache->get("leftbar_nav"), true);
                $cached_query_result_nav = $cache_nav['' . $cachedFormatNav . ''];


                if ($cached_query_result_nav) {
                    $modules = $cached_query_result_nav;

                    $load = 'false';
                }
            } else {
                $getLeftBarSession = $session->hasAuth('getLeftBar');

                if ($getLeftBarSession) {
                    $modules = html_entity_decode($session->getAuth('getLeftBar'));

                    $load = 'false';
                }
            }

            if ($type == "icon_label") {

                if ($arr_first == "navigation" && $arr_vals == "1") {
                    $ret .= '<li class="menu-context getSideBarNavigation displayChild" load="' . $load . '">';
                    /*  $ret .= '<svg class="icon-svg-main-nav" viewBox="0 0 100.264 100.597">';
                      $ret .= '<use xlink:href="#svg-icon-modules"></use>';
                      $ret .= '</svg>'; */
                    $ret .= '<a href="#"><svg class="icon-svg-main-nav" viewBox="0 0 100.264 100.597"><use xlink:href="#svg-icon-modules"></use></svg> Navigation</a>';
                    $ret .= '<strong class="fl-nav-down-icon" style="background-color:transparent;">';
                    $ret .= '<i class="fa fa-chevron-right" style="color:#7D7878;"></i>';
                    $ret .= '</strong>';
                    $ret .= $modules;
                    $ret .= '</li>';
                }
            } else {
                if ($arr_first == "navigation" && $arr_vals == "1") {
                    $ret .= '<li class="menu-context getSideBarNavigation displayChild" load="' . $load . '">';
                    $ret .= '<a href="#" custom-data-tooltip="Navigation" data-placement="right" class="fl-admin-leftbar3-style">';
                    $ret .= '<svg class="icon-svg-main-nav-icon-only" viewBox="0 0 100.264 100.597">';
                    $ret .= '<use xlink:href="#svg-icon-modules"></use>';
                    $ret .= '</svg>';
                    $ret .= '</a>';
                    $ret .= '<strong class="fl-nav-down-icon" style="background-color:transparent;top: 15px; padding: 1px;  margin-right: 3px;">';
                    $ret .= '<i class="fa fa-chevron-right" style="color:#7D7878;"></i>';
                    $ret .= '</strong>';
                    $ret .= $modules;
                    $ret .= '</li>';
                }
            }


// var_dump(expression)
// echo $arr_first;
// var_dump($properties[$arr_first]);
            if ($properties[$arr_first . "_" . $level]['properties']['user'] == $level) {

                if ($arr_first != "others") {
                    if (count($arr_vals) != 1) {
                        if ($properties[$arr_first . "_" . $level]['properties']['visibility'] != true) {
                            if ($type == "icon_label") {
                                $li = "";
                                $li .= ' <li>';
                                // $ret .= '<svg class="icon-svg-main-nav" viewBox="0 0 100.264 100.597">';
                                //     $ret .= '<use xlink:href="'.$properties[$arr_first]['properties']['icon'].'"></use>';
                                // $ret .= '</svg>';

                                

                                    $li .= '<a href="#"><svg class="icon-svg-main-nav" viewBox="'.$properties[$arr_first . "_" . $level]['properties']['view_box'].'"><use xlink:href="' . $properties[$arr_first . "_" . $level]['properties']['icon'] . '"></use></svg> ' . ucwords(str_replace("_", " ", $arr_first)) . '</a>';
                                    $li .= '<strong class="fl-nav-down-icon" style="background-color:transparent;">';
                                    $li .= '<i class="fa fa-chevron-right" style="color:#7D7878;"></i>';
                                    $li .= '</strong>';
                                
                                
                                $li .= '<ul class="fl-infinite-nav-sub">';
                                foreach ($arr_vals as $arr_vals_first => $arr_vals_vals) {
                                    $removeHashSvg = str_replace('#', '', $links_info[$arr_vals_first]['navigation']['icon']);
                                    if ($links_info[$arr_vals_first]['type'] != "click") {
                                        if ($links_info[$arr_vals_first]['root_user_view'] == "0") {
                                            $url = 'href="/user_view/' . $links_info[$arr_vals_first]['url'] . '" ';
                                        } else {
                                            // echo $links_info[$arr_first]['root_user_view'];
                                            $url = 'href="/' . $links_info[$arr_vals_first]['url'] . '" ';
                                        }
                                    } else {
                                        $url = "";
                                    }
                                    if(GO_LITE == 1){
                                        if ($arr_vals_vals == "1") {
                                            // echo "1";
                                            if($links_info[$arr_vals_first]['name'] != ""){
                                                $li .= '<li>';
                                                /* $ret .= '<svg class="icon-svg-main-nav-sub-nav '.$removeHashSvg.'" viewBox="0 0 100.264 100.597">';
                                                  $ret .= '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'.$links_info[$arr_vals_first]['navigation']['icon'].'"></use>';
                                                  $ret .= '</svg>'; */
                                                $li .= '<a ' . $url . ' class="' . $links_info[$arr_first]['class'] . '"><svg class="icon-svg-main-nav-sub-nav ' . $removeHashSvg . '" viewBox="'.$links_info[$arr_vals_first]['navigation']['view_box'].'"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . $links_info[$arr_vals_first]['navigation']['icon'] . '"></use></svg> ' . $links_info[$arr_vals_first]['name'] . '</a>';
                                                $li .= '</li>';
                                            }
                                        }
                                    }else{
                                        if(!in_array($arr_vals_first, $lite)){
                                            if ($arr_vals_vals == "1") {
                                                // echo "1";
                                                if($links_info[$arr_vals_first]['name'] != ""){
                                                    $li .= '<li>';
                                                    /* $ret .= '<svg class="icon-svg-main-nav-sub-nav '.$removeHashSvg.'" viewBox="0 0 100.264 100.597">';
                                                      $ret .= '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'.$links_info[$arr_vals_first]['navigation']['icon'].'"></use>';
                                                      $ret .= '</svg>'; */
                                                    $li .= '<a ' . $url . ' class="' . $links_info[$arr_first]['class'] . '"><svg class="icon-svg-main-nav-sub-nav ' . $removeHashSvg . '" viewBox="'.$links_info[$arr_vals_first]['navigation']['view_box'].'"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . $links_info[$arr_vals_first]['navigation']['icon'] . '"></use></svg> ' . $links_info[$arr_vals_first]['name'] . '</a>';
                                                    $li .= '</li>';
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                                $li .= '</ul>';
                                $li .= '</li>';

                                // Condition
                                if(GO_LITE == 1){
                                    $ret .= $li;
                                }else{
                                    if(!in_array($arr_first, $lite)){
                                        $ret .= $li;
                                    }
                                }
                                
                            } else {
                                
                                $li = "";

                                $li .= '<li>';
                                $li .= '<a  custom-data-tooltip="';
                                
                                $li .= ucwords(str_replace("_", " ", $arr_first));
                               
                                $li .=  '" data-placement="right" class="fl-admin-leftbar3-style">';
                                $li .= '<svg class="icon-svg-main-nav-icon-only" viewBox="'.$properties[$arr_first . "_" . $level]['properties']['view_box'].'">';
                                $li .= '<use xlink:href="' . $properties[$arr_first . "_" . $level]['properties']['icon'] . '"></use>';
                                $li .= '</svg>';
                                $li .= '</a>';
                                
                                $li .= '<strong class="fl-nav-down-icon" style="background-color:transparent;top: 15px; padding: 1px;  margin-right: 3px;">';
                                $li .= '<i class="fa fa-chevron-right" style="color:#7D7878;"></i>';
                                $li .= '</strong>';
                               
                                
                                $li .= '<ul class="fl-infinite-nav-sub">';
                                foreach ($arr_vals as $arr_vals_first => $arr_vals_vals) {
                                    if ($links_info[$arr_vals_first]['type'] != "click") {
                                        if ($links_info[$arr_vals_first]['root_user_view'] == "0") {
                                            $url = 'href="/user_view/' . $links_info[$arr_vals_first]['url'] . '" ';
                                        } else {
                                            // echo $links_info[$arr_first]['root_user_view'];
                                            $url = 'href="/' . $links_info[$arr_vals_first]['url'] . '" ';
                                        }
                                    } else {
                                        $url = "";
                                    }
                                    // For Lite
                                    if(GO_LITE == 1){
                                        
                                        if ($arr_vals_vals == "1") {
                                            if($links_info[$arr_vals_first]['name'] != ""){
                                                $li .= '<li>';
                                                /* $ret .= '<svg class="icon-svg-main-nav-sub-nav '.$removeHashSvg.'" viewBox="0 0 100.264 100.597">';
                                                  $ret .= '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'.$links_info[$arr_vals_first]['navigation']['icon'].'"></use>';
                                                  $ret .= '</svg>'; */
                                                
                                                $li .= '<a ' . $url . ' class="' . $links_info[$arr_first]['class'] . '"><svg class="icon-svg-main-nav-sub-nav ' . $removeHashSvg . '" viewBox="'.$links_info[$arr_vals_first]['navigation']['view_box'].'"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . $links_info[$arr_vals_first]['navigation']['icon'] . '"></use></svg> ' . $links_info[$arr_vals_first]['name'] . '</a>';
                                                
                                                $li .= '</li>';
                                            }
                                        }
                                    }else{
                                        if(!in_array($arr_vals_first, $lite)){
                                            if ($arr_vals_vals == "1") {
                                                if($links_info[$arr_vals_first]['name'] != ""){
                                                    $li .= '<li>';
                                                    /* $ret .= '<svg class="icon-svg-main-nav-sub-nav '.$removeHashSvg.'" viewBox="0 0 100.264 100.597">';
                                                      $ret .= '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'.$links_info[$arr_vals_first]['navigation']['icon'].'"></use>';
                                                      $ret .= '</svg>'; */
                                                    
                                                    $li .= '<a ' . $url . ' class="' . $links_info[$arr_first]['class'] . '"><svg class="icon-svg-main-nav-sub-nav ' . $removeHashSvg . '" viewBox="'.$links_info[$arr_vals_first]['navigation']['view_box'].'"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="' . $links_info[$arr_vals_first]['navigation']['icon'] . '"></use></svg> ' . $links_info[$arr_vals_first]['name'] . '</a>';
                                                    
                                                    $li .= '</li>';
                                                }
                                            }
                                        }
                                    }
                                    
                                }
                                $li .= '</ul>';
                                $li .= '</li>';

                                // COndition
                                if(GO_LITE == 1){
                                    $ret .= $li;
                                }else{
                                    if(!in_array($arr_first, $lite)){
                                        $ret .= $li;
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
        if ($type == "icon_label") {
            $ret .= '<div class="fl-copyright">';
            $ret .= '<p>&copy; Copyright @ ' . date("Y") . ' All rights reserved. Formalistics Version ' . SOFTWARE_VERSION . '</p>';

            if(GO_LITE == 0){
                $ret .= '<p>Lite Version</p>';
            }

            $ret .= '</div>';
        }
        $ret .= '</ul>';

        return $ret;
    }

}

?>