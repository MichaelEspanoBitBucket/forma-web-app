<?php

class ListViewFormula {

    public function setValue($json) {
        $fs = new functions;
        if (gettype($json) == "string") {
            $json = json_decode($json, true);
        } else {
            $json = json_decode(json_encode($json), true);
        }

        $data = $json['value'];

        /* Formula, Format, Visibility etc.. */

        //Visibility -> if not visbile return the data with empty string
        //Formula
        //Format
        if ($json['field_input_type'] == "Currency" || $json['input_type'] == "Currency") {
            $data = $this->currency($json['value']);
        } else {
            if ($json['field_type'] == "attachment_on_request" || $json['type'] == "attachment_on_request") {
                $data = explode("/", $data);
                $data = $data[count($data) - 1];
            } else if ($json['field_type'] == "multiple_attachment_on_request" || $json['type'] == "multiple_attachment_on_request") {
                $data = json_decode(json_decode($data, true), true); //"Multiple Attachment";
                $string = "";
                if (gettype($data) == "array") {
                    foreach ($data as $key => $value) {
                        # code...
                        $string .= $value['file_name'] . " | ";
                    }
                } else {
                    $data = "String";
                }
                $string = substr($string, 0, strlen($string) - 2);
                $data = $string;
            } else {
                // Additional By Sam
                // For Decryption of Records
                $strlen = strlen($json['value']);
                if (in_array($json['field_name'], $json['encryption_descryption']['encrypt'])) {
                    if ($strlen >= 44) {
                        $data_to_encrypt = $fs->encrypt_decrypt("decrypt", $json['value']);
                        $data = htmlentities($data_to_encrypt);
                    } else if ($strlen < 44) {
                        $data = $json['value'];
                    } else {
                        $data = $json['value'];
                    }
                } else if (in_array($json['field_name'], $json['encryption_descryption']['decrypt'])) {
                    if ($strlen >= 44) {
                        $data_to_encrypt = $fs->encrypt_decrypt("decrypt", $json['value']);
                        $data = htmlentities($data_to_encrypt);
                    } else if ($strlen < 44) {
                        $data = $json['value'];
                    } else {
                        $data = $json['value'];
                    }
                } else {
                    $data = $json['value'];
                }
            }
        }

        return $data;
    }

    public function currency($str) {
        return number_format($str, 2);
    }

}

?>