<?php

class Logs extends Database{
	

	public function createMessage($type,$others){
		$action_description = "";


		if(SYSTEM_LOGS=="0"){
			return $action_description;
		}

		if($type=="2"){ // log in 
			$action_description = "logged in to the system";
		}else if($type=="3"){ // log out
			$action_description = "logged out to the system";
		}else if($type=="8"){ // register
			$createdUser = new Person($this,$others);
            $getPosition = $this->query("SELECT * from tbpositions WHERE id = '". $createdUser->position ."'","row");
            $action_description = "registered ".$createdUser->display_name." as ".$getPosition['position'];
		}else if($type=="42"){ // deactivate
			$editedUser = new Person($this,$others);
            $action_description = "deactivated ".$editedUser->display_name;
		}else if($type=="24"){ // create form
			if($others['category_id']=="0"){
				$action_description = "created ". $others['Title'] ." form, all user admin are automatically has an admin rights to this form.";
			}else{
				$action_description = "created ". $others['Title'] ." form with the following admin ";
				$getAdminUser = json_decode($others['jsonAdmin'],true);
				foreach ($getAdminUser['users'] as $others['user_id']) {
					$getUser = $this->query("SELECT display_name FROM tbuser WHERE id = '". $others['user_id'] ."'","row");
					$action_description.=$getUser['display_name'].", ";
				}
				$action_description = substr($action_description, 0,strlen($action_description)-2);
			}
		}else if($type=="25"){ // edit form
			if($others['category_id']=="0"){
				$action_description = "updated ". $others['Title'] ." form, all user admin are automatically has an admin rights to this form.";
			}else{
				$action_description = "updated ". $others['Title'] ." form with the following admin ";
				$getAdminUser = json_decode($others['jsonAdmin'],true);
				foreach ($getAdminUser['users'] as $others['user_id']) {
					$getUser = $this->query("SELECT display_name FROM tbuser WHERE id = '". $others['user_id'] ."'","row");
					$action_description.=$getUser['display_name'].", ";
				}
				$action_description = substr($action_description, 0,strlen($action_description)-2);
			}
		}else if($type=="26"){ // activate form
			$getForm = new Form($this,$others);
          	$action_description = "activated ". $getForm->form_name ." form";
		}else if($type=="27"){ // deactivate form
			$getForm = new Form($this,$others);
          	$action_description = "deactivated ". $getForm->form_name ." form";
		}else if($type=="34"){ // delete form
			$getForm = new Form($this,$others);
			$action_description = "deleted ". $getForm->form_name ." form";
		}else if($type=="29"){ // create workflow
			$form = new Form($this,$others['form_id']);
            $action_description = "created ". $others['title'] ." workflow for ".$form->form_name." form";
		}else if($type=="30"){ // activate workflow
			$getWorkflow = $this->query("SELECT * FROM tbworkflow WHERE id = '". $others ."'","row");
	        $getForm = new Form($this,$getWorkflow['form_id']);
	        $action_description = "activated ". $getWorkflow['title'] ." workflow for ".$getForm->form_name." form";
		}else if($type=="31"){ // deactivate workflow
			$getWorkflow = $this->query("SELECT * FROM tbworkflow WHERE id = '". $others ."'","row");
	        $getForm = new Form($this,$getWorkflow['form_id']);
	        $action_description = "deactivated ". $getWorkflow['title'] ." workflow for ".$getForm->form_name." form";
		}else if($type=="35"){ // delete workflow
			$getWorkflow = $this->query("SELECT * FROM tbworkflow WHERE id = '". $others ."'","row");
            $getForm = new Form($this,$getWorkflow['form_id']);
            $action_description = "deleted ". $getWorkflow['title'] ." workflow for ".$getForm->form_name." form";
		}
		return $action_description;


	}

	public function deleteLogs($date,$company_id){
		$this->query("DELETE logs FROM tbaudit_logs logs LEFT JOIN tbuser tbu ON logs.user_id = tbu.id  WHERE logs.date <= '$date' AND tbu.company_id='". $company_id ."'");
	}
}


?>