<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MSSQLDatabase
 * @author Jewel Tolentino
 */
class MSSQLDatabase extends DatabaseConnection {

    public function __construct($host, $db_name, $user_name, $password) {
        $this->type = "ms_sql";
        $this->connect($host, $db_name, $user_name, $password);
    }
}
