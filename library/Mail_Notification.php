<?php

class Mail_Notification extends userQueries {
    /*
     * Email Notification for user
     * @company_type = type of notificaton
     *      - 1 (Formalistics)
     *      - 2 (Company_Only)
     * @idType = {company ID} logged on the system
     *
     *
     */

    public function notify_user($type, $company_type, $userID, $user_type) {
        $serverName = $this->curPageURL("serverName");
        $auth = Auth::getAuth('current_user');
        $db = new Database();
        $fs = new functions();
        // Get User Information

        $userInfo = $db->query("SELECT *,u.id as userID,
                                   c.email as companyEmail,
                                   u.email as userEmail,
                                   c.id as companyID,
                                   p.position as userPosition,
                                   u.password as uPassword
                                   FROM tbuser u
                                   LEFT JOIN tbcompany c on u.company_id=c.id
                                   LEFT JOIN tbpositions p on u.position=p.id
                                   WHERE u.id={$db->escape($userID)}", "row");


        $password = $fs->encrypt_decrypt("decrypt", $userInfo['uPassword']);



        $mail_format .= '<div style="margin: 0px auto; width: 600px;">';
        $mail_format .= '<div style="padding: 5px;">';
        $mail_format .= '<br />';

        //
        //switch ($company_type) {
        //
        //    case "Formalistics":
        //        $mail_format .= '<img src="' . $serverName . '/images/logo/mainLogo.png" width="150" height="60">';
        //        break;
        //
        //    case "Company_Only":
        //        $mail_format .= $this->avatarPic("companyAdmin", $userInfo['companyID'], "150", "45", "Original", "");
        //        break;
        //}
        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        // Settings    
        //$mail_format .= '<img src="' . $serverName . '/images/themes/hr_blueline.gif">';
        $mail_format .= '<br /><br />';
        $mail_format .= '<div style="padding: 10px;background-color:#fff;width:98%;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
        $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';


        if ($type == "regCompany") {
            $mail_title = "Thank you for registering an account with " . COMPANY_NAME . "!";
            $from = SMTP_FROM_EMAIL;
            $from_name = SMTP_FROM_NAME;
            $to_title = $userInfo['name'];

            $activation_code = $this->encrypt_decrypt("encrypt", $userID);

            $mail_format .= "<h1 style='color: #206ba4;display: block;font-family: Arial;font-size: 34px;line-height:100%;font-weight: bold;text-align: left;'>Congratulations!</h1> ";
            $mail_format .= '<br />';
            $mail_format .= 'Thank you for registering with <b>' . COMPANY_NAME . '</b> (' . $userInfo['name'] . ').';
            $mail_format .= '<br /><br />';
            // $mail_format .= 'Your ' . COMPANY_NAME;

            $mail_format .= '<br />';
            $mail_format .= '<br />';
            $mail_format .= 'Your credentials:';
            $mail_format .= '<br />';
            $mail_format .= 'Login account: ' . $userInfo['companyEmail'];
            $mail_format .= '<br />';
            $mail_format .= 'Password: ' . $password;

            $mail_format .= '<br /><br />';
            $mail_format .= 'Please confirm your email address to receive important notifications, messages from your company, and request to approved from time to time.';
            $mail_format .= '<br /><br />';
            $mail_format .= 'To confirm your email address, click here or go to this URL: <br />';

            $mail_format .= '<a href="' . MAIN_PAGE . 'activate_account?m=Regist&a=Activate&id=' . $userID . '" target="_blank">';
            $mail_format .= MAIN_PAGE . 'activate_account?m=Regist&a=Activate&id=' . $userID;
            $mail_format .= '</a>';

            $mail_format .= '<br /><br />';
            $mail_format .= 'Note: Please make sure that the whole URL above is copied into the browser.';
            $mail_format .= '<br /><br />';
            $mail_format .= 'If you have additional questions and/or comments, please visit our website at <a href="http://www.gs3.com.ph/" target="_blank">http://www.gs3.com.ph/</a>';
            $mail_format .= '<br /><br />';

            $mail_format .= 'Sincerely,<br />';
            $mail_format .= COMPANY_NAME . " Team <br />";

            $mail_format .= '<br /><br />';
            $mail_format .= '<div style="border-top: 1px solid #CCC;"></div>';
            $mail_format .= '<br /><br />';
            $mail_format .= "<div style='background-color:#e4001b;width:100%;height:35px;'>";
            $mail_format .= "<div style='padding: 7px; text-align: center; color: #fff; font-size: 11px;'>";
            $mail_format .= "If you believe you have received this notification in error, please delete and disregard.";
            $mail_format .= "</div>";
            $mail_format .= "</div>";
            $mail_format .= '</div>';
        } elseif ($type == "regUser") {
            $mail_title = "Your Formalistics account has been successfully created.";
            $from = $auth['email'];
            $from_name = $auth['display_name'];
            $to_title = $auth['position'];

            $mail_format .= 'Dear ' . $userInfo['first_name'] . ' ' . $userInfo['last_name'] . ',';
            $mail_format .= '<br /><br />';
            $mail_format .= '<br />';
            $mail_format .= 'Your company admin has successfully created your account.';
            $mail_format .= '<br /><br /><br />';
            $mail_format .= 'You may now login to Formalistics using the credentials below:';
            $mail_format .= '<br />';
            $mail_format .= 'Email Address: ' . $userInfo['userEmail'];
            $mail_format .= '<br />';
            $mail_format .= 'Password: ' . $password;
            $mail_format .= '<br />';
            $mail_format .= '<br />';
            $mail_format .= '<br />';


            // $mail_format .= 'Your new <b>' . COMPANY_NAME . '</b> account was created successfully.';
            $mail_format .= 'We recommend that you change your new password upon successful login.';
            $mail_format .= '<br /><br />';
            $mail_format .= '<a href="' . MAIN_PAGE . '" target="_blank">Click here to login</a>';
            $mail_format .= '<br /><br /><br />';
            $mail_format .= 'Should you encounter any problem accessing your account, please contact your System Administrator for assistance.';
            $mail_format .= '<br /><br /><br />';
            $mail_format .= 'Thank you for using the Formalistics.';
            $mail_format .= '<br /><br />';
            $mail_format .= '<br /><br />';
            $mail_format .= '*This email is system generated. Do not reply.*';

            // $mail_format .= '<ul style="list-style: none;">';
            // $mail_format .= '<li>Here are your account details:</li>';
            // $mail_format .= '<div style="margin-left: 30px;">';
            // $mail_format .= '<table border="1">';
            // $mail_format .= '<body>';
            // $mail_format .= '<tr>';
            // $mail_format .= '<td width="100">';
            // $mail_format .= '<b>Display Name:</b>';
            // $mail_format .= '</td>';
            // $mail_format .= '<td>';
            // $mail_format .= $userInfo['display_name'];
            // $mail_format .= '</td>';
            // $mail_format .= '</tr>';
            // $mail_format .= '<tr>';
            // $mail_format .= '<td width="100">';
            // $mail_format .= '<b>Real Name: </b>';
            // $mail_format .= '</td>';
            // $mail_format .= '<td>';
            // $mail_format .= $userInfo['first_name'] . ' ' . $userInfo['last_name'];
            // $mail_format .= '</td>';
            // $mail_format .= '</tr>';
            // $mail_format .= '<tr>';
            // $mail_format .= '<td width="100">';
            // $mail_format .= '<b>Email: </b>';
            // $mail_format .= '</td>';
            // $mail_format .= '<td>';
            // $mail_format .= $userInfo['userEmail'];
            // $mail_format .= '</td>';
            // $mail_format .= '</tr>';
            // if (ALLOW_ACTIVE_DIRECTORY == 0) {
            //     $mail_format .= '<tr>';
            //     $mail_format .= '<td width="100">';
            //     $mail_format .= '<b>Password:</b>';
            //     $mail_format .= '</td>';
            //     $mail_format .= '<td>';
            //     $mail_format .= $this->encrypt_decrypt("decrypt", $userInfo['password']);
            //     $mail_format .= '</td>';
            //     $mail_format .= '</tr>';
            // }
            // $mail_format .= '<tr>';
            // $mail_format .= '<td width="100">';
            // $mail_format .= '<b>Position:</b>';
            // $mail_format .= '</td>';
            // $mail_format .= '<td>';
            // $mail_format .= $userInfo['userPosition'];
            // $mail_format .= '</td>';
            // $mail_format .= '</tr>';
            // $mail_format .= '</body>';
            // $mail_format .= '</table>';
            // $mail_format .= '</div>';
            // //$mail_format .= '</div>';
            // $mail_format .= '</ul>';
        }

        $mail_format .= '</div>';
        $mail_format .= '<br />';
        $mail_format .= '</div>';
        $mail_format .= '</div>';

        //return $mail_format;
        $to = array($userInfo['userEmail'] => $userInfo['display_name']);
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, $to_title, $from_name, $mail_title);
    }

    public function workflow_notify_user($mail_info) {
        $db = new Database();
        $date = functions::dateType();
        $to = $mail_info['To'];
        $cc = $mail_info['CC'];
        $bcc = $mail_info['BCC'];
        $from = $mail_info['From'];
        $title = $mail_info['Title'];
        $from_title = $mail_info['From_Title'];
        $body = $mail_info['Body'];
        $company_id = $mail_info['Company_Id'];
        $trackingNo = $mail_info['TrackNo'];
        $formID = $mail_info['Form_Id'];
        $formDoc = new Form($db, $formID);
        $requestID = $mail_info['Request_Id'];
        $requestor_name = $mail_info['Requestor_name'];
        $processor_name = $mail_info['Processor_name'];
        $enable_guest = $mail_info["EnableGuest"];
        $salutation = array();
        foreach ($to as $value) {
            array_push($salutation, $value);
        }
        $serverName = $this->curPageURL("hasport");

        if ($enable_guest) {
            $path = MAIN_PAGE . "/login?trackNo=" . $trackingNo . "&requestID=" . $requestID . "&formID=" . $formID . "&type=go&user_type=guest&log_type=user&autologged=yes";
        } else {
            $path = MAIN_PAGE . "login?type=go&redirect=workspace?view_type=request|=%=|formID=" . $formID . "|=%=|requestID=" . $requestID . "|=%=|trackNo=" . $trackingNo;
        }

        $pdf_path = MAIN_PAGE . "/ajax/download-request-pdf?formID=" . $formID . "&requestID=" . $requestID . "&formName=" . $formDoc->form_name . ".pdf";


        $mail_format .= "<div style='margin:0px auto; width:600px;'>";
        $mail_format .= "<div style='float:left;width:100%;padding:5px;'>";
        $mail_format .= "<div style='float:left;width:100%;'>";

        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        //$mail_format .= $this->avatarPic("companyAdmin", $company_id, "130", "80", "large", "");

        $mail_format .= "</div>";
        $mail_format .= "</div>";
        $mail_format .= "<div style='float:left;height:50px;width:100%;text-align:right;'>";

        $mail_format .= "<p style='color: #505050;font-family: Arial;font-size: 10px;margin-top: 10px;'>" . $date . "</p>";
        //$mail_format .= $this->avatarPic("companyAdmin", $company_id, "130", "80", "large", "");

        $mail_format .= "</div>";
        $mail_format .= "<div style='float:left;width:100%;margin-bottom: 30px;font-size:11px;background-color:#fff;'>";
        $mail_format .= "<div style='padding:20px;color: #222222; font-family: Arial; font-size: 14px;line-height: 150%;'>";
        //$mail_format .= "<h1 style='color: #206ba4;display: block;font-family: Arial;font-size: 34px;font-weight: bold;line-height: 100%;text-align: left;'>Greetings!</h1>  <br>";
        //$mail_format .= "This message was sent from <b>" . COMPANY_NAME . "</b>.<br><br>";
        //$mail_format .= "This is to inform you that a request from <b>" . $requestor_name . "</b> has been processed and requires your immediate action.<br><br>";
//        $mail_format .= "<b>" . $requestor_name . "</b> sent a request and <br>";
        if ($body && $body != 'null' && $body != '') {
            //mail message
            //$mail_format .= "<div style='float:left;width:100%;font-size:11px;'>";
            //$body_msg = nl2br($body) . "<br><br>";
            $mail_format .= $body . "<br><br>";
            //$mail_format .= str_replace("@link", "<a href='" . $path . "' target='_blank'>Click Here</a>", $body_msg) . "<br>";
            //$mail_format .= "</div>";
        }
        $mail_format .= "Please refer to this link:<br>";
        $mail_format .= "<a href='" . $path . "' target='_blank'>Click Here</a><br> <br>";
        $mail_format .= "To download the pdf format of the request:<br>";
        $mail_format .= "<a href='" . $pdf_path . "' target='_blank'>Click Here</a><br> <br>";
        //$mail_format .= "Sincerely,<br>";
        //$mail_format .= "Formalistics Team<br><br>";
        //$mail_format .= "<div style='background-color:#e4001b;width:100%;height:35px;'>";
        //    $mail_format .= "<div style='padding: 7px; text-align: center; color: #fff; font-size: 11px;'>";
        //        $mail_format .= "If you believe you have received this notification in error, please delete and disregard.";
        //    $mail_format .= "</div>";
        //$mail_format .= "</div>";
        $mail_format .= "</div>";

        $mail_format .= "</div>";
        //$mail_format .= "<div style='float:left;background-color:#1f1f1f;width:100%;height: 62px;'>";
        //    $mail_format .= "<div style='padding: 12px; text-align: center; color: #fff; font-size: 11px;'>";
        //        $mail_format .= "Copyright @ 2014 All rights reserved. <br> Formalistics";
        //    $mail_format .= "</div>";
        //$mail_format .= "</div>";
        $mail_format .= "</div>";
        //$mail_format .= "This message was sent from <b>FORMALISITCS</b>.<br><br>";
        //if ($body && $body != 'null' && $body != '') {
        //    //mail message
        //    $mail_format .= "<div style='float:left;width:100%;font-size:11px;'>";
        //    $mail_format .= $body . "<br> <br>";
        //    $mail_format .= "</div>";
        //}
        //mail link
        //$mail_format .= "<div style='float:left;width:100%;font-size:11px;'>";
        //$mail_format .= "To view the request, browse to Formalistics Tracker (" . DIR_EXTERNAL_IMG . "):<br>";
        //
        //$mail_format .= "<a href='" . $path . "' target='_blank'>" . $path . "</a><br> <br>";
        //$mail_format .= "</div>";
        //
        //$mail_format .= "<div style='float:left;width:100%;font-size:11px;'>";
        //$mail_format .= "<br> <br>***********************************************************************************<br>" .
        //        "Please do not reply to this message. This is highly confidential. If you are not the addressee, you may not copy, forward, disclose or use any part of it. If you are not the intended recipient or you received this by mistake, please delete it and all copies from your system." .
        //        "<br>************************************************************************************";
        //$mail_format .= "</div>";
        //$mail_format .= "</div>";
        //echo print_r($to)

        $this->sendEmail_smtp($mail_format, $to, $cc, $bcc, $from, "", $from_title, $title);
    }

    public function import_notify_user($mail_info) {
        $fs = new functions;

        $date = functions::dateType();
        $to = $mail_info['To'];
        $cc = $mail_info['CC'];
        $bcc = $mail_info['BCC'];
        $from = $mail_info['From'];
        $title = $mail_info['Title'];
        $from_title = $mail_info['From_Title'];
        $body = $mail_info['Body'];
        $formID = $mail_info['Form_Id'];
        $salutation = array();
        foreach ($to as $value) {
            array_push($salutation, $value);
        }
        $serverName = $this->curPageURL("hasport");

        $path = MAIN_PAGE . "login?type=go&redirect=application?id=" . $fs->base_encode_decode("encrypt", $formID);

        $mail_format .= "<div style='margin:0px auto; width:600px;'>";
        $mail_format .= "<div style='float:left;width:100%;padding:5px;'>";
        $mail_format .= "<div style='float:left;width:100%;'>";

        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        //$mail_format .= $this->avatarPic("companyAdmin", $company_id, "130", "80", "large", "");

        $mail_format .= "</div>";
        $mail_format .= "</div>";
        $mail_format .= "<div style='float:left;height:50px;width:100%;text-align:right;'>";

        $mail_format .= "<p style='color: #505050;font-family: Arial;font-size: 10px;margin-top: 10px;'>" . $date . "</p>";
        //$mail_format .= $this->avatarPic("companyAdmin", $company_id, "130", "80", "large", "");

        $mail_format .= "</div>";
        $mail_format .= "<div style='float:left;width:100%;margin-bottom: 30px;font-size:11px;background-color:#fff;'>";
        $mail_format .= "<div style='padding:20px;color: #222222; font-family: Arial; font-size: 14px;line-height: 150%;'>";
        //$mail_format .= "<h1 style='color: #206ba4;display: block;font-family: Arial;font-size: 34px;font-weight: bold;line-height: 100%;text-align: left;'>Greetings!</h1>  <br>";
        //$mail_format .= "This message was sent from <b>" . COMPANY_NAME . "</b>.<br><br>";
        //$mail_format .= "This is to inform you that a request from <b>" . $requestor_name . "</b> has been processed and requires your immediate action.<br><br>";
//        $mail_format .= "<b>" . $requestor_name . "</b> sent a request and <br>";
        if ($body && $body != 'null' && $body != '') {
            //mail message
            //$mail_format .= "<div style='float:left;width:100%;font-size:11px;'>";
            //$body_msg = nl2br($body) . "<br><br>";
            $mail_format .= $body . "<br><br>";
            //$mail_format .= str_replace("@link", "<a href='" . $path . "' target='_blank'>Click Here</a>", $body_msg) . "<br>";
            //$mail_format .= "</div>";
        }
        $mail_format .= "Please refer to this link:<br>";
        $mail_format .= "<a href='" . $path . "' target='_blank'>Click Here</a><br> <br>";
        //$mail_format .= "Sincerely,<br>";
        //$mail_format .= "Formalistics Team<br><br>";
        //$mail_format .= "<div style='background-color:#e4001b;width:100%;height:35px;'>";
        //    $mail_format .= "<div style='padding: 7px; text-align: center; color: #fff; font-size: 11px;'>";
        //        $mail_format .= "If you believe you have received this notification in error, please delete and disregard.";
        //    $mail_format .= "</div>";
        //$mail_format .= "</div>";
        $mail_format .= "</div>";

        $mail_format .= "</div>";
        //$mail_format .= "<div style='float:left;background-color:#1f1f1f;width:100%;height: 62px;'>";
        //    $mail_format .= "<div style='padding: 12px; text-align: center; color: #fff; font-size: 11px;'>";
        //        $mail_format .= "Copyright @ 2014 All rights reserved. <br> Formalistics";
        //    $mail_format .= "</div>";
        //$mail_format .= "</div>";
        $mail_format .= "</div>";
        //$mail_format .= "This message was sent from <b>FORMALISITCS</b>.<br><br>";
        //if ($body && $body != 'null' && $body != '') {
        //    //mail message
        //    $mail_format .= "<div style='float:left;width:100%;font-size:11px;'>";
        //    $mail_format .= $body . "<br> <br>";
        //    $mail_format .= "</div>";
        //}
        //mail link
        //$mail_format .= "<div style='float:left;width:100%;font-size:11px;'>";
        //$mail_format .= "To view the request, browse to Formalistics Tracker (" . DIR_EXTERNAL_IMG . "):<br>";
        //
        //$mail_format .= "<a href='" . $path . "' target='_blank'>" . $path . "</a><br> <br>";
        //$mail_format .= "</div>";
        //
        //$mail_format .= "<div style='float:left;width:100%;font-size:11px;'>";
        //$mail_format .= "<br> <br>***********************************************************************************<br>" .
        //        "Please do not reply to this message. This is highly confidential. If you are not the addressee, you may not copy, forward, disclose or use any part of it. If you are not the intended recipient or you received this by mistake, please delete it and all copies from your system." .
        //        "<br>************************************************************************************";
        //$mail_format .= "</div>";
        //$mail_format .= "</div>";
        //echo print_r($to)

        $this->sendEmail_smtp($mail_format, $to, $cc, $bcc, $from, "", $from_title, $title);
    }

    public function recover_password($to, $to_title, $companyID, $password, $id, $array) {
        $serverName = $this->curPageURL("serverName");
        $mail_title = "Request to reset your formalistics password.";
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;


        $mail_format .= '<div style="margin: 0px auto; width: 580px;">';
        $mail_format .= '<div style="padding: 5px;">';
        $mail_format .= '<br />';

        //$mail_format .= $this->avatarPic("companyAdmin", $companyID, "150", "45", "Original", "");
        //
        //$mail_format .= '<img src="' . $serverName . '/images/themes/hr_blueline.gif">';
        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        $mail_format .= '<br /><br />';
        $mail_format .= '<div style="padding: 10px;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
        $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';

        $mail_format .= 'Dear ' . $array['display_name'] . ",";

        $mail_format .= '<br /><br />';

        //$mail_format .= 'Please save this message for future reference.';
        //$mail_format .= '<br /><br />';
        $mail_format .= ' You recently initiated a reset password for your Formalistics account. To complete the process, click the link below.';

        //$mail_format .= 'Your new <b>' . COMPANY_NAME . '</b> password was successfully reset.';
        $mail_format .= '<br /><br />';

        $mail_format .= '<a href="' . MAIN_PAGE . 'reset_password?id=' . $id . '" target="blank">Reset Password</a>';

        $mail_format .= '<br /><br />';

        $mail_format .= "If you didn't make this request, it's likely that another user has entered your email address by mistake and your account is still secure. ";

        $mail_format .= '<br /><br />';

        $mail_format .= '*This email is system generated. Do not reply.*';

        //$mail_format .= '<ul style="list-style: none;">';
        //$mail_format .= '<li>Here are your account details:</li>';
        //$mail_format .= '<div style="margin-left: 30px;">';
        //$mail_format .= '<li><b>Link:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="'.$serverName.'/reset_password?id='. $id .'" target="blank">' . $serverName . '/reset_password?id='. $id .'</a></li>';
        //$mail_format .= '<li><b>Your new password: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ' . $password . '</li>';
        //$mail_format .= '</div>';
        //$mail_format .= '</ul>';


        $mail_format .= '</div>';
        $mail_format .= '<br />';
        $mail_format .= '</div>';
        $mail_format .= '</div>';
        //return $mail_format;
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, $to_title, $from_name, $mail_title);
    }

    public function resetPasswordByAdmin($to, $to_title, $companyID, $password, $id, $array) {
        $serverName = $this->curPageURL("serverName");
        $mail_title = "Your Formalistics password has been successfully reset.";
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;


        $mail_format = '<div style="margin: 0px auto; width: 580px;">';
        $mail_format .= '<div style="padding: 5px;">';
        $mail_format .= '<br />';

        //$mail_format .= $this->avatarPic("companyAdmin", $companyID, "150", "45", "Original", "");
        //
        //$mail_format .= '<img src="' . $serverName . '/images/themes/hr_blueline.gif">';
        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        $mail_format .= '<br /><br />';
        $mail_format .= '<div style="padding: 10px;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
        $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';

        $mail_format .= 'Dear ' . ucwords($array['display_name']) . ",";

        $mail_format .= '<br /><br />';

        //$mail_format .= 'Please save this message for future reference.';
        //$mail_format .= '<br /><br />';
        $mail_format .= ' Your company admin has successfully reset your password.';

        $mail_format .= '<br /><br />';

        $mail_format .= ' You may now login to Formalistics using the password below:';

        $mail_format .= '<br /><br />';

        $mail_format .= '<b>' . $password . '</b>';

        $mail_format .= '<br /><br />';

        $mail_format .= ' We recommend that you change your new password upon successful login. ';

        $mail_format .= '<br /><br />';

        $mail_format .= '<a href="' . MAIN_PAGE . '" target="blank">Click here to login</a>';

        $mail_format .= '<br /><br />';

        $mail_format .= " Should you encounter any problem accessing your account, please contact your System Administrator for assistance.";

        $mail_format .= '<br /><br />';

        $mail_format .= ' Thank you for using the Formalistics.';

        $mail_format .= '<br /><br />';

        $mail_format .= ' *This email is system generated. Do not reply.*';

        $mail_format .= '</div>';
        $mail_format .= '<br />';
        $mail_format .= '</div>';
        $mail_format .= '</div>';
        // echo $mail_format;
        //return $mail_format;
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, $to_title, $from_name, $mail_title);
    }

    public function reset_password_notification($to, $to_title, $array) {
        $serverName = $this->curPageURL("serverName");
        $mail_title = "Your Formalistics account password has been reset.";
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;


        $mail_format .= '<div style="margin: 0px auto; width: 580px;">';
        $mail_format .= '<div style="padding: 5px;">';
        $mail_format .= '<br />';

        //$mail_format .= $this->avatarPic("companyAdmin", $companyID, "150", "45", "Original", "");
        //
        //$mail_format .= '<img src="' . $serverName . '/images/themes/hr_blueline.gif">';
        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        $mail_format .= '<br /><br />';
        $mail_format .= '<div style="padding: 10px;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
        $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';

        $mail_format .= 'Dear ' . $array['display_name'] . ",";

        $mail_format .= '<br /><br />';

        //$mail_format .= 'Please save this message for future reference.';
        //$mail_format .= '<br /><br />';
        $mail_format .= 'The password for your Formalistics account ' . $array['email'] . ' has been successfully reset.';

        //$mail_format .= 'Your new <b>' . COMPANY_NAME . '</b> password was successfully reset.';
        $mail_format .= '<br /><br />';

        $mail_format .= 'If you didn�t make this change or if you believe an unauthorized person has accessed your account, go to <a href="' . MAIN_PAGE . '" target="_blank">' . MAIN_PAGE . '</a> to reset your password immediately.';

        $mail_format .= '<br /><br />';

        $mail_format .= '*This email is system generated. Do not reply.*';

        //$mail_format .= '<ul style="list-style: none;">';
        //$mail_format .= '<li>Here are your account details:</li>';
        //$mail_format .= '<div style="margin-left: 30px;">';
        //$mail_format .= '<li><b>Link:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="'.$serverName.'/reset_password?id='. $id .'" target="blank">' . $serverName . '/reset_password?id='. $id .'</a></li>';
        //$mail_format .= '<li><b>Your new password: </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ' . $password . '</li>';
        //$mail_format .= '</div>';
        //$mail_format .= '</ul>';


        $mail_format .= '</div>';
        $mail_format .= '<br />';
        $mail_format .= '</div>';
        $mail_format .= '</div>';
        //return $mail_format;
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, $to_title, $from_name, $mail_title);
    }

    public function add_guest($to, $to_title, $companyID, $auth, $encrypted_id) {
        $serverName = $this->curPageURL("serverName");
        $mail_title = "Guest Account.";
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;


        $mail_format .= '<div style="margin: 0px auto; width: 580px;">';
        $mail_format .= '<div style="padding: 5px;">';
        $mail_format .= '<br />';

        //$mail_format .= $this->avatarPic("companyAdmin", $companyID, "150", "45", "Original", "");
        //
        //$mail_format .= '<img src="' . $serverName . '/images/themes/hr_blueline.gif">';
        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        $mail_format .= '<br /><br />';
        $mail_format .= '<div style="padding: 10px;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
        $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';


        $mail_format .= 'Welcome User,';
        $mail_format .= '<br /><br />';
        $mail_format .= 'Your email address has been added as a guest account by ' . $auth['display_name'] . '.';
        $mail_format .= '<br /><br />';
        $mail_format .= 'Please click the link below to activate your account and view the request.';
        $mail_format .= '<br /><br />';
        $path = MAIN_PAGE . "activation?id=" . $encrypted_id . "&type=guest_account&load_type=1";

        $mail_format .= "<a href='" . $path . "' target='_blank'>Click Here</a><br> <br>";


        $mail_format .= '</div>';
        $mail_format .= '<br />';
        $mail_format .= '</div>';
        $mail_format .= '</div>';
        //return $mail_format;
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, $to_title, $from_name, $mail_title);
    }

    public function reg_guest($to, $to_title, $companyID, $auth, $encrypted_id, $by, $type) {
        $serverName = $this->curPageURL("serverName");
        $mail_title = "Guest Account Email Notification";
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;


        $mail_format .= '<div style="margin: 0px auto; width: 580px;">';
        $mail_format .= '<div style="padding: 5px;">';
        $mail_format .= '<br />';

        //$mail_format .= $this->avatarPic("companyAdmin", $companyID, "150", "45", "Original", "");
        //
        //$mail_format .= '<img src="' . $serverName . '/images/themes/hr_blueline.gif">';
        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        $mail_format .= '<br /><br />';
        $mail_format .= '<div style="padding: 10px;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
        $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';


        $mail_format .= 'Welcome User,';
        $mail_format .= '<br /><br />';
        if ($type == "1") {
            $mail_format .= "You have been added as a guest viewer to a request by " . $by;
            $link_type = "2";
        } else {
            $mail_format .= "You've successfully created a formalistics guest account.";
            $link_type = "1";
        }

        #$mail_format .= "You've successfully created a formalistics guest account.";
        $mail_format .= '<br /><br />';
        $mail_format .= 'Please click the link below to activate your account.';
        $mail_format .= '<br /><br />';
        // $path = $this->curPageURL('port') . "/activation?id=" . $encrypted_id . "&type=guest_account&load_type="  . $link_type;
        $path = MAIN_PAGE . "activation?id=" . $encrypted_id . "&type=guest_account&load_type=" . $link_type;

        $mail_format .= "<a href='" . $path . "' target='_blank'>Click Here</a><br> <br>";


        $mail_format .= '</div>';
        $mail_format .= '<br />';
        $mail_format .= '</div>';
        $mail_format .= '</div>';
        //return $mail_format;
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, $to_title, $from_name, $mail_title);
    }

    public function request_add_guest($to, $to_title, $companyID, $auth, $data, $type_email) {
        $serverName = $this->curPageURL("serverName");
        $mail_title = "Guest Account.";
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;


        $mail_format .= '<div style="margin: 0px auto; width: 580px;">';
        $mail_format .= '<div style="padding: 5px;">';
        $mail_format .= '<br />';

        //$mail_format .= $this->avatarPic("companyAdmin", $companyID, "150", "45", "Original", "");
        //
        //$mail_format .= '<img src="' . $serverName . '/images/themes/hr_blueline.gif">';
        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        $mail_format .= '<br /><br />';
        $mail_format .= '<div style="padding: 10px;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
        $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';


        $mail_format .= 'Welcome User,';
        $mail_format .= '<br /><br />';
        $mail_format .= 'You have been added as a guest viewer to a request by ' . $auth['first_name'] . ' ' . $auth['last_name'] . '.';
        $mail_format .= '<br />';
        $mail_format .= 'Please click the link below to view the request.';
        $mail_format .= '<br /><br />';
        if ($type_email = "1") {
            $path = GUEST_MAIN_PAGE . "user_view/workspace?view_type=request&formID=" . $data['formID'] . "&requestID=" . $data['requestID'] . "&trackNo=" . $data['trackNo'] . "&embed_type=viewEmbedOnly";
        } else {
            $path = GUEST_MAIN_PAGE . "user_view/workspace?view_type=request&formID=" . $data['formID'] . "&requestID=" . $data['requestID'] . "&trackNo=" . $data['trackNo'];
        }

        $mail_format .= "<a href='" . $path . "' target='_blank'>Click Here To View</a><br> <br>";


        $mail_format .= '</div>';
        $mail_format .= '<br />';
        $mail_format .= '</div>';
        $mail_format .= '</div>';
        //return $mail_format;
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, $to_title, $from_name, $mail_title);
    }

    public function set_notifications($to, $msg, $img, $uID, $name, $msg_type, $f_name, $type, $t, $path) {
        $date = functions::dateType();
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;
        $mail_title = ucwords($t);

        $mail_format .= "<div style='margin:0px auto; width:600px;'>";
        $mail_format .= "<div style='float:left;width:100%;padding:5px;'>";
        $mail_format .= "<div style='float:left;width:100%;'>";

        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        //$mail_format .= $this->avatarPic("companyAdmin", $company_id, "130", "80", "large", "");

        $mail_format .= "</div>";
        $mail_format .= "</div>";
        $mail_format .= "<div style='float:left;height:50px;width:100%;text-align:right;'>";

        $mail_format .= "<p style='color: #505050;font-family: Arial;font-size: 10px;margin-top: 10px;'>" . $date . "</p>";
        //$mail_format .= $this->avatarPic("companyAdmin", $company_id, "130", "80", "large", "");

        $mail_format .= "</div>";
        $mail_format .= "<div style='float:left;width:100%;font-size:11px;background-color:#fff;'>";
        $mail_format .= "<div style='padding:20px;color: #222222; font-family: Arial; font-size: 14px;line-height: 150%;'>";
        //$mail_format .= "<h1 style='color: #206ba4;display: block;font-family: Arial;font-size: 20px;font-weight: bold;line-height: 100%;text-align: left;'>";
        $mail_format .= '<div style="float:left;width:100%;margin-bottom: 40px;">';
        $mail_format .= $img;
        $mail_format .= '<div style="float:left;margin-top: 25px;margin-right: 5px;color: #206ba4;display: block;font-family: Arial;font-size: 15px;font-weight: bold;line-height: 100%;text-align: left;">' . $name . '</div>';
        $mail_format .= '<div style="float:left;margin-top: 25px;margin-right: 5px;color: #000;display: block;font-family: Arial;font-size: 15px;font-weight: bold;line-height: 100%;text-align: left;">' . $msg_type . '.</div>';
        $mail_format .= '</div>';
        //$mail_format .= "<img style='border-radius:50px;' src='http://eforms.gs3.com.ph/images/formalistics/tbuser/c6b20bcb93196b3f5c1913dc92e583d2/small_c6b20bcb93196b3f5c1913dc92e583d2.png'>";
        //$mail_format .= "Cez Diane!</h1>  <br>";



        $mail_format .= "<label style='font-size:12px;'>" . $f_name . "'s " . $t . ": '" . $msg . "'</label><br><br>";


        $mail_format .= "<a href='" . $path . "' target='_blank'>";
        $mail_format .= "<div style='float:left;background-color:#e4001b;width:30%;height:35px;margin-right:10px;margin-bottom: 30px;margin-top:5px;'>";
        $mail_format .= "<div style='cursor:pointer;font-weight:bold;padding: 7px; text-align: center; color: #fff; font-size: 11px;'>";
        $mail_format .= "See " . $type;
        $mail_format .= "</div>";
        $mail_format .= "</div>";
        $mail_format .= "</a>";


        $mail_format .= "<br><br>";

        $mail_format .= "</div>";

        //$mail_format .= "</div>";
        //$mail_format .= "<div style='float:left;background-color:#1f1f1f;width:100%;height: 62px;margin-top:10px;'>";
        //    $mail_format .= "<div style='padding: 12px; text-align: center; color: #fff; font-size: 11px;'>";
        //        $mail_format .= "Copyright @ 2014 All rights reserved. <br> " . COMPANY_NAME;
        //    $mail_format .= "</div>";
        //$mail_format .= "</div>";
        //$mail_format .= "</div>";

        $this->sendEmail_smtp($mail_format, $to, "", "", $from, "Administrator", $from_name, $mail_title);
    }

    public function email_prompt_notifications($to, $name, $array, $noti) {
        $serverName = $this->curPageURL("serverName");
        $date = functions::dateType();
        if (ALLOW_USER_VIEW == "1") {
            if ($noti['user_level_id'] == "2" || $noti['user_level_id'] == "1") {
                $link = $this->curPageURL('port') . "/notifications";
            } else if ($noti['user_level_id'] == "3") {
                // $link = $this->curPageURL('port') . USER_VIEW . "notifications";
                $link = MAIN_PAGE . 'user_view/' . "notifications";
            }
        } else {
            // $link = $this->curPageURL('port') . "/notifications";
            $link = MAIN_PAGE . "notifications";
        }

        $mail_format .= "<div style='margin:0px auto; width:600px;'>";
        $mail_format .= "<div style='float:left;width:100%;padding:5px;'>";
        $mail_format .= "<div style='float:left;width:100%;'>";

        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        //$mail_format .= $this->avatarPic("companyAdmin", $company_id, "130", "80", "large", "");

        $mail_format .= "</div>";
        $mail_format .= "</div>";
        $mail_format .= "<div style='float:left;height:50px;width:100%;text-align:right;'>";

        $mail_format .= "<p style='color: #505050;font-family: Arial;font-size: 10px;margin-top: 10px;'>" . $date . "</p>";
        //$mail_format .= $this->avatarPic("companyAdmin", $company_id, "130", "80", "large", "");

        $mail_format .= "</div>";
        $mail_format .= "<div style='float:left;width:100%;margin-bottom: 30px;font-size:11px;background-color:#fff;'>";
        $mail_format .= "<div style='padding:20px;color: #222222; font-family: Arial; font-size: 14px;line-height: 150%;'>";
        //$mail_format .= "<h1 style='color: #206ba4;display: block;font-family: Arial;font-size: 20px;font-weight: bold;line-height: 100%;text-align: left;'>";
        $mail_format .= '<div style="float:left;width:100%;margin-bottom: 40px;">';
        $mail_format .= '<div style="float:left;margin-top: 22px;margin-right: 5px;color: #206ba4;display: block;font-family: Arial;font-size: 22px;font-weight: bold;line-height: 100%;text-align: left;">Hello</div>';
        //$mail_format .=  $noti['img'];
        $mail_format .= '<div style="float:left;margin-top: 22px;margin-right: 5px;color: #206ba4;display: block;font-family: Arial;font-size: 22px;font-weight: bold;line-height: 100%;text-align: left;">' . $name . '</div>';
        $mail_format .= '</div>';
        //$mail_format .= "<img style='border-radius:50px;' src='http://eforms.gs3.com.ph/images/formalistics/tbuser/c6b20bcb93196b3f5c1913dc92e583d2/small_c6b20bcb93196b3f5c1913dc92e583d2.png'>";
        //$mail_format .= "Cez Diane!</h1>  <br>";

        $mail_format .= "<font color='#e4001b' style='font-weight:bold;'>You have new notifications! </font>.<br><br>";
        $mail_format .= "A lot has happened on " . COMPANY_NAME . " since you last logged in. Here are some notifications you've missed.<br><br>";

        $mail_format .= "<div style='float:left;width:100%;'>";
        //$mail_format .= "<div style='float:left;width:100%;margin-top:5px;'><img style='float:left;' src='". DIR_EXTERNAL_IMG ."Notifications_icon/1/annoucnement-icon.png'> <div style='float:left;color:#206ba4;margin-left:5px;'><b>3</b> people commented on your post</div> </div><br>";
        if ($noti['Tadded'] != "0") {
            $mail_format .= "<div style='float:left;width:100%;margin-top:5px;'><img style='float:left;' src='" . DIR_EXTERNAL_IMG . "Notifications_icon/1/events-icon.png'> <div style='float:left;color:#206ba4;margin-left:5px;'><b>" . $noti['Tadded'] . "</b> Task added you as a team.</div> </div><br>";
        }

        if ($noti['Tview'] != "0") {
            $mail_format .= "<div style='float:left;width:100%;margin-top:5px;'><img style='float:left;' src='" . DIR_EXTERNAL_IMG . "Notifications_icon/1/events-icon.png'> <div style='float:left;color:#206ba4;margin-left:5px;'><b>" . $noti['Tview'] . "</b> Task set you as an editor.</div> </div><br>";
        }

        if ($noti['Tedit'] != "0") {
            $mail_format .= "<div style='float:left;width:100%;margin-top:5px;'><img style='float:left;' src='" . DIR_EXTERNAL_IMG . "Notifications_icon/1/events-icon.png'> <div style='float:left;color:#206ba4;margin-left:5px;'><b>" . $noti['Tedit'] . "</b> Task set you as a viewer.</div> </div><br>";
        }

        if ($noti['Tdue'] != "0") {
            $mail_format .= "<div style='float:left;width:100%;margin-top:5px;'><img style='float:left;' src='" . DIR_EXTERNAL_IMG . "Notifications_icon/1/events-icon.png'> <div style='float:left;color:#206ba4;margin-left:5px;'><b>" . $noti['Tdue'] . "</b> Task was not able to complete on its due date.</div> </div><br>";
        }

        if ($noti['Msg'] != "0") {
            $mail_format .= "<div style='float:left;width:100%;margin-top:5px;'><img style='float:left;' src='" . DIR_EXTERNAL_IMG . "Notifications_icon/1/msgspecific-icon.png'> <div style='float:left;color:#206ba4;margin-left:5px;'><b>" . $noti['Msg'] . "</b> Unread message was not unable to view.</div> </div><br>";
        }

        if ($noti['Lpost'] != "0") {
            $mail_format .= "<div style='float:left;width:100%;margin-top:5px;'><i class='icon-thumbs-up'></i> <div style='float:left;color:#206ba4;margin-left:5px;'><b>" . $noti['Lpost'] . "</b> Like's on post.</div> </div><br>";
        }

        if ($noti['Lcomment'] != "0") {
            $mail_format .= "<div style='float:left;width:100%;margin-top:5px;'><i class='icon-thumbs-up'></i> <div style='float:left;color:#206ba4;margin-left:5px;'><b>" . $noti['Lcomment'] . "</b> Like's on comment.</div> </div><br>";
        }

        if ($noti['request'] != "0") {
            $mail_format .= "<div style='float:left;width:100%;margin-top:5px;'><i class='icon-file'></i> <div style='float:left;color:#206ba4;margin-left:5px;'>You have <b>" . $noti['request'] . "</b> request.</div> </div><br>";
        }

        $mail_format .= "</div>";

        $mail_format .= "<a href='" . $link . "' target='_blank'>";
        $mail_format .= "<div style='float:left;background-color:#e4001b;width:48%;height:35px;margin-right:10px;margin-bottom: 10px;margin-top:50px;'>";
        $mail_format .= "<div style='font-weight:bold;padding: 7px; text-align: center; color: #fff; font-size: 11px;'>";
        $mail_format .= "View all notifications";
        $mail_format .= "</div>";
        $mail_format .= "</div>";
        $mail_format .= "</a>";

        $mail_format .= "<a href='" . $this->curPageURL('port') . "' target='_blank'>";
        $mail_format .= "<div style='float:left;background-color:#ff7200;width:48%;height:35px;margin-left:10px;margin-bottom: 10px;margin-top:50px;'>";
        $mail_format .= "<div style='font-weight:bold;padding: 7px; text-align: center; color: #fff; font-size: 11px;'>";
        $mail_format .= "Go to " . COMPANY_NAME;
        $mail_format .= "</div>";
        $mail_format .= "</div>";
        $mail_format .= "<br><br>";
        $mail_format .= "</a>";

        $mail_format .= "</div>";

        $mail_format .= "</div>";
        $mail_format .= "<div style='float:left;background-color:#1f1f1f;width:100%;height: 62px;'>";
        $mail_format .= "<div style='padding: 12px; text-align: center; color: #fff; font-size: 11px;'>";
        $mail_format .= "Copyright @ 2014 All rights reserved. <br> " . COMPANY_NAME;
        $mail_format .= "</div>";
        $mail_format .= "</div>";
        $mail_format .= "</div>";
        //return $mail_format;
        $from = SMTP_FROM_EMAIL;
        //$from = array();
        $from_title = "Notifications";
        $title = COMPANY_NAME . " Notifications";

        //print_r($to);
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, "", $from_title, $title);
    }

    public function sendMailCount($json) {

        $mail_title = "Import Notification";
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;


        $mail_format .= '<div style="margin: 0px auto; width: 580px;">';
        $mail_format .= '<div style="padding: 5px;">';
        $mail_format .= '<br />';


        //$mail_format .= "<img src='" . DIR_EXTERNAL_IMG . "layout/1/header.jpg'>";
        $mail_format .= '<br /><br />';
        $mail_format .= '<div style="padding: 10px;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
        $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';


        // $mail_format .= 'Please save this message for future reference.';
        // $mail_format .= '<br /><br />';
        $mail_format .= '<b>Imported Policies: ' . $json['count'] . ' record/s</b>';
        $mail_format .= '<br /><br />';

        $headers = $json['headers'];
        $data = $json['records'];

        $mail_format .='<table border="1">';
        $mail_format .='<thead>';
        $mail_format .='<tr>';
        foreach ($headers as $key => $value) {
            foreach ($value as $header) {
                $mail_format .="<td>$header</td>";
            }
        }
        $mail_format .='</tr>';
        $mail_format .='</thead>';
        $mail_format .='<tbody>';
        foreach ($data as $key => $value) {
            $mail_format .="<tr>";
            foreach ($value as $records) {
                $mail_format .="<td>$records</td>";
            }
            $mail_format .= "</tr>";
        }
        $mail_format .='</tbody>';
        $mail_format .='</table>';

        $mail_format .= '</div>';
        $mail_format .= '<br />';
        $mail_format .= '</div>';
        $mail_format .= '</div>';
        //return $mail_format;
        // $to = array($json['recipient']=>"AdminUser");
        $toArray = explode(";", $json['recipient']);
        $to = array();
        $valueAddress = "";
        foreach ($toArray as $value) {
            $valueAddress = trim($value);
            $to['' . $valueAddress . ''] = "AdminUser";
        }
        print_r($to);
        // $functions->sendEmail_smtp($mail_format, $to, "", "", $from, "", $from_name, $mail_title);
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, "", $from_name, $mail_title);
    }

    public function email_k_li($other) {
        $mail_title = "Company License Key";
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;
        $to = unserialize(L_KEY_TO);


        $mail_format .= '<div style="margin: 0px auto; width: 580px;">';
        $mail_format .= '<div style="padding: 5px;">';
        $mail_format .= '<br />';
        $mail_format .= '<br /><br />';
        $mail_format .= '<div style="padding: 10px;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
        $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';


        $mail_format .= 'Dear Admin,';
        $mail_format .= '<br /><br />';
        $mail_format .= 'This is the license key of ' . $other['company_name'] . '.';
        $mail_format .= '<br /><br />';
        $mail_format .= $other['k'];
        $mail_format .= '<br /><br />';


        $mail_format .= '</div>';
        $mail_format .= '<br />';
        $mail_format .= '</div>';
        $mail_format .= '</div>';
        //return $mail_format;
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, "fsdfsd", $from_name, $mail_title);
    }

    public function email_pin($other) {
        $mail_title = "Your Pin";
        $from = SMTP_FROM_EMAIL;
        $from_name = SMTP_FROM_NAME;
        $to = array($other['email'] => "User");

        // Editable

            $mail_format .= '<div style="margin: 0px auto; width: 580px;">';
            $mail_format .= '<div style="padding: 5px;">';
            $mail_format .= '<br />';
            $mail_format .= '<br /><br />';
            $mail_format .= '<div style="padding: 10px;color: #333;font-size: 13px;font-family: Arial, Helvetica, sans-serif;">';
            $mail_format .= '<div style="text-align: justify;ine-height: 20px;">';


            $mail_format .= 'Dear User,';
            $mail_format .= '<br /><br />';
            $mail_format .= '<b>' . $other['pin'] . '</b> is your private PIN. Use this to access the forms.';
            $mail_format .= '<br /><br />';
            $mail_format .= 'Thanks,<br />';
            $mail_format .= 'Formalistics<br /><br />';
            $mail_format .= 'Replies sent to this email cannot be answered.<br />';


            $mail_format .= '</div>';
            $mail_format .= '<br />';
            $mail_format .= '</div>';
            $mail_format .= '</div>';

        // Close Editable
        
        //return $mail_format;
        $this->sendEmail_smtp($mail_format, $to, "", "", $from, "fsdfsd", $from_name, $mail_title);
    }

}

?>