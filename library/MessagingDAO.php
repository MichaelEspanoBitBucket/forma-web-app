<?php

/**
 * Data access object for messaging
 *
 * @author Ervinne
 */
class MessagingDAO {

    /** @var Database */
    protected $database;
    protected $selectable_columns = array(
        "id", "email", "first_name", "last_name", "display_name", "company_id", "extension AS image_extension"
    );

    /** @var upload */
    protected $upload;

    /* thread read/unread */

    const THREAD_UNREAD = 0;
    const THREAD_READ   = 1;

    /* folders */
    const FOLDER_INBOX   = "INBOX";
    const FOLDER_ARCHIVE = "ARCHIVE";
    const FOLDER_SPAM    = "SPAM";

    public function __construct() {
        $this->database = new Database();
    }

    /*     * **************************************************************** */
    //  <editor-fold desc="Query Methods" defaultstate="collapsed">

    /**
     * @param type $user_id
     */
    public function getUserThreadList($user_id, $starting_from = 0, $fetch_count = 10000) {

        $folder    = MessagingDAO::FOLDER_INBOX;
        //        $raw_query = "  SELECT s.thread_id, s.type, s.thread_read,
        $raw_query = "  SELECT s.thread_id, s.type, 0 AS thread_read,
                            (SELECT GROUP_CONCAT(user_id) FROM tb_thread_subscribers WHERE thread_id = t.id)
                                AS subscribers, 
                            (SELECT GROUP_CONCAT(user_id) FROM tb_thread_subscribers WHERE thread_id = t.id AND user_id != {$user_id})
                                AS recipients, 
                            (SELECT COUNT(1) FROM tb_thread_subscriber_messages sub_msg
                                LEFT JOIN tb_thread_messages msg
                                    ON sub_msg.thread_message_id = msg.id
                                WHERE sub_msg.message_subscriber_user_id = {$user_id} 
                                    AND sub_msg.read = 0
                                    AND msg.thread_id = t.id) AS unread_messages,
                            latest_message.latest_message, latest_message.latest_message_date_posted, latest_message.latest_message_author_id ,thread_name
                            FROM tb_threads t 
                        LEFT JOIN tb_thread_subscribers s
                            ON t.id = s.thread_id
                        RIGHT JOIN (
                            SELECT m1.thread_id, m1.id AS latest_message_id, m1.message AS latest_message, m1.date_posted AS latest_message_date_posted, m1.author_id AS latest_message_author_id 
                            FROM tb_thread_messages m1,
                                (SELECT id, thread_id, MAX(date_posted) AS latest_date_posted FROM tb_thread_messages
                                    LEFT JOIN tb_thread_subscriber_messages sub_m
                                        ON id = sub_m.thread_message_id
                                    WHERE sub_m.message_subscriber_user_id = {$user_id}
                                        AND sub_m.is_active = 1
                                    GROUP BY thread_id) AS m2 	
                            WHERE m1.thread_id = m2.thread_id 
                                AND m1.date_posted = m2.latest_date_posted		
                            GROUP BY thread_id
                        ) latest_message 
                            ON t.id = latest_message.thread_id                        
                        WHERE s.user_id={$user_id}
                            AND t.folder = '{$folder}'
                        ORDER BY latest_message.latest_message_date_posted DESC
                        LIMIT {$starting_from}, {$fetch_count};";

       // echo $raw_query;
        //        exit();

        return $this->database->query($raw_query);
    }

    public function getUserTotalThreadCount($user_id) {
        $folder    = MessagingDAO::FOLDER_INBOX;
        $raw_query = "  SELECT 
                            COUNT(1) AS Thread_Count
                        FROM
                            tb_threads t
                                LEFT JOIN
                            tb_thread_subscribers s ON t.id = s.thread_id
                                RIGHT JOIN
                            (SELECT 
                                m1.thread_id,
                                    m1.id AS latest_message_id,
                                    m1.message AS latest_message,
                                    m1.date_posted AS latest_message_date_posted,
                                    m1.author_id AS latest_message_author_id
                            FROM
                                tb_thread_messages m1, (SELECT 
                                id, thread_id, MAX(date_posted) AS latest_date_posted
                            FROM
                                tb_thread_messages
                            LEFT JOIN tb_thread_subscriber_messages sub_m ON id = sub_m.thread_message_id
                            WHERE
                                sub_m.message_subscriber_user_id = {$user_id}
                                    AND sub_m.is_active = 1
                            GROUP BY thread_id) AS m2
                            WHERE
                                m1.thread_id = m2.thread_id
                                    AND m1.date_posted = m2.latest_date_posted
                            GROUP BY thread_id) latest_message ON t.id = latest_message.thread_id
                        WHERE
                            s.user_id = {$user_id} AND t.folder = '{$folder}'
                        ORDER BY latest_message.latest_message_date_posted DESC";
                            
        $result = $this->database->query($raw_query, "row");
        
        if ($result == null) {
            return 0;
        } else {
            return $result["Thread_Count"];
        }
    }

    /**
     * Gets the id of a thread with the exact set of subscribers passed to this method
     * @param Array $subscribers
     * @return The id of the thread if found, FALSE if not.
     */
    public function getThreadIdWithSubscribers($subscribers) {

        $subscriber_list_string = implode(",", $subscribers);
        $subscriber_count       = count($subscribers);

        $query = "  SELECT thread_id FROM tb_thread_subscribers
                        GROUP BY thread_id
                        HAVING SUM(user_id IN ({$subscriber_list_string})) = COUNT(*)
                        AND COUNT(user_id) = {$subscriber_count}";

        $query_result = $this->database->query($query, "row");

        if ($query_result) {
            return $query_result["thread_id"];
        } else {
            return FALSE;
        }
    }

    public function getThreadMessages($thread_id, $subscriber_id, $starting_message_index = 0, $message_fetch_count = 10000) {

        $query = "  SELECT  m.id, m.message, m.author_id, m.date_posted, m.thread_id,
                            u.display_name AS author_display_name, u.extension AS author_image_extension,
                            (SELECT group_concat(url) FROM tb_attachments WHERE thread_message_id = m.id) AS attachment_url_list
                    FROM tb_thread_messages m
                    LEFT JOIN tb_thread_subscriber_messages sub_m
                        ON m.id = sub_m.thread_message_id
                    LEFT JOIN tbuser u
                        ON m.author_id = u.id                    
                    WHERE m.thread_id = {$thread_id} AND sub_m.message_subscriber_user_id = {$subscriber_id}
                        AND m.is_active = 1
                        AND sub_m.is_active = 1
                    ORDER BY m.date_posted DESC
                    LIMIT {$starting_message_index}, {$message_fetch_count}";

        $query_result = $this->database->query($query);

        if ($query_result) {
            return $query_result;
        } else {
            return FALSE;
        }
    }

    public function getMessageById($message_id) {

        $query = "SELECT m.id, m.message, m.author_id, m.date_posted, m.thread_id,
                        u.display_name AS author_display_name, u.extension AS author_image_extension,
                        (SELECT group_concat(url) FROM tb_attachments WHERE thread_message_id = m.id) AS attachment_url_list
                    FROM tb_thread_messages m                    
                    LEFT JOIN tbuser u
                        ON m.author_id = u.id
                    WHERE m.id = {$message_id}";

        $query_result = $this->database->query($query, "row");

        if ($query_result) {
            return $query_result;
        } else {
            return FALSE;
        }
    }

    public function getUsersByIdList($user_id_list) {

        $columns_string      = implode(",", $this->selectable_columns);
        $user_id_list_string = implode(",", $user_id_list);

        $query = "SELECT {$columns_string} FROM tbuser "
                . "WHERE id IN ({$user_id_list_string})";

        $user_data_list = $this->database->query($query);

        return $user_data_list;
    }

    public function getThreadSubscribers($thread_id) {

        $aliased_columns = array();
        foreach ($this->selectable_columns AS $column) {
            array_push($aliased_columns, "u.{$column}");
        }

        $columns_string = implode(",", $aliased_columns);

        $query = "SELECT {$columns_string} FROM tb_thread_subscribers s
                    LEFT JOIN tbuser u
                        ON s.user_id = u.id
                    WHERE s.thread_id = {$thread_id}";

        $user_data_list = $this->database->query($query);

        return $user_data_list;
    }

    public function getUnreadThreadsCount($user_id) {

        $query = "
            SELECT 
                SUM(has_unread) AS unread_threads
            FROM
                (SELECT 
                    1 AS has_unread
                FROM
                    tb_threads t
                LEFT JOIN tb_thread_messages m ON t.id = m.thread_id
                LEFT JOIN tb_thread_subscriber_messages sub_msg ON m.id = sub_msg.thread_message_id
                WHERE
                    sub_msg.message_subscriber_user_id = {$user_id}
                        AND sub_msg.read = 0
                GROUP BY t.id) AS search_has_unread";

        $results = $this->database->query($query, "row");

        if ($results) {
            return $results["unread_threads"];
        } else {
            return 0;
        }
    }

    public function getUnreadMessageCount($user_id) {

        $query = "SELECT 
                    COUNT(1) AS unread_threads
                FROM
                    tb_thread_messages m
                        LEFT JOIN
                    tb_thread_subscriber_messages sub_msg ON m.id = sub_msg.thread_message_id
                WHERE
                    sub_msg.read = 0
                        AND sub_msg.message_subscriber_user_id = {$user_id}";

        $results = $this->database->query($query, "row");

        if ($results) {
            return $results["unread_threads"];
        } else {
            return 0;
        }
    }

    // </editor-fold>

    /*     * **************************************************************** */
    //  <editor-fold desc="Insert Methods" defaultstate="collapsed">

    /**
     * TODO: separate this function to createThread and add subscribers to thread
     * @param type $author_id
     * @param type $recipient_id_list
     * @return type
     */
    public function createThread($author_id, $recipient_id_list,$thread_name,$type,$collab_name) {

        //        $this->database->startTransaction();
        //  prepare thread data and insert
        $thread = array(
            "date_posted" => "NOW()",
            "date_updated" => "NOW()",
            "is_active" => 1
        );

        //  get the resulting thread id from the result
        $thread_id = $this->database->insert("tb_threads", $thread);

        //  prepare subscribers data and insert
        $subscribers = array(
            array(
                "user_id" => $author_id,
                "type" => "author",
                "thread_id" => $thread_id,
                "date_updated" => "NOW()"
            )
        );

        foreach ($recipient_id_list as $recipient_id) {
            array_push($subscribers, array(
                "user_id" => $recipient_id,
                "type" => "writer",
                "thread_id" => $thread_id,
                "date_updated" => "NOW()"
            ));
        }

        //  insert the thread subscribers
        $this->database->multipleInsert("tb_thread_subscribers", $subscribers);
        //        $this->database->commit();

        return $thread_id;
    }

    public function writeMessage($message, $thread_id, $author_id, $recipient_id_list) {
        //error_reporting(E_ALL);
        //  prepare message data and insert
        // var_dump($message);
        // var_dump($thread_id);
        // var_dump($author_id);
        // var_dump($recipient_id_list);
        // return;
        $message_data = array(
            "message" => $message,
            "thread_id" => $thread_id,
            "author_id" => $author_id,
            "date_posted" => "NOW()",
            "date_updated" => "NOW()"
        );

        $new_message_id = $this->database->insert("tb_thread_messages", $message_data);

        //  create subscribers for this message
        if (!$recipient_id_list) {
            //  search for the subscribers of the thread then add them as subscribers of the message
            $recipients        = $this->getThreadSubscribers($thread_id);
            $recipient_id_list = array();

            foreach ($recipients AS $recipient) {
                array_push($recipient_id_list, $recipient["id"]);
            }
        }

        $subscribers = array();
        foreach ($recipient_id_list AS $recipient_id) {
            array_push($subscribers, array(
                "message_subscriber_user_id" => $recipient_id,
                "thread_message_id" => $new_message_id,
                "read" => ($recipient_id == $author_id ? 1 : 0),
                "date_updated" => "NOW()"
            ));
        }

        //  insert the thread subscribers
        $this->database->multipleInsert("tb_thread_subscriber_messages", $subscribers);

        return $this->getMessageById($new_message_id);
    }

    public function recordAttachments($thread_message_id, $attachment_list) {

        $insert_data = array();

        foreach ($attachment_list as $attachment) {

            array_push($insert_data, array(
                "thread_message_id" => $thread_message_id,
                "path" => $attachment["path"],
                "url" => $attachment["url"]
            ));
        }

        $this->database->multipleInsert("tb_attachments", $insert_data);
    }

    //  </editor-fold>

    /*     * **************************************************************** */
    //  <editor-fold desc="Update Methods" defaultstate="collapsed">

    public function unsubscribeToMessages($message_id_list, $user_id) {

        $message_id_list_string = implode(",", $message_id_list);

        $query = "  UPDATE tb_thread_subscriber_messages
                        SET is_active = 0, read = 1, date_updated = NOW()
                    WHERE message_subscriber_user_id = {$user_id} 
                        AND thread_message_id IN ({$message_id_list_string})";

        $affected_rows = $this->database->query($query, "update");
        return array(
            "affected_message_count" => $affected_rows
        );
    }

    public function unsubscribeToAllThreadMessages($thread_id, $user_id) {

        $query = "  UPDATE tb_thread_subscriber_messages sub_msg
                    LEFT JOIN tb_thread_messages msg
                        ON sub_msg.thread_message_id = msg.id
                    SET sub_msg.is_active = 0, sub_msg.read = 1, sub_msg.date_updated = NOW()
                    WHERE message_subscriber_user_id = {$user_id} 
                        AND msg.thread_id = {$thread_id}";

        $affected_rows = $this->database->query($query, "update");
        return array(
            "affected_message_count" => $affected_rows
        );
    }

    public function deactivateMessagesWithoutSubscribers() {

        $query = "  UPDATE tb_thread_messages msg
                    SET msg.is_active = 0, msg.date_updated = NOW()
                        WHERE (
                            SELECT SUM(sub_msg.is_active) = 0 
                                FROM tb_thread_subscriber_messages sub_msg
                            WHERE sub_msg.thread_message_id = msg.id);";

        $affected_rows = $this->database->query($query, "update");

        return array(
            "deactivated_messages_count" => $affected_rows
        );
    }

    public function changeThreadReadFlag($thread_id, $user_id, $mark = ThreadsDAO::THREAD_READ) {

        $query = "  UPDATE tb_thread_subscriber_messages sub_msg
                    LEFT JOIN tb_thread_messages msg
                        ON sub_msg.thread_message_id = msg.id
                    SET sub_msg.read = {$mark}, sub_msg.date_updated = NOW()
                    WHERE message_subscriber_user_id = {$user_id} 
                        AND msg.thread_id = {$thread_id}
                        AND sub_msg.read != {$mark}";

        $affected_rows = $this->database->query($query, "update");
        return array(
            "affected_message_count" => $affected_rows
        );
    }

    public function moveThreadToFolder($thread_id, $folder = MessagingDAO::FOLDER_INBOX) {

        $query = "  UPDATE tb_threads
                    SET folder = '{$folder}'
                    WHERE id = {$thread_id}";

        $affected_rows = $this->database->query($query, "update");
        return array(
            "affected_thread_count" => $affected_rows
        );
    }

    //  </editor-fold>

    /**
     * TODO: remove this here as this is not a DAO method
     * @param type $user_data
     * @return type
     */
    public function getProcessedImageURL($user_id, $image_extension) {

        $table      = "tbuser";
        $id_encrypt = md5(md5($user_id));
        $size       = "small";

        return '/images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $image_extension;
    }
    
    public function addToGroup($thread_id,$user_id){
    }

    public function leaveToGroup(){
    }

    public function changeGroupName($thread_id,$thread_name){
        $query = "UPDATE `tb_threads` SET `thread_name` = '{$thread_name}'  WHERE `id` = {$thread_id};";

        $affected_rows = $this->database->query($query, "update");
                return array(
            "affected_message_count" => $affected_rows,
            "queryto" => $query
        );
    }

    public function getGroupNameByThreadId($thread_id){
        $query = "SELECT thread_name FROM `tb_threads`
                    WHERE thread_id = {$thread_id}";
        return $this->database->query($raw_query);        
    }
}
