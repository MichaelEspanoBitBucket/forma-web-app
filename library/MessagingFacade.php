<?php

/**
 * Description of MessagingFacade
 *
 * @author Ervinne
 */
class MessagingFacade {

    /** @var FilesDAO */
    protected $files_dao;

    /** @var MessagingDAO */
    protected $messaging_dao;

    public function __construct() {
        $this->files_dao     = new FilesDAO();
        $this->messaging_dao = new MessagingDAO();
    }

    public function getThreadMessages($thread_id, $current_user_id, $starting_message_index = 0, $message_fetch_count = 10000) {

        //  get all the requested messages under this thread
        $messages = $this->messaging_dao->getThreadMessages($thread_id, $current_user_id, $starting_message_index, $message_fetch_count);
        //  get the data of the users
        $users    = $this->messaging_dao->getThreadSubscribers($thread_id);

        $mapped_users = array();
        //  map get the images of each users
        for ($i = 0; $i < count($users); $i ++) {
            $users[$i]["image_url"]         = $this->getProcessedImageURL($users[$i]);
            $mapped_users[$users[$i]["id"]] = $users[$i];
        }

        return array(
            "thread_subscribers" => $mapped_users,
            "thread_messages" => $messages
        );
    }

    public function writeMessage($message, $author_id, $recipient_id_list, $attachment_list) {

        $subscribers = array();
        array_push($subscribers, $author_id);

        foreach ($recipient_id_list AS $recipient_id) {
            array_push($subscribers, $recipient_id);
        }

        //  find an existing thread
        $thread_id = $this->messaging_dao->getThreadIdWithSubscribers($subscribers);

        if (!$thread_id) {
            //  there is no existing thread for these subscribers yet, create one            
            $thread_id = $this->messaging_dao->createThread($author_id, $recipient_id_list);
        }

        $insert_result = $this->messaging_dao->writeMessage($message, $thread_id, $author_id, $subscribers);

        if ($attachment_list) {
            $moved_attachment_list = $this->files_dao->moveAttachments(
                    $insert_result["id"], $author_id, $attachment_list, "message_attachments"
            );
            $this->messaging_dao->recordAttachments($insert_result["id"], $moved_attachment_list);

            $attachment_url_list = array();
            foreach ($moved_attachment_list AS $moved_attachment) {
                array_push($attachment_url_list, $moved_attachment["url"]);
            }
          
            $insert_result["attachment_url_list"] = implode(",", $attachment_url_list);
        }

        return $insert_result;
    }

    public function unsubscribeToThreadMessages($thread_id, $current_user_id) {

        //  validate if the user is a subscriber of the thread
        $thread_subscribers = $this->messaging_dao->getThreadSubscribers($thread_id);
        $user_is_subscribed = false;

        $unsubscribed_message_count = 0;

        //        echo json_encode($thread_subscribers);

        foreach ($thread_subscribers AS $subscriber) {
            if ($subscriber["id"] === $current_user_id) {
                $user_is_subscribed = true;
                break;
            }
        }

        if ($user_is_subscribed) {

            //  unsubscribe the users to the messagess           
            $unsubscribe_results = $this->messaging_dao->unsubscribeToAllThreadMessages(
                    $thread_id, $current_user_id
            );

            $unsubscribed_message_count = $unsubscribe_results["affected_message_count"];

            //  remove the user as a subscriber
            //  TODO that^
            /**/

            //  clean up - if there are messages without subscribers anymore,
            //  deactivate them
            $deactivated_messages_count = $this->messaging_dao->deactivateMessagesWithoutSubscribers();
        } else {
            throw new Exception("Cannot delete a thread where you are not subscribed.");
        }

        return array(
            "unsubscribed_message_count" => $unsubscribed_message_count,
            "deactivated_messages_count" => $deactivated_messages_count
        );
    }

    public function changeGroupName($thread_id,$thread_name){
        $results = $this->messaging_dao->changeGroupName(
                $thread_id, $thread_name
        );
        return $results;
    }

}
