<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MySQLDatabase
 *
 * @author Jewel Tolentino
 */
class MySQLDatabase extends DatabaseConnection {

    //put your code here

    public function __construct($host, $db_name, $user_name, $password) {
        $this->type = "my_sql";
        $this->connect($host, $db_name, $user_name, $password);
    }

}
