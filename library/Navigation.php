<?php

class Navigation extends Database {

    var $auth;
    var $company_id;
    var $activeMenu = 0;

    public function __construct() {
        parent::__construct();
        $this->auth = Auth::getAuth('current_user');
        $this->company_id = $this->auth['company_id'];
    }

    public function getActiveNav($mode = 1) {
        if ($mode == 1) {
            $getNavQuery = "SELECT "
                    . " *, tb_n.title as nav_title, tb_n.description as nav_desc, tb_n.id as nav_id, tb_no.id as menu_id "
                    . " , tb_created.display_name as created_display_name, tb_updated.display_name as updated_display_name "
                    . " FROM tbnavigation tb_n "
                    . " LEFT JOIN tbnavigation_object tb_no "
                    . " ON tb_n.id = tb_no.navigation_id "
                    . " LEFT JOIN tbuser tb_created "
                    . " ON tb_created.id = tb_n.created_by "
                    . " LEFT JOIN tbuser tb_updated "
                    . " ON tb_updated.id = tb_n.updated_by "
                    . " WHERE tb_n.is_active = 1 "
                    . " AND tb_n.company_id = '" . $this->company_id . "'"
                    . " ORDER BY indexing ASC";
        } else {
            $getNavQuery = "SELECT 
                                tb_no . *,
                                tb_n.title as nav_title,
                                tb_n.description as nav_desc,
                                tb_n.id as nav_id,
                                tb_no.id as menu_id
                            FROM
                                tbnavigation tb_n
                                    LEFT JOIN
                                tbnavigation_object tb_no ON tb_n.id = tb_no.navigation_id
                            WHERE
                                tb_n.is_active = 1
                                AND tb_n.company_id = {$this->auth["company_id"]}
                                AND EXISTS(
                                    SELECT * FROM 
                                    tbnavigation_users tb_nu  
                                    LEFT JOIN
                                        tbdepartment_users tbdu ON tbdu.department_code = tb_nu.user
                                    LEFT JOIN
                                        tbform_groups_users tbfgu ON tbfgu.group_id = tb_nu.user
                                    LEFT JOIN
                                        tborgchart tbo ON tbo.id = tbdu.orgChart_id
                                    WHERE tb_no.id = tb_nu.navigation_object_id AND
                                    (
                                        (tb_nu.user_type = 3 AND tb_nu.user = {$this->auth["id"]})
                                        OR (tb_nu.user_type = 1 AND tb_nu.user = {$this->escape($this->auth["position"])})
                                        OR (tb_nu.user_type = 2 AND tbdu.user_id = {$this->auth["id"]} AND tbo.status = 1)
                                        OR (tb_nu.user_type = 4 AND tbfgu.user_id = {$this->auth["id"]} AND tbfgu.is_active = 1)
                                    )
                                )
                            $queryString
                            GROUP BY tb_no.id
                            ORDER BY indexing ASC";
        }

        
        $getNav = $this->query($getNavQuery, "array");

        $getCompanyNav = $this->query("SELECT * FROM tbnavigation tb_n WHERE
                                tb_n.is_active = 1
                                AND tb_n.company_id = {$this->auth["company_id"]}","array");
        $this->activeMenu = $getCompanyNav[0]['id'];
        return $getNav;
    }

    public function getSpecificNavigation($where) {
        $getNavQuery = "SELECT *, tb_n.title as nav_title, tb_n.description as nav_desc, tb_n.id as nav_id, tb_no.id as menu_id  "
                . " , tb_created.display_name as created_display_name, tb_updated.display_name as updated_display_name "
                . "FROM tbnavigation tb_n LEFT JOIN tbnavigation_object tb_no ON tb_n.id = tb_no.navigation_id"
                . " LEFT JOIN tbuser tb_created "
                . " ON tb_created.id = tb_n.created_by "
                . " LEFT JOIN tbuser tb_updated "
                . " ON tb_updated.id = tb_n.updated_by "
                . " WHERE $where ORDER BY indexing ASC";
        $getNav = $this->query($getNavQuery, "array");
        $menu = "";
        foreach ($getNav as $value) {
            $menu .= $this->navMenuTemplate($value['address'], $value['data_id'], $value['title'], $value, $value['menu_id']);
        }

        $ret = array("menu" => $menu,
            "title" => $getNav[0]['nav_title'],
            "desc" => $getNav[0]['nav_desc'],
            "id" => $getNav[0]['nav_id'],
            "date_created" => $getNav[0]['date_created'],
            "created_display_name" => $getNav[0]['created_display_name'],
            "date_updated" => $getNav[0]['date_updated'],
            "nav_json_data" => $getNav[0]['nav_json_data'],
            "updated_display_name" => $getNav[0]['updated_display_name']);

        return json_encode($ret);
    }

    public function navMenuTemplate($address, $id, $title, $json, $menu_id) {
        $getNavigationUsers = $this->query("SELECT * FROM `tbnavigation_users` WHERE navigation_object_id = '" . $menu_id . "'", "array");

        $json['navigation_users'] = compressToOldJsonFormat($getNavigationUsers);
        $json = htmlentities(json_encode($json));

        $ret = '<li class="nav-menu-li nav-menu-placeholder-' . $address . '" address="' . $address . '" data_id="' . $id . '" json-data="' . $json . '"  menu_id="' . $menu_id . '">';
        $ret .= '    <div class="original_data" json-original-data="' . $json . '">';
        $ret .= '        <div style="float:left" class="nav-menu">';
        $ret .= '            <div class="nav-menu-left nav-title fl-table-ellip" title="' . $title . '">' . $title . '</div>';
        $ret .= '            <div class="nav-menu-right"> ';
        $ret .= '                <span class="nav-menu-type" style=""></span>';
        $ret .= '                <a class="nav-settings cursor"><i class="fa fa-cog"></i></a>';
        $ret .= '                <a class="nav-remove cursor"><i class="fa fa-trash"></i></a>';
        $ret .= '            </div>';
        $ret .= '            <div class="clearfix"></div>';
        $ret .= '        </div>';
        $ret .= '        <div class="clearfix"></div>';
        $ret .= '        <ul class="to-transfer">';
        $ret .= '        </ul>';
        $ret .= '    </div>';
        $ret .= '</li>';
        return $ret;
    }

    public function setMenuNavigation() {
        $getNav = $this->getActiveNav(2);
        $ret = "<ul class='fl-infinite-nav-sub first'>";
        $ctrOpen = 0;

        //remove all element that are not accessible
        foreach ($getNav as $key => $value) {
            $results = 0;
            $parent_address = $value['parent_id'];

            // $results = array_filter($getNav, function($a) use($parent_address) {
            //     return $a['data_id'] == $parent_address;
            // });
            foreach ($getNav as $key1 => $value1) {
                if($value1['data_id']==$parent_address){
                    $results++;
                    continue;
                }
            }

            if (!($results > 0 || $parent_address == 0)) {
                unset($getNav[$key]);
            }

        }

        //reset index
        $getNav = array_values($getNav);
        // parent child logic
        foreach ($getNav as $key => $value) {
            $address = $value['address'];
            $title = htmlentities($value['title']);
            $data_id = $value['menu_id'];
            $nav_id = $value['nav_id'];
            // $nextAddress = $getNav[$key+1]['address'];
            $nextParentID = '';
            if ($getNav[$key + 1]['address']) {
                $nextAddress = $getNav[$key + 1]['address'];
                $nextParentID = $getNav[$key + 1]['parent_id'];
            } else {
                $nextAddress = 0;
            }
            $cursorStyle = "";
            $arrow = '<strong class="fl-nav-down-icon"><i class="fa fa-chevron-right"></i></strong>';
            $hoverClass = "displayChild";

            //link
            if ($value['link_type'] == "1") {
                $form_id = $value['form_id'];
                $link = $form_id;
                if ($value['form_link_type'] == "form_list") {
                    $form_id = functions::base_encode_decode("encrypt", $form_id);
                    $filter = "";
                    if($value['enableFilters']=="1"){
                        $filter = "&return_fields=".$value['fieldFilters'];
                    }
                    $link = "href='/user_view/application?id=" . $form_id . $filter ."'";

                } else if ($value['form_link_type'] == "newRequest") {
                    $link = "href='/user_view/workspace?view_type=request&formID=" . $form_id . "&requestID=0" . "'";
                }
            } else if ($value['link_type'] == "2") {
                $link = "href='" . $value['other_text'] . "'";
                
            } else if ($value['link_type'] == "3") {
                $link = "";
                $cursorStyle = "cursor:auto";
            } else if ($value['link_type'] == "4"){
                $link = "href='/user_view/nav_page?dataID=" . $data_id . "&navID=" .$nav_id. "'";
                $hoverClass = "pageNavigation";
                // $arrow = "";
                //$link = "href='/user_view/nav_page'";
            }

            $link_a = "<a $link custom-data-tooltip='" . $title . "' style='" . $cursorStyle . "'>$title</a>";
            //end link


            if ($nextAddress > $address) {
                $ret .= "<li class='". $hoverClass ."'>";
                $ret .= $link_a;
                $ret .= $arrow;
                $ret .= "<ul class='fl-infinite-nav-sub'>";
            } else if ($nextAddress == $address) {
                $ret .= "<li class='". $hoverClass ."'>" . $link_a . "</li>";
            } else {
                // $ret .= "<li>" . $link_a . " $nextAddress , $address</li>";
                $ret .= "<li class='". $hoverClass ."'>" . $link_a . "</li>";
                for ($b = $nextAddress; $b < $address; $b++) {
                    $ret .= "</ul>";
                    $ret .= "</li>";
                }
            }
        }
        if(count($getNav)==0){
            $ret .= "<li><a style='cursor:auto'>No menu found</a></li>";
        }
        $ret.="</ul>";
        return $ret;
    }
    public function setMenuNavigationPage($data_id_active) {
        $getNav = $this->getActiveNav(2);
        $ret = "<ul class='link-parent-ul'>";
        $ctrOpen = 0;

        //remove all element that are not accessible
        foreach ($getNav as $key => $value) {
            $parent_address = $value['parent_id'];
            $results = 0;
            // $results = array_filter($getNav, function($a) use($parent_address) {
            //     return $a['data_id'] == $parent_address;
            // });
            foreach ($getNav as $key1 => $value1) {
                if($value1['data_id']==$parent_address){
                    $results++;
                    continue;
                }
            }
            if (!($results > 0 || $parent_address == 0)) {
                unset($getNav[$key]);
            }
        }

        //reset index
        $getNav = array_values($getNav);
        $constantAddress = -1;
        $loopCondition = false;
        // parent child logic
        foreach ($getNav as $key => $value) {
            $address = $value['address'];
           
            
            $title = htmlentities($value['title']);
            $data_id = $value['menu_id'];

            if($data_id == $data_id_active){
                $loopCondition = true;
            }
            if(!$loopCondition){
                continue;
            }
            $nav_id = $value['nav_id'];
            // $nextAddress = $getNav[$key+1]['address'];
            $nextParentID = '';
            if ($getNav[$key + 1]['address']) {
                $nextAddress = $getNav[$key + 1]['address'];
                $nextParentID = $getNav[$key + 1]['parent_id'];
            } else {
                $nextAddress = 0;
            }
            
            $cursorStyle = "";
            //link
            if ($value['link_type'] == "1") {
                $form_id = $value['form_id'];
                $link = $form_id;
                if ($value['form_link_type'] == "form_list") {
                    $form_id = functions::base_encode_decode("encrypt", $form_id);
                    $filter = "";
                    if($value['enableFilters']=="1"){
                        $filter = "&return_fields=".$value['fieldFilters'];
                    }
                    $link = "href='/user_view/application?id=" . $form_id . $filter ."'";

                } else if ($value['form_link_type'] == "newRequest") {
                    $link = "href='/user_view/workspace?view_type=request&formID=" . $form_id . "&requestID=0" . "'";
                }
            } else if ($value['link_type'] == "2") {
                $link = "href='" . $value['other_text'] . "'";
                
            } else if ($value['link_type'] == "3") {
                $link = "";
                $cursorStyle = "cursor:auto";
            } else if ($value['link_type'] == "4"){
                
                $link = "href='/user_view/nav_page?dataID=" . $data_id . "&navID=" .$nav_id. "'";
                //$link = "href='/user_view/nav_page'";
            }
            if($data_id == $data_id_active){
                $link_a = "<a style='cursor:auto'>$title</a>";
            }else{
                $link_a = "<a $link target='_blank' style='" . $cursorStyle ."'>&#8627; " . " $title</a>";
            }
            
            //end link
            //var_dump($prevAddress);
            $linkClass = '';
            if( ($constantAddress + 1) == $address ){
                $linkClass = 'link-parent';
            }
            if($address <= $constantAddress && $constantAddress != -1){
               break;
            }
            else if($constantAddress === -1){
                $ret .= "<li class='link-parent link-parent-large'>" . $link_a . "</li>";
            }
            else if ($nextAddress > $address) {
                $ret .= "<li class='". $linkClass." next-parent nav-link-hover'>";
                $ret .= $link_a;
                $ret .= "<ul class=''>";
            } else if ($nextAddress == $address) { 
                $ret .= "<li class='". $linkClass." next-parent nav-link-hover'>" . $link_a . "</li>";
            } else {
                // $ret .= "<li>" . $link_a . " $nextAddress , $address</li>";
                $ret .= "<li class='". $linkClass." nav-link-hover'>" . $link_a . "</li>";
                for ($b = $nextAddress; $b < $address; $b++) {
                    $ret .= "</ul>";
                    $ret .= "</li>";
                }
            }
            if($constantAddress === -1){
                $constantAddress = $address; 
            }
            
            
        }
        $ret.="</ul>";
        return $ret;
    }

    public function setOldMenuModules() {
        $search = new Search();
        $getModules = $search->getModules("");
        $nav_type_ext = ""; //$_POST['nav_type_ext'];
        $ret = "<ul class='fl-infinite-nav-sub first'>";
        //loop forms here
        foreach ($getModules as $key_category => $value_category) {

            $ret.='<li>';
            $ret.='<a href="#" custom-data-tooltip="' . $key_category . '" data-placement="right">' . $key_category . ' </a><strong class="fl-nav-down-icon"><i class="fa fa-chevron-right"></i></strong>';
            $ret.='<ul class="fl-infinite-nav-sub">';
            foreach ($value_category as $key => $value) {
                $ret.= '<li>';
                $ret .= '<a href="/user_view/application?id=' . functions::base_encode_decode("encrypt", $value['form_id']) . '" custom-data-tooltip="' . $value['form_name'] . '"  data-placement="right">' . $value['form_name'] . '</a>';
                $ret.= '</li>';
            }
            $ret.= '</ul>';
            $ret.= '</li>';
        }
        if(count($getModules)==0){
            $ret .= "<li><a style='cursor:auto'>No menu found</a></li>";
        }
        $ret .= "</ul>";
        return $ret;
    }

}

?>