<?php

/**
 * Description of NotificationsDAO
 *
 * @author Ervinne
 */
class NotificationsDAO {

    /**
     * @var Database
     */
    protected $database;

    function __construct($database) {
        $this->database = $database;
    }

    public function getMobilitySupportedNotifications($user_id, $fetch_start_index = 0, $fetch_count = 20) {

        $tables_with_notification = $this->getMobilitySupportedNotificationTables($user_id, $fetch_start_index, $fetch_count);
        $partial_query_selections = $this->generatePartialQueryForDynamicTableSelection($tables_with_notification);
        $partial_query_joins      = $this->generatePartialQueryForDynamicTableJoins($tables_with_notification);

        $query = "  SELECT 
                        notification.id AS notification_id,
                        notification.type AS notification_type,
                        notification.noti_id AS related_record_id,
                        notification.noti_by AS notifier_user_id,
                        notifier_user.display_name AS notifier_user_display_name,
                        notification.is_read,
                        notification.date AS date_notified,
                        notification.table_name,                        
                        {$partial_query_selections}
                        comment.post_id AS commented_request_id,
                        comment.comment AS comment_text,                        
                        workspace.id AS related_form_id
                    FROM 
                        tbnotification notification
                            LEFT JOIN 
                        tbuser AS notifier_user ON notification.noti_by = notifier_user.id
                            LEFT JOIN 
                        tb_workspace AS workspace ON notification.table_name = workspace.form_table_name
                            LEFT JOIN 
                        tbcomment AS comment ON notification.table_name='tbcomment' 
                            AND comment.id = notification.noti_id
                        {$partial_query_joins}
                    WHERE 
                        notification.userID = {$user_id}
                            AND notification.is_active=1 
                            AND notification.noti_id != 0
                            AND (
                                (notification.type = 14 AND comment.is_active = 1)
                                OR (notification.type = 5 AND NOT workspace.MobileJsonData = '')
                            )
                    ORDER BY date DESC 
                    LIMIT {$fetch_start_index}, {$fetch_count}";

        $results = $this->database->query($query);
        $errors  = $this->database->getLastErrorMessage();

        if ($errors) {
            throw new Exception($errors);
        }

        return $this->convertNotificationTypes($results);
    }

    protected function convertNotificationTypes($notifications) {
        for ($i = 0; $i < count($notifications); $i ++) {
            if ($notifications[$i]["notification_type"] == "5") {
                $notifications[$i]["notification_type"] = "NEW_REQUEST_FOR_APPROVAL";
            } else if ($notifications[$i]["notification_type"] == "14") {
                $notifications[$i]["notification_type"] = "NEW_REQUEST_COMMENT";
            }
        }
        return $notifications;
    }

    protected function getMobilitySupportedNotificationTables($user_id, $fetch_start_index = 0, $fetch_count = 20) {
        $query = "  SELECT DISTINCT
                        (table_name)
                    FROM
                        tbnotification notification
                            LEFT JOIN
                        tb_workspace AS workspace ON notification.table_name = workspace.form_table_name
                    WHERE
                        userID = {$user_id}
                            AND (notification.type = 5
                            AND NOT workspace.MobileJsonData = '')
                    ORDER BY date DESC
                    LIMIT {$fetch_start_index} , {$fetch_count}";

        $results = $this->database->query($query);
        $errors  = $this->database->getLastErrorMessage();

        $tables = array();

        if (!$errors) {
            foreach ($results AS $row) {
                array_push($tables, $row["table_name"]);
            }
        } else {
            var_dump($errors);
        }

        return $tables;
    }

    protected function generatePartialQueryForDynamicTableJoins($tables) {
        $partial_query = "";
        foreach ($tables AS $table) {
            $partial_query .= "LEFT JOIN {$table} ON notification.table_name='{$table}' AND {$table}.id = notification.noti_id\n";
        }

        return $partial_query;
    }

    protected function generatePartialQueryForDynamicTableSelection($tables) {

        if (count($tables) == 0) {
            return "NULL AS request_tracking_number,";
//            return "";
        } else {

            $columns = array();
            foreach ($tables AS $table) {
                array_push($columns, "{$table}.TrackNo");
            }

            $tables_string = join(",", $columns);
            $partial_query = "CONCAT_WS('', {$tables_string}) AS request_tracking_number,";

            return $partial_query;
        }
    }

}
