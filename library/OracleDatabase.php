<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OracleDatabase
 *
 * note: enable php_pdo_oci.dll on php.ini
 * @author Jewel Tolentino
 */
class OracleDatabase extends DatabaseConnection {

    public function __construct($host, $db_name, $user_name, $password) {
        $this->type = "oracle";
        $this->connect($host, $db_name, $user_name, $password);
    }

}
