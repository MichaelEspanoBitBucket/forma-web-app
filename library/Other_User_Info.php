<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Other user info
 *
 * @author Samuel G. Pulta
 */
class Other_User_Info extends Formalistics {

    //put your code here

    public $info_json;
    public $user_id;
    public $location;
    public $date_added;
    public $is_active;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $this->tblname = 'tb_other_user_info';
        $userQuery = new userQueries();

        if ($id) {
            $result = $this->db->query("SELECT * FROM tb_other_user_info WHERE user_id = {$this->db->escape($id)}", "row");

            $this->id = $result['id'];
            $this->info_json = $result['info_json'];
            $this->user_id = $result['user_id'];
            $this->location = $result['location'];
            $this->date_added = $result['date_added'];
            $this->is_active = $result['is_active'];
            
        }
    }
    

    public function save() {
        $insert_array = array(
            "id" => $this->id,
            "info_json" => $this->info_json,
            "user_id" => $this->user_id,
            "location" => $this->location,
            "date_added" => $this->middle_name,
            "is_active" => $this->date_added
        );

        $insert_id = $this->db->insert($this->tblname, $insert_array);

        $this->id = $insert_id;
    }

    public function update() {
        $update_array = array(
            "id" => $this->id,
            "info_json" => $this->info_json,
            "user_id" => $this->user_id,
            "location" => $this->location,
            "date_added" => $this->middle_name,
            "is_active" => $this->date_added
        );

        $condition_array = array("user_id" => $this->user_id);

        $this->db->update($this->tblname, $update_array, $condition_array);
    }

}

?>
