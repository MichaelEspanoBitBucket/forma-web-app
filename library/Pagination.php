<?php
class Pagination
{
    public $row_per_page = 4;
    public $current_page = 1;
    protected $data_count;
    public function paginate($result_set)
    {
        $this->data_count = count($result_set);
        
        $current = ($this->current_page>0)? $this->current_page-1:0;
        $offset = ($current>0) ? ceil($current+=($this->row_per_page / $this->current_page)) : 0;
        $last = end($this->pages());
        if(intval($offset) == ($last))
        {
            $offset++;
        }
        $data = array_slice($result_set,$offset,$this->row_per_page,true);
        return $data;
    }
    
    public function pages()
    {
        $pages = ($this->data_count / $this->row_per_page)>0 ? ($this->data_count / $this->row_per_page) : 1;
        return range(1,intval(ceil($pages)));
    }
    
    public function prev()
    {
        $this->current_page--;
        if($this->current_page<=0)
        {
            return $this->current_page;
        }
        return false;
    }
    
    public function next()
    {
        $this->current_page++;
        if($this->current_page<end($this->pages()))
        {
            return $this->current_page;
        }
        return false;
    }
}
