<?php
class PerformanceLog {

	private $collector_model = [];
	private $collector_sequence = [];
	private $path;
	private $file_name;

	protected $total_memory_usage_in_bytes;

	public function __construct($path, $file_name){
		if(!$path){
			$this->path = APPLICATION_PATH.'\\AppsPerfLog\\';
		}else{
			$this->path = $path;
		}
		if(!$file_name){
			$this->file_name = 'PERF_LOG'.(date('Y-m-d'));
		}else{
			$this->file_name = $file_name;
		}
	}

	public function start($stamp_name) {
		$this->collector_model[$stamp_name] = array();
	# ===================================================================================== #
		$this->collector_model[$stamp_name]['start'] 			= 	$this->_stamp();
	# ===================================================================================== #
		$this->collector_model[$stamp_name]['mem_size_start'] 	= 	memory_get_usage();
	}
	public function end($stamp_name) {
	# ===================================================================================== #
		$this->collector_model[$stamp_name]['mem_size_end'] 	= 	memory_get_usage();
	# ===================================================================================== #
		$this->collector_model[$stamp_name]['mem_total_usage_in_byte'] 	= 	$this->collector_model[$stamp_name]['mem_size_end'] - $this->collector_model[$stamp_name]['mem_size_start'];
		$this->total_memory_usage_in_bytes = $this->collector_model[$stamp_name]['mem_total_usage_in_byte'];
		$this->collector_model[$stamp_name]['end'] 				= 	$this->_stamp();
		$this->collector_model[$stamp_name]['difference'] 		= 	$this->collector_model[$stamp_name]['end'] 					- 	$this->collector_model[$stamp_name]['start'];
		$this->collector_model[$stamp_name]['difference_ms'] 	= 	$this->collector_model[$stamp_name]['difference'] 			/ 	1000;
		
		array_push($this->collector_sequence, 
			array_merge(
				array(
					'stamp_name' => $stamp_name
				),
				$this->collector_model[$stamp_name]
			)
		);
	}

	private function _stamp(){
		return round(microtime(true) * 1000);
	}

	public function getSequence(){
		return $this->collector_sequence;
	}
	
	public function getModel(){
		return $this->collector_model;
	}

	public function getTotalMemoryUsage($unit_size) {
		$print_memory_total_usage = '';
		switch ($unit_size) {
			case 'GB' :
				$print_memory_total_usage = round( ($this->total_memory_usage_in_bytes / 1000000000), 4) . 'GB';
				break;
			
			case 'MB' :
				$print_memory_total_usage = round( ($this->total_memory_usage_in_bytes / 1000000), 4) . 'MB';
				break;
			
			case 'KB' :
				$print_memory_total_usage = round( ($this->total_memory_usage_in_bytes) / 1000, 4) . 'KB';
				break;

			default :
				$print_memory_total_usage = $this->total_memory_usage_in_bytes;
				break;
 		}
 		return $print_memory_total_usage;
	}

	public function getStamps($stamp_name_selection = '*', $data_key_selection = array('stamp_name','difference_ms'), $sort_selection = 'difference_ms', $sort = 'asc'){
		//kunyare gusto kong makuha lahat ng time difference ni LookupWhere na naka sort by desc
		//kunyare gusto kong makuha lahat ng mem total usage ni LookupWhere na naka sort by desc
		$temp1 = $this->collector_sequence;
		usort($temp, function($a,$b) use ($sort_selection,$sort) {
			if($sort == 'desc'){
				return $b[$sort_selection] - $a[$sort_selection];
			}else{
				return $a[$sort_selection] - $b[$sort_selection];
			}
		});
		return array_values(array_filter(array_map(function($data) use ($stamp_name_selection , $data_key_selection) {
			$stamp_name_found = false;
			if(is_array($stamp_name_selection)){
				$stamp_name_found = in_array($data['stamp_name'], $stamp_name_selection);
			}else{
				$stamp_name_found = $data['stamp_name'] == $stamp_name_selection || $data['stamp_name'] == '*';
			}
			if($stamp_name_found){
				$temp = array();
				if(is_array($data_key_selection)){
					foreach ($data_key_selection as $key => $value_name_selection) {
						$temp[$value_name_selection] = $data[$value_name_selection];
					}
				}else{
					$temp[$data_key_selection] = $data[$data_key_selection];
				}
				return $temp;
			}else{
				return null;
			}
		}, $temp1)));
	}
	public function logByFile(){
		$args = func_get_args();
		$args = array_values($args);
		$print_data = call_user_func_array(array('PerformanceLog', 'getStamps') , $args);
		error_log(json_encode($print_data,JSON_PRETTY_PRINT), 3, $this->path . $this->file_name . '.log');
		return $print_data;
	}
	public function logFileCleanUp(){
		unset($this->collector_sequence);
		unset($this->collector_model);
	}
	public function sample(){ //PANG SAMPLE LANG ... USE THIS TO THE DEBBUGER
		$my_path = APPLICATION_PATH.'\\AppsPerfLog\\';
		$my_file_name = 'PERF_LOG'.(date('Y-m-d'));
		$console = new PerformanceLog( $my_path , $my_file_name );

		$console->start('WHOLE');
		$console->start('FIRST_LOOP');
		$test_mem = '';
		for($ctr = 0 ; $ctr <= 10000000 ; $ctr ++){
			$test_mem .= ''.$ctr;
		}
		$console->end('FIRST_LOOP');


		$test_mem = '';
		$console->start('SECOND_LOOP');
		for($ctr = 0 ; $ctr <= 10000000 ; $ctr ++){
			$test_mem .= ''.$ctr;
		}
		$console->end('SECOND_LOOP');
		$console->end('WHOLE');

		$console->start('TESTSTAMPING');
		var_dump($console->logByFile( array('FIRST_LOOP', 'SECOND_LOOP','WHOLE','TESTSTAMPING'), array('stamp_name', 'mem_total_usage_in_byte', 'difference_ms')  ));

		var_dump($console->getTotalMemoryUsage('MB'));

		$console->end('TESTSTAMPING');
	}
}
?>