<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Person
 *
 * @author Jewel Tolentino
 */
class Person extends Formalistics {

    //put your code here

    public $email;
    public $display_name;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $contact_number;
    public $position;
    public $company;
    public $user_level_id;
    public $department_position_level;
    public $password;
    public $extension;
    public $date_registered;
    public $email_activate;
    public $is_available;
    public $timeout;
    public $is_active;
    public $department_id;
    public $department;
    public $image;
    public $json_tbl;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $this->tblname = 'tbuser';
        $redis_client = new Redis_Formalistics();
        $userQuery = new userQueries();

        if ($id) {
            $result = $redis_client->query("person_object_" . $id, "SELECT * FROM tbuser WHERE id = {$this->db->escape($id)}");

            $this->id = $result['id'];
            $this->email = $result['email'];
            $this->display_name = $result['display_name'];
            $this->first_name = $result['first_name'];
            $this->middle_name = $result['middle_name'];
            $this->last_name = $result['last_name'];
            $this->contact_number = $result['contact_number'];
            $this->position = $result['position'];
            $this->company = new Company($this->db, $result['company_id']);
            $this->user_level_id = new User_Level($this->db, $result['user_level_id']);
            $this->department_position_level = new Department_Position_Level($this->db, $result['department_position_level']);
            $this->password = $result['password'];
            $this->extension = $result['extension'];
            $this->date_registered = $result['date_registered'];
            $this->email_activate = $result['email_activate'];
            $this->is_available = $result['is_available'];
            $this->timeout = $result['timeout'];
            $this->is_active = $result['is_active'];
            $this->department_id = $result['department_id'];
            $this->department = new Department($this->db, $result['department_id']);
            $this->image = $userQuery->avatarPic($this->tblname, $this->id, "44", "44", "small", "avatar");
            $this->json_tbl = $result['json_tbl'];
        }
    }

    public function save() {
        $insert_array = array(
            "id" => $this->id,
            "email" => $this->email,
            "display_name" => $this->display_name,
            "first_name" => $this->first_name,
            "middle_name" => $this->middle_name,
            "last_name" => $this->last_name,
            "contact_number" => $this->contact_number,
            "position" => $this->position,
            "company_id" => $this->company->id,
            "user_level_id" => $this->user_level_id->id,
            "department_position_level" => $this->department_position_level->id,
            "password" => $this->password,
            "extension" => $this->extension,
            "date_registered" => $this->date_registered,
            "email_activate" => $this->email_activate,
            "is_available" => $this->is_available,
            "timeout" => $this->timeout,
            "is_active" => $this->is_active,
            "department_id" => $this->department->id,
            "json_tbl" => $this->json_tbl
        );

        $insert_id = $this->db->insert($this->tblname, $insert_array);

        $this->id = $insert_id;
    }

    public function update() {
        $update_array = array(
            "id" => $this->id,
            "email" => $this->email,
            "display_name" => $this->display_name,
            "first_name" => $this->first_name,
            "middle_name" => $this->middle_name,
            "last_name" => $this->last_name,
            "contact_number" => $this->contact_number,
            "position" => $this->position,
            "company_id" => $this->company->id,
            "user_level_id" => $this->user_level_id->id,
            "department_position_level" => $this->department_position_level->id,
            "password" => $this->password,
            "extension" => $this->extension,
            "date_registered" => $this->date_registered,
            "email_activate" => $this->email_activate,
            "is_available" => $this->is_available,
            "timeout" => $this->timeout,
            "is_active" => $this->is_active,
            "department_id" => $this->department->id,
            "json_tbl" => $this->json_tbl
        );

        $condition_array = array("id" => $this->id);

        $this->db->update($this->tblname, $update_array, $condition_array);
    }

}

?>
