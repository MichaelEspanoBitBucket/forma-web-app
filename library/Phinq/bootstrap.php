<?php

spl_autoload_register(function ($class) {
    $classParts = explode("\\", $class);
    $className = $classParts[sizeof($classParts)-1];
    include_once(realpath('.') . "/library/Phinq/" . $className.'.php');

});

?>