<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Redis_Formalistics
 *
 * @author Jewel Tolentino
 */
class Redis_Formalistics {

    //put your code here

    public $client;

    public function __construct() {
        $this->connect();
    }

    public function connect() {
        $this->client = getRedisConnection();
    }

    public function getArrayValue($key) {
        if ($this->client) {
            return json_decode($this->client->get($key), true);
        }
    }

    public function setArrayValue($key, $arr_value) {
        if ($this->client) {
            $this->client->set($key, json_encode($arr_value));
        }
    }

    public function query($key, $clause, $type = "row") {
        $conn = getCurrentDatabaseConnection();
        $return_array = array();
        $cache_object = $this->getArrayValue($key);

        if ($cache_object) {
            $return_array = $cache_object;
        } else {
            $return_array = $conn->query($clause, $type);
            $this->setArrayValue($key, $return_array);
        }

        return $return_array;
    }

}
