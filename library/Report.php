<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Report
 *
 * @author Jewel Tolentino
 */
class Report extends Formalistics {

    //put your code here

    public $form_id;
    public $form;
    public $title;
    public $description;
    public $parameters;
    public $columns;
    public $plotbands;
    public $symbol;
    public $yaxis_max;
    public $yaxis_min;
    public $yaxis_interval;
    public $company;
    public $date_created;
    public $date_updated;
    public $created_by;
    public $updated_by;
    public $is_active;

    public function __construct($db, $id) {
        $this->tblname = 'tbreport';
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();
        if ($id) {
            $result = $redis_client->query("report_object_" . $id, "SELECT * FROM tbreport WHERE id = {$this->db->escape($id)}");

            $this->id = $result['id'];
            $this->form_id = $result['form_id'];
            $this->form = new Form($this->db, $result['form_id']);
            $this->title = $result['title'];
            $this->description = $result['description'];
            $this->parameters = $result['parameters'];
            $this->columns = $result['columns'];
            $this->plotbands = $result['plotbands'];
            $this->symbol = $result['symbol'];
            $this->yaxis_max = $result['YAxisMax'];
            $this->yaxis_min = $result['YAxisMin'];
            $this->yaxis_interval = $result['YAxisInterval'];
            $this->company = new Company($this->db, $result['company_id']);
            $this->date_created = $result['date_created'];
            $this->date_updated = $result['date_updated'];
            $this->created_by = new Person($this->db, $result['created_by']);
            $this->updated_by = new Person($this->db, $result['updated_by']);
            $this->is_active = $result['is_active'];
        }
    }

    public function save() {
        $insert_array = array(
            "form_id" => $this->form_id,
            "title" => $this->title,
            "description" => $this->description,
            "parameters" => $this->parameters,
            "columns" => $this->columns,
            "plotbands" => $this->plotbands,
            "symbol" => $this->symbol,
            "YAxisMax" => $this->yaxis_max,
            "YAxisMin" => $this->yaxis_min,
            "YAxisInterval" => $this->yaxis_interval,
            "company_id" => $this->company->id,
            "date_created" => $this->date_created,
            "created_by" => $this->created_by->id,
            "is_active" => $this->is_active
        );

        $insert_id = $this->db->insert($this->tblname, $insert_array);

        $this->id = $insert_id;
    }

    public function update() {
        $update_array = array(
            "form_id" => $this->form_id,
            "title" => $this->title,
            "description" => $this->description,
            "parameters" => $this->parameters,
            "columns" => $this->columns,
            "plotbands" => $this->plotbands,
            "symbol" => $this->symbol,
            "YAxisMax" => $this->yaxis_max,
            "YAxisMin" => $this->yaxis_min,
            "YAxisInterval" => $this->yaxis_interval,
            "company_id" => $this->company->id,
            "date_updated" => $this->date_updated,
            "updated_by" => $this->updated_by->id,
            "is_active" => $this->is_active
        );

        $condition_array = array("id" => $this->id);
        $this->db->update($this->tblname, $update_array, $condition_array);
    }

}

?>
