<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Report_PDF
 *
 * @author Jewel Tolentino
 */
class Report_PDF extends FPDF {

    public function setHeader($logo_img = null, $title = null) {
        $auth = Auth::getAuth('current_user');
        $userCompany = new userQueries();
// Logo
        $this->Image($userCompany->getAvatarPicSrc("companyAdmin", $auth['company_id'], "100%", "100", "Original", "companyPic"), 10, 6, 30);
// Arial bold 15
        $this->SetFont('Arial', 'B', 15);
// Move to the right
        $this->Cell(30);
// Title
        $this->Cell(40, 5, $title);
// Line break
        $this->Ln(20);
    }

    public function setContent($header = null, $data = null) {
// Header
        $this->SetFont('Arial', 'B', 10);
        foreach ($header as $column) {
            $this->Cell($this->convertPixelToPt($column['Width']), 7, $column['Name'], 1);
        }

        $this->Ln();
        $this->SetFont('Arial', '', 10);

// Data
        foreach ($data as $row) {
            $column_ctr = 0;
            foreach ($row as $key => $col) {
                $this->Cell($this->convertPixelToPt($header[$column_ctr]['Width']), 7, $col, 1);
                $column_ctr++;
            }

            $this->Ln();
        }
    }

    public function convertPixelToPt($pxl) {
        return $pxl / 3;
    }

    public function WordWrap($text, $maxwidth) {
        $text = trim($text);
        if ($text === '')
            return 0;
        $space = $this->GetStringWidth(' ');
        $lines = explode("\n", $text);
        $text = '';
        $count = 0;

        foreach ($lines as $line) {
            $words = preg_split('/ +/', $line);
            $width = 0;

            foreach ($words as $word) {
                $wordwidth = $this->GetStringWidth($word);
                if ($width + $wordwidth <= $maxwidth) {
                    $width += $wordwidth + $space;
                    $text .= $word . ' ';
                } else {
                    $width = $wordwidth + $space;
                    $text = rtrim($text) . "\n" . $word . ' ';
                    $count++;
                }
            }
            $text = rtrim($text) . "\n";
            $count++;
        }
        $text = rtrim($text);
        return $text;
    }

}
