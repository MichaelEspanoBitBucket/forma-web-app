<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Request
 *
 * @author Jewel Tolentino
 */
class Request extends Database {

    //put your code here

    public $id;
    public $formName;
    public $referencePrefix;
    public $referenceType;
    public $fields;
    public $formId;
    public $data;
    public $trackNo;
    public $keywordFields;
    public $additionalReaders;
    public $additionalEditors;
    public $responseData;
    public $send_email;
    public $send_notification;
    public $process_workflow_events;
    public $process_triggers;
    public $auth;
    public $conn;

    public function forMyApproval($companyID, $userID) {
        $userDoc = new Person($db, "");

        $requestCount = "";
        $forms = $this->query("SELECT * FROM tb_workspace WHERE company_id = {$this->escape($companyID)} AND is_delete = 0", 'array');

        foreach ($forms as $form) {
            $strSQL = "SELECT * FROM " . $form['form_table_name'] . " WHERE Processor = {$this->escape($userID)}";
            $count = $this->query($strSQL, 'numrows');

            $requestCount += $count;
        }

        return $requestCount;
    }

    public function myRequests() {
        //TODO 
    }

    public function load($formId, $id, $currentUser) {
        $this->formId = $formId;
        $this->conn = getCurrentDatabaseConnection();
        $results = $this->query("SELECT form_table_name, reference_prefix, reference_type  FROM tb_workspace WHERE id = {$this->escape($formId)} AND is_delete = 0", 'row');

        $this->formName = $results['form_table_name'];
        $this->referencePrefix = $results['reference_prefix'];
        $this->referenceType = $results['reference_type'];
        $this->id = $id;

        $fieldResults = $this->query("SELECT DISTINCT COLUMN_NAME FROM information_schema.columns WHERE TABLE_SCHEMA={$this->escape(DB_NAME)} AND TABLE_NAME ={$this->escape($this->formName)}", 'array');
        $this->fields = $fieldResults;
        $this->send_email = true;
        $this->send_notification = true;
        $this->process_triggers = true;
        $this->process_workflow_events = true;
        $this->responseData["responses"] = array();

        if ($id != 0) {
            $this_data = $this->query("SELECT * FROM " . $results['form_table_name'] . " WHERE ID = {$id}", "row");
            $this->data = $this_data;
        }

        if ($currentUser) {
            $auth = $this->query("SELECT * FROM tbuser WHERE id = {$currentUser->id}", "row");
        } else {
            $auth = Auth::getAuth('current_user');
        }

        $this->auth = $auth;
    }

    public function save() {
        //insert record
        $this->id = $this->insert($this->formName, $this->data);
        $conditionFields['ID'] = $this->id;

        //generate tracking number
        $trackNoField['TrackNo'] = library::getTrackNo($this, $this->referencePrefix, $this->referenceType, $this->id);
        $this->trackNo = $trackNoField['TrackNo'];
        $_POST["TrackNo"] = $trackNoField['TrackNo'];
        $this->update($this->formName, $trackNoField, $conditionFields);
    }

    public function modify() {
        //update record
        $conditionFields['ID'] = $this->id;
        $this->update($this->formName, $this->data, $conditionFields);
    }

    // @selected_Fields - array("")
    public function logSelectedFieldsInNewRequest($fields, $selected_Fields) {
        $date = $this->currentDateTime();
        $fieldSQL = array();

        foreach ($fields as $field) {
            array_push($fieldSQL, "`" . $field . "`");
        }

        $result = $this->query("SELECT " . join(",", $fieldSQL) . " FROM {$this->formName} WHERE ID={$this->id}", "row");
        $fields = $this->query("SELECT * FROM tbfields WHERE form_id = {$this->formId}");

//        var_dump($fields);

        foreach ($fields as $field) {

            $newValue = $this->data[$field["field_name"]];

            if ($field["field_type"] == "dateTime") {

                $new_date = date_create($newValue);
                $newValue = date_format($new_date, "Y/m/d H:i:s");
            }

            if (in_array($field["field_type"], $selected_Fields)) {

                $logDoc = new Request_Log($this);
                $logDoc->form_id = $this->formId;
                $logDoc->request_id = $this->id;

                // Extract JSON

                $var = "";

                $resNew = json_decode($newValue, true);

                $new = "";

                // NEW
                foreach ($resNew as $Newres) {
                    $new .= " " . $Newres['file_name'] . ",";
                }

                $newResVal = substr($new, 0, -1);

                $logDoc->details = $field["data_field_label"] . ' has been added "' . $newResVal . '"';

                $logDoc->created_by_id = $this->auth['id'];
                $logDoc->date_created = $date;
                $logDoc->save();
            }
        }
    }

    function createFieldLevelLogs($fields) {
        $listViewFormula = new ListViewFormula();
//    public    $auth = Auth::getAuth('current_user');
        $date = $this->currentDateTime();
        $fieldSQL = array();

        foreach ($fields as $field) {
            array_push($fieldSQL, "`" . $field . "`");
        }

        $result = $this->query("SELECT " . join(",", $fieldSQL) . " FROM {$this->formName} WHERE ID={$this->id}", "row");
        $fields = $this->query("SELECT * FROM tbfields WHERE form_id = {$this->formId}");

        // For Encryption
        $db = new Database();
        $formObject = new Form($db, $this->formId);

        $encrypted_selected_flds = $this->encrypt_decrypt_request_details($formObject->form_json);
// var_dump($encrypted_selected_flds);
        foreach ($fields as $field) {
            $oldValue = $result[$field["field_name"]];
            $newValue = $this->data[$field["field_name"]];

            if ($field["field_type"] == "dateTime") {
                $old_date = date_create($oldValue);
                $oldValue = date_format($old_date, "Y/m/d H:i:s");

                $new_date = date_create($newValue);
                $newValue = date_format($new_date, "Y/m/d H:i:s");
            }

            if ($field["field_type"] == "datepicker" && $oldValue == "0000-00-00") {
                $oldValue = "";
            }

            // FOR ENCRYPT DECRYPT
            // OLD
            $set[0]->value = $oldValue;
            $set[0]->encryption_descryption = $encrypted_selected_flds;
            $set[0]->field_name = $field["field_name"];
            $json_field_attr_old = json_decode(json_encode($set[0]), true);

            $oldValue = $listViewFormula->setValue($json_field_attr_old);

            // NEW
            $set1[0]->value = $newValue;
            $set1[0]->encryption_descryption = $encrypted_selected_flds;
            $set1[0]->field_name = $field["field_name"];
            $json_field_attr_new = json_decode(json_encode($set1[0]), true);

            $newValue = $listViewFormula->setValue($json_field_attr_new);

//            print_r($oldValue . " = " . $newValue);
//            echo "<br/>";

            if ($oldValue != $newValue) {


                $logDoc = new Request_Log($this);
                $logDoc->form_id = $this->formId;
                $logDoc->request_id = $this->id;

                if ($field["field_type"] == "multiple_attachment_on_request") {
                    // Extract JSON

                    $var = "";

                    $res = json_decode($oldValue, true);
                    $resNew = json_decode($newValue, true);

                    $old = "";
                    $new = "";
                    // OLD
                    foreach ($res as $oldres) {
                        $old .= " " . $oldres['file_name'] . ",";
                    }
                    // NEW
                    foreach ($resNew as $Newres) {
                        $new .= " " . $Newres['file_name'] . ",";
                    }

                    $oldResVal = substr($old, 0, -1);
                    $newResVal = substr($new, 0, -1);

                    $logDoc->details = $field["data_field_label"] . ' has been added "' . $newResVal . '"';
                } else {
                    $logDoc->details = $field["data_field_label"] . ' has been changed from "' . $oldValue . '" to "' . $newValue . '"';
                }
                $logDoc->created_by_id = $this->auth['id'];
                $logDoc->date_created = $date;
                $logDoc->save();
            }
        }
    }

    public function createRequireComment($comment) {
//        $auth = Auth::getAuth('current_user');
        if ($comment == "") {
            return false;
        }
        $post = new post();
        $post->createComment($comment, 1, $this->id, $this->auth, $this->formId);
    }

    public function emailCancelledRequest() {
//        $auth = Auth::getAuth('current_user');
        //get request details 
        $processWF = $this->getNextWorflowNode(true);
        //get Processor
        $requestDetails = $this->query("SELECT FORM.*, concat_ws(' ',REQUESTOR.first_name,REQUESTOR.last_name) as Requestor_Fullname, REQUESTOR.display_name as Requestor_Name, 
                CASE FORM.ProcessorType
                    WHEN 1 THEN 
                        (SELECT user_processor.display_name FROM tbuser user_processor WHERE user_processor.id = getDepartmentUsers(FORM.Processor, FORM.ProcessorLevel, {$this->auth["company_id"]}))
                    WHEN 2 THEN 
                        (SELECT Position FROM tbpositions WHERE ID=FORM.Processor)
                    ELSE 
                        PROCESSOR.display_name
                END AS Processor_Name, 
                CASE FORM.ProcessorType
                    WHEN 1 THEN 
                        getDepartmentUsers(FORM.Processor, FORM.ProcessorLevel, {$this->auth["company_id"]})
                    WHEN 2 THEN 
                        getUsersByPosition(FORM.Processor)
                    ELSE
                        FORM.Processor
                END AS Processor, 
            CASE 
                    WHEN FORM.Editor IS NULL THEN getRequestUsers(302, FORM.ID, 1)
                    ELSE
                        ''
            END AS Editor FROM " . $this->formName . " FORM 
                LEFT JOIN tbuser REQUESTOR
                ON REQUESTOR.id = FORM.Requestor
                LEFT JOIN tbuser PROCESSOR
                ON PROCESSOR.id = FORM.Processor
                WHERE FORM.id = {$this->escape($this->id)}", "row");
        $this->sendMail($processWF, $requestDetails, true);
    }

    public function getNextWorflowNode($is_cancel = false) {


        if ($is_cancel) {
            $email_field = "WFO.email_cancel";
        } else {
            $email_field = "WFO.email";
        }

        $str_query = "SELECT
                FRM.TrackNo,
                FRM.Requestor,
                FRM.UpdatedBy,
                WFO.type_rel as NodeType, 
                WFO.Processor as Processor,
                PROCESSOR.department_position_level as Department_Position_Level,
                PROCESSOR.department_id as Processor_Department_Id,
                WFO.Status as Status,
                WFO.buttonStatus as Buttons,
                WFO.ProcessorType as ProcessorType, 
                WFO.condition_return as ConditionAction,
                WFO.field as Field,
                WFO.operator as Operator,
                WFO.field_value as FieldValue,
                WFO.fieldEnabled as WF_FieldEnabled,
                WFO.fieldRequired as WF_FieldRequired,
                WFO.fieldHiddenValue as WF_FieldHiddenValue,
                {$email_field} as Email,
                WFO.enable_delegate as enable_delegate,
                WFO.noti_message as Noti_Message,
                WFO.noti_enabled as Noti_Enabled,
                WFO.sms as SMS,
                WFO.json as WorkflowData,
                FIELD.field_type as Condition_Field_Type
                FROM " . $this->formName .
                " FRM LEFT JOIN tbworkflow WF 
                ON WF.id = FRM.Workflow_Id
                LEFT JOIN tbworkflow_objects WFO
                ON WFO.Workflow_Id = WF.Id AND WFO.Object_Id = FRM.Node_Id
                LEFT JOIN tbuser PROCESSOR
                ON PROCESSOR.Id = FRM.UpdatedBy
                LEFT JOIN tbfields FIELD
                ON FIELD.field_name = WFO.field
                WHERE FRM.Id = {$this->escape($this->id)}";

        // print_r($str_query); commented by japhet 03-16-2016 nag eerror yung request sa embedded view
        $processWF = $this->query($str_query, "row");


        return $processWF;
    }

    private function parseConditionNode($processWF) {
        $jsonAction = json_decode($processWF['ConditionAction'], true);
        $trueNode = $jsonAction['True']['child_id'];
        $falseNode = $jsonAction['False']['child_id'];

        $field = $processWF['Field'];
        $operator = $processWF['Operator'];
        $field_value = $processWF['FieldValue'];
        $form_field_value = $this->data[$field];
        $form_condition_field_type = $processWF["Condition_Field_Type"];

        switch ($operator) {
            case '==';
                if ($form_field_value == $field_value) {
                    $targetNode = $trueNode;
                } else {
                    $targetNode = $falseNode;
                }
                break;
            case '<':
                if ($form_condition_field_type == "datepicker" ||
                        $form_condition_field_type == "dateTime" || $form_condition_field_type == "time") {
                    if ($form_field_value < $field_value) {
                        $targetNode = $trueNode;
                    } else {
                        $targetNode = $falseNode;
                    }
                } else {
                    if (floatval($form_field_value) < floatval($field_value)) {
                        $targetNode = $trueNode;
                    } else {
                        $targetNode = $falseNode;
                    }
                }
                break;
            case '>':

                if ($form_condition_field_type == "datepicker" ||
                        $form_condition_field_type == "dateTime" || $form_condition_field_type == "time") {
                    if ($form_field_value > $field_value) {
                        $targetNode = $trueNode;
                    } else {
                        $targetNode = $falseNode;
                    }
                } else {
                    if (floatval($form_field_value) > floatval($field_value)) {
                        $targetNode = $trueNode;
                    } else {
                        $targetNode = $falseNode;
                    }
                }

                break;
            case '<=':
                if ($form_condition_field_type == "datepicker" ||
                        $form_condition_field_type == "dateTime" || $form_condition_field_type == "time") {
                    if ($form_field_value <= $field_value) {
                        $targetNode = $trueNode;
                    } else {
                        $targetNode = $falseNode;
                    }
                } else {
                    if (floatval($form_field_value) <= floatval($field_value)) {
                        $targetNode = $trueNode;
                    } else {
                        $targetNode = $falseNode;
                    }
                }
                break;
            case '>=':

                if ($form_condition_field_type == "datepicker" ||
                        $form_condition_field_type == "dateTime" || $form_condition_field_type == "time") {
                    if ($form_field_value >= $field_value) {
                        $targetNode = $trueNode;
                    } else {
                        $targetNode = $falseNode;
                    }
                } else {
                    if (floatval($form_field_value) >= floatval($field_value)) {
                        $targetNode = $trueNode;
                    } else {
                        $targetNode = $falseNode;
                    }
                }


                break;
            case '!=':
                if ($form_field_value != $field_value) {
                    $targetNode = $trueNode;
                } else {
                    $targetNode = $falseNode;
                }
                break;
        }

        $wfFields['Node_Id'] = $targetNode;
        $conditionFields['ID'] = $this->id;
        $this->update($this->formName, $wfFields, $conditionFields);
    }

    public function processWorkflowEvents() {
        //users editors
        $processWF = $this->getNextWorflowNode();
        $workflowData = json_decode($processWF['WorkflowData'], true);
        $wfFields = $this->data;

        $users = $workflowData['users'];
        $editors = $users["user_editor"];
        $viewers = $users["user_viewer"];

        $this->deletePreviousUsers();

        foreach ($editors["positions"] as $position_id) {
            $this->addUser($position_id, 1, 1);
        }

        foreach ($editors["departments"] as $department_code) {
            $this->addUser($department_code, 1, 2);
        }

        foreach ($editors["users"] as $user_id) {
            $this->addUser($user_id, 1, 3);
        }

        if ($editors["requestor"]) {
            $this->addUser($processWF['Requestor'], 1, 3);
        }

        foreach ($editors["groups"] as $group_id) {
            $this->addUser($group_id, 1, 4);
        }

        foreach ($this->additionalEditors as $display_name) {
            $personDoc = functions::getUsers("WHERE display_name={$this->escape($display_name)} AND company_id={$this->auth["company_id"]}")[0];
            $this->addUser($personDoc->id, 1, 3);
        }

        //users viewer
        foreach ($viewers["positions"] as $position_id) {
            $this->addUser($position_id, 2, 1);
        }

        foreach ($viewers["departments"] as $department_code) {
            $this->addUser($department_code, 2, 2);
        }


        foreach ($viewers["users"] as $user_id) {
            $this->addUser($user_id, 2, 3);
        }

        if ($viewers["requestor"]) {
            $this->addUser($processWF['Requestor'], 2, 3);
        }

        foreach ($viewers["groups"] as $group_id) {
            $this->addUser($group_id, 2, 4);
        }

        foreach ($this->additionalReaders as $display_name) {
            $personDoc = functions::getUsers("WHERE display_name={$this->escape($display_name)} AND company_id={$this->auth["company_id"]} ")[0];
            $this->addUser($personDoc->id, 2, 3);
        }

        //compute computed fields on server side
        $this->computeFields();

        //process triggers 
        if ($this->process_triggers == true) {
            $this->processTriggers($workflowData);
        }

        //send mail
        if ($this->send_email == true) {
            $this->sendMail($processWF, $wfFields, false, $workflowData);
        }

        //send sms
//        $this->sendSMS($processWF, $wfFields);
        //save audit log
        $this->saveLog($wfFields);
        $this->unlockDocument();
    }

    public function processWF($currentUser) {
        do {
            $processWF = $this->getNextWorflowNode();
            $node_type = $processWF['NodeType'];

            if ($node_type == 3) {
                //conditions
                $this->parseConditionNode($processWF);
            } elseif ($node_type == 5) {
                //process external databases
                $this->processDatabaseTriggers($processWF);
            } else {
                //condition loop will stop after this code

                $wfFields['Status'] = $processWF['Status'];
                $wfFields['LastAction'] = $processWF['Buttons'];

                $wfFields['fieldEnabled'] = $processWF['WF_FieldEnabled'];
                $wfFields['fieldRequired'] = $processWF['WF_FieldRequired'];
                $wfFields['fieldHiddenValues'] = $processWF['WF_FieldHiddenValue'];

                $workflowData = json_decode($processWF['WorkflowData'], true);

                $wfFields['SaveFormula'] = $workflowData['workflow_hw_save'];
                $wfFields['CancelFormula'] = $workflowData['workflow_hw_cancel'];
                $wfFields['enable_delegate'] = $processWF['enable_delegate'];

                $processor_type = $processWF['ProcessorType'];


                if ($processor_type == 1) {//head/assistant head
                    $user_department_position_level = $processWF['Department_Position_Level'];
                    $processorType = $processWF['Processor'];
                    $processor_department = $processWF['Processor_Department_Id'];

                    do {
                        //loop until processor is found


                        $parent_department = $this->query("SELECT PARENT_DEPARTMENT.id as Parent_Department FROM tborgchartobjects CHILD_DEPARTMENT
                        LEFT JOIN tborgchartline DEPARMENTLINE 
                        ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id AND DEPARMENTLINE.orgChart_id = CHILD_DEPARTMENT.orgChart_id 
                        LEFT JOIN tborgchartobjects PARENT_DEPARTMENT 
                        ON PARENT_DEPARTMENT.object_id = DEPARMENTLINE.parent AND PARENT_DEPARTMENT.orgChart_id  = DEPARMENTLINE.orgChart_id
                        WHERE CHILD_DEPARTMENT.id ={$this->escape($processor_department)} LIMIT 1 ", "row");

                        $parent_department_id = $parent_department['Parent_Department'];

                        if ($user_department_position_level == 1 && $parent_department_id != "") {
                            $processor_department = $parent_department_id;
                        }

                        if ($user_department_position_level == 2 && $processorType == 2 && $parent_department_id != "") {
                            $processor_department = $parent_department_id;
                        }

                        $processor_result = $this->query("SELECT json, department_code
                                    FROM tborgchartobjects DEPARTMENT WHERE id = {$this->escape($processor_department)}", "row");

                        $new_processor = $processor_result["department_code"];

                        if ($processorType == 1) {
                            $department_data = json_decode($processor_result['json'], true);
                            $processor = $department_data['orgchart_user_head'][0];


                            if ($processor == '' || $processor == 'null' || $processor == null || $processor == 0) {//if head not found request will be forwarded to assistant head of the parent department
                                $processor_department = $parent_department_id;
                                $processorType = 2;
                            }
                        } else {
                            //assistant head
                            $department_data = json_decode($processor_result['json'], true);
                            $processor = $department_data['orgchart_dept_assistant'][0];

                            if ($processor == '' || $processor == 'null' || $processor == null || $processor == 0) {//if assistant head not found request will be forwarded to head
//                                $new_processor = $processor_result["department_code"];
                                $processorType = 1;
                            }
                        }
                    } while (($processor == '' || $processor == 'null' || $processor == null || $processor == 0) && $parent_department_id != '');

                    if ($processor == '' || $processor == 'null' || $processor == null || $processor == 0) {
                        //if the system detects that the there's no next processor
                        $wfFields['Processor'] = $processWF['UpdatedBy'];
                        $wfFields["ProcessorType"] = 3;
                        $wfFields["ProcessorLevel"] = $processorType;
                    } else {
                        $wfFields['Processor'] = $new_processor;
                        $wfFields["ProcessorLevel"] = $processorType;
                        $wfFields["ProcessorType"] = $processor_type;
                    }
                }


                if ($processor_type == 2) {//position
//                    $processor_result = $this->query("SELECT group_concat(user_processor.id separator ',') as Processor
//                        FROM tbuser user_processor 
//                        WHERE user_processor.Position = {$this->escape($processWF['Processor'])} AND company_id={$auth['company_id']}", "row");
                    $wfFields['Processor'] = $processWF['Processor'];
                    $wfFields["ProcessorType"] = $processor_type;
                }

                if ($processor_type == 3) {//Specific User
                    $wfFields['Processor'] = $processWF['Processor'];
                    $wfFields["ProcessorType"] = $processor_type;
                }

                if ($processor_type == 4) {//Requestor
                    $wfFields['Processor'] = $processWF['Requestor'];
                    $wfFields["ProcessorType"] = $processor_type;
                }

                if ($processor_type == 5) {//Computed Field
                    //added and modified by michael e. may 24, 2016
                    $tb_field_type = $this->query("SELECT field_type
                        WHERE form_id = '" . $this->formId . "' AND field_name = '" . $processWF['Processor'] . "' LIMIT 1", "row");

                    //GETTING THE VALUE OF THE FIELD
                    $field_display_name = $this->query("SELECT " . $processWF['Processor'] .
                            " as Display_name FROM " . $this->formName . " WHERE id = {$this->id}", "row");
                    //for multiple (PREPARED WHEN VALUE IS MULTIPLE)
                    //the required basis of the field when specific type of field is selected 
                    $field_based_processor = "";
                    if ($tb_field_type == "contactNumber") { //DITO NA AKO
                        $field_display_name_arr = explode(",", $field_display_name["Display_name"]); //IBA UNG DELIMITER NG CONTACT
                        // array_map(function($value){
                        //     if(strlen($value) == 11){
                        //     }else if(strlen($value) == 13){
                        //     }else if(strlen($value) == 12){
                        //     }else{
                        //     }
                        //     return false;
                        // }, $field_display_name_str);
                        $field_display_name_str = "'" . implode("','", $field_display_name_arr) . "'";
                        $field_based_processor = "(contact_number IN ({$field_display_name_str}) AND user_level_id = 5)"; //contact number and mobile user level type
                    } else {
                        $field_display_name_arr = explode("|", $field_display_name["Display_name"]);
                        $field_display_name_str = "'" . implode("','", $field_display_name_arr) . "'";
                        $field_based_processor = "Display_Name IN ({$field_display_name_str})"; //display name basis for other values
                    }
                    $processor_result = $this->query("SELECT GROUP_CONCAT(id) as Processor
                                                        FROM tbuser
                                                        WHERE " . $field_based_processor . " 
                                                            AND company_id={$this->auth['company_id']}", "row");
                    $wfFields['Processor'] = $processor_result['Processor'];
                    $wfFields["ProcessorType"] = $processor_type;
                }

                if ($processor_type == 6) {//group
//                    $processor_result = $this->query("SELECT group_concat(user_processor.id separator ',') as Processor
//                        FROM tbuser user_processor 
//                        WHERE user_processor.Position = {$this->escape($processWF['Processor'])} AND company_id={$auth['company_id']}", "row");
                    $wfFields['Processor'] = $processWF['Processor'];
                    $wfFields["ProcessorType"] = $processor_type;
                }

                if ($processor_type == "" || $processor_type == "null" || $processor_type == null) {
                    $wfFields['Processor'] = "";
                }

                //  Added by ervinne: update the data with the processor:
                $this->data['Processor'] = $wfFields['Processor'];


                if ($this->process_workflow_events) {
                    //users editors
                    $users = $workflowData['users'];
                    $editors = $users["user_editor"];
                    $viewers = $users["user_viewer"];

                    $this->deletePreviousUsers();

                    foreach ($editors["positions"] as $position_id) {
                        $this->addUser($position_id, 1, 1);
                    }

                    foreach ($editors["departments"] as $department_code) {
                        $this->addUser($department_code, 1, 2);
                    }

                    foreach ($editors["users"] as $user_id) {
                        $this->addUser($user_id, 1, 3);
                    }

                    if ($editors["requestor"]) {
                        $this->addUser($processWF['Requestor'], 1, 3);
                    }

                    foreach ($editors["groups"] as $group_id) {
                        $this->addUser($group_id, 1, 4);
                    }



                    foreach ($this->additionalEditors as $display_name) {
                        $personDoc = functions::getUsers("WHERE display_name={$this->escape($display_name)} AND company_id={$this->auth["company_id"]}")[0];
                        $this->addUser($personDoc->id, 1, 3);
                    }

                    //users viewer

                    foreach ($viewers["positions"] as $position_id) {
                        $this->addUser($position_id, 2, 1);
                    }

                    foreach ($viewers["departments"] as $department_code) {
                        $this->addUser($department_code, 2, 2);
                    }

                    foreach ($viewers["users"] as $user_id) {
                        $this->addUser($user_id, 2, 3);
                    }

                    if ($viewers["requestor"]) {
                        $this->addUser($processWF['Requestor'], 2, 3);
                    }

                    foreach ($viewers["groups"] as $group_id) {
                        $this->addUser($group_id, 2, 4);
                    }



                    foreach ($this->additionalReaders as $display_name) {
                        $personDoc = functions::getUsers("WHERE display_name={$this->escape($display_name)} AND company_id={$this->auth["company_id"]} ")[0];
                        $this->addUser($personDoc->id, 2, 3);
                    }
                }


                $conditionFields['ID'] = $this->id;
                $this->update($this->formName, $wfFields, $conditionFields);

//               compute computed fields on server side
                if ($this->process_workflow_events == true) {
                    $this->computeFields();

                    //process triggers 

                    if ($this->process_triggers == true) {
                        $this->processTriggers($workflowData);
                    }

                    //send mail
                    if ($this->send_email == true) {
                        $this->sendMail($processWF, $wfFields, false, $workflowData);
                    }

                    //send sms
//               $this->sendSMS($processWF, $wfFields);
                    //save audit log
                    $this->saveLog($wfFields);
                }

                //create notification
                $wfFields['Noti_Message'] = $processWF['Noti_Message'];
                // check if notification is enabled
                if ($processWF['Noti_Enabled'] == "1") {
                    //send notification if form is displayed
                    $formDoc = new Form($this, $this->formId);
                    if ($this->send_notification == true) {
                        $this->createNotification($wfFields, $processWF);
                    }
                }
            }
        } while ($node_type == 3 || $node_type == 5);
    }

    public function processDatabaseTriggers($processWF) {
        $workflowData = json_decode($processWF['WorkflowData'], true);
        $external_database_triggers = $workflowData["workflow-external-trigger"];
        //export
        $export_triggers = $external_database_triggers["upload-ext"];
        foreach ($export_triggers as $trigger) {
            $this->process_ExtDBTriggers($trigger);
        }

        $wfFields['Node_Id'] = $processWF['Buttons'];
        $conditionFields['ID'] = $this->id;
        $this->update($this->formName, $wfFields, $conditionFields);
    }

    private function getExtDBConnection($data_source) {
        $config = $data_source[0];

        if ($data_source[0]["db_type"] == 1) {
            $conn = new MySQLDatabase($config["db_hostname"], $config["db_name"], $config["db_username"], $config["db_password"]);
        } elseif ($data_source[0]["db_type"] == 2) {
            $conn = new MSSQLDatabase($config["db_hostname"], $config["db_name"], $config["db_username"], $config["db_password"]);
        } elseif ($data_source[0]["db_type"] == 3) {
            $conn = new OracleDatabase($config["db_hostname"], $config["db_name"], $config["db_username"], $config["db_password"]);
        }

        return $conn;
    }

    private function extDB_populateFieldsValue($fields) {
        $return_array = array();
        foreach ($fields as $field) {
            $ms_sql_field_name = $field["workflow-field-update"];
            $field_formula = $field["workflow-field-formula"];
            $formulaDoc = new Formula($field_formula);
            $formulaDoc->DataFormSource[0] = $this->data;
            $field_value = $formulaDoc->evaluate();
            if ($ms_sql_field_name != "" && $ms_sql_field_name != "0" && $field_value != "0000-00-00" && $field_value != "0000-00-00 00:00:00" && $field_value != "00:00:00") {
                $return_array[$ms_sql_field_name] = $formulaDoc->evaluate();
            }
        }

        return $return_array;
    }

    public function process_ExtDBTriggers($trigger) {
        $table_name = $trigger["workflow-form-trigger"];
        $action = $trigger["workflow-trigger-action"];
        $fields = $trigger["workflow-field_formula"];
        $connection_id = $trigger["workflow-db-connection"];
        $data_source = getExternalDatabase($this->conn, " WHERE  company_id={$this->auth["company_id"]} AND id={$connection_id}"
                . " AND is_active=1 ");

        $conn = $this->getExtDBConnection($data_source);

        if ($conn->is_connected) {
            if ($action == "Create") {
                $insert_array = $this->extDB_populateFieldsValue($fields);
                $conn->insert($table_name, $insert_array);
            } elseif ($action == "Update Response") {
                $condition_array = array($trigger["workflow-ref-field-parent"] => $this->data[$trigger["workflow-ref-field"]]);

                $update_array = $this->extDB_populateFieldsValue($fields);
                $conn->update($table_name, $update_array, $condition_array);
            }

            $conn->disConnect();
        }

        unset($conn);
    }

    public function processTriggers($workflowData) {
        $trigger_data = $workflowData['workflow-trigger'];
        foreach ($trigger_data as $row) {
            if ($row['workflow-trigger-action'] == 'Create') {
                //form diverge
                // $this->diverge($row);
                $this->createResponse($row);
            } else if ($row['workflow-trigger-action'] == 'Update Response') {
                //form diverge
                // $this->diverge($row);
                $this->updateResponse($row);
            } else if ($row['workflow-trigger-action'] == 'Post Submit') {
                //form diverge
                // $this->diverge($row);
                $this->executePostSubmit($row);
            } else {
                //update keyword
                $this->updateKeyword($row);
            }
        }
    }

    //modified by joshua reyes 03/04/2016
    public function executePostSubmit($row) {
//        $strFormula = $row["workflow-postSubmit-action"];
//        var_dump($row);
//        $formulaPost = new Formula($strFormula);
//        $formulaPost->evaluate();

        $requestFormDoc = new Form($this, $this->formId);
        $formula = $row["workflow-postSubmit-action"];
        $formulaDoc = new Formula($formula);
        $keyNames = $formulaDoc->getAllKeyNames();
        $filtered_keys = $this->sanitizeFields($keyNames[0]);

        $field = "";
        foreach ($filtered_keys as $value) {
            if (!empty($value)) {
                $field .= $value . ",";
            }
        }
        $field_string = substr($field, 0, strlen($field) - 1);

        $formulaDoc->setSourceForm($requestFormDoc->form_name, $field_string, array(
            array("FieldName" => "ID",
                "Operator" => "=",
                "Value" => $this->id))
        );
        $formulaDoc->evaluate();
    }

    //added by joshua reyes 03/04/2016
    //modified by joshua reyes& michael 07/01/2016
    private function sanitizeFields($key_name) {
        // Remove key_name that are listed in function_name
        $special_keys = [
            'Today', 'Now', 'RequestID', 'InsertID', 'Department', 'CurrentUser', 'TimeStamp'
        ];
        $count = count($key_name);
        for ($x = 0; $x < $count; $x++) {
            $key_word = str_replace('@', '', $key_name[$x]);
            if (function_exists($key_word)) {
                $key_name[$x] = '';
            } else if (in_array($key_word, $special_keys)) {
                $key_name[$x] = '';
            } else {
                $key_name[$x] = $key_word;
            }
        }
        return $key_name;
    }

    public function executePostSaveEvent($formula) {
        $strFormula = $formula;
        $formulaPost = new Formula($strFormula);
        $formulaPost->evaluate(); //dito hindi na kailangan ung standard formatted json
    }

    public function computeFields() {
        $fields = functions::getFields(" WHERE form_id = {$this->formId} AND field_type  IN ('textbox_editor_support','textbox_reader_support')");
        $data = array();
        $conditionFields = array();
        $GLOBALS['auth'] = $this->auth;

        foreach ($fields as $fieldDoc) {
            $return_value = "";
            if ($fieldDoc->formula_type == 'computed') {
                $formulaDoc = new Formula($fieldDoc->formula);
                $formulaDoc->DataFormSource[0] = $this->data;
                $return_value = $formulaDoc->evaluate();
            } else {
                $return_value = $this->data[$fieldDoc->name];
            }



            if ($fieldDoc->type == "textbox_editor_support") {
//              add as editor   
                $return_value_arr = explode("|", $return_value);
                foreach ($return_value_arr as $value_return) {
                    $personDoc = functions::getUsers("WHERE display_name={$this->escape($value_return)} AND company_id={$this->auth["company_id"]} ")[0];
                    $this->addUser($personDoc->id, 1, 3);
                }
            }

            if ($fieldDoc->type == "textbox_reader_support") {
//              add as reader
                $return_value_arr = explode("|", $return_value);
                foreach ($return_value_arr as $value_return) {
                    $personDoc = functions::getUsers("WHERE display_name={$this->escape($value_return)} AND company_id={$this->auth["company_id"]} ")[0];
                    $this->addUser($personDoc->id, 2, 3);
                }
            }

            $data[$fieldDoc->name] = $return_value;
        }

        $conditionFields['ID'] = $this->id;
        $this->update($this->formName, $data, $conditionFields);
    }

    public function deletePreviousUsers() {
        $result = $this->query("DELETE FROM tbrequest_users WHERE Form_ID={$this->formId} AND RequestID={$this->id}");
    }

    public function addUser($user, $action_type, $user_type) {
        $formDoc = new Form($this, $this->formId);

        $requestUserDoc = new Request_User($this);
        $requestUserDoc->requestID = $this->id;
        $requestUserDoc->form = $formDoc;
        $requestUserDoc->user = $user;
        $requestUserDoc->user_type = $user_type;
        $requestUserDoc->action_type = $action_type;
        $requestUserDoc->save();
    }

    public function createNotification($wfFields, $processWF) {
        $node_type = $processWF['NodeType'];

        if ($node_type != 4) {
            $processorType = $wfFields["ProcessorType"];
            $processorLevel = $wfFields["ProcessorLevel"];
            $processors = $wfFields['Processor'];
            $user_processors = array();

            if ($processorType == 1) {
                //get head or assistant head
                $user_processors = functions::getDepartmentUsers($this->auth, $processors, $processorLevel);
            } else if ($processorType == 2) {
                //per position
                $user_processors = functions::getUsersByPosition($this->auth, $processors);
            } else if ($processorType == 6) {
                //per position
                $user_processors = functions::getUsersObjectByGroup($processors);
            } else {
                //processor id
                $user_processors_arr = explode(",", $processors);
                foreach ($user_processors_arr as $user) {
                    $personDoc = new Person($this, $user);
                    array_push($user_processors, $personDoc);
                }
            }

            foreach ($user_processors as $personDoc) {
                $processor = $personDoc->id;
                notifications::insertNofiRequest($this->id, $this->auth['id'], $this->formName, $processor, $wfFields['Noti_Message']);
            }
        } else {//end node
            $requestor = $processWF["Requestor"];
            notifications::insertNofiRequest($this->id, $this->auth['id'], $this->formName, $requestor, $wfFields['Noti_Message']);
        }
//        foreach ($processors as $processor) {
//            notifications::insertNofiRequest($this->id, $auth['id'], $this->formName, $processor, $wfFields['Noti_Message']);
//        }
    }

    public function saveLog($wfFields) {

        $date = $this->currentDateTime();

        $logDoc = new Request_Log($this);
        $logDoc->form_id = $this->formId;
        $logDoc->request_id = $this->id;
        $logDoc->details = $wfFields['Status'];
        $logDoc->created_by_id = $this->auth['id'];
        $logDoc->date_created = $date;
        $logDoc->save();
    }

    public function sendMail($processWF, $wfFields, $is_cancelled = false, $workflowData) {
//        $auth = Auth:: getAuth('current_user');
        //send mail
        $trackingNo = $processWF['TrackNo'];
        $emailJSON = $processWF['Email'];
        $requestor = $processWF['Requestor'];
        $processor = $wfFields['Processor'];

        $personDoc = new Person($this, $requestor);
        $processorDoc = new Person($this, $processor);

        $emailArr = json_decode($emailJSON, true);
        $recipients = array();
        $recipient_department_users = array();
        $recipient_position_users = array();
        $cc = array();
        $cc_department_users = array();
        $cc_position_users = array();
        $bcc = array();
        $bcc_department_users = array();
        $bcc_position_users = array();


//        $requestFormDoc = new Form($this, $this->formId);
        //to
        $recipient_department = $emailArr['email_recpient']['departments'];
        $recipient_position = $emailArr['email_recpient']['positions'];
        $recipient_users = $emailArr['email_recpient']['users'];
        $recipient_group = $emailArr['email_recpient']['groups'];
        $recipient_requestor = $emailArr['email_recpient']['requestor'];
        $recipient_processor = $emailArr['email_recpient']['processor'];
        //new aaron
        $recipient_otherRecepient_type = $emailArr['email_recpient']['otherRecepient_type'];
        $recipient_otherRecepient = $emailArr['email_recpient']['otherRecepient'];

        //other recepient
        if ($recipient_otherRecepient_type != "") {
            if ($recipient_otherRecepient_type == "1") {

                //   var_dump($recipient_otherRecepient);
                $formulaDoc = new Formula($recipient_otherRecepient);
                $formulaDoc->DataFormSource[0] = $this->data;
//                $formulaDoc->setSourceForm($requestFormDoc->form_name, "*", array(
//                    array("FieldName" => "TrackNo", "Operator" => "=",
//                        "Value" => $this->trackNo))
//                );
//var_dump($formulaDoc->evaluate());
                $recipient_otherRecepient = $formulaDoc->evaluate();
            }

            $recipient_otherRecepient_array = explode('|', $recipient_otherRecepient);
            foreach ($recipient_otherRecepient_array as $value) {
                if ($value != "") {
                    $recipients[$value] = "Other User";
                }
            }
        }

        //per department
        $department_to = functions::getUserMailInfo($this->auth, $recipient_department, 1);
        $recipients = array_merge($recipients, $department_to);

        //per position
        $position_to = functions::getUserMailInfo($this->auth, $recipient_position, 2);
        $recipients = array_merge($recipients, $position_to);

        //per group
        $group_to = functions::getUserMailInfo($this->auth, $recipient_group, 3);
        $recipients = array_merge($recipients, $group_to);

        //per user
        foreach ($recipient_users as $id) {
            $userDoc = new Person($this, $id);
            $recipients[$userDoc->email] = $userDoc->display_name;
        }

        //requestor
        if ($recipient_requestor == 1) {
            $requestorDoc = new Person($this, $requestor);
            $recipients[$requestorDoc->email] = $requestorDoc->display_name;
        }

        //processor

        if ($recipient_processor == 1) {
            $processorType = $wfFields["ProcessorType"];
            $processorLevel = $wfFields["ProcessorLevel"];
            if ($is_cancelled) {
                $processorType = 3;
            }
            if ($processorType == 1) {
                //get head or assistant head
                $processor_to = functions::getDepartmentUserByLevel($this->auth, $processor, $processorLevel);
                $recipients = array_merge($recipients, $processor_to);
            } else if ($processorType == 2) {
                $processor_to = functions::getUserMailInfo($this->auth, array($processor), 2);
                $recipients = array_merge($recipients, $processor_to);
            } else if ($processorType == 6) {
                $processor_to = functions::getUserMailInfo($this->auth, array($processor), 3);
                $recipients = array_merge($recipients, $processor_to);
            } else {
                $to_processor_arr = explode(",", $processor);
                foreach ($to_processor_arr as $proc) {
                    $rr_processorDoc = new Person($this, $proc);
                    $recipients[$rr_processorDoc->email] = $rr_processorDoc->display_name;
                }
            }
        }

        //cc
        $cc_department = $emailArr['email_cc']['departments'];
        $cc_position = $emailArr['email_cc']['positions'];
        $cc_users = $emailArr['email_cc']['users'];
        $cc_group = $emailArr['email_cc']['groups'];
        $cc_requestor = $emailArr['email_cc'] ['requestor'];
        $cc_processor = $emailArr ['email_cc']['processor'];
        //new aaron
        $cc_otherRecepient_type = $emailArr['email_cc']['otherRecepient_type'];
        $cc_otherRecepient = $emailArr['email_cc']['otherRecepient'];

        //other recepient
        if ($cc_otherRecepient_type != "") {
            if ($cc_otherRecepient_type == "1") {
                $formulaDoc = new Formula($cc_otherRecepient);
                $formulaDoc->DataFormSource[0] = $this->data;
//                $formulaDoc->setSourceForm($requestFormDoc->form_name, "*", array(
//                    array("FieldName" => "TrackNo",
//                        "Operator" => "=",
//                        "Value" => $this->trackNo))
//                );
                $cc_otherRecepient = $formulaDoc->evaluate();
            }

            $cc_otherRecepient_array = explode('|', $cc_otherRecepient);
            foreach ($cc_otherRecepient_array as $value) {
                if ($value != "") {
                    $cc[$value] = "Other User";
                }
            }
        }

        //per department
        $department_cc = functions::getUserMailInfo($this->auth, $cc_department, 1);
        $cc = array_merge($cc, $department_cc);

        //per position
        $position_cc = functions::getUserMailInfo($this->auth, $cc_position, 2);
        $cc = array_merge($cc, $position_cc);

        //per group
        $group_cc = functions::getUserMailInfo($this->auth, $cc_group, 3);
        $cc = array_merge($cc, $group_cc);

        //per user
        foreach ($cc_users as $id) {
            $userDoc = new Person($this, $id);
            $cc[$userDoc->email] = $userDoc->display_name;
        }

        //requestor
        if ($cc_requestor == 1) {
            $requestorDoc = new Person($this, $requestor);
            $cc[$requestorDoc->email] = $requestorDoc->display_name;
        }

        //processor
        if ($cc_processor == 1) {
            $processorType = $wfFields["ProcessorType"];
            $processorLevel = $wfFields["ProcessorLevel"];
            if ($is_cancelled) {
                $processorType = 3;
            }
            if ($processorType == 1) {
                //get head or assistant head
                $processor_cc = functions::getDepartmentUserByLevel($this->auth, $processor, $processorLevel);
                $cc = array_merge($cc, $processor_cc);
            } else if ($processorType == 2) {
                $processor_cc = functions::getUserMailInfo($this->auth, array($processor), 2);
                $cc = array_merge($cc, $processor_cc);
            } else if ($processorType == 6) {
                $processor_cc = functions::getUserMailInfo($this->auth, array($processor), 3);
                $cc = array_merge($cc, $processor_cc);
            } else {
                $cc_processor_arr = explode(",", $processor);
                foreach ($cc_processor_arr as $proc) {
                    $rr_processorDoc = new Person($this, $processor);
                    $cc[$rr_processorDoc->email] = $rr_processorDoc->display_name;
                }
            }
//            $processorDoc = new Person($this, $processor);
//            $cc[$processorDoc->email] = $processorDoc->display_name;
        }

        //bcc
        $bcc_department = $emailArr['email_bcc']['departments'];
        $bcc_position = $emailArr['email_bcc']['positions'];
        $bcc_users = $emailArr['email_bcc']['users'];
        $bcc_group = $emailArr['email_bcc']['groups'];
        $bcc_requestor = $emailArr['email_bcc']['requestor'];
        $bcc_processor = $emailArr['email_bcc'] ['processor'];
        //new aaron
        $bcc_otherRecepient_type = $emailArr['email_bcc']['otherRecepient_type'];
        $bcc_otherRecepient = $emailArr['email_bcc']['otherRecepient'];

        //other recepient
        if ($bcc_otherRecepient_type != "") {
            if ($bcc_otherRecepient_type == "1") {
                $formulaDoc = new Formula($bcc_otherRecepient);
                $formulaDoc->DataFormSource[0] = $this->data;
//                $formulaDoc->setSourceForm($requestFormDoc->form_name, "*", array(
//                    array("FieldName" => "TrackNo",
//                        "Operator" => "=",
//                        "Value" => $this->trackNo))
//                );
                $bcc_otherRecepient = $formulaDoc->evaluate();
            }

            $bcc_otherRecepient_array = explode('|', $bcc_otherRecepient);
            foreach ($bcc_otherRecepient_array as $value) {
                if ($value != "") {
                    $bcc[$value] = "Other User";
                }
            }
        }
        //per department

        $department_bcc = functions::getUserMailInfo($this->auth, $bcc_department, 1);
        $bcc = array_merge($bcc, $department_bcc);

        //per position
        $position_bcc = functions::getUserMailInfo($this->auth, $bcc_position, 2);
        $bcc = array_merge($bcc, $position_bcc);

        //per group
        $group_bcc = functions::getUserMailInfo($this->auth, $bcc_group, 3);
        $bcc = array_merge($bcc, $group_bcc);

        //per user
        foreach ($bcc_users as $id) {
            $userDoc = new Person($this, $id);
            $bcc[$userDoc->email] = $userDoc->display_name;
        }

        //requestor
        if ($bcc_requestor == 1) {
            $requestorDoc = new Person($this, $requestor);
            $bcc[$requestorDoc->email] = $requestorDoc->display_name;
        }

        //processor
        if ($bcc_processor == 1) {
            $processorType = $wfFields["ProcessorType"];
            $processorLevel = $wfFields["ProcessorLevel"];
            if ($is_cancelled) {
                $processorType = 3;
            }
            if ($processorType == 1) {
                //head or assistant head
                $processor_bcc = functions::getDepartmentUserByLevel($this->auth, $processor, $processorLevel);
                $bcc = array_merge($bcc, $processor_bcc);
            } else if ($processorType == 2) {
                $processor_bcc = functions::getUserMailInfo($this->auth, array($processor), 2);
                $bcc = array_merge($cc, $processor_bcc);
            } else if ($processorType == 6) {
                $processor_bcc = functions::getUserMailInfo($this->auth, array($processor), 3);
                $bcc = array_merge($cc, $processor_bcc);
            } else {
                $bcc_processor_arr = explode(",", $processor);
                foreach ($bcc_processor_arr as $proc) {
                    $rr_processorDoc = new Person($this, $processor);
                    $bcc[$rr_processorDoc->email] = $rr_processorDoc->display_name;
                }
            }
        }

        //subject and body
        $subject_type = $emailArr['email-title-type'];
        $subject_message = $emailArr['title'];
        $body_type = $emailArr['email-message-type'];
        $body_message = $emailArr['message'];
        // $requestFormDoc = new Form($this, $this->formId);

        if ($subject_type == 0) {
            $subject = $emailArr['title'];
        } else {
            $subject = "";
            if (trim($emailArr['title']) != "") {
                $formulaDoc = new Formula($emailArr['title']);
                $formulaDoc->DataFormSource[0] = $this->data;
//                $formulaDoc->setSourceForm($requestFormDoc->form_name, "*", array(
//                    array("FieldName" => "TrackNo",
//                        "Operator" => "=",
//                        "Value" => $this->trackNo))
//                );

                $subject = $formulaDoc->evaluate();
            }
        }

        if ($body_type == 0) {
            $message = $emailArr['message'];
        } else {
            $message = "";
            if (trim($emailArr['message']) != "") {
                $formulaDoc = new Formula($emailArr['message']);
                $formulaDoc->DataFormSource[0] = $this->data;
//                $formulaDoc->setSourceForm($requestFormDoc->form_name, "*", array(
//                    array("FieldName" => "TrackNo",
//                        "Operator" => "=",
//                        "Value" => $this->trackNo))
//                );

                $message = $formulaDoc->evaluate();
            }
        }

        $from = SMTP_FROM_EMAIL;
        $from_title = SMTP_FROM_NAME;
        $company_id = $this->auth['company_id'];
        $formID = $this->formId;
        $requestID = $this->id;

//
//        print_r($recipients);
//        echo "<br/>";
//
//        print_r($cc);
//
//        echo "<br/>";
//        print_r($bcc);
//        echo "<br/>";

        if (trim($subject) == "") {
            $formDoc = new Form($this, $this->formId);
            $subject = $formDoc->form_name;
        }

        if (trim($message) == "") {
            if (!$is_cancelled) {
                $message = $personDoc->display_name . " has submitted a request for processing.";
            } else {
                $message = $personDoc->display_name . " has cancelled the request.";
            }
        }

//        var_dump($subject);
//        var_dump($message);
//        var_dump($is_cancelled);
//        var_dump($recipients);
        $mail_info = array(
            "To" => $recipients,
            "CC" => $cc,
            "BCC" => $bcc,
            "From" => $from,
            "Title" => $subject,
            "From_Title" => $from_title,
            "Body" => $message,
            "Company_Id" => $company_id,
            "TrackNo" => $trackingNo,
            "Form_Id" => $formID,
            "Request_Id" => $requestID,
            "Requestor_name" => $personDoc->first_name . " " . $personDoc->last_name,
            "Processor_name" => $processorDoc->first_name . " " . $processorDoc->last_name,
            "EnableGuest" => $workflowData["workflow_guest_enabled"]
        );

//        var_dump($mail_info);
        // print_r("recepient: ");
        // print_r($recipients);
        // print_r("<br />cc: ");
        // print_r($cc);
        // print_r("<br />bcc: ");
        // print_r($bcc);
        $emailDoc = new Mail_Notification();
        $json_decode = Settings::noti_settings($processor, 'notifications');

        $json_request = $json_notifications['Request'];
        if ($json_request[0] == 1) {
            // var_dump(1);
            $emailDoc->workflow_notify_user($mail_info);
        }
        //$json_decode = Settings::noti_settings($processor,'notifications');
        //$json_notifications = $json_decode[0]['notifications'];
        //$json_request = $json_notifications['Request'];
        //if($json_request[0]==1){

        if (trim($subject) != "" || trim($message) != "") {
            // var_dump(2);
            $emailDoc->workflow_notify_user($mail_info);
        }

        //}
    }

    public function sendSMS($processWF, $wfFields) {
//        $auth = Auth::getAuth('current_user');
        //send sms
        $smsArr = json_decode($processWF['SMS'], true);
        $sms_recipients = array();

        $sms_recipient_department = $smsArr['departments'];
        $sms_recipient_position = $smsArr['positions'];
        $sms_recipient_users = $smsArr['users'];
        $sms_recipient_requestor = $smsArr['requestor'];
        $sms_recipient_processor = $smsArr['processor'];
        $sms_recipient_contact = $smsArr['contact'];
        $sms_recipient_contact_type = $smsArr['recipient-type'];


        if ($smsArr['message-type'] == '0') {
            $message = $smsArr['message'];
        } else {
//            $requestFormDoc = new Form($this, $this->formId);
            $formulaDoc = new Formula($smsArr['message']);
            $formulaDoc->DataFormSource[0] = $this->data;
//            $formulaDoc->setSourceForm($requestFormDoc->form_name, "*", array(
//                array("FieldName" => "TrackNo", "Operator" => "=",
//                    "Value" => $this->trackNo)));

            $message = $formulaDoc->evaluate();
        }

        $sms_recipient_department_users = array();
        $sms_recipient_position_users = array();
        //per department

        foreach ($sms_recipient_department as $deparment) {
            array_push($sms_recipient_department_users, functions::getUsers("WHERE department_id ={$this->escape($deparment)
                            }"));
        }

        foreach ($sms_recipient_department_users as $department_users) {
            foreach ($department_users as $userDoc) {
                array_push($sms_recipients, $userDoc->contact_number);
            }
        }


        //per position
        foreach ($sms_recipient_position as $position) {
            array_push($sms_recipient_position_users, functions::getUsers("WHERE position ={$this->escape($position)} AND company_id={$this->escape($this->auth['company_id'])}"));
        }

        foreach ($sms_recipient_position_users as $position_users) {
            foreach ($position_users as $userDoc) {
                array_push($sms_recipients, $userDoc->contact_number);
            }
        }

        //per user
        foreach ($sms_recipient_users as $id) {
            $userDoc = new Person($this, $id);
            array_push($sms_recipients, $userDoc->contact_number);
        }

        //requestor
        if ($sms_recipient_requestor) {
            $requestorDoc = new Person($this, $processWF['Requestor']);
            array_push($sms_recipients, $requestorDoc->contact_number);
        }

        //processor
        if ($sms_recipient_processor) {
            $processorDoc = new Person($this, $wfFields['Processor']);
            array_push($sms_recipients, $processorDoc->contact_number);
        }

        //contact_number
        if ($sms_recipient_contact != '') {
            if ($sms_recipient_contact_type == "1") {
//                $requestFormDoc = new Form($this, $this->formId);
                $formulaDoc = new Formula($smsArr['contact']);
                $formulaDoc->DataFormSource[0] = $this->data;
//                $formulaDoc->setSourceForm($requestFormDoc->form_name, "*", array(
//                    array("FieldName" => "TrackNo",
//                        "Operator" => "=",
//                        "Value" => $this->trackNo))
//                );
                $sms_recipient_contact = $formulaDoc->evaluate();
            } $sms_recipient_contact_arr = explode(';', $sms_recipient_contact);
            foreach ($sms_recipient_contact_arr as $contact_number) {
                if ($contact_number != "") {
                    array_push($sms_recipients, $contact_number);
                }
            }
        }

        foreach ($sms_recipients as $number) {
            $smsDoc = new SMS($number, $message);
            $smsDoc->send();
        }
    }

    public function updateKeyword($workflowData) {
//        $auth = Auth::getAuth('current_user');

        $keyword_update = $workflowData['workflow-trigger-fields-update'];
        $keyword_filter = $workflowData['workflow-trigger-fields-filter'];
        $keyword_operator = $workflowData['workflow-trigger-operator'];
        $quantity_to_update = floatval($this->data[$keyword_update]);

        foreach ($this->keywordFields as $key) {
            if ($key['Field'] == $keyword_filter) {
                $keywords = functions::getKeywords("WHERE ValueCode={$this->escape($key['Code'])} AND company_id={$this->escape($this->auth['company_id'])}");

                foreach ($keywords as $keywordDoc) {
                    $quantity_old_value = floatval($keywordDoc->quantity);

                    switch ($keyword_operator) {
                        case 'Addition':
                            $quantity_new_value = floatval($quantity_to_update) + floatval($quantity_old_value);

                            break;
                        case 'Subtraction':
                            $quantity_new_value = floatval($quantity_old_value) - floatval($quantity_to_update);

                            break;

                        case 'Multiplication':
                            $quantity_new_value = floatval($quantity_old_value) * floatval($quantity_to_update);

                            break;
                        case 'Division':

                            $quantity_new_value = floatval($quantity_old_value) / floatval($quantity_to_update);

                            break;
                    }

                    $keywordDoc->quantity = $quantity_new_value;
                    $keywordDoc->update();
                }
            }
        }
    }

    public function createResponse($workflowData) {
        // $auth = Auth::getAuth('current_user');
        // $date = $this->currentDateTime();
        // $form_id = $workflowData['workflow-form-trigger'];
        // $actions = $workflowData['workflow-field_formula'];
        // $requestFormDoc = new Form($this, $this->formId);
        // $formDoc = new Form($this, $form_id);
        // $personDoc = new Person($this, $auth['id']);
        // $result = array();
        // $result['Requestor'] = $auth['id'];
        // $result['Processor'] = $auth['id'];
        // $result['Status'] = 'Draft';
        // $result['imported'] = '1';
        // $result['DateCreated'] = $date;
        // $result['DateUpdated'] = $date;
        // foreach ($actions as $key) {
        // $field_name = $key['workflow-field-update'];
        // $formula = $key['workflow-field-formula'];
        // $formulaDoc = new Formula($formula);
        // $formulaDoc->setSourceForm($requestFormDoc->form_name, "*", array(
        // array("FieldName" => "TrackNo",
        // "Operator" => "=",
        // "Value" => $this->trackNo))
        // );
        // $returnValue = $formulaDoc->evaluate();
        // $result[$field_name] = $returnValue;
        // }
        // $requestDoc = new Request();
        // $requestDoc->load($form_id, '0');
        // $requestDoc->data = $result;
        // $requestDoc->save();
        // $requestLog = new Request_Log($this);
        // $requestLog->form = $formDoc;
        // $requestLog->request_id = $requestDoc->id;
        // $requestLog->details = 'Created from ' . $this->formName . ' with Tracking Number : ' . $this->trackNo;
        // $requestLog->created_by = $personDoc;
        // $requestLog->date_created = $date;
        // $requestLog->save();

        $repsonses = array();
        $repsonses = Response::create($this, $workflowData);


        array_push($this->responseData["responses"], $repsonses);
    }

    public function updateResponse($workflowData) {

//        $auth = Auth::getAuth('current_user');

        $redis_cache = getRedisConnection();
        $date = $this->currentDateTime();
        $form_id = $workflowData['workflow-form-trigger'];
        $reference_field = $workflowData['workflow-ref-field'];
        $parent_refernce_field = $workflowData['workflow-ref-field-parent'];
        $actions = $workflowData['workflow-field_formula'];
        $repsonses = array();

//        $requestFormDoc = new Form($this, $this->formId);
        $formDoc = new Form($this, $form_id);
        $result = array();

        $formulaDoc = new Formula();
        $formulaDoc->DataFormSource[0] = $this->data;
//        $formulaDoc->setSourceForm($requestFormDoc->form_name, "*", array(
//            array("FieldName" => "TrackNo",
//                "Operator" => "=",
//                "Value" => $this->trackNo))
//        );

        foreach ($actions as $key) {
            $field_name = $key['workflow-field-update'];
            $formula = $key['workflow-field-formula'];
            if (is_numeric($field_name)) {
                continue; // ADDED BY MICHAEL ESP ... ERROR ON COMPUTATION OF UPDATE REPONSE WHEN FIELDNAME IS 0 INSTEAD OF A REAL NAME; ISSUED BY TROY 2:17 PM 9/22/2015
            }
            $formulaDoc->MyFormula = $formula;
            $returnValue = $formulaDoc->evaluate();

            if ($field_name != "") {
                $result[$field_name] = $returnValue;
            }
        }

        $result["middleware_process"] = 0;

        $reference_field_value = $this->data[$reference_field];

        $parent_refernce_field_name = $parent_refernce_field;

        if ($parent_refernce_field == "") {
            $parent_refernce_field_name = "TrackNo";
        }

        $requests = $this->query("SELECT ID FROM " . $formDoc->form_table_name . " WHERE $parent_refernce_field_name = {$this->escape($reference_field_value)}", "array");

        foreach ($requests as $request_row) {
            $requestDoc = new Request();
            $requestDoc->load($form_id, $request_row ['ID']);
            $requestDoc->data = $result;
            $requestDoc->modify();

            $requestLog = new Request_Log($this);
            $requestLog->form_id = $form_id;
            $requestLog->request_id = $requestDoc->id;
            $requestLog->details = 'Updated from ' . $this->formName . ' with Tracking Number : ' . $this->trackNo;
            $requestLog->created_by_id = $this->auth['id'];
            $requestLog->date_created = $date;
            $requestLog->save();

            if ($redis_cache) {
                functions::clearRequestRelatedCache("request_details_" . $form_id, $requestDoc->id);
                functions::clearRequestRelatedCache("request_access_" . $form_id, $requestDoc->id);
            }
        }


        if ($redis_cache) {
            $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

            $deleteMemecachedKeys = array_merge($myrequest_memcached);

            functions::deleteMemcacheKeys($deleteMemecachedKeys);

            functions::clearRequestRelatedCache("request_list", "form_id_" . $form_id);
            functions::clearRequestRelatedCache("picklist", "form_id_" . $form_id);
            functions::clearRequestRelatedCache("calendar_view", "form_id_" . $form_id);
            functions::clearRequestRelatedCache("request_list_count", "form_id_" . $form_id);

            //formula
            $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
                "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
                "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
                "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
            functions::clearFormulaLookupCaches($form_id, $formula_arr);
        }

        array_push($repsonses, array("action" => "update_response",
            "form_id" => $form_id));
        array_push($this->responseData["responses"], $repsonses);
    }

    public function request_seen($get) {
        $date = $this->currentDateTime();
        $formID = $get['formID'];
        $requestID = $get['requestID'];
        $trackNo = $get['trackNo'];

        $tb_seen = $this->query("SELECT * FROM tb_request_seen WHERE trackno = {$this->escape($trackNo)}
            AND formID = {$this->escape($formID)}
            AND requestID = {$this->escape($requestID)} AND user_id = {$this->escape($this->auth['id'])}", "numrows");
        if ($tb_seen == "0") {
            $insert = array("user_id" => $this->auth['id'],
                "trackno" => $trackNo,
                "formID" => $formID,
                "requestID" => $requestID,
                "date_view" => $date,
                "date_review" => $date,
                "is_active" => "1");
            $request_seen = $this->insert("tb_request_seen", $insert);
        } else {
            $update = array("date_review" => $date);
            $con = array("user_id" => $this->auth['id'],
                "trackno" => $trackNo,
                "formID" => $formID,
                "requestID" => $requestID);
            $request_seen = $this->update("tb_request_seen", $update, $con);
        }
    }

    // Data encryption / descryption
    public function encrypt_decrypt_request_details($form_data_json) {
        $formJson = json_decode($form_data_json, true);
        $encrypted_fields = $formJson['form_json']['encrypted_fields'];
        $encrypted_selected_flds = array();
        $decrypted_selected_flds = array();
        foreach ($encrypted_fields as $encrypted_field) {
            if ($encrypted_field['set_encryption'] != "No") {
                array_push($encrypted_selected_flds, $encrypted_field['field_name']);
            } else {
                array_push($decrypted_selected_flds, $encrypted_field['field_name']);
            }
        }

        return array("encrypt" => $encrypted_selected_flds,
            "decrypt" => $decrypted_selected_flds);
    }

    public function validate_fields_in_encryption_decryption($fields_data, $encrypt_decrypt, $encrypted_selected_flds) {

        $form_data_decrypted = array();
        foreach ($fields_data as $data) {
            foreach ($data as $column => $value) {
                $strlen = strlen($data[$column]);
                if (in_array($column, $encrypted_selected_flds['encrypt'])) {

                    if ($strlen >= 44) {
                        $data_to_decrypt = functions::encrypt_decrypt($encrypt_decrypt, $data[$column]);
                        $form_data_decrypted[$column] = $data_to_decrypt;
                    } else if ($strlen < 44) {
                        $form_data_decrypted[$column] = $data[$column];
                    } else {
                        $form_data_decrypted[$column] = $data[$column];
                    }
                } else if (in_array($column, $encrypted_selected_flds['decrypt']) && $strlen == 44) {

                    if ($strlen >= 44) {
                        $data_to_decrypt = functions::encrypt_decrypt($encrypt_decrypt, $data[$column]);
                        $form_data_decrypted[$column] = $data_to_decrypt;
                    } else if ($strlen < 44) {
                        $form_data_decrypted[$column] = $data[$column];
                    } else {
                        $form_data_decrypted[$column] = $data[$column];
                    }
                } else {
                    $form_data_decrypted[$column] = $data[$column];
                }
            }
        }

        return $form_data_decrypted;
    }

    public function unlockDocument() {
        $dbh = Common::getDatabaseConnection();
        $delete_sql = "DELETE FROM tbrequest_lock_session WHERE form_id = :form_id AND request_id=:request_id AND user_id=:user_id AND is_active = 1";
        $delete_statement = $dbh->prepare($delete_sql);
        $delete_params_arr = array(":form_id" => $this->formId,
            ":request_id" => $this->id,
            ":user_id" => $this->auth["id"]);

        $delete_statement->execute($delete_params_arr);
    }

}

?>
