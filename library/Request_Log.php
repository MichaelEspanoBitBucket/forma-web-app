<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Request_Logs
 *
 * @author Jewel Tolentino
 */
class Request_Log extends Formalistics {

    //put your code here

    public $form;
    public $form_id;
    public $request_id;
    public $details;
    public $date_created;
    public $date_updated;
    public $created_by;
    public $created_by_id;
    public $updated_by;
    public $updated_by_id;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->tblname = 'tbrequest_logs';
        $this->db = $db;

        if ($id) {
            $result = $this->db->query("SELECT id, form_id, request_id, "
                    . " details, date_created, date_updated, created_by, updated_by  FROM tbrequest_logs WHERE id = {$this->db->escape($id)}", "row");

            $this->id = $result['id'];
            $this->form_id = $result['form_id'];
            $this->request_id = $result['request_id'];
            $this->details = $result['details'];
            $this->date_created = $result['date_created'];
            $this->date_updated = $result['date_updated'];
            $this->created_by_id = $result['created_by'];
            $this->updated_by_id = $result['updated_by'];
        }
    }

    public function save() {
        $insert_array = array(
            "form_id" => $this->form_id,
            "request_id" => $this->request_id,
            "details" => $this->details,
            "date_created" => $this->date_created,
            "created_by" => $this->created_by_id,
        );

        $insert_id = $this->db->insert($this->tblname, $insert_array);

        $this->id = $insert_id;
    }

}

?>
