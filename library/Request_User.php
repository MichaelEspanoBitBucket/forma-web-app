<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Request_User
 *
 * @author Jewel Jeims
 */
class Request_User extends Formalistics {

    //put your code here
    public $form;
    public $requestID;
    public $user;
    public $user_type;
    public $action_type;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $this->tblname = "tbrequest_users";


        if ($id) {
            $result = $this->db->query("SELECT * FROM {$this->tblname} WHERE ID={$this->id}", "row");

            $this->form = new Form($db, $result["Form_ID"]);
            $this->requestID = $result["RequestID"];
            $this->user = $result["User"];
            $this->user_type = $result["User_Type"];
            $this->action_type = $result["Action_Type"];
        }
    }

    public function save() {
        $insert_array = array(
            "ID" => $this->id,
            "Form_ID" => $this->form->id,
            "RequestID" => $this->requestID,
            "User" => $this->user,
            "User_Type" => $this->user_type,
            "Action_Type" => $this->action_type
        );

        $insert_id = $this->db->insert($this->tblname, $insert_array);

        $this->id = $insert_id;
    }

    public function update() {
        $update_array = array(
            "ID" => $this->id,
            "Form_ID" => $this->form->id,
            "RequestID" => $this->requestID,
            "User" => $this->user,
            "User_Type" => $this->user_type,
            "Action_Type" => $this->action_type
        );

        $condition_array = array("ID" => $this->id);

        $this->db->update($this->tblname, $update_array, $condition_array);
    }

}
