<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Response
 *
 * @author Jewel Tolentino
 */
class Response {

    //put your code here

    public static final function create($parentDoc, $workflowData) {

        $redis_cache = getRedisConnection();
        $date = $parentDoc->currentDateTime();
        $form_id = $workflowData['workflow-form-trigger'];
        $actions = $workflowData['workflow-field_formula'];

        $requestFormDoc = new Form($parentDoc, $parentDoc->formId);
        $formDoc = new Form($parentDoc, $form_id);

        $workflow_object = functions::getFormActiveWorkflow($formDoc);
        $workflow_json = json_decode($workflow_object["json"], true);
        $result = array();
        $repsonses = array();

        $result['Requestor'] = $parentDoc->auth['id'];
        $result['Processor'] = $parentDoc->auth['id'];
//        $result['Status'] = 'Draft';
//        $result['imported'] = '1';
        $result['DateCreated'] = $date;
        $result['DateUpdated'] = $date;
        $result['Workflow_ID'] = $workflow_object["workflow_id"];
        $result['Node_ID'] = $workflow_json["workflow-default-action"];

        foreach ($actions as $key) {
            $field_name = $key['workflow-field-update'];
            $formula = $key['workflow-field-formula'];
            if (is_numeric($field_name)) {
                continue; // ADDED BY MICHAEL ESP ... ERROR ON COMPUTATION OF UPDATE REPONSE WHEN FIELDNAME IS 0 INSTEAD OF A REAL NAME; ISSUED BY TROY 2:17 PM 9/22/2015
            }
            $formulaDoc = new Formula($formula);
            $formulaDoc->DataFormSource[0] = $parentDoc->data;
            $returnValue = $formulaDoc->evaluate();
            if ($field_name != "") {
                $result[$field_name] = $returnValue;
            }
        }

        $requestDoc = new Request();
        $requestDoc->load($form_id, '0');
        $requestDoc->auth = $parentDoc->auth;
        $requestDoc->data = $result;
        $requestDoc->save();
        array_push($repsonses, array("action" => "create_response",
            "form_id" => $form_id));

        $requestDoc->processWF();
        $requestLog = new Request_Log($parentDoc);
        $requestLog->form_id = $form_id;
        $requestLog->request_id = $requestDoc->id;
        $requestLog->details = 'Created from ' . $parentDoc->formName . ' with Tracking Number : ' . $parentDoc->trackNo;
        $requestLog->created_by_id = $parentDoc->auth["id"];
        $requestLog->date_created = $date;
        $requestLog->save();

        if ($redis_cache) {
            $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

            $deleteMemecachedKeys = array_merge($myrequest_memcached);

            functions::deleteMemcacheKeys($deleteMemecachedKeys);

            functions::clearRequestRelatedCache("request_list", "form_id_" . $form_id);
            functions::clearRequestRelatedCache("picklist", "form_id_" . $form_id);
            functions::clearRequestRelatedCache("calendar_view", "form_id_" . $form_id);
            functions::clearRequestRelatedCache("request_list_count", "form_id_" . $form_id);
            functions::clearRequestRelatedCache("request_details_" . $form_id, $requestDoc->id);
            functions::clearRequestRelatedCache("request_access_" . $form_id, $requestDoc->id);
            //formula
            $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
                "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
                "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
                "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
            functions::clearFormulaLookupCaches($form_id, $formula_arr);
        }

        return $repsonses;
    }

    public static final function update() {
        
    }

}
