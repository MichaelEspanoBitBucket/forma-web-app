<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rule
 *
 * @author Jewel Tolentino
 */
class Rule extends Formalistics {

    //put your code here


    public $name;
    public $description;
    public $form;
    public $formula;
    public $actions;
    public $schedule_start_date;
    public $schedule_end_date;
    public $rep_data;
    public $date_created;
    public $is_active;
    public $processDate;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $this->tblname = 'tbmiddleware_settings';
        $redis_client = new Redis_Formalistics();
        if ($id) {
            $result = $redis_client->query("rule_object_" . $id, "SELECT * FROM tbmiddleware_settings WHERE id = {$id}");

            $this->id = $result["id"];
            $this->name = $result["rulename"];
            $this->description = $result["description"];
            $this->form = new Form($this->db, $result["formname"]);
            $this->formula = $result["formula"];
            $this->actions = $result["actions"];
            $this->schedule_start_date = $result["scheduled_process_start"];
            $this->schedule_end_date = $result["scheduled_process_end"];
            $this->rep_data = $result["schedule_rep_data"];
            $this->date_created = $result["date_created"];
            $this->is_active = $result["is_active"];
            $this->processDate = $result["processDate"];
        }
    }

    public function update() {
        $update_array = array(
            "id" => $this->id,
            "rulename" => $this->name,
            "description" => $this->description,
            "formula" => $this->formula,
            "actions" => $this->actions,
            "scheduled_process_start" => $this->schedule_start_date,
            "scheduled_process_end" => $this->schedule_end_date,
            "schedule_rep_data" => $this->rep_data,
            "is_active" => $this->is_active,
            "processDate" => $this->processDate
        );

        $condition_array = array("id" => $this->id);

        $this->db->update($this->tblname, $update_array, $condition_array);
    }

}
