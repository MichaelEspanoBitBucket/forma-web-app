<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SMS
 *
 * @author ervinne
 */
class SMS {

    public $errno;
    public $errorMessage;
    public $server;
    public $socket;
    public $number;
    public $message;

    public function __construct($number, $message) {
        $this->number = $number;
        $this->message = $message;
    }

    public function send() {
        //  TODO modify hard coded address later, add it to the configuration
        //  print_r(SMS_SERVER)
        $client = stream_socket_client("tcp://127.0.0.1:3014", $this->errno, $this->errorMessage);

        if ($client === false) {
            // print_r("Failed to connect: " . $this->errorMessage);
        }

        $json = array("to" => $this->number, "message" => $this->message);

        fwrite($client, json_encode($json));
        fclose($client);
    }

}
