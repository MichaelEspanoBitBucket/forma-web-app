<?php

class Search extends Database {

    public $form_table;
    public $form_viewers;
    public $form_id;
    public $form_category_id;
    public $enable_deletion;
    public $auth;
    public $fields_header_allowGeneralSearch;
    public $fields_headerObjType;
    public $fields_headerObj;

    public function __construct() {
        parent::__construct();
        $this->auth = Auth::getAuth('current_user');
    }

    //FOR GET/SEARCH REQUEST

    public function getManyRequest($search_value, $fields, $id, $limit_start, $limit_end, $obj) {
        //get form table
        $this->getFormTable($id);
        $this->form_id = $id;
        $auth = $this->auth;
        if ($obj['isOtherUser'] == "1") {
            $auth = $obj['userData'];
        }
        $userID = $auth['id'];

        if ($fields != "0") {
            if ($fields == "Requestor") {
                $search = "WHERE (u.first_name LIKE '%" . $search_value . "%' OR u.last_name LIKE '%" . $search_value . "%' OR CONCAT_WS( ' ', u.first_name, u.last_name) LIKE  '%" . $search_value . "%')";
            } else if ($fields == "Processor") {
                $search = "WHERE (up.first_name LIKE '%" . $search_value . "%' OR up.last_name LIKE '%" . $search_value . "%' OR CONCAT_WS( ' ', up.first_name, up.last_name) LIKE  '%" . $search_value . "%')";
            } else if ($fields == "ID") {
                $search = "WHERE (form.ID = " . $search_value . ")";
            } else {
                $search = "WHERE (form.$fields LIKE '%" . $search_value . "%')";
            }
        } else {
            $search = "WHERE (";
            //get active fields
            $active_fields = functions::getFormFields($id);
            foreach ($active_fields as $value) {
                if ($value) {
                    $search.= "form.$value LIKE  '%" . $search_value . "%' OR ";
                }
            }
            $search .= " TrackNo LIKE  '%" . $search_value . "%' OR (u.first_name LIKE '%" . $search_value . "%' OR u.last_name LIKE '%" . $search_value . "%') OR 
                    (up.first_name LIKE '%" . $search_value . "%' OR up.last_name LIKE '%" . $search_value . "%' OR CONCAT_WS( ' ', up.first_name, up.last_name) LIKE  '%" . $search_value . "%') OR form.Status LIKE  '%" . $search_value . "%')";
            // $search .= " TrackNo LIKE  '%".$search_value."%' OR Requestor LIKE  '%".$search_value."%' OR 
            //         Processor LIKE  '%".$search_value."%' OR Status LIKE  '%".$search_value."%')";
        }
        $limit = " LIMIT $limit_start, $limit_end";

        if ($limit_end == "" && $limit_end == "") {
            $limit = "";
        }
        if ($obj['multi_search']) {
            $search .= $obj['multi_search'];
        }
        if ($obj['date_range']) {
            $search .= " AND (" . $obj['date_field'] . " between '" . $obj['date_from'] . "' and '" . $obj['date_to'] . "')";
        }
        $sort = "";
        if ($obj['column-sort']) {
            $sort = "ORDER BY A." . $obj['column-sort'] . " " . $_POST['column-sort-type'];
        } else {
            $sort = "ORDER BY A.ID DESC";
        }
        if ($obj['condition']) {
            $search .= $obj['condition'];
        }

        $readers = $this->getFormPrivacyUsers($this->form_viewers, $auth);
        //execute query
        $query_str = "SELECT  * FROM (SELECT form.id as request_id, u.first_name,u.last_name,u.middle_name, form.Status as form_status,
                    form.Requestor as requestor_id, form.Processor as processors_id, up.last_name as processor_lastname, form.*, '" . $readers . "' as IsReader , CONCAT_WS(' ',u.first_name, u.last_name) as requestorName
                    FROM `$this->form_table` form LEFT JOIN tbuser u ON u.id=form.requestor LEFT JOIN tbuser up on up.id = form.processor
                     $search) A WHERE (A.IsReader = '1' OR A.Requestor = " . $auth['id'] . " OR  FIND_IN_SET(" . $auth['id'] . ",A.Processor) OR FIND_IN_SET(" . $auth['id'] . ",A.Editor) OR FIND_IN_SET(" . $auth['id'] . ",A.Viewer)
                     OR EXISTS(SELECT * FROM tbrequest_viewer mention WHERE mention.user_id=" . $auth['id'] . " 
                        AND mention.request_id = A.request_id AND mention.form_id=" . $this->form_id . ")
                     ) $sort $limit";

        //        print_r($query_str);
        $query_count = "SELECT  count(*) as count FROM (SELECT form.Requestor, form.Processor, form.Editor, form.Viewer,'" . $readers . "' as IsReader , form.id as request_id
                   FROM `$this->form_table` form LEFT JOIN tbuser u ON u.id=form.requestor LEFT JOIN tbuser up on up.id = form.processor
                    $search) A WHERE (A.IsReader = '1' OR A.Requestor = " . $auth['id'] . " OR  FIND_IN_SET(" . $auth['id'] . ",A.Processor) OR FIND_IN_SET(" . $auth['id'] . ",A.Editor) OR FIND_IN_SET(" . $auth['id'] . ",A.Viewer)
                    OR EXISTS(SELECT * FROM tbrequest_viewer mention WHERE mention.user_id=" . $auth['id'] . " 
                       AND mention.request_id = A.request_id AND mention.form_id=" . $this->form_id . ")
                    )";
        $count = $this->query($query_count, "row");
        // $count = "124526";
        $count = $count['count'];



        $getRequest = $this->query($query_str, "array");
        // $count = count($getRequest);

        $returnRequest = array();
        foreach ($getRequest as $request) {
            $processors = explode(",", $request['processors_id']);

            $processor_bool = array_search($userID, $processors) . "";

            //            $request_viewer = $this->requestViewer($request['request_id']);
            // if (/*$processor_bool != "" || $request['requestor_id'] == $userID || $readers ||*/ $request_viewer) {
            $array_starred = array("starred" => "0");
            $buttons = array("buttons" => "");
            $hasStarred = $this->getStarred($request['request_id']);

            if ($hasStarred > 0) {
                $array_starred = array("starred" => $hasStarred);
            }

            //check if current user is one of the processors
            $cancel_button = array("Cancel" =>
                array(
                    "button_name" => "Cancel",
                    "child_id" => "Cancel",
                    "parent_id" => "Cancel",
                // "require_comment" => false,
                // "wokflow_button_line_color" => "424242"
            ));

            if ($processor_bool != "") {
                if ($request['requestor_id'] == $userID && $request['LastAction'] != '') {
                    $button_arr = json_decode($request['LastAction'], true);
                    $button_arr['Cancel'] = $cancel_button['Cancel'];
                    $buttons = array("buttons" => json_encode($button_arr));
                } else {
                    $buttons = array("buttons" => $request['LastAction']);
                }
            } else if ($request['requestor_id'] == $userID && $request['LastAction'] != '') {
                $buttons = array("buttons" => json_encode($cancel_button));
            }

            array_push($request, $array_starred);
            array_push($request, array("user_image" => post::avatarPic("tbuser", $request['requestor_id'], "50", "50", "small", "avatar")));
            array_push($request, $buttons);
            array_push($request, array("count" => $count));
            array_push($returnRequest, $request);
            // }
        }

        return $returnRequest;
        // return $query_str;
        // return $query_count;
    }

    public function getManyRequestV2($search_value, $fields, $id, $limit_start, $limit_end, $obj, $is_report = false, $is_mobile_doc = false) {
        $this->getFormTable($id);
        $this->form_id = $id;
        $auth = $this->auth;
        $search_value = $this->addslashes_escape(mysql_escape_string(mysql_escape_string($search_value)));
        $getDeleteAccess = $this->query("SELECT FIND_IN_SET({$auth["id"]}, getFormUsers({$id},6)) as delete_access_forma", "row");

        // echo "123 -> ".$this->fields_header_allowGeneralSearch;
        $userID = $auth['id'];
        if ($fields != "0") {
            if ($fields == "Requestor") {
                $search = "WHERE (requester.display_name LIKE  '%" . $search_value . "%')";
            } else if ($fields == "Processor") {
                $search = "WHERE (requester.display_name LIKE  '%" . $search_value . "%')";
            } else if ($fields == "ID") {
                $search = "WHERE (request.ID = " . $search_value . ")";
            } else {
                $search = "WHERE (request.$fields LIKE '%" . $search_value . "%')";
            }
        } else {
            if ($this->fields_header_allowGeneralSearch == "0") {
                if ($this->fields_headerObjType == "0") {

                    $search = "WHERE ( TrackNo LIKE  '%" . $search_value . "%' OR (requester.display_name LIKE '%" . $search_value . "%')
                    OR request.Status LIKE  '%" . $search_value . "%' OR request.DateCreated LIKE  '%" . $search_value . "%')";
                } else {
                    $search = "WHERE (";
                    $header = json_decode(json_encode($this->fields_headerObj), true);
                    foreach ($header as $value) {
                        if ($value) {
                            $field_name = str_replace("[]", "", $value['field_name']);
                            $search.= "request." . $field_name . " LIKE  '%" . $search_value . "%' OR ";
                        }
                    }
                    $search = substr($search, 0, strlen($search) - 4);
                    $search .= " OR processor_position.position LIKE '%" . $search_value . "%' OR group_processor.group_name LIKE '%" . $search_value . "%'"
                            . " OR processer.display_name LIKE '%" . $search_value . "%' OR user_processor.display_name LIKE '%" . $search_value . "%')";
                }
            } else {
                $search = "WHERE (";
                //get active fields
                $active_fields = functions::getFormFieldsData(" WHERE form_id = '" . $id . "'"); // functions::getFormFields($id);
                foreach ($active_fields as $value) {
                    if ($value) {
                        $search.= "request." . $value['field_name'] . " LIKE  '%" . $search_value . "%' OR ";
                    }
                }
                $search .= " TrackNo LIKE  '%" . $search_value . "%' OR (requester.display_name LIKE '%" . $search_value . "%')
                        OR request.Status LIKE  '%" . $search_value . "%' OR request.DateCreated LIKE  '%" . $search_value . "%'";

                //processor search
                $search .= " OR processor_position.position LIKE '%" . $search_value . "%' OR group_processor.group_name LIKE '%" . $search_value . "%'"
                        . " OR processer.display_name LIKE '%" . $search_value . "%' OR user_processor.display_name LIKE '%" . $search_value . "%')";
            }
        }

//        $limit = " LIMIT $limit_start, $limit_end";

        if ($limit_start == "" && $limit_end == "") {
            $limit_start = "0";
            $limit_end = "0";
        }

        if ($obj['multi_search']) {
            $search .= $obj['multi_search'];
        }
        if ($obj['date_range']) {
            $search .= " AND (" . $obj['date_field'] . " between '" . $obj['date_from'] . "' and '" . $obj['date_to'] . "')";
        }
        $sort = "";
        if ($obj['column-sort']) {
            $sort = "ORDER BY " . $obj['column-sort'] . " " . $_POST['column-sort-type'];

            if ($obj['column-sort'] == "TrackNo") {
                $sort = "ORDER BY request.ID " . $_POST['column-sort-type'];
            }
        } else {
            if ($obj['sorting']) {
                $sort = "ORDER BY request.ID " . $obj['sorting'];
            } else {
                $sort = "ORDER BY request.ID DESC";
            }
        }
        if ($obj['condition']) {
            $search .= $obj['condition'];
        }

        //for deleted
        $search .= " AND (NOT EXISTS(SELECT * FROM tbtrash_bin WHERE record_id = request.ID 
                                AND form_id = $id 
                                AND table_name='" . $this->form_table . "'))";

//        $query_str = "CALL getRequests( {$id}, {$auth['id']}, {$limit_start},{$limit_end},'{$sort}','{$search}'); ";
//        if (REQUEST_QUERY_DEBUGGER == "1") {
//            return $query_str;
//        }
//        $getRequest = $this->query($query_str, "array");
//        $count = $getRequest[0]["Total"];
        $query_str = $this->getRequestQuery($id, $auth, $sort, $search, false, $is_report, $is_mobile_doc);
        $count_query_str = $this->getRequestQuery($id, $auth, $sort, $search, true);
//         echo $query_str;
        $limit = "";
        if ($limit_end != "0") {
            $limit = " LIMIT $limit_start, $limit_end";
        }
        $getRequest = $this->query($query_str . $limit, "array");

        if ($is_report) {
//            print_r($query_str);
            return $getRequest;
        }

        if ($_POST["iTotalRecords"] == "" || $_POST["iTotalRecords"] == "0") {
            if ($obj['isAdvanceSearch'] == "1") {
                $count = $this->query($query_str, "numrows");
            } else {
                if ($obj['picklist_search']) {
                    $picklist_condition = $obj['picklist_search'];
                }
                $count = $this->cacheRequestListCount($id, $userID, $search_value, $count_query_str, $picklist_condition); //$this->query($query_str, "numrows");
            }
        } else {
            $count = $_POST["iTotalRecords"];
        }

        if (REQUEST_QUERY_DEBUGGER == "1") {
            return $query_str;
        }
        $returnRequest = array();
//return;
        // $delete_button = array("Delete" =>
        //     array(
        //         "button_name" => "Delete - default",
        //         "child_id" => "delete",
        //         "parent_id" => "delete",
        //         "processors" => "0",
        //     )
        // );

        foreach ($getRequest as $request) {
            /*
              Commented by aaron tolentino 10-27-2015
              added process_bool to query
             */
            // $processors = explode(",", $request['processor_ids']);
            // $processor_bool = array_search($userID, $processors) . "";
//print_r($userID);
//var_dump($request);
//            $request_viewer = $this->requestViewer($request['request_id']);
            // if (/*$processor_bool != "" || $request['requestor_id'] == $userID || $readers ||*/ $request_viewer) {
            $array_starred = array("starred" => "0");
            $buttons = array("buttons" => "");
            $hasStarred = $this->getStarred($request['request_id']);

            if ($hasStarred > 0) {
                $array_starred = array("starred" => $hasStarred);
            }

            //check if current user is one of the processors
            $cancel_button = array("Cancel" =>
                array(
                    "button_name" => "Cancel",
                    "child_id" => "Cancel",
                    "parent_id" => "Cancel",
                    "processors" => $processors
                // "require_comment" => false,
                // "wokflow_button_line_color" => "424242"
            ));

            // if ($processor_bool != "") {
            if ($request['processor_bool']) {
                if ($request['requestor_id'] == $userID && $request['LastAction'] != '') {
                    $button_arr = json_decode($request['LastAction'], true);
                    $button_arr['Cancel'] = $cancel_button['Cancel'];
                    $buttons = array("buttons" => json_encode($button_arr));
                } else {
                    $buttons = array("buttons" => $request['LastAction']);
                }
            } else if ($request['requestor_id'] == $userID && $request['LastAction'] != '') {
                $buttons = array("buttons" => json_encode($cancel_button));
            }

            if ($getDeleteAccess['delete_access_forma'] > 0 && ENABLE_DELETION == "1" && $this->enable_deletion == "1") {
                $buttons = json_decode($buttons['buttons'], true);
                // $buttons['Delete'] = $delete_button['Delete'];
                $buttons = array("buttons" => json_encode($buttons));
            }

            array_push($request, $array_starred);
            array_push($request, array("user_image" => post::avatarPic("tbuser", $request['requestor_id'], "50", "50", "small", "avatar")));
            array_push($request, $buttons);
            array_push($request, array("count" => $count));
            array_push($returnRequest, $request);
            // }
        }

        return $returnRequest;
    }

    public function cacheRequestListCount($form_id, $user_id, $search, $query_str, $picklist_condition) {
        $redis_cache = getRedisConnection();

        if ($redis_cache) {
            /*
              Cache Format
              1. User Id.
              2. Search Filter.
             */

            if ($picklist_condition != "") {
                $cachedFormat = "$user_id::$search::$picklist_condition";
            } else {
                $cachedFormat = "$user_id::$search";
            }


            // $memcache->delete("request_list_count");
            $cache_request_list_count = json_decode($redis_cache->get("request_list_count"), true);
            // print_r($cache_request_list_count)
            $cached_query_result_count = $cache_request_list_count['form_id_' . $form_id]['' . $cachedFormat . ''];
            if ($cached_query_result_count) {
                $count = $cached_query_result_count;
            } else {
                $count_row = $this->query($query_str, "row");
                $count = $count_row["rows_count"];

                $cache_request_list_count['form_id_' . $form_id]['' . $cachedFormat . ''] = $count;

                $redis_cache->set("request_list_count", json_encode($cache_request_list_count));
            }
        } else {
            $count_row = $this->query($query_str, "row");
            $count = $count_row["rows_count"];
        }


        return $count;
    }

    public function getRequestQuery($id, $auth, $sort, $search, $is_count = false, $is_report = false, $is_mobile_doc = false) {
        $db = new Database();

        $filter = "";
        $form_viewer_str_sql = "(SELECT COUNT(*) AS is_form_viewer FROM tbform_users tbfu
                    LEFT JOIN
                tbdepartment_users tbdu ON tbdu.department_code = tbfu.user
                    LEFT JOIN
                tbform_groups_users tbfgu ON tbfgu.group_id = tbfu.user
                    LEFT JOIN
                tborgchart tbo ON tbo.id = tbdu.orgChart_id
                                        LEFT JOIN
                tbpositions tbp ON tbp.id = tbfu.user
                WHERE
                                    tbfu.Form_ID = {$id}
                    AND (
                                            (tbfu.user_type = 3 AND tbfu.user = {$auth["id"]} AND tbfu.action_type = 2 ) OR 
                                            (tbfu.user_type = 1 AND tbfu.user = {$db->escape($auth["position"])} AND tbfu.action_type = 2) OR 
                                            (tbfu.user_type = 2 AND tbdu.user_id = {$auth["id"]} AND tbo.status = 1 AND tbfu.action_type = 2) OR 
                                            (tbfu.user_type = 4 AND tbfgu.user_id = {$auth["id"]} AND tbfgu.is_active = 1 AND tbfu.action_type = 2)
                    )
                                    )";

        $is_form_viewer = $db->query($form_viewer_str_sql, "row");
//        var_dump($is_form_viewer);
        if ($this->form_category_id != 0 && $is_form_viewer["is_form_viewer"] == 0) {

            $filter = " AND
                           (request.Requestor = {$auth["id"]}
                            OR FIND_IN_SET({$auth["id"]}, 
                                CASE request.ProcessorType
                                    WHEN 1 THEN CONCAT(@department_users:=getDepartmentUsers(request.Processor, request.ProcessorLevel, {$auth["company_id"]}), ',', IF(request.enable_delegate = 1, IFNULL(getDelegate(@department_users), ''),''))
                                    WHEN 2 THEN CONCAT(@position_users:=getUsersByPosition(request.Processor), ',', IF(request.enable_delegate = 1,IFNULL(getDelegate(@position_users), ''),''))
                                    WHEN 6 THEN CONCAT(@group_users:=getUsersByGroup(request.Processor), ',', IF(request.enable_delegate = 1,IFNULL(getDelegate(@group_users), ''),''))
                                    ELSE CONCAT(request.Processor, ',', IF(request.enable_delegate = 1,IFNULL(getDelegate(request.Processor), ''),''))
                                END 
                            )
                            OR FIND_IN_SET({$auth["id"]}, request.Editor)
                            OR FIND_IN_SET({$auth["id"]}, request.Viewer)
                            OR (EXISTS 
                                (SELECT * FROM tbrequest_users tbru
                                    LEFT JOIN
                                tbdepartment_users tbdu ON tbdu.department_code = tbru.user
                                    LEFT JOIN
                                tbform_groups_users tbfgu ON tbfgu.group_id = tbru.user
                                    LEFT JOIN
                                tborgchart tbo ON tbo.id = tbdu.orgChart_id
                                    LEFT JOIN
                                tbpositions tbp ON tbp.id = tbru.user
                                    WHERE tbru.RequestID = request.ID AND tbru.Form_ID = {$id} AND
                                    (
                                        (tbru.user_type = 3 AND tbru.user = {$auth["id"]}) OR 
                                        (tbru.user_type = 1 AND tbru.user = {$db->escape($auth["position"])}) OR 
                                        (tbru.user_type = 2 AND tbdu.user_id = {$auth["id"]} AND tbo.status = 1) OR 
                                        (tbru.user_type = 4 AND tbfgu.user_id = {$auth["id"]} AND tbfgu.is_active = 1)
                                    )
                                )
                            )
                            OR (EXISTS
                                (SELECT * FROM tbrequest_viewer tbrv 
                                    WHERE tbrv.Form_ID = {$id} AND tbrv.Request_ID=request.ID
                                        AND tbrv.user_id = {$auth["id"]}
                                )
                            )
                            OR (EXISTS
                                (SELECT * FROM tbform_users tbfu
                    LEFT JOIN
                tbdepartment_users tbdu ON tbdu.department_code = tbfu.user
                    LEFT JOIN
                tbform_groups_users tbfgu ON tbfgu.group_id = tbfu.user
                    LEFT JOIN
                tborgchart tbo ON tbo.id = tbdu.orgChart_id
                                        LEFT JOIN
                tbpositions tbp ON tbp.id = tbfu.user
                WHERE
                                    tbfu.Form_ID = {$id}
                    AND (
                                            (tbfu.user_type = 3 AND tbfu.user = {$auth["id"]} AND tbfu.action_type = 2 ) OR 
                                            (tbfu.user_type = 1 AND tbfu.user = {$db->escape($auth["position"])} AND tbfu.action_type = 2) OR 
                                            (tbfu.user_type = 2 AND tbdu.user_id = {$auth["id"]} AND tbo.status = 1 AND tbfu.action_type = 2) OR 
                                            (tbfu.user_type = 4 AND tbfgu.user_id = {$auth["id"]} AND tbfgu.is_active = 1 AND tbfu.action_type = 2)
                    )
                                    )
                )
                           )";
        }

        /* var processor_bool = Added by Aaron Tolentino 10-27-2015 for request list button validation by Sam Pulta */


        if ($is_count) {
            $columns_str = " COUNT(*) as rows_count ";
        } else {
            $processor_bool = "";
            if (!$is_report) {
                $processor_bool = "FIND_IN_SET({$auth["id"]}, 
                                CASE request.ProcessorType
                                    WHEN 1 THEN CONCAT(@department_users:=getDepartmentUsers(request.Processor, request.ProcessorLevel, {$auth["company_id"]}), ',', IF(request.enable_delegate = 1, IFNULL(getDelegate(@department_users), ''),''))
                                    WHEN 2 THEN CONCAT(@position_users:=getUsersByPosition(request.Processor), ',', IF(request.enable_delegate = 1,IFNULL(getDelegate(@position_users), ''),''))
                                    WHEN 6 THEN CONCAT(@group_users:=getUsersByGroup(request.Processor), ',', IF(request.enable_delegate = 1,IFNULL(getDelegate(@group_users), ''),''))
                                    ELSE CONCAT(request.Processor, ',', IF(request.enable_delegate = 1,IFNULL(getDelegate(request.Processor), ''),''))
                                END 
                            ) AS processor_bool, ";
            }

            if ($is_mobile_doc) {
                $mobile_fields = " request.Processor AS ProcessorID,
                    requester.display_name as Requestor_Fullname, requester.display_name as Requestor_Name, 
                        CASE request.ProcessorType
                            WHEN 1 THEN 
                                (SELECT user_processor.display_name FROM tbuser user_processor WHERE user_processor.id = getDepartmentUsers(request.Processor, request.ProcessorLevel, {$auth["company_id"]}))
                            WHEN 2 THEN 
                                (SELECT Position FROM tbpositions WHERE ID=request.Processor)
                            WHEN 5 THEN 
                                (SELECT GROUP_CONCAT(proc_user.Display_Name) FROM tbuser proc_user WHERE FIND_IN_SET(proc_user.ID, request.Processor))
                            WHEN 6 THEN 
                                (SELECT group_name FROM tbform_groups WHERE ID=request.Processor)
                            ELSE 
                                processer.display_name
                        END AS Processor_Name, 
                        CASE request.ProcessorType
                            WHEN 1 THEN CONCAT(@department_users:=getDepartmentUsers(request.Processor, request.ProcessorLevel, {$auth["company_id"]}),',' ,
                                        IF(request.enable_delegate = 1,IFNULL(getDelegate(@department_users),''),'')
                                    ) 
                            WHEN 2 THEN CONCAT(@position_users:=getUsersByPosition(request.Processor),',' ,
                                        IF(request.enable_delegate = 1,IFNULL(getDelegate(@position_users),''),'')
                                    ) 
                            WHEN 6 THEN CONCAT(@group_users:=getUsersByGroup(request.Processor),',' ,
                                        IF(request.enable_delegate = 1,IFNULL(getDelegate(@group_users),''),'')
                                    )
                            ELSE
                                    CONCAT(request.Processor,',' ,
                                        IF(request.enable_delegate = 1,IFNULL(getDelegate(request.Processor),''),'')
                                    )
                        END AS ProcessorUserID,
                        CASE WHEN request.enable_delegate = 1 THEN 
                            CASE request.ProcessorType
                            WHEN 1 THEN (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(getDepartmentUsers(request.Processor, request.ProcessorLevel, {$auth["company_id"]}))) ORDER BY user_delegate.display_name)
                            WHEN 2 THEN (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(getUsersByPosition(request.Processor))) ORDER BY user_delegate.display_name)
                            WHEN 6 THEN (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(getUsersByGroup(request.Processor))) ORDER BY user_delegate.display_name)            
                            ELSE
                                    (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(request.Processor)) ORDER BY user_delegate.display_name)
                        END
                        ELSE
                            ''
                        END AS Delegate_Name,
                        CASE 
                            WHEN request.Editor IS NULL OR  request.Editor = '' THEN getRequestUsers({$this->form_id}, request.ID, 1)
                            ELSE
                                ''
                        END AS Editor,
                    FIND_IN_SET({$auth["id"]}, getFormUsers({$this->form_id},6)) as delete_access_forma, ";
            }

            $columns_str = " request.ID as request_id,
                            request.Status as form_status,
                        request.Requestor as requestor_id,
                        requester.display_name AS requestorName,
                        requester.display_name AS requestor_display_name,
                        {$processor_bool}
                        {$mobile_fields}
                        request . *
                        ,CASE 
                            WHEN request.ProcessorType = 1 THEN 
                                user_processor.display_name
                            WHEN request.ProcessorType = 2 THEN
                                processor_position.position
                            WHEN request.ProcessorType = 6 THEN
                                group_processor.group_name
                            ELSE
                                processer.display_name
                        END as Processor";
        }

        $query_str = "SELECT 
                      {$columns_str}      
                    FROM
                    {$this->form_table} request
                    LEFT JOIN 
                    tbuser requester ON requester.id = request.Requestor
                    LEFT JOIN 
                    tbuser processer ON processer.id = request.Processor
                    LEFT JOIN
                    tbpositions processor_position
                    ON processor_position.id = request.Processor AND request.ProcessorType = 2
                    LEFT JOIN
                    tbform_groups group_processor
                    ON group_processor.ID = request.Processor AND request.ProcessorType = 6
                    LEFT JOIN 
                    tbuser user_processor
                    ON user_processor.ID = getDepartmentUsers(request.Processor, request.ProcessorLevel, {$auth["company_id"]}) AND request.ProcessorType = 1
                    {$search}
                        AND (Status <> '')
                    {$filter}
                   {$sort}";
        return $query_str;
    }

    //GET REQUESTvIEWER 
    public function requestViewer($request_id) {
        $auth = $this->auth;
        $userID = $auth['id'];

        $query_count = $this->query("SELECT * FROM tbrequest_viewer WHERE user_id = '$userID' AND request_id = '$request_id' AND form_id = '$this->form_id'", "numrows");
        if ($query_count > 0) {
            return true;
        }
    }

    //FOR OWN REQUEST
    public function countOwnRequest($id) {
        //get form table
        $this->getFormTable($id);
        $auth = $this->auth;
        $userID = $auth['id'];


        //execute query
        $query_str = "SELECT count(*) as countData
                    FROM `$this->form_table` form LEFT JOIN tbuser u ON u.id=form.requestor
                    WHERE form.requestor = '$userID'";



        $getRequest = $this->query($query_str, "row");

        return $getRequest['countData'];
    }

    //FOR OWN REQUEST
    public function getOwnRequest($id, $limit_start, $limit_end, $search_value) {
        //get form table
        $this->getFormTable($id);
        $auth = $this->auth;
        $userID = $auth['id'];
        $limit = " LIMIT $limit_start, $limit_end";
        if ($limit_end == "" && $limit_end == "") {
            $limit = "";
        }


        //execute query
        $query_str = "SELECT form.DateCreated as DateCreated, form.TrackNo as TrackNo,form.id as request_id, u.first_name,u.last_name,u.middle_name, form.Status as form_status,
                    form.Requestor as requestor_id, form.Processor as processors_id 
                    FROM `$this->form_table` form LEFT JOIN tbuser u ON u.id=form.requestor, tb_workspace tbw
                    WHERE form.requestor = '$userID' AND tbw.is_delete = 0 AND tbw.id = $id AND (tbw.form_name LIKE '%" . $search_value . "%' OR form.TrackNo LIKE '%" . $search_value . "%' OR form.Status LIKE '%" . $search_value . "%') ORDER BY form.id DESC $limit";



        $getRequest = $this->query($query_str, "array");
        $readers = $this->getFormPrivacyUsers($this->form_viewers);
        $returnRequest = array();
        foreach ($getRequest as $request) {
            $processors = explode(",", $request['processors_id']);

            $processor_bool = array_search($userID, $processors) . "";

            //if($processor_bool != "" || $request['requestor_id'] == $userID || $readers){
            $array_starred = array("starred" => "0");
            $hasStarred = $this->getStarred($request['request_id']);
            if ($hasStarred > 0) {
                $array_starred = array("starred" => $hasStarred);
            }

            array_push($request, $array_starred);
            array_push($returnRequest, $request);
            //}
        }

        return $returnRequest;
    }

    //FOR Starred request
    public function countStarredRequests($id) {
        //get form table
        $this->getFormTable($id);
        $auth = $this->auth;
        $userID = $auth['id'];
        $ctr = 0;

        //execute query
        if ($id == 0) {
            $query_str = "SELECT count(*) as countData
                    FROM `tbpost` form LEFT JOIN tbuser u ON u.id=form.postedBy, tbstarred tbs 
                    WHERE tbs.tablename = 'tbpost' and tbs.data_id = form.id and tbs.user_id = $userID";
            $getRequest = $this->query($query_str, "row");
            $ctr = $getRequest['countData'];
        } else {
            $query_str = "SELECT count(*) as countData
                        FROM `$this->form_table` form LEFT JOIN tbuser u ON u.id=form.requestor, tbstarred tbs 
                        WHERE tbs.tablename = '$this->form_table' and tbs.data_id = form.id and tbs.user_id = $userID";

            $getRequest = $this->query($query_str, "row");

            $ctr = $getRequest['countData'];
        }

        return $ctr;
    }

    public function getStarredRequests($id, $limit_start, $limit_end) {
        //get form table
        $this->getFormTable($id);

        $auth = $this->auth;
        $userID = $auth['id'];

        $limit = " LIMIT $limit_start, $limit_end";
        if ($limit_end == "" && $limit_end == "") {
            $limit = "";
        }
        $returnRequest = array();
        if ($id == 0) {
            $query_str = "SELECT tbs.*, p.postedBy as requestor_id, u.first_name,u.last_name,u.middle_name, p.date_posted as DateCreated FROM tbstarred tbs, tbpost p LEFT JOIN tbuser u ON u.id=p.postedBy WHERE tbs.tablename = 'tbpost' and tbs.data_id = p.id and tbs.user_id = $userID ORDER BY p.id DESC  $limit";
            // echo $query_str;
            $getPost = $this->query($query_str, "array");
            foreach ($getPost as $post) {
                $array_starred = array("starred" => "0", "link" => functions::base_encode_decode("encrypt", $post['data_id']));
                array_push($post, $array_starred);
                array_push($returnRequest, $post);
            }
        } else {
            //execute query
            $query_str = "SELECT tbs.type, form.DateCreated as DateCreated, form.TrackNo as TrackNo,form.id as request_id, u.first_name,u.last_name,u.middle_name, form.Status as form_status,
                        form.Requestor as requestor_id, form.Processor as processors_id,tbs.* 
                        FROM `$this->form_table` form LEFT JOIN tbuser u ON u.id=form.requestor, tbstarred tbs 
                        WHERE tbs.tablename = '$this->form_table' and tbs.data_id = form.id and tbs.user_id = $userID
                         ORDER BY form.id DESC  $limit";

            $readers = $this->getFormPrivacyUsers($this->form_viewers);

            $getRequest = $this->query($query_str, "array");


            foreach ($getRequest as $request) {
                $processors = explode(",", $request['processors_id']);

                $processor_bool = array_search($userID, $processors) . "";

                if ($processor_bool != "" || $request['requestor_id'] == $userID || $readers) {
                    $array_starred = array("starred" => "0");
                    // $hasStarred = $this->getStarred($request['request_id']);
                    // if($hasStarred>0){
                    //     $array_starred = array("starred"=>$hasStarred);
                    // }

                    array_push($request, $array_starred);
                    array_push($returnRequest, $request);
                }
            }
        }


        return $returnRequest;
    }

    public function getStarred($request_id) {
        $auth = $this->auth;
        $userID = $auth['id'];
        $tablename = $this->form_table;
        $strSQL = "SELECT * FROM tbstarred WHERE tablename = '$tablename' AND data_id = $request_id AND user_id = '$userID'";
        $starred = $this->query($strSQL, "row");
        return $starred['id'];
    }

    // public function getOwnRequest($search_value,$fields,$id){
    //     //get form table
    //     $this->getFormTable($id);
    //     $auth = $this->auth;
    //     $userID = $auth['id'];
    //     if($fields!="0"){
    //         $search = "WHERE ($fields LIKE '%".$search_value."%')";
    //     }else{
    //         $search = "WHERE ("; 
    //         //get active fields
    //         $active_fields = functions::getFormFields($id);
    //         foreach ($active_fields as $value) {
    //            //echo "<option value='". $value ."'>". $value ."</option>";
    //             if($value){
    //                 $search.= "$value LIKE  '%".$search_value."%' OR ";
    //             }
    //         }
    //         $search .= " TrackNo LIKE  '%".$search_value."%' OR Requestor LIKE  '%".$search_value."%' OR 
    //                 Processor LIKE  '%".$search_value."%' OR Status LIKE  '%".$search_value."%')";
    //     }
    //     //execute query
    //     $query_str = "SELECT form.DateCreated as DateCreated, form.TrackNo as TrackNo,form.id as request_id, u.first_name,u.last_name,u.middle_name, form.Status as form_status,
    //                 form.Requestor as requestor_id, form.Processor as processors_id 
    //                 FROM `$this->form_table` form LEFT JOIN tbuser u ON u.id=form.requestor $search AND form.Requestor = {$this->escape($userID)} ORDER BY form.id DESC";
    //     $readers = $this->getFormPrivacyUsers($this->form_viewers);
    //     $getRequest = $this->query($query_str,"array");
    //     return $getRequest;
    // }

    public function getCompaniesForm($others) {
        $auth = $this->auth;
        $strSQL = "SELECT id,form_name,form_table_name FROM tb_workspace WHERE  is_delete = 0 AND company_id = {$this->escape($auth['company_id'])} $others";
        $getForms = $this->query($strSQL, "array");
        return $getForms;
    }

    public function getFormTable($id) {
        $query_str = "SELECT * FROM tb_workspace WHERE is_delete = 0 AND id = {$this->escape($id)}";
        $getData = $this->query($query_str, "row");
        $this->form_table = $getData['form_table_name'];
        $this->form_category_id = $getData['category_id'];
        $this->form_viewers = $getData['form_viewers'];
        $this->enable_deletion = $getData['enable_deletion'];
        $form_json = json_decode($getData['form_json']);

        $this->fields_header_allowGeneralSearch = $form_json->{'form_json'}->{'allowGeneralSearch'};
        $this->fields_headerObjType = $form_json->{'form_json'}->{'headerInfoType'};
        $this->fields_headerObj = $form_json->{'form_json'}->{'collected_data_header_info'}->{'listedchoice'};
    }

    //FOR ORGCHART

    public function getOrgchart($search, $start, $json) {

        $auth = $this->auth;
        $end = "10";
        if ($json['end'] != "") {
            $end = $json['end'];
        }
        if ($start != "") {
            $limit = "LIMIT $start , $end";
        }

        $orderBy = " ORDER BY status DESC, date DESC ";
        if ($json['column-sort'] != "") {
            $orderBy = " ORDER BY " . $json['column-sort'] . " " . $json['column-sort-type'];
        }

        $condition = "";
        if ($json['condition']) {
            $condition = $json['condition'];
        }
        $status_condition = $this->status_condition("status", $search);

        $query_str = "SELECT * FROM tborgchart WHERE company_id={$this->escape($auth['company_id'])} AND is_active={$this->escape(1)} AND is_delete = 0 AND (title LIKE '%$search%' OR description LIKE '%$search%' OR `date` LIKE '%$search%' $status_condition) $condition $orderBy $limit";
        $getOrgchart = $this->query($query_str, "array");
        return $getOrgchart;
    }

    public function countOrgchart() {
        $auth = $this->auth;
        $query_str = "SELECT count(1) as count FROM tborgchart WHERE company_id={$this->escape($auth['company_id'])} AND is_active={$this->escape(1)} AND is_delete = 0";
        $getOrgchart = $this->query($query_str, "row");
        return $getOrgchart['count'];
    }

    public function getForms($search, $start, $json) {
        $auth = $this->auth;
        $limit = "";
        if ($start != "") {
            $limit = "LIMIT $start , 10";
        }
        $orderBy = " ORDER BY w.is_active DESC, date_created DESC ";
        if ($json['column-sort'] != "") {
            if ($json['column-sort'] == "is_active") {
                $json['column-sort'] = "w.is_active";
            }
            $orderBy = " ORDER BY " . $json['column-sort'] . " " . $json['column-sort-type'];
        }

        $condition = "";
        if ($json['condition']) {
            $condition = $json['condition'];
        }

        $query_str = "SELECT *,w.id as wsID,w.form_admin,w.created_by,w.is_active as ws_status FROM tb_workspace w
                                   LEFT JOIN tbuser u on u.id=w.created_by
                                   WHERE w.company_id={$this->escape($auth['company_id'])} AND w.is_delete = 0 AND w.form_name LIKE '%$search%' $condition $orderBy $limit";

        $getForms = $this->query($query_str, "array");
        return $getForms;
    }

    public function getFormsV2($search, $start, $json, $type) {
        $auth = $this->auth;
        $company_id = $auth['company_id'];
        $userID = $auth['id'];

        $limit = "";
        $end = "10";
        if ($json['end'] != "") {
            $end = $json['end'];
        }
        if ($start != "") {
            $limit = "LIMIT $start , $end";
        }
        $orderBy = " ORDER BY tbw.is_active DESC, tbw.form_name ASC ";
        if ($json['column-sort'] != "") {
            if ($json['column-sort'] == "is_active") {
                $json['column-sort'] = "tbw.is_active";
            }
            $orderBy = " ORDER BY " . $json['column-sort'] . " " . $json['column-sort-type'];
        }
//remove due to this function is not in used Aaron Tolentino 2/20/2015
//        $condition = "";
//        if ($json['condition']) {
//            $condition = $json['condition'];
//        }
        //added for other conditions Aaron Tolentino 2/20/2015
        $other_condition = "";
        if ($json['other_condition']) {
            $other_condition = " " . $json['other_condition'] . " ";
        }
        $other_fields = "";
        if ($json['other_fields']) {
            $other_fields = $json['other_fields'];
        }

        if ($type == "numrows") {
            $limit = "";
        }

        $status_condition = $this->status_condition("tbw.is_active", $search);
//        $type_condition = $this->type_condition("tbw.form_type",$search);
        $query_view = $this->vwget_forms($other_fields, "AND tbfu.action_type = 4 AND tbfu.user_type = 3");
        $query_str = $query_view . " AND
            tbw.company_id = $company_id AND
            ( 
                tbw.form_name LIKE '%$search%' OR
                tbw.form_description LIKE '%$search%' OR
                COALESCE(tbfc.category_name,'Others') LIKE '%$search%' OR
                tbw.form_alias LIKE '%$search%'
                $status_condition
            )
            $other_condition
            AND 
            (
                (tbw.category_id = 0) OR 
                (
                 tbfu.user = $userID
                )
                
            )  
            GROUP BY tbw.id $orderBy $limit";
        $getForms = $this->query($query_str, $type);

        return $getForms;
        // return $query_str;
    }

    /* Added by Japhet Morada */

    public function getKeywordsList($search, $start, $json, $type) {
        $auth = $this->auth;
        $company_id = $auth['company_id'];
        $userID = $auth['id'];

        $limit = "";
        $endLimit = "10";
        if ($json['endLimit'] != "") {
            $endLimit = $json['endLimit'];
        }
        if ($start != "") {
            $limit = "LIMIT $start , $endLimit";
        }
        $orderBy = " ORDER BY code ASC ";
        if ($json['column-sort'] != "") {
            $orderBy = " ORDER BY " . $json['column-sort'] . " " . $json['column-sort-type'];
        }


        if ($type == "numrows") {
            $limit = "";
        }

//        $type_condition = $this->type_condition("tbw.form_type",$search);
        $query_view = $this->vwget_keyword();
        $query_str = $query_view . " AND code LIKE '%$search%' OR description LIKE '%search%' OR date_created LIKE '%search%' OR date_updated LIKE '%search%' " . $orderBy . " " . $limit;
        $getKeywords = $this->query($query_str, $type);
        // return $getKeywords;
        return $getKeywords;
    }

    public function getGenerate($search, $start, $json, $type) {
        $auth = $this->auth;
        $company_id = $auth['company_id'];
        $userID = $auth['id'];
        $position = $auth['position'];
        $limit = "";
        $endlimit = "10";
        if ($json['endlimit'] != "") {
            $endlimit = $json['endlimit'];
        }
        if ($start != "") {
            $limit = "LIMIT $start , $endlimit";
        }
        $orderBy = " ORDER BY tbg.is_active DESC, tbw.form_name ASC ";
        if ($json['column-sort'] != "") {
            if ($json['column-sort'] == "is_active" || $json['column-sort'] == "date_created" || $json['column-sort'] == "date_updated") {
                $json['column-sort'] = "tbg." . $json['column-sort'];
            }
            $orderBy = " ORDER BY " . $json['column-sort'] . " " . $json['column-sort-type'];
        }

        $condition = "";
        if ($json['condition']) {
            $condition = $json['condition'];
        }
        if ($type == "numrows") {
            $limit = "";
        }
        $status_condition = $this->status_condition("tbw.is_active", $search);
        $query_view = $this->vwget_generate();
        $query_str = $query_view . " AND
            tbw.company_id = $company_id AND
            ( 
                tbw.form_name LIKE '%$search%' OR
                tbw.form_description LIKE '%$search%' OR
                tbg.date_created LIKE '%$search%' OR
                tbg.date_updated LIKE '%$search%'
                $status_condition
            )
            AND
			(
            (tbfu.action_type = 5 AND 
                tbfcu.access_type = 2 AND
            (
                
                
                (tbfcu.user_type = 3 AND tbfcu.user = $userID) OR 
                (tbfcu.user_type = 1 AND 
                tbfcu.user = {$this->escape($position)}) OR 
                (tbfcu.user_type = 2 AND 
                tbdu.user_id = {$this->escape($userID)} AND tbo.status = 1) OR
                (tbfcu.user_type = 4 AND 
                tbfgu.user_id = {$this->escape($userID)} AND tbfgu.is_active = 1) 
                
            ) ) OR tbw.category_id = 0 
)			
            GROUP BY tbw.id $orderBy $limit";

        $getForms = $this->query($query_str, $type);

        return $getForms;
        //return $query_str;
    }

    public function getFormsByAdmin($search) {
        $auth = $this->auth;
        $company_id = $auth['company_id'];
        $userID = $auth['id'];
        $query_str = $this->vwget_forms() . " AND 
            (
                tbw.company_id = $company_id
                AND 
                (
                    (tbw.category_id = 0) OR 
                    (
                        tbfu.action_type = 4 AND 
                        tbfu.user_type = 3 AND tbfu.user = $userID
                    )
                    
                ) 
            ) 
            $search 
            GROUP BY tbw.id";
        $getForms = $this->query($query_str, "array");
        return $getForms;
    }

    public function getAllActiveForms() {
        $auth = $this->auth;
        $company_id = $auth['company_id'];
        $userID = $auth['id'];

        $query_str = $this->vwget_forms("", "AND tbfu.action_type = 4 AND tbfu.user_type = 3") . " AND
            tbw.company_id = $company_id AND
            COALESCE((SELECT tbwf.id FROM tbworkflow tbwf WHERE tbwf.form_id = tbw.id AND tbwf.is_active = 1 and tbwf.is_delete = 0),0) != 0
            AND 
            (
                (tbw.category_id = 0) OR 
                (
                    tbfu.user = $userID
                )
            )  
            AND tbw.is_Active = 1
            GROUP BY tbw.id ORDER BY tbw.form_name";

//        print_r($query_str);
        $getForms = $this->query($query_str, "array");
        return $getForms;
    }

    public function countForms() {
        $auth = $this->auth;
        $query_str = "SELECT count(1) as count FROM tb_workspace w
                                   LEFT JOIN tbuser u on u.id=w.created_by
                                   WHERE w.is_delete = 0 AND w.company_id={$this->escape($auth['company_id'])}";
        $getForms = $this->query($query_str, "row");
        return $getForms['count'];
    }

    public function getPrintReports($selector) {
        $result = $this->query("SELECT * FROM tb_generate " . $selector, 'array');
        return $result;
    }

    public function getSpecificForms($array) {
        $cond_string = "";
        foreach ($array as $field => $value) {
            $cond_string .= $field . "=" . $this->escape($value) . " AND ";
        }
        $cond_string = substr($cond_string, 0, strlen($cond_string) - 4);
        $auth = $this->auth;
        $query_str = "SELECT *,w.id as wsID,w.is_active as ws_status FROM tb_workspace w
                                   LEFT JOIN tbuser u on u.id=w.created_by
                                   WHERE $cond_string";
        $getForms = $this->query($query_str, "array");
        return $getForms;
    }

    public function getSpecificForms2($cond_string) {
        $auth = $this->auth;
        $query_str = "SELECT *,w.id as wsID,w.is_active as ws_status FROM tb_workspace w
                                   LEFT JOIN tbuser u on u.id=w.created_by
                                   WHERE $cond_string";
        $getForms = $this->query($query_str, "array");
        return $getForms;
    }

    public function getWorkflows($search, $start, $json) {
        $auth = $this->auth;
        $limit = "";
        if ($start != "") {
            $limit = "LIMIT $start , 10";
        }
        $orderBy = " ORDER BY Is_Active DESC, date DESC ";
        if ($json['column-sort'] != "") {
            $orderBy = " ORDER BY " . $json['column-sort'] . " " . $json['column-sort-type'];
        }
        $condition = "";
        if ($json['condition']) {
            $condition = $json['condition'];
        }

        $getWorkflow = $this->query("SELECT ws.id as ws_id, wf.date as wf_date, wf.description as wf_description,ws.form_admin,ws.created_by,
                                          wf.id as wf_id, wf.title as wf_title, wf.form_id as form_id, wf.is_active as Is_Active, ws.form_name as form_name,
                                          tbu_c.display_name as creator  
                                          FROM tbworkflow wf LEFT JOIN tb_workspace ws on wf.form_id = ws.id LEFT JOIN tbuser tbu_c on wf.created_by = tbu_c.id  WHERE ws.company_id={$this->escape($auth['company_id'])} AND wf.is_delete = 0 AND ws.is_delete = 0 
                                          AND (wf.title LIKE '%$search%' OR ws.form_name LIKE '%$search%') $condition  $orderBy $limit", "array");
        return $getWorkflow;
    }

    public function getWorkflowsV2($search, $start, $json, $type) {
        $auth = $this->auth;
        $limit = "";
        $endlimit = "10";
        if ($json['endlimit'] != "") {
            $endlimit = $json['endlimit'];
        }
        if ($start != "") {
            $limit = "LIMIT $start , $endlimit";
        }
        $orderBy = " ORDER BY (CASE 
                    WHEN tbwf.is_active = '1'  THEN 2
                    WHEN tbwf.is_active = '2'  THEN 1
                    ELSE 0 END) DESC, tbwf.title ASC ";
        if ($json['column-sort'] != "") {
            $orderBy = " ORDER BY " . $json['column-sort'] . " " . $json['column-sort-type'];
        }
        $condition = "";
        if ($json['condition']) {
            $condition = $json['condition'];
        }
        $other_condition = "";
        if ($json['other_condition']) {
            $other_condition = " " . $json['other_condition'] . " ";
        }
        if ($type == "numrows") {
            $limit = "";
            $orderBy = "";
        }
        //numrow with limit
        if ($type == "numrows_limit") {
            $type = "numrows";
        }
        //for custom query
        $customQuery = "";
        if ($json['customQuery'] != "") {
            $customQuery = $json['customQuery'];
        }
        $status_condition = $this->status_condition("tbwf.is_active", $search);
        //changed 03/18/2015
        // $query_str = $this->vwget_workflow()." AND tbw.company_id={$this->escape($auth['company_id'])} AND 
        //         (
        //             (tbw.category_id = 0) OR 
        //             (
        //                 tbfu.action_type = 4 AND 
        //                 tbfcu.access_type = 1 AND 
        //                 tbfcu.user_type = 3 AND tbfcu.user = {$this->escape($auth['id'])}
        //             )
        //         ) AND (tbwf.title LIKE '%$search%' OR tbw.form_name LIKE '%$search%' OR tbwf.date LIKE '%$search%' $status_condition) $customQuery GROUP BY tbwf.id $orderBy $limit";
        $query_str = $this->vwget_workflow() . " AND tbw.company_id = {$this->escape($auth['company_id'])}
                    $other_condition 
                    AND (
                        FIND_IN_SET({$this->escape($auth['id'])}, 
                            CASE tbw.category_id
                            WHEN 0 THEN {$this->escape($auth['id'])}
                            ELSE (SELECT GROUP_CONCAT(user) FROM tbform_users WHERE action_type = 4 and form_id = tbw.ID)
                            END )
                    ) AND (tbwf.title LIKE '%$search%' OR tbw.form_name LIKE '%$search%' OR tbwf.date LIKE '%$search%' $status_condition) $customQuery $orderBy $limit";
        $getWorkflow = $this->query($query_str, $type);
        return $getWorkflow;
    }

    //FOR DEPARTMENT
    public function getAllDepartment() {
        $auth = $this->auth;
        $getDepartment = $this->query("SELECT oo.id,oo.department, oo.department_code FROM tborgchart o LEFT JOIN tborgchartobjects oo ON o.id = oo.orgchart_id WHERE o.status = 1 AND o.company_id={$this->escape($auth['company_id'])} AND o.is_delete = 0", "array");
        return $getDepartment;
    }

    //FOR DEPARTMENT V2
    public function getAllDepartmentV2() {
        $auth = $this->auth;
        $getDepartment = $this->query("SELECT oo.department_code as id,oo.department,oo.department_code as user FROM tborgchart o LEFT JOIN tborgchartobjects oo ON o.id = oo.orgchart_id WHERE o.status = 1 AND o.company_id={$this->escape($auth['company_id'])} AND o.is_delete = 0 ORDER BY oo.department ASC", "array");
        return $getDepartment;
    }

    //FOR POSITION
    public function getAllPosition() {
        $auth = $this->auth;
        $getPosition = $this->query("SELECT DISTINCT(position) FROM tbuser WHERE  is_active = 1 AND company_id={$this->escape($auth['company_id'])}", "array");
        return $getPosition;
    }

    //FOR POSITION
    public function getAllPositionV2() {
        $auth = $this->auth;
        $getPosition = $this->query("SELECT * FROM  tbpositions  WHERE  is_active = 1 AND company_id={$this->escape($auth['company_id'])} ORDER BY position ASC", "array");
        return $getPosition;
    }

    //FOR POSITION
    public function getAllGroups() {
        $auth = $this->auth;
        $getGroups = $this->query("SELECT * FROM tbgroups WHERE  is_active = 1 AND user_id={$this->escape($auth['id'])}", "array");
        return $getGroups;
    }

    //FOR POSITION
    public function getFormGroups() {
        $auth = $this->auth;
        $getGroups = $this->query("SELECT * FROM tbform_groups WHERE  is_active = 1 AND company_id={$this->escape($auth['company_id'])} ORDER BY TRIM(group_name) ASC", "array");
        return $getGroups;
    }

    //FOR USERS
    public function getAllUsers($str) {
        $auth = $this->auth;
        $getUsers = $this->query("SELECT id,first_name,last_name,display_name FROM tbuser WHERE is_active = 1 AND company_id={$this->escape($auth['company_id'])} $str ORDER BY TRIM(display_name)", "array");
        return $getUsers;
    }

    //form privacy (Customized Print) and etc...
    public function getFormPrivacyUsers($json, $auth = "") {
        if ($auth == "") {
            $auth = $this->auth;
        }
        $form_authors = json_decode($json, true);
        //$departments = $form_authors['departments'];
        $count = 0;
        foreach ($form_authors as $key => $value) {
            if ($key == "departments") {
                // Added by sam
                $departments = $this->query("SELECT * FROM tbdepartment_users u
                                        LEFT JOIN tborgchart o ON u.orgchart_id=o.id
                                        WHERE u.user_id={$this->escape($auth['id'])} AND o.status='1'
                                        ");

                foreach ($departments as $department) {

                    $search = array_search($department['department_code'], $value) . "";
                    if ($search != "") {
                        $count++;
                        break;
                    }
                }
            } else if ($key == "positions") {
                $search = array_search($auth['position'], $value) . "";
                if ($search != "") {
                    $count++;
                    break;
                }
            } else if ($key == "users") {
                $search = array_search($auth['id'], $value) . "";
                if ($search != "") {
                    $count++;
                    break;
                }
            }
        }
        if ($count > 0) {
            return true;
        }
    }

    public function getFormPrivacyOtherUsers($json, $id) {
        $getUserData = $this->query("SELECT * FROM tbuser WHERE id = $id", "row");
        $form_authors = json_decode($json, true);
        //$departments = $form_authors['departments'];
        $count = 0;
        foreach ($form_authors as $key => $value) {
            if ($key == "departments") {
                $count = 0;
                $search = array_search($getUserData['department_id'], $value) . "";
                if ($search != "") {
                    $count++;
                    // break;
                }
            } else if ($key == "positions") {
                $count = 0;
                $search = array_search($getUserData['position'], $value) . "";
                if ($search != "") {
                    $count++;
                    // break;
                }
            } else if ($key == "users") {
                $count = 0;
                $search = array_search($getUserData['id'], $value) . "";
                if ($search != "") {
                    $count++;
                    // break;
                }
            }
        }
        if ($count > 0) {
            return true;
        }
    }

    public function getCategory($id) {
        $auth = $this->auth;
        $company_id = $auth['company_id'];
        $getcategory = $this->query("SELECT * FROM tbform_category WHERE company_id = $company_id && id = $id AND is_delete = 0", "array");
        return $getcategory;
    }

    public function getAllCategory() {
        $auth = $this->auth;
        $getCategory = $this->query("SELECT * FROM tbform_category WHERE company_id = {$this->escape($auth['company_id'])} AND is_delete = 0", "array");
        $categ = array();
        foreach ($getCategory as $category) {
            $categ[] = array(
                "category_name" => $category['category_name'],
                "category_id" => $category['id']
            );
        }
        return $categ;
    }

    public function countCategory() {
        $auth = $this->auth;
        $company_id = $auth['company_id'];
        // get category that are using
        $forms_array = $this->query("SELECT ws.form_name as form_name, ws.id as id, ws.form_json as form_json
                                              FROM tb_workspace ws
                                              LEFT JOIN tbworkflow wf on ws.id = wf.form_id
                                              WHERE wf.is_active = 1 and ws.is_active = 1 AND ws.is_delete = 0 AND wf.is_delete = 0 
                                              AND company_id = {$this->escape($auth[company_id])}
                                              ORDER BY ws.form_name DESC", "array");

        $array_categoryID = array();
        foreach ($forms_array as $forms) {
            $json = json_decode($forms['form_json'], true);
            $formID = $forms['id'];
            $formName = $forms['form_name'];
            $forms = $json['form_json'];
            $categoryID = $json['categoryName'];

            array_push($array_categoryID, $categoryID);
        }
        $getcategory = $this->query("SELECT * FROM tbform_category WHERE company_id = $company_id AND is_delete = 0", "array");
        $ctr = 0;
        $categoryIDReturn = array();
        foreach ($getcategory as $category) {
            if ($category['users'] != "") {
                if ($this->getFormPrivacyUsers($category['users']) == false) {
                    continue;
                }
            }
            if (in_array($category['id'], $array_categoryID)) {
                $categoryIDReturn[] = $category['id'];
            }
        }
        if (in_array("0", $array_categoryID)) {
            $categoryIDReturn[] = 0;
        }
        return $categoryIDReturn;
    }

    public function getActiveOrgchart() {
        $auth = $this->auth;
        $company_id = $auth['company_id'];
        $orgchart = $this->query("SELECT * FROM tborgchart tbo WHERE tbo.company_id = {$this->escape($company_id)} and tbo.status = '1' and tbo.is_active = 1 AND is_delete = 0", "array");

        return $orgchart;
    }

    // public function getModules($search,$type="1") {
    //     $auth = $this->auth;
    //     $userID = $auth['id'];
    //     $position = $auth['position'];
    //     $company_id = $auth['company_id'];
    //     $queryGetModules = "SELECT * from vwget_modules WHERE 
    //                         company_id = {$this->escape($company_id)} AND
    //                         (
    //                             form_actiontype = 3 AND 
    //                             category_accesstype = 2 AND 
    //                             (
    //                                 (category_usertype = 3 AND 
    //                                 category_user = {$this->escape($userID)}) OR 
    //                                 (category_usertype = 1 AND 
    //                                 category_user = {$this->escape($position)}) OR 
    //                                 (category_usertype = 2 AND 
    //                                 department_user_id = {$this->escape($userID)} AND orgchart_status = 1) OR
    //                                 (category_usertype = 4 AND 
    //                                 group_user_id = {$this->escape($userID)}) AND group_is_active = 1
    //                             ) OR (category_id = 0)
    //                         ) 
    //                         $search
    //                         GROUP BY form_id ORDER BY category_name, form_name";
    //     $getModules = $this->query($queryGetModules, "array");
    //     $form_array = array();
    //     $others_array = array();
    //     $result = array();
    //     foreach ($getModules as $key => $value) {
    //         //push forms and category here
    //         if($type=="1"){
    //             if ($value['form_display_category'] == 1) {//dynamic
    //                 $temp_formula = new Formula($value['form_display_formula']);
    //                 $temp_formula_replaced = $temp_formula->replaceFormulaFieldNames();
    //                 $temp_formula_evaluated = $temp_formula->evaluate();
    //                 // print_r($temp_formula_replaced);
    //                 // echo '<br/>';
    //                 // print_r($temp_formula_evaluated);
    //                 if (gettype($temp_formula_evaluated) == 'boolean') {
    //                     if ($temp_formula_evaluated == false) {
    //                         continue;
    //                     }
    //                 }
    //                 // if($value['form_display_formula']=='Sample Edit Formula'){
    //                 //     continue;
    //                 // }
    //             } else {
    //                 if ($value['form_display_type'] != 1) { //static yes 1 no 0
    //                     continue;
    //                 }
    //             }
    //         }
    //         if ($value['category_id'] != 0) {
    //             $form_array['' . $value['category_name'] . ''][] = $value;
    //         } else {
    //             $others_array['' . $value['category_name'] . ''][] = $value;
    //         }
    //     }
    //     // echo json_encode($form_array);
    //     $result = array_merge($form_array, $others_array);
    //     return $result;
    // }
    public function getModules($search, $type = "1", $additionalFields = array(), $orderBy = null) {
        $auth = $this->auth;
        $userID = $auth['id'];
        $position = $auth['position'];
        $company_id = $auth['company_id'];

//        $modules_cache = $mem_cache->get("modules_" . $userID . "::" . $position . "::" . $company_id . "::" . );
        if (ENABLE_STAGING == "0") {
            // $stagingCond = " AND tbw.workspace_version = '1'";
        }
        $queryGetModules = $this->vwget_modules($additionalFields) . " AND tbw.company_id = $company_id
                                             AND
                                            ((tbfu.action_type = 3)
                                                    AND 
                                                    (
                                                            (tbfu.user_type = 3 AND tbfu.user = {$this->escape($userID)}) OR 
                                                            (tbfu.user_type = 1 AND tbfu.user = {$this->escape($position)}) OR 
                                                            (tbfu.user_type = 2 AND tbdu.user_id = {$this->escape($userID)} AND tbo.status = 1) OR 
                                                            (tbfu.user_type = 4 AND tbfgu.user_id = {$this->escape($userID)} AND tbfgu.is_active = 1)
                                                    )
                                            OR
                                            tbw.category_id = 0
                                            )   
                                            $search
                                            $stagingCond
                            GROUP BY tbw.id ";

        if ($orderBy) {
            $queryGetModules .= " ORDER BY {$orderBy}";
        } else {
            $queryGetModules .= " ORDER BY tbfc.category_name , tbw.form_name";
        }

        $getModules = $this->query($queryGetModules, "array");
        $form_array = array();
        $others_array = array();
        $result = array();
        foreach ($getModules as $key => $value) {
            //push forms and category here
            if ($type == "1") {
                if ($value['form_display_category'] == 1) {//dynamic
//                    $temp_formula = new Formula($value['form_display_formula']);
//                    $temp_formula_replaced = $temp_formula->replaceFormulaFieldNames();
//                    $temp_formula_evaluated = $temp_formula->evaluate();
//                    // print_r($temp_formula_replaced);
//                    // echo '<br/>';
//                    // print_r($temp_formula_evaluated);
//                    if (gettype($temp_formula_evaluated) == 'boolean') {
//                        if ($temp_formula_evaluated == false) {
//                            continue;
//                        }
//                    }
//                    // if($value['form_display_formula']=='Sample Edit Formula'){
//                    //     continue;
//                    // }
                } else {
                    if ($value['form_display_type'] != 1) { //static yes 1 no 0
                        // continue;
                    }
                }
            }
            if ($value['category_id'] != 0) {
                $form_array['' . $value['category_name'] . ''][] = $value;
            } else {
                $others_array['' . $value['category_name'] . ''][] = $value;
            }
        }
        // echo json_encode($form_array);
        $result = array_merge($form_array, $others_array);
        return $result;
    }

    // public function getModulesByAuthor($search) {
    //     $auth = $this->auth;
    //     $userID = $auth['id'];
    //     $position = $auth['position'];
    //     $company_id = $auth['company_id'];
    //     $queryGetModules = "SELECT * from vwget_modules WHERE 
    //                         (
    //                             company_id = {$company_id} AND
    //                             form_actiontype = 1 AND 
    //                             category_accesstype = 2 AND 
    //                             (
    //                                 (category_usertype = 3 AND 
    //                                 category_user = '{$userID}') OR 
    //                                 (category_usertype = 1 AND 
    //                                 category_user = '{$position}') OR 
    //                                 (category_usertype = 2 AND 
    //                                 department_user_id = {$this->escape($userID)} AND orgchart_status = 1) OR
    //                                 (category_usertype = 4 AND 
    //                                 group_user_id = {$this->escape($userID)})
    //                             ) OR (category_id = 0)
    //                         ) 
    //                         $search
    //                         GROUP BY form_id";
    //     $getModules = $this->query($queryGetModules, "array");
    //     return $getModules;
    // }
    public function getModulesByAuthor($search) {
        $auth = $this->auth;
        $userID = $auth['id'];
        $position = $auth['position'];
        $company_id = $auth['company_id'];
        $queryGetModules = $this->vwget_modules() . " AND 
                            tbw.company_id = {$this->escape($company_id)} AND
                            (
                                tbfu.action_type = 1 AND 
                                (
                                    (tbfu.user_type = 3 AND 
                                    tbfu.user = {$this->escape($userID)}) OR 
                                    (tbfu.user_type = 1 AND 
                                    tbfu.user = {$this->escape($position)}) OR 
                                    (tbfu.user_type = 2 AND 
                                    tbdu.user_id = {$this->escape($userID)} AND tbo.status = 1) OR
                                    (tbfu.user_type = 4 AND 
                                    tbfgu.user_id = {$this->escape($userID)}) AND tbfgu.is_active = 1
                                ) OR (tbw.category_id = 0)
                            ) 
                            $search
                            GROUP BY tbw.id";
        $getModules = $this->query($queryGetModules, "array");
        return $getModules;
    }

    // public function getModulesByPrintAuthor($search) {
    //     $auth = $this->auth;
    //     $userID = $auth['id'];
    //     $position = $auth['position'];
    //     $company_id = $auth['company_id'];
    //     $queryGetModules = "SELECT * from vwget_modules WHERE 
    //                         (
    //                             company_id = {$company_id} AND
    //                             form_actiontype = 5 AND 
    //                             category_accesstype = 2 AND 
    //                             (
    //                                 (category_usertype = 3 AND 
    //                                 category_user = '{$userID}') OR 
    //                                 (category_usertype = 1 AND 
    //                                 category_user = '{$position}') OR 
    //                                 (category_usertype = 2 AND 
    //                                 department_user_id = {$this->escape($userID)} AND orgchart_status = 1) OR
    //                                 (category_usertype = 4 AND 
    //                                 group_user_id = {$this->escape($userID)})
    //                             ) OR (category_id = 0)
    //                         ) 
    //                         $search
    //                         GROUP BY form_id";
    //     $getModules = $this->query($queryGetModules, "array");
    //     return $getModules;
    // }
    public function getModulesByPrintAuthor($search) {
        $auth = $this->auth;
        $userID = $auth['id'];
        $position = $auth['position'];
        $company_id = $auth['company_id'];
        $queryGetModules = $this->vwget_modules() . " AND 
                            tbw.company_id = {$this->escape($company_id)} AND
                            (
                                tbfu.action_type = 5 AND 
                                (
                                    (tbfu.user_type = 3 AND 
                                    tbfu.user = {$this->escape($userID)}) OR 
                                    (tbfu.user_type = 1 AND 
                                    tbfu.user = {$this->escape($position)}) OR 
                                    (tbfu.user_type = 2 AND 
                                    tbdu.user_id = {$this->escape($userID)} AND tbo.status = 1) OR
                                    (tbfu.user_type = 4 AND 
                                    tbfgu.user_id = {$this->escape($userID)}) AND tbfgu.is_active = 1
                                )
                            ) 
                            $search
                            GROUP BY tbw.id";
        $getModules = $this->query($queryGetModules, "array");
        return $getModules;
    }

    public function getFormUsersByDepartments($search) {
        $queryGetFormUsersByDepartments = "SELECT fcu.id as fcuID, fcu.user as id, tboo.department FROM tbform_users fu  
                                LEFT JOIN tbform_category_users fcu ON fu.user = fcu.user AND fu.user_type = fcu.user_type 
                                LEFT JOIN tborgchartobjects tboo on fcu.user = tboo.department_code 
                                LEFT JOIN tborgchart tbo on tbo.id = tboo.orgChart_id 
                                WHERE fu.action_type = 3 AND 
                                fcu.access_type = 2 AND fcu.access_type = 2 AND 
                                fu.user_type = 2 AND tbo.is_active = 1 AND tbo.status = 1 AND tbo.is_delete = 0" . $search;
        $getFormUsersByDepartments = $this->query($queryGetFormUsersByDepartments, "array");
        return $getFormUsersByDepartments;
    }

    public function getFormUsersByPositions($search) {
        $queryGetFormUsersByPositions = "SELECT *, fcu.id as fcuID FROM tbform_users fu 
                                LEFT JOIN tbform_category_users fcu ON fu.user = fcu.user AND 
                                fu.user_type = fcu.user_type LEFT JOIN tbpositions p on fcu.user = p.id 
                                WHERE fu.action_type = 3 AND 
                                fcu.access_type = 2 AND fu.user_type = 1" . $search;
        $getFormUsersByPosition = $this->query($queryGetFormUsersByPositions, "array");
        return $getFormUsersByPosition;
    }

    public function getFormUsersByUsers($search) {
        $queryGetFormUsersByUsers = "SELECT *, fcu.id as fcuID FROM tbform_users fu 
                                LEFT JOIN tbform_category_users fcu ON fu.user = fcu.user AND 
                                fu.user_type = fcu.user_type LEFT JOIN tbuser u on fcu.user = u.id 
                                WHERE fu.action_type = 3 AND 
                                fcu.access_type = 2 AND fu.user_type = 3" . $search;
        $getFormUsersByUsers = $this->query($queryGetFormUsersByUsers, "array");
        return $getFormUsersByUsers;
    }

    public function getFormUsersByGroups($search) {
        $queryGetFormUsersByUsers = "SELECT *, fcu.id as fcuID FROM tbform_users fu 
                                LEFT JOIN tbform_category_users fcu ON fu.user = fcu.user AND 
                                fu.user_type = fcu.user_type LEFT JOIN tbform_groups tbg on fcu.user = tbg.id 
                                WHERE fu.action_type = 3 AND 
                                fcu.access_type = 2 AND fu.user_type = 4" . $search;
        $getFormUsersByUsers = $this->query($queryGetFormUsersByUsers, "array");
        return $getFormUsersByUsers;
    }

    // public function vwget_forms($other_fields=null) {
    //     if(trim($other_fields)!=""){
    //         $other_fields = ", ".$other_fields;
    //     }
    //     $query = "SELECT 
    //             COALESCE((SELECT tbwf.id FROM tbworkflow tbwf WHERE tbwf.form_id = tbw.id AND tbwf.is_active = 1 and tbwf.is_delete = 0),0) as workflow_id,
    //             /* category */
    //             COALESCE(tbfc.id,0) as category_id,
    //             COALESCE(tbfc.category_name,'Others') as category_name,
    //             /* category users */
    //             tbfcu.user_type as category_usertype,
    //             tbfcu.user as category_user,
    //             tbfcu.access_type as category_accesstype,
    //             /* form */
    //             tbw.id as form_id,
    //             tbw.id as wsID,
    //             tbw.form_name,
    //             tbw.form_json,
    //             tbw.workspace_version,
    //             tbw.company_id,
    //             tbw.form_description,
    //             tbw.date_created as form_date_created,
    //             tbw.date_updated as form_date_updated,
    //             tbw.is_active as form_active,
    //             tbw.active_fields,
    //             tbw.workspace_version,
    //             /* form users */
    //             tbfu.action_type as form_actiontype,
    //             /* user department*/
    //             tbdu.user_id as department_user_id,
    //              /*created by */
    //             -- CONCAT_WS(' ',tbu_created_by.first_name,tbu_created_by.last_name) as created_by_name,
    //             tbu_created_by.display_name as created_by_name,
    //             /*updated by*/
    //             -- CONCAT_WS(' ',tbu_updated_by.first_name,tbu_updated_by.last_name,' ') as update_by_name
    //             tbu_updated_by.display_name as update_by_name,
    //             /*dev forms */
    //             tbdf.parent_id as parent_id
    //             $other_fields
    //             FROM tb_workspace tbw LEFT JOIN 
    //             tbdev_forms tbdf ON tbw.id = tbdf.form_id LEFT JOIN 
    //             tbform_category tbfc ON tbfc.id = tbw.category_id LEFT JOIN 
    //             tbform_category_users tbfcu ON tbfc.id = tbfcu.form_category_id LEFT JOIN 
    //             tbform_users tbfu ON tbfcu.user = tbfu.user AND tbfcu.user_type = tbfu.user_type AND tbfu.form_id = tbw.id LEFT JOIN 
    //             tbdepartment_users tbdu ON tbdu.department_code = tbfcu.user LEFT JOIN 
    //             tbpositions tbp ON tbp.id = tbfcu.user LEFT JOIN 
    //             tbuser tbu_created_by ON tbu_created_by.id = tbw.created_by LEFT JOIN
    //             tbuser tbu_updated_by ON tbu_updated_by.id = tbw.updated_by  
    //             WHERE 
    //             tbw.is_delete = 0";
    //     return $query;
    // }

    public function vwget_forms($other_fields = null, $additional_arg) {
        if (trim($other_fields) != "") {
            $other_fields = ", " . $other_fields;
        }
        $query = "SELECT 

                COALESCE((SELECT tbwf.id FROM tbworkflow tbwf WHERE tbwf.form_id = tbw.id AND tbwf.is_active = 1 and tbwf.is_delete = 0),0) as workflow_id,

                /* category */
                COALESCE(tbfc.id,0) as category_id,
                COALESCE(tbfc.category_name,'Others') as category_name,


                /* form */

                tbw.id as form_id,
                tbw.id as wsID,
                tbw.form_name,
                tbw.form_table_name,
                tbw.form_json,
                tbw.workspace_version,
                tbw.company_id,
                tbw.form_description,
                tbw.date_created as form_date_created,
                tbw.date_updated as form_date_updated,
                tbw.is_active as form_active,
                tbw.active_fields,
                tbw.workspace_version,
                tbw.form_alias,
                tbw.allow_portal,

                /* form users */
                tbfu.action_type as form_actiontype,


                /* user department*/

                /* created by */
                -- CONCAT_WS(' ',tbu_created_by.first_name,tbu_created_by.last_name) as created_by_name,
                tbu_created_by.display_name as created_by_name,

                /*updated by*/

                -- CONCAT_WS(' ',tbu_updated_by.first_name,tbu_updated_by.last_name,' ') as update_by_name
                tbu_updated_by.display_name as update_by_name,
                
                /*dev forms */
                tbdf.parent_id as parent_id
                
                $other_fields
                    
                FROM tb_workspace tbw LEFT JOIN 
                tbdev_forms tbdf ON tbw.id = tbdf.form_id LEFT JOIN 
                tbform_category tbfc ON tbfc.id = tbw.category_id LEFT JOIN 
                tbform_users tbfu ON tbfu.form_id = tbw.id $additional_arg LEFT JOIN 
                tbuser tbu_created_by ON tbu_created_by.id = tbw.created_by LEFT JOIN
                tbuser tbu_updated_by ON tbu_updated_by.id = tbw.updated_by  
                  
                WHERE 
                tbw.is_delete = 0";
        return $query;
    }

    /* added by Japhet Morada */

    public function vwget_keyword() {
        $auth = $this->auth;
        $company_id = $auth['company_id'];
        $query = "SELECT * FROM tbkeyword WHERE company_id = '" . $company_id . "' ";
        return $query;
    }

    public function vwget_generate() {
        $query = "SELECT 

                COALESCE((SELECT tbwf.id FROM tbworkflow tbwf WHERE tbwf.form_id = tbw.id AND tbwf.is_active = 1 and tbwf.is_delete = 0),0) as workflow_id,

                /* category */
                COALESCE(tbfc.id,0) as category_id,
                COALESCE(tbfc.category_name,'Others') as category_name,

                /* category users */
                tbfcu.user_type as category_usertype,
                tbfcu.user as category_user,
                tbfcu.access_type as category_accesstype,


                /* form */

                tbw.id as form_id,
                tbw.id as wsID,
                tbw.form_name,
                tbw.form_json,
                tbw.company_id,
                tbw.form_description,
                tbw.date_created as form_date_created,
                tbw.date_updated as form_date_updated,
                tbw.is_active as form_active,
                tbw.active_fields,
                tbw.form_alias,

                /* form users */
                tbfu.action_type as form_actiontype,


                /* user department*/
                tbdu.user_id as department_user_id,

                /* created by */
                -- CONCAT_WS(' ',tbu_created_by.first_name,tbu_created_by.last_name) as created_by_name,
                tbu_created_by.display_name as created_by_name,

                /*updated by*/

                -- CONCAT_WS(' ',tbu_updated_by.first_name,tbu_updated_by.last_name,' ') as update_by_name,
                tbu_updated_by.display_name as update_by_name,

                tbg.id as generate_id,
                tbg.is_active as generate_active,
                tbg.date_created as generate_date_created,
                tbg.date_updated as generate_date_updated
                
                FROM tb_generate tbg LEFT JOIN
                tb_workspace tbw ON tbg.form_id = tbw.id LEFT JOIN 
                tbform_category tbfc ON tbfc.id = tbw.category_id LEFT JOIN 
                tbform_category_users tbfcu ON tbfc.id = tbfcu.form_category_id LEFT JOIN 
                tbform_users tbfu ON tbfcu.user = tbfu.user AND tbfcu.user_type = tbfu.user_type AND tbfu.form_id = tbw.id LEFT JOIN 
                tbdepartment_users tbdu ON tbdu.department_code = tbfcu.user LEFT JOIN 
                tbpositions tbp ON tbp.id = tbfcu.user LEFT JOIN
                tbform_groups_users tbfgu ON tbfgu.group_id = tbfcu.user LEFT JOIN 
                tborgchart tbo on tbo.id = tbdu.orgChart_id LEFT JOIN
                tbuser tbu_created_by ON tbu_created_by.id = tbw.created_by LEFT JOIN
                tbuser tbu_updated_by ON tbu_updated_by.id = tbw.updated_by  
                  
                WHERE 
                tbw.is_delete = 0";
        return $query;
    }

    // changed 03/18/2015
    // public function vwget_workflow(){
    //     $str = "SELECT 
    //             /* Workflow */
    //             tbwf.id as wf_id,
    //             tbwf.title as wf_title,
    //             tbwf.is_active as Is_Active,
    //             tbwf.date as wf_date,
    //             /* category */
    //             COALESCE(tbfc.id,0) as category_id,
    //             COALESCE(tbfc.category_name,'Others') as category_name,
    //             /* category users */
    //             tbfcu.user_type as category_usertype,
    //             tbfcu.user as category_user,
    //             tbfcu.access_type as category_accesstype,
    //             /* form */
    //             tbw.id as form_id,
    //             tbw.form_name,
    //             tbw.company_id,
    //             /* form users */
    //             tbfu.action_type as form_actiontype,
    //             /* user department*/
    //             tbdu.user_id as department_user_id,
    //             /* creator */
    //             -- concat_ws(' ',tbu_c.first_name,tbu_c.last_name) as workflow_creator 
    //             tbu_c.display_name as workflow_creator  
    //             FROM tbworkflow tbwf LEFT JOIN tb_workspace tbw ON tbwf.form_id = tbw.id LEFT JOIN 
    //             tbform_category tbfc ON tbfc.id = tbw.category_id LEFT JOIN 
    //             tbform_category_users tbfcu ON tbfc.id = tbfcu.form_category_id LEFT JOIN 
    //             tbform_users tbfu ON tbfcu.user = tbfu.user AND tbfcu.user_type = tbfu.user_type AND tbfu.form_id = tbw.id LEFT JOIN 
    //             tbdepartment_users tbdu ON tbdu.department_code = tbfcu.user LEFT JOIN 
    //             tbpositions tbp ON tbp.id = tbfcu.user  LEFT JOIN 
    //             tbuser tbu_c on tbwf.created_by = tbu_c.id
    //             WHERE 
    //             /* active forms  and workflow*/
    //             tbw.is_active = 1 AND tbw.is_delete = 0 AND tbwf.is_delete = 0";
    //     return $str;
    // }
    public function vwget_workflow() {
        $str = "SELECT 
            /* Workflow */
            tbwf.id as wf_id,
            tbwf.title as wf_title,
            tbwf.is_active as Is_Active,
            tbwf.date as wf_date,

            /* category */
            COALESCE(tbfc.id,0) as category_id,
            COALESCE(tbfc.category_name,'Others') as category_name,

            /* form */
            tbw.id as form_id,
            tbw.form_name,
            tbw.company_id,

            /* creator */
            tbu_c.display_name as workflow_creator  

            FROM tbworkflow tbwf
            LEFT JOIN tb_workspace tbw
            ON tbw.id = tbwf.form_id
            LEFT JOIN 
            tbform_category tbfc 
            ON tbfc.id = tbw.category_id LEFT JOIN 
            tbuser tbu_c 
            on tbwf.created_by = tbu_c.id
            WHERE 
            tbw.is_active = 1 AND 
            tbw.is_delete = 0 AND
            tbwf.is_delete = 0";
        return $str;
    }

    public function vwget_modules($additionalFields = array()) {

        $additionalFieldsString = "";

        if ($additionalFields != null && count($additionalFields) > 0) {
            $additionalFieldsString = "," . join(",", $additionalFields);
        }

        $sqlStr = "SELECT 
                /* category */
                COALESCE(tbfc.id,0) as category_id,
                COALESCE(tbfc.category_name,'Others') as category_name,



                /* form */

                tbw.id as form_id,
                tbw.form_name,
                tbw.company_id,
                tbw.is_active as form_active,
                tbw.form_display_type as form_display_type,
                tbw.form_display_category as form_display_category,
                tbw.form_display_formula as form_display_formula,
                tbw.form_alias,

                /* form users */
                tbfu.action_type as form_actiontype,
                tbfu.user_type as form_usertype,
                tbfu.user as form_user,

                /* user department*/
                tbdu.id as department_users_id,
                tbdu.department_code as department_code,
                tbdu.user_id as department_user_id, 
                tbo.id as orgchart_id,
                tbo.status as orgchart_status,


                /* user group*/

                tbfgu.user_id as group_user_id,
                tbfgu.is_active as group_is_active

                /* additional fields to query */
                {$additionalFieldsString}

                FROM tb_workspace tbw LEFT JOIN 
                tbform_category tbfc ON tbfc.id = tbw.category_id LEFT JOIN 
                tbform_users tbfu ON tbfu.form_id = tbw.id LEFT JOIN 
                tbdepartment_users tbdu ON tbdu.department_code = tbfu.user LEFT JOIN 
                tbform_groups_users tbfgu ON tbfgu.group_id = tbfu.user LEFT JOIN 
                tborgchart tbo on tbo.id = tbdu.orgChart_id LEFT JOIN
                tbpositions tbp ON tbp.id = tbfu.user LEFT JOIN 
                tbworkflow tbwf ON tbwf.form_id = tbw.id 
                WHERE 
                /* active forms  and workflow*/
                tbw.is_active = 1 AND tbwf.is_active = 1 AND tbw.is_delete = 0 AND tbwf.is_delete = 0";
        return $sqlStr;
    }

    public function addslashes_escape($str) {
        $str = str_replace("_", "\\\\\\_", $str);
        $str = str_replace("%", "\\\\\\%", $str);

        return $str;
    }

    public function status_condition($field, $value) {
        $ret = "";
        if (strpos("active", strtolower($value)) !== false) {
            $ret = " OR " . $field . " = '1'";
        } else if (strpos("not active", strtolower($value)) !== false) {
            $ret = " OR " . $field . " = '0'";
        } else if (strpos("draft", strtolower($value)) !== false) {
            $ret = " OR " . $field . " = '2'";
        }
        return $ret;
    }

    public function type_condition($field, $value) {
        $ret = "";
        if (strpos("published", strtolower($value)) !== false) {
            $ret = " OR " . $field . " = '1'";
        } else if (strpos("template", strtolower($value)) !== false) {
            $ret = " OR " . $field . " = '2'";
        }
        return $ret;
    }

    public function searchFieldNotIncluded() {
        $arr = array('multiple_attachment_on_request', 'attachment_on_request');
        $string_arr = "";
        foreach ($arr as $value) {
            $string_arr.="'" . $value . "',";
        }
        $string_arr = substr($string_arr, 0, strlen($string_arr) - 1);
        return $string_arr;
    }

    public function getNextPreviousRecord($request_id, $form_id, $type) {
        $obj = array("condition" => " AND request.ID " . $type . " " . $request_id);
        if ($type == '>') {
            $obj['sorting'] = "ASC";
        }
        if ($type == '<') {
            $obj['sorting'] = "DESC";
        }
        $obj = json_decode(json_encode($obj), true);
        return $this->getManyRequestV2("", 0, $form_id, 0, 1, $obj);
    }

    public function getDevForms($args) {
        $query_string = "SELECT tbw.id as form_id,tbdf.*  "
                . "FROM tb_workspace tbw LEFT JOIN tbdev_forms tbdf on tbw.id = tbdf.form_id "
                . "WHERE tbw.company_id = '" . $this->auth['id'] . "' "
                . "AND tbw.workspace_version = '2' " . $args;
        $query = $this->query($query_string, "array");

        return $query;
    }

}

function array_insert(&$array, $element, $position = null) {
    if (count($array) == 0) {
        $array[] = $element;
    } elseif (is_numeric($position) && $position < 0) {
        if ((count($array) + position) < 0) {
            $array = array_insert($array, $element, 0);
        } else {
            $array[count($array) + $position] = $element;
        }
    } elseif (is_numeric($position) && isset($array[$position])) {
        $part1 = array_slice($array, 0, $position, true);
        $part2 = array_slice($array, $position, null, true);
        $array = array_merge($part1, array($position => $element), $part2);
        foreach ($array as $key => $item) {
            if (is_null($item)) {
                unset($array[$key]);
            }
        }
    } elseif (is_null($position)) {
        $array[] = $element;
    } elseif (!isset($array[$position])) {
        $array[$position] = $element;
    }
    $array = array_merge($array);
    return $array;
}

function getValidHeaderFields($fields_headerObj, $getCustomView, $fields_headerObjType) {

    $fields_headerObjDecodedTrue = json_decode(json_encode($fields_headerObj), true);


    if ($fields_headerObjType == "1") {

        $defaultFields = array();
        $customView = array();

        //migrate to default tupe of array not std
        //count of defaultFields
        $otherFieldsStartPos = count($defaultFields);

        //insert default fields in saved header
        foreach ($defaultFields as $defaultFields_value) {
            array_insert($fields_headerObjDecodedTrue, $defaultFields_value, $otherFieldsStartPos);
            $otherFieldsStartPos++;
        }

        //push valid header in custom view
        $customView = combineColumnHeaderInfo($getCustomView[0]['json'], $fields_headerObjDecodedTrue);
    } else if ($fields_headerObjType == "0") {
        $customView = array();
        // FOR DEFAULT COLUMN
        $defaultFields = array(array("field_label" => "Tracking Number", "field_name" => "TrackNo"), array("field_label" => "Requestor", "field_name" => "requestorName"), array("field_label" => "Status", "field_name" => "Status"), array("field_label" => "Date Created", "field_name" => "DateCreated"));
        //push valid header in custom view
        $customView = combineColumnHeaderInfo($getCustomView[0]['json'], $defaultFields);
    } else {

        $defaultFields = array(array("field_label" => "Tracking Number", "field_name" => "TrackNo"), array("field_label" => "Requestor", "field_name" => "requestorName"), array("field_label" => "Status", "field_name" => "Status"), array("field_label" => "Date Created", "field_name" => "DateCreated"));
        $customView = array();

        //migrate to default tupe of array not std
        $fields_headerObjDecodedTrue = json_decode(json_encode($fields_headerObj), true);

        //count of defaultFields
        $otherFieldsStartPos = count($defaultFields);

        //insert default fields in saved header
        foreach ($defaultFields as $defaultFields_value) {
            array_insert($fields_headerObjDecodedTrue, $defaultFields_value, $otherFieldsStartPos);
            $otherFieldsStartPos++;
        }
        $customView = combineColumnHeaderInfo($getCustomView[0]['json'], $fields_headerObjDecodedTrue);
    }
    $customView = json_decode(json_encode($customView), true);

    return $customView;
}

function combineColumnHeaderInfo($getCustomView, $fields_headerObjDecodedTrue) {
    $customView = array();
    //push valid header in custom view
    $getCustomView = json_decode($getCustomView, true);
    foreach ($getCustomView as $value) {
        foreach ($fields_headerObjDecodedTrue as $value2) {
            if ($value['field_name'] == $value2['field_name']) {
                $value2['width'] = $value['width'];
                if ($value2['field_name'] == "TrackNo") {
                    $value2['field_label'] = "Tracking Number";
                } else if ($value2['field_name'] == "DateCreated") {
                    $value2['field_label'] = "Date Created";
                }
                array_push($customView, $value2);
                continue;
            }
        }
    }

    $checker = array();
    foreach ($fields_headerObjDecodedTrue as $value2) {
        foreach ($getCustomView as $value) {
            if ($value['field_name'] == $value2['field_name']) {
                array_push($checker, $value);
            }
        }

        if (count($checker) >= 1) {
            
        } else {
            array_push($customView, $value2);
        }
        $checker = array();
    }
    return $customView;
}

?>
