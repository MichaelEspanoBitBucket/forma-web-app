<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class StagingForm extends Database{
    var $form_properties = array("form_content","form_buttons","button_content","form_json","active_fields","MobileJsonData","MobileContent");
    var $staging_id;var $parent_id;var $dev_form_prop;var $parent_prop;

    var $auth;
    
    public function __construct($staging_id) {
        parent::__construct();
        $this->auth = Auth::getAuth('current_user');


        $this->staging_id = $staging_id;
        
        $this->dev_form_prop = $this->getDevFormProp();
        $this->parent_id = $this->dev_form_prop['parent_id']; //set parent id property
        $this->parent_prop = $this->getParentFormProp();
        
    }
    /*

     * Update all properties
     *      
     */
    
    public function setProperties(){
        //fields
        $this->setFieldProp();
        
        //design and other properties
        $this->setFormProp();
        
        //workfow
        $this->setWorkflowProp();
    }
    
    /*
     * 
     * Form Properties in table tb_workspace
     * 
     *  
     */
    
    private function setFormProp() {
        $fs = new functions();
        $date = $fs->currentDateTime();
        $user_id = $this->auth['id'];
        $set = array();
        //set what to update
        
        foreach($this->form_properties as $value){
            $value_json = $this->dev_form_prop[''. $value .''];
            $parent_prop_json = json_decode($this->parent_prop['form_json'],true);
            if($value=="form_json"){
                $value_json = json_decode($value_json,true);
                //retain other information in json
                
                //for form name
                $value_json['Title'] = $this->parent_prop['form_name'];
                $value_json['form_json']['workspace_title'] = $this->parent_prop['form_name'];
                
                //for form description
                $value_json['DisplayName'] = $this->parent_prop['form_description'];
                $value_json['form_json']['workspace_displayName'] = $this->parent_prop['form_description'];
                
                //for button
                $value_json['form_json']['workspace_buttonName'] = $parent_prop_json['form_json']['workspace_buttonName'];

                //prefix
                $value_json['Prefix'] = $this->parent_prop['reference_prefix'];
                $value_json['form_json']['workspace_prefix'] = $this->parent_prop['reference_prefix'];

                //display on sidebar
                $value_json['fm-type-dos'] = $parent_prop_json['fm-type-dos'];
                $value_json['form_json']['fm-type-dos'] = $parent_prop_json['fm-type-dos'];

                //formula on sidebar
                $value_json['form_display-dynamic'] = $parent_prop_json['form_display-dynamic'];
                $value_json['form_json']['form_display-dynamic'] = $parent_prop_json['form_display-dynamic'];

                //display_import
                $value_json['display_import'] = $parent_prop_json['display_import'];
                $value_json['form_json']['display_import'] = $parent_prop_json['display_import'];

                //workspace_type
                $value_json['Type'] = $this->parent_prop['reference_type'];
                $value_json['form_json']['workspace_type'] = $this->parent_prop['reference_type'];
                
                //deletion
                $value_json['form_json']['delete_enable'] = $parent_prop_json['form_json']['delete_enable'];
                $value_json['form_json']['delete_type'] = $parent_prop_json['form_json']['delete_type'];
                

                //for workspace version
                $value_json['workspace_version'] = '1';
                $value_json['form_json']['workspace_version'] = '1';
                
                $value_json = json_encode($value_json);
            }
            $set['date_updated'] = $date;
            $set['updated_by'] = $user_id;

            $set[''. $value .''] = $value_json; //collect all declared fields to change
        }
        //id of form to update(parent)
        $where = array("id"=>$this->parent_id);
//        print_r($set);
//        print_r($where);
        $this->update("tb_workspace",$set,$where);
    }
    /*

     * Get Dev Form Properties     
     */
    private function getDevFormProp(){
        $search = new Search();
        $form_prop_str = "";
        foreach($this->form_properties as $value){
            $form_prop_str.= " tbw.".$value.", "; // collect all declared fields to change
        }
        $form_prop_str = substr($form_prop_str,0,  strlen($form_prop_str)-2);
        $json = array("other_condition"=>" AND tbdf.form_id = '". $this->staging_id ."'",
            "other_fields"=>$form_prop_str);
        $result2 = $search->getFormsV2("", "0", $json, "array");
        return $result2[0];
    }
    
    /*

     * Get Parent Properties     
     */
    private function getParentFormProp(){
        $parent_prop = new Form($this,$this->parent_id);
        return json_decode(json_encode($parent_prop),true);
    }
    /*
     * 
     * Fields in table tbfields
     * 
     *  
     */
    private function setFieldProp() {
        
        $active_fields = split(",",$this->parent_prop['active_fields']);
        $fields = $this->getFieldProp();
        $this->delete("tbfields",array("form_id"=>$this->parent_id));
        foreach($fields as $field){
            /*

             * 
             * Insert data in tbfields
             * 
             */
            
            $field['id'] = '';
            $field['form_id'] = $this->parent_id; // change the value of form id to parent id for reflection of field from staging
//            print_r($field);
            $this->insert("tbfields",$field);
            
            /*

             * 
             * Create or modify field in table
             * 
             */
            $fieldType = "longtext";
            $fieldName = $field['field_name'];
//            print_r($field);
            if($field['field_input_type']=="Number"){
                $fieldType = "int";
            }else if($field['field_input_type']=="Currency"){
                $fieldType = "double";
            }else{
                if($field['field_type']=="datepicker"){
                    $fieldType = "date";
                }else if($field['field_type']=="dateTime"){
                    $fieldType = "datetime";
                }
            }
            $field_index = in_array($fieldName, $active_fields);
            
            if($fieldName=="" || in_array($fieldName, $fieldAltered)){
                    continue;
            }
            $alterSQL = 'ALTER TABLE `' . $this->parent_prop['form_table_name']."` ";
            if ($field_index) {
                    //update field
                    $alterSQL .=  'MODIFY `' . $fieldName . '` ' . $fieldType . '; ';
            } else {
                    //new field
                    $alterSQL .= 'ADD `' . $fieldName . '` ' . $fieldType . '; ';
            }
//            echo $alterSQL."\n";
            //push the fieldnames in array to prevent multiple alter of fields
            $fieldAltered[] = $fieldName;
            $this->create($alterSQL);
        }
        
    }
    /*
     * 
     * Get Fields of parent
     * 
     *  
     */
    private function getFieldProp() {
        $getFields = $this->query("SELECT * FROM tbfields where form_id = ".$this->staging_id , "array");
        return $getFields;
    }
    /*
     * 
     * Workflow in table tbworkflow
     * 
     *  
     */
    private function setWorkflowProp() {
        $workflow = $this->getWorkflowProp();
        
        $workflow_prop = $workflow['workflow_data'];
        $workflow_obj = $workflow['workflow_objects'];
        $workflow_lines = $workflow['workflow_lines'];
        /*
         * Workflow
         */
        
        //update all active worflow
        $this->update("tbworkflow", array("is_active"=>"0"),array("form_id"=>$this->parent_id));
        
        //assign other fields in workflow
        $workflow_prop['id'] = "";
        $workflow_prop['form_id'] = $this->parent_id;
        $workflow_id = $this->insert("tbworkflow",$workflow_prop);
        
        /*
         * Workflow Objects
         */
        foreach ($workflow_obj as $object) {
            $object['id'] = "";
            $object['workflow_id'] = $workflow_id;
//            print_r($object);
            $this->insert("tbworkflow_objects",$object);
        }
        /*
         * Workflow Lines
         */
        foreach ($workflow_lines as $lines) {
            $lines['id'] = "";
            $lines['workflow_id'] = $workflow_id;
//            print_r($lines);
            $this->insert("tbworkflow_lines",$lines);
        }
    }
    private function getWorkflowProp() {
        $arr = array();
        /*
         * Workflow Data
         */
        $getWorkflow = $this->query("SELECT * FROM tbworkflow WHERE form_id='". $this->staging_id ."' AND is_active = 1 AND is_delete = 0","row");
        $workflow_id = $getWorkflow['id'];
        $getWorkflow['form_id'] = $this->parent_id;
        $arr['workflow_data'] = $getWorkflow;
        
        /*
         * Workflow Objects
         */
        $getWorkflowObj = $this->query("SELECT * FROM tbworkflow_objects WHERE workflow_id = '". $workflow_id ."'","array");
        $arr['workflow_objects'] = $getWorkflowObj;
        
        /*
         * Workflow Lines
         */
        $getWorkflowLines = $this->query("SELECT * FROM tbworkflow_lines WHERE workflow_id = '". $workflow_id ."'","array");
        $arr['workflow_lines'] = $getWorkflowLines;

        
        return $arr;
    }
}