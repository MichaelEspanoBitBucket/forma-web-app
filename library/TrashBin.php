<?php

include_once API_LIBRARY_PATH . 'API.php';

class TrashBin extends Database {

    var $auth;

    public function __construct() {
        parent::__construct();
        $this->auth = Auth::getAuth('current_user');
    }

    public function getTrashRecordDataTable($json, $type) {
        $limit = "";
        $order_by = "";
        $where = "";

        //formulate query attributes
        if ($json['start'] != "" AND $json['end'] != "") {
            $limit = " LIMIT " . $json['start'] . ", " . $json['end'] . "";
        }
        if ($json['condition'] != "") {
            $where = " WHERE tbtb.deletion_type != 2 AND  " . $json['condition'];
        }
        if ($json['column-sort'] != "" && $json['column-sort-type'] != "") {
            $order_by = " ORDER BY " . $json['column-sort'] . " " . $json['column-sort-type'];
        }

        //query
        $queryStr = "SELECT tbtb.*, tbw.form_name FROM tbtrash_bin tbtb LEFT JOIN tb_workspace tbw ON tbw.id = tbtb.form_id " . $where . $order_by . $limit;

        // echo $queryStr;
        $getTrash = $this->query($queryStr, $type);
        return $getTrash;
    }

    public function getSpecificTrashRecord($form_id, $record_id) {
        //query
        $redis_cache = getRedisConnection();

        if ($redis_cache) {
            $cache_trash_bin_record = json_decode($redis_cache->get("trash_bin_record_" . $form_id . "::" . $record_id), true);
        }

        if ($cache_trash_bin_record) {
            $getTrash = $cache_trash_bin_record;
        } else {
            $queryStr = "SELECT tbtb.*, tbw.form_name FROM tbtrash_bin tbtb LEFT JOIN tb_workspace tbw ON tbw.id = tbtb.form_id WHERE tbtb.form_id = '" . $form_id . "' AND record_id = '" . $record_id . "'";

            // echo $queryStr;
            $getTrash = $this->query($queryStr, "array");

            if ($redis_cache) {
                $redis_cache->set("trash_bin_record_" . $form_id . "::" . $record_id, json_encode($getTrash));
            }
        }
        return $getTrash;
    }

    public function getRecordTrashType($type) {
        $record_type = "";
        if ($type == "1") {
            $record_type = "Request";
        }
        return $record_type;
    }

    public function deleteRequest($form_id, $record_id, $type = "1") {
        $user_id = $this->auth['id']; // the value will become 0 if due date (48 hours)
        $audit_action = "38";
        $form = new Form($this, $form_id);
        $fs = new functions;
        $date = $fs->currentDateTime();


        if ($form->enable_deletion == "1") {
            if ($form->deletion_type == "2") {
                //permanent delete
                $where = array("id" => $record_id);
                $this->delete($form->form_table_name, $where);
            }
            $insert = array("record_id" => $record_id,
                "record_type" => 1,
                "form_id" => $form_id,
                "table_name" => $form->form_table_name,
                "user_id" => $user_id,
                "status" => $form->deletion_type,
                "deletion_type" => $form->deletion_type,
                "date_time" => $date);
            $this->insert("tbtrash_bin", $insert);
            //audit logs here
            $insert_audit_rec = array("user_id" => $user_id,
                "audit_action" => $audit_action,
                "table_name" => $form->form_table_name,
                "record_id" => $record_id,
                "date" => $date,
                "ip" => $_SERVER["REMOTE_ADDR"],
                "is_active" => 1);
            $this->insert("tbaudit_logs", $insert_audit_rec);
        }
    }

    public function deleteManyRequest($form_id, $records, $type = "1") {
        $user_id = $this->auth['id']; // the value will become 0 if due date (48 hours)
        $audit_action = "38";
        $form = new Form($this, $form_id);
        $fs = new functions;
        $date = $fs->currentDateTime();


        if ($form->enable_deletion == "1") {
            foreach ($records as $key => $value) {
                $record_id = $value;
                $getTrashRecordSql = "SELECT * FROM tbtrash_bin WHERE record_id = '" . $record_id . "' AND form_id = '" . $form_id . "' LIMIT 0,1";
                $getTrashRecord = $this->query($getTrashRecordSql, "array");
                if (count($getTrashRecord) == 0) {
                    if ($form->deletion_type == "2") {
                        //permanent delete
                        $where = array("id" => $record_id);
                        $this->delete($form->form_table_name, $where);
                    }
                    $insert = array("record_id" => $record_id,
                        "record_type" => 1,
                        "form_id" => $form_id,
                        "table_name" => $form->form_table_name,
                        "user_id" => $user_id,
                        "status" => $form->deletion_type,
                        "deletion_type" => $form->deletion_type,
                        "date_time" => $date);
                    $this->insert("tbtrash_bin", $insert);
                    //audit logs here
                    $insert_audit_rec = array("user_id" => $user_id,
                        "audit_action" => $audit_action,
                        "table_name" => $form->form_table_name,
                        "record_id" => $record_id,
                        "date" => $date,
                        "ip" => $_SERVER["REMOTE_ADDR"],
                        "is_active" => 1);
                    $this->insert("tbaudit_logs", $insert_audit_rec);
                }
            }
        }
    }

    public function retrieveRequest($dataId) {
        $fs = new functions;
        $date = $fs->currentDateTime();
        $getTrash = $this->query("SELECT * FROM tbtrash_bin WHERE id = " . $dataId, "row");
        $where = array("id" => $dataId);
        $this->delete("tbtrash_bin", $where);
        //audit logs here
        $insert_audit_rec = array("user_id" => $user_id,
            "audit_action" => "39",
            "table_name" => $getTrash['table_name'],
            "record_id" => $getTrash['record_id'],
            "date" => $date,
            "ip" => $_SERVER["REMOTE_ADDR"],
            "is_active" => 1);
        $this->insert("tbaudit_logs", $insert_audit_rec);
    }

    public function retrieveMultiRequest($retrieveRecordSelected) {
        $redis_cache = getRedisConnection();
        $apiDB = new APIDatabase();
        $fs = new functions;
        $date = $fs->currentDateTime();
        $apiDB->connect();

        $apiDB->beginTransaction();

        foreach ($retrieveRecordSelected as $value) {
            # code...
            $where = array("record_id" => $value['record_id'], "table_name" => $value['table_name']);
            $updated = $apiDB->delete("tbtrash_bin", $where);
            print($updated);
            if ($updated) {
                $insert_audit_rec = array("user_id" => $user_id,
                    "audit_action" => "39",
                    "table_name" => $value['table_name'],
                    "record_id" => $value['record_id'],
                    "date" => $date,
                    "ip" => $_SERVER["REMOTE_ADDR"],
                    "is_active" => 1);
                $apiDB->insert("tbaudit_logs", $insert_audit_rec);

                if ($redis_cache) {
                    //deletememcached
                    $form_id = $value['$form_id'];
                    $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
                    $form_record_memcached = array("form_list", "form_count");
                    $nav_memcached = array("leftbar_nav");
                    $starred_memcached = array("starred_list");
                    $request_memcached = array("form_access_" . $form_id,
                        "related_info_form_id_" . $form_id, "request_details_" . $form_id,
                        "request_access_" . $form_id, "request_author_access_" . $form_id,
                        "workspace_form_details_" . $form_id);


                    $deleteMemecachedKeys = array_merge(
                            $form_record_memcached, $nav_memcached, $starred_memcached, $request_memcached
                    );

                    $fs->deleteMemcacheKeys($deleteMemecachedKeys);
                    $fs->deleteMemcacheForm($request_record_memcacached, $form_id);
                }
            }
        }
        $apiDB->commit();
        $apiDB->disconnect();
    }

    public function record_type($value) {
        $ret = "";
        if (strpos("request", strtolower($value)) !== false) {
            $ret = " OR record_type = '1'";
        } else if (strpos("post", strtolower($value)) !== false) {
            $ret = " OR record_type = '2'";
        } else if (strpos("comment", strtolower($value)) !== false) {
            $ret = " OR record_type = '3'";
        }
        return $ret;
    }

    public function requestDueDate() {
        $date = functions::currentDateTime();
        $getTrashQuery = "SELECT *, ADDTIME(date_time, '48:0:0') as addTime FROM tbtrash_bin WHERE status = '1' AND record_type = '1' AND  '" . $date . "' >= ADDTIME(date_time, '48:0:0')    ";
        $getTrash = $this->query($getTrashQuery, "array");
        $audit_action = "41";
        foreach ($getTrash as $trash) {
            if ($date >= $trash['addTime']) {
                $table_name = $trash['table_name'];
                // print_r($date." >= ".$trash['addTime']); //AND  CURDATE() >= ADDTIME(date_time, '48:0:0')    
                //permanent delete
                $where = array("id" => $trash['record_id']);
                $this->delete($trash['table_name'], $where);

                //update in trash bin
                $update = array("status" => '2');
                $where = array("id" => $trash['id']);
                $this->update("tbtrash_bin", $update, $where);

                //audit logs here
                $insert_audit_rec = array("user_id" => 0,
                    "audit_action" => $audit_action,
                    "table_name" => $table_name,
                    "record_id" => $trash['record_id'],
                    "date" => $date,
                    "ip" => $_SERVER["REMOTE_ADDR"],
                    "is_active" => 1);
                $this->insert("tbaudit_logs", $insert_audit_rec);
            }
            // echo "<br />";
        }
    }

    //generate union


    public function getFormRecorsDeleted() {
        $user_id = $this->auth['id'];
        $getFormRecorsDeletedSqlQuery = "SELECT * FROM tbtrash_bin tbtb WHERE tbtb.user_id = '" . $user_id . "' and tbtb.deletion_type != 2";
        $getFormRecorsDeleted = $this->query($getFormRecorsDeletedSqlQuery, "array");
        return $getFormRecorsDeleted;
    }

    public function getTrashRecordDataTableV2($search_value, $others, $json, $query_type) {
        $forms = $this->getFormRecorsDeleted();
        $strSqlData = "";
        $counter = 1;
        $search_value = $this->addslash_escape(mysql_escape_string($search_value));

        $limit = "";
        $order_by = " ORDER BY DateCreated ASC";
        $where = "";

        //formulate query attributes
        if ($json['start'] != "" AND $json['end'] != "") {
            $limit = " LIMIT " . $json['start'] . ", " . $json['end'] . "";
        }
        // if($json['condition']!=""){
        //     $where = " WHERE tbtb.deletion_type != 2 AND  ".$json['condition'];
        // }
        if ($json['column-sort'] != "" && $json['column-sort-type'] != "") {
            $column_sort = $json['column-sort'];
            if ($column_sort == "requestor") {
                $column_sort = "display_name";
            }
            $order_by = " ORDER BY " . $column_sort . " " . $json['column-sort-type'];
        }

        foreach ($forms as $key => $value) {
            # code...
            $form_tablename = $value['table_name'];
            $date_deleted = $value['date_time'];
            $form_id = $value['form_id'];
            $trash_id = $value['id'];
            $record_id = $value['record_id'];

            $whereQuery = "(
                                tbw.form_name LIKE '%" . $search_value . "%' OR 
                                form.TrackNo LIKE '%" . $search_value . "%' OR 
                                form.Status LIKE '%" . $search_value . "%' OR 
                                form.DateCreated LIKE '%" . $search_value . "%' OR
                                u.display_name LIKE '%" . $search_value . "%' OR
                                u.first_name LIKE '%" . $search_value . "%' OR
                                u.last_name LIKE '%" . $search_value . "%' OR
                                '". $date_deleted ."' LIKE '%". $search_value ."%'

                            ) ";

            $query_str .= "(SELECT 
                            form.DateCreated as DateCreated, 
                            form.TrackNo as TrackNo,
                            form.id as request_id, 
                            form.Status as Status,
                            form.Requestor as requestor_id,
                            form.Processor as processors_id,
                            u.first_name,u.last_name,u.middle_name,u.display_name, 
                              tbw.form_name, 
                              tbw.id as form_id , 
                              COALESCE(tbfc.category_name,'Others') as category_name,
                              '$date_deleted' as date_deleted,
                              '$form_tablename' as table_name,
                              '$trash_id' as trash_id
                        FROM `$form_tablename` form LEFT JOIN tbuser u ON u.id=form.requestor, tb_workspace tbw LEFT JOIN tbform_category tbfc ON tbw.category_id = tbfc.id
                        WHERE form.id = $record_id AND tbw.is_delete = 0 AND tbw.is_active = 1 AND  tbw.id = $form_id AND $whereQuery 
                         $others) ";
            if ($counter < count($forms)) {
                $query_str .= " UNION ALL ";
            }
            $counter++;
        }

        if (count($forms) > 0) {
            //has deleted records
            if ($query_type == "array") {
                $query_str = $query_str . $order_by . $limit;
            } else if ($query_type == "numrows") {
                $query_str = $query_str . $order_by;
            }
            $queryReturn = $this->query($query_str, $query_type);
        } else {
            //for no record found
            if ($query_type == "array") {
                $queryReturn = array();
            } else if ($query_type == "numrows") {
                $queryReturn = 0;
            }
        }
        return $queryReturn;
    }

    public function deletePermanent($arrayDelete) {
        $redis_cache = getRedisConnection();
        $user_id = $this->auth['id'];
        $fs = new functions;
        $date = $fs->currentDateTime();
        //delete permanent in trash bin
        foreach ($arrayDelete as $key => $value) {
            # delete in table
            $record_id = $value['record_id'];
            $table_name = $value['table_name'];
            $where = array("id" => $record_id);
            $this->delete($table_name, $where);

            #delete in trash bin
            // $where = array("record_id"=>$record_id,"table_name"=>$table_name);
            // $this->delete("tbtrash_bin",$where);
            //42 = delete permanent
            //audit logs here
            $insert_audit_rec = array("user_id" => $user_id,
                "audit_action" => 51,
                "table_name" => $table_name,
                "record_id" => $record_id,
                "date" => $date,
                "ip" => $_SERVER["REMOTE_ADDR"],
                "is_active" => 1);
            $this->insert("tbaudit_logs", $insert_audit_rec);
            if ($redis_cache) {
                //deletememcached
                $form_id = $value['$form_id'];
                $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
                $form_record_memcached = array("form_list", "form_count");
                $nav_memcached = array("leftbar_nav");
                $starred_memcached = array("starred_list");
                $request_memcached = array("form_access_" . $form_id,
                    "related_info_form_id_" . $form_id, "request_details_" . $form_id,
                    "request_access_" . $form_id, "request_author_access_" . $form_id,
                    "workspace_form_details_" . $form_id);


                $deleteMemecachedKeys = array_merge(
                        $form_record_memcached, $nav_memcached, $starred_memcached, $request_memcached
                );

                $fs->deleteMemcacheKeys($deleteMemecachedKeys);
                $fs->deleteMemcacheForm($request_record_memcacached, $form_id);
            }
        }
    }

}
