<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User_Level
 *
 * @author Jewel Tolentino
 */
class User_Level extends Formalistics {

    //put your code here

    public $description;
    public $is_active;

    public function __construct($db, $id) {
        $this->init($db, $id);
    }

    public function init($db, $id) {
        $this->db = $db;
        $redis_client = new Redis_Formalistics();
        if ($id) {
            $result = $redis_client->query("user_level_object_" . $id, "SELECT * FROM tbuser_level WHERE id = {$this->db->escape($id)}");

            $this->id = $result['id'];
            $this->description = $result['user_level'];
            $this->is_active = $result['is_active'];
        }
    }

}

?>
