<?php

/**
 * Description of UsersDAO
 *
 * @author Ervinne Sodusta
 */
class UsersDAO {

    const SUBSCRIBER_TYPE_USER           = 1;
    const SUBSCRIBER_TYPE_GLOBAL_GROUP   = 2;
    const SUBSCRIBER_TYPE_DEPARTMENT     = 3;
    const SUBSCRIBER_TYPE_POSITION       = 4;
    const SUBSCRIBER_TYPE_PERSONAL_GROUP = 5;

    /** @var Database */
    protected $database;

    public function __construct() {
        $this->database = new Database();
    }

    public function extractUserIdListFromSubscribers($subscribers) {

        $userIdList = array();

        foreach ($subscribers AS $subscriber) {
            switch ($subscriber["type"]) {
                case UsersDAO::SUBSCRIBER_TYPE_USER:
                    array_push($userIdList, $subscriber["value"]);
                    break;
                case UsersDAO::SUBSCRIBER_TYPE_PERSONAL_GROUP:
                    $userIdList = array_merge($userIdList, $this->getPersonalGroupMemberIdList($subscriber["value"]));
                    break;
            }
        }
        
        return $userIdList;
    }

    public function getPersonalGroupMemberIdList($personalGroupId) {
        $query      = "SELECT members FROM tbgroups WHERE id = {$personalGroupId}";
        $row = $this->database->query($query, "row");        
        
        return split(",", $row["members"]);
    }

}
