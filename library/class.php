<?php
// Calculating the redeemed points and dealer points
function redeemPoints($type,$totalDealerPoints,$ID){
    $db = new Database();
    $query = $db->query("SELECT * FROM tbredeem WHERE dealerCode='$ID'","array");
    $totalRedeemedPoints = 0;
    $expiredPoints = 0;
    foreach($query as $row)
    {
        if($row['product_type']==$type){
            $totalRedeemedPoints += $row['total'];
        }
    }
    if($totalRedeemedPoints>0){
        return $totalDealerPoints-$totalRedeemedPoints;
        //return 1;
    }else{
        return $totalDealerPoints-$totalRedeemedPoints;
    }
    
}
// For Separating the delimeters of the dealers points
function extractForms($str){
    $forms = array();
    $sfirst = explode("[-]",$str);
    foreach($sfirst as $r)
    {
        $first = explode("-:-",$r);
        foreach($first as $f){
            $second = explode('/:/',$f);
            foreach($second as $s){
                $third = explode('-|-',$s);
                foreach($third as $key => $t){
                    $col1 = ""; $col2= "";$form=array();
                    if(isset($third[$key])){
                        $col1 = $third[$key]; 
                    }
                    if(isset($third[($key+1)])){
                        $col2 = $third[($key+1)];
                    }
                    
                    if(!empty($col1) && !empty($col2)){
                        if(isset($forms[$col1])){
                            $forms[$col1] += $col2; 
                        }
                        else{
                            $forms[$col1] = $col2;
                        }
                    } 
                }
            }
        }
    }
    return $forms;
}
// For Pagination
function pagination($cur_page,$page,$per_page,$start,$previous_btn,$next_btn,$first_btn,$last_btn,$no_of_paginations,$finaldata,$count){
    $finaldata.="</tbody></table>";
        if ($cur_page >= 7) {
            $start_loop = $cur_page - 3;
            if ($no_of_paginations > $cur_page + 3)
                $end_loop = $cur_page + 3;
            else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                $start_loop = $no_of_paginations - 6;
                $end_loop = $no_of_paginations;
            } else {
                $end_loop = $no_of_paginations;
            }
        } else {
            $start_loop = 1;
            if ($no_of_paginations > 7)
                $end_loop = 7;
            else
                $end_loop = $no_of_paginations;
        }
        /* ----------------------------------------------------------------------------------------------------------- */
        if($count>$per_page){
            $finaldata .= "<div class='pagination'><ul>";
            
            // FOR ENABLING THE FIRST BUTTON
            if ($first_btn && $cur_page > 1) {
                $finaldata .= "<li p='1' class='active'>First</li>";
            } else if ($first_btn) {
                $finaldata .= "<li p='1' class='inactive'>First</li>";
            }
            
            // FOR ENABLING THE PREVIOUS BUTTON
            if ($previous_btn && $cur_page > 1) {
                $pre = $cur_page - 1;
                $finaldata .= "<li p='$pre' class='active'>Previous</li>";
            } else if ($previous_btn) {
                $finaldata .= "<li class='inactive'>Previous</li>";
            }
            for ($i = $start_loop; $i <= $end_loop; $i++) {
            
                if ($cur_page == $i)
                    $finaldata .= "<li p='$i' style='color:#ccc;background-color:#808080;' class='active'>{$i}</li>";
                else
                    $finaldata .= "<li p='$i' class='active'>{$i}</li>";
            }
            
            // TO ENABLE THE NEXT BUTTON
            if ($next_btn && $cur_page < $no_of_paginations) {
                $nex = $cur_page + 1;
                $finaldata .= "<li p='$nex' class='active'>Next</li>";
            } else if ($next_btn) {
                $finaldata .= "<li class='inactive'>Next</li>";
            }
            
            // TO ENABLE THE END BUTTON
            if ($last_btn && $cur_page < $no_of_paginations) {
                $finaldata .= "<li p='$no_of_paginations' class='active'>Last</li>";
            } else if ($last_btn) {
                $finaldata .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
            }
            $finaldata = $finaldata . "</ul>" .  $total_string . "</div>";  // Content for pagination
        }
        echo $finaldata;
}
// Validation of the fields
function checkstrname($string){
        //echo "Here's your: ".$string."<br/>";
        $string;
        $checking_consecutives="";
        $letters=0;
        $spaces=0;
        $special=0;
        $errors=0;
        $magkasunod=0;
        $bilangmagkasunod=0;
        
        for($i=0;$i<strlen($string);$i++)
        {
                if(substr($string,$i-1,1)=="." && substr($string,$i,1)==" "){
                        if($spaces<=3){
                                $spaces++;
                        //	echo "spaces<br/>";
                        }else{
                                $errors++;
                        //	echo "lagpas na sa limitation spaces<br/>";
                        }
                }else{
                        if(  preg_match( '/^[a-zA-Z]/',substr($string,$i,1) )>0  ){ //validate from a to Z
                                $letters=1;
                                $magkasunod=0;
                        //	echo "letters<br/>";
                        }elseif( substr($string,$i,1)==" " ) { //if space
                                if($letters==0){
                                        $magkasunod++;
                                        if($spaces<=3){
                                                $spaces++;
                                        //	echo "spaces<br/>";
                                        }else{
                                                $errors++;
                                        //	echo "lagpas na sa limitation spaces<br/>";
                                        }
                                }else{
                                        $letters=0;
                                        if($spaces<=3){
                                                $spaces++;
                                        //	echo "spaces<br/>";
                                        }else{
                                                $errors++;
                                        //	echo "lagpas na sa limitation spaces<br/>";
                                        }
                                }
                                
                        }elseif( substr($string,$i,1)=="-" || substr($string,$i,1)=="."   ){ // if - and .
                                if($letters==0){
                                        $magkasunod++;
                                        $special++;
                                        $checking_consecutives=substr($string,$i);
                                //	echo "specials<br/>";
                                }else{
                                        $letters=0;
                                        $special++;
                                        $checking_consecutives=substr($string,$i);
                                //	echo "specials<br/>";
                                }
                        }else{  // if any unknown character
                                if($letters==0){
                                        $magkasunod++;
                                        $errors++;
                                //	echo "unknown: ".substr($string,$i,1)."<br/>";
                                }else{
                                        $letters=0;
                                        $errors++;
                                //	echo "unknown: ".substr($string,$i,1)."<br/>";
                                }
                        }
                }
                if($magkasunod>0){
                        $errors++;
                        $bilangmagkasunod++;
                //	echo "magkasunod to!<br/>";
                }
        }
        //echo "Count Mali: $errors<br/>Count Magkasunod: $bilangmagkasunod";
        if($errors>0){
                return 0;
        }else{
                return 1;
        }
}