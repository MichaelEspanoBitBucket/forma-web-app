<?php
function STA_CLARA_LeaveRequest($selected_fields,$excluded_fields,$formatted_data,$db){
    //RequestID
    $myvar['RequestID'] = $_POST['ID'];


    $myvar['Requestor'] = $db->query("SELECT display_name FROM `tbuser` WHERE id = " . $_POST['Requestor'])[0]['display_name'];


    //Status
    $myvar['Status'] = $_POST['Status']?:"";


    //Today
    $myvar['Today'] = date('Y-m-d');


    //MaNum
    $myvar['MaNum'] = $_POST['MaNum']?:"";
    $myvar['MaNum'] = GivenIf(
        ($myvar['RequestID'] ==0 && $myvar['MaNum']== ''),
        GetAuth("FormAuthID"),
        $myvar['MaNum']
    );


    //EmployeeNumber
    $myvar['EmployeeNumber'] = $_POST['EmployeeNumber']?:"";
    $myvar['EmployeeNumber'] = GivenIf(
        $myvar['Status'] == '' && $myvar['EmployeeNumber'] == '',
        Lookup( 'Personnel Information 201' , 'EmployeeNumber' , 'ManNum' , $myvar['MaNum']),
        $myvar['EmployeeNumber']);


    $pi_201 = $db->query("SELECT
                            PersonnelName,
                            PersonnelPosition,
                            PersonnelDepartment,
                            reportingTo,
                            PersonnelGender,
                            PersonnelDateOfBirth,
                            DueLeave,
                            EmploymentStatus,
                            TrackNo
                        FROM 
                            15_tbl_personnelinfo
                        WHERE
                            EmployeeNumber = '". $myvar['EmployeeNumber'] ."'"
                        );


    //EmployeeYear
    $myvar['EmployeeYear'] = $_POST['EmployeeYear']?:"";
    $myvar['EmployeeYear'] = StrConcat($myvar['EmployeeNumber'],FormatDate($myvar['Today'],'Y')) ;


    //PeriodFrom
    $myvar['PeriodFrom'] = $_POST['PeriodFrom']?:"";


    //PeriodTo
    $myvar['PeriodTo'] = $_POST['PeriodTo']?:"";


    //DueLeave
    $myvar['DueLeave'] = $_POST['DueLeave']?:"";
    $myvar['DueLeave'] = $pi_201[0]['DueLeave'];


    //DaysAbsent
    $myvar['DaysAbsent'] = $_POST['DaysAbsent']?:"";
    $myvar['DaysAbsent'] = GivenIf(
                            $myvar['ApplicableTo'] == 'Paternity Leave' || $myvar['ApplicableTo'] == 'Maternity Leave',
                            DiffDate('day', ThisDate($myvar['PeriodTo']), $myvar['PeriodFrom']) +1,
                            (DiffDate('day', ThisDate($myvar['PeriodTo']), $myvar['PeriodFrom'])+1) - CountDayIf($myvar['PeriodFrom'],$myvar['PeriodTo'],'Sunday')
                        );


    //DateReportToWork
    $myvar['DateReportToWork'] = $_POST['DateReportToWork']?:"";
    $myvar['DateReportToWork'] = GivenIf($myvar['PeriodTo']=='',$myvar['Today'],FormatDate(AdjustDate('day',$myvar['PeriodTo'],+1),'Y-m-d')) ;


    //txtReason
    $myvar['txtReason'] = $_POST['txtReason']?:"";


    //Empstatus
    $myvar['Empstatus'] = $_POST['Empstatus']?:"";
    $myvar['Empstatus'] = $pi_201[0]['EmploymentStatus'];


    //Gender
    $myvar['Gender'] = $_POST['Gender']?:"";
    $myvar['Gender'] = $pi_201[0]['PersonnelGender'];


    //ReasonforDisapproval
    $myvar['ReasonforDisapproval'] = $_POST['ReasonforDisapproval']?:"";


    //txtRemarks
    $myvar['txtRemarks'] = $_POST['txtRemarks']?:"";


    $leaves = $db->query("SELECT 
        `15_tbl_SLMaintenance`.`Earned` AS SLEarned, `15_tbl_SLMaintenance`.`LeaveUsed` AS SLLeaveUsed, `15_tbl_SLMaintenance`.`Balance` AS SLBalance, `15_tbl_SLMaintenance`.`Entitlement` AS SLEntitlement, `15_tbl_SLMaintenance`.`TrackNo` AS SLTrackNo,
        `15_tbl_VLMaintenance`.`Earned` As VLEarned, `15_tbl_VLMaintenance`.`LeaveUsed` AS VLLeaveUsed, `15_tbl_VLMaintenance`.`Balance` AS VLBalance, `15_tbl_VLMaintenance`.`Entitlement` AS VLEntitlement, `15_tbl_VLMaintenance`.`TrackNo` As VLTrackNo,
        `15_tbl_MLMaintenance`.`Earned` AS MLEarned, `15_tbl_MLMaintenance`.`LeaveUsed` AS MLLeaveUsed, `15_tbl_MLMaintenance`.`Balance` AS MLBalance, `15_tbl_MLMaintenance`.`Entitlement` AS MLEntitlement, `15_tbl_MLMaintenance`.`TrackNo` AS MLTrackNo,
        `15_tbl_PLMaintenance`.`Earned` AS PLEarned, `15_tbl_PLMaintenance`.`LeaveUsed` as PLLeaveUsed, `15_tbl_PLMaintenance`.`Balance` as PLBalance, `15_tbl_PLMaintenance`.`Entitlement` as PLEntitlement, `15_tbl_PLMaintenance`.`TrackNo` AS PLTrackNo,
        `15_tbl_SILMaintenance`.`Earned` AS SILEarned, `15_tbl_SILMaintenance`.`LeaveUsed` AS SILLeaveUsed, `15_tbl_SILMaintenance`.`Balance` AS SILBalance, `15_tbl_SILMaintenance`.`Entitlement` AS SILEntitlement, `15_tbl_SILMaintenance`.`TrackNo` AS SILTrackNo,
        `15_tbl_DLMaintenance`.`Earned` AS DLEarned, `15_tbl_DLMaintenance`.`LeaveUsed` AS DLLeaveUsed, `15_tbl_DLMaintenance`.`Balance` AS DLBalance, `15_tbl_DLMaintenance`.`Entitlement` AS DLEntitlement, `15_tbl_DLMaintenance`.`TrackNo` AS DLTrackNo,
        `15_tbl_BLMaintenance`.`Earned` AS BLEarned, `15_tbl_BLMaintenance`.`LeaveUsed` AS BLLeaveUsed, `15_tbl_BLMaintenance`.`Balance` AS BLBalance, `15_tbl_BLMaintenance`.`Entitlement` AS BLEntitlement, `15_tbl_BLMaintenance`.`TrackNo` AS BLTrackNo,
        `15_tbl_BDLMaintenance`.`Earned` AS BDLEarned, `15_tbl_BDLMaintenance`.`LeaveUsed` AS BDLLeaveUsed, `15_tbl_BDLMaintenance`.`Balance` AS BDLBalance, `15_tbl_BDLMaintenance`.`Entitlement` AS BDLEntitlement, `15_tbl_BDLMaintenance`.`TrackNo` AS BDLTrackNo,
        `15_tbl_SOLMaintenance`.`Earned` AS SOLEarned, `15_tbl_SOLMaintenance`.`LeaveUsed` AS SOLLeaveUsed, `15_tbl_SOLMaintenance`.`Balance` AS SOLBalance, `15_tbl_SOLMaintenance`.`Entitlement` AS SOLEntitlement, `15_tbl_SOLMaintenance`.`TrackNo` AS SOLTrackNo,
        `15_tbl_OtherMaintenance`.`TrackNo` AS OTrackNo
    FROM 
        15_tbl_SLMaintenance,
        15_tbl_VLMaintenance,
        15_tbl_MLMaintenance,
        15_tbl_PLMaintenance,
        15_tbl_SILMaintenance,
        15_tbl_DLMaintenance,
        15_tbl_BLMaintenance,
        15_tbl_BDLMaintenance,
        15_tbl_SOLMaintenance,
        15_tbl_OtherMaintenance
    WHERE 
        `15_tbl_SLMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "' AND
        `15_tbl_VLMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "' AND
        `15_tbl_MLMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "' AND
        `15_tbl_PLMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "' AND
        `15_tbl_SILMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "' AND
        `15_tbl_DLMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "' AND
        `15_tbl_BLMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "' AND
        `15_tbl_BDLMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "' AND
        `15_tbl_SOLMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "' AND
        `15_tbl_OtherMaintenance`.`EmployeeYear` = '" . $myvar['EmployeeYear'] . "'"
    );


    //ApplicableTo
    $myvar['ApplicableTo'] = $_POST['ApplicableTo']?:"";


    //EntitlementoBeUsed
    $myvar['EntitlementoBeUsed'] = $_POST['EntitlementoBeUsed']?:"";
    $myvar['EntitlementoBeUsed'] = GivenIf(
                                        $myvar['ApplicableTo'] == 'Sick Leave' ,
                                        GivenIf(
                                            (($leaves[0]['SLEntitlement']/12)*6) >= $myvar['NoAvailableDaysVL'],
                                            (($leaves[0]['SLEntitlement']/12)*6) + $myvar['BalanceToDateVL'],
                                            ($leaves[0]['SLEntitlement']-($myvar['BalanceToDateVL']+$myvar['AppliedLeavesVL']))+$myvar['BalanceToDateVL'] 
                                        ),
                                        GivenIf($myvar['ApplicableTo'] == 'Vacation Leave',
                                            GivenIf(
                                                (($leaves[0]['VLEntitlement']/12)*6) >= $myvar['NoAvailableDaysVL'],
                                                (($leaves[0]['SLEntitlement']/12)*6) + $myvar['BalanceToDateVL'],
                                                ($leaves[0]['VLEntitlement']-($myvar['BalanceToDateVL']+$myvar['AppliedLeavesVL']))+$myvar['BalanceToDateVL']  ),
                                            GivenIf(
                                                $myvar['ApplicableTo'] == 'Maternity Leave',
                                                $leaves[0]['MLEntitlement']-$myvar['AppliedLeavesVL'],
                                                GivenIf(
                                                    $myvar['ApplicableTo'] == 'Maternity Leave',
                                                    $leaves[0]['MLEntitlement']-$myvar['AppliedLeavesVL'],
                                                    GivenIf(
                                                        $myvar['ApplicableTo'] == 'Paternity Leave',
                                                        $leaves[0]['PLEntitlement']-$myvar['AppliedLeavesVL'],
                                                        GivenIf(
                                                            $myvar['ApplicableTo'] == 'SIL',
                                                            $leaves[0]['SILEntitlement']-$myvar['AppliedLeavesVL'],
                                                            GivenIf(
                                                                $myvar['ApplicableTo'] == 'Due Leave',
                                                                $leaves[0]['DLEntitlement']-$myvar['AppliedLeavesVL'],
                                                                GivenIf(
                                                                    $myvar['ApplicableTo'] == 'Bereavement Leave',
                                                                    $leaves[0]['BLEntitlement']-$myvar['AppliedLeavesVL'],
                                                                    GivenIf(
                                                                        $myvar['ApplicableTo'] == 'Birthday Leave',
                                                                        $leaves[0]['BDLEntitlement']-$myvar['AppliedLeavesVL'],
                                                                        GivenIf(
                                                                            $myvar['ApplicableTo'] == 'Solo Parent',
                                                                            $leaves[0]['SOLEntitlement']-$myvar['AppliedLeavesVL'],
                                                                            StrConcat('0')
                                                                        )
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    );


    //NoAvailableDaysVL
    $myvar['NoAvailableDaysVL'] = $_POST['NoAvailableDaysVL']?:"";
    $myvar['NoAvailableDaysVL'] = GivenIf(
                                    $myvar['ApplicableTo'] == 'Sick Leave',
                                    $leaves[0]['SLEarned'],
                                    GivenIf(
                                        $myvar['ApplicableTo'] == 'Vacation Leave',
                                        $leaves[0]['VLEarned'],
                                        GivenIf(
                                            $myvar['ApplicableTo'] == 'Maternity Leave',
                                            $leaves[0]['MLEarned'],
                                            GivenIf(
                                                $myvar['ApplicableTo'] == 'Paternity Leave',
                                                $leaves[0]['PLEarned'],
                                                GivenIf(
                                                    $myvar['ApplicableTo'] == 'SIL',
                                                    $leaves[0]['SILEarned'],
                                                    GivenIf(
                                                        $myvar['ApplicableTo'] == 'Due Leave',
                                                        $leaves[0]['DLEarned'],
                                                        GivenIf(
                                                            $myvar['ApplicableTo'] == 'Bereavement Leave',
                                                            $leaves[0]['BLEarned'],
                                                            GivenIf(
                                                                $myvar['ApplicableTo'] == 'Birthday Leave',
                                                                $leaves[0]['BDLEarned'],
                                                                GivenIf(
                                                                    $myvar['ApplicableTo'] == 'Solo Parent',
                                                                    $leaves[0]['SOLEarned'],
                                                                    StrConcat('0')
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                );


    //AppliedLeavesVL
    $myvar['AppliedLeavesVL'] = $_POST['AppliedLeavesVL']?:"";
    $myvar['AppliedLeavesVL'] = GivenIf(
                                    $myvar['ApplicableTo'] == 'Sick Leave',
                                    $leaves[0]['SLLeaveUsed'],
                                    GivenIf(
                                        $myvar['ApplicableTo'] == 'Vacation Leave',
                                        $leaves[0]['VLLeaveUsed'],
                                        GivenIf(
                                            $myvar['ApplicableTo'] == 'Maternity Leave',
                                            $leaves[0]['MLLeaveUsed'],
                                            GivenIf(
                                                $myvar['ApplicableTo'] == 'Paternity Leave',
                                                $leaves[0]['PLLeaveUsed'],
                                                GivenIf(
                                                    $myvar['ApplicableTo'] == 'SIL',
                                                    $leaves[0]['SILLeaveUsed'],
                                                    GivenIf(
                                                        $myvar['ApplicableTo'] == 'Due Leave',
                                                        $leaves[0]['DLLeaveUsed'],
                                                        GivenIf(
                                                            $myvar['ApplicableTo'] == 'Bereavement Leave',
                                                            $leaves[0]['BLLeaveUsed'],
                                                            GivenIf(
                                                                $myvar['ApplicableTo'] == 'Birthday Leave',
                                                                $leaves[0]['BDLLeaveUsed'],
                                                                StrConcat('0')
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                );


    //BalanceToDateVL
    $myvar['BalanceToDateVL'] = $_POST['BalanceToDateVL']?:"";
    $myvar['BalanceToDateVL'] = GivenIf(
                                    $myvar['ApplicableTo'] == 'Sick Leave',
                                    $leaves[0]['SLBalance'],
                                    GivenIf(
                                        $myvar['ApplicableTo'] == 'Vacation Leave',
                                        $leaves[0]['VLBalance'],
                                        GivenIf(
                                            $myvar['ApplicableTo'] == 'Maternity Leave',
                                            $leaves[0]['MLBalance'],
                                            GivenIf(
                                                $myvar['ApplicableTo'] == 'Paternity Leave',
                                                $leaves[0]['PLBalance'],
                                                GivenIf(
                                                    $myvar['ApplicableTo'] == 'SIL',
                                                    LookupWhere('SIL Maintenance','Balance','EmployeeID','=',$myvar['EmployeeNumber'],'Year','=',FormatDate($myvar['Today'],'Y'),'SILEndDate','<=',FormatDate($myvar['Today'],'Y')),
                                                    GivenIf(
                                                        $myvar['ApplicableTo'] == 'Due Leave',
                                                        $leaves[0]['DLBalance'],
                                                        GivenIf(
                                                            $myvar['ApplicableTo'] == 'Bereavement Leave',
                                                            $leaves[0]['BLBalance'],
                                                            GivenIf(
                                                                $myvar['ApplicableTo'] == 'Birthday Leave',
                                                                $leaves[0]['BDLBalance'],
                                                                GivenIf(
                                                                    $myvar['ApplicableTo'] == 'Solo Parent',
                                                                    $leaves[0]['SOLBalance'],
                                                                    StrConcat('0')
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                );


    //ThisRequestVL
    $myvar['ThisRequestVL'] = $_POST['ThisRequestVL']?:"";
    $myvar['ThisRequestVL'] = GetRound(GivenIf($myvar['txtRemarks'] == 'With Pay', GivenIf($myvar['DaysAbsent'] <= $myvar['EntitlementoBeUsed'] || $myvar['ApplicableTo'] == 'Others',$myvar['DaysAbsent'],$myvar['EntitlementoBeUsed']),0),2) ;


    //NetBalanceVL
    $myvar['NetBalanceVL'] = $_POST['NetBalanceVL']?:"";
    $myvar['NetBalanceVL'] = GivenIf($myvar['ApplicableTo'] != 'Others',GetRound($myvar['BalanceToDateVL'] - $myvar['ThisRequestVL'],2),0) ;


    //WithoutPay
    $myvar['WithoutPay'] = $_POST['WithoutPay']?:"";
    $myvar['WithoutPay'] = GetRound( GivenIf($myvar['txtRemarks'] == 'With Pay', GivenIf($myvar['DaysAbsent'] <= $myvar['EntitlementoBeUsed']|| $myvar['ApplicableTo'] == 'Others',0,$myvar['DaysAbsent']-$myvar['EntitlementoBeUsed']),$myvar['DaysAbsent']),2) ;


    //NoAvailableDaysSIL
    $myvar['NoAvailableDaysSIL'] = $_POST['NoAvailableDaysSIL']?:"";


    //AppliedLeavesSIL
    $myvar['AppliedLeavesSIL'] = $_POST['AppliedLeavesSIL']?:"";


    //BalanceToDateSIL
    $myvar['BalanceToDateSIL'] = $_POST['BalanceToDateSIL']?:"";


    //ThisRequestSIL
    $myvar['ThisRequestSIL'] = $_POST['ThisRequestSIL']?:"";


    //NetBalanceSIL
    $myvar['NetBalanceSIL'] = $_POST['NetBalanceSIL']?:"";


    //NoAvailableDaysSL
    $myvar['NoAvailableDaysSL'] = $_POST['NoAvailableDaysSL']?:"";


    //AppliedLeavesSL
    $myvar['AppliedLeavesSL'] = $_POST['AppliedLeavesSL']?:"";


    //BalanceToDateSL
    $myvar['BalanceToDateSL'] = $_POST['BalanceToDateSL']?:"";


    //ThisRequestSL
    $myvar['ThisRequestSL'] = $_POST['ThisRequestSL']?:"";


    //NetBalanceSL
    $myvar['NetBalanceSL'] = $_POST['NetBalanceSL']?:"";


    //txtName
    $myvar['txtName'] = $_POST['txtName']?:"";
    $myvar['txtName'] = $pi_201[0]['PersonnelName'];


    //Department
    $myvar['Department'] = $_POST['Department']?:"";
    $myvar['Department'] = $pi_201[0]['PersonnelDepartment'];


    //PositionTitle
    $myvar['PositionTitle'] = $_POST['PositionTitle']?:"";
    $myvar['PositionTitle'] = $pi_201[0]['PersonnelPosition'];


    //Requestedby
    $myvar['Requestedby'] = $_POST['Requestedby']?:"";
    $myvar['Requestedby'] = $myvar['Requestor'];


    //DeptHead
    $myvar['DeptHead'] = $_POST['DeptHead']?:"";
    $myvar['DeptHead'] = $pi_201[0]['reportingTo'];


    //HrManager
    $myvar['HrManager'] = $_POST['HrManager']?:"";


    //HrStaff
    $myvar['HrStaff'] = $_POST['HrStaff']?:"";


    //EmployeeTrackNo
    $myvar['EmployeeTrackNo'] = $_POST['EmployeeTrackNo']?:"";
    $myvar['EmployeeTrackNo'] = $pi_201[0]['TrackNo'];


    //MCattached
    $myvar['MCattached'] = $_POST['MCattached']?:"";


    //Birthday
    $myvar['Birthday'] = $_POST['Birthday']?:"";
    $myvar['Birthday'] = $pi_201[0]['PersonnelDateOfBirth'];


    //VLdays1
    $myvar['VLdays1'] = $_POST['VLdays1']?:"";


    //VL2
    $myvar['VL2'] = $_POST['VL2']?:"";


    //LastYearID
    $myvar['LastYearID'] = $_POST['LastYearID']?:"";
    $myvar['LastYearID'] = StrConcat($myvar['EmployeeNumber'],FormatDate(AdjustDate('year',$myvar['Today'],-1),'Y')) ;


    //SLMTrackno
    $myvar['SLMTrackno'] = $_POST['SLMTrackno']?:"";
    $myvar['SLMTrackno'] = $leaves[0]['SLTrackNo'];


    //VLMTrackno
    $myvar['VLMTrackno'] = $_POST['VLMTrackno']?:"";
    $myvar['VLMTrackno'] = $leaves[0]['VLTrackNo'];


    //MLMTrackno
    $myvar['MLMTrackno'] = $_POST['MLMTrackno']?:"";
    $myvar['MLMTrackno'] = $leaves[0]['MLTrackNo'];


    //PLMTrackno
    $myvar['PLMTrackno'] = $_POST['PLMTrackno']?:"";
    $myvar['PLMTrackno'] = $leaves[0]['PLTrackNo'];


    //BLMTrackno
    $myvar['BLMTrackno'] = $_POST['BLMTrackno']?:"";
    $myvar['BLMTrackno'] = $leaves[0]['BLTrackNo'];


    //DLMTrackno
    $myvar['DLMTrackno'] = $_POST['DLMTrackno']?:"";
    $myvar['DLMTrackno'] = $leaves[0]['DLTrackNo'];


    //SILMTrackno
    $myvar['SILMTrackno'] = $_POST['SILMTrackno']?:"";
    $myvar['SILMTrackno'] = $leaves[0]['SILTrackNo'];


    //BDLMTrackno
    $myvar['BDLMTrackno'] = $_POST['BDLMTrackno']?:"";
    $myvar['BDLMTrackno'] = $leaves[0]['BDLTrackNo'];


    $last_year_leaves = $db->query("SELECT 
        `15_tbl_SLMaintenance`.`Earned` AS SLEarned, `15_tbl_SLMaintenance`.`LeaveUsed` AS SLLeaveUsed, `15_tbl_SLMaintenance`.`Balance` AS SLBalance, `15_tbl_SLMaintenance`.`TrackNo` AS SLTrackNo,
        `15_tbl_VLMaintenance`.`Earned` As VLEarned, `15_tbl_VLMaintenance`.`LeaveUsed` AS VLLeaveUsed, `15_tbl_VLMaintenance`.`Balance` AS VLBalance, `15_tbl_VLMaintenance`.`TrackNo` As VLTrackNo
    FROM 
        15_tbl_SLMaintenance,
        15_tbl_VLMaintenance
    WHERE 
        `15_tbl_SLMaintenance`.`EmployeeYear` = '" . $myvar['LastYearID'] . "' AND
        `15_tbl_VLMaintenance`.`EmployeeYear` = '" . $myvar['LastYearID'] . "'"
    );


    //BalanceLastYr
    $myvar['BalanceLastYr'] = $_POST['BalanceLastYr']?:"";
    $myvar['BalanceLastYr'] = GivenIf(
                                    FormatDate($myvar['Today'],'Y-m') >= StrConcat(FormatDate($myvar['Today'],'Y'),'-03'),
                                    0,
                                    GivenIf(
                                        $myvar['ApplicableTo'] == 'Sick Leave',
                                        $last_year_leaves[0]['SLEarned'],
                                        GivenIf(
                                            $myvar['ApplicableTo'] == 'Vacation Leave',
                                            $last_year_leaves[0]['VLEarned'],
                                            StrConcat('0')
                                        )
                                    )
                                );


    //SLMTracknoLY
    $myvar['SLMTracknoLY'] = $_POST['SLMTracknoLY']?:"";
    $myvar['SLMTracknoLY'] = $last_year_leaves[0]['SLTrackNo'];


    //VLMTracknoLY
    $myvar['VLMTracknoLY'] = $_POST['VLMTracknoLY']?:"";
    $myvar['VLMTracknoLY'] = $last_year_leaves[0]['VLTrackNo'];


    //LateFilling
    $myvar['LateFilling'] = $_POST['LateFilling']?:"";
    $myvar['LateFilling'] = GivenIf(((DiffDate('day',$myvar['PeriodFrom'],$myvar['Today'])-CountDayIf($myvar['PeriodFrom'],$myvar['Today'],'Sunday')) < $myvar['VLdays1'])||($myvar['DaysAbsent'] >7 &&((DiffDate('day',$myvar['PeriodFrom'],$myvar['Today'])-CountDayIf($myvar['PeriodFrom'],$myvar['Today'],'Sunday')) < $myvar['VL2'])),StrConcat('Late Filing'),StrConcat('')) ;


    //SOLMTrackno
    $myvar['SOLMTrackno'] = $_POST['SOLMTrackno']?:"";
    $myvar['SOLMTrackno'] = $leaves[0]['SOLTrackNo'];


    //OMTrackno
    $myvar['OMTrackno'] = $_POST['OMTrackno']?:"";
    $myvar['OMTrackno'] = $leaves[0]['OTrackNo'];


    //PLCount
    $myvar['PLCount'] = $_POST['PLCount']?:"";
    $myvar['PLCount'] = Total('Application for Leave','ThisRequestVL','Status','=',StrConcat('Approved/Posted'),'ApplicableTo','=',StrConcat('Paternity Leave'),'EmployeeNumber','=',$myvar['EmployeeNumber']) ;


    //MLCount
    $myvar['MLCount'] = $_POST['MLCount']?:"";
    $myvar['MLCount'] = Total('Application for Leave','ApplicableTo','Status','=',StrConcat('Approved/Posted'),'ApplicableTo','=',StrConcat('Maternity Leave'),'EmployeeNumber','=',$myvar['EmployeeNumber']) ;
 
    //test
    $myvar['test'] = $_POST['test']?:"";
    $myvar['test'] = $myvar['BalanceToDateVL'] ;


    //test2
    $myvar['test2'] = $_POST['test2']?:"";
    $myvar['test2'] = Total('SIL Maintenance','Balance','EmployeeID','=',$myvar['EmployeeNumber'],'SILStartDate','>=',FormatDate($myvar['Today'],'Y'),'SILEndDate','<=',FormatDate($myvar['Today'],'Y')) ;
    

    //OLD VERSION BELOW
    $formatted_data["computed_value"] = array(
        "DaysAbsent"=>$myvar['DaysAbsent'],
        "DateReportToWork"=>$myvar['DateReportToWork'],
        "ApplicableTo"=>$myvar['ApplicableTo'],
        
        "NoAvailableDaysVL"=>$myvar['NoAvailableDaysVL'],
        "AppliedLeavesVL"=>$myvar['AppliedLeavesVL'],
        "BalanceToDateVL"=>$myvar['BalanceToDateVL'],
       
        "ThisRequestVL"=>$myvar['ThisRequestVL'],
        "NetBalanceVL"=>$myvar['NetBalanceVL'],
        "WithoutPay"=>$myvar['WithoutPay'],
        "MaNum"=>$myvar['MaNum'],
        "txtName"=>$myvar['txtName'],
        "Department"=>$myvar['Department'],
        "PositionTitle"=>$pi_201[0]['PersonnelPosition'],
        "Requestedby"=>$myvar['Requestor'],
        "DeptHead"=>$myvar['DeptHead'],
        "EmployeeTrackNo"=>$myvar['EmployeeTrackNo'],
        "Gender"=>$pi_201[0]['PersonnelGender'],
        "Birthday"=>$pi_201[0]['PersonnelDateOfBirth'],
        "DueLeave"=>$myvar['DueLeave'],
        "LastYearID"=>$myvar['LastYearID'],
        "EmployeeYear"=>$myvar['EmployeeYear'],
        "SLMTrackno"=>$myvar['SLMTrackno'],
        "VLMTrackno"=>$myvar['VLMTrackno'],
        "MLMTrackno"=>$myvar['MLMTrackno'],
        "PLMTrackno"=>$myvar['PLMTrackno'],
        "BLMTrackno"=>$myvar['BLMTrackno'],
        "DLMTrackno"=>$myvar['DLMTrackno'],
        "SILMTrackno"=>$myvar['SILMTrackno'],
        "BDLMTrackno"=>$myvar['BDLMTrackno'],
        "BalanceLastYr"=>$myvar['BalanceLastYr'],
        "SLMTracknoLY"=>$myvar['SLMTracknoLY'],
        "VLMTracknoLY"=>$myvar['VLMTracknoLY'],
        "EmployeeNumber"=>$myvar['EmployeeNumber'],
        "LateFilling"=>$myvar['LateFilling'],
        "SOLMTrackno"=>$myvar['SOLMTrackno'],
        "OMTrackno"=>$myvar['OMTrackno'],
        "Empstatus"=>$myvar['Empstatus'],
        "EntitlementoBeUsed"=>$myvar['EntitlementoBeUsed'],
        "PLCount"=>$myvar['PLCount'],
        "MLCount"=>$myvar['MLCount'],
        "test"=>$myvar['BalanceToDateVL'],
        "test2"=>$myvar['test2'],
    );

    $formatted_data["visibility"]["ReasonforDisapproval"] = $myvar['Status'] != '' ;
    $formatted_data["visibility"]["txtRemarks"] = $myvar['Status'] =='For Notification' || $myvar['Status'] == 'For Posting' ;
    $formatted_data["visibility"]["MaNum"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["EmployeeTrackNo"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["MCattached"] = $myvar['ApplicableTo'] == 'Paternity Leave' ;
    $formatted_data["visibility"]["Gender"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["Birthday"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["VLdays1"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["VL2"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["DueLeave"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["LastYearID"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["EmployeeYear"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["SLMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["VLMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["MLMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["PLMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["BLMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["DLMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["SILMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["BDLMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["BalanceLastYr"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["SLMTracknoLY"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["VLMTracknoLY"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["SOLMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["OMTrackno"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["Empstatus"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["EntitlementoBeUsed"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["PLCount"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["MLCount"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["test"] = $myvar['Status'] == 'false';
    $formatted_data["visibility"]["test2"] = $myvar['Status'] == 'false';


    $formatted_data['change_list_computed_value']['ApplicableTo'] = GivenIf(
        $myvar['Gender'] == 'Female' && $myvar['DueLeave'] == 'NO' && $myvar['Empstatus'] == 'Regular',
        ['Vacation Leave','Sick Leave','Maternity Leave','Bereavement Leave','Birthday Leave','Solo Parent','Others'],
        GivenIf(
            $myvar['Gender'] == 'Male' && $myvar['DueLeave'] == 'NO' && $myvar['Empstatus'] == 'Regular',
            ['Vacation Leave','Sick Leave','Paternity Leave','Bereavement Leave','Birthday Leave','Others'],
            GivenIf(
                $myvar['Gender'] == 'Female' && $myvar['DueLeave'] == 'YES' && $myvar['Empstatus'] == 'Regular',
                ['Vacation Leave','Sick Leave','Maternity Leave','Bereavement Leave','Birthday Leave','Due Leave','Solo Parent','Others'],
                GivenIf(
                    $myvar['Gender'] == 'Male' && $myvar['DueLeave'] == 'YES'&& $myvar['Empstatus'] == 'Regular',
                    ['Vacation Leave','Sick Leave','Paternity Leave','Bereavement Leave','Birthday Leave','Due Leave','Others'],
                    GivenIf(
                        $myvar['Gender'] == 'Male' && $myvar['DueLeave'] == 'YES'&& $myvar['Empstatus'] == 'Project Employee',
                        ['SIL','Due Leave','Paternity Leave'],
                        GivenIf(
                            $myvar['Gender'] == 'Female' && $myvar['DueLeave'] == 'YES'&& $myvar['Empstatus'] == 'Project Employee',
                            ['SIL','Due Leave','Maternity Leave'],
                            GivenIf(
                                $myvar['Gender'] == 'Male' && $myvar['DueLeave'] == 'NO'&& $myvar['Empstatus'] == 'Project Employee',
                                ['SIL','Paternity Leave'],
                                GivenIf(
                                    $myvar['Gender'] == 'Female' && $myvar['DueLeave'] == 'NO'&& $myvar['Empstatus'] == 'Project Employee',
                                    ['SIL','Maternity Leave'],
                                    StrConcat('This is not included in the list')
                                )
                            )
                        )
                    )
                )
            )
        ) 
    );

    $print_debugger .= "\$myvar = ".json_encode($myvar);
    $print_debugger .= "\n\n";
    $print_debugger .= "\$personnel_info_SCGEmpID = ".json_encode($personnel_info_SCGEmpID);
    
    if( ( $selected_fields && count($selected_fields)>0 )&& ( $excluded_fields && count($excluded_fields)>0 ) ){
        foreach ($selected_fields as $key => $value) {
            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];
        }
        foreach ($excluded_fields as $key => $value) {
            unset($new_formatted_data["computed_value"]["". $value .""]);
        }
        $new_formatted_data["__DEBUG"] = $print_debugger;
        return $new_formatted_data;
    }else if($selected_fields && count($selected_fields)>0){
        foreach ($selected_fields as $key => $value) {
            // echo $formatted_data["". $value .""];
            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];
        }
        $new_formatted_data["__DEBUG"] = $print_debugger;
        return $new_formatted_data;
    }else if($excluded_fields && count($excluded_fields)>0){
        foreach ($excluded_fields as $key => $value) {
            unset($formatted_data["computed_value"]["". $value .""]);
        }
        $formatted_data["__DEBUG"] = $print_debugger;
        //print_r($formatted_data);
        return $formatted_data;
    }else{
        return $formatted_data;
    }
    
}
?>