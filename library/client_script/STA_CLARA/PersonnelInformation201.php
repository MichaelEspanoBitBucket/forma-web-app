<?php
function STA_CLARA_PersonnelInformation201($selected_fields,$excluded_fields,$formatted_data,$db){
	$myvar['RequestID'] = $_POST['ID']?:"";
    $myvar['Today'] = date('Y-m-d');
    $myvar['EmailAddress'] = $_POST['EmailAddress']?:"";
    $myvar['Surname'] = $_POST['Surname']?:"";
    $myvar['FirstName'] = $_POST['FirstName']?:"";
    $myvar['MiddleName'] = $_POST['MiddleName']?:"";
    $myvar['PersonnelDateOfBirth'] = $_POST['PersonnelDateOfBirth']?:"";
    $myvar['PersonnelAge'] = $_POST['PersonnelAge']?:"";
    $myvar['PersonnelGender'] = $_POST['PersonnelGender']?:"";
    $myvar['CivilStatus'] = $_POST['CivilStatus']?:"";
    $myvar['PresentAddress'] = $_POST['PresentAddress']?:"";
    $myvar['weight'] = $_POST['weight']?:"";
    $myvar['height'] = $_POST['height']?:"";
    $myvar['religion'] = $_POST['religion']?:"";
    
    $myvar['SSSNumber'] = $_POST['SSSNumber']?:"";
    $myvar['TinNum'] = $_POST['TinNum']?:"";
    $myvar['HDMFnumber'] = $_POST['HDMFnumber']?:"";
    $myvar['NHIPnumber'] = $_POST['NHIPnumber']?:"";
    $myvar['Drivers_License_Number'] = $_POST['Drivers_License_Number']?:"";
    $myvar['IntellicareNum'] = $_POST['IntellicareNum']?:"";
    $myvar['AccountNumber'] = $_POST['AccountNumber']?:"";
    $myvar['PRCNumber'] = $_POST['PRCNumber']?:"";
    $myvar['Passport'] = $_POST['Passport']?:"";
    $myvar['PRCValidity'] = $_POST['PRCValidity']?:"";
    $myvar['PassportValidity'] = $_POST['PassportValidity']?:"";
    $myvar['PagibigNumber'] = $_POST['PagibigNumber']?:"";
    $myvar['MIDNo'] = $_POST['MIDNo']?:"";
    
    
	
    
	
    $myvar['Elementary'] = $_POST['Elementary']?:"";
    $myvar['FromElem'] = $_POST['FromElem']?:"";
    $myvar['ToElem'] = $_POST['ToElem']?:"";
    $myvar['ElementaryLYC'] = $_POST['ElementaryLYC']?:"";
    $myvar['ElementaryDYG'] = $_POST['ElementaryDYG']?:"";
    $myvar['ElementaryDegree'] = $_POST['ElementaryDegree']?:"";
    $myvar['HighSchool'] = $_POST['HighSchool']?:"";
    $myvar['fromHS'] = $_POST['fromHS']?:"";
    $myvar['ToHS'] = $_POST['ToHS']?:"";
    $myvar['HighSchoolLYC'] = $_POST['HighSchoolLYC']?:"";
    $myvar['HighSchoolDYG'] = $_POST['HighSchoolDYG']?:"";
    $myvar['HighSchoolDegree'] = $_POST['HighSchoolDegree']?:"";
    $myvar['College'] = $_POST['College']?:"";
    $myvar['ToCollege'] = $_POST['ToCollege']?:"";
    $myvar['fromCollege'] = $_POST['fromCollege']?:"";
    $myvar['CollegeLYC'] = $_POST['CollegeLYC']?:"";
    $myvar['CollegeDYG'] = $_POST['CollegeDYG']?:"";
    $myvar['CollegeDegree'] = $_POST['CollegeDegree']?:"";
    $myvar['OtherEdu'] = $_POST['OtherEdu']?:"";
    
    $myvar['fromOther'] = $_POST['fromOther']?:"";
    $myvar['toOther'] = $_POST['toOther']?:"";
    $myvar['OthersLYC'] = $_POST['OthersLYC']?:"";
    $myvar['OtherDYG'] = $_POST['OtherDYG']?:"";
    $myvar['OtherDegree'] = $_POST['OtherDegree']?:"";
    $myvar['AcademicHonors'] = $_POST['AcademicHonors']?:"";
    
    $myvar['FatherName'] = $_POST['FatherName']?:"";
    $myvar['MotherName'] = $_POST['MotherName']?:"";
    $myvar['ParentsAddress'] = $_POST['ParentsAddress']?:"";
    $myvar['MotherOccupation'] = $_POST['MotherOccupation']?:"";
    $myvar['FatherOccupation'] = $_POST['FatherOccupation']?:"";
    $myvar['MotherEmployer'] = $_POST['MotherEmployer']?:"";
    $myvar['FatherEmployer'] = $_POST['FatherEmployer']?:"";
    $myvar['EmergencyContact'] = $_POST['EmergencyContact']?:"";
    $myvar['ContactAddress'] = $_POST['ContactAddress']?:"";
    
    $myvar['CRName1'] = $_POST['CRName1']?:"";
    $myvar['CRCN1'] = $_POST['CRCN1']?:"";
    $myvar['CRStatus1'] = $_POST['CRStatus1']?:"";
    $myvar['CRRemarks1'] = $_POST['CRRemarks1']?:"";
    $myvar['CRName2'] = $_POST['CRName2']?:"";
    $myvar['CRCN2'] = $_POST['CRCN2']?:"";
    $myvar['CRStatus2'] = $_POST['CRStatus2']?:"";
    $myvar['CRReamarks2'] = $_POST['CRReamarks2']?:"";
    $myvar['CRName3'] = $_POST['CRName3']?:"";
    $myvar['CRCN3'] = $_POST['CRCN3']?:"";
    $myvar['CRStatus3'] = $_POST['CRStatus3']?:"";
    $myvar['CRRemarks3'] = $_POST['CRRemarks3']?:"";
    
    $myvar['PersonnelName'] = $_POST['PersonnelName']?:"";
    $myvar['Salutation'] = $_POST['Salutation']?:"";
    $myvar['SpouseName'] = $_POST['SpouseName']?:"";
    $myvar['SpouseEmployer'] = $_POST['SpouseEmployer']?:"";
    $myvar['PermanentAddress'] = $_POST['PermanentAddress']?:"";
    $myvar['telepermanent'] = $_POST['telepermanent']?:"";
    $myvar['telepresent'] = $_POST['telepresent']?:"";
    $myvar['BirthPlace'] = $_POST['BirthPlace']?:"";
    $myvar['extensionName'] = $_POST['extensionName']?:"";
    $myvar['middleInitial'] = $_POST['middleInitial']?:"";
    $myvar['zipCode'] = $_POST['zipCode']?:"";
    $myvar['EmploymentStatusSpouse'] = $_POST['EmploymentStatusSpouse']?:"";
    $myvar['PersonnelStat'] = $_POST['PersonnelStat']?:"";
    $myvar['PersonnelDateHired'] = $_POST['PersonnelDateHired']?:"";
    $myvar['jobDescription'] = $_POST['jobDescription']?:"";
    $myvar['PersonnelPosition'] = $_POST['PersonnelPosition']?:"";
    $myvar['PositionCategory'] = $_POST['PositionCategory']?:"";
    $myvar['TIPosition'] = $_POST['TIPosition']?:"";
    $myvar['Project'] = $_POST['Project']?:"";
    $myvar['DateRegular'] = $_POST['DateRegular']?:"";
    $myvar['EndofCOntractDate'] = $_POST['EndofCOntractDate']?:"";
    $myvar['ProbiMonths'] = $_POST['ProbiMonths']?:"";
    $myvar['DateHired2'] = $_POST['DateHired2']?:"";
    $myvar['reportingTo'] = $_POST['reportingTo']?:"";
    $myvar['WorkLocation'] = $_POST['WorkLocation']?:"";
    $myvar['PositionLevel'] = $_POST['PositionLevel']?:"";
    $myvar['StaffType'] = $_POST['StaffType']?:"";
    $myvar['EmploymentStatus'] = $_POST['EmploymentStatus']?:"";
	
    
	$myvar['positionNumber'] = $_POST['positionNumber']?:"";
    $myvar['Section'] = $_POST['Section']?:"";
    $myvar['Section'] = Lookup( 'Position Information Request' , 'Section' , 'TrackNo' , $myvar['positionNumber']);
    $myvar['PersonnelDepartment'] = $_POST['PersonnelDepartment']?:"";
    $myvar['Computation'] = $_POST['Computation']?:"";
    $myvar['NBIClearance'] = $_POST['NBIClearance']?:"";
    $myvar['nbicelarance'] = $_POST['nbicelarance']?:"";
    $myvar['BirthCertificate'] = $_POST['BirthCertificate']?:"";
    $myvar['bcert'] = $_POST['bcert']?:"";
    $myvar['PoliceClearance'] = $_POST['PoliceClearance']?:"";
    $myvar['Policelearance'] = $_POST['Policelearance']?:"";
    $myvar['MarriageClearance'] = $_POST['MarriageClearance']?:"";
    $myvar['mcert'] = $_POST['mcert']?:"";
    $myvar['BarangayClearance'] = $_POST['BarangayClearance']?:"";
    $myvar['brgyclearnce'] = $_POST['brgyclearnce']?:"";
    $myvar['bcc'] = $_POST['bcc']?:"";
    $myvar['bcertc'] = $_POST['bcertc']?:"";
    $myvar['MedicalClearance'] = $_POST['MedicalClearance']?:"";
    $myvar['TOR'] = $_POST['TOR']?:"";
    $myvar['torc'] = $_POST['torc']?:"";
    $myvar['physicalexamination'] = $_POST['physicalexamination']?:"";
    $myvar['CORC'] = $_POST['CORC']?:"";
    $myvar['cpec'] = $_POST['cpec']?:"";
    $myvar['ECGattach'] = $_POST['ECGattach']?:"";
    $myvar['sssandid'] = $_POST['sssandid']?:"";
    $myvar['sstin'] = $_POST['sstin']?:"";
    $myvar['DrugTest'] = $_POST['DrugTest']?:"";
    $myvar['prcid'] = $_POST['prcid']?:"";
    $myvar['prcidno'] = $_POST['prcidno']?:"";
    $myvar['xray'] = $_POST['xray']?:"";
    $myvar['twobytwo'] = $_POST['twobytwo']?:"";
    $myvar['idtwo'] = $_POST['idtwo']?:"";
    $myvar['urinalysis'] = $_POST['urinalysis']?:"";
    $myvar['onebyone'] = $_POST['onebyone']?:"";
    $myvar['idone'] = $_POST['idone']?:"";
    $myvar['hepab'] = $_POST['hepab']?:"";
    $myvar['passportphoto'] = $_POST['passportphoto']?:"";
    $myvar['passportc'] = $_POST['passportc']?:"";
    $myvar['pschotest'] = $_POST['pschotest']?:"";
    $myvar['ScannedCopy'] = $_POST['ScannedCopy']?:"";
    $myvar['scannedcopyc'] = $_POST['scannedcopyc']?:"";
    $myvar['EmploymentShift'] = $_POST['EmploymentShift']?:"";
    $myvar['ShiftGroup'] = $_POST['ShiftGroup']?:"";
    $myvar['searchtraining'] = $_POST['searchtraining']?:"";
    $myvar['VacationLeaveTotal'] = $_POST['VacationLeaveTotal']?:"";
    $myvar['VLReserved'] = $_POST['VLReserved']?:"";
    $myvar['VLRequested'] = $_POST['VLRequested']?:"";
    $myvar['VLeft'] = $_POST['VLeft']?:"";
    $myvar['VLAccu'] = $_POST['VLAccu']?:"";
    $myvar['VLconverted'] = $_POST['VLconverted']?:"";
	
    
	$myvar['SLReserved'] = $_POST['SLReserved']?:"";
    $myvar['SLRequested'] = $_POST['SLRequested']?:"";
    $myvar['SLeft'] = $_POST['SLeft']?:"";
    $myvar['SLAccu'] = $_POST['SLAccu']?:"";
    $myvar['SLconverted'] = $_POST['SLconverted']?:"";
    $myvar['emergencyTotal'] = $_POST['emergencyTotal']?:"";
    $myvar['ELReserved'] = $_POST['ELReserved']?:"";
    $myvar['emergencyrequested'] = $_POST['emergencyrequested']?:"";
	
    
	
	$myvar['MaternityTotal'] = $_POST['MaternityTotal']?:"";
    $myvar['MLReserved'] = $_POST['MLReserved']?:"";
    $myvar['maternityRequested'] = $_POST['maternityRequested']?:"";
	
    
	
	$myvar['paterTotal'] = $_POST['paterTotal']?:"";
    $myvar['PLReserved'] = $_POST['PLReserved']?:"";
    $myvar['paterRequested'] = $_POST['paterRequested']?:"";
	
    
	
	$myvar['BLTotal'] = $_POST['BLTotal']?:"";
    $myvar['BLReserve'] = $_POST['BLReserve']?:"";
    $myvar['BLRequested'] = $_POST['BLRequested']?:"";
	
    
	
	$myvar['DlTotal'] = $_POST['DlTotal']?:"";
    $myvar['DLReserved'] = $_POST['DLReserved']?:"";
    $myvar['DLRequested'] = $_POST['DLRequested']?:"";
    
	
	
    $myvar['SILTotal'] = $_POST['SILTotal']?:"";
    $myvar['SILReserved'] = $_POST['SILReserved']?:"";
    $myvar['SILRequested'] = $_POST['SILRequested']?:"";
	
    
	
	$myvar['BDLTotal'] = $_POST['BDLTotal']?:"";
    $myvar['BDLReserve'] = $_POST['BDLReserve']?:"";
    $myvar['BDLRequested'] = $_POST['BDLRequested']?:"";
    $myvar['BDLLeft'] = $_POST['BDLLeft']?:"";
    
    
	
    
	
    $myvar['BasicPerCutOff'] = $_POST['BasicPerCutOff']?:"";
	
    
	
	$myvar['BasicPerHour'] = $_POST['BasicPerHour']?:"";
    $myvar['TaxShield'] = $_POST['TaxShield']?:"";
    $myvar['Cola'] = $_POST['Cola']?:"";
    $myvar['TempTransAllw'] = $_POST['TempTransAllw']?:"";
    $myvar['DeMinimis'] = $_POST['DeMinimis']?:"";
    $myvar['Longevity'] = $_POST['Longevity']?:"";
    $myvar['allowance1'] = $_POST['allowance1']?:"";
    $myvar['allowanceStatus1'] = $_POST['allowanceStatus1']?:"";
    $myvar['taxableAllowance1'] = $_POST['taxableAllowance1']?:"";
    $myvar['allowance2'] = $_POST['allowance2']?:"";
    $myvar['allowanceStatus2'] = $_POST['allowanceStatus2']?:"";
    $myvar['taxableAllowance2'] = $_POST['taxableAllowance2']?:"";
    $myvar['allowance3'] = $_POST['allowance3']?:"";
    $myvar['allowanceStatus3'] = $_POST['allowanceStatus3']?:"";
    $myvar['taxableAllowance3'] = $_POST['taxableAllowance3']?:"";
    $myvar['allowance4'] = $_POST['allowance4']?:"";
    $myvar['allowanceStatus4'] = $_POST['allowanceStatus4']?:"";
    $myvar['taxableAllowance4'] = $_POST['taxableAllowance4']?:"";
    $myvar['FixedDeducName1'] = $_POST['FixedDeducName1']?:"";
    $myvar['FixedDeducAmt1'] = $_POST['FixedDeducAmt1']?:"";
    $myvar['FixedDeducDateOfLoan1'] = $_POST['FixedDeducDateOfLoan1']?:"";
    $myvar['FixedDeducStartDate1'] = $_POST['FixedDeducStartDate1']?:"";
    $myvar['FixedDeducEndDate1'] = $_POST['FixedDeducEndDate1']?:"";
    $myvar['FixedDeducName2'] = $_POST['FixedDeducName2']?:"";
    $myvar['FixedDeducAmt2'] = $_POST['FixedDeducAmt2']?:"";
    $myvar['FixedDeducDateOfLoan2'] = $_POST['FixedDeducDateOfLoan2']?:"";
    $myvar['FixedDeducStartDate2'] = $_POST['FixedDeducStartDate2']?:"";
    $myvar['FixedDeducEndDate2'] = $_POST['FixedDeducEndDate2']?:"";
    $myvar['FixedDeducName3'] = $_POST['FixedDeducName3']?:"";
    $myvar['FixedDeducAmt3'] = $_POST['FixedDeducAmt3']?:"";
    $myvar['FixedDeducDateOfLoan3'] = $_POST['FixedDeducDateOfLoan3']?:"";
    $myvar['FixedDeducStartDate3'] = $_POST['FixedDeducStartDate3']?:"";
    $myvar['FixedDeducEndDate3'] = $_POST['FixedDeducEndDate3']?:"";
    $myvar['FixedDeducName4'] = $_POST['FixedDeducName4']?:"";
    $myvar['FixedDeducAmt4'] = $_POST['FixedDeducAmt4']?:"";
    $myvar['FixedDeducDateOfLoan4'] = $_POST['FixedDeducDateOfLoan4']?:"";
    $myvar['FixedDeducStartDate4'] = $_POST['FixedDeducStartDate4']?:"";
    $myvar['FixedDeducEndDate4'] = $_POST['FixedDeducEndDate4']?:"";
    $myvar['FixedDeducName5'] = $_POST['FixedDeducName5']?:"";
    $myvar['FixedDeducAmt5'] = $_POST['FixedDeducAmt5']?:"";
    $myvar['FixedDeducDateOfLoan5'] = $_POST['FixedDeducDateOfLoan5']?:"";
    $myvar['FixedDeducStartDate5'] = $_POST['FixedDeducStartDate5']?:"";
    $myvar['FixedDeducEndDate5'] = $_POST['FixedDeducEndDate5']?:"";
    $myvar['Loan'] = $_POST['Loan']?:"";
    $myvar['PagibigLoanType'] = $_POST['PagibigLoanType']?:"";
    $myvar['ApplNo'] = $_POST['ApplNo']?:"";
    $myvar['TotalPagibigLoan'] = $_POST['TotalPagibigLoan']?:"";
    $myvar['PagIbigLoan'] = $_POST['PagIbigLoan']?:"";
    $myvar['AmountPagIbigPaid'] = $_POST['AmountPagIbigPaid']?:"";
    $myvar['RemainingPagIbig'] = $_POST['RemainingPagIbig']?:"";
    $myvar['DateofLoanHDMF'] = $_POST['DateofLoanHDMF']?:"";
    $myvar['StartLoanHDMF'] = $_POST['StartLoanHDMF']?:"";
    $myvar['cutoffRemainPagibig'] = $_POST['cutoffRemainPagibig']?:"";
    $myvar['PagibigLoanRemarks'] = $_POST['PagibigLoanRemarks']?:"";
    $myvar['TotalHousingLoan'] = $_POST['TotalHousingLoan']?:"";
    $myvar['PhilhealthLoan'] = $_POST['PhilhealthLoan']?:"";
    $myvar['AmountHousingPaid'] = $_POST['AmountHousingPaid']?:"";
    $myvar['RemainingHousing'] = $_POST['RemainingHousing']?:"";
    $myvar['DateOfLoanHDMFHL'] = $_POST['DateOfLoanHDMFHL']?:"";
    $myvar['StartOfLoanHDMFHL'] = $_POST['StartOfLoanHDMFHL']?:"";
    $myvar['cutoffRemainHDMF'] = $_POST['cutoffRemainHDMF']?:"";
    $myvar['HousingLoanRemarks'] = $_POST['HousingLoanRemarks']?:"";
    $myvar['SSSLoanType'] = $_POST['SSSLoanType']?:"";
    $myvar['TotalSSSLoan'] = $_POST['TotalSSSLoan']?:"";
    $myvar['SSSLoan'] = $_POST['SSSLoan']?:"";
    $myvar['AmuntSSSPaid'] = $_POST['AmuntSSSPaid']?:"";
    $myvar['RemainingSSS'] = $_POST['RemainingSSS']?:"";
    $myvar['DateOfLoanSSS'] = $_POST['DateOfLoanSSS']?:"";
    $myvar['StartOfLoanSSS'] = $_POST['StartOfLoanSSS']?:"";
    $myvar['cutoffRemainSSS'] = $_POST['cutoffRemainSSS']?:"";
    $myvar['SSSLoanRemarks'] = $_POST['SSSLoanRemarks']?:"";
    $myvar['TotalCALoan'] = $_POST['TotalCALoan']?:"";
    $myvar['CashAdvance'] = $_POST['CashAdvance']?:"";
    $myvar['AmountCAPaid'] = $_POST['AmountCAPaid']?:"";
    $myvar['RemainingCA'] = $_POST['RemainingCA']?:"";
    $myvar['DateOfLoanCA'] = $_POST['DateOfLoanCA']?:"";
    $myvar['StartOfLoanCA'] = $_POST['StartOfLoanCA']?:"";
    $myvar['cutoffRemainCA'] = $_POST['cutoffRemainCA']?:"";
    $myvar['CARemarks'] = $_POST['CARemarks']?:"";
    $myvar['advances'] = $_POST['advances']?:"";
    $myvar['canteen'] = $_POST['canteen']?:"";
    $myvar['helpAssist'] = $_POST['helpAssist']?:"";
    $myvar['uniform'] = $_POST['uniform']?:"";
    $myvar['advSalary'] = $_POST['advSalary']?:"";
    $myvar['totalDeductions'] = $_POST['totalDeductions']?:"";
    $myvar['BiometricsID'] = $_POST['BiometricsID']?:"";
    $myvar['DTRType'] = $_POST['DTRType']?:"";
    $myvar['CutOffType'] = $_POST['CutOffType']?:"";
    $myvar['DailyMonthly'] = $_POST['DailyMonthly']?:"";
    $myvar['SSSContri_copy1'] = $_POST['SSSContri_copy1']?:"";
    $myvar['SSSGovtDeducType_copy1'] = $_POST['SSSGovtDeducType_copy1']?:"";
    $myvar['PhilHealthContri_copy1'] = $_POST['PhilHealthContri_copy1']?:"";
    $myvar['PhilhealthGovtDeducType_copy1'] = $_POST['PhilhealthGovtDeducType_copy1']?:"";
    $myvar['PagIbigContri_copy1'] = $_POST['PagIbigContri_copy1']?:"";
    $myvar['PagibigGovtDeducType_copy1'] = $_POST['PagibigGovtDeducType_copy1']?:"";
    $myvar['WithholdingTax_copy1'] = $_POST['WithholdingTax_copy1']?:"";
    $myvar['WithholdingtaxGovtDeducType_copy1'] = $_POST['WithholdingtaxGovtDeducType_copy1']?:"";
    $myvar['FixedAllwName1_copy1'] = $_POST['FixedAllwName1_copy1']?:"";
    $myvar['FixedAllwAmt1_copy1'] = $_POST['FixedAllwAmt1_copy1']?:"";
    $myvar['FixedAllwName2_copy1'] = $_POST['FixedAllwName2_copy1']?:"";
    $myvar['FixedAllwAmt2_copy1'] = $_POST['FixedAllwAmt2_copy1']?:"";
    $myvar['FixedAllwName3_copy1'] = $_POST['FixedAllwName3_copy1']?:"";
    $myvar['FixedAllwAmt3_copy1'] = $_POST['FixedAllwAmt3_copy1']?:"";
    $myvar['FixedAllwName4_copy1'] = $_POST['FixedAllwName4_copy1']?:"";
    $myvar['FixedAllwAmt4_copy1'] = $_POST['FixedAllwAmt4_copy1']?:"";
    $myvar['FixedAllwName5_copy1'] = $_POST['FixedAllwName5_copy1']?:"";
    $myvar['FixedAllwAmt5_copy1'] = $_POST['FixedAllwAmt5_copy1']?:"";
    $myvar['SSSContri_copy2'] = $_POST['SSSContri_copy2']?:"";
    $myvar['PhilHealthContri_copy2'] = $_POST['PhilHealthContri_copy2']?:"";
    $myvar['PagIbigContri_copy2'] = $_POST['PagIbigContri_copy2']?:"";
    $myvar['WithholdingTax_copy2'] = $_POST['WithholdingTax_copy2']?:"";
    $myvar['DueLeave'] = $_POST['DueLeave']?:"";
    $myvar['LeaveYear'] = $_POST['LeaveYear']?:"";
    $myvar['EmergencyContactName'] = $_POST['EmergencyContactName']?:"";
    $myvar['EmegencyTelNumber'] = $_POST['EmegencyTelNumber']?:"";
    $myvar['EmegencyContactAddress'] = $_POST['EmegencyContactAddress']?:"";
    $myvar['EntitlementTrackNo'] = $_POST['EntitlementTrackNo']?:"";
    $myvar['CurrentUserGroup'] = $_POST['CurrentUserGroup']?:"";
	$myvar['CurrentUserGroup'] = UserInfo('user_group');
	
    $myvar['SLTrackNo'] = $_POST['SLTrackNo']?:"";
    $myvar['VLTrackNo'] = $_POST['VLTrackNo']?:"";
    $myvar['EmployeeYear'] = $_POST['EmployeeYear']?:"";
    $get_track_no = $db->query("SELECT TrackNo FROM 15_tbl_personnelinfo WHERE ID = " . $_POST['ID'])[0]['TrackNo'];
    $myvar['TrackNo201'] = $get_track_no?:"";
    $myvar['TodayYear'] = $_POST['TodayYear']?:"";
	
	
    $myvar['reportToNo'] = $_POST['reportToNo']?:"";
    $myvar['Division'] = $_POST['Division']?:"";
    $myvar['DateSeparated'] = $_POST['DateSeparated']?:"";
    $myvar['OfferStatus'] = $_POST['OfferStatus']?:"";
    $myvar['NameCounter'] = $_POST['NameCounter']?:"";
    $myvar['BioCounter'] = $_POST['BioCounter']?:"";
	
	$myvar['DateHiredYear'] = $_POST['DateHiredYear']?:"";
	$myvar['DateHiredYear'] = StrRight(FormatDate($myvar['PersonnelDateHired'],'Y'),2);
	
    $myvar['EmployeeNumber'] = $_POST['EmployeeNumber']?:"";
    //$myvar['EmployeeNumber'] = StrConcat(StrRight(FormatDate($myvar['PersonnelDateHired'],'Y'),2),'-',StrRight(StrConcat("0000" ,LookupCountIf('Personnel Information 201','EmployeeNumber','EmployeeNumber','!=',StrConcat(''),'TodayYear','=',$myvar['DateHiredYear'])+1) , 4));
    
	$myvar['EmployeeYear'] = StrConcat($myvar['EmployeeNumber'],FormatDate($myvar['Today'],'Y'));
    $myvar['Confidentiality'] = $_POST['Confidentiality']?:"";
    //formula remove @
	//field @myvar['fieldname']\
	$myvar['TimeStamp'] = $_POST['TimeStamp']?:"";
	$myvar['EHTimeStamp'] = $_POST['EHTimeStamp']?:"";
    //$myvar['EHTimeStamp'] = GivenIf($myvar['RequestID'] == 0, $myvar['EmployeeNumber'], $myvar['EHTimeStamp']);
	$myvar['EHTimeStamp'] = GivenIf($myvar['RequestID'] == 0, $myvar['TimeStamp'], $myvar['EHTimeStamp']);
	
	
	$myvar['PersonnelDependents'] = $_POST['PersonnelDependents']?:"";
	$myvar['PersonnelDependents'] = Total( 'Names and Birthdate of Dependents' , 'DepNo' , 'ndependentimestamp', '=', $myvar['EHTimeStamp'],'Age','<=',StrConcat('21'));
	
	$myvar['NoDependents'] = $_POST['NoDependents']?:"";
    $myvar['NoDependents'] = GivenIf($myvar['PersonnelDependents'] >= 4, StrConcat('4'),$myvar['PersonnelDependents']);
	
	$myvar['EndProbation'] = $_POST['EndProbation']?:"";
    $myvar['EndProbation'] = GivenIf($myvar['EmploymentStatus']== 'Probationary',FormatDate(AdjustDate('month',$myvar['PersonnelDateHired'],$myvar['ProbiMonths']),'Y-m-d'), $myvar['EndProbation']);
	
	$myvar['SickLeaveTotal'] = $_POST['SickLeaveTotal']?:"";
    $myvar['SickLeaveTotal'] = GivenIf(FormatDate(ThisDate($myvar['Today']),'Y') == FormatDate(ThisDate($myvar['PersonnelDateHired']),'Y'),    GetRound( (1.25*(DiffDate('month', ThisDate(StrConcat(FormatDate(ThisDate($myvar['Today']),'Y') , '-12-31')) , $myvar['PersonnelDateHired'] ))),0),    (15)); //GivenIf(ThisDate($myvar['Today']).formatDate('Y')== ThisDate($myvar['PersonnelDateHired']).formatDate('Y'),GetRound((1.25*(ThisDate(StrConcat(ThisDate($myvar['Today']).formatDate('Y'), '-12-31')).diffMonthsIn( $myvar['PersonnelDateHired']))),0),(15))
	
	$myvar['EmergencyLeft'] = $_POST['EmergencyLeft']?:"";
    $myvar['EmergencyLeft'] = GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['emergencyTotal'],$myvar['EmergencyLeft']);
	
	$myvar['maternityLeft'] = $_POST['maternityLeft']?:"";
    $myvar['maternityLeft'] = GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['MaternityTotal'],$myvar['maternityLeft']);
	
	$myvar['paterLeft'] = $_POST['paterLeft']?:"";
    $myvar['paterLeft'] = GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['paterTotal'],$myvar['paterLeft']);
	
	$myvar['BLLeft'] = $_POST['BLLeft']?:"";
    $myvar['BLLeft'] = GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['BLTotal'],$myvar['BLLeft']);
	
	$myvar['DLLeft'] = $_POST['DLLeft']?:"";
	$myvar['DLLeft'] = GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['DlTotal'],$myvar['DLLeft']);
	
	$myvar['SILLeft'] = $_POST['SILLeft']?:"";
    $myvar['SILLeft'] = GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['SILTotal'],$myvar['SILLeft']);
	
	$myvar['DaysPerMonth'] = $_POST['DaysPerMonth']?:"";
	$myvar['DaysPerMonth'] = StrConcat('26.217');
	
	$myvar['DaysPerCutOff'] = $_POST['DaysPerCutOff']?:"";
	$myvar['DaysPerCutOff'] = $myvar['DaysPerMonth'] / 2;
	
	$myvar['BasicPerMonth'] = $_POST['BasicPerMonth']?:"";
	//$myvar['BasicPerMonth'] = GivenIf($myvar['CutOffType'] == 'Daily - Weekly' || $myvar['CutOffType'] == 'Daily - Monthly',GetRound($myvar['BasicPerDay'] * $myvar['DaysPerMonth'],2),$myvar['BasicPerMonth']);
	
	$myvar['BasicPerDay'] = $_POST['BasicPerDay']?:"";
    $myvar['BasicPerDay'] = GivenIf($myvar['CutOffType'] == 'Semi-Monthly',GetRound($myvar['BasicPerMonth'] / $myvar['DaysPerMonth'],2),$myvar['BasicPerDay']);
	
	$myvar['ManNum'] = $_POST['ManNum']?:"";
	$myvar['ManNum'] = LookupAuthInfo('id','email',$myvar['EmailAddress']);
	
	
    $nprf = $db->query("SELECT remarks, txtposTitle, department, project FROM 15_tbl_NewPositionRequestForm WHERE  TrackNo = '" . $myvar['positionNumber'] . "'");


    $formatted_data["computed_value"] = array(
        "PersonnelAge"=>DiffDate('year', $myvar['Today'], $myvar['PersonnelDateOfBirth']),
        "PersonnelDependents"=>$myvar['PersonnelDependents'],
        "NoDependents"=>$myvar['NoDependents'],
        "PersonnelName"=>GivenIf($myvar['EmailAddress'] == '',StrConcat($myvar['FirstName'],' ',StrLeft($myvar['MiddleName'], 1),'. ',$myvar['Surname']),LookupAuthInfo('display_name','email',$myvar['EmailAddress'])),
        "jobDescription"=>$nprf[0]['remarks'],
        "PersonnelPosition"=>GivenIf($myvar['positionNumber'] == '',$myvar['PersonnelPosition'],$nprf[0]['txtposTitle']),
        "Project"=>GivenIf($myvar['positionNumber'] == '',$myvar['Project'],$nprf[0]['project']),
        "DateRegular"=>$myvar['EndProbation'],
        "EndProbation"=>$myvar['EndProbation'],
        // "Section"=>,
        "PersonnelDepartment"=>$nprf[0]['department'],
        "Computation"=>$myvar['Computation'],//GivenIf($myvar['EmploymentStatus'] == 'Regular',LookupWhere(    'Leave Entitlement','LeaveCredits',    'LeaveType','=','Vacation Leave',    'PositionLevel','=',$myvar['PositionLevel'],    'FromLengthofService','<=', DiffDate('year',$myvar['Today'],$myvar['PersonnelDateHired']),    'ToLengthofService','>=',  DiffDate('year',$myvar['Today'],$myvar['PersonnelDateHired'])),0),
        "searchtraining"=>StrConcat($myvar['ManNum'],'YES'),
        // "VacationLeaveTotal"=>GivenIf($myvar['EmploymentStatus'] == 'Regular' && @JobClassification == 'Managers' && DiffDate('year',$myvar['Today'],$myvar['PersonnelDateHired']) < 10, 12,GivenIf($myvar['EmploymentStatus'] == 'Regular' && @JobClassification == 'Managers' && DiffDate('year',$myvar['Today'],$myvar['PersonnelDateHired']) >= 10,15,GivenIf($myvar['EmploymentStatus'] == 'Regular' && @JobClassification != 'Managers' && DiffDate('year',$myvar['Today'],$myvar['PersonnelDateHired']) < 6,7,GivenIf($myvar['EmploymentStatus'] == 'Regular' && @JobClassification != 'Managers' && DiffDate('year',$myvar['Today'],$myvar['PersonnelDateHired']) < 10,12,GivenIf($myvar['EmploymentStatus'] == 'Regular' && @JobClassification != 'Managers' && DiffDate('year',$myvar['Today'],$myvar['PersonnelDateHired']) >= 10,15,StrConcat('0')))))),
        "VLeft"=>GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['VacationLeaveTotal'],$myvar['VLeft']),
        "VLAccu"=>$myvar['VLAccu'],//GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['VacationLeaveTotal'],$myvar['VLeft']),
        "VLconverted"=>$myvar['VLconverted'],//GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['VacationLeaveTotal'],$myvar['VLeft']),
        "SickLeaveTotal"=>$myvar['SickLeaveTotal'],
        "SLeft"=>GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['SickLeaveTotal'],$myvar['SLeft']),
        "SLAccu"=>$myvar['SLAccu'],//GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['VacationLeaveTotal'],$myvar['VLeft']),
        "SLconverted"=>$myvar['SLconverted'],//GivenIf($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added',$myvar['VacationLeaveTotal'],$myvar['VLeft']),
        "EmergencyLeft"=>$myvar['EmergencyLeft'],
        // "MaternityTotal"=>,
        "maternityLeft"=>$myvar['maternityLeft'],
        // "paterTotal"=>,
        "paterLeft"=>$myvar['paterLeft'],
        "BLLeft"=>$myvar['BLLeft'],
        // "DlTotal"=>GivenIf(($myvar['RequestID'] == 0 || $myvar['Status'] == 'Employee Added') && @JobClassification == 'Operators, Drivers and Maintenance Personnel' && $myvar['DueLeave'] == 'YES',StrConcat('0'),StrConcat('6')),
        "DLLeft"=>$myvar['DLLeft'],
        "SILLeft"=>$myvar['SILLeft'],
        "DaysPerMonth"=>$myvar['DaysPerMonth'],
        "DaysPerCutOff"=>$myvar['DaysPerCutOff'],
        "BasicPerMonth"=>$myvar['BasicPerMonth'],
        "BasicPerCutOff"=>GivenIf($myvar['CutOffType'] == 'Daily - Weekly' || $myvar['CutOffType'] == 'Daily - Monthly',$myvar['DaysPerCutOff'] * $myvar['BasicPerDay'],$myvar['BasicPerMonth']/2),
        "BasicPerDay"=>$myvar['BasicPerDay'],
        "BasicPerHour"=>$myvar['BasicPerDay'] / 8,
        // "Cola"=>@ColaPerMonth / 2,
        // "Longevity"=>,
        // "SSSContri_copy1"=>,
        // "PhilHealthContri_copy1"=>,
        // "PagIbigContri_copy1"=>GivenIf(@GovtDeducType == 'Based on Gross Pay', 0.00, @PagIbigContri),
        // "SSSContri_copy2"=>,
        // "PhilHealthContri_copy2"=>,
        // "PagIbigContri_copy2"=>GivenIf(@GovtDeducType == 'Based on Gross Pay', 0.00, @PagIbigContri),
        "LeaveYear"=>$myvar['LeaveYear'],//FormatDate($myvar['Today'],'Y'),
        "CurrentUserGroup"=>$myvar['CurrentUserGroup'],
        "SLTrackNo"=>$db->query("SELECT TrackNo FROM 15_tbl_SLMaintenance WHERE EmployeeYear = '" . $myvar['EmployeeYear'] . "'")[0]['TrackNo'],//Lookup('SL Maintenance','TrackNo','EmployeeYear',$myvar['EmployeeYear']),
        "VLTrackNo"=>$db->query("SELECT TrackNo FROM 15_tbl_VLMaintenance WHERE EmployeeYear = '" . $myvar['EmployeeYear'] . "'")[0]['TrackNo'],//Lookup('VL Maintenance','TrackNo','EmployeeYear',$myvar['EmployeeYear']),
        "EmployeeYear"=>$myvar['EmployeeYear'],
        "TrackNo201"=>$myvar['TrackNo201'],
        "TodayYear"=>StrRight(FormatDate($myvar['Today'],'Y'),2),
        "DateHiredYear"=>$myvar['DateHiredYear'],
        "EHTimeStamp"=>$myvar['EHTimeStamp'],
        "ManNum"=>$myvar['ManNum'],
        "reportToNo"=>$myvar['reportToNo'],//Lookup( 'Position Information' , 'reportTo' , 'TrackNo' , $myvar['positionNumber'] ),
        "NameCounter"=>LookupCountIf('Personnel Information 201','PersonnelName','PersonnelName','=',$myvar['PersonnelName'],'SSSNumber','=',$myvar['SSSNumber'],'PersonnelDateOfBirth','=',$myvar['PersonnelDateOfBirth']),
        "BioCounter"=>LookupCountIf('Personnel Information 201','BiometricsID','BiometricsID','=',$myvar['BiometricsID']),
        "EmployeeNumber"=>$myvar['EmployeeNumber']
    );
	
	$formatted_data["visibility"]["EmploymentShift"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["searchtraining"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["emergencyTotal"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["ELReserved"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["emergencyrequested"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["EmergencyLeft"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["MaternityTotal"] = $myvar['PersonnelGender']=='Female'&&$myvar['CivilStatus']=='Married' ;
	$formatted_data["visibility"]["MLReserved"] = $myvar['PersonnelGender']=='Female'&&$myvar['CivilStatus']=='Married' ;
	$formatted_data["visibility"]["maternityRequested"] = $myvar['PersonnelGender']=='Female'&&$myvar['CivilStatus']=='Married' ;
	$formatted_data["visibility"]["maternityLeft"] = $myvar['PersonnelGender']=='Female'&&$myvar['CivilStatus']=='Married' ;
	$formatted_data["visibility"]["paterTotal"] = $myvar['PersonnelGender']=='Male'&&$myvar['CivilStatus']=='Married' ;
	$formatted_data["visibility"]["PLReserved"] = $myvar['PersonnelGender']=='Male'&&$myvar['CivilStatus']=='Married' ;
	$formatted_data["visibility"]["paterRequested"] = $myvar['PersonnelGender']=='Male'&&$myvar['CivilStatus']=='Married' ;
	$formatted_data["visibility"]["paterLeft"] = $myvar['PersonnelGender']=='Male'&&$myvar['CivilStatus']=='Married' ;
	$formatted_data["visibility"]["DlTotal"] = $myvar['DueLeave'] == 'YES' ;
	$formatted_data["visibility"]["DLReserved"] = $myvar['DueLeave'] == 'YES' ;
	$formatted_data["visibility"]["DLRequested"] = $myvar['DueLeave'] == 'YES' ;
	$formatted_data["visibility"]["DLLeft"] = $myvar['DueLeave'] == 'YES' ;
	$formatted_data["visibility"]["SILTotal"] = $myvar['EmploymentStatus'] == 'Project Based' ;
	$formatted_data["visibility"]["SILReserved"] = $myvar['EmploymentStatus'] == 'Project Based' ;
	$formatted_data["visibility"]["SILRequested"] = $myvar['EmploymentStatus'] == 'Project Based' ;
	$formatted_data["visibility"]["SILLeft"] = $myvar['EmploymentStatus'] == 'Project Based' ;
	$formatted_data["visibility"]["DaysPerCutOff"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["BasicPerCutOff"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["BasicPerHour"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["TaxShield"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["Cola"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["TempTransAllw"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["Longevity"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["DailyMonthly"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["EHTimeStamp"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["reportToNo"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["form-tab-panel"]["tabs-3_form-tabbable-pane-134"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["form-tab-panel"]["tabs-6_form-tabbable-pane-134"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["form-tab-panel"]["tabs-9_form-tabbable-pane-134"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["form-tab-panel"]["tabs-3_form-tabbable-pane-137"] = GivenIf(
	
		ArraySearchBy( StrSplitBy( $myvar['CurrentUserGroup'] , ",") , "Highly Confidential" ) >= 0 && ($myvar['Confidentiality'] =='Highly Confidential' || $myvar['Confidentiality'] =='Confidential' || $myvar['Confidentiality'] =='None'),
		true,
		GivenIf(
			ArraySearchBy( StrSplitBy( $myvar['CurrentUserGroup'] , ",") , "Confidential" ) >=0 && ($myvar['Confidentiality'] =='Confidential' || $myvar['Confidentiality'] =='None'),
			true,
			false
		)
	) ;
	$formatted_data["visibility"]["form-tab-panel"]["tabs-5_form-tabbable-pane-674"] = $myvar['Status'] == 'false' ;
	$formatted_data["visibility"]["form-tab-panel"]["tabs-4_form-tabbable-pane-731"] = $myvar['PersonnelGender'] == 'Female' ;
	$formatted_data["visibility"]["form-tab-panel"]["tabs-5_form-tabbable-pane-731"] = $myvar['PersonnelGender'] == 'Male' ;
	$formatted_data["visibility"]["form-tab-panel"]["tabs-11_form-tabbable-pane-731"] = $myvar['PersonnelGender'] == 'Female' ;
    $formatted_data["visibility"]["embed"]["886"] = [$myvar['EmploymentStatus'] == 'Project Employee',1] ;
	$formatted_data["visibility"]["embed"]["131"] = [$myvar['Status'] == 'false',1] ;
	$formatted_data["visibility"]["embed"]["172"] = [$myvar['Status'] == 'false',3] ;
	$formatted_data["visibility"]["embed"]["730"] = [$myvar['Status'] == 'false',2] ;
	$formatted_data["visibility"]["embed"]["738"] = $formatted_data["visibility"]["embed"]["730"] ;
	$formatted_data["visibility"]["embed"]["739"] = $formatted_data["visibility"]["embed"]["730"] ;
	$formatted_data["visibility"]["embed"]["740"] = $formatted_data["visibility"]["embed"]["730"] ;
	$formatted_data["visibility"]["embed"]["741"] = $formatted_data["visibility"]["embed"]["730"] ;
	$formatted_data["visibility"]["embed"]["742"] = $formatted_data["visibility"]["embed"]["730"] ;
	$formatted_data["visibility"]["embed"]["743"] = $formatted_data["visibility"]["embed"]["730"] ;
	$formatted_data["visibility"]["embed"]["744"] = $formatted_data["visibility"]["embed"]["730"] ;
	$formatted_data["visibility"]["embed"]["773"] = $formatted_data["visibility"]["embed"]["730"] ;
	$formatted_data["visibility"]["embed"]["774"] = $formatted_data["visibility"]["embed"]["730"] ;
	
	
	$formatted_data["TEST_DEBUG"] = $myvar['RequestID'];
	
    $print_debugger .= "\$myvar = ".json_encode($myvar);
    $print_debugger .= "\n\n";
    $print_debugger .= "\$personnel_info_SCGEmpID = ".json_encode($personnel_info_SCGEmpID);
    
    if( ( $selected_fields && count($selected_fields)>0 )&& ( $excluded_fields && count($excluded_fields)>0 ) ){
        foreach ($selected_fields as $key => $value) {
            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];
        }
        foreach ($excluded_fields as $key => $value) {
            unset($new_formatted_data["computed_value"]["". $value .""]);
        }
        $new_formatted_data["__DEBUG"] = $print_debugger;
        return $new_formatted_data;
    }else if($selected_fields && count($selected_fields)>0){
        foreach ($selected_fields as $key => $value) {
            // echo $formatted_data["". $value .""];
            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];
        }
        $new_formatted_data["__DEBUG"] = $print_debugger;
        return $new_formatted_data;
    }else if($excluded_fields && count($excluded_fields)>0){
        foreach ($excluded_fields as $key => $value) {
            unset($formatted_data["computed_value"]["". $value .""]);
        }
        $formatted_data["__DEBUG"] = $print_debugger;
        //print_r($formatted_data);
        return $formatted_data;
    }else{
	$formatted_data["__DEBUG"] = $print_debugger;
        return $formatted_data;
    }
        
    

}
?>