<?php
function UPPC_LeaveRequest($selected_fields, $excluded_fields, $formatted_data, $db){

    $print_debugger = "";
    $myvar['Today'] = $_POST["Today"]?:date("Y-m-d H:i:s");
    $myvar['RequestID'] = $_POST["RequestID"]?:"";
    $myvar['EManNum'] = $_POST["EManNum"]?:"";
    $myvar['EManNum'] = GivenIf(($myvar['RequestID'] ==0 && $myvar['EManNum']== ''),GetAuth("FormAuthID"),$myvar['EManNum']);
    $myvar['EmployeeNo'] = $_POST["EmployeeNo"]?:"";

    $personnel_info_ManNum = $db->query("SELECT * FROM `15_tbl_personnelinfo`
        WHERE `ManNum` = '".$myvar['EManNum']."'
    ", "row");
    $myvar['EmployeeNo'] = GivenIf($myvar['RequestID'] == 0 && $myvar['EmployeeNo'] == '', $personnel_info_ManNum['SCGEmpID'], $myvar['EmployeeNo']);
    $personnel_info_SCGEmpID = $db->query("SELECT * FROM `15_tbl_personnelinfo`
        WHERE `SCGEmpID` = '".$myvar['EmployeeNo']."'
    ", "row");
    $myvar['LeaveEmployeeNumber'] = $personnel_info_SCGEmpID["ManNum"];//Lookup('Personnel Info','ManNum','SCGEmpID',$myvar['EmployeeNo']);
    $personnel_info_ManNum_LeaveEmployeeNumber = $db->query("SELECT * FROM `15_tbl_personnelinfo`
        WHERE `ManNum` = '".$myvar['LeaveEmployeeNumber']."'
    ", "row");
    $myvar['Division'] = $personnel_info_SCGEmpID["Division"];
    $division_maintenance_data = $db->query("SELECT * FROM `15_tbl_divisionmaintenance`
        WHERE `divisionName` = '".$myvar['Division']."'
    ", "row");
    $myvar['DivisionPosNo'] = $division_maintenance_data['posnumtrack'];
    $personnel_info_position_number_data = $db->query("SELECT * FROM `15_tbl_personnelinfo`
        WHERE `positionNumber` = '".$myvar['DivisionPosNo']."'
    ", "row");
    $myvar['DivisionManager'] = $personnel_info_position_number_data['PersonnelName']; //Lookup('Personnel Info','PersonnelName','positionNumber',$myvar['DivisionPosNo']);
    // $personnel_info_scg_emp_id_data = $db->query("SELECT * FROM `15_tbl_personnelinfo`
    //     WHERE `SCGEmpID` = '".$myvar['EmployeeNo']."'
    //     OR `positionNumber` = '".$myvar['DivisionPosNo']."'
    // ", "row");
    $myvar['Section'] = $personnel_info_SCGEmpID["Section"];//Lookup('Personnel Info','Section','SCGEmpID',$myvar['EmployeeNo']);
    $section_maintenance_SectionName = $db->query("SELECT * FROM `15_tbl_sectionmaintenance`
        WHERE `SectionName` = '".$myvar['Section']."'
    ", "array");
    $myvar['Uniontrackno'] = $_POST["Uniontrackno"]?:"";
    // $union_maintenance_data = $db->query("SELECT * FROM `12_tbl_unionmaintenance`
    //     WHERE `TrackNo` = '".$myvar['Uniontrackno']."'
    // ", "array");

    // $vl_el_days_data = $db->query("SELECT * FROM `15_tbl_vlandeldays`
    //     WHERE `TrackNo` = '".$myvar['VLEL']."'
    // ", "array");
    
    // $relational_fields = array(
    //     "VLHours" => array( "ApprovedLeave" ) , 
    //     "SLHours" => array( "ApprovedLeave" ) , 
    //     "ELHours" => array( "ApprovedLeave" ) , 
    //     "NoOfDays" => array( "VLHours","SLHours","ELHours","HoursPerDay" ) , 
    //     "LeaveFrom" => array( "VLHours","SLHours","ELHours","NoOfDays","LeaveTo","HoursPerDay","CountRest","TotalRestHol","Countsubmittedleave","MondayRest","TuesdayRest","WednesdayRest","ThursdayRest","FridayRest","SaturdayRest","SundayRest" ) , 
    //     "LeaveTo" => array( "NoOfDays","CountRest","TotalRestHol","Countsubmittedleave","MondayRest","TuesdayRest","WednesdayRest","ThursdayRest","FridayRest","SaturdayRest","SundayRest" ) , 
    //     "HoursPerDay" => array( "VLHours","SLHours","ELHours" ) , 
    //     "leaveBalance" => array( "Pay" ) , 
    //     "gender" => array( "TypeofLeave" ) , 
    //     "soloparent" => array( "TypeofLeave" ) , 
    //     "TypeofLeave" => array( "VLHours","SLHours","ELHours","NoOfDays","leaveStatus","leaveBalance","Pay","DayType","LeaveReserved","vleldays" ) , 
    //     "DayType" => array( "VLHours","SLHours","ELHours","NoOfDays","HoursPerDay" ) , 
    //     "CountRest" => array( "NoOfDays","TotalHoursRemoved" ) , 
    //     "LeaveReserved" => array( "leaveBalance" ) , 
    //     "Unionofficer" => array( "TypeofLeave" ) , 
    //     "LeaveEmployeeNumber" => array( "LeaveReserved" ) , 
    //     "TotalRestHol" => array( "NoOfDays" ) , 
    //     "maritalStatus" => array( "TypeofLeave" ) , 
    //     "Section" => array( "SectionPosNo" ) , 
    //     "EmployeeNo" => array( "EmployeeLeaveTrackno","leaveBalance","gender","soloparent","CountRest","EmploymentStatus","Unionofficer","LeaveEmployeeNumber","maritalStatus","Section","Pname","Division","SectionManager","EarnedRestDay","EmpStatus","plgroup","Countsubmittedleave","Branch","MondayRest","TuesdayRest","WednesdayRest","ThursdayRest","FridayRest","SaturdayRest","SundayRest","Approvesby" ) , 
    //     "Pname" => array( "validateApprover" ) , 
    //     "EManNum" => array( "SCGEmpID","EmployeeNo","HRtype","Name" ) , 
    //     "Division" => array( "DivisionPosNo" ) , 
    //     "DivisionPosNo" => array( "DivisionManager" ) , 
    //     "SectionManager" => array( "validateApprover" ) , 
    //     "Uniontrackno" => array( "leaveBalance" ) , 
    //     "EmpStatus" => array( "Pay" ) , 
    //     "HRtype" => array( "TypeofLeave" ) , 
    //     "VLEL" => array( "vleldays" ) , 
    //     "Branch" => array( "TotalRestHol" ) , 
    //     "MondayRest" => array( "CountRest" ) , 
    //     "TuesdayRest" => array( "CountRest" ) , 
    //     "WednesdayRest" => array( "CountRest" ) , 
    //     "ThursdayRest" => array( "CountRest" ) , 
    //     "FridayRest" => array( "CountRest" ) , 
    //     "SaturdayRest" => array( "CountRest" ) , 
    //     "SundayRest" => array( "CountRest" )
    // );
    $myvar['LeaveReason'] = $_POST["LeaveReason"]?:"";
    $myvar['LeaveRemarks'] = $_POST["LeaveRemarks"]?:"";
    $myvar['LeaveFrom'] = $_POST["LeaveFrom"]?:"";
    // $myvar['LeaveFrom'] = GivenIf($myvar['LeaveFrom'] == "", $myvar['Today'],$myvar['LeaveFrom']); // $myvar['LeaveFrom'] = GivenIf($myvar['RequestID'] == 0, $myvar['Today'],$myvar['LeaveFrom']);
    $myvar['LeaveTo'] = $_POST["LeaveTo"]?:"";
    // $myvar['LeaveTo'] = GivenIf($_POST["LeaveTo"] == "", $myvar['LeaveFrom'],$myvar['LeaveTo']); // $myvar['LeaveTo'] = GivenIf($myvar['RequestID'] == 0, $myvar['LeaveFrom'],$myvar['LeaveTo']);
    $myvar['Name'] = $personnel_info_ManNum["PersonnelName"];//$personnel_info_ManNum["PersonnelName"];// Lookup('Personnel Info','PersonnelName','ManNum',$myvar['EManNum']);
    $myvar['HRtype'] = $personnel_info_ManNum["HRType"];//Lookup('Personnel Info','HRType','ManNum',$myvar['EManNum']);
    $myvar['Unionofficer'] = $personnel_info_SCGEmpID["Unionofficer"];//Lookup('Personnel Info','Unionofficer','SCGEmpID',$myvar['EmployeeNo']);
    $myvar['gender'] = $personnel_info_SCGEmpID["PersonnelGender"];//Lookup('Personnel Info','PersonnelGender','SCGEmpID',$myvar['EmployeeNo']);
    $myvar['Pname'] = $personnel_info_SCGEmpID["PersonnelName"];//Lookup('Personnel Info','PersonnelName','SCGEmpID',$myvar['EmployeeNo']);
    $myvar['EmployeeLeaveTrackno'] = $personnel_info_SCGEmpID["TrackNo"];//Lookup('Personnel Info','TrackNo','SCGEmpID',$myvar['EmployeeNo']);
    $myvar['SectionPosNo'] = $section_maintenance_SectionName['posnumtrack'];//Lookup( 'Section Maintenance' , 'posnumtrack' , 'SectionName' , $myvar['Section']);
    $myvar['soloparent'] = $personnel_info_SCGEmpID["SoloParent"];//Lookup('Personnel Info','SoloParent','SCGEmpID',$myvar['EmployeeNo']);
    $myvar['SectionManager'] = $personnel_info_SCGEmpID["reportingTo"];//Lookup('Personnel Info','reportingTo','SCGEmpID',$myvar['EmployeeNo']);
    $myvar['SCGEmpID'] = $personnel_info_ManNum['SCGEmpID'];
    $myvar['maritalStatus'] = $personnel_info_SCGEmpID["PersonnelStatus"];//Lookup('Personnel Info','PersonnelStatus','SCGEmpID',$myvar['EmployeeNo']);
    $myvar['EmpStatus'] = $personnel_info_SCGEmpID["EmploymentStatus"];//Lookup('Personnel Info','EmploymentStatus','SCGEmpID',$myvar['EmployeeNo']);
    $myvar['plgroup'] = $personnel_info_SCGEmpID["PLGroup"];//Lookup('Personnel Info','PLGroup','SCGEmpID',$myvar['EmployeeNo']);
    $myvar['leaveStatus'] = $_POST["leaveStatus"]?:"";
    $myvar['leaveStatus'] = GivenIf($myvar['TypeofLeave']!='Sick Leave',StrConcat('Vacation Leave'),StrConcat('Sick Leave'));
    $myvar['TypeofLeave'] = $_POST["TypeofLeave"]?:"";
    $myvar['LeaveReserved'] = $_POST["LeaveReserved"]?:"";
    $myvar['LeaveReserved'] = GivenIf(
        $myvar['TypeofLeave']=='Vacation Leave',
        $personnel_info_ManNum_LeaveEmployeeNumber["VLReserved"],//Lookup('Personnel Info','VLReserved','ManNum',$myvar['LeaveEmployeeNumber']),
        GivenIf(
            $myvar['TypeofLeave']=='Sick Leave',
            $personnel_info_ManNum_LeaveEmployeeNumber["SLReserved"],//Lookup('Personnel Info','SLReserved','ManNum',$myvar['LeaveEmployeeNumber']),
            GivenIf(
                $myvar['TypeofLeave']=='Paternity Leave',
                $personnel_info_ManNum_LeaveEmployeeNumber["PLReserved"],//Lookup('Personnel Info','PLReserved','ManNum',$myvar['LeaveEmployeeNumber']),
                GivenIf(
                    $myvar['TypeofLeave']=='Bereavement Leave',
                    $personnel_info_ManNum_LeaveEmployeeNumber["BLReserved"],//Lookup('Personnel Info','BLReserved','ManNum',$myvar['LeaveEmployeeNumber']),
                    GivenIf(
                        $myvar['TypeofLeave']=='Solo Parent',
                        $personnel_info_ManNum_LeaveEmployeeNumber["SoloReserved"],//Lookup('Personnel Info','SoloReserved','ManNum',$myvar['LeaveEmployeeNumber']),
                        GivenIf(
                            $myvar['TypeofLeave']=='Emergency Leave',
                            $personnel_info_ManNum_LeaveEmployeeNumber["ELReserved"],//Lookup('Personnel Info','ELReserved','ManNum',$myvar['LeaveEmployeeNumber']),
                            GivenIf(
                                $myvar['TypeofLeave']=='Union Leave',
                                $personnel_info_ManNum_LeaveEmployeeNumber["ULReserved"],//Lookup('Personnel Info','ULReserved','ManNum',$myvar['LeaveEmployeeNumber']),
                                GivenIf(
                                    $myvar['TypeofLeave']=='Earned Rest Day',
                                    $personnel_info_ManNum_LeaveEmployeeNumber["ERdreserved"],//Lookup('Personnel Info','ERdreserved','ManNum',$myvar['LeaveEmployeeNumber']),
                                    $personnel_info_ManNum_LeaveEmployeeNumber["MLReserved"]//Lookup('Personnel Info','MLReserved','ManNum',$myvar['LeaveEmployeeNumber'])
                                )
                            )
                        )
                    )
                )
            )
        )
    );
    $myvar['leaveBalance'] = $_POST["leaveBalance"]?:"";
    $myvar['leaveBalance'] = GivenIf(
        $myvar['TypeofLeave']=='Vacation Leave',
        $personnel_info_SCGEmpID["VLeft"]/*Lookup('Personnel Info','VLeft','SCGEmpID',$myvar['EmployeeNo'])*/ - $myvar['LeaveReserved'],
        GivenIf(
            $myvar['TypeofLeave']=='Sick Leave',
            $personnel_info_SCGEmpID["SLeft"]/*Lookup('Personnel Info','SLeft','SCGEmpID',$myvar['EmployeeNo'])*/ -$myvar['LeaveReserved'],
            GivenIf(
                $myvar['TypeofLeave']=='Paternity Leave',
                $personnel_info_SCGEmpID["paterLeft"]/*Lookup('Personnel Info','paterLeft','SCGEmpID',$myvar['EmployeeNo'])*/ - $myvar['LeaveReserved'],
                GivenIf(
                    $myvar['TypeofLeave']=='Bereavement Leave',
                    $personnel_info_SCGEmpID["BrvLeft"]/*Lookup('Personnel Info','BrvLeft','SCGEmpID',$myvar['EmployeeNo'])*/ - $myvar['LeaveReserved'],
                    GivenIf(
                        $myvar['TypeofLeave']=='Solo Parent',
                        Lookup('Personnel Info','SoloLeft','SCGEmpID',$myvar['EmployeeNo'] ) - $myvar['LeaveReserved'],
                        GivenIf(
                            $myvar['TypeofLeave']=='Emergency Leave',
                            Lookup('Personnel Info','EmergencyLeft','SCGEmpID',$myvar['EmployeeNo'] ) - $myvar['LeaveReserved'],
                            GivenIf(
                                $myvar['TypeofLeave']=='Union Leave',
                                Lookup('Union Maintenance','leaveBalance','TrackNo',$myvar['Uniontrackno']),
                                GivenIf(
                                    $myvar['TypeofLeave']=='Maternity Leave',
                                    $personnel_info_SCGEmpID["maternityLeft"]/*Lookup('Personnel Info','maternityLeft','SCGEmpID',$myvar['EmployeeNo'])*/ - $myvar['LeaveReserved'],
                                    $personnel_info_SCGEmpID["ERDay"]/*Lookup('Personnel Info','ERDay','SCGEmpID',$myvar['EmployeeNo'])*/- $myvar['LeaveReserved']
                                )
                            )
                        )
                    )
                )
            )
        )
    );
    $myvar['Pay'] = $_POST["Pay"]?:"";
    $myvar['Pay'] = GivenIf(($myvar['leaveBalance']=='0' && $myvar['TypeofLeave'] != 'Earned Rest Day') || $myvar['EmpStatus'] =='Probationary',['Without Pay'],['With Pay','Without Pay']);
    $myvar['DayType'] = $_POST["DayType"]?:"";
    $myvar['MondayRest'] = $_POST["MondayRest"]?:"";
    $myvar['MondayRest'] = GivenIf($personnel_info_SCGEmpID["MondayRest"]/*Lookup('Personnel Info','MondayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Monday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Monday'),0);
    $myvar['TuesdayRest'] = $_POST["TuesdayRest"]?:"";
    $myvar['TuesdayRest'] = GivenIf($personnel_info_SCGEmpID["TuesdayRest"]/*Lookup('Personnel Info','TuesdayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Tuesday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Tuesday'),0);
    $myvar['WednesdayRest'] = $_POST["WednesdayRest"]?:"";
    $myvar['WednesdayRest'] = GivenIf($personnel_info_SCGEmpID["WednesdayRest"]/*Lookup('Personnel Info','WednesdayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Wednesday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Wednesday'),0);
    $myvar['ThursdayRest'] = $_POST["ThursdayRest"]?:"";
    $myvar['ThursdayRest'] = GivenIf($personnel_info_SCGEmpID["ThursdayRest"]/*Lookup('Personnel Info','ThursdayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Thursday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Thursday'),0);
    $myvar['FridayRest'] = $_POST["FridayRest"]?:"";
    $myvar['FridayRest'] = GivenIf($personnel_info_SCGEmpID["FridayRest"]/*Lookup('Personnel Info','FridayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Friday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Friday'),0);
    $myvar['SaturdayRest'] = $_POST["SaturdayRest"]?:"";
    $myvar['SaturdayRest'] = GivenIf($personnel_info_SCGEmpID["SaturdayRest"]/*Lookup('Personnel Info','SaturdayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Saturday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Saturday'),0);
    $myvar['SundayRest'] = $_POST["SundayRest"]?:"";
    $myvar['SundayRest'] = GivenIf($personnel_info_SCGEmpID["SundayRest"]/*Lookup('Personnel Info','SundayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Sunday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Sunday'),0);
    $myvar['CountRest'] = $_POST["CountRest"]?:"";
    $myvar['CountRest'] = GivenIf($myvar['RequestID']==0,GivenIf((LookupWhere('Personnel Info','WorkTimeIn','SCGEmpID','=',$myvar['EmployeeNo']) != '' && LookupWhere('Personnel Info','WorkTimeIn','SCGEmpID','=',$myvar['EmployeeNo']) != '') ||(LookupWhere('Personnel Info','WorkTimeIn','SCGEmpID','=',$myvar['EmployeeNo']) != '00:00:00' && LookupWhere('Personnel Info','WorkTimeIn','SCGEmpID','=',$myvar['EmployeeNo']) != '00:00:00') ||LookupWhere('Personnel Info','WorkTimeIn','SCGEmpID','=',$myvar['EmployeeNo']) != '00:00' && LookupWhere('Personnel Info','WorkTimeIn','SCGEmpID','=',$myvar['EmployeeNo']) != '00:00',(LookupCountIf('Employee Shifting Schedule','ShiftType','ShiftType','=','','DateOfShift','>=',$myvar['LeaveFrom'],'DateOfShift','<=',$myvar['LeaveTo'],'ManNo','=',$myvar['EmployeeNo']) - Total('Employee Shifting Schedule','Holiday','DateOfShift','>=',$myvar['LeaveFrom'],'DateOfShift','<=',$myvar['LeaveTo'],'ManNo','=',$myvar['EmployeeNo'])),$myvar['MondayRest'] + $myvar['TuesdayRest'] + $myvar['WednesdayRest'] + $myvar['TuesdayRest'] + $myvar['WednesdayRest'] + $myvar['ThursdayRest'] + $myvar['FridayRest'] + $myvar['SaturdayRest'] + $myvar['SundayRest']),$myvar['CountRest']);
    $myvar['Branch'] = Lookup('Personnel Info', 'WorkLocation', 'SCGEmpID', $myvar['EmployeeNo']);
    $myvar['TotalRestHol'] = $_POST["TotalRestHol"]?:"";
    $myvar['TotalRestHol'] = Total('Holiday Maintenance','Holiday','DateOfShift','>=',$myvar['LeaveFrom'],'DateOfShift','<=',$myvar['LeaveTo'],'Status','!=','Deactivated','HolidayCheck','=','All Employee') +Total('Holiday Maintenance','Holiday','DateOfShift','>=',$myvar['LeaveFrom'],'DateOfShift','<=',$myvar['LeaveTo'],'Status','!=','Deactivated','HolidayCheck','=','Per Location','Location','=',$myvar['Branch']);
    $myvar['NoOfDays'] = $_POST["NoOfDays"]?:"";
    $myvar['NoOfDays'] = GivenIf(
                            $myvar['DayType'] == 'Half Day' && $myvar['LeaveFrom'] != '' && $myvar['TypeofLeave'] != 'Maternity Leave',
                            0.5,
                            GivenIf(
                                $myvar['DayType'] == 'Whole Day' && $myvar['LeaveFrom'] != '' && $myvar['TypeofLeave'] != 'Maternity Leave',
                                ( DiffDate("day", ThisDate($myvar['LeaveTo']) , ThisDate($myvar['LeaveFrom']) ) + 1) - ( $myvar['CountRest'] + $myvar['TotalRestHol']),
                                GivenIf(
                                    $myvar['DayType'] == 'Whole Day' && $myvar['LeaveFrom'] != '' && $myvar['TypeofLeave'] == 'Maternity Leave',
                                    (DiffDate("day", ThisDate($myvar['LeaveTo']) , ThisDate($myvar['LeaveFrom']) ) + 1),
                                    0
                                )
                            )
                        );
    $myvar['HoursPerDay'] = $_POST["HoursPerDay"]?:"";
    $myvar['HoursPerDay'] = GivenIf($myvar['DayType'] == "Half Day" && $myvar['LeaveFrom'] != '',4,GivenIf($myvar['DayType'] == 'Whole Day' && $myvar['LeaveFrom'] != '',($myvar['NoOfDays'] * 8),0));
    $myvar['TotalHoursRemoved'] = $_POST["TotalHoursRemoved"]?:"";
    
    $myvar['EmploymentStatus'] = $personnel_info_SCGEmpID['JobClassification'];//Lookup('Personnel Info','JobClassification','SCGEmpID',$myvar['EmployeeNo']);
    
    $myvar['ANleave'] = $_POST["ANleave"]?:"";
    
    $myvar['EarnedRestDay'] = $_POST["EarnedRestDay"]?:"";
    $myvar['EarnedRestDay'] = GivenIf($myvar['RequestID'] == 0, ($personnel_info_SCGEmpID["ERDay"]/*Lookup( 'Personnel Info' , 'ERDay' , 'SCGEmpID',$myvar['EmployeeNo'])*/-$personnel_info_SCGEmpID["ERdreserved"]/*Lookup( 'Personnel Info' , 'ERdreserved' , 'SCGEmpID',$myvar['EmployeeNo'])*/),$myvar['EarnedRestDay']);

    $myvar['validateApprover'] = $_POST["validateApprover"]?:"";
    $myvar['validateApprover'] = GivenIf($myvar['Pname'] == $myvar['SectionManager'],StrConcat('1'),StrConcat('0'));
   
    $myvar['Countsubmittedleave'] = $_POST["Countsubmittedleave"]?:"";
    $myvar['Countsubmittedleave'] = GivenIf($myvar['RequestID'] == 0, ((LookupCountIf('Leave Request','LeaveFrom','LeaveTo','>=',$myvar['LeaveFrom'],'LeaveFrom','<=',$myvar['LeaveFrom'],'EmployeeNo','=',$myvar['EmployeeNo'],'Status','=','Approved','DayType','=','Whole Day') +LookupCountIf('Leave Request','LeaveTo','LeaveTo','>=',$myvar['LeaveTo'],'LeaveFrom','<=',$myvar['LeaveTo'],'EmployeeNo','=',$myvar['EmployeeNo'],'Status','=','Approved','DayType','=','Whole Day') + LookupCountIf('Leave Request','LeaveFrom','LeaveFrom','>=',$myvar['LeaveFrom'],'LeaveTo','<=',$myvar['LeaveTo'],'EmployeeNo','=',$myvar['EmployeeNo'],'Status','=','Approved','DayType','=','Whole Day'))+(LookupCountIf('Leave Request','LeaveFrom','LeaveTo','>=',$myvar['LeaveFrom'],'LeaveFrom','<=',$myvar['LeaveFrom'],'EmployeeNo','=',$myvar['EmployeeNo'],'Status','=','For Approval','DayType','=','Whole Day') +LookupCountIf('Leave Request','LeaveTo','LeaveTo','>=',$myvar['LeaveTo'],'LeaveFrom','<=',$myvar['LeaveTo'],'EmployeeNo','=',$myvar['EmployeeNo'],'Status','=','For Approval','DayType','=','Whole Day') + LookupCountIf('Leave Request','LeaveFrom','LeaveFrom','>=',$myvar['LeaveFrom'],'LeaveTo','<=',$myvar['LeaveTo'],'EmployeeNo','=',$myvar['EmployeeNo'],'Status','=','For Approval','DayType','=','Whole Day'))+(LookupCountIf('Leave Request','LeaveFrom','LeaveTo','>=',$myvar['LeaveFrom'],'LeaveFrom','<=',$myvar['LeaveFrom'],'EmployeeNo','=',$myvar['EmployeeNo'],'Status','=','Updated Approved Leave','DayType','=','Whole Day') +LookupCountIf('Leave Request','LeaveTo','LeaveTo','>=',$myvar['LeaveTo'],'LeaveFrom','<=',$myvar['LeaveTo'],'EmployeeNo','=',$myvar['EmployeeNo'],'Status','=','Updated Approved Leave','DayType','=','Whole Day') + LookupCountIf('Leave Request','LeaveFrom','LeaveFrom','>=',$myvar['LeaveFrom'],'LeaveTo','<=',$myvar['LeaveTo'],'EmployeeNo','=',$myvar['EmployeeNo'],'Status','=','Updated Approved Leave','DayType','=','Whole Day'))), $myvar['Countsubmittedleave']);
    
    $myvar['VLEL'] = $_POST["VLEL"]?:"";
    // $myvar['VLEL'] = StrConcat('VLEL0001');

    $myvar['vleldays'] = $_POST["vleldays"]?:"";
    $myvar['vleldays'] = GivenIf($myvar['RequestID'] == 0 && ($myvar['TypeofLeave'] == 'Vacation Leave' || $myvar['TypeofLeave'] == 'Earned Rest Day'),Lookup( 'VL and EL days' , 'VLDays' , 'TrackNo' , $myvar['VLEL']),GivenIf($myvar['RequestID'] == 0 && $myvar['TypeofLeave'] == 'Emergency Leave',Lookup( 'VL and EL days' , 'ELDays' , 'TrackNo' , $myvar['VLEL']),StrConcat('0')));

    $myvar['Approvesby'] = $_POST["Approvesby"]?:"";
    $myvar['Approvesby'] = GivenIf($myvar['RequestID'] == 0,Lookup('Personnel Info','reportingTo','SCGEmpID',$myvar['EmployeeNo']),$myvar['Approvesby']);

    $myvar['ApprovedLeave'] = $_POST["ApprovedLeave"]?:"";
    $myvar['ApprovedLeave'] = GivenIf($myvar['Status'] == 'For Approval' || $myvar['Status'] == 'Approved',$myvar['VLHours'] + $myvar['SLHours'] + $myvar['ELHours'],0);
    
    $myvar['VLHours'] = $_POST["VLHours"]?:"";
    $myvar['VLHours'] = GivenIf($myvar['TypeofLeave'] == 'Vacation Leave' || $myvar['TypeofLeave'] == 'Paternity Leave' || $myvar['TypeofLeave'] == 'Maternity Leave',GivenIf($myvar['DayType'] == "Half Day" && $myvar['LeaveFrom'] != '',4,GivenIf($myvar['DayType'] == 'Whole Day' && $myvar['LeaveFrom'] != '',$myvar['HoursPerDay'] / $myvar['NoOfDays'],0)));
    
    $myvar['SLHours'] = $_POST["SLHours"]?:"";
    $myvar['SLHours'] = GivenIf($myvar['TypeofLeave'] == 'Sick Leave',GivenIf($myvar['DayType'] == "Half Day" && $myvar['LeaveFrom'] != '',4,GivenIf($myvar['DayType'] == 'Whole Day' && $myvar['LeaveFrom'] != '',$myvar['HoursPerDay'] / $myvar['NoOfDays'],0)));

    $myvar['ELHours'] = $_POST["ELHours"]?:"";
    $myvar['ELHours'] = GivenIf($myvar['TypeofLeave'] == 'Bereavement Leave' || $myvar['TypeofLeave'] == 'Emergency Leave',GivenIf($myvar['DayType'] == "Half Day" && $myvar['LeaveFrom'] != '',4,GivenIf($myvar['DayType'] == 'Whole Day' && $myvar['LeaveFrom'] != '',$myvar['HoursPerDay'] / $myvar['NoOfDays'],0)));


    $myvar['Status'] = $_POST["Status"]?:"";
    $myvar['CountHo'] = $_POST["CountHo"]?:"";

    $formatted_data["change_list_computed_value"] = array(
        "TypeofLeave"=>GivenIf($myvar['HRtype'] == 'HR',['Vacation Leave','Emergency Leave','Paternity Leave','Union Leave','Earned Rest Day','Maternity Leave','Sick Leave'],GivenIf($myvar['gender'] == 'Female' && $myvar['maritalStatus'] == 'Single' && $myvar['Unionofficer'] != 'Union Officer' && $myvar['soloparent'] == '',['Vacation Leave','Emergency Leave','Maternity Leave','Earned Rest Day'],GivenIf($myvar['gender'] == 'Female' && $myvar['maritalStatus'] == 'Single' && $myvar['Unionofficer'] == 'Union Officer' && $myvar['soloparent'] == '',['Vacation Leave','Emergency Leave','Maternity Leave','Union Leave','Earned Rest Day'],GivenIf($myvar['gender'] == 'Female' && $myvar['maritalStatus'] == 'Single' && $myvar['Unionofficer'] == 'Union Officer' && $myvar['soloparent'] == 'Solo Parent',['Vacation Leave','Emergency Leave','Maternity Leave','Union Leave','Solo Parent','Earned Rest Day'],GivenIf($myvar['gender'] == 'Female' && $myvar['maritalStatus'] == 'Single' && $myvar['Unionofficer'] != 'Union Officer' && $myvar['soloparent'] == 'Solo Parent',['Vacation Leave','Emergency Leave','Maternity Leave','Solo Parent','Earned Rest Day'],GivenIf($myvar['gender'] == 'Female' && $myvar['maritalStatus'] == 'Married' && $myvar['Unionofficer'] != 'Union Officer' && $myvar['soloparent'] == '',['Vacation Leave','Emergency Leave','Maternity Leave','Earned Rest Day'],GivenIf($myvar['gender'] == 'Female' && $myvar['maritalStatus'] == 'Married' && $myvar['Unionofficer'] == 'Union Officer' && $myvar['soloparent'] == '',['Vacation Leave','Emergency Leave','Maternity Leave','Union Leave','Earned Rest Day'],GivenIf($myvar['gender'] == 'Male' && $myvar['maritalStatus'] == 'Single' && $myvar['Unionofficer'] != 'Union Officer' && $myvar['soloparent'] == '',['Vacation Leave','Emergency Leave','Earned Rest Day'],GivenIf($myvar['gender'] == 'Male' && $myvar['maritalStatus'] == 'Single' && $myvar['Unionofficer'] == 'Union Officer' && $myvar['soloparent'] == '',['Vacation Leave','Emergency Leave','Union Leave','Earned Rest Day'],GivenIf($myvar['gender'] == 'Male' && $myvar['maritalStatus'] == 'Single' && $myvar['Unionofficer'] == 'Union Officer' && $myvar['soloparent'] == 'Solo Parent',['Vacation Leave','Emergency Leave','Union Leave','Solo Parent','Earned Rest Day'],GivenIf($myvar['gender'] == 'Male' && $myvar['maritalStatus'] == 'Single' && $myvar['Unionofficer'] != 'Union Officer' && $myvar['soloparent'] == 'Solo Parent',['Vacation Leave','Emergency Leave','Paternity Leave','Solo Parent','Earned Rest Day'],GivenIf($myvar['gender'] == 'Male' && $myvar['maritalStatus'] == 'Married' && $myvar['Unionofficer'] != 'Union Officer' && $myvar['soloparent'] == '',['Vacation Leave','Emergency Leave','Paternity Leave','Earned Rest Day'],GivenIf($myvar['gender'] == 'Male' && $myvar['maritalStatus'] == 'Married' && $myvar['Unionofficer'] == 'Union Officer' && $myvar['soloparent'] == '',['Vacation Leave','Emergency Leave','Paternity Leave','Union Leave','Earned Rest Day'],['--Select--']))))))))))))),
        "DayType"=>GivenIf($myvar['TypeofLeave'] == 'Maternity Leave',['Whole Day'],['Whole Day','Half Day'])
    );
    
    $formatted_data["computed_value"] = array(
        "EManNum"=>$myvar['EManNum'],
        "VLHours"=>$myvar['VLHours'],
        "SLHours"=>$myvar['SLHours'],
        "ELHours"=>$myvar['ELHours'],
        "NoOfDays"=>$myvar['NoOfDays'],
        "LeaveFrom"=>$myvar['LeaveFrom'],
        "LeaveTo"=>$myvar['LeaveTo'],
        "ApprovedLeave"=>$myvar['ApprovedLeave'],
        "HoursPerDay"=>$myvar['HoursPerDay'],
        "leaveStatus"=>$myvar['leaveStatus'],
        "EmployeeLeaveTrackno"=>$myvar['EmployeeLeaveTrackno'],
        "Name"=>$myvar['Name'],
        "leaveBalance"=>$myvar['leaveBalance'],
        "gender"=>$myvar['gender'],
        "soloparent"=>$myvar['soloparent'],
        "Pay"=>$myvar['Pay'],
        "DayType"=>$myvar['DayType'],
        "CountRest"=>$myvar['CountRest'],
        // "TotalHoursRemoved"=>($myvar['CountRest'] * 8) + (@CountHoliday*8),
        "LeaveReserved"=>$myvar['LeaveReserved'],
        "EmploymentStatus"=>$myvar['EmploymentStatus'],
        "Unionofficer"=>$myvar['Unionofficer'],
        "LeaveEmployeeNumber"=>$myvar['LeaveEmployeeNumber'],
        "TotalRestHol"=>$myvar['TotalRestHol'],
        "SCGEmpID"=>$myvar['SCGEmpID'],
        "maritalStatus"=>$myvar['maritalStatus'],
        "Section"=>$myvar['Section'],
        "EmployeeNo"=>$myvar['EmployeeNo'],
        "Pname"=>$myvar['Pname'],
        
        "Division"=>$myvar['Division'],
        "DivisionPosNo"=>$myvar['DivisionPosNo'],
        "SectionPosNo"=>$myvar['SectionPosNo'],
        "DivisionManager"=>$myvar['DivisionManager'],
        "SectionManager"=>$myvar['SectionManager'],
        // "Uniontrackno"=>Lookup( 'Union Maintenance' , 'TrackNo' , 'Unionofficer' , @unionName ),
        "EarnedRestDay"=>$myvar['EarnedRestDay'],
        "validateApprover"=>$myvar['validateApprover'],
        "EmpStatus"=>$myvar['EmpStatus'],
        "plgroup"=>$myvar['plgroup'],
        "Countsubmittedleave"=>$myvar['Countsubmittedleave'],
        "HRtype"=>$myvar['HRtype'],
        "VLEL"=>$myvar['VLEL'],
        "vleldays"=>$myvar['vleldays'],
        "Branch"=>$myvar['Branch'],
        "MondayRest"=>$myvar['MondayRest'],//GivenIf($personnel_info_SCGEmpID["MondayRest"]/*Lookup('Personnel Info','MondayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Monday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Monday'),0),
        "TuesdayRest"=>$myvar['TuesdayRest'],//GivenIf($personnel_info_SCGEmpID["TuesdayRest"]/*Lookup('Personnel Info','TuesdayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Tuesday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Tuesday'),0),
        "WednesdayRest"=>$myvar['WednesdayRest'],//GivenIf($personnel_info_SCGEmpID["WednesdayRest"]/*Lookup('Personnel Info','WednesdayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Wednesday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Wednesday'),0),
        "ThursdayRest"=>$myvar['ThursdayRest'],//GivenIf($personnel_info_SCGEmpID["ThursdayRest"]/*Lookup('Personnel Info','ThursdayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Thursday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Thursday'),0),
        "FridayRest"=>$myvar['FridayRest'],//GivenIf($personnel_info_SCGEmpID["FridayRest"]/*Lookup('Personnel Info','FridayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Friday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Friday'),0),
        "SaturdayRest"=>$myvar['SaturdayRest'],//GivenIf($personnel_info_SCGEmpID["SaturdayRest"]/*Lookup('Personnel Info','SaturdayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Saturday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Saturday'),0),
        "SundayRest"=>$myvar['SundayRest'],//GivenIf($personnel_info_SCGEmpID["SundayRest"]/*Lookup('Personnel Info','SundayRest','SCGEmpID',$myvar['EmployeeNo'])*/ == 'Sunday',CountDayIf($myvar['LeaveFrom'],$myvar['LeaveTo'],'Sunday'),0),
        "Approvesby"=>$myvar['Approvesby']
    );
    
    //visibility
    
    
    $formatted_data["visibility"]["VLHours"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["SLHours"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["ELHours"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["LeaveTo"] = $myvar['TypeofLeave'] == 'Maternity Leave' || $myvar['TypeofLeave'] == 'Paternity Leave' || $myvar['DayType'] == 'Whole Day' || $myvar['DayType'] == 'Half Day' ;
    $formatted_data["visibility"]["ApprovedLeave"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["leaveStatus"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["EmployeeLeaveTrackno"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["gender"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["soloparent"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["Pay"] = ($myvar['leaveBalance'] == 0 && $myvar['TypeofLeave'] != 'Earned Rest Day')|| $myvar['EmpStatus']  == 'Probationary' ;
    $formatted_data["visibility"]["CountRest"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["TotalHoursRemoved"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["LeaveReserved"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["EmploymentStatus"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["Unionofficer"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["LeaveEmployeeNumber"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["TotalRestHol"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["SCGEmpID"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["maritalStatus"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["Section"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["EManNum"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["Division"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["DivisionPosNo"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["SectionPosNo"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["DivisionManager"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["SectionManager"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["ANleave"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["Uniontrackno"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["EarnedRestDay"] = $myvar['TypeofLeave'] == 'Vacation Leave' && $myvar['plgroup'] != 'O' ;
    $formatted_data["visibility"]["validateApprover"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["EmpStatus"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["plgroup"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["Countsubmittedleave"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["HRtype"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["VLEL"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["vleldays"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["Branch"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["MondayRest"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["TuesdayRest"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["WednesdayRest"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["ThursdayRest"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["FridayRest"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["SaturdayRest"] = $myvar['Status'] == 'false' ;
    $formatted_data["visibility"]["SundayRest"] = $myvar['Status'] == 'false' ;
    
    
    //return process
    
    $print_debugger .= "\$myvar = ".json_encode($myvar);
    $print_debugger .= "\n\n";
    $print_debugger .= "\$personnel_info_SCGEmpID = ".json_encode($personnel_info_SCGEmpID);
    
    if( ( $selected_fields && count($selected_fields)>0 )&& ( $excluded_fields && count($excluded_fields)>0 ) ){
        foreach ($selected_fields as $key => $value) {
            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];
        }
        foreach ($excluded_fields as $key => $value) {
            unset($new_formatted_data["computed_value"]["". $value .""]);
        }
        $new_formatted_data["__DEBUG"] = $print_debugger;
        return $new_formatted_data;
    }else if($selected_fields && count($selected_fields)>0){
        foreach ($selected_fields as $key => $value) {
            // echo $formatted_data["". $value .""];
            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];
        }
        $new_formatted_data["__DEBUG"] = $print_debugger;
        return $new_formatted_data;
    }else if($excluded_fields && count($excluded_fields)>0){
        foreach ($excluded_fields as $key => $value) {
            unset($formatted_data["computed_value"]["". $value .""]);
        }
        $formatted_data["__DEBUG"] = $print_debugger;
        //print_r($formatted_data);
        return $formatted_data;
    }else{
        return $formatted_data;
    }

    // if(count($relational_fields[$selected_fields])>0){
    //     foreach ($relational_fields[$selected_fields] as $key => $each_fieldname) {
    //         $new_formatted_data["computed_value"]["". $each_fieldname .""] = $formatted_data["computed_value"]["". $each_fieldname .""];
    //     }
    //     return $new_formatted_data; //onchange
    // }else{
    //     return $formatted_data; //onload
    // }
    

}
?>