<?php
function UPPC_PersonnelInformation201($selected_fields,$excluded_fields,$formatted_data,$db){
    //$db->query("");

    //Requestor
    $myvar['Requestor'] = $_POST['Requestor']?:"";
    

    //RequestID
    $myvar['RequestID'] = $_POST['RequestID']?:"";
    

    //Status
    $myvar['Status'] = $_POST['Status']?:"";
    

    //CurrentUser
    $myvar['CurrentUser'] = $_POST['CurrentUser']?:"";
    

    //Department
    $myvar['Department'] = $_POST['Department']?:"";
    

    //TrackNo
    $myvar['TrackNo'] = $_POST['TrackNo']?:"";
    

    //Today
    $myvar['Today'] = date("Y-m-d")?:"";
    

    //Now
    $myvar['Now'] = $_POST['Now']?:"";
    

    //SiteName
    $myvar['SiteName'] = $_POST['SiteName']?:"";
    
    //EmailAddress
    $myvar['EmailAddress'] = $_POST['EmailAddress']?:"";

    //ManNum
    $myvar['ManNum'] = $_POST['ManNum']?:"";
    $myvar['ManNum'] = LookupAuthInfo('id','email',$myvar['EmailAddress']) ;
    

    //DTRType
    $myvar['DTRType'] = $_POST['DTRType']?:"";
    

    //BiometricsID
    $myvar['BiometricsID'] = $_POST['BiometricsID']?:"";
    

    //Surname
    $myvar['Surname'] = $_POST['Surname']?:"";
    

    //FirstName
    $myvar['FirstName'] = $_POST['FirstName']?:"";
    

    //MiddleName
    $myvar['MiddleName'] = $_POST['MiddleName']?:"";
    

    //PersonnelDateOfBirth
    $myvar['PersonnelDateOfBirth'] = $_POST['PersonnelDateOfBirth']?:"";
    

    //PersonnelAge
    $myvar['PersonnelAge'] = $_POST['PersonnelAge']?:"";
    $myvar['PersonnelAge'] = DiffDate( "year" , ThisDate($myvar['PersonnelDateOfBirth']) , $myvar['Today'] ) ; //ThisDate($myvar['PersonnelDateOfBirth']).diffYearsIn($myvar['Today']) ;
    

    //PersonnelGender
    $myvar['PersonnelGender'] = $_POST['PersonnelGender']?:"";
    

    //PersonnelStatus
    $myvar['PersonnelStatus'] = $_POST['PersonnelStatus']?:"";
    

    //PersonnelAddress
    $myvar['PersonnelAddress'] = $_POST['PersonnelAddress']?:"";
    

    //AccountNumber
    $myvar['AccountNumber'] = $_POST['AccountNumber']?:"";
    

    //Contact_Number
    $myvar['Contact_Number'] = $_POST['Contact_Number']?:"";
    

    //Drivers_License_Number
    $myvar['Drivers_License_Number'] = $_POST['Drivers_License_Number']?:"";
    

    //weight
    $myvar['weight'] = $_POST['weight']?:"";
    

    //height
    $myvar['height'] = $_POST['height']?:"";
    

    //religion
    $myvar['religion'] = $_POST['religion']?:"";
    

    //citizenship
    $myvar['citizenship'] = $_POST['citizenship']?:"";
    

    //IntellicareNum
    $myvar['IntellicareNum'] = $_POST['IntellicareNum']?:"";
    

    //SSSNumber
    $myvar['SSSNumber'] = $_POST['SSSNumber']?:"";
    

    //TinNum
    $myvar['TinNum'] = $_POST['TinNum']?:"";
    

    //HDMFnumber
    $myvar['HDMFnumber'] = $_POST['HDMFnumber']?:"";
    

    //NHIPnumber
    $myvar['NHIPnumber'] = $_POST['NHIPnumber']?:"";
    

    //PagIbigNo
    $myvar['PagIbigNo'] = $_POST['PagIbigNo']?:"";
    

    //RTNNo
    $myvar['RTNNo'] = $_POST['RTNNo']?:"";
    

    //TimeStamp
    $myvar['TimeStamp'] = $_POST['TimeStamp']?:"";


    //timeStamp
    $myvar['timeStamp'] = $_POST['timeStamp']?:"";
    $myvar['timeStamp'] = GivenIf(($myvar['RequestID'] !=0 && $myvar['timeStamp']== ''),$myvar['TimeStamp'],$myvar['timeStamp']) ;


    //PersonnelDependents
    $myvar['PersonnelDependents'] = $_POST['PersonnelDependents']?:"";
    $myvar['PersonnelDependents'] = GivenIf(Total( 'Dependents Maintenance' , 'DepNo' , 'timeStamp', '=', $myvar['timeStamp']) > 4,4,Total( 'Dependents Maintenance' , 'DepNo' , 'timeStamp', '=', $myvar['timeStamp'])) ;
    

    //Bachelorlevel
    $myvar['Bachelorlevel'] = $_POST['Bachelorlevel']?:"";
    

    //BachelorDegree
    $myvar['BachelorDegree'] = $_POST['BachelorDegree']?:"";
    

    //BachelorDepartment
    $myvar['BachelorDepartment'] = $_POST['BachelorDepartment']?:"";
    

    //BachelorMajor
    $myvar['BachelorMajor'] = $_POST['BachelorMajor']?:"";
    

    //BachelorInstitute
    $myvar['BachelorInstitute'] = $_POST['BachelorInstitute']?:"";
    

    //BachelorGPA
    $myvar['BachelorGPA'] = $_POST['BachelorGPA']?:"";
    

    //BachelorYear
    $myvar['BachelorYear'] = $_POST['BachelorYear']?:"";
    

    //Masterlevel
    $myvar['Masterlevel'] = $_POST['Masterlevel']?:"";
    

    //MasterDegree
    $myvar['MasterDegree'] = $_POST['MasterDegree']?:"";
    

    //MasterDepartment
    $myvar['MasterDepartment'] = $_POST['MasterDepartment']?:"";
    

    //MasterMajor
    $myvar['MasterMajor'] = $_POST['MasterMajor']?:"";
    

    //MajorInstitute
    $myvar['MajorInstitute'] = $_POST['MajorInstitute']?:"";
    

    //MasterGPA
    $myvar['MasterGPA'] = $_POST['MasterGPA']?:"";
    

    //MasterYear
    $myvar['MasterYear'] = $_POST['MasterYear']?:"";
    

    //SecondaryLevel
    $myvar['SecondaryLevel'] = $_POST['SecondaryLevel']?:"";
    

    //SecondaryDegree
    $myvar['SecondaryDegree'] = $_POST['SecondaryDegree']?:"";
    

    //SecondaryInstitute
    $myvar['SecondaryInstitute'] = $_POST['SecondaryInstitute']?:"";
    

    //SecondaryGPA
    $myvar['SecondaryGPA'] = $_POST['SecondaryGPA']?:"";
    

    //SecondaryYear
    $myvar['SecondaryYear'] = $_POST['SecondaryYear']?:"";
    

    //PrimaryLevel
    $myvar['PrimaryLevel'] = $_POST['PrimaryLevel']?:"";
    

    //PrimaryDegree
    $myvar['PrimaryDegree'] = $_POST['PrimaryDegree']?:"";
    

    //PrimaryInstitute
    $myvar['PrimaryInstitute'] = $_POST['PrimaryInstitute']?:"";
    

    //PrimaryGPA
    $myvar['PrimaryGPA'] = $_POST['PrimaryGPA']?:"";
    

    //PrimaryYear
    $myvar['PrimaryYear'] = $_POST['PrimaryYear']?:"";
    

    //PersonnelName
    $myvar['PersonnelName'] = $_POST['PersonnelName']?:"";
    $myvar['PersonnelName'] = LookupAuthInfo('display_name','email',$myvar['EmailAddress']) ;
    

    //Salutation
    $myvar['Salutation'] = $_POST['Salutation']?:"";
    

    //BloodType
    $myvar['BloodType'] = $_POST['BloodType']?:"";
    

    //SpouseName
    $myvar['SpouseName'] = $_POST['SpouseName']?:"";
    

    //SpouseLname
    $myvar['SpouseLname'] = $_POST['SpouseLname']?:"";
    

    //SpouseBday
    $myvar['SpouseBday'] = $_POST['SpouseBday']?:"";
    

    //NoofChildren
    $myvar['NoofChildren'] = $_POST['NoofChildren']?:"";
    

    //SoloParent
    $myvar['SoloParent'] = $_POST['SoloParent']?:"";
    

    //EmploymentStatus
    $myvar['EmploymentStatus'] = $_POST['EmploymentStatus']?:"";
    // $myvar['EmploymentStatus'] =  ;
    

    //PersonnelStat
    $myvar['PersonnelStat'] = $_POST['PersonnelStat']?:"";
    

    //PersonnelDateHired
    $myvar['PersonnelDateHired'] = $_POST['PersonnelDateHired']?:"";
    

    //DateSeparated
    $myvar['DateSeparated'] = $_POST['DateSeparated']?:"";
    

    //CutOffType
    $myvar['CutOffType'] = $_POST['CutOffType']?:"";
    

    //positionNumber
    $myvar['positionNumber'] = $_POST['positionNumber']?:"";
    

    //jobDescription
    $myvar['jobDescription'] = $_POST['jobDescription']?:"";
    $myvar['jobDescription'] = Lookup( 'Position Information' , 'JobDesc' , 'TrackNo' , $myvar['positionNumber'] ) ;
    

    //PersonnelPosition
    $myvar['PersonnelPosition'] = $_POST['PersonnelPosition']?:"";
    $myvar['PersonnelPosition'] = Lookup( 'Position Information' , 'jobTitle' , 'TrackNo' , $myvar['positionNumber'] ) ;
    

    //reportToNo
    $myvar['reportToNo'] = $_POST['reportToNo']?:"";
    $myvar['reportToNo'] = Lookup( 'Position Information' , 'reportTo' , 'TrackNo' , $myvar['positionNumber'] ) ;
    

    //PositionLevel
    $myvar['PositionLevel'] = $_POST['PositionLevel']?:"";
    

    //JobLevel
    $myvar['JobLevel'] = $_POST['JobLevel']?:"";
    

    //JobLevelDate
    $myvar['JobLevelDate'] = $_POST['JobLevelDate']?:"";
    

    //PositiolLevelDate
    $myvar['PositiolLevelDate'] = $_POST['PositiolLevelDate']?:"";
    

    //JobClassification
    $myvar['JobClassification'] = $_POST['JobClassification']?:"";
    // $myvar['JobClassification'] =  ;
    

    //Section
    $myvar['Section'] = $_POST['Section']?:"";
    

    //Division
    $myvar['Division'] = $_POST['Division']?:"";
    

    //Unionofficer
    $myvar['Unionofficer'] = $_POST['Unionofficer']?:"";
    

    //reportingTo
    $myvar['reportingTo'] = $_POST['reportingTo']?:"";
    $myvar['reportingTo'] = Lookup( 'Personnel Info' ,'PersonnelName' , 'positionNumber' , $myvar['reportToNo'] ) ;
    

    //DepartmentID
    $myvar['DepartmentID'] = $_POST['DepartmentID']?:"";
    

    //EmployeeType
    $myvar['EmployeeType'] = $_POST['EmployeeType']?:"";
    

    //PLGroup
    $myvar['PLGroup'] = $_POST['PLGroup']?:"";
    $myvar['PLGroup'] = GivenIf(($myvar['PositionLevel'] == 'S1' || $myvar['PositionLevel'] == 'S2' || $myvar['PositionLevel'] == 'S3' || $myvar['PositionLevel'] == 'S4') , 'S' , GivenIf(($myvar['PositionLevel'] == 'M1' || $myvar['PositionLevel'] == 'M2'), 'M' ,  GivenIf(($myvar['PositionLevel'] == 'PC1' ||$myvar['PositionLevel'] == 'PC2' || $myvar['PositionLevel'] == 'PC3' ||$myvar['PositionLevel'] == 'PC4' ||$myvar['PositionLevel'] == 'PC5' ||$myvar['PositionLevel'] == 'PC6' ||$myvar['PositionLevel'] == 'PC7' ||$myvar['PositionLevel'] == 'PC8' ||$myvar['PositionLevel'] == 'PC9'), 'O' , $myvar['PLGroup']))) ;
    

    //PN
    $myvar['PN'] = $_POST['PN']?:"";
    

    //PositionPSCode
    $myvar['PositionPSCode'] = $_POST['PositionPSCode']?:"";
    

    //RPCODE
    $myvar['RPCODE'] = $_POST['RPCODE']?:"";
    

    //Branch
    $myvar['Branch'] = $_POST['Branch']?:"";
    

    //PersonnelDepartment
    $myvar['PersonnelDepartment'] = $_POST['PersonnelDepartment']?:"";
    

    //WorkLocation
    $myvar['WorkLocation'] = $_POST['WorkLocation']?:"";
    $myvar['WorkLocation'] = Lookup('Branch Maintenance','BranchName','BranchCode',$myvar['WorkLocation']) ;
    

    //CostCenter
    $myvar['CostCenter'] = $_POST['CostCenter']?:"";
    

    //Subarea
    $myvar['Subarea'] = $_POST['Subarea']?:"";
    

    //EligibilityGroup
    $myvar['EligibilityGroup'] = $_POST['EligibilityGroup']?:"";
    

    //Paygroup
    $myvar['Paygroup'] = $_POST['Paygroup']?:"";
    

    //TIPosition
    $myvar['TIPosition'] = $_POST['TIPosition']?:"";
    

    //esytotalmonths
    $myvar['esytotalmonths'] = $_POST['esytotalmonths']?:"";


    //esytotalyears
    $myvar['esytotalyears'] = $_POST['esytotalyears']?:"";


    //ESYyears
    $myvar['ESYyears'] = $_POST['ESYyears']?:"";
    $myvar['ESYyears'] = ($myvar['esytotalyears']*365.25)+($myvar['esytotalmonths']*30.4375) ;
    
    
    //HRType
    $myvar['HRType'] = $_POST['HRType']?:"";
    

    //sequence1
    $myvar['sequence1'] = $_POST['sequence1']?:"";
    

    //TORStat
    $myvar['TORStat'] = $_POST['TORStat']?:"";
    

    //TOR
    $myvar['TOR'] = $_POST['TOR']?:"";
    

    //sequence2
    $myvar['sequence2'] = $_POST['sequence2']?:"";
    

    //DiplomaStat
    $myvar['DiplomaStat'] = $_POST['DiplomaStat']?:"";
    

    //DiplomaCertification
    $myvar['DiplomaCertification'] = $_POST['DiplomaCertification']?:"";
    

    //sequence3
    $myvar['sequence3'] = $_POST['sequence3']?:"";
    

    //PRCStat
    $myvar['PRCStat'] = $_POST['PRCStat']?:"";
    

    //PRCid
    $myvar['PRCid'] = $_POST['PRCid']?:"";
    

    //sequence4
    $myvar['sequence4'] = $_POST['sequence4']?:"";
    

    //BirthCertificateStat
    $myvar['BirthCertificateStat'] = $_POST['BirthCertificateStat']?:"";
    

    //BirthCertificate
    $myvar['BirthCertificate'] = $_POST['BirthCertificate']?:"";
    

    //sequence5
    $myvar['sequence5'] = $_POST['sequence5']?:"";
    

    //MCStat
    $myvar['MCStat'] = $_POST['MCStat']?:"";
    

    //MarriageContract
    $myvar['MarriageContract'] = $_POST['MarriageContract']?:"";
    

    //sequence6
    $myvar['sequence6'] = $_POST['sequence6']?:"";
    

    //PagibigStatus
    $myvar['PagibigStatus'] = $_POST['PagibigStatus']?:"";
    

    //BCCert
    $myvar['BCCert'] = $_POST['BCCert']?:"";
    

    //sequence7
    $myvar['sequence7'] = $_POST['sequence7']?:"";
    

    //BCCCStat
    $myvar['BCCCStat'] = $_POST['BCCCStat']?:"";
    

    //BCD
    $myvar['BCD'] = $_POST['BCD']?:"";
    

    //sequence8
    $myvar['sequence8'] = $_POST['sequence8']?:"";
    

    //NBIStat
    $myvar['NBIStat'] = $_POST['NBIStat']?:"";
    

    //nbiclearance
    $myvar['nbiclearance'] = $_POST['nbiclearance']?:"";
    

    //sequence9
    $myvar['sequence9'] = $_POST['sequence9']?:"";
    

    //SSSSTat
    $myvar['SSSSTat'] = $_POST['SSSSTat']?:"";
    

    //sss
    $myvar['sss'] = $_POST['sss']?:"";
    

    //sequence10
    $myvar['sequence10'] = $_POST['sequence10']?:"";
    

    //BIRStat
    $myvar['BIRStat'] = $_POST['BIRStat']?:"";
    

    //tindocument
    $myvar['tindocument'] = $_POST['tindocument']?:"";
    

    //sequence11
    $myvar['sequence11'] = $_POST['sequence11']?:"";
    

    //PhilhealthStat
    $myvar['PhilhealthStat'] = $_POST['PhilhealthStat']?:"";
    

    //philhealth
    $myvar['philhealth'] = $_POST['philhealth']?:"";
    

    //sequence12
    $myvar['sequence12'] = $_POST['sequence12']?:"";
    

    //PagIbigStat
    $myvar['PagIbigStat'] = $_POST['PagIbigStat']?:"";
    

    //pagibigdocument
    $myvar['pagibigdocument'] = $_POST['pagibigdocument']?:"";
    

    //sequence13
    $myvar['sequence13'] = $_POST['sequence13']?:"";
    

    //LIEFStat
    $myvar['LIEFStat'] = $_POST['LIEFStat']?:"";
    

    //LIEF
    $myvar['LIEF'] = $_POST['LIEF']?:"";
    

    //sequence14
    $myvar['sequence14'] = $_POST['sequence14']?:"";
    

    //UppcIDStat
    $myvar['UppcIDStat'] = $_POST['UppcIDStat']?:"";
    

    //UPPCID
    $myvar['UPPCID'] = $_POST['UPPCID']?:"";
    

    //sequence15
    $myvar['sequence15'] = $_POST['sequence15']?:"";
    

    //AtmStat
    $myvar['AtmStat'] = $_POST['AtmStat']?:"";
    

    //ATMCard
    $myvar['ATMCard'] = $_POST['ATMCard']?:"";
    

    //sequence16
    $myvar['sequence16'] = $_POST['sequence16']?:"";
    

    //RSStat
    $myvar['RSStat'] = $_POST['RSStat']?:"";
    

    //residenceSketch
    $myvar['residenceSketch'] = $_POST['residenceSketch']?:"";
    

    //sequence17
    $myvar['sequence17'] = $_POST['sequence17']?:"";
    

    //LPEStat
    $myvar['LPEStat'] = $_POST['LPEStat']?:"";
    

    //LPE
    $myvar['LPE'] = $_POST['LPE']?:"";
    

    //sequence18
    $myvar['sequence18'] = $_POST['sequence18']?:"";
    

    //IDPicStatus
    $myvar['IDPicStatus'] = $_POST['IDPicStatus']?:"";
    

    //IDPic
    $myvar['IDPic'] = $_POST['IDPic']?:"";
    

    //WorkShift
    $myvar['WorkShift'] = $_POST['WorkShift']?:"";
    

    //WorkTimeIn
    $myvar['WorkTimeIn'] = $_POST['WorkTimeIn']?:"";
    

    //WorkTimeOut
    $myvar['WorkTimeOut'] = $_POST['WorkTimeOut']?:"";
    

    //MondayRest
    $myvar['MondayRest'] = $_POST['MondayRest']?:"";
    

    //FridayRest
    $myvar['FridayRest'] = $_POST['FridayRest']?:"";
    

    //TuesdayRest
    $myvar['TuesdayRest'] = $_POST['TuesdayRest']?:"";
    

    //SaturdayRest
    $myvar['SaturdayRest'] = $_POST['SaturdayRest']?:"";
    

    //WednesdayRest
    $myvar['WednesdayRest'] = $_POST['WednesdayRest']?:"";
    

    //SundayRest
    $myvar['SundayRest'] = $_POST['SundayRest']?:"";
    

    //ThursdayRest
    $myvar['ThursdayRest'] = $_POST['ThursdayRest']?:"";
    

    //PagibigLoanType
    $myvar['PagibigLoanType'] = $_POST['PagibigLoanType']?:"";
    

    //ApplNo
    $myvar['ApplNo'] = $_POST['ApplNo']?:"";
    

    //TotalPagibig
    $myvar['TotalPagibig'] = $_POST['TotalPagibig']?:"";
    

    //PagIbigLoan
    $myvar['PagIbigLoan'] = $_POST['PagIbigLoan']?:"";
    

    //DateofLoanHDMF
    $myvar['DateofLoanHDMF'] = $_POST['DateofLoanHDMF']?:"";
    

    //StartLoanHDMF
    $myvar['StartLoanHDMF'] = $_POST['StartLoanHDMF']?:"";
    

    //PagIbigLoanEnd
    $myvar['PagIbigLoanEnd'] = $_POST['PagIbigLoanEnd']?:"";
    

    //TotalHousingLoan
    $myvar['TotalHousingLoan'] = $_POST['TotalHousingLoan']?:"";
    

    //PhilhealthLoan
    $myvar['PhilhealthLoan'] = $_POST['PhilhealthLoan']?:"";
    

    //DateOfLoanHDMFHL
    $myvar['DateOfLoanHDMFHL'] = $_POST['DateOfLoanHDMFHL']?:"";
    

    //StartOfLoanHDMFHL
    $myvar['StartOfLoanHDMFHL'] = $_POST['StartOfLoanHDMFHL']?:"";
    

    //PhilhealthEnd
    $myvar['PhilhealthEnd'] = $_POST['PhilhealthEnd']?:"";
    

    //SSSLoanType
    $myvar['SSSLoanType'] = $_POST['SSSLoanType']?:"";
    

    //TotalSSS
    $myvar['TotalSSS'] = $_POST['TotalSSS']?:"";
    

    //SSSLoan
    $myvar['SSSLoan'] = $_POST['SSSLoan']?:"";
    

    //DateOfLoanSSS
    $myvar['DateOfLoanSSS'] = $_POST['DateOfLoanSSS']?:"";
    

    //StartOfLoanSSS
    $myvar['StartOfLoanSSS'] = $_POST['StartOfLoanSSS']?:"";
    

    //SSSLoanEnd
    $myvar['SSSLoanEnd'] = $_POST['SSSLoanEnd']?:"";
    

    //TotalCA
    $myvar['TotalCA'] = $_POST['TotalCA']?:"";
    

    //CashAdvance
    $myvar['CashAdvance'] = $_POST['CashAdvance']?:"";
    

    //DateOfLoanCA
    $myvar['DateOfLoanCA'] = $_POST['DateOfLoanCA']?:"";
    

    //StartOfLoanCA
    $myvar['StartOfLoanCA'] = $_POST['StartOfLoanCA']?:"";
    

    //CAEnd
    $myvar['CAEnd'] = $_POST['CAEnd']?:"";
    

    //SSSContri
    $myvar['SSSContri'] = $_POST['SSSContri']?:"";
    // $myvar['SSSContri'] =  ;
    

    //SSSContriER
    $myvar['SSSContriER'] = $_POST['SSSContriER']?:"";
    

    //SSSContriType
    $myvar['SSSContriType'] = $_POST['SSSContriType']?:"";
    

    //PhilHealthContri
    $myvar['PhilHealthContri'] = $_POST['PhilHealthContri']?:"";
    // $myvar['PhilHealthContri'] =  ;
    

    //PhilhealthContriER
    $myvar['PhilhealthContriER'] = $_POST['PhilhealthContriER']?:"";
    

    //PhilhealthContriType
    $myvar['PhilhealthContriType'] = $_POST['PhilhealthContriType']?:"";
    

    //PagIbigContri
    $myvar['PagIbigContri'] = $_POST['PagIbigContri']?:"";
    $myvar['PagIbigContri'] = GivenIf(@GovtDeducType == 'Based on Gross Pay', 0.00, $myvar['PagIbigContri']) ;
    

    //PagIbigContriER
    $myvar['PagIbigContriER'] = $_POST['PagIbigContriER']?:"";
    

    //PagIbigContriType
    $myvar['PagIbigContriType'] = $_POST['PagIbigContriType']?:"";
    

    //FixedDeducName1
    $myvar['FixedDeducName1'] = $_POST['FixedDeducName1']?:"";
    

    //FixedDeducAmt1
    $myvar['FixedDeducAmt1'] = $_POST['FixedDeducAmt1']?:"";
    

    //FixedDeducDateOfLoan1
    $myvar['FixedDeducDateOfLoan1'] = $_POST['FixedDeducDateOfLoan1']?:"";
    

    //FixedDeducStartDate1
    $myvar['FixedDeducStartDate1'] = $_POST['FixedDeducStartDate1']?:"";
    

    //FixedDeducEndDate1
    $myvar['FixedDeducEndDate1'] = $_POST['FixedDeducEndDate1']?:"";
    

    //FixedDeducName2
    $myvar['FixedDeducName2'] = $_POST['FixedDeducName2']?:"";
    

    //FixedDeducAmt2
    $myvar['FixedDeducAmt2'] = $_POST['FixedDeducAmt2']?:"";
    

    //FixedDeducDateOfLoan2
    $myvar['FixedDeducDateOfLoan2'] = $_POST['FixedDeducDateOfLoan2']?:"";
    

    //FixedDeducStartDate2
    $myvar['FixedDeducStartDate2'] = $_POST['FixedDeducStartDate2']?:"";
    

    //FixedDeducEndDate2
    $myvar['FixedDeducEndDate2'] = $_POST['FixedDeducEndDate2']?:"";
    

    //FixedDeducName3
    $myvar['FixedDeducName3'] = $_POST['FixedDeducName3']?:"";
    

    //FixedDeducAmt3
    $myvar['FixedDeducAmt3'] = $_POST['FixedDeducAmt3']?:"";
    

    //FixedDeducDateOfLoan3
    $myvar['FixedDeducDateOfLoan3'] = $_POST['FixedDeducDateOfLoan3']?:"";
    

    //FixedDeducStartDate3
    $myvar['FixedDeducStartDate3'] = $_POST['FixedDeducStartDate3']?:"";
    

    //FixedDeducEndDate3
    $myvar['FixedDeducEndDate3'] = $_POST['FixedDeducEndDate3']?:"";
    

    //FixedDeducName4
    $myvar['FixedDeducName4'] = $_POST['FixedDeducName4']?:"";
    

    //FixedDeducAmt4
    $myvar['FixedDeducAmt4'] = $_POST['FixedDeducAmt4']?:"";
    

    //FixedDeducDateOfLoan4
    $myvar['FixedDeducDateOfLoan4'] = $_POST['FixedDeducDateOfLoan4']?:"";
    

    //FixedDeducStartDate4
    $myvar['FixedDeducStartDate4'] = $_POST['FixedDeducStartDate4']?:"";
    

    //FixedDeducEndDate4
    $myvar['FixedDeducEndDate4'] = $_POST['FixedDeducEndDate4']?:"";
    

    //FixedDeducName5
    $myvar['FixedDeducName5'] = $_POST['FixedDeducName5']?:"";
    

    //FixedDeducAmt5
    $myvar['FixedDeducAmt5'] = $_POST['FixedDeducAmt5']?:"";
    

    //FixedDeducDateOfLoan5
    $myvar['FixedDeducDateOfLoan5'] = $_POST['FixedDeducDateOfLoan5']?:"";
    

    //FixedDeducStartDate5
    $myvar['FixedDeducStartDate5'] = $_POST['FixedDeducStartDate5']?:"";
    

    //FixedDeducEndDate5
    $myvar['FixedDeducEndDate5'] = $_POST['FixedDeducEndDate5']?:"";
    

    //FixedDeductionType
    $myvar['FixedDeductionType'] = $_POST['FixedDeductionType']?:"";
    

    //searchtraining
    $myvar['searchtraining'] = $_POST['searchtraining']?:"";
    $myvar['searchtraining'] = StrConcat($myvar['ManNum'],'YES') ;
    

    //DeMinimis
    $myvar['DeMinimis'] = $_POST['DeMinimis']?:"";
    

    //Longevity
    $myvar['Longevity'] = $_POST['Longevity']?:"";
    // $myvar['Longevity'] =  ;
    

    //Cola
    $myvar['Cola'] = $_POST['Cola']?:"";
    $myvar['Cola'] = @ColaPerMonth / 2 ;
    

    //TempTransAllw
    $myvar['TempTransAllw'] = $_POST['TempTransAllw']?:"";
    

    //FixedAllwName1
    $myvar['FixedAllwName1'] = $_POST['FixedAllwName1']?:"";
    

    //FixedAllwAmt1
    $myvar['FixedAllwAmt1'] = $_POST['FixedAllwAmt1']?:"";
    

    //FixedAllwName2
    $myvar['FixedAllwName2'] = $_POST['FixedAllwName2']?:"";
    

    //FixedAllwAmt2
    $myvar['FixedAllwAmt2'] = $_POST['FixedAllwAmt2']?:"";
    

    //FixedAllwName3
    $myvar['FixedAllwName3'] = $_POST['FixedAllwName3']?:"";
    

    //FixedAllwAmt3
    $myvar['FixedAllwAmt3'] = $_POST['FixedAllwAmt3']?:"";
    

    //FixedAllwName4
    $myvar['FixedAllwName4'] = $_POST['FixedAllwName4']?:"";
    

    //FixedAllwAmt4
    $myvar['FixedAllwAmt4'] = $_POST['FixedAllwAmt4']?:"";
    

    //FixedAllwName5
    $myvar['FixedAllwName5'] = $_POST['FixedAllwName5']?:"";
    

    //FixedAllwAmt5
    $myvar['FixedAllwAmt5'] = $_POST['FixedAllwAmt5']?:"";
    

    //VacationLeaveTotal
    $myvar['VacationLeaveTotal'] = $_POST['VacationLeaveTotal']?:"";
    $myvar['VacationLeaveTotal'] = GivenIf(ThisDate($myvar['Today']).formatDate('Y')== ThisDate($myvar['PersonnelDateHired']).formatDate('Y'),GetRound((1.25*(ThisDate(StrConcat(ThisDate($myvar['Today']).formatDate('Y'), '-12-31')).diffMonthsIn( $myvar['PersonnelDateHired']))),0),(15)) ;
    

    //VLReserved
    $myvar['VLReserved'] = $_POST['VLReserved']?:"";
    

    //VLRequested
    $myvar['VLRequested'] = $_POST['VLRequested']?:"";
    

    //VLeft
    $myvar['VLeft'] = $_POST['VLeft']?:"";
    $myvar['VLeft'] = GivenIf($myvar['VLeft'] == '',$myvar['VacationLeaveTotal'],$myvar['VLeft']) ;
    

    //SickLeaveTotal
    $myvar['SickLeaveTotal'] = $_POST['SickLeaveTotal']?:"";
    $myvar['SickLeaveTotal'] = GivenIf(ThisDate($myvar['Today']).formatDate('Y')== ThisDate($myvar['PersonnelDateHired']).formatDate('Y'),GetRound((1.25*(ThisDate(StrConcat(ThisDate($myvar['Today']).formatDate('Y'), '-12-31')).diffMonthsIn( $myvar['PersonnelDateHired']))),0),(15)) ;
    

    //SLReserved
    $myvar['SLReserved'] = $_POST['SLReserved']?:"";
    

    //SLRequested
    $myvar['SLRequested'] = $_POST['SLRequested']?:"";
    

    //SLeft
    $myvar['SLeft'] = $_POST['SLeft']?:"";
    $myvar['SLeft'] = GivenIf($myvar['SLeft'] == '',$myvar['SickLeaveTotal'],$myvar['SLeft']) ;
    

    //emergencyTotal
    $myvar['emergencyTotal'] = $_POST['emergencyTotal']?:"";
    

    //ELReserved
    $myvar['ELReserved'] = $_POST['ELReserved']?:"";
    

    //emergencyrequested
    $myvar['emergencyrequested'] = $_POST['emergencyrequested']?:"";
    

    //EmergencyLeft
    $myvar['EmergencyLeft'] = $_POST['EmergencyLeft']?:"";
    $myvar['EmergencyLeft'] = GivenIf($myvar['EmergencyLeft'] == '',$myvar['emergencyTotal'],$myvar['EmergencyLeft']) ;
    

    //MaternityTotal
    $myvar['MaternityTotal'] = $_POST['MaternityTotal']?:"";
    // $myvar['MaternityTotal'] =  ;
    

    //MLReserved
    $myvar['MLReserved'] = $_POST['MLReserved']?:"";
    

    //maternityRequested
    $myvar['maternityRequested'] = $_POST['maternityRequested']?:"";
    

    //maternityLeft
    $myvar['maternityLeft'] = $_POST['maternityLeft']?:"";
    $myvar['maternityLeft'] = GivenIf($myvar['maternityLeft'] == '',$myvar['MaternityTotal'],$myvar['maternityLeft']) ;
    

    //paterTotal
    $myvar['paterTotal'] = $_POST['paterTotal']?:"";
    // $myvar['paterTotal'] =  ;
    

    //PLReserved
    $myvar['PLReserved'] = $_POST['PLReserved']?:"";
    

    //paterRequested
    $myvar['paterRequested'] = $_POST['paterRequested']?:"";
    

    //paterLeft
    $myvar['paterLeft'] = $_POST['paterLeft']?:"";
    $myvar['paterLeft'] = GivenIf($myvar['paterLeft'] == '',$myvar['paterTotal'],$myvar['paterLeft']) ;
    

    //SoloTotal
    $myvar['SoloTotal'] = $_POST['SoloTotal']?:"";
    

    //SoloReserved
    $myvar['SoloReserved'] = $_POST['SoloReserved']?:"";
    

    //SoloRequested
    $myvar['SoloRequested'] = $_POST['SoloRequested']?:"";
    

    //SoloLeft
    $myvar['SoloLeft'] = $_POST['SoloLeft']?:"";
    $myvar['SoloLeft'] = GivenIf($myvar['SoloLeft'] == '',$myvar['SoloTotal'],$myvar['SoloLeft']) ;
    

    //ULTotal
    $myvar['ULTotal'] = $_POST['ULTotal']?:"";
    

    //ULReserved
    $myvar['ULReserved'] = $_POST['ULReserved']?:"";
    

    //ULRequested
    $myvar['ULRequested'] = $_POST['ULRequested']?:"";
    

    //ULLeft
    $myvar['ULLeft'] = $_POST['ULLeft']?:"";
    $myvar['ULLeft'] = GivenIf($myvar['ULLeft']== '',$myvar['ULTotal'],$myvar['ULLeft']) ;
    

    //ERdreserved
    $myvar['ERdreserved'] = $_POST['ERdreserved']?:"";
    

    //ERdrequested
    $myvar['ERdrequested'] = $_POST['ERdrequested']?:"";
    

    //ERDay
    $myvar['ERDay'] = $_POST['ERDay']?:"";
    

    //DaysPerMonth
    $myvar['DaysPerMonth'] = $_POST['DaysPerMonth']?:"";
    $myvar['DaysPerMonth'] = StrConcat('26.17') ;
    

    //DaysPerCutOff
    $myvar['DaysPerCutOff'] = $_POST['DaysPerCutOff']?:"";
    $myvar['DaysPerCutOff'] = GetRound(($myvar['DaysPerMonth'] / 2),2) ;
    

    //BasicPerMonth
    $myvar['BasicPerMonth'] = $_POST['BasicPerMonth']?:"";
    // $myvar['BasicPerMonth'] =  ;
    

    //BasicPerCutOff
    $myvar['BasicPerCutOff'] = $_POST['BasicPerCutOff']?:"";
    $myvar['BasicPerCutOff'] = GetRound(($myvar['BasicPerMonth'] / 2),2) ;
    

    //BasicPerDay
    $myvar['BasicPerDay'] = $_POST['BasicPerDay']?:"";
    $myvar['BasicPerDay'] = GetRound(($myvar['BasicPerMonth'] / $myvar['DaysPerMonth']),2) ;
    

    //BasicPerHour
    $myvar['BasicPerHour'] = $_POST['BasicPerHour']?:"";
    $myvar['BasicPerHour'] = GetRound(($myvar['BasicPerDay'] / 8),2) ;
    

    //TaxShield
    $myvar['TaxShield'] = $_POST['TaxShield']?:"";
    

    //SAAdjDate
    $myvar['SAAdjDate'] = $_POST['SAAdjDate']?:"";
    

    //AppTrackNo
    $myvar['AppTrackNo'] = $_POST['AppTrackNo']?:"";
    

    //firstAppraisalUpdate
    $myvar['firstAppraisalUpdate'] = $_POST['firstAppraisalUpdate']?:"";
    

    //JETimeStamp
    $myvar['JETimeStamp'] = $_POST['JETimeStamp']?:"";
    

    //SampleTrackNo
    $myvar['SampleTrackNo'] = $_POST['SampleTrackNo']?:"";
    $myvar['SampleTrackNo'] = $myvar['TrackNo'] ;
    

    //OnePageTrackNo
    $myvar['OnePageTrackNo'] = $_POST['OnePageTrackNo']?:"";
    $myvar['OnePageTrackNo'] = Lookup( 'One Page Resume Form' , 'TrackNo' , 'Employee_Number' , $myvar['ManNum'] ) ;
    

    //headCount
    $myvar['headCount'] = $_POST['headCount']?:"";
    // $myvar['headCount'] =  ;
    

    //LastDay
    $myvar['LastDay'] = $_POST['LastDay']?:"";
    

    //SCGEmpID
    $myvar['SCGEmpID'] = $_POST['SCGEmpID']?:"";
    

    //CountSb
    $myvar['CountSb'] = $_POST['CountSb']?:"";
    // $myvar['CountSb'] =  ;
    

    //CountProbi
    $myvar['CountProbi'] = $_POST['CountProbi']?:"";
    

    //PersonnelEditor
    $myvar['PersonnelEditor'] = $_POST['PersonnelEditor']?:"";
    

    //SectionWithCode
    $myvar['SectionWithCode'] = $_POST['SectionWithCode']?:"";
    $myvar['SectionWithCode'] = GivenIf(Lookup('Section Maintenance','SectionNo','SectionName',$myvar['Section']) != '',StrConcat(Lookup('Section Maintenance','SectionNo','SectionName',$myvar['Section']),' ',$myvar['Section']),$myvar['Section']) ;
    

    //picklist_604
    $myvar['picklist_604'] = $_POST['picklist_604']?:"";
    

    //dropdown_605
    $myvar['dropdown_605'] = $_POST['dropdown_605']?:"";
    

    //select_606
    $myvar['select_606'] = $_POST['select_606']?:"";
    

    $formatted_data["computed_value"]["ManNum"] = $myvar["ManNum"] ;
    $formatted_data["computed_value"]["PersonnelAge"] = $myvar["PersonnelAge"] ;
    $formatted_data["computed_value"]["PersonnelDependents"] = $myvar["PersonnelDependents"] ;
    $formatted_data["computed_value"]["PersonnelName"] = $myvar["PersonnelName"] ;
    $formatted_data["computed_value"]["jobDescription"] = $myvar["jobDescription"] ;
    $formatted_data["computed_value"]["PersonnelPosition"] = $myvar["PersonnelPosition"] ;
    $formatted_data["computed_value"]["reportToNo"] = $myvar["reportToNo"] ;
    $formatted_data["computed_value"]["JobClassification"] = $myvar["JobClassification"] ;
    $formatted_data["computed_value"]["reportingTo"] = $myvar["reportingTo"] ;
    $formatted_data["computed_value"]["PLGroup"] = $myvar["PLGroup"] ;
    $formatted_data["computed_value"]["WorkLocation"] = $myvar["WorkLocation"] ;
    $formatted_data["computed_value"]["ESYyears"] = $myvar["ESYyears"] ;
    $formatted_data["computed_value"]["SSSContri"] = $myvar["SSSContri"] ;
    $formatted_data["computed_value"]["PhilHealthContri"] = $myvar["PhilHealthContri"] ;
    $formatted_data["computed_value"]["PagIbigContri"] = $myvar["PagIbigContri"] ;
    $formatted_data["computed_value"]["searchtraining"] = $myvar["searchtraining"] ;
    $formatted_data["computed_value"]["Longevity"] = $myvar["Longevity"] ;
    $formatted_data["computed_value"]["Cola"] = $myvar["Cola"] ;
    $formatted_data["computed_value"]["VacationLeaveTotal"] = $myvar["VacationLeaveTotal"] ;
    $formatted_data["computed_value"]["VLeft"] = $myvar["VLeft"] ;
    $formatted_data["computed_value"]["SickLeaveTotal"] = $myvar["SickLeaveTotal"] ;
    $formatted_data["computed_value"]["SLeft"] = $myvar["SLeft"] ;
    $formatted_data["computed_value"]["EmergencyLeft"] = $myvar["EmergencyLeft"] ;
    $formatted_data["computed_value"]["MaternityTotal"] = $myvar["MaternityTotal"] ;
    $formatted_data["computed_value"]["maternityLeft"] = $myvar["maternityLeft"] ;
    $formatted_data["computed_value"]["paterTotal"] = $myvar["paterTotal"] ;
    $formatted_data["computed_value"]["paterLeft"] = $myvar["paterLeft"] ;
    $formatted_data["computed_value"]["SoloLeft"] = $myvar["SoloLeft"] ;
    $formatted_data["computed_value"]["ULLeft"] = $myvar["ULLeft"] ;
    $formatted_data["computed_value"]["DaysPerMonth"] = $myvar["DaysPerMonth"] ;
    $formatted_data["computed_value"]["DaysPerCutOff"] = $myvar["DaysPerCutOff"] ;
    $formatted_data["computed_value"]["BasicPerMonth"] = $myvar["BasicPerMonth"] ;
    $formatted_data["computed_value"]["BasicPerCutOff"] = $myvar["BasicPerCutOff"] ;
    $formatted_data["computed_value"]["BasicPerDay"] = $myvar["BasicPerDay"] ;
    $formatted_data["computed_value"]["BasicPerHour"] = $myvar["BasicPerHour"] ;
    $formatted_data["computed_value"]["timeStamp"] = $myvar["timeStamp"] ;
    $formatted_data["computed_value"]["SampleTrackNo"] = $myvar["SampleTrackNo"] ;
    $formatted_data["computed_value"]["OnePageTrackNo"] = $myvar["OnePageTrackNo"] ;
    $formatted_data["computed_value"]["headCount"] = $myvar["headCount"] ;
    $formatted_data["computed_value"]["CountSb"] = $myvar["CountSb"] ;
    $formatted_data["computed_value"]["SectionWithCode"] = $myvar["SectionWithCode"] ;


    $formatted_data["change_list_computed_value"]["EmploymentStatus"] = $myvar["EmploymentStatus"] ;

    //return process
    
    $print_debugger .= "\$myvar = ".json_encode($myvar);
    $print_debugger .= "\n\n";
    $print_debugger .= "\$personnel_info_SCGEmpID = ".json_encode($personnel_info_SCGEmpID);
    
    if( ( $selected_fields && count($selected_fields)>0 )&& ( $excluded_fields && count($excluded_fields)>0 ) ){
        foreach ($selected_fields as $key => $value) {
            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];
        }
        foreach ($excluded_fields as $key => $value) {
            unset($new_formatted_data["computed_value"]["". $value .""]);
        }
        $new_formatted_data["__DEBUG"] = $print_debugger;
        return $new_formatted_data;
    }else if($selected_fields && count($selected_fields)>0){
        foreach ($selected_fields as $key => $value) {
            // echo $formatted_data["". $value .""];
            $new_formatted_data["computed_value"]["". $value .""] = $formatted_data["computed_value"]["". $value .""];
        }
        $new_formatted_data["__DEBUG"] = $print_debugger;
        return $new_formatted_data;
    }else if($excluded_fields && count($excluded_fields)>0){
        foreach ($excluded_fields as $key => $value) {
            unset($formatted_data["computed_value"]["". $value .""]);
        }
        $formatted_data["__DEBUG"] = $print_debugger;
        //print_r($formatted_data);
        return $formatted_data;
    }else{
        return $formatted_data;
    }

}
?>