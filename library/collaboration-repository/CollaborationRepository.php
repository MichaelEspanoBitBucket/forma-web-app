<?php

class CollaborationRepository
{
	
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}

	public function SaveCollaboration($name, $creator_id)
	{
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("INSERT Into tb_collaboration (`name`,`creator_id`) VALUES (?,?)");
		$urStmnt->bind_param("si",
				$name,
				$this->user["id"]);
		$urStmnt->execute();
		$collabID = $urStmnt->insert_id;
		
		$this->conn->query("COMMIT");
		return $collabID;
	}

	public function addCollaborator($collaboration_id, $user_id, $display_name)
	{
		$this->conn->query("START TRANSACTION");

		$collbtrsStm = $this->conn->prepare
			("SELECT * From tb_collaboration_collaborators WHERE collaboration_id = ? AND user_id = ?");
		$collbtrsStm->bind_param("ii",$collabID, $user_id);
		$collbtrsStm->execute();
		$collbtrs = $collbtrsStm->get_result();
		$collabID = null;
		if($collbtrs->num_rows == 0){
			$urStmnt = $this->conn->prepare
			("INSERT Into tb_collaboration_collaborators (`collaboration_id`,`user_id`, `display_name`) VALUES (?,?,?)");
			$urStmnt->bind_param("iis",
					$collaboration_id,
					$user_id,
					$display_name);
			$urStmnt->execute();
			$collabID = $urStmnt->insert_id;
		}


		
		
		$this->conn->query("COMMIT");
		return $collabID;
	}

	public function removeCollaborator($collaboration_id, $user_id)
	{
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("DELETE FROM tb_collaboration_collaborators WHERE collaboration_id = ? AND user_id = ?");
		$urStmnt->bind_param("ii",
				$collaboration_id,
				$user_id);
		$urStmnt->execute();
		$this->conn->query("COMMIT");
	}

	public function getCollaboration($collabID){
		$this->conn->query("START TRANSACTION");
		$collStm = $this->conn->prepare
			("SELECT * From tb_collaboration WHERE id = ?");
		$collStm->bind_param("i",$collabID);
		$collStm->execute();
		$coll = $collStm->get_result()->fetch_assoc();


		
		$this->conn->query("COMMIT");
		return $coll;

	}

	public function isCollaborator($collabID, $user_id){
		$this->conn->query("START TRANSACTION");
		$collStm = $this->conn->prepare
			("SELECT * From tb_collaboration_collaborators WHERE user_id = ? AND collaboration_id = ?");
		$collStm->bind_param("ii",$user_id, $collabID);
		$collStm->execute();
		$coll = $collStm->get_result()->fetch_assoc();


		
		$this->conn->query("COMMIT");
		return $coll;

	}


	public function getCollaborators($collabID){
		$this->conn->query("START TRANSACTION");
		$collbtrsStm = $this->conn->prepare
			("SELECT * From tb_collaboration_collaborators WHERE collaboration_id = ?");
		$collbtrsStm->bind_param("i",$collabID);
		$collbtrsStm->execute();
		$collbtrs = $collbtrsStm->get_result()->fetch_all(MYSQLI_ASSOC);

		// foreach ($collbtrs as $key => $collbtr) {
			

		// 	$collbtr["schedules"] = [];
		// 	# code...
		// }
		$this->conn->query("COMMIT");
		return $collbtrs;

	}


}








