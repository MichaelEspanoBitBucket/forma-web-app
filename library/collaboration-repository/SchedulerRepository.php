<?php

class SchedulerRepository
{
	
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}


	
	public function saveUserSchedules($collabID, $user_id, $schedules){
		$this->conn->query("START TRANSACTION");
		foreach ($schedules as $skey => $sched) {
			$stringProperties = json_encode($sched);
			
			if($sched==undefined){
				continue;
			}
			$newSched=null;
			if($sched["id"]==null||$sched["id"]==undefined){

				$urStmnt = $this->conn->prepare
					("INSERT Into tb_collaboration_scheduler_schedules (`user_id`, `date`, `legend_id`, `collaboration_id`) VALUES (?,?,?,?)");
				$urStmnt->bind_param("isii",
						$user_id,
						$skey,
						$sched["legend_id"],
						$collabID);
				$urStmnt->execute();
				$newSchedid = $urStmnt->insert_id;
				$newSched = $sched;
				$newSched["id"] = $newSchedid;
				$newSched["date"] = $skey;
			}else{
				$urStmnt = $this->conn->prepare
					("UPDATE tb_collaboration_scheduler_schedules SET  user_id = ?, `date` = ?, `legend_id` = ? WHERE id = ?");
				$urStmnt->bind_param("isii",
						$user_id,
						$skey,
						$sched["legend_id"],
						$sched["id"]);
				$urStmnt->execute();
			}
		}
		$this->conn->query("COMMIT");
		return $newSched;
	}

	public function removeSchedule($sched_id){
		$this->conn->query("START TRANSACTION");
		
		//$stringProperties = json_encode($legend);
			//var_dump($stringProperties);
		$urStmnt = $this->conn->prepare
			("DELETE FROM tb_collaboration_scheduler_schedules WHERE id = ?");
		$urStmnt->bind_param("i",
				$sched_id);
		$urStmnt->execute();
		$this->conn->query("COMMIT");
		//return $legendId;
		return 1;
		
	}

	public function getSchedules($collabID, $fromDate, $toDate){
		$this->conn->query("START TRANSACTION");
		$collbtrsStm = $this->conn->prepare
			("SELECT 
			    sched.id, DATE_FORMAT(sched.date, '%Y-%m-%d') as `date`, sched.user_id, legend.id as `legend_id`, legend.title, legend.color, legend.description
			FROM
			    tb_collaboration_collaborators clbtr
			        RIGHT JOIN
			    tb_collaboration_scheduler_schedules sched ON sched.user_id = clbtr.user_id
			    Left Join
			    tb_collaboration_scheduler_legends legend ON legend.id = sched.legend_id
			WHERE
			    clbtr.collaboration_id = ? AND (DATE_FORMAT(?, '%Y,%m,%d') <= sched.date AND sched.date <= DATE_FORMAT(?,'%Y,%m,%d') );");

			$collbtrsStm->bind_param("iss",$collabID, $fromDate, $toDate);
			$collbtrsStm->execute();
			$collbtrs = $collbtrsStm->get_result()->fetch_all(MYSQLI_ASSOC);
			$this->conn->query("COMMIT");
			return $collbtrs;
	}

	public function getSchedulesByUser($user_id, $fromDate, $toDate){
		$this->conn->query("START TRANSACTION");
		$collbtrsStm = $this->conn->prepare
			("SELECT 
			    sched.id, DATE_FORMAT(sched.date, '%Y-%m-%d') as `date`, sched.user_id, legend.id as `legend_id`, legend.title, legend.color, legend.description
			FROM
			    tb_collaboration_scheduler_schedules sched
			    Left Join
			    tb_collaboration_scheduler_legends legend ON legend.id = sched.legend_id
			WHERE
			    sched.user_id = ? AND (DATE_FORMAT(?, '%Y,%m,%d') <= sched.date AND sched.date <= DATE_FORMAT(?,'%Y,%m,%d') );");

			$collbtrsStm->bind_param("iss",$user_id, $fromDate, $toDate);
			$collbtrsStm->execute();
			$collbtrs = $collbtrsStm->get_result()->fetch_all(MYSQLI_ASSOC);
			$this->conn->query("COMMIT");
			return $collbtrs;
	}


	public function getCollaborators($collabID){
		$this->conn->query("START TRANSACTION");
		$collbtrsStm = $this->conn->prepare
			("SELECT * From tb_collaboration_collaborators WHERE collaboration_id = ?");
		$collbtrsStm->bind_param("i",$collabID);
		$collbtrsStm->execute();
		$collbtrs = $collbtrsStm->get_result()->fetch_all(MYSQLI_ASSOC);
		$newCollaborators = [];
		foreach ($collbtrs as $key => $collbtr) {
			
			$newCollaborators[$collbtr["user_id"]] = $collbtr;
			//$newCollaborators[$collbtr["user_id"]]["schedules"][] = "";
			# code...
		}
		$this->conn->query("COMMIT");
		return $newCollaborators;

	}

	public function getLegends($collabID){
		$this->conn->query("START TRANSACTION");
		$collbtrsStm = $this->conn->prepare
			("SELECT id as `legend_id`, title, color, description, collaboration_id From tb_collaboration_scheduler_legends WHERE collaboration_id = ? AND is_removed<>1");
		$collbtrsStm->bind_param("i",$collabID);
		$collbtrsStm->execute();
		$collbtrs = $collbtrsStm->get_result()->fetch_all(MYSQLI_ASSOC);

		$this->conn->query("COMMIT");
		return $collbtrs;
	}

	public function addLegend($legend){
		$this->conn->query("START TRANSACTION");
		
		$stringProperties = json_encode($legend);
			//var_dump($stringProperties);
		$urStmnt = $this->conn->prepare
			("INSERT Into tb_collaboration_scheduler_legends (`collaboration_id`, `title`, `color`, `description`) VALUES (?,?,?,?);");
		$urStmnt->bind_param("isss",
				$legend["collabId"],
				$legend["title"],
				$legend["color"],
				$legend["description"]);
		$urStmnt->execute();
		$legendId = $urStmnt->insert_id;
		$this->conn->query("COMMIT");
		//return $legendId;
		return $legendId;
		
	}

	public function removeLegend($legend_id){
		$this->conn->query("START TRANSACTION");
		
		$stringProperties = json_encode($legend);
			//var_dump($stringProperties);
		$urStmnt = $this->conn->prepare
			("UPDATE tb_collaboration_scheduler_legends Set is_removed = 1 WHERE id = ?");
		$urStmnt->bind_param("i",
				$legend_id);
		$urStmnt->execute();
		$this->conn->query("COMMIT");
		//return $legendId;
		return 1;
		
	}

}








