<?php

include_once API_LIBRARY_PATH . 'API.php';
include_once 'CustomScriptAuth.php';
include_once 'CustomScriptDAO.php';
include_once 'CustomScriptUtilities.php';

include_once 'CustomScriptFacade.php';

/**
 * Description of CustomScriptFunctionList
 *
 * @author Joshua Clifford Reyes
 */

class CustomScriptFunctionList {

	public function scriptList () {

		$facade = new CustomScriptFacade();

		$functions = array(
			
			/*======= POSTING - RESPONSES =======*/
	        'CreateResponse' 
	        	=> function ($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $params, $tag) use ($facade){
					$facade->executeCreateResponse($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $params);
	        },                  
	        'CreateResponseWithLine' 
	        	=> function ($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $parent_unique) use ($facade) {
					$facade->executeCreateResponseWithLine($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $parent_unique);
	        },
	        	        
	        'UpdateResponse' 
	        	=> function ($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $id_field, $id_value, $parent_field, $parent_value, $params) use ($facade) {
					$facade->executeUpdateResponse($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $id_field, $id_value, $parent_field, $parent_value, $params);  
	        },

	        'UpdateResponseWhere' 
	        	=> function ($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $params, $references) use ($facade) {
					$facade->executeUpdateResponseWhere($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $params, $references);  
	        },

	        'UpdateResponseWithLine' 
	        	=> function ($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $id_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $id_line, $parent_unique) use ($facade) {
					$facade->executeUpdateResponseWithLine($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $id_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $id_line, $parent_unique);
	        },
	        //CREATE IF DATA NOT EXIST AND UPDATE IF EXIST
	        //ADDED BY JOSHUA CLIFFORD REYES January 26, 2016 12PM
	        'CreateUpdateResponseWithLine' 
	        	=> function ($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $id_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $id_line, $parent_unique, $number_of_condition, $additional_condition_filter_line) use ($facade) {
	            	$facade->executeCreateUpdateResponseWithLine($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $id_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $id_line, $parent_unique, $number_of_condition, $additional_condition_filter_line);
	        },
	        /*===================================*/
	        
	        /*======= POSTING - DYNAMIC - RESPONSES =======*/
	        'CreateDataOnChange' 
	        	=> function ($form, $fields, $status) use ($facade) {
					$proc = $facade->executeCreateDataOnChange($form, $fields, $status);
	            return $proc;
	        },
	        'UpdateDataOnChange' 
	        	=> function ($form, $fields, $id_name, $id_value, $parent_name, $parent_value) use ($facade) {
					$facade->executeUpdateDataOnChange($form, $fields, $id_name, $id_value, $parent_name, $parent_value);
	        },

	        'UpdateDataOnChangeWhere' 
	        	=> function ($form, $fields, $references) use ($facade) {
					$facade->executeUpdateDataOnChange($form, $fields, $references);
	        },

	        'ComputeDataOnChange' 
	        	=> function ($form, $fields, $return_value, $line_name, $id_name, $id_value, $parent_name, $parent_value, $computation) use ($facade) {
					$proc = $facade->executeComputeDataOnChange($form, $fields, $return_value, $line_name, $id_name, $id_value, $parent_name, $parent_value, $computation);
	            return $proc; 
	        },
	        /*=============================================*/ 
	         
	        /*======= MISC =======*/
	        'StringSeparator' 
	        	=> function ($form, $field, $filter, $status) use ($facade) {
					$facade->executeStringSeparator($form, $field, $filter, $status);
	        },
	        'RecordCountWithFilter' 
	        	=> function ($form, $filter_name, $filter_value) use ($facade) {
					$proc = $facade->executeRecordCountWithFilter($form, $filter_name, $filter_value);
	            return $proc;
	        },      
	        'LookUp' 
	        	=> function ($form, $field, $field_id, $id, $return_type) use ($facade) {
					$proc = $facade->executeLookUp($form, $field, $field_id, $id, $return_type);
	            return $proc; 
	        },
	        //MULTIPLE FILTER CONDITION WORKING ON WORKFLOW POSTSUBMIT, TOGETHER WITH OTHER FUNCTION updateDataOnChange etc.
	        //ADDED BY JOSHUA CLIFFORD REYES January 27, 2016 12PM        
	        'LookUpWhere' 
	        	=> function ($form, $return_field, $filter_params) use ($facade) {
					$proc = $facade->executeLookUpWhere($form, $return_field, $filter_params);
	            return $proc; 
	        },
	        //RETURN LOOKUP MULTIPLE VALUE  
	        //ADDED BY JOSHUA CLIFFORD REYES January 28, 2016 2PM
	        'LookUpWhereStringReturn' 
	        	=> function ($form, $return_field, $separator, $id_field, $id_value) use ($facade) {
					$proc = $facade->executeLookUpWhereStringReturn($form, $return_field, $separator, $id_field, $id_value);
	            return $proc; 
	        },
	        //MULTIPLE PICKLIST RETURN VALUE RETURN OR CREATE TO DIFFERENT FORM
	        //ADDED BY JOSHUA CLIFFORD REYES January 27, 2016 5PM   
	        'PickListDataSource' 
	        	=> function ($source_form, $source_field, $destination_form, $destination_field, $destination_status, $destination_node_id, $source_field_id, $picklist_value, $unique_key_params, $data_send_params, $condition_params) use ($facade) {
	            	$proc = $facade->executePickListDataSource($source_form, $source_field, $destination_form, $destination_field, $destination_status, $destination_node_id, $source_field_id, $picklist_value, $unique_key_params, $data_send_params, $condition_params);
	            return $proc;
	        },
	        //RETURN LOOKUP MULTIPLE VALUE  
	        //ADDED BY JOSHUA CLIFFORD REYES January 28, 2016 2PM
	        'GroupUserCount' 
	        	=> function ($group_name) use ($facade) {
					$proc = $facade->executeGroupUserCount($group_name);
	            return $proc;
	        },
	        'RecordDelete' 
	        	=> function ($form, $field_id, $id) use ($facade) {
					$facade->executeRecordDelete($form, $field_id, $id);
	        },
	        //Return Value of IF Condition are functions listed in $custom_function array
	        //ADDED BY JOSHUA CLIFFORD REYES February 22, 2016 5PM
	        'GivenIfCustomScriptFunction' 
	        	=> function ($condition, $action_true, $action_false) {

	            $parameters = func_get_args();
	            if( gettype($parameters[1]) != "array" || gettype($parameters[2]) != "array" ){
	            	 $temp = "";
	            	 if ($condition) {
	            	 	// $temp = gettype($parameters[1]);
	            	 	$temp = $parameters[1];
	            	 }else{
	            	 	// $temp = gettype($parameters[2]);
	            	 	$temp = $parameters[2];
	            	 }
	            	 return $temp;
	            	// return "Use GivenIfCustomScriptFunction if your return parameters are function but your ".($condition?"true":"false")." value is ".gettype($parameters[2]) ;
	            }
	            if ($condition) {
	                $fetch_function_name = $parameters[1][0];
	                unset($parameters[1][0]);
	                $params = array_values($parameters[1]);
	            } else {
	                $fetch_function_name = $parameters[2][0];
	                unset($parameters[2][0]);
	                $params = array_values($parameters[2]);
	            }
	            $custom_function = $GLOBALS['custom_function'];
	            if (isset($custom_function[$fetch_function_name]) && !empty($fetch_function_name)) { //kapag existing ung custom function name
	                return call_user_func_array($custom_function[$fetch_function_name], $params);
	            } else if (empty($fetch_function_name)) { //for empty return
	                return '';
	            } else {
	                return 'Function not found!';
	            }
	        },
	        /*====================*/
	                
	        /*======= CUSTOMIZED - FUNCTION =======*/
	        'GenerateId' 
	        	=> function ($form_used, $series_code, $start_series, $end_series, $separator) use ($facade) {
					$proc = $facade->executeGenerateId($form_used, $series_code, $start_series, $end_series, $separator);
	            return $proc;
	        },
	        'ClearUnusedData' 
	        	=> function ($form_check, $field_check, $form_base, $field_base) use ($facade) {
					$proc = $facade->executeClearUnusedData($form_check, $field_check, $form_base, $field_base);
	            return $proc;
	        },       
	        'CreateResponseOnChange' 
	        	=> function ($form_based, $field_based, $form_child, $field_child, $status, $header_line_id, $header_line_id_value, $ref_form_based_id, $ref_form_child_id, $ref_form_based_child_id_value, $pk_form_based_id, $pk_form_child_id) use ($facade) {
					$facade->executeCreateResponseOnChange($form_based, $field_based, $form_child, $field_child, $status, $header_line_id, $header_line_id_value, $ref_form_based_id, $ref_form_child_id, $ref_form_based_child_id_value, $pk_form_based_id, $pk_form_child_id);                           
	        },
	        'CreateResponseOnChangeComputeTotal' 
	        	=> function ($form_source, $field_source, $form_destination, $field_destination, $field_id_source, $field_id_destination, $field_id_value, $ref_field_first, $ref_field_second, $field_to_compute, $field_to_output, $computation_type, $status) use ($facade) {
					$proc = $facade->executeCreateResponseOnChangeComputeTotal($form_source, $field_source, $form_destination, $field_destination, $field_id_source, $field_id_destination, $field_id_value, $ref_field_first, $ref_field_second, $field_to_compute, $field_to_output, $computation_type, $status);
	            return $proc;
	        },
	        'SetDataComputationToEmbedded' 
	        	=> function ($form, $field_value, $field_id, $field_ref, $parent_name, $parent_value, $form_static_data, $field_static_id, $field_static_ref, $field_to_compute, $field_to_compute_opt, $field_compute_output, $field_compute_output_opt, $type_computation) use ($facade) {
					$facade->executeSetDataComputationToEmbedded($form, $field_value, $field_id, $field_ref, $parent_name, $parent_value, $form_static_data, $field_static_id, $field_static_ref, $field_to_compute, $field_to_compute_opt, $field_compute_output, $field_compute_output_opt, $type_computation);
		            $proc = 'Ok';                                            
	            return $proc;
	        },
	        'CheckFormData' 
	        	=> function ($source_form, $source_id, $destination_form, $destination_id, $tag, $tag_value, $source_tagging, $source_tagging_value) use ($facade) {
					$facade->executeCheckFormData($source_form, $source_id, $destination_form, $destination_id, $tag, $tag_value, $source_tagging, $source_tagging_value);
	        },
	        //Posting Matrix Functions
			'PostingMatrix' 
				=> function ($source_name, $accting_action_code, $trackno, $status, $node_id) use ($facade) {
				//For Shortlist if form is for Posting Matrix Checking
	            if ($accting_action_code != '') {
					$facade->executePostingMatrix($source_name, $trackno, $status, $node_id);
				}
			},
	        //FOR POSTING IN JOURNAL ENTRY PURPOSE
			//ADDED BY JOSHUA CLIFFORD REYES February 22, 2016 5PM
			'JournalPostingMatrix' 
				=> function ($source_form, $repository_form, $destination_form, $trackno, $status, $node_id) use ($facade) {
	           		$facade->executeJournalPostingMatrix($source_form, $repository_form, $destination_form, $trackno, $status, $node_id);
	        },
			//Financial Scheme Functions
	        'FixedRateLoanCalculate_generic' 
	        	=> function ($form_header, $form_child, $trackno, $status, $node_id) use ($facade) {
	            	$facade->executeFixedRateLoan_generic($form_header, $form_child, $trackno, $status, $node_id);
	        },
			'FixedRateLoanCalculate_cfic' 
				=> function ($form_header, $form_child, $trackno, $status, $node_id) use ($facade) {
					$facade->executeFixedRateLoan_cfic($form_header, $form_child, $trackno, $status, $node_id);
	        },
			'FixedPrincipalLoanCalculate_cfic' 
				=> function ($form_header, $form_child, $trackno, $status, $node_id) use ($facade) {
					$facade->executeFixedPrincipalLoan_cfic($form_header, $form_child, $trackno, $status, $node_id);
			},
			'Sample_createResponse' 
				=> function ($sourceFormName, $trackno, $destinationFormName, $nodeId, $fieldsParams) {
					$database = new APIDatabase();
			        $auth = new CustomScriptAuth($database);
			        $dao = new CustomScriptDAO($database, $auth);

			        $database->connect();

			        $sourceTableName = $dao->getFormTableName($sourceFormName);
			        $destinationTableName = $dao->getFormTableName($destinationFormName);
			        
			        $data = array();
			        $data = $dao->generateSystemReserveFields($sourceTableName, $destinationTableName, $trackno, $nodeId);

			        foreach ($fieldsParams as $value) {
			        	$data[$value[0]] = $value[1];
			        }
			        $dao->insertData($destinationTableName, $data);

			        $database->disconnect();
			},
			'Sample_updateResponse'
				=> function ($sourceFormName, $trackno, $destinationFormName, $nodeId, $fieldsParams, $conditionParams) {
					$database = new APIDatabase();
			        $auth = new CustomScriptAuth($database);
			        $dao = new CustomScriptDAO($database, $auth);

			        $database->connect();

			        $sourceTableName = $dao->getFormTableName($sourceFormName);
			        $destinationTableName = $dao->getFormTableName($destinationFormName);
			        
			        $data = array();
			        $data = $dao->generateSystemReserveFields($sourceTableName, $destinationTableName, $trackno, $nodeId);

			        foreach ($fieldsParams as $fieldvalue) {
			        	$data[$fieldvalue[0]] = $fieldvalue[1];
			        }

			        $condition = array();
			        foreach ($conditionParams as $conditionValue) {
			        	$condition[$conditionValue[0]] = $conditionValue[1];
			        }
			        $dao->updateData($destinationTableName, $data, $condition);

			        $database->disconnect();
			},
			'GenerateNextID' => function ($form_used, $prefix, $separator) use ($facade) {
				$process = $facade->executeGenerateNextID($form_used, $prefix, $separator);
				return $process;
			}
		);

		return $functions;
	}
}