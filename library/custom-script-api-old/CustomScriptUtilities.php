<?php

/**
 * Description of CustomScriptUtilities
 *
 * @author Joshua Clifford Reyes
 */

class CustomScriptUtilities {

    public function clearParamString($paramString) {
        
        $KEY_WORDS = array(
            'Parent:', 
            'Form:',            
            'Source:',
            'Child:',
            'Destination:',
            'Field:',
            'Status:',
            'NodeId:',
            'Id:', 
            'UniqueKey:', 
            'Ref:',
            'Sign:',
            'ProcessTrigger:',
            'Computation:',
            'Filter:',
            'Line:',
            'SeriesCode:',
            'Tag:', 
            'ReturnValue:',
            '[', ']'
        );

        $clearedParamString = str_replace($KEY_WORDS, '', $paramString);

        return $clearedParamString;
    }

    public function convertParamString($paramString) {
        
        $clearedParamString = $this->clearParamString($paramString);
        $explodedParamString = explode(',', $clearedParamString);

        $preparedParamString = array();
        foreach ($explodedParamString as $index) {
                $preparedParamString[$index] = $_POST[$index];
        }

        return $preparedParamString; 
    }

    public function sanitizedParamString($paramString) {

        $clearedParamString = $this->clearParamString($paramString);
        $seperatedParamString = $this->seperateParamString($clearedParamString);
        
        return $seperatedParamString;
    }

    public function seperateParamString($paramString) {
        
        $explodedParamString = explode(',', $paramString);
        
        return $explodedParamString;
    }
    
    public function convertStringToArray($paramString = []) {
        
        $convertedParamString = [];
        
        $paramStringCount = count($paramString);
        for ($x = 0; $x < $paramStringCount; $x += 2) {
            $convertedParamString[$paramString[$x]] = $paramString[$x + 1];
        }

        return $convertedParamString; 
    }

    public function checkValueIsNumeric($currentValue) {

        return (is_numeric($currentValue)) ? $currentValue: "'" . $currentValue . "'";
    }

    public function setFieldToString($field, $parentUnique, $id) {

        $fieldString = '';
        $fieldString .= (!empty($id)) ? fieldColumnEscaped($id).',' : '';
        $fieldString .= (!empty($parentUnique)) ? fieldColumnEscaped($parentUnique).',' : '';

        $additionalFields = $this->defaultFields($field);

        foreach ($additionalFields as $index) {
			if (strpos($index, '<{') === 0) {
			
			} else if (!empty($index)) {
                $fieldString .= '`'.$index.'`,';   
            }    
        }

        $collectedFieldString = substr($fieldString, 0, strlen($fieldString) - 1);
        
        return $collectedFieldString;
    }

    private function fieldColumnEscaped($field) {
        
        return '`' . $field . '`';
    }

    public function setStatus($status) {

        return (empty($status)) ? 'Saved' : $status;
    }

    private function defaultFields($fields) {
        
        array_push ($fields, 
            'ID',                 'TrackNo',
            'Requestor',          'Status',
            // 'Processor',          'ProcessorType',
            // 'ProcessorLevel',     'LastAction', 
            'DateCreated',        'DateUpdated',
            'CreatedBy',          'UpdatedBy'
            // 'Unread',             'Node_ID',
            // 'Workflow_ID',        'fieldEnabled',
            // 'fieldRequired',      'fieldHiddenValues',
            // 'imported',           'Repeater_Data',
            // 'Editor',             'Viewer',
            // 'middleware_process', 'SaveFormula',
            // 'CancelFormula',      'enable_delegate'
        );

        return $fields;    
    }

    //Produce TrackNo
    public function incrementTrackNo($value, $pointer) {

        $matches = [];
        preg_match_all('/([a-zA-Z])|([0-9])/', $value, $matches);
         
        $numberString = implode($matches[2]);
        $prefixString = implode($matches[1]);
        
        return $prefixString.str_pad($numberString + $pointer, strlen($numberString), '0', STR_PAD_LEFT);
    }

    public function errorException($message, $variable) {

        throw new Exception($message . ' - ' . $variable);
    }

    public function errorReportingLogs($errorLog, $fileName) {

        error_log(print_r($errorLog), 3, APPLICATION_PATH.'\\library\\custom-script\\reporting-logs\\report-'. $fileName .'-'. $_SERVER['REQUEST_TIME'] .'.log');
    }
}