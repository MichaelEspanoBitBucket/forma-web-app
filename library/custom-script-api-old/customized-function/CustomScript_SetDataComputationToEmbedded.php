<?php

/**
 * Description of CustomScript_SetDataComputationToEmbedded
 *
 * @author Joshua Clifford Reyes
 */

class CustomScript_SetDataComputationToEmbedded {
	
    /** @var CustomScriptDAO */
    protected $dao;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
       	$this->dao = $dao;
    }

    public function customScriptSetDataComputationToEmbedded($form, $field_value, $field_id, $field_ref, $parent_name, $parent_value, $form_static_data, $field_static_id, $field_static_ref, $field_to_compute, $field_to_compute_opt, $field_compute_output, $field_compute_output_opt, $type_computation) {

        /**
        * FORMULA:
        *	@CustomScript("SetDataComputationToEmbedded",
        *		              "Destination:[]",					 ->$form 									FORM EMBEDDED
        *                     @field_value,					     ->$field_value 							VALUE TRIGGER
        *                     "Id:[]", "Ref:[]",                 ->$field_id, $field_ref 					ID & REF ID
        *                     "UniqueKey:[]", @parent_value,     ->$parent_name & $parent_value 			PARENT NAME & ID
        *
        *                     "Source:[]",					     ->$form_static_data 						FORM FOR COMPUTE
        *                     "Id:[]","Ref:[]",					 ->$field_static_id & $field_static_ref 		
        *				  
        *                     "Field:[]",					     ->$field_to_compute 						FIELD FOR COMPUTE
        *                     "Field:[]",					     ->$field_to_compute_opt
        *				  
        *                     "Field:[]",					     ->$field_compute_output  					FIELD COMPUTE RESULT
        *                     "Field:[]",					     ->$field_compute_output_opt
        *
        *                     "Computation:[]")					 ->$type_computation 						TYPE COMPUTATION MDAS ONLY
        */

        /*Formula: @CustomScript("SetDataComputationToEmbedded",
                                 "20_tbl_firmplannedprodorderline",             $form                                   <- ConvertName
                                 @Quantity,
                                 "Item_No","Production_BOM_No",			        $field_id & $field_ref
                                 "Parent_Unique", @Parent_Unique,		        $parent_name
                                    
                                 "20_tbl_productionbomlist",			        $form_static_data                       <- ConvertName    
                                 "Item","PB_No",					            $field_static_id & $field_static_ref					
                                 "Quantity_per"					                $field_to_compute
                                 "Unit_Cost",					                $field_to_compute_opt
                                 "Quantity", Multiple                           $field_compute_output
                                 "Cost_Amount",                                 $field_compute_output_opt
                                 "Multiply") 					                $type_computation
        */							

        if (!empty($field_value)) {

            $form = $this->dao->getFormTableName($form);
            $form_static_data = $this->dao->getFormTableName($form_static_data);

            $get_data = $this->dao->selectData($form, "{$field_id}, {$field_ref}", " WHERE {$parent_name} = '{$parent_value}'", "array");
            
            $field_compute_output_counter = count($field_compute_output) - 1;
            $insert = array();
            foreach ($get_data as $value_getdata) {
                $get_id_for_selector = $value_getdata[$field_id];
                $get_ref_for_selector = $value_getdata[$field_ref];

                $selector_data = $this->dao->selectData($form_static_data, "{$field_static_id}, {$field_to_compute}, {$field_to_compute_opt}", "WHERE {$field_static_ref} = '{$get_ref_for_selector}' AND {$field_static_id} = '{$get_id_for_selector}'", "row");
            
                if ($type_computation == "Multiply") {
                    for ($x = 0; $x <= $field_compute_output_counter; $x++) {
                        $insert[$field_compute_output[$x]] = $selector_data[$field_to_compute] * $field_value;
                    }
                    $insert[$field_compute_output_opt] = $selector_data[$field_to_compute_opt] * $selector_data[$field_to_compute] * $field_value;
                } else if ($type_computation == "Divide") {
                    for ($x = 0; $x <= $field_compute_output_counter; $x++) {
                        $insert[$field_compute_output[$x]] = $selector_data[$field_to_compute] * $field_value;
                    }
                    $insert[$field_compute_output_opt] = $selector_data[$field_to_compute_opt] / $selector_data[$field_to_compute] * $field_value;
                } else if ($type_computation == "Add") {
                    for ($x = 0; $x <= $field_compute_output_counter; $x++) {
                        $insert[$field_compute_output[$x]] = $selector_data[$field_to_compute] * $field_value;
                    }
                    $insert[$field_compute_output_opt] = $selector_data[$field_to_compute_opt] + $selector_data[$field_to_compute] * $field_value;
                } else if ($type_computation == "Subtract") {
                    for ($x = 0; $x <= $field_compute_output_counter; $x++) {
                        $insert[$field_compute_output[$x]] = $selector_data[$field_to_compute] * $field_value;
                    }
                    $insert[$field_compute_output_opt] = $selector_data[$field_to_compute_opt] - $selector_data[$field_to_compute] * $field_value;
                } 
                $condition = array($field_id => $value_getdata[$field_id]);
                $this->dao->updateData($form, $insert, $condition);
            }
        }
        return $field_value;
    }
}