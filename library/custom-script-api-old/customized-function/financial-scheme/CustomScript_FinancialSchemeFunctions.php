<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_FinancialSchemeFunctions
 *
 * @author Joshua Reyes
 */
class CustomScript_FinancialSchemeFunctions {
    //put your code here
    
    public function PMT ($interest, $num_of_payments, $PV, $FV, $Type) {
    	$xp = pow((1 + $interest), $num_of_payments);
        if ($Type == 0) {
            $type_val = 1;
        } else {
            $type_val = (1 / ($interest + 1));
        }
        $return_val = round(($PV * $interest * $xp / ($xp-1) + $interest / ($xp-1) * $FV ) * $type_val, 2);
        return $return_val;
    }
    
    public function paymentType ($type) {
        $beginning  = 1;
        $end        = 0;
        $return_val = 0;
        switch ($type) {
            case "Beginning of Period":
                $return_val = $beginning;
                break;
            case "End of Period":
                $return_val = $end;
                break;
        }
        return $return_val;
    }
    
    public function periodPerYear ($period) {
        $return_val = 0;
        switch ($period) {
            case "Annual":
                $return_val = 1;
                break;
            case "Semi-Annual":
                $return_val = 2;
                break;
            case "Quarterly":
                $return_val = 4;
                break;
            case "Bi-Monthly":
                $return_val = 6;
                break;
            case "Monthly":
                $return_val = 12;
                break;
            case "Semi-Monthly":
                $return_val = 24;
                break;
            case "Bi-Weekly":
                $return_val = 26;
                break;
            case "Weekly":
                $return_val = 52;
                break;
        }
        return $return_val;
    }
    
    //Annual
    //1 year
    public function annual_CAL ($pointer, $first_payment_date) {
        $new_date_object = date_create($first_payment_date);
        date_add($new_date_object, date_interval_create_from_date_string("{$pointer} years"));
        return date_format($new_date_object, "Y-m-d"); 
    }
    
    //Semi-Annual
    //6 months
    public function semi_annual_CAL ($pointer, $first_payment_date) {
        $months = 6 * $pointer;
        $new_date_object = date_create($first_payment_date);
        date_add($new_date_object, date_interval_create_from_date_string("{$months} months"));
        return date_format($new_date_object, "Y-m-d");
    }
    
    //Quarterly
    //3 months
    public function quarterly_CAL ($pointer, $first_payment_date) {
        $months = 3 * $pointer;
        $new_date_object = date_create($first_payment_date);
        date_add($new_date_object, date_interval_create_from_date_string("{$months} months"));
        return date_format($new_date_object, "Y-m-d");
    }
    
    //Bi-Monthly
    //2 months
    public function bi_monthly_CAL ($pointer, $first_payment_date) {
        $months = 2 * $pointer;
        $new_date_object = date_create($first_payment_date);
        date_add($new_date_object, date_interval_create_from_date_string("{$months} months"));
        return date_format($new_date_object, "Y-m-d");
    }
    
    //Monthly
    //1 months
    public function monthly_CAL ($pointer, $first_payment_date) {
        $new_date_object = date_create($first_payment_date);
        date_add($new_date_object, date_interval_create_from_date_string("{$pointer} months"));
        return date_format($new_date_object, "Y-m-d");
    }

    //Semi-Monthly
    public function semi_monthly_CAL ($pointer, $first_payment_date) {
        $weeks = 0;
        if ($pointer % 2 == 0) {
            $weeks = $pointer;
        }
        $pointer_rev = 0;
        if ($pointer != 1 || $pointer != 2) {
            if ($pointer % 2 == 0) {
                $pointer_rev = $pointer / 2;
            } else {
                $res_val = $pointer + 1;
                $pointer_rev = $res_val / 2;  
            }
        }
        $new_date_object = date_create($first_payment_date);
        date_add($new_date_object, date_interval_create_from_date_string("{$pointer_rev} months {$weeks} weeks"));
        return date_format($new_date_object, "Y-m-d");
    }

    //Bi-Weekly
    //14 days
    public function bi_weekly_CAL ($pointer, $first_payment_date) {
        $weeks = 2 * $pointer;
        $new_date_object = date_create($first_payment_date);
        date_add($new_date_object, date_interval_create_from_date_string("{$weeks} weeks"));
        return date_format($new_date_object, "Y-m-d");
    }

    //Weekly
    //7 days
    public function weekly_CAL ($pointer, $first_payment_date) {
        $new_date_object = date_create($first_payment_date);
        date_add($new_date_object, date_interval_create_from_date_string("{$pointer} weeks"));
        return date_format($new_date_object, "Y-m-d");
    }
}
