<?php

/**
 * Description of CustomScript_FixedPrincipalLoan
 *
 * @author Joshua Clifford Reyes
 */

class CustomScript_FixedPrincipalLoan {

    /** @var CustomScriptDAO & CustomScriptUtilities & FinancialSchemeFunctions */
    protected $dao;
    protected $utilities;
    protected $fs_functions;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities & FinancialSchemeFunctions $fs_functions */
    public function __construct($dao, $utilities, $fs_functions) {
       	$this->dao = $dao;
        $this->utilities = $utilities;
        $this->fs_functions = $fs_functions;
    }

    public function customScriptFixedPrincipalLoan ($form_header, $form_child, $trackno, $status, $node_id) {
    	$fields = array(
            "Form_Head_Name"                => $this->dao->getFormTableName($form_header),
            "Form_Child_Name"               => $this->dao->getFormTableName($form_child),
            "Header_ID"                     => "uid",
            "TrackNo"                       => $trackno,
            "Status"                        => $status,
            "NodeId"                        => $node_id,

            "HEAD_FIELDS" => array(
                "Loan_Amount"                   => "totalAmount",
                "Annual_Interest"               => "intRate",
                "Terms_Loan_Years"              => "loanyears",
                "First_Payment_Date"            => "firstPaymentDate",
                "First_Payment"                 => "firstPayment",
                "Payment_Frequency"             => "drdPaymentFreq",
                "Compound_Period"               => "drdCompoundPeriod",
                "Payment_Type"                  => "drdPaymentType",

                "Rate_Per_Period"               => "periodRate",
                "Number_Of_Payments"            => "numberofpayment",
                "Total_Payments"                => "totalPayment",
                "Total_Interest"                => "totalInterest",
                "Estimated_Interest_Savings"    => "interestSavings",

                "Payment_Period"                => "txtPayment_Period"),
            
            "CHILD_FIELDS" => array(
                "Header_ID_Link"                => "uid",
                "No"                            => "txtNo",
                "Due_Date"                      => "txtDueDate",
                "Payment"                       => "txtPayment",
                "Additional_Payment"            => "txtAdditionalPayment",
                "Interest"                      => "txtInterest",
                "Principal"                     => "txtPrincipal",
                "Balance"                       => "txtBalance",
                "Status"                        => "txtStatus")
        );
        $this->CALCULATE($fields);
    }

    private function POSTVAL ($string) {
        return $_POST[$string];
    }

    private function CALCULATE ($fields) {
    	$head_collected_data    = $this->CALC_HEAD($fields);
    	$child_collected_data 	= $this->CALC_CHILD($head_collected_data, $fields);

    	//UPDATING FIELDS OF PARENT HERE
        $this->UPDATE_HEAD($fields, $head_collected_data, $child_collected_data);
        $this->dao->COMMIT_TRANSACT();
    }

    private function UPDATE_HEAD ($fields, $head_collected_data, $child_collected_data) {
        $insert_arr = array(
        $fields['HEAD_FIELDS']['Rate_Per_Period']   =>  $head_collected_data['calRate'],
        $fields['HEAD_FIELDS']['Payment_Period']    =>  $head_collected_data['calPrincipalPeriod']
        );
        $insert 	= array_merge($insert_arr, $child_collected_data);
        $condition 	= array($fields['Header_ID'] => $this->POSTVAL($fields['Header_ID']));

        $this->dao->updateData($fields['Form_Head_Name'], $insert, $condition);
    }

    private function CALC_HEAD ($fields) {
    	$head_collected_data = [];
    	$head_collected_data['calRate']             = $this->calRate($fields['HEAD_FIELDS']['Annual_Interest'], $fields['HEAD_FIELDS']['Compound_Period'], $fields['HEAD_FIELDS']['Payment_Frequency']);
    	$head_collected_data['calPrincipalPeriod']  = $this->calPrincipalPeriod($this->calFirstPayment($fields), $this->fs_functions->periodPerYear($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Frequency'])));
    	return $head_collected_data;
    }

    // for RATE
    private function calRate ($annual_interest, $compound_period, $payment_frequency) {
    	$rate 	= (pow((1 + ($this->POSTVAL($annual_interest) / 100) / $this->fs_functions->periodPerYear($this->POSTVAL($compound_period))), ($this->fs_functions->periodPerYear($this->POSTVAL($compound_period)) / $this->fs_functions->periodPerYear($this->POSTVAL($payment_frequency)))))-1;
        $result = ($rate * 100);
        return $result;
    }

    // for PRINCIPAL PERIOD
    private function calPrincipalPeriod ($loan_amount, $payment_frequency) {
    	$result = round($loan_amount / $payment_frequency, 2);
    	return $result;
    }

    // for FIRST PAYMENT/DOWN PAYMENT
    private function calFirstPayment ($fields) {
        $first_payment 	= $this->POSTVAL($fields['HEAD_FIELDS']['First_Payment']);
        $loan_amount 	= $this->POSTVAL($fields['HEAD_FIELDS']['Loan_Amount']);

        if ($first_payment) {
                $amount = round($loan_amount - $first_payment, 2);
                return $amount;
        } else {
                $amount = round($loan_amount, 2);
                return $amount;
        }
    }

    private function CALC_CHILD ($head_collected_data, $fields) {
    	$current_trackno    = $this->dao->getCurrentTrackNoWithTableName($fields['Form_Child_Name']);

    	//DELETING HERE OF CHILD USING PARENT UNIQUE
        $this->dao->deleteData($fields['Form_Child_Name'], "{$fields['CHILD_FIELDS']['Header_ID_Link']} = '{$this->POSTVAL($fields['Header_ID'])}'");

        //FIRST PAYMENT CHECKER
        $loan_amount = $this->calFirstPayment($fields);
        
        //DEFAULT FIELDS
        $track_no           = $this->getTrackNo($fields['Form_Head_Name'], $fields['TrackNo']);
        $child_status       = $this->utilities->setStatus($fields['Status']);
        $header_keys_str    = $this->utilities->setFieldToString($arr = [], "", "");
        $header_form        = $this->dao->selectDataFieldNameWhereCondition($fields['Form_Head_Name'], "{$header_keys_str}", "TrackNo = '{$track_no}'", "row");
    	
    	$workflow_data      = $this->getWorkflow($fields['Form_Child_Name'], $child_status, $fields['NodeId']);
        $insert_arr         = $this->dao->getFormaMainFields($header_form, $workflow_data);
	    
        $insert_arr[$fields['CHILD_FIELDS']['Status']] = "PENDING";
        
        //COUNTER
        $terms_loan_years   = $this->POSTVAL($fields['HEAD_FIELDS']['Terms_Loan_Years']);
        $payment_frequency  = $this->fs_functions->periodPerYear($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Frequency']));
        $counter            = ($terms_loan_years * $payment_frequency);
        
        //VALUE HOLDER
        $insert_data        = [];
        $past_balance       = 0;
        $number_payments    = 0;
        $total_principal    = 0;
        $total_interest     = 0;

        $this->dao->BEGIN_TRANSACT();
        for ($x = 1; $x <= $counter; $x++) {
            if ($past_balance == 0) {
                $past_balance = $loan_amount;
            }

            //CHILD COMPUTATION
            $no 	= $x;
            $due_date  	= $this->paymentFrequency_PICK_CAL($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Frequency']), round($x - 1), $this->POSTVAL($fields['HEAD_FIELDS']['First_Payment_Date']));
            $interest   = $this->interest_PICK_CAL($x, $this->fs_functions->paymentType($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Type'])), $head_collected_data, $past_balance);
            $principal  = $this->principal_PICK_CAL($head_collected_data, $past_balance, $x, $counter);
            $payment 	= $this->payment_PICK_CAL($principal, $interest);
            $balance    = $this->balance_PICK_CAL($principal, $past_balance);

            $insert_data[$fields['CHILD_FIELDS']['No']]              = $no;
            $insert_data[$fields['CHILD_FIELDS']['Due_Date']]        = $due_date;
            $insert_data[$fields['CHILD_FIELDS']['Interest']]        = round($interest, 2);
            $insert_data[$fields['CHILD_FIELDS']['Payment']]         = round($payment, 2);
            $insert_data[$fields['CHILD_FIELDS']['Principal']]       = round($principal, 2);
            $insert_data[$fields['CHILD_FIELDS']['Balance']]         = round($balance, 2);
            $insert_data[$fields['CHILD_FIELDS']['Header_ID_Link']]  = $this->POSTVAL($fields['Header_ID']);

            $insert_data['TrackNo'] = $this->incrementTrackNo($current_trackno, $x);
            $insert = array_merge($insert_arr, $insert_data);

            $this->dao->insertDataGeneric($fields['Form_Child_Name'], $insert);

            $past_balance       =   round($balance, 2);
            $number_payments    =   $x;
            $total_principal   +=   round($principal['retPrincipal'], 2);
            $total_interest    +=   round($interest, 2);

            if ($balance == 0) {
                break;
            }
            unset($insert_data);
        }

        //HEAD COMPUTATION
        $total_payments = round($total_principal + $total_interest, 2);
        $estimated_interest_savings = $this->calEstInterestSavings($loan_amount, $terms_loan_years, $payment_frequency, $this->fs_functions->paymentType($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Type'])), $head_collected_data['calRate'], $total_interest);
        $insert_head_arr = array(
            $fields['HEAD_FIELDS']['Number_Of_Payments']         => $number_payments,
            $fields['HEAD_FIELDS']['Total_Payments']             => $total_payments,
            $fields['HEAD_FIELDS']['Total_Interest']             => $total_interest,
            $fields['HEAD_FIELDS']['Estimated_Interest_Savings'] => $estimated_interest_savings
        );
        return $insert_head_arr;
    }

    //for EST INTEREST SAVINGS
    private function calEstInterestSavings ($loan_amount, $terms_loan_years, $payment_frequency, $payment_type, $rate, $total_interest) {
        $nper 		= ($terms_loan_years * $payment_frequency);
        $rate_val 	= ($rate / 100);
        $PMT 		= $this->fs_functions->PMT($rate_val, $nper, $loan_amount, 0, $payment_type);
		
        $PMT_nper 	= round($nper * $PMT, 2);
        $PMTnper_loan 	= round($PMT_nper - $loan_amount, 2);
        $result 	= round($PMTnper_loan - round($total_interest, 2), 2);
        return $this->checkLessThanZero($result);
    }

    //TRACKNO
    private function incrementTrackNo ($value, $pointer) {
        $matches = [];
        preg_match_all('/([a-zA-Z])|([0-9])/', $value, $matches);
         
        $numbers_str    = implode($matches[2]);
        $prefix_str     = implode($matches[1]);
        
        $trackno_incremented = $prefix_str.str_pad($numbers_str + $pointer, strlen($numbers_str), '0', STR_PAD_LEFT);
        return $trackno_incremented;
    }

    //BALANCE
    private function balance_PICK_CAL ($principal, $past_balance) {
        $result = round($past_balance - $principal, 2);
        return $this->checkLessThanZero($result);
    }

    //PRINCIPAL
    private function principal_PICK_CAL ($head_collected_data, $past_balance, $pointer, $nper) {
    	if ($nper == $pointer) {
            $return_val = $past_balance;
            return $return_val;
        } else {
            $return_val = $head_collected_data['calPrincipalPeriod'];
            return $return_val;
        }     
    }

    //INTEREST
    private function interest_PICK_CAL ($pointer, $payment_type, $head_collected_data, $past_balance) {
        if ($pointer == 1 && $payment_type == 1) {
            return 0;
        } else {
            return round($past_balance * ($head_collected_data['calRate'] / 100), 2);
        }
    }

    //PAYMENT
    private function payment_PICK_CAL ($principal, $interest) {
        $result = round($principal + $interest, 2);
        return $result;
    }

    //DUE DATE
    private function paymentFrequency_PICK_CAL ($period, $pointer, $first_payment_date) {
        $return_val = "";
        switch ($period) {
            case "Annual":
                $return_val = $this->fs_functions->annual_CAL($pointer, $first_payment_date);
                break;
            case "Semi-Annual":
                $return_val = $this->fs_functions->semi_annual_CAL($pointer, $first_payment_date);
                break;
            case "Quarterly":
                $return_val = $this->fs_functions->quarterly_CAL($pointer, $first_payment_date);
                break;
            case "Bi-Monthly":
                $return_val = $this->fs_functions->bi_monthly_CAL($pointer, $first_payment_date);
                break;
            case "Monthly":
                $return_val = $this->fs_functions->monthly_CAL($pointer, $first_payment_date);
                break;
            case "Semi-Monthly":
                $return_val = $this->fs_functions->semi_monthly_CAL($pointer, $first_payment_date);
                break;
            case "Bi-Weekly":
                $return_val = $this->fs_functions->bi_weekly_CAL($pointer, $first_payment_date);
                break;
            case "Weekly":
                $return_val = $this->fs_functions->weekly_CAL($pointer, $first_payment_date);
                break;
        }
        return $return_val;
    }

    private function checkLessThanZero ($value) {
        if ($value >= 0) {
            return $value;
        } else {
            return 0;
        }
    }
	
    private function getTrackNo ($form, $trackno) {
        if (empty($trackno)) {
            $trackno_value = $this->dao->getCurrentTrackNoWithTableName($form);
            return $trackno_value;
        } else {
            return $trackno;
        }
    }
    
    private function getWorkflow ($form, $status, $node_id) {
        if (empty($node_id)) {
            $workflow = $this->dao->getWorkflowData($form, $status);
            return $workflow;
        } else {
            $workflow = $this->dao->getWorkflowDataWithNodeId($form, $status, $node_id);
            return $workflow;
        }
    }
}