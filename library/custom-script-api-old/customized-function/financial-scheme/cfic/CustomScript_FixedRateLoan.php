<?php

/**
 * Description of CustomScript_FixedRateLoan
 *
 * @author Joshua Clifford Reyes
 */

class CustomScript_FixedRateLoan {

    /** @var CustomScriptDAO & CustomScriptUtilities & FinancialSchemeFunctions */
    protected $dao;
    protected $utilities;
    protected $fs_functions;
    protected $dataSource;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities & FinancialSchemeFunctions $fs_functions */
    public function __construct($dao, $utilities, $fs_functions) {
        $this->dao = $dao;
        $this->utilities = $utilities;
        $this->fs_functions = $fs_functions;
    }

    public function customScriptFixedRateLoan ($form_header, $form_child, $trackno, $status, $node_id) {

        $fields = array(
            "Form_Head_Name"                => $this->dao->getFormTableName($form_header),
            "Form_Child_Name"               => $this->dao->getFormTableName($form_child),
            "Header_ID"                     => "uid",
            "TrackNo"                       => $trackno,
            "Status"                        => $status,
            "NodeId"                        => $node_id,
            "Opportunity_Code_Header"       => "Opportunity_Code",

            "HEAD_FIELDS" => array(
                "Loan_Amount"                   => "totalAmount",
                "Annual_Interest"               => "intRate",
                "Terms_Loan_Years"              => "loanyears",
                "First_Payment_Date"            => "firstPaymentDate",
                "First_Payment"                 => "firstPayment",
                "Payment_Frequency"             => "drdPaymentFreq",
                "Compound_Period"               => "drdCompoundPeriod",
                "Payment_Type"                  => "drdPaymentType",

                "Rate_Per_Period"               => "periodRate",
                "Number_Of_Payments"            => "numberofpayment",
                "Total_Payments"                => "totalPayment",
                "Total_Interest"                => "totalInterest",
                "Estimated_Interest_Savings"    => "interestSavings",

                "Payment_Period"                => "txtPayment_Period",
                "Financial_Scheme_Type"         => "fs_type",
                "Reservation_Payment"           => "reservationPayment"),
            
            "CHILD_FIELDS" => array(
                "Header_ID_Link"                => "uid",
                "Opportunity_Code"              => "Opportunity_Code",
                "No"                            => "txtNo",
                "Due_Date"                      => "txtDueDate",
                "Payment"                       => "txtPayment",
                "Additional_Payment"            => "txtAdditionalPayment",
                "Interest"                      => "txtInterest",
                "Principal"                     => "txtPrincipal",
                "Balance"                       => "txtBalance",
                "Status"                        => "txtStatus",
                "Payment_Desc"                  => "Payment_Desc")
        );

        $this->dataSource = $this->dao->selectData($fields['Form_Head_Name'], "*", "WHERE TrackNo = '{$trackno}'", "row");
        $this->CALCULATE($fields);
    }

    private function POSTVAL ($string) {
        return $this->dataSource[$string];
    }

    private function CALCULATE ($fields) {
        $head_collected_data = $this->CALC_HEAD($fields);
        $this->CALC_CHILD($fields, $head_collected_data);
    }

    private function UPDATE_HEAD ($fields, $head_collected_data, $child_collected_data) {
        $insert_arr = array(
            $fields['HEAD_FIELDS']['Rate_Per_Period']   =>  $head_collected_data['calRate'],
            $fields['HEAD_FIELDS']['Payment_Period']    =>  $head_collected_data['calAnnualPeriod']
        );
        $insert     = array_merge($insert_arr, $child_collected_data);
       
        $condition  = array(
            $fields['Header_ID'] => $this->POSTVAL($fields['Header_ID'])
        );
        $this->dao->updateData($fields['Form_Head_Name'], $insert, $condition);
    }

    private function CALC_HEAD ($fields) {
        //CHECK FINANCIAL SCHEME TYPE IN-HOUSE OR REM
        if ($this->POSTVAL($fields['HEAD_FIELDS']['Financial_Scheme_Type']) == "In-House") {
            $head_collected_data = [];
            $head_collected_data['calRate'] = $this->calRate(
                $fields['HEAD_FIELDS']['Annual_Interest'], 
                $fields['HEAD_FIELDS']['Compound_Period'], 
                $fields['HEAD_FIELDS']['Payment_Frequency']
            );    
            $head_collected_data['calAnnualPeriod'] = $this->calAnnualPeriod(
                $head_collected_data['calRate'], 
                $fields['HEAD_FIELDS']['Terms_Loan_Years'], 
                $fields, 
                $fields['HEAD_FIELDS']['Payment_Type'], 
                $fields['HEAD_FIELDS']['Payment_Frequency']
            );
            return $head_collected_data;
        } else if ($this->POSTVAL($fields['HEAD_FIELDS']['Financial_Scheme_Type']) == "Real Estate Mortgage (REM)"){
            $head_collected_data = [];
            $head_collected_data['calRate'] = 0;
            $head_collected_data['calAnnualPeriod'] = 0;
            return $head_collected_data;
        }
    }

    // FOR RATE
    private function calRate ($annual_interest, $compound_period, $payment_frequency) {
        $annual_interest_val = $this->POSTVAL($annual_interest);
        $compound_period_val = $this->fs_functions->periodPerYear($this->POSTVAL($compound_period));
        $payment_frequency_val = $this->fs_functions->periodPerYear($this->POSTVAL($payment_frequency));
        $rate = (
            pow(
                (1 + 
                    ($annual_interest_val/100)
                    / 
                    $compound_period_val
                ), 
                (
                    $compound_period_val 
                    / 
                    $payment_frequency_val
                )
            )
        )-1;
        $result = ($rate * 100);
        return $result;
    }

    // FOR HEAD PAYMENT PERIOD
    private function calAnnualPeriod ($interest, $num_of_payments, $fields, $Type, $payment_frequency) {
        $num_of_payments_val    = $this->POSTVAL($num_of_payments);
        $PV_val                 = $this->calFirstPayment($fields); //FIRST PAYMENT CHECKER
        $Type_val               = $this->fs_functions->paymentType($this->POSTVAL($Type));
        $payment_frequency_val  = $this->fs_functions->periodPerYear($this->POSTVAL($payment_frequency));
        $interest_val           = ($interest / 100);
        $nper                   = ($num_of_payments_val * $payment_frequency_val);
        $annual_period_ret      = $this->fs_functions->PMT($interest_val, $nper, $PV_val, 0, $Type_val);
        return round($annual_period_ret, 2);
    }

    // FOR FIRST PAYMENT/DOWN PAYMENT
    private function calFirstPayment ($fields) {
        $first_payment = $this->POSTVAL($fields['HEAD_FIELDS']['First_Payment']);
        $loan_amount = $this->POSTVAL($fields['HEAD_FIELDS']['Loan_Amount']);
        $reservation_payment = $this->POSTVAL($fields['HEAD_FIELDS']['Reservation_Payment']); //RESERVATION

        $loan_amount = $loan_amount - $reservation_payment;
        if ($first_payment) {
            $amount = round($loan_amount - $first_payment, 2);
            return $amount;
        } else {
            $amount = round($loan_amount, 2);
            return $amount;
        }
    }
    
    // FOR EST INTEREST SAVINGS
    private function calEstInterestSavings ($loan_amount, $terms_loan_years, $payment_frequency, $payment_type, $rate, $total_interest) {
        $nper       = ($terms_loan_years * $payment_frequency);
        $rate_val   = ($rate / 100);
        $PMT        = $this->fs_functions->PMT($rate_val, $nper, $loan_amount, 0, $payment_type);
        
        $PMT_nper       = round($nper * $PMT, 2);
        $PMTnper_loan   = round($PMT_nper - $loan_amount, 2);
        $result         = round($PMTnper_loan - round($total_interest, 2), 2);
        return $this->checkLessThanZero($result);
    }

    private function CALC_CHILD ($fields, $head_collected_data) {
        //$additional_payment = $this->additional_payment_PICK_CAL($fields);
        $current_trackno = $this->dao->getCurrentTrackNoWithTableName($fields['Form_Child_Name']);
        
        //DELETING HERE OF CHILD USING PARENT UNIQUE
        $this->dao->deleteData($fields['Form_Child_Name'], "{$fields['CHILD_FIELDS']['Header_ID_Link']} = '{$this->POSTVAL($fields['Header_ID'])}'");
        
        //FIRST PAYMENT CHECKER
        $loan_amount = $this->calFirstPayment($fields);
    
        $track_no           = $this->getTrackNo($fields['Form_Head_Name'], $fields['TrackNo']);
        $child_status       = $this->utilities->setStatus($fields['Status']);
        $header_keys_str    = $this->utilities->setFieldToString($arr = [], "", "");
        $header_form        = $this->dao->selectDataFieldNameWhereCondition($fields['Form_Head_Name'], "{$header_keys_str}", "TrackNo = '{$track_no}'", "row");
    
        $workflow_data      = $this->getWorkflow($fields['Form_Child_Name'], $child_status, $fields['NodeId']);
        $insert_arr         = $this->dao->getFormaMainFields($header_form, $workflow_data);
      
        $insert_arr[$fields['CHILD_FIELDS']['Status']] = "PENDING";

        $terms_loan_years   = $this->POSTVAL($fields['HEAD_FIELDS']['Terms_Loan_Years']);
        $payment_frequency  = $this->fs_functions->periodPerYear($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Frequency']));

        //NEW VARIABLES [TAG, DELAY OF CHILD INSERT PURPOSE]
        $if_first_payment_is_enable = "";
        $first_payment_sched_list_counter = 0;
        $first_payment_sched_list_arr = array();
        $default_sched_list_counter = 0;
        $default_sched_list_arr = array();

        //IF FIRST PAYMENT HAVE VALUE THEN DO THIS FIRSTPAYMENT/21
        if ($this->POSTVAL($fields['HEAD_FIELDS']['First_Payment']) != "" || $this->POSTVAL($fields['HEAD_FIELDS']['First_Payment']) != 0) {
            
            $past_balance_y = 0;
            for ($y = 1; $y<=21; $y++) {
                
                if ($past_balance_y == 0) {
                    $past_balance_y = $this->POSTVAL($fields['HEAD_FIELDS']['First_Payment']);
                }

                $no_y             =   $y;
                $due_date_y       =   $this->paymentFrequency_PICK_CAL("Monthly", round($y - 1), $this->POSTVAL($fields['HEAD_FIELDS']['First_Payment_Date']));
                $payment_y        =   round($this->POSTVAL($fields['HEAD_FIELDS']['First_Payment'])/21, 2);
                //$interest       =   $this->interest_PICK_CAL($x, $this->fs_functions->paymentType($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Type'])), $head_collected_data, $past_balance);
                $principal_y      =   $this->principal_PICK_CAL($payment_y, 0);
                $balance_y        =   $this->balance_PICK_CAL($principal_y, $past_balance_y);

                $insert_data_y[$fields['CHILD_FIELDS']['No']]              = $no_y;
                $insert_data_y[$fields['CHILD_FIELDS']['Due_Date']]        = $due_date_y;
                $insert_data_y[$fields['CHILD_FIELDS']['Payment']]         = round($payment_y, 2);
                $insert_data_y[$fields['CHILD_FIELDS']['Interest']]        = 0;
                $insert_data_y[$fields['CHILD_FIELDS']['Principal']]       = round($principal_y['retPrincipal'], 2);
                $insert_data_y[$fields['CHILD_FIELDS']['Balance']]         = round($balance_y, 2);
                $insert_data_y[$fields['CHILD_FIELDS']['Header_ID_Link']]  = $this->POSTVAL($fields['Header_ID']);
                $insert_data_y[$fields['CHILD_FIELDS']['Opportunity_Code']]= $this->POSTVAL($fields['Opportunity_Code_Header']);
                $insert_data_y[$fields['CHILD_FIELDS']['Payment_Desc']]    = "DP-".$no_y;

                $insert_data_y['TrackNo'] = $this->incrementTrackNo($current_trackno, $y);
                $insert = array_merge($insert_arr, $insert_data_y);
                
                // $this->dao->insertDataGeneric($fields['Form_Child_Name'], $insert);
                $$first_payment_sched_list_arr[$first_payment_sched_list_counter] = $insert;
                $first_payment_sched_list_counter += 1;

                $past_balance_y       =   round($balance_y, 2);
                $number_payments_y    =   $y;
                $total_principal_y   +=   round($principal_y['retPrincipal'], 2);
                $total_interest_y    +=   0;

                if ($balance_y == 0) {
                    break;
                }
                unset($insert_data_y);
            }

            $fpd = $due_date_y;
            $counter = ($terms_loan_years * $payment_frequency + 21);
            $minus_due_date = 21;
            $starting_x  = 22;
            $if_first_payment_is_enable = "yes";

        //ELSE DO NORMAL
        } else {

            $fpd = $this->POSTVAL($fields['HEAD_FIELDS']['First_Payment_Date']);
            $counter = ($terms_loan_years * $payment_frequency);
            $minus_due_date = 1;
            $starting_x  = 1;
            $if_first_payment_is_enable = "no";
        
        }
        
        $financial_scheme_type = $this->POSTVAL($fields['HEAD_FIELDS']['Financial_Scheme_Type']);
        //IN-HOUSE COMPUTATION 
        if ($financial_scheme_type === "In-House") {
            $insert_data        = [];
            $past_balance       = 0;
            $number_payments    = 0;
            $total_principal    = 0;
            $total_interest     = 0;
      
            for ($x = $starting_x; $x <= $counter; $x++) {
                if ($past_balance == 0) {
                    $past_balance = $loan_amount;
                }
          
                //Child Values Each Computation
                $no             =   $x;
                $due_date       =   $this->paymentFrequency_PICK_CAL($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Frequency']), round($x - $minus_due_date), $fpd);
                $payment        =   $this->payment_PICK_CAL($head_collected_data, $past_balance, $x, $counter);
                $interest       =   $this->interest_PICK_CAL($x, $this->fs_functions->paymentType($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Type'])), $head_collected_data, $past_balance);
                $principal      =   $this->principal_PICK_CAL($payment, $interest);
                $balance        =   $this->balance_PICK_CAL($principal, $past_balance);
                
                $insert_data[$fields['CHILD_FIELDS']['No']]              = $no;
                $insert_data[$fields['CHILD_FIELDS']['Due_Date']]        = $due_date;
                $insert_data[$fields['CHILD_FIELDS']['Payment']]         = round($payment, 2);
                $insert_data[$fields['CHILD_FIELDS']['Interest']]        = round($interest, 2);
                $insert_data[$fields['CHILD_FIELDS']['Principal']]       = round($principal['retPrincipal'], 2);
                $insert_data[$fields['CHILD_FIELDS']['Balance']]         = round($balance, 2);
                $insert_data[$fields['CHILD_FIELDS']['Header_ID_Link']]  = $this->POSTVAL($fields['Header_ID']);
                $insert_data[$fields['CHILD_FIELDS']['Opportunity_Code']]= $this->POSTVAL($fields['Opportunity_Code_Header']);
                $insert_data[$fields['CHILD_FIELDS']['Payment_Desc']]    = "DP-".$no;
                // if ($principal['retAdditionalPayment']) {
                //     $insert_data[$fields['CHILD_FIELDS']['Additional_Payment']] = $principal['retAdditionalPayment'];
                // }
                
                $insert_data['TrackNo'] = $this->incrementTrackNo($current_trackno, $x);
                $insert = array_merge($insert_arr, $insert_data);
                
                //$this->dao->insertDataGeneric($fields['Form_Child_Name'], $insert);
          $default_sched_list_arr[$default_sched_list_counter] = $insert;
                $default_sched_list_counter += 1;

                $past_balance       =   round($balance, 2);
                $number_payments    =   $x;
                $total_principal   +=   round($principal['retPrincipal'], 2);
                $total_interest    +=   round($interest, 2);
                
                if ($balance == 0) {
                    break;
                }
                unset($insert_data); 
            } 
            
            $total_payments = round($total_principal + $total_interest + $this->POSTVAL($fields['HEAD_FIELDS']['First_Payment']) + $this->POSTVAL($fields['HEAD_FIELDS']['Reservation_Payment']), 2);
            $estimated_interest_savings = $this->calEstInterestSavings($loan_amount, $terms_loan_years, $payment_frequency, $this->fs_functions->paymentType($this->POSTVAL($fields['HEAD_FIELDS']['Payment_Type'])), $head_collected_data['calRate'], $total_interest);
            $insert_head_arr = array(
                $fields['HEAD_FIELDS']['Number_Of_Payments']         => $number_payments,
                $fields['HEAD_FIELDS']['Total_Payments']             => $total_payments,
                $fields['HEAD_FIELDS']['Total_Interest']             => $total_interest,
                $fields['HEAD_FIELDS']['Estimated_Interest_Savings'] => $estimated_interest_savings
            );
            // return $insert_head_arr;

        //COMPUTATION REM
        } else if ($financial_scheme_type === "Real Estate Mortgage (REM)") {
            
            $insert_data = [];
            $insert_data[$fields['CHILD_FIELDS']['No']]              = $starting_x;
            $insert_data[$fields['CHILD_FIELDS']['Due_Date']]        = "";
            $insert_data[$fields['CHILD_FIELDS']['Payment']]         = 0;
            $insert_data[$fields['CHILD_FIELDS']['Interest']]        = 0;
            $insert_data[$fields['CHILD_FIELDS']['Principal']]       = 0;
            $insert_data[$fields['CHILD_FIELDS']['Balance']]         = round($loan_amount, 2);
            $insert_data[$fields['CHILD_FIELDS']['Header_ID_Link']]  = $this->POSTVAL($fields['Header_ID']);
            $insert_data[$fields['CHILD_FIELDS']['Opportunity_Code']]= $this->POSTVAL($fields['Opportunity_Code_Header']);
            $insert_data[$fields['CHILD_FIELDS']['Payment_Desc']]    = "DP-".$starting_x;
            
            $insert_data['TrackNo'] = $this->incrementTrackNo($current_trackno, $starting_x);
            $insert = array_merge($insert_arr, $insert_data);

            // $this->dao->insertDataGeneric($fields['Form_Child_Name'], $insert);
            $default_sched_list_arr[$default_sched_list_counter] = $insert;
            $default_sched_list_counter += 1;

            $insert_head_arr = array(
                $fields['HEAD_FIELDS']['Number_Of_Payments']         => $starting_x,
                $fields['HEAD_FIELDS']['Total_Payments']             => 0 + $this->POSTVAL($fields['HEAD_FIELDS']['First_Payment']) + $this->POSTVAL($field['HEAD_FIELDS']['Reservation_Payment']),
                $fields['HEAD_FIELDS']['Total_Interest']             => 0,
                $fields['HEAD_FIELDS']['Estimated_Interest_Savings'] => 0
            );
            // return $insert_head_arr;
        }

        //UPDATING FIELDS OF PARENT HERE
        $this->UPDATE_HEAD($fields, $head_collected_data, $insert_head_arr);

        //THIS IS THE DELAYED LAYER OF PAYMENT SCHEDULE DATA
        //$this->dao->BEGIN_TRANSACT();
        if ($if_first_payment_is_enable == "yes") {
            // for($first_payment_sched_list_ctr = 0; $first_payment_sched_list_ctr < count($first_payment_sched_list_arr); $first_payment_sched_list_ctr++){
            //     $this->dao->insertDataGeneric($fields['Form_Child_Name'], $first_payment_sched_list_arr[$first_payment_sched_list_ctr]);
            // }
            echo $this->dao->insertDataBulkLoadData($fields['Form_Child_Name'], $first_payment_sched_list_arr);
            unset($first_payment_sched_list_arr);
        }

        // for($default_sched_list_ctr = 0; $default_sched_list_ctr < count($default_sched_list_arr); $default_sched_list_ctr++){
        //     $this->dao->insertDataGeneric($fields['Form_Child_Name'], $default_sched_list_arr[$default_sched_list_ctr]);
        // }
        echo $this->dao->insertDataBulkLoadData($fields['Form_Child_Name'], $default_sched_list_arr);
        unset($default_sched_list_arr);
        unset($insert_arr);
        //$this->dao->COMMIT_TRANSACT();
    }

    //TRACKNO
    private function incrementTrackNo ($value, $pointer) {
        $matches = [];
        preg_match_all('/([a-zA-Z])|([0-9])/', $value, $matches);
         
        $numbers_str    = implode($matches[2]);
        $prefix_str     = implode($matches[1]);
        
        $trackno_incremented = $prefix_str.str_pad($numbers_str + $pointer, strlen($numbers_str), '0', STR_PAD_LEFT);
        return $trackno_incremented;
    }

//ADDITIONAL PAYMENT
//    private function additional_payment_PICK_CAL ($fields) {
//        $additional_payment = $this->dao->selectData($fields['Form_Child_Name'], "{$fields['CHILD_FIELDS']['No']}, {$fields['CHILD_FIELDS']['Additional_Payment']}", "WHERE {$fields['CHILD_FIELDS']['Header_ID_Link']} = '{$this->POSTVAL($fields['Header_ID'])}' AND {$fields['CHILD_FIELDS']['Additional_Payment']} NOT LIKE '' ORDER BY {$fields['CHILD_FIELDS']['No']} ASC", "array");
//        return $additional_payment;
//    }

    //BALANCE
    private function balance_PICK_CAL ($principal, $past_balance) {
        $result = round($past_balance - $principal['retPrincipal'], 2);
        return $this->checkLessThanZero($result);
    }

    //PRINCIPAL
    private function principal_PICK_CAL ($payment, $interest) {
//        $principal_arr = [];
//        if ($additional_payment) {
//            foreach ($additional_payment as $value) {
//                if ($pointer == $value[$fields['CHILD_FIELDS']['No']]) {
//                    $principal_arr['retPrincipal'] = ($payment - $interest + $value[$fields['CHILD_FIELDS']['Additional_Payment']]);
//                    $principal_arr['retAdditionalPayment'] = $value[$fields['CHILD_FIELDS']['Additional_Payment']];
//                    return $principal_arr;
//                } else {
//                    $principal_arr['retPrincipal'] = $payment - $interest;
//                    $principal_arr['retAdditionalPayment'] = 0;
//                    return $principal_arr;
//                }
//            }
//        } else {
//            $principal_arr['retPrincipal'] = $payment - $interest;
//            $principal_arr['retAdditionalPayment'] = 0;
//            return $principal_arr;
//        }
        $principal_arr['retPrincipal'] = ($payment - $interest);
        // $principal_arr['retAdditionalPayment'] = 0;
        return $principal_arr;
    }
    
    //INTEREST
    private function interest_PICK_CAL ($pointer, $payment_type, $head_collected_data, $past_balance) {
        if ($pointer == 1 && $payment_type == 1) {
            return 0;
        } else {
            return round($past_balance * ($head_collected_data['calRate'] / 100), 2);
        }
    }

    //PAYMENT
    private function payment_PICK_CAL ($head_collected_data, $past_balance, $pointer, $nper) {
        if ($nper == $pointer) {
            $return_val = round((1 + ($head_collected_data['calRate'] / 100)) * $past_balance, 2);
            return $return_val;
        } else {
            $return_val = $head_collected_data['calAnnualPeriod'];
            return $return_val;
        }
    }

    //DUE DATE
    private function paymentFrequency_PICK_CAL ($period, $pointer, $first_payment_date) {
        $return_val = "";
        switch ($period) {
            case "Annual":
                $return_val = $this->fs_functions->annual_CAL($pointer, $first_payment_date);
                break;
            case "Semi-Annual":
                $return_val = $this->fs_functions->semi_annual_CAL($pointer, $first_payment_date);
                break;
            case "Quarterly":
                $return_val = $this->fs_functions->quarterly_CAL($pointer, $first_payment_date);
                break;
            case "Bi-Monthly":
                $return_val = $this->fs_functions->bi_monthly_CAL($pointer, $first_payment_date);
                break;
            case "Monthly":
                $return_val = $this->fs_functions->monthly_CAL($pointer, $first_payment_date);
                break;
            case "Semi-Monthly":
                $return_val = $this->fs_functions->semi_monthly_CAL($pointer, $first_payment_date);
                break;
            case "Bi-Weekly":
                $return_val = $this->fs_functions->bi_weekly_CAL($pointer, $first_payment_date);
                break;
            case "Weekly":
                $return_val = $this->fs_functions->weekly_CAL($pointer, $first_payment_date);
                break;
        }
        return $return_val;
    }
    
    private function checkLessThanZero ($value) {
        if ($value > 0) {
            return $value;
        } else {
            return 0;
        }
    }
  
    private function getTrackNo ($form, $trackno) {
        if (empty($trackno)) {
            $trackno_value = $this->dao->getCurrentTrackNoWithTableName($form);
            return $trackno_value;
        } else {
            return $trackno;
        }
    }
    
    private function getWorkflow ($form, $status, $node_id) {
        if (empty($node_id)) {
            $workflow = $this->dao->getWorkflowData($form, $status);
            return $workflow;
        } else {
            $workflow = $this->dao->getWorkflowDataWithNodeId($form, $status, $node_id);
            return $workflow;
        }
    }
}