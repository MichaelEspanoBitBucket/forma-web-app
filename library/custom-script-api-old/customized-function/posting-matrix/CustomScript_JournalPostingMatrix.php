<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_JournalPostingMatrix
 *
 * @author Joshua Reyes
 */
class CustomScript_JournalPostingMatrix {
    //put your code here
    
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    protected $formulaDoc;
    protected $source_form_tbl_name;
    protected $repository_form_tbl_name;
    protected $destination_form_tbl_name;

    protected $source_form_data;
    
    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
        $this->formulaDoc = new Formula();
    }

    public function customScriptJournalPostingMatrix ($source_form, $repository_form, $destination_form, $trackno, $status, $node_id) {
        
       /**
        * FORMULA:
        *   @CustomScript("JournalPostingMatrix",
        *                     "Source:[]",                  -> $source_form                 This is Transaction Form
        *                     "Source:[]",                  -> $repository_form             This is Repository Form or Set of many posting procedure
        *                     "Destination:[]",             -> $destination_form            This is Destination Form
        *                     @TrackNo,                     -> $trackno                     Required Trackno of transaction Form
        *                     "Status:[]",                  -> $status                      Status of Destination Form
        *                     "NodeId:[]")                  -> $node_id                     Node ID of Destination Form
        */

        $this->source_form_tbl_name = $this->dao->getFormTableName($source_form);
        $this->repository_form_tbl_name = $this->dao->getFormTableName($repository_form);
        $this->destination_form_tbl_name = $this->dao->getFormTableName($destination_form);

        $default_data_fields = $this->getDefaultFields($this->source_form_tbl_name, $this->destination_form_tbl_name, $trackno, $status, $node_id);

        # SOURCE FORM INIT
        $source_form_data = $this->dao->selectData($this->source_form_tbl_name, "*", "WHERE TrackNo = '{$trackno}'", "row");
        $this->source_form_data = $source_form_data;

        # REPOSITORY FORM INIT
        # Parent Unique
        $short_list_field_name = 'drpDown_TransactionFormName';
        $short_list_field_value = $source_form;
        $repository_form_data = $this->dao->selectData($this->repository_form_tbl_name, "*", "WHERE {$short_list_field_name} = '{$short_list_field_value}'", "array");

        # ID Unique
        $matcher_base = 'pckList_ItemCategoryNo';
        $matcher_value = 'txtbox_srcItemCode';
		# additional condition
		$matcher_base_2 =  'txtbox_Condition2Code'; 
        $matcher_value_2 =  'txtbox_Condition2'; 
		
        $repository_form_data_storage_array = [];
        foreach ($repository_form_data as $repository_form_data_value) {
            //TO TRIGGER THE RESPONSE
          #  if ($repository_form_data_value[$matcher_base] ==
			#	($this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value[$matcher_value]))
			$val_matcher_base_2 = $repository_form_data_value[$matcher_base_2];
			if (($repository_form_data_value[$matcher_base] ==
					($this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value[$matcher_value]))
				) &&
				((($val_matcher_base_2 != "" && $val_matcher_base_2 != NULL) && 
					($val_matcher_base_2 == ($this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value[$matcher_value_2])))
						)
						|| ($val_matcher_base_2 == "" || $val_matcher_base_2 == NULL)
				)					
			) {
			
                $map_data_fields = $this->executeCollectDataManager($repository_form_data_value);
                $overall_data = array_merge($default_data_fields, $map_data_fields);
                $this->processAction($this->destination_form_tbl_name, $overall_data);
            }
        }
    }

    private function executeCollectDataManager ($repository_form_data_value) {
        #CUSTOM MAPPING
        $glAccountSetValue = $this->glAccountConditionValue($repository_form_data_value);

        # MAPPING TF+JPM to JE
        $params_field = [
            "Form_Name"             ,   $repository_form_data_value['drpDown_TransactionFormName'],
            "BookOfAccounts"        ,   $repository_form_data_value['pcklist_Book'],
            "AccountTitle"          ,   $glAccountSetValue['GLAccountName'],
            "GL_Account_Code"       ,   $glAccountSetValue['GLAccountNo'],
            "TransactType"          ,   $repository_form_data_value['drpDown_TransactionType'],
            "Affected_Cost_Center"  ,   $repository_form_data_value['pckList_CostCenter'],    
            "SourceDateCreated"     ,   "NOW()",
            "Source_TrackNo"        ,   $this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value['txtbox_UID']),
            "particulars"           ,   $this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value['txtbox_srcParticular']),
            ($repository_form_data_value['drpDown_TransactionType'] == 'Debit' ? "DebitAmount" : "CreditAmount"), $this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value['txtArea_Formula']),
            "GLAccountCode"         ,   $repository_form_data_value['GLAccountCode'],
            "AccountCode"           ,   $repository_form_data_value['AccountCode'],
            "withSubsidiary"        ,   $repository_form_data_value['withSubsidiary'],
            "GenLedgerCode"         ,   $repository_form_data_value['GenLedgerCode'],
			'Bank_No'				, 	$this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value['Bank_No']),
			'Bank_Name'				, 	$this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value['Bank_Name']),
			'txt_partnercode'		,	$this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value['Partner_No']),
			'txt_partnername'		,	$this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value['Partner_Name']),
			'Date_Posted'			,	$this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $repository_form_data_value['Date_Posted']),
			'chckreconciliation'	,	$repository_form_data_value['Reconciled']
        ];

        $params = $this->utilities->convertStringToArray($params_field);

        foreach ($params as $key => $value) {
            $data[$key] = $value;
        }
        return $data;
    }

    private function formulaParser ($form_name, $data, $formula) {
        $this->formulaDoc->MyFormula = $formula;
        $this->formulaDoc->DataFormSource[0] = $data;
        return $this->formulaDoc->evaluate();
    }

    private function getDefaultFields ($source_form, $destination_form, $trackno, $status, $node_id) {
        $destination_status         = $this->utilities->setStatus($status);
        $field_source_keys_str      = $this->utilities->setFieldToString($arr = [], "", "");
        $form_source_data           = $this->dao->selectDataFieldNameWhereCondition($source_form, "{$field_source_keys_str}", "TrackNo = '{$trackno}'", "row");
        $workflow_data              = $this->getWorkflow($destination_form, $destination_status, $node_id);
        $forma_default_field        = $this->dao->getFormaMainFields($form_source_data, $workflow_data);
        return $forma_default_field;
    }

    private function getWorkflow ($form, $status, $node_id) {
        if (empty($node_id)) {
            $workflow = $this->dao->getWorkflowData($form, $status);
            return $workflow;
        } else {
            $workflow = $this->dao->getWorkflowDataWithNodeId($form, $status, $node_id);
            return $workflow;
        }
    }

    private function incrementTrackNo ($value, $pointer) {
        $matches = [];
        preg_match_all('/([a-zA-Z])|([0-9])/', $value, $matches);
        $numbers_str    = implode($matches[2]);
        $prefix_str     = implode($matches[1]);
        $trackno_incremented = $prefix_str.str_pad($numbers_str + $pointer, strlen($numbers_str), '0', STR_PAD_LEFT);
        return $trackno_incremented;
    }

    private function processAction ($form, $data) {
        $current_trackno = $this->dao->getCurrentTrackNoWithTableName($form);
        $data['TrackNo'] = $this->utilities->incrementTrackNo($current_trackno, 1);
        $this->dao->insertDataGeneric($form, $data);
    }

    //added function [04-04-2016] for GL Account [Static | Computed] value
    private function glAccountConditionValue ($data) {
        $collected_data = array();
        if ($data['radGLAccountSource'] == 'Static') {
            $collected_data['GLAccountName'] = $data['txtBox_GLAccountName'];
            $collected_data['GLAccountNo'] = $data['pckList_GLAccountNo'];
        } else {
            $collected_data['GLAccountName'] = $this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $data['txtFieldGLAccountName']);
            $collected_data['GLAccountNo'] = $this->formulaParser($this->source_form_tbl_name, $this->source_form_data, $data['txtFieldGLAccountNo']);
        }
        return $collected_data;
    }
}