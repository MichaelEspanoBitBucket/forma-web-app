<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_GroupUserCount
 *
 * @author Joshua Reyes
 */
class CustomScript_GroupUserCount {
    //put your code here
    
    /** @var CustomScriptDAO */
    protected $dao;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
    }
    
    public function customScriptGroupUserCount($group_name) {
        
        /**
        *  FORMULA:
        *   @CustomScript("GroupUserCount",
        *                 @Group_Name)
        */

        $this->dao->BEGIN_TRANSACT();
        
        //$formHead = "SELECT id, group_name FROM `tbform_groups` WHERE group_name = '{$group_name}'";
        //$formChild = "SELECT id FROM `tbform_groups_users` where group_id = '{}' and is_active = '1'";
        $record_tbform_groups = $this->dao->selectData('`tbform_groups`', 'id', "WHERE group_name = '{$group_name}'", 'row');
        $record_count_tbform_groups_users = $this->dao->selectData('`tbform_groups_users`', 'id', "WHERE group_id = '{$record_tbform_groups['id']}' AND is_active = '1'", 'numrows');
        
        $this->dao->COMMIT_TRANSACT();

        return $record_count_tbform_groups_users;
    }
}