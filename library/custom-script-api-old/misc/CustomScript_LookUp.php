<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_LookUp
 *
 * @author Joshua Reyes
 */
class CustomScript_LookUp {
    
    /** @var CustomScriptDAO */
    protected $dao;
    
    /** @param CustomScriptDAO $dao */	
    public function __construct($dao) {
        $this->dao = $dao;
    }
    
    public function customScriptLookUp ($form, $field, $field_id, $id, $return_type) {
       
       /**
        *  FORMULA:
        *   @CustomScript("LookUp",
        *                 "Form:[]",                    -> $form
        *                 "Field:[]",                   -> $field
        *                 "Id:[]",                      -> $field_id
        *                 @Id_Value,                    -> $id
        *                 "ReturnValue:[]" or @value)   -> $return_type
        */ 
        
        $form_table = $this->dao->getFormTableName($form);
        $checkedArray = $this->checkArrayElement($return_type);
        if ($checkedArray == 0) {
            $data = $this->withOutReturnValue($form_table, $field, $field_id, $id);
            return $data; 
        } else if ($checkedArray == 1) {
            $data = $this->withReturnValue($form_table, $field, $field_id, $id, $return_type, $checkedArray);
            return $data;
        } else {
            $data = $this->withReturnValue($form_table, $field, $field_id, $id, $return_type, $checkedArray);
            return $data;
        }
    }
    
    private function withReturnValue ($form_table, $field, $field_id, $id, $return_type, $count) {
        $data = $this->dao->selectDataFieldNameWhereCondition($form_table, $field, "{$field_id} = '{$id}'", 'row');
        //do return ReturnValue:[] if set
        if ($count == 1) {
            if (empty($data)) {
               $value = $return_type[0];
            } else {
               $value = $data[$field];
            }
           return $value;
        //do return @value if lookup throw empty
        } else {
            if (empty($data)) {
                $value = $return_type[1];
            } else {
                $value = $return_type[0]; 
            }
            return $value;
        }
    }
    
    private function withOutReturnValue ($form_table, $field, $field_id, $id) {
       //do return selected field
       $data = $this->dao->selectDataFieldNameWhereCondition($form_table, $field, "{$field_id} = '{$id}'", 'row');
       return $data[$field]; 
    }
    
    private function checkArrayElement ($array) {
        $counter = 0;
        foreach ($array as $value) {
            if (!empty($value)){
                $counter +=1;
            }
        }
        return $counter;
    }
}

