<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_LookUpWhere
 *
 * @author Joshua Reyes
 */
class CustomScript_LookUpWhere {
    //put your code here
    
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }
    
    public function customScriptLookUpWhere ($form, $return_field, $filter_params) {
        
        /**
        *  FORMULA:
        *   @CustomScript("LookUpWhere",
        *                 "Form:[]",                    -> $form
        *                 "Field:[]",                   -> $return_field
        *                 [])                           -> $filter_params
        */
        
        $form_table = $this->dao->getFormTableName($form);
        $condition = $this->expandFilterParams($filter_params);
        $data = $this->dao->selectData($form_table, $return_field, 'WHERE '.$condition, 'row');
        
        return $data[$return_field];
    }
    
    private function expandFilterParams ($filter_params) {
        $filter_string = '';
        foreach ($filter_params as $key => $value) {
            $val = $this->utilities->checkValueIsNumeric($value);
            $filter_string .= "{$key} = {$val} AND ";
        }
        $conditions_string = substr($filter_string, 0, strlen($filter_string) - 4);
        return $conditions_string;
    }
}