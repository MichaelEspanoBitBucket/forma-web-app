<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_UpdateDataOnChange
 *
 * @author Joshua Reyes
 */
class CustomScript_UpdateDataOnChange {
    //put your code here
    
    /** @var CustomScriptDAO */
    protected $dao;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
       $this->dao = $dao;
    }
    
    public function customScriptUpdateDataOnChange ($form, $params, $id_name, $id_value, $parent_name, $parent_value) {
        
        /**
        * FORMULA:
        *   @CustomScript("UpdateDataOnChange",
        *                  "Form:[]",                  ->$form
        *                  ["Key",@Values],            ->$params
        *                  "Id:[]", @Id,               ->$id_name & $id_value 
        *                  "UniqueKey:[]", @Parent)    ->$parent_name & $parent_value
        */
        
        $form_name = $this->dao->getFormTableName($form);

        $condition = $this->getCondition($id_name, $id_value, $parent_name, $parent_value);
        $insert = [];
        foreach ($params as $key => $value) {
            $insert[$key] = $value;
        }
        $this->dao->updateData($form_name, $insert, $condition);
    }

    private function getCondition ($id_name, $id_value, $parent_name, $parent_value) {
        $condition = [];
        if (!empty($id_name) && !empty($id_value)) {
            $condition[$id_name] = $id_value;
        }
        if (!empty($parent_name) && !empty($parent_value)) {
            $condition[$parent_name] = $parent_value;
        }
        return $condition;
    }
}
