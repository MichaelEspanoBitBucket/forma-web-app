<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_UpdateDataOnChangeWhere
 *
 * @author Joshua Reyes
 */
class CustomScript_UpdateDataOnChangeWhere {
    //put your code here
    
    /** @var CustomScriptDAO */
    protected $dao;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
       $this->dao = $dao;
    }
    
    public function customScriptUpdateDataOnChangeWhere ($form, $params, $references) {
        
        /**
        * FORMULA:
        *   @CustomScript("UpdateDataOnChangeWhere",
        *                  "Form:[]",                       ->$form
        *                  ["Key",@Values],                 ->$params
        *                  ["Field", "operator", @value])   ->$references
        */
        
        $form_name = $this->dao->getFormTableName($form);

        $insert = [];
        foreach ($params as $key => $value) {
            $insert[$key] = $value;
        }
        $this->dao->updateDataWhere($form_name, $insert, $references);
    }
}