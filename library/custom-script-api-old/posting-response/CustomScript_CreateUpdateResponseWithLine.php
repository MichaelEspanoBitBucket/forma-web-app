<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_CreateUpdateResponseWithLine
 *
 * @author Joshua Reyes
 */
class CustomScript_CreateUpdateResponseWithLine {
    //put your code here
    
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }
    
    public function customScriptCreateUpdateResponseWithLine($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $id_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $id_line, $parent_unique, $number_of_condition, $additional_condition_filter_line) {
        
        /**
        * FORMULA:
        *   @CustomScript("CreateUpdateResponseWithLine",
        *                 "Parent:[]",            ->$form_source_parent         FORM PARENT (BASE, FROM)
        *                 "Field:[]",             ->$field_source_parent
        *
        *                 "Child:[]",             ->$form_destination_parent    FORM PARENT (CHILD, TO)
        *                 "Field:[]",             ->$field_destination_parent
        *
        *                 "Status:[]",            ->$status_parent              PARENT STATUS
        *                 "NodeId:[]"             ->$node_id_parent             PARENT NODE ID
        *                 @TrackNo,               ->$trackno_parent             PARENT TRACKNO
        *                 "ProcessTrigger:[]",    ->$process_trigger_parent     PARENT PROCESS TRIGGER ENABLE/DISABLE
        *                 "UniqueKey:[ID,ID]",    ->$id_parent                  ID TO FILTER
        *
        *                 "Parent Form Line",     ->$form_source_line           FORM LINE (BASE, FROM)
        *                 "Fields",               ->$field_source_line
        *
        *                 "Child Form Line",      ->$form_destination_line      FORM LINE (CHILD, TO)
        *                 "Fields",               ->$field_destination_line
        *
        *                 "Status:[]",            ->$status_line                LINE STATUS
        *                 "NodeId:[]",            ->$node_id_line               LINE NODE ID
        *                 "ProcessTrigger:[]",    ->$process_trigger_line       LINE PROCESS TRIGGER ENABLE/DISABLE              
        *                 "UniqueKey:[ID,ID]",    ->$id_line                    ID TO FILTER LINE
        *                  
        *                 "Parent_Name",          ->$parent_unique              REF OF PARENT AND CHILD
        *                 "2", [])                ->$number_of_condition && $additional_condition_filter_line        NUMBER OF CONDITION TO USE IN LINE AND CUSTOM PARENT UNIQUE INSTEAD OF REAL PARENT_UNIQUE
        */
        
        $form_source_parent_name = $this->dao->getFormTableName($form_source_parent);
        $field_source_parent_keys = $field_source_parent;
        
        $trackno = $this->getTrackNoParent($form_source_parent_name, $parent_unique, $trackno_parent);
        
        $field_source_parent_keys_str = $this->utilities->setFieldToString($field_source_parent_keys, $parent_unique, $id_parent[0]);
        $form_source_data = $this->dao->selectDataFieldNameWhereCondition($form_source_parent_name, $field_source_parent_keys_str, "TrackNo = '{$trackno}'", 'row'); 
        
        if (!empty($status_parent)) {
            $form_destination_parent_name = $this->dao->getFormTableName($form_destination_parent);
            $field_destination_parent_keys = $field_destination_parent;
            
            $destination_parent_status = $this->utilities->setStatus($status_parent);
            
            $workflow_data = $this->getWorkflow($form_destination_parent_name, $destination_parent_status, $node_id_parent);
            $insert_arr = $this->dao->getFormaMainFields($form_source_data, $workflow_data);
            
            $counter_header = count($field_source_parent_keys) - 1;
            for ($x = 0; $x <= $counter_header; $x++) {
                $insert_arr[$field_destination_parent_keys[$x]] = $form_source_data[$field_source_parent_keys[$x]];
            }
            //Update Parent
            $condition = array($id_parent[1] => $form_source_data[$id_parent[0]]);
            
            //Validate if Data in Parent Already Exists
            $validation = $this->validateCondition($form_destination_parent_name, $condition);
            if ($validation == 'create') {
                $this->processActionCreate($form_destination_parent_name, $insert_arr, $process_trigger_parent, 1);
            } else if ($validation == 'update') {
                $this->processActionUpdate($form_destination_parent_name, $insert_arr, $condition, $process_trigger_parent); 
            }
        }
        $parent_unique_value = $form_source_data[$parent_unique];
        $this->executeCreateUpdateResponseLine($form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $id_line, $parent_unique, $parent_unique_value, $number_of_condition, $additional_condition_filter_line);
    }

    private function getTrackNoParent ($form_source_parent_name, $parent_unique, $trackno_parent) {
        if (!empty($trackno_parent)) {
            return $trackno_parent;
        } else if (!empty($_POST[$parent_unique])) {
            $parent_unique_val = $_POST[$parent_unique];
            $trackno = $this->dao->selectDataFieldNameWhereCondition($form_source_parent_name, 'TrackNo', "{$parent_unique} = '{$parent_unique_val}'", 'row');
            return $trackno;
        } else {
            $trackno = $this->dao->getCurrentTrackNoWithTableName($form_source_parent_name);
            return $trackno;
        }
    }
    
    private function executeCreateUpdateResponseLine ($form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $id_line, $parent_unique, $parent_unique_value, $number_of_condition, $additional_condition_filter_line) {
        $this->dao->connect();
        $form_source_line_name = $this->dao->getFormTableName($form_source_line);

        $field_source_line_str = $this->utilities->setFieldToString($field_source_line, $parent_unique, $id_line[0]);
        $form_source_line_data = $this->dao->selectDataFieldNameWhereCondition($form_source_line_name, $field_source_line_str, "{$parent_unique} = '{$parent_unique_value}'", 'row');

        if (!empty($form_source_line_data[$parent_unique])) {
            $form_destination_line_name = $this->dao->getFormTableName($form_destination_line);
            
            $destination_line_status = $this->utilities->setStatus($status_line);

            $workflow_data = $this->getWorkflow($form_destination_line_name, $destination_line_status, $node_id_line);
            $insert_line_arr = $this->dao->getFormaMainFields($form_source_line_data, $workflow_data);

            $get_data_line_base = $this->dao->selectDataFieldNameWhereCondition($form_source_line_name, $field_source_line_str, "{$parent_unique} = '{$parent_unique_value}'", 'array');

            $counter_line = count($field_source_line) - 1;
            foreach ($get_data_line_base as $value_line_base) {
                for ($x_line = 0; $x_line <= $counter_line; $x_line++) {
                    $insert_line_arr[$field_destination_line[$x_line]] = $value_line_base[$field_source_line[$x_line]];
                }
                
                //Update Lines
                if ($number_of_condition == '2') {
                    $condition_line = array($id_line[1] => $value_line_base[$id_line[0]], 
                                            $parent_unique => $value_line_base[$parent_unique]);
                } else if ($number_of_condition == '1') {
                    $condition_line = array($id_line[1] => $value_line_base[$id_line[0]]);
                } else if ($number_of_condition == 'custom_identifier') {
                    $condition_line = array($id_line[1] => $value_line_base[$id_line[0]]);
                    foreach ($additional_condition_filter_line as $key_add_filter_line => $value_add_filter_line) {
                        $condition_line[$key_add_filter_line] = $value_line_base[$value_add_filter_line];
                    }
                }

                //Validate if Data Already Exists
                $validation = $this->validateCondition($form_destination_line_name, $condition_line);
                if ($validation == 'create') {
                    $this->processActionCreate($form_destination_line_name, $insert_line_arr, $process_trigger_line, 1);
                } else if ($validation == 'update') {
                    $this->processActionUpdate($form_destination_line_name, $insert_line_arr, $condition_line, $process_trigger_line);
                }    
            }
        }    
    }

    private function getWorkflow ($form, $status, $node_id) {
        if (!empty($node_id)) {
            $workflow = $this->dao->getWorkflowDataWithNodeId($form, $status, $node_id);
            return $workflow;
        } else {
            $workflow = $this->dao->getWorkflowData($form, $status);
            return $workflow;
        }
    }

    private function processActionCreate ($form, $data, $process_trigger, $pointer) {
        if ($process_trigger == 'Enable') {
            $this->dao->connect();
            $this->dao->insertDataWithProcessTrigger($form, $data);
        } else if ($process_trigger == 'Disable') {
            $current_trackno = $this->dao->getCurrentTrackNoWithTableName($form);
            $data['TrackNo'] = $this->utilities->incrementTrackNo($current_trackno, $pointer);
            $this->dao->insertDataGeneric($form, $data);
        }
    }

    private function processActionUpdate ($form, $data, $condition, $process_trigger) {
        if ($process_trigger == 'Enable') {
            $this->dao->connect();
            $this->dao->updateDataWithProcessTrigger($form, $data, $condition);
        } else if ($process_trigger == 'Disable') {
            $this->dao->updateData($form, $data, $condition);
        }
    }
    
    private function validateCondition ($form, $condition) {
        $conditions_string = '';
        $condition_qry = '';
        foreach ($condition as $key => $value) {
            $condition_qry .= "{$key} = '{$value}' AND ";
        }
        $conditions_string = substr($condition_qry, 0, strlen($condition_qry) - 4);
        $data = $this->dao->selectData($form, 'id', 'WHERE ' . $conditions_string, 'row');
        if ($data == '' || empty($data)) {
            return 'create';
        } else {
            return 'update';
        }
    }
}