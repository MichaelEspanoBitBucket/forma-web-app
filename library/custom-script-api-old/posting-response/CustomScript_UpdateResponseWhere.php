<?php

/**
 * Description of CustomScript_UpdateResponseWhere
 *
 * @author Joshua Clifford Reyes
 */

class CustomScript_UpdateResponseWhere {
	
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }

    public function customScriptUpdateResponseWhere ($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $params, $references) {

       /**
        * FORMULA:	
        *	@CustomScript("UpdateResponseWhere",
        *                     "Source:[]",                       -> $form_source
        *                     "Destination:[]",                  -> $form_destination
        *                     "Status:[]",                       -> $status
        *                     "NodeId:[]",                       -> $node_id
        *                     @TrackNo,                          -> $trackno
        *                     "ProcessTrigger:[]",               -> $process_trigger             Enable or Disable
        *                     ["Field", @Value, ...],            -> $params
        *                     ["Field", "operator", @value])     -> $references
        */
        
        $form_source_name = $this->dao->getFormTableName($form_source);
        $form_destination_name = $this->dao->getFormTableName($form_destination);
        
        $track_no = $this->getTrackNo($form_source_name, $trackno);
        $destination_status = $this->utilities->setStatus($status);

        $field_source_keys_str = $this->utilities->setFieldToString($arr = [], '', '');
        
        $form_source_data = $this->dao->selectDataFieldNameWhereCondition($form_source_name, $field_source_keys_str, "TrackNo = '{$track_no}'", 'row'); 
        $workflow_data = $this->getWorkflow($form_destination_name, $destination_status, $node_id);
        $insert_arr = $this->dao->getFormaMainFields($form_source_data, $workflow_data);
        
        foreach ($params as $key => $value) {
            $insert_arr[$key] = $value;
        }
        //Save
        $this->processAction($form_destination_name, $insert_arr, $references, $process_trigger);
    }

    private function getTrackNo ($form_source_name, $trackno) {
        if (empty($trackno)) {
            $trackno_value = $this->dao->getCurrentTrackNoWithTableName($form_source_name);
            return $trackno_value;
        } else {
            return $trackno;
        }
    }
    
    private function getWorkflow ($form, $status, $node_id) {
        if (empty($node_id)) {
            $workflow = $this->dao->getWorkflowData($form, $status);
            return $workflow;
        } else {
            $workflow = $this->dao->getWorkflowDataWithNodeId($form, $status, $node_id);
            return $workflow;
        }
    }

    private function processAction ($form, $data, $condition, $process_trigger) {
        if ($process_trigger == 'Enable') {
            $this->dao->updateDataWithProcessTriggerWhere($form, $data, $condition);
        } else if ($process_trigger == 'Disable') {
            $this->dao->updateDataWhere($form, $data, $condition);
        }
    }
}