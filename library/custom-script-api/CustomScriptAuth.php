<?php

/**
 * Description of CustomScriptAuth
 *
 * @author Joshua Clifford Reyes
 */

class CustomScriptAuth {

	/** @var APIDatabase */
    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }
	
	public function getAuthentication() {

        $authentication = new Auth();

        if ($authentication->hasAuth('current_user')) {

            $auth = $authentication->getAuth('current_user');
            return $auth;

        } else if ($GLOBALS['auth']) {

            $auth = $GLOBALS['auth'];
            return $auth;

        } else {

        	$options = getopt('f:r:u:');
        	$form_id = $options['f'];
        	$request_id = $options['r'];
        	$user_id = $options['u'];

            $query = "SELECT * FROM tbuser WHERE id = {$this->database->escape($user_id)}";
        	$auth = $this->database->query($query, 'row');
        	return $auth;
        }   
	}
}