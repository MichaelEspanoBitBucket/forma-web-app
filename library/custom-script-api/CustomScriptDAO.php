<?php

/**
 * Description of CustomScriptDAO
 *
 * @author Joshua Clifford Reyes
 */

class CustomScriptDAO {
    
    /** @var APIDatabase & CustomScriptAuth*/
    
    protected $allowed_mysql_functions = array(
        "NOW()", "CURDATE()", "CURTIME()"
    );

    protected $database;
    protected $auth;
    
    protected $currentForm;

    /** @param APIDatabase $database & CustomScriptAuth $auth*/
    public function __construct($database, $auth) {
        $this->database = $database;
        $this->auth = $auth;
    }

    private function getCompanyId() {
        $auth = $this->auth->getAuthentication();
        if ($auth) {
            $company_filter = "AND company_id = {$auth['company_id']}";
        } else {
            $company_filter = "AND company_id = {$GLOBALS['company_id']}";
        }
        return $company_filter;
    }
    
    public function getCurrentTrackNo($form) {
        $company_filter = $this->getCompanyId();
    
        $data = $this->database->query("SELECT form_table_name, form_name, reference_prefix, reference_type, company_id  FROM `tb_workspace` WHERE form_name = {$this->database->escape($form)} {$company_filter}", 'row');      
        $table_name = $data['form_table_name'];

        //$id = $this->database->query("SELECT MAX(id) as last_id FROM {$table_name}", 'row');
        $id = $this->database->query("SELECT  (`AUTO_INCREMENT`-1) as last_id FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA =  '". DB_NAME ."' AND TABLE_NAME =  '". $table_name ."' LIMIT 1", 'row');
        $last_id = $id['last_id'];
        
        $ref_prefix = $data['reference_prefix'];
        $ref_type = $data['reference_type'];

        $library = new library();
        $trackno_field['TrackNo'] = $library->getTrackNo($this->database, $ref_prefix, $ref_type, $last_id);      
    
        return $trackno_field['TrackNo'];
    }
    
    public function getCurrentTrackNoWithTableName($table_name) {
        $company_filter = $this->getCompanyId();
        
        $data = $this->database->query("SELECT form_table_name, form_name, reference_prefix, reference_type, company_id  FROM `tb_workspace` WHERE form_table_name = {$this->database->escape($table_name)} {$company_filter} LIMIT 1", 'row');
        
        //$id = $this->database->query("SELECT MAX(id) as last_id FROM {$table_name}", 'row');
        $id = $this->database->query("SELECT  (`AUTO_INCREMENT`-1) as last_id FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA =  '". DB_NAME ."' AND TABLE_NAME =  '". $table_name ."' LIMIT 1", 'row');
        $last_id = $id['last_id'];
        
        $ref_prefix = $data['reference_prefix'];
        $ref_type = $data['reference_type'];

        $library = new library();
        $trackno_field['TrackNo'] = $library->getTrackNo($this->database, $ref_prefix, $ref_type, $last_id);      

        return $trackno_field['TrackNo'];
    }

    public function getCurrentDate() {
        $date = $this->database->query("SELECT NOW() as date", 'row');
        return $date['date'];
    }

    public function getWorkflowData($form, $status) {
        $this->currentForm = $form;
        $query = "SELECT 
                        `tbworkflow_objects`.status,
                        `tbworkflow_objects`.processor, 
                        `tbworkflow_objects`.processorType, 
                        `tbworkflow_objects`.buttonStatus, 
                        `tbworkflow_objects`.object_id, 
                        `tbworkflow_objects`.workflow_id, 
                        `tbworkflow_objects`.fieldEnabled, 
                        `tbworkflow_objects`.fieldRequired, 
                        `tbworkflow_objects`.fieldHiddenValue,
                        `tbworkflow_objects`.enable_delegate,
                        `tbworkflow_objects`.json  
                  FROM  `tb_workspace`, `tbworkflow`, `tbworkflow_objects` 
                  WHERE `tb_workspace`.form_table_name = {$this->database->escape($form)} 
                  AND   `tbworkflow`.form_id = `tb_workspace`.id 
                  AND   `tbworkflow`.is_active = '1' 
                  AND   `tbworkflow`.id = `tbworkflow_objects`.workflow_id 
                  AND   `tbworkflow_objects`.status = {$this->database->escape($status)}";
        $workflow_data = $this->database->query($query, "row");
        return $workflow_data;
    }
    
    public function getWorkflowDataWithNodeId($form, $status, $node_id) {
        $this->currentForm = $form;
        $query = "SELECT 
                        `tbworkflow_objects`.status,
                        `tbworkflow_objects`.processor, 
                        `tbworkflow_objects`.processorType, 
                        `tbworkflow_objects`.buttonStatus, 
                        `tbworkflow_objects`.object_id, 
                        `tbworkflow_objects`.workflow_id, 
                        `tbworkflow_objects`.fieldEnabled, 
                        `tbworkflow_objects`.fieldRequired, 
                        `tbworkflow_objects`.fieldHiddenValue,
                        `tbworkflow_objects`.enable_delegate,
                        `tbworkflow_objects`.json  
                  FROM  `tb_workspace`, `tbworkflow`, `tbworkflow_objects` 
                  WHERE `tb_workspace`.form_table_name = {$this->database->escape($form)} 
                  AND   `tbworkflow`.form_id = `tb_workspace`.id 
                  AND   `tbworkflow`.is_active = '1' 
                  AND   `tbworkflow`.id = `tbworkflow_objects`.workflow_id 
                  AND   `tbworkflow_objects`.status = {$this->database->escape($status)}
                  AND   `tbworkflow_objects`.object_id = {$this->database->escape($node_id)}";
        $workflow_data = $this->database->query($query, "row");
        return $workflow_data;
    }

    //ON TRIGGER
    public function getFormaMainFields($form_data = [], $workflow_data = []) {

        $processor = '';
        
		switch ($workflow_data['processorType']) {
			case 1: //head|assistant head 
				// TO DO:
				$processor = $workflow_data['processor'];
				break;
			case 2: //position
				$processor = $workflow_data['processor'];
				break;
			case 3:	//specific user
				$processor = $workflow_data['processor'];
				break;
			case 4: //requestor
				$processor = $form_data['Requestor'];
				break;
			case 5: //computed field
                $result = $this->database->query("SELECT `".$workflow_data['processor']."` as 'ProcessorField' FROM `".$this->currentForm."` WHERE `".$this->currentForm."`.`TrackNo` = '".$form_data['TrackNo']."'","row");
                /*
                    Specific Requirements
                        Converting Field Value (Contains Display Name instead of ID) into ID
                    handle by: Joshua Reyes [11-22-2016]
                */
                    //commented by Krist Iann Tablan [01-25-2017]
                    // $processorId = $this->database->query("SELECT id FROM  `tbuser` WHERE display_name =  '{$result['ProcessorField']}'", "row");
                    // $processor = $processorId['id'];
                /*
                    Specific Requirements
                        Converting Field Value (Contains Multiple Display Name) into ID  
                    handle by: Krist Iann Tablan [01-25-2017]
                */
                $processorId = implode("','", explode("|", $result['ProcessorField']));
                $id = $this->database->query("SELECT `id` FROM `tbuser` WHERE `display_name` IN  ('{$processorId}')", "array");
                foreach ($id as $key => $value) {
                    $procId .= $value['id']. ',';
                }
                $processor = substr($procId, 0, strlen($procId)-1);
				break;
			case 6: //group
				$processor = $workflow_data['processor'];
				break;
		}
		
        $workflow_json = json_decode($workflow_data['json'], true);
		
        $data = array(
			'Requestor'			=> $form_data['Requestor'],             
			'Status'            => $workflow_data['status'],
            'Processor'       	=> $processor,                          
			'ProcessorType'     => $workflow_data['processorType'], 
			'LastAction'      	=> $workflow_data['buttonStatus'],      
			'DateCreated'       => $form_data['DateCreated'], 
            'DateUpdated'     	=> $this->getCurrentDate(),           	
			'CreatedBy'         => $form_data['CreatedBy'],
            'UpdatedBy'       	=> $form_data['UpdatedBy'],             
			'Node_ID'           => $workflow_data['object_id'],
            'Workflow_ID'     	=> $workflow_data['workflow_id'],       
			'fieldEnabled'      => $workflow_data['fieldEnabled'],
            'fieldRequired'   	=> $workflow_data['fieldRequired'],     
			'fieldHiddenValues' => $workflow_data['fieldHiddenValue'],
            'SaveFormula'     	=> $workflow_json['workflow_hw_save'],  
			'CancelFormula'     => $workflow_json['workflow_hw_cancel'],
			'enable_delegate'	=> $workflow_data['enable_delegate']
		);
			
        return $data;
    }

    //ON CHANGE
    public function getFormaMainFieldsCustom($workflow_data = []) {
        //$auth = new Auth();
        //$authID = $auth->getAuth('current_user');
        $auth = $this->auth->getAuthentication();
        $date = $this->getCurrentDate();
        $processor = '';
        if ($workflow_data['processor'] == '1') {
            $processor = $auth['id'];
        } else {
            $processor = $workflow_data['processor'];
        }
        
        $workflow_json = json_decode($workflow_data['json'], true);
        $data = array('Requestor'       => $auth['id'],                         'Status'            => $workflow_data['status'],
                      'Processor'       => $processor,                          'ProcessorType'     => $workflow_data['processorType'],
                      'LastAction'      => $workflow_data['buttonStatus'],      'DateCreated'       => $date, 
                      'DateUpdated'     => $date,                               'CreatedBy'         => $auth['id'],                            
                      'UpdatedBy'       => '',                                  'Node_ID'           => $workflow_data['object_id'],         
                      'Workflow_ID'     => $workflow_data['workflow_id'],       'fieldEnabled'      => $workflow_data['fieldEnabled'],      
                      'fieldRequired'   => $workflow_data['fieldRequired'],     'fieldHiddenValues' => $workflow_data['fieldHiddenValue'],
                      'SaveFormula'     => $workflow_json['workflow_hw_save'],  'CancelFormula'     => $workflow_json['workflow_hw_cancel'],  
                      'enable_delegate' => $workflow_data['enable_delegate']);
        return $data;
    }

    public function getFormaOptionalFields($form_data = []) {
        $data = array('ProcessorLevel'            => $form_data['ProcessorLevel'],
                      'imported'                  => $form_data['imported'],
                      'Repeater_Data'             => $form_data['Repeater_Data'],
                      'Editor'                    => $form_data['Editor'],
                      'Viewer'                    => $form_data['Viewer'],
                      'middleware_process'        => $form_data['middleware_process'],
                      'SaveFormula'               => $form_data['SaveFormula'],
                      'CancelFormula'             => $form_data['CancelFormula']);
        return $data;
    }

    public function getFormTableName($form) {
        $company_filter = $this->getCompanyId();
        $query = "SELECT form_table_name, company_id FROM `tb_workspace` WHERE form_name = {$this->database->escape($form)} {$company_filter}";
        $form_name  = $this->database->query($query, 'row');
        return $form_name['form_table_name'];
    }

    public function getFormData($form, $trackno) {
        $query = "SELECT * FROM {$form} WHERE TrackNo = {$this->database->escape($trackno)}";
        $form_data = $this->database->query($query, 'row');
        return $form_data;
    }
    
    public function selectData($form, $field = '', $condition = '', $type) {
        $query = "SELECT {$field} FROM {$form} {$condition}";
        $data = $this->database->query($query, $type);
        return $data;
    }

    public function selectDataWhereCondition($form, $condition = '', $type) {
        $query = "SELECT * FROM {$form} WHERE {$condition}";
        $data = $this->database->query($query, $type);
        return $data;
    }

    public function selectDataFieldNameWhereCondition($form, $field = '', $condition = '', $type) {
        $query = "SELECT {$field} FROM {$form} WHERE {$condition}";
        $data = $this->database->query($query, $type);
        return $data;
    }

    public function deleteData($form, $condition = '') {
        $query = "DELETE FROM {$form} WHERE {$condition}";
        $this->database->query($query, 'update');
    }

    public function insertData($form, $data = []) {
        
        $request_doc = new Request();
        $auth = $this->auth->getAuthentication();
        $form_data = $this->database->query("SELECT `tb_workspace`.`id` as frmTBWS_Id FROM `tb_workspace` WHERE form_table_name = {$this->database->escape($form)}", 'row');
        $request_doc->load($form_data['frmTBWS_Id'], '', $auth['id']);
        $request_doc->data = $data;
        $request_doc->save();
		
		//Clear Redis Cached
        $this->clearRedisCached($form_data['frmTBWS_Id'], '', 'create');
    }
    
    public function insertDataGeneric($form, $data = []) {
        $form_data = $this->database->query("SELECT `tb_workspace`.`id` as frmTBWS_Id FROM `tb_workspace` WHERE form_table_name = {$this->database->escape($form)}", 'row');
        $this->database->insert($form, $data);   
		
		//Clear Redis Cached
        $this->clearRedisCached($form_data['frmTBWS_Id'], '', 'create');
    }

    public function insertDataWithProcessTrigger($form, $data = []) {
        
        $request_doc = new Request();
        $auth = $this->auth->getAuthentication();
        $form_data = $this->database->query("SELECT `tb_workspace`.`id` as frmTBWS_Id FROM `tb_workspace` WHERE form_table_name = {$this->database->escape($form)}", 'row');
        $request_doc->load($form_data['frmTBWS_Id'], '', $auth['id']);
        $request_doc->data = $data;
        $request_doc->save();
        $request_doc->process_triggers = true;
        // $request_doc->processWF(); // OLD WAY BUG: No processor, DESC: processWF is checking processor type
		$request_doc->processWorkflowEvents();
		
		//Clear Redis Cached
        $this->clearRedisCached($form_data['frmTBWS_Id'], '', 'create');
    }

    public function updateData($form, $data = [], $condition = []) {
        
        $filter_str = '';
        foreach ($condition as $key => $value) {
            $filter_str .= " AND `{$form}`.`{$key}` = {$this->database->escape($value)}";  
        }

		$form_data = $this->database->query("SELECT `tb_workspace`.`id` as frmTBWS_Id, `{$form}`.`id` as frmTB_Id FROM `tb_workspace`, `{$form}` WHERE `tb_workspace`.`form_table_name` = {$this->database->escape($form)} {$filter_str}", 'row');
        $this->database->update($form, $data, $condition);

		//Clear Redis Cached
        // $this->clearRedisCached($form_data['frmTBWS_Id'], $request_doc->id, 'update');
		$this->clearRedisCached($form_data['frmTBWS_Id'], '', 'create');
    }

    public function updateDataWhere($form, $data = [], $condition = []) {

        $cond_string = '';
        $condition_count = count($condition);
        for($x = 0; $x < $condition_count; $x += 3) {
            $field = $condition[$x];
            $operator = $condition[$x+1];
            $value = $condition[$x+2];
            if ($operator == "IN") {
                
                $explodedValue = (explode('|', $value));
                $inValue_string = '';
                foreach($explodedValue as $inValue) {
                    $inValue_string .= "{$this->database->escape($inValue)},";
                }
                $inValue_string  = substr($inValue_string, 0, strlen($inValue_string) - 1);
                $cond_string .= "`{$form}`.`{$field}` {$operator} ({$inValue_string}) AND ";   
            
            } else {
                $cond_string .= "`{$form}`.`{$field}` {$operator} {$this->database->escape($value)} AND "; 
            }  
        }
        $cond_string  = substr($cond_string, 0, strlen($cond_string) - 4);

        $field_string = '';
        foreach ($data as $field => $value) {
            $value = !in_array($value, $this->allowed_mysql_functions) && !is_numeric($value) ?
                    $this->database->escape($value) : $value;
            $field_string .= $field . "=" . $value . ",";
        }
        $field_string = substr($field_string, 0, strlen($field_string) - 1);
        $form_data = $this->database->query("SELECT `tb_workspace`.`id` as frmTBWS_Id, `{$form}`.`id` as frmTB_Id FROM `tb_workspace`, `{$form}` WHERE `tb_workspace`.`form_table_name` = {$this->database->escape($form)} AND {$cond_string}", 'row');
        $this->database->query("UPDATE {$form} SET {$field_string} WHERE {$cond_string}", "update");
        
        //Clear Redis Cached
        $this->clearRedisCached($redis_cache, $form_data['frmTBWS_Id'], $form_data['frmTB_Id'], 'update');
    }

    public function updateDataWithProcessTrigger($form, $data = [], $condition = []) {

        $filter_str = '';
        foreach ($condition as $key => $value) {
            $filter_str .= " AND `{$form}`.`{$key}` = {$this->database->escape($value)}";  
        }
        $form_data = $this->database->query("SELECT `tb_workspace`.`id` as frmTBWS_Id, `{$form}`.`id` as frmTB_Id FROM `tb_workspace`, `{$form}` WHERE `tb_workspace`.`form_table_name` = {$this->database->escape($form)} {$filter_str}", 'row');
        $request_doc = new Request();
        $auth = $this->auth->getAuthentication();
        $request_doc->load($form_data['frmTBWS_Id'], $form_data['frmTB_Id'], $auth['id']);
        $request_doc->data = $data;
        $request_doc->modify();
        $request_doc->process_triggers = true;
        // $request_doc->processWF(); // OLD WAY BUG: No processor, DESC: processWF is checking processor type
		$request_doc->processWorkflowEvents();
		
		//Clear Redis Cached
        // $this->clearRedisCached($form_data['frmTBWS_Id'], $form_data['frmTB_Id'], 'update');
		$this->clearRedisCached($form_data['frmTBWS_Id'], '', 'create');
    }

    public function updateDataWithProcessTriggerWhere($form, $data = [], $condition = []) {
        
        $condition_count = count($condition);
        for($x = 0; $x < $condition_count; $x += 3) {
            $field = $condition[$x];
            $operator = $condition[$x+1];
            $value = $condition[$x+2];
            $cond_string .= "`{$form}`.`{$field}` {$operator} {$this->database->escape($value)} AND ";
        }
        $cond_string  = substr($cond_string, 0, strlen($cond_string) - 4);

        $field_string = '';
        foreach ($data as $field => $value) {
            $value = !in_array($value, $this->allowed_mysql_functions) && !is_numeric($value) ?
                    $this->escape($value) : $value;
            $field_string .= $field . "=" . $value . ",";
        }
        $field_string = substr($field_string, 0, strlen($field_string) - 1);

        $this->database->query("UPDATE {$form} SET {$field_string} WHERE {$cond_string}", "update");
        $form_data = $this->database->query("SELECT `tb_workspace`.`id` as frmTBWS_Id, `{$form}`.`id` as frmTB_Id FROM `tb_workspace`, `{$form}` WHERE `tb_workspace`.`form_table_name` = {$this->database->escape($form)} AND {$cond_string}", 'row');
        $request_doc = new Request();
        $auth = $this->auth->getAuthentication();
        $request_doc->load($form_data['frmTBWS_Id'], $form_data['frmTB_Id'], $auth['id']);
        $request_doc->data = $data;
        $request_doc->process_triggers = true;
        // $request_doc->processWF(); // OLD WAY BUG: No processor, DESC: processWF is checking processor type
		$request_doc->processWorkflowEvents();
		
		//Clear Redis Cached
        // $this->clearRedisCached($form_data['frmTBWS_Id'], $form_data['frmTB_Id'], 'update');
		$this->clearRedisCached($form_data['frmTBWS_Id'], '', 'create');
    }
	
    public function BEGIN_TRANSACT() {	
        $this->database->beginTransaction();
    }

    public function COMMIT_TRANSACT() {
        $this->database->commit();
    }

    public function connect() {
        $this->database->connect();
    }

    public function close() {
        $this->database->disconnect();
    }
	
	public function insertDataBulkLoadData($form, $data) {
        $redis_cache = getRedisConnection();

        $db_bq = new DatabaseBulkQueries();

        $form_data = $this->database->query("SELECT id FROM tb_workspace WHERE form_table_name = {$this->database->escape($form)}", 'row');
		//Clear Redis Cached
        $this->clearRedisCached($form_data['id'], '', 'create');

        $db_bq->init($data, $form, 'LOAD_DATA_INFILE',
            array(
                'action' => 'write_data',
                'query_handler' => 'mysql_query',
                'trigger_errors' => false,
                'clean_memory' => true,
                'link_identifier' => null
            )
        );
        $db_bq->load();
        $db_bq->execute();
        $db_bq->loadDataInFileCleanUp();
    }

    private function clearRedisCached($form_id, $doc_id, $response_type) {

		if ($response_type == "update") {
			functions::clearRequestRelatedCache("request_details_" . $form_id, $doc_id);
			functions::clearRequestRelatedCache("request_access_" . $form_id, $doc_id);
		}
		
		$myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

		$deleteMemecachedKeys = array_merge($myrequest_memcached);

		functions::deleteMemcacheKeys($deleteMemecachedKeys);

		functions::clearRequestRelatedCache("request_list", "form_id_" . $form_id);
		functions::clearRequestRelatedCache("picklist", "form_id_" . $form_id);
		functions::clearRequestRelatedCache("calendar_view", "form_id_" . $form_id);
		functions::clearRequestRelatedCache("request_list_count", "form_id_" . $form_id);

		//formula
		$formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
			"formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
			"formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
			"formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
		functions::clearFormulaLookupCaches($form_id, $formula_arr);
    }
}