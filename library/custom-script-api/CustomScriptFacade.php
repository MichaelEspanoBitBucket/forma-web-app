<?php

include_once API_LIBRARY_PATH . 'API.php';
include_once 'CustomScriptAuth.php';
include_once 'CustomScriptDAO.php';
include_once 'CustomScriptUtilities.php';

/**
 * Description of CustomScriptFacade
 *
 * @author Joshua Clifford Reyes
 */

class CustomScriptFacade {

    /*================== POSTING-RESPONSE ==================*/
    public function executeCreateResponse ($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $params) {
        include_once 'posting-response/CustomScript_CreateResponse.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_createresponse = new CustomScript_CreateResponse($dao, $utilities);
            
        $database->connect();

        $form_source_converted = $utilities->clearParamString($form_source);
        $form_destination_converted = $utilities->clearParamString($form_destination);
        $status_converted = $utilities->clearParamString($status);
        $node_id_converted = $utilities->clearParamString($node_id);
        $process_trigger_converted = $utilities->clearParamString($process_trigger);
        $params_converted = $utilities->convertStringToArray($params);

        $customscript_createresponse->customScriptCreateResponse($form_source_converted, $form_destination_converted, $status_converted, $node_id_converted, $trackno, $process_trigger_converted, $params_converted);

        $database->disconnect();
    }
    
    public function executeCreateResponseWithLine ($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $parent_unique) {
        include_once 'posting-response/CustomScript_CreateResponseWithLine.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_createResponseWithLine = new CustomScript_CreateResponseWithLine($dao, $utilities);

        $database->connect();

        //PARENT
        $form_source_parent_converted = $utilities->clearParamString($form_source_parent);
        $field_source_parent_converted = $utilities->sanitizedParamString($field_source_parent);

        $form_destination_parent_converted = $utilities->clearParamString($form_destination_parent);
        $field_destination_parent_converted = $utilities->sanitizedParamString($field_destination_parent);

        $status_parent_converted = $utilities->clearParamString($status_parent);
        $node_id_parent_converted = $utilities->clearParamString($node_id_parent);
        $process_trigger_parent_converted = $utilities->clearParamString($process_trigger_parent);
        //LINE
        $form_source_line_converted = $utilities->clearParamString($form_source_line);
        $field_source_line_converted = $utilities->sanitizedParamString($field_source_line);
        
        $form_destination_line_converted = $utilities->clearParamString($form_destination_line);
        $field_destination_line_converted = $utilities->sanitizedParamString($field_destination_line);
        
        $status_line_converted = $utilities->clearParamString($status_line);
        $node_id_line_converted = $utilities->clearParamString($node_id_line);
        $process_trigger_line_converted = $utilities->clearParamString($process_trigger_line);
        
        $customscript_createResponseWithLine->customScriptCreateResponseWithLine($form_source_parent_converted, $field_source_parent_converted, $form_destination_parent_converted, $field_destination_parent_converted, $status_parent_converted, $node_id_parent_converted, $trackno_parent, $process_trigger_parent_converted, $form_source_line_converted, $field_source_line_converted, $form_destination_line_converted, $field_destination_line_converted, $status_line_converted, $node_id_line_converted, $process_trigger_line_converted, $parent_unique);

        $database->disconnect();
    }
    
    public function executeUpdateResponse ($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $id_field, $id_value, $parent_field, $parent_value, $params) {
        include_once 'posting-response/CustomScript_UpdateResponse.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_updateresponse = new CustomScript_UpdateResponse($dao, $utilities);

        $database->connect();

        $form_source_converted = $utilities->clearParamString($form_source);
        $form_destination_converted = $utilities->clearParamString($form_destination);
        $status_converted = $utilities->clearParamString($status);
        $node_id_converted = $utilities->clearParamString($node_id);
        $process_trigger_converted = $utilities->clearParamString($process_trigger);
        $id_field_converted = $utilities->clearParamString($id_field);
        $parent_field_converted = $utilities->clearParamString($parent_field);
        $params_converted = $utilities->convertStringToArray($params);

        $customscript_updateresponse->customScriptUpdateResponse($form_source_converted, $form_destination_converted, $status_converted, $node_id_converted, $trackno, $process_trigger_converted, $id_field_converted, $id_value, $parent_field_converted, $parent_value, $params_converted);

        $database->disconnect();
    }
    
    public function executeUpdateResponseWhere ($form_source, $form_destination, $status, $node_id, $trackno, $process_trigger, $params, $references) {
        include_once 'posting-response/CustomScript_UpdateResponseWhere.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_updateresponseWhere = new CustomScript_UpdateResponseWhere($dao, $utilities);

        $database->connect();

        $form_source_converted = $utilities->clearParamString($form_source);
        $form_destination_converted = $utilities->clearParamString($form_destination);
        $status_converted = $utilities->clearParamString($status);
        $node_id_converted = $utilities->clearParamString($node_id);
        $process_trigger_converted = $utilities->clearParamString($process_trigger);
        $params_converted = $utilities->convertStringToArray($params);

        $customscript_updateresponseWhere->customScriptUpdateResponseWhere($form_source_converted, $form_destination_converted, $status_converted, $node_id_converted, $trackno, $process_trigger_converted, $params_converted, $references);

        $database->disconnect();
    }
    
    public function executeUpdateResponseWithLine ($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $id_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $id_line, $parent_unique){
        include_once 'posting-response/CustomScript_UpdateResponseWithLine.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();

        $customscript_updateResponseWithLine = new CustomScript_UpdateResponseWithLine($dao, $utilities);

        $database->connect();

        //PARENT
        $form_source_parent_converted = $utilities->clearParamString($form_source_parent);
        $field_source_parent_converted = $utilities->sanitizedParamString($field_source_parent);

        $form_destination_parent_converted = $utilities->clearParamString($form_destination_parent);
        $field_destination_parent_converted = $utilities->sanitizedParamString($field_destination_parent);

        $status_parent_converted = $utilities->clearParamString($status_parent);
        $node_id_parent_converted = $utilities->clearParamString($node_id_parent);
        $process_trigger_parent_converted = $utilities->clearParamString($process_trigger_parent);
        
        $id_parent_converted = $utilities->sanitizedParamString($id_parent);
        //LINE
        $form_source_line_converted = $utilities->clearParamString($form_source_line);
        $field_source_line_converted = $utilities->sanitizedParamString($field_source_line);
        
        $form_destination_line_converted = $utilities->clearParamString($form_destination_line);
        $field_destination_line_converted = $utilities->sanitizedParamString($field_destination_line);

        $status_line_converted = $utilities->clearParamString($status_line);
        $node_id_line_converted = $utilities->clearParamString($node_id_line);
        $process_trigger_line_converted = $utilities->clearParamString($process_trigger_line);
        $id_line_converted = $utilities->sanitizedParamString($id_line);

        $customscript_updateResponseWithLine->customScriptUpdateResponseWithLine($form_source_parent_converted, $field_source_parent_converted, $form_destination_parent_converted, $field_destination_parent_converted, $status_parent_converted, $node_id_parent_converted, $trackno_parent, $process_trigger_parent_converted, $id_parent_converted, $form_source_line_converted, $field_source_line_converted, $form_destination_line_converted, $field_destination_line_converted, $status_line_converted, $node_id_line_converted, $process_trigger_line_converted, $id_line_converted, $parent_unique);

        $database->disconnect();
    }
    
    //ADDED BY JOSHUA CLIFFORD REYES January 26, 2016 12PM
    public function executeCreateUpdateResponseWithLine ($form_source_parent, $field_source_parent, $form_destination_parent, $field_destination_parent, $status_parent, $node_id_parent, $trackno_parent, $process_trigger_parent, $id_parent, $form_source_line, $field_source_line, $form_destination_line, $field_destination_line, $status_line, $node_id_line, $process_trigger_line, $id_line, $parent_unique, $number_of_condition, $additional_condition_filter_line){
        include_once 'posting-response/CustomScript_CreateUpdateResponseWithLine.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();

        $customscript_createUpdateResponseWithLine = new CustomScript_CreateUpdateResponseWithLine($dao, $utilities);

        $database->connect();

        //PARENT
        $form_source_parent_converted = $utilities->clearParamString($form_source_parent);
        $field_source_parent_converted = $utilities->sanitizedParamString($field_source_parent);

        $form_destination_parent_converted = $utilities->clearParamString($form_destination_parent);
        $field_destination_parent_converted = $utilities->sanitizedParamString($field_destination_parent);

        $status_parent_converted = $utilities->clearParamString($status_parent);
        $node_id_parent_converted = $utilities->clearParamString($node_id_parent);
        $process_trigger_parent_converted = $utilities->clearParamString($process_trigger_parent);
        
        $id_parent_converted = $utilities->sanitizedParamString($id_parent);
        //LINE
        $form_source_line_converted = $utilities->clearParamString($form_source_line);
        $field_source_line_converted = $utilities->sanitizedParamString($field_source_line);
        
        $form_destination_line_converted = $utilities->clearParamString($form_destination_line);
        $field_destination_line_converted = $utilities->sanitizedParamString($field_destination_line);

        $status_line_converted = $utilities->clearParamString($status_line);
        $node_id_line_converted = $utilities->clearParamString($node_id_line);
        $process_trigger_line_converted = $utilities->clearParamString($process_trigger_line);
        $id_line_converted = $utilities->sanitizedParamString($id_line);

        $additional_condition_filter_line_converted = $utilities->convertStringToArray($additional_condition_filter_line);

        $customscript_createUpdateResponseWithLine->customScriptCreateUpdateResponseWithLine($form_source_parent_converted, $field_source_parent_converted, $form_destination_parent_converted, $field_destination_parent_converted, $status_parent_converted, $node_id_parent_converted, $trackno_parent, $process_trigger_parent_converted, $id_parent_converted, $form_source_line_converted, $field_source_line_converted, $form_destination_line_converted, $field_destination_line_converted, $status_line_converted, $node_id_line_converted, $process_trigger_line_converted, $id_line_converted, $parent_unique, $number_of_condition, $additional_condition_filter_line_converted);

        $database->disconnect();
    }
    /*=======================================================*/
    
    /*================== POSTING-DYNAMIC-RESPONSE ==================*/
    public function executeCreateDataOnChange ($form, $fields, $status) {
        include_once 'posting-dynamic-response/CustomScript_CreateDataOnChange.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        
        $customscript_createDataOnChange = new CustomScript_CreateDataOnChange($dao);
        
        $database->connect();
        
        $form_converted = $utilities->clearParamString($form);
        $fields_converted = $utilities->convertStringToArray($fields);
        $status_converted = $utilities->clearParamString($status);
        
        $process = $customscript_createDataOnChange->customScriptCreateDataOnChange($form_converted, $fields_converted, $status_converted);
        
        $database->disconnect();
        return $process;
    }
    
    public function executeUpdateDataOnChange ($form, $fields, $id_name, $id_value, $parent_name, $parent_value) {
        include_once 'posting-dynamic-response/CustomScript_UpdateDataOnChange.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        
        $customscript_updateDataOnChange = new CustomScript_UpdateDataOnChange($dao);
        
        $database->connect();
        
        $form_converted = $utilities->clearParamString($form);
        $fields_converted = $utilities->convertStringToArray($fields);
        $id_name_converted = $utilities->clearParamString($id_name);
        $parent_name_converted = $utilities->clearParamString($parent_name);
        
        $customscript_updateDataOnChange->customScriptUpdateDataOnChange($form_converted, $fields_converted, $id_name_converted, $id_value, $parent_name_converted, $parent_value);
        
        $database->disconnect();
    }
	
    public function executeUpdateDataOnChangeWhere ($form, $fields, $references) {
        include_once 'posting-dynamic-response/CustomScript_UpdateDataOnChangeWhere.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        
        $customscript_updateDataOnChangeWhere = new CustomScript_UpdateDataOnChangeWhere($dao);
        
        $database->connect();
        
        $form_converted = $utilities->clearParamString($form);
        $fields_converted = $utilities->convertStringToArray($fields);
        
        $customscript_updateDataOnChangeWhere->customScriptUpdateDataOnChangeWhere($form_converted, $fields_converted, $references);
        
        $database->disconnect();
    }
    
    public function executeComputeDataOnChange ($form, $fields, $return_value, $line_name, $id_name, $id_value, $parent_name, $parent_value, $computation) {
        include_once 'posting-dynamic-response/CustomScript_ComputeDataOnChange.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        
        $customscript_computeDataOnChange = new CustomScript_ComputeDataOnChange($dao, $utilities);
        
        $database->connect();
        
        $form_converted = $utilities->clearParamString($form);
        $fields_converted = $utilities->convertStringToArray($fields);
        $return_value_converted = $utilities->clearParamString($return_value);
        $line_name_converted = $utilities->clearParamString($line_name);
        $id_name_converted = $utilities->clearParamString($id_name);
        $parent_name_converted = $utilities->clearParamString($parent_name);
        $computation_converted = $utilities->clearParamString($computation);
        
        $process = $customscript_computeDataOnChange->customScriptComputeDataOnChange($form_converted, $fields_converted, $return_value_converted, $line_name_converted, $id_name_converted, $id_value, $parent_name_converted, $parent_value, $computation_converted);
    
        $database->disconnect();
        
        return $process;
    }
    /*==============================================================*/
   
    /*================== MISC ==================*/
    public function executeStringSeparator ($form, $field, $filter, $status) {
        include_once 'misc/CustomScript_StringSeparator.php';

        $database = new APIDatabase();	
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_stringseparator = new CustomScript_StringSeparator($dao);

        $database->connect();

        $form_converted = $utilities->clearParamString($form);
        $field_converted = $utilities->convertParamString($field);
        $filter_converted = $utilities->clearParamString($filter);
        $status_converted = $utilities->clearParamString($status);

        $customscript_stringseparator->customScriptStringSeparator($form_converted, $field_converted, $filter_converted, $status_converted);

        $database->disconnect();
    }

    public function executeRecordCountWithFilter ($form, $filter_name, $filter_value) {
        include_once 'misc/CustomScript_RecordCountWithFilter.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_recordcountwithfilter = new CustomScript_RecordCountWithFilter($dao);

        $database->connect();

        $form_converted = $utilities->clearParamString($form);
        $filter_name_converted = $utilities->clearParamString($filter_name);
        $process = $customscript_recordcountwithfilter->customScriptRecordCountWithFilter($form_converted, $filter_name_converted, $filter_value);

        $database->disconnect();

        return $process;
    }
    
    public function executeLookUp ($form, $field, $field_id, $id, $return_type) {
        include_once 'misc/CustomScript_LookUp.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_lookup = new CustomScript_LookUp($dao);
        
        $database->connect();
        
        $form_converted = $utilities->clearParamString($form);
        $field_converted = $utilities->clearParamString($field);
        $field_id_converted = $utilities->clearParamString($field_id);
        
        $return_type_converted = $utilities->sanitizedParamString($return_type);
        
        $process = $customscript_lookup->customScriptLookUp($form_converted, $field_converted, $field_id_converted, $id, $return_type_converted);
        
        $database->disconnect();
        return $process;
    }
    
    //ADDED BY JOSHUA CLIFFORD REYES January 27, 2016 12PM
    public function executeLookUpWhere ($form, $return_field, $filter_params) {
        include_once 'misc/CustomScript_LookUpWhere.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_lookUpWhere = new CustomScript_LookUpWhere($dao, $utilities);
        
        $database->connect();
        
        $form_converted = $utilities->clearParamString($form);
        $return_field_converted = $utilities->clearParamString($return_field);
        $filter_params_converted = $utilities->convertStringToArray($filter_params);
        
        $process = $customscript_lookUpWhere->customScriptLookUpWhere($form_converted, $return_field_converted, $filter_params_converted);
        
        $database->disconnect();
        
        return $process; 
    }
    
    //ADDED BY JOSHUA CLIFFORD REYES January 28, 2016 2PM
    public function executeLookUpWhereStringReturn ($form, $return_field, $separator, $id_field, $id_value) {
        include_once 'misc/CustomScript_LookUpWhereStringReturn.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_lookUpWhereStringReturn = new CustomScript_LookUpWhereStringReturn($dao, $utilities);
        
        $database->connect();
        
        $form_converted = $utilities->clearParamString($form);
        $return_field_converted = $utilities->clearParamString($return_field);
        $id_field_converted = $utilities->clearParamString($id_field);
        
        $process = $customscript_lookUpWhereStringReturn->customScriptLookUpWhereStringReturn($form_converted, $return_field_converted, $separator, $id_field_converted, $id_value);
        
        $database->disconnect();
        
        return $process;
    }
    
    //ADDED BY JOSHUA CLIFFORD REYES January 27, 2016 5PM
    public function executePickListDataSource ($source_form, $source_field, $destination_form, $destination_field, $destination_status, $destination_node_id, $source_field_id, $picklist_value, $unique_key_params, $data_send_params, $condition_params) {
        include_once 'misc/CustomScript_PickListDataSource.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_pickListDataSource = new CustomScript_PickListDataSource($dao, $utilities);
        
        $database->connect();
        
        $source_form_converted = $utilities->clearParamString($source_form);
        $source_field_converted = $utilities->sanitizedParamString($source_field);
        $destination_form_converted = $utilities->clearParamString($destination_form);
        $destination_field_converted = $utilities->sanitizedParamString($destination_field);
        
        $destination_status_converted = $utilities->clearParamString($destination_status);
        $destination_node_id_converted = $utilities->clearParamString($destination_node_id);
        
        $source_field_id_converted = $utilities->clearParamString($source_field_id);
        
        $unique_key_params_converted = $utilities->convertStringToArray($unique_key_params);
        $data_send_params_converted = $utilities->convertStringToArray($data_send_params);
        
        $process = $customscript_pickListDataSource->customScriptPickListDataSource($source_form_converted, $source_field_converted, $destination_form_converted, $destination_field_converted, $destination_status_converted, $destination_node_id_converted, $source_field_id_converted, $picklist_value, $unique_key_params_converted, $data_send_params_converted, $condition_params);
        
        $database->disconnect();
    
        return $process;
    }
    
    //ADDED BY JOSHUA CLIFFORD REYES January 28, 2016 5PM
    public function executeGroupUser ($type_use, $group_name) {
        include_once 'misc/CustomScript_GroupUser.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        //$utilities = new CustomScriptUtilities();
        $customscript_groupUserCount = new CustomScript_GroupUser($dao);
        
        $database->connect();
        
        $process = $customscript_groupUserCount->customScriptGroupUser($type_use, $group_name);
        
        $database->disconnect();
        
        return $process;
    }

    //ADDED BY Krist Iann Tablan May 28, 2016 1820H
    public function executeGetUserDelegate ($delegate_name) {
        include_once 'misc/CustomScript_GetUserDelegate.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        
        $customscript_getUserDelegate = new CustomScript_GetUserDelegate($dao);
        
        $database->connect();
        
        $process = $customscript_getUserDelegate->customScriptGetUserDelegate($delegate_name);
        
        $database->disconnect();
        
        return $process;
    }

    //ADDED BY Krist Iann Tablan July 6, 2016 1116H
    public function executeGetWorkDateTime ($type, $start_date, $days) {
        include_once 'misc/CustomScript_GetWorkDateTime.php';
        
        // $database = new APIDatabase();
        // $auth = new CustomScriptAuth($database);
        // $dao = new CustomScriptDAO($database, $auth);

        $customscript_getWorkDateTime = new CustomScript_GetWorkDateTime($dao);
        
        // $database->connect();
        
        $process = $customscript_getWorkDateTime->customscript_getWorkDateTime($type, $start_date, $days);
        
        // $database->disconnect();
        
        return $process;
    }

    public function executeGetUserID ($display_name) {
        include_once 'misc/CustomScript_GetUserID.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);

        $customscript_getUserID = new CustomScript_GetUserID($dao);

        $database->connect();

        $process = $customscript_getUserID->customScript_GetUserID($display_name);

        $database->disconnect();

        return $process;
    }
    
    public function executeRecordDelete ($form, $field_id, $id) {
        include_once 'misc/CustomScript_RecordDelete.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_recorddelete = new CustomScript_RecordDelete($dao);
        
        $database->connect();
        
        $form_converted = $utilities->clearParamString($form);
        $field_id_converted = $utilities->clearParamString($field_id);
        
        $customscript_recorddelete->customScriptRecordDelete($form_converted, $field_id_converted, $id);
        
        $database->disconnect();
    }
    /*==========================================*/
    
    /*================== CUSTOMIZED - FUNCTION ==================*/
    public function executeGenerateId ($form_used, $series_code, $start_series, $end_series, $separator) {
        include_once 'customized-function/CustomScript_GenerateId.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customScript_generateId = new CustomScript_GenerateId($dao, $utilities);
        
        $database->connect();
        
        $form_used_converted = $utilities->clearParamString($form_used);
        
        $process = $customScript_generateId->customScriptGenerateId($form_used_converted, $series_code, $start_series, $end_series, $separator);
        
        $database->disconnect();
        
        return $process;
    }
    
    public function executeClearUnusedData ($form_check, $field_check, $form_base, $field_base) {
        include_once 'customized-function/CustomScript_ClearUnusedData.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customScript_clearUnusedData = new CustomScript_ClearUnusedData($dao, $utilities);
        
        $database->connect();
        
        $form_check_converted = $utilities->clearParamString($form_check);
        $field_check_converted = $utilities->clearParamString($field_check);   
        $form_base_converted = $utilities->clearParamString($form_base);
        $field_base_converted = $utilities->clearParamString($field_base);
        
        $process = $customScript_clearUnusedData->customScriptClearUnusedData($form_check_converted, $field_check_converted, $form_base_converted, $field_base_converted);
        
        $database->disconnect();
        
        return $process;
    }
    
    public function executeCreateResponseOnChange ($form_based, $field_based, $form_child, $field_child, $status, $header_line_id, $header_line_id_value, $ref_form_based_id, $ref_form_child_id, $ref_form_based_child_id_value, $pk_form_based_id, $pk_form_child_id) {
        include_once 'customized-function/CustomScript_CreateResponseOnChange.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_createResponseOnChange = new CustomScript_CreateResponseOnChange($dao, $utilities);

        $database->connect();

        $form_based_converted = $utilities->clearParamString($form_based);
        $field_based_converted = $utilities->sanitizedParamString($field_based);
        $form_child_converted = $utilities->clearParamString($form_child);
        $field_child_converted = $utilities->sanitizedParamString($field_child);

        $status_converted = $utilities->clearParamString($status);

        $header_line_id_converted = $utilities->clearParamString($header_line_id);

        $ref_form_based_id_converted = $utilities->clearParamString($ref_form_based_id);
        $ref_form_child_id_converted = $utilities->clearParamString($ref_form_child_id);

        $pk_form_based_id_converted = $utilities->clearParamString($pk_form_based_id);
        $pk_form_child_id_converted = $utilities->clearParamString($pk_form_child_id);

        $customscript_createResponseOnChange->customScriptCreateResponseOnChange($form_based_converted, $field_based_converted, $form_child_converted, $field_child_converted, $status_converted, $header_line_id_converted, $header_line_id_value, $ref_form_based_id_converted, $ref_form_child_id_converted, $ref_form_based_child_id_value, $pk_form_based_id_converted, $pk_form_child_id_converted);

        $database->disconnect();
    }

    public function executeCreateResponseOnChangeComputeTotal ($form_source, $field_source, $form_destination, $field_destination, $field_id_source, $field_id_destination, $field_id_value, $ref_field_first, $ref_field_second, $field_to_compute, $field_to_output, $computation_type, $status){
        include_once 'customized-function/CustomScript_CreateResponseOnChangeComputeTotal.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_createResponseOnChangeComputeTotal = new CustomScript_CreateResponseOnChangeComputeTotal($dao, $utilities);

        $database->connect();

        $form_source_converted = $utilities->clearParamString($form_source);
        $field_source_converted = $utilities->sanitizedParamString($field_source);

        $form_destination_converted = $utilities->clearParamString($form_destination);
        $field_destination_converted = $utilities->sanitizedParamString($field_destination);

        $field_id_source_converted = $utilities->clearParamString($field_id_source);
        $field_id_destination_converted = $utilities->clearParamString($field_id_destination);

        $ref_field_first_converted = $utilities->clearParamString($ref_field_first);
        $ref_field_second_converted = $utilities->clearParamString($ref_field_second);

        $field_to_compute_converted = $utilities->clearParamString($field_to_compute);
        $field_to_output_converted = $utilities->clearParamString($field_to_output);

        $computation_type_converted = $utilities->clearParamString($computation_type);

        $status_converted = $utilities->clearParamString($status);

        $process = $customscript_createResponseOnChangeComputeTotal->customCreateResponseOnChangeComputeTotal($form_source_converted, $field_source_converted, $form_destination_converted, $field_destination_converted, $field_id_source_converted, $field_id_destination_converted, $field_id_value, $ref_field_first_converted, $ref_field_second_converted, $field_to_compute_converted, $field_to_output_converted, $computation_type_converted, $status_converted);

        $database->disconnect();
        return $process;
    }

    public function executeSetDataComputationToEmbedded ($form, $field_value, $field_id, $field_ref, $parent_name, $parent_value, $form_static_data, $field_static_id, $field_static_ref, $field_to_compute, $field_to_compute_opt, $field_compute_output, $field_compute_output_opt, $type_computation) {
        include_once 'customized-function/CustomScript_SetDataComputationToEmbedded.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();
        $customscript_setDataComputationToEmbedded = new CustomScript_SetDataComputationToEmbedded($dao);

        $database->connect();

        $form_converted = $utilities->clearParamString($form);
        $field_id_converted = $utilities->clearParamString($field_id);
        $field_ref_converted = $utilities->clearParamString($field_ref);
        $parent_name_converted = $utilities->clearParamString($parent_name);
        
        $form_static_data_converted = $utilities->clearParamString($form_static_data);
        
        $field_static_id_converted = $utilities->clearParamString($field_static_id);
        $field_static_ref_converted = $utilities->clearParamString($field_static_ref);
        
        $field_to_compute_converted = $utilities->clearParamString($field_to_compute);
        $field_to_compute_opt_converted = $utilities->clearParamString($field_to_compute_opt);
        
        $field_compute_output_converted = $utilities->sanitizedParamString($field_compute_output);
        
        $field_compute_output_opt_converted = $utilities->clearParamString($field_compute_output_opt);
        
        $type_computation_converted = $utilities->clearParamString($type_computation);
        
        $customscript_setDataComputationToEmbedded->customScriptSetDataComputationToEmbedded($form_converted, $field_value, $field_id_converted, $field_ref_converted, $parent_name_converted, $parent_value, $form_static_data_converted, $field_static_id_converted, $field_static_ref_converted, $field_to_compute_converted, $field_to_compute_opt_converted, $field_compute_output_converted, $field_compute_output_opt_converted, $type_computation_converted);
        
        $database->disconnect();
    }

    public function executeCheckFormData ($source_form, $source_id, $destination_form, $destination_id, $tag, $tag_value, $source_tagging, $source_tagging_value) {
        include_once 'customized-function/CustomScript_CheckFormData.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();

        $customscript_checkFormData = new CustomScript_CheckFormData($dao);

        $database->connect();

        $source_form_converted = $utilities->clearParamString($source_form);
        $source_id_converted = $utilities->clearParamString($source_id);

        $destination_form_converted = $utilities->clearParamString($destination_form);
        $destination_id_converted = $utilities->clearParamString($destination_id);
        $tag_converted = $utilities->clearParamString($tag);

        $source_tagging_converted = $utilities->clearParamString($source_tagging);

        $customscript_checkFormData->customScriptCheckFormData($source_form_converted, $source_id_converted, $destination_form_converted, $destination_id_converted, $tag_converted, $tag_value, $source_tagging_converted, $source_tagging_value);										

        $database->disconnect();
    }

    /*==== POSTING MATRIX ====*/
    public function executePostingMatrix ($source_name, $trackno, $status, $node_id) {
        include_once 'customized-function/posting-matrix/CustomScript_PostingMatrix.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();

        $customscript_postingMatrix = new CustomScript_PostingMatrix($dao, $utilities);

        $database->connect();
        
        $source_name_converted = $utilities->clearParamString($source_name);
        $status_converted = $utilities->clearParamString($status);
        $node_id_converted = $utilities->clearParamString($node_id);

        $customscript_postingMatrix->customScriptPostingMatrix($source_name_converted, $trackno, $status_converted, $node_id_converted);
    
        $database->disconnect();
    }

    public function executeJournalPostingMatrix ($source_form, $repository_form, $destination_form, $trackno, $status, $node_id) {
        include_once 'customized-function/posting-matrix/CustomScript_JournalPostingMatrix.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();

        $customscript_journalPostingMatrix = new CustomScript_JournalPostingMatrix($dao, $utilities);

        $database->connect();
        
        $source_form_converted = $utilities->clearParamString($source_form);
        $repository_form_converted = $utilities->clearParamString($repository_form);
        $destination_form_converted = $utilities->clearParamString($destination_form);
        $status_converted = $utilities->clearParamString($status);
        $node_id_converted = $utilities->clearParamString($node_id);

        $customscript_journalPostingMatrix->customScriptJournalPostingMatrix($source_form_converted, $repository_form_converted, $destination_form_converted, $trackno, $status_converted, $node_id_converted);
    
        $database->disconnect(); 
    }
    /*========================*/

    
    /*===== Financial Scheme =====*/
    //Generic Script
    public function executeFixedRateLoan_generic ($form_header, $form_child, $trackno, $status, $node_id) {
        include_once 'customized-function/financial-scheme/CustomScript_FinancialSchemeFunctions.php';
        include_once 'customized-function/financial-scheme/generic/CustomScript_FixedRateLoan.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();

        $fs_functions = new CustomScript_FinancialSchemeFunctions();

        $customscript_fixedRateLoan = new CustomScript_FixedRateLoan($dao, $utilities, $fs_functions);

        $database->connect();

        $form_header_converted = $utilities->clearParamString($form_header);
        $form_child_converted = $utilities->clearParamString($form_child);
        $status_converted = $utilities->clearParamString($status);
        $node_id_converted = $utilities->clearParamString($node_id);

        $customscript_fixedRateLoan->customScriptFixedRateLoan($form_header_converted, $form_child_converted, $trackno, $status_converted, $node_id_converted);                                     

        $database->disconnect();
    }
    //Customized Script
    public function executeFixedRateLoan_cfic ($form_header, $form_child, $trackno, $status, $node_id) {
        include_once 'customized-function/financial-scheme/CustomScript_FinancialSchemeFunctions.php';
        include_once 'customized-function/financial-scheme/cfic/CustomScript_FixedRateLoan.php';
        
        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();

        $fs_functions = new CustomScript_FinancialSchemeFunctions();

        $customscript_fixedRateLoan = new CustomScript_FixedRateLoan($dao, $utilities, $fs_functions);

        $database->connect();

        $form_header_converted = $utilities->clearParamString($form_header);
        $form_child_converted = $utilities->clearParamString($form_child);
        $status_converted = $utilities->clearParamString($status);
        $node_id_converted = $utilities->clearParamString($node_id);

        $customscript_fixedRateLoan->customScriptFixedRateLoan($form_header_converted, $form_child_converted, $trackno, $status_converted, $node_id_converted);										

        $database->disconnect();
    }
    public function executeFixedPrincipalLoan_cfic ($form_header, $form_child, $trackno, $status, $node_id) {
        include_once 'customized-function/financial-scheme/CustomScript_FinancialSchemeFunctions.php';
        include_once 'customized-function/financial-scheme/cfic/CustomScript_FixedPrincipalLoan.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();

        $fs_functions = new CustomScript_FinancialSchemeFunctions();

        $customscript_fixedPrincipalLoan = new CustomScript_FixedPrincipalLoan($dao, $utilities, $fs_functions);

        $database->connect();

        $form_header_converted = $utilities->clearParamString($form_header);
        $form_child_converted = $utilities->clearParamString($form_child);
        $status_converted = $utilities->clearParamString($status);
        $node_id_converted = $utilities->clearParamString($node_id);

        $customscript_fixedPrincipalLoan->customScriptFixedPrincipalLoan($form_header_converted, $form_child_converted, $trackno, $status_converted, $node_id_converted);

        $database->disconnect();
    }

    /*===========================================================*/
    
    public function executeNoSeries ($noseries_type, $line_no) {
        include_once 'customized-function/no-series/CustomScript_NoSeries.php';

        $database = new APIDatabase();
        $auth = new CustomScriptAuth($database);
        $dao = new CustomScriptDAO($database, $auth);
        $utilities = new CustomScriptUtilities();

        $customscript_noSeries = new CustomScript_NoSeries($dao);

        $database->connect();

        $noseries_type_converted = $utilities->clearParamString($noseries_type);
        $line_no_converted = $utilities->clearParamString($line_no);

        $process = $customscript_noSeries->customScriptNoSeries($noseries_type_converted, $line_no_converted);

        $database->disconnect();

        return $process;
    }
}