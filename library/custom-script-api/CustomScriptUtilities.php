<?php

/**
 * Description of CustomScriptUtilities
 *
 * @author Joshua Clifford Reyes
 */

class CustomScriptUtilities {

    public function convertParamString ($param) {
        $get_cleared_param = $this->clearParamString($param);
        $get_converted_param = (explode(',', $get_cleared_param));

        $values = array();
        foreach ($get_converted_param as $value) {
                $values[$value] = $_POST[$value];
        }
        return $values; 
    }

    public function sanitizedParamString ($param) {
        $sanitized = $this->clearParamString($param);
        $converted = $this->seperateParamString($sanitized);
        return $converted;
    }

    public function seperateParamString ($param) {
        $param_converted = (explode(',', $param));
        return $param_converted;
    }
    
    public function clearParamString ($param) {
        $labels = array(
            'Parent:', 
            'Form:',            
            'Source:',
            'Child:',
            'Destination:',
            'Field:',
            'Status:',
            'NodeId:',
            'Id:', 
            'UniqueKey:', 
            'Ref:',
            'Sign:',
            'ProcessTrigger:',
            'Computation:',
            'Filter:',
            'Line:',
            'SeriesCode:',
            'Tag:', 
            'ReturnValue:',
            '[', ']'
        );
        $result = str_replace($labels, '', $param);
        return $result;
    }
    
    public function convertStringToArray ($str = []) {
        $value = [];
        for ($x = 0; $x < count($str); $x += 2) {
            $value[$str[$x]] = $str[$x+1];
        }
        return $value; 
    }

    public function checkValueIsNumeric ($value) {
        $str_val = '';
        if (is_numeric($value)) {
            $str_val .= " ".$value." ";
        } else {
            $str_val .= " '".$value."' ";
        }
        return $str_val;
    }

    public function setFieldToString ($field, $parent_unique, $id) {
        $str = '';
        if (!empty($parent_unique)) {
            $str .= '`'.$parent_unique.'`,';
        }
        if (!empty($id)) {
            $str .= '`'.$id.'`,';
        }
        $field_added = $this->defaultFields($field);
        foreach ($field_added as $value) {
			if (strpos($value, '<{') !== false) {
			
			} else if (!empty($value)) {
                $str .= '`'.$value.'`,';   
            }    
        }
        $field_str = substr($str, 0, strlen($str) - 1);
        return $field_str;
    }

    public function setStatus ($status) {
        if (empty($status)) {
            $status = 'Saved';
            return $status;
        } else {
            return $status;
        }
    }

    private function defaultFields ($array) {
        array_push ($array, 
            'ID',                 'TrackNo',
            'Requestor',          'Status',
            // 'Processor',          'ProcessorType',
            // 'ProcessorLevel',     'LastAction', 
            'DateCreated',        'DateUpdated',
            'CreatedBy',          'UpdatedBy'
            // 'Unread',             'Node_ID',
            // 'Workflow_ID',        'fieldEnabled',
            // 'fieldRequired',      'fieldHiddenValues',
            // 'imported',           'Repeater_Data',
            // 'Editor',             'Viewer',
            // 'middleware_process', 'SaveFormula',
            // 'CancelFormula',      'enable_delegate'
        );
        return $array;    
    }

    //Produce TrackNo
    public function incrementTrackNo ($value, $pointer) {
        $matches = [];
        preg_match_all('/([a-zA-Z\-])|([0-9])/', $value, $matches);
         
        $numbers_str    = implode($matches[2]);
        $prefix_str     = implode($matches[1]);
        
        $trackno_incremented = $prefix_str.str_pad($numbers_str + $pointer, strlen($numbers_str), '0', STR_PAD_LEFT);
        return $trackno_incremented;
    }

    public function errorException ($message, $variable) {
        throw new Exception($message . ' - ' . $variable);
    }

    public function errorReportingLogs ($error_log, $file_name) {
        error_log(print_r($error_log), 3, APPLICATION_PATH.'\\library\\custom-script\\reporting-logs\\report-'.$file_name.'-'.$_SERVER['REQUEST_TIME'].'.log');
    }
}