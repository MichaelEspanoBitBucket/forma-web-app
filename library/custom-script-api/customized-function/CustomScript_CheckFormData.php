<?php

/**
 * Description of CustomScript_CheckFormData
 *
 * @author Joshua Clifford Reyes
 */

class CustomScript_CheckFormData {

    /** @var CustomScriptDAO */
    protected $dao;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
       	$this->dao = $dao;
    }

    public function customScriptCheckFormData($source_form, $source_id, $destination_form, $destination_id, $tag, $tag_value, $source_tagging, $source_tagging_value) {

       /**
        * FORMULA:
        * Check if Data in destination form still have the not like value of @tag value, if false then output tag to source form
        *	@CustomScript("CheckFormData",
        *                     "Source:[] Source Form",				-> $ref_form                                        FORM BASE NAME
        *                     "Source ID",					-> $parent_id                                       PARENT / BASE FIELD
        *                     "Destination:[] Destination Form",                -> $form                                            CHILD BASE NAME
        *                     "Destination ID",					-> $child_id                                        CHILD / BASE FIELD
        *                     "Tag_Field", @tagValue,                           -> $tag & $tag_value                                TO CHECK FIELDS :: Example: Field_Name Not Like "0"
        *                     "TagOutput", @tagOutputValue)			-> $parent_tagging & $parent_tagging_value          OUTPUT OF TAGGING
        */
		
        $source_form_name = $this->dao->getFormTableName($source_form);
        $destination_form_name = $this->dao->getFormTableName($destination_form);
        $database = new Database();
        $get_field_id = $database->query("SELECT {$source_id} FROM {$source_form_name}","array");
        foreach ($get_field_id as $value_get_field_id) {
            $source_id_value = $value_get_field_id[$source_id];
            //Configure here
            $data = $database->query("SELECT {$destination_id} FROM {$destination_form_name} WHERE {$destination_id} = '{$source_id_value}' AND {$tag} NOT LIKE '{$tag_value}'", "row");
            if ($data == 0) {
                $condition = array($source_id => $source_id_value);
                $insert = array($source_tagging => $source_tagging_value);
                $database->update($source_form_name, $insert, $condition);
            }
        }
    }	
}