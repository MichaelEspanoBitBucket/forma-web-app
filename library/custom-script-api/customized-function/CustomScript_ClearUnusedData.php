<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_ClearUnusedData
 *
 * @author Joshua Reyes
 */
class CustomScript_ClearUnusedData {
    //put your code here
    
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }
    
    public function customScriptClearUnusedData ($form_check, $field_check, $form_base, $field_base) {
        
        /**
         *  FORMULA:
         *   @CustomScript("ClearUnusedData",
         *                 "Form:[]",               -> $form_check
         *                 "Field:[]",              -> $field_check
         * 
         *                 "Form:[]",               -> $form_base
         *                 "Field:[]")              -> $field_base
         */
        
        $form_check_name = $this->dao->getFormTableName($form_check);
        $form_base_name = $this->dao->getFormTableName($form_base);
        $process = $this->searchUnusedData($form_check_name, $field_check, $form_base_name, $field_base);
        return $process;
    }
    
    private function searchUnusedData ($form_check, $field_check, $form_base, $field_base) {
        $data = $this->dao->selectData("`{$form_base}`", 
                                       "`{$field_base}` as Unused_Line",
                                       "WHERE `{$field_base}` NOT IN (SELECT `{$field_check}` FROM `{$form_check}`) 
                                        AND (DATE_ADD(`{$form_base}`.`DateCreated`, INTERVAL 2 DAY)) <= NOW() 
                                        GROUP BY `{$form_base}`.`{$field_base}`",
                                       "array");
        if (!empty($data)) {
            $value_str = "";
            foreach ($data as $value) {
                $value_filtered = $this->utilities->checkValueIsNumeric($value['Unused_Line']);
                $value_str .= $value_filtered.",";
            }
            $value_sliced = substr($value_str, 0, strlen($value_str) - 1);
            $this->executeCleanUp($form_base, $field_base, $value_sliced);
        }
        return "Ok";
    }
    
    private function executeCleanUp ($form_base, $field_base, $value) {
        $this->dao->deleteData($form_base, "{$field_base} IN ({$value})");
    }
}