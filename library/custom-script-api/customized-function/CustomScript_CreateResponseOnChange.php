<?php

/**
 * Description of CustomScript_CreateResponseOnChange
 *
 * @author Joshua Clifford Reyes
 */

class CustomScript_CreateResponseOnChange {

    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }

    public function customScriptCreateResponseOnChange ($form_based, $field_based = array(), $form_child, $field_child = array(), $status, $header_line_id, $header_line_id_value, $ref_form_based_id, $ref_form_child_id, $ref_form_based_child_id_value, $pk_form_based_id, $pk_form_child_id) {
			
        /**
        * FORMULA:
        * 	@CustomScript("CreateResponseOnChange",
        *                 "Parent:[]", "Field:[]",				             ->$form_based & $field_based													FORM BASED NAME AND FIELDS
        *                 "Child:[]", "Field:[]",				             ->$form_child & $field_child													FORM CHILD NAME AND FIELDS
        *                 "Status:[]",					                     ->$status 																		STATUS
        *                 "UniqueKey:[Parent_Unique]", @Parent_Value,	     ->$header_line_id & $header_line_id_value 										CONNECTION OF HEADER FORM TO ITS LINE FORM WITH CURRENT VALUE
        *                 "Ref:[]", "Ref:[]", @Ref_Value,                    ->$ref_form_based_id & $ref_form_child_id & $ref_form_based_child_id_value 	REF OF BASED AND CHILD WITH CURRENT VALUE
        *                 "Id:[]", "Id:[]")					                 ->$pk_form_based_id & $pk_form_child_id 										PK OF BASED AND CHILD
        */

        /* @CustomScript( "CreateResponseOnChange" , 
        "20_tbl_productionbomlist", "Item,Description,Unit_of_Measure_Code,Quantity_per,Unit_Cost",
        "20_tbl_firmplannedprodorderline", "Item_No,Description,Unit_of_Measure_Code,Quantity,Unit_Cost",
        "Submitted", 
        "Parent_Unique", @Parent_Unique, 
        "PB_No", "Production_BOM_No", @Get_P_BOM_No, 
        "Item", "Item_No" ) 
        */

        if (!empty($ref_form_based_child_id_value)) {

            $form_based = $this->dao->getFormTableName($form_based);
            $form_child = $this->dao->getFormTableName($form_child);
            
            $chck_parent = $this->checkExistingData($header_line_id, $ref_form_child_id, $form_child, $ref_form_based_child_id_value, $header_line_id_value);
            
            $this->deleteExistingData($chck_parent, $form_child, $header_line_id, $chck_parent[$header_line_id]);
			
            $ref_form_data = $this->dao->selectData($form_based, $pk_form_based_id, "WHERE {$ref_form_based_id} = '{$ref_form_based_child_id_value}'", "array");
            $missing_id_values = $this->checkMissingId($ref_form_data, $form_child, $pk_form_based_id, $pk_form_child_id, $header_line_id, $header_line_id_value);

            $child_status = $this->utilities->setStatus($status);

            $workflowdata = $this->dao->getWorkflowData($form_child, $child_status);
            $insert = $this->dao->getFormaMainFieldsCustom($workflowdata);
            
            $field_required = $this->getRequiredFields($field_based);

            $counter = count($field_based);
            foreach ($missing_id_values as $value_missing_id) {				 
                $this->dao->connect();
                $data = $this->dao->selectData($form_based, $field_required, "WHERE {$pk_form_based_id} = '{$value_missing_id}'", "row");
                //REFERENCE ID
                $insert[$header_line_id] = $header_line_id_value;
                $insert[$ref_form_child_id] = $ref_form_based_child_id_value;

                for ($x = 0; $x <= $counter - 1; $x++) {
                    $insert[$field_child[$x]] = $data[$field_based[$x]]; 
                }
                $this->dao->insertData($form_child, $insert);
            }
        }
    }
    private function checkExistingData ($header_line_id, $ref_form_child_id, $form_child, $ref_form_based_child_id_value, $header_line_id_value) {
        $data = $this->dao->selectData($form_child, 
                                       "{$header_line_id}, {$ref_form_child_id}", 
                                       "WHERE {$ref_form_child_id} 
                                        NOT LIKE '{$ref_form_based_child_id_value}' 
                                        AND {$header_line_id} = '{$header_line_id_value}'", "row");
        return $data; 
    }

    private function deleteExistingData ($chck_parent, $form_child, $header_line_id, $parent_uk) {
        if (!empty($chck_parent)) {
            $this->dao->deleteData($form_child, "{$header_line_id} = '{$parent_uk}'");
        }            
    }

    private function checkMissingId ($ref_form_data, $form_child, $pk_form_based_id, $pk_form_child_id, $header_line_id, $header_line_id_value) {
        $missing_id_values = array();
        foreach ($ref_form_data as $value_ref_form) {
            $missing_id = $value_ref_form[$pk_form_based_id];
            $check_form_data = $this->dao->selectData($form_child, $pk_form_child_id, "WHERE {$pk_form_child_id} = '{$missing_id}' AND {$header_line_id} = '{$header_line_id_value}'", "row");
            if ($check_form_data[$pk_form_child_id] == "") {
                $missing_id_values[] = $missing_id;
            }
        }
        return $missing_id_values;
    }

    private function getRequiredFields ($field_based) {
        $value_str = "";
        foreach ($field_based as $value) {
            $value_str .= "`".$value."`,";
        }
        $value_sliced = substr($value_str, 0, strlen($value_str) - 1);
        return $value_sliced;
    }
}