<?php

/**
 * Description of CustomScript_CreateResponseOnChangeComputeTotal
 *
 * @author Joshua Clifford Reyes
 */

class CustomScript_CreateResponseOnChangeComputeTotal {
	
	/** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }

    public function customCreateResponseOnChangeComputeTotal($form_source, $field_source = array(), $form_destination, $field_destination = array(), $field_id_source, $field_id_destination, $field_id_value, $ref_field_first, $ref_field_second, $field_to_compute, $field_to_output, $computation_type, $status){
   		
        /**
        * FORMULA:
        *	@CustomScript("CreateResponseOnChangeComputeTotal",
        *                     "Source:[]", "Field:[]",								                             -> $form_source & $field_source  
        *                     "Destination:[]","Field:[]",							                             -> $form_destination & $field_destination
        *				  
        *                     "Id:[field_id_source]", "Id:[field_id_destination]", @field_id_value, (Parent ID)  -> $field_id_source & $field_id_destination & $field_id_value
        *				  
        *                     "Ref:[field_first]", 								                                 -> $ref_field_first
        *                     "Ref:[field_second]",								                                 -> $ref_field_second
        *				  
        *                     "Field:[]", "Field:[]",								                             -> $field_to_compute & $field_to_output
        *                     "Computation:[]", 								                                 -> $computation_type
        *
        *                     "Status:[]")									                                     -> $status
        */

        $form_source_name = $this->dao->getFormTableName($form_source);
        $form_destination_name = $this->dao->getFormTableName($form_destination);

        $form_source_status = $this->utilities->setStatus($status);

        $this->dao->deleteData($form_destination_name, "{$field_id_destination} = '{$field_id_value}'");
        
        $field_required = $this->getRequiredFields($field_source, $ref_field_first, $ref_field_second);
        $form_source_data_arr = $this->dao->selectData($form_source_name, $field_required, "WHERE {$field_id_source} = '{$field_id_value}'", "array");

        $counter = count($field_source);
        foreach ($form_source_data_arr as $form_source_data_arr_value) {
            $this->dao->connect();
            $get_workflow_data = $this->dao->getWorkflowData($form_destination_name, $form_source_status);
            $insert = $this->dao->getFormaMainFields($form_source_data_arr_value, $get_workflow_data);

            $data_checker = $this->referenceChecking($form_source_data_arr_value, $ref_field_first, $ref_field_second, $form_destination_name, $field_id_destination, $field_id_value);

            if(empty($data_checker)){
                if (empty($ref_field_second)) {
                    $reference_type = "first";
                    $data_filtered = $this->executeComputeAction($computation_type, $form_source_data_arr_value, $reference_type, $field_to_compute, $form_source_name, $field_id_source, $field_id_value, $ref_field_first, $ref_field_second);
                } else {
                    $reference_type = "second";
                    $data_filtered = $this->executeComputeAction($computation_type, $form_source_data_arr_value, $reference_type, $field_to_compute, $form_source_name, $field_id_source, $field_id_value, $ref_field_first, $ref_field_second);
                }

                for ($x = 0; $x <= $counter - 1; $x++) {
                    if ($form_source_data_arr_value[$field_source[$x]] == "") {
                        if (strpos($field_source[$x],'{') !== false) {
                            $str_value_sanitized = str_replace('{','', $field_source[$x]);
                            $str_value_converted = str_replace('}','', $str_value_sanitized);
                            $insert[$field_destination[$x]] = $str_value_converted;
                        } else {
                            $insert[$field_destination[$x]] = "";
                        } 
                    } else {
                            $insert[$field_destination[$x]] = $form_source_data_arr_value[$field_source[$x]];
                    }
                }

                //Computed Value
                $insert[$field_to_output] = $data_filtered["CustomScriptFieldComputed"];
                $this->dao->insertData($form_destination_name, $insert);
            } 
        }		
    }

    private function getRequiredFields ($field_source, $ref_field_first, $ref_field_second) {
        $value_str = "";
        if (!empty($ref_field_first)) {
            $value_str .= "`".$ref_field_first."`,";
        }
        if (!empty($ref_field_second)) {
            $value_str .= "`".$ref_field_second."`,";
        }

        foreach ($field_source as $value) {
            $value_str .= "`".$value."`,";
        }
        $value_sliced = substr($value_str, 0, strlen($value_str) - 1);
        return $value_sliced;
    }   

    private function referenceChecking ($form_source_data_arr_value, $ref_field_first, $ref_field_second, $form_destination_name, $field_id_destination, $field_id_value) {
        $ref_str = "";
        if (empty($ref_field_second)) {
            $ref_str .= " AND {$ref_field_first} = '{$form_source_data_arr_value[$ref_field_first]}'";
        } else {
            $ref_str .= " AND {$ref_field_first} = '{$form_source_data_arr_value[$ref_field_first]}'";
            $ref_str .= " AND {$ref_field_second} = '{$form_source_data_arr_value[$ref_field_second]}'";
        }
        $data_checker = $this->dao->selectData($form_destination_name, 
                                               $field_id_destination, 
                                              "WHERE {$field_id_destination} = '{$field_id_value}' 
                                               {$ref_str}", "row");
        return $data_checker;
    }

    private function computationAction ($computation_command, $reference_type, $field_to_compute, $form_source_name, $field_id_source, $field_id_value, $ref_field_first, $ref_field_first_value, $ref_field_second, $ref_field_second_value) {
        $ref_str = "";
        if ($reference_type == "first") {
            $ref_str .= " AND {$ref_field_first} = '{$ref_field_first_value}'";
        } else if ($reference_type == "second") {
            $ref_str .= " AND {$ref_field_first} = '{$ref_field_first_value}'";
            $ref_str .= " AND {$ref_field_second} = '{$ref_field_second_value}'";
        }

        $data_filtered = $this->dao->selectData($form_source_name, "{$computation_command}($field_to_compute) as CustomScriptFieldComputed", "WHERE {$field_id_source} = '{$field_id_value}' {$ref_str}", "row");
        return $data_filtered;
    }

    private function executeComputeAction ($computation_type, $form_source_data_arr_value, $reference_type, $field_to_compute, $form_source_name, $field_id_source, $field_id_value, $ref_field_first, $ref_field_second) {
        if ($computation_type == "Total"){
            $computation_command = "SUM";
            $data_filtered = $this->computationAction($computation_command, $reference_type, $field_to_compute, $form_source_name, $field_id_source, $field_id_value, $ref_field_first, $form_source_data_arr_value[$ref_field_first], $ref_field_second, $form_source_data_arr_value[$ref_field_second]);
            return $data_filtered;
        } else if ($computation_type ==  "Average") {
            $computation_command = "AVG";
            $data_filtered = $this->computationAction($computation_command, $reference_type, $field_to_compute, $form_source_name, $field_id_source, $field_id_value, $ref_field_first, $form_source_data_arr_value[$ref_field_first], $ref_field_second, $form_source_data_arr_value[$ref_field_second]);
            return $data_filtered;
        } else if ($computation_type == "Count") {
            $computation_command = "COUNT";
            $data_filtered = $this->computationAction($computation_command, $reference_type, $field_to_compute, $form_source_name, $field_id_source, $field_id_value, $ref_field_first, $form_source_data_arr_value[$ref_field_first], $ref_field_second, $form_source_data_arr_value[$ref_field_second]);
            return $data_filtered;
        }
    }
}