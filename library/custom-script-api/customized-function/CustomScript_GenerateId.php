<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_GenerateId
 *
 * @author Joshua Reyes
 */
class CustomScript_GenerateId {
    //put your code here
    
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }
    
    public function customScriptGenerateId ($form_used, $series_code, $start_series, $end_series, $separator) {
        $access = md5(uniqid(). 'unbreakable' . round(microtime(true)));

        $current_datetime = $this->dao->getCurrentDate();
        // CustomScript Generated ID need tb_custom_generate_id table
        $tb_storage = 'tb_custom_generate_id';
        $form_used_name = $this->dao->getFormTableName($form_used);
        
        $series_code_repository = $this->dao->selectDataFieldNameWhereCondition($tb_storage, 'Series_Code, Current_Access, Date_Used, Last_Used_Series, End_Series', "Series_Code = '{$series_code}'", 'row');
        if (empty($series_code_repository['Series_Code'])) {
            $this->newSeriesCode($tb_storage, $form_used_name, $series_code, $start_series, $end_series, $current_datetime, $access);
            $new_series = $this->incrementSeries($start_series, 'false', $separator);
            return $new_series;
        } else {
            if ($series_code_repository['Date_Used'] != $current_datetime && $series_code_repository['Current_Access'] != $access) {
                $new_series = $this->incrementSeries($series_code_repository['Last_Used_Series'], 'true', $separator);
                //Check if Series is equal to End Series
                $process = $this->checkEndSeries($tb_storage, $series_code, $current_datetime, $access, $series_code_repository['End_Series'], $new_series);
                return $process;
            }
        }
    }
    
    private function newSeriesCode ($tb_storage, $form_used_name, $series_code, $start_series, $end_series, $current_datetime, $access) {
        $data = array(
            'Form_Used'          => $form_used_name,
            'Series_Code'        => $series_code,                
            'Start_Series'       => $start_series,
            'End_Series'         => $end_series,
            'Last_Used_Series'   => $start_series,
            'Date_Created'       => $current_datetime,
            'Date_Used'          => $current_datetime,
            'Current_Access'     => $access 
        );
        $this->dao->insertDataGeneric($tb_storage, $data);
    }
    
    private function updateSeriesCode ($tb_storage, $series_code, $current_datetime, $access, $new_series) {
        $data = array(
            'Last_Used_Series'  => $new_series,
            'Date_Used'         => $current_datetime,
            'Current_Access'    => $access
        );
        $condition = array('Series_Code' => $series_code);
        $this->dao->updateData($tb_storage, $data, $condition);
    }
    
    private function incrementSeries ($last_used_series, $increment_status, $separator) {
        $matches = [];
        preg_match_all('/([a-zA-Z]-?)|([0-9])/', $last_used_series, $matches);
        
        $numbers_str = implode($matches[2]);
        $series_str = implode($matches[1]);
        if ($increment_status == 'true') {
           //[1] saved to database, [0] for viewing
           $last_no_used_new[0] = $series_str.$separator.str_pad($numbers_str + 1, strlen($numbers_str), '0', STR_PAD_LEFT); 
           $last_no_used_new[1] = $series_str.str_pad($numbers_str + 1, strlen($numbers_str), '0', STR_PAD_LEFT);
           return $last_no_used_new; 
        } else if ($increment_status == 'false') {
           $last_no_used_new = $series_str.$separator.$numbers_str;
           return $last_no_used_new;
        }  
    }
    
    private function checkEndSeries ($tb_storage, $series_code, $current_datetime, $access, $check_last_used_series, $new_series) {
        if ($check_last_used_series == $new_series[1]) {
            $this->utilities->errorReportingLogs('Series is depleted, please contact your administrator.', 'CustomGenerateId');              
        } else {
            $this->updateSeriesCode($tb_storage, $series_code, $current_datetime, $access, $new_series[1]);
            return $new_series[0];
        }
    }
}