<?php

/**
 * Description of CustomScript_NoSeries
 *
 * @author Raycille Dimla & Joshua Clifford A. Reyes
 * 
 *
 */

class CustomScript_NoSeries {

    /** @var CustomScriptDAO */
    protected $dao;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
       	$this->dao = $dao;
    }
    
    public function customScriptNoSeries ($id, $noseries_type, $line_no) {
		
        /**
        * FORMULA:
        *	@CustomScript("NoSeries",
        *                     @id,              -> $id
        *                     "SeriesCode:[]",  -> $noseries_type
        *                     "Id:[]")		-> $line_no
        *
        */
        
        $form = "20_tbl_noseries";
        $form_line = "20_tbl_numberseriesline";
        
        $parent_fields = $this->getParentFields($form, $noseries_type);
        $line_fields = $this->getLineFields($form_line, $noseries_type, $line_no);
        
        //Manual Active
        if (($parent_fields['manual_no'] == "Yes") && ($parent_fields['default_no'] != " Yes")) {
           
        //Default Active
        } else if (($parent_fields['manual_no'] != "Yes") && ($parent_fields['default_no'] == " Yes")) {
            if ($line_fields['open_status'] == "Yes") {
                $return_no_series = $this->checkLastNoUsed($line_fields['last_no_used'], $line_fields['starting_no'], $line_fields['increment']);
            }
        }

        return $last_no_used_new;
    }
    
    private function checkLastNoUsed ($last_no_used, $starting_no, $increment) {
        $last_no_used_new = "";
        if (strlen($last_no_used) == 0){
            $last_no_used_new = $starting_no;
        } else {
            $matches = [];
            preg_match_all('/(\d)|(\w)/', $last_no_used, $matches);
            $numbers = implode($matches[1]);
            $series_code = implode($matches[2]);
            $last_no_used_new = $series_code.str_pad ($numbers + $increment, strlen($numbers), '0', STR_PAD_LEFT);
        }
        
        return $last_no_used_new;
    }
    
    private function defaultNoAction ($default_no_status) {
       if ($default_no_status == "Yes") {
           
       } 
    }
    
    private function manualNoAction ($manual_no_status) {
        if ($manual_no_status == "Yes") {
            
        }
    }
    
    private function getParentFields ($form, $noseries_type) {
        $data_parent = $this->dao->selectDataWhereCondition($form, "Code = '{$noseries_type}'", "row");
        $fields = array();
        $fields['default_no'] = $data_parent['DefaultNos_'];
        $fields['manual_no']  = $data_parent['ManualNos_'];
        
        return $fields;
    }
    
    private function getLineFields ($form_line, $noseries_type, $line_no) {
        $data = $this->dao->selectDataWhereCondition($form_line, "Series_Code = '{$noseries_type}' AND Line_No = '{$line_no}'", "row");
        $fields = array();
        $fields['increment']    = $data['Increment_by_No'];
        $fields['starting_no']  = $data['Starting_No'];
        $fields['last_no_used'] = $data['Last_No_Used'];
        $fields['open_status']  = $data['Open'];
        
        return $fields;
    }
    
    private function updateNoSeriesParent ($form, $noseries_type, $last_no_used_new) {
        $insert_parent = array('Date_Last_Used' => $last_no_used_new);
        $condition_parent = array('Code' => $noseries_type);
        $this->dao->updateData($form, $insert_parent, $condition_parent);
    }
    
    private function updateNoSeriesLine ($form_line, $noseries_type, $line_no, $last_no_used_new) {
        $insert_line = array('Last_No_Used' => $last_no_used_new);
        $condition_line = array('Series_Code' => $noseries_type, 'Line_No' => $line_no);
        $this->dao->updateData($form_line, $insert_line, $condition_line);
    }
}