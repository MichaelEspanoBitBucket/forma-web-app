<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_GLPostingMatrix
 *
 * @author Joshua Reyes
 */
class CustomScript_PostingMatrix {
    //put your code here
    
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }
    
    public function customScriptPostingMatrix ($source_form, $trackno, $status, $node_id) {
        $POSTING_MATRIX = "Posting Matrix";
        $POSTING_MATRIX_LINE = "Posting Matrix Line";
        $JOURNAL_ENTRY  = "Add Journal Entry";
        
        //FIELDS
        $field_params = array(
            'SOURCE_NAME'               => $source_form,
            'TRACKNO'                   => $trackno,
            'STATUS'                    => $status,
            'NODE_ID'                   => $node_id
        );
        
        //TABLE NAME
        $form_params = array(
            "SOURCE_TBL_NAME"               => $this->dao->getFormTableName($field_params['SOURCE_NAME']),
            "DESTINATION_TBL_NAME"          => $this->dao->getFormTableName($JOURNAL_ENTRY),
            "POSTING_MATRIX_TBL_NAME"       => $this->dao->getFormTableName($POSTING_MATRIX),
            "POSTING_MATRIX_TBL_NAME_LINE"  => $this->dao->getFormTableName($POSTING_MATRIX_LINE)
        );
		
		//DEFAULT FIELDS FORMALISTICS
		$forma_default_field = $this->getDefaultFields($field_params, $form_params);
		$this->executePosting($field_params, $form_params, $forma_default_field);
	}
    
    private function POSTVAL ($value) {
        return $_POST[$value];
    }
    
    private function getDefaultFields ($field_params, $form_params) {
        $track_no                   = $this->getTrackNo($form_params['SOURCE_TBL_NAME'], $field_params['TRACKNO']);
        $destination_status         = $this->utilities->setStatus($field_params['STATUS']);

        $field_source_keys_str      = $this->utilities->setFieldToString($arr = [], "", "");
        $form_source_data           = $this->dao->selectDataFieldNameWhereCondition($form_params['SOURCE_TBL_NAME'], "{$field_source_keys_str}", "TrackNo = '{$track_no}'", "row");
        
        $workflow_data              = $this->getWorkflow($form_params['DESTINATION_TBL_NAME'], $destination_status, $field_params['NODE_ID']);
        $forma_default_field        = $this->dao->getFormaMainFields($form_source_data, $workflow_data);
        return $forma_default_field;
    }
    
    private function executePosting ($field_params, $form_params, $forma_default_field) {
        $current_trackno = $this->dao->getCurrentTrackNoWithTableName($form_params['DESTINATION_TBL_NAME']);        
		$posting_matrix  = $this->dao->selectData(
            $form_params['POSTING_MATRIX_TBL_NAME'], 
            "*", 
            "WHERE Form_Name = '{$field_params['SOURCE_NAME']}'", 
            "array");

        $counter = 1;
        foreach ($posting_matrix as $value) {
            $reference_status_field_name = $this->POSTVAL($value['Reference_Status_Field_Name']);
            $base_amount = $this->POSTVAL($value['SourceFieldName']);
            if ($reference_status_field_name == $value['Permitted_Status_Value']) {

                $forma_default_field['TrackNo'] = $this->incrementTrackNo($current_trackno, $counter);
                $collected_data = $this->filterTypeHeader($value, $field_params);
                $insert_arr = array_merge($forma_default_field, $collected_data);
                
                $this->dao->insertDataGeneric($form_params['DESTINATION_TBL_NAME'], $insert_arr);

                $this->postingMatrixChild($value, $field_params, $form_params, $forma_default_field);

                $counter += 1;  
            }
        }
    }
	
	private function postingMatrixChild ($value, $field_params, $form_params, $forma_default_field) {
        $reference_status_field_name = $this->POSTVAL($value['Reference_Status_Field_Name']);
        $base_amount = $this->POSTVAL($value['SourceFieldName']);

        $current_trackno = $this->dao->getCurrentTrackNoWithTableName($form_params['DESTINATION_TBL_NAME']);
		$posting_matrix_line  = $this->dao->selectData(
            $form_params['POSTING_MATRIX_TBL_NAME_LINE'], 
			"*", 
            "WHERE uid = '{$value['uid']}'", 
            "array");

		$counter = 1;
		foreach ($posting_matrix_line as $value_line) {
            if ($reference_status_field_name == $value_line['Permitted_Status_Value']) {
                
                $forma_default_field['TrackNo'] = $this->incrementTrackNo($current_trackno, $counter);
                $collected_data = $this->filterTypeChild($value_line, $field_params, $base_amount);
				$insert_arr = array_merge($forma_default_field, $collected_data);

                $this->dao->insertDataGeneric($form_params['DESTINATION_TBL_NAME'], $insert_arr);

                $counter += 1;  
            }
		}
	}

    //HEADER DATA
    private function filterTypeHeader ($value, $field_params) {
        $collected_data = [];

        //FRONTEND USER INPUT
        $collected_data['particulars']              =   $this->POSTVAL($value['Description']);
        $collected_data['Affected_Cost_Center']     =   $this->POSTVAL($value['Cost_Center']);
        $collected_data['BookOfAccounts']           =   $value['BooksOfAccount'];
        $collected_data['AccountTitle']             =   $value['GL_Account_No'];
        $collected_data['GL_Account_Name']          =   $value['GL_Account_Name'];
        $collected_data['GL_Account_Type']          =   $value['GL_Account_Type'];
        $collected_data['TransactType']             =   $value['Debit_Credit'];

        $base_amount = $this->POSTVAL($value['SourceFieldName']);
        if ($value['Debit_Credit'] == "Debit") {
            $collected_data['DebitAmount'] = $base_amount;
        } else if ($value['Debit_Credit'] == "Credit") {
            $collected_data['CreditAmount'] = $base_amount;
        }

        $collected_data['uniquekey']                =   $value['uid']; 
        $collected_data['Form_Name']                =   $value['Form_Name'];
        $collected_data['Source_TrackNo']           =   $field_params['TRACKNO'];
        $collected_data['SourceDateCreated']        =   $this->dao->getCurrentDate();

        return $collected_data;
    }
	
    //CHILD DATA
	private function filterTypeChild ($value_line, $field_params, $base_amount) {
		$collected_data = [];

        //FRONTEND USER INPUT
        $collected_data['particulars']              =   $this->POSTVAL($value_line['Description']);
        $collected_data['Affected_Cost_Center']     =   $this->POSTVAL($value_line['Affected_Cost_Center']);
        $collected_data['BookOfAccounts']           =   $value_line['BooksOfAccount'];
        $collected_data['AccountTitle']             =   $value_line['GL_Accounts'];
        $collected_data['GL_Account_Name']          =   $value_line['GL_Account_Name'];
        $collected_data['GL_Account_Type']          =   $value_line['GL_Account_Type'];
        $collected_data['TransactType']             =   $value_line['Type'];

        if ($value_line['Type'] == "Debit") {
            if ($value_line['Amount_Type_Value'] == "Percent (%) of Base Amount") {
                $collected_data['DebitAmount']  =  ($base_amount * ($value_line['Percent_Amount'] / 100));
            } else if ($value_line['Amount_Type_Value'] == "Amount Value") {
                $collected_data['DebitAmount']  =  $base_amount; 
            }
        } else if ($value_line['Type'] == "Credit") {
            if ($value_line['Amount_Type_Value'] == "Percent (%) of Base Amount") {
                $collected_data['CreditAmount']  =  ($base_amount * ($value_line['Percent_Amount'] / 100));
            } else if ($value_line['Amount_Type_Value'] == "Amount Value") {
                $collected_data['CreditAmount'] =  $base_amount; 
            }
        }
        //BACKEND ADDITION
        $collected_data['uniquekey']                =   $value_line['uid']; 
        $collected_data['Form_Name']                =   $value_line['Form_Name'];
        $collected_data['Source_TrackNo']           =   $field_params['TRACKNO'];
        $collected_data['SourceDateCreated']        =   $this->dao->getCurrentDate();
		
        return $collected_data;
	}

    //TRACKNO
    private function incrementTrackNo ($value, $pointer) {
        $matches = [];
        preg_match_all('/([a-zA-Z])|([0-9])/', $value, $matches);
         
        $numbers_str    = implode($matches[2]);
        $prefix_str     = implode($matches[1]);
        
        $trackno_incremented = $prefix_str.str_pad($numbers_str + $pointer, strlen($numbers_str), '0', STR_PAD_LEFT);
        return $trackno_incremented;
    }
    
    private function getTrackNo ($form_source_name, $trackno) {
        if (empty($trackno)) {
            $trackno_value = $this->dao->getCurrentTrackNoWithTableName($form_source_name);
            return $trackno_value;
        } else {
            return $trackno;
        }
    }
      
    private function getWorkflow ($form, $status, $node_id) {
        if (empty($node_id)) {
            $workflow = $this->dao->getWorkflowData($form, $status);
            return $workflow;
        } else {
            $workflow = $this->dao->getWorkflowDataWithNodeId($form, $status, $node_id);
            return $workflow;
        }
    }
}
