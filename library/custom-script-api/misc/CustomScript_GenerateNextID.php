<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_GenerateNextID
 *
 * @author Michael Espano
 */
class CustomScript_GenerateNextID{
	/** @var CustomScriptDAO */
    protected $dao;
    
    /** @param CustomScriptDAO $dao */	
    public function __construct($database, $auth) {
    	$this->database = $database;
    	$this->auth = $auth;
    }
    public function customGenerateNextID($form_name, $prefix="", $separator=""){
        if(!$separator){
            $separator = "";
        }
        if(!$prefix){
            $prefix = "";
        }
    	$generated_id = "";
    	$auth_data = $this->auth->getAuthentication();
    	// echo 'SELECT `form_table_name`, `reference_prefix` FROM tb_workspace WHERE `company_id` = '.$auth_data['company_id'].' AND `form_name` = "'.$form_name.'"', 'row';
    	$table_data = $this->database->query('SELECT `form_table_name`, `reference_prefix` FROM tb_workspace WHERE `company_id` = '.$auth_data['company_id'].' AND `form_name` = "'.$form_name.'"', 'row');
    	$get = $this->database->query("SELECT (`AUTO_INCREMENT`) as next_id FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA =  '". DB_NAME ."' AND TABLE_NAME = '". $table_data['form_table_name'] ."' LIMIT 1", 'row');
    	// $generated_id = $get['next_id'];
    	//$table_data['reference_prefix']
        if($prefix == "" && $table_data['reference_prefix']){
            $prefix = $table_data['reference_prefix'];
        }
    	$generated_id = $prefix.$separator.str_pad($get['next_id'], 4, "0", STR_PAD_LEFT);//json_encode($this->auth->getAuthentication(), JSON_PRETTY_PRINT);
    	return $generated_id;
    }
}

?>