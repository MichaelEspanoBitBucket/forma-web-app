<?php

/**
 * Description of CustomScript_GetUserDelegate
 *
 * @author Krist Iann Tablan 201605281820H
 *
 */
class CustomScript_GetUserDelegate {
    //put your code here
    
    /** @var CustomScriptDAO */
    protected $dao;
    protected $delegate_name;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
        $this->dao = $dao;
    }
    
    public function customScriptGetUserDelegate($delegate_name) {
        
        /* FORMULA: @CustomScript("GetUserDelegate", "display_name") */

        $this->delegate_name = $delegate_name;
        return $this->getUserDelegate($delegate_name);
    }

    private function getUserDelegate($delegate_name) {

        $user_display_name_collector = array();

        if ($delegate_name === "") {
            return "Invalid Parameters!";
        } else {
            $delegate_id = $this->dao->selectData("`tbuser`", "`id`", "WHERE `display_name` = '{$delegate_name}'","row");
            $user_id = $this->dao->selectData("`tbdelegate`", "`user_id`", "WHERE `user_delegate_id` = '{$delegate_id['id']}'","array");

            // if(count($user_id) > 1){
            //     $user_display_name = $this->dao->selectData("`tbuser`", "`display_name`", "WHERE `id` = '{$user_id['user_id']}'","row");

            //     for ($i=0; $i <= count($user_id)-1; $i++) { 
            //         $user_display_name = $this->dao->selectData("`tbuser`", "`display_name`", "WHERE `id` = '{$user_id[$i]['user_id']}'","row");
            //         array_push($user_display_name_collector, $user_display_name['display_name']);
            //     }

            //     $collected_user_name = implode("|", $user_display_name_collector);

            // } else {
                $user_display_name = $this->dao->selectData("`tbuser`", "`display_name`", "WHERE `id` = '{$user_id[0]['user_id']}'","row");
                $collected_user_name = $user_display_name['display_name'];
            // }

            return $collected_user_name;
        }
    }
}