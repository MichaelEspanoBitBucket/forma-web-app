<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_RecordDelete
 *
 * @author Krist Iann Tablan
 */
class CustomScript_GetUserID {
    //put your code here
    
    /** @var CustomScriptDAO */
    protected $dao;
    
    /** @param CustomScriptDAO $dao */	
    public function __construct($dao) {
        $this->dao = $dao;
    }
    
    public function customScript_GetUserID ($display_name) {
        
        /**
         * FORMULA:
         *  @CustomScript("GetUserID"
         *                "display_name")  -> $display_name (can be separated by "|")
         */
        
        $user_id = "";
        $display_name = implode("','", explode("|", $display_name));
        $id = $this->dao->selectData("`tbuser`", "`id`", "WHERE `display_name` IN ('{$display_name}')", "array");
        
        foreach ($id as $key => $value) {
            $user_id .= $value['id']. ',';
        }
        
        $user_id = substr($user_id, 0, strlen($user_id)-1);

        return $user_id;
    }
}
