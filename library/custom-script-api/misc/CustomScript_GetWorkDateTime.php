<?php

/**
 * Description of CustomScript_GetWorkDateTime
 *
 * @author Krist Iann Tablan 201607061116H
 *
 */
class CustomScript_GetWorkDateTime {
    //put your code here
    
    /** @var CustomScriptDAO */
    protected $dao;
    protected $start_date;
    protected $days;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
        $this->dao = $dao;
    }
    
    public function customscript_getWorkDateTime($type, $start_date, $days) {
        
        /* FORMULA: @CustomScript('GetWorkDateTime', 
                                  ['date','time'], 
                                  @start_date, 
                                  @value) 
        */

        if($type === 'date'){
            $compute_time = AdjustDate('hour', $start_date, 9);
            $diff_time = DiffDate('minute', $compute_time, substr($start_date, 0, 10).' 17:00');
            $getWorkingDays = $this->getWorkingDays($start_date, $days);

            if($diff_time >= 480){ // 480mins = 8hrs (8am-4pm)
                $datetime_finished = $this->getWorkingDays(AdjustDate('day', $getWorkingDays, 1), 1);
                $datetime_finished = substr($datetime_finished, 0, 10).' 17:00';
            } else {
                $datetime_finished = $this->getWorkingDays(AdjustDate('day', $getWorkingDays, 1), 1);
            }
        } else if($type === 'time'){
            $with_added_hrs = AdjustDate('hour', $start_date, $days);
            $get_remainder_mins = DiffDate('minute', $with_added_hrs, substr($start_date, 0, 10).' 17:00');

            if($get_remainder_mins > 0){
                $getWorkingDays = CustomScript('GetWorkDateTime', 'date', $start_date, 1);
                $datetime_finished = AdjustDate('minute', substr($getWorkingDays, 0, 10).' 08:00', $get_remainder_mins);
            } else {
                $datetime_finished = AdjustDate('hour', substr($start_date, 0, 10).' 08:00', $days);
            }
        }

        return $datetime_finished;
    }

    private function getWorkingDays($start_date, $days, $resultDates = array()) {
        if ($days === 0) {
            return $resultDates[count($resultDates) - 1];
        }

        $date = date("w", strtotime($start_date));
        $holiday = LookupWhereArray('Holiday Maintenance', 'Holiday_Date', 'TrackNo', '!=', '');
        if (($date != 0 && $date != 6) && !in_array(StrGet($start_date, 1, 10), $holiday)) {
            $resultDates[] = $start_date;
            $days -= 1;
        }

        $start_date = new DateTime("$start_date + 1 day");
        $start_date = $start_date->format('Y-m-d H:i');

        return $this->getWorkingDays($start_date, $days, $resultDates);
    }
}