<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_GroupUserCount
 *
 * @author Joshua Reyes
 */
class CustomScript_GroupUser {
    //put your code here
    
    /** @var CustomScriptDAO */
    protected $dao;

    protected $group_name;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao) {
        $this->dao = $dao;
    }
    
    public function customScriptGroupUser($type_use, $group_name) {
        
        /**
        *  FORMULA:
        *   @CustomScript("GroupUser",
        *                 "user" or "count",
        *                 @Group_Name)
        */

        $this->group_name = $group_name;
        return $this->conditionType($type_use);
    }

    private function conditionType($type_use) {
        $return_value = '';
        switch ($type_use) {
            case "count" :
                $return_value = $this->actionCount();
                break;
            case "user" :
                $return_value = $this->actionUser();
                break;
            default :
        }
        
        return $return_value;
    }

    private function actionCount() {
        //$formHead = "SELECT id, group_name FROM `tbform_groups` WHERE group_name = '{$group_name}'";
        $record_tbform_groups = $this->dao->selectData("`tbform_groups`", "id", "WHERE group_name = '{$this->group_name}'", "row");
        //$formChild = "SELECT id FROM `tbform_groups_users` where group_id = '{}' and is_active = '1'";
        // $record_count_tbform_groups_users = $this->dao->selectData("`tbform_groups_users`", "id", "WHERE group_id = '{$record_tbform_groups['id']}' AND is_active = '1'", "numrows");
        $record_count_tbform_groups_users = $this->dao->selectData("`tbform_groups_users` as TBGU, `tbuser` as TBUSER", "TBUSER.`display_name` as USER_GROUP_NAME", "WHERE TBGU.`user_id` = TBUSER.`id` AND TBGU.`group_id` = '{$record_tbform_groups['id']}' AND TBGU.`is_active` = '1'", "numrows");
        
        return $record_count_tbform_groups_users;
    }

    private function actionUser() {

        $record_tbform_groups = $this->dao->selectData("`tbform_groups`", "id", "WHERE group_name = '{$this->group_name}'", "row");
        $record_count_tbform_groups_users = $this->dao->selectData("`tbform_groups_users` as TBGU, `tbuser` as TBUSER", "TBUSER.`display_name` as USER_GROUP_NAME", "WHERE TBGU.`user_id` = TBUSER.`id` AND TBGU.`group_id` = '{$record_tbform_groups['id']}' AND TBGU.`is_active` = '1'", "array");
        
        $group_users_name_str = '';
        foreach ($record_count_tbform_groups_users as $value) {
            $group_users_name_str .= $value['USER_GROUP_NAME'] . "|";
        }
        $group_users_name_str_filtered = substr($group_users_name_str, 0, strlen($group_users_name_str) - 1);
        
        return $group_users_name_str_filtered;
    }
}