<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_LookUpWhereStringReturn
 *
 * @author Joshua Reyes
 */
class CustomScript_LookUpWhereStringReturn {
    //put your code here
    
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
       	$this->dao = $dao;
        $this->utilities = $utilities;
    }
    
    public function customScriptLookUpWhereStringReturn ($form, $return_field, $separator, $id_field, $id_value) {
        
        /**
        *  FORMULA:
        *   @CustomScript("LookUpWhereStringReturn",
        *                 "Form:[]",                    -> $form
        *                 "Field:[]",                   -> $return_field
        *                 "|",                          -> $separator
        *                 "Id:[]", @Id)                 -> $id                  THIS CAN BE MULTIPLE VALUE EX: "A1|A2|A3"
        */
        
        if ($id_value != '') {
            $form_table = $this->dao->getFormTableName($form);
            $condition = $this->conditionFilter($id_value, $separator);

            $data = $this->dao->selectData($form_table, $return_field, "WHERE {$id_field} IN ({$condition})", 'array');
            
            $value_return = '';
            foreach ($data as $value) {
                $value_return .= "{$value[$return_field]}{$separator}";
            }
            $value_return_string = substr($value_return, 0, strlen($value_return) - 1);
            return $value_return_string;
        }
    }
    
    private function conditionFilter ($value, $separator) {
        $value_arr = $this->filterPipe($value, $separator);
        $condition_str = '';
        foreach ($value_arr as $condition_value) {
            $condition_str .= "'{$condition_value}', ";
        }
        $conditions_string = substr($condition_str, 0, strlen($condition_str) - 2);
        return $conditions_string;
    } 
    
    private function filterPipe($value, $separator) {
        $data = (explode($separator, $value));
        return $data;
    }
}