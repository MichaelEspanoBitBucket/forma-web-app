<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_PickListDataSource
 *
 * @author Joshua Reyes
 */
class CustomScript_PickListDataSource {
    //put your code here
    
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }
    
    public function customScriptPickListDataSource($source_form, $source_field, $destination_form, $destination_field, $destination_status, $destination_node_id, $source_field_id, $picklist_value, $unique_key_params, $data_send_params, $condition_params) {
        
        /**
        * FORMULA:
        *   @CustomScript("PickListDataSource",
        *                     "Source:[]",                                                          -> $source_form
        *                     "Field:[]",                                                           -> $source_field
        *                     "Destination:[]",                                                     -> $destination_form
        *                     "Field:[]",                                                           -> $destination_field
        *                     "Status:[]",                                                          -> $destination_status
        *                     "NodeId:[]",                                                          -> $destination_node_id
        *                     "Id:[]",                                                              -> $source_field_id
        *                     @PickListReturnValue,                                                 -> $picklist_value
        *                     ["Parent_Unique_Source", @Parent_Unique_Source],                      -> $unique_key_params          IF DATASOURCE IS CHILD ELSE BLANK WITH "", NOTE THIS VALUE IS CONNECTED TO $data_send_params
        *                     ["Field_DataSend",@Value_DataSend],                                   -> $data_send_params           DATA SEND THE $unique_key_params or $parent_unique set to destination, to DESTINATION FORM to view in embed
        *                     (@Status == "Value" ... ))                                            -> $condition_params
        */

        if ($condition_params) {
            if (!empty($picklist_value) || $picklist_value !== '') {

                //Initialization of needed objects
                $source_form_table = $this->dao->getFormTableName($source_form);
                $destination_form_table = $this->dao->getFormTableName($destination_form);
                
                $picklist_value_array = $this->filterPipe($picklist_value);
                
                //Clear All Existing Data Related to DataSend
                $this->clearExistingDataSource($destination_form_table, $data_send_params);

                //Run the logic
                $this->executePickListDataSource($source_form_table, $destination_form_table, $source_field, $destination_field, $destination_status, $destination_node_id, $source_field_id, $picklist_value_array, $unique_key_params, $data_send_params);
                
                return 'Done!';

            } else {

                return 'Picklist Value is Empty!';
            
            }
        } else {
            return 'Formula Condition is False! Abort DataSource';
        }
    }

    private function filterPipe($picklist_value) {
        $data = (explode('|', $picklist_value));
        return $data;
    }

    private function clearExistingDataSource($dest_form, $data_send_params) {
        $condition_str = '';
        foreach($data_send_params as $key => $value) {
            $condition_str .= "`{$key}` = '{$value}' AND ";
        }
        $conditions_string = substr($condition_str, 0, strlen($condition_str) - 4);
        $this->dao->deleteData($dest_form, $conditions_string);
    }
    
    private function executePickListDataSource($src_frm, $dest_frm, $src_field, $dest_field, $dest_status, $dest_node_id, $src_field_id, $picklist_value, $unique_key_params, $data_send_params) {
 
        # Note:
        # src = source
        # dest = destination

        $unique_key_str_val_concat = '';
        foreach ($unique_key_params as $unique_key_params_key => $unique_key_params_val) {
            if (!empty($unique_key_params_val) || $unique_key_params_val !== '') {
                $unique_key_str_val_concat .= "`{$unique_key_params_key}` = '{$unique_key_params_val}' AND";
            }
        }
        
        $picklist_str_val_concat = '';
        foreach ($picklist_value as $picklist_str_val) {
            $picklist_str_val_concat .= "'{$picklist_str_val}', ";
        }
        $conditions_str = substr($picklist_str_val_concat, 0, strlen($picklist_str_val_concat) - 2);
        
        $status             = $this->utilities->setStatus($dest_status);
        $src_field_keys     = $this->utilities->setFieldToString($src_field, '', '');
        $form_source_data   = $this->dao->selectDataFieldNameWhereCondition($src_frm, $src_field_keys, "{$unique_key_str_val_concat} {$src_field_id} IN ({$conditions_str})", 'array');

        $workflow_data      = $this->getWorkflow($dest_frm, $status, $dest_node_id);
        $insert_arr         = $this->dao->getFormaMainFieldsCustom($workflow_data);
        
        foreach ($data_send_params as $key_data_send => $value_data_send) {
            $insert_arr[$key_data_send] = $value_data_send;
        }

        $current_trackno = $this->dao->getCurrentTrackNoWithTableName($dest_frm);

        $data_collector = array();
        $counter        = count($src_field)-1;
        $pointer        = 0;
        foreach ($form_source_data as $form_source_value) {
            $this->dao->connect();
            for ($x = 0; $x <= $counter; $x++) {
                $src_field_val = $this->stringifyField($src_field[$x], $form_source_value);
                $insert_arr[$dest_field[$x]] = $src_field_val; 
            }
            // $insert_arr['TrackNo']      = $this->utilities->incrementTrackNo($current_trackno, $pointer+1);
            // $data_collector[$pointer]   = $insert_arr;
            $this->dao->insertData($dest_frm, $insert_arr);
            // $pointer++;
        }

        //Save Record via InsertBulkQueries
        // $this->dao->insertDataBulkLoadData($dest_frm, $data_collector);

        // unset($data_collector);
        // unset($insert_arr);
    }
    
    private function getWorkflow($form, $status, $node_id) {
        if (empty($node_id)) {
            $workflow = $this->dao->getWorkflowData($form, $status);
            return $workflow;
        } else {
            $workflow = $this->dao->getWorkflowDataWithNodeId($form, $status, $node_id);
            return $workflow;
        }
    }
	
	private function stringifyField($field, $source_value) {
		if (strpos($field, '<{') !== false) {
			$delimiter = array(
				'<{',
				'}>'
			);
			$field = str_replace($delimiter, '', $field);
			return $field;
		} else {
			return $source_value[$field];
		}
	}
}