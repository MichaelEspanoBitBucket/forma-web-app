<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_RecordCountWithFilter
 *
 * @author Joshua Reyes
 */
class CustomScript_RecordCountWithFilter {
       
    /** @var CustomScriptDAO */
    protected $dao;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
       	$this->dao = $dao;
    }
    
    public function customScriptRecordCountWithFilter ($form, $filter_name, $filter_value) {
        
        /**
         * FORMULA:
         *  @CustomScript("RecordCountWithFilter",
         *                "Form:[]",                    -> $form
         *                "Filter:[]",                  -> $filter_name
         *                @filter_value)                -> $filter_value  
         */
        
        if (!empty($filter_value)) {
           $form_table_name = $this->dao->getFormTableName($form);
           $record_count = $this->dao->selectDataFieldNameWhereCondition($form_table_name, $filter_name,"{$filter_name} = '{$filter_value}'", 'numrows');
           return $record_count;
        } 
    } 
}
