<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_RecordDelete
 *
 * @author Joshua Reyes
 */
class CustomScript_RecordDelete {
    //put your code here
    
    /** @var CustomScriptDAO */
    protected $dao;
    
    /** @param CustomScriptDAO $dao */	
    public function __construct($dao) {
        $this->dao = $dao;
    }
    
    public function customScriptRecordDelete ($form, $field_id, $id) {
        
        /**
         * FORMULA:
         *  @CustomScript("RecordDelete",
         *                "Form:[]",                -> $form
         *                "Id:[]",                  -> $field_id
         *                @id_value or "id value")  -> $id
         */
        
        $form_table = $this->dao->getFormTableName($form);
        $this->dao->deleteData($form_table, "{$field_id} = '{$id}'");
    }
}
