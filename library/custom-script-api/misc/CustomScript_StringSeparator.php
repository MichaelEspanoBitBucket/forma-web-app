<?php

/**
 * Description of CustomScript_StringSeparator
 *
 * @author Joshua Clifford Reyes
 */

class CustomScript_StringSeparator {

    /** @var CustomScriptDAO */
    protected $dao;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
       	$this->dao = $dao;
    }

    public function customScriptStringSeparator ($form, $field, $filter, $status) {
		
        /**
        * FORMULA:
        *	@CustomScript("CustomStringSeparator",
        *                 "Form:[]",				-> $form_name
        *                 "Field:[]",			    -> $field
        *		          "Filter:[]",			    -> $filter
        *		          "Status:[]")			    -> $status
        */
		
        $trackno = $this->dao->getCurrentTrackNo($form);
        $form_name = $this->dao->getFormTableName($form);
        $form_data = $this->dao->getFormData($form_name, $trackno);
        
        $form_status = $this->getStatus($status);
        
        $workflow_data = $this->dao->getWorkflowData($form_name, $form_status);

        //Forma main fields
        $forma_main_fields_data = $this->dao->getFormaMainFields($form_data, $workflow_data);
        $forma_optional_fields_data = $this->dao->getFormaOptionalFields($form_data);
        $insert = array_merge($forma_main_fields_data, $forma_optional_fields_data);

        foreach ($field as $key_value => $row_value) {
            $string_value_exploded = (explode("{$filter}", $row_value)); 
            foreach ($string_value_exploded as $key_separated => $row_separated) {
                if ($key_separated == 0){
                    $this->executeUpdate($key_value, $row_separated, $status, $trackno, $form_name);
                } else {
                    $this->executeInsert($field, $form_data, $key_value, $row_separated, $status, $form_name, $insert);
                }
            }
        }
    }
    
    private function executeUpdate ($key_value, $row_seperated, $status, $trackno, $form_name) {
        $data_updated = [];
        $data_updated[$key_value] = $row_seperated;
        $data_updated['Status'] = $status;
        $condition = array('TrackNo' => $trackno);
        $this->dao->connect();
        $this->dao->updateData($form_name, $data_updated, $condition);
    }
    
    private function executeInsert ($field, $form_data, $key_value, $row_separated, $status, $form_name, $insert) {
        $field_keys = array_keys($field);
        foreach ($field_keys as $key_for_update) {
            $insert[$key_for_update] = $form_data[$key_for_update];
        }
        $insert[$key_value] = $row_separated;
        $insert['Status'] = $status;
        $this->dao->connect();
        $this->dao->insertData($form_name, $insert); 
    }
    
    private function getStatus ($status) {
        if (empty($status)) {
            $status = "Saved";
            return $status;
        } else {
            return $status;
        }
    }
}