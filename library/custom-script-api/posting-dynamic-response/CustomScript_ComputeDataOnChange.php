<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_ComputeDataOnChange
 *
 * @author Joshua Reyes
 */
class CustomScript_ComputeDataOnChange {
    //put your code here
    
    /** @var CustomScriptDAO & CustomScriptUtilities */
    protected $dao;
    protected $utilities;

    /** @param CustomScriptDAO $dao & CustomScriptUtilities $utilities */
    public function __construct($dao, $utilities) {
        $this->dao = $dao;
        $this->utilities = $utilities;
    }
    
    public function customScriptComputeDataOnChange ($form, $params, $return_value, $line_name, $id_name, $id_value, $parent_name, $parent_value, $computation) {
        
        /**
         * FORMULA:
         *  @CustomScript("ComputeDataOnChange",
         *                "Form:[]",                            -> $form
         *                ["KeyFields", @KeyValue, ...],        -> $params
         *                "Line:[]",                            -> $line_name                           FOR COUNTER
         *                "Id:[]", @Id,                         -> $id_name & $id_value
         *                "UniqueKey:[]", @Parent_Id,           -> $parent_name & $parent_value
         *                "Computation:[MDAS]")                 -> $computation
         */
        
        $form_name = $this->dao->getFormTableName($form);
        
        foreach ($params as $key => $value) {
            $insert = [];
            $condition_string = $this->getConditionString($id_name, $id_value, $parent_name, $parent_value);
            $key_value = $this->dao->selectDataFieldNameWhereCondition($form_name, "{$key}, {$line_name}", $condition_string, "array");
            foreach ($key_value as $each_value) {
                $computed_value = $this->checkComputation($computation, $each_value[$key], $value);
                $insert[$return_value] = $computed_value;
                $condition = $this->getCondition($line_name, $each_value[$line_name], $id_name, $id_value, $parent_name, $parent_value);
                $this->dao->updateData($form_name, $insert, $condition);
            }
        }
        return "Ok!";
    }

    private function getConditionString ($id_name, $id_value, $parent_name, $parent_value) {
        $condition_string = "";
        if (!empty($id_name) && !empty($id_value)) {
            $id_value_checked = $this->utilities->checkValueIsNumeric($id_value);
            $condition_string .= " `".$id_name."` =".$id_value_checked;
        }
        if (!empty($parent_name) && !empty($parent_value)) {
            $parent_value_checked = $this->utilities->checkValueIsNumeric($parent_value);
            $condition_string .= " AND `".$parent_name."` =".$parent_value_checked;
        }
        return $condition_string;
    }

    private function getCondition ($line_name, $line_value, $id_name, $id_value, $parent_name, $parent_value) {
        $condition = [];
        if (!empty($id_name) && !empty($id_value)) {
            $condition[$id_name] = $id_value;
        }
        if (!empty($parent_name) && !empty($parent_value)) {
            $condition[$parent_name] = $parent_value;
        }
        $condition[$line_name] = $line_value;
        return $condition; 
    }
    
    private function checkComputation ($computation, $compute_value, $base_value) {
        $result = 0;
        if ($computation == "Add") {
            $result = ($compute_value + $base_value);
        } else if ($computation == "Subtract") {
            $result = ($compute_value - $base_value);
        } else if ($computation == "Divide") {
            $result = ($compute_value / $base_value);
        } else if ($computation == "Multiply") {
            $result = ($compute_value * $base_value);
        }
        return $result;
    }
}
