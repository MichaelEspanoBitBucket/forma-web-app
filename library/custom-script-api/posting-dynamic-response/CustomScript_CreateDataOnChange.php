<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CustomScript_CreateDataOnChange
 *
 * @author Joshua Reyes
 */
class CustomScript_CreateDataOnChange {
    //put your code here
    
    /** @var CustomScriptDAO */
    protected $dao;

    /** @param CustomScriptDAO $dao */
    public function __construct($dao) {
       $this->dao = $dao;
    }
    
    public function customScriptCreateDataOnChange ($form, $params, $status) {
        
        /**
         * FORMULA:
         *   @CustomScript("CreateDataOnChange", 
         *                 "Form:[]",               -> $form
         *                 ["Key", @Value],         -> $params
         *                 "Status:[]")             -> $status
         */
        

        $form_name = $this->dao->getFormTableName($form);
        $workflowdata = $this->dao->getWorkflowData($form_name, $status);
        $insert = $this->dao->getFormaMainFieldsCustom($workflowdata);
        foreach ($params as $key => $value) {
            $insert[$key] = $value;
        }
        $this->dao->insertData($form_name, $insert);
    }
}
