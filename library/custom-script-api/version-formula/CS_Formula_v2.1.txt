CS FORMULA SYNTAX Ver 2.1:


============ POSTING RESPONSES ============ 

CREATE RESPONSE WITH LINE

* @CustomScript("CreateResponseWithLine",
		"Source:[]",
		"Field:[]",
		"Destination:[]",
		"Field:[]",

		"Status:[]",
		"NodeId:[]",

		
		"Source:[]",
		"Field:[]",
		"Destination:[]",
		"Field:[]",

		"Status:[]",
		"NodeId:[]",
		
		"Parent_Unique")

UPDATE RESPONSE WITH LINE

* @CustomScript("UpdateResponseWithLine",
		 "Source:[]",
		 "Field:[]",
	   	 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
		 "Id:[Parent_ID,Child_ID]",

		 "Source:[]",
		 "Field:[]",
		 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
		 "Id:[SourceLine_ID,DestinationLine_ID]",

		 "Parent_Unique")


CREATE RESPONSE

* @CustomScript("CreateResponse",
		 "Source:[]",
		 "Destination:[]",
		 "Status:[]",
		 "NodeId:[]",
		 ["Destination_Field", @Source_Value, ... ])

UPDATE RESPONSE

* @CustomScript("UpdateResponse",
		 "Source:[]",
		 "Destination:[]",
		 "Status:[]",
		 "NodeId:[]",
		 "Id:[]", @Id_Value,
		 ["Destination_Field", @Source_Value, ... ])


============ DYNAMIC RESPONSES ============ 

CREATE DATA

* @CustomScript("CreateDataOnChange",
		 "Destination:[]",
		 ["Destination_Field", @Source_Value, ... ],
		 "Status:[]")


UPDATE DATA

* @CustomScript("UpdateDataOnChange",
		 "Destination:[]",
		 ["Destination_Field", @Source_Value, ... ],
		 "Id:[]", @Id_Value,
		 "UniqueKey:[]", @Parent_Value)

GET DATA

* @CustomScript("GetDataOnChange",
		 "Form:[]",
		 "Field:[]",
		 "Id:[]", @Id_Value,
		 "UniqueKey:[]", @Parent_Value)


============ MISC / COMMON GENERALIZED FORMULA ============ 

LOOKUP

* @CustomScript("LookUp",
		 "Form:[]",
		 "Field:[]",
		 "Id:[]", @Id_Value,
		 "ReturnValue:[True_Str,False_Str]" or @Id_Value)

RECORD COUNT

* @CustomScript("RecordCountWithFilter",
		 "Form:[]",
		 "Filter:[]", @Filter_Value)


NUMBER LENGTH AUTO ARRANGE

* @CustomScript("NumberLengthAutoCorrect",
		 @Field_To_Check,
		 @Field_To_Base)

DATA DELETE

* @CustomScript("RecordDelete",
		 "Form:[]",
		 "Id:[]", @Id_Value)




		