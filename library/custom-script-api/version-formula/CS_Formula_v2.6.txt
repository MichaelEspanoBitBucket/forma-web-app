CS FORMULA SYNTAX Ver 2.6

==== UPDATES ====
New Functions:
Added "CreateUpdateResponseWithLine" [This function check if there is data to update if non then do create]
Added "LookUpWhere" [This function supported in PostSubmit Workflow and Formbuilder On Refresh]
Added "LookUpWhereStringReturn" [This function return multiple value with define separator]
Added "PickListDataSource" [This function return value that is selected in picklist]
Added "GroupUserCount" [This function return the User Count in Group -> Numrows]

Fixed:
CustomScript Migrated to Background Process

Dev Fixed:
Remove "$pointer" in posting-response
Added "function ifCondition" in Utilities
Added Redis for Caching getRedisConnection(); affected query is insert and update
Added clearRedisCached() for clearing cached in redis after query insert or update


============ POSTING RESPONSES ============ 

CREATE RESPONSE WITH LINE

* @CustomScript("CreateResponseWithLine",
		"Source:[]",
		"Field:[]",
		"Destination:[]",
		"Field:[]",

		"Status:[]",
		"NodeId:[]",
		@TrackNo,
		"ProcessTrigger:[Enable or Disable]",

		
		"Source:[]",
		"Field:[]",
		"Destination:[]",
		"Field:[]",

		"Status:[]",
		"NodeId:[]",
		"ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		
		"Parent_Unique") <-- must same column name Source and Destination Form

UPDATE RESPONSE WITH LINE

* @CustomScript("UpdateResponseWithLine",
		 "Source:[]",
		 "Field:[]",
	   	 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
	 	 @TrackNo,
		 "ProcessTrigger:[Enable or Disable]",
		 "Id:[Parent_ID,Child_ID]",

		 "Source:[]",
		 "Field:[]",
		 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
		 "ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		 "Id:[SourceLine_ID,DestinationLine_ID]",

		 "Parent_Unique") <-- must same column name Source and Destination Form

CREATE RESPONSE

* @CustomScript("CreateResponse",
		 "Source:[]",
		 "Destination:[]",
		 "Status:[]",
		 "NodeId:[]",
		 @TrackNo,
		 "ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		 ["Destination_Field", @Source_Value, ... ])

UPDATE RESPONSE

* @CustomScript("UpdateResponse",
		 "Source:[]",
		 "Destination:[]",
		 "Status:[]",
		 "NodeId:[]",
		 @TrackNo,
		 "ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		 "Id:[]", @Id_Value,
		 "UniqueKey:[]", @Parent_Unique,
		 ["Destination_Field", @Source_Value, ... ])

CREATE & UPDATE RESPONSE WITH LINE

* @CustomScript("CreateUpdateResponseWithLine",
		 "Source:[]",
		 "Field:[]",
	   	 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
	 	 @TrackNo,
		 "ProcessTrigger:[Enable or Disable]",
		 "Id:[Parent_ID,Child_ID]",

		 "Source:[]",
		 "Field:[]",
		 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
		 "ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		 "Id:[SourceLine_ID,DestinationLine_ID]",

		 "Parent_Unique") <-- must same column name Source and Destination Form


============ DYNAMIC RESPONSES ============ 

CREATE DATA

* @CustomScript("CreateDataOnChange",
		 "Destination:[]",
		 ["Destination_Field", @Source_Value, ... ],
		 "Status:[]")


UPDATE DATA

* @CustomScript("UpdateDataOnChange",
		 "Destination:[]",
		 ["Destination_Field", @Source_Value, ... ],
		 "Id:[]", @Id_Value,
		 "UniqueKey:[]", @Parent_Value)

COMPUTE DATA

* @CustomScript("ComputeDataOnChange",
                "Form:[]",
		["KeyFields", @KeyValue, ...],
		"Line:[]",
		"Id:[]", @Id, 
		"UniqueKey:[]", @Parent_Id,       
		"Computation:[MDAS]")     

============ MISC / COMMON GENERALIZED FORMULA ============ 


GROUP USER COUNT

* @CustomScript("GroupUserCount",
               @Group_Name)				<- Group Name	

LOOKUP

* @CustomScript("LookUp",
		 "Form:[]",
		 "Field:[]",
		 "Id:[]", @Id_Value,
		 "ReturnValue:[True_Str,False_Str]" or @Id_Value)

LOOKUPWHERE

* @CustomScript("LookUpWhere",			
                "Form:[]",				<- Form Name to Lookup
		"Field:[]",				<- Field to Return
 		["Id_Field", @Id_Field, ... ])		<- Fields to Filter

LOOKUPWHERE STRING RETURN

* @CustomScript("LookUpWhereStringReturn",
               "Form:[]",                    		<- Form Name to Lookup
               "Field:[]",                   		<- Field to Return
               "|",                          		<- Separator to Filter and Return
               "Id:[]", @Id)                 		<- Referrence IDs

PICKLIST DATASOURCE

* @CustomScript("PickListDataSource",
               "Source:[]",                          	<- Source Form
               "Field:[]",                           	<- Source Fields
               "Destination:[]",                     	<- Destination Form
               "Field:[]",                           	<- Destination Fields
               "Status:[]",                          	<- Destination Status
               "NodeId:[]",                          	<- Destination NodeId
               "Id:[]", @PickListReturnValue,           <- Referrence ID
               ["Field_DataSend",@Value_DataSend],   	<- Unique Key to Header DataSend in Embed inorder to View
               (@Status == "Value" ... ))           	<- Condition Value

RECORD COUNT

* @CustomScript("RecordCountWithFilter",
		 "Form:[]",
		 "Filter:[]", @Filter_Value)

DATA DELETE

* @CustomScript("RecordDelete",
		 "Form:[]",
		 "Id:[]", @Id_Value)

CLEAR UNUSED DATA

* @CustomScript("ClearUnusedData",
              "Form:[]",               -> FORM TO BASE
              "Field:[]",              -> FIELD UNIQUE FORM BASE
          
              "Form:[]",               -> FORM TO CLEAR
              "Field:[]")              -> FIELD UNIQUE TO FORM BASE

AUTO GENERATED ID

* @CustomScript("GenerateId", 
	      "Form:[Firm Planned Prod Orders]", 	<- Form to use Auto Generate ID
              "FPPO", 					<- ID and PREFIX
              "FPPO000001", 				<- Starting ID PREFIX+NUMBER
              "FPPO999999" , 				<- Ending ID PREFIX+NUMBER
              "-")					<- OPTIONAL: SEPARATOR PREFIX-NUMBER