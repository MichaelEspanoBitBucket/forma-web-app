CS FORMULA SYNTAX Ver 3.0

==== API UPDATES ====

Added Custom Script:
 	GroupUser -> get Users or User's Count in specified group
	GetUserID ->  get the id of specified user (use to change Processor using UpdateDateOnChange)
	GetUserDelegate -> get the user who appoints the delegate
	GetWorkDateTime-> get Work days and Work Time (defaults: Monday to Friday, 8am - 5pm)

Dev Fixed:
	Remove LOAD DATA IN FILE function in PicklistDataSource
	GroupUserCount revised to GroupUser now has params use to get users or user count in specified group


============ POSTING RESPONSES ============ 

CREATE RESPONSE WITH LINE

* @CustomScript("CreateResponseWithLine",
		"Source:[]",
		"Field:[]",
		"Destination:[]",
		"Field:[]",

		"Status:[]",
		"NodeId:[]",
		@TrackNo,
		"ProcessTrigger:[Enable or Disable]",

		
		"Source:[]",
		"Field:[]",
		"Destination:[]",
		"Field:[]",

		"Status:[]",
		"NodeId:[]",
		"ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		
		"Parent_Unique") <-- must same column name Source and Destination Form

UPDATE RESPONSE WITH LINE

* @CustomScript("UpdateResponseWithLine",
		 "Source:[]",
		 "Field:[]",
	   	 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
	 	 @TrackNo,
		 "ProcessTrigger:[Enable or Disable]",
		 "Id:[Parent_ID,Child_ID]",

		 "Source:[]",
		 "Field:[]",
		 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
		 "ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		 "Id:[SourceLine_ID,DestinationLine_ID]",

		 "Parent_Unique") <-- must same column name Source and Destination Form

CREATE RESPONSE

* @CustomScript("CreateResponse",
		 "Source:[]",
		 "Destination:[]",
		 "Status:[]",
		 "NodeId:[]",
		 @TrackNo,
		 "ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		 ["Destination_Field", @Source_Value, ... ])

UPDATE RESPONSE

* @CustomScript("UpdateResponse",
		 "Source:[]",
		 "Destination:[]",
		 "Status:[]",
		 "NodeId:[]",
		 @TrackNo,
		 "ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		 "Id:[]", @Id_Value,
		 "UniqueKey:[]", @Parent_Unique,
		 ["Destination_Field", @Source_Value, ... ])

UPDATE RESPONSE WHERE

* @CustomScript("UpdateResponseWhere",
		 "Source:[]",
		 "Destination:[]",
		 "Status:[]",
		 "NodeId:[]",
		 @TrackNo,
		 "ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		 ["Destination_Field", @Source_Value, ... ],
		 ["Field", "operator", @value])

CREATE & UPDATE RESPONSE WITH LINE

* @CustomScript("CreateUpdateResponseWithLine",
		 "Source:[]",
		 "Field:[]",
	   	 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
	 	 @TrackNo,
		 "ProcessTrigger:[Enable or Disable]",
		 "Id:[Parent_ID,Child_ID]",

		 "Source:[]",
		 "Field:[]",
		 "Destination:[]",
		 "Field:[]",

		 "Status:[]",
		 "NodeId:[]",
		 "ProcessTrigger:[Enable or Disable]", <-- Enable to process the trigger of destination form, Disable to stop the process of trigger in destination form
		 "Id:[SourceLine_ID,DestinationLine_ID]",

		 "Parent_Unique", <-- must same column name Source and Destination Form
		 "1 or 2 or custom_identifier", <-- Number of Condition if 1 = Default filter, 2 = 1 Filter without Parent_Unique
		 ["Source_Field", "Destination_Field", ... ]) <-- custom_identifier = [] additional filter for child
		 

============ DYNAMIC RESPONSES ============ 

CREATE DATA

* @CustomScript("CreateDataOnChange",
		 "Destination:[]",
		 ["Destination_Field", @Source_Value, ... ],
		 "Status:[]")


UPDATE DATA

* @CustomScript("UpdateDataOnChange",
		 "Destination:[]",
		 ["Destination_Field", @Source_Value, ... ],
		 "Id:[]", @Id_Value,
		 "UniqueKey:[]", @Parent_Value)


UPDATE DATA WHERE

* @CustomScript("UpdateDataOnChangeWhere",
         "Form:[]",               
         ["Key",@Values],               
         ["Field", "operator", @value])


COMPUTE DATA

* @CustomScript("ComputeDataOnChange",
                "Form:[]",
		["KeyFields", @KeyValue, ...],
		"Line:[]",
		"Id:[]", @Id, 
		"UniqueKey:[]", @Parent_Id,       
		"Computation:[MDAS]")     

============ MISC / COMMON GENERALIZED FORMULA ============ 


GROUP USER COUNT

* @CustomScript("GroupUserCount",
               @Group_Name)				<- Group Name	

LOOKUP

* @CustomScript("LookUp",
		 "Form:[]",
		 "Field:[]",
		 "Id:[]", @Id_Value,
		 "ReturnValue:[True_Str,False_Str]" or @Id_Value)

LOOKUPWHERE

* @CustomScript("LookUpWhere",			
                "Form:[]",				<- Form Name to Lookup
		"Field:[]",				<- Field to Return
 		["Id_Field", @Id_Field, ... ])		<- Fields to Filter

LOOKUPWHERE STRING RETURN

* @CustomScript("LookUpWhereStringReturn",
               "Form:[]",                    		<- Form Name to Lookup
               "Field:[]",                   		<- Field to Return
               "|",                          		<- Separator to Filter and Return
               "Id:[]", @Id)                 		<- Referrence IDs

PICKLIST DATASOURCE

* @CustomScript("PickListDataSource",
	      "Source:[]",                          				<- Source Form EX: PURCHASE ORDER LINE
              "Field:[]",                           				<- Source Fields EX: PURCHASE_ORDER_LINE_NAME... etc.
              "Destination:[]",                     				<- Destination Form EX: EXTERNAL RECEIVING LINE
              "Field:[]",                           				<- Destination Fields EX: EXTERNAL_RECEIVING_LINE_NAME... etc.
              "Status:[]",                          				<- Destination Status EX: Open (Process Node in EXTERNAL RECEIVING LINE)
              "NodeId:[]",                          				<- Destination NodeId EX: node_1 (Process Node in EXTERNAL RECEIVING LINE)
              "Id:[]", @PickList_MultipleSelection,            			<- Referrence Field_Name (ID) TO SOURCE FORM AND VALUE SELECTED FROM PICKLIST EX: [PO-LINE-001|PO-LINE-002|PO-LINE-003]
              ["Source_Field_Name", @CurrentForm_Source_Selected], 		<- Unique ID that link to Source Form Note this is important! EX: PO-HEAD-0001 in Field (PO_No [Purchase Order Line Form])
              ["Destination_Field_Name", @Value_to_DataSend],   		<- Unique Key FROM Header to DataSend in Embed inorder to View the record inserted in EMBED
              (@Status == "Value" ... ))           				<- Condition Value for When the DataSource will be execute

RECORD COUNT

* @CustomScript("RecordCountWithFilter",
	      "Form:[]",				<- FORM NAME
	      "Filter:[]", @Filter_Value)		<- SHORTLIST RECORD

DATA DELETE

* @CustomScript("RecordDelete",
		"Form:[]",				<- FORM NAME
		"Id:[]", @Id_Value)			<- ID OF RECORD

CLEAR UNUSED DATA

* @CustomScript("ClearUnusedData",
		"Form:[]",               		<- FORM TO BASE
		"Field:[]",              		<- FIELD UNIQUE FORM BASE     
		"Form:[]",               		<- FORM TO CLEAR
		"Field:[]")              		<- FIELD UNIQUE TO FORM BASE

AUTO GENERATED ID

* @CustomScript("GenerateId", 							
		"Form:[Firm Planned Prod Orders]", 	<- FORM TO USE Auto Generate ID      
		"FPPO", 				<- ID and PREFIX
		"FPPO000001", 				<- Starting ID PREFIX+NUMBER
		"FPPO999999" , 				<- Ending ID PREFIX+NUMBER
		"-")					<- OPTIONAL: SEPARATOR PREFIX-NUMBER

GIVEN IF CUSTOM SCRIPT FUNCTION

* @CustomScript("GivenIfCustomScriptFunction",
		(Condition),				<- CONDITION
		["Function Name", ...],			<- IF TRUE
		["Function Name", ...])			<- ELSE FALSE

GET WORK DATE & TIME

* @CustomScript('GetWorkDateTime', 
                ['date','time'],     <- 'date' or 'time'
                @start_date, 		 <- Date to Adjust
                @value) 			 <- Adjust value (int)

GET USER DELEGATE

* @CustomScript("GetUserDelegate",   
				"display_name")      <- Delegate Name

GET USER ID

* @CustomScript("GetUserID"
         		"display_name")  -> $display_name (can be separated by "|")

