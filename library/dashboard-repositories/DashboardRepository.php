<?php

class DashboardRepository
{
	
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}


	public function getUserPreferences(){
		$this->conn->query("START TRANSACTION");


		// var_dump($this->user["id"]);
	
		$objStmt = $this->conn->prepare
			("SELECT layout, apps , ganttConfig FROM tb_dashboard_user_preferences WHERE user_id = ?");
		$objStmt->bind_param("i",$this->user["id"]);
		//echo $this->user["id"];
		$objStmt->execute();
		$Objects = $objStmt->get_result()->fetch_assoc();
		
		$userPref = [];
		
		
		$userPref['Layout'] = json_decode($Objects['layout']);
		$userPref['Apps'] = json_decode($Objects['apps']);
		$userPref['GanttConfig'] = json_decode($Objects['ganttConfig']);

		$this->conn->query("COMMIT");
		return $userPref;
	}

	public function getUserPinnedObjects(){
		$this->conn->query("START TRANSACTION");
	
		$objStmt = $this->conn->prepare
			("SELECT Content FROM tbdashboard WHERE UserID = ?");
		$objStmt->bind_param("i",$this->user["id"]);
		$objStmt->execute();
		$Objects = $objStmt->get_result()->fetch_assoc();
		
		$userPref = [];


	
		//$userPref['Content'] = json_decode($Objects['Content']);
		

		$this->conn->query("COMMIT");
		return json_decode($Objects['Content']);
	}

	public function saveUserApps($apps){


		$stringProperties = json_encode($apps);
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("UPDATE tb_dashboard_user_preferences SET apps = ? WHERE user_id = ?");
		$urStmnt->bind_param("si",
				$stringProperties,
				$this->user["id"]);
		$urStmnt->execute();

		$this->conn->query("COMMIT");
		//return $userPref;
	}

	public function saveUserGanttConfig($ganttConfig){


		$stringProperties = json_encode($ganttConfig);
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("UPDATE tb_dashboard_user_preferences SET ganttConfig = ? WHERE user_id = ?");
		$urStmnt->bind_param("si",
				$stringProperties,
				$this->user["id"]);
		$urStmnt->execute();

		$this->conn->query("COMMIT");
		//return $userPref;
	}


	public function createUserPreference(){
		
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("INSERT Into tb_dashboard_user_preferences (`user_id`,`layout`,`apps`) VALUES (?,'[]','[]')");
		$urStmnt->bind_param("i",
				$this->user["id"]);
		$urStmnt->execute();
		
		$this->conn->query("COMMIT");
		//return $userPref;
	}

	
	public function saveUserLayout($layout){
		$pref = $this->getUserPreferences();
		//var_dump($pref);
		if($pref["Layout"] == null){
			$this->createUserPreference();
		}
		$this->conn->query("START TRANSACTION");
		$stringProperties = json_encode($layout);
		
		$urStmnt = $this->conn->prepare
			("UPDATE tb_dashboard_user_preferences SET layout = ? WHERE user_id = ?");
		$urStmnt->bind_param("si",
				$stringProperties,
				$this->user["id"]);
		$urStmnt->execute();
		$this->conn->query("COMMIT");
		//return $userPref;
	}

	public function getSurveyResult($id){
		$this->conn->query("START TRANSACTION");
		$query = "SELECT o.id as `ID`, o.response as `Option`, count(r.id) as `Count` 
					From 
						tb_message_board_surveys as s
						left join 
						tb_message_board_survey_selectable_responses as o
						on o.survey_id = s.id 
					left join 
					tb_message_board_survey_responses as r
					on r.response_id = o.id 
					where s.id = ?
					group by o.response Order By ID";

		$objStmt = $this->conn->prepare
			($query);
		$objStmt->bind_param("i",$id);
		$objStmt->execute();
		$Objects = $objStmt->get_result()->fetch_all(MYSQLI_BOTH);

	
		//$userPref['Content'] = json_decode($Objects['Content']);
		

		$this->conn->query("COMMIT");
		return $Objects;
	}	

	public function appendPinnedView($view){
		if($this->getUserPreferences()->Layout == null){

			$this->createUserPreference();
		}
		$this->conn->query("START TRANSACTION");
		$doExist = false;
		$objStmt = $this->conn->prepare
			("SELECT layout FROM tb_dashboard_user_preferences WHERE user_id = ?");
		$objStmt->bind_param("i",$this->user["id"]);
		$objStmt->execute();
		$Objects = $objStmt->get_result()->fetch_assoc();
		
		$userPref = [];


	
		$Layout = json_decode($Objects['layout']);
		if($Layout == null){
			$Layout = [];
		}
		$i = 0;
		foreach ($Layout as $widget) {

			if($widget->type == "widgetView" && $widget->object_id == $view[0]['object_id']){
				$Layout[$i] = $view[0];
				$doExist = true;
			}
			$i++;
		}
		if(!$doExist){
			array_push($Layout, $view[0]);
		}
		
		$this->saveUserLayout($Layout);
	


		$this->conn->query("COMMIT");
		return $Layout;
	}

	public function getViewDetailsByFormID($form_id){
		$this->conn->query("START TRANSACTION");
	
		$objStmt = $this->conn->prepare
			("SELECT  form_json, 
				deletion_type, 
				active_fields, 
				form_alias, 
				form_name, 
				active_fields, 
				form_table_name 
			FROM tb_workspace 
			WHERE id = ? AND is_delete = 0");

		$objStmt->bind_param("i",$form_id);
		$objStmt->execute();
		$Objects = $objStmt->get_result()->fetch_assoc();

	
		$objStmt = $this->conn->prepare
			("SELECT * FROM tb_generate WHERE form_id=?");
		$objStmt->bind_param("i",$form_id);
		$objStmt->execute();
		$getGeneratePrint = $objStmt->get_result()->fetch_assoc();
		$print_form_id = $getGeneratePrint['id'];
		$Objects["print_form_id"] = $print_form_id;
		
		$form_json = json_decode($Objects['form_json']);
		// var_dump($form_json);
		$Objects['custom_view'] = [];
		unset($Objects['form_json']);
		$mod = unserialize(ENABLE_COMPANY_MOD);
		$Objects['show_starred'] = $mod['others']['starred'];
		//$Objects["form_json"] = $form_json;

		$fields_headerObjType_allowReorder = $form_json->form_json->allowReorder;
		$fields_headerObjType = $form_json->form_json->headerInfoType;
		$headerInfo = $form_json->form_json->collected_data_header_info->listedchoice;
		$Objects["workspace_buttonName"] =  $form_json->form_json->workspace_buttonName;
		//This Code was copied and modified on formalistic code to separate view and model... Copied From Layout/application_table_view.php
		$objStmt = $this->conn->prepare
			("SELECT * FROM tbcustomview WHERE user_id = ? AND form_id = ? ");

		$objStmt->bind_param("ii",$this->user["id"],$form_id);
		$objStmt->execute();
		$getCustomView = $objStmt->get_result()->fetch_assoc();

		 if($fields_headerObjType_allowReorder=="0"){
			  $getCustomView = array();
		 }
        if (count($getCustomView) > 0) {	
            $customView = getValidHeaderFields($headerInfo,$getCustomView,$fields_headerObjType);
        } else {

            // for default view
            $customView = array(array("field_label" => "Tracking Number", "field_name" => "TrackNo"), array("field_label" => "Requestor", "field_name" => "requestorName"), array("field_label" => "Status", "field_name" => "Status"), array("field_label" => "Date Created", "field_name" => "DateCreated"));
            // push to array in specific location
            $otherFieldsStartPos = 2;
            $fields_headerObj = $headerInfo;
            $fields_headerObj = json_decode(json_encode($fields_headerObj));


            foreach ($fields_headerObj as $fields_header) {
                array_merge($customView, array("field_label" => "" . $fields_header->{'field_label'} . "", "field_name" => "" . $fields_header->{'field_name'} . ""), $otherFieldsStartPos);
                $otherFieldsStartPos++;
            }
            //Copied
            //Updated by AARON TOLENTINO
            // - For having a specific and default header information
            if($fields_headerObjType=="1"){

                $customView = array();
                foreach ($fields_headerObj as $fields_header) {
                    array_push($customView, array("field_label" => "" . $fields_header->{'field_label'} . "", "field_name" => "" . $fields_header->{'field_name'} . ""));
                }
            }else if($fields_headerObjType=="0"){
                // FOR DEFAULT COLUMN
                $customView = array(array("field_label" => "Tracking Number", "field_name" => "TrackNo"), array("field_label" => "Requestor", "field_name" => "requestorName"), array("field_label" => "Status", "field_name" => "Status"), array("field_label" => "Date Created", "field_name" => "DateCreated"));
            }
            $customView = json_decode(json_encode($customView), true);
            // print_r($customView);
        }

		$Objects['custom_view'] = $customView;	
		$this->conn->query("COMMIT");
		return $Objects;
		//return $form_json;
	}


	/*
		Changed by Egay for Default Templates
	*/



}





//Temporary Function - Copied from C:\wamp\www\gs3-insights-2016\trunk\library\Search.php Line 1929
function getValidHeaderFields($fields_headerObj, $getCustomView, $fields_headerObjType) {

    $fields_headerObjDecodedTrue = json_decode(json_encode($fields_headerObj), true);


    if ($fields_headerObjType == "1") {

        $defaultFields = array();
        $customView = array();

        //migrate to default tupe of array not std
        //count of defaultFields
        $otherFieldsStartPos = count($defaultFields);

        //insert default fields in saved header
        foreach ($defaultFields as $defaultFields_value) {
            array_merge($fields_headerObjDecodedTrue, $defaultFields_value, $otherFieldsStartPos);
            $otherFieldsStartPos++;
        }

        //push valid header in custom view

        $customView = combineColumnHeaderInfo($getCustomView['json'], $fields_headerObjDecodedTrue);
    } else if ($fields_headerObjType == "0") {
        $customView = array();
        // FOR DEFAULT COLUMN
        $defaultFields = array(array("field_label" => "Tracking Number", "field_name" => "TrackNo"), array("field_label" => "Requestor", "field_name" => "requestorName"), array("field_label" => "Status", "field_name" => "Status"), array("field_label" => "Date Created", "field_name" => "DateCreated"));
        //push valid header in custom view
        $customView = combineColumnHeaderInfo($getCustomView['json'], $defaultFields);
    } else {

        $defaultFields = array(array("field_label" => "Tracking Number", "field_name" => "TrackNo"), array("field_label" => "Requestor", "field_name" => "requestorName"), array("field_label" => "Status", "field_name" => "Status"), array("field_label" => "Date Created", "field_name" => "DateCreated"));
        $customView = array();

        //migrate to default tupe of array not std
        $fields_headerObjDecodedTrue = json_decode(json_encode($fields_headerObj), true);

        //count of defaultFields
        $otherFieldsStartPos = count($defaultFields);

        //insert default fields in saved header
        foreach ($defaultFields as $defaultFields_value) {
            array_merge($fields_headerObjDecodedTrue, $defaultFields_value, $otherFieldsStartPos);
            $otherFieldsStartPos++;
        }
        $customView = combineColumnHeaderInfo($getCustomView['json'], $fields_headerObjDecodedTrue);
    }
    $customView = json_decode(json_encode($customView), true);

    return $customView;
}


function combineColumnHeaderInfo($getCustomView, $fields_headerObjDecodedTrue) {
    $customView = array();
    //push valid header in custom view
    $getCustomView = json_decode($getCustomView, true);
    foreach ($getCustomView as $value) {
        foreach ($fields_headerObjDecodedTrue as $value2) {
            if ($value['field_name'] == $value2['field_name']) {
                $value2['width'] = $value['width'];
                if ($value2['field_name'] == "TrackNo") {
                    $value2['field_label'] = "Tracking Number";
                } else if ($value2['field_name'] == "DateCreated") {
                    $value2['field_label'] = "Date Created";
                }
                array_push($customView, $value2);
                continue;
            }
        }
    }

    $checker = array();
    foreach ($fields_headerObjDecodedTrue as $value2) {
        foreach ($getCustomView as $value) {
            if ($value['field_name'] == $value2['field_name']) {
                array_push($checker, $value);
            }
        }

        if (count($checker) >= 1) {
            
        } else {
            array_push($customView, $value2);
        }
        $checker = array();
    }
    return $customView;
}



