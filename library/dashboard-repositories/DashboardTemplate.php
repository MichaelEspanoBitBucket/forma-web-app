<?php
	

	class DashboardTemplateRepository
{
	
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}

	public function getAllFormaForms(){
		$this->conn->query("START TRANSACTION");

		$objStmt = $this->conn->prepare
			("SELECT id, IF(form_alias='',form_name,form_alias) AS display_name FROM tb_workspace WHERE is_delete = 0 AND is_active = 1");
		//$objStmt->bind_param("i",$this->user["id"]);
		$objStmt->execute();
		$Forms = $objStmt->get_result()->fetch_all(MYSQLI_BOTH);
	
		$this->conn->query("COMMIT");
		return $Forms;
	}


}




?>