<?php

class DashboardTemplateDAO
{
	
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}


	public function createTemplate($templateName,$layout){

		$layoutJSON = json_encode($layout);
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("INSERT Into tb_dashboard_templates (`layout`,`templateName`,`ganttConfig`,`apps`) VALUES (?,?,'[]','[]')");
		$urStmnt->bind_param("ss",
				$layoutJSON,$templateName);
		$urStmnt->execute();
		$insert_id = $urStmnt->insert_id;
		$this->conn->query("COMMIT");

		return $insert_id;
	}

	public function createUserTemplate($id,$users,$groups){
		$this->conn->query("START TRANSACTION");

		foreach ($users as $value) {
		    $urStmnt = $this->conn->prepare
				("INSERT Into tb_dashboard_user_template (`template_id`,`user_id`) VALUES (?,?)");
			$urStmnt->bind_param("ii",$id,$value['id']
					);
			$urStmnt->execute();
		}

		foreach ($groups as $value) {
		    $urStmnt = $this->conn->prepare
				("INSERT Into tb_dashboard_group_template (`template_id`,`group_id`) VALUES (?,?)");
			$urStmnt->bind_param("ii",$id,$value['id']
					);
			$urStmnt->execute();
		}

		$this->conn->query("COMMIT");

	}

	public function saveLayout($id,$templateName,$users,$groups,$layout){

		$layoutJSON = json_encode($layout);

		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("UPDATE tb_dashboard_templates SET layout = ?, SET templateName = ? WHERE template_id = ?");
		$urStmnt->bind_param("ssi",
				$layoutJSON ,$templateName,$id);
		$urStmnt->execute();
		


		$urStmnt = $this->conn->prepare
			("DELETE from tb_dashboard_user_template WHERE id = ?");
		$urStmnt->bind_param("i",
				$id);
		$urStmnt->execute();
		
		$urStmnt = $this->conn->prepare
			("DELETE from tb_dashboard_group_template WHERE id = ?");
		$urStmnt->bind_param("i",
				$id);
		$urStmnt->execute();


		$this->conn->query("COMMIT");

		$this.createUserTemplate($id,$users,$groups);
		
		
	}

	public function saveUserApps($apps){


		$stringProperties = json_encode($apps);
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("UPDATE tb_dashboard_templates SET apps = ? WHERE user_id = ?");
		$urStmnt->bind_param("si",
				$stringProperties,
				$this->user["id"]);
		$urStmnt->execute();

		$this->conn->query("COMMIT");
		//return $userPref;
	}

	public function saveUserGanttConfig($ganttConfig){


		$stringProperties = json_encode($ganttConfig);
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("UPDATE tb_dashboard_templates SET ganttConfig = ? WHERE user_id = ?");
		$urStmnt->bind_param("si",
				$stringProperties,
				$this->user["id"]);
		$urStmnt->execute();

		$this->conn->query("COMMIT");
		//return $userPref;
	}

	public function getAllTemplates(){
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("Select * from tb_dashboard_templates");
		$urStmnt->execute();
		$results = $urStmnt->get_result()->fetch_all(MYSQLI_BOTH);
		$this->conn->query("COMMIT");
		return $results;
	}

	public function getAllTemplatesByUserId($user_id){
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("SELECT tb_dashboard_user_template.user_id,
				tb_dashboard_user_template.template_id,
				tb_dashboard_templates.templateName,
				tb_dashboard_templates.layout,
				tb_dashboard_templates.apps,
				tb_dashboard_templates.ganttConfig 
				FROM tb_dashboard_user_template 
				LEFT JOIN tb_dashboard_templates on 
				tb_dashboard_user_template.template_id = tb_dashboard_templates.id 
				WHERE tb_dashboard_user_template.user_id = ?");
		$urStmnt->bind_param("i",
				$user_id);
		$urStmnt->execute();
		$results = $urStmnt->get_result()->fetch_all(MYSQLI_BOTH);
		$this->conn->query("COMMIT");
		return $results;
	}

	public function getAllTemplatesByGroupId($group_id){
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("SELECT tb_dashboard_group_template.group_id,
				tb_dashboard_group_template.template_id,
				tb_dashboard_templates.templateName,
				tb_dashboard_templates.layout,
				tb_dashboard_templates.apps,
				tb_dashboard_templates.ganttConfig 
				FROM tb_dashboard_group_template 
				LEFT JOIN tb_dashboard_templates on 
				tb_dashboard_group_template.template_id = tb_dashboard_templates.id 
				WHERE tb_dashboard_group_template.group_id = ?");
		$urStmnt->bind_param("i",
				$group_id);
		$urStmnt->execute();
		$results = $urStmnt->get_result()->fetch_all(MYSQLI_BOTH);
		$this->conn->query("COMMIT");
		return $results;
	}


	
}



