<?php

/**
 * Description of FormsDAO
 *
 * @author Ervinne Sodusta
 */
class FormsDAO {

    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    public function getForm($form_id) {
        $query = "SELECT * FROM tb_workspace WHERE is_delete = 0 AND id = {$form_id}";
        return $this->database->query($query, APIDatabase::QUERY_FETCH_ROW);
    }

}
