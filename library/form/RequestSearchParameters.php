<?php

/**
 * Description of RequestSearchParameters
 *
 * @author Ervinne Sodusta
 */
class RequestSearchParameters {

    const DEFAULT_FETCH_COUNT = 20;

    public $form;
    public $user;
    public $filter_by_form_category_id;
    public $filter_by_field_name;
    public $filter_by_field_value;
    public $start_from_index;
    public $fetch_count;
    public $custom_selection;

    public function __construct() {
        $this->filter_by_form_category_id = 0;
        $this->start_from_index = 0;
        $this->fetch_count = RequestSearchParameters::DEFAULT_FETCH_COUNT;

        $custom_selection = array();
    }

}
