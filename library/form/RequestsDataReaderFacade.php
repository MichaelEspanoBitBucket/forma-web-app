<?php

include_once './adapters/WorkflowAdapter.php';

include_once '/FormsDAO.php';
include_once '/RequestsDAO.php';

/**
 * Description of RequestsDataReaderFacade
 *
 * @author Ervinne Sodusta
 */
class RequestsDataReaderFacade {

    const DEFAULT_REQUEST_SEARCH_SIZE = 20;

    public function searchRequests($database, $user, $form_id, $start_index, $fetch_count) {
        $forms_dao    = new FormsDAO($database);
        $requests_dao = new RequestsDAO($database);

        $request_search_parameters                   = new RequestSearchParameters();
        $request_search_parameters->user             = $user;
        $request_search_parameters->form             = $forms_dao->getForm($form_id);
        $request_search_parameters->start_from_index = $start_index;
        $request_search_parameters->fetch_count      = $fetch_count;

        $requests = $requests_dao->searchRequests($request_search_parameters);

        return $requests;
    }

    public function searchRequestsWithPreview($database, $user, $form, $start_index, $fetch_count) {

        $workflows_dao = new WorkflowsDAO($database);
        
        $raw_workflow_notification_messages = $workflows_dao->getWorkflowNotificationMessages($form["workflow_id"]);
        
    }

}
