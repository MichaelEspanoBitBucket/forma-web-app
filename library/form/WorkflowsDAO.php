<?php

/**
 * Description of WorkflowsDAO
 *
 * @author Ervinne Sodusta
 */
class WorkflowsDAO {

    /** @var APIDatabase */
    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }
    
    public function getFormWorkflowNotificationMessages($form_id) {
        $query = "SELECT     
                    workflow_id,
                    object_id AS workflow_object_id,
                    noti_message AS notification_message
                FROM
                    tbworkflow_objects wobjects
                        LEFT JOIN
                    tbworkflow w ON wobjects.workflow_id = w.id
                WHERE
                    w.form_id = {$form_id} AND w.is_active = 1";

        return $this->database->query($query);
    }

}
