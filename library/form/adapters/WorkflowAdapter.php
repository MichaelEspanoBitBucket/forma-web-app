<?php

/**
 * Description of WorkflowAdapter
 *
 * @author Ervinne Sodusta
 */
class WorkflowAdapter {

    public static function adaptWorkflowNotificationMessagesToPortal($workflow_notification_messages) {

        $adapted_messages = array();

        foreach ($workflow_notification_messages AS $message) {
            $raw_message = json_decode($message["notification_message"]);
            array_push($adapted_messages, array(
                "workflow_id" => $message["workflow_id"],
                "object_id" => $message["object_id"],
                "message_type" => $raw_message["type"],
                "message_contents" => $raw_message["message"]
            ));
        }

        return $adapted_messages;
    }

}
