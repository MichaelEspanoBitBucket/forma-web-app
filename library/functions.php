<?php

class functions extends library {

    

    // Validate JSON 
    public function json_validate($string) {
        // decode the JSON data
        $result = json_decode($string);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid // No error has occurred
                break;
            case JSON_ERROR_DEPTH:
                $error = 'The maximum stack depth has been exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Invalid or malformed JSON.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Control character error, possibly incorrectly encoded.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.';
                break;
            // PHP >= 5.3.3
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_RECURSION:
                $error = 'One or more recursive references in the value to be encoded.';
                break;
            // PHP >= 5.5.0
            case JSON_ERROR_INF_OR_NAN:
                $error = 'One or more NAN or INF values in the value to be encoded.';
                break;
            case JSON_ERROR_UNSUPPORTED_TYPE:
                $error = 'A value of a type that cannot be encoded was given.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }

        if ($error !== '') {
            // throw the Exception or exit // or whatever :)
            exit($error);
        }

        // everything is OK
        return $result;
    }

    // Format Bytes
    function formatBytes($size, $precision = 2) {
        $base = log($size, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');

        return round(pow(1024, $base - floor($base)), $precision);
    }

    // config functions for validation
    public function config_validation($data) {
        if ($data['validate_empty'] == 0) {
            return ($data['name'] == $data['value_to_compare'] ? $data['default_value'] : $data['else_value']);
        } else {
            return ($data['name'] == "" ? $data['default_value'] : $data['else_value']) || ($data['name'] == $data['value_to_compare'] ? $data['default_value'] : $data['else_value']);
        }
    }

    // Generate Random chars

    public function random_chars($limit) {
        $random = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789', 5)), 0, $limit);

        return $random;
    }

    public function guest_redirection_auto_logged() {
        if (Auth::hasAuth('Guest_AUTO_LOGGED') && Auth::getAuth('Guest_AUTO_LOGGED') == 1) {
            $guest_auto_logged = Auth::getAuth('Guest_AUTO_LOGGED');
            $guest_url = Auth::getAuth('Guest_URL');

            header('Location: ' . $guest_url);

            // return false;
        }
    }

    public function loadJSCSS_Script($modules, $folder) {
        $js_mod = unserialize($modules); // Other config
        $page = functions::get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']);
        $ret = "";
        foreach ($js_mod as $key => $value) {
            // var_dump($value['modules_in']);
            //foreach ($value['modules_in'] as $module_key => $module_value) {

            if (in_array($page, $value['modules_in'])) {
                // echo $page."-".$module_value."-".$key;
                // echo "<br>";

                if ($value['has_folder'] == "1") {
                    if ($value['read_all_file'] == "1") {
                        $files = $this->loadJSCSSFUNC("js/" . $folder . "/" . $key);
                        // echo "1";
                        // break;
                        // print_r($files);
                        // echo '<script type="text/javascript" src="/'.$files.'"></script>';
                    } else {
                        $ret .= '<script type="text/javascript" src="/js/' . $folder . '/' . $value['folder_name'] . "/" . $key . '.js"></script>';
                        $ret .= PHP_EOL;
                        // echo '<script type="text/javascript" src="/js/functions/' . $key . '.js"></script>';
                    }
                } else {
                    $ret .= '<script type="text/javascript" src="/js/' . $folder . '/' . $key . '.js"></script>';
                    $ret .= PHP_EOL;
                }
            } else if (in_array('all', $value['modules_in'])) {
                if ($page != "login" && $page != "") {
                    if ($value['has_folder'] == "0") {
                        $ret .= '<script type="text/javascript" src="/js/' . $folder . '/' . $key . '.js"></script>';
                    } else {
                        $files = $this->loadJSCSSFUNC("js/" . $folder . "/" . $key);
                    }

                    $ret .= PHP_EOL;
                }
            }
            //}
        }

        return $ret;
    }

    public function loadJSCSSFUNC($dir) {
        $ffs = scandir($dir);
        $i = 0;
        $list = array();

        foreach ($ffs as $ff) {
            if ($ff != '.' && $ff != '..') {
                //if ( strlen($ff)>=5 ) {
                //if ( substr($ff, -4) == '.php' ) {
                // $list[] = $ff;
                //echo dirname($ff) . $ff . "<br/>";
                $ext = explode(".", $ff);
                $get_ext = $ext[count($ext) - 1];

                if ($get_ext == "js") {
                    echo '<script type="text/javascript" src="/' . $dir . '/' . $ff . '' . '"></script>';
                    echo PHP_EOL;
                    // echo $dir.'/'.$ff.'';
                } elseif ($get_ext == "css") {
                    echo '<link rel="Stylesheet" type="text/css" href="/' . $dir . '/' . $ff . '' . '" />';
                    echo PHP_EOL;
                    // echo $dir.'/'.$ff.'';
                } elseif ($get_ext == "json") {
                    echo '<script type="text/javascript" src="/' . $dir . '/' . $ff . '' . '"></script>';
                    echo PHP_EOL;
                    // echo $dir.'/'.$ff.'';
                }
                //}    
                //}       
                if (is_dir($dir . '/' . $ff))
                    $this->loadJSCSSFUNC($dir . '/' . $ff);
            }
        }
        return $list;
    }

    /* ========== Get Page Visit ========== */

    public function get_page_visit($dis) {
        //$server = $_SERVER["REQUEST_URI"];
        $auth = Auth::getAuth('current_user');

        $date = $dis->currentDateTime();

        $sql = "SELECT * FROM tb_page_logs WHERE page = {$dis->escape($_SERVER["REQUEST_URI"])} AND visit_by = {$dis->escape($auth['id'])}
				AND company_id = {$dis->escape($auth['company_id'])}";

        $get_page_query = $dis->query($sql, "row");
        $get_page_nums = $dis->query($sql, "numrows");

        if ($_SERVER["REQUEST_URI"] != USER_VIEW) {
            if ($get_page_nums == 0) {
                $insert = array("page" => $_SERVER["REQUEST_URI"],
                    "main_page" => MAIN_PAGE,
                    "date_visit" => $date,
                    "date_last_visit" => $date,
                    "visit_times" => "1",
                    "visit_by" => $auth['id'],
                    "company_id" => $auth['company_id'],
                    "is_active" => "1"
                );
                $dis->insert("tb_page_logs", $insert);
            } else {
                $insert = array(
                    "date_last_visit" => $date,
                    "visit_times" => $get_page_query['visit_times'] + 1
                );
                $dis->update("tb_page_logs", $insert, array("id" => $get_page_query['id']));
            }
        }
    }

    public function set_default_timezone_datetime() {
        $timezone = "Asia/Manila";
        if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);
        $date = date('Y-m-d');
        $time = date('H:i:s');
        $date_time = date('Y-m-d H:i:s');

        return array("date" => $date,
            "time" => $time,
            "date_time" => $date_time);
    }

    public function login_validation($db, $session, $post, $field) {
        $date_time = $this->set_default_timezone_datetime();
        $username = $post['username'];
        $password = $post['password'];

        $login = $session->login($username, $password, $field, 'password', 'tb_users');

        //$this->login_redirection($db, $login, $date_time['date_time']);

        return $login;
    }

    /* Save Audit logs for every action of the user 
      /* ====================================================================================== */

    public function save_audit_logs($db, $auth_id, $action_id, $table_name, $date_time, $record_id,$action_description) {
        $audit_logs = new Audit_Logs($db, $auth_id);

        $audit_logs->user_id = $auth_id;
        $audit_logs->audit_action = $action_id;
        $audit_logs->table_name = $table_name;
        $audit_logs->record_id = $record_id;
        $audit_logs->ip = $_SERVER['REMOTE_ADDR'];
        $audit_logs->date = $date_time;
        $audit_logs->is_active = "1";
        $audit_logs->action_description = $action_description;
        $audit_logs->save();
    }

    /* Encode and Decode URL */
    /* ====================================================== */

    public function encode_decode_url($url, $type) {
        if ($type == "encode") {
            return urlencode($url);
        } else {
            return urldecode($url);
        }
    }

    /* URL REDIRECT  */
    /* ====================================================== */

    public function url_redirect() {
        $current_url = $this->encode_decode_url($this->curPageURL("module_parameter"), "encode");

        $url = $_SERVER['REQUEST_URI'];
        if ($url != "") {
            exit(header('location: /login?redirect_url=' . $current_url));
        } else {
            exit(header('location: /login'));
        }
    }

    public function redirection_admin($user_level_id) {
        if (ALLOW_USER_ADMIN != "1") {
            if (ALLOW_USER_VIEW == "1" && $user_level_id == "3") {
                header('location: ' . USER_VIEW);
            }
        } else {
            if ($user_level_id == "2" && ALLOW_USER_ADMIN == "1" || $user_level_id == "3" && ALLOW_USER_ADMIN == "1") {
                if (ALLOW_USER_VIEW == "1" && $user_level_id == "2") {
                    header('location: ' . USER_VIEW);
                } else if (ALLOW_USER_VIEW == "1" && $user_level_id == "3") {
                    header('location: ' . USER_VIEW);
                }
            }
        }
    }

    public function redirection_user($user_level_id) {
        if (ALLOW_USER_ADMIN != "1") {
            if ((ALLOW_USER_VIEW == "1" && $user_level_id != "3") || (ALLOW_USER_VIEW != "1" && $user_level_id != "3") || (ALLOW_USER_VIEW != "1" && $user_level_id == "3")) {
                header('location: /');
            }
        } else {
            //if($user_level_id=="3" && ALLOW_USER_ADMIN == "1"){
            //    header('location: /');
            //}
            //if($user_level_id=="2" && ALLOW_USER_ADMIN == "1" || $user_level_id=="3" && ALLOW_USER_ADMIN == "1"){
            //if((ALLOW_USER_VIEW=="1" && $user_level_id!="2") || (ALLOW_USER_VIEW!="1" && $user_level_id!="2")
            //    || (ALLOW_USER_VIEW!="1" && $user_level_id=="2")){
            //     header('location: /');
            //}
            //}
        }
    }

    public function userLevelAuth_v2($level) {
        $db = new Database();
        $user_level = $db->query("SELECT * FROM tbuser_level WHERE id='$level'", "row");
        if (ALLOW_USER_ADMIN != "1") {
            if ($user_level['user_level'] != "Company Admin") {
                header('Location: ' . MAIN_PAGE . substr(USER_VIEW, 1) . 'application_portal');
            }
        } else {
            if ($user_level['user_level'] == "Company Admin") {
                header('Location: /');
                header('Location: ' . MAIN_PAGE . substr(USER_VIEW, 1) . 'application_portal');
                ;
            }
        }
    }

    public function get_checked($data) {
        if ($data == "1") {
            $checked = "checked=checked";
            $val = "1";
            $hide = "";
        } else {
            $checked = "";
            $val = "0";
            $hide = "display";
        }
        return array("checked" => $checked, "val" => $val, "hide" => $hide);
    }

    public function landing_page($auth) {
        if ($this->get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "formbuilder" || $this->get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "organizational_chart" || $this->get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "workflow" || $this->get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "report" || $this->get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "generate" || $this->get_module($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']) == "workspace") {
            $display = "display";
        }
        // Set Default Landing Page
        // Main Content
        if ($auth['user_level_id'] == "1" || $auth['user_level_id'] == "2") {
            echo '<div class="' . $display . '" style="float: right;border: 1px solid #d5d5d5;padding: 5px;border-right: none;border-top: none;">';

            $db = new Database();
            $current_page = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

            $getPage = $db->query("SELECT * FROM page_settings WHERE userID={$db->escape($auth['id'])} AND is_active={$db->escape(1)}", "row");

            if ($current_page == $getPage['page']) {
                $checked = "checked=checked";
            }
            $popover = 'rel="popover" data-placement="bottom" data-content="Allow you to set the default landing page once you logged in." data-original-title="New Update!"';


            echo '<input type="checkbox"  ' . $checked . ' class="setting_landing_page cursor updates_settings" style="float: left; margin-top: 3px; margin-right: 5px;"> Make as default landing page?';


            echo '</div>';
        }
    }

    // Base encode / decode
    public function base_encode_decode($action, $string) {
        if ($action == "encrypt") {
            $encode = base64_encode(base64_encode(base64_encode(base64_encode($string))));
            return $encode;
        } elseif ($action == "decrypt") {
            $decode = base64_decode(base64_decode(base64_decode(base64_decode($string))));
            return $decode;
        }
    }

    // Shorten a word of string
    public function truncate($str, $len) {
        $tail = max(0, $len - 10);
        $trunk = substr($str, 0, $tail);
        $trunk .= strrev(preg_replace('~^..+?[\s,:]\b|^...~', '...', strrev(substr($str, $tail, $len - $tail))));
        return $trunk;
    }

    // Abbreviate string
    public function initials($str) {
        $ret = '';
        foreach (explode(' ', $str) as $word)
            $ret .= strtoupper($word[0]);
        return $ret;
    }

    //public $pubkey = '...public key here...';
    // public $privkey = '...private key here...';
    // User Level
    public function userLevelAuth($level) {
        $db = new Database();
        $user_level = $db->query("SELECT * FROM tbuser_level WHERE id='$level'", "row");
        if ($user_level['user_level'] != "Company Admin") {
            header('Location: /');
        }
    }

    public function removePairedBracket($string) {
        $pattern = "[]";
        return str_replace($pattern, "", $string);
    }

    /*
     * Arrange json formatter 
     * for better viewing of json_encode
     */

    public function view_json_formatter($string) {
        $pattern = array(',"', '{', '}');
        $replacement = array(",\n\t\"", "\n{\n\t", "\n}");
        return str_replace($pattern, $replacement, $string);
    }

    public function getStatus($val) {
        if ($val == "1") {
            return "<div class='fl_badge_SpringGreen isDisplayInlineBlock'>Active</div>";
        } else if ($val == "2") {
            return "<div class='fl_badge_silver isDisplayInlineBlock'>Draft</div>";
        } else {
            return "<div class='fl_badge_silver isDisplayInlineBlock'>Not Active</div>";
        }
    }

//    public function getFormType($val) {
//        if ($val == "1") {
//            return "Published";
//        } else if($val=="2"){
//            return "Template";
//        }
//    }
    public function getFormVersion($val) {
        if ($val == "1") {
            return "<div class='fl_badge_SpringGreen isDisplayInlineBlock'>Production</div>";
        } else if ($val == "2") {
            return "<div class='fl_badge_silver isDisplayInlineBlock'>Development</div>";
        }
    }

    public function getFormVersionStr($val) {
        if ($val == "1") {
            return "Production";
        } else if ($val == "2") {
            return "Development";
        }
    }

    public function getStatus_revert($val) {
        if ($val == "1") {
            return "Deactivate";
        } else {
            return "Activate";
        }
    }

    public function setSelected($value, $selectedValue) {
        $ret = "";
        if ($value == $selectedValue) {
            $ret = "selected = 'selected'";
        }
        return $ret;
    }

    public function workspace_header($workspace_name, $default) {
        $auth = Auth::getAuth('current_user');
        $ret = "";
        $ret .= '<div class="head">';
        $ret .= '<div class="title_head">';
        $ret .= '<i class="icon-asterisk"></i> ' . $workspace_name;
        $ret .= '</div>';
        //if($auth['user_level_id']=="1" || $auth['user_level_id']=="2"  || $auth['user_level_id']=="3"){
        //    return $ret;
        //}
        $ret .= '<div class="right_bar_breadcrumbs">';
        $ret .= '<div class="breadCrumb module">';
        $ret .= '<ul>';
        $ret .= '<li class="firstB"><a href="/">Home</a> </li>';
        $ret .= '<li><a href="/organizational_chart" class="';
        if ($default == "organizational_chart") {
            $ret .= 'red';
        }
        $ret .= '">Organizational Chart</a></li>';
        $ret .= '<li><a href="/formbuilder?formID=0" class="';
        if ($default == "formbuilder") {
            $ret .= 'red';
        }
        $ret .= '">Form Builder</a></li>';
        $ret .= '<li class="lastB"><a href="/workflow" class="cursor getForm ';
        if ($default == "workflow") {
            $ret .= 'red';
        }
        $ret .= '">Workflow</a></li>';
        $ret .= '</ul>';
        $ret .= '</div>';
        $ret .= '</div>';
        $ret .= '</div>';

        return $ret;
    }

    // Replace get method special character
    /*
     * @$var = string to be replace
     * @$char_str_replace = character to be replace on the string
     * @$char_str_replace = character to be add after replace
     *
     */
    public function getMethod_replace($var, $char_str_replace, $char_remove_replace) {
        $string = str_replace(" ", $char_str_replace, $var); // Replaces all spaces with hyphens.

        $string = preg_replace('/-/', $char_remove_replace, $string);

        return $this->encrypt_decrypt("decrypt", $string);
    }

    // Validate Email on the field

    public function VerifyMailAddress($emailAddress) {
        $Syntax = '/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/';
        if (preg_match($Syntax, $emailAddress))
            return true;
        else
            return false;
    }

    // Encrypt String

    public function encrypt_decrypt($action, $string) {
        $output = false;

        $key = 'MyNaturalSuperStrongRandomKey';

        // initialization vector 
        $iv = md5(md5(md5($key)));

        if ($action == 'encrypt') {
            $output = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(md5(md5($key))), $string, MCRYPT_MODE_CBC, md5(md5(md5($iv))));
            $output = base64_encode($output);
            //$output = htmlspecialchars($output, ENT_QUOTES);
        } else if ($action == 'decrypt') {
            $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(md5(md5($key))), base64_decode($string), MCRYPT_MODE_CBC, md5(md5(md5($iv))));
            $output = rtrim($output);
            //$output = htmlspecialchars_decode($output);
        }
        return $output;
        //$plain_txt = "password";
        //echo "Encrypted Text = ".$fs->encrypt_decrypt('encrypt', $plain_txt)."\n";
        //echo "Decrypted Text = ".$fs->encrypt_decrypt('decrypt', $fs->encrypt_decrypt('encrypt', $plain_txt))."\n";
        //echo "\n";
    }

    // Send Email Via PHP Mailer
    // optional, gets called from within class.phpmailer.php if not already loaded
    // the true param means it will throw exceptions on errors, which we need to catch

    public function sendEmail_smtp($body, $to = array(), $cc = array(), $bcc = array(), $from, $to_title, $from_title, $mail_title, $mail_doc_id) {
        $date = $this->currentDateTime();


        $mail_doc = array();
        $mail_doc["body"] = str_replace('\\', '', $body);
        $mail_doc["mail_to"] = json_encode($to);
        $mail_doc["cc"] = json_encode($cc);
        $mail_doc["bcc"] = json_encode($bcc);
        $mail_doc["mail_from"] = $from;
        $mail_doc["to_title"] = $to_title;
        $mail_doc["from_title"] = $from_title;
        $mail_doc["mail_title"] = $mail_title;


        $mail = new PHPMailer(true);

        $mail->IsSMTP(); // telling the class to use SMTP

        try {
            $mail->Host = SMTP_HOST; // SMTP server
            $mail->SMTPDebug = 2;                     // enables SMTP debug information (for testing)
            if (SMTP_AUTH != "") {
                $mail->SMTPAuth = true;                  // enable SMTP authentication
            }else{
                $mail->SMTPAuth = false;    
            }
            if (SMTP_SECURE != "") {
                $mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
            }else{
                $mail->SMTPSecure = "tls";       
            }
            //$mail->Host = "smtp.gmail.com";      // sets GMAIL as the SMTP server
            $mail->Port = SMTP_PORT;                   // set the SMTP port for the GMAIL server
            $mail->Username = SMTP_USERNAME;  // GMAIL username
            $mail->Password = SMTP_PASSWORD;            // GMAIL password
            // To
            foreach ($to as $email => $name) {
                $mail->AddAddress($email, $name);
            }
            // CC
            foreach ($cc as $email => $name) {
                $mail->addCC($email, $name);
            }
            // BCC
            foreach ($bcc as $email => $name) {
                $mail->addBCC($email, $name);
            }

            //$mail->AddAddress('samuel_boa2000@yahoo.com', 'Samuel Pulta');
            $mail->SetFrom($from, $from_title);
            //$mail->AddReplyTo('ervinne.sodusta@gs3.com.ph', 'Ervinne Sodusta');
            $mail->Subject = $mail_title;
            //$mail->AddCC('admin@example.com', 'Example.com');
            //$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically


            $mail->MsgHTML($body);
            //$mail->AddAttachment('images/phpmailer.gif');      // attachment
            //$mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
            ob_start();
            $mail->Send();
            ob_get_clean();
            $mail_doc["status"] = "Success";
            $mail_doc["status_message"] = "Email Sent.";
            if (count($to) > 0 || count($cc) > 0 || count($bcc) > 0) {
                createEmailLog("Mail (" . $mail_title . ") sent to " . json_encode($to) . "," . json_encode($cc) . " ," . json_encode($bcc));
            }
            //return "Message Sent OK</p>\n";
        } catch (phpmailerException $e) {
            $mail_doc["status"] = "Failed";
            $mail_doc["status_message"] = $e->errorMessage();
            if (count($to) > 0 || count($cc) > 0 || count($bcc) > 0) {
                createEmailLog("Unable to send mail (" . $mail_title . ") to " . json_encode($to) . "," . json_encode($cc) . " ," . json_encode($bcc) . " : " . $e->errorMessage());
            }
//            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {

//            echo $e->getMessage(); //Boring error messages from anything else!
        }

        if (count($to) > 0 || count($cc) > 0 || count($bcc) > 0) {
            $conn = getCurrentDatabaseConnection();

            if ($mail_doc_id) {
                $mail_doc["updated_date"] = $date;
                $conn->update("tbemail_logs", $mail_doc, array("id" => $mail_doc_id));
            } else {
                $mail_doc["created_date"] = $date;
                $conn->insert("tbemail_logs", $mail_doc);
            }
        }
    }

    // Date

    public function currentDateTime() {
        // $timezone = "America/Los_Angeles";
        // if (function_exists('date_default_timezone_set'))
        // date_default_timezone_set($timezone);
        // return date("Y-m-d  H:i:s");
        $db = new Database();
        $row = $db->query("SELECT NOW() as date", "row");
        return $row['date'];
    }

    public function dateType() {
        $timezone = "Asia/Manila";
        if (function_exists('date_default_timezone_set'))
            date_default_timezone_set($timezone);

        return date("F j, Y");
    }

    // Notifications

    public function setNotification($container, $icon, $msg) {
        $ret = "";
        $ret .= '<div class="notification_wrapper">';
        $ret .= '<div class="' . $container . '">';
        $ret .= '<img src="/images/warning/' . $icon . '.png" width="22" height="19" class="img-notification-position">';
        $ret .= '<div class="notification-position">';
        $ret .= $msg;
        $ret .= '</div>';
        $ret .= '</div>';
        $ret .= '</div>';
        return $ret;
    }

    // Get Url of the page
    public function curPageURL($params) {
        $pageURL = 'http';
        $pageURL .= "://";
        if ($params == "?") {
            $par = $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            $par = explode("?", $par);
            $pageURL .= $par[0];
        } elseif ($params == "") {
            if ($_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
            }
        } elseif ($params == "module") {
            if ($_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            if ($_SERVER["SERVER_PORT"] == "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
            } else {
                $pageURL = substr($_SERVER["REQUEST_URI"], 1);
                // $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
            }
        } elseif ($params == "module_parameter") {
            if ($_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            $pageURL = substr($_SERVER["REQUEST_URI"], 1);
        } elseif ($params == "serverName") {
            if ($_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            $pageURL .= $_SERVER["SERVER_NAME"];
        } elseif ($params == "hasport") {
            if ($_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
        } elseif ($params == "port") {
            if ($_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            $auth = Auth::getAuth('current_user');
            if (ALLOW_USER_ADMIN != "1") {
                if (ALLOW_USER_VIEW == "1") {
                    if ($user_level_id == "2" || $user_level_id == "1") {
                        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
                    } else if ($user_level_id == "3") {
                        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . "/user_view";
                    } else {
                        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . "/user_view";
                    }
                }
            } else {
                if (ALLOW_USER_VIEW == "1") {
                    if ($auth['user_level_id'] == "2" && ALLOW_USER_ADMIN == "1" || $auth['user_level_id'] == "3" && ALLOW_USER_ADMIN == "1") {
                        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . "/user_view";
                    } else {
                        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . "/user_view";
                    }
                } else {
                    $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
                }
            }
            //if (ALLOW_USER_VIEW == "1" && $auth['user_level_id'] == "3") {
            //    $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . "/user_view";
            //} else {
            //    $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
            //}
        } elseif ($params == "baseuri") {
            if ($_SERVER["HTTPS"] == "on") {
                $pageURL .= "s";
            }
            if ($_SERVER["SERVER_PORT"] != "80") {
                $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"];
            } else {
                $pageURL .= $_SERVER["SERVER_NAME"];
            }
        }

        return $pageURL;
    }

    public function get_module($uri, $string) {
        return basename($uri, '?' . $string);
    }

    // Limit Counter
    public function counterLimmit($count) {
        if ($count > 100) {
            return "+" . substr($count, 0, 3);
        } else {
            return $count;
        }
    }

    // Tabs
    public function tabber($tab_name) {
        $ret = "";
        $ret .= '<ul class="tabs">';
        foreach ($tab_name as $name => $key) {
            $ret .= '<li rel="' . $name . '" ';
            if ($name == "tab1") {
                $ret .= 'class="active"';
            }
            $ret .= '>' . $key . '</li>';
        }
        $ret .= '</ul>';
        return $ret;
    }

    //get fields
    public function getFormFields($id) {
        $db = new Database();
        $sql = "SELECT active_fields FROM tb_workspace WHERE is_delete = 0 AND id = {$db->escape($id)}";
        $getActiveFields = $db->query($sql, "row");
        $array_activeFields = explode(",", $getActiveFields['active_fields']);
        return $array_activeFields;
    }

    //added jewel tolentino
    //get reports
    public static final function getReports($selector) {
        $db = new Database();
        $result = $db->query("SELECT id FROM tbreport " . $selector, 'array');
        $reports = array();

        foreach ($result as $value) {
            $reportDoc = new Report($db, $value['id']);
            array_push($reports, $reportDoc);
        }

        return $reports;
    }

    public static final function getWorkspace($selector) {
        $db = new Database();
        $result = $db->query("SELECT id FROM tb_workspace " . $selector, 'array');
        $forms = array();

        foreach ($result as $value) {
            $formDoc = new Form($db, $value['id']);
            array_push($forms, $formDoc);
        }

        return $forms;
    }

    public static final function getUsers($selector) {
        $db = new Database();
        $result = $db->query("SELECT id FROM tbuser " . $selector, 'array');
        $users = array();

        foreach ($result as $value) {
            $personDoc = new Person($db, $value['id']);

            array_push($users, $personDoc);
        }

        return $users;
    }

    public static final function getRequestLogs($conn, $selector) {
        $result = $conn->query("SELECT * FROM tbrequest_logs " . $selector);

        return $result;
    }

    public static final function getKeywords($selector) {
        $db = new Database();
        $result = $db->query("SELECT id FROM tbkeyword " . $selector, 'array');

        $keywords = array();

        foreach ($result as $value) {
            $keywordDoc = new Keyword($db, $value['id']);

            array_push($keywords, $keywordDoc);
        }

        return $keywords;
    }

    public static final function getDashboards($selector) {
        $db = new Database();
        $result = $db->query("SELECT id FROM tbdashboard " . $selector, 'array');

        $dashboards = array();

        foreach ($result as $value) {
            //$keywordDoc = new Keyword($db, $value['id']);
            $dashboardDoc = new Dashboard($db, $value['id']);

            array_push($dashboards, $dashboardDoc);
        }

        return $dashboards;
    }

    public static final function getDashboardsObjects($selector) {
        $db = new Database();
        $result = $db->query("SELECT id FROM tbdashboard_objects " . $selector, 'array');

        $dashboards_objects = array();

        foreach ($result as $value) {
            //$keywordDoc = new Keyword($db, $value['id']);
            $dashboardObjectDoc = new Dashboard_Object($db, $value['id']);

            array_push($dashboards_objects, $dashboardObjectDoc);
        }

        return $dashboards_objects;
    }

    public static final function getFormActiveWorkflow($formDoc) {
        $db = new Database();
        $result = $db->query("SELECT "
                . " wfo.* "
                . " FROM tbworkflow_objects wfo"
                . " LEFT JOIN tbworkflow wf"
                . " ON wf.id =  wfo.workflow_id"
                . " WHERE wf.is_active='1' AND wf.form_id = {$formDoc->id} AND wf.is_delete = 0 AND type_rel=1", 'row');

        return $result;
    }

    public static final function getUsersByMode($array_info, $mode = 1) {
        $db = new Database();
        $users = array();


        switch ($mode) {
            case 1:
//                $result = $db->query("SELECT user_id FROM tbdepartment_users department_users "
//                        . "LEFT JOIN tbuser user "
//                        . "ON user.id = department_users.user_id  " . $selector, "array");

                $department_users = $db->query("SELECT "
                        . "getDepartmentUsers({$db->escape($array_info["Department_Code"])},{$array_info["Position_Level"]}, {$array_info["Company_ID"]}) as Users", "row");
//                print_r("SELECT user_id FROM tbdepartment_users department_users "
//                        . "LEFT JOIN tbuser user "
//                        . "ON user.id = department_users.user_id  " . $selector);

                $ids = split(",", $department_users["Users"]);

                foreach ($ids as $id) {
                    $personDoc = new Person($db, $id);
                    array_push($users, $personDoc);
                }

                break;
        }

        return $users;
    }

    public static final function getUserMailInfo($auth, $array_info, $mode = 1) {
        $db = new Database();
        $return_array = array();

        switch ($mode) {
            case 1 :
                //base on department
                foreach ($array_info as $department_code) {
//                    $recipient_department_users = functions::getUsersByMode("WHERE department_code = {$db->escape($department_code)}"
//                                    . " AND company_id={$db->escape($auth['company_id'])}", 1);
                    $recipient_department_users = functions::getUsersByMode(array(
                                "Department_Code" => $department_code,
                                "Position_Level" => 0,
                                "Company_ID" => $auth['company_id']), 1);

                    foreach ($recipient_department_users as $userDoc) {
                        $return_array[$userDoc->email] = $userDoc->display_name;
                    }
                }

                break;
            case 2:
                //base on position
                foreach ($array_info as $position) {
                    $recipient_position_users = functions::getUsers("WHERE position ={$db->escape($position)} AND company_id={$db->escape($auth['company_id'])}");
                    foreach ($recipient_position_users as $userDoc) {
                        $return_array[$userDoc->email] = $userDoc->display_name;
                    }
                }

                break;
            case 3:
                //base on group
                foreach ($array_info as $group) {
                    $group_users = functions::getUsersByGroup($group);
                    foreach ($group_users as $user) {
                        $return_array[$user["email"]] = $user["display_name"];
                    }
                }
        }

        return $return_array;
    }

    public static final function getUsersByGroup($group_id) {
        $db = new Database();
        $users = $db->query("SELECT email, display_name FROM tbuser WHERE FIND_IN_SET(id, getGroupUsers({$group_id}))", "array");
        return $users;
    }

    public static final function getUsersObjectByGroup($group_id) {

        $db = new Database();

        $return_users = array();

        $result = $db->query("SELECT getGroupUsers({$group_id}) as Users", "row");

        $users = split(",", $result["Users"]);
        foreach ($users as $user) {
            $personDoc = new Person($db, $user);
            array_push($return_users, $personDoc);
        }

        return $return_users;
    }

    public static final function getDepartmentUserByLevel($auth, $department_code, $processor_level) {
        $db = new Database();
        $return_array = array();

//        $department_head_recipients = functions::getUsersByMode("WHERE department_code={$db->escape($department_code)}"
//                        . " AND department_position = {$processor_level}", 1);
        $department_head_recipients = functions::getUsersByMode(array(
                    "Department_Code" => $department_code,
                    "Position_Level" => $processor_level,
                    "Company_ID" => $auth['company_id']), 1);

        foreach ($department_head_recipients as $processorDoc) {
            $return_array[$processorDoc->email] = $processorDoc->display_name;
        }

        return $return_array;
    }

    public static final function getDepartmentUsers($auth, $department_code, $user_level) {
        $db = new Database();
        $return_users = array();

        $result = $db->query("SELECT getDepartmentUsers({$db->escape($department_code)}, {$user_level}, {$auth["company_id"]}) as Users", "row");
        $users = split(",", $result["Users"]);
        foreach ($users as $user) {
            $personDoc = new Person($db, $user);
            array_push($return_users, $personDoc);
        }

        return $return_users;
    }

    public static final function getUsersByPosition($auth, $position_id) {
        $db = new Database();

        $return_users = array();

        $result = $db->query("SELECT getUsersByPosition({$position_id}) as Users", "row");

        $users = split(",", $result["Users"]);
        foreach ($users as $user) {
            $personDoc = new Person($db, $user);
            array_push($return_users, $personDoc);
        }

        return $return_users;
    }

    public static final function getDepartmentHeadAssistantHead($arr = array()) {
        $auth = Auth::getAuth('current_user');
        $db = new Database();
        $user_department_position_level = $arr["user_department_position_level"];
        $processorType = $arr["processorType"];
        $processor_department = $arr["processor_department"];

        do {
            //loop until processor is found
            $parent_department = $db->query("SELECT PARENT_DEPARTMENT.id as Parent_Department FROM tborgchartobjects CHILD_DEPARTMENT
                        LEFT JOIN tborgchartline DEPARMENTLINE 
                        ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id AND DEPARMENTLINE.orgChart_id = CHILD_DEPARTMENT.orgChart_id 
                        LEFT JOIN tborgchartobjects PARENT_DEPARTMENT 
                        ON PARENT_DEPARTMENT.object_id = DEPARMENTLINE.parent AND PARENT_DEPARTMENT.orgChart_id  = DEPARMENTLINE.orgChart_id
                        WHERE CHILD_DEPARTMENT.id ={$db->escape($processor_department)} LIMIT 1 ", "row");

            $parent_department_id = $parent_department['Parent_Department'];

            if ($user_department_position_level == 1 && $parent_department_id != "") {
                $processor_department = $parent_department_id;
            }

            if ($user_department_position_level == 2 && $processorType == 2 && $parent_department_id != "") {
                $processor_department = $parent_department_id;
            }
//                        $processor_result = $this->query("SELECT json
//                                    FROM tborgchartobjects DEPARTMENT WHERE id = {$this->escape($processor_department)}", "row");
//                        MODIFIED JEWEL 08072014 FROM SPECIFIC USER ID AS PROCESSOR TO DEPARTMENT CODE

            $processor_result = $db->query("SELECT json, department_code
                                    FROM tborgchartobjects DEPARTMENT WHERE id = {$db->escape($processor_department)}", "row");

            $new_processor = $processor_result["department_code"];

            if ($processorType == 1) {
                $department_data = json_decode($processor_result['json'], true);
                $processor = $department_data['orgchart_user_head'][0];


                if ($processor == '' || $processor == 'null' || $processor == null || $processor == 0) {//if head not found request will be forwarded to assistant head of the parent department
                    $processor_department = $parent_department_id;
                    $processorType = 2;
                }
            } else {
                //assistant head
                $department_data = json_decode($processor_result['json'], true);
                $processor = $department_data['orgchart_dept_assistant'][0];

                if ($processor == '' || $processor == 'null' || $processor == null || $processor == 0) {//if assistant head not found request will be forwarded to head
//                                $new_processor = $processor_result["department_code"];
                    $processorType = 1;
                }
            }
        } while (($processor == '' || $processor == 'null' || $processor == null || $processor == 0) && $parent_department_id != '');

        if ($processor == '' || $processor == 'null' || $processor == null || $processor == 0) {
            return "";
        } else {
            $processorDoc = new Person($db, $processor);
            return $processorDoc->display_name;
        }
    }

    public static final function getFormFieldsData($selector) {
        $db = new Database();
        $result = $db->query("SELECT * FROM tbfields " . $selector, "array");

        return $result;
    }

    public static final function getFields($selector) {
        $db = new Database();
        $fields = $db->query("SELECT id FROM tbfields " . $selector, "array");
        $return_fields = array();

        foreach ($fields as $field) {
            $fieldDoc = new Field($db, $field["id"]);
            array_push($return_fields, $fieldDoc);
        }

        return $return_fields;
    }

    public function changeDepartments($array_var, $search_val, $repalce_val) {
        if ($array_var == "") {
            return "";
        }
        $json_res_a = $array_var;
        $db = new Database();
        foreach ($json_res_a as $key_value => $this_value) {
            if ($key_value == "departments") {
                foreach ($json_res_a->{$key_value} as $key_value2 => $this_value2) {
                    $getDept = $db->query("SELECT * FROM tborgchartobjects WHERE id='" . $this_value2 . "'", "row");
                    if ($getDept['department'] == $search_val) {
                        $json_res_a->{$key_value}[$key_value2] = $repalce_val;
                    }
                }
            }
        }
        return json_encode($json_res_a);
    }

    public function changeDepartmentsArray($array_var, $search_val, $repalce_val) {
        if ($array_var == "") {
            return "";
        }
        $db = new Database();
        $array_var = explode(",", $array_var);
        // $new_array = new Array();
        $new_array = array();
        foreach ($array_var as $value) {
            $getDept = $db->query("SELECT department FROM tborgchartobjects WHERE id='" . $value . "'", "row");
            if ($getDept['department'] == $search_val) {
                array_push($new_array, $repalce_val);
            } else {
                array_push($new_array, $value);
            }
            // print_r($getDept);
        }
        return implode(",", $new_array);
    }

    public function getDashboardTheme($categoryID) {
        $db = new Database();
        $auth = Auth::getAuth('current_user');
        $getDashboardThem_object = $db->query("SELECT * FROM tbdashboad_theme WHERE category_id = '$categoryID' AND user_id = '" . $auth['id'] . "'", "array");
        return $getDashboardThem_object;
    }

    //added by roni
    public function getDashboardNavView($categoryID) {
        $db = new Database();
        $auth = Auth::getAuth('current_user');
        $getDashboardNavView_object = $db->query("SELECT * FROM tbdashboard_navview WHERE user_id = '" . $auth['id'] . "' LIMIT 0,1", "array");
        return $getDashboardNavView_object;
    }

    public function getFaqDetails($categoryID) {
        $db = new Database();
        $auth = Auth::getAuth('current_user');
        $getFaqDetails_object = $db->query("SELECT * FROM tbfaqdetails WHERE user_id = '" . $auth['id'] . "' LIMIT 0,1", "array"); //numrows
        return $getFaqDetails_object;
    }

    public static final function my_array_unique($array, $keep_key_assoc = false) {
        $duplicate_keys = array();
        $tmp = array();

        foreach ($array as $key => $val) {
            // convert objects to arrays, in_array() does not support objects
            if (is_object($val))
                $val = (array) $val;

            if (!in_array($val, $tmp))
                $tmp[] = $val;
            else
                $duplicate_keys[] = $key;
        }

        foreach ($duplicate_keys as $key)
            unset($array[$key]);

        return $keep_key_assoc ? $array : array_values($array);
    }

    public function getPinnedRequest($getID, $requestID) {
        $auth = Auth::getAuth('current_user');
        $getRequestPinned = $this->getDashboards(" WHERE UserID = {$auth['id']} AND ApplicationID = ''");
        $pinRequest = false;
        foreach ($getRequestPinned as $value) {
            $content = json_decode($value->content, true);
            foreach ($content as $value2) {
                if ($value2['formId'] == $getID && $value2['requestId'] == $requestID) {
                    $pinRequest = true;
                    break;
                }
            }
        }
        return $pinRequest;
    }

    public function asortSerializedArray($originalSerializedArray) {
        $sortedArray = explode(',', $originalSerializedArray);
        asort($sortedArray, SORT_NUMERIC);

        $sortedSerializedArray = '';

        foreach ($sortedArray as $key => $item) {
            $sortedSerializedArray = $sortedSerializedArray . $item . ',';
        }

        //  remove the last ,        
        return substr($sortedSerializedArray, 0, -1);
    }

    public function setPageForAdmin() {
        $auth = Auth::getAuth('current_user');
        if ($auth['user_level_id'] != 2) {
            header("location:/user_view/youre-not-allowed-to-visit-this-page");
        }
    }

    public function addslash_escape($str) {
        $str = str_replace("_", "\\_", $str);
        $str = str_replace("%", "\\%", $str);

        return $str;
    }

    public function removeSpecialChars($str) {
        $str = preg_replace('/[^A-Za-z0-9\.\s-]/', '', $str);
        $str = $this->cleanFieldWithoutSeparation($str);
        return $str;
    }

    public function cleanFieldWithoutSeparation($str) {
        if ($str == "TrackingNumber") {
            $str = "Tracking Number";
        } else if ($str == "DateCreated") {
            $str = "Date Created";
        }
        return $str;
    }

    public function empty_in_list($value, $empty_value, $type) {
        $ret = $value;
        if (trim($value) == "") {
            if ($type == "0") {
                $ret = $empty_value;
            } else {
                $ret = "<font color='#cccccc'>" . $empty_value . "</font>";
            }
        }

        return $ret;
    }

    public function setStatusButtonIcon($status) {
        return "fa fa-check-circle-o";

        if ($status == 0) {
            //to activate
            return "fa fa-check-circle-o";
        } else if ($status == 1) {
            //to deactivate
            return "fa fa-ban";
        } else {
            // others
            return "fa fa-question-circle";
        }
    }

//-----Added by Japhet Morada----------//
    public function getSingleDefaultAction($form_json, $requestID, $form_id, $type = '1', $action_name) {
        $d_action = $form_json['default-action-button'];
        $li = "";
        foreach ($d_action as $act) {
            if ($act['action'] == $action_name) {
                if ($requestID != '0') {
                    if ($type == "1") {
                        $li .= "<button class='default-action-btn notes btn-basicBtn fl-buttonEffect' data-id='$requestID' data-f-id='$form_id' data-type='1'  id='" . $act['action'] . "'><a>" . $act['action'] . "<span style='float:right; font-size:10px;'>" . $act['key'] . "</span></a>"; //" . $act['key'] . "
                    } else {
                        $li .= "<button class='default-action-btn notes btn-basicBtn fl-buttonEffect' data-id='$requestID' data-f-id='$form_id' data-type='1'  id='" . $act['action'] . "'><a> <i class='fa fa-pencil-square-o'></i></a>"; //" . $act['action'] . " 
                    }
                }
            }
        }
        return $li;
    }

//------------------------------------//

    public function getDefaultActionButtons($form_json, $requestID, $form_id, $type = "1", $not_include) {
        $search = new Search();
        if (ALLOW_DEFAULT_ACTIONS == "1") {
            $d_action = $form_json['default-action-button'];
            $li = "";
            // For Print 
            if ($_GET['print_form'] != "true" && !isset($_GET['print_form'])) {
                foreach ($d_action as $act) {
                    if ($act['action'] == "Notes") {
                        // if($act['action'] != $not_include){
                        if ($requestID != '0') {
                            if ($type == "1") {
                                $li .= "<li class='default-action-btn notes' data-id='$requestID'  data-f-id='$form_id' data-type='1'  id='" . $act['action'] . "'><a>" . $act['action'] . "<span style='float:right; font-size:10px;'>" . $act['key'] . "</span></a>";
                            } else {
                                // $li .= "<li class='default-action-btn notes btn-basicBtn fl-buttonEffect' data-id='$requestID' data-f-id='$form_id' data-type='1'  id='" . $act['action'] . "'><a> <i class='fa fa-pencil-square-o'></i></a>";
                            }
                        }
                        // }
                    } else if ($act['action'] == 'New') {
                        //echo '<script>alert("' . $enc_id . '")</script>';
                        //$enc_id = '';

                        if ($type == '1') {
                            $li .= "<li class='default-action-btn'  id='" . $act['action'] . "' data-id='$form_id'><a href= '/user_view/workspace?view_type=request&formID=$form_id&requestID=0'>" . $act['action'] . " <span style='float:right; font-size:10px;'>" . $act['key'] . "</span></a>";
                        } else {
                            $li .= "<li class='default-action-btn btn-basicBtn fl-buttonEffect tip' data-original-title='" . $act['action'] . "' id='" . $act['action'] . "' data-id='$form_id' style='margin-right: 8px;'><a href= '/user_view/workspace?view_type=request&formID=$form_id&requestID=0'> <i class='fa fa-file-o'></i></a>";
                        }
                    } else if ($act['action'] == 'Next') {
                        if ($requestID > 0) {
                            if ($type == '1') {
                                $li .= "<li class='default-action-btn' request-id='" . $requestID . "' form-id='" . $form_id . "'  id='" . $act['action'] . "' ><a href='#'>" . $act['action'] . " <span style='float:right; font-size:10px;'>" . $act['key'] . "</span></a>";
                            } else {
                                $li .= "<li class='default-action-btn btn-basicBtn fl-buttonEffect tip' request-id='" . $requestID . "' form-id='" . $form_id . "' data-original-title='" . $act['action'] . "' id='" . $act['action'] . "' style='margin-right: 8px;'><a href='#'> <i class='fa fa-caret-square-o-right'></i></a>";
                            }
                        }
                    } else if ($act['action'] == 'Previous') {
                        if ($requestID > 0) {
                            if ($type == '1') {
                                $li .= "<li class='default-action-btn' request-id='" . $requestID . "' form-id='" . $form_id . "'  id='" . $act['action'] . "' ><a href='#'>" . $act['action'] . " <span style='float:right; font-size:10px;'>" . $act['key'] . "</span></a>";
                            } else {
                                $li .= "<li class='default-action-btn btn-basicBtn fl-buttonEffect tip' request-id='" . $requestID . "' form-id='" . $form_id . "' data-original-title='" . $act['action'] . "' id='" . $act['action'] . "' style='margin-right: 8px;'><a href='#'> <i class='fa fa-caret-square-o-left'></i></a>";
                            }
                        }
                    } else if ($act['action'] == 'View') {
                        $enc_id = functions::base_encode_decode("encrypt", $form_id);
                        if ($type == '1') {
                            // $li .= "<li class='default-action-btn'  id='" . $act['action'] . "'><a href='/user_view/application?id=$enc_id'>" . $act['action'] . " <span style='float:right; font-size:10px;'>" . $act['key'] . "</span></a>";
                        } else {
                            $li .= "<li class='default-action-btn btn-basicBtn fl-buttonEffect tip' data-original-title='" . $act['action'] . "' id='" . $act['action'] . "' style='margin-right: 8px;'><a href='/user_view/application?id=$enc_id'><i class='fa fa-list-alt'></i></a>";
                        }
                    } else if ($act['action'] == 'Refresh') {
                        if ($type == '1') {
                            $li .= "<li class='default-action-btn'  id='" . $act['action'] . "'><a>" . $act['action'] . " <span style='float:right; font-size:10px;'>" . $act['key'] . "</span></a>";
                        } else {
                            $li .= "<li class='default-action-btn btn-basicBtn fl-buttonEffect tip' data-original-title='" . $act['action'] . "'  id='" . $act['action'] . "' style='margin-right: 8px;'><a> <i class='fa fa-refresh'></i></a>";
                        }
                    } else if ($act['action'] == 'Print') {
                        if ($type == '1') {
                            // $li .= "<li class='default-action-btn' data-original-title='" . $act['key'] . "' id='" . $act['action'] . "' style='margin-right: 8px;'><a>" . $act['action'] . " <span style='float:right; font-size:10px;'></span></a>";
                        } else {
                            // $li .= "<li class='default-action-btn btn-basicBtn fl-buttonEffect tip' data-original-title='" . $act['action'] . "' id='" . $act['action'] . "'><a> <i class='fa fa-print'></i>  </a>";
                        }
                    }
                }
            }
            //echo "<li class='default-action-btn' value='" . $act['action'] . "'><a>" . $act['action'] . "</a></li>";
        }
        return $li;
    }

    public function getRelatedInfo() {
        if (ALLOW_EXTERNAL_LINKS == "1") {

            $li = "";
            if ($act['action'] == "Links") {
                $li .= "<ul class='sublist'>";
                $form_link = $form_json['external_link'];
                //$link_list = "";
                $query_condition = " AND tbw.id IN(";
                $counter = 1;
                foreach ($form_link as $ext_link) {
                    $query_condition.= $ext_link['id'];
                    if ($counter < count($form_link)) {
                        $query_condition.=" , ";
                    }
                    $counter++;
                }
                $query_condition .= ")";

                $getModules = $search->getModules($query_condition);
                foreach ($getModules as $key_category => $value_category) {
                    $li .= "<li class='ext-btn' title='" . $key_category . "'><a>&nbsp;" . $key_category . "&nbsp;</a>";
                    $li .= "<ul class='sublist'>";
                    foreach ($value_category as $key => $value) {
                        $get_form_id = functions::base_encode_decode("encrypt", $value['form_id']);
                        if ($type == '1') {
                            $li .= "<li title='" . $value['form_name'] . "'><a class='ext-links' href='/user_view/application?id=" . $get_form_id . "' target='_blank'> " . $value['form_name'] . "</a></li>";
                        } else {
                            $li .= "<li title='" . $value['form_name'] . "' ><a class='ext-links' href='/user_view/application?id=" . $get_form_id . "' target='_blank'> " . $value['form_name'] . "</a> </li>";
                        }
                    }
                    $li .= "</ul>";
                    $li .= "</li>";
                }
                $li .= "</ul>";
            }
            $counter++;
        }
        $query_condition .= ")";
        return $query_condition;
    }

    public function setRelatedInfoConditions($form_link) {
        //$link_list = "";
        $query_condition = " AND tbw.id IN(";
        $counter = 1;
        foreach ($form_link as $ext_link) {

            $query_condition .= $ext_link['id'];
            if ($counter < count($form_link)) {
                $query_condition.=" , ";
            }
            $counter++;
        }
        $query_condition .= ")";
        return $query_condition;
    }

    public function getActionType($action_type) {
        $ret = "";
        if ($action_type == "1") {
            $ret = "Author";
        } else if ($action_type == "2") {
            $ret = "Viewer";
        } else if ($action_type == "3") {
            $ret = "User";
        } else if ($action_type == "4") {
            $ret = "Admin";
        }
        return $ret;
    }

    public function getUserType($action_type) {
        $ret = "";
        if ($action_type == "1") {
            $ret = "Positions";
        } else if ($action_type == "2") {
            $ret = "Departments";
        } else if ($action_type == "3") {
            $ret = "User";
        } else if ($action_type == "4") {
            $ret = "Groups";
        }
        return $ret;
    }

    //to be used by migration purpose
    public function syncFormUsers($db, $condition) {
        $getForms = $db->query("SELECT *,id as form_id FROM tb_workspace $condition", "array");
        $getAllRecordsToDelete = "DELETE FROM tbform_users WHERE ";
        $strReturn = "";
        foreach ($getForms as $key => $value) {
            $strReturn.= "Form: " . $value['form_name'];
            $strReturn.= "<br />";
            $strReturn.= "Category id: " . $value['category_id'];
            $strReturn.= "<br />";
            $getFormUsers = $db->query("SELECT * FROM tbform_users WHERE form_id = '" . $value['form_id'] . "'", "array");
            foreach ($getFormUsers as $key2 => $value2) {
                $strReturn.= "&nbsp;&nbsp;&nbsp; User: " . $value2['user'];
                $strReturn.= "<br />";
                $strReturn.= "&nbsp;&nbsp;&nbsp; User Type: " . $this->getUserType($value2['user_type']);
                $strReturn.= "<br />";
                $strReturn.= "&nbsp;&nbsp;&nbsp; Action Type: " . $this->getActionType($value2['action_type']);
                $strReturn.= "<br />";
                $access_type = "2"; //for users
                if ($value2['action_type'] == "4") {
                    $access_type = "1"; //for admin
                }
                $getCounterPartFormCategoryUsers = $db->query("SELECT * FROM tbform_category_users WHERE 
                            form_category_id ='" . $value['category_id'] . "' AND 
                            user_type = '" . $value2['user_type'] . "' AND 
                            user = '" . $value2['user'] . "' AND
                            access_type = '" . $access_type . "'
                            ", "array");
                if (count($getCounterPartFormCategoryUsers) == 0) {
                    $strReturn.= "Delete";
                    $getAllRecordsToDelete.= " (user_type = '" . $value2['user_type'] . "' AND user = '" . $value2['user'] . "' AND form_id = '" . $value2['form_id'] . "' AND action_type = '" . $value2['action_type'] . "') OR";
                    // $db->delete("tbform_users",array("user_type"=>$value['user_type'],"user"=>$value['user'],"form_id"=>$value['form_id']));
                } else {
                    // print_r($getCounterPartFormCategoryUsers);
                    $strReturn.= json_encode($getCounterPartFormCategoryUsers);
                }
                $strReturn.= "<br />";
                $strReturn.= "_______________________________";
                $strReturn.= "<br />";
            }
        }
        $getAllRecordsToDelete = substr($getAllRecordsToDelete, 0, strlen($getAllRecordsToDelete) - 3);
        // echo $getAllRecordsToDelete;
        $db->create($getAllRecordsToDelete);
        return $strReturn;
    }

    //new sync users to be used by form category updating
    public function syncFormUsersV2($db, $condition, $jsonUnChecked, $action_type) {
        $getForms = $db->query("SELECT *,id as form_id FROM tb_workspace $condition", "array");
        foreach ($getForms as $key1 => $form) {

            foreach ($jsonUnChecked as $key2 => $valueUnChecked) {
                $user_type = functions::setUserType($key2);

                //delete
                foreach ($valueUnChecked as $key => $user) {
                    $deleteFormUser = "DELETE FROM tbform_users 
                    WHERE 
                    form_id = '" . $form['form_id'] . "' 
                    AND 
                    user = '" . $user . "' 
                    AND 
                    user_type = '" . $user_type . "' ";

                    //check if for admin
                    if ($action_type == 4) {
                        $deleteFormUser .= "AND action_type = 4";
                    } else if ($action_type == 0) {
                        $deleteFormUser .= "AND action_type != 4";
                    }
                    //delete
                    $db->create($deleteFormUser);

                    //if not admin
                    if ($action_type == 0) {
                        // parent is form users
                        //delete report

                        $deleteReportUser = "DELETE tbru FROM 
                        tbreport tbr 
                        LEFT JOIN 
                        tbreport_users tbru 
                        ON 
                        tbr.id = tbru.report_id 
                        WHERE 
                        form_id = '" . $form['form_id'] . "' and tbru.user = '" . $user . "'";
                        $db->create($deleteReportUser);
                    }
                }
            }
        }
    }

    public function setUserType($key) {
        $user_type = 0;
        // if ($access_type != "4") {
        if ($key == "positions") {
            $user_type = 1;
        }
        if ($key == "departments") {
            $user_type = 2;
        }
        if ($key == "groups") {
            $user_type = 4;
        }
        // }
        if ($key == "users") {
            $user_type = 3;
        }
        return $user_type;
    }

    public function pushFieldColumns($array) {
        $ret = array();
        foreach ($array as $key => $value) {
            array_push($ret, strToLower(trim($value['Field'])));
        }
        return $ret;
    }

    public function deleteFormUsers($db, $formId, $type = "all") {
        if ($type == "all") {
            //author
            $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 1));
            //viewer
            $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 2));
            //users
            $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 3));
            //admin
            $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 4));
            //customize print author
            $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 5));
        } else {
            foreach ($type as $value) {
                $db->delete("tbform_users", array("form_id" => $formId, "action_type" => $value));
            }
        }
    }

    public function delete_request_memcache() {
        $redis_cache = getRedisConnection();


        if ($redis_cache) {
            $redis_cache->del("request_list");
            $redis_cache->del("picklist");
            $redis_cache->del("request_list_count");
            $redis_cache->del("calendar_view");
        }
    }

    //Delete Memcache
    public function deleteMemcacheKeys($arr) {
        $redis_cache = getRedisConnection();
        //delete memcached
        if ($redis_cache) {
            foreach ($arr as $key => $value) {
                //check if key exists
                $cache_key = $redis_cache->get($value);
                if ($cache_key) {
                    $redis_cache->del($value);
                }
            }
        }
    }

    public function deleteMemcacheForm($arr, $form_id) {
        $redis_cache = getRedisConnection();

        //delete memcached
        if ($redis_cache) {
            foreach ($arr as $key => $value) {
                $cache_request_list = json_decode($redis_cache->get($value), true);

                // echo "SET<br />";
                // print_r($cache_request_list);
                // echo $value."<br />";
                unset($cache_request_list['form_id_' . $form_id]);

                // echo "<br />Unset: <br /><br /><br />";
                // print_r($cache_request_list);
                $cache_request_list = $redis_cache->set($value, json_encode($cache_request_list));
            }
        }
    }

    public static final function clearRequestRelatedCache($key, $sub_key) {
        $redis_cache = getRedisConnection();

        if ($redis_cache) {
            $cache_result = json_decode($redis_cache->get($key), true);
            if ($cache_result) {
                unset($cache_result[$sub_key]);
                $redis_cache->set($key, json_encode($cache_result));
            }
        }
    }

    public static final function clearFormulaLookupCaches($sub_key, $arr = array()) {
        $redis_cache = getRedisConnection();
        if ($redis_cache) {
            foreach ($arr as $key) {
                $cache_result = json_decode($redis_cache->get($key), true);
                if ($cache_result) {
                    unset($cache_result[$sub_key]);
                    $redis_cache->set($key, json_encode($cache_result));
                }
            }
        }
    }

}

function IsPublicFormaPageHTTPReferer(){
    $public_iframe_access_rights = false;
    if($_SERVER['HTTP_REFERER']){
        $iframe_parent_url_data = parse_url( $_SERVER['HTTP_REFERER'] );
        $application_main_page_url_data = parse_url(MAIN_PAGE);
        $current_actual_url_data = parse_url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
        // var_dump($current_actual_url_data);
        // var_dump($_SERVER);
        // var_dump($iframe_parent_url_data);
        if($iframe_parent_url_data['host'] != $application_main_page['host']){
            $public_iframe_access_rights = true;
            if($current_actual_url_data['path'] == ''){

            }
        }
    }
    return $public_iframe_access_rights;
}
function IsPublicFormaPage(){
    $public_forma_access_rights = false;
    if($_GET['public_forma_page'] == '1'){
        $public_forma_access_rights = true;
    }
    return $public_forma_access_rights;
}

function var_debug($variable, $strlen = PHP_INT_MAX, $width = 25, $depth = 10, $i = 0, &$objects = array()) {
    $search = array("\0", "\a", "\b", "\f", "\n", "\r", "\t", "\v", '');
    $replace = array('\0', '\a', '\b', '\f', '<br/>', '\r', '\t', '\v', '');
    $variable_gettype = gettype($variable);
    $variable_length = '';

    if ($variable_gettype == 'string') {
        $string = $variable_gettype . ' ';
        $variable_length = '<i>(length=' . strlen($variable) . ')</i>';
    } elseif ($variable) {
        $string = $variable_gettype . ' ';
        $variable_length = '<i>(size=' . count($variable) . ')</i>';
        $string = $string . $variable_length . ' ';
    }
    switch (gettype($variable)) {
        case 'boolean': $string.= $variable ? 'true' : 'false';
            break;
        case 'integer': $string.= $variable;
            break;
        case 'double': $string.= $variable;
            break;
        case 'resource': $string.= '[resource]';
            break;
        case 'NULL': $string.= "null";
            break;
        case 'unknown type': $string.= '???';
            break;
        case 'string':
            $len = strlen($variable);
            $variable = str_replace($search, $replace, substr($variable, 0, $strlen), $count);
            $variable = substr($variable, 0, $strlen);
            if ($len < $strlen)
                $string.= '"' . $variable . '"';
            else
                $string.= 'string(' . $len . '): "' . $variable . '"...';
            break;
        case 'array':
            $len = count($variable);
            if ($i == $depth)
                $string.= 'array(' . $len . ') {...}';
            elseif (!$len)
                $string.= 'array(0) {}';
            else {
                $keys = array_keys($variable);
                $spaces = str_repeat(' ', $i * 2);
                $string.= "array($len)\n" . $spaces . '{';
                $count = 0;
                foreach ($keys as $key) {
                    if ($count == $width) {
                        $string.= "\n" . $spaces . "  ...";
                        break;
                    }
                    $string.= "\n" . $spaces . "  [$key] => ";
                    $string.= var_debug($variable[$key], $strlen, $width, $depth, $i + 1, $objects);
                    $count++;
                }
                $string.="\n" . $spaces . '}';
            }
            break;
        case 'object':
            $id = array_search($variable, $objects, true);
            if ($id !== false)
                $string.=get_class($variable) . '#' . ($id + 1) . ' {...}';
            else if ($i == $depth)
                $string.=get_class($variable) . ' {...}';
            else {
                $id = array_push($objects, $variable);
                $array = (array) $variable;
                $spaces = str_repeat(' ', $i * 2);
                $string.= get_class($variable) . "#$id\n" . $spaces . '{';
                $properties = array_keys($array);
                foreach ($properties as $property) {
                    $name = str_replace("\0", ':', trim($property));
                    $string.= "\n" . $spaces . "  [$name] => ";
                    $string.= var_debug($array[$property], $strlen, $width, $depth, $i + 1, $objects);
                }
                $string.= "\n" . $spaces . '}';
            }
            break;
    }
    if ($i > 0)
        return $string;
    $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
    do
        $caller = array_shift($backtrace);
    while ($caller && !isset($caller['file']));
    // if ($caller) $string = $caller['file'].':'.$caller['line']."\n".$string;
    if ($variable_gettype == 'string')
        $string = $string . ' ' . $variable_length;
    echo $string . '<br/>';
}

function setAdminViewer($result) {
    $return = array();
    $form_admin;
    $auth = Auth::getAuth('current_user');
    foreach ($result as $value) {
        $flag = 0;
        if ($value['form_admin'] == "") {
            array_push($return, $value);
            $flag = 1;
        } else if ($value['form_admin'] == "0") {
            if ($value['created_by'] == $auth['id'] && $flag == 0) {
                array_push($return, $value);
                $flag = 1;
            }
        } else {
            $form_admin = json_decode($value['form_admin'], true);
            // foreach ($form_admin['departments'] as $value_position) {
            //     if ($value_position == $auth['department_id'] && $flag == 0) {
            //         array_push($return, $value);
            //         $flag = 1;
            //     }
            // }
            // foreach ($form_admin['positions'] as $value_position) {
            //     if ($value_position == $auth['position'] && $flag == 0) {
            //         array_push($return, $value);
            //         $flag = 1;
            //     }
            // }
            foreach ($form_admin['users'] as $value_position) {
                if ($value_position == $auth['id'] && $flag == 0) {
                    array_push($return, $value);
                    $flag = 1;
                }
            }
        }
        if ($value['created_by'] == $auth['id'] && $flag == 0) {
            array_push($return, $value);
            $flag = 1;
        }
    }
    return json_encode($return);
}

/*
  $db = database class
  $array = data to insert
  $data = data of form
  $access_type = user or admin
  $insert_type = 1 for form and 2 form form_category
  $action_type = 1 = author, 2 = view, 3 = user, 4 = admin
  setUsersMigration($db,$array,$data,$access_type,$insert_type,$action_type)
 */

function setUsersMigration($db, $array, $data, $access_type, $insert_type, $action_type) {
    foreach ($array as $key => $value) {
        $user_type = 0;
        if ($access_type != "4") {
            if ($key == "positions") {
                $user_type = 1;
            }
            if ($key == "departments") {
                $user_type = 2;
            }
            if ($key == "groups") {
                $user_type = 4;
            }
        }
        if ($key == "users") {
            $user_type = 3;
        }

        if ($user_type != 0) {
            if ($insert_type == 1) {
                insertFormUsers($db, $value, $data, $access_type, $user_type, $action_type);
            } else if ($insert_type == 2) {
                insertFormCategoryUsers($db, $value, $data, $access_type, $user_type);
            }
        }
    }
}

//for form category
function insertFormCategoryUsers($db, $array, $data, $access_type, $user_type) {
    foreach ($array as $value) {

        //for admin
        // if ($access_type == "1") {
        //     $getFormCategoryAdmin = $db->query("SELECT * FROM tbform_category_users WHERE user = $value AND form_category_id ='" . $data['id'] . "' AND access_type = 1", "numrows");
        //     if ($getFormCategoryAdmin > 0) {
        //         continue;
        //     }
        // }
        if ($value != "") {
            $user = $value;

            $checkIfExist = $db->query("SELECT * FROM tbform_category_users WHERE user = '" . $user . "' AND form_category_id ='" . $data['id'] . "' AND access_type = '" . $access_type . "' AND user_type = '" . $user_type . "'", "numrows");
            if ($checkIfExist > 0) {
                continue;
            }

            $insert = array("form_category_id" => $data['id'],
                "user" => $user,
                "user_type" => $user_type,
                "access_type" => $access_type
            );
            $db->insert("tbform_category_users", $insert);
        }
    }
}

//for forms
function insertFormUsers($db, $array, $data, $access_type, $user_type, $action_type) {

    foreach ($array as $value) {
        //set user value
        $user = $value;
        $getFormUsers = $db->query("SELECT * FROM tbform_users WHERE form_id = '" . $data['id'] . "' 
            AND user = '$user' AND user_type = '$user_type' AND action_type = '$action_type'", "numrows");
        if ($getFormUsers > 0) {
            continue;
        }
        //insert in junction table
        if ($user != "") {
            $insert = array("form_id" => $data['id'],
                "user" => $user,
                "user_type" => $user_type,
                "action_type" => $action_type
            );
            $db->insert("tbform_users", $insert);
        }
    }
}

function setDepartmentsNodeIdQuery($value) {
    $auth = Auth::getAuth('current_user');

    return "SELECT tboo.object_id FROM tborgchart tbo LEFT JOIN tborgchartobjects tboo on tbo.id = tboo.orgchart_id 
                            WHERE tbo.status = 1 AND tbo.is_delete = 0 AND (tboo.id = '" . $value . "' OR tboo.object_id = '" . $value . "') AND tbo.company_id = '" . $auth['company_id'] . "'";
}

function compressToOldJsonFormat($data) {
    $array = array();
    foreach ($data as $value) {
        if ($value['user_type'] == "1") { //position
            $array['positions'][] = $value['user'];
        } else if ($value['user_type'] == "2") { // department
            $array['departments'][] = $value['user'];
        } else if ($value['user_type'] == "3") { // user
            $array['users'][] = $value['user'];
        } else if ($value['user_type'] == "4") { // group
            $array['groups'][] = $value['user'];
        }
    }
    return json_encode($array);
}

function getRequestDetails($conn, $formID, $requestID, $auth, $user_id, $tblname) {
    $redis_cache = getRedisConnection();
    if ($redis_cache) {
        $cache_request_details = json_decode($redis_cache->get("request_details_" . $formID), true);
    }

    if ($cache_request_details[$requestID][$user_id]) {
        $requestDetails = $cache_request_details[$requestID][$user_id];
    } else {
        $str_sql = "SELECT FORM.*, FORM.Processor AS ProcessorID, 
    REQUESTOR.display_name as Requestor_Fullname, REQUESTOR.display_name as Requestor_Name, 
        CASE FORM.ProcessorType
            WHEN 1 THEN 
                (SELECT user_processor.display_name FROM tbuser user_processor WHERE user_processor.id = getDepartmentUsers(FORM.Processor, FORM.ProcessorLevel, {$auth["company_id"]}))
            WHEN 2 THEN 
                (SELECT Position FROM tbpositions WHERE ID=FORM.Processor)
            WHEN 5 THEN 
                (SELECT GROUP_CONCAT(proc_user.Display_Name) FROM tbuser proc_user WHERE FIND_IN_SET(proc_user.ID, FORM.Processor))
            WHEN 6 THEN 
                (SELECT group_name FROM tbform_groups WHERE ID=FORM.Processor)
            ELSE 
                PROCESSOR.display_name
        END AS Processor_Name, 
        CASE FORM.ProcessorType
            WHEN 1 THEN CONCAT(@department_users:=getDepartmentUsers(FORM.Processor, FORM.ProcessorLevel, {$auth["company_id"]}),',' ,
			IF(FORM.enable_delegate = 1,IFNULL(getDelegate(@department_users),''),'')
                    ) 
            WHEN 2 THEN CONCAT(@position_users:=getUsersByPosition(FORM.Processor),',' ,
                        IF(FORM.enable_delegate = 1,IFNULL(getDelegate(@position_users),''),'')
                    ) 
            WHEN 6 THEN CONCAT(@group_users:=getUsersByGroup(FORM.Processor),',' ,
                        IF(FORM.enable_delegate = 1,IFNULL(getDelegate(@group_users),''),'')
                    )
            ELSE
                    CONCAT(FORM.Processor,',' ,
                        IF(FORM.enable_delegate = 1,IFNULL(getDelegate(FORM.Processor),''),'')
                    )
        END AS Processor,
        CASE WHEN FORM.enable_delegate = 1 THEN 
            CASE FORM.ProcessorType
            WHEN 1 THEN (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(getDepartmentUsers(FORM.Processor, FORM.ProcessorLevel, {$auth["company_id"]}))) ORDER BY user_delegate.display_name)
            WHEN 2 THEN (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(getUsersByPosition(FORM.Processor))) ORDER BY user_delegate.display_name)
            WHEN 6 THEN (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(getUsersByGroup(FORM.Processor))) ORDER BY user_delegate.display_name)            
            ELSE
                    (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(FORM.Processor)) ORDER BY user_delegate.display_name)
        END
        ELSE
            ''
        END AS Delegate_Name,
        CASE 
            WHEN FORM.Editor IS NULL OR  FORM.Editor = '' THEN getRequestUsers({$formID}, FORM.ID, 1)
            ELSE
                ''
	END AS Editor,
    FIND_IN_SET({$user_id}, getFormUsers({$formID},6)) as delete_access_forma FROM " . $tblname . " FORM 
        LEFT JOIN tbuser REQUESTOR
        ON REQUESTOR.id = FORM.Requestor
        LEFT JOIN tbuser PROCESSOR
        ON PROCESSOR.id = FORM.Processor
        WHERE FORM.id = {$conn->escape($requestID)}";

        $requestDetails = $conn->query($str_sql);

        if ($requestDetails[0]["SaveFormula"] != "") {
            $formulaDoc = new Formula($requestDetails[0]["SaveFormula"]);
            $formulaDoc->DataFormSource[0] = $requestDetails[0];
            $requestDetails[0]["SaveFormula"] = $formulaDoc->evaluate();
        }


        if ($requestDetails[0]["CancelFormula"] != "") {
            $formulaDoc = new Formula($requestDetails[0]["CancelFormula"]);
            $formulaDoc->DataFormSource[0] = $requestDetails[0];
            $requestDetails[0]["CancelFormula"] = $formulaDoc->evaluate();
        }

        if ($redis_cache) {
            if (!$cache_request_details) {
                $cache_request_details = array();
            }
            $cache_request_details[$requestID][$user_id] = $requestDetails;
            $redis_cache->set("request_details_" . $formID, json_encode($cache_request_details));
        }
    }



    return $requestDetails;
}

function backgroundProcessRequestWF($form_id, $request_id) {
    $auth = Auth::getAuth('current_user');
    $now = new DateTime();
    $php_path = getPHPExecutableFromPath();
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $command = 'start /B php ' . APPLICATION_PATH . '\scheduled_task\process_workflow.php -f ' . $form_id . ' -r ' . $request_id . ' -u ' . $auth["id"] . ' > ' . APPLICATION_PATH . '\logs\workflow_logs\wf_log_' . $now->getTimestamp() . '_' . $form_id . '_' . $request_id . '.txt 2>&1';
        $handle = popen($command, 'r');
        pclose($handle);
    } else {
        $command = $php_path . ' ' . APPLICATION_PATH . '/scheduled_task/process_workflow.php -f ' . $form_id . ' -r ' . $request_id . ' -u ' . $auth["id"] . ' 2>&1';
        exec($command);
    }
}

function backgroundProcessImportV2($form_id, $import_option, $import_update_reference_field, $tmpname, $action, $embed_ref_key = "", $embed_ref_val = "", $embed_submission = "") {
    $auth = Auth::getAuth('current_user');
    $now = new DateTime();
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $command = 'start /B php ' . APPLICATION_PATH . '\scheduled_task\process_import.php -f ' . $form_id . ' -o ' . $import_option . ' -r' . $import_update_reference_field . ' -c ' . $tmpname . ' -a ' . $action . ' -u ' . $auth["id"] . ' -k ' . $embed_ref_key . ' -v ' . $embed_ref_val . ' -s ' . $embed_submission . '  > ' . APPLICATION_PATH . '\logs\import_logs\import_log_' . $now->getTimestamp() . '_' . $form_id . '.txt 2>&1';
        $handle = popen($command, 'r');
        pclose($handle);
    } else {
        $command = '/opt/lampp/bin/php ' . APPLICATION_PATH . '\scheduled_task\process_import.php -f ' . $form_id . ' -o ' . $import_option . ' -r' . $import_update_reference_field . ' -c ' . $tmpname . ' -a ' . $action . ' -u ' . $auth["id"] . ' -k ' . $embed_ref_key . ' -v ' . $embed_ref_val . ' -s ' . $embed_submission . ' 2>&1';
        exec($command);
    }
}

function backgroundProcessImport($form_id, $import_option, $import_update_reference_field, $tmpname, $action, $update_option, $import_update_add_value_field) {
    $auth = Auth::getAuth('current_user');
    $now = new DateTime();
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        $command = 'start /B php ' . APPLICATION_PATH . '\scheduled_task\process_import.php -f ' . $form_id . ' -o ' . $import_option . ' -r' . $import_update_reference_field . ' -c ' . $tmpname . ' -a ' . $action . ' -m ' . $update_option . ' -n ' . $import_update_add_value_field . ' -u ' . $auth["id"] . '  > ' . APPLICATION_PATH . '\logs\import_logs\import_log_' . $now->getTimestamp() . '_' . $form_id . '.txt 2>&1';
        $handle = popen($command, 'r');
        pclose($handle);
    } else {
        $command = '/opt/lampp/bin/php ' . APPLICATION_PATH . '\scheduled_task\process_import.php -f ' . $form_id . ' -o ' . $import_option . ' -r' . $import_update_reference_field . ' -c ' . $tmpname . ' -a ' . $action . ' -m ' . $update_option . ' -n ' . $import_update_add_value_field . ' -u ' . $auth["id"] . ' 2>&1';
        exec($command);
    }
}

function getPHPExecutableFromPath() {
    $paths = explode(PATH_SEPARATOR, getenv('PATH'));
    foreach ($paths as $path) {
        // we need this for XAMPP (Windows)
        if (strstr($path, 'php.exe') && isset($_SERVER["WINDIR"]) && file_exists($path) && is_file($path)) {
            return $path;
        } else {
            $php_executable = $path . DIRECTORY_SEPARATOR . "php" . (isset($_SERVER["WINDIR"]) ? ".exe" : "");
            if (file_exists($php_executable) && is_file($php_executable)) {
                return $php_executable;
            }
        }
    }
    return FALSE; // not found
}

function getExternalDatabase($conn, $additional_query) {
    $query = "SELECT connection_name, db_type, db_username, db_password, db_hostname, db_name "
            . "FROM tbexternal_database" . $additional_query;

    $result = $conn->query($query);

    return $result;
}

function createQueryLog($message) {
//    $fs = new functions;
//    $date = $fs->currentDateTime();
//    $now = date("Y-m-d", strtotime($date));
//    error_log($date . " - " . $message . "\r\n", 3, APPLICATION_PATH . "/logs/sql_logs/sql_logs_" . $now . ".txt");
}

function createErrorLog($message) {
    $fs = new functions;
    $date = $fs->currentDateTime();
    error_log($date . " - " . $message . "\r\n  ", 3, APPLICATION_PATH . " / logs/sql_error_logs.txt");
}

function createEmailLog($message) {
    $fs = new functions;
    $date = $fs->currentDateTime();
    $now = date("Y-m-d", strtotime($date));
    error_log($date . " - " . $message . "\r\n", 3, APPLICATION_PATH . "/logs/email_logs/email_logs_" . $now . ".txt");
}

function getCurrentDatabaseConnection() {
    $conn = new MySQLDatabase(DB_HOSTNAME, DB_NAME, DB_USERNAME, DB_PASSWORD);
    return $conn;
}

//CLEAN COPY
function SetArrayAddressKey(&$array, $addressing, $new_value, $key_address = array()) { //limitation ... cannot set when address contains array
    //version 3
    foreach ($array as $key => $value) {
        if (gettype($array[$key]) == "array") {
            array_push($key_address, $key);
            $array[$key] = SetArrayAddressKey($array[$key], $addressing, $new_value, $key_address);
            unset($key_address[count($key_address) - 1]);
            $key_address = array_values($key_address);
        } else {
            array_push($key_address, $key);
            $param_address = implode(">>", $addressing);
            $recur_address = implode(">>", $key_address);
            if ($param_address == $recur_address) {
                $array[$key] = $new_value;
                unset($key_address[count($key_address) - 1]);
                array_values($key_address);
                return $array;
            } else {
                unset($key_address[count($key_address) - 1]);
                $key_address = array_values($key_address);
            }
        }
    }
    return $array;
    //SAMPLE USAGE
    // $array = array(    "test"=>array(        "test1"=>array(            "test2"=>"ito na",            "test3"=>"ito na"        ),        "test0"=>"hindi ito"    ),    "testZAP"=>array(        "test1"=>array(            "test2"=>"ito na"        ),        "test0"=>"hindi ito"    ));
    // $address = array("test","test1","test3");
    // SetArrayAddressKey( $array , $address , "NEW VALUE");
    // $address = array("test","test0");
    // SetArrayAddressKey( $array , $address , "NEW VALUE");
    // var_dump( $array );
}

;

function getMemCacheConnection() {

    if (ENABLE_MEMCACHED === "1") {
        $mem_cache = new Memcache;
        $mem_cache->addserver(MEMCACHED_SERVER_IP);

        return $mem_cache;
    }
}

function getRedisConnection() {
    if (ENABLE_MEMCACHED === "1") {
        try {
            $redis_client = new Predis\Client(array(
                "host" => MEMCACHED_SERVER_IP
            ));

            $redis_client->connect();
            return $redis_client;
        } catch (Predis\Connection\ConnectionException $ex) {
            return null;
        }
    }
}

function getRedisArrayValue($redis_cache, $key) {
    return json_decode($redis_cache->get($key), true);
}

function setRedisArrayValue($redis_cache, $key, $array_value) {
    $redis_cache->set($key, json_encode($array_value));
}

function getRedisData($redis_cache, $key, $str_sql) {
    
}

function getFormWorkspaceData($conn, $form_id) {
    $redis_cache = getRedisConnection();
    if ($redis_cache) {
        $workspace_cache = json_decode($redis_cache->get("workspace_form_details_" . $form_id), true);
    }

    if ($workspace_cache) {
        $form_data = $workspace_cache;
    } else {
        $str = "SELECT WS.*, WFO.workflow_id, WFO.buttonStatus,WFO.fieldEnabled, WFO.fieldRequired, WFO.fieldHiddenValue, G.id as generate_id, WFO.json as wfo_json FROM tb_workspace WS 
                           LEFT JOIN tbworkflow WF
                            ON WS.Id = WF.form_id 
                           LEFT JOIN tbworkflow_objects WFO
                            ON WFO.workflow_id = WF.id 
                            LEFT JOIN tb_generate G
                            ON G.form_id = WS.Id
                            WHERE WS.id={$conn->escape($form_id)} AND WFO.type_rel = '1'  AND WF.Is_Active = '1' AND WS.is_delete = 0 AND WF.is_delete = 0 ";

        $form_data = $conn->query($str, "row");

        if ($redis_cache) {
            $redis_cache->set("workspace_form_details_" . $form_id, json_encode($form_data));
        }
    }
    // $form_data['form_content'] = utf8_encode($form_data['form_content']); //for special characters like copyright etc..
    return $form_data;
}

?>