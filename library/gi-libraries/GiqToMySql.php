<?php

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

class GiqToMySql
{
	public function __construct(){
	}

	public function giqsToSqlRollup($giqs){
		$giqsSql = [];
		for($i=0; $i<count($giqs); $i++){
			$giqsSql[] = $this->giqToSqlRollup($giqs[$i]);
		}
		return $giqsSql;
	}
	
	//GIQ to SQL, includes basic areas: aggregates, groupings, optional groupings and filters
	public function giqToSqlBasic($giq){
		$sql = "select ";
		//fields and aggregates
		$fieldsAggsSql = [];
		if(isset($giq["groupings"])){
			for($i=0;$i<count($giq["groupings"]); $i++){
				$field = $giq["groupings"][$i];
				$fieldsAggsSql[] = $this->expToSql($field["dispExp"]) . " as `" . $field["alias"] . "`";
			}
		}
		if(isset($giq["optionalGroupings"])){
			for($i=0;$i<count($giq["optionalGroupings"]); $i++){
				$field = $giq["optionalGroupings"][$i];
				$fieldsAggsSql[] = $this->expToSql($field["dispExp"]) . " as `" . $field["alias"] . "`";
			}
		}
		if(isset($giq["aggregates"])){
			for($i=0;$i<count($giq["aggregates"]); $i++){
				$agg = $giq["aggregates"][$i];
				$fieldsAggsSql[] = $this->expToSql($agg["exp"]) . " as `" . $agg["alias"] . "`";
			}
		}
		$sql .= implode(", ", $fieldsAggsSql) . " ";
		
		//tables
		$tablesSql = [];
		if(isset($giq["entities"])){
			for($i=0;$i<count($giq["entities"]); $i++){
				$ent = $giq["entities"][$i];
				$tablesSql[] = "`". $ent["entityName"] . "` as `" . $ent["alias"] . "`";
			}
		}
		if(count($tablesSql) > 0){
			$sql .= "from " . implode(", ", $tablesSql) . " ";
		}
		
		//relationships and filters
		$filtersSql = [];
		if(isset($giq["filters"])){
			for($i=0;$i<count($giq["filters"]); $i++){
				$filter = $giq["filters"][$i];
				$filtersSql[] = $this->expToSql($filter["exp"]);
			}
		}
		
		if(isset($giq["entities"])){
			for($i=0;$i<count($giq["entities"]); $i++){
				$ent = $giq["entities"][$i];
				if(isset($ent["relationships"])){
					for($j=0; $j<count($ent["relationships"]); $j++){
						$rel = $ent["relationships"][$j];
						$filtersSql[] = 
							"`". $ent["alias"] . "`.`" . $rel["attributeName"] . "`" . " = " . 
							"`". $rel["refEntityAlias"] . "`.`" . $rel["refAttributeName"] . "`";
					}
				}
			}
		}
		
		if(count($filtersSql) > 0){
			$sql .= "where " . implode(" and ", $filtersSql) . " ";
		}
		
		//groupings
		$groupingsSql = [];
		if(isset($giq["groupings"])){
			for($i=0;$i<count($giq["groupings"]); $i++){
				$grp = $giq["groupings"][$i];
				$groupingsSql[] = $this->expToSql($grp["groupExp"]);
			}
		}
		if(isset($giq["optionalGroupings"])){
			for($i=0;$i<count($giq["optionalGroupings"]); $i++){
				$grp = $giq["optionalGroupings"][$i];
				$groupingsSql[] = $this->expToSql($grp["groupExp"]);
			}
		}
		if(count($groupingsSql) > 0){
			$sql .= "group by " . implode(", ", $groupingsSql) . " ";
		}
		
		return $sql;
	}
	
	//translates Expression Tree to SQL
	public function expToSql($exp){
		$sqlExp = " ";
		for($i=0; $i<count($exp); $i++){
			$node = $exp[$i];
			if($node["type"] == "operator"){
				$sqlExp .= $node["displayText"] . " ";
			}elseif($node["type"] == "string"){
				$sqlExp .= $node["value"] . " ";
			}elseif($node["type"] == "number"){
				$sqlExp .= $node["value"] . " ";
			}elseif($node["type"] == "object"){
				$sqlExp .= "`" . implode("`,`", $node["namespaces"]) . "`.`". $node["objectName"] . "` ";
			}elseif($node["type"] == "null"){
				$sqlExp .= $node["displayText"] . " ";
			}elseif($node["type"] == "grouping"){
				$sqlExp .= "(". $this->expToSql($node["content"]) .") ";
			}elseif($node["type"] == "function"){
	
				$paramSql = [];
 				if(isset($node["parameters"])){
					for($j=0; $j<count($node["parameters"]); $j++){
						$param = $node["parameters"][$j];
						$paramSql[] = $this->expToSql($param);
					}
				}
				$sqlExp .=  
					$node["displayText"] . "(" . implode(",", $paramSql) . ") ";
			
			}
		}
		return $sqlExp;
	}
	
	//translates GIQ into optimized SQL count by removing aggregates
	public function giqToSqlCount($giq){
		$sql = "select count(*) as row_count from (select ";
		//fields and aggregates
		$fieldsAggsSql = [];
		if(isset($giq["groupings"])){
			for($i=0;$i<count($giq["groupings"]); $i++){
				$field = $giq["groupings"][$i];
				$fieldsAggsSql[] = $this->expToSql($field["dispExp"]) . " as `" . $field["alias"] . "`";
			}
		}
		if(isset($giq["optionalGroupings"])){
			for($i=0;$i<count($giq["optionalGroupings"]); $i++){
				$field = $giq["optionalGroupings"][$i];
				$fieldsAggsSql[] = $this->expToSql($field["dispExp"]) . " as `" . $field["alias"] . "`";
			}
		}
		
		$sql .= implode(", ", $fieldsAggsSql) . " ";
		
		//tables
		$tablesSql = [];
		if(isset($giq["entities"])){
			for($i=0;$i<count($giq["entities"]); $i++){
				$ent = $giq["entities"][$i];
				$tablesSql[] = "`". $ent["entityName"] . "` as `" . $ent["alias"] . "`";
			}
		}
		if(count($tablesSql) > 0){
			$sql .= "from " . implode(", ", $tablesSql) . " ";
		}
		
		//relationships and filters
		$filtersSql = [];
		if(isset($giq["filters"])){
			for($i=0;$i<count($giq["filters"]); $i++){
				$filter = $giq["filters"][$i];
				$filtersSql[] = $this->expToSql($filter["exp"]);
			}
		}
		
		if(isset($giq["entities"])){
			for($i=0;$i<count($giq["entities"]); $i++){
				$ent = $giq["entities"][$i];
				if(isset($ent["relationships"])){
					for($j=0; $j<count($ent["relationships"]); $j++){
						$rel = $ent["relationships"][$j];
						$filtersSql[] = 
							"`". $ent["alias"] . "`.`" . $rel["attributeName"] . "`" . " = " . 
							"`". $rel["refEntityAlias"] . "`.`" . $rel["refAttributeName"] . "`";
					}
				}
			}
		}
		
		if(count($filtersSql) > 0){
			$sql .= "where " . implode(" and ", $filtersSql) . " ";
		}
		
		//groupings
		$groupingsSql = [];
		if(isset($giq["groupings"])){
			for($i=0;$i<count($giq["groupings"]); $i++){
				$grp = $giq["groupings"][$i];
				$groupingsSql[] = $this->expToSql($grp["groupExp"]);
			}
		}
		if(isset($giq["optionalGroupings"])){
			for($i=0;$i<count($giq["optionalGroupings"]); $i++){
				$grp = $giq["optionalGroupings"][$i];
				$groupingsSql[] = $this->expToSql($grp["groupExp"]);
			}
		}
		if(count($groupingsSql) > 0){
			$sql .= "group by " . implode(", ", $groupingsSql) . " ";
			$sql .= ") as count_table";
		}
		
		return $sql;
	}
	
	//translates
	public function giqToSqlPage($giq){
		return $this->giqToSqlBasic($giq) . " limit " . (($giq["page"] - 1) * $giq["recordsPerPage"]) . " ," .  $giq["recordsPerPage"] . ";";
	}
	
	public function giqToSqlRollup($giq){
		$sql = $this->giqToSqlBasic($giq);
		$sql .= " with rollup ";
		return $sql;
	}
}