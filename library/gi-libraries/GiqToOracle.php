<?php

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

class GiqToOracle
{
	public function __construct(){
	}
	
	public function isNumericType($sqlType){
		return in_array($sqlType, 
			["bigint",
			"numeric",
			"bit",
			"smallint",
			"decimal",
			"smallmoney",
			"int",
			"tinyint",
			"money",
			"float",
			"real"]);
	}
	
	public function isTextType($sqlType){
		return in_array($sqlType, 
			["char",
			"varchar",
			"text",
			"nchar",
			"nvarchar",
			"ntext"]);
	}
	
	public function isDateType($sqlType){
		return in_array($sqlType, 
			["date",
			"datetimeoffset",
			"datetime2",
			"smalldatetime",
			"datetime",
			"time"]);
	}
	
	public function giqToSql($giq){
		$sql = "select ";
		//fields and aggregates
		$fieldsAggsSql = [];
		if(isset($giq["groupings"])){
			for($i=0;$i<count($giq["groupings"]); $i++){
				$field = $giq["groupings"][$i];
				$fieldsAggsSql[] = $this->expToSql($field["dispExp"]) . " as \"" . $field["alias"] . "\"";
			}
		}
		if(isset($giq["optionalGroupings"])){
			for($i=0;$i<count($giq["optionalGroupings"]); $i++){
				$field = $giq["optionalGroupings"][$i];
				$fieldsAggsSql[] = $this->expToSql($field["dispExp"]) . " as \"" . $field["alias"] . "\"";
			}
		}
		if(isset($giq["aggregates"])){
			for($i=0;$i<count($giq["aggregates"]); $i++){
				$agg = $giq["aggregates"][$i];
				$fieldsAggsSql[] = $this->expToSql($agg["exp"]) . " as \"" . $agg["alias"] . "\"";
			}
		}
		$sql .= implode(", ", $fieldsAggsSql) . " ";
		
		//tables
		$tablesSql = [];
		if(isset($giq["entities"])){
			for($i=0;$i<count($giq["entities"]); $i++){
				$ent = $giq["entities"][$i];
				$tablesSql[] = "\"". $ent["entityName"] . "\" \"" . $ent["alias"] . "\"";
			}
		}
		if(count($tablesSql) > 0){
			$sql .= "from " . implode(", ", $tablesSql) . " ";
		}
		
		//relationships and filters
		$filtersSql = [];
		if(isset($giq["filters"])){
			for($i=0;$i<count($giq["filters"]); $i++){
				$filter = $giq["filters"][$i];
				$filtersSql[] = $this->expToSql($filter["exp"]);
			}
		}
		
		if(isset($giq["entities"])){
			for($i=0;$i<count($giq["entities"]); $i++){
				$ent = $giq["entities"][$i];
				if(isset($ent["relationships"])){
					for($j=0; $j<count($ent["relationships"]); $j++){
						$rel = $ent["relationships"][$j];
						$filtersSql[] = 
							"\"". $ent["alias"] . "\".\"" . $rel["attributeName"] . "\"" . " = " . 
							"\"". $rel["refEntityAlias"] . "\".\"" . $rel["refAttributeName"] . "\"";
					}
				}
			}
		}
		
		if(count($filtersSql) > 0){
			$sql .= "where " . implode(" and ", $filtersSql) . " ";
		}
		
		//groupings
		$groupingsSql = [];
		if(isset($giq["groupings"])){
			for($i=0;$i<count($giq["groupings"]); $i++){
				$grp = $giq["groupings"][$i];
				$groupingsSql[] = $this->expToSql($grp["groupExp"]);
			}
		}
		if(isset($giq["optionalGroupings"])){
			for($i=0;$i<count($giq["optionalGroupings"]); $i++){
				$grp = $giq["optionalGroupings"][$i];
				$groupingsSql[] = $this->expToSql($grp["groupExp"]);
			}
		}
		if(count($groupingsSql) > 0){
			$sql .= "group by rollup (" . implode(", ", $groupingsSql) . ") ";
			//$sql .= "with rollup "
		}
		
		return $sql;
	}
	
	public function giqsToSql($giqs){
		$giqsSql = [];
		for($i=0; $i<count($giqs); $i++){
			$giqsSql[] = $this->giqToSql($giqs[$i]);
		}
		return $giqsSql;
	}
	
	public function expToSql($exp){
		$sqlExp = " ";
		for($i=0; $i<count($exp); $i++){
			$node = $exp[$i];
			if($node["type"] == "operator"){
				$sqlExp .= $node["displayText"] . " ";
			}elseif($node["type"] == "string"){
				$sqlExp .= $node["value"] . " ";
			}elseif($node["type"] == "number"){
				$sqlExp .= $node["value"] . " ";
			}elseif($node["type"] == "object"){
				$sqlExp .= "\"" . implode("].[", $node["namespaces"]) . "\".\"". $node["objectName"] . "\" ";
			}elseif($node["type"] == "null"){
				$sqlExp .= $node["displayText"] . " ";
			}elseif($node["type"] == "grouping"){
				$sqlExp .= "(". $this->expToSql($node["content"]) .") ";
			}elseif($node["type"] == "function"){
	
				$paramSql = [];
 				if(isset($node["parameters"])){
					for($j=0; $j<count($node["parameters"]); $j++){
						$param = $node["parameters"][$j];
						$paramSql[] = $this->expToSql($param);
					}
				}
				
				$paramSqlStr = "(" . implode(",", $paramSql) . ") ";
				
				switch($node["displayText"]){
					case "average": $sqlExp .= "avg" . $paramSqlStr ; break;
					case "date": $sqlExp .= "EXTRACT(YEAR FROM " . $paramSql[0] . ")" ; break;
					default: $sqlExp .= $node["displayText"] . $paramSqlStr;
				}
			
			}
		}
		return $sqlExp;
	}

}