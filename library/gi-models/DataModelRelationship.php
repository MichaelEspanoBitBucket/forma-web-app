<?php

class DataModelRelationship
{
	public function __construct(){}
	
	private $id = null;
	public function getId(){ return $this->id; }
	public function setId($id){ $this->id = $id; }
	
	private $modelId = null;
	public function getModelId(){ return $this->modelId; }
	public function setModelId($modelId){ $this->modelId = $modelId; }
	
	private $name = null;
	public function getName(){ return $this->name; }
	public function setName($name){ $this->name = $name; }
	
	private $dsId = null;
	public function getDsId(){ return $this->dsId; }
	public function setDsId($dsId){ $this->dsId = $dsId; }
	
	private $dsEntityName = null;
	public function getDsEntityName(){ return $this->dsEntityName; }
	public function setDsEntityName($dsEntityName){ $this->dsEntityName = $dsEntityName; }
}