<?php
class DataSource
{
	public function __construct(){}

	public $id = null;
	public $name = null;
	public $typeId = null;
	public $type = null;
	public $isHost = null;
	public $server = null;
	public $fileName = null;
	public $username = null;
	public $password = null;
}