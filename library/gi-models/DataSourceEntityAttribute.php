<?php

class DataSourceEntityAttribute
{
	public function __construct($id, $name){ 
		$this->id = $id; 
		$this->name = $name; 
	}
	
	public $id = null;
	public $name = null;
}