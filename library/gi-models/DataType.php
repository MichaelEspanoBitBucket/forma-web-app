<?php

class DataType
{
	public function __construct(){}

	private $id = 0;
	public function getId(){ return $this->id; }
	public function setId($id){ $this->id = $id; }
	
	private $name = "";
	public function getName(){ return $this->name; }
	public function setName($name){ $this->name = $name; }
}