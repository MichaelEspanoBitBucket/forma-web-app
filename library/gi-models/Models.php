<?php

require_once(realpath(".") . "/library/gi-models/DataType.php");
require_once(realpath(".") . "/library/gi-models/DataSourceType.php");
require_once(realpath(".") . "/library/gi-models/DataSource.php");
require_once(realpath(".") . "/library/gi-models/DataSourceEntity.php");

require_once(realpath(".") . "/library/gi-models/DataModel.php");
require_once(realpath(".") . "/library/gi-models/DataModelEntity.php");

