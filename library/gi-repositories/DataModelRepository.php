<?php


class DataModelRepository
{
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}
	
	public function getAllDataModels(){
		$dsStatement = $this->conn->prepare
			("select id, name from gi_data_models where company_id = ? order by name asc");
		$dsStatement->bind_param("i", $this->user["company_id"]);
		
		$dsStatement->execute();
		$dmResult = $dsStatement->get_result();	
		
		$dataModels = array();
		
		while($dmRow = $dmResult->fetch_object()){ 
			$dmObj = [ "id" => $dmRow->id, "name" => $dmRow->name ];
			$dataModels[] = $dmObj;
        } 
		
		return $dataModels;
	}
	
	public function getModel($id){
		//get model
		$dmStatement = $this->conn->prepare
			("select id, name, ui_tab_id, uid from gi_data_models where id = ? and company_id = ?");
		$dmStatement->bind_param("ii", $id, $this->user["company_id"]);
		$dmStatement->execute();
		$model = $dmStatement->get_result()->fetch_array();	
		
		//get entities
		$eStatement = $this->conn->prepare
			("select id, uid, model_id, name, ds_id, ds_entity_name, ui_left, ui_top
			from gi_model_entities where model_id = ?");
		$eStatement->bind_param("i", $model["id"]);
		$eStatement->execute();
		$entityList = $eStatement->get_result();	
		
		//get attributes
		$aStatement = $this->conn->prepare
			("select dt.common_data_type_id, attr.id, attr.entity_id, attr.name, attr.specific_dt_name, attr.ds_type_id, attr.is_primary_key, attr.ds_attribute_name
			from gi_entity_attributes as attr
			inner join gi_specific_data_types as dt
				on attr.specific_dt_name = dt.name and attr.ds_type_id = dt.ds_type_id
			where attr.entity_id in (select id from gi_model_entities where model_id = ?)");
		$aStatement->bind_param("i", $model["id"]);
		$aStatement->execute();
		$attributeList = $aStatement->get_result();	
		
		//get relationships
		$relStatement = $this->conn->prepare
			("select refAttr.name as ref_attr_name, depAttr.name as dep_attr_name,
				refEnt.name as ref_entity_name, depEnt.name as dep_entity_name,
				rel.pair_code, 
				refEnt.ds_entity_name as ref_ds_entity_name, 
				refAttr.ds_attribute_name as ref_ds_attr_name,  
				depEnt.ds_entity_name as dep_ds_entity_name, 
				depAttr.ds_attribute_name as dep_ds_attr_name
			from gi_entity_relationships as rel
			inner join gi_entity_attributes as refAttr
				on rel.reference_attribute_id = refAttr.id
			inner join gi_entity_attributes as depAttr
				on rel.dependent_attribute_id = depAttr.id
			inner join gi_model_entities as refEnt
				on refAttr.entity_id = refEnt.id
			inner join gi_model_entities as depEnt 
				on depAttr.entity_id = depEnt.id
			inner join gi_data_models as m
				on refEnt.model_id = m.id
			where m.id = ?");
		$relStatement->bind_param("i", $id);
		$relStatement->execute();
		$relList = $relStatement->get_result();
		
		/* combine data here */
		//model
		$output = [];
		$output["id"] = $model["id"];
		$output["name"] = $model["name"];
		$output["tabId"] = $model["ui_tab_id"];
		$output["uid"] = $model["uid"];
		//entities
		$output["entities"] = [];
		foreach($entityList as $entityRaw){
			$entity = [];
			$entity["id"] = $entityRaw["id"];
			$entity["uid"] = $entityRaw["uid"];
			$entity["name"] = $entityRaw["name"]; 
			$entity["dsId"] = $entityRaw["ds_id"];
			$entity["dsEntityName"] = $entityRaw["ds_entity_name"];
			$entity["left"] = $entityRaw["ui_left"];
			$entity["top"] = $entityRaw["ui_top"];
			$entity["attributes"] = [];
			//add attributes here
			foreach($attributeList as $attrKey => $attrVal){
				if($attrVal["entity_id"] == $entityRaw["id"]){
					$attribute = [];
					$attribute["id"] = $attrVal["id"];
					$attribute["name"] = $attrVal["name"];
					$attribute["dataTypeId"] = "";
					$attribute["dataTypeName"] = $attrVal["specific_dt_name"];
					$attribute["dsTypeId"] = $attrVal["ds_type_id"];
					$attribute["commonDataTypeId"] = $attrVal["common_data_type_id"];
					$attribute["dsAttributeName"] = $attrVal["ds_attribute_name"];
					$entity["attributes"][] = $attribute;
					//unset($attributeList[$attrKey]);
				}
			}
			$output["entities"][] = $entity;
		}
		
		//relationships
		$output["relationships"] = [];
		foreach($relList as $relRaw){
			$rel = [];
			$rel["refEntityName"] = $relRaw["ref_entity_name"];
			$rel["refAttrName"] = $relRaw["ref_attr_name"];
			$rel["depEntityName"] = $relRaw["dep_entity_name"];
			$rel["depAttrName"] = $relRaw["dep_attr_name"];
			$rel["pairCode"] = $relRaw["pair_code"];	
			
			$rel["refDsEntityName"] = $relRaw["ref_ds_entity_name"];
			$rel["refDsAttrName"] = $relRaw["ref_ds_attr_name"];
			$rel["depDsEntityName"] = $relRaw["dep_ds_entity_name"];
			$rel["depDsAttrName"] = $relRaw["dep_ds_attr_name"];	
			
			$output["relationships"][] = $rel;
		}
		
		return $output;
	}
	
	public function saveModel($modelData){
		//TODO: Add transaction rollback
		$stm = null;
		
		//model
		
		if($modelData["id"] == null){
			$stm = $this->conn->prepare("insert into gi_data_models(name, company_id, uid, ui_tab_id) values(?, ?, ?, ?)");
			$stm->bind_param("siss", $modelData["name"], $this->user["company_id"], $modelData["uid"], $modelData["tabId"]);
			$stm->execute();
			$modelData["id"] = $this->conn->insert_id;
		}else{
			$stm = $this->conn->prepare("update gi_data_models set name = ? where id = ?");
			$stm->bind_param("si", $modelData["name"], $modelData["id"]);
			$stm->execute();
		}
		
		//** entities and attributes **
		
		//delete entities and attributes (delete cascade so no need to delete attributes explicitly)
		$stm = $this->conn->prepare
			("delete from gi_model_entities where model_id = ?");
		$stm->bind_param("i", $modelData["id"]);
		$stm->execute();
		
		//prepare insert entity query
		$e_uid = null; $e_modid = null; $e_name = null; $e_dsid = null; $e_dsename = null; $e_uileft = null;  $e_uitop = null;
		$entityStm = $this->conn->prepare
			("insert into gi_model_entities(uid, model_id, name, ds_id, ds_entity_name, ui_left, ui_top) values (?, ?, ?, ?, ?, ?, ?)");
		$entityStm->bind_param("sisisdd", $e_uid, $e_modid, $e_name, $e_dsid, $e_dsename, $e_uileft, $e_uitop);
		
		//prepare insert attribute query
		$a_eid = null; $a_name = null; $a_sdtname = null; $a_dstid = null; $a_ispk = null; $a_dsattname = null;
		$attrStm = $this->conn->prepare
			("insert into gi_entity_attributes(entity_id, name, specific_dt_name, ds_type_id, is_primary_key, ds_attribute_name) values(?, ?, ?, ?, ?, ?)");
		$attrStm->bind_param("issiis", $a_eid, $a_name, $a_sdtname, $a_dstid, $a_ispk, $a_dsattname);
			
		$e_modid = $modelData["id"];
		
		$entities = $modelData["entities"];
		foreach($entities as $entity)
		{
			$e_uid = $entity["uid"];
			$e_name = $entity["entityName"];
			$e_dsid = $entity["dsId"];
			$e_dsename = $entity["dsEntityName"];
			$e_uitop = $entity["uiTop"];
			$e_uileft = $entity["uiLeft"];
			$entityStm->execute();
			//if($this->conn->error){ echo $this->conn->error; }
			
			$a_eid = $this->conn->insert_id;
			//echo "eid: " . $a_eid;
			$attributes = $entity["attributes"];
			foreach($attributes as $attribute){
				//echo '{ "attr": "' . $a_eid . '" }, ';
				$a_name = $attribute["name"];
				$a_sdtname = $attribute["dataTypeName"];
				$a_dstid =  $attribute["dsTypeId"];
				$a_ispk = null;
				$a_dsattname = $attribute["dsAttrName"];
				$attrStm->execute();
				if($this->conn->error){ echo $this->conn->error; }
			}
		}
		
		/* save relationships, don't let go! If she can't be with you, kill her. */
		$rel_modelId = $modelData["id"];
		$rel_depEntName = null;
		$rel_depAttrName = null;
		$rel_refEntName = null;
		$rel_refAttrName = null;
		$rel_pairCode = null;
		
		$relStm = $this->conn->prepare
			("insert into gi_entity_relationships(reference_attribute_id, dependent_attribute_id, pair_code) 
			select r.id, d.id, ? 
			from gi_entity_attributes as r
			join gi_entity_attributes as d
			left join gi_model_entities as re
				on r.entity_id = re.id
			left join gi_model_entities as de
				on d.entity_id = de.id 
			where 
				re.name = ? and r.name = ? and
				de.name = ? and d.name = ? and
				re.model_id = ? and de.model_id = ?");
		$relStm->bind_param("ssssssi", 
			$rel_pairCode,
			$rel_refEntName,
			$rel_refAttrName,
			$rel_depEntName, 
			$rel_depAttrName,
			$rel_modelId,
			$rel_modelId);
		
		$relationships = isset($modelData["relationships"])? $modelData["relationships"]: [];
		foreach($relationships as $rel){
			$rel_depEntName = $rel["depEntityName"];
			$rel_depAttrName = $rel["depAttributeName"];
			$rel_refEntName = $rel["refEntityName"];
			$rel_refAttrName = $rel["refAttributeName"];
			$rel_pairCode = $rel["pairCode"];
			$relStm->execute();
			if($this->conn->error){ echo $this->conn->error; }
		}
			
		return [ "isSuccess" => true, "id" => $modelData["id"] ];
	}
	
	public function validateModel($modelData){
		$errors = [];
		
		if(isset($modelData["entities"]) == false || sizeof($modelData["entities"]) == 0){
			$errors[] = "No entities defined.";
		}
		
		$stm = $this->conn->prepare("select count(id) as count from gi_data_models where name = ? and id <> ?");
		$stm->bind_param("si", $modelData["name"], $modelData["id"]);
		$stm->execute();
		if($stm->get_result()->fetch_array()["count"] != 0){
			$errors[] = "Model name already exist.";
		}
		
		return $errors;
	}
}