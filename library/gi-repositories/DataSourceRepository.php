<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

require_once(realpath('.') . "/library/gi-libraries/spreadsheet-reader-master/php-excel-reader/excel_reader2.php");
require_once(realpath('.') . "/library/gi-libraries/spreadsheet-reader-master/SpreadsheetReader.php");

// require_once(realpath('.') . "/library/gi-libraries/ExcelLib/PHPExcel.php");

class DataSourceRepository
{
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}
	
	public function getAllDataSources(){
		$dsStatement = $this->conn->prepare
			("select ds.id, ds.name, ds.type_id as typeId, dst.`name` as typeName, ds.is_host as isHost, 
				ds.server, ds.file_name as fileName, ds.excel_table_names as excelTableNames, 
				ds.`username`, ds.`password`
			from gi_data_sources as ds
			inner join gi_ds_types as dst
			on ds.type_id = dst.id
			where ds.company_id = ? or ds.company_id is null
			order by ds.name asc");
		
		$dsStatement->bind_param("i", $this->user["company_id"]);
		
		$dsStatement->execute();
		$dsResult = $dsStatement->get_result();
		
		$dataSources = array();
		
		while($dsRow = $dsResult->fetch_object()){ 
			$dsObj = [
				"id" => $dsRow->id,
				"name" => $dsRow->name,
				"type" => [ "id" => $dsRow->typeId, "name" => $dsRow->typeName ],
				"isHost" => $dsRow->isHost
			];
			$dataSources[] = $dsObj;
        } 
		
		return $dataSources;
	}

	public function getAllDsEntityAttributes($dsId, $entityId){
		$statement = $this->conn->prepare
			("select ds.id, ds.name, ds.type_id as typeId, dst.`name` as typeName, ds.is_host as isHost, 
				ds.server, ds.file_name as fileName, ds.excel_table_names as excelTableNames, ds.username, ds.`password`, ds.dsn, 
				ds.database_name as databaseName, ds.port
			from gi_data_sources as ds
			inner join gi_ds_types as dst
			on ds.type_id = dst.id
			where ds.id = ? and (ds.company_id = ? or ds.company_id is null)");
						
		$statement->bind_param("ii", $dsId, $this->user["company_id"]);
		
		$statement->execute();
		$dsRow = $statement->get_result()->fetch_object();
		
		$attributes = array();
		
		/* Get attributes of entity based on its type. */
		switch($dsRow->typeName)
		{
			case "Formalistics":
				//get field names
				$statement = $this->conn->prepare
					("select f.id as formId, f.form_name as formName, f.form_table_name as id, f.active_fields as fields 
					from tb_workspace as f
					where f.is_active = 1 and f.is_delete = 0 and f.company_id = ? and f.form_table_name = ?");
					
				$statement->bind_param("is", $this->user["company_id"], $entityId);
				$statement->execute();
				$formRow = $statement->get_result()->fetch_object();
				
				$fieldNames = explode(",", $formRow->fields);
				
				//get field data types
				//echo var_dump("select " . $formRow->fields . " from " . $formRow->id . " where 1=0");
				
				$statement = $this->conn->prepare
					("SELECT cinfo.COLUMN_NAME as `name`, cinfo.DATA_TYPE as `type`
					FROM `INFORMATION_SCHEMA`.`COLUMNS` as cinfo 
					WHERE `cinfo`.`TABLE_SCHEMA` = '" . DB_NAME . "' 
					AND `cinfo`.`TABLE_NAME` = '" . $entityId . "' 
					AND cinfo.COLUMN_NAME in ('ID', 'TrackNo', 'Status', 'DateCreated', 'Requestor', '". implode("','",$fieldNames) ."') 
					ORDER BY name");
				//$statement->bind_param("ss", DB_NAME . "", $formRow->formName);
				$statement->execute();
				$result = $statement->get_result();
				
				while($fieldInfo = $result->fetch_object()){ 
					$attributes[] = 
						[
							"id" => $fieldInfo->name, 
							"name" => $fieldInfo->name, 
							"type" => [ "id" => $fieldInfo->type, "name" => $fieldInfo->type],
							"dsType"  => [ "id" => $dsRow->typeId, "name" =>  $dsRow->typeName ] 
						];
				}
				
				break;
			case "SQL Server":
				$conn = odbc_connect($dsRow->dsn, "", "");
					
				$tableAttributes = odbc_exec($conn, "SELECT COLUMN_NAME as [name], data_type as [type]
					FROM INFORMATION_SCHEMA.COLUMNS
					WHERE TABLE_NAME = '".$entityId."'");
				
				while($tableAttrRow = odbc_fetch_object($tableAttributes)){ 
					$attributes[] = 
						[
							"id" => $tableAttrRow->name, 
							"name" => $tableAttrRow->name, 
							"type" => [ "id" => null, "name" => $tableAttrRow->type],
							"dsType"  => [ "id" => $dsRow->typeId, "name" =>  $dsRow->typeName ] 
						];
				}
				
				break;
			case "MySQL":
				$conn = new mysqli($dsRow->server, $dsRow->username, $dsRow->password, $dsRow->databaseName, $dsRow->port);
				$statement = $conn->prepare
					("SELECT COLUMN_NAME as name, DATA_TYPE as type
					FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ?");
					
				$statement->bind_param("ss", $dsRow->databaseName, $entityId);
				$statement->execute();
				
				$result = $statement->get_result();
				
				while($fieldInfo = $result->fetch_object()){ 
					$attributes[] = 
						[
							"id" => $fieldInfo->name, 
							"name" => $fieldInfo->name, 
							"type" => [ "id" => $fieldInfo->type, "name" => $fieldInfo->type],
							"dsType"  => [ "id" => $dsRow->typeId, "name" =>  $dsRow->typeName ] 
						];
				}
				break;
				
			case "Oracle":
				$conn = odbc_connect($dsRow->dsn, "", $dsRow->password);
				
				$tableAttributes = odbc_exec($conn, "SELECT column_name as \"name\", data_type as \"type\"
					FROM all_tab_columns
					WHERE table_name = '".$entityId."'");
					
				while($tableAttrRow = odbc_fetch_object($tableAttributes)){ 
					$attributes[] = 
						[
							"id" => $tableAttrRow->name, 
							"name" => $tableAttrRow->name, 
							"type" => [ "id" => null, "name" => $tableAttrRow->type],
							"dsType"  => [ "id" => $dsRow->typeId, "name" =>  $dsRow->typeName ] 
						];
				}
				
				break;
				
			case "Excel":
				//$conn = new mysqli($dsRow->server, $dsRow->username, $dsRow->password, $dsRow->databaseName, $dsRow->port);
				$statement = $this->conn->prepare
					("SELECT COLUMN_NAME as name, DATA_TYPE as type
					FROM INFORMATION_SCHEMA.COLUMNS 
					WHERE TABLE_SCHEMA = database() AND TABLE_NAME = ?");
					
				$statement->bind_param("s", $entityId);
				$statement->execute();
				
				$result = $statement->get_result();
				
				while($fieldInfo = $result->fetch_object()){ 
					$attributes[] = 
						[
							"id" => $fieldInfo->name, 
							"name" => $fieldInfo->name, 
							"type" => [ "id" => $fieldInfo->type, "name" => $fieldInfo->type],
							"dsType"  => [ "id" => $dsRow->typeId, "name" =>  $dsRow->typeName ] 
						];
				}
				break;
		}
		
		return $attributes;
	}	

	public function getDsEntities($dsId, $search = null){
	
		$statement = $this->conn->prepare
			("select ds.id, ds.name, ds.type_id as typeId, dst.`name` as typeName, ds.is_host as isHost, 
				ds.server, ds.file_name as fileName, ds.excel_table_names as excelTableNames, 
				ds.username, ds.`password`, ds.database_name as `databaseName`,
				ds.dsn, ds.port
			from gi_data_sources as ds
			inner join gi_ds_types as dst
			on ds.type_id = dst.id
			where ds.id = ? and (ds.company_id = ? or ds.company_id is null)");
		
		$statement->bind_param("ii", $dsId, $this->user["company_id"]);
		
		$statement->execute();
		$dsResult = $statement->get_result();
		 
		$dsRow = $dsResult->fetch_object();
		
		$entities = array();
		
		/* Get entities of Data Source based on its type. */
		switch($dsRow->typeName)
		{
			case "Formalistics":
			
				$statement = $this->conn->prepare
					("select f.id as formId, f.form_name as formName, f.form_table_name as tableName 
					from tb_workspace as f
					where f.is_active = 1 and f.is_delete = 0 and f.company_id = " . $this->user["company_id"] . " " . 
						($search == null ? "" : " and f.form_name like ? ") . " Order by f.form_name");
				
				if($search != null){
					$sString = "%".$search."%";
					$statement->bind_param("s", $sString);
				}
				

				$statement->execute();
				$eformsResult = $statement->get_result();
				
				while($formRow = $eformsResult->fetch_object()){ 
					$entities[] = 
						[ 
						"id" => $formRow->tableName, 
						"name" => $formRow->formName,
						"entityName" => $formRow->tableName,
						"formName" => $formRow->formName,
						"formId" => $formRow->formId
						];
				}
				
				break;
				
			case "SQL Server":
				try{
					$conn = odbc_connect($dsRow->dsn, "", "");
					
					$tableNameStm = odbc_prepare($conn, 
						"SELECT name 
						FROM sysobjects 
						WHERE xtype='U' " .
						($search == null? "" : " and name like ?"));
					
					if($search == null){
						odbc_execute($tableNameStm);
					}else{
						odbc_execute($tableNameStm, array("%" . $search . "%"));
					}
					
					while($tableNameRow = odbc_fetch_object($tableNameStm)){ 
						$entities[] = 
							["id" => $tableNameRow->name, 
							"name" => $tableNameRow->name,
							"entityName" => $tableNameRow->name,
							"formName" => null,
							"formId" => null];
					}
				}catch(Exception $e){
					echo $e->getMessage();
				}
				
				break;
			case "MySQL":
				/*
				ds.id, ds.name, ds.type_id as typeId, dst.`name` as typeName, ds.is_host as isHost, 
				ds.server, ds.file_name as fileName, ds.username, ds.`password`, ds.database_name as `databaseName`,
				ds.dsn 
				*/
				$conn = new mysqli($dsRow->server, $dsRow->username, $dsRow->password, $dsRow->databaseName, $dsRow->port);
				$statement = $conn->prepare
					("select table_name as tableName 
					from information_schema.tables 
					where TABLE_SCHEMA = '" . $dsRow->databaseName . "' " .
					($search == null? "" : " and table_name like ? ") .
					"order by table_name");
					
				if($search != null){
					$stement->bind_param("s", "%".$search."%");
				}
				$statement->execute();
				$res = $statement->get_result();
				
				while($row = $res->fetch_object()){ 
					$entities[] = 
						["id" => $row->tableName, 
						"name" => $row->tableName,
						"entityName" => $row->tableName,
						"formName" => null,
						"formId" => null];
				}
				
				break;
				
			case "Oracle":
				try{
					$conn = odbc_connect($dsRow->dsn, "", $dsRow->password);
					
					$tableNameStm = odbc_prepare($conn, 
						"SELECT table_name as \"name\"
							FROM user_tables" .
							($search == null? "" : " where table_name like ?"));
					
					if($search == null){
						odbc_execute($tableNameStm);
					}else{
						odbc_execute($tableNameStm, array("%" . $search . "%"));
					}
					
					while($tableNameRow = odbc_fetch_object($tableNameStm)){ 
						$entities[] = 
							["id" => $tableNameRow->name, 
							"name" => $tableNameRow->name,
							"entityName" => $tableNameRow->name,
							"formName" => null,
							"formId" => null];
					}
				}catch(Exception $e){
					echo $e->getMessage();
				}
				
				break;
			case "Excel":
				
				$excelTableNames = explode(",", $dsRow->excelTableNames);
				
				for($ti = 0; $ti < count($excelTableNames); $ti++){
					$tableName = trim($excelTableNames[$ti]);
					if($search == null || $search == ""){
						$entities[] = 
							[ 
							"id" => $tableName, 
							"name" =>  $tableName,
							"entityName" =>  $tableName,
							"formName" => null,
							"formId" => null
							];
					}else if(stripos($tableName, $search) > -1){
						$entities[] = 
							[ 
							"id" => $tableName, 
							"name" =>  $tableName,
							"entityName" => $tableName,
							"formName" => null,
							"formId" => null
							];
					}
				}
				
				break;
		}
		
		return $entities;
	}

	public function addDataSource($dsName, $dsTypeId, $dsn = null, $filePath = null){

		switch ($dsTypeId) {
			case 2:
				// SQL Server

				$objStatement = $this->conn->prepare
					("INSERT INTO gi_data_sources (name, type_id, dsn) VALUES (?,?,?)");
				$objStatement->bind_param("sis",
							$dsName,
							$dsTypeId,
							$dsn);
				$objStatement->execute();

				break;
			case 5 :
				// MS Excel
				$tables = "";
				$SqlQuery = "";
				$this->conn->query("START TRANSACTION");
				$SheetsInfo = [];
			
				
			
					$Reader = new SpreadsheetReader($filePath);
				    $Sheets = $Reader -> Sheets();
				    foreach ($Sheets as $Index => $Name)
				    {	
				    	$SheetInfo["Name"] = $dsName . "_" . $Name;

				        $Reader -> ChangeSheet($Index);

				        $SqlQuery = "CREATE TABLE IF NOT EXISTS `" . $dsName . "_" . $Name . "` (";

				       // echo 'Sheet #'.$Index.': '.$Name . PHP_EOL;
				        $headers = [];
				        $datatypes = [];
				       // $rowCount = 0;
				        $SheetInfo["header"] = [];
				        $SheetInfo["datatype"] = [];
				        
				        foreach ($Reader as $RowIndex => $Row)
				        {
				        	//echo $RowIndex;
				        	switch ($RowIndex) {
					        		case 0:
					        			# Header...
					        		//	echo "Headers";
					        			foreach ($Row as $col => $value) {
							        		//var_dump($value);
							        		if($value!=""){
							        			$SheetInfo["header"][] = " `" . $value . "` ";
							        		}else{
							        			$SheetInfo["header"][] = "";
							        		}
							        		
							        		//$SqlQuery = $SqlQuery . " `" . $value . "` ";
							        	}
					        			break;
					        		case 1:
					        			# Get datatype...
					        			//echo "Datatypes";
					        			foreach ($Row as $key => $value) {
							        		//var_dump(gettype($value));
							        		//var_dump(gettype($value));
							        		if(gettype($value) == "string"){
							        			// if(strlen($value) == 8){
							        			if(check_your_datetime($value)){
							        					//$SqlQuery = $SqlQuery . " DATETIME";
							        					$SheetInfo["datatype"][] = "DATETIME";
							        				// }else{
							        				// 	//$SqlQuery = $SqlQuery . " VARCHAR(250)";
							        				// 	$SheetInfo["datatype"][] = "VARCHAR(250)";
							        				// }
							        			}else if(is_numeric($value)){
							        				$SheetInfo["datatype"][] = "double";
							        			}else{
							        				//$SqlQuery = $SqlQuery . " VARCHAR(250)";
							        				$SheetInfo["datatype"][] = "VARCHAR(250)";

							        			}
							        		}else if(gettype($value) == "double"){
							        			//$SqlQuery = $SqlQuery . " DOUBLE";
							        			$SheetInfo["datatype"][] = "double";
							        		}else if(gettype($value) == "integer"){
							        			//$SqlQuery = $SqlQuery . "INT";
							        			$SheetInfo["datatype"][] = "int";
							        		}
	        		
							        	}
					        			break;
					        		default:
					        				break;
					        			break;
					        	}

					        //	$rowCount += 1;

				           
				        }
				        	if(count($SheetInfo["header"])!=0){
				        		$tables = $tables . ($tables!=""? ",":"") .  $dsName . "_" . $Name;
				        	//	var_dump($SheetInfo);
				        		foreach ($SheetInfo["header"] as $key => $header) {

				        		if($header!="" && $header!=null){
				        			$SqlQuery = $SqlQuery . ($key!=0?"," : "") . $header . " " . $SheetInfo["datatype"][$key];
				        		}

				        		if(($key+1) == count($SheetInfo["header"])){

				        			$SqlQuery = $SqlQuery . ");";
								}
				    
				        		
					        	}

					        	$queryStatement = $this->conn->prepare
									($SqlQuery);
								$queryStatement->execute();
					        ///	echo $SqlQuery;
								$SheetsInfo[] = $SheetInfo;
				        	}

				        	
				        	
    				}
    				
    				$objStatement = $this->conn->prepare
						("INSERT INTO gi_data_sources (name, type_id, file_name, excel_table_names) VALUES (?,?,?,?)");
					$objStatement->bind_param("siss",
								$dsName,
								$dsTypeId,
								$filePath,
								$tables);
					$objStatement->execute();


					//$Reader = new SpreadsheetReader($filePath);
				    $Sheets = $Reader -> Sheets();
				    foreach ($SheetsInfo as $Index => $SheetInfo)
				    {	
				    	$dttypes = "";

				        $Reader -> ChangeSheet($Index);

				        $SqlQuery = "INSERT INTO `" . $SheetInfo["Name"] . "` " ;

						
				        foreach ($Reader as $RowIndex => $Row)
				        {
				        	$QParam = "";
				        	if($RowIndex == 0){
				        		foreach ($SheetInfo["header"] as $key => $value) {

				        			if($value != ''){
				        			
				        					$SqlQuery = $SqlQuery . ($key==0?"(" : ",") . $value;
				        				
				        				
				        				$QParam = $QParam . ($key==0? "" : ",") . "?";
				        				switch ($SheetInfo["datatype"][$key]) {
				        					case 'int':
				        						$dttypes = $dttypes . "i";
				        						break;
				        					case 'double':
				        						$dttypes = $dttypes . "d";
				        						break;
				        					case 'VARCHAR(250)':
				        						$dttypes = $dttypes . "s";
				        						break;
				        					case 'DATETIME':
				        						$dttypes = $dttypes . "s";
				        						break;
				        					default:
				        						$dttypes = $dttypes . "s";
				        						break;
				        				}
				        				
				        			}
				        			
				        		}
				        		$SqlQuery = $SqlQuery . ") VALUES (" . $QParam . ")";
								//echo $SqlQuery . PHP_EOL;
				        	}else{ 

								$a_params = [];			        		
								/* with call_user_func_array, array params must be passed by reference */
								$a_params[] = & $dttypes;
								//echo $dttypes . PHP_EOL;
								foreach ($Row as $col => $value) {
										if(check_your_datetime($value)){
											$dt = DateTime::createFromFormat('m/d/Y', $value);
				        						$Row[$col] = $dt->format('Y-m-d');
				        					 $a_params[] = & $Row[$col];
				        				}else{
				        					 $a_params[] = & $Row[$col];
				        				}
									
								//	 echo $value;
								}

								//var_dump($a_params);
								// for($i = 0; $i < $n; $i++) {
								//    with call_user_func_array, array params must be passed by reference 
								//   $a_params[] = & $a_bind_params[$i];
								// }
								 
								/* Prepare statement */
								$stmt = $this->conn->prepare($SqlQuery);
								if($stmt === false) {
								  trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $this->conn->errno . ' ' . $this->conn->error, E_USER_ERROR);
								}
								 
								/* use call_user_func_array, as $stmt->bind_param('s', $param); does not accept params array */
								call_user_func_array(array($stmt, 'bind_param'), $a_params);
								 
								/* Execute statement */
								$stmt->execute();
					        	
				        	}
				           
				        }
							//echo $SqlQuery . PHP_EOL;
				        	
    				}	

    				if (file_exists($filePath)) {
				        unlink($filePath);
				    }
					
    				


    				$this->conn->query("COMMIT");
    				//Case 5 ending
				break;
			default:
				# code...
				break;



		}


	}

	public function removeDataSource($dsId){

		$statement = $this->conn->prepare
			("SELECT ds.id, ds.name, ds.type_id as typeId, dst.`name` as typeName, ds.is_host as isHost, 
				ds.server, ds.file_name as fileName, ds.excel_table_names as excelTableNames, 
				ds.username, ds.`password`, ds.database_name as `databaseName`,
				ds.dsn, ds.port
			from gi_data_sources as ds
			inner join gi_ds_types as dst
			on ds.type_id = dst.id
			where ds.id = ? and (ds.company_id = ? or ds.company_id is null)");
		
		$statement->bind_param("ii", $dsId, $this->user["company_id"]);
		
		$statement->execute();
		$dsResult = $statement->get_result();
		 
		$dsRow = $dsResult->fetch_object();
		if(trim($dsRow->excelTableNames)!=""){
			$excelTableNames = explode(",", $dsRow->excelTableNames);

			$this->conn->query("START TRANSACTION");

			for($ti = 0; $ti < count($excelTableNames); $ti++){
				$tableName = trim($excelTableNames[$ti]);
				$dltTbl = $this->conn->prepare("DROP TABLE IF EXISTS `" . $tableName . "`");
				$dltTbl->execute();
			}
		}
		

		$dltDS = $this->conn->prepare("DELETE FROM `gi_data_sources` WHERE id = ?;");
		$dltDS->bind_param("i", $dsId);
		$dltDS->execute();

		$this->conn->query("COMMIT");
		
	}



}





function get_sql_datatype($value){
	if(gettype($value) == "string"){
		if(strlen($value) == 8){
			if(check_your_datetime($value)){
				return " DATETIME";
			}else{
				return " VARCHAR(250)";
			}
		}else if(is_numeric($value)){
			return " DOUBLE";
		}else{
			return " VARCHAR(250)";
		}
	}else if(gettype($value) == "double"){
		return " DOUBLE";
	}else if(gettype($value) == "integer"){
		return " INT";
	}
}

function check_your_datetime($myString) {

    return DateTime::createFromFormat('m/d/Y', $myString) !== FALSE;
}