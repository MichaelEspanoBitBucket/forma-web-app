<?php

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

set_time_limit (3600);
$path = $_SERVER["DOCUMENT_ROOT"];
include_once($path . "/library/gi-libraries/GiqToMySql.php");
include_once($path . "/library/gi-libraries/GiqToSqlServer.php");
include_once($path . "/library/gi-libraries/GiqToOracle.php");
include_once($path . "/library/Phinq/bootstrap.php");

use Phinq\Phinq as Phinq;

class DataWarehouse
{
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}
	
	public function getChartData($giqs){
		
		$giqsData = [ "sql" => [], "timeGenerated" => null, "data" => [], "metaData" =>[], "dataTable" => [], "fieldsInfo" => [] ];
		
		for($i=0; $i<count($giqs); $i++){
			$giq = $giqs[$i];
			$giqsData["metaData"][] = ["recordCount" => null];
			
			//get ds info first
			$dsStatement = $this->conn->prepare
				("select ds.id, ds.name, ds.type_id as typeId, dst.`name` as typeName, ds.is_host as isHost, 
					ds.server, ds.file_name as fileName, ds.`username`, ds.`password`, ds.dsn, ds.database_name as databaseName
				from gi_data_sources as ds
				inner join gi_ds_types as dst
				on ds.type_id = dst.id
				where ds.id = ? and (ds.company_id = ? or ds.company_id is null)
				order by ds.name asc");
			
			$dsStatement->bind_param("ii", $giq["dataSource"]["id"], $this->user["company_id"]);
			
			$dsStatement->execute();
			$dsResult = $dsStatement->get_result();
			
			$dsRow = $dsResult->fetch_object();
			
			
			if($giq["dataSource"]["type"]["name"] == "Formalistics" && $giq["dataSource"]["id"] == 1){
				$giqToSql = new GiqToMySql();
				$sqlCount = isset($giq["page"]) ? $giqToSql->giqToSqlCount($giq) : null;
				$sql = isset($giq["page"]) ? $giqToSql->giqToSqlPage($giq) : $giqToSql->giqToSqlRollup($giq);
				//$giqsData["sql"][] = $sql;
				//echo "[" . $sql . "]";
				$sqlStartTime = microtime(true);
				$stm = $this->conn->prepare($sql);
				$stm->execute();
				
				$sqlResult = $stm->get_result(); 
				$result = $sqlResult->fetch_all(MYSQLI_ASSOC);
				
				$sqlEndTime = microtime(true);
				$giqsData["sql exec time"] = $sqlEndTime - $sqlStartTime;
				
				//get field info such as data type
				$fieldsInfoRaw = $sqlResult->fetch_fields();
				$fieldsInfo = [];
				
				foreach($fieldsInfoRaw as $fieldInfoRaw){
					$fieldInfo = [];
					$fieldInfo["name"] = $fieldInfoRaw->name;
					if($fieldInfoRaw->flags & MYSQLI_NUM_FLAG){
						$fieldInfo["type"] = "Numeric";
					}elseif(in_array($fieldInfoRaw->type, [254, 253, 252])){
						$fieldInfo["type"] = "Text";
					}elseif($fieldInfoRaw->type == MYSQLI_TYPE_DATE || $fieldInfoRaw->type == MYSQLI_TYPE_DATETIME){
						$fieldInfo["type"] = "Date";
					}
					$fieldsInfo[] = $fieldInfo;
				}
				
				$giqsData["fieldsInfo"][] = $fieldsInfo;
				
				//pagination count
				if(isset($giq["page"])){
					//echo $sqlCount;
					
					$countStm = $this->conn->prepare($sqlCount);
					$countStm->execute();
					$countResult = $countStm->get_result();
					$giqsData["metaData"][0]["recordCount"] = $countResult->fetch_object()->row_count;
				}
			}else if($giq["dataSource"]["type"]["name"] == "SQL Server"){
				try{
					$conn = odbc_connect($dsRow->dsn, "", "");
					
					$giqToSql = new GiqToSqlServer();
					$sql = $giqToSql->giqToSql($giq);
					//$giqsData["sql"][] = $sql;
					
					//$stm = odbc_prepare($conn, $sql);
					$sqlStartTime = microtime(true);
					$sqlResult = odbc_exec($conn, $sql);
					$sqlEndTime = microtime(true);
					$giqsData["sql exec time"] = $sqlEndTime - $sqlStartTime;
					
					$result = [];
					while($resultRow = odbc_fetch_object($sqlResult)){
						$result[] = $resultRow;
					}
					
					//get field info such as data type
					$fieldsInfo = [];
					if(isset($result[0])){
						$firstRow = $result[0];
						
						for($ci=0; $ci<odbc_num_fields($sqlResult); $ci++){
							$fieldInfo = [];
							$fieldInfo["name"] = odbc_field_name($sqlResult, $ci+1);
							$type = odbc_field_type($sqlResult, $ci+1);
							if($giqToSql->isNumericType($type)){
								$fieldInfo["type"] = "Numeric";
							}else if($giqToSql->isTextType($type)){
								$fieldInfo["type"] = "Text";
							}
							$fieldsInfo[] = $fieldInfo;
						}
					}
					
					$giqsData["fieldsInfo"][] = $fieldsInfo;
					
				}catch(Exception $e){
					echo $e->getMessage();
				}
			}else if($giq["dataSource"]["type"]["name"] == "MySQL"){
				$giqToSql = new GiqToMySql();
				$sql = $giqToSql->giqToSqlRollup($giq);
				//$giqsData["sql"][] = $sql;
				$conn = new mysqli($dsRow->server, $dsRow->username, $dsRow->password, $dsRow->databaseName, $dsRow->port);
				$stm = $conn->prepare($sql);
				$sqlStartTime = microtime(true);
				$stm->execute();
				$sqlEndTime = microtime(true);
				$giqsData["sql exec time"] = $sqlEndTime - $sqlStartTime;
				
				 
				$sqlResult = $stm->get_result(); 
				$result = $sqlResult->fetch_all(MYSQLI_ASSOC);
				
				//get field info such as data type
				$fieldsInfoRaw = $sqlResult->fetch_fields();
				$fieldsInfo = [];
				
				foreach($fieldsInfoRaw as $fieldInfoRaw){
					$fieldInfo = [];
					$fieldInfo["name"] = $fieldInfoRaw->name;
					if($fieldInfoRaw->flags & MYSQLI_NUM_FLAG){
						$fieldInfo["type"] = "Numeric";
					}elseif(in_array($fieldInfoRaw->type, [254, 253, 252])){
						$fieldInfo["type"] = "Text";
					}elseif($fieldInfoRaw->type == MYSQLI_TYPE_DATE || $fieldInfoRaw->type == MYSQLI_TYPE_DATETIME){
						$fieldInfo["type"] = "Date";
					}
					$fieldsInfo[] = $fieldInfo;
				}
				
				$giqsData["fieldsInfo"][] = $fieldsInfo;
			}else if($giq["dataSource"]["type"]["name"] == "Oracle"){
				try{
					$conn = odbc_connect($dsRow->dsn, "", $dsRow->password);
					
					$giqToSql = new GiqToOracle();
					$sql = $giqToSql->giqToSql($giq);
					$giqsData["sql"][] = $sql;
					
					//$stm = odbc_prepare($conn, $sql);
					$sqlStartTime = microtime(true);
					$sqlResult = odbc_exec($conn, $sql);
					$sqlEndTime = microtime(true);
					$giqsData["sql exec time"] = $sqlEndTime - $sqlStartTime;
					
					$result = [];
					while($resultRow = odbc_fetch_object($sqlResult)){
						$result[] = $resultRow;
					}
					
					//get field info such as data type
					$fieldsInfo = [];
					if(isset($result[0])){
						$firstRow = $result[0];
						
						for($ci=0; $ci<odbc_num_fields($sqlResult); $ci++){
							$fieldInfo = [];
							$fieldInfo["name"] = odbc_field_name($sqlResult, $ci+1);
							$type = odbc_field_type($sqlResult, $ci+1);
							if($giqToSql->isNumericType($type)){
								$fieldInfo["type"] = "Numeric";
							}else if($giqToSql->isTextType($type)){
								$fieldInfo["type"] = "Text";
							}
							$fieldsInfo[] = $fieldInfo;
						}
					}
					
					$giqsData["fieldsInfo"][] = $fieldsInfo;
					
				}catch(Exception $e){
					echo $e->getMessage();
				}
			}
			else if($giq["dataSource"]["type"]["name"] == "Excel"){
				$giqToSql = new GiqToMySql();
				$sql = isset($giq["page"]) ? $giqToSql->giqToSqlPage($giq) :$giqToSql->giqToSqlRollup($giq);
				//$giqsData["sql"][] = $sql;
				
				$stm = $this->conn->prepare($sql);
				$sqlStartTime = microtime(true);
				$stm->execute();
				$sqlEndTime = microtime(true);
				$giqsData["sql exec time"] = $sqlEndTime - $sqlStartTime;
				
				 
				$sqlResult = $stm->get_result(); 
				$result = $sqlResult->fetch_all(MYSQLI_ASSOC);
				
				//get field info such as data type
				$fieldsInfoRaw = $sqlResult->fetch_fields();
				$fieldsInfo = [];
				
				foreach($fieldsInfoRaw as $fieldInfoRaw){
					$fieldInfo = [];
					$fieldInfo["name"] = $fieldInfoRaw->name;
					if($fieldInfoRaw->flags & MYSQLI_NUM_FLAG){
						$fieldInfo["type"] = "Numeric";
					}elseif(in_array($fieldInfoRaw->type, [254, 253, 252])){
						$fieldInfo["type"] = "Text";
					}elseif($fieldInfoRaw->type == MYSQLI_TYPE_DATE || $fieldInfoRaw->type == MYSQLI_TYPE_DATETIME){
						$fieldInfo["type"] = "Date";
					}
					$fieldsInfo[] = $fieldInfo;
				}
				
				$giqsData["fieldsInfo"][] = $fieldsInfo;
			}
		}
		
		
		$giqsData["timeGenerated"] = time();
		
		$giqsData["dataTable"][] = $result;
		//var_dump();
		return $giqsData;
	
	}

	public function getChartExportData($giq){
		$result = [];
		
		$dsStatement = $this->conn->prepare
			("select ds.id, ds.name, ds.type_id as typeId, dst.`name` as typeName, ds.is_host as isHost, 
				ds.server, ds.file_name as fileName, ds.`username`, ds.`password`, ds.dsn, ds.database_name as databaseName
			from gi_data_sources as ds
			inner join gi_ds_types as dst
			on ds.type_id = dst.id
			where ds.id = ? and (ds.company_id = ? or ds.company_id is null)
			order by ds.name asc");
		
		$dsStatement->bind_param("ii", $giq["dataSource"]["id"], $this->user["company_id"]);
		
		$dsStatement->execute();
		$dsResult = $dsStatement->get_result();
		
		$dsRow = $dsResult->fetch_object();
		
		if($giq["dataSource"]["type"]["name"] == "Formalistics" && $giq["dataSource"]["id"] == 1){
			$giqToSql = new GiqToMySql();
			$sql = $giqToSql->giqToSqlBasic($giq);
			
			$stm = $this->conn->prepare($sql);
			$stm->execute();
			
			$sqlResult = $stm->get_result(); 
			$result = $sqlResult->fetch_all(MYSQLI_ASSOC);
			
		}else if($giq["dataSource"]["type"]["name"] == "SQL Server"){
			try{
				$conn = odbc_connect($dsRow->dsn, "", "");
				
				$giqToSql = new GiqToSqlServer();
				$sql = $giqToSql->giqToSql($giq);
				
				$sqlResult = odbc_exec($conn, $sql);
				
				while($resultRow = odbc_fetch_object($sqlResult)){
					$result[] = $resultRow;
				}
			}catch(Exception $e){
				echo $e->getMessage();
			}
		}else if($giq["dataSource"]["type"]["name"] == "MySQL"){
			$giqToSql = new GiqToMySql();
			$sql = $giqToSql->giqToSqlBasic($giq);
			
			$stm = $this->conn->prepare($sql);
			$stm->execute();
			
			$sqlResult = $stm->get_result(); 
			$result = $sqlResult->fetch_all(MYSQLI_ASSOC);
			
		}else if($giq["dataSource"]["type"]["name"] == "Oracle"){
			try{
				$conn = odbc_connect($dsRow->dsn, "", $dsRow->password);
				
				$giqToSql = new GiqToOracle();
				$sql = $giqToSql->giqToSql($giq);
				
				$sqlResult = odbc_exec($conn, $sql);
				
				$result = [];
				while($resultRow = odbc_fetch_object($sqlResult)){
					$result[] = $resultRow;
				}
				
			}catch(Exception $e){
				echo $e->getMessage();
			}
		}
		
		return $result;
		
	}
	
}
	
