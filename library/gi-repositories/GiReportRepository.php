<?php
/*
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
*/
require_once(realpath('.') . "/library/gi-repositories/DataModelRepository.php");
require_once(realpath('.') . "/library/Phinq/bootstrap.php");
use Phinq\Phinq as Phinq;
//install driver mysqlnd. for  get_result (PHP 5 >= 5.3.0) supported
//sudo apt-get install php5-mysqlnd
class ReportRepository
{
	
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}

	public function saveReport($report){
		
		$rStatement = $this->conn->prepare
			("INSERT INTO gi_reports (uid, name, creator_id, company_id, gi_paper_size_id, dashboard_published, dashboard_department_id) VALUES (?,?,?,?,?,?,?)");
		$rStatement->bind_param("ssiiiii",
					$report['uid'],
					$report['name'],
					$this->user["id"],
					$this->user["company_id"],
					$report['paper_size_id'],
					$report['dashboard_published'],
					$report['dashboard_department_id']);
		$rStatement->execute();
		$repID = $rStatement->insert_id;
		
		if(isset($report['object_properties'])){
			foreach($report['object_properties'] as $object){
				$this->saveObject($repID, $object);
			}
		}

		return 	$repID;		
	}
	
	
	public function updateReport($report){
		
		//echo  $report['id'];
		$urStmnt = $this->conn->prepare
			("UPDATE gi_reports SET name = ?, date_modified = NOW(), gi_paper_size_id = ?, dashboard_published = ?, dashboard_department_id = ? WHERE id = ?");
		$urStmnt->bind_param("siiii",
				$report['name'],
				$report['paper_size_id'],
				$report['dashboard_published'],
				$report['dashboard_department_id'],
				$report['id']);
		//, date_modified = GETDATE()
		$urStmnt->execute();
		

		$dltShapes = $this->conn->prepare
			("DELETE FROM gi_report_shapes WHERE report_id = ?");
		$dltShapes->bind_param('i', $report['id']);
		$dltShapes->execute();
		
				

		
		if(isset($report['shapes'])){
			foreach($report['shapes'] as $shape){
				$this->saveShape($report['id'], $shape);
			}
		}
		
		
		return  $report['id'];
	}
	
	
	public function deleteReport($reportID){
		$dltCharts = $this->conn->prepare
			("DELETE FROM gi_reports WHERE id = ?");
		$dltCharts->bind_param('i', $reportID);
		$dltCharts->execute();
	}
	

	public function saveObject($repID, $objects){
		
		$stringProperties = json_encode($objects);
		
		$objStatement = $this->conn->prepare
			("INSERT INTO gi_report_objects (report_id, ordinal, gi_properties, identifier_name) VALUES (?,?,?,?)");
		$objStatement->bind_param("iiss",
					$repID,
					$objects['Ordinal'],
					$stringProperties,
					$objects['Identifier Name']);
		$objStatement->execute();
	
	}
	
	

	public function getAllReports(){
		$rStatmnt = $this->conn->prepare
			("SELECT giRep.id, giRep.name, user.display_name as creator_name, giRep.date_created, giRep.date_modified FROM gi_reports as giRep
			 INNER JOIN tbuser as user
			 ON user.id = giRep.creator_id
			 Order By  giRep.date_created DESC, giRep.date_modified DESC");
		$rStatmnt->execute();
		
		$a =  $rStatmnt->get_result();
		return $a->fetch_all(MYSQLI_ASSOC);
	}
	
	
	public function getReportsDataTable($start,$limit, $sort, $sortDir, $search_value){
		
		
		$search_value = "%" . $search_value . "%";
		$rStatmnt = $this->conn->prepare
			("SELECT giRep.id, giRep.name as name, user.display_name as creator_name, giRep.date_created as date_created, giRep.date_modified as date_modified
			 FROM gi_reports as giRep
			 INNER JOIN tbuser as user
			 ON user.id = giRep.creator_id
			 WHERE name LIKE ? OR user.display_name LIKE ? OR giRep.date_created LIKE ? OR giRep.date_modified LIKE ?
			 Order By " . $sort . " " . $sortDir . " , date_modified DESC, date_created DESC
			 LIMIT ? OFFSET ?");
		$rStatmnt->bind_param("ssssii", $search_value, $search_value, $search_value, $search_value, $limit, $start);
		$rStatmnt->execute();
		
		$data =  $rStatmnt->get_result();
		$totalDisplay = $data->num_rows;
		$data = $data->fetch_all(MYSQLI_ASSOC);
		
		$cStatmnt = $this->conn->prepare("Select id FROM gi_reports");
		$cStatmnt->execute();
		$totalReprots = $cStatmnt->get_result()->num_rows;
		
		
		
		$report = [];
		
		
		
		
		$report['data'] = $data;
		$report['totalReports'] =  $totalReprots;
		$report['totalDisplayRecords'] = $totalDisplay;
		
		return $report;
	}
	
	public function getReportByDepartmentId($id){
		$repStmt = $this->conn->prepare
			("SELECT * FROM gi_reports WHERE dashboard_department_id = ? AND dashboard_published = 1");
		$repStmt->bind_param("i",$id);
		$repStmt->execute();
		$report = $repStmt->get_result()->fetch_assoc();
		
		//PaperSize	
		$psStmt = $this->conn->prepare
			("SELECT * FROM gi_paper_sizes WHERE id = ?");
		$psStmt->bind_param("i",$report['gi_paper_size_id']);
		$psStmt->execute();
		$paperSize = $psStmt->get_result()->fetch_assoc();
		
		$report['paper_size'] = $paperSize;
		
		$freport = array_merge($report, $this->getReportObjects($report['id']));
		
		return $freport;
	}
	
	
	public function getReportById($id){
		$repStmt = $this->conn->prepare
			("SELECT * FROM gi_reports WHERE id = ?");
		$repStmt->bind_param("i",$id);
		$repStmt->execute();
		$report = $repStmt->get_result()->fetch_assoc();
		
		//PaperSize	
		$psStmt = $this->conn->prepare
			("SELECT * FROM gi_paper_sizes WHERE id = ?");
		$psStmt->bind_param("i",$report['gi_paper_size_id']);
		$psStmt->execute();
		$paperSize = $psStmt->get_result()->fetch_assoc();
		
		$report['paper_size'] = $paperSize;
		
		$freport = array_merge($report, $this->getReportObjects($report['id']));
		
		return $freport;
		
	}
	

}


