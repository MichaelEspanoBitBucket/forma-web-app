<?php

class GiqFunctionsRepository
{
	private $conn = null;
	
	public function __construct($conn){
		$this->conn = $conn;
	}
	
	public function getAggregateFunctions(){
		$stm = $this->conn->prepare
			("select 
				cdt.id as commonDataTypeId, 
				f.name as functionName, 
				f.id as functionId,
				f.ordinal
			from gi_common_data_types as cdt 
			inner join gi_common_dt_aggregate_functions as dtf 
				on cdt.id = dtf.common_dt_id
			inner join gi_aggregate_functions as f
				on f.id = dtf.aggregate_function_id
			order by f.ordinal asc");
		$stm->execute();
		$result = $stm->get_result();
		$aggFunctions = [];
		
		while($aggFunc = $result->fetch_object()){
			$aggFunctions[] = $aggFunc;
		}
		
		return $aggFunctions;
	}
	
	public function getFunctions(){
		$stm = $this->conn->prepare
			("select 
				f.id as functionId, f.name as functionName, 
				cdt.id as commonDtId, cdt.name as commonDtName,
				cdtOut.id as outputCommonDtId, cdtOut.name as outputCommonDtName
			from gi_functions as f
			inner join gi_common_data_types as cdtOut
				on cdtOut.id = f.output_common_type_id
			inner join gi_common_dt_functions as dtf
				on f.id = dtf.function_id 
			inner join gi_common_data_types as cdt
				on cdt.id = dtf.common_dt_id
			order by f.name");
		$stm->execute();
		$result = $stm->get_result();
		$functions = [];
		
		while($func = $result->fetch_object()){
			$functions[] = $func;
		}
		
		return $functions;
	}
}