<?php


class ReportDesignerRepository
{
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}	
	
	public function getAllReports($params, $limit_start, $limit_end){
		$dsStatement = $this->conn->prepare
			("select id, name from gi_reports where company_id = ? order by name asc");
		$dsStatement->bind_param("i", $this->user["company_id"]);
		
		$dsStatement->execute();
		$dmResult = $dsStatement->get_result();	
	
		return $dmResult->fetch_all(MYSQLI_BOTH);
		
	}
	
	public function getAllPaperSizes(){
		$dsStatement = $this->conn->prepare
			("select id, name, ui_width, ui_height, orientation, unit from gi_paper_sizes order by name asc");
		
		$dsStatement->execute();
		$dmResult = $dsStatement->get_result();	
	
		return $dmResult->fetch_all(MYSQLI_BOTH);
		
	}

	public function getAllCharts(){
		$cStatement = $this->conn->prepare
			("SELECT ct.*, oc.name as category FROM gi_chart_types as ct
			 Left Join gi_object_categories as oc
			 ON oc.id = ct.category_id
			 ORDER BY ordinal");
		
		$cStatement->execute();
		$cResult = $cStatement->get_result();	
	
		return $cResult->fetch_all(MYSQLI_BOTH);
	}
	
	public function getAllShapeTypes(){
		$sStatement = $this->conn->prepare
			("SELECT st.*, oc.name as category FROM gi_shape_types as st
			 Left Join gi_object_categories as oc
			 ON oc.id = st.category_id
			 ORDER BY ordinal");
		
		$sStatement->execute();
		$sResult = $sStatement->get_result();	
	
		
	
		return $sResult->fetch_all(MYSQLI_BOTH);
	}
	
	public function getAllContainerTypes(){
		$conStatement = $this->conn->prepare
			("SELECT ct.*, oc.name as category FROM gi_container_types as ct
			 Left Join gi_object_categories as oc
			 ON oc.id = ct.category_id
			 ORDER BY ordinal");
		
		$conStatement->execute();
		$cResult = $conStatement->get_result();	
	
		return $cResult->fetch_all(MYSQLI_BOTH);
	}
	
	public function getAllOtherTypes(){
		$conStatement = $this->conn->prepare
			("SELECT ct.*, oc.name as category FROM gi_other_types as ct
			 Left Join gi_object_categories as oc
			 ON oc.id = ct.category_id
			 ORDER BY ordinal");
		
		$conStatement->execute();
		$cResult = $conStatement->get_result();	
	
		return $cResult->fetch_all(MYSQLI_BOTH);
	}

	public function getAllObjects(){
		$objStatement = $this->conn->prepare
			("SELECT ct.*, oc.name as category FROM gi_object_types as ct
			 Left Join gi_object_categories as oc
			 ON oc.id = ct.category_id
			 ORDER BY ordinal");
		
		$objStatement->execute();
		$cResult = $objStatement->get_result();	
	
		return $cResult->fetch_all(MYSQLI_BOTH);
		
	}

	

	public function getChartGiqSets($chartID){
		$dsStatement = $this->conn->prepare
			("SELECT cgs.id, cgs.name FROM `gi_chart_types` ct 
				JOIN `gi_chart_giq_sets` cgs   
				ON ct.id = cgs.chart_type_id
				Where ct.id = ? ORDER BY name");
		$dsStatement->bind_param("i", $chartID);
		$dsStatement->execute();
		$dmResult = $dsStatement->get_result();	
	
		return $dmResult->fetch_all(MYSQLI_BOTH);
	}	


	public function getChartGiqSetAreas($setID){
		$dsStatement = $this->conn->prepare
			("SELECT ga.* FROM `gi_chart_giq_sets` cgs   
				JOIN `gi_chart_giq_set_areas` cgsa
				ON cgs.id = cgsa.giq_set_id
				JOIN `gi_giq_areas` ga
				ON cgsa.giq_area_id = ga.id
				Where cgs.id = ? ORDER BY id");

		$dsStatement->bind_param("i", $setID);
		$dsStatement->execute();
		$dmResult = $dsStatement->get_result();	
	
		return $dmResult->fetch_all(MYSQLI_BOTH);
	}	

	public function getAllPositionLevel(){
		$objStatement = $this->conn->prepare
			("SELECT * FROM tbuser_position_level WHERE is_active = 1");
		
		$objStatement->execute();
		$cResult = $objStatement->get_result();	
	
		return $cResult->fetch_all(MYSQLI_BOTH);
		
	}

	
}