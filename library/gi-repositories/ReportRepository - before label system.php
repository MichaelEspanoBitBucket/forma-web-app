<?php

require_once("/library/gi-repositories/DataModelRepository.php");
require_once("/library/Phinq/bootstrap.php");

class ReportRepository
{
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}
	
	public function getChartGiqs($chartId){
		/*
		Query chart info
		*/
		
		$giqStm = $this->conn->prepare
			("select gq.id, gq.model_id as modelId
			from gi_report_charts as rch
			inner join gi_reports as r
				on rch.report_id = r.id
			inner join gi_chart_giqs as gq
				on rch.id = gq.chart_id
			where rch.id = ?");
		$giqStm->bind_param("i", $chartId);
		
		$giqStm->execute();
		$giqs = $giqStm->get_result();
		
		//query areas info
		//fields
		$giqId = null;
		$fieldStm = $this->conn->prepare
			("select gf.id, gf.giq_id as giqId, gf.ordinal, 
				gf.dm_ds_name as dsName, gf.dm_entity_name as dmEntityName, gf.dm_attribute_name as dmAttributeName,
				gf.ui_attribute_label as uiAttributeLabel, gf.ui_pair_id as uiPairId, gf.ui_attribute_id as uiAttributeId,
				dm.name as modelName, dm.id as modelId,
				ent.ds_entity_name as dsEntityName,
				attr.ds_attribute_name as dsAttributeName
			from gi_giq_fields as gf
			inner join gi_chart_giqs as giq
				on gf.giq_id = giq.id
			inner join gi_data_models as dm
				on giq.model_id = dm.id
			inner join gi_model_entities as ent
				on ent.model_id = dm.id and ent.name = gf.dm_entity_name
			inner join gi_entity_attributes as attr
				on ent.id = attr.entity_id and attr.name = gf.dm_attribute_name
			where gf.giq_id = ?
			order by gf.ordinal asc;");
		$fieldStm->bind_param("i", $giqId);
		//aggregates
		$aggStm = $this->conn->prepare
			("select gagg.id, gagg.giq_id as giqId, gagg.ordinal, 
				gagg.dm_ds_name as dsName, 
				gagg.dm_entity_name as dmEntityName, gagg.dm_attribute_name as dmAttributeName,
				gagg.ui_attribute_label as uiAttributeLabel, gagg.ui_pair_id as uiPairId, gagg.ui_attribute_id as uiAttributeId,
				dm.name as modelName, dm.id as modelId,
				gagg.aggregate_function_id as dsAggregateFunctionId, aggf.name as functionName,
				ent.ds_entity_name as dsEntityName,
				attr.ds_attribute_name as dsAttributeName
			from gi_giq_aggregates as gagg
			inner join gi_chart_giqs as giq
				on gagg.giq_id = giq.id
			inner join gi_aggregate_functions as aggf
				on gagg.aggregate_function_id = aggf.id
			inner join gi_data_models as dm
				on giq.model_id = dm.id
			inner join gi_model_entities as ent
				on ent.model_id = dm.id and ent.name = gagg.dm_entity_name
			inner join gi_entity_attributes as attr
				on ent.id = attr.entity_id and attr.name = gagg.dm_attribute_name
			where gagg.giq_id = ?
			order by gagg.ordinal asc");
		$aggStm->bind_param("i", $giqId);
		//groupings
		$grpStm = $this->conn->prepare
			("select grp.id, grp.giq_id as giqId, grp.ordinal, 
				grp.dm_ds_name as dsName, grp.dm_entity_name as dmEntityName, grp.dm_attribute_name as dmAttributeName,
				grp.ui_attribute_label as uiAttributeLabel, grp.ui_pair_id as uiPairId, grp.ui_attribute_id as uiAttributeId,
				dm.name as modelName, dm.id as modelId,
				ent.ds_entity_name as dsEntityName,
				attr.ds_attribute_name as dsAttributeName
			from gi_giq_groupings as grp
			inner join gi_chart_giqs as giq
				on grp.giq_id = giq.id
			inner join gi_data_models as dm
				on giq.model_id = dm.id
			inner join gi_model_entities as ent
				on ent.model_id = dm.id and ent.name = grp.dm_entity_name
			inner join gi_entity_attributes as attr
				on ent.id = attr.entity_id and attr.name = grp.dm_attribute_name
			where grp.giq_id = ?
			order by grp.ordinal asc;");
		$grpStm->bind_param("i", $giqId);
	
		$optGrpStm = $this->conn->prepare
			("select optGrp.id, optGrp.giq_id as giqId, optGrp.ordinal, 
				optGrp.dm_ds_name as dsName, optGrp.dm_entity_name as dmEntityName, optGrp.dm_attribute_name as dmAttributeName,
				optGrp.ui_attribute_label as uiAttributeLabel, optGrp.ui_pair_id as uiPairId, optGrp.ui_attribute_id as uiAttributeId,
				dm.name as modelName, dm.id as modelId,
				ent.ds_entity_name as dsEntityName,
				attr.ds_attribute_name as dsAttributeName
			from gi_giq_optional_groupings as optGrp
			inner join gi_chart_giqs as giq
				on optGrp.giq_id = giq.id
			inner join gi_data_models as dm
				on giq.model_id = dm.id
			inner join gi_model_entities as ent
				on ent.model_id = dm.id and ent.name = optGrp.dm_entity_name
			inner join gi_entity_attributes as attr
				on ent.id = attr.entity_id and attr.name = optGrp.dm_attribute_name
			where optGrp.giq_id = ?
			order by optGrp.ordinal asc;");
		$optGrpStm->bind_param("i", $giqId);
	
		$giqDetailed = [];
		//get area fields from each GIQ
		foreach($giqs as $giq){
			$giqId = $giq["id"];
			
			$fieldStm->execute();
			$fieldResult = $fieldStm->get_result()->fetch_all(MYSQLI_ASSOC);
			$aggStm->execute();
			$aggResult = $aggStm->get_result()->fetch_all(MYSQLI_ASSOC);
			$grpStm->execute();
			$grpResult = $grpStm->get_result()->fetch_all(MYSQLI_ASSOC);
			$optGrpStm->execute();
			$optGrpResult = $optGrpStm->get_result()->fetch_all(MYSQLI_ASSOC);
			
			$dmRepo = new DataModelRepository($this->conn, $this->user);
			$model = $dmRepo->getModel($giq["modelId"]);
			
			$giqDetailed[] = 
				[
					"fields" => $fieldResult,
					"aggregates" => $aggResult,
					"groupings" => $grpResult,
					"optionalGroupings" => $optGrpResult,
					"model" => $model,
					"modelId" => $model["id"]
				];
		}
		
		return $giqDetailed;
	}
	
	/*
	- get model
	- translate to native
	- get the cube 
	*/
	public function getChartGiqData($giq){
		/* 
		Translate to native
		*/
		
		$attributesPhinq = 
			Phinq\Phinq::create($giq["fields"])
			->union(Phinq\Phinq::create($giq["aggregates"])->toArray())
			->union(Phinq\Phinq::create($giq["groupings"])->toArray())
			->union(Phinq\Phinq::create($giq["optionalGroupings"])->toArray());
	
		$native = "select ";
		//fields
		$fields = Phinq\Phinq::create($giq["fields"])
			->select(function($i){ 
				return $i["dsEntityName"] . "." . $i["dsAttributeName"] . " as `" .
					$i["dsEntityName"] . " " . $i["dsAttributeName"] . "`"; })
			->toArray();
		$native = $native . " " . implode(", ", $fields) . ",";
		
		//aggregates
		$aggregates = Phinq\Phinq::create($giq["aggregates"])
			->select(function($i){ 
				return $i["functionName"] . "(" . $i["dsEntityName"] . "." . $i["dsAttributeName"] . ") " . 
					" as `" . $i["functionName"] . " of " . 
					$i["dsEntityName"] . " " . $i["dsAttributeName"] . "`"; })
			->toArray();
			
		$native = $native . " " . implode(", ", $aggregates) . " ";
		
		$native = trim($native, ",");
		
		//entities
		$entities = 
			Phinq\Phinq::create($giq["fields"])->select(function($i){ return $i["dsEntityName"]; })
			->union(Phinq\Phinq::create($giq["aggregates"])
				->select(function($i){ return $i["dsEntityName"]; })->toArray())
			->union(Phinq\Phinq::create($giq["groupings"])
				->select(function($i){ return $i["dsEntityName"]; })->toArray())
			->union(Phinq\Phinq::create($giq["optionalGroupings"])
				->select(function($i){ return $i["dsEntityName"]; })->toArray())
			->distinct()
			->toArray();
		
		$native = $native . "from " . implode(", ", $entities) . " ";
		
		//conditions
		$conditions = [];
		
		//join conditions
		$entityRelationships = Phinq\Phinq::create($giq["model"]["relationships"])
			->where(function($i) use($attributesPhinq){ 
				return 
					$attributesPhinq->any(function($j) use($i){ return $j["dmEntityName"] == $i["refEntityName"]; }) && 
					$attributesPhinq->any(function($j) use($i){ return $j["dmEntityName"] == $i["depEntityName"]; }); 
			})
			->select(function($er){ 
				return $er["refDsEntityName"].".".$er["refDsAttrName"] . " = " . 
				$er["depDsEntityName"].".".$er["depDsAttrName"]; })
			->toArray();
		
		$conditions = array_merge($conditions, $entityRelationships);
		
		if(count($conditions) > 0){
			$native = $native . "where " . implode(" and ", $conditions) . " ";
		}
		
		//groupings and optional groupings
		$allGroupings = Phinq\Phinq::create($giq["groupings"])
			->select(function($i){ return $i["dsEntityName"] . "." . $i["dsAttributeName"]; })
			->union(Phinq\Phinq::create($giq["optionalGroupings"])
				->select(function($i){ return $i["dsEntityName"] . "." . $i["dsAttributeName"]; })->toArray())
			->toArray();
		$giqNative = $native . "group by " . implode(", ", $allGroupings) . " with rollup";
		
		
		//execute query
		//var_dump($giqNative);
		$setStm = $this->conn->prepare($giqNative);
		$setStm->execute();
		$set = $setStm->get_result()->fetch_all(MYSQLI_ASSOC);
		
		return $set;
	}

	public function getChartInitialConfig($giq){
		
		return 
		[
			"aggregates" => Phinq\Phinq::create($giq["aggregates"])
				->select(function($g){ return $g["functionName"] . " of " . $g["dsEntityName"] . " " . $g["dsAttributeName"]; })
				->toarray(),
			"groupings" => Phinq\Phinq::create($giq["groupings"])
				->select(function($g){ return $g["dsEntityName"] . " " . $g["dsAttributeName"]; })
				->toarray(),
			"optionalGroupings" => Phinq\Phinq::create($giq["optionalGroupings"])
				->select(function($g){ return $g["dsEntityName"] . " " . $g["dsAttributeName"]; })
				->toarray()
		];
			
		/*
		config 
		- categories (list)
		- optionalCategories (list)
		- sliceOptions (list)*/
	}
	
	public function saveReport($report){
		
		$rStatement = $this->conn->prepare
			("INSERT INTO gi_reports (uid, name, creator_id, company_id, gi_paper_size_id) VALUES (?,?,?,?,?)");
		$rStatement->bind_param("ssiii", $report['uid'], $report['name'], $this->user["id"], $this->user["company_id"], $report['paper_size_id']);
		$rStatement->execute();
		$repID = $rStatement->insert_id;
		
		if(isset($report['charts'])){
			foreach($report['charts'] as $chart){
				$this->saveChartUI($repID, $chart);
			} 
		}
		
		if(isset($report['texts'])){
			foreach($report['texts'] as $text){
				$this->saveText($repID, $text);
			}
		}
				
	}
	
	
	public function updateReport($report){
		
		//echo  $report['id'];
		$urStmnt = $this->conn->prepare
			("UPDATE gi_reports SET name = ?, date_modified = NOW(), gi_paper_size_id = ? WHERE id = ?");
		$urStmnt->bind_param("sii",
				     $report['name'],
				     $report['paper_size_id'],
				     $report['id']);
		//, date_modified = GETDATE()
		$urStmnt->execute();
		
		$dltCharts = $this->conn->prepare
			("DELETE FROM gi_report_charts WHERE report_id = ?");
		$dltCharts->bind_param('i', $report['id']);
		$dltCharts->execute();
		
		
		$dltTexts = $this->conn->prepare
			("DELETE FROM gi_report_texts WHERE report_id = ?");
		$dltTexts->bind_param('i', $report['id']);
		$dltTexts->execute();
				
		if(isset($report['charts'])){
			foreach($report['charts'] as $chart){
				$this->saveChartUI($report['id'], $chart);
			} 
		}
		
		if(isset($report['texts'])){
			foreach($report['texts'] as $text){
				
				$this->saveText($report['id'], $text);
			}
		}
	}
	
	
	public function deleteReport($reportID){
		$dltCharts = $this->conn->prepare
			("DELETE FROM gi_reports WHERE id = ?");
		$dltCharts->bind_param('i', $reportID);
		$dltCharts->execute();
	}
	
	public function saveChartUI($repID, $chart){		
		$cuiStatement = $this->conn->prepare
			("INSERT INTO gi_report_charts (uid ,report_id, chart_type_id, ui_top, ui_left, ui_width, ui_height) VALUES (?,?,?,?,?,?,?)");
		$cuiStatement->bind_param("siidddd", $chart['uid'], $repID, $chart['type_id'],$chart['ui_top'], $chart['ui_left'], $chart['ui_width'],$chart['ui_height']);
		$cuiStatement->execute();
		$chartID = $cuiStatement->insert_id;
		
		if(isset($chart['giqs'])){
			foreach($chart['giqs'] as $giq){
				if(isset($giq['modelId'])){
					$this->saveChartGiqs($chartID, $giq);
				}
			} 
		}
		
	}
	
	public function saveChartGiqs($cID, $giq){
		
		$cgStatement = $this->conn->prepare
			("INSERT INTO gi_chart_giqs (chart_id, model_id) VALUES (?,?)");
		$cgStatement->bind_param("ii", $cID, $giq['modelId']);
		$cgStatement->execute();
		$giqID = $cgStatement->insert_id;
		
		if(isset($giq['fields'])){
			foreach($giq['fields'] as $field){
				$areaStmnt = $this->conn->prepare
				("INSERT INTO gi_giq_fields
					(giq_id, ordinal, dm_entity_name,
					dm_attribute_name, dm_ds_name, ui_attribute_label,
					ui_pair_id, ui_attribute_id)
				 VALUES
					(?,?,?,?,?,?,?,?)");
				$areaStmnt->bind_param("iissssss",
					       $giqID,
					       $field['ordinal'],
					       $field['dmEntityName'],
					       $field['dmAttributeName'],
					       $field['dmAttributeName'],
					       $field['uiAttributeLabel'],
					       $field['uiPairId'],
					       $field['uiAttributeId']);
				$areaStmnt->execute();
			}
			
		}
		$one = 1;
		if(isset($giq['aggregates'])){
			foreach($giq['aggregates'] as $aggregates){
				$areaStmnt = $this->conn->prepare
				("INSERT INTO gi_giq_aggregates
					(giq_id, ordinal, aggregate_function_id, dm_entity_name, dm_attribute_name, dm_ds_name, ui_attribute_label, ui_attribute_id)
				 VALUES
					(?,?,?,?,?,?,?,?)");
				$areaStmnt->bind_param("iiisssss",
					       $giqID,
					       $aggregates['ordinal'],
					       $aggregates['dsAggregateFunctionId'],	
					       $aggregates['dmEntityName'],
					       $aggregates['dmAttributeName'],
					       $aggregates['dsAttributeName'],
					       $aggregates['uiAttributeLabel'],
					       $aggregates['uiAttributeId']);
				$areaStmnt->execute();
			}
			
		}
		
		if(isset($giq['groupings'])){
			foreach($giq['groupings'] as $groupings){
				$areaStmnt = $this->conn->prepare
				("INSERT INTO gi_giq_groupings
					(giq_id, ordinal, dm_entity_name, dm_attribute_name, dm_ds_name, ui_attribute_label,
					ui_pair_id, ui_attribute_id)
				 VALUES
					(?,?,?,?,?,?,?,?)");
				$areaStmnt->bind_param("iissssss",
					       $giqID,
					       $groupings['ordinal'],
					       $groupings['dmEntityName'],
					       $groupings['dmAttributeName'],
					       $groupings['dsAttributeName'],
					       $groupings['uiAttributeLabel'],
					       $groupings['uiPairId'],
					       $groupings['uiAttributeId']);
				$areaStmnt->execute();
			}
			
		}
		
		if(isset($giq['optionalGroupings'])){
			foreach($giq['optionalGroupings'] as $optionalGroupings){
				$areaStmnt = $this->conn->prepare
				("INSERT INTO gi_giq_optional_groupings
					(giq_id, ordinal, dm_entity_name, dm_attribute_name, dm_ds_name, ui_attribute_label,
					ui_pair_id, ui_attribute_id)
				 VALUES
					(?,?,?,?,?,?,?,?)");
				$areaStmnt->bind_param("iissssss",
					       $giqID,
					       $optionalGroupings['ordinal'],
					       $optionalGroupings['dmEntityName'],
					       $optionalGroupings['dmAttributeName'],
					       $optionalGroupings['dsAttributeName'],
					       $optionalGroupings['uiAttributeLabel'],
					       $optionalGroupings['uiPairId'],
					       $optionalGroupings['uiAttributeId']);
				$areaStmnt->execute();
			}
		}	
	}
	
	
	public function saveText($repID, $text){
		$txtStatement = $this->conn->prepare
			("INSERT INTO gi_report_texts (uid , content, report_id, ui_top, ui_left, ui_width, ui_height) VALUES (?,?,?,?,?,?,?)");
		$txtStatement->bind_param("ssidddd",
					$text['uid'],
					$text['content'],
					$repID,
					$text['ui_top'],
					$text['ui_left'],
					$text['ui_width'],
					$text['ui_height']);
		$txtStatement->execute();
		
		//echo $txtStatement->insert_id;
		
	}
	

	public function getAllReports(){
		$rStatmnt = $this->conn->prepare
			("SELECT giRep.id, giRep.name, user.display_name as creator_name, giRep.date_created, giRep.date_modified FROM gi_reports as giRep
			 INNER JOIN tbuser as user
			 ON user.id = giRep.creator_id");
		$rStatmnt->execute();
		return $rStatmnt->get_result()->fetch_all(MYSQLI_ASSOC);
	}
	
	public function getReportById($id){
		$repStmt = $this->conn->prepare
			("SELECT id, name, creator_id, company_id, date_created, date_modified,
			gi_paper_size_id, gi_custom_width, gi_custom_height, uid FROM gi_reports WHERE id = ?");
		$repStmt->bind_param("i",$id);
		$repStmt->execute();
		$report = $repStmt->get_result()->fetch_assoc();
		
		//PaperSize	
		$psStmt = $this->conn->prepare
			("SELECT * FROM gi_paper_sizes WHERE id = ?");
		$psStmt->bind_param("i",$report['gi_paper_size_id']);
		$psStmt->execute();
		$paperSize = $psStmt->get_result()->fetch_assoc();
		
		$report['paper_size'] = $paperSize;
		
		//Charts		
		$chStmt = $this->conn->prepare
			("SELECT * FROM gi_report_charts WHERE report_id = ?");
		$chStmt->bind_param("i",$report['id']);
		$chStmt->execute();
		$charts = $chStmt->get_result()->fetch_all(MYSQLI_ASSOC);
		
		$report['charts'] = [];
		
		foreach($charts as $chart){
			$cType = $this->conn->prepare
			("SELECT * FROM gi_chart_types WHERE id = ?");
			$cType->bind_param("i",$chart['chart_type_id']);
			$cType->execute();
			$chart['chart_type'] = $cType->get_result()->fetch_assoc();
			
			
			$report['charts'][] = $chart;
		}
		
		//$report['paper']
		
		//Texts
		//Charts		
		$txtStmt = $this->conn->prepare
			("SELECT * FROM gi_report_texts WHERE report_id = ?");
		$txtStmt->bind_param("i",$report['id']);
		$txtStmt->execute();
		$texts = $txtStmt->get_result()->fetch_all(MYSQLI_ASSOC);
		
		$report['texts'] = [];
		
		foreach($texts as $text){		
			$report['texts'][] = $text;
		}
		
		return $report;
		
	}
}