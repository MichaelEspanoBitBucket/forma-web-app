<?php

require_once("/library/gi-repositories/DataModelRepository.php");
require_once("/library/Phinq/bootstrap.php");

class ReportRepository
{
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}
	
	public function getChartGiqDetails($chartId){
		/*
		Query chart info
		*/
		
		$giqStm = $this->conn->prepare
			("select gq.id, gq.model_id
			from gi_report_charts as rch
			inner join gi_reports as r
				on rch.report_id = r.id
			inner join gi_chart_giqs as gq
				on rch.id = gq.chart_id
			where rch.id = ?");
		$giqStm->bind_param("i", $chartId);
		
		$giqStm->execute();
		$giqs = $giqStm->get_result();
		
		//query areas info
		$giqId = null;
		$fieldStm = $this->conn->prepare
			("select gf.id, gf.giq_id, gf.ordinal, gf.dm_ds_name, gf.dm_entity_name, gf.dm_attribute_name,
				dm.name as model_name, dm.id as model_id,
				ent.ds_entity_name as ds_entity_name,
				attr.ds_attribute_name as ds_attribute_name
			from gi_giq_fields as gf
			inner join gi_chart_giqs as giq
				on gf.giq_id = giq.id
			inner join gi_data_models as dm
				on giq.model_id = dm.id
			inner join gi_model_entities as ent
				on ent.model_id = dm.id and ent.name = gf.dm_entity_name
			inner join gi_entity_attributes as attr
				on ent.id = attr.entity_id and attr.name = gf.dm_attribute_name
			where gf.giq_id = ?
			order by gf.ordinal asc;");
		$fieldStm->bind_param("i", $giqId);
		
		$aggStm = $this->conn->prepare
			("select gagg.id, gagg.giq_id, gagg.ordinal, gagg.dm_ds_name, gagg.dm_entity_name,gagg.dm_attribute_name,
				dm.name as model_name, dm.id as model_id,
				gagg.function_id, aggf.name as function_name,
				ent.ds_entity_name as ds_entity_name,
				attr.ds_attribute_name as ds_attribute_name
			from gi_giq_aggregates as gagg
			inner join gi_chart_giqs as giq
				on gagg.giq_id = giq.id
			inner join gi_aggregate_functions as aggf
				on gagg.function_id = aggf.id
			inner join gi_data_models as dm
				on giq.model_id = dm.id
			inner join gi_model_entities as ent
				on ent.model_id = dm.id and ent.name = gagg.dm_entity_name
			inner join gi_entity_attributes as attr
				on ent.id = attr.entity_id and attr.name = gagg.dm_attribute_name
			where gagg.giq_id = ?
			order by gagg.ordinal asc");
		$aggStm->bind_param("i", $giqId);
			
		$grpStm = $this->conn->prepare
			("select grp.id, grp.giq_id, grp.ordinal, grp.dm_ds_name, grp.dm_entity_name, grp.dm_attribute_name,
				dm.name as model_name, dm.id as model_id,
				ent.ds_entity_name as ds_entity_name,
				attr.ds_attribute_name as ds_attribute_name
			from gi_giq_groupings as grp
			inner join gi_chart_giqs as giq
				on grp.giq_id = giq.id
			inner join gi_data_models as dm
				on giq.model_id = dm.id
			inner join gi_model_entities as ent
				on ent.model_id = dm.id and ent.name = grp.dm_entity_name
			inner join gi_entity_attributes as attr
				on ent.id = attr.entity_id and attr.name = grp.dm_attribute_name
			where grp.giq_id = ?
			order by grp.ordinal asc;");
		$grpStm->bind_param("i", $giqId);
	
		$optGrpStm = $this->conn->prepare
			("select optGrp.id, optGrp.giq_id, optGrp.ordinal, optGrp.dm_ds_name, optGrp.dm_entity_name, optGrp.dm_attribute_name,
				dm.name as model_name, dm.id as model_id,
				ent.ds_entity_name as ds_entity_name,
				attr.ds_attribute_name as ds_attribute_name
			from gi_giq_optional_groupings as optGrp
			inner join gi_chart_giqs as giq
				on optGrp.giq_id = giq.id
			inner join gi_data_models as dm
				on giq.model_id = dm.id
			inner join gi_model_entities as ent
				on ent.model_id = dm.id and ent.name = optGrp.dm_entity_name
			inner join gi_entity_attributes as attr
				on ent.id = attr.entity_id and attr.name = optGrp.dm_attribute_name
			where optGrp.giq_id = ?
			order by optGrp.ordinal asc;");
		$optGrpStm->bind_param("i", $giqId);
	
		$giqDetailed = [];
		//get area fields from each GIQ
		foreach($giqs as $giq){
			$giqId = $giq["id"];
			
			$fieldStm->execute();
			$fieldResult = $fieldStm->get_result()->fetch_all(MYSQLI_ASSOC);
			$aggStm->execute();
			$aggResult = $aggStm->get_result()->fetch_all(MYSQLI_ASSOC);
			$grpStm->execute();
			$grpResult = $grpStm->get_result()->fetch_all(MYSQLI_ASSOC);
			$optGrpStm->execute();
			$optGrpResult = $optGrpStm->get_result()->fetch_all(MYSQLI_ASSOC);
			
			$dmRepo = new DataModelRepository($this->conn, $this->user);
			$model = $dmRepo->getModel($giq["model_id"]);
			
			$giqDetailed[] = 
				[
					"fields" => $fieldResult,
					"aggregates" => $aggResult,
					"groupings" => $grpResult,
					"optionalGroupings" => $optGrpResult,
					"model" => $model,
					"modelId" => $model["id"]
				];
		}
		
		return $giqDetailed;
	}
	
	/*
	- get model
	- translate to native
	- get the cube 
	*/
	public function getChartData($giqs){

		/* 
		Translate to native
		*/
		$giqNatives = [];
		foreach($giqs as $giq){
		
			$attributesPhinq = 
				Phinq\Phinq::create($giq["fields"])
				->union(Phinq\Phinq::create($giq["aggregates"])->toArray())
				->union(Phinq\Phinq::create($giq["groupings"])->toArray())
				->union(Phinq\Phinq::create($giq["optionalGroupings"])->toArray());
		
			$native = "select ";
			//fields
			$fields = Phinq\Phinq::create($giq["fields"])
				->select(function($i){ 
					return $i["ds_entity_name"] . "." . $i["ds_attribute_name"] . " as `" .
						$i["ds_entity_name"] . " " . $i["ds_attribute_name"] . "`"; })
				->toArray();
			$native = $native . " " . implode(", ", $fields) . ",";
			
			//aggregates
			$aggregates = Phinq\Phinq::create($giq["aggregates"])
				->select(function($i){ 
					return $i["function_name"] . "(" . $i["ds_entity_name"] . "." . $i["ds_attribute_name"] . ") " . 
						" as `" . $i["function_name"] . " of " . 
						$i["ds_entity_name"] . " " . $i["ds_attribute_name"] . "`"; })
				->toArray();
				
			$native = $native . " " . implode(", ", $aggregates) . " ";
			
			$native = trim($native, ",");
			
			//entities
			$entities = 
				Phinq\Phinq::create($giq["fields"])->select(function($i){ return $i["ds_entity_name"]; })
				->union(Phinq\Phinq::create($giq["aggregates"])
					->select(function($i){ return $i["ds_entity_name"]; })->toArray())
				->union(Phinq\Phinq::create($giq["groupings"])
					->select(function($i){ return $i["ds_entity_name"]; })->toArray())
				->union(Phinq\Phinq::create($giq["optionalGroupings"])
					->select(function($i){ return $i["ds_entity_name"]; })->toArray())
				->distinct()
				->toArray();
			
			$native = $native . "from " . implode(", ", $entities) . " ";
			
			//conditions
			$conditions = [];
			
			//join conditions
			$entityRelationships = Phinq\Phinq::create($giq["model"]["relationships"])
				->where(function($i) use($attributesPhinq){ 
					return 
						$attributesPhinq->any(function($j) use($i){ return $j["dm_entity_name"] == $i["refEntityName"]; }) && 
						$attributesPhinq->any(function($j) use($i){ return $j["dm_entity_name"] == $i["depEntityName"]; }); 
				})
				->select(function($er){ 
					return $er["refDsEntityName"].".".$er["refDsAttrName"] . " = " . 
					$er["depDsEntityName"].".".$er["depDsAttrName"]; })
				->toArray();
			
			$conditions = array_merge($conditions, $entityRelationships);
			
			if(count($conditions) > 0){
				$native = $native . "where " . implode(" and ", $conditions) . " ";
			}
			
			//groupings and optional groupings
			$allGroupings = Phinq\Phinq::create($giq["groupings"])
				->select(function($i){ return $i["ds_entity_name"] . "." . $i["ds_attribute_name"]; })
				->union(Phinq\Phinq::create($giq["optionalGroupings"])
					->select(function($i){ return $i["ds_entity_name"] . "." . $i["ds_attribute_name"]; })->toArray())
				->toArray();
			$native = $native . "group by " . implode(", ", $allGroupings) . " with rollup";
			
			$giqNatives[] = $native;
		}
		
		//TODO:Delete
		//print_r($giqNatives)
		
		//execute query
		$sets = [];
		foreach($giqNatives as $giqNative){
			$setStm = $this->conn->prepare($giqNative);
			$setStm->execute();
			$sets[] = $setStm->get_result()->fetch_all(MYSQLI_ASSOC);
		}
		
		return $sets;
	}

}