<?php
/*
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
*/
$path = $_SERVER["DOCUMENT_ROOT"];

	
include_once($path . "/library/gi-repositories/DataModelRepository.php");
include_once($path . "/library/Phinq/bootstrap.php");
use Phinq\Phinq as Phinq;
//install driver mysqlnd. for  get_result (PHP 5 >= 5.3.0) supported
//sudo apt-get install php5-mysqlnd
class ReportRepository
{
	
	private $conn = null;
	private $user = null;
	
	public function __construct($conn, $user){
		$this->conn = $conn;
		$this->user = $user;
	}

	public function saveReport($report){
		$this->conn->query("START TRANSACTION");
		$rStatement = $this->conn->prepare
			("INSERT INTO gi_reports (uid, name, creator_id, company_id, dashboard_published) VALUES (?,?,?,?,?)");
		$rStatement->bind_param("ssiis",
					$report['UID'],
					$report['Name'],
					$this->user["id"],
					$this->user["company_id"],
					$report['Dashboard Published']);
		$rStatement->execute();
		$repID = $rStatement->insert_id;
		
		if(isset($report['Objects'])){
			foreach($report['Objects'] as $object){
				$this->saveObject($repID, $object);
			}
		}

		if(isset($report['Dashboard Department IDs'])){
			foreach($report['Dashboard Department IDs'] as $depId){
				$this->saveDashboardDepartment($repID, $depId);
			}
		}

		if(isset($report['Dashboard Position IDs'])){
			foreach($report['Dashboard Position IDs'] as $depId){
				$this->saveDashboardPosition($repID, $depId);
			}
		}

		if(isset($report['Dashboard Group IDs'])){
			foreach($report['Dashboard Group IDs'] as $depId){
				$this->saveDashboardGroup($repID, $depId);
			}
		}
		$this->conn->query("COMMIT");
		return 	$repID;		
	}
	
	
	public function updateReport($report){
		
		//echo  $report['id'];

		// try {
			
		
		$this->conn->query("START TRANSACTION");
		$urStmnt = $this->conn->prepare
			("UPDATE gi_reports SET name = ?, date_modified = NOW(), dashboard_published = ?, dashboard_department_id = ? WHERE id = ?");
		$urStmnt->bind_param("sisi",
				$report['Name'],
				$report['Dashboard Published'],
				$report['Dashboard Department ID'],
				$report['ID']);
		$urStmnt->execute();
		

		$dltObjects = $this->conn->prepare
			("DELETE FROM gi_report_objects WHERE report_id = ?");
		$dltObjects->bind_param('i', $report['ID']);
		$dltObjects->execute();

		$dltDeps = $this->conn->prepare
			("DELETE FROM gi_report_dashboard_dep WHERE report_id = ?");
		$dltDeps->bind_param('i', $report['ID']);
		$dltDeps->execute();

		$dltPos = $this->conn->prepare
			("DELETE FROM gi_report_dashboard_position WHERE report_id = ?");
		$dltPos->bind_param('i', $report['ID']);
		$dltPos->execute();

		$dltGrps = $this->conn->prepare
			("DELETE FROM gi_report_dashboard_group WHERE report_id = ?");
		$dltGrps->bind_param('i', $report['ID']);
		$dltGrps->execute();
		
		//Save Objects
		if(isset($report['Objects'])){

			//str_repeat(".",13) " . str_repeat("(?,?,?,?),",count($report['Objects']) - 1) . "
			$strQuery = "INSERT INTO gi_report_objects (report_id, ordinal, gi_properties, identifier_name) VALUES (?,?,?,?)";
			//var_dump($strQuery);
			$objStatement = $this->conn->prepare($strQuery);

			
			 foreach($report['Objects'] as $object){
			 	$stringProperties = json_encode($object);
				$objStatement->bind_param("iiss",
							$report['ID'],
							$object['Ordinal'],
							$stringProperties,
							$object['Identifier Name']);
				$objStatement->execute();
			 }
			$objStatement->close();
			
			

			// foreach($report['Objects'] as $object){
			// 	$this->saveObject($report['ID'], $object);
			// }
		}

		if(isset($report['Dashboard Department IDs'])){
			foreach($report['Dashboard Department IDs'] as $depId){
				$this->saveDashboardDepartment($report['ID'], $depId);
			}
		}

		if(isset($report['Dashboard Position IDs'])){
			foreach($report['Dashboard Position IDs'] as $posId){
				$this->saveDashboardPosition($report['ID'], $posId);
			}
		}

		if(isset($report['Dashboard Group IDs'])){
			foreach($report['Dashboard Group IDs'] as $grpId){
				$this->saveDashboardGroup($report['ID'], $grpId);
			}
		}
		$this->conn->query("COMMIT");
		// } catch (Exception $e) {
		// 	$this->conn->query("ROLLBACK");
		// 	throw $e;
		// }
		return  $report['ID'];
	}
	
	
	public function deleteReport($reportID){
		$dltCharts = $this->conn->prepare
			("DELETE FROM gi_reports WHERE id = ?");
		$dltCharts->bind_param('i', $reportID);
		$dltCharts->execute();
	}
	

	public function saveObject($repID, $objects){
		
		$stringProperties = json_encode($objects);
		
		$objStatement = $this->conn->prepare
			("INSERT INTO gi_report_objects (report_id, ordinal, gi_properties, identifier_name) VALUES (?,?,?,?)");
		$objStatement->bind_param("iiss",
					$repID,
					$objects['Ordinal'],
					$stringProperties,
					$objects['Identifier Name']);
		$objStatement->execute();
	
	}

	public function saveSharedObject($object){
		//var_dump(expression)
		$stringProperties = json_encode($object['giProperties']);
		
		$objStatement = $this->conn->prepare
			("INSERT INTO gi_shared_objects (report_id, gi_properties, identifier_name, name) VALUES (?,?,?,?)");
		$objStatement->bind_param("isss",
					$object['Report ID'],
					$stringProperties,
					$object['giProperties']['Identifier Name'],
					$object['Name']);
		$objStatement->execute();
	
	}


	public function saveDashboardDepartment($repID, $depID){
		$objStatement = $this->conn->prepare
			("INSERT INTO gi_report_dashboard_dep (report_id, dashboard_department_id) VALUES (?,?)");
		$objStatement->bind_param("is",
					$repID,
					$depID);
		$objStatement->execute();
	}

	public function saveDashboardPosition($repID, $posID){
		$objStatement = $this->conn->prepare
			("INSERT INTO gi_report_dashboard_position (report_id, position_id) VALUES (?,?)");
		$objStatement->bind_param("is",
					$repID,
					$posID);
		$objStatement->execute();
	}

	public function saveDashboardGroup($repID, $grpID){
		$objStatement = $this->conn->prepare
			("INSERT INTO gi_report_dashboard_group (report_id, group_id) VALUES (?,?)");
		$objStatement->bind_param("is",
					$repID,
					$grpID);
		$objStatement->execute();
	}

	public function getAllReports(){
		$rStatmnt = $this->conn->prepare
			("SELECT giRep.id, giRep.name, user.display_name as creator_name, giRep.date_created, giRep.date_modified FROM gi_reports as giRep
			 INNER JOIN tbuser as user
			 ON user.id = giRep.creator_id
			 Order By  giRep.date_created DESC, giRep.date_modified DESC");
		$rStatmnt->execute();
		
		$a =  $rStatmnt->get_result();
		return $a->fetch_all(MYSQLI_ASSOC);
	}
	
	public function getReportsDataTable($start,$limit, $sort, $sortDir, $search_value){
		
		
		$search_value = "%" . $search_value . "%";
		$rStatmnt = $this->conn->prepare
			("SELECT giRep.id, giRep.name as name, user.display_name as creator_name, giRep.date_created as date_created, giRep.date_modified as date_modified
			 FROM gi_reports as giRep
			 INNER JOIN tbuser as user
			 ON user.id = giRep.creator_id
			 WHERE name LIKE ? OR user.display_name LIKE ? OR giRep.date_created LIKE ? OR giRep.date_modified LIKE ?
			 Order By " . $sort . " " . $sortDir . " , date_modified DESC, date_created DESC
			 LIMIT ? OFFSET ?");
		$rStatmnt->bind_param("ssssii", $search_value, $search_value, $search_value, $search_value, $limit, $start);
		$rStatmnt->execute();
		
		$data =  $rStatmnt->get_result();
		$totalDisplay = $data->num_rows;
		$data = $data->fetch_all(MYSQLI_ASSOC);
		
		$cStatmnt = $this->conn->prepare("Select id FROM gi_reports");
		$cStatmnt->execute();
		$totalReprots = $cStatmnt->get_result()->num_rows;
		
		
		
		$report = [];
		
		
		
		
		$report['data'] = $data;
		$report['totalReports'] =  $totalReprots;
		$report['totalDisplayRecords'] = $totalDisplay;
		
		return $report;
	}
	
	public function getReportByDepartmentId($id){
		$repStmt = $this->conn->prepare
			("SELECT dsDep.report_id as ReportID FROM gi_report_dashboard_dep as dsDep
				LEFT JOIN gi_reports as giRep
				On giRep.id = dsDep.report_id
				WHERE dsDep.dashboard_department_id = ? AND giRep.dashboard_published = 1");
		$repStmt->bind_param("s",$id);
		$repStmt->execute();
		$reportIDs = $repStmt->get_result()->fetch_all(MYSQLI_ASSOC);
		
		$freport = [];
		//var_dump($reportIDs);
			
			foreach($reportIDs as $repID){
				
				$freport[] = $this->getReportById($repID['ReportID']);
			}

		
		
		return $freport;
	}

	public function getReportByUserId($id){
		//$departments = $this->getUserDepartmentById($id);
		//var_dump($departments[0]);
		$freport = [];
		// $sqlQuery = "SELECT Distinct dsDep.report_id as ReportID FROM gi_report_dashboard_dep as dsDep
		// 		LEFT JOIN gi_reports as giRep On giRep.id = dsDep.report_id WHERE dsDep.dashboard_department_id in 
		// 			(SELECT DISTINCT department_code FROM tbdepartment_users WHERE is_active = 1 and user_id = ? ORDER By orgChart_id Desc) 
		// 		AND giRep.dashboard_published = 1";
		$sqlQuery = "Select DISTINCT ReportID From
					((SELECT dsDep.report_id as ReportID FROM gi_report_dashboard_dep as dsDep
					LEFT JOIN gi_reports as giRep On giRep.id = dsDep.report_id WHERE dsDep.dashboard_department_id in 
							(SELECT DISTINCT department_code FROM tbdepartment_users WHERE is_active = 1 and user_id = ? ORDER By orgChart_id Desc) 
					AND giRep.dashboard_published = 1)
					UNION ALL
					(SELECT dsPos.report_id as ReportID FROM gi_report_dashboard_position as dsPos
					LEFT JOIN gi_reports as giRep1 On giRep1.id = dsPos.report_id WHERE dsPos.position_id = 
							(SELECT position FROM tbuser WHERE id = ?) 
					AND giRep1.dashboard_published = 1)
					UNION ALL
					(SELECT dsGroup.report_id as ReportID FROM gi_report_dashboard_group as dsGroup
					LEFT JOIN gi_reports as giRep2 On giRep2.id = dsGroup.report_id WHERE dsGroup.group_id in 
							(SELECT group_id FROM tbform_groups_users WHERE is_active = 1 and user_id = ?) 
					AND giRep2.dashboard_published = 1)) as AllRep";
		$repStmt = $this->conn->prepare($sqlQuery);
		$repStmt->bind_param("sss",$id,$id,$id);
		$repStmt->execute();
		$reportIDs = $repStmt->get_result()->fetch_all(MYSQLI_ASSOC);



		foreach($reportIDs as $repID){
			$freport[] = $this->getReportById($repID['ReportID']);
		}

		
		//\var_dump($freport);
		return $freport;
	}

	public function getReportChartsByUserId($id){
		//$departments = $this->getUserDepartmentById($id);
		//var_dump($departments[0]);
		$freport = [];
		// $sqlQuery = "SELECT Distinct dsDep.report_id as ReportID FROM gi_report_dashboard_dep as dsDep
		// 		LEFT JOIN gi_reports as giRep On giRep.id = dsDep.report_id WHERE dsDep.dashboard_department_id in 
		// 			(SELECT DISTINCT department_code FROM tbdepartment_users WHERE is_active = 1 and user_id = ? ORDER By orgChart_id Desc) 
		// 		AND giRep.dashboard_published = 1";
		$sqlQuery = "Select DISTINCT ReportID From
					((SELECT dsDep.report_id as ReportID FROM gi_report_dashboard_dep as dsDep
					LEFT JOIN gi_reports as giRep On giRep.id = dsDep.report_id WHERE dsDep.dashboard_department_id in 
							(SELECT DISTINCT department_code FROM tbdepartment_users WHERE is_active = 1 and user_id = ? ORDER By orgChart_id Desc) 
					AND giRep.dashboard_published = 1)
					UNION ALL
					(SELECT dsPos.report_id as ReportID FROM gi_report_dashboard_position as dsPos
					LEFT JOIN gi_reports as giRep1 On giRep1.id = dsPos.report_id WHERE dsPos.position_id = 
							(SELECT position FROM tbuser WHERE id = ?) 
					AND giRep1.dashboard_published = 1)
					UNION ALL
					(SELECT dsGroup.report_id as ReportID FROM gi_report_dashboard_group as dsGroup
					LEFT JOIN gi_reports as giRep2 On giRep2.id = dsGroup.report_id WHERE dsGroup.group_id in 
							(SELECT group_id FROM tbform_groups_users WHERE is_active = 1 and user_id = ?) 
					AND giRep2.dashboard_published = 1)) as AllRep";
		$repStmt = $this->conn->prepare($sqlQuery);
		$repStmt->bind_param("sss",$id,$id,$id);
		$repStmt->execute();
		$reportIDs = $repStmt->get_result()->fetch_all(MYSQLI_ASSOC);


		$isfirst = true;
		foreach($reportIDs as $repID){
			if($isfirst){

				$freport = $this->getReportChartsById($repID['ReportID']);
				$isfirst = false;
			}else{
				$freport = array_merge($freport,$this->getReportChartsById($repID['ReportID']));
			}
			
			
			//$freport[] = $this->getReportChartsById($repID['ReportID']);
		}

		
		//\var_dump($freport);
		return $freport;
	}
	
	// public function getReportByActiveUser(){
		
	// 	$freport = [];
	// 	$sqlQuery = "Select DISTINCT ReportID From
	// 				((SELECT dsDep.report_id as ReportID FROM gi_report_dashboard_dep as dsDep
	// 				LEFT JOIN gi_reports as giRep On giRep.id = dsDep.report_id WHERE dsDep.dashboard_department_id in 
	// 						(SELECT DISTINCT department_code FROM tbdepartment_users WHERE is_active = 1 and user_id = ? ORDER By orgChart_id Desc) 
	// 				AND giRep.dashboard_published = 1)
	// 				UNION ALL
	// 				(SELECT dsPos.report_id as ReportID FROM gi_report_dashboard_position as dsPos
	// 				LEFT JOIN gi_reports as giRep1 On giRep1.id = dsPos.report_id WHERE dsPos.position_id = 
	// 						(SELECT position FROM tbuser WHERE id = ?) 
	// 				AND giRep1.dashboard_published = 1)
	// 				UNION ALL
	// 				(SELECT dsGroup.report_id as ReportID FROM gi_report_dashboard_group as dsGroup
	// 				LEFT JOIN gi_reports as giRep2 On giRep2.id = dsGroup.report_id WHERE dsGroup.group_id in 
	// 						(SELECT group_id FROM tbform_groups_users WHERE is_active = 1 and user_id = ?) 
	// 				AND giRep2.dashboard_published = 1)) as AllRep";
	// 	$repStmt = $this->conn->prepare($sqlQuery);
	// 	$repStmt->bind_param("sss",$id,$id,$id);
	// 	$repStmt->execute();
	// 	$reportIDs = $repStmt->get_result()->fetch_all(MYSQLI_ASSOC);



	// 	foreach($reportIDs as $repID){
	// 		$freport[] = $this->getReportById($repID['ReportID']);
	// 	}

		
	// 	//\var_dump($freport);
	// 	return $freport;
	// }

	public function getUserDepartmentById($userID){
		$depStmt = $this->conn->prepare
			("SELECT DISTINCT department_code FROM tbdepartment_users WHERE is_active = 1 and user_id = ? ORDER By orgChart_id Desc;");
		$depStmt->bind_param("i",$userID);
		$depStmt->execute();
		$dep = $depStmt->get_result()->fetch_all(MYSQLI_ASSOC);
		//echo "2";
		return $dep;
	}
	
	public function getReportById($id){
		$this->conn->query("START TRANSACTION");
		$repStmt = $this->conn->prepare
			("SELECT id as 'ID', name as 'Name', uid as 'UID', dashboard_published as 'Dashboard Published' FROM gi_reports WHERE id = ?");
		$repStmt->bind_param("i",$id);
		$repStmt->execute();
		$report = $repStmt->get_result()->fetch_assoc();
		
		//Report Objects
		$objStmt = $this->conn->prepare
			("SELECT * FROM gi_report_objects WHERE report_id = ? Order By ordinal ASC");
		$objStmt->bind_param("i",$id);
		$objStmt->execute();
		$Objects = $objStmt->get_result()->fetch_all(MYSQLI_ASSOC);
		
		$report['Objects'] = [];

		foreach($Objects as $object){
			$report['Objects'][] = json_decode($object['gi_properties']);
		}

		//Report Departments
		$depStmt = $this->conn->prepare
			("SELECT * FROM gi_report_dashboard_dep WHERE report_id = ?");
		$depStmt->bind_param("i",$id);
		$depStmt->execute();
		$DepIDs = $depStmt->get_result()->fetch_all(MYSQLI_ASSOC);
		
		$report['Dashboard Department IDs'] = [];
		foreach($DepIDs as $depid){
			$report['Dashboard Department IDs'][] = $depid['dashboard_department_id'];
		}

		//Report Position
		$posStmt = $this->conn->prepare
			("SELECT * FROM gi_report_dashboard_position WHERE report_id = ?");
		$posStmt->bind_param("i",$id);
		$posStmt->execute();
		$PosIDs = $posStmt->get_result()->fetch_all(MYSQLI_ASSOC);
		
		$report['Dashboard Position IDs'] = [];
		foreach($PosIDs as $posid){
			$report['Dashboard Position IDs'][] = $posid['position_id'];
		}
		
		//Group Position
		$grpStmt = $this->conn->prepare
			("SELECT * FROM gi_report_dashboard_group WHERE report_id = ?");
		$grpStmt->bind_param("i",$id);
		$grpStmt->execute();
		$GrpIDs = $grpStmt->get_result()->fetch_all(MYSQLI_ASSOC);
		
		$report['Dashboard Group IDs'] = [];
		foreach($GrpIDs as $grpid){
			$report['Dashboard Group IDs'][] = $grpid['group_id'];
		}
		$this->conn->query("COMMIT");
		return $report;
	}


	public function getReportChartsById($id){
		$this->conn->query("START TRANSACTION");
	
		//Report Objects
		$objStmt = $this->conn->prepare
			("SELECT * FROM gi_report_objects WHERE report_id = ? AND identifier_name in (Select identifier_name From gi_object_types where category_id = 1)  Order By ordinal ASC");
		$objStmt->bind_param("i",$id);
		$objStmt->execute();
		$Objects = $objStmt->get_result()->fetch_all(MYSQLI_ASSOC);
		
		$report['Objects'] = [];

		foreach($Objects as $object){
			$report['Objects'][] = json_decode($object['gi_properties']);
		}

		$this->conn->query("COMMIT");
		return $report["Objects"];
	}


	public function getSharedObjects(){
		
		$objStatement = $this->conn->prepare("SELECT * FROM gi_shared_objects Order By date_added desc");
		
		$objStatement->execute();
		$cResult = $objStatement->get_result();	
		
		$Objects = $cResult->fetch_all(MYSQLI_BOTH);
		$ObjectsOut = [];
		foreach($Objects as $object){
			$ObjectsOut[] = array("giProperties" => json_decode($object['gi_properties']),
				"ID" => $object["id"],
				"Date Added" => $object["date_added"],
				"Name" => $object["name"],
				"Identifier Name" => $object["identifier_name"]);
		}

		return $ObjectsOut;
	}
	

}


