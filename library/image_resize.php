<?php
class image_resize {
        
    public function create_thumbnail($path,$save,$width,$height){
        $info = getimagesize($path);
        $size = array($info[0], $info[1]);
        
        if($info['mime'] == 'image/png'){
            $src = imagecreatefrompng($path);
        }elseif($info['mime'] == 'image/jpeg'){
            $src = imagecreatefromjpeg($path);
        }elseif($info['mime'] == 'image/gif'){
            $src = imagecreatefromgif($path);
        }else{
            return false;
        }
        
        $thumb = imagecreatetruecolor($width,$height);
        
        $src_aspect = $size[0] / $size[1];
        $thumb_aspect = $width / $height;
        
        if($src_aspect < $thumb_aspect){
            // Narrower
            $scale = $width / $size[0];
            $new_size = array($width, $width / $src_aspect);
            $src_pos = array(0,($size[1] * $scale - $height) / $scale / 2);
        }elseif($src_aspect > $thumb_aspect){
            // Wider
            $scale = $height / $size[1];
            $new_size = array($height * $src_aspect, $height);
            $src_pos = array(($size[0] * $scale - $width) / $scale / 2, 0);
        }else{
            // Same Shape
            $new_size = array($width, $height);
            $src_pos = array(0,0);
        }
        
        $new_size[0] = max($new_size[0],1);
        $new_size[1] = max($new_size[1],1);
        
        imagecopyresampled($thumb,$src,0,0,$src_pos[0],$src_pos[1],$new_size[0],$new_size[1],$size[0],$size[1]);
        
        
        if($info['mime'] == 'image/png'){
            $img = imagepng($thumb,$save);
        }elseif($info['mime'] == 'image/jpeg'){
            $img = imagejpeg($thumb,$save);
        }elseif($info['mime'] == 'image/gif'){
            $img = imagegif($thumb,$save);
        }else{
            return false;
        }
        
        if($save === false){
            return false;
        }else{
            return $img;
        }
        
        imagedestroy($thumb);
    }
    
    public function image_reduce(){
        //if($last=="jpg" || $last=="jpeg" || $last=="JPG"){
        //    if($_POST['uploadType']=="postFileUpload"){
        //            $uploadedfile = $_FILES['postFile']['tmp_name'];
        //    }
        //    $src = imagecreatefromjpeg($uploadedfile);
        //}
        //else if($last=="png" || $last=="PNG"){
        //    if($_POST['uploadType']=="postFileUpload"){
        //            $uploadedfile = $_FILES['postFile']['tmp_name'];
        //    }
        //    $src = imagecreatefrompng($uploadedfile);
        //}
        //else{
        //    $src = imagecreatefromgif($uploadedfile);
        //}
        //echo $scr;
        //
        //list($width,$height)=getimagesize($uploadedfile);
        //
        //
        //// Set 1 (Large)
        //$newwidth=500;
        //
        //$newheight=($height/$width)*$newwidth;
        //$tmp=imagecreatetruecolor($newwidth,$newheight);
        //
        //imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
        //
        //$thumbnail = $dir."/large_".$filename.".png";
        //
        //imagejpeg($tmp,$thumbnail,100);
        //
        //
        //imagedestroy($tmp); // Large
    }
}


?>