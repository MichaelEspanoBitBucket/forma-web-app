<?php

class import {

    public static final function importFolderFiles($fileLocation) {
        $locFile = $fileLocation; // Path Location of the file to be uploaded in the system.
        $destdir = $locFile;

        //check if folder location is empty
        $handle = opendir($destdir);
        $c = 0;
        while ($file = readdir($handle) && $c < 3) {
            $c++;
        }

        if ($c > 2) {
            $dirs = scandir($locFile);
            foreach ($dirs as $filename) {

                if (($filename == '.') || ($filename == '..')) {
                    
                } elseif (is_dir($locFile . $filename)) {
                    //filesInDir($locFile.'|'.$filename);
                } else {

                    //$location = $locFile.'/'.$file; // Read the file Location
                    $extension = explode(".", $filename);


                    if ($extension[1] == "csv") {
                        $location = $locFile . $filename;
                        $loop = 0;
                        $fp = @fopen($location, 'r');

                        while (!feof($fp)) {

                            $dest = $_POST['location'];
                            $return_data = array();
                            while (($data = fgetcsv($fp, 1000, ",")) !== FALSE) {
                                array_push($return_data, $data);
                            }

                            $loc = $locFile;
                            $dest = "C:/wamp/www/GS3/Tupperware/gs3_tupperware/images/products/";

                            //        copy($loc . $filename, $dest . $filename); // If we copied this successfully, mark it for deletion
                            $fp++;
                            return $return_data;
                        }

                        fclose($fp);
                        //   unlink($loc . $filename); // Delete all successfully-copied files
                    } else {
                        return "Invalid File Format.";
                    }
                }
            }
        } else {

            return $locFile . " Folder location is empty.";
        }
    }

    public static final function importCSV($location) {
        $loop = 0;
        $fp = @fopen($location, 'r');

        $return_data = array();
        while (!feof($fp)) {
            $dest = $_POST['location'];

            while (($data = fgetcsv($fp, 100000, ",")) !== FALSE) {
                array_push($return_data, $data);
            }

            $fp++;
        }

        return $return_data;
        fclose($fp);
    }

    public static final function autoImportCSV($location) {
        //check if folder location is empty
            $handle = opendir($location);
            $locFile = $location;
            $c = 0;$outerLoop = 0;
            
            while ($file = readdir($handle)&& $c<3)
            {
                $c++;
            }
            
            $return_data2 = array();
            if ($c>2)
            {
                $ret = "";
                $dirs = scandir($locFile);
                if(!is_dir($location."\\imported_files")){
                    mkdir($location."\\imported_files", 0777);
                }

                // var_dump(is_dir($location."\\imported_files"));
                // print_r($dirs);
                foreach($dirs as $filename)
                {
                    
                    if (($filename == '.')||($filename == '..'))
                    {
                    }
                    elseif (is_dir($locFile.$filename))
                    {
                        //filesInDir($locFile.'|'.$filename);
                    }
                    else
                    {
                        $ext = explode(".", $filename);
                        $ext = $ext[count($ext)-1];
                        if($ext!="csv"){
                            continue;
                        }
                        $innerLoop = 0;
                        $fp = @fopen($location."\\".$filename, 'r');
                        while (!feof($fp)) {
                            $return_data = array();
                            while (($data = fgetcsv($fp, 100000, ",")) !== FALSE) {
                                //do not push the other headers
                                // if($innerLoop>0 || $outerLoop == 0){
                                    $return_data[] = $data;
                                    // array_push($return_data, $data);
                                // }
                                $innerLoop++;
                            }
                            array_push($return_data2,$return_data);
                            copy($location."\\".$filename, $location."\\imported_files\\".$filename);
                            $fp++;
                            $outerLoop++;
                        }
                        fclose($fp);
                        unlink($location."\\".$filename);
                    }
                }
            }
            return $return_data2;
        // return $dirs;
        
    }

}

?>