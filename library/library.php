<?php

class library {

    public function getTrackNo($db, $referencePrefix, $referenceType, $insert_result) {

        if ($referenceType == 'Sequential') {
            $trackNo = $referencePrefix . '%1$04d';

            return sprintf($trackNo, $insert_result);
        } else {
            $dateNow = $db->query('SELECT Now() as DateTimeNow', 'row');
            $date = new DateTime($dateNow['DateTimeNow']);

            $trackNo = $referencePrefix . date_format($date, '-ymd-himA');
            return $trackNo;
        }
    }

    // Li Encrypt String

    public function li_en_dec_method($action, $string) {
        $output = false;

        $key = 'FormalisticsGS3JDCGoFormsPS7GenerationOfMiracles';

        // initialization vector 
        $iv = md5(md5(md5($key)));

        if ($action == 'encrypt') {
            $output = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(md5(md5($key))), $string, MCRYPT_MODE_CBC, md5(md5(md5($iv))));
            $output = base64_encode($output);
            //$output = htmlspecialchars($output, ENT_QUOTES);
        } else if ($action == 'decrypt') {
            $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(md5(md5($key))), base64_decode($string), MCRYPT_MODE_CBC, md5(md5(md5($iv))));
            $output = rtrim($output);
            //$output = htmlspecialchars_decode($output);
        }
        return $output;
        //$plain_txt = "password";
        //echo "Encrypted Text = ".$fs->encrypt_decrypt('encrypt', $plain_txt)."\n";
        //echo "Decrypted Text = ".$fs->encrypt_decrypt('decrypt', $fs->encrypt_decrypt('encrypt', $plain_txt))."\n";
        //echo "\n";
    }
}

?>