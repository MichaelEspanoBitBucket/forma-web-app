<?php

/**
 * Description of Notification
 *
 * @author Ervinne Sodusta
 */
class Notification {

    public $id;
    public $type;
    public $notify_to_user_id;
    public $notification_by_user_id;
    public $is_read_by_user;
    public $date_notified;

    /**
     * The id of the record related to this notification. Ex. if this is a
     * notification about a new announcement, related_record_id is equal to
     * the id of that announcement.
     * @var int
     */
    public $related_record_id;

    /**
     * The table name of the record related to this notification. Ex. if this is
     * a notification about a new announcement, related_table_name is equal to
     * the table name of the announcements table (tb_message_board_announcements)
     * @var int 
     */
    public $related_table_name;
    public $is_active;
    public $comment_id;
    public $request_notification_message;

    public function __construct() {
        // default values
        $this->is_read_by_user = 0;
        $this->is_active       = 1;
        $this->$comment_id     = 0;        
    }

    // ===============================
    // notification types

    const TYPE_ANNOUNCEMENT = 15;
    const TYPE_SURVEY       = 16;

}
