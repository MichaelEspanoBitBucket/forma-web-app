<?php

include_once '/Notification.php';

/**
 * Description of NotificationsDAO
 *
 * @author Ervinne Sodusta
 */
class NotificationsDAO {

    /** @var APIDatabase */
    protected $database;

    /**
     * 
     * @param APIDatabase $database
     */
    public function __construct($database) {
        $this->database = $database;
    }

    /**
     * 
     * @param \Notification $notification
     * @return int
     */
    public function insertNotification($notification) {

        $insertable_row = $this->notificationToInsertableRow($notification);
        $resulting_id   = $this->database->insert("tbnotification", $insertable_row);

        return $resulting_id;
    }

    /**
     * 
     * @param \Notification $notification
     * @return Array
     */
    private function notificationToInsertableRow($notification) {

        $row = array(
            "noti_id" => $notification->related_record_id,
            "type" => $notification->type,
            "userID" => $notification->notify_to_user_id,
            "noti_by" => $notification->notification_by_user_id,
            "user_read" => $notification->is_read_by_user,
            "date" => $notification->date_notified,
            "table_name" => $notification->related_table_name,
            "is_active" => $notification->is_active,
            "is_read" => $notification->is_read_by_user,
            "commentID" => $notification->comment_id,
            "request_noti_message" => $notification->request_notification_message
        );

        return $row;
    }

}
