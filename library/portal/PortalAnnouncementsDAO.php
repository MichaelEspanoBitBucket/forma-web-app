<?php

/**
 * Description of MessageBoardAnnouncementDAO
 *
 * @author Ervinne Sodusta
 */
class PortalAnnouncementsDAO {

    /** @var APIDatabase */
    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    /**
     * @param PortalAnnouncement $announcement
     * @return int
     */
    public function insertAnnouncement($announcement) {

        $insert_row = $this->announcementToInsertableRow($announcement);
        $resulting_id = $this->database->insert("tb_message_board_announcements", $insert_row);

        return $resulting_id;
    }

    public function updateAnnouncement($announcement) {

        $update_row = $this->announcementToInsertableRow($announcement);
        $update_condition = array("id" => $announcement->id);

        $this->database->update("tb_message_board_announcements", $update_row, $update_condition);

        return $announcement->id;
    }

    /**  @param PortalAnnouncement $announcement */
    private function announcementToInsertableRow($announcement) {

        $row = array(
            "id" => $announcement->id,
            "text_content" => $announcement->text_content
        );

        return $row;
    }

}
