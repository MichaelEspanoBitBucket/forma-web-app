<?php

include_once API_LIBRARY_PATH . 'API.php';
include_once 'PortalPostDAO.php';
include_once 'PortalSurveysDAO.php';
include_once 'PortalUserDAO.php';
include_once 'PortalSurveysDAO.php';

/**
 * Description of MessageBoardPostDataReaderFacade
 *
 * @author Ervinne Sodusta
 */
class PortalDataReaderFacade {

    public function getPostsWithTotalCount($current_user, $filter_post_type, $from_index, $fetch_count, $other_cond) {

        $start_time = microtime(true);

        $posts_with_total_count = array();

        try {
            $database = new APIDatabase();

            $posts_dao = new PortalPostDAO($database);
            $surveys_dao = new PortalSurveysDAO($database);
            $users_dao = new PortalUserDAO($database);

            $database->connect();

            $posts_with_total_count["total_post_count"] = $posts_dao->getTotalPostCount($current_user, $filter_post_type);

            $posts = $this->getPostsFromOpenDAOs(
                    $posts_dao, $users_dao, $current_user, $filter_post_type, $from_index, $fetch_count, $other_cond
            );

            $post_id_list = $this->getPostIdListFromPosts($posts);

            $survey_selectable_responses = array();
            if (count($post_id_list) > 0) {
                $survey_selectable_responses = $surveys_dao->getSurveyListSelectableResponses($post_id_list);
            }

            $posts_with_total_count["posts"] = $this->assignSurveySelectableResponsesToPosts(
                    $posts, $survey_selectable_responses
            );

            $database->disconnect();
        } catch (Exception $e) {

            if ($database) {
                $database->rollback();
                $database->disconnect();
            }

            echo $e->getMessage();

            throw $e;
        }

        $posts_with_total_count["time_elapsed_secs"] = microtime(true) - $start_time;

        return $posts_with_total_count;
    }

    public function getPosts($current_user, $from_index, $fetch_count) {
        $database = new APIDatabase();
        return $this->getPostsFromOpenDatabase($database, $current_user, $from_index, $fetch_count);
    }

    public function getSurveys($current_user) {
        try {

            $database = new APIDatabase();
            $survey_dao = new PortalSurveysDAO($database);
            $users_dao = new PortalUserDAO($database);

            $database->connect();

            $current_user["groups"] = $users_dao->getUserGroupList($current_user["id"]);
            $current_user["departments"] = $users_dao->getUserDepartmentList($current_user["id"]);
            $announcements = $survey_dao->getSurveys(
                    $current_user
            );

            $database->disconnect();

            return $announcements;
        } catch (Exception $e) {

            if ($database) {
                $database->rollback();
                $database->disconnect();
            }

            throw $e;
        }

        return array();
    }

    private function getPostIdListFromPosts($posts) {
        $post_id_list = array();

        foreach ($posts AS $post) {
            array_push($post_id_list, $post["id"]);
        }

        return $post_id_list;
    }

    private function assignFollowersToPosts($posts, $followers) {

        for ($pi = 0; $pi < count($posts); $pi ++) {

            for ($fi = 0; $fi < count($followers); $fi ++) {
                
            }
        }
    }

    private function assignSurveySelectableResponsesToPosts($posts, $survey_selectable_responses) {

        if (count($survey_selectable_responses) > 0) {
            for ($pi = 0; $pi < count($posts); $pi ++) {
                if (!array_key_exists("survey_selectable_responses", $posts[$pi])) {
                    $posts[$pi]["survey_selectable_responses"] = array();
                }

                for ($ri = 0; $ri < count($survey_selectable_responses); $ri ++) {
                    if ($survey_selectable_responses[$ri]["survey_id"] == $posts[$pi]["id"]) {
                        $posts[$pi]["survey_selectable_responses"][$ri] = $survey_selectable_responses[$ri];
                    }
                }
            }
        }

        return $posts;
    }

    private function getPostsFromOpenDAOs($posts_dao, $users_dao, $current_user, $filter_post_type, $from_index, $fetch_count, $other_cond) {

        $current_user["groups"] = $users_dao->getUserGroupList($current_user["id"]);
        $current_user["departments"] = $users_dao->getUserDepartmentList($current_user["id"]);

        $posts = $posts_dao->getPosts(
                $current_user, $filter_post_type, $from_index, $fetch_count, $other_cond
        );

        return $posts;
    }

}
