<?php

include_once API_LIBRARY_PATH . 'API.php';
include_once '/PortalPostDAO.php';
include_once '/PortalSurveysDAO.php';
include_once '/PortalUserDAO.php';
include_once '/PortalSurveysDAO.php';

/**
 * Description of MessageBoardPostDataReaderFacade
 *
 * @author Ervinne Sodusta
 */
class PortalDataReaderFacade {

    public function getPostsWithTotalCount($current_user, $filter_post_type, $from_index, $fetch_count) {

        $posts_with_total_count = array();

        try {
            $database = new APIDatabase();

            $posts_dao = new PortalPostDAO($database);
            $surveys_dao = new PortalSurveysDAO($database);
            $users_dao = new PortalUserDAO($database);

            $database->connect();

            $posts_with_total_count["total_post_count"] = $posts_dao->getTotalPostCount($current_user, $filter_post_type);

            $posts_with_total_count["posts"] = $this->getPostsFromOpenDAOs(
                    $posts_dao, $users_dao, $current_user, $filter_post_type, $from_index, $fetch_count
            );

            $post_id_list = $this->getPostsIdList($posts_with_total_count["posts"]);



            $database->disconnect();
        } catch (Exception $e) {

            if ($database) {
                $database->rollback();
                $database->disconnect();
            }

            echo $e->getMessage();

            throw $e;
        }

        return $posts_with_total_count;
    }

    public function getPosts($current_user, $from_index, $fetch_count) {
        $database = new APIDatabase();
        return $this->getPostsFromOpenDatabase($database, $current_user, $from_index, $fetch_count);
    }

    public function getSurveys($current_user) {
        try {

            $database = new APIDatabase();
            $survey_dao = new PortalSurveysDAO($database);
            $users_dao = new PortalUserDAO($database);

            $database->connect();

            $current_user["groups"] = $users_dao->getUserGroupList($current_user["id"]);
            $current_user["departments"] = $users_dao->getUserDepartmentList($current_user["id"]);
            $announcements = $survey_dao->getSurveys(
                    $current_user
            );

            $database->disconnect();

            return $announcements;
        } catch (Exception $e) {

            if ($database) {
                $database->rollback();
                $database->disconnect();
            }

            throw $e;
        }

        return array();
    }

    private function getPostsFromOpenDAOs($posts_dao, $users_dao, $current_user, $filter_post_type, $from_index, $fetch_count) {

        $current_user["groups"] = $users_dao->getUserGroupList($current_user["id"]);
        $current_user["departments"] = $users_dao->getUserDepartmentList($current_user["id"]);

        $posts = $posts_dao->getPosts(
                $current_user, $filter_post_type, $from_index, $fetch_count
        );

        return $posts;
    }

    private function getPostsIdList($posts) {

        $id_list = array();

        foreach ($posts AS $post) {
            array_push($id_list, $post["id"]);
        }

        return $id_list;
    }

}
