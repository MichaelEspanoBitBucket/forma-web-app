<?php

include_once API_LIBRARY_PATH . 'API.php';

include_once 'PortalPostDAO.php';
include_once 'PortalPostCommentsDAO.php';
include_once 'PortalAnnouncementsDAO.php';
include_once 'PortalSurveysDAO.php';
include_once 'PortalPostFollowersDAO.php';

include_once 'PortalPostDataExtractor.php';

include_once '/../notification/NotificationsDAO.php';

/**
 * Description of PortalAnnouncementDAOFacade
 *
 * @author Ervinne Sodusta
 */
class PortalDataWriterFacade {

    /** @param PortalAnnouncement $announcement */
    public function saveAnnouncement($announcement) {

        $saved_announcement = $announcement;

        $saved_announcement->text_content = PortalPostDataExtractor::extractImages(
                        $saved_announcement->text_content
        );

        $database = new APIDatabase();

        try {
            $database->connect();
            $database->beginTransaction();

            $message_board_post_dao           = new PortalPostDAO($database);
            $message_board_announcement_dao   = new PortalAnnouncementsDAO($database);
            $message_board_post_followers_dao = new PortalPostFollowersDAO($database);
//            $notifications_dao                = new NotificationsDAO($database);

            if ($announcement->id == null || $announcement->id == 0) {
                // save the message board post and assign a new id using the resulting id
                $saved_announcement->id = $message_board_post_dao->insertPost($saved_announcement);
                $message_board_announcement_dao->insertAnnouncement($saved_announcement);
            } else {
                $message_board_post_dao->updatePost($announcement);
                $message_board_announcement_dao->updateAnnouncement($announcement);
            }

            if ($saved_announcement->visibility_type == 3) { // Custom visibility
                $message_board_post_followers_dao->clearFollowers($saved_announcement->id);
                $message_board_post_followers_dao->insertFollowers(
                        $saved_announcement->id, $saved_announcement->followers
                );
            }

            $database->commit();
            $database->disconnect();
        } catch (Exception $e) {

            if ($database) {
                $database->rollback();
                $database->disconnect();
            }

            throw $e;
        }

        return $saved_announcement;
    }

    public function saveSurvey($survey) {
        $saved_survey = null;

        $database = new APIDatabase();

        try {
            $database->connect();
            $database->beginTransaction();

            $message_board_post_dao           = new PortalPostDAO($database);
            $message_board_survey_dao         = new PortalSurveysDAO($database);
            $message_board_post_followers_dao = new PortalPostFollowersDAO($database);

            // save the message board post and assign a new id using the resulting id
            if ($survey->id == null || $survey->id == 0) {
                $survey->id = $message_board_post_dao->insertPost($survey);
                $message_board_survey_dao->insertSurvey($survey);
            } else {
                $message_board_post_dao->updatePost($survey);
                $message_board_survey_dao->updateSurvey($survey);
            }

            if ($survey->visibility_type == 3) { // Custom visibility                
                $message_board_post_followers_dao->clearFollowers($survey->id);
                $message_board_post_followers_dao->insertFollowers(
                        $survey->id, $survey->followers
                );
            }

            foreach ($survey->survey_responses as $response) {
                if ($response["id"] == 0 || $response["id"] == null) {
                    $message_board_survey_dao->insertSurveyResponseSelectablePost($response, $survey->id);
                } else {
                    // $message_board_survey_dao->updateSurveyResponseSelectablePost($response, $survey->id);
                    if ($response["text"] == "" || $response["text"] == null) {
                        $message_board_survey_dao->deleteSurveyResponseSelectablePost($response, $survey->id);
                    } else {
                        $message_board_survey_dao->updateSurveyResponseSelectablePost($response, $survey->id);
                    }
                }
            }

            $database->commit();

            $saved_survey = $survey;

            $database->disconnect();
        } catch (Exception $e) {

            if ($database) {
                $database->rollback();
                $database->disconnect();
            }

            throw $e;
        }

        return $saved_survey;
    }  

    public function saveSurveyResponses($survey) {
        $saved_survey = null;

        $database = new APIDatabase();

        try {
            $database->connect();
            $database->beginTransaction();

            // $message_board_post_dao   = new MessageBoardPostDAO($database);
            $message_board_survey_res_dao = new PortalSurveysDAO($database);


            //Remove past responses
            $message_board_survey_res_dao->deleteSurveyResponseUserPost($survey->survey_id, $survey->responder_id);

            foreach ($survey->survey_responses as $response) {
                $message_board_survey_res_dao->insertSurveyResponsePost($response, $survey->survey_id, $survey->responder_id);
            }

            $database->commit();

            $saved_survey = $survey;

            $database->commit();
        } catch (Exception $e) {

            if ($database) {
                $database->rollback();
                $database->disconnect();
            }

            throw $e;
        }

        return $saved_survey;
    }

    public function updateSurveyPublish($survey){
        $saved_survey = null;

        $database = new APIDatabase();

        try {
            $database->connect();
            $database->beginTransaction();

            
            $message_board_survey_dao         = new PortalSurveysDAO($database);
           

            // save the message board post and assign a new id using the resulting id
          
            $message_board_survey_dao->updateSurveyPublish($survey);
            

            $database->commit();

            $saved_survey = $survey;

            $database->disconnect();
        } catch (Exception $e) {

            if ($database) {
                $database->rollback();
                $database->disconnect();
            }

            throw $e;
        }

        return $saved_survey;
    }


    /** @param PortalPostComment $comment */
    public function savePostComment($comment) {
        $database      = new APIDatabase();
        $saved_comment = array();

        try {
            $database->connect();
            $database->beginTransaction();

            $post_comments_dao = new PortalPostCommentsDAO($database);
            $posts_dao         = new PortalPostDAO($database);

            $saved_comment = $post_comments_dao->writeComment($comment);
            $posts_dao->updatePostDateLastCommented($comment->post_id);

            $database->commit();
            $database->disconnect();
        } catch (Exception $e) {

            if ($database) {
                $database->rollback();
                $database->disconnect();
            }

            throw $e;
        }

        return $saved_comment;
    }

}
