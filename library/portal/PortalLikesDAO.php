<?php

/**
 * Description of PortalLikesDAO
 *
 * @author Ervinne Sodusta
 */
class PortalLikesDAO {

    const LIKE_POST    = 1;
    const LIKE_COMMENT = 2;

    /** @var APIPDatabase */
    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    public function likePost($userId, $postId) {

        if (!$this->isPostLiked($userId, $postId)) {
            $row = array(
                'user_id' => $userId,
                'post_id' => $postId,
                'comment_id' => 0,
                'date_liked' => date("Y-m-d H:i:s"),
            );

            $this->database->insert('tb_message_board_likes', $row);
        } else {
            throw new APIException("Post is already liked");
        }
    }

    public function likeComment($userId, $commentId) {

        if (!$this->isCommentLiked($userId, $commentId)) {
            $row = array(
                'user_id' => $userId,
                'post_id' => 0,
                'comment_id' => $commentId,
                'date_liked' => date("Y-m-d H:i:s"),
            );
            $this->database->insert('tb_message_board_likes', $row);
        } else {
            throw new APIException("Comment is already liked");
        }
    }

    public function removePostLike($userId, $postId) {

        $conditions = array(
            'user_id' => $userId,
            'post_id' => $postId
        );
        $this->database->delete('tb_message_board_likes', $conditions);
    }

    public function removeCommentLike($userId, $commentId) {

        $conditions = array(
            'user_id' => $userId,
            'comment_id' => $commentId
        );
        $this->database->delete('tb_message_board_likes', $conditions);
    }

    public function isPostLiked($userId, $postId) {

        $row = $this->database->query("SELECT 
                    *
                FROM
                    tb_message_board_likes
                WHERE
                    user_id = {$userId} AND post_id = {$postId}", APIDatabase::QUERY_FETCH_ROW);
        return $row != null;
    }

    public function isCommentLiked($userId, $commentId) {

        $row = $this->database->query("SELECT 
                    *
                FROM
                    tb_message_board_likes
                WHERE
                    user_id = {$userId} AND comment_id = {$commentId}", APIDatabase::QUERY_FETCH_ROW);
        return $row != null;
    }

}
