<?php

/**
 * Description of Post
 *
 * @author Ervinne Sodusta
 */
class PortalPost {

    const TYPE_ANNOUNCEMENT = 1;
    const TYPE_SURVEY = 2;
    const TYPE_FORM_REQUEST = 3;

    public $id;
    public $author_id;
    public $date_last_commented;
    public $date_posted;
    public $date_updated;

    /**
     * @var int
     * 1=PUBLIC
     * 2=COMPANY
     * 3=CUSTOM

     */
    public $visibility_type;

    /**
     * @var int
     * 
     * See TYPE_* constants   
     */
    public $type;

    /** @var Array */
    public $followers;

    public function __construct() {
        $this->id = 0;
    }

}
