<?php

/**
 * Description of PortalPostComment
 *
 * @author Ervinne Sodusta
 */
class PortalPostComment {

    public $id;
    public $author_id;
    public $date_posted;
    public $post_id;
    public $reply_to_comment_id;
    public $text_content;

    public function __construct() {
        $this->id = 0;
        $this->reply_to_comment_id = 0;
    }

}
