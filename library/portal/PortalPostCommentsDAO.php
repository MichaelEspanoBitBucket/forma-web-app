<?php

/**
 * Description of PortalPostCommentsDAO
 *
 * @author Ervinne Sodusta
 */
class PortalPostCommentsDAO {

    /** @param APIDatabase $database */
    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    /** @param PortalPostComment $post_comment */
    public function writeComment($post_comment) {

        $insert_row = $this->commentToInsertableRow($post_comment);

        try {
            $result_id = $this->database->insert("tb_message_board_post_comments", $insert_row);
        } catch (Exception $e) {
            echo $e->getMessage();
            throw $e;
        }

        return $result_id;
    }

    public function deleteComment($comment_id, $current_user) {

        $delete_condition = array(
            "id" => $comment_id
        );

        if ($current_user["user_level_id"] > 2) { // users & guests
            $delete_condition["author_id"] = $current_user["id"];
        }

        return $this->database->delete("tb_message_board_post_comments", $delete_condition);
    }

    public function getPostCommentCount($post_id) {

        $query = "SELECT COUNT(id) AS comment_count FROM tb_message_board_post_comments WHERE post_id = {$post_id}";
        $row   = $this->database->query($query, "row");
        return $row["comment_count"];
    }

    public function getPostComments($post_id, $current_user_id, $from_index, $fetch_count) {

        $query = "SELECT 
                        comments.*,
                        users.display_name AS author_display_name,
                        users.extension AS author_image_extension,
                        (SELECT 
                                mood
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.comment_id = comments.id
                                    AND moods.user_id = {$current_user_id}) user_mood,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.comment_id = comments.id
                                    AND moods.user_id != {$current_user_id}
                                    AND moods.mood = 'Happy') happy_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.comment_id = comments.id
                                    AND moods.user_id != {$current_user_id}
                                    AND moods.mood = 'Inspired') inspired_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.comment_id = comments.id
                                    AND moods.user_id != {$current_user_id}
                                    AND moods.mood = 'Sad') sad_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.comment_id = comments.id
                                    AND moods.user_id != {$current_user_id}
                                    AND moods.mood = 'Angry') angry_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.comment_id = comments.id
                                    AND moods.user_id != {$current_user_id}
                                    AND moods.mood = 'Annoyed') annoyed_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.comment_id = comments.id
                                    AND moods.user_id != {$current_user_id}
                                    AND moods.mood = 'Don\'t Care') dont_care_users_count
                    FROM
                        tb_message_board_post_comments comments
                            LEFT JOIN
                        tbuser users ON comments.author_id = users.id
                    WHERE
                        post_id = {$post_id}
                    ORDER BY date_posted DESC
                    LIMIT {$from_index} , {$fetch_count}";

//        echo $query;

        return $this->database->query($query);
    }

    /** @param PortalPostComment $post_comment */
    private function commentToInsertableRow($post_comment) {

        $row = array(
            "author_id" => $post_comment->author_id,
            "text_content" => $post_comment->text_content,
            "post_id" => $post_comment->post_id,
            "reply_to_comment_id" => $post_comment->reply_to_comment_id,
            "date_posted" => $post_comment->date_posted
        );

        if ($post_comment->id != null && $post_comment->id >= 0) {
            $row["id"] = $post_comment->id;
        }

        return $row;
    }

}
