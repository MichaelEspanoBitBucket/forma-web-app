<?php

/**
 * Description of PortalPostDAO
 *
 * @author Ervinne Sodusta
 */
class PortalPostDAO {

    /** @var APIDatabase */
    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    public function getTotalPostCount($current_user, $filter_post_type) {

        $group_condition_clause      = "";
        $department_condition_clause = "";
        $position_condition_clause   = "";

        if (count($current_user["groups"]) > 0) {
            $user_groups_joined     = join(",", $current_user["groups"]);
            $group_condition_clause = "OR (follower.follower_type = 2 AND find_in_set(follower.follower, '{$user_groups_joined}'))";
        }

        if (count($current_user["departments"]) > 0) {
            $user_departments_joined     = join(",", $current_user["departments"]);
            $department_condition_clause = "OR (follower.follower_type = 3 AND find_in_set(follower.follower, '{$user_departments_joined}'))";
        }

        if (trim($current_user["position"]) != "") {
            $position_condition_clause = "OR (follower.follower_type = 4 AND follower.follower = {$current_user["position"]})";
        }

        if ($filter_post_type && trim($filter_post_type) != "") {
            $filter_clause = "post.type = {$filter_post_type} AND ";
        }

        $custom_visibility_condition_clause = "";

        // Apply visibility type 3 (custom) to non admins
        if ($current_user["user_level_id"] == 3) {
            $custom_visibility_condition_clause = " AND (
                        author.id = {$current_user["id"]}
                        OR (follower.follower_type = 1 AND follower.follower = {$current_user["id"]})
                        {$group_condition_clause}
                        {$department_condition_clause}
                        {$position_condition_clause}                        
                    )";
        }

        $query = "SELECT 
                        COUNT(DISTINCT(post.id)) AS post_count
                    FROM
                        tb_message_board_posts post
                            LEFT JOIN 
                        tbuser author ON post.author_id = author.id
                            LEFT JOIN
                        tb_message_board_post_followers follower ON post.id = follower.post_id
                WHERE
                    {$filter_clause}
                    (visibility_type_id = 1
                    OR (visibility_type_id = 2 AND author.company_id = {$current_user["company_id"]})
                    OR (
                        visibility_type_id = 3 
                        AND author.company_id = {$current_user["company_id"]} 
                        {$custom_visibility_condition_clause})
                    )";

//        echo $query;

        $result_row = $this->database->query($query, "row");
        return $result_row["post_count"];
    }

    public function getPostListFollowers($post_id_list) {

        $post_id_list_csv = join(",", $post_id_list);

        $query = "SELECT 
                    post_id,
                    f.id,
                    follower,
                    follower_display_name,
                    enable_notification,
                    follower_type,
                    u.extension AS image_extension
                FROM
                    tb_message_board_post_followers f
                        LEFT JOIN
                    tbuser u ON u.id = f.follower
                WHERE
                    post_id IN ({$post_id_list_csv})
                ORDER BY post_id;";

        return $this->database->query($query);
    }

    public function getPostFollowers($post_id) {

        $post_id_list = array();
        array_push($post_id_list, $post_id);

        return $this->getPostListFollowers($post_id_list);
    }

    public function getPosts($current_user, $filter_post_type, $from_index, $fetch_count, $other_cond) {
        /*
            $other_cond = array(array("field"=>"","operator"=>"=","value"=>""));
        */

        $group_condition_clause             = "";
        $department_condition_clause        = "";
        $position_condition_clause          = "";
        $filter_clause                      = "";
        $custom_visibility_condition_clause = "";

        if (count($current_user["groups"]) > 0) {
            $user_groups_joined     = join(",", $current_user["groups"]);
            $group_condition_clause = "OR (follower.follower_type = 2 AND find_in_set(follower.follower, '{$user_groups_joined}'))";
        }

        if (count($current_user["departments"]) > 0) {
            $user_departments_joined     = join(",", $current_user["departments"]);
            $department_condition_clause = "OR (follower.follower_type = 3 AND find_in_set(follower.follower, '{$user_departments_joined}'))";
        }

        if (trim($current_user["position"]) != "") {
            $position_condition_clause = "OR (follower.follower_type = 4 AND follower.follower = {$current_user["position"]})";
        }

        if (trim($from_index) == "") {
            $from_index  = 0;
            $fetch_count = 20;
        }

        if ($filter_post_type && trim($filter_post_type) != "") {
            $filter_clause = "post.type = {$filter_post_type} AND ";
        }

        if(count($other_cond)>0){
            foreach ($other_cond as $key => $value) {
                $filter_clause .= $value['field']." ".$value['operator']." ".$value['value']." AND ";
            }   
        }

        // Apply visibility type 3 (custom) to non admins
        if ($current_user["user_level_id"] == 3) {
            $custom_visibility_condition_clause = " AND (
                        author.id = {$current_user["id"]}
                        OR (follower.follower_type = 1 AND follower.follower = {$current_user["id"]})
                        {$group_condition_clause}
                        {$department_condition_clause}
                        {$position_condition_clause}                        
                    )";
        }

        $this->database->query("SET SESSION group_concat_max_len=8192");

        $query = "SELECT 
                        post.*,
                        author.id AS author_id,
                        author.display_name AS author_display_name,
                        author.extension AS author_image_extension,
                        announcement.text_content AS announcement,
                        survey.text_content AS survey_question,
                        survey.enable_multiple_responses,
                        survey.publish_results,                        
                        (SELECT 
                                COUNT(1)
                            FROM
                                tb_message_board_post_comments comments
                            WHERE
                                comments.post_id = post.id) comment_count,
                        (SELECT 
                                mood
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.post_id = post.id
                                    AND moods.comment_id = 0
                                    AND moods.user_id = {$current_user['id']}) user_mood,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.post_id = post.id
                                    AND moods.comment_id = 0
                                    AND moods.user_id != {$current_user['id']}
                                    AND moods.mood = 'Happy') happy_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.post_id = post.id
                                    AND moods.comment_id = 0
                                    AND moods.user_id != {$current_user['id']}
                                    AND moods.mood = 'Inspired') inspired_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.post_id = post.id
                                    AND moods.comment_id = 0
                                    AND moods.user_id != {$current_user['id']}
                                    AND moods.mood = 'Sad') sad_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.post_id = post.id
                                    AND moods.comment_id = 0
                                    AND moods.user_id != {$current_user['id']}
                                    AND moods.mood = 'Angry') angry_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.post_id = post.id
                                    AND moods.comment_id = 0
                                    AND moods.user_id != {$current_user['id']}
                                    AND moods.mood = 'Annoyed') annoyed_users_count,
                        (SELECT 
                                COUNT(*)
                            FROM
                                tb_message_board_post_moods moods
                            WHERE
                                moods.post_id = post.id
                                    AND moods.comment_id = 0
                                    AND moods.user_id != {$current_user['id']}
                                    AND moods.mood = 'Don\'t Care') dont_care_users_count,
                        (SELECT 
                                CONCAT('[', GROUP_CONCAT(my_responses.response_id) , ']')
                            FROM
                                tb_message_board_survey_responses my_responses
                            WHERE
                                post.id = my_responses.survey_id
                                    AND my_responses.responder_id = {$current_user["id"]}) survey_my_responses,
                        (SELECT 
                            CONCAT('[',
                                GROUP_CONCAT(CONCAT('{\"id\":',
                                    '\"',
                                    followers.id,
                                    '\",\"follower_type\":\"',
                                    followers.follower_type,
                                    '\",\"follower\":\"',
                                    followers.follower,
                                    '\",\"follower_display_name\":\"',
                                    followers.follower_display_name,
                                    '\"}')),
                                ']')
                            FROM
                                tb_message_board_post_followers followers
                            WHERE
                                followers.post_id = post.id) post_followers
                    FROM
                        tb_message_board_posts post
                            LEFT JOIN
                        tbuser author ON post.author_id = author.id
                            LEFT JOIN
                        tb_message_board_announcements announcement ON post.id = announcement.id
                            LEFT JOIN
                        tb_message_board_surveys survey ON post.id = survey.id                            
                            LEFT JOIN
                        tb_message_board_post_followers follower ON post.id = follower.post_id
                WHERE
                    {$filter_clause}
                    (visibility_type_id = 1
                    OR (visibility_type_id = 2 AND author.company_id = {$current_user["company_id"]})
                    OR (
                        visibility_type_id = 3 
                        AND author.company_id = {$current_user["company_id"]} 
                        {$custom_visibility_condition_clause})
                    )
                GROUP BY post.id
		ORDER BY CASE
                            WHEN
                                post.type = 2
                                    AND survey_my_responses IS NULL
                            THEN
                                1
                            ELSE -1
                        END DESC, 
                        post.date_last_commented DESC, 
                        post.date_updated DESC 
                LIMIT {$from_index}, {$fetch_count}";

//        echo $query;

        $posts                = $this->database->query($query);
        $post_id_list         = array();
        $posts_with_followers = array();

        foreach ($posts AS $post) {
            array_push($post_id_list, $post["id"]);
        }

        if (count($post_id_list) > 0) {
            $post_list_followers = $this->getPostListFollowers($post_id_list);
        } else {
            $post_list_followers = array();
        }

        $mapped_post_list_followers = array();

        foreach ($post_list_followers AS $post_follower) {
            if (!$mapped_post_list_followers[$post_follower["post_id"]]) {
                $mapped_post_list_followers[$post_follower["post_id"]] = array();
            }
            array_push($mapped_post_list_followers[$post_follower["post_id"]], $post_follower);
        }

        foreach ($posts AS $post) {
            $post_with_followers              = $post;
            $post_with_followers["followers"] = $mapped_post_list_followers[$post["id"]];
            array_push($posts_with_followers, $post_with_followers);
        }

        return $posts_with_followers;
    }

    /**
     * @param PortalPost $message_board_post
     * @return int
     */
    public function insertPost($message_board_post) {
        $insert_row   = $this->postToInsertableRow($message_board_post);
        $resulting_id = $this->database->insert("tb_message_board_posts", $insert_row);

        return $resulting_id;
    }

    /** @param PortalPost $message_board_post */
    public function updatePost($message_board_post) {

        if (!$message_board_post->id) {
            throw new Exception("The message board post object to update does not have an id.");
        }

        $insert_row       = $this->postToUpdateRow($message_board_post);
        $update_condition = array("id" => $message_board_post->id);

        $this->database->update("tb_message_board_posts", $insert_row, $update_condition);

        return $message_board_post->id;
    }

    public function updatePostDateLastCommented($post_id) {

        $update_row       = array("date_last_commented" => "NOW()");
        $update_condition = array("id" => $post_id);

        $this->database->update("tb_message_board_posts", $update_row, $update_condition);
    }

    public function deletePost($message_board_post_id, $current_user) {
        
        $delete_condition = array(
            "id" => $message_board_post_id
        );

        //added by joshua reyes 04-07-2016 [reason: foreign key constraint in tb_message_board_posts]
        $delete_condition_moods = array(
            "post_id" => $message_board_post_id
            
        );
        
        //modified by joshua reyes 04-07-2016 [reason: mobile compatibility about key array]
        if (is_array($current_user)) {
            $current_user = $current_user['id'];
            if ($current_user["user_level_id"] > 2) { // users & guests
                $delete_condition["author_id"] = $current_user;
            }
            $delete_condition_moods["user_id"] = $current_user;
        }

        $this->database->delete("tb_message_board_post_moods", $delete_condition_moods);
        return $this->database->delete("tb_message_board_posts", $delete_condition);
    }

    /** @param PortalPost $message_board_post */
    private function postToInsertableRow($message_board_post) {

        // required fields
        $message_board_post_row = array(
            "author_id"          => $message_board_post->author_id,
            "date_updated"       => $message_board_post->date_updated,
            "visibility_type_id" => $message_board_post->visibility_type,
            "type"               => $message_board_post->type
        );

        // optional fields
        if ($message_board_post->date_posted) {
            $message_board_post_row["date_posted"] = $message_board_post->date_posted;
        }

        if ($message_board_post->date_last_commented) {
            $message_board_post_row["date_last_commented"] = $message_board_post->date_last_commented;
        }

        return $message_board_post_row;
    }

    /**
     * 
     * @param PortalPost $message_board_post
     * @return type
     */
    private function postToUpdateRow($message_board_post) {

        // required fields
        $message_board_post_row = array(
            "date_updated"       => $message_board_post->date_updated,
            "visibility_type_id" => $message_board_post->visibility_type,
            "type"               => $message_board_post->type
        );

        // optional fields
        if ($message_board_post->date_posted) {
            $message_board_post_row["date_posted"] = $message_board_post->date_posted;
        }

        if ($message_board_post->date_last_commented) {
            $message_board_post_row["date_last_commented"] = $message_board_post->date_last_commented;
        }

        return $message_board_post_row;
    }

}
