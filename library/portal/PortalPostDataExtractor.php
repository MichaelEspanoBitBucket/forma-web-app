<?php 
/**
 * Description of PortalPostDataExtractor
 *
 * @author Ervinne Sodusta
 */
class PortalPostDataExtractor {

    const EXTRACTED_IMAGES_PATH = "/images/announcement_attachments/";

    public static function extractImages($post_raw_text_html) {
        $target_file_extraction_path = APPLICATION_PATH . PortalPostDataExtractor::EXTRACTED_IMAGES_PATH;

        error_reporting(E_ERROR);

        $dom = new DOMDocument();
        $dom->loadHTML($post_raw_text_html);

        $images = $dom->getElementsByTagName("img");
        for ($i = 0; $i < $images->length; $i ++) {
            if (is_object($images->item($i))) {
                $base64_encoded_image = $images->item($i)->getAttribute("src");
                $original_file_name = $images->item($i)->getAttribute("data-filename");

                if (PortalPostDataExtractor::isTextData($base64_encoded_image)) {
                    $new_file_name = PortalPostDataExtractor::generateStorageFileNameFromRawFileName($original_file_name);
                    PortalPostDataExtractor::base64ToFile(
                            $base64_encoded_image, $target_file_extraction_path . $new_file_name
                    );

                    $images->item($i)->setAttribute("src", PortalPostDataExtractor::EXTRACTED_IMAGES_PATH . $new_file_name);
                }
            }
        }

        error_reporting(0);

        return $dom->saveHTML();
    }

    static function isTextData($text) {
        return substr($text, 0, 5) === "data:";
    }

    static function base64ToFile($base64_string, $output_file_name) {

        $data = explode(',', $base64_string);
        $image = $data[1];

        $ifp = fopen($output_file_name, "wb");

        fwrite($ifp, base64_decode($image));
        fclose($ifp);

        return $output_file_name;
    }

    static function generateStorageFileNameFromRawFileName($raw_file_name) {

        $splitted_file_name = explode('.', $raw_file_name);
        $extension = $splitted_file_name[count($splitted_file_name) - 1];

        return $splitted_file_name[0] . "_". uniqid() . "." . $extension;
    }

}
