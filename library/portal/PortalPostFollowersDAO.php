<?php

/**
 * Description of PortalPostFollowersDAO
 *
 * @author Ervinne Sodusta
 */
class PortalPostFollowersDAO {

    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    public function clearFollowers($post_id) {

        $delete_condition = array(
            "post_id" => $post_id
        );

        $this->database->delete("tb_message_board_post_followers", $delete_condition);
    }

    public function insertFollowers($post_id, $followers) {

        foreach ($followers AS $follower) {
            $insert_row["post_id"] = $post_id;
            $insert_row["follower"] = $follower["value"];
            $insert_row["follower_type"] = $follower["type"];
            $insert_row["follower_display_name"] = $follower["display_text"];
            $insert_row["enable_notification"] = 1; // enable

            $this->database->insert("tb_message_board_post_followers", $insert_row);
        }
    }

}
