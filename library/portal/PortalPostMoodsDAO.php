<?php

include_once API_LIBRARY_PATH . 'API.php';

/**
 * Description of PortalPostMoodsDAO
 *
 * @author Ervinne Sodusta
 */
class PortalPostMoodsDAO {

    /** @var APIDatabase */
    protected $database;
    protected $table = "tb_message_board_post_moods";

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    public function getUserMoods($postId, $commentId, $currentUserId, $from_index, $fetch_count) {
//        $this->query("SET CHARACTER SET utf8;");

        $limit = "";

        if (trim($from_index) != "") {
            
            $limit = "LIMIT $from_index, $fetch_count";
        }

        // $limit = "LIMIT $from_index, $fetch_count";

        $query = "SELECT 
                        mood, user_id, display_name, u.extension as user_image_extension
                    FROM
                        tb_message_board_post_moods m
                            LEFT JOIN
                        tbuser u ON m.user_id = u.id
                    WHERE
                        post_id = {$postId} 
                            AND comment_id = {$commentId}
                            AND user_id != {$currentUserId}
                    ORDER BY display_name {$limit};";

        return $this->database->query($query);
    }

    public function getPostMood($userId, $postId) {
        $query = "SELECT * FROM {$this->table} WHERE post_id = {$postId} AND comment_id = 0 AND user_id = {$userId}";
        return $this->database->query($query, APIDatabase::QUERY_FETCH_ROW);
    }

    public function getCommentMood($userId, $commentId) {
        $query = "SELECT * FROM {$this->table} WHERE comment_id = {$commentId} AND user_id = {$userId}";
        return $this->database->query($query, APIDatabase::QUERY_FETCH_ROW);
    }

    public function insertMood($userId, $mood, $postId, $commentId) {

        $data = array(
            'user_id' => $userId,
            'mood' => $mood,
            'post_id' => $postId
        );

        if ($commentId > 0) {
            $data['comment_id'] = $commentId;
        }

        return $this->database->insert($this->table, $data);
    }

    public function updateMood($userId, $mood, $postId, $commentId) {

        $data = array(
            'user_id' => $userId,
            'mood' => $mood,
            'post_id' => $postId
        );

        if ($commentId > 0) {
            $data['comment_id'] = $commentId;
        }

        $conditions = array(
            'user_id' => $userId,
            'post_id' => $postId,
            'comment_id' => $commentId
        );

        return $this->database->update($this->table, $data, $conditions);
    }

}
