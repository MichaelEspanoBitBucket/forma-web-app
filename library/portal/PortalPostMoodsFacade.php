<?php

include_once API_LIBRARY_PATH . 'API.php';
include_once 'PortalPostMoodsDAO.php';

/**
 * Description of PortalPostMoodsFacade
 *
 * @author Ervinne Sodusta
 */
class PortalPostMoodsFacade {
    
    public function setPostMood($userId, $mood, $postId) {

        $db       = new APIDatabase();
        $moodsDAO = new PortalPostMoodsDAO($db);

        $updatedMoodId = 0;

        $db->connect();

        $previousMood = $moodsDAO->getPostMood($userId, $postId);
        if (!$previousMood) {
            $updatedMoodId = $moodsDAO->insertMood($userId, $mood, $postId, 0);
        } else {
            if ($previousMood['mood'] != $mood) {
                $moodsDAO->updateMood($userId, $mood, $postId, 0);
            }

            $updatedMoodId = $previousMood['id'];
        }

        $db->disconnect();

        return $updatedMoodId;
    }

    public function setCommentMood($userId, $mood, $postId, $commentId) {

        $db       = new APIDatabase();
        $moodsDAO = new PortalPostMoodsDAO($db);

        $updatedMoodId = 0;

        $db->connect();

        $previousMood = $moodsDAO->getCommentMood($userId, $commentId);
        if (!$previousMood) {
            $updatedMoodId = $moodsDAO->insertMood($userId, $mood, $postId, $commentId);
        } else {
            if ($previousMood['mood'] != $mood) {
                $moodsDAO->updateMood($userId, $mood, $postId, $commentId);
            }

            $updatedMoodId = $previousMood['id'];
        }

        $db->disconnect();

        return $updatedMoodId;
    }

}
