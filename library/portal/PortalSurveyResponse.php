<?php

include_once 'PortalPost.php';

/**
 * Description of PortalAnnouncement
 *
 * @author Mark Rowi Dizon
 */
class PortalSurvey extends PortalPost {

    /** @var String */
    public $text_content;
    public $survey_responses;
    public $enable_multiple_responses;
    public $publish_results;
    public $chart_type;

    public function __construct() {
        parent::__construct();
        $this->prioritize = 1;
    }

}

class PortalSurveyResponse {

    /** @var String */
    public $survey_id;
    public $responder_id;
    public $survey_responses;

}
