<?php

/**
 * Description of PortalSurveyDAO
 *
 * @author Mark Rowi Dizon
 */
class PortalSurveysDAO {

    /** @var APIDatabase */
    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    /**
     * @param PortalAnnouncement $survey
     * @return int
     */
    public function insertSurvey($survey) {

        $insert_row = $this->surveyToInsertableRow($survey);
        $resulting_id = $this->database->insert("tb_message_board_surveys", $insert_row);

        return $resulting_id;
    }

    public function updateSurvey($announcement) {

        $update_row = $this->surveyToInsertableRow($announcement);
        $update_condition = array("id" => $announcement->id);

        $this->database->update("tb_message_board_surveys", $update_row, $update_condition);

        return $announcement->id;
    }

    public function updateSurveyPublish($survey) {

        $update_row = array("publish_results" => $survey->publish_results, "chart_type" => $survey->chart_type);
        $update_condition = array("id" => $survey->survey_id);
        $this->database->update("tb_message_board_surveys", $update_row, $update_condition);

        return $survey->id;
    }

    /**  @param PortalAnnouncement $survey */
    private function surveyToInsertableRow($survey) {

        $row = array(
            "id" => $survey->id,
            "text_content" => $survey->text_content,
            "enable_multiple_responses" => $survey->enable_multiple_responses,
            "publish_results" => $survey->publish_results
        );

        return $row;
    }

    public function updateSurveyResponseSelectablePost($survey_response, $survey_id) {

        $update_row = $this->surveyResponseSelectablePostToInsertableRow($survey_response, $survey_id);
        $update_condition = array("id" => $survey_response["id"]);

        $this->database->update("tb_message_board_survey_selectable_responses", $update_row, $update_condition);

        return $announcement->id;
    }

    // For Selectable Responses
    public function insertSurveyResponseSelectablePost($survey_response, $survey_id) {

        $insert_row = $this->surveyResponseSelectablePostToInsertableRow($survey_response, $survey_id);
        $resulting_id = $this->database->insert("tb_message_board_survey_selectable_responses", $insert_row);

        return $resulting_id;
    }

    // For Selectable Responses
    public function deleteSurveyResponseSelectablePost($survey_response, $survey_id) {
        $delete_condition = array(
            "id" => $survey_response["id"],
            "survey_id" => $survey_id
        );

        return $this->database->delete("tb_message_board_survey_selectable_responses", $delete_condition);
    }

    /** @param PortalPost $message_board_post */
    private function surveyResponseSelectablePostToInsertableRow($survey_response, $survey_id) {

        // required fields
        $survey_response = array(
            "response" => $survey_response["text"],
            "survey_id" => $survey_id
        );

        return $survey_response;
    }

    // For Selectable Responses
    public function insertSurveyResponsePost($survey_response, $survey_id, $responder_id) {

        $insert_row = $this->surveyResponsePostToInsertableRow($survey_response, $survey_id, $responder_id);
        $resulting_id = $this->database->insert("tb_message_board_survey_responses", $insert_row);

        return $resulting_id;
    }

    /** @param PortalPost $message_board_post */
    private function surveyResponsePostToInsertableRow($survey_response_id, $survey_id, $responder_id) {

        // required fields
        $survey_response = array(
            "response_id" => $survey_response_id,
            "survey_id" => $survey_id,
            "responder_id" => $responder_id
        );

        return $survey_response;
    }

    public function deleteSurveyResponseUserPost($survey_id, $responder_id) {
        $delete_condition = array(
            "responder_id" => $responder_id,
            "survey_id" => $survey_id
        );

        return $this->database->delete("tb_message_board_survey_responses", $delete_condition);
    }

    public function getSurveyListSelectableResponses($survey_id_list) {

        $survey_id_list_csv = join(",", $survey_id_list);
        $query = "SELECT 
                        id, survey_id, response AS text
                    FROM
                        tb_message_board_survey_selectable_responses
                    WHERE
                        survey_id IN ({$survey_id_list_csv});";

//        echo $query;

        return $this->database->query($query);
    }

    public function getSurveyListUserResponses($user, $survey_id_list) {

        $survey_id_list_csv = join(",", $survey_id_list);
        $query = "SELECT 
                    *
                FROM
                    tb_message_board_survey_responses my_responses
                WHERE
                    survey_id IN ($survey_id_list_csv)
                    AND my_responses.responder_id = {$user["id"]}";

        return $this->database->query($query);
    }

    public function getSurveys($current_user) {
        $group_condition_clause = "";
        $department_condition_clause = "";
        $position_condition_clause = "";

        if (count($current_user["groups"]) > 0) {
            $user_groups_joined = join(",", $current_user["groups"]);
            $group_condition_clause = "OR (follower.follower_type = 2 AND find_in_set(follower.follower, '{$user_groups_joined}'))";
        }

        if (count($current_user["departments"]) > 0) {
            $user_departments_joined = join(",", $current_user["departments"]);
            $department_condition_clause = "OR (follower.follower_type = 3 AND find_in_set(follower.follower, '{$user_departments_joined}'))";
        }

        if (trim($current_user["position"]) != "") {
            $position_condition_clause = "OR (follower.follower_type = 4 AND follower.follower = {$current_user["position"]})";
        }

        // if (trim($from_index) == "") {
        //     $from_index = 0;
        //     $fetch_count = 20;
        // }

        $this->database->query("SET SESSION group_concat_max_len=8192");

        $view_only_published_graphs_condition_clause = "";

        if ($current_user["user_level_id"] <= 2) { // admin or company admin            
            $custom_visibility_condition_clause = " OR (visibility_type_id = 3 AND author.company_id = {$current_user["company_id"]})";
        } else {

            $view_only_published_graphs_condition_clause = " AND (survey.publish_results = 1 OR author.id = {$current_user["id"]})";
            $custom_visibility_condition_clause = " OR (visibility_type_id = 3 AND (
                        author.id = {$current_user["id"]}
                        OR (follower.follower_type = 1 AND follower.follower = {$current_user["id"]})
                        {$group_condition_clause}
                        {$department_condition_clause}
                        {$position_condition_clause}
                        )
                    )
                    OR post.author_id = {$current_user["id"]}";

            // $custom_visibility_condition_clause_survey = "AND (publish_results = 1 OR author.id = {$current_user["id"]})" 
        }

        $query = "SELECT 
                        post.*,
                        author.id AS author_id,
                        author.display_name AS author_display_name,
                        author.extension AS author_image_extension,
                        announcement.text_content AS announcement,
                        survey.text_content AS survey_question,
                        survey.enable_multiple_responses,
                        survey.publish_results,
                        survey.chart_type,
                        (SELECT 
                            CONCAT('[',
                                GROUP_CONCAT(CONCAT('{\"id\":',
                                    '\"',
                                    followers.id,
                                    '\",\"follower_type\":\"',
                                    followers.follower_type,
                                    '\",\"follower\":\"',
                                    followers.follower,
                                    '\",\"follower_display_name\":\"',
                                    followers.follower_display_name,
                                    '\"}')),
                                ']')
                            FROM
                                tb_message_board_post_followers followers
                            WHERE
                                followers.post_id = post.id) post_followers
                    FROM
                        tb_message_board_surveys survey
                            LEFT JOIN
                        tb_message_board_posts post ON post.id = survey.id  
                            LEFT JOIN
                        tbuser author ON post.author_id = author.id
                            LEFT JOIN
                        tb_message_board_announcements announcement ON post.id = announcement.id                                                      
                            LEFT JOIN
                        tb_message_board_post_followers follower ON post.id = follower.post_id
                WHERE
                    (visibility_type_id = 1
                    OR (visibility_type_id = 2 AND author.company_id = {$current_user["company_id"]})
                    {$custom_visibility_condition_clause})
                    {$view_only_published_graphs_condition_clause}
                GROUP BY date_posted desc
        ";

        // echo $query;

        return $this->database->query($query);
    }

}
