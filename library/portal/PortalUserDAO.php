<?php

/**
 * Description of PortalUserDAO
 *
 * @author Ervinne Sodusta
 */
class PortalUserDAO {

    /** @var APIDatabase $database */
    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    public function getUserGroupList($user_id) {
        $query = "SELECT 
                        g.id
                    FROM
                        tbform_groups_users gu
                            LEFT JOIN
                        tbform_groups g ON gu.group_id = g.id
                    WHERE
                        gu.user_id = {$user_id} 
                            AND gu.is_active = 1
                            AND g.is_active = 1";

        $rows          = $this->database->query($query);
        $group_id_list = array();

        foreach ($rows AS $row) {
            array_push($group_id_list, $row["id"]);
        }
        return $group_id_list;
    }

    public function getUserDepartmentList($user_id) {

        $query = "SELECT 
                        orgobj.department_code
                    FROM
                        tborgchartobjects orgobj
                            LEFT JOIN
                        tborgchart org ON orgobj.orgChart_id = org.id
                            LEFT JOIN
                        tbdepartment_users deptuser ON orgobj.department_code = deptuser.department_code
                    WHERE
                        org.status = 1 AND deptuser.user_id = {$user_id}
                    GROUP BY deptuser.department_code";

        $rows               = $this->database->query($query);
        $department_id_list = array();

        foreach ($rows AS $row) {
            array_push($department_id_list, $row["department_code"]);
        }

        return $department_id_list;
    }

}
