<?php

include_once '/../PortalPost.php';

/**
 * Description of FormPostAdapter
 *
 * @author Ervinne Sodusta
 */
class FormPostAdapter {

    public static function requestToPost($request) {

        if ($request["requestor_image_extension"] == null || $request["requestor_image_extension"] == "") {
            $image_url = "";
        } else {
            $encrypted_id = md5(md5($request['requestor_id']));
            $path         = $encrypted_id . "." . "/small_" . $encrypted_id . "." . $request["requestor_image_extension"];
            $image_url    = "/images/formalistics/tbuser/" . $path;
        }

//        var_dump($request);

        $post = array();

        $post["id"]                        = $request["id"];
        $post["type"]                      = PortalPost::TYPE_FORM_REQUEST;
        $post["visibility_type_id"]        = 1; //test only
        $post["post_followers"]            = array();
        $post["enable_multiple_responses"] = 0;
        $post["publish_results"]           = 0;
        $post["author_image_url"]          = $image_url;
        $post["author_display_name"]       = $request["requestor_display_name"];
        $post["date_updated"]              = $request["DateUpdated"];
        $post["status"]                    = $request["status"];
        $post["preview"]                   = $request["TrackNo"] . " - " . $request["description"];

        return $post;
    }

}
