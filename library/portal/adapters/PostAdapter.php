<?php

include_once '/../PortalPost.php';

/**
 * Description of FormPostAdapter
 *
 * @author Ervinne Sodusta
 */
class PostAdapter {

    /**
     * 
     * @param Array $request
     * @param RequestBodyParser $request_body_parser
     * @return Array
     */
    public static function requestToPost($form_id, $request, $request_body_parser) {
//        var_dump($request);

        $post = array();

        $post["id"]                        = $request["request_id"];
        $post["type"]                      = PortalPost::TYPE_FORM_REQUEST;
        $post["visibility_type_id"]        = 1; //test only
        $post["post_followers"]            = array();
        $post["enable_multiple_responses"] = 0;
        $post["publish_results"]           = 0;
        $post["author_image_url"]          = PostAdapter::generateAuthorImageURL($request);
        $post["author_display_name"]       = $request["requestor_display_name"];
        $post["date_updated"]              = $request["DateUpdated"];
        $post["status"]                    = $request["Status"];
        $post["preview"]                   = $request_body_parser->parse($request);
        $post["details_link"]              = PostAdapter::generateRequestLink($form_id, $request["request_id"], $request["track_no"]);
        $post["details_link_label"]        = "View request details";

        return $post;
    }

    private static function generateAuthorImageURL($request) {
        if ($request["requestor_image_extension"] == null || $request["requestor_image_extension"] == "") {
            $image_url = "";
        } else {
            $encrypted_id = md5(md5($request['requestor_id']));
            $path         = $encrypted_id . "." . "/small_" . $encrypted_id . "." . $request["requestor_image_extension"];
            $image_url    = "/images/formalistics/tbuser/" . $path;
        }

        return $image_url;
    }

    private static function generateRequestLink($form_id, $request_id, $track_no) {
        return "/user_view/workspace?view_type=request&formID={$form_id}&requestID={$request_id}&trackNo={$track_no}";
    }

}
