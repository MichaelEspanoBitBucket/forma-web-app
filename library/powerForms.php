<?php

class powerForms extends functions{

    // @data contains company_id and email key

    /*
        Sample Use

        $data = array();
        $data['email'] = "samuel.pulta07@gmail.com";
        $data['company_id'] = $auth['company_id'];

        echo powerForms::auto_register($db,$data); // JSON return


    */


    public function auto_register($db,$data){

        $Person = new Person($db,"");

        $date = $this->currentDateTime();

        $generate_pin = $this->random_chars(5);

        $encrypt_password =  $this->encrypt_decrypt("encrypt",$generate_pin);

        $Person->email = $data['email'];
        // $Person->display_name = $result['display_name'];
        $Person->first_name = $data['email'];
        // $Person->middle_name = $result['middle_name'];
        // $Person->last_name = $result['last_name'];
        // $Person->contact_number = $result['contact_number'];
        // $Person->position = $result['position'];
        $Person->company->id = $data['company_id'];

        $Person->user_level_id->id = 4;
        // $Person->department_position_level = new Department_Position_Level($this->db, $result['department_position_level']);
        $Person->password = $encrypt_password;
        // $Person->extension = $result['extension'];
        $Person->date_registered = $date;
        $Person->email_activate = "1";
        // $Person->is_available = $result['is_available'];
        // $Person->timeout = $result['timeout'];
        $Person->is_active = 1;
        // $Person->department_id = $result['department_id'];
        // $Person->department = new Department($this->db, $result['department_id']);
        // $Person->image = $userQuery->avatarPic($this->tblname, $this->id, "44", "44", "small", "avatar");
        // $Person->json_tbl = $result['json_tbl'];
        // var_dump($Person);

        $Person->save();

        $return_arr = array();

        $return_arr['email'] = $data['email'];
        $return_arr['pin']  =   $generate_pin;

        $Mail_Notification = new Mail_Notification();
        
        $Mail_Notification->email_pin($return_arr);

        return json_encode($return_arr);

        

    }

    /*

        $pf_auth = new powerForms();

        $value = array();
        $value[pin_only] = "1"; // value 1 or 0
        $value['password'] = ""; // your pin
        $value['email'] = ""; // your email if 0

        $pf_auth->pin_only_auth($value);
    */

    public function pin_only_auth($db,$value)
    {
        $user_password = functions::encrypt_decrypt("encrypt",$value['password']);
        
        if($value['pin_only'] == 1){
            $sql = "password={$db->escape($user_password)}";
        }else{
            $sql = "email={$db->escape($value["email"])} AND password={$db->escape($user_password)}";
        }

        //$db = new Database();
        $login = $db->query("SELECT *
                                FROM tbuser
                                WHERE 
                                {$sql}
                                AND is_active = 1
                                ","row");
        // var_dump($login);
        if($login){
            $session = new Auth();
            $session->setAuth('current_user',$login);
            // setcookie('USERAUTH',serialize($login),0,'/',COOKIE_URL);
            // setcookie('application', "false",0,'/','' . COOKIE_URL);
            return 'true';
        }

        return false;
    }


    // For Logout

    public function logout_powerForms(){

        $destroy = Auth::destroyAuth('current_user');

        if ($destroy || !$destroy) {

            return "You have successfully logged out.";
        }
               
    }

    /*
        Array()
        @$val['company_id'] 


    */

    public function get_guest_user_per_company($db,$val){

        $company_id = $val['company_id'];

        $query = $db->query("SELECT * FROM tbuser WHERE company_id = {$db->escape($company_id)} AND user_level_id = {$db->escape(4)} ORDER BY id ASC","row");
        
        $decrypt_password =  $this->encrypt_decrypt("decrypt",$query['password']);

        $session = new Auth();  
        $ret = $session->login($query['email'],$decrypt_password,'email','password','tbuser');

        return json_encode($ret);


    }


    /*
        $.ajax({
            type:"post",
            url:"/user/logout",
            success:function(e){
            console.log(e)
            }
            })

    */
}

?>