<?php

include_once '\RequestSearchParameters.php';

/**
 * Description of RequestsDAO
 *
 * @author Ervinne Sodusta
 */
class RequestsDAO {

    /** @var APIDatabase */
    protected $database;

    /** @param APIDatabase $database */
    public function __construct($database) {
        $this->database = $database;
    }

    /**
     * 
     * @param RequestSearchParameters $search_parameters
     */
    public function searchRequests($search_parameters) {

        $filter = $this->buildCategoryFilterClause($search_parameters);
        $query  = $this->buildFetchRequestsQuery($search_parameters, $filter);        
        
        return $this->database->query($query);
    }

    public function getRequestCount($search_parameters) {

        $filter = $this->buildCategoryFilterClause($search_parameters);
        $query  = $this->buildFetchRequestCountQuery($search_parameters, $filter);
        
        return $this->database->query($query, APIDatabase::QUERY_FETCH_ROW);
    }

    private function buildFetchRequestCountQuery($search_parameters, $category_filter_clause) {

        return "SELECT 
                        count(*) AS request_count
                    FROM
                        {$search_parameters->form["form_table_name"]} request
                            LEFT JOIN
                        tbuser requester ON requester.id = request.Requestor
                            LEFT JOIN
                        tbuser processer ON processer.id = request.Processor                        
                    WHERE                   
                        (NOT EXISTS(SELECT 
                                *
                            FROM
                                tbtrash_bin
                            WHERE
                                record_id = request.ID AND form_id = {$search_parameters->form["id"]}
                                AND table_name = '{$search_parameters->form["form_table_name"]}'
                            LIMIT 0, 1
                        ))
                        {$category_filter_clause}
                    ORDER BY request.ID DESC";
    }

    /**
     * 
     * @param RequestSearchParameters $search_parameters
     */
    private function buildFetchRequestsQuery($search_parameters, $category_filter_clause) {
        return "SELECT 
                        request.ID AS request_id,
                        request.TrackNo AS track_no,
                        requester.first_name,
                        requester.last_name,
                        requester.middle_name,
                        request.Status AS form_status,
                        request.Requestor AS requestor_id,                        
                        requester.display_name AS requestor_display_name,
                        requester.extension AS requestor_image_extension,
                        CASE request.ProcessorType
                            WHEN 1 THEN request.Processor
                            WHEN 2 THEN request.Processor
                            ELSE processer.display_name
                        END AS processors_display_name,
                        request.*
                    FROM
                        {$search_parameters->form["form_table_name"]} request
                            LEFT JOIN
                        tbuser requester ON requester.id = request.Requestor
                            LEFT JOIN
                        tbuser processer ON processer.id = request.Processor                        
                    WHERE                   
                        (NOT EXISTS(SELECT 
                                *
                            FROM
                                tbtrash_bin
                            WHERE
                                record_id = request.ID AND form_id = {$search_parameters->form["id"]}
                                AND table_name = '{$search_parameters->form["form_table_name"]}'
                            LIMIT 0, 1
                        ))
                        {$category_filter_clause}
                    GROUP BY request.id                    
                    ORDER BY request.ID DESC
                    LIMIT {$search_parameters->start_from_index}, {$search_parameters->fetch_count}";
    }

    /**
     * 
     * @param RequestSearchParameters $search_parameters
     */
    private function buildCategoryFilterClause($search_parameters) {

        if ($search_parameters->form["category_id"] == 0) {
            // no filter is needed for forms under "Others" category
            return "";
        }

        return " AND
               (request.Requestor = {$search_parameters->user["id"]}
                OR FIND_IN_SET({$search_parameters->user["id"]}, 
                    CASE request.ProcessorType
                        WHEN 1 THEN CONCAT(@department_users:=getDepartmentUsers(request.Processor, request.ProcessorLevel, {$search_parameters->user["company_id"]}), ',', IF(request.enable_delegate = 1, IFNULL(getDelegate(@department_users), ''),''))
                        WHEN 2 THEN CONCAT(@position_users:=getUsersByPosition(request.Processor), ',', IF(request.enable_delegate = 1,IFNULL(getDelegate(@position_users), ''),''))
                        ELSE CONCAT(request.Processor, ',', IF(request.enable_delegate = 1,IFNULL(getDelegate(request.Processor), ''),''))
                    END
                )
                OR FIND_IN_SET({$search_parameters->user["id"]}, request.Editor)
                OR FIND_IN_SET({$search_parameters->user["id"]}, request.Viewer)
                OR (EXISTS 
                    (SELECT * FROM tbrequest_users tbru
                        LEFT JOIN
                    tbdepartment_users tbdu ON tbdu.department_code = tbru.user
                        LEFT JOIN
                    tbform_groups_users tbfgu ON tbfgu.group_id = tbru.user
                        LEFT JOIN
                    tborgchart tbo ON tbo.id = tbdu.orgChart_id
                        LEFT JOIN
                    tbpositions tbp ON tbp.id = tbru.user
                        WHERE tbru.RequestID = request.ID AND tbru.Form_ID = {$search_parameters->form["id"]} AND
                        (
                            (tbru.user_type = 3 AND tbru.user = {$search_parameters->user["id"]}) OR 
                            (tbru.user_type = 1 AND tbru.user = {$this->database->escape($search_parameters->user["position"])}) OR 
                            (tbru.user_type = 2 AND tbdu.user_id = {$search_parameters->user["id"]} AND tbo.status = 1) OR 
                            (tbru.user_type = 4 AND tbfgu.user_id = {$search_parameters->user["id"]} AND tbfgu.is_active = 1)
                        )
                    )
                )
                OR (EXISTS
                    (SELECT * FROM tbrequest_viewer tbrv 
                        WHERE tbrv.Form_ID = {$search_parameters->form["id"]} AND tbrv.Request_ID=request.ID
                            AND tbrv.user_id = {$search_parameters->user["id"]}
                    )
                )
                OR (EXISTS
                    (SELECT * FROM tbform_users tbfu
                            LEFT JOIN
                    tbdepartment_users tbdu ON tbdu.department_code = tbfu.user
                            LEFT JOIN
                    tbform_groups_users tbfgu ON tbfgu.group_id = tbfu.user
                            LEFT JOIN
                    tborgchart tbo ON tbo.id = tbdu.orgChart_id
                            LEFT JOIN
                    tbpositions tbp ON tbp.id = tbfu.user
                    WHERE
                        tbfu.Form_ID = {$search_parameters->form["id"]}
                            AND (
                                (tbfu.user_type = 3 AND tbfu.user = {$search_parameters->user["id"]} AND tbfu.action_type = 2 ) OR 
                                (tbfu.user_type = 1 AND tbfu.user = {$this->database->escape($search_parameters->user["position"])} AND tbfu.action_type = 2) OR 
                                (tbfu.user_type = 2 AND tbdu.user_id = {$search_parameters->user["id"]} AND tbo.status = 1 AND tbfu.action_type = 2) OR 
                                (tbfu.user_type = 4 AND tbfgu.user_id = {$search_parameters->user["id"]} AND tbfgu.is_active = 1 AND tbfu.action_type = 2)
                            )
                        )
                    )
               )";
    }

}
