<?php

require_once APPLICATION_PATH . '/library/form/adapters/WorkflowAdapter.php';
require_once APPLICATION_PATH . '/library/form/FormsDAO.php';
require_once APPLICATION_PATH . '/library/form/WorkflowsDAO.php';
include_once APPLICATION_PATH . '/library/portal/adapters/PostAdapter.php';
include_once APPLICATION_PATH . '/library/request/parsers/RequestBodyParser.php';
require_once '/RequestsDAO.php';

/**
 * Description of RequestsDataReaderFacade
 *
 * @author Ervinne Sodusta
 */
class RequestsDataReaderFacade {

    const DEFAULT_REQUEST_SEARCH_SIZE = 20;

    public function searchRequests($database, $user, $form_id, $start_index, $fetch_count) {
        $forms_dao    = new FormsDAO($database);
        $requests_dao = new RequestsDAO($database);

        $request_search_parameters                   = new RequestSearchParameters();
        $request_search_parameters->user             = $user;
        $request_search_parameters->form             = $forms_dao->getForm($form_id);
        $request_search_parameters->start_from_index = $start_index;
        $request_search_parameters->fetch_count      = $fetch_count;

        $requests = $requests_dao->searchRequests($request_search_parameters);

        return $requests;
    }

    public function getRequestCount($database, $user, $form_id) {

        $forms_dao    = new FormsDAO($database);
        $requests_dao = new RequestsDAO($database);

        $form = $forms_dao->getForm($form_id);

        if (!$form) {
            throw new Exception("Form {$form_id} not found");
        }

        $request_search_parameters       = new RequestSearchParameters();
        $request_search_parameters->user = $user;
        $request_search_parameters->form = $form;

        $result = $requests_dao->getRequestCount($request_search_parameters);

        if (!$result) {
            return 0;
        } else {
            return $result["request_count"];
        }
    }

    public function searchRequestsAsPosts($database, $user, $form_id, $start_index, $fetch_count) {

        $forms_dao     = new FormsDAO($database);
        $requests_dao  = new RequestsDAO($database);
        $workflows_dao = new WorkflowsDAO($database);

        $form = $forms_dao->getForm($form_id);

        if (!$form) {
            throw new Exception("Form {$form_id} not found");
        }

        $raw_workflow_notification_messages = $workflows_dao->getFormWorkflowNotificationMessages($form_id);
        $request_body_parser                = new RequestBodyParser($raw_workflow_notification_messages, $form);

        $request_search_parameters                   = new RequestSearchParameters();
        $request_search_parameters->user             = $user;
        $request_search_parameters->form             = $form;
        $request_search_parameters->start_from_index = $start_index;
        $request_search_parameters->fetch_count      = $fetch_count;

        $requests = $requests_dao->searchRequests($request_search_parameters);
        $posts    = array();

        foreach ($requests AS $request) {
            array_push($posts, PostAdapter::requestToPost($form["id"], $request, $request_body_parser));
        }

        return $posts;
    }

}
