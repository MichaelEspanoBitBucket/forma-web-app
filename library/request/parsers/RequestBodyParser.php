<?php

/**
 * Description of RequestBodyParser
 *
 * @author Ervinne Sodusta
 */
class RequestBodyParser {

    protected $messages;
    protected $form;

    public function __construct($form_notification_messages, $form) {
        $this->messages = $this->prepareFormNotificationMessages($form_notification_messages);
        $this->form     = $form;
    }

    public function parse($request) {
//        $request_node_id = $request["Node_ID"];
//
//        if (array_key_exists($request_node_id, $this->messages) && $this->messages[$request_node_id]) {
//            $parsed_body = $this->getParsedRequestBody($request, $this->messages[$request_node_id]);
//        } else {
//            $parsed_body = $this->getDefaultRequestBody($request);
//        }
//
//        return $parsed_body;
        return $this->getDefaultRequestBody($request);
    }

    private function getParsedRequestBody($request, $raw_message) {

        preg_match('/@[a-zA-Z0-9_]*/', trim($raw_message), $matches);

        $message = $raw_message;

        foreach ($matches AS $match) {
            $key = substr($match, 1);   //  remove leading @
            if (array_key_exists($key, $request) && $request[$key]) {
                $message = str_replace("@" . $key, $request[$key], $message);
            }
        }

        return $message;
    }

    private function getDefaultRequestBody($request) {
        return "A new document with tracking number {$request["TrackNo"]} under {$this->form["form_name"]} has been submitted.";
    }

    private function prepareFormNotificationMessages($form_notification_messages) {

        $messages = array();

        foreach ($form_notification_messages AS $message) {
            $raw_message                              = json_decode($message["notification_message"])->message;
            $messages[$message["workflow_object_id"]] = $raw_message;
        }

        return $messages;
    }

}
