<?php


class upload extends Auth{
    
    /* ========== Force download files on php ========== */
    public function force_download($action,$filename,$file_location){
        if(isset($action)){
            // header('Content-type: audio/mpeg3');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.$filename.'"');
            readfile($file_location);
            exit();
        }
    }

    public function get_all_directory($path){
        $dir = new DirectoryIterator($path);
        $ret = "";
        foreach ($dir as $fileinfo) {
            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                $ret .= $fileinfo->getFilename().'<br>';
            }
        }
        return $ret;
    }
    
    //
    //public function removeAllfiles_fromDirectory($type,$path){
    //    
    //        if($type=="1"){
    //            $files = glob($path.'/*'); // get all file names
    //                   
    //        }else{
    //            $files = glob($path.'/{,.}*', GLOB_BRACE);
    //        }
    //            foreach($files as $file){ // iterate files
    //              if(is_file($file))
    //                unlink($file); // delete file
    //            }   
    //  
    //    
    //}
    /**
    * Recursively delete a directory
    *
    * @param string $dir Directory name
    * @param boolean $deleteRootToo Delete specified top-level directory as well
    */
    function unlinkRecursive($dir, $deleteRootToo){
       if(!$dh = @opendir($dir)){
           return;
       }
       while (false !== ($obj = readdir($dh))){
           if($obj == '.' || $obj == '..'){
               continue;
           }
   
           if (!@unlink($dir . '/' . $obj)){
               unlinkRecursive($dir.'/'.$obj, true);
           }
        }
   
       closedir($dh);
   
       if ($deleteRootToo)
       {
           @rmdir($dir);
       }
   
       return;
   }
    /*
     * Get all files on the selected folder
     * @path = location of the folder
     */
    
    public function getAllfiles_fromDirectory($path){
        $log_directory = $path;

        $results_array = array();
        
        if (is_dir($log_directory))
        {
                if ($handle = opendir($log_directory))
                {
                        //Notice the parentheses I added:
                        while(($file = readdir($handle)) !== FALSE)
                        {
                          if($file != "." && $file != "..")
                          {
                                $results_array[] = $file;
                          }
                        }
                        closedir($handle);
                }
        }
        
        return $results_array;
    }
    
    /*
     * Create a folder on the selected path
     * @path = location of the folder
     */
    
    public function createFolder($path){
        if(!is_dir($path)){
            mkdir($path); // location
        }
    }
    
    /*
     *
     *
     *
     */
    
    //public function listFolderFiles($dir){
    //    //$ffs = scandir($dir);
    //    //foreach($ffs as $ff){
    //    //    if($ff != '.' && $ff != '..'){
    //    //        $get_extension = explode(".",$ff);
    //    //        $extension = $get_extension[count($get_extension) - 1];
    //    //        if($extension=="js"){
    //    //            echo '<script type="text/javascript" src="' . $dir . '/' .$ff.'"></script>';
    //    //        }elseif($extension=="css"){
    //    //            echo '<link rel="stylesheet" type="text/css" href="' . $dir . '/' .$ff .'" />';
    //    //        }elseif(is_dir($dir.'/'.$ff)){
    //    //            listFolderFiles($dir.'/'.$ff);
    //    //        }
    //    //        
    //    //    }
    //    //}
    //}
    //
    public function move_attach_files($name,$path,$auth,$other,$to){
        $id_encrypt = md5(md5($name));
        $dir = $path."/".$id_encrypt;
            upload::createFolder($dir);
            // Move files to the directory
            $copy_foldername = md5(md5($auth['id']));
            $from = $to.$copy_foldername;
            $postFiles = upload::getAllfiles_fromDirectory($from);
                    foreach($other as $img){
                        copy($from . '/' . $img, $dir . '/' . $img);
                    }
                upload::unlinkRecursive($from, "");
        // Return        
        $postImage = upload::getAllfiles_fromDirectory($dir);
    }
    
    public function move_attach_files_all($name,$path,$auth,$other,$from){
        $id_encrypt = md5(md5($name));
        $dir = $path."/".$id_encrypt;
            upload::createFolder($dir);
            // Move files to the directory
            $copy_foldername = md5(md5($auth['id']));
            
            $postFiles = upload::getAllfiles_fromDirectory($from);
            
                    foreach($postFiles as $img){
                        copy($from . '/' . $img, $dir . '/' . $img);
                    }
                upload::unlinkRecursive($from, "");
        // Return        
        $postImage = upload::getAllfiles_fromDirectory($dir);
    }
}



?>