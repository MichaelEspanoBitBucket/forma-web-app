<?php

/**
 * Description of UserImageUtilities
 *
 * @author Ervinne Sodusta
 */
class ImageUtilities {

    const AVATAR_USER    = 0;
    const AVATAR_COMPANY = 1;

    public static function getAvatarImageURL($user_id, $image_extension, $avatar = self::AVATAR_USER, $image_size = "small") {
        if ($image_extension == null || $image_extension == "") {
            $image_url = "/images/avatar/{$image_size}.png";
        } else {
            $encrypted_id = md5(md5($user_id));
            $path         = $encrypted_id . "." . "/small_" . $encrypted_id . "." . $image_extension;

            if ($avatar === self::AVATAR_USER) {
                $image_url = "/images/formalistics/tbuser/" . $path;
            } else {
                $image_url = "/images/formalistics/tbcompany/" . $path;
            }
        }

        return $image_url;
    }

}
