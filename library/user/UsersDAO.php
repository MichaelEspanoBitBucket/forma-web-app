<?php

require_once '/ImageUtilities.php';

/**
 * Description of UsersDAO
 *
 * @author Ervinne Sodusta
 */
class UsersDAO {

    /** @var APIDatabase */
    protected $database;

    /**
     * 
     * @param APIDatabase $database
     */
    public function __construct($database) {
        $this->database = $database;
    }

    public function getUserImageURL($user_id) {

        $query = "SELECT extension FROM tbuser WHERE id = {$user_id}";
        $row   = $this->database->query($query, APIDatabase::QUERY_FETCH_ROW);

        if ($row["extension"] == null || $row["requestor_image_extension"] == "") {
            $image_url = "";
        } else {
            $encrypted_id = md5(md5($user_id));
            $path         = $encrypted_id . "." . "/small_" . $encrypted_id . "." . $user_id["extension"];
            $image_url    = "/images/formalistics/tbuser/" . $path;
        }

        return ImageUtilities::getAvatarImageURL($user_id, $row["extension"]);
    }

    public function getUserCompany($user_id) {

        $query = "SELECT c.* FROM tbcompany c LEFT JOIN tbuser u ON c.id = u.company_id WHERE u.id = {$user_id} AND c.is_active = 1";
        $row   = $this->database->query($query, APIDatabase::QUERY_FETCH_ROW);

        $row["image_url"] = ImageUtilities::getAvatarImageURL($row["id"], $row["extension"], ImageUtilities::AVATAR_COMPANY);

        return $row;
    }

    public function getUserCompanyImageURL($user_id) {

        $query = "SELECT c.id, c.extension FROM tbcompany c LEFT JOIN tbuser u ON c.id = u.company_id WHERE u.id = {$user_id} AND c.is_active = 1";
        $row   = $this->database->query($query, APIDatabase::QUERY_FETCH_ROW);

        return ImageUtilities::getAvatarImageURL($row["id"], $row["extension"], ImageUtilities::AVATAR_COMPANY);
    }

    public function getUserGroups($user_id) {

        $query  = "SELECT group_id, group_name FROM tbform_groups_users gu LEFT JOIN tbform_groups g ON gu.group_id = g.id WHERE user_id = {$user_id} AND gu.is_active = 1 AND g.is_active = 1;";
        $groups = $this->database->query($query, APIDatabase::QUERY_FETCH_ALL);

        return $groups;
    }

}
