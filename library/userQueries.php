<?php

class userQueries extends upload {

    // Audit Logs
    public function auditLogs($auth, $table, $actionID, $recordID) {
        $date = $this->currentDateTime();

        $insert_audit_rec = array("user_id" => $auth['id'],
            "audit_action" => $actionID,
            "table_name" => $table,
            "record_id" => $recordID,
            "date" => $date,
            "ip" => $_SERVER["REMOTE_ADDR"],
            "is_active" => 1);

        $audit_log = $this->insert("tbaudit_logs", $insert_audit_rec);
    }

    public function getCompany($companyID) {
        $num = "1";
        $query = $this->query("SELECT * FROM tbcompany WHERE id={$this->escape($companyID)} AND is_active={$this->escape($num)}", "row");
        return $query;
    }

    public function countCompanyUsers($companyID) {
        $num = "1";
        $query = $this->query("SELECT * FROM tbuser WHERE company_id={$this->escape($companyID)} AND is_active={$this->escape($num)}", "numrows");
        if ($query < 0) {
            return "0";
        } else {
            return $query;
        }
    }

    public function countMsg($userID) {
        $msgCount = $this->query("SELECT COUNT(DISTINCT r.message_id)  FROM tbseen s
                                 LEFT JOIN tbreply_message r on r.id=s.seen_id
                                 WHERE s.userID={$this->escape($userID)} AND s.type={$this->escape(1)} AND s.user_read={$this->escape(0)} AND s.is_active={$this->escape(1)}
                                 GROUP BY r.message_id", "numrows");


        if ($msgCount < 0) {
            return "0";
        } else {
            return $msgCount;
        }
    }
    
    public function guest_avatarPic($userType, $idType, $width, $height, $size, $class, $style,$num) {
        $serverName = GUEST_MAIN_PAGE;
        
        $num = $num;

        if ($userType == "companyAdmin") {
            $table = "tbcompany";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)} AND is_active={$this->escape($num)}", "row");
            $title = $getExtension['name'];
        } else {
            $table = "tbuser";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)} AND is_active={$this->escape($num)}", "row");
            $title = $getExtension['first_name'] . ' ' . $getExtension['last_name'];
        }
        $ext = $getExtension['extension'];
        $id_encrypt = md5(md5($idType));
        if ($ext != "") {
            $img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $ext . '" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        } else {
            $img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/avatar/' . $size . '.png" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        }
        return $img;
    }

    public function guest_avatarPicSRC($userType, $idType, $width, $height, $size, $class, $style,$num) {
        $serverName = GUEST_MAIN_PAGE;
        
        $num = $num;

        if ($userType == "companyAdmin") {
            $table = "tbcompany";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)} AND is_active={$this->escape($num)}", "row");
            $title = $getExtension['name'];
        } else {
            $table = "tbuser";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)} AND is_active={$this->escape($num)}", "row");
            $title = $getExtension['first_name'] . ' ' . $getExtension['last_name'];
        }
        $ext = $getExtension['extension'];
        $id_encrypt = md5(md5($idType));
        if ($ext != "") {
            $src = $serverName . 'images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $ext;
            // $img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $ext . '" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        } else {
            $src = $serverName . 'images/avatar/' . $size . '.png';
            // $img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/avatar/' . $size . '.png" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        }
        return $src;
    }
    
    /* Avatar View
     * @userType = {companyAdmin / companyUser}
     * @$idType = {user ID  / company ID}
     * @width = width of the image
     * @height = height of the image
     * @size = {small,medium,large}
     * @class = image design/class/method
     */

    public function avatarPic($userType, $idType, $width, $height, $size, $class, $style) {
        $serverName = MAIN_PAGE;
        
        $num = "1";

        if ($userType == "companyAdmin") {
            $table = "tbcompany";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)}", "row");
            $title = $getExtension['name'];
        } else {
            $table = "tbuser";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)}", "row");
            $title = $getExtension['first_name'] . ' ' . $getExtension['last_name'];
        }
        $ext = $getExtension['extension'];
        $id_encrypt = md5(md5($idType));
        // $img_error = BROKEN_IMAGE;
        if ($ext != "") {
            $img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $ext . '" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar hehe" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        } else {
            $img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/avatar/' . $size . '.png" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        }
        return $img;
    }
    public function avatarPicSRC($userType, $idType, $width, $height, $size, $class, $style) {
        $serverName = MAIN_PAGE;
        
        $num = "1";

        if ($userType == "companyAdmin") {
            $table = "tbcompany";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)}", "row");
            $title = $getExtension['name'];
        } else {
            $table = "tbuser";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)}", "row");
            $title = $getExtension['first_name'] . ' ' . $getExtension['last_name'];
        }
        $ext = $getExtension['extension'];
        $id_encrypt = md5(md5($idType));
        // $img_error = BROKEN_IMAGE;
        if ($ext != "") {
            $src = 'images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $ext;
            //$img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $ext . '" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar hehe" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        } else {
            $src = 'images/avatar/' . $size . '.png';
            // $img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/avatar/' . $size . '.png" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        }
        return $src;
    }
    public function look_up_avatarPic($userType, $idType, $width, $height, $size, $class, $style) {
        $serverName = LOOK_UP_MAIN_PAGE;
        
        $num = "1";

        if ($userType == "companyAdmin") {
            $table = "tbcompany";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)}", "row");
            $title = $getExtension['name'];
        } else {
            $table = "tbuser";
            $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)}", "row");
            $title = $getExtension['first_name'] . ' ' . $getExtension['last_name'];
        }
        $ext = $getExtension['extension'];
        $id_encrypt = md5(md5($idType));
        if ($ext != "") {
            $img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $ext . '" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        } else {
            $img = '<img data-placement="bottom" data-original-title="' . $title . '" src="' . $serverName . 'images/avatar/' . $size . '.png" width="' . $width . '" height="' . $height . '" class="' . $class . ' userAvatar" onerror="this.src='. BROKEN_IMAGE . '" ' . $style . '>';
        }
        return $img;
    }
    public function getAvatarPicSrc($userType, $idType, $width, $height, $size, $class, $style) {
        //$serverName = $this->curPageURL("port");
        $serverName = MAIN_PAGE;
        $num = "1";
        if ($userType == "companyAdmin") {
            $table = "tbcompany";
        } else {
            $table = "tbuser";
        }

        $getExtension = $this->query("SELECT * FROM {$table} WHERE id={$this->escape($idType)} AND is_active={$this->escape($num)}", "row");
        $ext = $getExtension['extension'];
        $id_encrypt = md5(md5($idType));
        if ($ext != "") {
            $img = 'images/formalistics/' . $table . '/' . $id_encrypt . '/' . $size . '_' . $id_encrypt . '.' . $ext;
        } else {
            $img = 'images/avatar/' . $size . '.png';
        }
        return $img;
    }

    /* Avatar Group Images
     *
     *  @recipient_id = list of recipient on the msg
     *  @type = 
     *  @size_type = 
     *  @other_style = other css style included
     *
     */

    public function img_avatar_length($recipient_id, $type, $size_type, $other_style) {
        $ret = "";

        //foreach($recipient_id as $rID){
        $split_rID = explode(",", $recipient_id);
        $length_rID = count($split_rID);


        $count = "1";
        $count_1 = "1";
        foreach ($split_rID as $ID) {
            $id = $this->query("SELECT * FROM tbuser WHERE id={$this->escape($ID)}", "row");

            $extension[$length_rID][][$count++] = $id['extension'];
            $userId[$length_rID][][$count_1++] = $id['id'];
            $length[$length_rID] = $length_rID;
        }
        //}

        $list_extension = json_encode($extension);
        $list_userID = json_encode($userId);
        $list_length = json_encode($length);


        $obj_extension = json_decode($list_extension, true);
        $obj_userID = json_decode($list_userID, true);
        $obj_length = json_decode($list_length, true);

        foreach ($obj_length as $id) {

            $ret .= '<div class="pull-left avatar" style="width: 44px;height: 44px;">';

            if ($id == "1") {
                if ($obj_extension[$id][0][1] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][0][1])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][0][1])) . '.' . $obj_extension[$id][0][1] . '" width="44" height="44" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="44" height="44" class="pull-left ' . $other_style . '">';
                }
            } elseif ($id == "2") {

                if ($obj_extension[$id][0][1] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][0][1])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][0][1])) . '.' . $obj_extension[$id][0][1] . '" width="22" height="44" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="22" height="44" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][1][2] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][1][2])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][1][2])) . '.' . $obj_extension[$id][1][2] . '" width="22" height="44" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="22" height="44" class="pull-left ' . $other_style . '">';
                }
            } elseif ($id == "3") {

                if ($obj_extension[$id][0][1] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][0][1])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][0][1])) . '.' . $obj_extension[$id][0][1] . '" width="44" height="20" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="44" height="20" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][1][2] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][1][2])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][1][2])) . '.' . $obj_extension[$id][1][2] . '" width="22" height="25" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="22" height="25" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][2][3] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][2][3])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][2][3])) . '.' . $obj_extension[$id][2][3] . '" width="22" height="25" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="22" height="25" class="pull-left ' . $other_style . '">';
                }
            } elseif ($id == "4") {

                if ($obj_extension[$id][0][1] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][0][1])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][0][1])) . '.' . $obj_extension[$id][0][1] . '" width="15" height="44" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="15" height="44" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][1][2] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][1][2])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][1][2])) . '.' . $obj_extension[$id][1][2] . '" width="29" height="20" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="29" height="20" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][2][3] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][2][3])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][2][3])) . '.' . $obj_extension[$id][2][3] . '" width="14.5" height="24" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="14.5" height="24" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][3][4] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][3][4])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][3][4])) . '.' . $obj_extension[$id][3][4] . '" width="14.5" height="24" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="14.5" height="24" class="pull-left ' . $other_style . '">';
                }
            } elseif ($id == "5" || $length_rID > "5") {

                if ($obj_extension[$id][0][1] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][0][1])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][0][1])) . '.' . $obj_extension[$id][0][1] . '" width="10" height="44" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="10" height="44" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][1][2] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][1][2])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][1][2])) . '.' . $obj_extension[$id][1][2] . '" width="17" height="20" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="17" height="20" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][2][3] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][2][3])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][2][3])) . '.' . $obj_extension[$id][2][3] . '" width="17" height="20" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="17" height="20" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][3][4] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][3][4])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][3][4])) . '.' . $obj_extension[$id][3][4] . '" width="17" height="24" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="17" height="24" class="pull-left ' . $other_style . '">';
                }

                if ($obj_extension[$id][4][5] != "") {
                    $ret .= '<img src="/images/formalistics/' . $type . '/' . md5(md5($obj_userID[$id][4][5])) . '/' . $size_type . '_' . md5(md5($obj_userID[$id][4][5])) . '.' . $obj_extension[$id][4][5] . '" width="17" height="24" class="pull-left ' . $other_style . '">';
                } else {
                    $ret .= '<img src="/images/avatar/' . $size_type . '.png" width="17" height="24" class="pull-left ' . $other_style . '">';
                }
            }

            $ret .= '</div>';
        }



        return $ret;
    }

    // View Online Users
    public function onlineUser($companyID, $userID, $last_id) {

        $num = "1";
        $img = "";
        $onlineUser = $this->query("SELECT * FROM tbuser WHERE company_id={$this->escape($companyID)} AND id!={$this->escape($userID)} AND is_available={$this->escape($num)} AND is_active={$this->escape($num)}", "array");
        $countonlineUser = $this->query("SELECT * FROM tbuser WHERE company_id={$this->escape($companyID)} AND id!={$this->escape($userID)} AND is_available={$this->escape($num)} AND is_active={$this->escape($num)}", "numrows");
        $arr = array();
        $auth = Auth::getAuth('current_user');
        foreach ($onlineUser as $data) {
            
            // if (ALLOW_USER_VIEW == "1" && $auth['user_level_id'] == "3") {
            //     $host = 'http://' . $_SERVER['HTTP_HOST'] . '/user_view/';
            // } else {
            //     $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            // }
            // if (ALLOW_USER_ADMIN == "1" && $auth['user_level_id'] == "2") {
            //     $host = 'http://' . $_SERVER['HTTP_HOST'] . '/user_view/';
            // } else {
            //     $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
            // }

            $host = 'http://' . $_SERVER['HTTP_HOST'] . '/'; //default value
            if($auth['user_level_id'] == "3"){
                if(ALLOW_USER_VIEW == "1"){
                    $host = 'http://' . $_SERVER['HTTP_HOST'] . '/user_view/';
                }
            }else if($auth['user_level_id'] == "2"){
                if(ALLOW_USER_ADMIN == "1"){
                    $host = 'http://' . $_SERVER['HTTP_HOST'] . '/user_view/';
                }
            }
            
            $current_page = $host . $data['last_name'] . "_" . $data['first_name'];
//            $img .= "<a id='online_user_" . $data['id'] . "' data-id='" . $data['id'] . "' href='" . $current_page . "' class='online-user'>";
            $img .= "<a id='online_user_" . $data['id'] . "' data-id='" . $data['id'] . "' href='#' class='online-user'>";
            $img .= $this->avatarPic("", $data['id'], "23", "23", "small", "avatar onlineUser");
            $img .= "</a>";
        }
        $arr[] = array("images" => $img, "countUserOnline" => $countonlineUser, "last_id" => $data['id']);
        return $arr;
    }

    /**
     * Added by ervinne
     * Gets the only the data of the online users
     */
    public function getOnlineUsersForChat($companyID, $currentUserID) {

        $userDataList = array();

        $isActive = "1";        
        $onlineUsers = $this->query("SELECT id, display_name, first_name, last_name FROM tbuser "
                . "WHERE company_id={$this->escape($companyID)} "
                . "AND id!={$this->escape($currentUserID)} "
                . "AND is_available={$this->escape($isActive)} "
                . "AND is_active={$this->escape($isActive)}", "array");
                
        //  get the images of each users
        foreach ($onlineUsers as $onlineUser) {
 
            $imageURL = $this->getAvatarPicSrc("user", $onlineUser['id'], 23, 23, "Small");
            $userData = array(
                'id' => $onlineUser['id'],
                'displayName' => $onlineUser['display_name'],
                'imageURL' => $imageURL
            );
            
            array_push($userDataList, $userData);
        }
        
        return $userDataList;
    }

    // User level

    public function userLevel($level) {

        $userLevel = $this->query("SELECT * FROM tbuser_level WHERE id={$this->escape($level)}", "row");

        return $userLevel['user_level'];
    }

}

?>