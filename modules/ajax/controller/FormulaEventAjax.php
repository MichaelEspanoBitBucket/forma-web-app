<?php
// error_reporting(E_ALL);
    if( defined("ENABLE_FORMBUILDER_FORM_EVENTS") != null  && ENABLE_FORMBUILDER_FORM_EVENTS == "1" ){
        $post_variable = $_POST;
        $formula_variable = $post_variable["formula"];
        $field_model_variable = json_decode($post_variable["field_model"],true);
        //tanong ko bukas
        $formula_doc = new Formula($formula_variable);
        $formula_doc->DataFormSource[0] = $field_model_variable;

        $_POST = $field_model_variable;

        // var_dump($post_variable["computed_formula_event_list"]);
        $_POST['_formula_list_data'] = json_decode($post_variable["computed_formula_event_list"],true);
        $_POST['_form_reserve_keys'] = $post_variable["form_reserve_keys"];
        $_POST['_form_user_fields'] = $post_variable["form_user_fields"];
        $_POST['_form_variable_data_source'] = $post_variable["form_variable_data_source"];
        $_POST['_formula_list_onchange_auto_excluded_fieldname'] = $post_variable["formula_list_onchange_auto_excluded_fieldname"];
        // echo $formula_doc->evaluate();

        $data_sources = $_POST['_form_variable_data_source'];
        foreach ($data_sources as $var) {
			if ($var["var_compute_type"] != "static") {
                $formulaDoc = new Formula($var["var_formula"]);
    			$formulaDoc->DataFormSource[0] = $_POST;
                $_SESSION["DataSource_".$var["var_name"]] = $formulaDoc->evaluate();
			}
        }

		
        echo json_encode( $formula_doc->evaluate() ,JSON_PRETTY_PRINT);

        // {
        //     "visibility":{
        //         "field_name1":true,
        //         "field_name2":false
        //     },
        //     "computed_value":{
        //         "field_name1":"value"
        //     },
        //     "readonly":{
        //         "field_name1":true
        //     },
        //     "validation":{
        //         "field_name1":{
        //             "condition":false,
        //             "message":"invali data"
        //         },
        //         "field_name2":{
        //             "condition":false,
        //             "message":"invali data"
        //         }
        //     }
        // }
    }else{
        echo "['this was disabled on the configuration']";
    }
?>