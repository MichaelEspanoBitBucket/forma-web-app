<?php
// error_reporting(E_ALL);

$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->SetFont('Arial','',12);
$pdf->Image('http://chart.googleapis.com/chart?cht=p3&chd=t:60,40&chs=250x100&chl=Hello|World',0,5,90,0,'PNG');
// $pdf->Cell(40,10,'4/F, ODC International Plaza', 0, 0, 'L' ); 
$pdf->TextWithDirection(130, 15, '4/F, ODC International Plaza!', 'R');
$pdf->Ln(5);
$pdf->TextWithDirection(130, 20, '219 Salcedo Street, Legaspi Village', 'R');
$pdf->Ln(5);
$pdf->TextWithDirection(130, 25, 'Makati Metro Manila', 'R');
$pdf->Ln(5);
$pdf->TextWithDirection(130, 30, 'Philippines', 'R');
$pdf->Ln(5);
$pdf->TextWithDirection(130, 35, 'Tel Nos. 818 00 55, 818 01 40', 'R');
$pdf->Ln(5);
$pdf->TextWithDirection(130, 40, 'Fax No.  817 46 70', 'R');
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Ln(5);

// $pdf->Ln(5);
$pdf->Cell(40,10,'BPI/MS INSURANCE CORPORATION (077)', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->Cell(40,10,'16th Flr., BPI Main Bldg.', 0, 0, 'L' );      
$pdf->Ln(5);     
$pdf->Cell(40,10,'6668 Ayala cor. Paseo de Roxas', 0, 0, 'L' ); 
$pdf->Ln(5);     
$pdf->Cell(40,10,'Makati City', 0, 0, 'L' );   
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Ln(5);


$pdf->Cell(40,10,'Attn.: Mr. Perfecto M. Domingo ', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->TextWithDirection(20, 96, 'Sr. Business Director', 'R');      
$pdf->Ln(5);
$pdf->Ln(5);


$pdf->Cell(40,10,'Quot.: BPV-2014-0002-01 ', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->TextWithDirection(150, 106, 'Date : Feb 21, 2015', 'R');
$pdf->Ln(5);
$pdf->Ln(5);

$pdf->Cell(180,10,'BOILER & PRESSURE VESSEL INS. ', 0, 0, 'C' ); 
$pdf->Ln(5);
$pdf->Cell(180,10,'BPI Rental Corp. fot the Account of ', 0, 0, 'C' ); 
$pdf->Ln(5);
$pdf->Cell(180,10,'CYS AQ-AG Enterprises, Inc.', 0, 0, 'C' ); 
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Ln(5);

$pdf->Cell(40,10,'We are pleased to confirm the following quotation upon the request of your Ms. J. Judi ', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->Ln(5);

$pdf->Cell(30,10,'Particulars', 0, 0, 'R' ); 
$pdf->Ln(5);
$pdf->Cell(30,10,'Insuring', 0, 0, 'R' ); 
$pdf->Ln(5);
$pdf->TextWithDirection(50, 161, ': 1 Unit DZG4-1.25-M Boiler w/ Standard Accessories', 'R');
$pdf->Ln(5);
$pdf->Ln(5);


$pdf->Cell(30,10,'Site', 0, 0, 'R' ); 
$pdf->Ln(5);
$pdf->TextWithDirection(50, 176, ': Brgy. Panalayunan, Mabalacat, Pampanga ', 'R');
$pdf->Ln(5);
$pdf->Ln(5);

$pdf->Cell(30,10,'Sum Insured', 0, 0, 'R' ); 
$pdf->Ln(5);
$pdf->Cell(30,10,'Machinery', 0, 0, 'R' ); 
$pdf->Ln(5);
$pdf->TextWithDirection(50, 196, ': pp9,297,000.00', 'R');

$pdf->Cell(30,10,'Property', 0, 0, 'R' ); 
$pdf->TextWithDirection(50, 201, ': pp0.00', 'R');
$pdf->Ln(5);
$pdf->Cell(30,10,'TPL', 0, 0, 'R' ); 
$pdf->TextWithDirection(50, 206, ': pp0.00', 'R');
$pdf->Ln(5);
$pdf->Cell(30,10,'Total', 0, 0, 'R' ); 
$pdf->TextWithDirection(50, 211, ': pp9,297,000.00', 'R');
$pdf->Ln(5);
$pdf->Cell(30,10,'Duration', 0, 0, 'R' ); 
$pdf->TextWithDirection(50, 216, ': Annual Basis', 'R');


$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Cell(30,10,'Our Quote', 0, 0, 'R' ); 
$pdf->TextWithDirection(50, 226, ': ', 'R');
$pdf->Ln(5);
$pdf->Cell(30,10,'Annual Rate', 0, 0, 'R' ); 
$pdf->TextWithDirection(50, 231, ': 0.2000% computed on pp9,297,000.00', 'R');
$pdf->Ln(5);
$pdf->Cell(30,10,'Deductibles', 0, 0, 'R' ); 
$pdf->TextWithDirection(50, 236, ': P50,000.00 any one accident', 'R');
$pdf->Ln(5);
$pdf->Cell(30,10,'Endorsements', 0, 0, 'R' ); 
$pdf->TextWithDirection(50, 241, ': 996 Terrorism Exclusion', 'R');
$pdf->Ln(5);
$pdf->Ln(5);

$pdf->Cell(40,10,'Please advise your client that sum insured should be based on new replacement value.', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Cell(30,10,'This quotation is subject to our standard policy wordings,its limitations, exclusions, conditions, ', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->Cell(30,10,'provisions and the,endorsements indicated hereto, if any.', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->Ln(5);


$pdf->Cell(30,10,'Quot.', 0, 0, 'L' ); 
$pdf->TextWithDirection(30, 16, ': BPV-2014-0002-01', 'R');
$pdf->Ln(5);

$pdf->TextWithDirection(150, 16, 'Date : 02/21/2015', 'R');
$pdf->TextWithDirection(150, 21, 'Page : 2', 'R');
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Ln(5);

$pdf->Cell(40,10,'This quotation is valid until 05/30/2014', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Cell(40,10,'Very truly yours,', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Cell(40,10,'Phil. Machinery Management Services Corp.', 0, 0, 'L' ); 
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Ln(5);

$pdf->Line(10,90,80,90);
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Ln(5);
$pdf->Cell(40,10,'Authorized Signature', 0, 0, 'L' ); 



$pdf->Output('pdf-request/Quotation Letter BFV.pdf','F');
?>