<?php

// var_dump("Whew");

    $db = new Database();
    $messaging_dao    = new MessagingDAO();
    
    $parameters = filter_input_array(INPUT_POST);


    $auth = Auth::getAuth('current_user');
    $author_id = $auth['id'];
    $thread_id = $parameters['thread_id'];
    $recipient_tobe_added = $parameters['tobeadded'];
    $tb_thread_subscriber_messages = array();
    $subscribers = array();

        foreach ($recipient_tobe_added as $recipient_id) {
            array_push($subscribers, array(
                "user_id" => $recipient_id,
                "type" => "writer",
                "thread_id" => $thread_id,
                "date_updated" => "NOW()"
            ));
        }
        //add as Thread viewer
        $result = $db->multipleInsert("tb_thread_subscribers", $subscribers);


        //add new view to previous messages
        $sql2 = 'SELECT id FROM tb_thread_messages WHERE thread_id = '.$thread_id;
        $threadMsgId = $db->query($sql2);
        foreach ($recipient_tobe_added as $newrecp) {
            foreach ($threadMsgId as $msgID) {
                array_push($tb_thread_subscriber_messages, array(
                    "message_subscriber_user_id" => $newrecp,
                    "thread_message_id" => $msgID['id'],
                    "read" => '0',
                    "is_active" => '1',
                    "date_updated" => "NOW()"
                ));
            }
        }
        $result2 = $db->multipleInsert("tb_thread_subscriber_messages", $tb_thread_subscriber_messages);

        //get new Recipient name
        $sql = 'SELECT display_name FROM tbuser WHERE id IN ("'.implode('","',$recipient_tobe_added).'")';
        $name = $db->query($sql);

        //create message for callback
        if(count($name) == 1){
            $message = 'Added '. $name[0]['display_name'] .' to the group.';
        }elseif (count($name) == 2) {
            $message = 'Added '. $name[0]['display_name'] .' and '.$name[1]['display_name'].' to the group.';
        }else{
            $message = 'Added '. $name[0]['display_name'] .','. $name[1]['display_name'] .' and '. (count($name) - 2) .' '.((count($name) - 2) > 1 ? "others" : "other").' to the group.';
        }
        echo $message;

   ?>