<?php
$db = new Database();
$data_sources = json_decode($_POST["dataSources"], true);

foreach ($data_sources as $var) {
    $formulaDoc = new Formula($var["var_formula"]);
    $_SESSION["DataSource_".$var["var_name"]] = $formulaDoc->evaluate();
//    var_dump($_SESSION["DataSource_".$var["var_name"]]);
}

$fields = json_decode($_POST['data'], true);
$return_arr = array();

// print_r($fields);
foreach ($fields as $key) {
    $field_name = $key['FieldName'];
    $field_formula = $key['Formula'];
    $formulaDoc = new Formula($field_formula);
    $formulaDoc->addFormSourceData(array("RequestID" => $key['RequestID']));
    $return_value = $formulaDoc->evaluate();

    array_push($return_arr, array(
        "FieldName" => $field_name,
        "FieldValue" => $return_value)
    );
}

print_r(json_encode($return_arr));
?>