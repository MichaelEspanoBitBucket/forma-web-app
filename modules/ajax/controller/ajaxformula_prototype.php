	<?php
	$db = new Database();
	$field_models = $_POST['field_models'];
	// $collection_name = $_POST['trigger_chain_collection'];
	$waiting_collection = array();
	$result = array();
	$onload_result = array();
	$chain_debug = array();
	$count = "";
	$formulas = "";
	$prefix = "";
	if(isset($_POST['onload_fields'])){
		// foreach($_POST['onload_fields'] as $key => $value){
		// 	if(strlen($value['default_computed']) > 0){
		// 		$formulaDoc = new Formula($value['default_computed']);
		// 		$formulaDoc->setFieldModel("field_models", $field_models);
		// 		$field_models[$key] = $formulaDoc->evaluate();
		// 		$onload_result[$key]['default_computed'] = $formulaDoc->evaluate();
		// 		$formulas .= "default computed key" . ", " . $value[$key]['default_computed'] . "; ";
		// 	}
			
		// 	//readonly==========================================================>

		// 	if(strlen($value['readonly']) > 0){
		// 		$formulaDoc = new Formula($value['readonly']);
		// 		$formulaDoc->setFieldModel("field_models", $field_models);
		// 		$onload_result[$key]['readonly'] = $formulaDoc->evaluate();
		// 		$formulas .= "readonly key" . ", " . $value[$key]['readonly'] . "; ";
		// 	}

		// 	//visibility========================================================>

		// 	if(strlen($value['visibility']) > 0){
		// 		$formulaDoc = new Formula($value['visibility']);
		// 		$formulaDoc->setFieldModel("field_models", $field_models);
		// 		$onload_result[$key]['visibility'] = $formulaDoc->evaluate();
		// 		$formulas .= "visibility key" . ", " . $value[$key]['visibility'] . "; ";
		// 	}
		// }
		// $onload_result['on_load'] = true;
		// echo json_encode($onload_result);
		// print_r($_POST['onload_fields']);
	}
	else{
		// print_r($_POST['lookup_fields']);
		if(isset($_POST['field_models'])){
			$field_models = $_POST['field_models'];
			if(ISSET($_POST['prefix'])){
				$prefix = $_POST['prefix'];
			}
			else{
				$prefix = "";
			}
			foreach($_POST['lookup_fields'] as $key => $value){
				$count .= "key: " . $key . "; \n";
				// print_r($_POST['lookup_fields'][$key][$key]);

				//default computed==================================================>
				if(strlen($value[$key]['default_computed']) > 0){
					$field_model_key = $prefix . $key;
					$formulaDoc = new Formula($value[$key]['default_computed']);
					$formulaDoc->setFieldModel("field_models", $field_models);
					$field_models[$field_model_key] = $formulaDoc->evaluateV2();
					$result[$key]['default_computed'] = $field_models[$field_model_key];
					$formulas .= "Key: " . $key . ", default computed result: " . $field_models[$key] . "; \n";
				}
		
				//readonly==========================================================>

				if(strlen($value[$key]['readonly']) > 0){
					$formulaDoc = new Formula($value[$key]['readonly']);
					$formulaDoc->setFieldModel("field_models", $field_models);
					$result[$key]['readonly'] = $formulaDoc->evaluateV2();
					$formulas .= "readonly key" . ", " . $value[$key]['readonly'] . "; ";
				}

				//visibility========================================================>

				if(strlen($value[$key]['visibility']) > 0){
					$formulaDoc = new Formula($value[$key]['visibility']);
					$formulaDoc->setFieldModel("field_models", $field_models);
					$field_models[$field_model_key] = $formulaDoc->evaluateV2();
					$result[$key]['visibility'] = $formulaDoc->evaluateV2();
					$formulas .= "visibility key" . ", " . $value[$key]['visibility'] . "; ";
				}
				// print_r( $key . ", " . $field_models[$key]);
				// print_r($value);
				//invovle fields====================================================>
				foreach($_POST['lookup_fields'][$key]['invovle_fields']['root_formula_default_computed'] as $name => $value){
					$field_model_key = $prefix . $name;
					$fm_key = $prefix . $key;
					// print_r("field_model: " . $fm_key . ",  default computed value: "  . $field_models[$fm_key] . "\n");
					$formulaDoc = new Formula($value);
					$formulaDoc->setFieldModel("field_models", $field_models);
					$res = $formulaDoc->evaluateV2();
					$field_models[$field_model_key] = $res;
					$result[$name]['default_computed'] =  $res;
					// print_r(" involve key " . $name . ", formula : " . $value . " default_computed result: " . $res . ", field_model: " . $field_models[$field_model_key] . "\n");
				}
				foreach($_POST['lookup_fields'][$key]['invovle_fields']['root_formula_readonly'] as $name => $value){
					$field_model_key = $prefix . $name;
					$fm_key = $prefix . $key;
					// print_r("field_model: " . $fm_key . ", readonly  value: "  . $field_models[$fm_key] . "\n");
					$formulaDoc = new Formula($value);
					$formulaDoc->setFieldModel("field_models", $field_models);
					$res = $formulaDoc->evaluateV2();
					$result[$name]['readonly'] = $res;
					// print_r(" involve key " . $name . ", formula : " . $value . ", readonly result: " . $res . ", field_model: " . $field_models[$field_model_key] . "\n");
				}
				foreach($_POST['lookup_fields'][$key]['invovle_fields']['root_formula_visibility'] as $name => $value){
					$field_model_key = $prefix . $name;
					$fm_key = $prefix . $key;
					// print_r("field_model: " . $fm_key . ", visibility value: "  . $field_models[$fm_key] . "\n");
					$formulaDoc = new Formula($value);
					$formulaDoc->setFieldModel("field_models", $field_models);
					$res = $formulaDoc->evaluateV2();
					$result[$name]['visibility'] = $res;
					// print_r(" involve key " . $name . ", formula : " . $value . " visibility result: " . $res . ", field_model: " . $field_models[$field_model_key] . "\n");
				}
			}
			echo json_encode($result);
			// echo $count;
			// echo $formulas;
		}
	}
?>