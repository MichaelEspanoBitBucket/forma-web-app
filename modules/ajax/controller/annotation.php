<?php
$db = new Database();
$fs = new functions();
$auth = Auth::getAuth('current_user');
$date = $fs->currentDateTime();
$count = 0;
if(isset($_POST['action'])){
    
    if($_POST['action'] == "draft_annotation"){
        $FormName = $_POST['FormName'];
        $TrackNo = $_POST['TrackNo'];
        $FormID = $_POST['FormID'];
        $ID = $_POST['ID'];
        
        
        
        $id_encrypt = md5(md5($auth['id']));
				$dir1 = 'frm-annotation-img/temporary/'.$id_encrypt;
				if(!is_dir($dir1)){
				    mkdir($dir1); // location
				}
				$dir = $dir1 . '/' .$FormID;
				if(!is_dir($dir)){
				    mkdir($dir); // location
				}
        
        // Get all first letter in a word
        $words = explode(" ", $FormName);
        $acronym = "";
        
        foreach ($words as $w) {
          $acronym .= $w[0];
        }
        
        upload::unlinkRecursive($dir, "");
        
        //// Save to DB
        //$annotation_insert = array("Anno_FormID"            =>      $FormID,
        //                            "Anno_RequestID"        =>      $ID,
        //                            "Anno_TrackNo"          =>      "000",
        //                            "Anno_CreatedBy"        =>      $auth['id'],
        //                            "Anno_UpdatedBy"        =>      $auth['id'],
        //                            "Anno_DataCreated"      =>      $date,
        //                            "Anno_DateUpdated"      =>      $date,
        //                            "Anno_is_active"        =>      "1");
        //
        //$db->insert("tb_annotation",$annotation_insert);
        
        // Generate Image
        $image = imagecreatefrompng($_POST['image']);
        $id = uniqid();
        
        imagealphablending($image, false);
        imagesavealpha($image, true);
        imagepng($image, $dir .'/'. $acronym . '-' . $id . '-' . $count .'.png');
        
        
        
        echo json_encode($_POST);
    }

}





