<?php
$db = new Database;
$fs = new functions;

if(isset($_POST) and $_SERVER['REQUEST_METHOD']=="POST"){
    
    // Registration Values
    
        $companyName = $_POST['companyName'];
        $companyCode = $_POST['companyCode'];
        $companyCNumber = $_POST['companyCNumber'];
        //$companyLogo = $_FILES['companyLogo']['name'];
        //    $companyLogo_size = $_FILES['companyLogo']['size']; // File Size
        //    list($companyLogo_txt, $companyLogo_ext) = explode(".", $companyLogo);
        $companyEmail = $_POST['companyEmail'];
        $companyDisplayName = $_POST['companyDisplayName'];
        $companyFname = $_POST['companyFname'];
        $companyLname = $_POST['companyLname'];
        $companyPosition = $_POST['companyPosition'];
        $companyPassword = md5($_POST['companyPassword']);
        //$companyUserPic = $_FILES['companyUserPic']['name'];
        //    $companyUserPic_size = $_FILES['companyUserPic']['size']; // File Size
        //    list($companyUserPic_txt, $companyUserPic_ext) = explode(".", $companyUserPic);
        //
        //// User file path
        //$path = "images/users/";
        //    
        //// Allowed extension for file upload in the registration    
        //$valid_formats = array("jpg", "png", "JPG");
        
        
        
        
        // Set Conditions on the registration
        if(empty($companyName) /*or empty($companyCode) or empty($companyCNumber) or empty($companyEmail) or empty($companyDisplayName) or empty($companyFname) or empty($companyLname) or empty($companyPosition) or empty($companyPassword)*/){
            echo functions::setNotification("wrong","wrong","Please Complete all required fields.");
        }else{
            //if(!in_array($companyLogo_ext,$valid_formats) && !empty($companyLogo) || !in_array($companyUserPic_ext,$valid_formats) && !empty($companyUserPic) ){
            //    echo "Invalid File Format";
            //}else{
                if(!$fs->VerifyMailAddress($companyEmail)){
                    echo $fs->setNotification("wrong","wrong","Please type your correct email format.");
                }else{
                    //if (($companyLogo_size >= 2097152 *2097152 ) || ($companyUserPic_size >= 2097152 *2097152 )){
                    //    echo "File too large. File must be less than 4 megabytes.";
                    //}else{
                        $date = $fs->currectDateTime();
                        // Save Company to db
                        $insertCompany = array("code"=>$companyCode,
                                                "name"=>$companyName,
                                                "contact_number"=>$companyCNumber,
                                                "email"=>$companyEmail,
                                                "extension"=>$companyLogo_txt,
                                                "date_registered"=>$date,
                                                "is_active"=>1);
                            $companyID = $db->insert("tbcompany",$insertCompany);
                        
                        // Save user to db
                        $insertUser = array("email"=>$companyEmail,"display_name"=>$companyDisplayName,
                                            "first_name"=>$companyFname,"last_name"=>$companyLname,
                                            "contact_number"=>$companyCNumber,"position"=>$companyPosition,
                                            "password"=>$companyPassword,"extension"=>$companyUserPic_ext,
                                            "date_registered"=>$date,"is_active"=>1);
                            $userID = $db->insert("tbuser",$insertUser);
                            
                        echo $fs->setNotification("correct","correct","User was successfully saved.");
                        //// Create Folder of the user for their image
                        //mkdir($path."/".$userID); // location
                        //    mkdir($path."/".$userID."/1"); // location
                        //    mkdir($path."/".$userID."/2"); // location
                        //    
                        //// Resize an image
                        //    if($companyLogo_ext=="jpg" || $companyLogo_ext=="jpeg" || $companyLogo_ext=="JPG"){
                        //         $uploadedfile = $_FILES['companyLogo']['tmp_name'];
                        //         $src = imagecreatefromjpeg($uploadedfile);
                        //    }
                        //    else if($companyLogo_ext=="png" || $companyLogo_ext=="PNG"){
                        //         $uploadedfile = $_FILES['companyLogo']['tmp_name'];
                        //         $src = imagecreatefrompng($uploadedfile);
                        //    }
                        //    else{
                        //         $src = imagecreatefromgif($uploadedfile);
                        //    }
                        //    echo $scr;
                        //    
                        //    list($width,$height)=getimagesize($uploadedfile);
                        //    // Set 1
                        //    $newwidth=100;
                        //    $newheight=($height/$width)*$newwidth;
                        //    $tmp=imagecreatetruecolor($newwidth,$newheight);
                        //    
                        //    imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
                        //    
                        //    $thumbnail = $path."/".$userID."/"."1/yourAvatar_".$userID.".".$ext;
                        //    
                        //    imagejpeg($tmp,$thumbnail,100);
                        //    
                        //    // Set 2
                        //    $newwidth1=60;
                        //    $newheight1=($height/$width)*$newwidth1;
                        //    $tmp1=imagecreatetruecolor($newwidth1,$newheight1);
                        //    
                        //    imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);
                        //    
                        //    $thumbnail1 = $path."/".$userID."/"."2/yourAvatar2_".$userID.".".$ext;
                        //    
                        //    imagejpeg($tmp1,$thumbnail1,100);
                        //    
                        //    imagedestroy($src);
                        //    imagedestroy($tmp);
                        //    imagedestroy($tmp1);
                    //}
                }
            //}
            //echo "Successfully Saved.";
        }
        
}


?>