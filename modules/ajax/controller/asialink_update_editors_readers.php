<?php

$db = new Database();

$fields = $db->query("SELECT field_name, field_type FROM tbfields WHERE form_id = 325 AND field_type IN ('textbox_editor_support', 'textbox_reader_support')");
$field_references = array();
foreach ($fields as $field) {
    array_push($field_references, $field["field_name"]);
}

$requests = $db->query("SELECT id, " . implode(",", $field_references) . "  FROM 1_tbl_editorreaderupdate");

//$insert_sql = " INSERT INTO tbrequest_users (Form_ID, RequestID, User, User_Type, Action_Type)  VALUES ";
foreach ($requests as $request) {
    foreach ($fields as $field) {
        $field_name = $field["field_name"];
        $field_type = $field["field_type"];
        $field_value = $request[$field_name];

        $person = $db->query("SELECT id FROM tbuser WHERE display_name={$db->escape($field_value)}", "row");

        if ($field_type == "textbox_editor_support") {
            $action_type = "1";
        }

        if ($field_type == "textbox_reader_support") {
            $action_type = 2;
        }

        $db->insert("tbrequest_users", array("Form_ID" => "325",
            "RequestID" => $request["id"],
            "User" => $person["id"],
            "User_Type" => "3",
            "Action_Type" => $action_type
        ));

//        var_dump($field_value);
//        var_dump($field_type);
    }
}

//$insert_sql = substr($insert_sql, 0, -1);
//$db->query($insert_sql);
//print_r($insert_sql);
