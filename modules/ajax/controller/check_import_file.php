<?php
    $auth = Auth::getAuth('current_user');
    $action = $_POST['action'];
    $keyword_to_be_checked = $_POST['keyword_to_be_checked'];
    $final_keyword = $_POST['final_keyword']?:"";
    $filename = $_POST['file_name'];
    $offset = $_POST['offset']?:"";
    $fileloc = APPLICATION_PATH . "\\logs\\import_logs\\" . $filename . ".txt";
    $file = fopen($fileloc, "r") or die('could not find the file');
    $text_file = fread($file, filesize($fileloc));
    fclose($file);
    // echo $keyword_to_be_checked;
    if($action == "manual"){
        if(strpos($text_file, $keyword_to_be_checked[0]) || strpos($text_file, $keyword_to_be_checked[1]) || strpos($text_file, $keyword_to_be_checked[2])){
            echo "true";
        }
        else{
            echo "false";
        }
    }
    else{
        if(strpos($text_file, $final_keyword)){
            echo "true";
        }
        else{
            $field_name = $_POST['field_name'];
            $field_value = $_POST['field_value'];
            $form_id = $_POST['form_id'];
            $db = new Database();
            $formDoc = new Form($db, $form_id);
            $tbl_name = $formDoc->form_table_name;
            $result = $db->query("SELECT COUNT(*) as TotalCount FROM " . $tbl_name . " WHERE `" . $field_name . "` = '" . $field_value . "' AND TrackNo is not null");
            $ret = array(
                        "query"=>"SELECT COUNT(*) as TotalCount FROM " . $tbl_name . " WHERE `" . $field_name . "` = '" . $field_value . "' AND TrackNo is not null",
                        "result"=>$result,
                        "offset"=>$result[0]['TotalCount']
                    );
            echo json_encode($ret);
        }
    }
?>