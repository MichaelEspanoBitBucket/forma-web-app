<?php

$redis_cache = getRedisConnection();
$get_array = filter_input_array(INPUT_GET);
$form_id = $get_array["form_id"];
$request_id = $get_array["request_id"];

if ($redis_cache) {
    $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

    $deleteMemecachedKeys = array_merge($myrequest_memcached);

    functions::deleteMemcacheKeys($deleteMemecachedKeys);

    functions::clearRequestRelatedCache("request_list", "form_id_" . $form_id);
    functions::clearRequestRelatedCache("picklist", "form_id_" . $form_id);
    functions::clearRequestRelatedCache("calendar_view", "form_id_" . $form_id);
    functions::clearRequestRelatedCache("request_list_count", "form_id_" . $form_id);
    functions::clearRequestRelatedCache("request_details_" . $form_id, $request_id);
    functions::clearRequestRelatedCache("request_access_" . $form_id, $request_id);


    //formula
    $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
        "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
        "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
        "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
    functions::clearFormulaLookupCaches($form_id, $formula_arr);
}