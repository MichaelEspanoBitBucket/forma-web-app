<?php

$auth = Auth::getAuth('current_user');
$hasAuth = Auth::hasAuth('current_user');
$userCompany = new userQueries();
$notifications = new notifications();
functions::currentDateTime();

if ($hasAuth) {
    $homePage = null;
    $loginPage = null;
    $indexPage = null;
    $is_available = '1';
} else {
    $homePage = '/';
    $loginPage = '/login';
    $indexPage = '/index';
    $is_available = '0';
}
$counter = array(   "userCount"             =>      userQueries::counterLimmit(userQueries::countCompanyUsers($auth['company_id'])),
                    "messageCount"          =>      userQueries::countMsg($auth['id']),
                    "requestCount"          =>      Request::forMyApproval($auth['company_id'], $auth['id']),
                    "notiCount"             =>      $notifications->countNotification(),
                    "onlineUsers"           =>      $userCompany->onlineUser($auth['company_id'], $auth['id'],""),
                    "home_page"             =>      $homePage,
                    "login_page"            =>      $loginPage,
                    "index_page"            =>      $indexPage,
                    "is_available"          =>      $is_available,
                    "getDateTime"           =>      date("F d, Y H:i:s"));

echo json_encode($counter)
?>