<?php

$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions();
$date = $fs->currentDateTime();

$timesheets = $db->query("SELECT * FROM 15_tbl_paysheet", "array");

foreach ($timesheets as $timesheet) {
    $requestDoc = new Request();
    $requestDoc->load(235, 0);
    $formDoc = new Form($db, 235);
    $data["PayslipManNo"] = $timesheet["PaysheetManNum"];
    $data["EMpName"] = $timesheet["PaysheetName"];
    $data["PayslipFrom"] = $timesheet["PayPeriodStart"];
    $data["PayslipTo"] = $timesheet["PayPeriodEnd"];
    $data['Requestor'] = "240";
    $data['Processor'] = "240";
//    $data['Status'] = 'Draft';
//    $data['imported'] = '1';
    $workflow_object = functions::getFormActiveWorkflow($formDoc);
    $workflow_json = json_decode($workflow_object["json"], true);
    $data['Workflow_ID'] = $workflow_object["workflow_id"];
    $data['Node_ID'] = $workflow_json["workflow-default-action"];
    $data['DateCreated'] = $date;
    $data['DateUpdated'] = $date;
    $requestDoc->data = $data;

    $requestDoc->save();
    $requestDoc->processWF();
}

print_r("success");
?>