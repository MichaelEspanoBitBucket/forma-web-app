<?php
$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$userCompany = new userQueries();
$company_id = $auth['company_id'];
$userID = $auth['id'];
$timezone = "Asia/Manila";
$search = new Search();
$delegate = new Delegate();
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

    if(isset($_POST['action'])){
        $action = $_POST['action'];
        $date = $fs->currentDateTime();
        $search_value = $_POST['search_value'];
        if($action=="loadDelegateDatatable"){
            $start = $_POST['iDisplayStart'];
            $arr = array('column-sort'=>$_POST['column-sort'],'column-sort-type'=>$_POST['column-sort-type'],"endLimit"=>$_POST['limit']);


            $getDelegate = $delegate->getDelegate($search_value,$start,$arr,"array");
            $countDelegate = $delegate->getDelegate($search_value,$start,array(),"numrows");
            $output = array(
                "sEcho" => intval($_POST['sEcho']),
                "iTotalRecords" => $countDelegate,
                "iTotalDisplayRecords" => $countDelegate,
                "start"=>$start,
                "aaData" => array(),
            );    
            foreach ($getDelegate as $delegate) {
                $arr = array();  
                $actions = '<center>
                <i class="icon-trash fa fa-trash-o tip cursor deleteDelegate" data-placement="top" data-original-title="Delete" style="margin-right:5px" data-id="'. $delegate['id'] .'"></i> 
                <i class="icon-edit fa fa-pencil-square-o tip cursor updateDelegate" data-placement="top" data-original-title="Edit" json_data="'. htmlentities(json_encode($delegate)) .'"></i>';
                
                $createdDate = $info['createdDate'];
                if($createdDate=="0000-00-00 00:00:00"){
                    $createdDate = "";
                }
                $updatedDate = $info['updatedDate'];
                if($updatedDate=="0000-00-00 00:00:00"){
                    $updatedDate = "";
                }
                
                $arr[] = "<div class='fl-table-ellip'>" . $delegate['delegate_user']."</div>";
                $arr[] = "<div class='fl-table-ellip'>" . $delegate['delegate_to_user']."</div>";
                $arr[] = "<div class='fl-table-ellip'>" . $delegate['start_date']."</div>";
                $arr[] = "<div class='fl-table-ellip'>" . $delegate['end_date']."</div>";
                $arr[] = "<div class='fl-table-ellip'>" . $delegate['remarks']."</div>";
                // $arr[] = "<div class='fl-table-ellip'>" . $delegate['date_created']."</div>";
                // $arr[] = "<div class='fl-table-ellip'>" . $delegate['date_updated']."</div>";
                $arr[] = $actions;
                $output['aaData'][] = $arr;
            }
            echo json_encode($output);
        }else if($action=="deleteDelegate"){
            $id = $_POST['data_id'];
            echo $db->delete("tbdelegate",array("id"=>$id));
        }else if($action=="addDelegate"){
            $user_id = $_POST['user_id'];
            $user_delegate_id = $_POST['user_delegate_id'];
            $remarks = $_POST['remarks'];
            $start_date = $_POST['start_date'];
            $end_date = $_POST['end_date'];
            $json = array("condition"=>" AND user_id = $user_id","endLimit"=>1);
            $countDelegate = $delegate->getDelegate("",0,$json,"numrows");
            if($countDelegate>0){
                echo "0";
                return false;
            }
            $insert = array("user_id"=>$user_id,
                "user_delegate_id" =>$user_delegate_id,
                "remarks"=>$remarks,
                "start_date"=>$start_date,
                "end_date"=>$end_date,
                "created_by"=>$auth['id'],
                "date_created"=>$date
            );
            echo $db->insert("tbdelegate",$insert);

        }else if($action=="editDelegate"){
            $data_id = $_POST['data_id'];
            $user_delegate_id = $_POST['user_delegate_id'];
            $remarks = $_POST['remarks'];
            $start_date = $_POST['start_date'];
            $end_date = $_POST['end_date'];

            $insert = array("remarks"=>$remarks,
                "user_delegate_id" =>$user_delegate_id,
                "start_date"=>$start_date,
                "end_date"=>$end_date,
                "updated_by"=>$auth['id'],
                "date_updated"=>$date,

            );
            $where = array("id"=>$data_id);
            echo $db->update("tbdelegate",$insert,$where);
        }

    }


?>