<?php

//error_reporting(E_ALL);
$db = new Database();

$forms = $db->query("select id, form_table_name, company_id, form_name from tb_workspace where is_active = 1");
$query = "SELECT "
        . " processor_delegate.display_name as delegate_display_name, "
        . " processor_delegate.email as delegate_email, "
        . " processor.display_name AS processor_display_name, "
        . " processor.id AS processor_id "
        . " FROM tbdelegate delegate"
        . " LEFT JOIN tbuser processor"
        . " ON processor.id = delegate.user_id"
        . " LEFT JOIN tbuser processor_delegate "
        . " ON processor_delegate.id = delegate.user_delegate_id "
        . " WHERE (DATE(now()) BETWEEN DATE(start_date) AND DATE(end_date)) ";

$result = $db->query($query, "array");
$from = SMTP_FROM_EMAIL;
$from_title = SMTP_FROM_NAME;
$subject = "Requests For Delegation";

foreach ($result as $row) {
    //get processor position
    $to = array();
    $to[$row["delegate_email"]] = $row["delegate_display_name"];

    $message = "<div style='float:left;width:100%;margin-bottom: 30px;font-size:11px;background-color:#fff;'>";
    $message .= "<div style='padding:20px;color: #222222; font-family: Arial; font-size: 14px;line-height: 150%;'>";
    $message .= $row["processor_display_name"] . " delegated the following request(s) for your action.<br/><br/>Click on the Tracking Number to open the request.<br/><br/>";

    foreach ($forms as $form) {
        $request_query = "SELECT * FROM (SELECT"
                . " request.ID,"
                . " request.TrackNo, "
                . " CASE request.ProcessorType
                        WHEN 1 THEN getDepartmentUsers(request.Processor, request.ProcessorLevel, {$form["company_id"]})
                        WHEN 2 THEN getUsersByPosition(request.Processor)
                        ELSE request.Processor
                    END AS processors_id "
                . " FROM {$form["form_table_name"]} request) A WHERE FIND_IN_SET({$row["processor_id"]}, A.processors_id)";

        $requests = $db->query($request_query, "array");

        if (count($requests) > 0) {
            $message .= "<b>" . $form["form_name"] . "</b><br/>";
        }

        foreach ($requests as $request) {
            $link = "<a href='" . MAIN_PAGE . "user_view/workspace?view_type=request&formID={$form["id"]}&requestID={$request["ID"]}&trackNo={$request["TrackNo"]}'>"
                    . "{$request["TrackNo"]}</a>";
            $message.=$link . "<br/><br/>";
        }
    }

    $message .= "</div>";
    $message .= "</div>";
    $message .= "</div>";
    
//    print_r($message);
    $mailDoc = new Mail_Notification();
    $mailDoc->sendEmail_smtp($message, $to, null, null, $from, null, $from_title, $subject);
}


