<?php
    $db = new Database();
    $field_reference = $_POST['reference_field']?:"";
    $field_value = $_POST['reference_value']?:"";
    $form_id = $_POST['form_id']?:"";
    if($field_reference != "" && $form_id != "" && $field_value != ""){
        $formDoc = new Form($db, $form_id);
        $workflow_object = functions::getFormActiveWorkflow($formDoc);
        $query = "UPDATE " . $formDoc->form_table_name  . " SET ";
        $query .= "`Status` = 'DRAFT', ";
        $query .= "`LastAction` = '" . $workflow_object['buttonStatus'] . "', ";
        $query .= "`fieldEnabled` = '" . $workflow_object['fieldEnabled'] . "', ";
        $query .= "`fieldRequired` = '" . $workflow_object['fieldRequired'] . "', ";
        $query .= "`fieldHiddenValues` = '" . $workflow_object['fieldHiddenValue'] . "', ";
        $query .= "`Node_ID` = 'Draft' ";
        $query .= "WHERE `" . $field_reference . "` = '" . $field_value . "'";
        $db->query($query);
        // echo "done";
        echo $query;
    }
    else{
        echo "error";
    }
?>