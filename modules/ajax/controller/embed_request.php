<?php
    //check if session is alive if not.. retun false;
    if(!Auth::hasAuth('current_user')){ 
        return false;
    }
    $db = new Database();
    if(isset($_POST['form_id'])){
        $form_object = new Form($db, $_POST['form_id']);
        $query = "SELECT ";
        if(isset($_POST['columns_needed'])){
            foreach($_POST['columns_needed'] as $key => $value){
                $query .= $value . ", ";
            }
            $query = substr($query, 0, -2);
        }
        else{
            $query .= "*";
        }
        $query .= " FROM tbfields WHERE form_id = " . $_POST['form_id'];
        // print_r($query);
        $res = $db->query($query);
        
        if(ISSET($_POST['get_row_count'])){
            $query = "SELECT MAX(ID) as count FROM " . $form_object->form_table_name;
            $res['row_count'] = $db->query($query)[0]['count']?:0;
        }
        if(ISSET($_POST['get_request_details'])){

            $query = "SELECT 
                        `tbworkflow_objects`.`workflow_id`,
                        `tbworkflow_objects`.`buttonStatus`, 
                        `tbworkflow_objects`.`fieldEnabled`, 
                        `tbworkflow_objects`.`fieldRequired`, 
                        `tbworkflow_objects`.`fieldHiddenValue` 
                    FROM 
                        tb_workspace, 
                        tbworkflow, 
                        tbworkflow_objects 
                    WHERE 
                            `tb_workspace`.`id` = `tbworkflow`.`form_id` 
                        AND `tbworkflow_objects`.`workflow_id` = `tbworkflow`.`id` 
                        AND `tbworkflow_objects`.`type_rel` = '1' 
                        AND `tbworkflow`.`Is_Active` = '1' 
                        AND `tb_workspace`.`is_delete` = '0' 
                        AND `tbworkflow`.`is_delete` = '0' 
                        AND `tb_workspace`.`id` = " . $_POST['form_id'];
            $res['other_details'] = $db->query($query)[0];
        }
        echo json_encode($res);
    }
?>