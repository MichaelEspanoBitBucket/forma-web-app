<?php
// error_reporting(E_ALL);
$auth = Auth::getAuth('current_user');
$db = new Database;

$search = new Search();
$form_source_type = $_POST['formsourcetype']?:"";
if($form_source_type != ""){
    $source_forms = $_POST['form_id_list'];
    $reference_fields = $_POST['column_reference_fields'];
    $selected_columns = $_POST['column_settings'];
}
$form_id = $_POST['FormID'];
$reference_field = $_POST['FieldReference'];
$reference_value = $_POST['FieldValue'];
$reference_filter = $_POST['FieldFilter'];
$HLData = $_POST['HLData'];
$HLData_json = "";
$HLAllow = $_POST['HLAllow'];
$HLType = $_POST['HLType'];
$field_column = $_POST['column_data'];
$current_form_fields_data = $_POST["current_form_fields_data"];
//enable_embed_row_click
$enable_embed_row_click = isset($_POST['enable_embed_row_click'])?$_POST['enable_embed_row_click']:'';
$embed_action_click_copy = isset($_POST['embed_action_click_copy'])?$_POST['embed_action_click_copy']:'';
$embed_action_click_delete = isset($_POST['embed_action_click_delete'])?$_POST['embed_action_click_delete']:'';
$embed_action_click_edit = isset($_POST['embed_action_click_edit'])?$_POST['embed_action_click_edit']:'';
$embed_action_click_view = isset($_POST['embed_action_click_view'])?$_POST['embed_action_click_view']:'';
$embed_action_click_number = isset($_POST['embed_action_click_number'])?$_POST['embed_action_click_number']:'';
$embed_action_click_edit_popup = isset($_POST['embed_action_click_edit_popup'])?$_POST['embed_action_click_edit_popup']:'';
$field_conditional_operator = isset($_POST['field_conditional_operator'])?$_POST['field_conditional_operator']:'=';
$embed_additional_filter_formula = isset($_POST['embed_additional_filter_formula'])?$_POST['embed_additional_filter_formula']:'';
$search_field_object_name = isset($_POST['search_field_obj_name'])?$_POST['search_field_obj_name']:'';
//embed row category

$embed_row_category = isset($_POST['embed_row_category'])?$_POST['embed_row_category']:'';

$embed_print = false;

if(isset($_GET['embed_type']) and isset($_GET['print_form']) ){
    if($_GET['embed_type'] == "viewEmbedOnly" and $_GET['print_form'] == "true"){
        $embed_print = true;
        
    }
}

// print_r($current_form_fields_data);
try {
    // do something that can go wrong
    $HLData_json = json_decode($HLData);
} catch (Exception $e) {
    //throw new Exception( 'Something really gone wrong', 0, $e);
}
try {
    $field_column_json = json_decode($field_column, true);
    // print_r($field_column_json);
} catch (Exception $e) {
    
}
try {
    // print_r($current_form_fields_data);
    $current_form_fields_data = json_decode($current_form_fields_data);
    // print_r($current_form_fields_data);
} catch (Exception $e) {
    
}
function returnFieldType($fld_typ, $field_name, $field_id, $field_value, $multiple_values, $radio_name_count,$other_attributes,$obj_id){
   $other_attributes_array = array();  
   $multiple_values = json_decode($multiple_values,true);
   $other_attributes = json_decode($other_attributes,true);

   

    $ret = "";
    switch($fld_typ){
        case "dropdown":
            $ret .= '<select data-type="longtext" data-field-name="'.$field_name.'" class="form-select display tip form-text embed_getFields inlineEdit_' . $field_id . '" data-embed-name="embed_textbox_' . $field_id . '" value="' . $field_value . '">';
                foreach ($multiple_values[0] as $key => $value) {
                    $isSelected = "";
                    if($key==$field_value){
                        $isSelected = "selected";
                    }
                    $ret .= '<option value="'.$key.'"'.$isSelected.'>'.$value.'</option>';
                }
            $ret .= '</select>';
            break;
        
        case "radioButton":
            
            foreach($multiple_values[0] as $key => $value) {

                $isChecked = "";
               if($key==trim($field_value)){
                $isChecked = "checked";
               }

               $ret .= '<label class = "display"><input data-field-name="'.$field_name.'" type="radio" data-type="longtext" class="tip embed_getFields inlineEdit_' . $field_id . '" name="radio_' . $radio_name_count . '" data-embed-name="embed_textbox_' . $field_id . '" value="'.$key.'" '.$isChecked.'/><span class="radio-input-label"> '.$value.'</span></label><span class="rc-prop-alignment rc-align-vertical"></span>';     
                
            }

            break;

        case "textbox":
        case "computed":
        case "textbox_editor_support":
        case "textbox_reader_support":
            $ret .= '<input type="text" data-field-name="'.$field_name.'" class="display tip form-text embed_getFields inlineEdit_' . $field_id . '" data-embed-name="embed_textbox_' . $field_id . '"  value="' . $field_value . '">';
            break;

        case "datepicker":

            $ret .= '<input type="text" data-field-name="'.$field_name.'" class="display tip form-text embed_getFields inlineEdit_' . $field_id . '" data-embed-name="embed_textbox_' . $field_id . '"  value="' . $field_value . '">';
            break;

        case "selectMany":
          
            $ret .= '<select data-field-name="'.$field_name.'" class="form-selectMany display tip form-text embed_getFields inlineEdit_' . $field_id . '" data-embed-name="embed_textbox_' . $field_id . '" value="' . $field_value . '" multiple="multiple">';
                
                foreach ($multiple_values[0] as $key => $value) {

                    $isSelected = "";
                    $field_value_array = explode(",", $field_value);
                    
                    if(in_array($key,$field_value_array)){
                        
                        $isSelected = "selected";
                    }
                    $ret .= '<option value="'.$key.'"'.$isSelected.'>'.$value.'</option>';
                }
            $ret .= '</select>';
            break;
        case "textArea":

            $ret .= '<textarea style="resize:vertical;" data-type="longtext" data-field-name="'.$field_name.'" class="form-textarea display tip embed_getFields inlineEdit_' . $field_id . '" data-embed-name="embed_textbox_' . $field_id . '">' . $field_value . '</textarea>';
            break;

        case "dateTime":
            $ret .= '<input type="text" data-field-name="'.$field_name.'" class="display tip form-text embed_getFields inlineEdit_' . $field_id . '" data-embed-name="embed_textbox_' . $field_id . '"  value="' . $field_value . '">';
            break;

        case "time":
            $ret .= '<input type="text" data-field-name="'.$field_name.'" class="display tip form-text embed_getFields inlineEdit_' . $field_id . '" data-embed-name="embed_textbox_' . $field_id . '"  value="' . $field_value . '">';
            break;

        case "checkbox":
            foreach($multiple_values[0] as $key => $value) {

                $isChecked = "";
                $field_value_array = explode(",", $field_value);
            if(in_array($key,$field_value_array)){
                $isChecked = "checked";
            }

               $ret .= '<label class = "display"><input data-field-name="'.$field_name.'" type="checkbox" data-type="longtext" class="tip embed_getFields inlineEdit_' . $field_id .   ' "  name="checkbox_' . $radio_name_count . '"  value="'.$key.'" '.$isChecked.'/><span class="checkbox-input-label" style="display: inline;"> '.$value.'</span></label><span class="rc-prop-alignment rc-align-vertical"></span>';
                
            }
            
            break;

        case "pickList":
            $ret .='<table style="border:none;" class="display picklist-table">';
                $ret .= '<tbody>';
                $ret .= '<tr class="picklist-tr">';
                    $ret .= '<td class = "picklist-td" style="border:none;">';
                        $ret .='<input data-field-name="'.$field_name.'" style="min-width:30px;display:table-cell;font-size:14px;" data-type="longtext" type="text"  class="tip form-text getFields inlineEdit_' . $field_id . '" value="'.$field_value.'" id="'.$other_attributes['return-field'].'" data-placement="bottom"/>';
                    $ret .= '</td>';
                    $ret .= '<td class = "picklist-td" style="border:none;">';
                        $ret .='<span style="width:23px;display:table-cell;white-space:nowrap;"><a class="'.$other_attributes['class'].'" picklist-button-id="'.$other_attributes['picklist-button-id'].'" return-field = "'.$other_attributes['return-field'].'" picklist-type="'.$other_attributes['picklist-type'].'" form-id = "'.$other_attributes['form-id'].'" return-field-name="'.$other_attributes['return-field-name'].'" formname="'.$other_attributes['formname'].'" display_columns="'.htmlentities($other_attributes['display_columns']).'" display_column_sequence = "'.htmlentities($other_attributes['display_column_sequence']).'" selection-type = "'.$other_attributes['selection-type'].'" condition="'.$other_attributes['condition'].'" enable-add-entry="'.$other_attributes['enable_entry'].'" data-original-title="Pick Keyword" style="margin-left:5px;" ><i class="icon-list-alt fa fa-list-alt"></i></a></span>';
                    $ret .= '</td>';
                $ret .= '</tr>';
                $ret .= '</tbody>';
            $ret .='</table>';
            break;
        case "listNames":
            $ret .='<table style="border:none;" class="display picklist-table">';
                $ret .= '<tbody>';
                $ret .= '<tr class="picklist-tr">';
                    $ret .= '<td class = "picklist-td" style="border:none;">';
                        $ret .='<input data-field-name="'.$field_name.'" style="min-width:30px;display:table-cell;font-size:14px;" data-type="longtext" type="text"  class="tip form-text getFields inlineEdit_' . $field_id . '" value="'.$field_value.'" id="'.$other_attributes['return-field'].'" data-placement="bottom"/>';
                    $ret .= '</td>';
                    $ret .= '<td class = "picklist-td" style="border:none;">';
                        $ret .='<span style="width:23px;display:table-cell;white-space:nowrap;"><a class="'.$other_attributes['class'].'"  return-field = "'.$other_attributes['return-field'].'" list-type-selection="'.$other_attributes['list-type-selection'].'" data-original-title="Select Name" style="margin-left:5px;" ><i class="icon-list-alt fa fa-list-alt"></i></a></span>';
                    $ret .= '</td>';
                $ret .= '</tr>';
                $ret .= '</tbody>';
            $ret .='</table>';
            break;
        case "noteStyleTextarea":
            $ret .= '<textarea style="resize:vertical;" data-type="longtext" data-field-name="'.$field_name.'" class="form-textarea display tip embed_getFields inlineEdit_' . $field_id . '" data-embed-name="embed_textbox_' . $field_id . '">' . $field_value . '</textarea>';
            break;
        case "multiple_attachment_on_request"://added by japhet morada 03-30-2016
            $ret .= '<form id="embed_multiple_on_attach_' . $obj_id . '_' . $field_id . '" method="post" enctype="multipart/form-data" action="/ajax/multiple_request_on_attachment" encoding="multipart/form-data">';
                $ret .= '<input type="file" class="embed_multiple_file_attachement obj-attachment display" data-action-id="' . $obj_id . '" name="multiple_file_attachment[]" multiple id="' . $field_name . '" size="24" data-action-type="multiple_attachmentForm" field-id="' . $field_id . '">';
            $ret .= '</form>';
            $ret .= '<input type="text" data-attachment="attachment_multiple_on_attachment_' . $obj_id . '" class="display getFields inlineEdit_' . $field_id . '" data-field-name="' . $field_name . '" name="embed_' . $field_name . '_' . $field_id . '" value="' . htmlentities($field_value) . '"/>';
        break;
        case "attachment_on_request"://added by japhet morada 03-30-2016
            $ret .= '<form id="embed_on_attach_' . $obj_id . '_' . $field_id . '" method="post" enctype="multipart/form-data" action="/ajax/request_on_attachment" encoding="multipart/form-data">';
                $ret .= '<input type="file" class="embed_file_attachement obj-attachment display" data-action-id="' . $obj_id . '" name="file_attachment" id="' . $field_name . '" size="24" data-action-type="attachmentForm" field-id="' . $field_id . '">';
            $ret .= '</form>';
            $ret .= '<input type="text" data-single-attachment="true" class="display getFields inlineEdit_' . $field_id . '" data-field-name="' . $field_name . '" name="embed_' . $field_name . '_' . $field_id . '" value="' . htmlentities($field_value) . '"/>';
        break;
    }
    

    return $ret;
    
}
function allowColumn($field_column_json, $column_field_name) {
    if (isset($field_column_json) and count($field_column_json) >= 1) {
        $passed_field_name = $column_field_name;
        $bool_checker = false;
        foreach ($field_column_json as $val) {//loop tr
            if ($passed_field_name == $val) {
                $bool_checker = true;
                break;
            }
        }
        return $bool_checker;
    } else {
        return true; ///allow col field
    }
}

// Set Data field type
function data_fld_type($fld_type, $fld_input_type,$val){
    if($fld_type == "double" and  $fld_input_type == "Currency"){
        //return number_format($val, 2 , '.', ',');
        $arr = array("value"=>number_format($val, 2 , '.', ','),"align"=>"right");
    }else{
        if($val == "0000-00-00"){
            $arr = array("value"=>"","align"=>"left");
        }else{
            $arr = array("value"=>$val,"align"=>"left");
        }
    }

    return $arr;
}

if($form_source_type != ""){
    $str_query = "";
    $tb_field_str = "";
    $form_fields_origin = array();
    $selection = array();
    $reserve_keys = array("ID","TrackNo");
    $embed_sort_header_click_name = $_POST['embed_default_sorting']['column'];
    $embed_sort_type = $_POST['embed_default_sorting']['type'];
    $computed_fields = []; 
    $field_name_with_formula = array(); 
    foreach($source_forms as $form_index => $form_details){
        $formDoc = new Form($db, $form_details['source_form']);
        $form_json = json_decode($formDoc->form_json,true);
        $column_str = "";
        $form_fields = json_decode($form_json['form_fields'],true);
        $str_query .= "SELECT ";
        $forms_str = "";
        $reference_str = "";
        foreach($selected_columns as $column_index => $column_data){
            if(in_array($form_details['source_form'], explode(", ", $column_data['source_form']))){
            // if($form_details['source_form'] == ){
                $column_str .= "COALESCE(`" . $column_data['column_field_name'] . "`, '" . ($column_data['column_field_replacer']?:"---") . "') as '" . $column_data['column_field_name'] . "', "; 
            }
            else if($column_data['column_field_name'] == "system_icon"){
                $column_str .= "'system_icon' as '" . $column_data['column_field_name'] . "_"  . $column_index . "', "; 
            }
            else{
                $column_str .= "'" . ($column_data['column_field_replacer']?:"---") . "' as '" . $column_data['column_field_name'] . "', ";
            }
            //getting the field formulas
            if($column_data['field_value']['field_type'] == "computed"){
                if(!in_array($column_data['column_field_name'], $computed_fields)){
                    // $field_formula_data = array("" . $column_data['column_field_name'] => $column_data['field_value']);
                    $field_name_with_formula["" . $column_data['column_field_name']] = $column_data['field_value'];
                    array_push($computed_fields, $column_data['column_field_name']);
                }
            }
            array_push($selection, $column_data["column_field_name"]);  
        }
        for($i = 0; $i < count($reserve_keys); $i++){
            $column_str .= "`" . $formDoc->form_table_name . "`.`" . $reserve_keys[$i] ."` , ";
        }
        $column_str .= "COALESCE((SELECT IF(COUNT(`tbrequest_users`.`Action_Type`) > 0, 1, 0) FROM `tbrequest_users` WHERE RequestID = `" . $formDoc->form_table_name . "`.`ID` AND Form_ID = " . $form_details['source_form'] . " AND User = " . $auth['id'] . " AND Action_Type = '1'), 0) AS is_editable, ";
        $column_str = substr($column_str, 0, strlen($column_str ) - 2); 
        $column_str .= ", (SELECT id FROM tb_workspace WHERE form_table_name = '" . $formDoc->form_table_name . "') as form_id";
        
        $reference_str .= "`" . $form_details['lookup_field'] . "` " . $form_details['lookup_operator'] . " '" . $reference_fields[$form_details['lookup_field']] . "' AND ";
        
        $form_fields_origin = array_merge($form_fields_origin, $form_fields);
        
        $tb_field_str .= "`form_id` = " . $form_details['source_form'] . " OR ";
        
        $forms_str .= $formDoc->form_table_name . ", ";
        $forms_str = substr($forms_str, 0, strlen($forms_str) - 2);
        
        $reference_str = substr($reference_str, 0, strlen($reference_str) - 4);

        if($form_details['advance_filter_formula'] != ""){
            $adv_filter_formula = $form_details['advance_filter_formula'];
            $adv_filter_formula = str_replace("@", "", $form_details['advance_filter_formula']);
            $adv_filter_formula = str_replace("==", "=", $adv_filter_formula);
            $str_query .= $column_str . " FROM " . $forms_str . " WHERE " . $reference_str . ' AND (' . $adv_filter_formula . ')';
        }
        else{
            $str_query .= $column_str . " FROM " . $forms_str . " WHERE " . $reference_str;
        }
        $str_query .= " UNION ALL ";
    }
    $formDoc = new Form($db, $_POST['embed_default_sorting']['form_id']);
    $sorting = " ORDER BY " .  $_POST['embed_default_sorting']['column'] . " " . $_POST['embed_default_sorting']['type'] . " "; // LIMIT 0, 10
    $str_query = substr($str_query, 0, strlen($str_query) - 10) . $sorting;
    $tb_field_str = substr($tb_field_str, 0, strlen($tb_field_str) - 4);
    $get_form_fields = functions::getFields("WHERE ".$tb_field_str);
    $form_fields = $get_form_fields;
    $get_grouped_field = "";
    if($embed_row_category != ""){
        $get_grouped_field .= "SELECT " . $embed_row_category . " FROM (" . $str_query . ") tb GROUP BY " . $embed_row_category; 
    }
    $tb_field_query .= "SELECT * FROM tbfields WHERE " . $tb_field_str;
    // echo $str_query;
    $result = $db->query($str_query, "array");
    $tbfields_data = $db->query($tb_field_query, "array");
    $embed_row_category_list = $db->query($get_grouped_field,"array");
    $embed_row_category_data = array();
    foreach($embed_row_category_list as $key => $value){
        array_push($embed_row_category_data, $value[$embed_row_category]);
    }

}
else{
    $formDoc = new Form($db, $form_id);
    $headers = explode(",", $formDoc->active_fields);

    // Get form JSON
    $form_json = json_decode($formDoc->form_json,true);
    //$form_json = $formDoc->form_json;
    // Encode Form Fields'

    $selection = array();
    $reserve_keys = array("ID","TrackNo");
    foreach($field_column_json as $column){
          array_push($selection, $column["FieldName"]);  
    }

    //var_dump(join($selection,","));
    $get_form_fields = functions::getFields("WHERE form_id = ".$form_id);
    //var_dump($get_form_fields);


    //error_reporting(E_ALL);
    $form_fields_origin = json_decode($form_json['form_fields'],true);
    $form_fields = $get_form_fields;


    $sort_query  = " ORDER BY ID ASC ";

    if(isset($_POST['embed_default_sorting'])){
        $embed_sort_header_click_name = $_POST['embed_default_sorting']['column'];
        $embed_sort_type = $_POST['embed_default_sorting']['type'];
        $sort_query = " ORDER BY `".$embed_sort_header_click_name."` ".$embed_sort_type." ";
        //var_dump("TAAS".$embed_sort_header_click_name);
    }

    if( isset($_POST['embed_sort_type']) and isset($_POST['embed_sort_header_click_name']) and !empty($_POST['embed_sort_type']) and !empty($_POST['embed_sort_header_click_name']) ){
    	$embed_sort_header_click_name = $_POST['embed_sort_header_click_name'];
    	$embed_sort_type = $_POST['embed_sort_type'];
    	$sort_query = " ORDER BY `".$embed_sort_header_click_name."` ".$embed_sort_type." ";
    }

    $merged_sql_select = array_merge($selection,$reserve_keys);

    if($reference_filter == "--Select--" || empty($reference_filter) ){
        // var_dump("taas");
        $q_str = "SELECT `" . join($merged_sql_select,"`,`") . "` FROM " . $formDoc->form_table_name  . $sort_query;
        $result = $db->query($q_str, "array");
        $record_env_data = "SELECT `fieldRequired`, `fieldEnabled` FROM " . $formDoc->form_table_name  . $sort_query; //mandatory fields
        $result_node_data = $db->query($record_env_data, "array");
    }else{
        // var_dump("baba".$field_conditional_operator);
        $where_filter_first_conditional_operator = "";
        if($field_conditional_operator == "contain"){
            $where_filter_first_conditional_operator = " `" . $reference_filter . "` LIKE '%" .$db->escape($reference_value)."%' ";
        }else if($field_conditional_operator == "not_contain"){
            $where_filter_first_conditional_operator = " `" . $reference_filter . "` NOT LIKE '%" .$db->escape($reference_value)."%' ";
        }else{
            if(hasMultipleValue($search_field_object_name) == true){
                $ref_value = "";
                foreach($reference_value as $refval){
                    $ref_value .= "'" . $refval . "', ";
                }
                $ref_value = substr($ref_value, 0, (strlen($ref_value) - 2));
                if($field_conditional_operator == '='){
                    $where_filter_first_conditional_operator .= " `".$reference_filter . "` IN (" . $ref_value . ") ";
                }
                else{
                    $where_filter_first_conditional_operator .= " `".$reference_filter . "` NOT IN (" . $ref_value . ") ";
                }
                
            }
            else{
                $where_filter_first_conditional_operator = " `".$reference_filter . "` ".$field_conditional_operator." " . $db->escape($reference_value)." ";
            }
        }

        if ($embed_additional_filter_formula != "") {
            $embed_additional_filter_formula = str_replace("@", "", $embed_additional_filter_formula);
            // $embed_additional_filter_formula = str_replace("'", "''", $embed_additional_filter_formula);
            $embed_additional_filter_formula = " AND (" . str_replace("==", "=", $embed_additional_filter_formula) . ")";
        //    $formulaObject = new Formula($condition);
        };

        $where_filter_first_conditional_operator .= $embed_additional_filter_formula." ";

        $q_str = "SELECT `" . join($merged_sql_select,"`,`") . "` FROM " . $formDoc->form_table_name . " WHERE " . $where_filter_first_conditional_operator ." AND TrackNo is not null ". $sort_query;
        $result = $db->query($q_str, "array");
        $record_env_data = "SELECT `fieldRequired`, `fieldEnabled` FROM " . $formDoc->form_table_name . " WHERE " . $where_filter_first_conditional_operator ." ". $sort_query; //mandatory fields
        $result_node_data = $db->query($record_env_data, "array");
    }
    // var_dump($_POST);
    //tbfields
    $tbfields_qstr = "SELECT * FROM `tbfields` WHERE `form_id` = ".$form_id." "; //AND `formula_type` = 'computed' AND ( `formula` IS NOT NULL OR `formula` <> '') 
    $tbfields_data = $db->query($tbfields_qstr, "array");
    // var_dump($tbfields_qstr);
    // $formPrivacy_authors = $search->getFormPrivacyUsers($formDoc->form_authors);
    $formPrivacy_authors = $search->getModulesByAuthor(" AND tbw.id = $form_id");
    if($auth['user_level_id'] != "4"){
        if ($formPrivacy_authors) {
            if (count($formPrivacy_authors)==1) {
                $formPrivacy_authors = "true";
            } else {
                $formPrivacy_authors = "false";
            }
        }else{
            $formPrivacy_authors = "false";
        }
    }else{
        $formPrivacy_authors = "true";
    }
    $get_grouped_field = "";
    if($embed_row_category != ""){
        $get_grouped_field .= "SELECT " . $embed_row_category . " FROM (" . $q_str . ") tb GROUP BY " . $embed_row_category; 
    }
    $embed_row_category_list = $db->query($get_grouped_field,"array");
    $embed_row_category_data = array();
    foreach($embed_row_category_list as $key => $value){
        array_push($embed_row_category_data, $value[$embed_row_category]);
    }

    // var_dump($q_str);
}




function evaluateHighlightRule($key2_inner = null, $val_inner = null, $result = null, $current_form_fields_data) {
    $condition_stat = false;
    $value2 = "";
    $value1 = "";
    if ($key2_inner->{"value_type"} == "computed") {
        if (!empty($key2_inner->{"hl_value2"})) {
            $formula_value_v2 = new Formula($key2_inner->{"hl_value2"});
            $formula_value_v2->DataFormSource[0] = $result;
            foreach ($current_form_fields_data as $value) {
                $formula_value_v2->updateDataFormSource($value->{"f_name"}, $value->{"values"});
            }
            $value2 = $formula_value_v2->evaluate();
        }

        if (!empty($key2_inner->{"hl_value"})) {
            $formula_value_v1 = new Formula($key2_inner->{"hl_value"});
            $formula_value_v1->DataFormSource[0] = $result;
            foreach ($current_form_fields_data as $value) {
                $formula_value_v1->updateDataFormSource($value->{"f_name"}, $value->{"values"});
            }
            $value1 = $formula_value_v1->evaluate();
        }
    }

    if ($key2_inner->{"condition_type"} == "range") {
        if ($key2_inner->{"value_type"} == "static") {
            if ($key2_inner->{"hl_value2"} >= $val_inner and $key2_inner->{"hl_value"} <= $val_inner) {// value pairing
                $condition_stat = true;
            }
        } else if ($key2_inner->{"value_type"} == "computed") {
            // if (floatval($value2) >= $val_inner and floatval($value1) <= $val_inner) {// value pairing
                // $condition_stat = true;
            // }
            if(is_date_str($val_inner) && is_date_str($value1) && is_date_str($value2)){
                // $_POST["1234567890"] = ThisDate($value2)." >= ".ThisDate($val_inner)." and ".ThisDate($value1)." <= ".ThisDate($val_inner);
                if (ThisDate($value2) >= ThisDate($val_inner) and ThisDate($value1) <= ThisDate($val_inner)) {// value pairing
                    $condition_stat = true;
                }
            }else{
                if ($val_inner <= floatval($value1)) {// value pairing
                    $condition_stat = true;
                }
            }
        }
    } elseif ($key2_inner->{"condition_type"} == "in_between") {
        if ($key2_inner->{"value_type"} == "static") {
            if ($key2_inner->{"hl_value2"} > $val_inner and $key2_inner->{"hl_value"} < $val_inner) {// value pairing
                $condition_stat = true;
            }
        } else if ($key2_inner->{"value_type"} == "computed") {
            // if (floatval($value2) > $val_inner and floatval($value1) < $val_inner) {// value pairing
                // $condition_stat = true;
            // }
            if(is_date_str($val_inner) && is_date_str($value1) && is_date_str($value2)){
                // $_POST["1234567890"] = ThisDate($value2)." > ".ThisDate($val_inner)." and ".ThisDate($value1)." < ".ThisDate($val_inner);
                if (ThisDate($value2) > ThisDate($val_inner) and ThisDate($value1) < ThisDate($val_inner)) {// value pairing
                    $condition_stat = true;
                }
            }else{
                if ($val_inner <= floatval($value1)) {// value pairing
                    $condition_stat = true;
                }
            }
        }
    } elseif ($key2_inner->{"condition_type"} == "greater_than_equals") {
        if ($key2_inner->{"value_type"} == "static") {
            if ($val_inner >= $key2_inner->{"hl_value"}) {
                $condition_stat = true;
            }
        } else if ($key2_inner->{"value_type"} == "computed") {
            // if ($val_inner >= floatval($value1)) {
                // $condition_stat = true;
            // }
            if(is_date_str($val_inner) && is_date_str($value1)){
                // $_POST["1234567890"] = ThisDate($val_inner)." >= ".ThisDate($value1)."    ".$val_inner." >= ".$value1;
                if (ThisDate($val_inner) >= ThisDate($value1)) {// value pairing
                    $condition_stat = true;
                }
            }else{
                if ($val_inner >= floatval($value1)) {// value pairing
                    $condition_stat = true;
                }
            }
        }
    } elseif ($key2_inner->{"condition_type"} == "greater_than") {
        if ($key2_inner->{"value_type"} == "static") {
            if ($val_inner > $key2_inner->{"hl_value"}) {// value pairing
                $condition_stat = true;
            }
        } else if ($key2_inner->{"value_type"} == "computed") {
            // if ($val_inner > floatval($value1)) {// value pairing
                // $condition_stat = true;
            // }
            if(is_date_str($val_inner) && is_date_str($value1)){
                // $_POST["1234567890"] = ThisDate($val_inner)." > ".ThisDate($value1)."    ".$val_inner." > ".$value1;
                if (ThisDate($val_inner) > ThisDate($value1)) {// value pairing
                    $condition_stat = true;
                }
            }else{
                if ($val_inner > floatval($value1)) {// value pairing
                    $condition_stat = true;
                }
            }
        }
    } elseif ($key2_inner->{"condition_type"} == "less_than_equals") {
        if ($key2_inner->{"value_type"} == "static") {
            if ($val_inner <= $key2_inner->{"hl_value"}) {// value pairing
                $condition_stat = true;
            }
        } else if ($key2_inner->{"value_type"} == "computed") {
            if(is_date_str($val_inner) && is_date_str($value1)){
                // $_POST["1234567890"] = ThisDate($val_inner)." <= ".ThisDate($value1)."    ".$val_inner." <= ".$value1;
                if (ThisDate($val_inner) <= ThisDate($value1)) {// value pairing
                    $condition_stat = true;
                }
            }else{
                if ($val_inner <= floatval($value1)) {// value pairing
                    $condition_stat = true;
                }
            }
        }
    } elseif ($key2_inner->{"condition_type"} == "less_than") {
        if ($key2_inner->{"value_type"} == "static") {
            if ($val_inner < $key2_inner->{"hl_value"}) {// value pairing
                $condition_stat = true;
            }
        } else if ($key2_inner->{"value_type"} == "computed") {
            // if ($val_inner < floatval($value1)) {// value pairing
                // $condition_stat = true;
            // }
            if(is_date_str($val_inner) && is_date_str($value1)){
                // $_POST["1234567890"] = ThisDate($val_inner)." < ".ThisDate($value1)."    ".$val_inner." < ".$value1;
                if (ThisDate($val_inner) < ThisDate($value1)) {// value pairing
                    $condition_stat = true;
                }
            }else{
                if ($val_inner < floatval($value1)) {// value pairing
                    $condition_stat = true;
                }
            }
        }
    } else {
        if ($key2_inner->{"value_type"} == "static") {
            if ($key2_inner->{"hl_value"} == $val_inner) {// value pairing
                $condition_stat = true;
            }
        } else if ($key2_inner->{"value_type"} == "computed") {
            if ($value1 == $val_inner) {// value pairing
                $condition_stat = true;
            }
        }
    }
    return $condition_stat;
}

//added by japhet morada
function getFormulaEvent($formId){
    $db2 = new Database();
    $query = "SELECT form_json FROM tb_workspace WHERE id = $formId";
    $result = json_decode($db2->query($query, "row")['form_json'], true);
    $return_value = array();
    if($result['form_json']['form_events']){
        if($result['form_json']['ComputedFormulaEventList'] != null){
            $return_value['onload'] = $result['form_json']['form_events']['compute_onload']?: "";
            $return_value['presave'] = $result['form_json']['form_events']['compute_presave']?: "";
            $return_value['postsave'] = $result['form_json']['form_events']['compute_postsave']?: "";
            $return_value['ComputedFormulaEventList'] = json_encode($result['form_json']['ComputedFormulaEventList']?:"[]");
            $return_value['form_variables'] = json_encode($result['form_json']['form_variables']?:"[]");
        }
    }
    return json_encode($return_value);
}

function hasMultipleValue($data_type){
    if($data_type == 'pickList' || $data_type == 'checkbox' || $data_type == 'selectMany'){
        return true;
    }
    else{
        return false;
    }
}
// $rekta = json_encode($form_fields_origin,JSON_PETTY_PRINT);
// var_dump($form_fields_origin);
if($embed_action_click_number == "true"){
    /*removed class 03-30-2015*/
    /*dataTable*/
    $enabled_embed_action_click_number_class = "enabled_embed_action_click_number_class table_data display_data";
}
else {
    $enabled_embed_action_click_number_class = "table_data display_data";
}
?>

<table <?php //echo 'data-tracking="';print_r($form_json);echo '"'; ?> author-valid="<?php echo $formPrivacy_authors; ?>" class="<?php echo $enabled_embed_action_click_number_class; ?>" form-events="<?php /**added by japhet morada**/ echo htmlentities(getFormulaEvent($form_id)); ?>">
    <thead>
        <tr>
            <?php
            //if(in_array("",$form_fields)){
                //var_dump($form_fields);
            //}
            $column_index = 0;
            $collect_allowed_columns = array();
            $column_type = array();
            // Get Field Type
            $arr = array();
            //foreach($form_fields as $flds){
            //    $arr[str_replace("[]","",$flds['fieldName'])] = array(
            //        "fieldName"=>$flds['name'],
            //        "fieldInputType"=>$flds['input_type'],
            //        "fieldObjectType"=>$flds['type']
            //    );
            //}
            
            $val_temp_type = "";
            $collect_at_fn = [];//ADDED FOR FS#4094:Embedded View (in-line edit) : All fields with computed value or being used in computation should not be editable.
            foreach($form_fields as $data_index => $data_val){

                $arr[str_replace("[]","",$data_val->{"name"})] = array(
                    "fieldName"=>$data_val->{"name"},
                    "fieldInputType"=>$data_val->{"input_type"},
                    "fieldObjectType"=>$data_val->{"type"},
                    "fieldType"=>array_values(array_filter($form_fields_origin, function($v) use ($data_val) {
                        return $v['fieldName'] == $data_val->{"name"} || $v['fieldName'] == $data_val->{"name"}."[]";
                    }))[0]["fieldType"],
                    "formula"=>array_values(array_filter($tbfields_data, function($v) use ($data_val) {
                        return $v['field_name'] == $data_val->{"name"};
                    }))[0]["formula"],
                    "formula_type"=>array_values(array_filter($tbfields_data, function($v) use ($data_val) {
                        return $v['field_name'] == $data_val->{"name"};
                    }))[0]["formula_type"],
                    "data_readonly_state"=>array_values(array_filter($tbfields_data, function($v) use ($data_val) {
                        return $v['field_name'] == $data_val->{"name"};
                    }))[0]["readonly"],
                    "multiple_radio_values" => array_values(array_filter($tbfields_data, function($v) use ($data_val) {
                        return $v['field_name'] == $data_val->{"name"};
                    }))[0]["multiple_values"],
                    "other_attributes_value" => array_values(array_filter($tbfields_data, function($v) use ($data_val) {
                        return $v['field_name'] == $data_val->{"name"};
                    }))[0]["other_attributes"],
                    "obj_id" => array_values(array_filter($tbfields_data, function($v) use ($data_val) {
                        return $v['field_name'] == $data_val->{"name"};
                    }))[0]["object_id"]
                    /*"radio_values"=>$form_json['']*/
                );
                
                $commentedFormula = $arr[str_replace("[]","",$data_val->{"name"})]['formula'];
                $commentedFormulaPos = stripos($commentedFormula,"//");
                
                if($commentedFormulaPos === 0){
                        $commentedFormula = "";
                }
                

                $testing = new Formula($commentedFormula);//FS#4094
                $collect_at_fn = array_merge ( $collect_at_fn , $testing->getAllKeyNames()[0] );//FS#4094
                // var_dump($arr[str_replace("[]","",$data_val->{"name"})]['formula']);
                // array_push($collect_at_fn, $arr[str_replace("[]","",$data_val->{"name"})]['formula']);
            }

            $used_in_computation = array_values(array_unique($collect_at_fn));//FS#4094

            $str_used_in_computation = json_encode($used_in_computation);//FS#4094
            // var_dump($str_used_in_computation);
            // var_dump(array_values(array_unique($collect_at_fn)));
            //var_dump($arr);
            if(!$embed_print && ( $enable_embed_row_click == "true") ){
                echo '<th class="embed-table-actions" style="width:15%;"><div class="fl-table-ellip" style="min-width:75px;"></div></th>';
            }
            if($embed_action_click_number == "true"){
                echo '<th class="embed-table-numbering" style="width:5%;text-align:center;"><div class="fl-table-ellip">#</div></th>';
            }
            if($form_source_type != ""){
                $field_replacer_list = array();
                foreach($selected_columns as $column_index => $column){
                    array_push($collect_allowed_columns, $column_index);
                    array_push($column_type,array(
                        'fldType' => $arr[$column['column_field_name']]['fieldType'],
                        'fldInputType' => $arr[$column['column_field_name']]['fieldInputType']
                    ));
                    if(gettype($column['column_field_replacer']) != "NULL"){
                        $field_replacer_list["" . $column['column_field_name']] = $column['column_field_replacer'];
                    }
                    else{
                        $field_replacer_list["" . $column['column_field_name']] =  "---";
                    }

                    if( isset($embed_sort_type) and isset($embed_sort_header_click_name) ){
                            // echo '<th><div class="fl-table-ellip columnHeader" data-form-fld-name="'.$arr[$column['FieldName']]['fieldName'].'" data-input-fld-type="'.$arr[$column['FieldName']]['fieldInputType'].'" data-fld-type="'.$arr[$column['FieldName']]['fieldType'].'" data-fld-name="'.$column['FieldName'].'">' . $column["FieldLabel"] . '</div></th>';
                            if($arr[$column['column_field_name']]['fieldName'] == $embed_sort_header_click_name ){
                                    $sorting_html = '';
                                    // echo $arr[$column['column_field_name']]['fieldName'] . " == " . $embed_sort_header_click_name . ", " . $column['column_field_width'] . " \n";
                                    // if($_POST['embed_default_sorting']['column'] == $arr[$column['column_field_name']]['fieldName']){
                                        if($embed_sort_type == "ASC"){
                                            $sorting_html = '<i class="fa fa-sort-asc" style="padding-right:5px;"></i>';
                                        }
                                        else{
                                            $sorting_html = '<i class="fa fa-sort-desc" style="padding-right:5px;"></i>';
                                        }
                                    // }
                                    echo '<th><div class="fl-table-ellip columnHeader" data-all-used-atkey-formula="'.htmlentities($str_used_in_computation).'" sorted="'.$embed_sort_type.'" data-form-fld-name="'.$arr[$column['column_field_name']]['fieldName'].'" data-field-formula="'.htmlentities($arr[$column['column_field_name']]['formula'],ENT_QUOTES).'" data-readonly-state="'.$arr[$column['column_field_name']]['data_readonly_state'].'" data-field-formula-type="'.$arr[$column['column_field_name']]['formula_type'].'" data-field-object-type="'.$arr[$column['column_field_name']]['fieldObjectType'].'" data-input-fld-type="'.$arr[$column['column_field_name']]['fieldInputType'].'" data-fld-type="'.$arr[$column['column_field_name']]['fieldType'].'" data-fld-name="'.$column['column_field_name'].'" style="min-width:' . $column['column_field_width'] . '" form-id-origin="' . $column['source_form'] . '">' . $sorting_html . $column["column_field_caption"] . '</div></th>';
                            }else{
                                    // if($_POST['embed_default_sorting']['column'] == $arr[$column['column_field_name']]['fieldName']){
                                    //     if($_POST['embed_default_sorting']['type'] == "ASC"){
                                    //         $sorting_html = '<div class="sortable-image fl-module-tbl-arrow"><i class="fa fa-sort-asc"></i></div>';
                                    //     }
                                    //     else{
                                    //         $sorting_html = '<div class="sortable-image fl-module-tbl-arrow"><i class="fa fa-sort-desc"></i></div>';
                                    //     }
                                    // }
                                    echo '<th><div class="fl-table-ellip columnHeader" data-all-used-atkey-formula="'.htmlentities($str_used_in_computation).'" data-form-fld-name="'.$arr[$column['column_field_name']]['fieldName'].'" data-field-formula="'.htmlentities($arr[$column['column_field_name']]['formula'],ENT_QUOTES).'" data-readonly-state="'.$arr[$column['column_field_name']]['data_readonly_state'].'" data-field-formula-type="'.$arr[$column['column_field_name']]['formula_type'].'" data-field-object-type="'.$arr[$column['column_field_name']]['fieldObjectType'].'" data-input-fld-type="'.$arr[$column['column_field_name']]['fieldInputType'].'" data-fld-type="'.$arr[$column['column_field_name']]['fieldType'].'" data-fld-name="'.$column['column_field_name'].'" style="min-width:' . $column['column_field_width'] . '" form-id-origin="' . $column['source_form'] . '">' . $column["column_field_caption"] . '</div></th>';
                            }
                    }else{
                            // if($_POST['embed_default_sorting']['column'] == $arr[$column['column_field_name']]['fieldName']){
                            //     if($_POST['embed_default_sorting']['type'] == "ASC"){
                            //         $sorting_html = '<div class="sortable-image fl-module-tbl-arrow"><i class="fa fa-sort-asc"></i>';
                            //     }
                            //     else{
                            //         $sorting_html = '<div class="sortable-image fl-module-tbl-arrow"><i class="fa fa-sort-desc"></i>';
                            //     }
                            // }
                            echo '<th><div class="fl-table-ellip columnHeader" data-all-used-atkey-formula="'.htmlentities($str_used_in_computation).'" data-form-fld-name="'.$arr[$column['column_field_name']]['fieldName'].'" data-field-formula="'.htmlentities($arr[$column['column_field_name']]['formula'],ENT_QUOTES).'" data-readonly-state="'.$arr[$column['column_field_name']]['data_readonly_state'].'" data-field-formula-type="'.$arr[$column['column_field_name']]['formula_type'].'" data-field-object-type="'.$arr[$column['column_field_name']]['fieldObjectType'].'" data-input-fld-type="'.$arr[$column['column_field_name']]['fieldInputType'].'" data-fld-type="'.$arr[$column['column_field_name']]['fieldType'].'" data-fld-name="'.$column['column_field_name'].'" style="min-width:' . $column['column_field_width'] . '" form-id-origin="' . $column['source_form'] . '">' . $column["column_field_caption"] . '</div></th>';
                    }
                }
            }
            else{
                foreach ($field_column_json as $column_index =>$column) {
                    
                    array_push($collect_allowed_columns, $column_index);
                    array_push($column_type,array(
                        'fldType' => $arr[$column['FieldName']]['fieldType'],
                        'fldInputType' => $arr[$column['FieldName']]['fieldInputType']
                    ));

                    if( isset($embed_sort_type) and isset($embed_sort_header_click_name) ){
                            // echo '<th><div class="fl-table-ellip columnHeader" data-form-fld-name="'.$arr[$column['FieldName']]['fieldName'].'" data-input-fld-type="'.$arr[$column['FieldName']]['fieldInputType'].'" data-fld-type="'.$arr[$column['FieldName']]['fieldType'].'" data-fld-name="'.$column['FieldName'].'">' . $column["FieldLabel"] . '</div></th>';
                            if( $arr[$column['FieldName']]['fieldName'] == $embed_sort_header_click_name ){
                                    
                                    echo '<th><div class="fl-table-ellip columnHeader" data-all-used-atkey-formula="'.htmlentities($str_used_in_computation).'" sorted="'.$embed_sort_type.'" data-form-fld-name="'.$arr[$column['FieldName']]['fieldName'].'" data-field-formula="'.htmlentities($arr[$column['FieldName']]['formula'],ENT_QUOTES).'" data-readonly-state="'.$arr[$column['FieldName']]['data_readonly_state'].'" data-field-formula-type="'.$arr[$column['FieldName']]['formula_type'].'" data-field-object-type="'.$arr[$column['FieldName']]['fieldObjectType'].'" data-input-fld-type="'.$arr[$column['FieldName']]['fieldInputType'].'" data-fld-type="'.$arr[$column['FieldName']]['fieldType'].'" data-fld-name="'.$column['FieldName'].'">' . $column["FieldLabel"] . '</div></th>';
                            }else{
                                   
                                    echo '<th><div class="fl-table-ellip columnHeader" data-all-used-atkey-formula="'.htmlentities($str_used_in_computation).'" data-form-fld-name="'.$arr[$column['FieldName']]['fieldName'].'" data-field-formula="'.htmlentities($arr[$column['FieldName']]['formula'],ENT_QUOTES).'" data-readonly-state="'.$arr[$column['FieldName']]['data_readonly_state'].'" data-field-formula-type="'.$arr[$column['FieldName']]['formula_type'].'" data-field-object-type="'.$arr[$column['FieldName']]['fieldObjectType'].'" data-input-fld-type="'.$arr[$column['FieldName']]['fieldInputType'].'" data-fld-type="'.$arr[$column['FieldName']]['fieldType'].'" data-fld-name="'.$column['FieldName'].'">' . $column["FieldLabel"] . '</div></th>';
                            }
                    }else{
                            
                            echo '<th><div class="fl-table-ellip columnHeader" data-all-used-atkey-formula="'.htmlentities($str_used_in_computation).'" data-form-fld-name="'.$arr[$column['FieldName']]['fieldName'].'" data-field-formula="'.htmlentities($arr[$column['FieldName']]['formula'], ENT_QUOTES).'" data-readonly-state="'.$arr[$column['FieldName']]['data_readonly_state'].'" data-field-formula-type="'.$arr[$column['FieldName']]['formula_type'].'" data-field-object-type="'.$arr[$column['FieldName']]['fieldObjectType'].'" data-input-fld-type="'.$arr[$column['FieldName']]['fieldInputType'].'" data-fld-type="'.$arr[$column['FieldName']]['fieldType'].'" data-fld-name="'.$column['FieldName'].'">' . $column["FieldLabel"] . '</div></th>';
                    }
                    //echo '<th><div class="fl-table-ellip columnHeader" data-form-fld-name="'.$arr[$column['FieldName']]['fieldName'].'" data-input-fld-type="'.$arr[$column['FieldName']]['fieldInputType'].'" data-fld-type="'.$arr[$column['FieldName']]['fieldType'].'" data-fld-name="'.$column['FieldName'].'">' . $column["FieldLabel"] . '</div></th>';
                }
            }
            
            foreach ($headers as $value) {
                $column_index = $column_index + 1;
                if(allowColumn($field_column_json,$value) == true){
                   array_push($collect_allowed_columns, $column_index);
                   echo '<th><div class="fl-table-ellip">' . $value . '</div></th>';
                }
                // echo '<th>' . $value . '</th>';
            }
            ?>
        </tr>
    </thead>
    <tbody>
    <?php
    //print_r($column_type);
    //echo data_fld_type($column_type[$column_index],"11111.23232");
    $temporary_store = "";
    $embed_count = 1;
    $embed_tr_ctr = 0;
    $formula = new Formula();
    if($embed_row_category != ""){
        $category_array = array();
        foreach($embed_row_category_list as $key => $value){
            if(is_null($selected_columns)){
                $column_count = count(json_decode($field_column, true));
            }
            else{
                $column_count = count($selected_columns);
            }
            if($enable_embed_row_click == "true"){
                $column_count += 1;
            }
            if($embed_action_click_number == "true"){
                $column_count += 1;
            }
            ?>
            <tr class="embed_row_category" is-display="false" category-field="<?php echo $embed_row_category; ?>" category-value="<?php echo $value[$embed_row_category]; ?>" id="embed_row_category_<?php  echo $key ?>">
                <td colspan="<?php echo $column_count;  ?>"><?php echo $value[$embed_row_category] ?> <div class="fl-floatRight"><span>Total: </span><span class="embed_row_category_counter"></span></div></td>
            </tr>
            <?php
            array_push($category_array, $value[$embed_row_category]);
        }

    }
    // if($embed_row_category != ""){
    //     if(in_array($category_array, $val[$embed_row_category])){
    //         echo "category-field='" . $embed_row_category . "' category-value='" . $val[$embed_row_category] . "'"; 
    //     }
    // }
    foreach ($result as $key => $val) {//loop tr
        $formula->DataFormSource[0] = $val;
        $td_hl_index = 0;
        $td_hl_index_val = "";
        ?>
        <?php 
            if($form_source_type != ""){
        ?>
        <tr class="tr_<?php echo  $val['form_id'] . "_" . $val["ID"]; ?> embed-context-menu" <?php if($embed_row_category != ""){if(in_array( $val[$embed_row_category],$category_array)){ echo "category-field='" . $embed_row_category . "' category-value='" . $val[$embed_row_category] . "'"; }}?> is-editable="<?php echo "1";//$val['is_editable']; ?>" form-id="<?php echo $val['form_id'] ?>" record-trackno="<?php echo $val["TrackNo"]; ?>" record-id="<?php echo $val["ID"]; ?>" <?php echo 'data-required-field="'.htmlentities($result_node_data[$key]['fieldRequired']).'"' ;?> >
            <?php if(!$embed_print && ( $enable_embed_row_click == "true") ){ ?>

            <td <?php echo ( ( !empty($HLData_json) and $HLAllow == "true" )?$temporary_store:"" ); ?> >
                <div class="fl-table-ellip" style="text-align: center;">
                    <i id="add_data" class="display inline_add_request"><i class="fa fa-floppy-o dataTip cursor" id="addEmbedded" data-original-title="Add Row"></i></i>
                    <i id="cancel_add_data" class="display cancel_inline_add_request"><i class="fa fa-times dataTip cursor" id="canceladdEmbedded" data-original-title="Cancel Add Row"></i></i>
                    <?php if($embed_action_click_view == "true"){?><i class="fa fa-search dataTip cursor viewTDEmbedded" data-action-attr="view" id="" data-original-title="View Record"></i>&nbsp;&nbsp;<?php }?>
                    <?php if($embed_action_click_edit_popup == "true"){?><i class="fa fa-edit dataTip cursor viewTDEmbedded" data-action-attr="edit" id="" data-original-title="Popup Edit"></i>&nbsp;&nbsp;<?php }?>
                    
                    
                    <?php if($embed_action_click_edit == "true"){?>
                        <i id="showEditActions_<?php echo $val["ID"]; ?>" class="">
                            <i class="fa fa-pencil dataTip cursor editEmbedClass" id="editDEmbedded" data-original-title="Inline Edit"></i>&nbsp;&nbsp;
                        </i>
                        <i id="hideEditActions_<?php echo $val["ID"]; ?>" class="display">
                            <i class="fa fa-check dataTip cursor " id="okeditDEmbedded" data-original-title="Save Edit"></i>&nbsp;&nbsp;
                            <i class="fa fa-times dataTip cursor " id="canceleditDEmbedded" data-original-title="Cancel Edit"></i>&nbsp;&nbsp;
                        </i>
                    <?php }?>
                    <?php if($embed_action_click_copy == "true"){?>
                    <i class="showpasteActions_<?php echo $val["ID"]; ?> ">
                        <i class="fa fa-copy dataTip cursor" id="copyTDEmbedded" data-original-title="Copy Record"></i>&nbsp;&nbsp;
                    </i>
                    <i class="hidepasteActions_<?php echo $val["ID"]; ?> display">
                        <i class="fa fa-paste dataTip cursor " id="pasteTDEmbedded" data-original-title="Paste Record"></i>&nbsp;&nbsp;
                        <i class="fa fa-times dataTip cursor cancelpasteDEmbedded" data-original-title="Cancel Copy Record"></i>&nbsp;&nbsp;
                    </i>
                    <?php }?>
                    <?php if($embed_action_click_delete == "true"){?><i class="fa fa-trash-o dataTip cursor" id="deleteTDEmbedded" data-original-title="Delete Record"></i>&nbsp;&nbsp;<?php }?>
                </div>
            </td>
            <?php } ?>

            <?php if($embed_action_click_number == "true"){ ?>
            <td style="text-align:center;"> 
                <?php echo $embed_count++; ?>
            </td>
            <?php } ?>

            <?php
            $column_index = -1;
            $embed_td_ctr = 0;
            if ($HLType == "cell") {
                
                foreach ($val as $key_inner => $val_inner) { //loop td fn col
                    // echo "val_inner: " . $val_inner;
                    $column_index = $column_index + 1;
                    $new_val = str_replace("|^|",",",$val_inner);
                    if (!empty($HLData_json) and $HLAllow == "true") { //kapag naka enable ung checkboxs
                        foreach ($HLData_json as $ik => $key2_inner) { // HL data loop
                            if ($key2_inner->{"hl_fn"} == $key_inner) { // column pairing
                                if (evaluateHighlightRule($key2_inner, $val_inner, $val, $current_form_fields_data) == true) {
                                    $temporary_store = 'data-embed-highlight'.$ik.' style="background-color:' . $key2_inner->{"hl_color"} . ';"';
                                    break;
                                }
                            }
                        }
                        if(in_array($key_inner, $computed_fields)){
                            $formula->MyFormula = $field_name_with_formula[$key_inner]['field_value'];
                            echo '<td ' . $temporary_store . '   style="text-align:' .  data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['align'] .'">';
                            echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . $formula->evaluate() . '</label>';
                            echo '</td>';
                        }
                        else if($val_inner == 'system_icon'){
                            var_dump($val_inner);
                            foreach($selected_columns[$column_index]['column_field_icon_formulas'] as $key => $icon_val){
                                $formula->MyFormula =  $icon_val['icon_selected_formula'];
                                $result = $formula->evaluate();
                                if($result == true){
                                    $icon_to_be_displayed = $icon_val['icon_selected_id'];
                                    echo '<td ' . $temporary_store . '   style="text-align:' .  data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['align'] .'">';
                                    echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '"><svg class="icon-svg icon-svg-workspace" viewBox="0 0 50 50"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' + $icon_to_be_displayed + '"></use></svg></label>';
                                    echo '</td>';
                                    break;
                                }
                            }
                        }
                        else{
                            if (in_array($column_index, $collect_allowed_columns)) {
                                echo '<td ' . $temporary_store . '   style="text-align:' .  data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['align'] .'">';
                                //var_dump($arr[$key_inner]['other_attributes_value']);
                                if($field_replacer_list[$key_inner] != $new_val){
                                    echo returnFieldType($arr[$key_inner]['fieldObjectType'],$arr[$key_inner]['fieldName'],$val["ID"],data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'],$arr[$key_inner]['multiple_radio_values'],$embed_tr_ctr.$embed_td_ctr,$arr[$key_inner]['other_attributes_value'], $arr[$key_inner]['obj_id']);
                                }
                                //echo '<input type="text" class="display tip form-text embed_getFields inlineEdit_' . $val["ID"] . '" data-embed-name="embed_textbox_' . $val["ID"] . '"  value="' . data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['value'] . '">';
                                echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['value'] . '</label>';
                                echo '</td>';
                            }
                        }
                        $temporary_store = "";
                    } else {
                        if (in_array($column_index, $collect_allowed_columns)) {

                            echo '<td style="text-align:' .  data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['align'] .'">';
                            if($field_replacer_list[$key_inner] != $new_val){
                                echo '<input type="text" class="display tip form-text embed_getFields inlineEdit_' . $val["ID"] . '" data-embed-name="embed_textbox_' . $val["ID"] . '"  value="' . data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['value'] . '">';
                            }
                            echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['value'] . '</label>';
                            echo '</td>';
                        }
                    }
                    $embed_td_ctr++;
                }
            } else {
                foreach ($val as $key_inner => $val_inner) { //loop td fn col
                    $column_index = $column_index + 1;
                    $td_hl_index = $td_hl_index + 1;
                    if (!empty($HLData_json) and $HLAllow == "true") { //kapag naka enable ung checkbox
                        foreach ($HLData_json as $ik => $key2_inner) { // HL data loop
                            if ($key2_inner->{"hl_fn"} == $key_inner) { // column pairing
                                if (evaluateHighlightRule($key2_inner, $val_inner, $val, $current_form_fields_data) == true) {
                                    $temporary_store = 'data-embed-highlight'.$ik.' style="background-color:' . $key2_inner->{"hl_color"} . ';"';
                                    break;
                                }
                            }
                        }
                        if (!empty($temporary_store)) {
                            break;
                        }
                    }
                }

                $count_td = -1;
                foreach ($val as $key => $value) { //loop td fn col
                    // echo "val_inner: " . $value;
                    $count_td = $count_td + 1;
                    $new_val = str_replace("|^|",",",$value); 
                    if (in_array($count_td, $collect_allowed_columns)) {
                        echo '<td  ' . $temporary_store . '  style="text-align:' .  data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['align'] .'">';
                       //var_dump($arr[$key]['fieldObjectType']);
                        if(in_array($key, $computed_fields)){
                            $formula->MyFormula = $field_name_with_formula[$key]['field_value'];
                            echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . $formula->evaluate() . '</label>';
                        }
                        else if($value == 'system_icon'){
                            foreach($selected_columns[$count_td]['column_field_icon_formulas'] as $key => $icon_val){
                                $formula->MyFormula =  $icon_val['icon_selected_formula'];
                                $result = $formula->evaluate();
                                if($result == true){
                                    $icon_to_be_displayed = $icon_val['icon_selected_id'];
                                    echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '"><svg class="icon-svg icon-svg-workspace" viewBox="0 0 50 50"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#' . $icon_to_be_displayed . '"></use></svg></label>';
                                    // echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '"> ' . $value . ' </label>';
                                }
                            }
                        }
                        else{
                            if($field_replacer_list[$key] != $new_val){
                                echo returnFieldType($arr[$key]['fieldObjectType'],$arr[$key]['fieldName'],$val["ID"],data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'],$arr[$key]['multiple_radio_values'],$embed_tr_ctr.$embed_td_ctr,$arr[$key]['other_attributes_value'], $arr[$key]['obj_id']);
                            }
                            //echo '<input type="text" class="display tip form-text embed_getFields inlineEdit_' . $val["ID"] . '" data-embed-name="embed_textbox_' . $val["ID"] . '"  value="' . data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'] . '">';
                            if($arr[$key]['fieldObjectType'] == "multiple_attachment_on_request"){
                                $json = json_decode(data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'], true);
                                if(gettype($json) == 'string'){
                                    $json = json_decode($json, true);
                                }
                                $file_type = json_decode($json[0]['file_type'], true);
                                $file_name = $file_type['name'];
                                $file_length = count($file_name);
                                if($file_length > 0){
                                    $view_html = '<a id="view_files_in_modal" data-body-name="embed_' . $arr[$key]['fieldName'] . '_' . $val["ID"] . '" style="';
                                    $view_html .= 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
                                    $view_html .= '"><u> <i class="fa fa-search"></i> Show (' . $file_length . ') File`s </u></a>';
                                    echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . $view_html . '</label>';
                                }
                                else{
                                    $view_html = '<a id="view_files_in_modal" data-body-name="embed_' . $arr[$key]['fieldName'] . '_' . $val["ID"] . '" style="';
                                    $view_html .= 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
                                    $view_html .= '">No file attachment.</a>';
                                    echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . $view_html . '</label>';
                                }
                                
                            }
                            else if($arr[$key]['fieldObjectType'] == "attachment_on_request"){
                                $value = data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'];
                                $view_html = '<a target="_blank" href="' . $value . '" style="';
                                $view_html .= 'float: left;color: #000;white-space:nowrap;'; //margin-top: 5px;margin-left: 5px;
                                $file = explode("/", $value);
                                if(strlen($value) > 0){
                                    $view_html .= '"><u> <i class="fa fa-search"></i> View File </u> ( <strong>' . $file[count($file) - 1] . '</strong> )</a>';
                                }
                                else{
                                    $view_html .= '">No file attachment.</a>';
                                }
                                echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . $view_html . '</label>';
                            }
                            else{
                                echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'] . '</label>';
                            }
                        }
                        echo '</td>';
                    }
                  $embed_td_ctr++;  
                }
            }
            
            ?>
            
        </tr>
        <?php
            } 
            else{ 
        ?>
        <tr class="tr_<?php echo $val["ID"]; ?> embed-context-menu" <?php if($embed_row_category != ""){if(in_array( $val[$embed_row_category],$category_array)){ echo "category-field='" . $embed_row_category . "' category-value='" . $val[$embed_row_category] . "'"; }}?> record-trackno="<?php echo $val["TrackNo"]; ?>" record-id="<?php echo $val["ID"]; ?>" <?php echo 'data-required-field="'.htmlentities($result_node_data[$key]['fieldRequired']).'"' ;?> >
            <?php if(!$embed_print && ( $enable_embed_row_click == "true" || $enable_embed_row_click == true ) ){ ?>

            <td <?php echo ( ( !empty($HLData_json) and $HLAllow == "true" )?$temporary_store:"" ); ?> >
                <div class="fl-table-ellip" style="text-align: center;">
                    <i id="add_data" class="display inline_add_request"><i class="fa fa-floppy-o dataTip cursor" id="addEmbedded" data-original-title="Add Row"></i></i>
                    <i id="cancel_add_data" class="display cancel_inline_add_request"><i class="fa fa-times dataTip cursor" id="canceladdEmbedded" data-original-title="Cancel Add Row"></i></i>
                    <?php if($embed_action_click_view == "true"){?><i class="fa fa-search dataTip cursor viewTDEmbedded" data-action-attr="view" id="" data-original-title="View Record"></i>&nbsp;&nbsp;<?php }?>
                    <?php if($embed_action_click_edit_popup == "true"){?><i class="fa fa-edit dataTip cursor viewTDEmbedded" data-action-attr="edit" id="" data-original-title="Popup Edit"></i>&nbsp;&nbsp;<?php }?>
                    
                    
                    <?php if($embed_action_click_edit == "true"){?>
                        <i id="showEditActions_<?php echo $val["ID"]; ?>" class="">
                            <i class="fa fa-pencil dataTip cursor editEmbedClass" id="editDEmbedded" data-original-title="Inline Edit"></i>&nbsp;&nbsp;
                        </i>
                        <i id="hideEditActions_<?php echo $val["ID"]; ?>" class="display">
                            <i class="fa fa-check dataTip cursor " id="okeditDEmbedded" data-original-title="Save Edit"></i>&nbsp;&nbsp;
                            <i class="fa fa-times dataTip cursor " id="canceleditDEmbedded" data-original-title="Cancel Edit"></i>&nbsp;&nbsp;
                        </i>
                    <?php }?>
                    <?php if($embed_action_click_copy == "true"){?>
                    <i class="showpasteActions_<?php echo $val["ID"]; ?> ">
                        <i class="fa fa-copy dataTip cursor" id="copyTDEmbedded" data-original-title="Copy Record"></i>&nbsp;&nbsp;
                    </i>
                    <i class="hidepasteActions_<?php echo $val["ID"]; ?> display">
                        <i class="fa fa-paste dataTip cursor " id="pasteTDEmbedded" data-original-title="Paste Record"></i>&nbsp;&nbsp;
                        <i class="fa fa-times dataTip cursor cancelpasteDEmbedded" data-original-title="Cancel Copy Record"></i>&nbsp;&nbsp;
                    </i>
                    <?php }?>
                    <?php if($embed_action_click_delete == "true"){?><i class="fa fa-trash-o dataTip cursor" id="deleteTDEmbedded" data-original-title="Delete Record"></i>&nbsp;&nbsp;<?php }?>
                </div>
            </td>
            <?php } ?>

            <?php if($embed_action_click_number == "true"){ ?>
            <td style="text-align:center;"> 
                <?php echo $embed_count++; ?>
            </td>
            <?php } ?>

            <?php
            $column_index = -1;
            $embed_td_ctr = 0;
            if ($HLType == "cell") {
                
                foreach ($val as $key_inner => $val_inner) { //loop td fn col
                    
                    $column_index = $column_index + 1;
                    $new_val = str_replace("|^|",",",$val_inner);
                    if (!empty($HLData_json) and $HLAllow == "true") { //kapag naka enable ung checkboxs
                        foreach ($HLData_json as $ik => $key2_inner) { // HL data loop
                            if ($key2_inner->{"hl_fn"} == $key_inner) { // column pairing
                                if (evaluateHighlightRule($key2_inner, $val_inner, $val, $current_form_fields_data) == true) {
                                    $temporary_store = 'data-embed-highlight'.$ik.' style="background-color:' . $key2_inner->{"hl_color"} . ';"';
                                    break;
                                }
                            }
                        }
                        if (in_array($column_index, $collect_allowed_columns)) {
                            echo '<td ' . $temporary_store . '   style="text-align:' .  data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['align'] .'">';
                            //var_dump($arr[$key_inner]['other_attributes_value']);
                            if($arr[$key_inner]['fieldObjectType'] == 'textArea'){
                                echo '<pre>';
                            }
                            echo returnFieldType($arr[$key_inner]['fieldObjectType'],$arr[$key_inner]['fieldName'],$val["ID"],data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'],$arr[$key_inner]['multiple_radio_values'],$embed_tr_ctr.$embed_td_ctr,$arr[$key_inner]['other_attributes_value'], $arr[$key_inner]['obj_id']);
                            //echo '<input type="text" class="display tip form-text embed_getFields inlineEdit_' . $val["ID"] . '" data-embed-name="embed_textbox_' . $val["ID"] . '"  value="' . data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['value'] . '">';
                            echo '<label style="white-space:pre-line;" id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['value'] . '</label>';
                            if($arr[$key_inner]['fieldObjectType'] == 'textArea'){
                                echo '</pre>';
                            }
                            echo '</td>';
                        }
                        $temporary_store = "";
                    } else {
                        if (in_array($column_index, $collect_allowed_columns)) {

                            echo '<td style="text-align:' .  data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['align'] .'">';
                            if($field_replacer_list[$key_inner]  != $new_val){
                                echo '<input type="text" class="display tip form-text embed_getFields inlineEdit_' . $val["ID"] . '" data-embed-name="embed_textbox_' . $val["ID"] . '"  value="' . data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['value'] . '">';
                            }
                            echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . data_fld_type($column_type[$column_index]['fldType'],$column_type[$column_index]['fldInputType'],$new_val)['value'] . '</label>';
                            echo '</td>';
                        }
                    }
                    $embed_td_ctr++;
                }
            } else {
                foreach ($val as $key_inner => $val_inner) { //loop td fn col
                    $column_index = $column_index + 1;
                    $td_hl_index = $td_hl_index + 1;
                    if (!empty($HLData_json) and $HLAllow == "true") { //kapag naka enable ung checkbox
                        foreach ($HLData_json as $ik => $key2_inner) { // HL data loop
                            if ($key2_inner->{"hl_fn"} == $key_inner) { // column pairing
                                if (evaluateHighlightRule($key2_inner, $val_inner, $val, $current_form_fields_data) == true) {
                                    $temporary_store = 'data-embed-highlight'.$ik.' style="background-color:' . $key2_inner->{"hl_color"} . ';"';
                                    break;
                                }
                            }
                        }
                        if (!empty($temporary_store)) {
                            break;
                        }
                    }
                }

                $count_td = -1;
                foreach ($val as $key => $value) { //loop td fn col
                    $count_td = $count_td + 1;
                    $new_val = str_replace("|^|",",",$value);
                    if (in_array($count_td, $collect_allowed_columns)) {
                        echo '<td  ' . $temporary_store . '  style="text-align:' .  data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['align'] .'">';
                       //var_dump($arr[$key]['fieldObjectType']);
                        if($field_replacer_list[$key_inner] != $new_val){
                            echo returnFieldType($arr[$key]['fieldObjectType'],$arr[$key]['fieldName'],$val["ID"],data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'],$arr[$key]['multiple_radio_values'],$embed_tr_ctr.$embed_td_ctr,$arr[$key]['other_attributes_value'], $arr[$key]['obj_id']);
                        }
                        //echo '<input type="text" class="display tip form-text embed_getFields inlineEdit_' . $val["ID"] . '" data-embed-name="embed_textbox_' . $val["ID"] . '"  value="' . data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'] . '">';
                        if($arr[$key]['fieldObjectType'] == "multiple_attachment_on_request"){
                            $json = json_decode(data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'], true);
                            if(gettype($json) == 'string'){
                                $json = json_decode($json, true);
                            }
                            $file_type = json_decode($json[0]['file_type'], true);
                            $file_name = $file_type['name'];
                            $file_length = count($file_name);
                            if($file_length > 0){
                                $view_html = '<a id="view_files_in_modal" data-body-name="embed_' . $arr[$key]['fieldName'] . '_' . $val["ID"] . '" style="';
                                $view_html .= 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
                                $view_html .= '"><u> <i class="fa fa-search"></i> Show (' . $file_length . ') File`s </u></a>';
                                echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . $view_html . '</label>';
                            }
                            else{
                                $view_html = '<a id="view_files_in_modal" data-body-name="embed_' . $arr[$key]['fieldName'] . '_' . $val["ID"] . '" style="';
                                $view_html .= 'float: left;color: #000;'; //margin-top: 5px;margin-left: 5px;
                                $view_html .= '">No file attachment.</a>';
                                echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . $view_html . '</label>';
                            }
                            
                        }
                        else if($arr[$key]['fieldObjectType'] == "attachment_on_request"){
                            $value = data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'];
                            $view_html = '<a target="_blank" href="' . $value . '" style="';
                            $view_html .= 'float: left;color: #000;white-space:nowrap;'; //margin-top: 5px;margin-left: 5px;
                            $file = explode("/", $value);
                            if(strlen($value) > 0){
                                $view_html .= '"><u> <i class="fa fa-search"></i> View File </u> ( <strong>' . $file[count($file) - 1] . '</strong> )</a>';
                            }
                            else{
                                $view_html .= '">No file attachment.</a>';
                            }
                            echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . $view_html . '</label>';
                        }
                        else{
                            echo '<label id="lblEmbed_' . $val["ID"] . '" class="lblEmbed_' . $val["ID"] . '">' . data_fld_type($column_type[$count_td]['fldType'],$column_type[$count_td]['fldInputType'],$new_val)['value'] . '</label>';
                        }
                        echo '</td>';
                    }
                  $embed_td_ctr++;  
                }
            }
            
            ?>
            
        </tr>
        <?php
            }
        ?>
        <?php
        $temporary_store = "";
        $embed_tr_ctr++;
    }
    ?>
    <tr class="display appendNewRecord"></tr>
    </tbody>
</table>















































<?php
/*
  //kay jewel below
  ?>
  <tr>
  <?php
  $count_td = 0;
  foreach ($val as $value) { //loop td fn col
  $count_td = $count_td + 1;
  if($HLType == "cell"){
  if($td_hl_index_val == $value){
  echo '<td '.$temporary_store.' >' . $value . '</td>';
  }else{
  echo '<td>' . $value . '</td>';
  }
  }else{
  echo '<td '.$temporary_store.' >' . $value . '</td>';
  }
  }
  ?>
  </tr>
  <?php
 */
?>