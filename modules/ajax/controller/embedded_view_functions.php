<?php
$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$redis_cache = getRedisConnection();

$cur_date = $fs->currentDateTime();
    if(isset($_POST['action'])){
        $action = $_POST['action'];
        
        // Delete Record on Embedded View
            if($action == "embed_deleteRecord"){
                $track_no = $_POST['trackNo'];
                $form_id = $_POST['formID'];
                $record_id = $_POST['recordID'];    
                $workspace_query = $db->query("SELECT * FROM tb_workspace WHERE id={$db->escape($form_id)}","row");

                $record_name = $workspace_query['form_table_name'];
                
                // Delete Record on selected request
                $con = array("TrackNo"  =>  $track_no,
                             "ID"       =>  $record_id);
                $db->delete($record_name,$con);
                $json_data = array("notification"       =>  "Successfull",
                                   "message"            =>  "Record was successfully deleted.");
                echo json_encode($json_data);
                
            }
        // Delete Multiple Records on Embedded View

            else if($action == "embed_multiple_delete"){
               
                $embedRecord = $_POST['trackNos']; //string pa to
                 $embedRecord = json_decode($embedRecord,true);
                 $formArray = [];
                 foreach($embedRecord as $value){
                    $formId = $value['formId'];
                    array_push($formArray, $formId);
                 }
                $formArray = array_unique($formArray);
                 $workspace_query_str = "SELECT * FROM tb_workspace";
                 if(count($embedRecord) >= 1){
                     $workspace_query_str .= " WHERE";
                    foreach ($formArray as $value) {
                      $workspace_query_str .= " `id` = ".$value." OR";
                    }
                 }

                $workspace_query_str = substr($workspace_query_str, 0, count($workspace_query_str) - 3);
                
                
                $mga_form = $db->query($workspace_query_str ,"array");
                foreach($mga_form as $value){
                 $tableName =  $value['form_table_name'];
                 $formId_mgaForm = $value['id'];
                 $requesIDArr = [];
                 foreach($embedRecord as $value){
                   $formId_embedRecord = $value['formId'];
                   if($formId_embedRecord == $formId_mgaForm){
                    $requestId_embedRecord = $value['id'];
                      array_push($requesIDArr,$requestId_embedRecord);
                   }
                 }
                 $requesIDArr = implode(",", $requesIDArr);
                 $delete_multiple_query = ("DELETE FROM ".$tableName." WHERE ID in(".$requesIDArr.")");
                 $deleteQueryExe = $db->query($delete_multiple_query,"array");
                }
                
                // print_r($deleteQueryExe);
                $balik = array(
                    'message' => "Successfully deleted 4 records",
                );
                print_r(json_decode($balik));

                // foreach($array as $value){
                //    $tN =  $value['trackNo'];
                //    $requestId = $value['id'];
                //    $formId = $value['formId'];
                //    $concatenationQuery = $tN . $requestId . $formId;
                // }
                // print_r(json_decode($embedRecord));
                
                
                // $record_name = $workspace_query['form_table_name'];
                
                // // Delete Record on selected request
                
                // $con = array("TrackNo"  =>  $track_no,
                //              "ID"       =>  $record_id);
                
                // $db->delete($record_name,$con);
                
                // $json_data = array("notification"       =>  "Successfull",
                //                    "message"            =>  "Record was successfully deleted.");
                
                // echo json_encode($json_data);
                
            }
        // Update Record on Embedded View
            else if($action == "embed_saveRecord"){
                $track_no = $_POST['trackNo'];
                $form_id = $_POST['formID'];
                $record_id = $_POST['recordID'];
                $flds = $_POST['flds'];
                $form_parent_id = $_POST['form_parent_id'];
                $workspace_query = $db->query("SELECT * FROM tb_workspace WHERE id={$db->escape($form_id)}","row");
                $record_name = $workspace_query['form_table_name'];
                
                // Update Record on selected request
                //$set = array();
                $con = array("TrackNo"  =>  $track_no,
                             "ID"       =>  $record_id);
                foreach($flds as $key=>$fld){
                    //$set[] = array($key   =>  $fld);
                    $db->update($record_name,array($key   =>  $fld),$con);
                }
                
                $json_data = array("notification"       =>  "Successfull",
                                   "message"            =>  "Record was successfully updated.");
                if ($redis_cache) {
                    $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
                        "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
                        "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
                        "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
                    functions::clearFormulaLookupCaches($form_parent_id, $formula_arr);
                    $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
                        "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
                        "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
                        "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
                    functions::clearFormulaLookupCaches($form_id, $formula_arr);
                }
                echo json_encode($json_data);
                
            }
        
        // Copy Record on Embedded View
            else if($action == "embed_copyRecord"){
                $track_no = $_POST['trackNo'];
                $form_id = $_POST['formID'];
                $record_id = $_POST['recordID'];
                
                $workspace_query = $db->query("SELECT * FROM tb_workspace WHERE id={$db->escape($form_id)}","row");
                $record_name = $workspace_query['form_table_name'];
                
                
                $cols = array();
                $result = $db->query("SHOW COLUMNS FROM {$record_name}"); 
                 foreach($result as $r) {
                  if (!in_array($r["Field"],array("ID","TrackNo","DateCreated","DateUpdated"))) {//add other columns here to want to exclude from the insert
                   $cols[]= $r["Field"];
                  } 
                }
                
                //build and do the insert
                $insert = array();
                $result = $db->query("SELECT * FROM {$record_name} WHERE ID={$db->escape($record_id)} AND TrackNo={$db->escape($track_no)}");
                    foreach($result as $r) {
                        
                        foreach($cols as $counter=>$col) {
                            if($col == "ProcessorType" && $r["ProcessorType"] == ""){
                                $insert["ProcessorType"] = 'null';
                            }elseif($col == "ProcessorLevel" && $r["ProcessorLevel"] == ""){
                                $insert["ProcessorLevel"] = 'null';
                            }elseif($col == "Editor" && $r["Editor"] == ""){
                                $insert["Editor"] = 'null';
                            }elseif($col == "Viewer" && $r["Viewer"] == ""){
                                $insert["Viewer"] = 'null';
                            }else{
                                $insert[$col] = $r[$col];
                            }
                            
                        }
                        //$insertSQL = "INSERT INTO {$record_name} (".implode(", ",$cols).") VALUES (";
                        //$count=count($cols);
                        //foreach($cols as $counter=>$col) {
                        //    $insertSQL .= "'".$r[$col]."'";
                        //        if ($counter<$count-1) {$insertSQL .= ", ";}//dont want a , on the last one
                        //}
                        //$insertSQL .= ")";
                        //
                        //$db->query($insertSQL);
                  }
                $copyID = $db->insert($record_name,$insert); //execute the query
                $generate_trackNo = library::getTrackNo($db, $workspace_query['reference_prefix'], $workspace_query['reference_type'], $copyID);
                $db->update($record_name,array("TrackNo"=>$generate_trackNo,
                                               "DateCreated"=>$cur_date,
                                               "DateUpdated"=>$cur_date),array("ID"=>$copyID));
                $json_data = array("notification"       =>  "Successfull",
                                   "trackNo"            =>  $generate_trackNo,
                                   "recordID"           =>  $copyID,
                                   "oldRecordID"        =>  $_POST['recordID'],
                                   "referenceIndex"     =>  $_POST['index'],
                                   "message"            =>  "Record was successfully pasted.");
                
                echo json_encode($json_data);
            }
            else if($action == "embed_newRecord"){
               
                $track_no = $_POST['trackNo'];
                $form_id = $_POST['formID'];
                $record_id = $_POST['recordID'];

                $insert = array();
                $workspace_query = $db->query("SELECT * FROM tb_workspace WHERE id={$db->escape($form_id)}","row");
                $table_name = $workspace_query['form_table_name'];
                $flds = $_POST['flds'];
                $workflow_id = $_POST['Workflow_ID'];
                $fieldEnabled = $_POST['fieldEnabled'];
                $fieldRequired = $_POST['fieldRequired'];
                foreach ($flds as $key => $value) {
                    $insert[$key] = $value;
                }
                $copyID = $db->insert($table_name,$insert);
                
                $generate_trackNo = library::getTrackNo($db, $workspace_query['reference_prefix'], $workspace_query['reference_type'], $copyID);
                $db->update($table_name,array("TrackNo"=>$generate_trackNo,
                                               "DateCreated"=>$cur_date,
                                               "DateUpdated"=>$cur_date,
                                               "Requestor" =>  $auth["id"],
                                               "Workflow_ID"=>$workflow_id,
                                               "fieldEnabled"=>$fieldEnabled,
                                               "fieldRequired"=>$fieldRequired,
                                               "CreatedBy"=> $auth["id"],
                                               "UpdatedBy"=>  $auth["id"],
                                               "Status" => "Draft"
                                               ),array("ID"=>$copyID));
                $json_data = array("notification"       =>  "Successfull",
                                   "trackNo"            =>  $generate_trackNo,
                                   "recordID"           =>  $copyID,
                                   "debug"              =>  $table_name."unique".$copyID,
                                   "workflow_id"        => $workflow_id,
                                   "fieldEnabled"       => $fieldEnabled,
                                   "fieldRequired"      => $fieldRequired,
                                   "message"            =>  "Record was successfully added.");
                
                echo json_encode($json_data);
                
                
                

            }
            elseif ($action == "embed_searchRecord") {
                $form_id = $_POST['formID'];

                // $str = "SELECT * FROM tb_workspace WHERE id = {$db->escape($form_id)}";
                // $form = $db->query($str, "row");
                $str = "SELECT WS.*, WFO.workflow_id, WFO.buttonStatus,WFO.fieldEnabled, WFO.fieldRequired, WFO.fieldHiddenValue FROM tb_workspace WS 
                                       LEFT JOIN tbworkflow WF
                                        ON WS.Id = WF.form_id 
                                       LEFT JOIN tbworkflow_objects WFO
                                        ON WFO.workflow_id = WF.id 
                                        WHERE WS.id={$db->escape($form_id)} AND WFO.type_rel = '1'  AND WF.Is_Active = '1' AND WS.is_delete = 0 AND WF.is_delete = 0 ";
                $form = $db->query($str, "row");

                $workflow_id = $form['workflow_id'];
                $fieldEnabled = $form['fieldEnabled'];
                $fieldRequired = $form['fieldRequired'];
                $json_data = array(
                      "currentUSER" =>  $auth["id"],
                      "workflowID" => $workflow_id,
                      "fieldEnabled" => $fieldEnabled,
                      "fieldRequired" => $fieldRequired
                );
                
                echo json_encode($json_data);
                
                                       
            }
            else if($action == "embed_saveRecordWithWorkflow"){
                $track_no = $_POST['trackNo'];
                $form_id = $_POST['formID'];
                $record_id = $_POST['recordID'];
                $flds = $_POST['flds'];
                $personDoc = new Person($db, $auth["id"]);
                $request = new Request();
                $request->load($form_id, $record_id, $personDoc);
                $request->process_triggers = true;
                foreach($flds as $key=>$fld){
                    $request->data[$key] = $fld;
                    //$set[] = array($key   =>  $fld);
                }
                $request->createFieldLevelLogs($fieldEnabled);
                $request->modify();
                $request->processWF();

                $json_data = array("notification"       =>  "Successfull",
                                   "message"            =>  "Record was successfully updated.");
                
                echo json_encode($json_data);
            }
            else if($action == "embed_deleMultipleRecord"){
                $details = $_POST['details'];
                
                $query_arr = array();
                // $where_clause = "START TRANSACTION";
                $db->query("START TRANSACTION");
                // array_push($query_arr, $where_clause);
                foreach($details as $data_key => $data_value){
                    $formDoc = new Form($db, $data_value['formID']);
                    $where_clause = "DELETE FROM " . $formDoc->form_table_name . " WHERE " . "`TrackNo` = '" . $data_value['trackNo'] . "' AND id = " . $data_value['recordID'];
                    $db->query("DELETE FROM " . $formDoc->form_table_name . " WHERE " . "`TrackNo` = '" . $data_value['trackNo'] . "' AND id = " . $data_value['recordID']);
                    array_push($query_arr, $where_clause);
                }
                $db->query("COMMIT");
                // $where_clause = "COMMIT";
                // array_push($query_arr, $where_clause);
                $json_data = array("notification"       =>  "Successfull",
                                   "message"            =>  "Record was successfully Deleted.",
                                   "query_array"        =>  $query_arr);


                echo json_encode($json_data);
            }
            else if($action == "embed_editMultipleRecord"){
                $details = $_POST['details'];
                $query_arr = array();
                $where_clause = "START TRANSACTION";
                // $db->query("START TRANSACTION");
                array_push($query_arr, $where_clause);
                foreach($details as $data_key => $data_value){
                    $formDoc = new Form($db, $data_value['formID']);
                    $data_flds = $data_value['flds'];
                    $fields_qry = "";
                    //echo gettype($data_flds) . ", " . (gettype($data_flds) != "NULL") . ", " . $data_flds . "\n";
                    if($data_flds != "0"){
                        foreach($data_flds as $key => $value){
                            $fields_qry .= "`" . $key . "`" . " = '" . $value . "', ";
                        }
                        $fields_qry = substr($fields_qry, 0, strlen($fields_qry) - 2);
                        $where_clause = "UPDATE " . $formDoc->form_table_name . " SET " . $fields_qry . " WHERE " . "`TrackNo` = '" . $data_value['trackNo'] . "' AND id = " . $data_value['recordID'];
                        // $db->query("DELETE FROM " . $formDoc->form_table_name . " WHERE " . "`TrackNo` = '" . $data_value['trackNo'] . "' AND id = " . $data_value['recordID']);
                        array_push($query_arr, $where_clause);
                    }
                }
                // $db->query("COMMIT");
                $where_clause = "COMMIT";
                array_push($query_arr, $where_clause);
                $json_data = array("notification"       =>  "Successfull",
                                   "message"            =>  "Record was successfully updated.",
                                   "query_array"        =>  $query_arr);


                echo json_encode($json_data);
            }
            else if($action == "embed_executeCustomActions"){
                $form_source_type = $_POST['source_form_type']?:"Single";
                $row_data = $_POST['row_data']?:[];
                $custom_action_formula = $_POST['custom_action_formula']?:"";
                $return_value = array();
                foreach($row_data as $row_key => $row_value){
                    $formDoc = new Form($db, $row_value['form_id']);
                    $formula = new Formula($custom_action_formula);
                    // echo "SELECT * FROM " . $formDoc->form_table_name . " WHERE ID = " . $row_value['request_id'] . " AND TrackNo = '" . $row_value['track_no'] . "'";
                    $formula->DataFormSource[0] = $db->query("SELECT * FROM " . $formDoc->form_table_name . " WHERE ID = " . $row_value['request_id'] . " AND TrackNo = '" . $row_value['track_no'] . "'", "row");
                    $return_val = $formula->evaluate();
                    // echo $formula->getProccessedFormula();
                    // echo json_encode($formula->DataFormSource[0]);
                    $return_value[$row_value['request_id']] = $return_val;
                }
                echo json_encode($return_value);
            }
        
    }

?>