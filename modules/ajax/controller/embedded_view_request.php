<?php

use Extensions;

//check if session is alive if not.. retun false;
if (!Auth::hasAuth('current_user')) {
    return false;
}
$db = new Database();
$auth = Auth::getAuth('current_user');
$ext = new Extensions();
$embed_data = $_POST['embed_row_data'];
$embed_deleted_data = $_POST['embed_deleted_row'];
// print_r(json_encode($embed_data));
$prefix = $_POST['prefix'];

if($embed_deleted_data != ""){
    foreach($embed_deleted_data as $obj_id => $details){
        $form_id = $details['form_id'];
        $formObject = new Form($db, $form_id);

        $id_list = implode(", ", $details['id_list']);
        echo "DELETE FROM `" . $formObject->form_table_name . "` WHERE ID IN (" . $id_list . ")\n";
        $db->query("DELETE FROM `" . $formObject->form_table_name . "` WHERE ID IN (" . $id_list . ")", "array");
    }
}
foreach ($embed_data as $form_id => $form_details) {
    foreach ($form_details as $reference_id => $fields_data) {
        if ($fields_data['submitted'] == "true") {
            continue;
        }
        $formID = $fields_data['form_id'];
        $formObject = new Form($db, $form_id);
        $field_models = $fields_data['field_models'];
        // print_r(json_encode($field_models));
        if ($fields_data['fields_data']['TrackNo']['value'] == "pending...") {
            $fields_data['fields_data']['ID']['value'] = 0;
            $field_models['RequestID'] = 0;
        }
        $insert_result = $field_models['RequestID']? : 0;
        $workflow_id = $field_models['Workflow_ID'];
        $node_id = $field_models['Node_ID']['value'];
        $mode = 'formApproval';
        $origin = $fields_data['origin'];
        $field_and_values = array();

        $status_embed = ""; //$fields_data['fields_data']['status-embed']['value'];
        $prop_emebed = ""; //$fields_data['fields_data']['prop-emebed']['value'];
        if ($origin == "in-line") {
            foreach ($field_models as $key => $value) {
                if (strpos($key, $prefix) > -1) {
                    $field_and_values[$key] = $value['value'];
                    $new_key = str_replace($prefix, '', $key);
                    //for fixing the values
                    // print_r('field_model: ' . $new_key . " embed key: " . $key . " value: " . $field_models[$key] . "\n");
                    $field_and_values[$new_key] = $field_models[$key];
                } else {
                    $field_and_values[$key] = $field_models[$key];
                }
            }
        } else {
            foreach ($fields_data['fields_data'] as $key => $value) {
                if ($key != "undefined") {
                    // print_r($key . " " . $value['value'] . "\n");
                    $field_and_values[$key] = $value['value'];
                }
            }
        }

        $myRequest = new Request();
        $myRequest->load($formID, $insert_result);
        //validate save conflict
        $formObject = new Form($db, $formID);
        $current_node_id = $db->query("SELECT LastAction FROM " . $formObject->form_table_name . " WHERE id = {$insert_result}", "row");
        // if ($current_node_id["LastAction"] != $field_and_values["LastAction"] && $insert_result != 0) {
        //     $return_arr["message"] = "Request was already processed by other user.";
        //     $return_arr["headerLocation"] = "/application?id=" . functions::base_encode_decode("encrypt", $form_id);
        //     $return_arr['workspaceLocation'] = "/workspace?view_type=request&formID=$form_id&requestID=0";
        //     print_r(json_encode($return_arr));
        //     continue;
        // }
        $fields = array();
        $fieldResults = $myRequest->fields;
        $date = $this->currentDateTime();
        $fieldEnabled = json_decode($field_and_values['fieldEnabled'], true);
        $computedFields = json_decode($field_and_values['computedFields'], true);
        $reservedFields = array("TrackNo", "Requestor", "Status", "Processor", "LastAction", "DateCreated", "DateUpdated", "CreatedBy", "UpdatedBy", "Unread", "Node_ID", "Workflow_ID"
            , "fieldEnabled", "fieldRequired", "fieldHiddenValues", "imported", "Repeater_Data", "SaveFormula", "CancelFormula");
        $fieldRequired = "";
        if ($mode == 'formApproval') {
            foreach ($fieldResults as $field) {
                $index_search = in_array($field['COLUMN_NAME'], $fieldEnabled);
                $reserved_index = in_array($field['COLUMN_NAME'], $reservedFields);
                $computed_index = in_array($field['COLUMN_NAME'], $computedFields);

                $fields["middleware_process"] = 0;
                if ($index_search || $reserved_index || $computed_index) {
                    if (is_array($field_and_values[$field['COLUMN_NAME']])) {
                        $fields[$field['COLUMN_NAME']] = join("|^|", $field_and_values[$field['COLUMN_NAME']]);
                    } else if ($field['COLUMN_NAME'] == 'DateCreated' && $insert_result == '0') {
                        $fields[$field['COLUMN_NAME']] = $date;
                    } else if ($field['COLUMN_NAME'] == 'DateUpdated') {
                        $fields[$field['COLUMN_NAME']] = $date;
                    } else if ($field['COLUMN_NAME'] == 'fieldRequired') {
                        //if from embed and new request only
                        $fieldRequired = $field_and_values['fieldRequired'];
                    } else {

                        if ($field['COLUMN_NAME'] == 'TrackNo') {
                            $myRequest->trackNo = $field_and_values[$field['COLUMN_NAME']];
                        }
                        // print_r($field['COLUMN_NAME'] . ": " . $field_and_values[$field['COLUMN_NAME']] . "\n");
                        $fields[$field['COLUMN_NAME']] = $field_and_values[$field['COLUMN_NAME']];
                    }
                }
            }
        }
        if ($mode == 'viewApproval') {
            $fields['Node_ID'] = $node_id;
            $fields['Workflow_ID'] = $workflow_id;
        }
        if ($insert_result == '0') {
            //for parent in embeded
            if ($status_embed == 'Draft') {
                $fields['fieldRequired'] = $fieldRequired;
                $fields['Status'] = 'Draft';
                $fields['Processor'] = '';
                $fields['LastAction'] = '';
            }
            if ($node_id == 'Draft') {
                $fields['Status'] = 'DRAFT';
            }
            $myRequest->data = $fields;
            $add_editors = $field_and_values["AdditionalEditors"];
            if ($add_editors != "") {
                $add_editors_arr = explode("|", $add_editors);
                $myRequest->additionalEditors = $add_editors_arr;
            }

            $add_readers = $field_and_values["AdditionalReaders"];
            if ($add_readers != "") {
                $add_readers_arr = explode("|", $add_readers);
                $myRequest->additionalReaders = $add_readers_arr;
            }
            try {
                $extensionResponse = $ext->executeExtension($myRequest, Extensions::$PRE_SAVE);
                if ($extensionResponse) {
                    $myRequest->responseData['Pre_Save_Extension_Response'] = $extensionResponse;
                }
            } catch (Exception $ex) {
                //  TODO add logging capabilities here to know if an extension had an error
            }
            $myRequest->save();

            try {
                $extensionResponse = $ext->executeExtension($myRequest, Extensions::$POST_SAVE);
                if ($extensionResponse) {
                    $myRequest->responseData['Post_Save_Extension_Response'] = $extensionResponse;
                }
            } catch (Exception $ex) {
                //  TODO add logging capabilities here to know if an extension had an error
            }

            if ($node_id != 'Save' && $node_id != 'Cancel' && $node_id != 'Draft') {
                if ($status_embed != 'Draft') { //for parent in embeded
                    // $myRequest->keywordFields = $keywordsField;
                    $myRequest->process_workflow_events = false;
                    $myRequest->processWF();
                    backgroundProcessRequestWF($myRequest->formId, $myRequest->id);
                    $FormID = $myRequest->formId;
                    $myRequest->createRequireComment($_POST['RequireComment']);
                    try {
                        $ext->executeExtension($myRequest, Extensions::$POST_PROCESS_WORKFLOW);
                    } catch (Exception $ex) {
                        //  TODO add logging capabilities here to know if an extension had an error
                    }
                }
            }
        } else {
            if ($node_id == 'Cancel') {
                $fields['Status'] = 'Cancelled';
                $fields['Processor'] = '';
                $fields['LastAction'] = '';
                $myRequest->emailCancelledRequest();
            }
            if ($node_id == 'Save' || $node_id == 'Cancel') {
                $logDoc = new Request_Log($db);
                $logDoc->form_id = $formID;
                $logDoc->request_id = $insert_result;

                if ($node_id == 'Save') {
                    $logDoc->details = 'Saved';
                } else {
                    $logDoc->details = 'Cancelled';
                }

                $logDoc->created_by_id = $auth['id'];
                $logDoc->date_created = $date;
                $logDoc->save();
            }

            // print_r("\n" . json_encode($fields) . "\n");
            $myRequest->data = $fields;

            $add_editors = $_POST["AdditionalEditors"];
            if ($add_editors != "") {
                $add_editors_arr = explode("|", $add_editors);
                $myRequest->additionalEditors = $add_editors_arr;
            }

            $add_readers = $_POST["AdditionalReaders"];
            if ($add_readers != "") {
                $add_readers_arr = explode("|", $add_readers);
                $myRequest->additionalReaders = $add_readers_arr;
            }
            $myRequest->createFieldLevelLogs($fieldEnabled);

            try {
                $extensionResponse = $ext->executeExtension($myRequest, Extensions::$PRE_MODIFY);
                if ($extensionResponse) {
                    $myRequest->responseData['Pre_Modify_Extension_Response'] = $extensionResponse;
                }
            } catch (Exception $ex) {
                //  TODO add logging capabilities here to know if an extension had an error
            }
            // print_r("\n" . json_encode($myRequest->data) . "\n");
            $myRequest->modify();

            try {
                $extensionResponse = $ext->executeExtension($myRequest, Extensions::$POST_MODIFY);
                if ($extensionResponse) {
                    $myRequest->responseData['Post_Modify_Extension_Response'] = $extensionResponse;
                }
            } catch (Exception $ex) {
                //  TODO add logging capabilities here to know if an extension had an error
            }
            if ($node_id != 'Save' && $node_id != 'Cancel' && $node_id != 'Draft') {
                if ($status_embed != 'Draft') { //for parent in embeded
                    // $myRequest->keywordFields = $keywordsField;
                    $myRequest->process_workflow_events = false;
                    $myRequest->processWF();
                    backgroundProcessRequestWF($myRequest->formId, $myRequest->id);
                    $FormID = $myRequest->formId;

                    $myRequest->createRequireComment($_POST['RequireComment']);
                    try {
                        $ext->executeExtension($myRequest, Extensions::$POST_PROCESS_WORKFLOW);
                    } catch (Exception $ex) {
                        //  TODO add logging capabilities here to know if an extension had an error
                    }
                }
            }
        }
    }
}
?>