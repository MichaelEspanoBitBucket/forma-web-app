<?php

$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$userCompany = new userQueries();
$company_id = $auth['company_id'];
$userID = $auth['id'];
$timezone = "Asia/Manila";
$search = new Search();
if (function_exists('date_default_timezone_set'))
    date_default_timezone_set($timezone);

if (isset($_POST['action'])) {
    $action = $_POST['action'];
    $date = $fs->currentDateTime();
    $search_value = $_POST['search_value'];
    if ($action == "loadExternalDBDataTable") {
        //get external db connection
        $start = $_POST['iDisplayStart'];
        $externalDBDatatable = new ExternalDatabase($db);
        $arr = array('column-sort' => $_POST['column-sort'],
            'column-sort-type' => $_POST['column-sort-type'],
            "start" => $start,
            "endLimit" => $_POST['limit'],
            "search_value" => $search_value);

        $getExternalDBDatatable = $externalDBDatatable->getExternalDBDatatable($arr, $company_id, "array");
        $countExternalDBDatatable = $externalDBDatatable->getExternalDBDatatable($arr, $company_id, "numrows");
        // echo $getExternalDBDatatable;
        // return false;
        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $countExternalDBDatatable,
            "iTotalDisplayRecords" => $countExternalDBDatatable,
            "start" => $start,
            "aaData" => array()
        );
        foreach ($getExternalDBDatatable as $externalDB) {
            $arr = array();
            $actions = '<center>
                <i class="icon-trash fa fa-trash-o tip cursor deleteExternalDB" data-placement="top" data-original-title="Delete" style="margin-right:5px" data-id="' . $externalDB['id'] . '"></i> 
                <i class="icon-edit fa fa-pencil-square-o tip cursor update-DBConnection" data-placement="top" data-original-title="Edit" json_data="' . htmlentities(json_encode($externalDB)) . '"></i>';

            $createdDate = $info['createdDate'];
            if ($createdDate == "0000-00-00 00:00:00") {
                $createdDate = "";
            }
            $updatedDate = $info['updatedDate'];
            if ($updatedDate == "0000-00-00 00:00:00") {
                $updatedDate = "";
            }
            $status = $fs->getStatus($externalDB['is_active']);
            $arr[] = "<div class='fl-table-ellip'>" . $externalDB['connection_name'] . "</div>";
            $arr[] = "<div class='fl-table-ellip'>" . $externalDB['db_type_name'] . "</div>";
            $arr[] = "<div class='fl-table-ellip'>" . $status . "</div>";
            $arr[] = "<div class='fl-table-ellip'>" . $externalDB['created_displayname'] . "</div>";
            $arr[] = "<div class='fl-table-ellip'>" . $externalDB['date_created'] . "</div>";
            $arr[] = "<div class='fl-table-ellip'>" . $externalDB['updated_displayname'] . "</div>";
            $arr[] = "<div class='fl-table-ellip'>" . $externalDB['date_updated'] . "</div>";
            $arr[] = $actions;
            $output['aaData'][] = $arr;
        }
        echo json_encode($output);
    } else if ($action == "addExternalDB") {
        $externalDatabase = new ExternalDatabase($db);
        unset($_POST['action']); // unset action
        $values = $_POST; // assign to different variable

        $condition = " AND connection_name = {$db->escape($values['connection_name'])} ";
        $limit = " LIMIT 0,1 ";
        $getDuplicate = $externalDatabase->getExternalDB($condition, $limit, "", $company_id, "numrows");
        //check duplicate
        if ($getDuplicate == 0) {
            $values['date_created'] = $date;
            $values['created_by'] = $userID;
            $values['company_id'] = $company_id;

            echo $db->insert("tbexternal_database", $values);
        } else {
            echo "duplicate";
        }
        /* audit logs here */
    } else if ($action == "deleteExternalDB") {
        $id = $_POST['id'];
        $where = array("id" => $id);
        $set = array("is_delete" => "1",
            "date_updated" => $date,
            "updated_by" => $userID);
        echo $db->update("tbexternal_database", $set, $where);

        /* audit logs here */
    } else if ($action == "updateExternalDB") {
        $externalDatabase = new ExternalDatabase($db);
        unset($_POST['action']); // unset action
        $values = $_POST; // assign to different variable
        $condition = " AND connection_name = {$db->escape($values['connection_name'])} AND tbe_d.id!= {$db->escape($values['id'])}";
        $limit = " LIMIT 0,1 ";
        $getDuplicate = $externalDatabase->getExternalDB($condition, $limit, "", $company_id, "numrows");
        //check duplicate
        if ($getDuplicate == 0) {
            $id = $values['id'];
            unset($values['id']); //unset id before update
            $values['date_updated'] = $date;
            $values['updated_by'] = $userID;
            $where = array("id" => $id);
            echo $db->update("tbexternal_database", $values, $where);
        } else {
            echo "duplicate";
        }

        /* audit logs here */
    } else if ($action == "test_connection") {
        $db_type = $_POST['db_type'];

        $db_host = $_POST['db_hostname'];
        $db_name = $_POST['db_name'];
        $db_username = $_POST['db_username'];
        $password = $_POST['db_password'];


        if ($db_type == "1") {
            $mysqlConn = new MySQLDatabase($db_host, $db_name, $db_username, $password);
            
            if($mysqlConn->is_connected){
                echo "200";
                $mysqlConn->disConnect();
            }else{
                 echo "Failed My SQL";
            }
//            echo "wala pa";
        } else if ($db_type == "2") {
            $dbMSSQL = new MSSQLDatabase($db_host, $db_name, $db_username, $password);
            if ($dbMSSQL->is_connected) {
                echo "200";
                $dbMSSQL->disconnect();
            } else {
                echo "Failed MS SQL";
            }
            //sample get tables
            // $getTables = $dbMSSQL->query("SELECT * FROM information_schema.tables","array");
            //sample get columns
            // $getColumns = $dbMSSQL->query("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='test_table1'");
            // print_r($getColumns);
        } else if ($db_type == "3") {
            $dbOracle = new OracleDatabase($db_host, $db_name, $db_username, $password);
            if ($dbOracle->is_connected) {
                echo "200";
                $dbOracle->disConnect();
            } else {
                print_r($e);
                echo "Failed Oracle";
            }
            //sample get table
            // $getTables = $dbOracle->query("SELECT TABLE_NAME FROM user_tables ORDER BY table_name DESC");
            //sample get columns
            // $getColumns = $dbOracle->query("SELECT column_name FROM USER_TAB_COLUMNS WHERE table_name = 'test_table_1'");
            // print_r($getColumns);
        }
    }
}
?>