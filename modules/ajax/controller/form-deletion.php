<?php
include_once API_LIBRARY_PATH . 'API.php';
$auth = Auth::getAuth('current_user');
$hasAuth = Auth::hasAuth('current_user');
$userCompany = new userQueries();
$notifications = new notifications();
$db = new APIDatabase();
$db_old = new Database();
functions::currentDateTime();

if (isset($_POST['action'])) {
    $action = $_POST['action'];
    if ($action == "getFormList") {
        $db->connect();
        $form_list = $db->query("SELECT form_name as FRM_NAME FROM tb_workspace ORDER BY form_name ASC", "array");
        echo json_encode($form_list);
        $db->disconnect();
    
    } else if ($action == "getProcess_DeleteForm") {
        $db->connect();
        $db->beginTransaction();
        $form_name = $_POST['form_name'];
            
        $ws_id = $db->query("SELECT `tb_workspace`.`id` as WorkspaceID, `tb_workspace`.`form_table_name` as WorkspaceTableName FROM `tb_workspace` WHERE `tb_workspace`.`form_name` = {$db->escape($form_name)}", "row");
        
        try {

            $wf_id = $db->query("SELECT `tbworkflow`.`id` as WorkflowID  FROM `tbworkflow` WHERE `tbworkflow`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");
            $flds_id = $db->query("SELECT `tbfields`.`id` as FieldsID FROM `tbfields` where `tbfields`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");
            $frmUsers_id = $db->query("SELECT `tbform_users`.`id` as FormUserID FROM `tbform_users` WHERE `tbform_users`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");
            $reqLogs_id = $db->query("SELECT `tbrequest_logs`.`id` as RequestLogsID FROM `tbrequest_logs` WHERE `tbrequest_logs`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");
            $notif_id = $db->query("SELECT `tbnotification`.`id` as NotificationID FROM `tbnotification` WHERE `tbnotification`.`table_name` = {$db->escape($ws_id['WorkspaceTableName'])}", "numrows");
            $reqUsers_id = $db->query("SELECT `tbrequest_users`.`id` as RequestUsersID FROM `tbrequest_users` WHERE `tbrequest_users`.`Form_ID` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");
            $reqSeen_id = $db->query("SELECT `tb_request_seen`.`id` as RequestSeenID FROM `tb_request_seen` WHERE `tb_request_seen`.`formID` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");
            $annotation_id = $db->query("SELECT `tb_annotation`.`Anno_UniqueID` as AnnotationID FROM `tb_annotation` WHERE `tb_annotation`.`Anno_FormID` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");

            $db->commit();
            $db->disconnect();

            $data = array(
                "workspace"     =>1,
                "workflow"      =>$wf_id,
                "fields"        =>$flds_id,
                "formusers"     =>$frmUsers_id,
                "requestlogs"   =>$reqLogs_id,
                "notification"  =>$notif_id,
                "requestusers"  =>$reqUsers_id,
                "requestseen"   =>$reqSeen_id,
                "annotation"    =>$annotation_id
            );
            echo json_encode($data);

        } catch (APIException $e) {
            echo "0";
        }

    } else if ($action == "execute_DeleteForm") {
        $db->connect();
        $db->beginTransaction();
        $time_start = microtime(true);
        $form_name = $_POST['form_name'];
        $ws_id_array = $db->query("SELECT `tb_workspace`.`id` as WorkspaceID, `tb_workspace`.`form_table_name` as WorkspaceTableName FROM `tb_workspace` WHERE `tb_workspace`.`form_name` = {$db->escape($form_name)}", "array");
            
        foreach($ws_id_array as $key => $ws_id) {
            if ($ws_id['WorkspaceID'] !== "") {
                $db->query("DELETE FROM `tb_annotation` WHERE `tb_annotation`.`Anno_FormID` = {$db->escape($ws_id['WorkspaceID'])}", "update");
                $db->query("DELETE FROM `tb_request_seen` WHERE `tb_request_seen`.`formID` = {$db->escape($ws_id['WorkspaceID'])}", "update");
                $db->query("DELETE FROM `tbrequest_users` WHERE `tbrequest_users`.`Form_ID` = {$db->escape($ws_id['WorkspaceID'])}", "update");
                $db->query("DELETE FROM `tbnotification` WHERE `tbnotification`.`table_name` = {$db->escape($ws_id['WorkspaceTableName'])}", "update");
                $db->query("DELETE FROM `tbrequest_logs` WHERE `tbrequest_logs`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "update");
                $db->query("DELETE FROM `tbform_users` WHERE `tbform_users`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "update");
                $db->query("DELETE FROM `tbfields` WHERE `tbfields`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "update");

                $wf_id = $db->query("SELECT `tbworkflow`.`id` as WorkflowID  FROM `tbworkflow` WHERE `tbworkflow`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "array");
                foreach ($wf_id as $val_wf_id) {
                    $tbworkflow_lines_ret = $db->query("DELETE FROM `tbworkflow_lines` WHERE `tbworkflow_lines`.`workflow_id` = {$db->escape($val_wf_id['WorkflowID'])}", "update");
                    $tbworkflow_objects_ret = $db->query("DELETE FROM `tbworkflow_objects` WHERE `tbworkflow_objects`.`workflow_id` = {$db->escape($val_wf_id['WorkflowID'])}", "update");
                    $tbworkflow_ret = $db->query("DELETE FROM `tbworkflow` WHERE `tbworkflow`.`form_id` = {$db->escape($ws_id['WorkspaceID'])} AND `tbworkflow`.`id` = {$db->escape($val_wf_id['WorkflowID'])}", "update");
                }

                //Drop table Structured for Request
                try {
                    $db->query("DROP TABLE {$ws_id['WorkspaceTableName']}", "update");
                } catch (APIException $e) {

                }

                $affected_form_arr = array(
                    "annotation"    => "Annotation",
                    "fields"        => "Fields",
                    "formusers"     => "Form Users",
                    "notification"  => "Notification",
                    "requestlogs"   => "Request Logs",
                    "requestseen"   => "Request Seen",
                    "requestusers"  => "Request Users",
                    "workflow"      => "Workflow",
                    "workspace"     => "Workspace" 
                );

                try {
                    formDeletionInsertLogs($db, $form_name, $auth, "Form Deletion", $affected_form_arr); //For Deletion Logs
                } catch (APIException $e) {
                    echo "ERROR_NO_FORM_DELETION_LOG";
                    break;
                }

                //Workspace
                $tb_workspace_ret = $db->query("DELETE FROM `tb_workspace` WHERE `tb_workspace`.`form_name` = {$db->escape($form_name)}", "update");

                $db->commit();
                $db->disconnect();

                if ($tb_workspace_ret > 0 && $ws_id['WorkspaceID'] !== "") {
                    $time_end = microtime(true);
                    $exec_time = (($time_end - $time_start));
                    echo round($exec_time, 4);
                } else {    
                    echo "0";
                }
            } else {
                echo "0";
                break;
            }
        }
    
    } else if ($action == "getProcess_FormCleanUp") {
        $db->connect();
        $db->beginTransaction();
        $form_name = $_POST['form_name'];
        
        $ws_id = $db->query("SELECT `tb_workspace`.`id` as WorkspaceID, `tb_workspace`.`form_table_name` as WorkspaceTableName FROM `tb_workspace` WHERE `tb_workspace`.`form_name` = {$db->escape($form_name)}", "row");

        try {

            $reqLogs_id = $db->query("SELECT `tbrequest_logs`.`id` as RequestLogsID FROM `tbrequest_logs` WHERE `tbrequest_logs`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");
            $reqSeen_id = $db->query("SELECT `tb_request_seen`.`id` as RequestSeenID FROM `tb_request_seen` WHERE `tb_request_seen`.`formID` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");
            $notif_id = $db->query("SELECT `tbnotification`.`id` as NotificationID FROM `tbnotification` WHERE `tbnotification`.`table_name` = {$db->escape($ws_id['WorkspaceTableName'])}", "numrows");
            $annotation_id = $db->query("SELECT `tb_annotation`.`Anno_UniqueID` as AnnotationID FROM `tb_annotation` WHERE `tb_annotation`.`Anno_FormID` = {$db->escape($ws_id['WorkspaceID'])}", "numrows");

            $db->commit();
            $db->disconnect();

            $data = array(
                "request_logs"  =>$reqLogs_id,
                "request_seen"  =>$reqSeen_id,
                "notification"  =>$notif_id,
                "annotation"    =>$annotation_id
            );
            echo json_encode($data);

        } catch (APIException $e) {
            echo "0";
        }
    
    } else if ($action == "execute_FormCleanUp") {
        $db->connect();
        $db->beginTransaction();
        $form_name = $_POST['form_name'];
        $list_to_cleanup_array = json_decode($_POST['list_to_cleanup'], true);
        $time_start = microtime(true);

        $ws_id = $db->query("SELECT `tb_workspace`.`id` as WorkspaceID, `tb_workspace`.`form_table_name` as WorkspaceTableName FROM `tb_workspace` WHERE `tb_workspace`.`form_name` = {$db->escape($form_name)}", "row");
        if ($ws_id['WorkspaceID'] !== "") {
            
            $affected_tables_arr = array();
            foreach ($list_to_cleanup_array as $list_allowed) {
                if ($list_allowed == "annotation") {
                    $annotation = $db->query("DELETE FROM `tb_annotation` WHERE `tb_annotation`.`Anno_FormID` = {$db->escape($ws_id['WorkspaceID'])}", "update");
                    $affected_tables_arr["tb_annotation"] = "Annotation";
                } else if ($list_allowed == "notification") {
                    $notif = $db->query("DELETE FROM `tbnotification` WHERE `tbnotification`.`table_name` = {$db->escape($ws_id['WorkspaceTableName'])}", "update");
                    $affected_tables_arr["tb_notification"] = "Notification";
                } else if ($list_allowed == "request_logs") {
                    $req_logs = $db->query("DELETE FROM `tbrequest_logs` WHERE `tbrequest_logs`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "update");
                    $affected_tables_arr["tb_request_logs"] = "Request Logs";
                } else if ($list_allowed == "request_seen") {
                    $req_seen = $db->query("DELETE FROM `tb_request_seen` WHERE `tb_request_seen`.`formID` = {$db->escape($ws_id['WorkspaceID'])}", "update");
                    $affected_tables_arr["tb_request_seen"] = "Request Seen";
                } else {
                    echo "0";
                }
            }

            formDeletionInsertLogs($db, $form_name, $auth, "Form clean-up", $affected_tables_arr); //For Deletion Logs

            $db->commit();
            $db->disconnect();

            if (($req_logs > 0) || ($req_seen > 0) || ($notif > 0) || ($annotation > 0)) {
                $time_end = microtime(true);
                $exec_time = (($time_end - $time_start));
                echo round($exec_time, 4);
            } else {
                $db->rollback();
                echo "0";
            }
        } else {
            echo "0";
        }

    } else if ($action == "getProcess_WorkflowCleanUp") {
        $db->connect();
        $db->beginTransaction();
        $form_name = $_POST['form_name'];
        
        try {

            $ws_id = $db->query("SELECT `tb_workspace`.`id` as WorkspaceID FROM `tb_workspace` WHERE `tb_workspace`.`form_name` = {$db->escape($form_name)}", "row");
            $wf_id = $db->query("SELECT `tbworkflow`.`id` as WorkflowID, `tbworkflow`.`title` as WorkflowID_Title, `tbworkflow`.`date` as WorkflowID_DateCreated, `tbworkflow`.`is_active` as WorkflowID_Active FROM `tbworkflow` WHERE `tbworkflow`.`form_id` = {$db->escape($ws_id['WorkspaceID'])}", "array");

            $db->commit();
            $db->disconnect();
            
            $data = array(
                "workspace_id"  =>$ws_id['WorkspaceID'],
                "workflow_id"   =>$wf_id
            );
            echo json_encode($data);

        } catch (APIException $e) {
            echo "0";
        }
 
    } else if ($action == "execute_WorkflowCleanUp") {
        $db->connect();
        $db->beginTransaction();
        $workspace_id = $_POST['workspace_id'];
        $form_name = $_POST['form_name'];
        $workflow_id_array = json_decode($_POST['workflow_id'], true);
        $time_start = microtime(true);
        
        foreach ($workflow_id_array as $workflow_id) {
            $tbworkflow_lines_ret = $db->query("DELETE FROM `tbworkflow_lines` WHERE `tbworkflow_lines`.`workflow_id` = '{$workflow_id}'", "update");
            $tbworkflow_objects_ret = $db->query("DELETE FROM `tbworkflow_objects` WHERE `tbworkflow_objects`.`workflow_id` = '{$workflow_id}'", "update");
            $tbworkflow_ret = $db->query("DELETE FROM `tbworkflow` WHERE `tbworkflow`.`form_id` = '{$workspace_id}' AND `tbworkflow`.`id` = '{$workflow_id}'", "update");
        }

        formDeletionInsertLogs($db, $form_name, $auth, "Workflow clean-up", $workflow_id_array); //For Deletion Logs

        $db->commit();
        $db->disconnect();

        if (($tbworkflow_lines_ret > 0) && ($tbworkflow_objects_ret > 0) && ($tbworkflow_ret > 0)) {
            $time_end = microtime(true);
            $exec_time = (($time_end - $time_start));
            echo round($exec_time, 4);
        } else {
            echo "0";
        }
    
    } else if ($action == "showList_FormDeletionHistory") {
        $search_value = trim($_POST['search_value']);
        $start = $_POST['iDisplayStart'];
        $limit = $_POST['limit'];
        $end_limit = $_POST['endlimit'];
        // $form_id = $post_data['form_id'];
        // $workflow_id = $post_data['workflow_id'];
        $column_sort = $_POST['column-sort'];
        $column_sort_type = $_POST['column-sort-type'];
        
        $search_value = $db_old->addslash_escape(mysql_escape_string($search_value));

        $limit_qry = "LIMIT {$start} , {$end_limit}";
        $orderBy_qry = "ORDER BY {$column_sort} {$column_sort_type}";
       
        $db->connect();

        try {

            $counter_form_deletion_history = $db->query("SELECT * FROM tb_form_deletion_logs", "numrows");
            $form_deletion_history_data = $db->query("SELECT * FROM tb_form_deletion_logs WHERE Form_Name LIKE '%{$search_value}%' {$orderBy_qry} {$limit_qry}", "array");

            $output = array(
                "sEcho" => intval($_POST['sEcho']),
                "iTotalRecords" => $counter_form_deletion_history,
                "iTotalDisplayRecords" => $counter_form_deletion_history,
                "start" => $start,
                "aaData" => array()
            );

            foreach ($form_deletion_history_data as $value) {
                $pushArray = array();
                    //$pushArray[] = "<div class='fl-table-ellip' title='" . $value['Form_ID'] . "'>" . $value['Form_ID'] . "</div>";
                    $pushArray[] = "<div class='fl-table-ellip form-deletion-history-row' id='a' title='" . $value['Form_Name']       . "' data-original-title='". $value['Form_Name']      ."'>" . $value['Form_Name']    . "</div>";
                    $pushArray[] = "<div class='fl-table-ellip form-deletion-history-row' id='b' title='" . $value['Action']          . "' data-original-title='". $value['Action']         ."'>" . $value['Action']       . "</div>";
                    $pushArray[] = "<div class='fl-table-ellip form-deletion-history-row' id='c' title='" . $value['Affected_ID']     . "' data-original-title='". $value['Affected_ID']    ."'>" . $value['Affected_ID']  . "</div>";
                    $pushArray[] = "<div class='fl-table-ellip form-deletion-history-row' id='d' title='" . $value['Date_Changed']    . "' data-original-title='". $value['Data_Changed']   ."'>" . $value['Date_Changed'] . "</div>";
                    $pushArray[] = "<div class='fl-table-ellip form-deletion-history-row' id='e' title='" . $value['Auth_User']       . "' data-original-title='". $value['Auth_User']      ."'>" . $value['Auth_User']    . "</div>";
                $output['aaData'][] = $pushArray;
            }
            echo json_encode($output);
            $db->disconnet();

        } catch (APIException $e) {
            echo "0";
        }
    }

    // else if ($action == "sqlbulkquery") {
    //     $data = $_POST['dataaaa'];
    //     $db_bq = new DatabaseBulkQueries();

    //     $db->connect();
    //     try {
            
    //         $data = json_decode($data, true);

    //         $db_bq->init($data, '1_tbl_product', 'LOAD_DATA_INFILE', 
    //             array(
    //                 'action'=>'write_data',
                    
    //                 'query_handler' => 'mysql_query',
    //                 'trigger_errors' => true,
    //                 'clean_memory' => true,
    //                 'link_identifier' => null
    //             )
    //         );

    //         $db_bq->load();
    //         $db_bq->execute();
    //         $db_bq->loadDataInFileCleanUp();

    //         $db->disconnect();

    //     } catch (APIException $e) {
    //         echo "0";
    //     }
    // }
}

function formDeletionInsertLogs ($db, $form_name, $auth, $action, $details_collected) {
    $ws_id = $db->query("SELECT `tb_workspace`.`id` as WorkspaceID FROM `tb_workspace` WHERE `tb_workspace`.`form_name` = {$db->escape($form_name)}", "row");
    $affected_id = "";
    foreach ($details_collected as $details_value) {
        $affected_id .= $details_value.", ";
    }
    $affected_id_string = substr($affected_id, 0, strlen($affected_id) - 2);
    $data = array(
        "Form_ID"       =>  $ws_id['WorkspaceID'],
        "Form_Name"     =>  $form_name,
        "Action"        =>  $action,
        "Affected_ID"   =>  $affected_id_string,
        "Date_Changed"  =>  "NOW()",
        "Auth_ID"       =>  $auth['id'],
        "Auth_User"     =>  $auth['display_name']
    );
    $db->insert("tb_form_deletion_logs", $data);

}