<?php
$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$userCompany = new userQueries();
$company_id = $auth['company_id'];
$userID = $auth['id'];
$timezone = "Asia/Manila";
$search = new Search();
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

    if(isset($_POST['action'])){
        $action = $_POST['action'];
        $date = $fs->currentDateTime();
        
            if($action=="loadFormCategory"){
                $res = "";
                    $out = "";
                    $fields_out = "";
                    $company_user_info = array();
                    $fields = array(); 
                    
                    //$companyUsers_fields = $db->query("SHOW COLUMNS FROM tbuser");
                    $company = $userCompany->getCompany($auth['company_id']);
                    
                    $suggestion_sql = $db->query("SELECT * FROM  `tbform_category` WHERE company_id = {$db->escape($company_id)} AND is_active = 1 AND is_delete = 0 ORDER BY id DESC","array");
                    // Table fields
                        //foreach($companyUsers as $info){
                        //    $company_user_info[] = $info;
                        //}
                        
                        
                        foreach($suggestion_sql as $info){
                            // Actions
                            $actions = '<center><i class="icon-trash fa fa-trash-o tip cursor deleteformCategory" data-placement="top" data-original-title="Delete" style="margin-right:5px" data-id="'. $info['id'] .'"></i> <i class="icon-edit fa fa-pencil-square-o tip cursor updateformCategory" data-placement="top" data-original-title="Edit" data-id="'. $info['id'] .'" data-name="'. $info['category_name'] .'"></i><div class="display users_'. $info['id'] .'">'. $info['users'] .'</div><div class="display image_'. $info['id'] .'">'. $info['image'] .'</div></center>';
                              
                            $company_user_info[] = array("id"=>$info['id'],
                                                        "title"=>$info['category_name'],
                                                        "actions"=>$actions);
                            
                        }
                            $res = array(/*"sEcho"                        =>      "3",
                                         "iTotalRecords"                =>      $countRows,
                                         "iTotalDisplayRecords"         =>      $countRows,*/
                                         "company_user"                 =>      $company_user_info);
                        
                            $string = json_encode($res);
                            
                            echo $string;
            }else if($action=="loadFormCategoryDatatable"){
                $search_value = $_POST['search_value'];
                $start = $_POST['iDisplayStart'];
                $end = "10";
                if($_POST['limit']!=""){
                    $end = $_POST['limit'];
                }
                $limit = "";
                if($start!=""){
                    $limit = " LIMIT $start, $end";
                } 
                $orderBy = " ORDER BY category_name ASC";
                if($_POST['column-sort']){
                  $orderBy = " ORDER BY ".$_POST['column-sort']." ".$_POST['column-sort-type'];
                }
                $vwget_form_categories = "select tbfc.*,tbfcu.user_type, tbfcu.user, 
                (select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 1 AND access_type = 2) AS positions,
                (select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 2 AND access_type = 2) AS departments,
                (select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 3 AND access_type = 2) AS users, 
                (select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 4 AND access_type = 2) AS groups, 
                (select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 3 AND access_type = 1) AS admin, 
                tbu_c.display_name as creator, tbu_u.display_name as editor FROM tbform_category tbfc LEFT JOIN 
                tbform_category_users tbfcu ON tbfc.id = tbfcu.form_category_id LEFT JOIN tbuser tbu_c on tbfc.createdBy = tbu_c.id LEFT JOIN 
                tbuser tbu_u ON tbfc.updatedBy = tbu_u.id WHERE tbfc.is_delete = 0 AND 
                tbfcu.access_type = 1";
                // $query = "SELECT * FROM  `tbform_category` WHERE company_id = {$db->escape($company_id)} AND is_active = 1 AND is_delete = 0 AND category_name LIKE '%". $search_value ."%' $orderBy ";
                // $query = "SELECT * FROM `vwget_form_categories` WHERE company_id = {$db->escape($company_id)} AND (user_type = 3 AND user = $userID) GROUP BY id $orderBy";
                // echo $orderBy;
                
                $query = $vwget_form_categories." AND tbfc.company_id = {$db->escape($company_id)} AND (tbfcu.user_type = 3 AND tbfcu.user = '$userID') AND 
                (tbfc.category_name LIKE '%". $search_value ."%' || tbfc.description LIKE '%". $search_value ."%' ||
                tbfc.createdDate LIKE '%". $search_value ."%' || tbfc.updatedDate LIKE '%". $search_value ."%' ||
                tbu_c.display_name LIKE '%". $search_value ."%' || tbu_u.display_name LIKE '%". $search_value ."%') 
                GROUP BY id $orderBy ";


                $suggestion_sql = $db->query($query.$limit,"array");
                $countForm = $db->query($query,"numrows");

                if(!$countForm){
                    $countForm = 0;
                }
                
                $output = array(
                    "sEcho" => intval($_POST['sEcho']),
                    "iTotalRecords" => $countForm,
                    "iTotalDisplayRecords" => $countForm,
                    "start"=>$start,
                    "aaData" => array(),
                );    
                    foreach($suggestion_sql as $info){
                        $departments = explode(",", $info['departments']);
                        $positions = explode(",", $info['positions']);
                        $users = explode(",", $info['users']);
                        $groups = explode(",", $info['groups']);
                        $array = array();
                        foreach ($departments as $key => $value_dept) {
                            $array['departments'][] = $value_dept;
                        }
                        foreach ($positions as $key => $value_pos) {
                            $array['positions'][] = $value_pos;
                        }
                        foreach ($users as $key => $value_users) {
                            $array['users'][] = $value_users;
                        }
                        foreach ($groups as $key => $value_groups) {
                            $array['groups'][] = $value_groups;
                        }
                        $users = json_encode($array);

                        $admin_array = explode(",", $info['admin']);
                        // $admin_array = $info['admin'];
                        $admin = array();
                        foreach ($admin_array as $key => $value_users) {
                            $admin['users'][] = $value_users;
                        } 

                        $admin = json_encode($admin);
                        // Actions
                        $company_user_info = array();  
                        $actions = '<center><i class="icon-trash fa fa-trash-o tip cursor deleteformCategory" data-placement="top" data-original-title="Delete" style="margin-right:5px" data-id="'. $info['id'] .'"></i> <i class="icon-edit fa fa-pencil-square-o tip cursor updateformCategory" data-placement="top" data-original-title="Edit" data-id="'. $info['id'] .'" data-name="'. htmlentities($info['category_name']) .'" data-description="'. htmlentities($info['description']) .'"></i><div class="display users_'. $info['id'] .'">'. $users .'</div><div class="display image_'. $info['id'] .'">'. $info['image'] .'</div><div class="display admin_'. $info['id'] .'">'. $admin .'</div></center>';
                        
                        $createdDate = $info['createdDate'];
                        if($createdDate=="0000-00-00 00:00:00"){
                            $createdDate = "";
                        }
                        $updatedDate = $info['updatedDate'];
                        if($updatedDate=="0000-00-00 00:00:00"){
                            $updatedDate = "";
                        }
                        
                        $company_user_info[] = "<div class='fl-table-ellip'>" . htmlentities($info['category_name'])."</div>";
                        $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list(htmlentities($info['description']),"")."</div>";
                        $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($info['creator'],"")."</div>";
                        $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($createdDate,"")."</div>";
                        $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($info['editor'],"")."</div>";
                        $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($updatedDate,"")."</div>";
                        $company_user_info[] = $actions;
                        $output['aaData'][] = $company_user_info;
                        
                    }
                    
                    
                    echo json_encode($output);
                    // echo $query.$limit;
            }else if($action=="deleteformCategory"){
                $id = $_POST['id'];
                $strSql = "SELECT id,form_json FROM tb_workspace WHERE company_id = {$db->escape($auth[company_id])} AND is_delete = 0";
                $getForms = $db->query($strSql,"array");
                $json = array();
                foreach ($getForms as $forms) {
                    $json_arr =  json_decode($forms['form_json'],true);
                    if($id == $json_arr['categoryName']){
                        array_push($json, $forms);
                    }
                }

                //deletememcached
                $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
                $form_record_memcached = array("form_list","form_count");
                $nav_memcached = array("leftbar_nav");
                $starred_memcached = array("starred_list");

                $deleteMemecachedKeys = array_merge($request_record_memcacached,
                                                    $form_record_memcached,
                                                    $nav_memcached,
                                                    $starred_memcached
                                                  );
                $fs->deleteMemcacheKeys($deleteMemecachedKeys);
                echo json_encode($json);
            }else if($action=="updateForms"){
                $formToUpdate = $_POST['formToUpdate'];
                $id = $_POST['id'];
                foreach ($formToUpdate as $value) {
                    $update = array("form_json"=>$value['form_json']);
                    $where = array("id"=>$value['id']);
                    $db->update("tb_workspace",$update,$where);
                }
                $where_fc = array("id"=>$id);
                $db->delete("tbform_category",$where_fc);
            }else if($action=="updateForms_softDelete"){
                $formToUpdate = $_POST['formToUpdate'];
                $id = $_POST['id'];
                foreach ($formToUpdate as $value) {
                    $update = array("form_json"=>$value['form_json'],"category_id"=>0);
                    $where = array("id"=>$value['id']);
                    $db->update("tb_workspace",$update,$where);
                }
                $where_fc = array("id"=>$id);
                // $db->delete("tbform_category",$where_fc);
                $set = array("is_delete" => "1");
                $where = array("id" => $id);
                $this->update("tbform_category", $set, $where);

                //audit trail

                $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"37",
                                  "table_name"=>"tbform_category",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

                $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);


            }else if($action=="addCategory"){
                $category_name = $_POST['category_name'];
                $category_description = $_POST['category_description'];
                $json_privacy = $_POST['users'];

                $admin = json_decode($_POST['admin'],true);
                $admin['users'][] = $auth['id'];
                $getDuplicate = "SELECT * FROM tbform_category WHERE category_name = '". $category_name ."' AND company_id = '". $auth['company_id'] ."' AND is_delete = 0";
                $getDuplicate = $db->query($getDuplicate,"numrows");

                if($getDuplicate>0){
                    echo "0";
                    return false;
                }
                $insert = array("category_name"=>$category_name,
                                "description"=>$category_description,
                                "company_id"=>$auth['company_id'],
                                "users"=>$json_privacy,
                                "image"=>$_POST['logo'],
                                "createdBy"=>$auth['id'],
                                "createdDate"=>$date,
                                "is_active"=>1);



                $category_id = $db->insert("tbform_category",$insert);

                $insert['id'] = $category_id;

                //for user
                setUsersMigration($db,json_decode($json_privacy),$insert,2,2);

                //for admin
                setUsersMigration($db,$admin,$insert,1,2);
                // --here
            }else if($action=="editCategory"){
                $category_name = $_POST['category_name'];
                $category_description = $_POST['category_description'];
                $category_id = $_POST['category_id'];


                $getDuplicate = "SELECT * FROM tbform_category WHERE category_name = '". $category_name ."' AND company_id = '". $auth['company_id'] ."' AND id!='". $category_id ."'";
                $getDuplicate = $db->query($getDuplicate,"numrows");

                if($getDuplicate>0){
                    echo "0";
                    return false;
                }
                
                $json_privacy = $_POST['users'];

                $admin = json_decode($_POST['admin'],true);
                $admin['users'][] = $auth['id'];

                $update = array("category_name"=>$category_name,
                                "description"=>$category_description,
                                "users"=>$json_privacy,
                                "image"=>$_POST['logo'],
                                "updatedBy"=>$auth['id'],
                                "updatedDate"=>$date);
                $where = array("id"=>$category_id);
                $db->update("tbform_category",$update,$where);

                $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"36",
                                  "table_name"=>"tbform_category",
                                  "record_id"=>$category_id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

                $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);

                // $db->delete("tbform_category_users",array("form_category_id"=>$category_id));

                $update['id'] = $category_id;

                $formCategoryAccess = new FormCategoryUserAccess($db,$category_id);
                //for user
                // setUsersMigration($db,json_decode($json_privacy),$update,2,2);
                $formCategoryAccess->insertUser($_POST['checkedEle']['users'],2);
                $formCategoryAccess->deleteUser($_POST['uncheckedEle']['users'],2);

                //for admin
                // setUsersMigration($db,$admin,$update,1,2);
                $formCategoryAccess->insertUser($_POST['checkedEle']['admin'],1);
                $formCategoryAccess->deleteUser($_POST['uncheckedEle']['admin'],1);


                //to sync form category users to form users
                // functions::syncFormUsers($db,"WHERE category_id = '". $category_id ."'");
                functions::syncFormUsersV2($db,"WHERE category_id = '". $category_id ."'",$_POST['uncheckedEle']['users'],0);
                functions::syncFormUsersV2($db,"WHERE category_id = '". $category_id ."'",$_POST['uncheckedEle']['admin'],4);

                //deletememcached
                $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
                $form_record_memcached = array("form_list","form_count");
                $nav_memcached = array("leftbar_nav");
                $starred_memcached = array("starred_list");

                $deleteMemecachedKeys = array_merge($request_record_memcacached,
                                                    $form_record_memcached,
                                                    $nav_memcached,
                                                    $starred_memcached
                                                  );
                $fs->deleteMemcacheKeys($deleteMemecachedKeys);
                // --here
            }else if($action == "getFormCategories"){
                // $query = "SELECT * FROM `vwget_form_categories` WHERE company_id = {$db->escape($company_id)} AND (user_type = 3 AND user = $userID) GROUP BY id;";
                $vwget_form_categories = 'select tbfc.id,category_name,image,tbfc.company_id,tbfcu.user_type, tbfcu.user, 
(select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 1 AND access_type = 2) AS positions,
(select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 2 AND access_type = 2) AS departments,
(select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 3 AND access_type = 2) AS users, 
(select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 3 AND access_type = 1) AS admin 
FROM tbform_category tbfc LEFT JOIN tbform_category_users tbfcu ON tbfc.id = tbfcu.form_category_id WHERE tbfc.is_delete = 0 AND 
tbfcu.access_type = 1';
                $query = $vwget_form_categories." AND tbfc.company_id = {$db->escape($company_id)} AND (tbfcu.user_type = 3 AND tbfcu.user = '$userID') GROUP BY id ORDER BY category_name ASC ";
                $data = $db->query($query,"array");


                echo json_encode($data);
            }else if($action == "getCategoryUsers"){
                $category_id = $_POST['category_id'];
                if($category_id=="0"){
                    $getDepartments = $search->getAllDepartmentV2();
                    $getPosition = $search->getAllPositionV2();
                    $getFormGroups = $search->getFormGroups();
                    $getUsers = $search->getAllUsers("");
                    $getUsersAdmin = $search->getAllUsers(" AND user_level_id = '2'");
                }else{
                    //get departments
                    $querydepartments = "select * FROM vwget_form_category_departments WHERE company_id = '". $auth['company_id'] ."' AND form_category_id = $category_id GROUP BY user";
                    $getDepartments = $db->query($querydepartments,"array");


                    //get positions
                    $querypositions = "select * FROM vwgetform_category_positions WHERE company_id = '". $auth['company_id'] ."' AND form_category_id = $category_id GROUP BY position";
                    $getPosition = $db->query($querypositions,"array");

                    //get groups
                    $queryGroups = "select * FROM vwgetform_category_groups WHERE company_id = '". $auth['company_id'] ."' AND form_category_id = $category_id GROUP BY group_name";
                    $getFormGroups = $db->query($queryGroups,"array");

                    //get users
                    $queryusers = "select * FROM vwgetform_category_users WHERE company_id = '". $auth['company_id'] ."' AND form_category_id = $category_id";
                    $getUsers = $db->query($queryusers." AND access_type = 2","array");

                    //get admin of form category
                    $getUsersAdmin = $db->query($queryusers." AND access_type = 1 AND id!='". $auth['id'] ."'","array");
                }

                $ret = array(
                            "departments"=>json_encode($getDepartments),
                            "positions"=>json_encode($getPosition),
                            "users"=>json_encode($getUsers),
                            "users_admin"=>json_encode($getUsersAdmin),
                            "form_groups"=>json_encode($getFormGroups),
                        );

                echo json_encode($ret);
            }

    }


?>