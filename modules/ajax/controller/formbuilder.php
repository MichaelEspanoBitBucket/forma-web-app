<?php

$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$upload = new upload();
$search = new Search();
$redis_cache = getRedisConnection();

if (Auth::hasAuth('current_user')) {
    if (isset($_POST["user_id"])) {
        $old_log_authid = $_POST["user_id"];
        if ($auth["id"] != $old_log_authid) {
            echo "not same id";
            return;
        }
    }
    if (isset($_POST['action'])) {
        $date = $fs->currentDateTime();

        switch ($_POST['action']) {

            // Save workspace to the databasea

            case "saveWorkspace":
                //$workspace = $_POST['workspace'];
                $json_return = array();
                $data = $_POST['json_encode'];
                $json = json_decode($data, true);
                $fields = $json['form_fields'];
                $category_id = $json['categoryName'];
                $form_display_type = $json['form_display_type'];
                $display_import = $json['display_import'];
                $form_display_category = $json['fm-type-dos'];
                $form_display_formula = $json['form_display-dynamic'];
                $myrequest_visibility = $json["myrequest_visibility"]; //my request visibility
//              $workspace_status = $json['workspace_status']; //published or template
                $workspace_version = $json['workspace_version'];
                $formId = $_POST['formId'];
                $sanitized_data = $json;
                $allow_portal = $json['allow_portal'];
                $portal_icon = $json['portal_icon'];
                $save_type = $_POST['form_save_type'];

                //for deletion
                $delete_enable = $json['form_json']['delete_enable'];
                $delete_type = $json['form_json']['delete_type'];

                $sanitized_data['button_content'] = "";
                $sanitized_data['WorkspaceContent'] = "";
                if (gettype($sanitized_data) == "object" or gettype($sanitized_data) == "array") {
                    $sanitized_data = json_encode($sanitized_data);
                }
                $workspace_content = str_replace('\\', '', $json['WorkspaceContent']);
                $reservedFields = array('TrackNo'
                    , 'Requestor', 'Status', 'Processor', 'ProcessorType', 'ProcessorLevel', 'LastAction', 'DateCreated'
                    , 'DateUpdated', 'CreatedBy', 'UpdatedBy', 'Unread', 'Node_ID', 'Workflow_ID', 'fieldEnabled', 'fieldRequired', 'fieldHiddenValues', 'imported', 'Repeater_Data', 'Editor', 'Viewer', "middleware_process"
                    , "SaveFormula", "CancelFormula", "enable_delegate","GeoLocationDetails");
                // NOTE: TO REARRANGE THE POSITION COLUMN OF RESERVED COLUMNS USE: ALTER TABLE 1_tbl_test_mobile_acces MODIFY COLUMN GeoLocationDetails LONGTEXT AFTER `enable_delegate`
                // Insert record to the database of tb_workspace    
                //
				$fields_decode = json_decode($fields, true);
                //fields data
                $fields_data = $json["fields_data"];

                //added by joshua reyes 03/07/2016 [Button Visibility]
                $button_properties_data = json_decode($json["BtnProperties"], true);

                if ($save_type == "2") {
                    $oldFormID = $formId;
                    $formId = '0';
                }
                //force workspace version to 1 if enable staging setting is off
                if (ENABLE_STAGING == "0") {
                    $workspace_version = "1";
                }
                if ($formId == '0') {
                    $tblname = $json['Title'];

                    $tblname = $auth['company_id'] . '_tbl_' . str_replace(' ', '', $tblname);


                    //admin
                    if ($json["form-admin"] == "0") {
                        $jsonAdmin = json_encode(array("users" => array($auth['id'])));
                    } else {
                        $jsonAdmin = json_decode($json["form-admin"], true);
                        // array_push($jsonAdmin['users'], $auth['id']);
                        $jsonAdmin['users'][] = $auth['id'];
                        $jsonAdmin = json_encode($jsonAdmin);
                    }

                    //insert array for new form
                    $insert = array("form_name" => $json['Title'],
                        "form_table_name" => $tblname,
                        "form_alias" => $json['Alias'],
                        "form_description" => $json['DisplayName'],
                        "form_name_formula" =>$json['Aliasformu'],
                        "enable_formula" =>$json['Enableformu'],
                        "formula_is_field" =>$json['Enablefield'],
                        "form_content" => $workspace_content,
                        "form_buttons" => $json['BtnName'],
                        "created_by" => $auth['id'],
                        "updated_by" => $auth['id'],
                        "company_id" => $auth['company_id'],
                        "reference_prefix" => $json['Prefix'],
                        "reference_type" => $json['Type'],
                        "version" => 1,
                        "form_json" => $sanitized_data,
                        "date_created" => $date,
                        "button_content" => $json['button_content'],
                        "active_fields" => $json["existing_fields"],
                        "date_updated" => $date,
                        "is_active" => 1,
                        "is_active_mobility" => $json["is_active_mobility"],
                        "MobileJsonData" => $json["MobileJsonData"],
                        "MobileContent" => $json["MobileContent"],
                        "form_authors" => $json["form_authors"],
                        "form_viewers" => $json["form_viewers"],
                        "form_admin" => $jsonAdmin,
                        "category_id" => $category_id,
                        "form_display_type" => $form_display_type,
                        "display_import" => $display_import,
                        "my_request_visible" => $myrequest_visibility, //my request visibility
                        "form_display_category" => $form_display_category,
                        "form_display_formula" => $form_display_formula,
//                                                "form_type"=>$workspace_status,
                        "workspace_version" => $workspace_version,
                        "enable_deletion" => $delete_enable,
                        "deletion_type" => $delete_type,
                        "allow_portal" => $allow_portal,
                        "portal_icon" => $portal_icon,
                    );
                    // $array_condition = array("form_table_name" => $tblname,"form_name"=>$json['Title'],"w.company_id"=>$auth['company_id']);


                    // if ($json['Alias'] != "") {
                    //     $formCount2 = $search->getSpecificForms2(" form_alias = '" . $json['Alias'] . "' AND w.company_id = '" . $auth['company_id'] . "' AND w.is_delete = 0 ");
                    //     if (count($formCount2) > 0) {
                    //         $message = "Duplicate The entered form Alias already exists. Please modify and resave." . count($formCount2);
                    //         $json_return = array("status" => "warning",
                    //             "message" => $message);
                    //         echo json_encode($json_return);
                    //         return false;
                    //     }
                    // }

                    $formCount = $search->getSpecificForms2(" (form_table_name = '" . $tblname . "' OR form_name = '" . $json['Title'] . "') AND w.company_id = '" . $auth['company_id'] . "' AND w.is_delete = 0 ");
                    if (count($formCount) > 0) {
                        // $message = "This form is already created";
                        $message = "Duplicate The entered form title already exists. Please modify and resave.";
                        $json_return = array("status" => "warning",
                            "message" => $message);
                        echo json_encode($json_return);
                        return false;
                        // }else if ($json['Alias'] != "") {
                        //     // $message = "This form is already created";
                        //     if(count($formCount2) > 0){
                        //         //do nothing .reserved.
                        //         $message = "Duplicate The entered form Alias already exists. Please modify and resave.";
                        //         $json_return = array("status" => "warning",
                        //             "message" => $message);
                        //         echo json_encode($json_return);
                        //         return false;
                        //     }
                    } else {
                        $strSQL = 'CREATE TABLE ' . $tblname . ' (';
                        $strSQL.= ' ID int PRIMARY KEY AUTO_INCREMENT, ';

                        foreach ($reservedFields as $field_name) {
                            if ($field_name == 'DateCreated' || $field_name == 'DateUpdated') {
                                $strSQL .= $field_name . ' datetime,';
                            } else {
                                $strSQL .= $field_name . ' longtext,';
                            }
                        }

                        $strSQL = substr($strSQL, 0, $strSQL - 1);
                        $strSQL .= ' ) COLLATE utf8_general_ci';


                        $tblID = $db->create($strSQL);



                        //alter fields

                        $alterSQL = 'ALTER TABLE `' . $tblname . "` ";
                        foreach ($fields_decode as $field) {
                            $fieldName = trim(functions::removePairedBracket($field['fieldName']));
                            $fieldType = $field['fieldType'];
                            //$strSQL.=$fieldName.' '.$fieldType.',';
                            //$dropSQL = 'ALTER TABLE ' . $tblname . ' DROP ' . $fieldName;
                            //$db->create($dropSQL);
                            // $active_fields = split(",", $formDoc->active_fields);
                            $field_index = in_array($fieldName, $active_fields);


                            if ($fieldName == "" || in_array($fieldName, $fieldAltered)) {
                                continue;
                            }

                            // if ($field_index) {
                            //update field
                            // 	$alterSQL .=  ' MODIFY `' . $fieldName . '` ' . $fieldType . ', ';
                            // } else {
                            //new field
                            $alterSQL .= ' ADD `' . $fieldName . '` ' . $fieldType . ', ';
                            // }
                            //push the fieldnames in array to prevent multiple alter of fields
                            $fieldAltered[] = $fieldName;
                        }
                        //remove space and comma of the sql string


                        $alterSQL = substr($alterSQL, 0, strlen($already) - 2);
                        $alterTable = $db->create($alterSQL);

                        if (!$alterTable) { //conditon
                            $message = " There's something wrong in saving of your formbuilder. Please contact your administrator for assistance.";
                            $json_return = array("status" => "error",
                                "message" => $message);
                            echo json_encode($json_return);
                            return false;
                        }

                        //end alter fields

                        $workspaceID = $db->insert("tb_workspace", $insert);


                        $insert['id'] = $workspaceID;
                        if ($save_type == "2" && ENABLE_STAGING == "1" && $workspace_version == "2") {
                            $dev_forms_arr = array("form_id" => $workspaceID,
                                "parent_id" => $oldFormID,
                                "author" => $auth['id']);
                            $db->insert("tbdev_forms", $dev_forms_arr);
                        }
                        /*
                          $db = database class
                          $array = data to insert
                          $data = data of form
                          $access_type = user or admin
                          $insert_type = 1 for form and 2 form form_category
                          $action_type = 1 = author, 2 = view, 3 = user, 4 = admin,
                          5 = customized print, 6 = delete access

                          setUsersMigration($db,$array,$data,$access_type,$insert_type,$action_type)
                         */

                        if ($category_id != "0") {
                            //author
                            setUsersMigration($db, json_decode($json["form_authors"]), $insert, 2, 1, 1);
                            //viewer
                            setUsersMigration($db, json_decode($json["form_viewers"]), $insert, 2, 1, 2);
                            //users
                            setUsersMigration($db, json_decode($json["form_users"]), $insert, 2, 1, 3);
                            //admin
                            setUsersMigration($db, json_decode($jsonAdmin), $insert, 1, 1, 4);
                        }

                        //customize print author
                        setUsersMigration($db, json_decode($json["customized-print"]), $insert, 2, 1, 5);

                        //delete access
                        if (ENABLE_DELETION == "1") {
                            setUsersMigration($db, json_decode($json["delete-access"]), $insert, 2, 1, 6);
                        }
                        //nav access
                        // setUsersMigration($db, json_decode($json["nav-viewers"]), $insert, 2, 1, 7);
                        //fields data properties
                        foreach ($fields_data as $value) {
                            $db->insert("tbfields", array("field_name" => $value['data_field_name'],
                                "form_id" => $workspaceID,
                                "field_type" => $value['field_type'],
                                "field_input_type" => $value['data_field_input_type'],
                                "formula_type" => $value['data_field_default_type'],
                                "formula" => $value['data_field_formula'],
                                "visibility_formula" => $value['data_field_visibility_formula'],
                                "data_field_label" => $value['data_field_label'],
                                "readonly" => ($value['readonly'] == "readonly") ? 1 : 0,
                                "multiple_values" => ($value['data_multiple_values']),
                                "other_attributes" => $value['other_attributes'],
                                "object_id" => $value['object_id'],
                                "input_validation" => $value['input_validation']
                            ));
                        }

                        // added by joshua reyes 03/07/2016 [Button Visibility]
                        foreach ($button_properties_data as $value_bvd) {
                            $db->insert("tb_action_button_properties", array(
                                "form_id" => $workspaceID,
                                "button_id" => $value_bvd['buttonID'],
                                "button_name" => $value_bvd['buttonName'],
                                "visibility_formula" => $value_bvd['buttonVisibilityFormula']));
                        }

                        // Create folder for the postImage

                        $path = "images/formLogo";
                        $id_encrypt = md5(md5($workspaceID));
                        $dir = $path . "/" . $id_encrypt;
                        if (!is_dir($filename)) {
                            $upload->createFolder($dir);
                        }

                        // Move files to the directory
                        $copy_foldername = md5(md5($auth['id']));
                        $from = "images/formLogo/temporaray_files/" . $copy_foldername;
                        $postFiles = $upload->getAllfiles_fromDirectory($from);

                        foreach ($postFiles as $file) {
                            copy($from . '/' . $file, $dir . '/' . $file);
                        }
                        $upload->unlinkRecursive($from, "yes");
                        rmdir($from);


                        //SYSTEM_LOGS
                        // $messageAttr = array("category_id"=>$category_id,
                        // 					"Title"=>$json['Title'],
                        // 					"jsonAdmin"=>$jsonAdmin,
                        // 					"user_id"=>$user_id
                        // 					);
                        // $action_description = Logs::createMessage("24",$messageAttr);
                        //audit trail
                        $insert_audit_rec = array("user_id" => $auth['id'],
                            "audit_action" => "24",
                            "table_name" => "tb_workspace",
                            "record_id" => $workspaceID,
                            "date" => $date,
                            "ip" => $_SERVER["REMOTE_ADDR"],
                            "is_active" => 1);

                        $audit_log = $db->insert("tbaudit_logs", $insert_audit_rec);

                        $message = "Your form (" . $json['Title'] . ") was successfully created.";
                        $json_return = array("status" => "success",
                            "message" => $message,
                            "saved_form_id" => $workspaceID);
                        echo json_encode($json_return);
                    }


                    if ($save_type == "2") {

                        //get workflow
                        $queryGetWorkflow = "SELECT * FROM tbworkflow WHERE form_id = '" . $oldFormID . "' AND is_active='1'";
                        $getWorkflow = $db->query($queryGetWorkflow, "row");
                        $countActiveWorkflow = $db->query($queryGetWorkflow, "numrows");
                        //insert workflow if there are workflow
                        if ($countActiveWorkflow == 1) {
                            $oldWorkflowID = $getWorkflow['id'];


                            //INSERT WORKFLOW DETAILS
                            $insert = array("title" => $getWorkflow['title'],
                                "form_id" => $workspaceID,
                                "description" => $getWorkflow['description'],
                                "date" => $date,
                                "tbw_json" => $getWorkflow['tbw_json'],
                                "created_by" => $auth['id'],
                                "is_active" => 1
                            );
                            $workflow_id = $db->insert("tbworkflow", $insert);


                            //get workflow objects
                            $getWorkflowObjects = $db->query("SELECT * FROM tbworkflow_objects WHERE workflow_id = '" . $oldWorkflowID . "'", "array");
                            //INSERT WORKFLOW OBJECTS
                            foreach ($getWorkflowObjects as $nodes) {

                                $insert_obects = array("workflow_id" => $workflow_id,
                                    "object_id" => $nodes['object_id'],
                                    "processorType" => $nodes['processorType'],
                                    "processor" => $nodes['processor'],
                                    "buttonStatus" => $nodes['buttonStatus'],
                                    "condition_return" => $nodes['condition_return'],
                                    "status" => $nodes['status'],
                                    "type_rel" => $nodes['type_rel'],
                                    "operator" => $nodes['operator'],
                                    "field" => $nodes['field'],
                                    "field_value" => $nodes['field_value'],
                                    "email" => $nodes['email'],
                                    "email_cancel" => $nodes['email_cancel'],
                                    "sms" => $nodes['sms'],
                                    "fieldEnabled" => $nodes['fieldEnabled'],
                                    "fieldRequired" => $nodes['fieldRequired'],
                                    "fieldHiddenValue" => $nodes['fieldHiddenValue'],
                                    "json" => $nodes['json'],
                                    "noti_message" => $nodes['noti_message']
                                );
                                $db->insert("tbworkflow_objects", $insert_obects);
                            }

                            //get workflow lines
                            $getWorkflowObjects = $db->query("SELECT * FROM tbworkflow_lines WHERE workflow_id = '" . $oldWorkflowID . "'", "array");

                            //INSERT WORKFLOW LINES
                            foreach ($getWorkflowObjects as $lines) {
                                $insert_obects = array("workflow_id" => $workflow_id,
                                    "parent" => $lines['parent'],
                                    "child" => $lines['child'],
                                    "json" => $lines['json'],
                                );
                                $db->insert("tbworkflow_lines", $insert_obects);
                            }
                        }
                    }
                    if ($redis_cache) {
                        $form_record_memcached = array("form_list", "form_count");
                        $nav_memcached = array("leftbar_nav");
                        $deleteMemecachedKeys = array_merge(
                                $form_record_memcached, $nav_memcached
                        );
                        $fs->deleteMemcacheKeys($deleteMemecachedKeys);
                    }
                } else {
                    $formDoc = new Form($db, $formId);
                    //clear cache of request list, calendar list, request_list_count  etc..
                    // $memcache->delete("request_list");
                    // $memcache->delete("picklist");
                    // $memcache->delete("request_list_count");
                    if ($redis_cache) {
                        //deletememcached
                        $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
                        $form_record_memcached = array("form_list", "form_count");
                        $related_info_cached = array("related_info");
                        $nav_memcached = array("leftbar_nav");
                        $starred_memcached = array("starred_list");
                        $request_memcached = array("form_access_" . $formId,
                            "request_details_" . $formId,
                            "request_access_" . $formId, "request_author_access_" . $formId,
                            "workspace_form_details_" . $formId, "form_object_" . $formId);
                        $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");


                        $deleteMemecachedKeys = array_merge(
                                $form_record_memcached, $related_info_cached, $nav_memcached, $starred_memcached, $request_memcached, $myrequest_memcached
                        );

                        $fs->deleteMemcacheKeys($deleteMemecachedKeys);
                        $fs->deleteMemcacheForm($request_record_memcacached, $formId);
                    }


                    
                    $tblname = $formDoc->form_table_name;

                    //alter fields
                    $active_fields = $db->query("SHOW columns from " . $tblname, "array");

                    $active_fields = $fs->pushFieldColumns($active_fields);
                    // $arrayField = split(",", $formDoc->active_fields);
                    $alterSQL = 'ALTER TABLE `' . $tblname . "` ";
                    foreach ($fields_decode as $field) {
                        $fieldName = trim(functions::removePairedBracket($field['fieldName']));
                        $fieldType = $field['fieldType'];
                        //$strSQL.=$fieldName.' '.$fieldType.',';
                        //$dropSQL = 'ALTER TABLE ' . $tblname . ' DROP ' . $fieldName;
                        //$db->create($dropSQL);
                        // $active_fields = split(",", $formDoc->active_fields);
                        $field_index = in_array(strToLower($fieldName), $active_fields);


                        if ($fieldName == "" || in_array($fieldName, $fieldAltered)) {
                            continue;
                        }

                        if ($field_index) {
                            //update field
                            $alterSQL .= ' MODIFY `' . $fieldName . '` ' . $fieldType . ', ';
                        } else {
                            //new field
                            $alterSQL .= ' ADD `' . $fieldName . '` ' . $fieldType . ', ';
                        }

                        //push the fieldnames in array to prevent multiple alter of fields
                        $fieldAltered[] = $fieldName;
                    }
                    //remove space and comma of the sql string


                    $alterSQL = substr($alterSQL, 0, strlen($already) - 2);
                    $create = $db->create($alterSQL);

                    if (!$create) {
                        $message = " There's something wrong in saving of your formbuilder. Please contact your administrator for assistance.";
                        $json_return = array("status" => "error",
                            "message" => $message,
                            "active_fields" => $active_fields);
                        echo json_encode($json_return);
                        return false;
                    }

                    //end alter fields
                    //admin
                    if ($json["form-admin"] == "0") {
                        $jsonAdmin = json_encode(array("users" => array($auth['id'])));
                    } else {
                        $jsonAdmin = json_decode($json["form-admin"], true);
                        // array_push($jsonAdmin['users'], $auth['id']);
                        $jsonAdmin['users'][] = $auth['id'];
                        $jsonAdmin = json_encode($jsonAdmin);
                    }



                    $insert = array("form_name" => $json['Title'],
                        "form_alias" => $json['Alias'],
                        "form_description" => $json['DisplayName'],
                        "form_content" => $workspace_content,
                        "form_buttons" => $json['BtnName'],
                        "form_name_formula" => $json['Aliasformu'],
                        "enable_formula" =>$json['Enableformu'],
                        "formula_is_field" =>$json['Enablefield'],
                        "created_by" => $auth['id'],
                        "updated_by" => $auth['id'],
                        "company_id" => $auth['company_id'],
                        "reference_prefix" => $json['Prefix'],
                        "reference_type" => $json['Type'],
                        "version" => 1,
                        "form_json" => $sanitized_data,
                        // "date_created" => $date,
                        "button_content" => $json['button_content'],
                        "active_fields" => $json["existing_fields"],
                        "date_updated" => $date,
                        "is_active" => 1,
                        "is_active_mobility" => $json["is_active_mobility"],
                        "MobileJsonData" => $json["MobileJsonData"],
                        "MobileContent" => $json["MobileContent"],
                        "form_authors" => $json["form_authors"],
                        "form_viewers" => $json["form_viewers"],
                        "form_admin" => $jsonAdmin,
                        "category_id" => $category_id,
                        "form_display_type" => $form_display_type,
                        "display_import" => $display_import,
                        "my_request_visible" => $myrequest_visibility, //my request visibility
                        "form_display_category" => $form_display_category,
                        "form_display_formula" => $form_display_formula,
//                                                "form_type"=>$workspace_status,
                        "enable_deletion" => $delete_enable,
                        "deletion_type" => $delete_type,
                        "allow_portal" => $allow_portal,
                        "portal_icon" => $portal_icon,
                    );
                    $formCount = $search->getSpecificForms2(" (form_table_name = '" . $tblname . "' OR form_name = '" . $json['Title'] . "') AND w.company_id = '" . $auth['company_id'] . "' AND w.id != $formId AND w.is_delete = 0");
                    // $formCount2 = $search->getSpecificForms2(" form_alias = '" . $json['Alias'] . "') AND w.company_id = '" . $auth['company_id'] . "' AND w.id != $formId AND w.is_delete = 0");
                    // echo count($formCount);
                    // return false;
                    if (count($formCount) > 0) {
                        // $message = "This form is already created, Please change your title.";
                        $message = "The entered form title already exists. Please modify and resave.";
                        $json_return = array("status" => "warning",
                            "message" => $message);
                        echo json_encode($json_return);
                        return false;
                    } 
                    // else if (count($formCount2) > 0) {
                    //     // $message = "This form is already created, Please change your title.";
                    //     $message = "The entered form Alias already exists. Please modify and resave.";
                    //     $json_return = array("status" => "warning",
                    //         "message" => $message);
                    //     echo json_encode($json_return);
                    //     return false;
                    // }
                    /*
                      $db = database class
                      $array = data to insert
                      $data = data of form
                      $access_type = user or admin
                      $insert_type = 1 for form and 2 form form_category
                      $action_type = 1 = author, 2 = view, 3 = user, 4 = admin, 5 = custom print, 6 = delete access, 7 = nav viewers
                      setUsersMigration($db,$array,$data,$access_type,$insert_type,$action_type)
                     */
                    $formAccess = new FormUserAccess($db, $formId);
                    $getFormProperties = $search->getSpecificForms2(" w.id = " . $formId);
                    $formProperies_json = json_decode($getFormProperties[0]['form_json'], true);
                    $formProperies_json_author = $formProperies_json['form_authors'];
                    $formProperies_json_viewers = $formProperies_json['form_viewers'];
                    $formProperies_json_users = $formProperies_json['form_users'];
                    $formProperies_json_customize_author = $formProperies_json['customized-print'];
                    $formProperies_json_nav_viewers = $formProperies_json['nav-viewers'];


                    $formProperies_json_delete_access = $formProperies_json['delete-access'];
                    $formProperies_json_admin = $formProperies_json['form-admin'];

                    $insert['id'] = $formId;
                    if ($category_id == "0") {
                        // //author
                        // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 1));
                        // //viewer
                        // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 2));
                        // //users
                        // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 3));
                        // //admin
                        // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 4));
                        // //customize print author
                        // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 5));

                        $fs->deleteFormUsers($db, $formId,array("1","2","3","4"));
                    } else {

                        if ($_POST['resetUsersFlag'] == "1") {
                            $fs->deleteFormUsers($db, $formId,"all");
                        }
                        //author
                        // if ($formProperies_json_author != $json["form_authors"]) {
                        // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 1));
                        // setUsersMigration($db, json_decode($json["form_authors"]), $insert, 2, 1, 1);
                        $formAccess->insertUser($json['form_json']['form-authors_checked'], 1);
                        $formAccess->deleteUser($json['form_json']['form-authors_unChecked'], 1);
                        // }
                        //viewer
                        // if ($formProperies_json_viewers != $json["form_viewers"]) {
                        // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 2));
                        // setUsersMigration($db, json_decode($json["form_viewers"]), $insert, 2, 1, 2);
                        $formAccess->insertUser($json['form_json']['form-readers_checked'], 2);
                        $formAccess->deleteUser($json['form_json']['form-readers_unChecked'], 2);
                        // }
                        //users
                        // if ($formProperies_json_users != $json["form_users"]) {
                        // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 3));
                        // setUsersMigration($db, json_decode($json["form_users"]), $insert, 2, 1, 3);
                        $formAccess->insertUser($json['form_json']['form-users_checked'], 3);
                        $formAccess->deleteUser($json['form_json']['form-users_unChecked'], 3);
                        // }
                        //admin
                        // if ($formProperies_json_admin != $json["form-admin"]) {
                        $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 4));
                        setUsersMigration($db, json_decode($jsonAdmin), $insert, 1, 1, 4);
                        // }
                    }


                    //customize print author
                    // if ($formProperies_json_customize_author != $json["customized-print"]) {
                    // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 5));
                    // setUsersMigration($db, json_decode($json["customized-print"]), $insert, 2, 1, 5);
                    $formAccess->insertUser($json['form_json']['customized-print_checked'], 5);
                    $formAccess->deleteUser($json['form_json']['customized-print_unChecked'], 5);
                    // }
                    //nav access
                    // if ($formProperies_json_nav_viewers != $json["nav-viewers"]) {
                    // 	$db->delete("tbform_users", array("form_id" => $formId, "action_type" => 7));
                    // 	setUsersMigration($db, json_decode($json["nav-viewers"]), $insert, 2, 1, 7);
                    // }
                    if (ENABLE_DELETION == "1") {
                        //delete access
                        // if ($formProperies_json_delete_access != $json["delete-access"]) {
                        // $db->delete("tbform_users", array("form_id" => $formId, "action_type" => 6));
                        // setUsersMigration($db, json_decode($json["delete-access"]), $insert, 2, 1, 6);
                        $formAccess->insertUser($json['form_json']['delete-access_checked'], 6);
                        $formAccess->deleteUser($json['form_json']['delete-access_unChecked'], 6);
                        // }
                    }



                    /* UPDATE THE FORM */

                    $condition = array("ID" => $formId);
                    $workspaceID = $db->update("tb_workspace", $insert, $condition);






                    //fields data properties
                    $db->delete("tbfields", array("form_id" => $formId));
                    foreach ($fields_data as $value) {
                        $db->insert("tbfields", array("field_name" => $value['data_field_name'],
                            "form_id" => $formId,
                            "field_type" => $value['field_type'],
                            "field_input_type" => $value['data_field_input_type'],
                            "formula_type" => $value['data_field_default_type'],
                            "formula" => $value['data_field_formula'],
                            "visibility_formula" => $value['data_field_visibility_formula'],
                            "data_field_label" => $value['data_field_label'],
                            "readonly" => ($value['readonly'] == "readonly") ? 1 : 0,
                            "multiple_values" => ($value['data_multiple_values']),
                            "other_attributes" => $value['other_attributes'],
                            "object_id" => $value['object_id'],
                            "input_validation" => $value['input_validation']
                        ));
                    }
                    
                    //added by joshua reyes 03/07/2016 [Button Visibility]
                    $db->delete("tb_action_button_properties", array("form_id" => $formId));
                    foreach ($button_properties_data as $value_bvd) {
                        $db->insert("tb_action_button_properties", array(
                            "form_id" => $formId,
                            "button_id" => $value_bvd['buttonID'],
                            "button_name" => $value_bvd['buttonName'],
                            "visibility_formula" => $value_bvd['buttonVisibilityFormula']));
                    }

                    // Create folder for the postImage
                    $path = "images/formLogo";
                    $id_encrypt = md5(md5($formId));
                    $dir = $path . "/" . $id_encrypt;
                    if (!is_dir($filename)) {
                        $upload->createFolder($dir);
                    }

                    // Move files to the directory
                    $copy_foldername = md5(md5($auth['id']));
                    $from = "images/formLogo/temporaray_files/" . $copy_foldername;
                    $postFiles = $upload->getAllfiles_fromDirectory($from);

                    foreach ($postFiles as $file) {
                        copy($from . '/' . $file, $dir . '/' . $file);
                    }
                    $upload->unlinkRecursive($from, "yes");
                    rmdir($from);

                    // $messageAttr = array("category_id"=>$category_id,
                    // 					"Title"=>$json['Title'],
                    // 					"jsonAdmin"=>$jsonAdmin,
                    // 					"user_id"=>$user_id
                    // 					);
                    // $action_description = Logs::createMessage("25",$messageAttr);
                    //audit trail
                    $insert_audit_rec = array("user_id" => $auth['id'],
                        "audit_action" => "25",
                        "table_name" => "tb_workspace",
                        "record_id" => $formId,
                        "date" => $date,
                        "ip" => $_SERVER["REMOTE_ADDR"],
                        "is_active" => 1);

                    $audit_log = $db->insert("tbaudit_logs", $insert_audit_rec);

                    $message = "Your form (" . $json['Title'] . ") was successfully updated.";
                    $json_return = array("status" => "success",
                        "message" => $message,
                        "active_fields" => json_encode($active_fields));
                    echo json_encode($json_return);
                }


                foreach ($reservedFields as $key => $value) {
                    $fieldName = $value;
                    //$strSQL.=$fieldName.' '.$fieldType.',';   
                    if ($fieldName == 'DateCreated' || $fieldName == 'DateUpdated') {
                        $fieldType = " datetime ";
                    } else {
                        $fieldType = " longtext ";
                    }

                    $alterSQL = 'ALTER TABLE `' . $tblname . '` ADD `' . $fieldName . '` ' . $fieldType . ' ';
                    $db->create($alterSQL);
                }


                // Tagging
                if ($json["allow_tag"] == "1") {
                    $db->query("Truncate table 1_tbl_slotform");
                    $json_tags = json_decode($json["tag_id"], true);

                    foreach ($json_tags as $tags) {

                        $insert = array(
                            "tag_id" => $tags['tag_id'],
                            "customer" => $tags['tag_customer'],
                            "width" => $tags['tag_slot_width'],
                            "length" => $tags['tag_length'],
                            "height" => $tags['tag_slot_height'],
                            "referencenumber" => $tags['tag_container_number'],
                            "description" => $tags['tag_description'],
                            "type" => $tags['tag_type'],
                            "class" => $tags['tag_class'],
                            "time_storage" => $tags['tag_time_storage'],
                            "start_storage" => $tags['tag_start_storage'],
                            "slot_height" => $tags['tag_height'],
                            "slot_width" => $tags['tag_width'],
                            "top" => $tags['tag_top'],
                            "left" => $tags['tag_left']);

                        $db->insert("1_tbl_slotform", $insert);
                    }
                }


                //echo $formId;
                //echo "Your form (" . $json['Title'] . ") was successfully saved.";

                break;

            // Get Form  created workspace on the database

            case "getForm":

                $getForm = $db->query("SELECT * FROM tb_workspace WHERE company_id = {$db->escape($auth['company_id'])} AND is_delete = 0", "array");

                foreach ($getForm as $value) {
                    $form[] = array(
                        "form_name" => $value['form_name'],
                        "form_id" => $value['id']
                    );
                }
                echo json_encode($form);
                break;

            // Get Form Category

            case "getCategory":
                $getCategory = $db->query("SELECT * FROM tbform_category WHERE company_id = {$db->escape($auth['company_id'])} AND is_delete = 0", "array");

                foreach ($getCategory as $category) {
                    $categ[] = array(
                        "category_name" => $category['category_name'],
                        "category_id" => $category['id']
                    );
                }
                echo json_encode($categ);
                break;
            case "getFormList":
                $search = new Search();
                $getForms = $search->getForms();
                echo json_encode($getForms);
                break;
            case "form_reflectDesign":
                $staging_id = $_POST['staging_id'];
                $stagingForm = new StagingForm($staging_id);
                $stagingForm->setProperties();
                break;
            case "form_backup":
                break;
        }
    }
} else {
    echo "false";
}
?>