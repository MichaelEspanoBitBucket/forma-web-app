<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php
if ($_GET['type'] == 3) {
    $auth = Auth::getAuth('current_user');
    ?>
    <script type="text/javascript" src="/js/library/jqueryLib/jquery-1.10.0.min.js"></script>
    <script type="text/javascript" src="/js/library/jqueryLib/jquery-ui.js"></script>
    <!-- <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/modules/exporting.js"></script> -->
    <script src="/js/functions/charts/highcharts.js"></script>
    <script src="/js/functions/charts/exporting.js"></script>
    <!-- start socket  -->
    <script src="/js/functions/sockets/initializer.js"></script>
    <script src="/js/functions/sockets/notification.js"></script>
    <!-- end socket-->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">

    <div id="current-user-info" data-socket-active="<?php echo SOCKET_ACTIVE ?>" data-socket-server="<?php echo SOCKET_SERVER ?>" style="display: none;">
        <!--General Info of Users-->
        <div id="current-user-id"><?php echo $auth['id'] ?></div>       
        <div id="current-user-company-id"><?php echo $auth['company_id'] ?></div>       
        <div id="current-user-display-name"><?php echo $auth['display_name'] ?></div>
        <div id="current-user-first-name"><?php echo $auth['first_name'] ?></div>
        <div id="current-user-last-name"><?php echo $auth['last_name'] ?></div>
        <div id="current-user-profile-link"><?php echo $current_page = 'http://' . $_SERVER['HTTP_HOST'] . '/user_view/' . $auth['last_name'] . "_" . $auth['first_name']; ?></div>    
        <div id="current-user-profile-image"><?php echo $companyUserImg ?></div>

        <!--Socket Info-->
        <div id="current-selected-message"></div>
    </div>
    <?php
}


$auth = Auth::getAuth('current_user');
if (!Auth::hasAuth('current_user')) {
    return false;
}
$db = new Database;
$fs = new functions;
$search = new Search();

/* added by aaron for page security */
if (isset($_GET['form_id'])) {
    $id = $_GET['form_id'];
    $obj = array("condition" => " AND w.id = " . $id . " ");
    $obj = json_decode(json_encode($obj), true);
    $result = $search->getForms("", 0, $obj);
    // print_r($result);
    $countForm = count($result);
    if ($countForm == 0) {
        header("location:/user_view/");
    }
}

$date = $fs->currentDateTime();
$application_id = $_COOKIE['application'];

$id = $_GET['id'];
$formId = $_GET['form_id'];
$keyword = $_GET['keyword'];
$type = $_GET['type'];
$chartFields = $_GET['chartFields'];
$chartDataType = $_GET['chartDataType'];

$reportDoc = new Report($db, $id);
$formDoc = new Form($db, $formId);

$parameters = json_decode($reportDoc->parameters, true);

$whereClause = '';
$parameters_array = array();
$formula_array = array();

foreach ($parameters as $arr) {
    if ($arr["RadioField"] != "computed" && $arr["Type"] != "computed") {
        if ($whereClause != '') {
            $whereClause .= ' AND ';
        }

        if ($parameters_array[$arr['Field']] == '') {
            $parameters_array[$arr['Field']] = '0';
        } else {
            $parameters_array[$arr['Field']] = $parameters_array[$arr['Field']] + 1;
        }
        foreach ($arr as $key => $value) {
            if ($key == 'Field') {
                $field_type = $db->query("SELECT field_type FROM tbfields WHERE form_id={$formId} AND field_name={$db->escape($value)}", "row");
                $field_value = $_GET[$value][$parameters_array[$arr['Field']]];
            }

            if ($key != 'Column' && $key != 'RadioField' && $key != 'Type') {

                if ($key == 'Operator' && $value == '%') {
                    $whereClause .= ' LIKE ';
                } else if ($key == 'Operator' && $value == '!%') {
                    $whereClause .= ' NOT LIKE ';
                } else {
                    if ($value != "" && $key != 'Operator') {
                        $whereClause .= " IFNULL(" . $value . ", '')";
                    } else {
                        $whereClause .= $value;
                    }
                }
            }

            if ($key == 'Operator') {

                if ($value == '%') {
                    $whereClause .= " '%" . $field_value . "%' ";
                } else if ($value == '!%') {
                    $whereClause .= " '%" . $field_value . "%' ";
                } else {
                    if ($field_type["field_type"] == "datepicker") {
                        if (strtotime($field_value)) {
                            $whereClause .= "STR_TO_DATE('" . $field_value . "', '%Y-%m-%d')";
                        } else {
                            $whereClause .= "asdsadasdsa";
                        }
                    } else {
                        $whereClause .= " '" . $field_value . "' ";
                    }
                }
            }
        }
    } else {
        switch ($arr["Operator"]) {
            case "=":
                $computed_operator = "==";
                break;
            case "<=":
                $computed_operator = "<=";
                break;
            case ">=":
                $computed_operator = ">=";
                break;
            case ">":
                $computed_operator = ">";
                break;
            case "<":
                $computed_operator = "<";
                break;
            case "!=":
                $computed_operator = "!=";
                break;
            case "%":
                $computed_operator = "";
                break;
            case "!%":
                $computed_operator = "";
                break;
        }

        $str_formula = $arr["FieldFormula"] . $computed_operator . $arr["TypeFormula"];
        array_push($formula_array, $str_formula);
    }
}

//$columns = explode(",", $reportDoc->columns);

$columns = json_decode($reportDoc->columns, true);
//$strSQL = 'SELECT * FROM ' . $formDoc->form_table_name;
$strSQL = 'SELECT ID, TrackNo, Requestor, Processor, DateCreated, DateUpdated, ' . $formDoc->active_fields . ', Repeater_Data FROM ' . $formDoc->form_table_name;
//$strSQL = "SELECT ";
//foreach ($columns as $column) {
//    $strSQL.="'".$column['Value'] ."', "; 
//}
//$strSQL = substr($strSQL, 0, strlen($strSQL) - 2);
//
//$strSQL.=' FROM ' . $formDoc->form_table_name;
//var_dump($whereClause);
if ($whereClause != '') {
    $strSQL .= ' WHERE ' . $whereClause;
}

$sort_count = 0;
foreach ($columns as $column) {
    $colum_name = ltrim($column['Value'], '@');
    $sort = $column['Sort'];
    if ($sort == "ascending") {
        $sort_count++;
        if ($sort_count == 1) {
            $strSQL.= " ORDER BY `" . $colum_name . "` ASC ";
            $objSearch['column-sort'] = $colum_name;
            $_POST['column-sort-type'] = "ASC";
        } else {
            $strSQL.= ", " . $colum_name . " ASC ";
        }
    }

    if ($sort == 'descending') {
        $sort_count++;
        if ($sort_count == 1) {
            $strSQL.= " ORDER BY `" . $colum_name . "` DESC ";
            $objSearch['column-sort'] = $colum_name;
            $_POST['column-sort-type'] = "DESC";
        } else {
            $strSQL.= ", `" . $colum_name . "` DESC ";
        }
    }
}

if ($whereClause != "") {
    $objSearch["multi_search"] = " and  " . $whereClause;
}

if ($id != "14" && $formId != "387") {
    $search = new Search();
    $result = $search->getManyRequestV2("", 0, $formDoc->id, "", "", $objSearch, true);
}

//$result = $db->query($strSQL, "array");

$tableClass = "";
if ($type == 2) {
    $filename = $reportDoc->title . '_' . $date . '.xls';
    $filename = str_replace(" ", "_", $filename);
    header('Content-type: application/ms-excel');
    header('Content-Disposition: attachment; filename=' . $filename);
}


$ret = '<title>' . $reportDoc->title . '</title>';
$ret .= '<body style="font-family:Arial;font-size:12px">';
$ret .= '<h2 class="reportTitle">' . $reportDoc->title . '</h2>';
$ret .= '<label style="font-weight:Bold">Generated By:</label> ' . $auth['display_name'] . '<br/>';
$ret .= '<label style="font-weight:Bold">Date Generated:</label> ' . $date . '<br/>';

$parameters_array = array();
foreach ($parameters as $arr) {
//    var_dump($arr);
    if ($parameters_array[$arr['Field']] == '') {
        $parameters_array[$arr['Field']] = '0';
    } else {
        $parameters_array[$arr['Field']] = $parameters_array[$arr['Field']] + 1;
    }
    $ret .= '<label style="font-weight:Bold">' . $arr["Column"] . ':</label> ' . $_GET[$arr["Field"]][$parameters_array[$arr['Field']]] . '<br/>';
}

if ($type != 3 && $type != 4) {
    if ($id == "14" && $formId == "387") {
        $start_date = $_GET["DatePresent"][0];
        $end_date = $_GET["DatePresent"][1];
        $company = $_GET["company"][0];
        $timesheet_query = "SELECT 
	MAIN.ID,
	MAIN.TrackNo,
	MAIN.EmpID as 'Employee Number',
	MAIN.lname as 'Last Name',
	MAIN.fname as 'First Name',
	MAIN.TotalHoursWorked as 'Total Hours Worked',
	ROUND(SUM(CASE WHEN LR.leaveType = 'Vacation Leave' THEN LR.NodaysLeave ELSE 0 END),2) AS 'Vacation Leave ( Day/s )',
	ROUND(SUM(CASE WHEN LR.leaveType = 'Sick Leave' THEN LR.NodaysLeave ELSE 0 END),2) AS 'Sick Leave ( Day/s )',
	ROUND(SUM(CASE WHEN LR.leaveType = 'Emergency Leave' THEN LR.NodaysLeave ELSE 0 END),2) AS 'Emergency Leave ( Day/s )',
	ROUND(SUM(CASE WHEN LR.leaveType = 'Leave without Pay' THEN LR.NodaysLeave ELSE 0 END),2) AS 'Leave without Pay ( Day/s )',
	IFNULL(ROUND(SUM(DA.totaldays),2),0) AS 'Suspension (Day/s )',
	'0.00' AS 'Absences ( Day/s )',
	MAIN.TotalLate AS 'No. Of Late',
	MAIN.TotalUndertime AS 'Undertime',
	MAIN.TotalHoursOT AS 'Regular Overtime',
	MAIN.TotalWrdHoursOT AS 'Restday Overtime',
	MAIN.TotalHoursOTND30 AS 'Overtime Night Differential',
	MAIN.TotalRHOT AS 'Overtime Regular Holiday',
	MAIN.TotalSHOT AS 'Overtime Special Holiday'
FROM (SELECT 
    MAIN.ID,
    MAIN.TrackNo,
    MAIN.EmpID,
    MAIN.lname,
    MAIN.fname,
    IFNULL(ROUND(SUM(TotalHoursWorked),2), 0) AS TotalHoursWorked,
    IFNULL(ROUND(SUM(lateCounter),2), 0) AS TotalLate,
    IFNULL(ROUND(SUM(undetimeCounter),2), 0) as TotalUndertime,
	IFNULL(ROUND(SUM(RegOTHours),2), 0) as TotalHoursOT,
	IFNULL(ROUND(SUM(wrdOTHours),2), 0) as TotalWrdHoursOT,
	IFNULL(ROUND(SUM(RegOTND30),2), 0) as TotalHoursOTND30,
	IFNULL(ROUND(SUM(RHOT),2), 0) as TotalRHOT,
	IFNULL(ROUND(SUM(SHOT),2), 0) as TotalSHOT,
	MAIN.Company,
	MAIN.DatePresent
FROM
    15_tbl_timesheet MAIN
WHERE
    MAIN.company LIKE '%{$company}%'
        AND (MAIN.DatePresent BETWEEN {$db->escape($start_date)} AND {$db->escape($end_date)})
GROUP BY MAIN.EmpID) MAIN
LEFT JOIN 20_tbl_leaverequest LR
ON LR.employeenumber = MAIN.EmpID AND LR.Status = 'Approved' AND LR.leaveFrom BETWEEN {$db->escape($start_date)} AND {$db->escape($end_date)}
LEFT JOIN 20_tbl_dacases DA
ON DA.EmployeeID = MAIN.EmpID AND (suspensiondate BETWEEN {$db->escape($start_date)} AND {$db->escape($end_date)}) AND (cleanslaatedate BETWEEN {$db->escape($start_date)} AND {$db->escape($end_date)})
GROUP BY MAIN.EmpID
ORDER BY MAIN.lname 
";

//        print_r($timesheet_query);
        $timesheet_requests = $db->query($timesheet_query);
//        var_dump(count($timesheet_requests));

        $ret .= '<table id="' . $tableClass . '" style="margin-top:5px" cellpadding=5 cellspacing=0>';
        $ret .= '<tr>';
        foreach ($columns as $column) {
            $ret .= '<th style="text-align:left;font-size:12px;background: #4590D2;border:1px Solid #ccc;color:#fff;width:' . $column['Width'] . '">' . $column['Name'] . '</th>';
        }
        $ret .= '</tr>';

        foreach ($timesheet_requests as $row) {
            $ret .= '<tr>';
            foreach ($row as $column_name => $column) {
                if ($column_name == "ID") {
                    $time_sheet_id = $column;
                }
                if ($column_name == "TrackNo") {
                    $time_sheet_track_no = $column;
                }
                if ($column_name == "Employee Number") {
                    $ret .= '<td style="font-size:12px;border:1px Solid #ccc;' . $alignment . '"><a href="/user_view/workspace?view_type=request&formID=' . $formDoc->id . '&requestID=' . $time_sheet_id . '&trackNo=' . $time_sheet_track_no . '">' . $column . '</td>';
                } else {
                    if ($column_name != "ID" && $column_name != "TrackNo") {
                        $ret .= '<td style="font-size:12px;border:1px Solid #ccc;' . $alignment . '">' . $column . '</td>';
                    }
                }
            }
            $ret .= '</tr>';
        }
//        echo "<i>" . count($timesheet_requests) . " document/s found.</i>";
        echo $ret;
        return;
    }
    $ret .= '<table id="' . $tableClass . '" style="margin-top:5px" cellpadding=5 cellspacing=0>';
    $ret .= '<tr>';

    foreach ($columns as $column) {
        $ret .= '<th style="text-align:left;font-size:12px;background: #4590D2;border:1px Solid #ccc;color:#fff;width:' . $column['Width'] . '">' . $column['Name'] . '</th>';
    }
    $ret .= '</tr>';

    $tmp_category = array();
    $pdf_data = array();
    $pdf_data_row = array();
    $total = array();
    $total_index = 0;
    $average_index = 0;
    foreach ($result as $row) {
        //$ret .= '<tr>';
        $con = true;

        if (count($formula_array) > 0) {
            foreach ($formula_array as $str_formula) {
                $formulaC = new Formula($str_formula);
                $formulaC->DataFormSource[0] = $row;
//                var_dump($formulaC->evaluate());
//                if ($formulaC->evaluate() == false) {
//                    $con = false;
//                    break;
//                }
            }
        }



        if ($con == true) {
            $columnIndex = 0;
            foreach ($columns as $column) {
                if ($column['ValueType'] == 'static') {
                    $column_value = $column['Value'];
//                    if ($column['Value'] == "Requestor") {
//                        $column_value = $row["requestor_display_name"];
//                    } else {
//                        $column_value = nl2br($row[$column['Value']]);
//                    }
                } else if ($column['ValueType'] == 'field') {
                    $column_value = $row[$column['Value']];
                } else {
                    $formulaDoc = new Formula($column['Value']);
                    $formulaDoc->DataFormSource[0] = $row;

                    $column_value = $formulaDoc->evaluate();
                }

//            var_dump($column['Group']);

                if ($column['Group'] == "categorized") {
                    if (!$tmp_category[$column['Name']]) {
                        $tmp_category[$column['Name']] = $column_value;
                    } else {
                        if ($tmp_category[$column['Name']] != $column_value) {
                            $tmp_category[$column['Name']] = $column_value;
                        } else {
                            $column_value = "";
                            continue 2;
                        }
                    }
                }

//            var_dump($column['Name']);
//            $fieldObject = functions::getFields("WHERE form_id={$formId} AND field_name={$db->escape($column['Name'])}");
//            
                $alignment = 'text-align:left';

                //show total
                if ($column['ShowTotal'] == 'true') {
                    $total_index++;
                    $total[$column['Name']] = $total[$column['Name']] + $column_value;
                }

                if ($column['ShowAverage'] == 'true') {
                    $average_index++;
                }

                //if ($column['Format'] == 'Currency') {
                //    $column_value = number_format($column_value, 2);
                //    $alignment = 'text-align:right';
                //}
                //
            //if ($column['Format'] == 'Number') {
                //    $alignment = 'text-align:right';
                //}



                if ($columnIndex == 0) {
                    //    $ret .= '<td style="font-size:12px;border:1px Solid #ccc;' . $alignment . '"><a href="/workspace?view_type=request&formID=' . $formDoc->id . '&requestID=' . $row['ID'] . '&trackNo=' . $row['TrackNo'] . '">' . $column_value . '</td>';
                } else {
                    //    $ret .= '<td style="font-size:12px;border:1px Solid #ccc;' . $alignment . '">' . $column_value . '</td>';
                }

                $pdf_data_row[$column['Name']] = $column_value;

                $columnIndex++;
            }

            $pdf_data_row["linkID"] = $row["ID"];
            $pdf_data_row["linkTrackNo"] = $row["TrackNo"];
            array_push($pdf_data, $pdf_data_row);
        }

        //$ret .= '</tr>';
    }

    $pdf_data = functions::my_array_unique($pdf_data);


    foreach ($pdf_data as $pdf_row) {
        $ret .= '<tr>';
        $columnIndex = 0;

        foreach ($pdf_row as $key => $column_value) {
            $alignment = '';
            if (is_numeric($column_value)) {
                $column_value = number_format($column_value, 2);
                $alignment = 'text-align:right';
            }
            if ($key != "linkID" && $key != "linkTrackNo") {
                if ($columnIndex == 0) {
                    $ret .= '<td style="font-size:12px;border:1px Solid #ccc;' . $alignment . '"><a href="/user_view/workspace?view_type=request&formID=' . $formDoc->id . '&requestID=' . $pdf_row['linkID'] . '&trackNo=' . $pdf_row['linkTrackNo'] . '">' . $column_value . '</td>';
                } else {
                    $ret .= '<td style="font-size:12px;border:1px Solid #ccc;' . $alignment . '">' . $column_value . '</td>';
                }
            }

            $columnIndex++;
        }
        $ret .= '</tr>';
    }

    $total_Average = $total;
    //show total
    if ($total_index != 0) {
        $ret .= '<tr>';
        foreach ($columns as $column) {
            //show total

            if ($column['Format'] == 'Currency') {
                $total[$column["Name"]] = number_format($total[$column["Name"]], 2);
            }

            if ($column['ShowTotal'] == 'true') {
                $ret .= '<td style="font-size:12px;border:1px Solid #ccc;text-align:right"> <b>Total: </b>' . $total[$column["Name"]] . '</td>';
            } else {
                $ret .= '<td style="font-size:12px;border:1px Solid #ccc;text-align:right"></td>';
            }
        }
        $ret .= '</tr>';
    }

    //show average
    if ($average_index != 0) {
        $ret .= '<tr>';
        foreach ($columns as $column) {
            //show average
            if ($column['ShowAverage'] == 'true') {
                $total_Average[$column['Name']] = $total_Average[$column['Name']] / count($result);
                if ($column['Format'] == 'Currency') {
                    $total_Average[$column['Name']] = number_format($total_Average[$column['Name']], 2);
                }

                $ret .= '<td style="font-size:12px;border:1px Solid #ccc;text-align:right"> <b>Average: </b>' . $total_Average[$column['Name']] . '</td>';
            } else {
                $ret .= '<td style="font-size:12px;border:1px Solid #ccc;text-align:right"></td>';
            }
        }
        $ret .= '</tr>';
    }

    $ret .= '</table>';
    $ret .= '</body>';

//    if ($type != 4 && $type != 3) {
//        //create pdf
//        ob_end_clean();
//        header("Content-Encoding: None", true);
//        $pdfDoc = new Report_PDF();
//        $pdfDoc->AliasNbPages();
//        $pdfDoc->AddPage('L');
//        $pdfDoc->setHeader('', $reportDoc->title);
//        $pdfDoc->setContent($columns, $pdf_data);
//
//
//        $pdfDoc->Output();
//        return;
//    }
//    $tmp_category = array();
//    $column_ctr = 0;
//    foreach ($result as $row) {
//
//        do {
//
//            
//            $column_name = $columns[$column_ctr]['Name'];
//            $column_group = $columns[$column_ctr]['Group'];
//            $column_ctr++;
//        } while ($column_group == 'categorized');
//    }
}

if ($type == 4) {
    $filename = $reportDoc->title . '_' . $date . '.csv';
    $filename = str_replace(" ", "_", $filename);
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $filename);

    // create a file pointer connected to the output stream
    $output = fopen('php://output', 'w');
    //filter values
    $add_row_arr = array();
    foreach ($columns as $key => $column) {
        $add_row_arr[$key] = $column["Name"];
    }

    fputcsv($output, $add_row_arr);

    $tmp_category = array();
    $pdf_data = array();
    $pdf_data_row = array();
    $total = array();
    $total_index = 0;
    $average_index = 0;

    foreach ($result as $row) {
        //$ret .= '<tr>';
        $formulaDoc = new Formula();
        $formulaDoc->DataFormSource[0] = $row;


        $con = true;

        if (count($formula_array) > 0) {
            foreach ($formula_array as $str_formula) {
                $formulaC = new Formula($str_formula);
                $formulaC->DataFormSource[0] = $row;
//                var_dump($formulaC->evaluate());
//                if ($formulaC->evaluate() == false) {
//                    $con = false;
//                    break;
//                }
            }
        }



        if ($con == true) {
            $columnIndex = 0;
//            $add_row_arr[0] = "jewel";
//            $add_row_arr[1] = "jewel";
//            $add_row_arr[2] = "jewel";
//            fputcsv($output, $add_row_arr);
            foreach ($columns as $column) {
                if ($column['ValueType'] == 'static') {
                    $column_value = $column['Value'];
                } else if ($column['ValueType'] == 'field') {
                    $column_value = $row[$column['Value']];
                } else {
                    $formulaDoc->MyFormula = $column['Value'];
                    $column_value = $formulaDoc->evaluate();
                }

//            var_dump($column['Group']);

                if ($column['Group'] == "categorized") {
                    if (!$tmp_category[$column['Name']]) {
                        $tmp_category[$column['Name']] = $column_value;
                    } else {
                        if ($tmp_category[$column['Name']] != $column_value) {
                            $tmp_category[$column['Name']] = $column_value;
                        } else {
                            $column_value = "";
//                        continue 2;
                        }
                    }
                }

//            var_dump($column['Name']);
//            $fieldObject = functions::getFields("WHERE form_id={$formId} AND field_name={$db->escape($column['Name'])}");
//            
                $alignment = 'text-align:left';

                //show total
                if ($column['ShowTotal'] == 'true') {
                    $total_index++;
                    $total[$column['Name']] = $total[$column['Name']] + $column_value;
                }

                if ($column['ShowAverage'] == 'true') {
                    $average_index++;
                }

                //if ($column['Format'] == 'Currency') {
                //    $column_value = number_format($column_value, 2);
                //    $alignment = 'text-align:right';
                //}
                //
            //if ($column['Format'] == 'Number') {
                //    $alignment = 'text-align:right';
                //}



                if ($columnIndex == 0) {
                    //    $ret .= '<td style="font-size:12px;border:1px Solid #ccc;' . $alignment . '"><a href="/workspace?view_type=request&formID=' . $formDoc->id . '&requestID=' . $row['ID'] . '&trackNo=' . $row['TrackNo'] . '">' . $column_value . '</td>';
                } else {
                    //    $ret .= '<td style="font-size:12px;border:1px Solid #ccc;' . $alignment . '">' . $column_value . '</td>';
                }

                $pdf_data_row[$column['Name']] = $column_value;

                $columnIndex++;
            }



            $pdf_data_row["linkID"] = $row["ID"];
            $pdf_data_row["linkTrackNo"] = $row["TrackNo"];


            array_push($pdf_data, $pdf_data_row);
        }

        //$ret .= '</tr>';
    }


    $pdf_data = functions::my_array_unique($pdf_data);
    foreach ($pdf_data as $pdf_row) {
        $columnIndex = 0;
        $add_row_arr = array();
        foreach ($pdf_row as $key => $column_value) {
            if (is_numeric($column_value)) {
                $column_value = number_format($column_value, 2);
            }
            if ($key != "linkID" && $key != "linkTrackNo") {
                $add_row_arr[$columnIndex] = $column_value;
            }
            $columnIndex++;
        }
        fputcsv($output, $add_row_arr);
    }

//    foreach ($result as $row) {
//        $add_row_arr = array();
//        foreach ($row as $key => $value) {
//            $field_type = functions::getFields("WHERE form_id={$formId} AND field_name={$db->escape($key)}");
//            if ($field_type[0]->type == "attachment_on_request") {
//                $att_file_name = explode("/", $value);
//                $add_row_arr[$key] = $att_file_name[4];
//            } else {
//                $add_row_arr[$key] = $value;
//            }
//        }
//
//        fputcsv($output, $add_row_arr);
//    }
    fwrite($file, "\r\n");
    fclose($output);
    return;
} else {
    echo $ret;
}

// for chart
if ($type == 3) {
    echo '<input type="text" style="display:none" class="formIDForSocket" value="' . $formId . '">';
    echo '<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>';
    echo '<input type="text" style="display:none" id="chart-type" value="' . $_GET['chartType'] . '"/>';
    echo '<input type="text" style="display:none" id="series" value="' . $_GET['series'] . '"/>';
    echo '<input type="text" style="display:none" id="yaxis" value="' . $_GET['yaxis'] . '"/>';
    echo '<input type="text" style="display:none" id="chartDataType" value="' . $chartDataType . '"/>';
}

$symbol_data = json_decode($reportDoc->symbol, true);
$whole_chart_data = array();

if ($chartDataType == 1) {
    $active_fields = split(",", $_GET['chartFields']);

    foreach ($active_fields as $field) {
        $values = $db->query("SELECT DISTINCT(" . $field . ") as Field_Value FROM " . $formDoc->form_table_name . " WHERE " . $whereClause, "array");
        $value_row = array();

        foreach ($values as $key => $value) {
            $isMultiple = strpos($value["Field_Value"], '|^|');

            if ($isMultiple) {

                $field_value_arr = explode('|^|', $value["Field_Value"]);

                foreach ($field_value_arr as $field_value_row) {
                    $valueCount = $db->query("SELECT COUNT(*) as ValueCount  FROM " . $formDoc->form_table_name . " WHERE " . $field . " LIKE '%" . $field_value_row . "%' AND " . $whereClause, "row");

                    array_push($value_row, array("FieldValue" => $field_value_row,
                        "Count" => $valueCount["ValueCount"]));
                }
            } else {
                if ($field == "Requestor" || $field == "Processor") {
                    $valueCount = $db->query("SELECT COUNT(*) as ValueCount  FROM " . $formDoc->form_table_name . " WHERE " . $field . " = {$db->escape($value["Field_Value"])} AND " . $whereClause, "row");

                    $personDoc = new Person($db, $value["Field_Value"]);
                    array_push($value_row, array("FieldValue" => $personDoc->display_name,
                        "Count" => $valueCount["ValueCount"]));
                } else {
                    $valueCount = $db->query("SELECT COUNT(*) as ValueCount  FROM " . $formDoc->form_table_name . " WHERE " . $field . " LIKE '%" . $value["Field_Value"] . "%' AND " . $whereClause, "row");

                    array_push($value_row, array("FieldValue" => $value["Field_Value"],
                        "Count" => $valueCount["ValueCount"]));
                }
            }
        }

        array_push($whole_chart_data, array("Column_Name" => $field,
            "Data" => $value_row));
    }
} else {
    $series = $_GET['series'];
    $versus = $_GET['versus'];

//    $values = $db->query("SELECT (" . $versus . ") as Versus FROM " . $formDoc->form_table_name . " WHERE " . $whereClause, "array");
    $plotbands_arr = json_decode($reportDoc->plotbands, true);

    $plot_bands_ds = array();
    foreach ($parameters as $key => $value) {
        $plot_bands_ds[$value["Field"]] = implode(",", $_GET[$value["Field"]]);
    }

    foreach ($result as $request) {
        $withSymbol = false;

        if ($series == "Requestor" || $series == "Processor") {
            $personDoc = new Person($db, $request[$series]);
            $field_value = $personDoc->display_name;
        } else {
            $field_value = $request[$series];
        }

        foreach ($plotbands_arr as $key => $plot) {
            if ($plot["FromType"] == 'static') {
                $from = $plot["From"];
            } else {
                $formulaPlotDoc = new Formula($plot["From"]);
                $formulaPlotDoc->DataFormSource[0] = $plot_bands_ds;
                $from = $formulaPlotDoc->evaluate();
            }

            if ($plot["ToType"] == 'static') {
                $to = $plot["To"];
            } else {
                $formulaPlotDoc = new Formula($plot["To"]);
                $formulaPlotDoc->DataFormSource[0] = $plot_bands_ds;
                $to = $formulaPlotDoc->evaluate();
            }

            // var_dump($field_value . " >= " . $from);
            // var_dump($field_value . " <= " . $to);
            // var_dump($plot["Name"]);
            // var_dump($plot["ShowSymbol"]);

            if ($plot["ShowSymbol"] == "true" && $field_value >= $from && $field_value <= $to) {
                $withSymbol = true;
                // var_dump("blink");
            }
            // echo "==================";
        }


        $value_row = array();
        array_push($value_row, array("FieldValue" => round($field_value, 2),
            "Count" => 100,
            "WithSymbol" => $withSymbol));

        if ($versus == 'Requestor' || $versus == 'Processor') {
            $personDoc = new Person($db, $request[$versus]);
            array_push($whole_chart_data, array("Column_Name" => $personDoc->display_name,
                "Data" => $value_row));
        } else {
            array_push($whole_chart_data, array("Column_Name" => $value["Versus"],
                "Data" => $value_row));
        }
    }

//    print_r(json_encode($whole_chart_data));
//    foreach ($values as $value) {
//        // $seriesArr = $db->query("SELECT COUNT(DISTINCT {$series}) as Series, {$series}  FROM " . $formDoc->form_table_name . " WHERE " . $versus . " = {$db->escape($value["Versus"])} AND " . $whereClause, "array");
//        $seriesArr = $db->query("SELECT main.{$series} as Series, (
//SELECT COUNT(item.{$series}) FROM " . $formDoc->form_table_name . " item WHERE item.{$series} = main.{$series} AND item.{$versus} = main.{$versus}
//) as Count, main.* FROM " . $formDoc->form_table_name . " main WHERE {$versus} = {$db->escape($value["Versus"])} AND " . $whereClause . " GROUP BY main.{$series}", "array");
//
//
//
//
//        foreach ($seriesArr as $seriesRow) {
////            var_dump($seriesRow);
//            $value_row = array();
//            // if ($symbol_data["ValueType"] == "computed") {
//            // $formulaDoc = new Formula($symbol_data["Value"]);
//            // $formulaDoc->DataFormSource[0] = $seriesRow;
//            // $withSymbol = $formulaDoc->evaluate();
//            // } else {
//            // $withSymbol = true;
//            // }
//            // var_dump($withSymbol);
//            // $withSymbol = false;
//
//            if ($series == 'Requestor' || $series == 'Processor') {
//                $personDoc = new Person($db, $seriesRow["Series"]);
//                array_push($value_row, array("FieldValue" => $personDoc->display_name,
//                    "Count" => $seriesRow["Count"],
//                    "WithSymbol" => $withSymbol));
//            } else {
//                array_push($value_row, array("FieldValue" => $seriesRow["Series"],
//                    "Count" => $seriesRow["Count"],
//                    "WithSymbol" => $withSymbol));
//            }
//
//            if ($versus == 'Requestor' || $versus == 'Processor') {
//                $personDoc = new Person($db, $value["Versus"]);
//                array_push($whole_chart_data, array("Column_Name" => $personDoc->display_name,
//                    "Data" => $value_row));
//            } else {
//                array_push($whole_chart_data, array("Column_Name" => $value["Versus"],
//                    "Data" => $value_row));
//            }
//        }
//
//        //merge duplicates
////        $field_value = 0;
////        $field_count = 0;
////        $filed_symbol = 0;
////        foreach ($value_row as $value_data) {
////            $field_value += $value_data["FieldValue"];
////            $field_count = $value_data["Count"];
////            $filed_symbol = $value_data["WithSymbol"];
////        }
////
////        foreach ($plotbands_arr as $key => $plot) {
////            if ($plot["FromType"] == 'static') {
////                $from = $plot["From"];
////            } else {
////                $formulaPlotDoc = new Formula($plot["From"]);
////                $formulaPlotDoc->DataFormSource[0] = $plot_bands_ds;
////                $from = $formulaPlotDoc->evaluate();
////            }
////
////            if ($plot["ToType"] == 'static') {
////                $to = $plot["To"];
////            } else {
////                $formulaPlotDoc = new Formula($plot["To"]);
////                $formulaPlotDoc->DataFormSource[0] = $plot_bands_ds;
////                $to = $formulaPlotDoc->evaluate();
////            }
////
////            // var_dump($field_value . " >= " . $from);
////            // var_dump($field_value . " <= " . $to);
////            // var_dump($plot["Name"]);
////            // var_dump($plot["ShowSymbol"]);
////
////            if ($plot["ShowSymbol"] == "true" && $field_value >= $from && $field_value <= $to) {
////                $filed_symbol = true;
////                // var_dump("blink");
////            }
////            // echo "==================";
////        }
////
////        $value_row = array();
////        array_push($value_row, array("FieldValue" => $field_value,
////            "Count" => $field_count,
////            "WithSymbol" => $filed_symbol));
////        //end
////
////        if ($versus == 'Requestor' || $versus == 'Processor') {
////            $personDoc = new Person($db, $value["Versus"]);
////            array_push($whole_chart_data, array("Column_Name" => $personDoc->display_name,
////                "Data" => $value_row));
////        } else {
////            array_push($whole_chart_data, array("Column_Name" => $value["Versus"],
////                "Data" => $value_row));
////        }
//    }
}


// var_dump($whole_chart_data);
//set plotbands

$plot_bands_ds = array();
foreach ($parameters as $key => $value) {
    $plot_bands_ds[$value["Field"]] = implode(",", $_GET[$value["Field"]]);
}

$chart_plot_bands = array();
foreach ($plotbands_arr as $key => $plot) {
    if ($plot["FromType"] == 'static') {
        $from = $plot["From"];
    } else {
        $formulaPlotDoc = new Formula($plot["From"]);
        $formulaPlotDoc->DataFormSource[0] = $plot_bands_ds;
        $from = $formulaPlotDoc->evaluate();
    }

    if ($plot["ToType"] == 'static') {
        $to = $plot["To"];
    } else {
        $formulaPlotDoc = new Formula($plot["To"]);
        $formulaPlotDoc->DataFormSource[0] = $plot_bands_ds;
        $to = $formulaPlotDoc->evaluate();
    }

    $array_plot = array(
        "from" => $from,
        "to" => $to,
        "color" => $plot["Color"],
        "label" => array(
            "text" => $plot["Name"],
            "style" => array("color" => "#606060"))
    );

    array_push($chart_plot_bands, $array_plot);
}


//sort plot bands
$plot_range = array();
foreach ($chart_plot_bands as $key => $value) {
    $plot_range[$key] = $value["t"];
}

array_multisort($plot_range, SORT_DESC, $chart_plot_bands);

//var_dump($chart_plot_bands);
//echo "<input type='text' style='display:none' id='chart-data' value='" . json_encode($whole_chart_data) . "'/>";
echo "<div style='display:none' id='chart-data'>" . json_encode($whole_chart_data) . "</div>";
?>
<?php
if ($type == 3) {
    //if report type is chart
    ?>
    <div style='display:none'>
        <label>Y-Axis Max</label>
        <input id="yaxisMax" type="number" value="<?php echo $reportDoc->yaxis_max; ?>"/>
        <!--<div id="slider"></div>-->
        <label>Y-Axis Min</label>
        <input id="yaxisMin" type="number" value="<?php echo $reportDoc->yaxis_min; ?>"/>
        <label>Y-Axis Interval</label>
        <input id="yaxisInterval" type="number" value="<?php echo $reportDoc->yaxis_interval; ?>"/>
    </div>
    <div id="plot-bands-report-data" style="display: none"><?php echo json_encode($chart_plot_bands); ?></div>
    <?php
}

$mod = unserialize(ENABLE_COMPANY_MOD);
$gi_manager = $mod["gs3_insights"]["gi-report-designer"];
$gi_designer = $mod["gs3_insights"]["gi-report-designer-list"];
$allow_pin = ($gi_manager == 0 && $gi_designer == 0);
?>
<script type="text/javascript" src="/js/library/jqueryLib/jquery-1.10.0.min.js"></script>
<?php
if ($allow_pin == true) {
    ?>
    <button class="pinit icon dataTip dashboard_pin" fl-data-application-id="false">Pin this to Dashboard</button>
<?php } ?>
<table id="chart-table" style="margin-top:10px; display:none;width: 100%; background-color: #fff; border: 1px solid #CFDBE2;" cellpadding=5 cellspacing=0>
    <thead>
        <tr class="series">
            <th style="text-align:left;font-size:12px;background: #4590D2;border:1px Solid #ccc;color:#fff"></th>
        </tr>
    </thead>
    <tbody class="data">
    </tbody>
</table>

        <!-- <table id="" style="margin-top:5px" cellpadding=5 cellspacing=0>
            <thead>
                <tr>
                    <th  style="font-size:12px;background: #4590D2;border:1px Solid #ccc;color:#fff"></th>
                    <th  style="font-size:12px;background: #4590D2;border:1px Solid #ccc;color:#fff">Jane</th>
                    <th  style="font-size:12px;background: #4590D2;border:1px Solid #ccc;color:#fff">John</th>
                </tr>
            </thead>
            <tbody>
                <tr style="font-size:12px;border:1px Solid #ccc">
                    <th>Apples</th>
                    <td>3</td>
                    <td>4</td>
                </tr>
                <tr style="font-size:12px;border:1px Solid #ccc">
                    <th>Pears</th>
                    <td>2</td>
                    <td>0</td>
                </tr>
                <tr style="font-size:12px;border:1px Solid #ccc">
                    <th>Plums</th>
                    <td>5</td>
                    <td>11</td>
                </tr>
                <tr style="font-size:12px;border:1px Solid #ccc">
                    <th>Bananas</th>
                    <td>1</td>
                    <td>1</td>
                </tr>
                <tr style="font-size:12px;border:1px Solid #ccc">
                    <th>Oranges</th>
                    <td>2</td>
                    <td>4</td>
                </tr>
            </tbody>
        </table> -->
<?php
if ($formId == 131) {
//    $result = $db->query("SELECT * FROM  `11_tbl_redings`");
    ?>
    <table id="" style="margin-top:5px" cellpadding=5 cellspacing=0>
        <thead>
            <tr>
                <th  style="font-size:11px;background: #4590D2;border:1px Solid #ccc;color:#fff;width:300px"></th>
                <?php
                foreach ($result as $row) {
                    $requestor_reading = new Person($db, $row["Requestor"]);
                    echo "<th  style='font-size:11px;background: #4590D2;border:1px Solid #ccc;color:#fff'>" . $requestor_reading->display_name . "</th>";
                }
                ?>
            </tr>
        </thead>
        <tbody>
            <tr style="font-size:11px;border:1px Solid #ccc">
                <th>Average per Subplot</th>
                <?php
                foreach ($result as $row) {
                    echo "<td style='font-size:11px;border:1px Solid #ccc'>" . round($row["XBar"], 2) . "</td>";
                }
                ?>
            </tr>
            <tr  style="font-size:11px;border:1px Solid #ccc">
                <th>Min per sub plot</th>
                <?php
                foreach ($result as $row) {
                    echo "<td style='font-size:11px;border:1px Solid #ccc'>" . round($row["Min"], 2) . "</td>";
                }
                ?>
            </tr>
            <tr style="font-size:11px;border:1px Solid #ccc">
                <th>Max per subplot</th>
                <?php
                foreach ($result as $row) {
                    echo "<td style='font-size:11px;border:1px Solid #ccc'>" . round($row["Max"], 2) . "</td>";
                }
                ?>
            </tr>
            <tr style="font-size:11px;border:1px Solid #ccc">
                <th>Range per subplot</th>
                <?php
                foreach ($result as $row) {
                    echo "<td style='font-size:11px;border:1px Solid #ccc'>" . round($row["xrange"], 2) . "</td>";
                }
                ?>
            </tr>
            <tr style="font-size:11px;border:1px Solid #ccc">
                <th>Standard deviation </th>
                <?php

                function standard_deviation($sample) {
                    if (is_array($sample)) {
                        $mean = array_sum($sample) / count($sample);
                        foreach ($sample as $key => $num)
                            $devs[$key] = pow($num - $mean, 2);
                        return sqrt(array_sum($devs) / (count($devs) - 1));
                    }
                }

                foreach ($result as $row) {
                    $repeater_data = json_decode($row["Repeater_Data"], true);
                    $sdev_arr = array();

                    foreach ($repeater_data[0]["Row"] as $repeater_row) {
                        array_push($sdev_arr, $repeater_row["Data"][0]["Values"]);
                    }

                    $sdev_return = standard_deviation($sdev_arr);
                    echo "<td style='font-size:11px;border:1px Solid #ccc'>" . round($sdev_return, 2) . "</td>";
                }
                ?>
            </tr>
        </tbody>
    </table>
    <?php
}
//var_dump($result);
?>

<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('change', '#yaxisMax, #yaxisMin, #yaxisInterval', function () {
            //chart.generateChart();
            var thisId = $(this).attr('id');
            var thisVal = $(this).val();
            var currentLocation = String(window.location);

            window.location = currentLocation + '&' + thisId + '=' + thisVal;

        });

<?php
if ($type == 3) {
    ?>


            //            $('#chart-table').show();
            chart.genereteTable();
    <?php
}
?>
        var isInIframe = (window.location != window.parent.location) ? true : false;
        if (isInIframe) {
            $('.pinit').hide();
            //              $('#chart-table').show();
        }
        pin.init();
    });

    var pin = {
        init: function () {
            this.addEventListeners();
        },
        addEventListeners: function () {
            var self = this;
            $('body').on('click', '.dashboard_pin', function () {
                var pin = this;
                var content = [];

                content.push({
                    object_type: "report",
                    object_id: "<?php echo $reportDoc->title; ?>",
                    formId: "<?php echo $formDoc->id; ?>",
                    reportId: "<?php echo $reportDoc->id; ?>",
                    url: String(window.location)
                });

                self.save({
                    application_id: $(this).attr('fl-data-application-id'),
                    content: JSON.stringify(content)
                }, function (result) {
                    console.log(result);
                    alert('Content has been pinned.');
                    //$(pin).css('opacity', '.4');
//                showNotification({
//                    message: "Panel s.",
//                    type: "success",
//                    autoClose: true,
//                    duration: 3
//                });
                });
            });
        },
        save: function (data, callback) {
            $.get('/ajax/dashboard', data, function () {
                callback(data);
            });
        }
    }


    var chart = {
        genereteTable: function () {
            var chartData = $("#chart-data").text();
            chartData = JSON.parse(chartData);
            console.log(chartData);
            var startCell = 0;
            var endCell = 0;
            var headers = [];
            //set header and fields//
            for (var index in chartData) {

                var columnName = chartData[index].Column_Name;
                var data = chartData[index].Data;

                if (columnName != '' && columnName != null) {
                    var valueRow = '<tr id="tr_' + columnName.replace(/ /g, '_') + '" data-id="tr_' + columnName.replace(/ /g, '_') + '" style="font-size:12px;border:1px Solid #ccc"><th>' + columnName + '</th></tr>';
                    $('.data').append(valueRow);
                    for (var row in data) {
                        var columnHeader = data[row].FieldValue;
                        var withSymbol = data[row].WithSymbol;
                        // if (headers.indexOf(columnHeader) == -1) {
                        headers.push(columnHeader);
                        $('.series').append('<th style="text-align:left;font-size:12px;background: #4590D2;border:1px Solid #ccc;color:#fff" symbol="' + withSymbol + '">' + columnHeader + '</th>');
                        //}
                    }
                }
            }
            //data
            for (var index in chartData) {
                var columnName = chartData[index].Column_Name;
                var data = chartData[index].Data;
                var seriesCount = $('.series').find('th').length;


                if (columnName != '' && columnName != null) {

                    for (var ctr = 0; ctr <= headers.length - 1; ctr++) {
                        $('[data-id="tr_' + columnName.replace(/ /g, '_') + '"]').append('<td>0</td>');
                    }

                    for (var row in data) {
                        var count = data[row].Count;
                        var columnHeader = data[row].FieldValue;

                        var position = headers.indexOf(columnHeader) + 1;
                        console.log();
                        $('[data-id="tr_' + columnName.replace(/ /g, '_') + '"]').children().eq(position).text(count);
                    }

                }

            }
            this.generateChart();
        }, getLineAreaSeries: function () {
            var data = [];
            $('#chart-table').find('.data th').each(function () {
                data.push($(this).text());
            });

            return data;
        }, getLineAreaData: function () {
            var data = [];
            $('#chart-table').find('.series th').each(function (index) {
                if (index > 0) {
                    if (parseFloat($(this).text())) {
                        if ($(this).attr('symbol') == "true") {
                            data.push({
                                y: parseFloat($(this).text()),
                                marker: {
                                    symbol: 'url(/images/icon/bullet.gif)'}
                            });
                        } else {
                            data.push({
                                y: parseFloat($(this).text())
                            });
                        }
                    } else if ($(this).text() == '0') {
                        if ($(this).attr('symbol') == "true") {
                            data.push({
                                y: 0,
                                marker: {
                                    symbol: 'url(/images/icon/bullet.gif)'}
                            });
                        } else {
                            data.push({
                                y: 0
                            });
                        }
                    } else {
                        data.push({
                            y: parseFloat($(this).text())
                        });
                    }
                    ;
                }
            });

            return data;
        }, getDataType: function () {
            var dataType = 'String';
            $('#chart-table').find('.series th').each(function (index) {
                if (index > 0) {
                    if (parseInt($(this).text())) {
                        dataType = 'Float';
                    }
                }
            });

            return dataType;
        }, generateChart: function () {
            var self = this;
            var chartType = $("#chart-type").val();
            var chartDataType = $('#chartDataType').val();
            var yaxis = $('#yaxis').val();
            var reportTitle = $(".reportTitle").text();
            var yAxisMax = $('#yaxisMax').val();
            var yAxisMin = $('#yaxisMin').val();
            var yAxisInverval = $('#yaxisInterval').val();
            var series = $("#series").val();
            var plotBands = JSON.parse($('#plot-bands-report-data').html());

            console.log('Plot Bands', plotBands);
            Highcharts.visualize = function (table, options) {
                // the categories
                options.xAxis.categories = [];
                $('tbody th', table).each(function (i) {
                    options.xAxis.categories.push(this.innerHTML);
                });

                // the data series
                options.series = [];
                $('tr', table).each(function (i) {
                    var tr = this;
                    $('th, td', tr).each(function (j) {
                        if (j > 0) { // skip first column
                            if (i == 0) { // get the name and init the series
                                options.series[j - 1] = {
                                    name: this.innerHTML,
                                    data: []
                                };
                            } else { // add values
                                options.series[j - 1].data.push(parseFloat(this.innerHTML));
                            }
                        }
                    });
                });

                var chart = new Highcharts.Chart(options);
            }

            if (chartDataType == 2 && yaxis != 0) {
                var chart = $('#container').highcharts({
                    chart: {
                        type: chartType
                    },
                    title: {
                        text: reportTitle,
                        x: -20 //center
                    },
                    xAxis: {
                        categories: self.getLineAreaSeries(),
                        labels: {
                            rotation: -90,
                        }
                    },
                    yAxis: {
                        min: parseInt(yAxisMin),
                        max: parseInt(yAxisMax),
                        tickInterval: parseInt(yAxisInverval),
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }],
                        plotBands: plotBands
                    },
                    tooltip: {
                        valueSuffix: ''
                    },
                    series: [{
                            name: series,
                            data: self.getLineAreaData()
                        }]
                });

                console.log('chart data', {
                    chart: {
                        type: chartType
                    },
                    title: {
                        text: reportTitle,
                        x: -20 //center
                    },
                    xAxis: {
                        categories: self.getLineAreaSeries()
                    },
                    yAxis: {
                        min: parseInt(yAxisMin),
                        max: parseInt(yAxisMax),
                        tickInterval: parseInt(yAxisInverval),
                        plotLines: [{
                                value: 0,
                                width: 1,
                                color: '#808080'
                            }],
                        plotBands: plotBands
                    },
                    tooltip: {
                        valueSuffix: ''
                    },
                    series: [{
                            name: series,
                            data: self.getLineAreaData()
                        }]
                });
            } else {
                var table = document.getElementById('chart-table'),
                        options = {
                            chart: {
                                renderTo: 'container',
                                type: chartType
                            },
                            title: {
                                text: reportTitle
                            },
                            xAxis: {
                            },
                            yAxis: {
                                title: {
                                    text: 'Number of Record'
                                }
                            },
                            tooltip: {
                                formatter: function () {
                                    return '<b>' + this.series.name + '</b><br/>' +
                                            this.y + ' ' + this.x.toLowerCase();
                                }
                            }
                        };

                Highcharts.visualize(table, options);
            }
        }
    }

</script>