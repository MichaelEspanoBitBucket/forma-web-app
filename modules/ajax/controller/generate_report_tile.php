<?php
$auth = Auth::getAuth('current_user');
if(!Auth::hasAuth('current_user')){ 
   return false;
}
$db = new Database;
$fs = new functions();
$date = $fs->currentDateTime();
$application_id = $_COOKIE['application'];
$category = $_GET["category"];


//var_dump($_GET);

$formDoc = new Form($db, $_GET["form_id"]);
$categories = $db->query("SELECT " . $category . " FROM " . $formDoc->form_table_name . " WHERE " . $category . " <> '' "
        . " GROUP BY " . $category, "array");

$parameters = $_GET;
$reserved_parameters = array("", "id", "form_id", "type",
    "chartType", "chartDataType", "series", "versus", "yaxis", "yaxisMax", "yaxisMin", "yaxisInterval", "category");

foreach ($categories as $index => $category_value) {
    $parameters_string = "";
    $paremeter_index = 0;
    foreach ($parameters as $paremeter_key => $parameter_value) {
        $paremeter_index++;
        $is_reserved = array_search($paremeter_key, $reserved_parameters);
        $ampersand = "&";
        if (count($parameters) == $paremeter_index) {
            $ampersand = "";
        }
        if ($is_reserved == false) {
            if ($paremeter_key == $category) {
                $parameter_value = $category_value;
            }

            $parameters_string.=$paremeter_key . "[]=" . implode(",", $parameter_value) . $ampersand;
        } else {
            $parameters_string.=$paremeter_key . "=" . $parameter_value . $ampersand;
        }
    }

//    var_dump($parameters_string);
    ?>
    <style>
        #wrapper {   width:23%;padding: 0; overflow: hidden; display: inline-table; }
        .scaled-frame { width: 100%; height: 400px; overflow: hidden; zoom: 100%;}

    </style>
    <div id="wrapper" ><iframe class="scaled-frame" src="<?php echo "/ajax/generateReport?" . $parameters_string ?>"></iframe></div>
    <?php
}
//
//$dashboards = functions::getDashboards(" WHERE UserID = {$auth['id']} AND ApplicationID = {$application_id} ");
//foreach ($dashboards as $dashboardDoc) {
//    $dashboard_objects = json_decode($dashboardDoc->content, true);
//}
//
//$dashboard_objects = functions::my_array_unique($dashboard_objects);
//
//foreach ($dashboard_objects as $dashboard_object_doc) {
//    if ($dashboard_object_doc["object_type"] == 'report') {
//        
?>
