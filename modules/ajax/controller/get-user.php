<?php

$db = new Database();
$auth = Auth::getAuth('current_user');
$fs = new Functions();

error_reporting(0);


$array = array();

$term = trim(strip_tags($_GET['term']));//retrieve the search term that autocomplete sends
$query = $db->query("SELECT * FROM tbuser 
	WHERE 
		company_id = {$db->escape($auth['company_id'])} 
		&& 
		(
			first_name LIKE '%".$term."%' || 
			last_name LIKE '%".$term."%' || 
			email LIKE '%".$term."%' || 
			display_name LIKE '%".$term."%'
		)
		&& 
			is_active={$db->escape(1)} 
		&& 
			display_name != ''
		&& 
			last_name != ''
		&& 
			first_name != ''
			ORDER BY `tbuser`.`display_name` ASC","array");
// echo "SELECT * FROM tbuser 
// 	WHERE 
// 		company_id = {$db->escape($auth['company_id'])} 
// 		&& 
// 		(
// 			first_name LIKE '%".$term."%' || 
// 			last_name LIKE '%".$term."%' || 
// 			email LIKE '%".$term."%' || 
// 			display_name LIKE '%".$term."%'
// 		)
// 		&& 
// 			is_active={$db->escape(1)} 
// 		&& 
// 			display_name != ''
// 			ORDER BY `tbuser`.`first_name` ASC";

//Modified affected by text-tagging it uses the key preferenceID preferenceText
$preferenceText = 'display_name'; 
$preferenceID = 'id';
if(isset($_GET['preferenceID']) && !empty($_GET['preferenceID'])){
	$preferenceID = $_GET['preferenceID'];
}
if(isset($_GET['preferenceText']) && !empty($_GET['preferenceText'])){
	$preferenceText = $_GET['preferenceText'];
}

foreach ($query as $value) {
	$array[] = array("text"	=>	ucwords($value[$preferenceText]),
				 	 "id"	=>	$value[$preferenceID]);
}

echo json_encode($array);



?>