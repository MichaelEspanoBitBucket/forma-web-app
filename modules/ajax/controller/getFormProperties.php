<?php
$auth = Auth::getAuth('current_user');
if(!Auth::hasAuth('current_user')){ 
   return false;
}
$db = new Database;
$fs = new functions;
$company_id = $auth['company_id'];
$search = new Search();
if(isset($_POST['action'])){
	if($_POST['action']=="getForm"){
		//$getForms = $db->query("SELECT * FROM tb_workspace WHERE company_id={$db->escape($auth['company_id'])}","array");
		// $getForms = $db->query("SELECT * FROM tb_workspace WHERE is_active={$db->escape(1)} AND company_id = {$db->escape($company_id)} AND is_delete = 0","array");
		// $form_json = array();
		// foreach($getForms as $form){
		// 	$arr = array("form_id"=>$form['id'],
		// 		     "form_name"=>$form['form_name']);
		// 	array_push($form_json,$arr);
		   
		// }
		//  echo json_encode($form_json);
		$getForms = $search->getFormsV2("","","","array");
		echo json_encode($getForms);
	}else if($_POST['action']=="getActiveForms"){
		echo json_encode($search->getAllActiveForms());
	}
	
	// Get Form Fields
	elseif($_POST['action']=="getFlds"){
		$formID = $_POST['formID'];
		
			$getForm_flds = $db->query("SELECT * FROM tb_workspace WHERE id={$db->escape(1)} AND is_active={$db->escape(1)} AND is_delete = 0","row");
			
			$form_flds = $getForm_flds['form_json'];
			
				$decode_flds = json_decode($form_flds, true);
				$fields = $decode_flds['form_fields'];
				$fldName = json_decode($fields, true);
				foreach($fldName as $flds){
					$f = array("fieldName"	=>	 $flds['fieldName']);
					$arr[] = array_diff($f, array("TrackNo", "Requestor", "Status", "Processor",
									"LastAction", "DateCreated", "DateUpdated", "CreatedBy",
									"UpdatedBy", "Unread", "Node_ID", "Workflow_ID"));
				}
					
					echo json_encode(array_filter($arr));
					
			
	}
}
?>