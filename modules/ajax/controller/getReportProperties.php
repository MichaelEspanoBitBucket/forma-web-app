<?php

$auth = Auth::getAuth('current_user');
$db = new Database;

$id = $_POST['id'];

$reportDoc = new Report($db, $id);
if (!Auth::hasAuth('current_user')) {
    return false;
}
echo json_encode($reportDoc);
?>
