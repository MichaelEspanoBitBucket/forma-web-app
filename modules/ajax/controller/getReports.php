<?php

$auth = Auth::getAuth('current_user');
$db = new Database;
$form_id = $_GET['form_id'];

$formDoc = new Form($db, $form_id);
$application_id = $_COOKIE['application'];
$personDoc = new Person($db, $auth["id"]);
if(!Auth::hasAuth('current_user')){ 
   return false;
}

//$reports = functions::getReports(" LEFT JOIN tb_workspace workspace"
//                . " ON workspace.ID = tbreport.form_id "
//                . " WHERE tbreport.company_id={$db->escape($auth['company_id'])} AND tbreport.is_active=1");
//
//$aaData = array();
//$report_data = array();
//foreach ($reports as $report) {
//    array_push($report_data, array($report->title, $report->description, $report->date_created,
//        '<a link-type="report-generation" data-report-id="' . $report->id . '">Generate</a>'));
//}

$result = $db->query("SELECT report.id as report_id, workspace.* FROM tbreport report"
        . " LEFT JOIN tb_workspace workspace "
        . " ON workspace.Id = report.form_id "
        . " WHERE report.company_id={$db->escape($auth['company_id'])} AND report.is_active=1 AND workspace.is_delete = 0", "array");


$aaData = array();
$report_data = array();
foreach ($result as $row) {
    $report = new Report($db, $row["report_id"]);
    $form_json = json_decode($row["form_json"], true);
    $form_category_id = $form_json["categoryName"];


    if ($form_category_id == $application_id || $personDoc->user_level_id == 2) {
        array_push($report_data, array($report->title, $report->description, $report->date_created,
            '<a link-type="report-generation" data-report-id="' . $report->id . '">Generate</a>'));
    }
}

$aaData['aaData'] = $report_data;
print_r(json_encode($aaData));
?>