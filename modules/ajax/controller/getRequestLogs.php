<?php

$auth = Auth::getAuth('current_user');
$post_data = filter_input_array(INPUT_POST);
$form_id = $post_data['formId'];
$request_id = $post_data['request_Id'];
$conn = getCurrentDatabaseConnection();
if (!Auth::hasAuth('current_user')) {
    return false;
}

if ($conn->is_connected) {
    $logs = $conn->query("SELECT "
            . " l.details, "
            . " l.date_created,"
            . " u.display_name as created_by_display_name "
            . " FROM tbrequest_logs l "
            . " LEFT JOIN tbuser u "
            . " ON u.id = l.created_by "
            . " WHERE l.form_id={$conn->escape($form_id)} AND l.request_id={$conn->escape($request_id)} "
            . " ORDER BY l.id DESC");
    $conn->disConnect();
    exit(json_encode($logs));
}
