<?php
//ini_set('display_startup_errors',1);
//ini_set('display_errors',1);
error_reporting(-1);
	
/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-models/Models.php");
require_once(realpath('.') . "/library/gi-repositories/DataModelRepository.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);

$dmRepo = new DataModelRepository($conn, $auth);

$modelId = $_POST["id"];

$deleteState = $dmRepo->deleteModel($modelId);

echo json_encode($deleteState);

?>
