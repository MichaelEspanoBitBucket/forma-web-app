<?php
error_reporting(E_ALL);

/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-models/Models.php");
require_once(realpath('.') . "/library/gi-repositories/DataModelRepository.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$dmRepo = new DataModelRepository($conn, $auth);

$modelData = $_POST["data"];

$modelState = ["status" => "valid"];

$validationErrors = $dmRepo->validateModel($modelData);
/* If validation passed, attempt to save */

if(sizeof($validationErrors) == 0)
{
	$saveState = $dmRepo->saveModel($modelData);
	if($saveState["isSuccess"]==false){
		$modelState["status"] = "invalid";
		$modelState["data"] = [ "errorSummary" => "Model cannot be saved. Something went wrong. Please try again." ];
	}else{
		$modelState["id"] = $saveState["id"];
	}
}
else
{
	$modelState["status"] = "invalid";
	$modelState["errorSummary"] = "Model cannot be saved. Invalid data.";
	$modelState["validationErrors"] = $validationErrors;
}

echo json_encode($modelState);

?>
