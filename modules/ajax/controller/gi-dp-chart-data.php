<?php
error_reporting(E_ALL);

/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load chart data. User not logged in.";
} 

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-repositories/ReportRepository.php");
//require_once(realpath('.') . "/library/Phinq/bootstrap.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$rptRepo = new ReportRepository($conn, $auth);

$chartGiqs = null;
if(isset($_GET["id"])){
	//saved chart, get saved chart baby
	$chartGiqs = $rptRepo->getChartGiqs($_GET["id"]);
	//TODO:delete echo json_encode($chartGiqs); echo "<br/><br/><br/>";
}else{
	//unsaved chart! giq and model id provided though, get model and inject it
	$dmRepo = new DataModelRepository($conn, $auth);
	
	$chartGiqs = $_POST["chartGiqs"];
	for($i = 0; $i<count($chartGiqs); $i++){
		$model = $dmRepo->getModel($chartGiqs[$i]["modelId"]);
		$chartGiqs[$i]["model"] = $model;
		if(!isset($chartGiqs[$i]["optionalGroupings"])){
			$chartGiqs[$i]["optionalGroupings"] = array();
		}
	}
}


//get chart data using giqs and models.
$dataSets = [];
$metaDataList = [];
$chartForms = [];
//var_dump($rptRepo->getChartForms($chartGiqs[0]));
for($i = 0; $i<count($chartGiqs); $i++){
	
	$dataSets[] = $rptRepo->getChartGiqData($chartGiqs[$i]);
	$metaDataList[] = $rptRepo->getChartInitialConfig($chartGiqs[$i]);
	$chartForms[] = $rptRepo->getChartForms($chartGiqs[$i]);
}



$dataAndConfigSets = [ "data" => $dataSets, "metaData" => $metaDataList, "chartForms" => $chartForms ];

/*
data

config 
- categories (list)
- optionalCategories (list)
- sliceOptions(list)

userConfig 
- sliceList (list of field and value)
- drillDownList (list)

*/

echo json_encode($dataAndConfigSets);


?>
