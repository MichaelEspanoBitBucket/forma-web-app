<?php
error_reporting(E_ALL);
/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-repositories/DataSourceRepository.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$dsRepo = new DataSourceRepository($conn, $auth);

header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

$dsEntities = $dsRepo->getAllDsEntities($_GET["id"]);
$dsEntitiesJson = json_encode($dsEntities);

echo $dsEntitiesJson;

?>
