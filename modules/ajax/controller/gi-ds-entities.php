<?php
error_reporting(E_ALL);
/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-repositories/DataSourceRepository.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$dsRepo = new DataSourceRepository($conn, $auth);

$dsEntities = $dsRepo->getAllDsEntities($_GET["id"]);
?>

<?php foreach($dsEntities as $dsEntity){ ?>
	<li class='ds-entity-item' data-id="<?php echo $dsEntity["id"] ?>">
		<i class="gi-ui-ms-pointer ds-entity-toggle fa fa-plus-square-o"></i>
		<span class="li-item-icon entity-icon"></span>
		<span class='ds-entity-label gi-ui-ms-move'><?php echo $dsEntity["name"] ?></span>
		<ul class='ds-entity-attributes' style="display:none"></ul>
	</li>
<?php } ?>
