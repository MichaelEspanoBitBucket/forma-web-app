<?php
error_reporting(E_ALL);
/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load data sources. User not logged in.";
}

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-repositories/DataSourceRepository.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$dsRepo = new DataSourceRepository($conn, $auth);

$entityAttributes = $dsRepo->getAllDsEntityAttributes($_GET["ds-id"], $_GET["entity-id"]);

?>

<?php foreach($entityAttributes as $entityAttribute){ ?>
	<li class='entity-attribute-item' data-id="<?php echo $entityAttribute["id"] ?>" 
		data-data-type-id="<?php echo $entityAttribute["type"]["id"] ?>"
		data-data-type-name="<?php echo $entityAttribute["type"]["name"] ?>"
		data-ds-type-id="<?php echo $entityAttribute["dsType"]["id"] ?>"
		data-ds-type-name="<?php echo $entityAttribute["dsType"]["name"] ?>">
		<span class="li-item-icon attribute-icon"></span>
		<span class='entity-attribute-label'><?php echo $entityAttribute["name"] ?></span>
	</li>
<?php } ?>
