<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);

require_once(realpath('.') . "/library/Phinq/bootstrap.php");

use Phinq\Phinq as Phinq;


/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load chart data. User not logged in.";
} 

$auth = Auth::getAuth('current_user');
$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);

$giqsData = [ "timeGenerated" => null, "dataTable" => [], "giqs" => [] ];
$sqls = [];

if($_GET["ccdid"] == 1){
	$	["giqs"][] = 
		[
			"aggregates"=> [["exp"=>[], "alias"=>"TEU"]],
			"groupings"=> [["groupExp"=>[], "dispExp"=>[], "alias"=>"Category"]],
			"optionalGroupings"=> [],
			"filters"=> [["exp"=>[]]],
			"dataSource"=> [ "id"=>1, "name"=>"Default", "type"=>["name"=>"Formalistics","id"=>1]],
			"entities"=> [[ 
					"formName"=>"Container Information", 
					"formId"=>1,
					"alias"=>"Container Information",
					"relationships"=>[]
				]]
		];
		
	$sqls[] = 
		"(select 'Utilizied' as Category, sum(if(c.pick_size=20, 6, 12) * cCount.count) as TEU
		from `20_tbl_submissionofexpectedcontainers` as c
		inner join 
			(select container_location, count(ID)  as `count`
			from `20_tbl_submissionofexpectedcontainers` 
			where containerTransaction = 'Incoming'
			group by container_location) as cCount
			on c.container_location = cCount.container_location
		inner join 20_tbl_areamaintenance as aaa on c.AreaCode = aaa.txt_AreaName
		where containerTransaction = 'Incoming'
		and c.AreaCode <> ''  and c.pick_size <> 0 and cCount.count <= 6
		and areawidth <> '')
		union all
		(select 'Available' as Category, 
			sum(if(pick_Size = 20, 6, 12) * a.txt_Slots * 6) - 
				(select sum(if(c.pick_size=20, 6, 12) * cCount.count) as TEU
				from `20_tbl_submissionofexpectedcontainers` as c
				inner join 
					(select container_location, count(ID)  as `count`
					from `20_tbl_submissionofexpectedcontainers` 
					where containerTransaction = 'Incoming'
					group by container_location) as cCount
					on c.container_location = cCount.container_location
				inner join 20_tbl_areamaintenance as aaa on c.AreaCode = aaa.txt_AreaName
				where containerTransaction = 'Incoming'
				and c.AreaCode <> ''  and c.pick_size <> 0 and cCount.count <= 6
				and areawidth <> '')
			as teu
		from 20_tbl_areamaintenance as a)";
}

if($_GET["ccdid"] == 2){
	$giqsData["giqs"][] = 
		[
			"aggregates"=> [["exp"=>[], "alias"=>"Utilized"], ["exp"=>[], "alias"=>"Available"]],
			"groupings"=> [["groupExp"=>[], "dispExp"=>[], "alias"=>"Area"]],
			"optionalGroupings"=> [],
			"filters"=> [["exp"=>[]]],
			"dataSource"=> [ "id"=>1, "name"=>"Default", "type"=>["name"=>"Formalistics","id"=>1]],
			"entities"=> [[ 
					"formName"=>"Container Information", 
					"formId"=>1,
					"alias"=>"Container Information",
					"relationships"=>[]
				]]
		];
		
	$sqls[] = 
		"select cap.Area, ifnull(util.Utilized, 0) as Utilized, cap.Capacity - ifnull(util.Utilized, 0) as Available
		from
		(select c.AreaCode as Area, sum(if(c.pick_size=20, 6, 12) * cCount.count) as Utilized
			from `20_tbl_submissionofexpectedcontainers` as c
			inner join 
				(select container_location, count(ID)  as `count`
				from `20_tbl_submissionofexpectedcontainers` 
				where containerTransaction = 'Incoming'
				group by container_location) as cCount
				on c.container_location = cCount.container_location
			inner join 20_tbl_areamaintenance as aaa 
			on c.AreaCode = aaa.txt_AreaName
			where containerTransaction = 'Incoming'
			and c.AreaCode <> ''  and c.AreaCode <> 'null' and c.pick_size <> 0 and cCount.count <= 6
			and areawidth <> '' and areawidth <> 'null' and c.container_location <> ''
			and c.pick_size <> '' and areawidth > 0
			and c.pick_size = aaa.pick_Size
			and c.areawidth = aaa.txt_Slots
			group by c.AreaCode) as util
		right join 
		(select a.txt_AreaName as Area, 
			sum(if(pick_Size = 20, 6, 12) * txt_Slots * 6) as Capacity
		from 20_tbl_areamaintenance as a
		group by a.txt_AreaName) as cap
		on util.Area = cap.Area ";
}

if($_GET["ccdid"] == 3){
	$giqsData["giqs"][] = 
		[
			"aggregates"=> [["exp"=>[], "alias"=>"Count"]],
			"groupings"=> [["groupExp"=>[], "dispExp"=>[], "alias"=>"Month"]],
			"optionalGroupings"=> [],
			"filters"=> [["exp"=>[]]],
			"dataSource"=> [ "id"=>1, "name"=>"Default", "type"=>["name"=>"Formalistics","id"=>1]],
			"entities"=> [[ 
					"formName"=>"Incident or Accident Investigation Report", 
					"formId"=>32,
					"alias"=>"Incident or Accident Investigation Report",
					"relationships"=>[]
				]]
		];
		
	$sqls[] = 
		"select monthname as `Month`, `count` as `Count` from 

		(select  monthname(`Incident or Accident Investigation Report`.`Accidentdate`)  as `monthname`,   count( `Incident or Accident Investigation Report`.`ID` )  as `count` 

		from `20_tbl_IncidentorAccidentInvestigationReport` as `Incident or Accident Investigation Report` 

		where year( `Incident or Accident Investigation Report`.`Accidentdate` ) = year(  now() )  

		group by  monthname(`Incident or Accident Investigation Report`.`Accidentdate`)
		with rollup) x
		order by month(str_to_date(monthname, '%M'))";
}

//
if($_GET["ccdid"] == 4){
	$giqsData["giqs"][] = 
		[
			"aggregates"=> [["exp"=>[], "alias"=>"Actual"]],
			"groupings"=> [["groupExp"=>[], "dispExp"=>[], "alias"=>"Month"]],
			"optionalGroupings"=> [],
			"filters"=> [["exp"=>[]]],
			"dataSource"=> [ "id"=>1, "name"=>"Default", "type"=>["name"=>"Formalistics","id"=>1]],
			"entities"=> []
		];
		
	$sqls[] = 
		"select i.month as Month, sum(if(i.val is not null and i.val <> '',1,0)) as Actual
		from 
		((select sapr_tbactivity_id as act_id, 'Jan' as month, 1 as month_no, january_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Feb' as month, 2 as month_no, february_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Mar' as month, 3 as month_no, march_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Apr' as month, 4 as month_no, april_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'May' as month, 5 as month_no, may_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Jun' as month, 6 as month_no, june_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Jul' as month, 7 as month_no, july_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Aug' as month, 8 as month_no, august_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Sep' as month, 9 as month_no, september_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Oct' as month, 10 as month_no, october_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Nov' as month, 11 as month_no, november_f1 as val from sapr_tbitem)
		union all
		(select sapr_tbactivity_id as act_id, 'Dec' as month, 12 as month_no, december_f1 as val from sapr_tbitem)) as i
		group by i.month_no";
}

for($i=0;$i<count($giqsData["giqs"]);$i++){
	$giq = $giqsData["giqs"][$i];
	
	$stm = $conn->prepare($sqls[$i]);
	$stm->execute();
	$sqlResult = $stm->get_result()->fetch_all(MYSQLI_ASSOC);
	/*
	$initialCats = Phinq::create(isset($giq["groupings"])?$giq["groupings"]:[])
		->union(isset($giq["optionalGroupings"])?$giq["optionalGroupings"]:[])
		->select(function($g){ return $g["alias"]; })
		->toArray();
	
	$groupResult = function($cats, $slice, $table)use(&$groupResult){
		//echo json_encode( $cats) . " -> \n";
		//echo json_encode( $slice) . "\n";
		
		$grouped = [ "groupKey" => null, 
			"groupValue" => null, 
			"item" => [], 
			"subGroups" => [] ];
		
		//find item, cats null, slice
		$rowsForSubGroups = [];
		//echo count($table) . "\n";
		for($ri=0;$ri<count($table);$ri++){
			$row = $table[$ri];
			//cat fields should be null
			$catsNull = true;
			for($ci=0; $ci<count($cats); $ci++){
				//echo $row["acrprojectno"] . " | " . 
					//$row["TrackNo"] . " -> " .
					//$row[$cats[$ci]] . " === null -> " . 
					//($row[$cats[$ci]] === null ? "T" : "F") . ", \n";
				if($row[$cats[$ci]] !== null){
					//echo $row[$cats[$ci]];
					$catsNull = false;
					break;
				}
			}
			
			//should pass the slice
			$slicePassed = true;
			for($sli=0; $sli<count($slice); $sli++){
				$sliceItem = $slice[$sli];
				if($row[$sliceItem["fieldName"]] !== $sliceItem["expectedValue"]){
					$slicePassed = false;
					break;
				}
			}
			
			//now decide if it's an item or subject for sub-items
			if($catsNull && $slicePassed){
				$grouped["item"] = $row;
				if(count($slice) > 0){
					$grouped["groupKey"] = $slice[0]["fieldName"];
					$grouped["groupValue"] = $slice[0]["expectedValue"];
				}
			}else{
				$rowsForSubGroups[] = $row;
				
			}
		}
		
		//find sub items, remove first cat and add it to slice
		if(count($cats) > 0){
			$newSliceCat = array_shift($cats);
			$subItemCats = $cats; //array_splice($cats, 0);
			$newSliceValues = Phinq::create($rowsForSubGroups)
				->select(function($rfsi)use($newSliceCat){ return $rfsi[$newSliceCat]; })
				->distinct()
				->toArray();
			//echo $newSliceCat;
			//echo(json_encode($subItemCats));
			//echo(json_encode($rowsForSubGroups));
			//echo json_encode($newSliceValues);
			for($sici=0; $sici<count($newSliceValues); $sici++){
				
				$newSlice = 
					["fieldName" => $newSliceCat, 
					"expectedValue" => $newSliceValues[$sici]];
					//echo json_encode($newSlice);
				$subGroup = $groupResult($subItemCats, array_merge( $slice,[$newSlice]), $rowsForSubGroups);
				
				if(count($subGroup["item"]) > 0){
					$grouped["subGroups"][] = $subGroup;
				}
			}
		}
		
		return $grouped;
	};
	
	$groupedResult = $groupResult($initialCats, [], $sqlResult);*/
	
	$giqsData["dataTable"][] = $sqlResult;
}

$giqsData["timeGenerated"] = time();

echo json_encode($giqsData);

?>
