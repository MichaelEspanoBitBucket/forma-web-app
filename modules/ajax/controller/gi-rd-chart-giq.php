<?php
error_reporting(E_ALL);

/* Redirect if not authenticated */
if(!Auth::hasAuth('current_user')){
	http_response_code(401);
	echo "Failed to load chart data. User not logged in.";
} 

$auth = Auth::getAuth('current_user');

require_once(realpath('.') . "/library/gi-repositories/ReportRepository.php");
require_once(realpath('.') . "/library/Phinq/bootstrap.php");

$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
$rptRepo = new ReportRepository($conn, $auth);

$chartGiqs = null;



if(isset($_GET["id"])){
	//saved chart, get saved chart baby
	$chartGiqs = $rptRepo->getChartGiqs($_GET["id"]);
	//TODO:delete echo json_encode($chartGiqs); echo "	

	echo json_encode($chartGiqs);

}

?>
