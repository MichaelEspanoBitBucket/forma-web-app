<?php
	// error_reporting(E_ALL);

	/* Redirect if not authenticated */
	if(!Auth::hasAuth('current_user')){
		http_response_code(401);
		echo "Failed to load data models. User not logged in.";
	}

	$auth = Auth::getAuth('current_user');

	require_once(realpath('.') . "/library/gi-repositories/ReportDesignerRepository.php");

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
	$rdRepo = new ReportDesignerRepository($conn, $auth);
	//$data = null;
	
	
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');
	//var_dump($_GET['id'])
	// $search = new Search();
    $getPosition = $rdRepo->getAllPositionLevel();

	$data = json_encode($getPosition);
	
	echo $data;
	// ;

?>
