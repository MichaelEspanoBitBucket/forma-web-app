<?php
	error_reporting(E_ALL);

	/* Redirect if not authenticated */
	if(!Auth::hasAuth('current_user')){
		http_response_code(401);
		echo "Failed to load data models. User not logged in.";
	}

	$auth = Auth::getAuth('current_user');

	require_once(realpath('.') . "/library/gi-repositories/ReportDesignerRepository.php");

	$conn = new mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME);
	$rdRepo = new ReportDesignerRepository($conn, $auth);
	$data = null;
	
	
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-type: application/json');

	if($_GET['GetData'] == 'Paper_Sizes'){

		$data = $rdRepo->getAllPaperSizes();

	}elseif($_GET['GetData'] == 'Chart_Types'){

		$charts = $rdRepo->getAllCharts();

		foreach ($charts as $chart) {
			foreach ($rdRepo->getChartGiqSets($chart['id']) as $giqSets) {
				foreach ($rdRepo->getChartGiqSetAreas($giqSets['id']) as $area) {
					$giqSets['areas'][] = $area;
				}
				$chart['giq_sets'][]= $giqSets;
			}
			$data[] = $chart;
		}

	}
	
	$data = json_encode($data);

	echo $data;

?>
