<?php

//check if session is alive if not.. retun false;
if (!Auth::hasAuth('current_user')) {
    return false;
}

$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$userCompany = new userQueries();
$company_id = $auth['company_id'];
$userID = $auth['id'];
$timezone = "Asia/Manila";
$redis_cache = getRedisConnection();

if (function_exists('date_default_timezone_set'))
    date_default_timezone_set($timezone);

if (isset($_POST['action'])) {
    $action = $_POST['action'];
    $date = $fs->currentDateTime();

    if ($action == "loadGroupsDatatable") {
        $search_value = $_POST['search_value'];
        $start = $_POST['iDisplayStart'];
        $limit = "";
        $end = "10";
        if ($_POST['limit'] != "") {
            $end = $_POST['limit'];
        }
        if ($start != "") {
            $limit = " LIMIT $start, $end";
        }
        $orderBy = " ORDER BY group_name ASC";
        if ($_POST['column-sort']) {
            $orderBy = " ORDER BY " . $_POST['column-sort'] . " " . $_POST['column-sort-type'];
        }


        $query = "SELECT tbfg.*,  
                tbu_c.display_name as creator, tbu_u.display_name as editor FROM tbform_groups tbfg LEFT JOIN tbuser tbu_c on tbfg.created_by = tbu_c.id LEFT JOIN tbuser tbu_u ON tbfg.updated_by = tbu_u.id WHERE 
                    (group_name LIKE '%" . $search_value . "%' || description LIKE '%" . $search_value . "%' || 
                        tbfg.date_created LIKE '%$search_value%' || tbfg.date_updated LIKE '%$search_value%' ||
                        tbu_c.display_name LIKE '%" . $search_value . "%' || tbu_u.display_name LIKE '%" . $search_value . "%') 
                AND tbfg.is_active = 1 AND tbfg.company_id = '" . $company_id . "'" . $orderBy;

        $suggestion_sql = $db->query($query . $limit, "array");
        $countForm = $db->query($query, "numrows");

        if (!$countForm) {
            $countForm = 0;
        }


        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $countForm,
            "iTotalDisplayRecords" => $countForm,
            "start" => $start,
            "aaData" => array(),
        );
        foreach ($suggestion_sql as $info) {
            $getUsers = $db->query("select user_id from tbform_groups_users where group_id = '" . $info['id'] . "' and is_active = 1", "array");
            //$users = explode(",", $getUsers);
            $array = array();

            foreach ($getUsers as $key => $value_users) {
                $array['users'][] = $value_users['user_id'];
            }
            $users = json_encode($array);
            // Actions
            $company_user_info = array();
            $actions = '<center><i class="icon-trash fa fa-trash-o tip cursor deleteGroup" data-placement="top" data-original-title="Delete" style="margin-right:5px" data-id="' . $info['id'] . '"></i> <i class="icon-edit fa fa-pencil-square-o tip cursor updateGroup" data-placement="top" data-original-title="Edit" data-id="' . $info['id'] . '" data-name="' . $info['group_name'] . '" data-description="' . $info['description'] . '"></i><div class="display users_' . $info['id'] . '">' . $users . '</div></center>';

            $company_user_info[] = "<div class='fl-table-ellip'>" . $info['group_name'] . "</div>";
            $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($info['description'], "None") . "</div>";
            $company_user_info[] = "<div class='fl-table-ellip'>" . $info['creator'] . "</div>";
            $company_user_info[] = "<div class='fl-table-ellip'>" . $info['date_created'] . "</div>";
            $company_user_info[] = "<div class='fl-table-ellip'>" . $info['editor'] . "</div>";
            $company_user_info[] = "<div class='fl-table-ellip'>" . $info['date_updated'] . "</div>";
            $company_user_info[] = $actions;
            $output['aaData'][] = $company_user_info;
        }


        echo json_encode($output);
        // echo $query.$limit;
    } else if ($action == "deleteGroup") {
        $id = $_POST['id'];
        //update name, etc.
        $update = array("is_active" => 0);

        $where = array("id" => $id);
        $db->update("tbform_groups", $update, $where);

        $where = array("group_id" => $id);
        $db->update("tbform_groups_users", $update, $where);

        //deletememcached
        $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
        $form_record_memcached = array("form_list", "form_count");
        $nav_memcached = array("leftbar_nav");
        $starred_memcached = array("starred_list");

        $deleteMemecachedKeys = array_merge($request_record_memcacached, $form_record_memcached, $nav_memcached, $starred_memcached
        );
        $fs->deleteMemcacheKeys($deleteMemecachedKeys);
    } else if ($action == "addGroups") {
        $group_name = $_POST['group_name'];
        $group_description = $_POST['group_description'];

        $json_privacy = json_decode($_POST['users'], true);

        $getDuplicate = "SELECT * FROM tbform_groups WHERE group_name = '" . $group_name . "' AND company_id = '" . $auth['company_id'] . "' AND is_active = 1";
        $getDuplicate = $db->query($getDuplicate, "numrows");

        if ($getDuplicate > 0) {
            echo "0";
            return false;
        }

        $insert = array("group_name" => $group_name,
            "description" => $group_description,
            "created_by" => $auth['id'],
            "date_created" => $date,
            "updated_by" => $auth['id'],
            "date_updated" => $date,
            "company_id" => $auth['company_id'],
            "is_active" => 1);



        $group_id = $db->insert("tbform_groups", $insert);
        $group_users = $json_privacy['users'];
        foreach ($group_users as $key => $value) {
            $insert = array("group_id" => $group_id,
                "user_id" => $value,
                "is_active" => 1);

            $db->insert("tbform_groups_users", $insert);
        }
    } else if ($action == "editGroups") {
        $group_name = $_POST['group_name'];
        $group_description = $_POST['group_description'];
        $group_id = $_POST['group_id'];
        $json_privacy = json_decode($_POST['users'], true);

        //get duplicate
        $getDuplicate = "SELECT * FROM tbform_groups WHERE group_name = '" . $group_name . "' AND company_id = '" . $auth['company_id'] . "' AND id!='" . $group_id . "' AND is_active = 1";
        $getDuplicate = $db->query($getDuplicate, "numrows");


        //return if has duplicate
        if ($getDuplicate > 0) {
            echo "0";
            return false;
        }

        //update name, etc.
        $update = array("group_name" => $group_name,
            "description" => $group_description,
            "updated_by" => $auth['id'],
            "date_updated" => $date,
            "is_active" => 1);

        $where = array("id" => $group_id);
        $db->update("tbform_groups", $update, $where);


        //delete old members
        // $db->delete("tbform_groups_users",array("group_id"=>$group_id));
        //insert new members
        // $group_users = $json_privacy['users'];
        // foreach ($group_users as $key => $value) {
        //     $insert = array("group_id"=>$group_id,
        //                 "user_id"=>$value,
        //                 "is_active"=>1);
        //     $db->insert("tbform_groups_users",$insert);
        // }
        $jsonCheckedEle = $_POST['userCheckedEle'];
        $jsonUnCheckedEle = $_POST['userUnCheckedEle'];
        foreach ($jsonCheckedEle as $key => $valueChecked) {
            //insert
            foreach ($valueChecked as $key => $user) {
                $insert = array("group_id" => $group_id,
                    "user_id" => $user,
                    "is_active" => 1
                );
                echo $db->insert("tbform_groups_users", $insert);
            }
        }
        foreach ($jsonUnCheckedEle as $key => $valueUnChecked) {
            $user_type = functions::setUserType($key);

            //delete
            foreach ($valueUnChecked as $key => $user) {
                $insert = array("group_id" => $group_id,
                    "user_id" => $user,
                    "is_active" => 1
                );
                $db->delete("tbform_groups_users", $insert);
            }
        }

        //deletememcached
        $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
        $form_record_memcached = array("form_list", "form_count");
        $nav_memcached = array("leftbar_nav");
        $starred_memcached = array("starred_list");


        $deleteMemecachedKeys = array_merge($request_record_memcacached, $form_record_memcached, $nav_memcached, $starred_memcached
        );
        $fs->deleteMemcacheKeys($deleteMemecachedKeys);

        if ($redis_cache) {
            $redis_cache->flushall();
        }
    }
}
?>