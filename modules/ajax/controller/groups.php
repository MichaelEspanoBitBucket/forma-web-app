<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$search = new Search();
if(isset($_POST['action'])){

	$date = $fs->currentDateTime();
	switch($_POST['action']){
		
		case "create_update":
			$type = strtolower($_POST['type']);
			$name = $_POST['name'];
			$users = implode(",",$_POST['users']);

			if($type=="create"){
				$getExistingGroupStrQuery = "SELECT count(id) AS count FROM tbgroups WHERE name = '".$name."' AND user_id='". $auth['id'] ."' AND is_active=1 LIMIT 0,1";
				$getExistingGroup = $db->query($getExistingGroupStrQuery,"row");
				if($getExistingGroup['count']>0){
					echo "existing";
					return;
				}

				$insert = array("user_id"=>$auth['id'],
								"name"=>$name,
								"members"=>$users,
								"is_active"=>"1");
				echo $db->insert("tbgroups",$insert);
			}else{
				$id = $_POST['id'];
				$getExistingGroupStrQuery = "SELECT count(id) AS count FROM tbgroups WHERE name = '".$name."' AND user_id='". $auth['id'] ."' AND id != $id AND is_active=1 LIMIT 0,1";
				$getExistingGroup = $db->query($getExistingGroupStrQuery,"row");
				if($getExistingGroup['count']>0){
					echo "existing";
					return;
				}
				
				$update = array("name"=>$name,
								"members"=>$users);
				$where = array("id"=>$id);
				$db->update("tbgroups",$update,$where);
			}
			break;
		case "loadGroups":
			$getGroups = $db->query("SELECT * FROM tbgroups WHERE user_id = ". $auth['id'] ." AND is_active=1","array");
			echo json_encode($getGroups);
			break;
		case "loadGroupsMembers" :
			$id = $_POST['id'];
			$json = array();
			$getGroup = $db->query("SELECT * FROM tbgroups WHERE id = ". $id ."","row");
			$getMemebers_array = explode(",",$getGroup['members']);
			foreach($getMemebers_array as $data_id){
				$getUser = $db->query("SELECT id,first_name,last_name,display_name FROM tbuser WHERE id = ".$data_id,"row");
				array_push($json, $getUser);
			}
			echo json_encode($json);
			break;
		case "deleteGroup" :
			$id = $_POST['id'];
			$update = array("is_active"=>"0");
			$where = array("id"=>$id);
			$db->update("tbgroups",$update,$where);

	}
	
}

?>
