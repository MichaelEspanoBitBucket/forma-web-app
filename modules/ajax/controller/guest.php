<?php
$db = new Database;
$fs = new functions;
$auth = Auth::getAuth('current_user');
$guest = new Guest_Account();
$date = $fs->currentDateTime();
$mail = new Mail_Notification();
//error_reporting(E_ALL);
if(isset($_POST['action'])){
    
    //if($_POST['action'] == "add_guest"){
    //    $guest_email = $_POST['guest_email'];
    //    $data_action = $_POST['data_action'];
    //    $query = $db->query("SELECT * FROM tbuser WHERE email = {$db->escape($guest_email)}","numrows");
    //    if($query == "1"){
    //        echo "Email is already registered.";
    //    }else{
    //        $password = $fs->encrypt_decrypt("encrypt", "password");
    //        $json = array("email"   =>  $guest_email,
    //                      "password"=>  $password,
    //                      "first_name"=>"Anonymous",
    //                      "last_name"=>"Guest User");
    //        $query = $guest->save_new_guest($db,$auth,$json);
    //        
    //        // Insert in guest tbl
    //        $json_encode = json_encode(array(   "data_action"  =>  $data_action,
    //                                            "guest_email"  =>  $guest_email,
    //                                            "guest_url"  =>  "all"));
    //       
    //        $insert = array("guest_user_id"     =>  $query['id'],
    //                        "guest_json"        =>  $json_encode,
    //                        "guest_type"        =>  $data_action,
    //                        "guest_date"        =>  $date,
    //                        "guest_is_active"   =>  "1");
    //        $db->insert("tb_guest",$insert);
    //        
    //        echo "Successful";
    //    }
    //    
    //}else
    
    if($_POST['action'] == "guest_password"){
        $new_password = $_POST['new_password'];
        $retype_password = $_POST['retype_password'];
        $password = $fs->encrypt_decrypt("encrypt", $new_password);
        $user_id = $_POST['user_id'];
        $set = array("password"=>$password,
                     "email_activate"=>"1",
                     "is_active"=>"1");
        $condition = array("id"=>$user_id);
        $db->update("tbuser",$set,$condition);
        
        echo "Successful";
        
    }else if($_POST['action'] == "selected_module_for_guest"){
        $guest_email = $_POST['guest_email'];
        $data_action = $_POST['data_action'];
        $get_cur_url = $_POST['get_cur_url'];
        $requestID = $_POST['requestID'];
        $getFormID = $_POST['getFormID'];
        $getTrackNo = $_POST['getTrackNo'];
        
        $query_rows = $db->query("SELECT * FROM tbuser WHERE email = {$db->escape($guest_email)} AND user_level_id={$db->escape(4)} AND email_activate={$db->escape(1)} AND is_active={$db->escape(1)}
                                 OR
                                 email = {$db->escape($guest_email)} AND user_level_id={$db->escape(4)} AND email_activate={$db->escape(0)} AND is_active={$db->escape(0)}","numrows");
        $query_rows_in_active = $db->query("SELECT * FROM tbuser WHERE email = {$db->escape($guest_email)} AND user_level_id={$db->escape(4)} AND email_activate={$db->escape(1)} AND is_active={$db->escape(0)}","numrows");
        
        $q = $db->query("SELECT * FROM tbuser WHERE email = {$db->escape($guest_email)} AND user_level_id={$db->escape(4)} AND email_activate={$db->escape(1)} AND is_active={$db->escape(1)}
                    OR
                    email = {$db->escape($guest_email)} AND user_level_id={$db->escape(4)} AND email_activate={$db->escape(0)} AND is_active={$db->escape(0)}
                    OR
                    email = {$db->escape($guest_email)} AND user_level_id={$db->escape(4)} AND email_activate={$db->escape(1)} AND is_active={$db->escape(0)}
                    ","row");
        
        $guest_num = $db->query("SELECT * FROM tb_guest WHERE guest_user_id={$db->escape($q['id'])} AND guest_formID = {$db->escape($getFormID)} AND guest_requestID={$db->escape($requestID)} AND  guest_trackNo={$db->escape($getTrackNo)} AND guest_is_active=1","numrows");
        
        
        
        if($query_rows_in_active == "0"){
            if($guest_num == "0"){
                if($query_rows == "0"){
                    $password = $fs->encrypt_decrypt("encrypt", "password");
                    $explode_email = explode("@",$guest_email);
                    $json = array("email"   =>  $guest_email,
                                  "password"=>  $password,
                                  "first_name"=>$explode_email[0],
                                  "last_name"=>"",
                                  "display_name"    =>  $explode_email[0],
                                  "type"    =>"register_guest",
                                  "reg_type"	=> "1",
                                  "date_registered"=>$date);
                    $query = $guest->save_new_guest($db,$auth,$json);
                    // Add in the table of guest expiration
                    $futureDate	= date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($date)) . " + 181 day"));
                    $insert = array("user_id"  =>  $query['id'],
                                    "expiration_date"   =>  $futureDate,
                                    "is_active" =>  "1");
                    $db->insert("tb_guest_expiration",$insert); 
                }else{
                    $data = array("requestID"   =>  $requestID,
                                  "formID"      =>  $getFormID,
                                  "trackNo"     =>  $getTrackNo);
                    
                    $query = $db->query("SELECT * FROM tbuser WHERE email = {$db->escape($guest_email)} AND user_level_id={$db->escape(4)}","row");
                    $mail->request_add_guest(array($guest_email=>"Anonymous Guest User"),"",$auth['company_id'],$auth,$data,"1");
                }
                $json_encode = json_encode(array(   "data_action"  =>  $data_action,
                                                    "guest_email"  =>  $guest_email,
                                                    "guest_url"  =>  $get_cur_url));
               
                $insert = array("guest_user_id"     =>  $query['id'],
                                "guest_json"        =>  $json_encode,
                                "guest_type"        =>  $data_action,
                                "guest_date"        =>  $date,
                                "guest_added_by"    =>  $auth['id'],
                                "guest_formID"      =>  $getFormID,
                                "guest_requestID"   =>  $requestID,
                                "guest_trackNo"     =>  $getTrackNo,
                                "guest_is_active"   =>  "1");
                $db->insert("tb_guest",$insert);
                
                
                
                // Request Log
                    
                    $logs = array("form_id"     =>  $getFormID,
                                  "request_id"  =>  $requestID,
                                  "details"     =>  $guest_email . " was added as a guest user.",
                                  "date_created"=>  $date,
                                  "date_updated"=>  $date,
                                  "created_by"=>  $auth['id'],
                                  "updated_by"=>  $auth['id']);
                    
                    $db->insert("tbrequest_logs",$logs);
              
                echo "Successful";
            }else{
                echo "This email is already guest in this request.";
            }
        }else{
            echo "The entered guest email is currently inactive. You may request the activation from your System Administrator.";
        }
        
    }
    // View List
    else if($_POST['action'] == "view_list"){
        $formID = $_POST['getFormID'];
        $requestID = $_POST['requestID'];
        $getTrackNo = $_POST['getTrackNo'];
        
        
        $query = $db->query("SELECT * FROM tb_guest g
                            LEFT JOIN tbuser u ON g.guest_user_id=u.id
                            WHERE g.guest_formID = {$db->escape($formID)}
                            AND g.guest_requestID = {$db->escape($requestID)}
                            AND g.guest_trackNo = {$db->escape($getTrackNo)}","array");
        $query_numrows = $db->query("SELECT * FROM tb_guest WHERE guest_formID = {$db->escape($formID)}
                                    AND guest_requestID = {$db->escape($requestID)}
                            AND guest_trackNo = {$db->escape($getTrackNo)}","numrows");
        if($query_numrows == "0"){
            echo "none";
        }else{
            //foreach($query as $q){
            //    if($q['id'] != ""){
                    echo json_encode($query);
            //    }
            //}
        }
        
    }
    // Delete Guest in request
    else if($_POST['action'] == "remove_guest_in_form"){
        $guestID = $_POST['guestID'];
        $del = array("guest_id" =>  $guestID);
        $db->delete("tb_guest",$del);
        
        echo "Guest was successfully deleted.";
    }
}

?>