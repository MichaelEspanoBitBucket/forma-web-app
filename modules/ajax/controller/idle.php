<?php
$db = new Database();
$auth = Auth::getAuth('current_user');
$action = $_POST['action'];

if(isset($action)){
    
    switch($action){
        case "userAway":
            $personDoc = new Person($db,$auth['id']);
            $personDoc->is_available = '0';
            $personDoc->update();
            break;
        
        case "userBack":
            $personDoc = new Person($db,$auth['id']);
            $personDoc->is_available = '1';
            $personDoc->update();
            break;
        case "reset":
            $personDoc = new Person($db,$auth['id']);
            $personDoc->timeout = '0';
            $personDoc->update();
            break;
        case "getReset":
            $personDoc = new Person($db,$auth['id']);
            $timeout = $personDoc->timeout;
            echo $timeout;
            break;
        
        case "update":
            $personDoc = new Person($db,$auth['id']);
            $personDoc->timeout = $_POST['idleTime'];
            $personDoc->update();
            echo $_POST['idleTime'];
            break;
            
    }
    
}


?>