<?php
$auth = Auth::getAuth('current_user');
$db = new Database;

$keySearch = $_GET['Keyword'];
if ($keySearch) {
    $selector = "WHERE company_id= " . $auth['company_id'] . " AND "
            . " (code LIKE '%$keySearch%' OR description LIKE '%$keySearch%' "
            . "OR valuecode LIKE '%$keySearch%' OR valuedescription LIKE '%$keySearch%') ORDER BY code LIMIT 10";
    $keywords = functions::getKeywords($selector);
} else {
    $selector = "WHERE company_id= " . $auth['company_id'] . " ORDER BY code LIMIT 10";
    $keywords = functions::getKeywords($selector);
}

if (count($keywords) == 0) {
    ?>
    <div class="notification_wrapper" style="height:auto">
        <div class="wrong" style="width:98.5%">
            <div class="padding_5">
                <img src="/images/warning/wrong.png" width="22" height="19" class="img-notification-position">
                <div class="notification-position">No Record Found</div>
            </div>
        </div>
    </div>
    <?php
}

foreach ($keywords as $keywordDoc) {
    $ret .= '<div class="hr"></div>';
    $ret .= '<div class="supTicket nobg">';
    $ret .= '<div class="issueType keywordheader" code="' . $keywordDoc->code . '" description="' . $keywordDoc->description . '">';
    $ret .= '<span class="issueInfo">' . $keywordDoc->description . '</span>';
    $ret .= '<span class="issueNum">';

    if ($auth['user_level_id'] == "2") {
        $ret .= '<a href="#"  keyword-id="' . $keywordDoc->id . '" class="tip cursor editkeyword" data-original-title="Edit ' . $keywordDoc->value_description . '" data-placement="top" id="" data-orgchart-id=""><button class="btn-basicBtn padding_5 cursor"><i class="icon-edit" ></i></button></a> ';
        $ret .= '<a class="tip cursor update_status_workflow" data-original-title="' . functions::getStatus_revert($keywordDoc->is_active) . '" data-status="' . $keywordDoc->is_active . '" data-placement="top" id="" data-id="' . $reportDoc->id . '"><button class="btn-basicBtn padding_5 cursor"><i class="icon-globe"></i></button></a>';
    }

    $ret .= '</span>';

    $ret .= '</div>';

    $ret .= '<div class="issueSummary">';
    $ret .= '<a href="#" title="" class="pull-left"><i style="height:30px; width:30px;font-size:30px;color: #676767;" class="icon-key"></i></a>';
    $ret .= '<div class="ticketInfo">';
    $ret .= '<ul>';
    $ret .= '<li><a href="" title="">' . $keywordDoc->value_description . '</a></li>';
    $ret .= '<li class="even"><strong class="red">[ Keyword ]</strong></li>';
    $ret .= '<li>Status: <strong class="green">[ ' . functions::getStatus($keywordDoc->is_active) . ' ]</strong></li>';
    $ret .= '<li class="even">' . $keywordDoc->date_created . '</li>';
    $ret .= '</ul>';
    $ret .= '</div>';
    $ret .= '</div>';
    $ret .= '</div>';
    $ret .= '<div class="hr"></div>';
}
echo $ret;
?>
