<?php

$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;

$date = $fs->currentDateTime();
$id = $_POST['ID'];

$companyDoc = new Company($db, $auth['company_id']);
$personDoc = new Person($db, $auth['id']);

if ($id == 0) {
    //new doc
    $keywordDoc = new Keyword($db);
    $keywordDoc->code = $_POST['Code'];
    $keywordDoc->description = $_POST['Description'];
    $keywordDoc->value_code = $_POST['Value_Code'];
    $keywordDoc->value_description = $_POST['Value_Description'];
    $keywordDoc->quantity = $_POST['Quantity'];
    $keywordDoc->company = $companyDoc;
    $keywordDoc->created_by = $personDoc;
    $keywordDoc->date_created = $date;
    $keywordDoc->is_active = 1;
    $keywordDoc->save();
    echo 'Keyword(' . $keywordDoc->value_description . ') has been successfully saved.';
} else {
    //edit doc
    $keywordDoc = new Keyword($db, $id);
    $keywordDoc->code = $_POST['Code'];
    $keywordDoc->description = $_POST['Description'];
    $keywordDoc->value_code = $_POST['Value_Code'];
    $keywordDoc->value_description = $_POST['Value_Description'];
    $keywordDoc->quantity = $_POST['Quantity'];
    $keywordDoc->company = $companyDoc;
    $keywordDoc->date_updated = $date;
    $keywordDoc->updated_by = $personDoc;
    $keywordDoc->is_active = 1;
    $keywordDoc->update();
    echo 'Keyword(' . $keywordDoc->value_description . ') has been successfully updated.';
}
?>
