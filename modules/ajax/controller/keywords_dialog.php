<?php

$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$date = $fs->currentDateTime();
//$keywords = functions::getKeywords("WHERE company_id={$db->escape($auth['company_id'])}");

$action = $_GET['Mode'];
$code = $_GET['Code'];
$search = $_GET['Search'];


switch ($action) {
    case "KeywordDialog":
        $search_query = " AND (Code LIKE '%{$search}%' OR Description LIKE '%{$search}%')";
        $keywords = $db->query("SELECT DISTINCT Code, Description FROM tbkeyword WHERE company_id={$db->escape($auth['company_id'])} " . $search_query, "array");
        
        break;
    case "KeywordPicklist":
        $search_query = " AND (ValueCode LIKE '%{$search}%' OR ValueDescription LIKE '%{$search}%')";
        $keywords = $db->query("SELECT ValueCode as Code, ValueDescription as Description FROM tbkeyword WHERE Code = {$db->escape($code)} AND company_id={$db->escape($auth['company_id'])} " . $search_query, "array");
       
        break;
}

print_r(json_encode($keywords));
?>
