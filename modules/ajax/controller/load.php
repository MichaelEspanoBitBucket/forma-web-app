<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}

$conn = getCurrentDatabaseConnection();
$auth = Auth::getAuth('current_user');
$db = new Database();
$userCompany = new userQueries();
$post_data = filter_input_array(INPUT_POST);
// header('Content-Type: application/json');

$action = $_POST['action'];

if (isset($action)) {

    //Load Table data

    switch ($action) {
        case "companyUserActive":
            $res = "";
            $out = "";
            $fields_out = "";
            $company_user_info = array();
            $fields = array();

            //$companyUsers_fields = $db->query("SHOW COLUMNS FROM tbuser");
            $company = $userCompany->getCompany($auth['company_id']);
            $companyUsers = $db->query("SELECT u.*,o.department as depName
                                               FROM tbuser u
                                               LEFT JOIN tborgchartobjects o on o.id=u.department_id
                                               WHERE u.company_id={$db->escape($auth['company_id'])}
                                                    AND u.is_active='1'
                                                    AND u.user_level_id!='4'
                                                ORDER BY u.date_registered DESC", "array");
            $countRows = $db->query("SELECT u.*,o.department as depName
                                               FROM tbuser u
                                               LEFT JOIN tborgchartobjects o on o.id=u.department_id
                                               WHERE u.company_id={$db->escape($auth['company_id'])}
                                               AND u.user_level_id!='4'
                                                    AND u.is_active='1'", "numrows");
            // Table fields
            //foreach($companyUsers as $info){
            //    $company_user_info[] = $info;
            //}


            foreach ($companyUsers as $info) {
                // Actions
                $actions = '<center><div class="icon-edit fl-margin-right fa fa-pencil cursor editUserListInfo" id="editUserListInfo_' . $info['id'] . '" data-type="user_deactivate" data-user-id="' . $info['id'] . '"></div>' .
                        '<div class="icon-trash fa fa-trash-o cursor deleteUserListInfo" id="deleteUser_' . $info['id'] . '" data-type="deactivate" data-user-id="' . $info['id'] . '"></div></center>';

                // $checkbox = '<center><input type="checkbox" data-id="'.$info['id'].'" name="selectCheckBox[]" class="selectCheckBox" ></center>';
                if (ALLOW_ACTIVE_DIRECTORY == "1") {
                    $company_user_info[] = array(
                        "userCheckbox" => $checkbox,
                        "userID" => $info['id'],
                        "image" => $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar"),
                        "email" => $info['email'],
                        "username" => $info['username'],
                        "displayName" => $info['display_name'],
                        "fullname" => $info['first_name'] . " " . $info['last_name'],
                        "position" => $info['position'],
                        "department" => $info['depName'],
                        "dateRegistered" => $info['date_registered'],
                        "actions" => $actions);
                } else {
                    $company_user_info[] = array(
                        "userCheckbox" => $checkbox,
                        "userID" => $info['id'],
                        "image" => $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar"),
                        "email" => $info['email'],
                        //"username"=>'',
                        "displayName" => $info['display_name'],
                        "fullname" => $info['first_name'] . " " . $info['last_name'],
                        "position" => $info['position'],
                        "department" => $info['depName'],
                        "dateRegistered" => $info['date_registered'],
                        "actions" => $actions);
                }
            }
            $res = array(/* "sEcho"                        =>      "3",
                  "iTotalRecords"                =>      $countRows,
                  "iTotalDisplayRecords"         =>      $countRows, */
                "company_user" => $company_user_info);

            $string = json_encode($res);

            echo functions::view_json_formatter($string);
            break;

        case "companyUserActiveDatatable":
            $res = "";
            $company_user_info = array();
            $start = $post_data['iDisplayStart'];
            $company = $userCompany->getCompany($auth['company_id']);
            $search = $post_data['search_value'];
            $orderBy = " ORDER BY u.email ASC";
            if ($post_data['column-sort']) {
                $column_sort = $post_data['column-sort'];
                $column_sort_type = $post_data['column-sort-type'];
                if ($post_data['column-sort'] == "position") {
                    $column_sort = "p.position";
                }
                $orderBy = " ORDER BY " . $column_sort . " " . $column_sort_type;
            }
            $limit = "";
            $end = "10";
            if ($post_data['limit'] != "") {
                $end = $post_data['limit'];
            }
            if ($start != "") {
                $limit = " LIMIT $start, $end";
            }

            $query = "SELECT 
                        u.id,
                        u.email,
                        u.password,
                        u.username,
                        u.display_name,
                        u.first_name,
                        u.last_name,
                        u.date_registered,
                        p.position as position_name,
                        o.department as depName,
                        (SELECT group_concat(tbfg.group_name) FROM tbform_groups tbfg LEFT JOIN tbform_groups_users tbfgu ON tbfg.id = tbfgu.group_id WHERE tbfgu.user_id = u.id AND tbfg.is_active = 1) as form_group 
                    FROM
                        tbuser u
                            LEFT JOIN
                        tbpositions p ON u.position = p.id
                            LEFT JOIN
                        tbdepartment_users du ON du.user_id = u.id
                            LEFT JOIN
                        tborgchart org ON org.id = du.orgChart_id AND org.status = 1
                            LEFT JOIN
                        tborgchartobjects o ON o.orgChart_id = org.id AND o.department_code = du.department_code
                    WHERE
                        u.company_id = {$conn->escape($auth['company_id'])}
                            AND u.user_level_id != '4'
                            AND u.is_active = '1'
                                    AND (u.email LIKE '%{$search}%'
                            || u.first_name LIKE '%{$search}%'
                            || u.username LIKE '%{$search}%'
                            || u.last_name LIKE '%{$search}%'
                            || u.display_name LIKE '%{$search}%'
                            || u.date_registered LIKE '%{$search}%'
                            || p.position LIKE '%{$search}%'
                            || o.department LIKE '%{$search}%'
                            || (SELECT group_concat(tbfg.group_name) FROM tbform_groups tbfg LEFT JOIN tbform_groups_users tbfgu ON tbfg.id = tbfgu.group_id WHERE tbfgu.user_id = u.id AND tbfg.is_active = 1) LIKE '%{$search}%'
                        ) 
                    GROUP BY u.id";


            if ($conn->is_connected) {
                $companyUsers = $conn->query($query . " $orderBy $limit");
                $countRows = $conn->query($query, "numrows");

                $res = array("sEcho" => intval($post_data['sEcho']),
                    "iTotalRecords" => $countRows,
                    "iTotalDisplayRecords" => $countRows,
                    "aaData" => array());
                foreach ($companyUsers as $info) {
                    $company_user_info = array();
                    // Actions
                    $actions = '<center><div class="icon-edit fl-margin-right fa fa-pencil cursor editUserListInfo tip" title="Edit" id="editUserListInfo_' . $info['id'] . '" data-type="user_activate" data-user-id="' . $info['id'] . '"></div>' .
                            '<div class="icon-trash fa fa-trash-o cursor deleteUserListInfo tip" title="Deactivate" id="deleteUser_' . $info['id'] . '" data-type="deactivate" data-user-id="' . $info['id'] . '"></div></center>';

                    $checkbox = '<center><input type="checkbox" data-id="' . $info['id'] . '" name="selectCheckBox[]" class="selectCheckBox css-checkbox" id="selectCheckBox_' . $info['id'] . '"><label for="selectCheckBox_' . $info['id'] . '" class="css-label"></label></center>';

                    //$company_user_info[] = $checkbox;
                    if (ALLOW_ACTIVE_DIRECTORY == "1") {
                        $company_user_info[] = '<div align="center">' . $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar");
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['email'] . '</div>';
                        // $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $password . '</div>';
                        if(SHOW_ENCRYPTED_PASSWORD == "1"){
                            $decrypted_password = functions::encrypt_decrypt('decrypt',$info['password']);
                            $password = $decrypted_password;
                            $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $password . '</div>';
                        }
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['username'] . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['display_name'] . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['first_name'] . " " . $info['last_name'] . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . functions::empty_in_list($info['position_name'], "None") . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip tip" style="margin-left:0px" title="' . $info['depName'] . '">' . functions::empty_in_list($info['depName'], "None") . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip tip" style="margin-left:0px" title="' . $info['form_group'] . '">' . functions::empty_in_list($info['form_group'], "None") . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['date_registered'] . '</div>';
                        $company_user_info[] = $actions;
                    } else {
                        $company_user_info[] = '<div align="center">' . $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar");
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['email'] . '</div>';
                        if(SHOW_ENCRYPTED_PASSWORD == "1"){
                            $decrypted_password = functions::encrypt_decrypt('decrypt',$info['password']);
                            $password = $decrypted_password;
                            $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $password . '</div>';
                        }
                        //$company_user_info[] = '';
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['display_name'] . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['first_name'] . " " . $info['last_name'] . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . functions::empty_in_list($info['position_name'], "None") . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip tip" style="margin-left:0px" title="' . $info['depName'] . '">' . functions::empty_in_list($info['depName'], "None") . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip tip" style="margin-left:0px" title="' . $info['form_group'] . '">' . functions::empty_in_list($info['form_group'], "None") . '</div>';
                        $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['date_registered'] . '</div>';
                        $company_user_info[] = $actions;
                    }
                    $res['aaData'][] = $company_user_info;
                }

                $conn->disConnect();
                exit(json_encode($res));
            }

            break;
        case "companyUserDeactivated":
            $res = "";
            $out = "";
            $fields_out = "";
            $company_user_info = array();
            $fields = array();

            //$companyUsers_fields = $db->query("SHOW COLUMNS FROM tbuser");
            $company = $userCompany->getCompany($auth['company_id']);
            $companyUsers = $db->query("SELECT *
                                               FROM tbuser WHERE company_id={$db->escape($auth['company_id'])}
                                                    AND is_active='0'
                                                    AND user_level_id!='4'
                                                ORDER BY date_registered DESC/* LIMIT 0, 10*/", "array");
            $countRows = $db->query("SELECT *
                                               FROM tbuser WHERE company_id={$db->escape($auth['company_id'])}
                                               AND user_level_id!='4'
                                                    AND is_active='0'/* LIMIT 0, 10*/", "numrows");
            // Table fields
            //foreach($companyUsers as $info){
            //    $company_user_info[] = $info;
            //}


            foreach ($companyUsers as $info) {
                // Actions
                $actions = '<center><div class="icon-edit  cursor fa fa-pencil editUserListInfo" id="editUserListInfo_' . $info['id'] . '" data-type="activate" data-user-id="' . $info['id'] . '"></div>';
                //'<div class="icon-trash cursor deleteUserListInfo" id="deleteUser_'.$info['id'].'" data-type="activate" data-user-id="'.$info['id'].'"></div></center>';

                $checkbox = '<center><input type="checkbox" data-id="' . $info['id'] . '" name="selectCheckBox[]" class="selectCheckBox" ></center>';

                if (ALLOW_ACTIVE_DIRECTORY == "1") {
                    $company_user_info[] = array(
                        "userCheckbox" => $checkbox,
                        "userID" => $info['id'],
                        "image" => $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar"),
                        "email" => $info['email'],
                        "username" => $info['username'],
                        "displayName" => $info['display_name'],
                        "fullname" => $info['first_name'] . " " . $info['last_name'],
                        "position" => $info['position'],
                        "department" => $info['depName'],
                        "dateRegistered" => $info['date_registered'],
                        "actions" => $actions);
                } else {
                    $company_user_info[] = array(
                        "userCheckbox" => $checkbox,
                        "userID" => $info['id'],
                        "image" => $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar"),
                        "email" => $info['email'],
                        //"username"=>'',
                        "displayName" => $info['display_name'],
                        "fullname" => $info['first_name'] . " " . $info['last_name'],
                        "position" => $info['position'],
                        "department" => $info['depName'],
                        "dateRegistered" => $info['date_registered'],
                        "actions" => $actions);
                }
            }
            $res = array(/* "sEcho"                        =>      "3",
                  "iTotalRecords"                =>      $countRows,
                  "iTotalDisplayRecords"         =>      $countRows, */
                "company_user" => $company_user_info);

            $string = json_encode($res);

            echo functions::view_json_formatter($string);
            break;
        case "companyUserDeactivatedDatatable":
            $res = "";
            $start = $_POST['iDisplayStart'];
            $search = $_POST['search_value'];
            $orderBy = " ORDER BY u.email ASC";
            if ($_POST['column-sort']) {
                $orderBy = " ORDER BY " . $_POST['column-sort'] . " " . $_POST['column-sort-type'];
            }
            $company = $userCompany->getCompany($auth['company_id']);
            // $companyUsers = $db->query("SELECT u.*,o.department as depName
            //                            FROM tbuser u
            //                            LEFT JOIN tborgchartobjects o on o.id=u.department_id
            //                            WHERE u.company_id={$db->escape($auth['company_id'])}
            //                                 AND u.is_active='0' AND (u.email LIKE '%$search%' || u.first_name LIKE '%$search%' || u.last_name LIKE '%$search%' || u.position LIKE '%$search%')
            //                             $orderBy LIMIT $start,10","array");
            // $countRows = $db->query("SELECT *
            //                            FROM tbuser WHERE company_id={$db->escape($auth['company_id'])}
            //                                 AND is_active='0' AND (email LIKE '%$search%' || first_name LIKE '%$search%' || last_name LIKE '%$search%' || position LIKE '%$search%')","numrows");
            // $query = "SELECT u.*,o.department as depName, p.position as position_name
            //                        FROM tbuser u
            //                        LEFT JOIN tborgchartobjects o on o.id=u.department_id
            //                        LEFT JOIN tbpositions p on u.position = p.id 
            //                        WHERE u.company_id={$db->escape($auth['company_id'])}
            //                             AND u.is_active='0' AND (u.email LIKE '%$search%' || u.first_name LIKE '%$search%' || u.last_name LIKE '%$search%' || p.position LIKE '%$search%')";
            $limit = "";
            $end = "10";
            if ($_POST['limit'] != "") {
                $end = $_POST['limit'];
            }
            if ($start != "") {
                $limit = " LIMIT $start, $end";
            }
            $query = "SELECT u.*, p.position as position_name, (SELECT group_concat(tboo.department) FROM tbdepartment_users tbdu LEFT JOIN tborgchart tbo ON tbo.id = tbdu.orgchart_id LEFT JOIN tborgchartobjects tboo ON tbdu.department_code = tboo.department_code AND tbdu.orgChart_id = tboo.orgChart_id WHERE user_id = u.id AND tbo.status = 1 AND department !='') as depName , 
                                            (SELECT group_concat(tbfg.group_name) FROM tbform_groups tbfg LEFT JOIN tbform_groups_users tbfgu ON tbfg.id = tbfgu.group_id WHERE tbfgu.user_id = u.id AND tbfg.is_active = 1) as form_group 
                                               FROM tbuser u
                                               LEFT JOIN tborgchartobjects o on o.id=u.department_id
                                               LEFT JOIN tbpositions p on u.position = p.id
                                               WHERE u.company_id={$db->escape($auth['company_id'])}
                                                AND u.user_level_id!='4'
                                                    AND u.is_active='0' AND (u.email LIKE '%$search%' || u.first_name LIKE '%$search%' || u.last_name LIKE '%$search%' || u.display_name LIKE '%$search%' || u.date_registered LIKE '%$search%' || p.position LIKE '%$search%' || (SELECT group_concat(tboo.department) FROM tbdepartment_users tbdu LEFT JOIN tborgchart tbo ON tbo.id = tbdu.orgchart_id LEFT JOIN tborgchartobjects tboo ON tbdu.department_code = tboo.department_code AND tbdu.orgChart_id = tboo.orgChart_id WHERE user_id = u.id AND tbo.status = 1 AND department !='')  LIKE '%$search%'
                                                            || (SELECT group_concat(tbfg.group_name) FROM tbform_groups tbfg LEFT JOIN tbform_groups_users tbfgu ON tbfg.id = tbfgu.group_id WHERE tbfgu.user_id = u.id AND tbfg.is_active = 1) LIKE '%$search%')";
            $companyUsers = $db->query($query . " $orderBy $limit", "array");
            $countRows = $db->query($query, "numrows");
            $res = array("sEcho" => intval($_POST['sEcho']),
                "iTotalRecords" => $countRows,
                "iTotalDisplayRecords" => $countRows,
                "aaData" => array());

            foreach ($companyUsers as $info) {
                $company_user_info = array();
                // Actions
                $actions = '<center><div class="icon-edit  cursor fa fa-pencil editUserListInfo tip" title="Edit" id="editUserListInfo_' . $info['id'] . '" data-type="activate" data-user-id="' . $info['id'] . '"></div>';
                //'<div class="icon-trash cursor deleteUserListInfo" id="deleteUser_'.$info['id'].'" data-type="activate" data-user-id="'.$info['id'].'"></div></center>';

                $checkbox = '<center><input type="checkbox" data-id="' . $info['id'] . '" name="selectCheckBox[]" class="selectCheckBox css-checkbox" id="selectCheckBox2_' . $info['id'] . '"><label for="selectCheckBox2_' . $info['id'] . '" class="css-label"></label></center>';

                //$company_user_info[] = $checkbox;
                if (ALLOW_ACTIVE_DIRECTORY == "1") {
                    $company_user_info[] = '<div align="center">' . $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar");
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['email'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['username'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['display_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['first_name'] . " " . $info['last_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['position_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip tip" style="margin-left:0px" title="' . $info['depName'] . '">' . $info['depName'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip tip" style="margin-left:0px" title="' . $info['form_group'] . '">' . $info['form_group'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['date_registered'] . '</div>';
                    $company_user_info[] = $actions;
                } else {
                    $company_user_info[] = '<div align="center">' . $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar");
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['email'] . '</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px"></div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['display_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['first_name'] . " " . $info['last_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['position_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip tip" style="margin-left:0px" title="' . $info['depName'] . '">' . $info['depName'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip tip" style="margin-left:0px" title="' . $info['form_group'] . '">' . $info['form_group'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['date_registered'] . '</div>';
                    $company_user_info[] = $actions;
                }
                $res['aaData'][] = $company_user_info;
            }


            $string = json_encode($res);

            echo $string;
            break;
        case "auditLogs":
            $res = "";
            $out = "";
            $fields_out = "";
            $audit_logs = array();
            $fields = array();

            //$companyUsers_fields = $db->query("SHOW COLUMNS FROM tbuser");
            $company = $userCompany->getCompany($auth['company_id']);
            $getAuditLog = $db->query("SELECT *,a.id as auditID,a.user_id as uID,
                                              au.value as action,a.date as dateTime ,
                                              a.ip as ipAddress
                                              FROM tbaudit_logs a
                                              LEFT JOIN tbuser u on u.id=a.user_id
                                              LEFT JOIN tbaudit_action au on au.id
                                              WHERE a.is_active={$db->escape(1)}
                                              ORDER BY dateTime DESC LIMIT 3000", "array");
            // Table fields
            //foreach($companyUsers as $info){
            //    $company_user_info[] = $info;
            //}


            foreach ($getAuditLog as $info) {
                // Actions
                $actions = '<center><div class="icon-edit fa fa-pencil cursor editUserListInfo" id="editUserListInfo_' . $info['auditID'] . '" data-type="deactivate" data-user-id="' . $info['auditID'] . '"></div>' .
                        '<div class="icon-trash cursor fa fa-trash-o deleteUserListInfo" id="deleteUser_' . $info['auditID'] . '" data-type="deactivate" data-user-id="' . $info['auditID'] . '"></div></center>';

                $checkbox = '<center><input type="checkbox" data-id="' . $info['auditID'] . '" name="selectCheckBox[]" class="selectCheckBox" ></center>';

                $audit_logs[] = array(
                    "userCheckbox" => $checkbox,
                    "userID" => $info['uID'],
                    "image" => $userCompany->avatarPic("tbuser", $info['uID'], "30", "30", "small", "avatar"),
                    "fullname" => $info['first_name'] . " " . $info['last_name'],
                    "action" => $info['first_name'] . " " . $info['last_name'] . " " . $info['action'],
                    "ip" => $info['ipAddress'],
                    "dateRegistered" => $info['dateTime'],
                    "tablename" => $info['table_name'],
                    "type" => $info['type'],
                    "actions" => $actions);
            }
            $res = array(/* "sEcho"                        =>      "3",
                  "iTotalRecords"                =>      $countRows,
                  "iTotalDisplayRecords"         =>      $countRows, */
                "company_user" => $audit_logs);

            $string = json_encode($res);

            echo functions::view_json_formatter($string);
            break;

        case "companyUserGuestDatatable":
            $res = "";
            $company_user_info = array();
            $start = $_POST['iDisplayStart'];
            $company = $userCompany->getCompany($auth['company_id']);
            $search = $_POST['search_value'];
            $orderBy = " ORDER BY u.date_registered DESC";
            if ($_POST['column-sort']) {
                $orderBy = " ORDER BY " . $_POST['column-sort'] . " " . $_POST['column-sort-type'];
            }
            $limit = "";
            $end = "10";
            if ($_POST['limit'] != "") {
                $end = $_POST['limit'];
            }
            if ($start != "") {
                $limit = " LIMIT $start, $end";
            }
            $companyUsers = $db->query("SELECT u.*,o.department as depName, tbge.expiration_date 
                                               FROM tbuser u
                                               LEFT JOIN tborgchartobjects o on o.id=u.department_id 
                                               LEFT JOIN tb_guest_expiration tbge ON u.id = tbge.user_id 
                                               WHERE u.company_id={$db->escape($auth['company_id'])}
                                                    AND u.is_active='1' AND u.user_level_id='4' AND (u.email LIKE '%$search%' || u.first_name LIKE '%$search%' || u.last_name LIKE '%$search%' || u.display_name LIKE '%$search%' || u.date_registered LIKE '%$search%' || tbge.expiration_date LIKE '%$search%'  || u.position LIKE '%$search%')
                                                $orderBy $limit", "array");
            $countRows = $db->query("SELECT u.*,o.department as depName, tbge.expiration_date 
                                               FROM tbuser u
                                               LEFT JOIN tborgchartobjects o on o.id=u.department_id 
                                               LEFT JOIN tb_guest_expiration tbge ON u.id = tbge.user_id 
                                               WHERE u.company_id={$db->escape($auth['company_id'])}
                                                    AND u.is_active='1' AND u.user_level_id='4' AND (u.email LIKE '%$search%' || u.first_name LIKE '%$search%' || u.last_name LIKE '%$search%' || u.display_name LIKE '%$search%' || u.date_registered LIKE '%$search%' || tbge.expiration_date LIKE '%$search%'  || u.position LIKE '%$search%')", "numrows");

            $res = array("sEcho" => intval($_POST['sEcho']),
                "iTotalRecords" => $countRows,
                "iTotalDisplayRecords" => $countRows,
                "aaData" => array());
            foreach ($companyUsers as $info) {
                // Gueet
                $guest_info = $db->query("SELECT * FROM tb_guest WHERE guest_user_id={$db->escape($info['id'])}", "row");
                if ($guest_info['guest_type'] != "guest") {
                    $img = $userCompany->guest_avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar", "", "1");
                } else {
                    $img = $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar", "");
                }

                // Guest Information
                // $guest = $db->query("SELECT * FROM tb_guest_expiration WHERE user_id={$db->escape($info['id'])}","row");


                $company_user_info = array();
                // Actions
                $actions = '<center><div class="icon-edit fl-margin-right fa fa-pencil cursor editUserListInfo tip" title="Edit" id="editUserListInfo_' . $info['id'] . '" data-type="deactivate" data-user-id="' . $info['id'] . '"></div>' .
                        '</center>';

                $checkbox = '<center><input type="checkbox" data-id="' . $info['id'] . '" name="selectCheckBox[]" class="selectCheckBox css-checkbox" id="selectCheckBox_' . $info['id'] . '"><label for="selectCheckBox_' . $info['id'] . '" class="css-label"></label></center>';

                //$company_user_info[] = $checkbox;
                if (ALLOW_ACTIVE_DIRECTORY == "1") {
                    $company_user_info[] = '<div align="center">' . $img;
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['email'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['username'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['display_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['first_name'] . " " . $info['last_name'] . '</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">'.$info['position'].'</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">'.$info['depName'].'</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['date_registered'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['expiration_date'] . '</div>';

                    $company_user_info[] = $actions;
                } else {
                    $company_user_info[] = '<div align="center">' . $img;
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['email'] . '</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px"></div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['display_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['first_name'] . " " . $info['last_name'] . '</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">'.$info['position'].'</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">'.$info['depName'].'</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['date_registered'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['expiration_date'] . '</div>';

                    $company_user_info[] = $actions;
                }

                $res['aaData'][] = $company_user_info;
            }


            $string = json_encode($res);

            echo $string;
            break;

        case "companyUserDeactivateGuestDatatable":
            $res = "";
            $company_user_info = array();
            $start = $_POST['iDisplayStart'];
            $company = $userCompany->getCompany($auth['company_id']);
            $search = $_POST['search_value'];
            $orderBy = " ORDER BY u.date_registered DESC";
            if ($_POST['column-sort']) {
                $orderBy = " ORDER BY " . $_POST['column-sort'] . " " . $_POST['column-sort-type'];
            }
            $limit = "";
            $end = "10";
            if ($_POST['limit'] != "") {
                $end = $_POST['limit'];
            }
            if ($start != "") {
                $limit = " LIMIT $start, $end";
            }
            $companyUsers = $db->query("SELECT u.*,o.department as depName, tbge.expiration_date 
                                               FROM tbuser u
                                               LEFT JOIN tborgchartobjects o on o.id=u.department_id
                                               LEFT JOIN tb_guest_expiration tbge ON u.id = tbge.user_id
                                               WHERE u.company_id={$db->escape($auth['company_id'])}
                                                    AND u.is_active='0' AND u.user_level_id='4' AND (u.email LIKE '%$search%' || u.first_name LIKE '%$search%' || u.last_name LIKE '%$search%' || u.display_name LIKE '%$search%' || u.date_registered LIKE '%$search%' || tbge.expiration_date LIKE '%$search%'  || u.position LIKE '%$search%')
                                                $orderBy $limit", "array");
            $countRows = $db->query("SELECT u.*,o.department as depName , tbge.expiration_date 
                                               FROM tbuser u
                                               LEFT JOIN tborgchartobjects o on o.id=u.department_id
                                               LEFT JOIN tb_guest_expiration tbge ON u.id = tbge.user_id
                                               WHERE u.company_id={$db->escape($auth['company_id'])}
                                                    AND u.is_active='0' AND u.user_level_id='4' AND (u.email LIKE '%$search%' || u.first_name LIKE '%$search%' || u.last_name LIKE '%$search%' || u.display_name LIKE '%$search%' || u.date_registered LIKE '%$search%' || tbge.expiration_date LIKE '%$search%' || u.position LIKE '%$search%')", "numrows");



            $res = array("sEcho" => intval($_POST['sEcho']),
                "iTotalRecords" => $countRows,
                "iTotalDisplayRecords" => $countRows,
                "aaData" => array());
            foreach ($companyUsers as $info) {
                // Gueet
                $guest_info = $db->query("SELECT * FROM tb_guest WHERE guest_user_id={$db->escape($info['id'])}", "row");
                if ($guest_info['guest_type'] != "guest") {
                    $img = $userCompany->guest_avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar", "", "1");
                } else {
                    $img = $userCompany->avatarPic("tbuser", $info['id'], "30", "30", "small", "avatar", "");
                }
                // Guest Information
                // $guest = $db->query("SELECT * FROM tb_guest_expiration WHERE user_id={$db->escape($info['id'])}","row");

                $company_user_info = array();
                // Actions
                $actions = '<center><div class="icon-edit fl-margin-right fa fa-pencil cursor editUserListInfo tip" title="Edit" id="editUserListInfo_' . $info['id'] . '" data-type="activate" data-user-id="' . $info['id'] . '"></div>' .
                        '</center>';

                $checkbox = '<center><input type="checkbox" data-id="' . $info['id'] . '" name="selectCheckBox[]" class="selectCheckBox css-checkbox" id="selectCheckBox_' . $info['id'] . '"><label for="selectCheckBox_' . $info['id'] . '" class="css-label"></label></center>';

                //$company_user_info[] = $checkbox;
                if (ALLOW_ACTIVE_DIRECTORY == "1") {
                    $company_user_info[] = '<div align="center">' . $img;
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['email'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['username'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['display_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['first_name'] . " " . $info['last_name'] . '</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">'.$info['position'].'</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">'.$info['depName'].'</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['date_registered'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['expiration_date'] . '</div>';

                    $company_user_info[] = $actions;
                } else {
                    $company_user_info[] = '<div align="center">' . $img;
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['email'] . '</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px"></div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['display_name'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['first_name'] . " " . $info['last_name'] . '</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">'.$info['position'].'</div>';
                    //$company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">'.$info['depName'].'</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['date_registered'] . '</div>';
                    $company_user_info[] = '<div class="fl-table-ellip" style="margin-left:0px">' . $info['expiration_date'] . '</div>';

                    $company_user_info[] = $actions;
                }

                $res['aaData'][] = $company_user_info;
            }
            $string = json_encode($res);

            echo $string;
            break;
    }
}
?>