<?php
error_reporting(E_ERROR);
$redis_cache = getRedisConnection();

$keys = $redis_cache->keys("*");
echo "<table>";
echo "<tr><th align='left'>Key</th><th align='left'>Size</th><th align='left' style='width:400px'>Value</th></tr>";

foreach ($keys as $k) {
    try {
        $serializedFoo = serialize($redis_cache->get($k));
        if (function_exists('mb_strlen')) {
            $size = mb_strlen($serializedFoo, '8bit');
        } else {
            $size = strlen($serializedFoo);
        }

        echo "<tr>";
        echo "<td valign='top' style='border:1px Solid #000'>" . $k . "</td>";
        echo "<td valign='top' style='border:1px Solid #000'>" . formatBytes($size) . "</td>";
        echo "<td style='border:1px Solid #000;'>" . htmlentities(json_encode($redis_cache->get($k))) . "</td>";
        echo "</tr>";
    } catch (Exception $ex) {
        
    }
}


echo "</table>";

function formatBytes($size, $precision = 2) {
    $base = log($size, 1024);
    $suffixes = array('', 'k', 'M', 'G', 'T');

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}
