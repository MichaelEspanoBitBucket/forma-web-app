<?php

//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}

	$auth = Auth::getAuth('current_user');
	$db = new Database();
	$fs = new functions();
	$upload = new upload();
	$search = new Search();


	if( isset($_POST['rule_action']) ){
		$rule_action = $_POST['rule_action'];
		


		if($rule_action == "save"){
			$rule_data = json_decode($_POST['rule_data'], true);

			$insert = array(
				"id" => '',
				//"unique_id" => date("Y") . date("m") . time() . date("d"),
				"rulename" => $rule_data[0]["mid_rule_name"],
				"description" => $rule_data[0]["mid_rule_description"],
				"formname" => $rule_data[0]["mid_rule_sel_formname"],
				"formula" => $rule_data[0]["mid_rule_formula"],
				"actions" => json_encode($rule_data[0]["mid_rule_actions"]),
				"scheduled_process_start" => $rule_data[0]["mid_rule_date_start"],
				"scheduled_process_end" => $rule_data[0]["mid_rule_date_end"],
				"schedule_rep_data" => json_encode($rule_data[0]["mid_rule_sched_data"]),
				"date_created" => date("Y-m-d"),
				"is_active" => 1
			);
			$insert_data = array();
			$data_id = $db->insert("tbmiddleware_settings", $insert);
			$insert['start_formatted'] = date('Y-m-d H:i', strtotime($insert['scheduled_process_start']));
			$insert['end_formatted'] = date('Y-m-d H:i', strtotime($insert['scheduled_process_end']));
			$insert_data["stat"] = "success";
			$insert["id"] = $data_id;
			$insert_data["data_ins"] = $insert;
			
			print_r(json_encode($insert_data));
		}

		if($rule_action == "update"){
			$rule_data = json_decode($_POST['rule_data'], true);

			$insert = array(
				"rulename" => $rule_data[0]["mid_rule_name"],
				"description" => $rule_data[0]["mid_rule_description"],
				"formname" => $rule_data[0]["mid_rule_sel_formname"],
				"formula" => $rule_data[0]["mid_rule_formula"],
				"actions" => json_encode($rule_data[0]["mid_rule_actions"]),
				"scheduled_process_start" => $rule_data[0]["mid_rule_date_start"],
				"scheduled_process_end" => $rule_data[0]["mid_rule_date_end"],
				"schedule_rep_data" => json_encode($rule_data[0]["mid_rule_sched_data"]),
			);

			$insert_data = array();

			$db->update("tbmiddleware_settings",$insert,array("id"=>$rule_data[0]["id"]));

			$insert_data["stat"] = "success";
			$insert["id"] = $rule_data[0]["id"];
			$insert['start_formatted'] = date('Y-m-d H:i', strtotime($insert['scheduled_process_start']));
			$insert['end_formatted'] = date('Y-m-d H:i', strtotime($insert['scheduled_process_end']));
			$insert_data["data_ins"] = $insert;

			print_r(json_encode($insert_data));
		}


		if($rule_action == "enable_rule"){
			$rule_data = json_decode($_POST['rule_data'], true);
			$insert = array(
				"is_active" => "1"
			);

			$insert_data = array();
			$db->update("tbmiddleware_settings",$insert,array("id"=>$rule_data["id"]));
			$insert_data["stat"] = "success";
			$insert_data["id"] = $rule_data["id"];
			$insert_data["data_ins"] = $insert;
			$json_decode_var = json_encode($insert_data);
			print_r($json_decode_var);
		}


		if($rule_action == "disable_rule"){
			$rule_data = json_decode($_POST['rule_data'], true);
			
			$insert = array(
				"is_active" => "0"
			);

			$insert_data = array();
			$db->update("tbmiddleware_settings",$insert,array("id"=>$rule_data["id"]));
			$insert_data["stat"] = "success";
			$insert_data["id"] = $rule_data["id"];
			$insert_data["data_ins"] = $insert;
			$json_decode_var = json_encode($insert_data);
			print_r($json_decode_var);
		}

		if($rule_action == "retrieve"){
			$queryStr = "SELECT ms.*, DATE_FORMAT( ms.scheduled_process_start,  '%Y-%m-%d %H:%i' ) AS start_formatted, DATE_FORMAT( ms.scheduled_process_end,  '%Y-%m-%d %H:%i' ) AS end_formatted, tbw.company_id FROM tbmiddleware_settings ms LEFT JOIN tb_workspace tbw ON ms.formname = tbw.id WHERE tbw.is_delete = 0 AND tbw.company_id =  '". $auth['company_id'] ."';";
			$getMidRules = $db->query($queryStr, "array");

			// $getMidRules[0]['scheduled_process_start'] = date('Y-m-d H:i',strtotime($getMidRules[0]['scheduled_process_start']));
			// $getMidRules['scheduled_process_end'] = $getMidRules['scheduled_process_end'];
			$retrieve_data = json_encode($getMidRules);
			print_r($retrieve_data);
		}

		if($rule_action=="loadRuleSettings"){
			$search_value = $_POST['search_value'];
            $start = $_POST['iDisplayStart'];
            $end = "10";
            if($_POST['limit']!=""){
                $end = $_POST['limit'];
            }
            //for limit
            $limit = "";
            if($start!=""){
                $limit = " LIMIT $start, $end";
            } 
            //for order by
            $orderBy = " ORDER BY rulename ASC";
            if($_POST['column-sort']){
              $orderBy = " ORDER BY ".$_POST['column-sort']." ".$_POST['column-sort-type'];
            }

            $where = " AND (ms.rulename LIKE '%". $search_value ."%' || ms.description LIKE '%". $search_value ."%')";

			$queryStr = "SELECT ms.*, DATE_FORMAT( ms.scheduled_process_start,  '%Y-%m-%d %H:%i' ) AS start_formatted, DATE_FORMAT( ms.scheduled_process_end,  '%Y-%m-%d %H:%i' ) AS end_formatted, tbw.company_id FROM tbmiddleware_settings ms LEFT JOIN tb_workspace tbw ON ms.formname = tbw.id WHERE tbw.is_delete = 0 AND tbw.company_id =  '". $auth['company_id'] ."'";
			$queryMidRuleRows = $queryStr.$where.$orderBy.$limit;
			$getMidRules = $db->query($queryMidRuleRows, "array");
			

			$countMidRules = $db->query($queryStr.$where, "numrows");

			$output = array(
                "sEcho" => intval($_POST['sEcho']),
                "iTotalRecords" => $countMidRules,
                "iTotalDisplayRecords" => $countMidRules,
                "start"=>$start,
                "aaData" => array(),
                "query" =>$queryMidRuleRows
            );  


            foreach($getMidRules as $data){
            	$info = array();
            	$status = $data['is_active'];
            	$button = '<a><i class="fa fa-edit updateRules tip" title="Edit" style="cursor:pointer;" data-id="'.$data["id"].'"></i></a>';
                	// <i class="fa fa-times removeRules tip" title="Deactivate" style="cursor:pointer;'.(($status=="0")?"color:red !important;":"color:inherit !important;").'" data-id="'.$data["id"].'"></i>&nbsp;&nbsp;&nbsp;
                	// <i class="fa fa-check enableRules tip" title="Activate" style="cursor:pointer;'.(($status=="1")?"color:lightgreen !important;":"color:inherit !important;").'" data-id="'.$data["id"].'"></i></center></div>
            	$button .= '<a><i class="fa fa-check-circle-o '.(($status=="0")?"enableRules":"removeRules").' tip" title="'.  $fs->getStatus_revert($status) .'" style="cursor:pointer;" data-id="'.$data["id"].'"></i></a>';
                $button .= '<div class="rule-data display">'. json_encode($data) .'</div>';
            	if($data['is_active']==0){
            		$status = "";
            	}else{
            		$status = "Active";
            	}

            	$info[] = "<div class='fl-table-ellip'>" . $data['rulename']."</div>";
                $info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($data['description'],"")."</div>";
                $info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($status,"Disabled")."</div>";
                $info[] = "<div class='fl-table-ellip'>" . $button ."</div>";

                $output['aaData'][] = $info;
            }

			echo json_encode($output);
		}
	}
?>