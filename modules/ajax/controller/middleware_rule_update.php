<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}

	$auth = Auth::getAuth('current_user');
	$db = new Database();
	$fs = new functions();
	$upload = new upload();
	$search = new Search();


	if(isset($_POST['update_rule_set'])){
		$data_id = $_POST['data_id'];
	
		$collected_data = $_POST['data_inputs_mid_set'];
		$json = json_decode($collected_data, true);

		//print_r($json[0]);
		$insert = array(
			//"unique_id" => date("Y") . date("m") . time() . date("d"),
			"rulename" => $json[0]["mid_rule_name"],
			"description" => $json[0]["mid_rule_description"],
			"formname" => $json[0]["mid_rule_sel_formname"],
			"formula" => $json[0]["mid_rule_formula"],
			"actions" => json_encode($json[0]["mid_rule_actions"]),
			"scheduled_process_start" => $json[0]["mid_rule_date_start"],
			"scheduled_process_end" => $json[0]["mid_rule_date_end"],
			"date_created" => date("Y-m-d"),
			"is_active" => 1
		);
		// print_r("GRABE".$collected_data);
		//$db->update("tbmiddleware_settings",$insert,array("id"=>$data_id));
		echo "success";
	
	}
	
	
?>