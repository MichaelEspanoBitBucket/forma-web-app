<?php

$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();

$forms = functions::getWorkspace(" WHERE company_id={$auth["company_id"]}");

foreach ($forms as $formDoc) {
    $requests = $db->query(" SELECT id FROM " . $formDoc->form_table_name, "array");

    foreach ($requests as $request) {
        $requestDoc = new Request();
        $requestDoc->load($formDoc->id, $request["id"]);
        $requestDoc->computeFields($auth);
    }
}

echo "done...";

