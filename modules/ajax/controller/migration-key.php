<?php


// Migration

// error_reporting(E_ALL);
$db = new Database();
$fs = new functions();
$session = new Auth();
$Auth = Auth::getAuth('current_user');


$company_list = $db->query("SELECT * FROM tbcompany WHERE l_code = ''","array");

$count = 1;
foreach ($company_list as $value) {


    // Encryt Licensing Code
    $c_1 = $fs->random_chars(4);
    $c_2 = $fs->random_chars(4);
    $c_3 = $fs->random_chars(4);
    $c_4 = $fs->random_chars(4);
    $set_encrytion = $c_1 . "-" . $c_2 . "-" . $c_3 . "-" . $c_4;
    $code = $fs->li_en_dec_method("encrypt",$set_encrytion);

    $db->update("tbcompany",array("l_code"=>$code),array("id"=>$value['id']));
    $db->update("tb_app_configuration",array("app_key"=>$code),array("app_company_id"=>$value['id']));

    echo $count++ . ") Company Name <b>" . $value['name'] . "</b> with a product key of <b>(" . $set_encrytion .")</b> was successfully migrated.";
    echo "<br><br>";

    $password = $fs->encrypt_decrypt("encrypt", $Auth['password']);
    $login = $session->login($Auth['email'], $password, 'email', 'password', 'tbuser');


}

?>