<?php
$db = new Database();
ini_set('max_execution_time', 0);



$getMigrationFlag = $db->query("SELECT * FROM tbaudit_logs WHERE user_id = 0 AND audit_action = 0 AND table_name = 'migration_0002'","numrows");
if($getMigrationFlag>0){
	echo "Not Permitted migration_0002<br />";
	// return false;
}else{
	// echo "not permitted";return false;
	$queries = $db->query("SELECT CONCAT(  'ALTER TABLE ', ws.form_table_name,  ' ADD Editor longtext NOT NULL' ) AS ExecuteTheseSQLCommands FROM tb_workspace ws","array");
	foreach ($queries as $value) {
	    echo $db->query($value['ExecuteTheseSQLCommands'],"");
	}
	$queries = $db->query("SELECT CONCAT(  'ALTER TABLE ', ws.form_table_name,  ' ADD Viewer longtext NOT NULL' ) AS ExecuteTheseSQLCommands FROM tb_workspace ws","array");
	foreach ($queries as $value) {
	    echo $db->query($value['ExecuteTheseSQLCommands'],"");
	}
	$queries = $db->query("SELECT CONCAT(  'ALTER TABLE ', ws.form_table_name,  ' ADD middleware_process longtext NOT NULL' ) AS ExecuteTheseSQLCommands FROM tb_workspace ws","array");
	foreach ($queries as $value) {
	    echo $db->query($value['ExecuteTheseSQLCommands'],"");
	}
	$queries = $db->query("SELECT CONCAT(  'rename table ', LCASE(ws.form_table_name),  ' to ',ws.form_table_name ) AS ExecuteTheseSQLCommands FROM tb_workspace ws","array");
	foreach ($queries as $value) {
	   $db->query($value['ExecuteTheseSQLCommands']);
	}

	/*
	Audit Trail for ogchart, workflow and forms
	Working date 7/22/2014(unit testing aaron pc)


	file affected

	ajax/controller/updateStatus.php
	ajax/controller/formbuilder.php
	ajax/controller/orgchart.php
	ajax/controller/workflow.php


	*/

	//for audit action(aaron)
	$db->insert("tbaudit_action",array("id"=>"20","value"=>"create orgchart","type"=>"orgchart","is_active"=>"1")); //done
	$db->insert("tbaudit_action",array("id"=>"21","value"=>"activate orgchart","type"=>"orgchart","is_active"=>"1")); //done
	$db->insert("tbaudit_action",array("id"=>"22","value"=>"deactivate orgchart","type"=>"orgchart","is_active"=>"1")); //done
	$db->insert("tbaudit_action",array("id"=>"23","value"=>"delete orgchart","type"=>"orgchart","is_active"=>"1")); //done


	$db->insert("tbaudit_action",array("id"=>"24","value"=>"create form","type"=>"form","is_active"=>"1")); // done
	$db->insert("tbaudit_action",array("id"=>"25","value"=>"edit form","type"=>"form","is_active"=>"1")); // done
	$db->insert("tbaudit_action",array("id"=>"26","value"=>"activate form","type"=>"form","is_active"=>"1")); // done
	$db->insert("tbaudit_action",array("id"=>"27","value"=>"deactivate form","type"=>"form","is_active"=>"1")); // done
	$db->insert("tbaudit_action",array("id"=>"28","value"=>"delete form","type"=>"form","is_active"=>"1")); //done

	$db->insert("tbaudit_action",array("id"=>"29","value"=>"create workflow","type"=>"workflow","is_active"=>"1")); // done
	$db->insert("tbaudit_action",array("id"=>"30","value"=>"activate workflow","type"=>"workflow","is_active"=>"1")); // done
	$db->insert("tbaudit_action",array("id"=>"31","value"=>"deactivate workflow","type"=>"workflow","is_active"=>"1")); //done
	$db->insert("tbaudit_action",array("id"=>"32","value"=>"delete workflow","type"=>"workflow","is_active"=>"1")); //done


	$db->insert("tbaudit_action",array("id"=>"33","value"=>"soft delete orgchart","type"=>"orgchart","is_active"=>"1")); //done
	$db->insert("tbaudit_action",array("id"=>"34","value"=>"soft delete form","type"=>"form","is_active"=>"1")); //done
	$db->insert("tbaudit_action",array("id"=>"35","value"=>"soft delete workflow","type"=>"workflow","is_active"=>"1")); //done


	$db->insert("tbaudit_action",array("id"=>"36","value"=>"update form category","type"=>"tbform_category","is_active"=>"1"));
	$db->insert("tbaudit_action",array("id"=>"37","value"=>"soft delete form category","type"=>"tbform_category","is_active"=>"1"));


	/*
	Soft Delete for orgchart, workflow, forms and form category
	*/
	$db->query("ALTER TABLE  `tborgchart` ADD  `is_delete` TINYINT( 1 ) NOT NULL DEFAULT  '0';","");
	$db->query("ALTER TABLE  `tbworkflow` ADD  `is_delete` TINYINT( 1 ) NOT NULL DEFAULT  '0';","");
	$db->query("ALTER TABLE  `tb_workspace` ADD  `is_delete` TINYINT( 1 ) NOT NULL DEFAULT  '0';","");
	$db->query("ALTER TABLE  `tbform_category` ADD  `is_delete` TINYINT( 1 ) NOT NULL DEFAULT  '0';","");


	/*
	Starred status
	*/

	$db->query("ALTER TABLE  `tbstarred` ADD  `is_active` TINYINT( 1 ) NOT NULL DEFAULT  '1';","");


	/*
	department code

	*/
	$db->query("ALTER TABLE  `tborgchartobjects` ADD  `department_code` varchar( 255 ) NOT NULL;","");

	/*
	department code

	*/
	$db->query("ALTER TABLE  `tbrequest_viewer` ADD  `type` varchar( 255 ) NOT NULL DEFAULT  '0';","");

	/*
	Add form category as field
	*/

	$db->query("ALTER TABLE  `tb_workspace` ADD  `category_id` TINYINT( 1 ) NOT NULL DEFAULT  '0';","");

	$getForms = $db->query("SELECT form_json,id FROM tb_workspace ws where is_delete = 0","array");

	foreach ($getForms as $value) {
		$json =  json_decode($value['form_json'],true);
		$categoryID = $json['categoryName'];
		$form_json = $json['form_json'];
		if($categoryID==""){
			$categoryID = $form_json['form_category'];
		}
		// echo $value['id']." = ".$categoryID." = ".$form_json['form_category']."<br />";
		$db->update("tb_workspace",array("category_id"=>$categoryID),array("id"=>$value['id']));
	}

	$db->query("ALTER TABLE  `tbnotification` ADD  `is_read` TINYINT( 1 ) NOT NULL DEFAULT  '0';","");

	$getForms = $db->update("tbnotification",array("is_read"=>0),array("user_read"=>0));
	$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migration_0002"));
}

$getMigrationFlag = $db->query("SELECT * FROM tbaudit_logs WHERE user_id = 0 AND audit_action = 0 AND table_name = 'migration_0003'","numrows");
if($getMigrationFlag>0){
	echo "Not Permitted migration_0003<br />";
	// return false;
}else{

	$db->query("ALTER TABLE  `tb_workspace` ADD  `form_display_type` TINYINT( 1 ) NOT NULL DEFAULT  '1';","");

	$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migration_0003"));
}

$getMigrationFlag_save_cance = $db->query("SELECT * FROM tbaudit_logs WHERE user_id = 0 AND audit_action = 0 AND table_name = 'migration_0004'","numrows");
if($getMigrationFlag_save_cance>0){
	echo "Not Permitted migration_0004<br />";
	// return false;
}else{

	$queries = $db->query("SELECT CONCAT(  'ALTER TABLE ', ws.form_table_name,  ' ADD SaveFormula longtext NOT NULL, ADD CancelFormula longtext NOT NULL' ) AS ExecuteTheseSQLCommands FROM tb_workspace ws","array");
	foreach ($queries as $value) {
	    echo $db->query($value['ExecuteTheseSQLCommands'],"");
	}
	
	$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migration_0004"));
}
print_r($db->query("SET GLOBAL group_concat_max_len=15000"));

//add enable migration field
$getMigrationFlagDelegate = $db->query("SELECT * FROM tbaudit_logs WHERE user_id = 0 AND audit_action = 0 AND table_name = 'migration_delagate_field'","numrows");
if($getMigrationFlagDelegate>0){
	echo "Not Permitted getMigrationFlagDelegate<br />";
	// return false;
}else{
	// echo "not permitted";return false;
	$queries = $db->query("SELECT CONCAT(  'ALTER TABLE ', ws.form_table_name,  ' ADD enable_delegate TINYINT( 1 ) NOT NULL' ) AS ExecuteTheseSQLCommands FROM tb_workspace ws","array");
	foreach ($queries as $value) {
	    echo $db->query($value['ExecuteTheseSQLCommands'],"");
	}
	$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migration_delagate_field"));
}


echo "Done";

?>

