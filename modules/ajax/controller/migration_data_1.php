<?php

$db = new Database();
$search = new Search();
ini_set('max_execution_time', 0);

//$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migrate_0001"));
//$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migrate_0001_dept_users"));
//$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migrate_0001_announcement_users"));
return false;
//functions
function updateUsers($table_name, $data){
	$db = new Database();
	if($data['user_type']=="1"){
		// position
		$getPosition = $db->query("SELECT * FROM tbpositions WHERE position = '". $data['user'] ."' AND company_id = '". $data['company_id'] ."'","row");
		echo "From ". $data['user'] ." to ". $getPosition['id'] ."";
		if($getPosition['id']!=""){
			$db->update($table_name,array("user"=>$getPosition['id']),array("id"=>$data["id"]));
		}
	}else if($data['user_type']=="2"){
		// department
		$getDeptCode = $db->query("SELECT department_code FROM tborgchartobjects WHERE id = '". $data['user'] ."'","row");
		echo "From ". $data['user'] ." to ". $getDeptCode['department_code'] ."";
		if($getDeptCode['department_code']!=""){
			$db->update($table_name,array("user"=>$getDeptCode['department_code']),array("id"=>$data["id"]));
		}
	}
	echo "<br />";
}
// function for replacing department id to department code and position name to position id
function idMigrationInJson($array,$type){
	$db = new Database();
	foreach ($array as $key => $data) {
		if($key=="departments"){
			$departmentArray = array();
			foreach ($data as $department) {
				$getDepartmentCode = $db->query("SELECT * FROM tborgchartobjects WHERE id = '". $department ."'","row");
				// echo $getDepartmentCode['department_code'];
				if($getDepartmentCode['department_code']){
					array_push($departmentArray, $getDepartmentCode['department_code']);
				}else{
					array_push($departmentArray, $department);
				}
				
			}
			$array['departments'] = $departmentArray;
		}else if($key=="positions"){
			$positionArray = array();
			foreach ($data as $position) {
				$getPosition = $db->query("SELECT * FROM tbpositions WHERE position = '". $position ."'","row");
				if($getPosition['id']){
					array_push($positionArray, $getPosition['id']);
				}else{
					array_push($positionArray, $position);
				}
			}
			$array['positions'] = $positionArray;
		}
	}
	return $array;
}

function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

//START OF MIGRATION


$getMigrationFlag = $db->query("SELECT * FROM tbaudit_logs WHERE user_id = 0 AND audit_action = 0 AND table_name = 'migrate_0001'","numrows");
if($getMigrationFlag==0){
	/* EXPECTED RESULT - LAHAT NG POSITION NG USER MASSAVE SA tbposition SAKA YUNG MGA POSITION ID MAASSIGN SA tbuser
		Migration of Position
		- to migrate all position from users in tbpositions by company
	*/
		$getCompanies = $db->query("SELECT id, name FROM tbcompany","array");
		foreach ($getCompanies as $key => $value) {
			$getPositionsFromUsers = $db->query("SELECT position,company_id FROM tbuser WHERE company_id='". $value['id'] ."' AND position!='' GROUP BY position","array");
			foreach ($getPositionsFromUsers as $position) {
				$inserted = $db->insert("tbpositions",array("position"=>$position['position'],"company_id"=>$value['id'],"is_active"=>"1"));
				if($inserted>0){
					echo $position['position']."<br />";
				}
			}
		}

	/*
		Migration of Position of users
		- to replace position of users with position id from tbposition

	*/
		$getPositions = $db->query("SELECT id,position,company_id FROM tbpositions","array");
		foreach ($getPositions as $value_position) {
			$getUsersHasPosition = $db->query("SELECT id,display_name FROM tbuser WHERE position = '". $value_position['position'] ."' AND company_id = '". $value_position['company_id'] ."'","array");
			foreach ($getUsersHasPosition as $value_users) {
				$db->update("tbuser",array("position"=>$value_position['id']),array("id"=>$value_users['id']));
				echo $value_users['display_name']." from position ".$value_position['position']." to position id: ". $value_position['id'] ."<br />";
			}
		}

		
	/* 
		Additional field in tborgchartobjects
		- to create department code in departments for reference
		- default department code will be
		DC-{department name}
	 	EXPECTED RESULT - MAG-AASIGN NG DEPARTMENT CODE SA orgchart, MAG-SSAVE NG NG DEPARTMENT USERS SA JUNCTION TABLE
		
	*/	
	 	$getDepartments = $db->query("SELECT * FROM tborgchartobjects","array");
		foreach ($getDepartments as $value_dept) {
			$department_code = "DC-". $value_dept['id'] . "_" . $value_dept['department'];
			$db->update("tborgchartobjects",array("department_code"=>$department_code),array("id"=>$value_dept['id']));
			echo $value_dept['department']." assigned department code".$department_code."<br />";
		}

	/*
		Migration of Department Users
		- to create junction table for department users
	*/
		$getDepartments = $db->query("SELECT * FROM tborgchartobjects","array");
		foreach ($getDepartments as $value_dept) {
			$json = json_decode($value_dept['json'],true);
			//$department_code = "DC-".$value_dept['department'];
			//head
			echo "department id:".$value_dept['id']."<br />";
			$db->insert("tbdepartment_users",array("department_code"=>$value_dept['department_code'],"department_position"=>"1","user_id"=>$json['orgchart_user_head'][0],"orgChart_id"=>$value_dept['orgChart_id']));
			echo "head ".$json['orgchart_user_head'][0]."<br />";
			//assistant head
			if($json['orgchart_dept_assistant'][0]){
				$db->insert("tbdepartment_users",array("department_code"=>$value_dept['department_code'],"department_position"=>"2","user_id"=>$json['orgchart_dept_assistant'][0],"orgChart_id"=>$value_dept['orgChart_id']));
			}
			echo "assistant head ".$json['orgchart_dept_assistant'][0]."<br />";
			//members
			foreach ($json['orgchart_dept_members'] as $user_id) {
				$db->insert("tbdepartment_users",array("department_code"=>$value_dept['department_code'],"department_position"=>"3","user_id"=>$user_id,"orgChart_id"=>$value_dept['orgChart_id']));
				echo "members ".$user_id."<br />";
			};
			echo "<br /><br /><br />";
		}
		//insert to audit trail to stop the migration at the bottom "migrate_0001_dept_users" for adding orgchart id in department users
		//added 9/16/2014
		$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migrate_0001_dept_users"));

	/*
		Migration of Form Category Users
		- to move form "users" in tbform_category_users
		EXPECTED RESULT - MAASSIGN SA FORM CATEGORY USERS 
	*/	
		

		$getFormCategories = $db->query("SELECT id,users FROM tbform_category ws where is_delete = 0","array");
		foreach ($getFormCategories as $formCategory) {
			$form_category_users = json_decode($formCategory['users'],true);
			echo "form category id ".$formCategory['id'];
			print_r($form_category_users);
			$dataToInsert = setUsersMigration($db,$form_category_users,$formCategory,2,2);
			print_r($dataToInsert);
			echo "<br /><br />";
		}

	/*
		Migration of form authors, viewers, admin and form category admin, partial of form users
		- to migration form authors, viewers and admin from json to junction table
		- if form category is Others. the only on that will insert is form admin as the creator of the form.
		- form authors and viewers will be automatically become form users and form category users
		- form admin has 3 diferrent types of values
			- blank value 	- this is old forms that is not migrated after we develop the form admin module.
						  	- this considered as all admin can access the form.
			- 0 value 	- this is only a flag if the creator of the form wanted to disallow other admin to view the form.
						- only the creator of the form can access the form.
			- array value - all listed in array will be the admin of the form.

		- to migrate form category admin. This is new feature thats why we will consider 
			all the form admin to become form category admin for migration purpose only.
		
	*/

		$getForms = $db->query("SELECT id,form_authors,form_viewers,form_admin, category_id, created_by, company_id FROM tb_workspace ws","array");
		foreach ($getForms as $form) {
			echo "Form ID: ". $form['id'];
			echo "<br /><br />";
			if($form['category_id']=="0"){
				//only admin will insert in junction table
				$form_admin_0 = array();
				$form_admin_0['users'] = array($form['created_by']);
				//insert statement here
				//form admin
				setUsersMigration($db,$form_admin_0,$form,1,1,4);
				echo "Form Admin ";
				print_r($form_admin_0);
				echo "<br /><br /><br /><br />";
			 }else{
				//get form category for form category admin
				$category = $db->query("SELECT id,category_name FROM tbform_category WHERE id = '".$form['category_id']."'","row");

				$form_author = json_decode($form['form_authors'],true);
				$form_viewers = json_decode($form['form_viewers'],true);
				$form_admin = json_decode($form['form_admin'],true);

				//for author and viewers
				setUsersMigration($db,$form_author,$form,2,1,1);
				setUsersMigration($db,$form_viewers,$form,2,1,2);
				
				//form users
				setUsersMigration($db,$form_author,$form,2,1,3);
				setUsersMigration($db,$form_viewers,$form,2,1,3);

				//form category users
				setUsersMigration($db,$form_author,$category,2,2);
				setUsersMigration($db,$form_viewers,$category,2,2);

				echo "Form author ";
				print_r($form_author);
				echo "<br >";
				echo "Form viewers ";
				print_r($form_viewers);
				echo "<br />";
				echo "Form users ";
				print_r($form_author);
				print_r($form_viewers);
				echo "<br />";
				echo "Form Category Users ";
				print_r($form_author);
				print_r($form_viewers);
				echo "<br />";
				if($form['form_admin']==""){
					// all admin of company
					$getAllAdmin = $db->query("SELECT id FROM tbuser WHERE user_level_id = 2 AND company_id = '". $form['company_id'] ."'","array");
					$form_admin_1 = array();
					foreach ($getAllAdmin as $admin) {
						$form_admin_1['users'][] = $admin['id'];
					}
					//insert statement here
					//form admin
					setUsersMigration($db,$form_admin_1,$form,1,1,4);
					echo "Form Admin ";
					print_r($form_admin_1);
					echo "<br />";
					//form category admin
					setUsersMigration($db,$form_admin_1,$category,1,2);

				}else if($form['form_admin']=="0"){
					//only admin will insert in junction table
					$form_admin_2 = array();
					$form_admin_2['users'] = array($form['created_by']);
					//insert statement here

					//form admin
					setUsersMigration($db,$form_admin_2,$form,1,1,4);
					echo "Form Admin ";
					print_r($form_admin_2);
					echo "<br />";
					//form category admin
					setUsersMigration($db,$form_admin_2,$category,1,2);
				}else{
					$form_admin['departments'] = array();
					$form_admin['positions'] = array();
					// form admin
					 setUsersMigration($db,$form_admin,$form,1,1,4);
					 echo "Form Admin ";
					print_r($form_admin);
					echo "<br />";
					// form category admin
					 setUsersMigration($db,$form_admin,$category,1,2);
					
				}
				echo "<br /><br /><br /><br />";
			}
			
		}

	/*
		To Replace department id to department code
		to replace position name to position id
	*/
		// $getFormCategoryUsers = $db->query("SELECT * FROM tbform_category_users WHERE user_type!=3");
		$getFormCategoryUsers = $db->query("SELECT fcu.*, fc.company_id FROM tbform_category_users fcu LEFT JOIN tbform_category fc ON fcu.form_category_id = fc.id WHERE user_type!=3");
		echo "Form Category";
		foreach ($getFormCategoryUsers as $value) {
			updateUsers("tbform_category_users", $value);
		}
		echo "<br /><br /><br />";
		echo "Forms";
		// tbform_users
		// $getFormUsers = $db->query("SELECT * FROM tbform_users WHERE user_type!=3");
		$getFormUsers = $db->query("SELECT fu.*, ws.company_id FROM tbform_users fu LEFT JOIN tb_workspace ws ON fu.form_id = ws.id WHERE user_type!=3");

		foreach ($getFormUsers as $value) {
			updateUsers("tbform_users",$value);	
		}

		
		
	/*
		Migration of mail,sms,viewer,editor, company position(processor)
		- to replace department id to department code
		- to replace position name to position id
	*/
		$getWorkflowNodes = $db->query("SELECT * FROM tbworkflow_objects","array");
		foreach ($getWorkflowNodes as $nodes) {
			$set = array();
			// whole json
			$json = json_decode($nodes['json'],true);

			if($nodes['email']!="null"){
				// mail
				$json_nodes_mail = json_decode($nodes['email'],true);
				// recepient
				$recepient = $json_nodes_mail['email_recpient'];
				$json_nodes_mail['email_recpient'] = idMigrationInJson($recepient);

				// cc
				$cc = $json_nodes_mail['email_cc'];

				$json_nodes_mail['email_cc'] = idMigrationInJson($cc);

				// cc
				$bcc = $json_nodes_mail['email_bcc'];
				$json_nodes_mail['email_bcc'] = idMigrationInJson($bcc);


				// set mail here
				$node_mail = json_encode($json_nodes_mail);
				$set['email'] = $node_mail;
				// json
				$json['workflow_email'] = $json_nodes_mail;
			}
			if($nodes['sms']!="null"){
				// sms
				$json_nodes_sms = json_decode($nodes['sms'],true);
				$json_sms = idMigrationInJson($json_nodes_sms);
				$node_sms = json_encode($json_sms);
				$set['sms'] = $node_sms;

				// json
				$json['workflow_sms'] = $json_sms;
			}
			

			// users

			// viewer
			if($json['users']['user_viewer']){
				$json_node_viewer = idMigrationInJson($json['users']['user_viewer']);
				$json['users']['user_viewer'] = $json_node_viewer;
			}

			// editor
			if($json['users']['user_editor']){
				$json_node_editor = idMigrationInJson($json['users']['user_editor']);
				$json['users']['user_editor'] = $json_node_editor;
			}
			


			// processor type 2 position
			if($nodes['processorType']=="2"){
				$getPosition = $db->query("SELECT * FROM tbpositions WHERE position = '". $nodes['processor'] ."'","row");
				if($getPosition['id']){
					$set = array("processor"=>$getPosition['id']);
					$json['processor'] = $getPosition['id'];
				}
			}

			

			$json_str = json_encode($json);

			// update here
			
			$set['json'] = $json_str;

			$where = array("id"=>$nodes['id']);
			echo "ID ".$nodes['id'];
			print_r($set);
			echo "<br /><br />";
			$db->update("tbworkflow_objects",$set,$where);
		}

	/*
		Migration of Rules email and sms
		- to replace department id to department code
		- to replace position name to position id
	*/

		$getRules = $db->query("SELECT * FROM tbmiddleware_settings","array");
		foreach ($getRules as $rule) {
			$rule_actions = json_decode($rule['actions'],true);
			$ruleToSave = json_decode($rule['actions'],true);
			foreach ($rule_actions as $key => $value) {
				if($value['name']=="etrigger"){
					$ruleToSave[$key]['value']['email_recpient'] = idMigrationInJson($value['value']['email_recpient']);
					$ruleToSave[$key]['value']['email_cc'] = idMigrationInJson($value['value']['email_cc']);
					$ruleToSave[$key]['value']['email_bcc'] = idMigrationInJson($value['value']['email_bcc']);
				}
				if($value['name']=="smstrigger"){
					$ruleToSave[$key]['value'] = idMigrationInJson($value['value']);
				}
			}
			$ruleToSave = json_encode($ruleToSave);
			echo "ID ".$rule['id'];
			print_r($ruleToSave);
			echo "<br /><br />";
			$db->update("tbmiddleware_settings",array("actions"=>$ruleToSave),array("id"=>$rule['id']));
		}


	/*
		Migration of form users (except from authors and viewers that are already migrated in obove)
		- all users that has a request to view in the form.
			- tagged
			- viewer in workflow
			- editor in workflow
		- validation should be
			- form category users only
			- if already inserted
		- if not exisit in form category users, it will automatically inserted it in the form category user table.
			- type is 3 as user
	*/
		
		$getForms = $db->query("SELECT id,company_id,category_id FROM tb_workspace","array");
		foreach ($getForms as $form) {
			$getUsers = $db->query("SELECT * FROM tbuser WHERE company_id = '". $form['company_id'] ."'  AND (user_level_id = '2' OR user_level_id = '3')","array");
			foreach ($getUsers as $user) {
				//check if the user has a request to access
				$obj = array("isOtherUser"=>"1","userData"=>$user);
		        $obj = json_decode(json_encode($obj),true);
		        $result = $search->getManyRequest("",0,$form['id'],0,1,$obj);
		        
		        if(count($result)==1){
		        	echo "Form ID: ".$form['id']." | User ID: ".$user['id']." | Request Count: ".count($result)."<br /><br /><br />";
		        	$form_id = $form['id'];
		        	$category_id = $form['category_id'];
		        	
		        	//check if already exist in form user
		        	$getFormUsersByUsers = $search->getFormUsersByUsers(" AND fcu.form_category_id = '$category_id' AND fu.form_id = '$form_id' AND fu.user = '". $user['id'] ."'");
		        	if(count($getFormUsersByUsers)==0){
		        		//form user
		        		$insert = array("form_id"=>$form_id,
			                            "user"=>$user['id'],
			                            "user_type"=>"3",
			                            "action_type"=>"3"
			                                );
		        		$db->insert("tbform_users",$insert);
		        	}
		        }
			}
		}

		 $forms = $db->query("SELECT form_table_name FROM tb_workspace","array");
		 foreach($forms as $form){
			$query  = $db->query("ALTER TABLE {$form["form_table_name"]} ADD ProcessorType INT");
			$query  = $db->query("ALTER TABLE {$form["form_table_name"]} ADD ProcessorLevel INT");
		 }
		
	$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migrate_0001"));

	echo "DONE<br />";
}else{
	echo "Not Permitted migrate_0001<br />";
}



$getMigrationFlag_DeptUsers = $db->query("SELECT * FROM tbaudit_logs WHERE user_id = 0 AND audit_action = 0 AND table_name = 'migrate_0001_dept_users'","numrows");
if($getMigrationFlag_DeptUsers==0){
	$db->query("TRUNCATE tbdepartment_users","");
	$getDepartments = $db->query("SELECT * FROM tborgchartobjects","array");
	foreach ($getDepartments as $value_dept) {
		$json = json_decode($value_dept['json'],true);
		//$department_code = "DC-".$value_dept['department'];
		//head
		echo "department id:".$value_dept['id']."<br />";
		$db->insert("tbdepartment_users",array("department_code"=>$value_dept['department_code'],"department_position"=>"1","user_id"=>$json['orgchart_user_head'][0],"orgChart_id"=>$value_dept['orgChart_id']));
		echo "head ".$json['orgchart_user_head'][0]."<br />";
		//assistant head
		if($json['orgchart_dept_assistant'][0]){
			$db->insert("tbdepartment_users",array("department_code"=>$value_dept['department_code'],"department_position"=>"2","user_id"=>$json['orgchart_dept_assistant'][0],"orgChart_id"=>$value_dept['orgChart_id']));
		}
		echo "assistant head ".$json['orgchart_dept_assistant'][0]."<br />";
		//members
		foreach ($json['orgchart_dept_members'] as $user_id) {
			$db->insert("tbdepartment_users",array("department_code"=>$value_dept['department_code'],"department_position"=>"3","user_id"=>$user_id,"orgChart_id"=>$value_dept['orgChart_id']));
			echo "members ".$user_id."<br />";
		};
		echo "<br /><br /><br />";
	}
	$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migrate_0001_dept_users"));
	echo "DONE migrate_0001_dept_users<br />";
}else{
	echo "Not Permitted migrate_0001_dept_users<br />";
}

$getMigrationFlag_AnnouncementUsers = $db->query("SELECT * FROM tbaudit_logs WHERE user_id = 0 AND audit_action = 0 AND table_name = 'migrate_0001_announcement_users'","numrows");
$postPrivacyIDs = "";
if($getMigrationFlag_AnnouncementUsers==0){
	$getAnnouncement = $db->query("SELECT p.*, u.company_id FROM tbpost p LEFT JOIN tbuser u on p.postedBy = u.id WHERE p.postPrivacyType != 0 AND p.postPrivacyIDs !=''","array");
	foreach ($getAnnouncement as $key => $value) {
		$privacyType =  $value['postPrivacyType'];
		$privacy =  split(",",$value['postPrivacyIDs']);
		foreach ($privacy as $key2 => $value2) {
			$postPrivacyIDs = $value2;
			if($privacyType==3){
				$getPosition = $db->query("SELECT id FROM tbpositions WHERE position = '". $value2 ."' AND company_id = '". $value['company_id'] ."'","row");
				$postPrivacyIDs = $getPosition['id'];
			}
			$arrayToInsert = array("announcement_id"=>$value['id'],"user"=>$postPrivacyIDs,"user_type"=>$privacyType);
			print_r($arrayToInsert);
			echo $db->insert("tb_announcement_user",$arrayToInsert);
			echo "<br>----------------------------<br>";

		}
	}
	$db->insert("tbaudit_logs",array("user_id"=>"0","audit_action"=>"0","table_name"=>"migrate_0001_announcement_users"));
}else{
	echo "Not Permitted migrate_0001_announcement_users<br />";
}

