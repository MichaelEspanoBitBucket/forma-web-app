<?php

$db = new Database();

//echo "not permitted";return false;
//create form category users
$db->query("CREATE TABLE IF NOT EXISTS `tbform_category_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_category_id` int(11) NOT NULL,
  `user` text NOT NULL,
  `user_type` tinyint(1) NOT NULL,
  `access_type` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=0");

//create form users
$db->query("CREATE TABLE IF NOT EXISTS `tbform_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  -- `form_category_users_id` int(11) NOT NULL,
  `user` text NOT NULL,
  `user_type` tinyint(1) NOT NULL,
  `action_type` int(11) NOT NULL, /* action 1 = author, 2 = viewer 3 = user, 4 = admin */
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;");

//create position
$db->query("CREATE TABLE IF NOT EXISTS `tbpositions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(255) NOT NULL,
  `company_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;");

//create department users
$db->query("CREATE TABLE IF NOT EXISTS `tbdepartment_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_code` varchar(255) NOT NULL,
  `department_position` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=0 ;");

/*
Migrate form authors, viewer and admin, form category users
7/28/2014
*/

// $getFormCategories = $db->query("SELECT id,users FROM tbform_category ws where is_delete = 0","array");

// foreach ($getFormCategories as $formCategory) {
// 	$db->query("DELETE from tbform_category_users WHERE form_category_id = '". $formCategory['id'] ."'","");

// 	$form_category_users = json_decode($formCategory['users'],true);

// 	setUsersMigration($db,$form_category_users,$formCategory,2,2);
// }



// $getForms = $db->query("SELECT id,form_authors,form_viewers,form_admin, category_id FROM tb_workspace ws where is_delete = 0 AND category_id !=0","array");
// foreach ($getForms as $form) {
// 	$db->query("DELETE from tbform_users WHERE form_id = '". $form['id'] ."'","");

// 	$form_author = json_decode($form['form_authors'],true);
// 	$form_viewers = json_decode($form['form_viewers'],true);
// 	$form_admin = json_decode($form['form_admin'],true);


// 	setUsersMigration($db,$form_author,$form,2,1,1);
// 	setUsersMigration($db,$form_viewers,$form,2,1,2);


// 	setUsersMigration($db,$form_admin,$form,1,1,4);
	
// }




//remove unnecessary fields from tbworkspace
// $db->query("ALTER TABLE `tb_workspace` DROP `form_authors`;","");
// $db->query("ALTER TABLE `tb_workspace` DROP `form_viewers`;","");
// $db->query("ALTER TABLE `tb_workspace` DROP `form_admin`;","");


//remove unnecessary fields from tbformcategory
// $db->query("ALTER TABLE `tbform_category` DROP `users`;","");


//view for form category
// $db->query("create view vwget_form_categories as select id,category_name,image,company_id,
// (select group_concat(user) from tbform_category_users where form_category_id = fc.id and user_type = 1 AND access_type = 2) AS positions,
// (select group_concat(user) from tbform_category_users where form_category_id = fc.id and user_type = 2 AND access_type = 2) AS departments,
// (select group_concat(user) from tbform_category_users where form_category_id = fc.id and user_type = 3 AND access_type = 2) AS users, 
// (select group_concat(user) from tbform_category_users where form_category_id = fc.id and user_type = 3 AND access_type = 1) AS admin 
// FROM tbform_category fc where fc.is_delete = 0","");

$db->query("create view vwget_form_categories as select tbfc.id,category_name,image,tbfc.company_id,tbfcu.user_type, tbfcu.user, 
(select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 1 AND access_type = 2) AS positions,
(select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 2 AND access_type = 2) AS departments,
(select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 3 AND access_type = 2) AS users, 
(select group_concat(user) from tbform_category_users where form_category_id = tbfc.id and user_type = 3 AND access_type = 1) AS admin 
FROM tbform_category tbfc LEFT JOIN tbform_category_users tbfcu ON tbfc.id = tbfcu.form_category_id WHERE tbfc.is_delete = 0 AND 
tbfcu.access_type = 1","");



//view form category departments
$db->query("DROP VIEW vwget_form_category_departments","");
$db->query("create view vwget_form_category_departments as select fcu.id as fcuID, fcu.*, tboo.department, tbo.company_id 
				FROM tbform_category_users fcu LEFT JOIN 
				tborgchartobjects tboo on fcu.user = tboo.department_code 
				LEFT JOIN tborgchart tbo on tboo.orgchart_id = tbo.id 
				WHERE 
				tbo.is_active = 1 AND 
				tbo.status = 1 AND 
				tbo.is_delete = 0 AND 
				fcu.user_type = 2 ORDER BY tboo.department ASC","");

//view form category per position
$db->query("DROP VIEW vwgetform_category_positions","");
$db->query("create view vwgetform_category_positions as select fcu.id as fcuID, p.position,p.id, company_id, fcu.form_category_id as form_category_id
				FROM tbform_category_users fcu LEFT JOIN 
				tbpositions p on fcu.user = p.id 
				WHERE 
				p.is_active = 1 AND  
				fcu.user_type = 1 ORDER BY p.position ASC","");

//view form category per users
$db->query("DROP VIEW vwgetform_category_users","");
$db->query("create view vwgetform_category_users as select fcu.id as fcuID, u.id, u.last_name, u.first_name,u.display_name, company_id, fcu.form_category_id, fcu.access_type, u.user_level_id 
				FROM tbform_category_users fcu LEFT JOIN 
				tbuser u on fcu.user = u.id 
				WHERE 
				u.is_active = 1 AND  
				fcu.user_type = 3 ORDER BY u.display_name ASC, u.last_name Asc","");

//view form category per groups
$db->query("DROP VIEW vwgetform_category_groups","");
$db->query("create view vwgetform_category_groups as select fcu.id as fcuID, tbfp.group_name,tbfp.id, company_id, fcu.form_category_id as form_category_id
				FROM tbform_category_users fcu LEFT JOIN 
				tbform_groups tbfp on fcu.user = tbfp.id 
				WHERE 
				tbfp.is_active = 1 AND  
				fcu.user_type = 4 ORDER BY tbfp.group_name ASC","");


//view forms, categories
$db->query("DROP VIEW vwget_modules","");
$db->query("CREATE VIEW vwget_modules as SELECT 
				/* category */
				COALESCE(tbfc.id,0) as category_id,
				COALESCE(tbfc.category_name,'Others') as category_name,

				/* category users */
				tbfcu.user_type as category_usertype,
				tbfcu.user as category_user,
				tbfcu.access_type as category_accesstype,


				/* form */

				tbw.id as form_id,
				tbw.form_name,
				tbw.company_id,
				tbw.is_active as form_active,
				tbw.form_display_type as form_display_type,
				tbw.form_display_category as form_display_category,
				tbw.form_display_formula as form_display_formula,

				/* form users */
				tbfu.action_type as form_actiontype,


				/* user department*/
				tbdu.id as department_users_id,
				tbdu.department_code as department_code,
				tbdu.user_id as department_user_id, 
				tbo.id as orgchart_id,
				tbo.status as orgchart_status,


				/* user group*/

				tbfgu.user_id as group_user_id,
				tbfgu.is_active as group_is_active

				FROM tb_workspace tbw LEFT JOIN 
				tbform_category tbfc ON tbfc.id = tbw.category_id LEFT JOIN 
				tbform_category_users tbfcu ON tbfc.id = tbfcu.form_category_id LEFT JOIN 
				tbform_users tbfu ON tbfcu.user = tbfu.user AND tbfcu.user_type = tbfu.user_type AND tbfu.form_id = tbw.id LEFT JOIN 
				tbdepartment_users tbdu ON tbdu.department_code = tbfcu.user LEFT JOIN 
				tbform_groups_users tbfgu ON tbfgu.group_id = tbfcu.user LEFT JOIN 
				tborgchart tbo on tbo.id = tbdu.orgChart_id LEFT JOIN
				tbpositions tbp ON tbp.id = tbfcu.user LEFT JOIN 
				tbworkflow tbwf ON tbwf.form_id = tbw.id 
				WHERE 
				/* active forms  and workflow*/
				tbw.is_active = 1 AND tbwf.is_active = 1 AND tbw.is_delete = 0 AND tbwf.is_delete = 0","");


//view form list admin
$db->query("CREATE VIEW vwget_forms as SELECT 

				COALESCE((SELECT tbwf.id FROM tbworkflow tbwf WHERE tbwf.form_id = tbw.id AND tbwf.is_active = 1 and tbwf.is_delete = 0),0) as workflow_id,

				/* category */
				COALESCE(tbfc.id,0) as category_id,
				COALESCE(tbfc.category_name,'Others') as category_name,

				/* category users */
				tbfcu.user_type as category_usertype,
				tbfcu.user as category_user,
				tbfcu.access_type as category_accesstype,


				/* form */

				tbw.id as form_id,
				tbw.form_name,
				tbw.company_id,
				tbw.form_description,
				tbw.date_created as form_date_created,
				tbw.date_updated as form_date_updated,
				tbw.is_active as form_active,
				tbw.active_fields,

				/* form users */
				tbfu.action_type as form_actiontype,


				/* user department*/
				tbdu.user_id as department_user_id


				FROM tb_workspace tbw LEFT JOIN 
				tbform_category tbfc ON tbfc.id = tbw.category_id LEFT JOIN 
				tbform_category_users tbfcu ON tbfc.id = tbfcu.form_category_id LEFT JOIN 
				tbform_users tbfu ON tbfcu.user = tbfu.user AND tbfcu.user_type = tbfu.user_type AND tbfu.form_id = tbw.id LEFT JOIN 
				tbdepartment_users tbdu ON tbdu.department_code = tbfcu.user LEFT JOIN 
				tbpositions tbp ON tbp.id = tbfcu.user  
				  
				WHERE 
				tbw.is_delete = 0","");


//view workflow list admin

$db->query("CREATE VIEW vwget_workflow as SELECT 

				/* Workflow */
				tbwf.id as workflow_id,
				tbwf.title as workflow_title,
				tbwf.is_active as workflow_active,
				tbwf.date as date,


				/* category */
				COALESCE(tbfc.id,0) as category_id,
				COALESCE(tbfc.category_name,'Others') as category_name,

				/* category users */
				tbfcu.user_type as category_usertype,
				tbfcu.user as category_user,
				tbfcu.access_type as category_accesstype,


				/* form */

				tbw.id as form_id,
				tbw.form_name,
				tbw.company_id,

				/* form users */
				tbfu.action_type as form_actiontype,


				/* user department*/
				tbdu.user_id as department_user_id


				FROM tbworkflow tbwf LEFT JOIN tb_workspace tbw ON tbwf.form_id = tbw.id LEFT JOIN 
				tbform_category tbfc ON tbfc.id = tbw.category_id LEFT JOIN 
				tbform_category_users tbfcu ON tbfc.id = tbfcu.form_category_id LEFT JOIN 
				tbform_users tbfu ON tbfcu.user = tbfu.user AND tbfcu.user_type = tbfu.user_type AND tbfu.form_id = tbw.id LEFT JOIN 
				tbdepartment_users tbdu ON tbdu.department_code = tbfcu.user LEFT JOIN 
				tbpositions tbp ON tbp.id = tbfcu.user  
				  
				WHERE 
				/* active forms  and workflow*/
				tbw.is_active = 1 AND tbw.is_delete = 0 AND tbwf.is_delete = 0 ","");


echo $db->query("ALTER TABLE `tbdepartment_users` ADD orgChart_id int(11) NOT NULL;","");

echo "DONE";

