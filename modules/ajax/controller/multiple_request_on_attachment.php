<?php
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$userCompany = new userQueries();
$company = $userCompany->getCompany($auth['company_id']);
if(isset($_POST) and $_SERVER['REQUEST_METHOD']=="POST"){
	// User file path
	$path = "images/on_multiple_attachment";
	$maxsize = REQUEST_MULTIPLE_FILE_ATTACHMENT;
	$profilePic = $_FILES['multiple_file_attachment']['name'];
	
	
	    // Allowed extension for file upload in the registration    
	    $valid_formats = unserialize(FILES_EXTENSION);
	// $succ = [];
	for($key = 0; $key <  count($profilePic); $key++){
		$profilePic_size = $_FILES['multiple_file_attachment']['size'][$key]; // File Size
	    $file = $profilePic;
	    $ext = explode(".", $file[$key]);
            $last_ext = $ext[count($ext)-1];
	    
	    if(!in_array($last_ext,$valid_formats)){
                //echo "Invalid File Format";
	    	$succ[] = array("message"	=>	"Invalid File Format.",
	    		"size"=>$profilePic_size,
	    		"etype"=>"error"
					    // "location"		=>	"/"  . $p . "/",
					    // "file_name"		=>	$file[$key],
					    // "file_type"		=>	json_encode($_FILES['multiple_file_attachment'])
					    );
        }elseif(($profilePic_size > $maxsize) || ($profilePic_size == 0)) {
//				echo "File too large. File must be less than 4 megabytes.";
        	$succ[] = array("message"	=>	"File too large. File must be less than ". $fs->formatBytes($maxsize, 0) ." megabytes.",
        		"size"=>$profilePic_size,
        		"filesize"=>$_FILES,
	    		"etype"=>"error"
					    // "location"		=>	"/"  . $p . "/",
					    // "file_name"		=>	$file[$key],
					    // "file_type"		=>	json_encode($_FILES['multiple_file_attachment'])
					    );
	    }else{
	    	$x = $key * time();
			$p = $path . "/" . $x . "-" . $key . "-" . time() . md5($auth['id']);
			if(!is_dir($p)){
			    mkdir($p);
			    error_log("Option -Indexes", 3, APPLICATION_PATH."/".$p."/.htaccess");
			}

			$fileName = preg_replace('/[^.[:alnum:]_-]/','_',trim($file[$key]));  // converting all on alphanumeric chars to _
			$fileName = preg_replace('/\.*$/','',$fileName); // removing dot . after file extension

			$thumbnail = $p . "/" . $fileName;        
			move_uploaded_file($_FILES['multiple_file_attachment']['tmp_name'][$key],$thumbnail);
			//$files = [];
			//    $files["files"] = "/"  . $thumbnail;
			    $succ[] = array("successful"	=>	"Your file was successfully selected.",
			    	"message"=>"ok",
					    "location"		=>	"/"  . $p . "/",
	    				"etype"=>"success",
	    				"userID"=>$auth['id'],
					    "size"=>$profilePic_size,
					    "file_name"		=>	$fileName,
					    "file_type"		=>	json_encode($_FILES['multiple_file_attachment']));
			//$succ[] = array("sada"=>$files);
		
	    }

	}
        
             	echo json_encode($succ);           
        
            
}


?>