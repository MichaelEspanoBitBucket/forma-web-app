<?php

$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions();
$date = $fs->currentDateTime();
$selection_type = $_POST["selectionType"];
$selectedValuesRestore = $_POST["selectedValues"];
$search = $_POST["search"];
$sortIndex = $_POST["iSortCol_0"];

if (!Auth::hasAuth('current_user')) {
    return false;
}

$order_by = "";
if ($sortIndex == 0) {
    $order_by = "display_name";
} else {
    $order_by = "position.position";
}
$short_list_type = $_POST["shortListType"];

switch ($short_list_type) {
    case "All Registered Users" :
        $short_list_condition = " AND user_level_id <> 4";
        break;
    case "All Guest Users" :
        $short_list_condition = " AND user_level_id = 4";
        break;
    default :
        $short_list_condition = "";
        break;
}
$start = 0;

$sort = $_POST["sSortDir_0"];

if ($_POST["iDisplayStart"]) {
    $start = $_POST["iDisplayStart"];
}


if ($search == "") {
    $users = $db->query("SELECT user.*,  position.position as user_position FROM tbuser user"
            . " LEFT JOIN tbpositions position"
            . " ON  position.id = user.position"
            . " WHERE "
            . " user.company_id = {$auth["company_id"]} AND user.is_active=1 {$short_list_condition} ORDER BY {$order_by} {$sort} LIMIT {$start},{$_POST["iDisplayLength"]}");

    $usersCount = $db->query("SELECT * FROM tbuser WHERE company_id = {$auth["company_id"]} AND is_active=1 {$short_list_condition}");
} else {
    $users = $db->query("SELECT user.*,  position.position as user_position FROM tbuser user"
            . " LEFT JOIN tbpositions position"
            . " ON  position.id = user.position"
            . " WHERE (display_name like '%{$search}%' OR position.position like '%{$search}%') AND "
            . " user.company_id = {$auth["company_id"]} AND user.is_active=1 {$short_list_condition} ORDER BY {$order_by} {$sort} LIMIT {$start},{$_POST["iDisplayLength"]}");
    $usersCount = $db->query("SELECT user.*,  position.position as user_position FROM tbuser user"
            . " LEFT JOIN tbpositions position"
            . " ON  position.id = user.position WHERE (display_name like '%{$search}%' OR position.position like '%{$search}%') AND user.company_id = {$auth["company_id"]} AND user.is_active=1 {$short_list_condition}");
}


$output = $_POST;
$output["iTotalRecords"] = count($usersCount);
$output["iTotalDisplayRecords"] = count($usersCount);
$output["aaData"] = array();

foreach ($users as $user) {
    $row = array();

    $checkbox = "";
    if ($selection_type == "Multiple") {
        $pos = strpos($selectedValuesRestore, $user["display_name"]);
        $checked = "";
        if ($pos >= -1) {
            $checked = "checked";
        }

        $checkbox = "<label><input type='checkbox' class='names-selection css-checkbox' id='" . $user["id"] . "' " . $checked . "></input>";
        $checkbox.= "<label for='" . $user["id"] . "' class='css-label'></label>";
        $checkbox.='<span class="limit-text-ws">' . $user["display_name"] . '</span>';
        array_push($row, "<div class='fl-table-ellip' return-value='" . $user["display_name"] . "' return-field='" . $_POST["returnField"] . "'>" . $checkbox . "</div>");
    } else {
        array_push($row, "<div class='fl-table-ellip'  return-value='" . $user["display_name"] . "' return-field='" . $_POST["returnField"] . "'>" . $user["display_name"] . "</div>");
    }

    array_push($row, "<div class='fl-table-ellip'  return-value='" . $user["display_name"] . "' return-field='" . $_POST["returnField"] . "'>" . $user["user_position"] . "</div>");
    $output['aaData'][] = $row;
}

print_r(json_encode($output));
/*
  //START TABLE
  echo '<h3><i class="icon-key"></i><span class="popup-picklist-title">Select Name</span></h3>';
  echo '<div class="hr"></div>';
  echo '<div></div>';
  echo "<div class='fl-list-of-app-record-wrapper'><table id='namesData' class='display_data dataTable' style='cursor:pointer'><thead class='fl-header-tbl-wrapper'>";
  echo "<tr>";
  echo "<th>Account Name</th>";
  echo "</tr></thead>";
  echo "<tbody>";

  foreach ($users as $userDoc) {
  echo "<tr class='select_name' return-value='" . $userDoc->display_name . "' return-field='" . $_GET["returnField"] . "'>";
  $checkbox = "";
  if ($selection_type == "Multiple") {
  $pos = strpos($selectedValuesRestore, $userDoc->display_name);
  $checked = "";
  if ($pos >= -1) {
  $checked = "checked";
  }
  $checkbox = "<label><input type='checkbox' class='names-selection css-checkbox' id='" . $userDoc->id . "' " . $checked . "></input>";
  $checkbox.= "<label for='" . $userDoc->id . "' class='css-label'></label>";
  $checkbox.='<span class="limit-text-ws">' . $userDoc->display_name . '</span>';
  echo "<td>" . $checkbox . "</td>";
  } else {
  echo "<td>" . $userDoc->display_name . "</td>";
  }

  echo "</tr>";
  }

  echo "</tbody>";
  echo "</table></div>";

  if ($selection_type == "Multiple") {
  echo '<input type="button" class="btn-blueBtn namesOkButton fl-margin-right fl-margin-bottom fl-positive-btn fl-buttonEffect" return-field="' . $_GET["returnField"] . '" object_type="workflow_chart-fields-trigger" id="" value="Ok" node-data-id="node_2" style="" >';
  }
 