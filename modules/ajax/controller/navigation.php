<?php

//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    echo "logged out";
    return false;
}

$session = new Auth();
$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$userCompany = new userQueries();
$company_id = $auth['company_id'];
$userID = $auth['id'];
$timezone = "Asia/Manila";
$search = new Search();
$navigation = new Navigation();
$redis_cache = getRedisConnection();

if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

    if(isset($_POST['action'])){
        $action = $_POST['action'];
        $date = $fs->currentDateTime();
        
        if($action=="loadNavigation"){
            $search_value = $_POST['search_value'];
            $start = $_POST['iDisplayStart'];
            $end = "7";
            if($_POST['limit']!=""){
                $end = $_POST['limit'];
            }
            $limit = "";
            if($start!=""){
                $limit = " LIMIT $start, $end";
            } 
            $orderBy = " ORDER BY is_active DESC";

            if($_POST['column-sort']){
              $orderBy = " ORDER BY ".$_POST['column-sort']." ".$_POST['column-sort-type'];
            }

            $queryStr = "SELECT * FROM tbnavigation WHERE company_id = '". $company_id ."' AND (title like '%". $search_value ."%' OR description like '%". $search_value ."%') $orderBy";
            $getNavigation = $db->query($queryStr.$limit,"array");
            $queryCount = $db->query($queryStr,"numrows");

            $output = array(
                "sEcho" => intval($_POST['sEcho']),
                "iTotalRecords" => $queryCount,
                "iTotalDisplayRecords" => $queryCount,
                "start"=>$start,
                "aaData" => array(),
            ); 
            foreach ($getNavigation as $key => $value) {
                $company_user_info = array();  
                $actions = '<center>
                                <a class="updateNavigationView" data-id="'. $value['id'] .'" data-id="'. $value['id'] .'"><i class="icon-edit fa fa-pencil-square-o tip cursor"  title="Edit"></i></a> 
                                <a class="updateNavStatus display" data-id="'. $value['id'] .'"><i class="icon-trash fa fa fa-check-circle-o tip cursor" title="'. $fs->getStatus_revert($value['is_active']) .'" style="margin-right:5px"></i></a> 
                                <a class="deleteNavigation" data-id="'. $value['id'] .'" data-id="'. $value['id'] .'"><i class="icon-trash fa fa-trash-o tip cursor" data-placement="top" data-original-title="Delete" style="margin-right:5px"></i></a>  

                            </center>';
                $status = $fs->getStatus($value['is_active']);
                $company_user_info[] = $actions;
                $company_user_info[] = "<div class='fl-table-ellip'>" . $value['title']."</div>";
                $company_user_info[] = "<div class='fl-table-ellip'>" . $value['description']."</div>";
                $company_user_info[] = "<div class='fl-table-ellip'>" . $status."</div>";
                
                $output['aaData'][] = $company_user_info;   
            }
            echo json_encode($output);
        }else if($action=="saveNavigation"){
            $navID = $_POST['navID'];

            $title = $_POST['title'];
            $description = $_POST['description'];
            $status = $_POST['status'];
            $deletedMenu = json_decode($_POST['deletedMenu']);
            $navJSONProperties = $_POST['navJSONProperties'];
            if($status=="1"){
                $db->update("tbnavigation",array("is_active"=>0),array("company_id"=>$company_id));
            }
            $insertNav = array("title"=>$title,
                                "description"=>$description,
                                "updated_by"=>$userID,
                                "date_updated"=>$date,
                                "is_active"=>$status,
                                "nav_json_data"=>$navJSONProperties
                                );

            
            $db->update("tbnavigation",$insertNav,array("id"=>$navID));
            $navObjects = json_decode($_POST['navObjects'],true);
            $unchecked = "";
            foreach ($navObjects as $key => $value) {

                $insertNavObj = array("navigation_id"=>$navID,
                                "data_id"=>$value['data_id'],
                                "indexing"=>$value['indexing'],
                                "parent_id"=>$value['parent_id'],
                                "address"=>$value['address'],
                                "title"=>$value['title'],
                                "link_type"=>$value['link_type'],
                                "form_link_type"=>$value['form_link_type'],
                                "form_id"=>$value['form_id'],
                                "other_text"=>$value['other_text'],
                                "navigation_users"=>$value['navigation_users'],
                                "enableFilters"=>$value['enableFilters'],
                                "fieldFilters"=>$value['fieldFilters']
                                // "fields_date_filter"=>$value['fields_date_filter'],
                                );
                $navObjectID = $value['menu_id'];
                if($navObjectID=="0"){
                    $navObjectID = $db->insert("tbnavigation_object",$insertNavObj);
                }else{
                    $db->update("tbnavigation_object",$insertNavObj,array("id"=>$navObjectID));
                }

                //add
                $checkedEle = json_decode($value['checkedEle'],true);
                foreach ($checkedEle as $key => $valueChecked) {
                    $user_type = functions::setUserType($key);

                    //insert
                    foreach ($valueChecked as $key => $user) {
                        $insert = array("navigation_object_id" => $navObjectID,
                            "user" => $user,
                            "user_type" => $user_type,
                        );
                        $db->insert("tbnavigation_users",$insert);
                    }
                }


                //deleted
                $uncheckedEle = json_decode($value['unCheckedEle'],true);

                foreach ($uncheckedEle as $key => $valueUnChecked) {
                    $user_type = functions::setUserType($key);

                    //insert
                    foreach ($valueUnChecked as $key => $user) {
                        $insert = array("navigation_object_id" => $navObjectID,
                            "user" => $user,
                            "user_type" => $user_type,
                        );
                        $db->delete("tbnavigation_users",$insert);
                    }
                }
            }


            //delete all menu in deleted menu array
            foreach ($deletedMenu as $key => $valueDeleted) {
                $db->delete("tbnavigation_object",array("id"=>$valueDeleted));
            }

            $sideBarNavigation = $navigation->setMenuNavigation();
            $activeMenu = $navigation->activeMenu;
            if($activeMenu==0){
                $sideBarNavigation = $navigation->setOldMenuModules();
            }

            if($redis_cache){
                $redis_cache->del("leftbar_nav");
            }else{
                $session->setAuth('getLeftBar',$sideBarNavigation);
            }
            echo json_encode(array("id"=>$navID,"navObjects"=>$navObjects,"menu"=>$sideBarNavigation));
        }else if($action=="addNavigation"){
            $title = $_POST['title'];
            $description = $_POST['description'];
            $status = $_POST['status'];
            $navJSONProperties = $_POST['navJSONProperties'];
            if($status=="1"){
                $db->update("tbnavigation",array("is_active"=>0),array("company_id"=>$company_id));
            }

            $insertNav = array("title"=>$title,
                                "description"=>$description,
                                "company_id"=>$company_id,
                                "created_by"=>$userID,
                                "date_created"=>$date,
                                "is_active"=>$status,
                                "nav_json_data"=>$navJSONProperties
                                );

            $navID = $db->insert("tbnavigation",$insertNav);

            $navObjects = json_decode($_POST['navObjects'],true);

            foreach ($navObjects as $key => $value) {

                $insertNavObj = array("navigation_id"=>$navID,
                                "data_id"=>$value['data_id'],
                                "indexing"=>$value['indexing'],
                                "parent_id"=>$value['parent_id'],
                                "address"=>$value['address'],
                                "title"=>$value['title'],
                                "link_type"=>$value['link_type'],
                                "form_link_type"=>$value['form_link_type'],
                                "form_id"=>$value['form_id'],
                                "other_text"=>$value['other_text'],
                                "navigation_users"=>$value['navigation_users'],
                                "enableFilters"=>$value['enableFilters'],
                                "fieldFilters"=>$value['fieldFilters']
                                // "fields_date_filter"=>$value['fields_date_filter'],

                                );
                $navObjectID = $db->insert("tbnavigation_object",$insertNavObj);
                $navigation_users = json_decode($value['navigation_users'],true);
                foreach ($navigation_users as $key => $valueChecked) {
                    $user_type = 0;
                    if ($access_type != "4") {
                        if ($key == "positions") {
                            $user_type = 1;
                        }
                        if ($key == "departments") {
                            $user_type = 2;
                        }
                        if ($key == "groups") {
                            $user_type = 4;
                        }
                    }
                    if ($key == "users") {
                        $user_type = 3;
                    }

                    //insert
                    foreach ($valueChecked as $key => $user) {
                        $insert = array("navigation_object_id" => $navObjectID,
                            "user" => $user,
                            "user_type" => $user_type,
                        );
                        $db->insert("tbnavigation_users",$insert);
                    }
                }
            }
            if($redis_cache){
                $redis_cache->del("leftbar_nav");
            }
            echo json_encode(array("id"=>$navID,"menu"=>$navigation->setMenuNavigation()));
        }else if($action=="getNavigation"){
            $id = $_POST['id'];
            $getNav = $navigation->getSpecificNavigation(" tb_n.id = $id");
            echo $getNav;
        }else if($action=="deleteNavigation"){
            $id = $_POST['id'];
            echo $db->create("DELETE tbn, tbno, tbnu FROM tbnavigation tbn  
                LEFT JOIN tbnavigation_object tbno 
                ON tbn.id = tbno.navigation_id 
                LEFT JOIN tbnavigation_users tbnu 
                ON tbno.id = tbnu.navigation_object_id
                WHERE tbn.id = '". $id ."'");

            if($redis_cache){
                $redis_cache->del("leftbar_nav");
            }
        }else if($action=="updateStatus"){
            // $id = $_POST['id'];
            // $navStatusQuery = $db->query("SELECT is_active FROM tbnavigation WHERE id = $db->escape{$id}","row");
            // $navStatus = $navStatusQuery['is_active'];
            // echo $navStatus;
        }else if($action=="getLeftBar"){
            
            if($redis_cache){


                $cachedFormatNav = "$userID";
                $cache_nav = json_decode($redis_cache->get("leftbar_nav"),true);
                $cached_query_result_nav = $cache_nav[''. $cachedFormatNav .''];


                if($cached_query_result_nav){
                    $sideBarNavigation = $cached_query_result_nav;
                }else{
                    $sideBarNavigation = $navigation->setMenuNavigation();
                    $activeMenu = $navigation->activeMenu;
                    if($activeMenu==0){
                        $sideBarNavigation = $navigation->setOldMenuModules();
                    }

                    $cache_nav[''. $cachedFormatNav .''] = $sideBarNavigation;
                    $redis_cache->set("leftbar_nav", json_encode($cache_nav));
                }

                echo $sideBarNavigation;
                
                
            }else{
                $getLeftBarSession = $session->hasAuth('getLeftBar');
                if($getLeftBarSession){
                    echo html_entity_decode($session->getAuth('getLeftBar'));
                }else{
                    $sideBarNavigation = $navigation->setMenuNavigation();
                    $activeMenu = $navigation->activeMenu;
                    if($activeMenu==0){
                        $sideBarNavigation = $navigation->setOldMenuModules();
                    }
                    $session->setAuth('getLeftBar',htmlentities($sideBarNavigation));
                    echo $sideBarNavigation;
                }
            }
        }

    }


?>