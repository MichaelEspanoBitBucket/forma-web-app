<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}


$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$search = new Search();
$user_id = $auth['id'];
if(isset($_POST['action'])){
	if($_POST['action']=="setNavui"){
		$navuidisplay_a = $_POST['navuidisplay_a'];
		$navuidisplay_b = $_POST['navuidisplay_b'];
		$navuitype = $_POST['navuitype'];
		//echo $navuidata . $navuitype;
		$getNavui = $fs->getDashboardNavView(0);
		//insert / update array
		$insertNavui = array("navuidisplay_a"=>$navuidisplay_a,
							"navuidisplay_b"=>$navuidisplay_b,
							"navuitype"=>$navuitype,
							"user_id"=>$auth['id']);

		if(count($getNavui)=="0"){
			//insert
			$db->insert("tbdashboard_navview",$insertNavui);
		}else{
			//update
			$whereNavui = array(
						"user_id"=>$auth['id']);			
			$db->update("tbdashboard_navview",$insertNavui,$whereNavui);
		}

		$navuidisplay_a_empty = json_decode($navuidisplay_a, true);
		
		$redis_cache = getRedisConnection();
		//reset nav of user
		if($redis_cache){
			

		    $cachedFormat = "$user_id";

		    $cache_user_nav = json_decode($redis_cache->get("user_nav"),true);

		    $cached_query_result = $cache_user_nav[''. $cachedFormat .''];

		    if($cached_query_result){
		        unset($cache_user_nav[''. $cachedFormat .'']);
		        $redis_cache->set("user_nav", json_encode($cache_user_nav));
		    }
		}
	}
}
?>

