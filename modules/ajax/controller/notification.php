<?php

//check if session is alive if not.. retun false;
if (!Auth::hasAuth('current_user')) {
    return false;
}

$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$notifications = new notifications();
$upload = new upload();
$post = new post();

if (isset($_POST['action'])) {

    $action = $_POST['action'];
    $date = $fs->currentDateTime();

    // 
    if ($action == "notificationPost") {
        $json = array("count_notif" => $_POST['count_notif'],
            "limit" => $_POST['limit']);
        echo json_encode($notifications->noti($json));
        // $notifications->noti();
    } elseif ($action == "updateNoti") {
        $notiID = $_POST['notiID'];

        $set = array("user_read" => 1);
        $condition = array("id" => $notiID);

        $db->update("tbnotification", $set, $condition);
    } else if ($action == "updateisRead") {
        $set = array("is_read" => 1);
        $condition = array("userID" => $auth['id']);

        $db->update("tbnotification", $set, $condition);
        // echo "hello";
    }
}
?>
