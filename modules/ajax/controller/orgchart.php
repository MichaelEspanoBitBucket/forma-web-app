<?php

//check if session is alive if not.. retun false;
if (!Auth::hasAuth('current_user')) {
    return false;
}

$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;

if (isset($_POST['action'])) {
    $date = $fs->currentDateTime();

    switch ($_POST['action']) {

        case "saveOrgchart":
            $orgchart = $_POST['json_encode'];
            $json = json_decode($orgchart, true);
            //DEACTIVATE OLD ORGCHART
            if ($json['status'] == 1) {
                $update_active = array('status' => '0');
                $where_active = array('company_id' => $auth['company_id']);
                $db->update("tborgchart", $update_active, $where_active);
            }
            //INSERT ORGCHART DETAILS
            $insert = array("company_id" => $auth['company_id'],
                "title" => $json['title'],
                "description" => $json['description'],
                "date" => $date,
                "status" => $json['status'],
                "json" => json_encode(array("width" => $json['width'], "height" => $json['height'], "template_form_size" => $json['template_form_size'])),
                "is_active" => 1,
                "created_by" => $auth['id']
            );
            $orgchart_id = $db->insert("tborgchart", $insert);
            //INSERT ORGCHART OBJECTS
            foreach ($json['nodes'] as $nodes) {
                $department_code = $nodes['orgchart_dept_code'];

                if ($nodes['orgchart_dept_status'] == "1") {
                    $department_code = $nodes['orgchart_dept_code_existing'];
                }
                $insert_obects = array("orgChart_id" => $orgchart_id,
                    "object_id" => $nodes['node_data_id'],
                    "department" => $nodes['node_text'],
                    "department_code" => $department_code,
                    "json" => json_encode($nodes),
                );
                $department_id = $db->insert("tborgchartobjects", $insert_obects);

                // for auto generated department code
                if ($nodes['orgchart_dept_code_type'] == 0) {
                    // update department code here
                    $auto_generated_department_code = "DC-" . $department_id . "_" . $nodes['node_text'];
                    $nodes['orgchart_dept_code_existing'] = $auto_generated_department_code;
                    $nodes['orgchart_dept_code'] = $auto_generated_department_code;

                    //update department code
                    $db->update("tborgchartobjects", array("department_code" => $auto_generated_department_code, "json" => json_encode($nodes)), array("id" => $department_id));

                    //if department code is auto generated
                    $department_code = $auto_generated_department_code;
                }

                //if status = active 
                //Note: 9/22/2014 - this will not efective anymore because we will now use tbdepartment_users as a reference for the department of the users
                if ($json['status'] == 1) {
                    //head
                    $head = array('id' => $nodes['orgchart_user_head'][0]);
                    $head_set = array("department_id" => $department_id,
                        "department_position_level" => 1);
                    $db->update("tbuser", $head_set, $head);

                    //assistant head
                    $asst_head = array('id' => $nodes['orgchart_dept_assistant'][0]);
                    $asst_head_set = array("department_id" => $department_id,
                        "department_position_level" => 2);
                    $db->update("tbuser", $asst_head_set, $asst_head);

                    //members
                    foreach ($nodes['orgchart_dept_members'] as $user_id) {
                        $members_array = array("id" => $user_id);
                        //echo "members".$user_id;
                        $members_set = array("department_id" => $department_id,
                            "department_position_level" => 3);
                        $db->update("tbuser", $members_set, $members_array);
                    };
                }

                //INSERT IN TBDEPARMTNET_USERS
                //head
                $db->insert("tbdepartment_users", array("department_code" => $department_code, "department_position" => "1", "user_id" => $nodes['orgchart_user_head'][0], "orgChart_id" => $orgchart_id));



                //assistant head
                if ($nodes['orgchart_dept_assistant'][0]) {
                    $db->insert("tbdepartment_users", array("department_code" => $department_code, "department_position" => "2", "user_id" => $nodes['orgchart_dept_assistant'][0], "orgChart_id" => $orgchart_id));
                }

                //members
                foreach ($nodes['orgchart_dept_members'] as $user_id) {
                    $db->insert("tbdepartment_users", array("department_code" => $department_code, "department_position" => "3", "user_id" => $user_id, "orgChart_id" => $orgchart_id));
                };
            }

            //INSERT ORGCHART LINES
            foreach ($json['lines'] as $lines) {
                $insert_obects = array("orgChart_id" => $orgchart_id,
                    "parent" => $lines['parent'],
                    "child" => $lines['child'],
                    "json" => json_encode($lines),
                );
                $db->insert("tborgchartline", $insert_obects);
            }

            //audit trail

            $insert_audit_rec = array("user_id" => $auth['id'],
                "audit_action" => "20",
                "table_name" => "tborgchart",
                "record_id" => $orgchart_id,
                "date" => $date,
                "ip" => $_SERVER["REMOTE_ADDR"],
                "is_active" => 1);

            $audit_log = $db->insert("tbaudit_logs", $insert_audit_rec);

            $ret = array("message" => "Your Organizational Chart (" . $json['title'] . ") was successfully saved.", "id" => $orgchart_id);

            $orgchart_record_memcacached = array("orgchart_list", "orgchart_count");
            $nav_memcached = array("leftbar_nav");

            $deleteMemecachedKeys = array_merge($orgchart_record_memcacached, $nav_memcached
            );

            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            echo json_encode($ret);
            break;
        case "getHeadName":
            $id = $_POST['userID'];
            $person = new Person($db, $id);
            echo $person->first_name . " " . $person->last_name;
            break;
    }
}
?>
