<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}

$db = new Database();
$fs = new functions();
$auth = Auth::getAuth('current_user');
$email = new Mail_Notification();
$userCompany = new userQueries();
        
    $action = $_POST['action'];
    
    if(isset($action)){
        
        // Edit
        if($action=="edit_personal_info"){
            $dataID = stripslashes(htmlspecialchars($_POST['dataID'],ENT_QUOTES));
            $displayName = stripslashes(htmlspecialchars($_POST['displayName'],ENT_QUOTES));
            $firstName = stripslashes(htmlspecialchars($_POST['firstName'],ENT_QUOTES));
            $middleName = stripslashes(htmlspecialchars($_POST['middleName'],ENT_QUOTES));
            $lastName = stripslashes(htmlspecialchars($_POST['lastName'],ENT_QUOTES));
            $contactNumber = stripslashes(htmlspecialchars($_POST['contactNumber'],ENT_QUOTES));
            
                $set = array("display_name"     =>      $displayName,
                             "first_name"       =>      $firstName,
                             "middle_name"      =>      $middleName,
                             "last_name"        =>      $lastName,
                             "contact_number"   =>      $contactNumber);
            
            $condition = array("id"             =>      $dataID);
            
            $updateInfo = $db->update("tbuser",$set,$condition);
            
                $info = array("display_name"    =>      $displayName,
                             "first_name"       =>      $firstName,
                             "middle_name"      =>      $middleName,
                             "last_name"        =>      $lastName,
                             "contact_number"   =>      $contactNumber,
                             "msg"              =>      "Your information was successfully updated.");
        
            echo json_encode($info);
            
            $login = $db->query("SELECT *
                                    FROM tbuser
                                    WHERE id={$db->escape($dataID)} ","row");
            Auth::setAuth('current_user',$login);
        }
        
    }
?>