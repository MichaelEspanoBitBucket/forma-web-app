<?php

$auth = Auth::getAuth('current_user');
$conn = getCurrentDatabaseConnection();
$fs = new functions();
$date = $fs->currentDateTime();
$listViewFormula = new ListViewFormula();
$post_data = filter_input_array(INPUT_POST);
$redis_cache = getRedisConnection();
$request = new Request();
$user_id = $auth['id'];

if (!Auth::hasAuth('current_user')) {
    return false;
}
// var_dump($post_data);
$picklist_selection_type = $post_data["picklist_selection_type"];
$search = $post_data["search"];
$condition = $post_data["condition"];
$column_displays = json_decode($post_data["columnsDisplay"], true);
$return_field = $post_data["returnField"];
$returnFieldName = $post_data["returnFieldName"];
$selection_type = $post_data["valueSelectionType"];
$selectedValuesRestore = $post_data["selectedValuesRestore"];
$showPickList = $post_data['showPickListView'];

//Added by sam for encrypt / decrypt
$db = new Database();
$formObject = new Form($db, $post_data["formId"]);

$encrypted_selected_flds = $request->encrypt_decrypt_request_details($formObject->form_json);


$start = 0;
if ($post_data["iDisplayStart"]) {
    $start = $post_data["iDisplayStart"];
}

$sortIndex = $post_data["iSortCol_0"];
$post_data["column-sort-type"] = $post_data["sSortDir_0"];
$_POST["column-sort-type"] = $post_data["sSortDir_0"];

$sort_flag = $post_data['column-sort-type'];
$column_sort = "";

if ($showPickList == 'Yes' || $selection_type == "Multiple") {
    if ($sortIndex - 1 >= 0) {
        $column_sort = $column_displays[$sortIndex - 1]["FieldName"];
    }
} else {
    if ($sortIndex >= 0) {
        $column_sort = $column_displays[$sortIndex]["FieldName"];
    }
}

if ($column_sort == "Requestor") {
    $column_sort = "requester.display_name";
}


if ($column_sort == "Processor") {
    $column_sort = "processer.display_name";
}

if ($condition != "") {
    $condition = str_replace("@", "", $condition);
    $condition = " AND (" . str_replace("==", "=", $condition) . ")";
};

$obj = array(
    "column-sort" => $column_sort,
    "multi_search" => $condition,
    "picklist_search" => $condition
);

if ($picklist_selection_type == 1) {
    $connection_id = $post_data["external_database_connection_id"];
    $external_database_connection_table = $post_data["external_database_connection_table"];

    $data_source = getExternalDatabase($conn, " WHERE  company_id={$auth["company_id"]} AND id={$connection_id}"
            . " AND is_active=1 ");
    $config = $data_source[0];

    if ($data_source[0]["db_type"] == 2) {
        $ext_conn = new MSSQLDatabase($config["db_hostname"], $config["db_name"], $config["db_username"], $config["db_password"]);
        if ($ext_conn->is_connected) {
            $table_columns = $ext_conn->getTableColumns($external_database_connection_table);
            foreach ($table_columns as $index => $table_column) {
                if ($index == 0) {
                    $search_query.=" WHERE (";
                }
                $search_query .= "LOWER(t." . $table_column["COLUMN_NAME"] . ") LIKE '%" . strtolower($search) . "%' OR ";
            }
            $search_query = substr($search_query, 0, strlen($search_query) - 4) . ")";
            $search_query.=$condition;
            $end = $start + $post_data["iDisplayLength"];
            $start++;
            $clause = "SELECT * FROM(SELECT "
                    . "ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS row_id, "
                    . "* FROM " . $external_database_connection_table . " t {$search_query}) A ";

            $limit = " WHERE row_id BETWEEN {$start} AND {$end}";
            if ($column_sort != "") {
                $column_sort_query = " ORDER BY " . $column_sort . " " . $sort_flag;
            }


            if ($redis_cache) {
                $request_key = "picklist_external_ms_sql_" . $external_database_connection_table . "::" . $search . "::" . $start . "::" . $end . "::" . $column_sort . "::" . $sort_flag;
                $request_count_key = "picklist_external_ms_sql_count_" . $external_database_connection_table . "::" . $search . "::" . $start . "::" . $end . "::" . $column_sort . "::" . $sort_flag;

                $picklist_cache = json_decode($redis_cache->get($request_key), true);
                $picklist_count_cache = json_decode($redis_cache->get($request_count_key), true);
            }

            if ($picklist_cache) {
                $requests = $picklist_cache;
            } else {
                $requests = $ext_conn->query($clause . $limit . $column_sort_query);

                if ($redis_cache) {
                    $redis_cache->set($request_key, json_encode($requests));
                }
            }

            if ($picklist_count_cache) {
                $countRequest = $picklist_count_cache;
            } else {
                $countRequest = $ext_conn->query($clause, "numrows");

                if ($redis_cache) {
                    $redis_cache->set($request_count_key, json_encode($countRequest));
                }
            }
        }
    } elseif ($data_source[0]["db_type"] == 3) {
        $ext_conn = new OracleDatabase($config["db_hostname"], $config["db_name"], $config["db_username"], $config["db_password"]);

        if ($ext_conn->is_connected) {
//            if ($search != "") {
            $table_columns = $ext_conn->getTableColumns($external_database_connection_table);
            foreach ($table_columns as $index => $table_column) {
                if ($index == 0 && $search_query == "") {
                    $search_query.=" WHERE (";
                }
                $search_query .= "LOWER(t." . $table_column["COLUMN_NAME"] . ") LIKE '%" . strtolower($search) . "%' OR ";
            }
//            }

            $search_query = substr($search_query, 0, strlen($search_query) - 4) . ")";
            $search_query.=$condition;
            $end = $start + $post_data["iDisplayLength"];
            $start++;
            $clause = "SELECT * FROM (SELECT "
                    . 'row_number() OVER(ORDER BY t.empno) AS "ROW_ID",t.* '
                    . "FROM " . $external_database_connection_table . " t {$search_query}) A";

            $limit = " WHERE A.ROW_ID BETWEEN {$start} AND {$end}";

            if ($column_sort != "") {
                $column_sort_query = " ORDER BY " . $column_sort . " " . $sort_flag;
            }

            if ($redis_cache) {
                $request_key = "picklist_external_oracle_" . $external_database_connection_table . "::" . $search . "::" . $start . "::" . $end . "::" . $column_sort . "::" . $sort_flag;
                $request_count_key = "picklist_external_oracle_count_" . $external_database_connection_table . "::" . $search . "::" . $start . "::" . $end . "::" . $column_sort . "::" . $sort_flag;

                $picklist_cache = json_decode($redis_cache->get($request_key), true);
                $picklist_count_cache = json_decode($redis_cache->get($request_count_key), true);
            }

            if ($picklist_cache) {
                $requests = $picklist_cache;
            } else {
                $requests = $ext_conn->query($clause . $limit . $column_sort_query);

                if ($redis_cache) {
                    $redis_cache->set($request_key, json_encode($requests));
                }
            }

            if ($picklist_count_cache) {
                $countRequest = $picklist_count_cache;
            } else {
                $countRequest = $ext_conn->query($clause, "numrows");

                if ($redis_cache) {
                    $redis_cache->set($request_count_key, json_encode($countRequest));
                }
            }
        }
    }
} else {
    /*
      Cache Format
      1. User Id.
      2. Page Number.
      3. Page Limit.
      4. Search Filter.
      5. Column Sort.
      6. Type of Sort.
     */
    $limit = $post_data["iDisplayLength"];
    $form_id = $post_data["formId"];

    if ($redis_cache) {
        $cachedFormat = "$user_id::$start::$limit::$search::" . $obj["multi_search"] . "::" . $obj['column-sort'] . "::" . $sort_flag . "";

        // $memcache->delete("picklist");
        $cache_picklist = json_decode($redis_cache->get("picklist"), true);
        // print_r($cache_picklist)
        $cached_query_result = $cache_picklist ['form_id_' . $form_id] ['' . $cachedFormat . ''];

        // echo "Format==".$cachedFormat;
        if ($cached_query_result) {
            // echo "from cache";

            $requests = $cached_query_result; //array();//$search->getManyRequestV2($search_value, $field, $id, $start, $limit, $obj);
        } else {
            // echo "from db";

            $searchObject = new Search();
            $requests = $searchObject->getManyRequestV2($search, "0", $post_data["formId"], $start, $post_data["iDisplayLength"], $obj);


            // $to_cache_result = array("form_id_".$id=>array($cachedFormat=>$result));
            $cache_picklist ['form_id_' . $form_id] ['' . $cachedFormat . ''] = $requests;

            if ($redis_cache) {
                $redis_cache->set("picklist", json_encode($cache_picklist));
            }
        }
    } else {
        $searchObject = new Search();
        $requests = $searchObject->getManyRequestV2($search, "0", $post_data["formId"], $start, $post_data["iDisplayLength"], $obj);
    }

    $countRequest = $requests[0][3]['count'];
}


$output = $post_data;
$output["search_query"] = $search_query;
if (count($requests) > 0) {
    $output["iTotalRecords"] = $countRequest;
    $output["iTotalDisplayRecords"] = $countRequest;
} else {
    $output["iTotalRecords"] = 0;
    $output["iTotalDisplayRecords"] = 0;
}

$output["aaData"] = array();
$aaData = array();
foreach ($requests as $request) {
    $row = array();

    if ($returnFieldName == "Processor") {
        $returnFieldName = "processors_display_name";
    }

    if ($returnFieldName == "Requestor") {
        $returnFieldName = "requestor_display_name";
    }

    foreach ($column_displays as $columnIndex => $column) {
        $field = functions::getFields("WHERE field_name={$conn->escape($column ["FieldName"])}"
                        . " AND form_id={$post_data["formId"]}");

        $checkbox = "";
        $data_value = str_replace("|^|", ",", $request[$returnFieldName]);
        $field[0]->value = $data_value;


        // Added by sam for encrypt/decrypt
        $field[0]->encryption_descryption = $encrypted_selected_flds;
        $field[0]->field_name = $returnFieldName;
        $json_field_attr = json_decode(json_encode($field[0]), true);

        $data_value = $listViewFormula->setValue($json_field_attr);

        // if (in_array($returnFieldName, $encrypted_selected_flds['encrypt'])) {
        //     $data_value = functions::encrypt_decrypt("decrypt", $data_value);
        // } else if (in_array($returnFieldName, $encrypted_selected_flds['decrypt'])) {
        //     $data_value = functions::encrypt_decrypt("decrypt", $data_value);
        // }

        $data_selector = "selection-type='" . $selection_type . "'  return-field='" . $return_field . "'  return-value='" . htmlentities($data_value, ENT_QUOTES) . "' data-form-source-id='" . $request["ID"] . "' data-trackNo='" . $request ["TrackNo"] . "' data-request-id='" . json_encode($request ['request_id']) . "'";

        $view_details = "";

        if ($showPickList == 'Yes') {
            $view_details = '<div style="float:left; width:10%"><i class="fa fa-binoculars view-picklist"></i></div>';
        }
        if ($selection_type == "Multiple" && $columnIndex == 0) {
            $selectedValuesArr = explode("|", $selectedValuesRestore);
            $pos = strpos($selectedValuesRestore, $request[$returnFieldName]);
            $is_selected = in_array($request[$returnFieldName], $selectedValuesArr);
            $checked = "";
            if ($is_selected) {
                $checked = "checked";
            }
            if ($picklist_selection_type == 1) {
                $request_id = $request["row_id"];
            } else {
                $request_id = $request["ID"];
            }
            //$request[$returnFieldName] || $selectedValuesRestore || $pos 
            $checkbox = "<label><input type='checkbox' class='request-picklist css-checkbox' id='" . $request_id . "' " . $checked . "></input>";
            $checkbox .= "<label for='" . $request_id . "' class='css-label'></label><span class='limit-text-ws'>";

            array_push($row, "<ul style='width:90%; text-align:center;' " . $data_selector . " class='fl-table-actions'><li>" . $checkbox . "</li><li>" . $view_details . "</li></ul>");
            if ($field [0]->input_type == 'Currency') {
                $text_align = "text-align:right;";

                array_push($row, "<div class='fl-table-ellip' style='width:90%;" . $text_align . "'  " . $data_selector . ">" . number_format($request[$column ["FieldName"]], 2) . "</div>");
            } else if ($field [0]->input_type == 'Number' || $field [0]->input_type == 'Double') {
                $text_align = "text-align:right";
                array_push($row, "<div class='fl-table-ellip' style='width:90%;" . $text_align . "'  " . $data_selector . ">" . $request[$column ["FieldName"]] . "</div>");
            } else {
                if ($column ["FieldName"] == "Requestor") {
//                $personDoc = new Person($db, $request[$column["FieldName"]]);
                    array_push($row, "<div style='width:90%;' class='fl-table-ellip'  " . $data_selector . " >" . str_replace("|^|", ", ", $request["requestor_display_name"]) . "</div>");
                } else if ($column ["FieldName"] == "Processor") {
                    array_push($row, "<div style='width:90%;' class='fl-table-ellip'  " . $data_selector . ">" . str_replace("|^|", ", ", $request["processors_display_name"]) . "</div>");
                } else {
                    array_push($row, "<div style='width:90%;' class='fl-table-ellip'  " . $data_selector . ">" . str_replace("|^|", ",", $request[$column ["FieldName"]]) . "</div>");
                }
            }
        } else {
            if ($columnIndex == 0 && $showPickList == 'Yes') {
                array_push($row, "<ul style='width:90%; text-align:center;' " . $data_selector . " class='fl-table-actions'><li>" . $checkbox . "</li><li>" . $view_details . "</li></ul>");
            }
            if ($field [0]->input_type == 'Currency') {
                $text_align = "text-align:right;";
                array_push($row, "<div  class='fl-table-ellip' style='width:90%;" . $text_align . "' " . $data_selector . ">" . number_format($request[$column ["FieldName"]], 2) . "</div>");
            } else if ($field [0]->input_type == 'Number' || $field [0]->input_type == 'Double') {
                $text_align = "text-align:right";
                array_push($row, "<div  class='fl-table-ellip' style='width:90%;" . $text_align . "' " . $data_selector . ">" . $request[$column ["FieldName"]] . "</div>");
            } else {

                if ($column ["FieldName"] == "Requestor") {
                    array_push($row, "<div style='width:90%;' class='fl-table-ellip' " . $data_selector . ">" . str_replace("|^|", ", ", $request["requestor_display_name"]) . "</div>");
                } else if ($column ["FieldName"] == "Processor") {
                    array_push($row, "<div style='width:90%;' class='fl-table-ellip' " . $data_selector . ">" . str_replace("|^|", ", ", $request["processors_display_name"]) . "</div>");
                } else {
                    $data_value = str_replace("|^|", ",", $request[$column["FieldName"]]);
                    // Added by sam for encrypt/decrypt
                    $field[0]->value = $data_value;
                    $field[0]->encryption_descryption = $encrypted_selected_flds;
                    $field[0]->field_name = $column ["FieldName"];
                    $json_field_attr = json_decode(json_encode($field[0]), true);

                    // $json_field_attr['encryption_descryption'] = $encrypted_selected_flds;
                    $data_value = $listViewFormula->setValue($json_field_attr);


                    array_push($row, "<div style='width:90%;' class='fl-table-ellip'" . $data_selector . "  return-value='" . htmlentities($data_value, ENT_QUOTES) . "'>" . $data_value . "</div>");
                }
            }
        }

//        }
    }
    $output["sort"] = $obj;
    $output["sort_order"] = $sort_flag;
    $output['aaData'][] = $row;
}

$conn->disConnect();
exit(json_encode($output));



