<?php
$auth = Auth::getAuth('current_user');

$db = new Database;
$search = new Search();
$userID = $auth['id'];
$fs = new functions;
//DEFINE("TBSLOT","tbslot");
//DEFINE("TBCONTAINER","tbcontainer");
//DEFINE("TBCONTAINERINSLOT","tbcontainer_in_slot");

DEFINE("TBSLOT","1_tbl_slotform");
DEFINE("TBCONTAINER","1_tbl_containerform");
DEFINE("TBCONTAINERINSLOT","1_tbl_slotcontainerform");
DEFINE("TBVELCOWORKSPACE","25_tbl_Velco");


if(isset($_POST['action'])){
   $date = $fs->currentDateTime();
   if($_POST['action']=="getContainers"){
      $slot_id = $_POST['slot_id'];
      $getSlot = $db->query("SELECT *, width*length as slot_area FROM ". TBSLOT ." WHERE id='". $slot_id ."'","row");
      $queryContainerInSlot  = "SELECT *, tbc.height as container_height, tbs.description as slot_description, tbc.description as container_description,
                                    tbc.length as container_length,
                                    tbc.width as container_width 
                                    FROM ". TBCONTAINERINSLOT ." tbis
                                    LEFT JOIN ". TBCONTAINER ." tbc on tbis.container_id = tbc.id
                                    LEFT JOIN ". TBSLOT ." tbs on tbis.slot_id = tbs.id
                                    WHERE tbis.slot_id = ". $slot_id ." AND tbc.customer = ".$auth['company_id'];
      $getContainersInSlot = $db->query($queryContainerInSlot,"array");
      $countContainers = $db->query($queryContainerInSlot,"numrows");
      
      echo "<b style='font-weight: bold;'>Slot Reference Number:</b> ".$getSlot['referencenumber'] ."<br />".
          "<b style='font-weight: bold;'>Slot Width:</b> ".$getSlot['width'] ."<br />".
          "<b style='font-weight: bold;'>Slot Length:</b> ".$getSlot['length'] ."<br />".
          "<b style='font-weight: bold;'>Slot Area:</b> ".$getSlot['slot_area'] ."<br />".
          "<b style='font-weight: bold;'>Slot Height:</b> ".$getSlot['height'] ."<br />";
      if($countContainers>0){
         echo "<br />";
         echo "<b style='font-weight: bold;'>List of own Containers</b>";
         echo "<br />";
         echo "___________________________________";
         echo "<br />";
         echo "<br />";
         foreach($getContainersInSlot as $container){
            echo "CONTAINER description: ".$container['container_description'];
            echo "<br />";
            echo "CONTAINER Height: ".$container['container_height'];
            echo "<br />";
            echo "CONTAINER LENGTH: ".$container['container_length'];
            echo "<br />";
            echo "CONTAINER WIDTH: ".$container['container_width'];
            echo "<br />";
            echo "----------------";
            echo "<br />";
         }  
      }
      
   }else{
      $getAllSlots = $db->query("SELECT *, width*length as slot_area FROM ". TBSLOT ."");
      foreach($getAllSlots as $slot){
          $queryContainerInSlot  = "SELECT *, tbc.height as container_height, tbc.customer as container_customer FROM ". TBCONTAINERINSLOT ." tbis
                                          LEFT JOIN ". TBCONTAINER ." tbc on tbis.container_id = tbc.id
                                          LEFT JOIN ". TBSLOT ." tbs on tbis.slot_id = tbs.id
                                          WHERE tbis.slot_id = ". $slot['ID'] ."";
         if($_POST['action']=="getOwnContainer"){
            $queryContainerInSlot.= " AND tbc.company_id = ".$auth['company_id'];
         }
          $getContainerInSlot = $db->query($queryContainerInSlot,"array");
          $countContainerInSlot = $db->query($queryContainerInSlot,"numrows");
          if(true){
              $slot_address = $slot['description'];
              $slot_remaining_height = $slot['height'];
              $slot_area = $slot['slot_area'];
              $company_id = $auth['company_id'];
              $ctr_company = 0;
              foreach($getContainerInSlot as $container){
                
                
                $slot_remaining_height-=$container['container_height'];
                if($container['container_customer']==$company_id){
                  $ctr_company++;
                }
              }
               $fulltxt = "";
               $otherText = "";
               $openSlottxt = "";
               $reservedtxt = "";
               $youContainertxt = "";
               
               $colorType = "";
               $slotTypeImg = "";
               
               $hasContainerBorder = "1px ridge #CCC;";
               
               $otherText = "Remaining Height: ".$slot_remaining_height ."<br />";
               if($slot_remaining_height<=0){
                  $fulltxt = setFullSlotSlotHtml();
                  $colorType = "red";
                  $slotTypeImg = "fullslot.png";
               }else{
                  if($slot['customer']>0){
                     if($company_id==$slot['customer']){
                        $reservedtxt = setReserveHtml();
                        $colorType = "green";
                        $slotTypeImg = "reserveown.png";
                     }else{
                        $reservedtxt = setReserveByOthersHtml();
                        $colorType = "orange";
                        $slotTypeImg = "reserveother.png";
                     }
                  }else{
                     $openSlottxt = setOpenSlotHtml();
                     $colorType = "blue";
                     $slotTypeImg = "openslot.png";
                  }
               }
               if($countContainerInSlot==0){
                  $otherText .= "No Container found";
               }else{
                  $otherText .= "All Containers: ".$countContainerInSlot;
                  if($ctr_company>0){
                     $otherText .= "<br />";
                     $otherText .= "<a class='cursor getContainer' slot-id='". $slot['ID'] ."'>Your Containers: ".$ctr_company."</a>";
                     $youContainertxt = "<a class='cursor getSlotInfo' slot-id='". $slot['ID'] ."'>Your Containers: ".$ctr_company."</a>";
                     $hasContainerBorder = "2px dotted #22C200;";
                  }
               }
               
               
               echo '<div style="cursor:pointer;top:'. $slot['top'] .';left:'. $slot['left'] .';position:absolute;width:'. $slot['slot_width'] .';height:'. $slot['slot_height'] .';border: '. $hasContainerBorder .';text-align: center;">'.
                        '<div class="getSlotInfo" slot-id="'. $slot['ID'] .'" style="width:100%;height:100%;background-color: '. $colorType .';"><img style="width:100%;height:100%" src="/images/velco/'. $slotTypeImg .'"></div>'.
                        //$fulltxt.
                        //$openSlottxt.
                        //$reservedtxt.
                        //'<div style="display: table-cell;vertical-align: middle;font-size:13px;font-family: Helvetica Neue, Helvetica, Arial, sans-serif;color: #333333;line-height: 15px;">'.
                        //    "Slot Reference Number: ".$slot['referencenumber'] ."<br />".
                        //    "Slot Area: ".$slot['slot_area'] ."<br />".
                        //    "Slot Height: ".$slot['height'] ."<br />".
                              //$otherText .
                              //$youContainertxt.
                        //'</div>'.
                     '</div>';
          }
      }  
   }
 }
function setReserveHtml(){
   return '<div style="position:absolute;width: 100%;background-color: #050505;padding: 5px 0px 5px 0px;color: rgb(0, 224, 20);font-weight: bolder;"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;&nbsp;Reserved for you</div>';
}
function setReserveByOthersHtml(){
   return '<div style="position:absolute;width: 100%;background-color: #050505;padding: 5px 0px 5px 0px;color: red;font-weight: bolder;"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;&nbsp;Reserved by others</div>';
}
function setOpenSlotHtml(){
   return '<div style="position:absolute;width: 100%;background-color: #050505;padding: 5px 0px 5px 0px;color: blue;font-weight: bolder;"><i class="fa fa-check"></i>&nbsp;&nbsp;&nbsp;Open Slot</div>';
}
function setFullSlotSlotHtml(){
   return '<div style="position:absolute;width: 100%;background-color: #050505;padding: 5px 0px 5px 0px;color: red;font-weight: bolder;"><i class="fa fa-warning"></i>&nbsp;&nbsp;&nbsp;Slot is full</div>';
}
?>