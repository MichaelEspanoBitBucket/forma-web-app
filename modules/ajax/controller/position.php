<?php

//check if session is alive if not.. retun false;
if (!Auth::hasAuth('current_user')) {
    return false;
}

$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$userCompany = new userQueries();
$company_id = $auth['company_id'];
$userID = $auth['id'];
$timezone = "Asia/Manila";
$search = new Search();
if (function_exists('date_default_timezone_set'))
    date_default_timezone_set($timezone);

if (isset($_POST['action'])) {
    $action = $_POST['action'];
    $date = $fs->currentDateTime();

    if ($action == "loadPositionDatatable") {
        $search_value = $_POST['search_value'];
        $start = $_POST['iDisplayStart'];
        $limit = "";
        $end = "10";
        if ($_POST['limit'] != "") {
            $end = $_POST['limit'];
        }
        if ($start != "") {
            $limit = " LIMIT $start, $end";
        }
        $orderBy = " ORDER BY position ASC";
        if ($_POST['column-sort']) {
            $orderBy = " ORDER BY " . $_POST['column-sort'] . " " . $_POST['column-sort-type'];
        }
        $query = "SELECT tbp.*, tbu_c.display_name as creator, tbu_u.display_name as editor FROM  tbpositions tbp LEFT JOIN tbuser tbu_c on tbp.createdBy = tbu_c.id LEFT JOIN tbuser tbu_u ON tbp.updatedBy = tbu_u.id WHERE  tbp.is_active = 1 AND tbp.company_id={$db->escape($auth['company_id'])} AND 
                    (tbp.position LIKE '%$search_value%' || tbp.description LIKE '%$search_value%' ||
                    tbp.createdDate LIKE '%$search_value%' || tbp.updatedDate LIKE '%$search_value%' ||
                    tbu_c.display_name LIKE '%$search_value%' || tbu_u.display_name LIKE '%$search_value%')";
        $getPositions = $db->query($query . $orderBy . $limit, "array");
        $countPositions = $db->query($query, "numrows");

        if (!$countPositions) {
            $countForm = 0;
        }


        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $countPositions,
            "iTotalDisplayRecords" => $countPositions,
            "start" => $start,
            "aaData" => array(),
        );
        foreach ($getPositions as $info) {
            // Actions
            $company_user_info = array();
            $actions = '<center><i class="icon-trash fa fa-trash-o tip cursor deletePosition" data-placement="top" data-original-title="Delete" style="margin-right:5px" data-id="' . $info['id'] . '"></i> <i class="icon-edit fa fa-pencil-square-o tip cursor updatePosition" data-placement="top" data-original-title="Edit" data-id="' . $info['id'] . '" data-name="' . htmlentities($info['position']) . '" data-desc="' . htmlentities($info['description']) . '"></i></center>';

            $createdDate = $info['createdDate'];
            if ($createdDate == "0000-00-00 00:00:00") {
                $createdDate = "";
            }
            $updatedDate = $info['updatedDate'];
            if ($updatedDate == "0000-00-00 00:00:00") {
                $updatedDate = "";
            }


            $company_user_info[] = "<div class='fl-table-ellip'>" . htmlentities($info['position']) . "</div>";
            $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list(htmlentities($info['description']), "None") . "</div>";
            $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($info['creator'], "") . "</div>";
            $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($createdDate, "") . "</div>";
            $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($info['editor'], "") . "</div>";


            $company_user_info[] = "<div class='fl-table-ellip'>" . functions::empty_in_list($updatedDate, "") . "</div>";
            $company_user_info[] = $actions;
            $output['aaData'][] = $company_user_info;
        }


        echo json_encode($output);
        // echo $query.$limit;
    } else if ($action == "deletePosition") {
        $id = $_POST['id'];
        $if_exist = $db->query("Select id from tbuser WHERE position = $id", "numrows");
        $ret = array();
        $ret['status'] = 1;

        if ($if_exist > 0) {
            $ret['status'] = 0;
        }
        $getPositionProcessor = "SELECT tbwo.id FROM tbworkflow tbw LEFT JOIN tbworkflow_objects tbwo on tbw.id = tbwo.workflow_id WHERE tbw.is_active = 1 AND tbw.is_delete = 0 AND tbwo.processorType = 2 AND tbwo.processor = '" . $id . "'";

        $if_existProcessorPosition = $db->query($getPositionProcessor, "numrows");
        if ($if_existProcessorPosition > 0) {
            $ret['status'] = 0;
        }
        if ($ret['status'] == 1) {
            $db->update("tbpositions", array("is_active" => 0), array("id" => $id));
        }


        //deletememcached
        $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
        $nav_memcached = array("leftbar_nav");
        $starred_memcached = array("starred_list");

        $deleteMemecachedKeys = array_merge($request_record_memcacached, $nav_memcached, $starred_memcached
        );
        $fs->deleteMemcacheKeys($deleteMemecachedKeys);


        echo json_encode($ret);
    } else if ($action == "addPositionOld") {
        $positions = $_POST['positions'];
        $notInserted = array();
        $inserted = array();
        $inserted_id = array();
        foreach ($positions as $value) {
            $value = trim($value);
            if ($value == "") {
                continue;
            }
            $insert = array("position" => $value,
                "company_id" => $company_id,
                "createdBy" => $auth['id'],
                "createdDate" => $date,
                "is_active" => 1);
            $if_exist = $db->query("Select * from tbpositions WHERE company_id = $company_id AND position = '" . $value . "' AND is_active = 1", "numrows");
            if ($if_exist == 0) {
                $id = $db->insert("tbpositions", $insert);
                // array_push($inserted,$value);
                // array_push($inserted_id,$id);
                $inserted[] = array("id" => $id, "value" => $value);
            } else {
                array_push($notInserted, $value);
            }
        }
        $return = array("notInserted" => $notInserted, "inserted" => $inserted);
        if (count($notInserted) > 0) {
            $return['status'] = 0;
        } else {
            $return['status'] = 1;
        }
        echo json_encode($return);
    } else if ($action == "addPosition") {
        $positions = $_POST['positions'];
        $notInserted = array();
        $inserted = array();
        $inserted_id = array();
        foreach ($positions as $value_position) {
            $value = trim($value_position['value']);
            $desc = trim($value_position['description']);
            if ($value == "") {
                continue;
            }
            $insert = array("position" => $value,
                "description" => $desc,
                "company_id" => $company_id,
                "createdBy" => $auth['id'],
                "createdDate" => $date,
                //"updatedBy"=>$auth['id'],
                //"updatedDate"=>$date,
                "is_active" => 1);
            $if_exist = $db->query("Select * from tbpositions WHERE company_id = $company_id AND position = '" . $value . "' AND is_active = 1", "numrows");
            if ($if_exist == 0) {
                $id = $db->insert("tbpositions", $insert);
                // array_push($inserted,$value);
                // array_push($inserted_id,$id);
                $inserted[] = array("id" => $id, "value" => $value);
            } else {
                array_push($notInserted, $value);
            }
        }
        $return = array("notInserted" => $notInserted, "inserted" => $inserted);
        if (count($notInserted) > 0) {
            $return['status'] = 0;
        } else {
            $return['status'] = 1;
        }
        echo json_encode($return);
    } else if ($action == "editPosition") {
        $value = $_POST['value'];
        $description = $_POST['description'];
        $id = $_POST['id'];
        $return = array();
        $if_exist = $db->query("Select * from tbpositions WHERE id != $id AND company_id = $company_id AND position = '" . $value . "' AND is_active = 1", "numrows");
        if ($if_exist == 0) {
            $db->update("tbpositions", array("position" => $value,
                "description" => $description,
                "updatedBy" => $auth['id'],
                "updatedDate" => $date), array("id" => $id));
            $return['status'] = 1;
        } else {
            $return['status'] = 0;
        }
        echo json_encode($return);
    } else if ($action == "batch") {
        $date = $fs->currentDateTime();
        $row = 0;
        // check there are no errors
        if ($_FILES['csv']['error'] == 0) {
            $name = $_FILES['csv']['name'];
            $get_extention = explode(".", $_FILES['csv']['name']);


            $ext = strtolower($get_extention[count($get_extention) - 1]);
            $type = $_FILES['csv']['type'];
            $tmpName = $_FILES['csv']['tmp_name'];

            // check the file is a csv
            if ($ext === 'csv') {
                if (($handle = fopen($tmpName, 'r')) !== FALSE) {
                    // necessary if a large csv file
                    set_time_limit(0);



                    while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                        // number of fields in the csv
                        $num = count($data);

                        // get the values from the csv
                        //mysql_query("INSERT INTO `tbuser` (`email`,`display_name`, `first_name`,`last_name`,`position`) VALUES ('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]')");
                        $position_name = trim($data[0]); //mysql_escape_string(utf8_encode(trim($data[0])));
                        $position_description = trim($data[1]); // mysql_escape_string(utf8_encode(trim($data[1])));

                        $if_exist = $db->query("Select * from tbpositions WHERE company_id = $company_id AND position = '" . $position_name . "' AND is_active = 1", "numrows");
                        if ($if_exist == 0) {
                            $insert = array("position" => $position_name,
                                "description" => $position_description,
                                "company_id" => $company_id,
                                "createdBy" => $auth['id'],
                                "createdDate" => $date,
                                //"updatedBy"=>$auth['id'],
                                //"updatedDate"=>$date,
                                "is_active" => "1");
                            $db->insert("tbpositions", $insert);
                            $row++;
                        }
                    }
                    fclose($handle);
                }
                if ($row == 0) {
                    echo json_encode(array("message" => "No Position has been uploaded, Please check your csv file.", "count" => $row, "type" => "warning"));
                } else {
                    echo json_encode(array("message" => $row . " position/s has been successfully uploaded.", "count" => $row, "type" => "success"));
                }
            } else {
                //echo "Only CSV file is allowed.";
                echo json_encode(array("message" => "Only CSV file is allowed.", "count" => $row, "type" => "error"));
            }
        } else {
            //echo "Please select a file.";
            echo json_encode(array("message" => "Please select a file.", "count" => $row, "type" => "error"));
        }
    }
}
?>