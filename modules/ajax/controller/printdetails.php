<?php

$auth = Auth::getAuth('current_user');
$db = new Database();
$userCompany = new userQueries();

$formID = $_POST['FormID'];
$requestID = $_POST['RequestID'];

//$results = $db->query("SELECT form_table_name FROM tb_generate WHERE form_id = {$db->escape($formID)}", 'row');
$results = $db->query("SELECT form_table_name FROM tb_workspace WHERE ID = {$db->escape($formID)} AND is_delete = 0", 'row');
$tblname = $results['form_table_name'];


$requestDetails = $db->query("SELECT FORM.*, REQUESTOR.display_name as Requestor_Name, REQUESTOR.display_name as Requestor_Fullname ,  
        PROCESSOR.display_name as Processor_Name FROM " . $tblname . " FORM 
        LEFT JOIN tbuser REQUESTOR
        ON REQUESTOR.id = FORM.Requestor
        LEFT JOIN tbuser PROCESSOR
        ON PROCESSOR.id = FORM.Processor
        WHERE FORM.id = {$db->escape($requestID)}");


echo json_encode($requestDetails);
?>