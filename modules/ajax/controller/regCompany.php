<?php
$db = new Database();
$fs = new functions();
$email = new Mail_Notification();
$session = new Auth();


if(isset($_POST) and $_SERVER['REQUEST_METHOD']=="POST"){
    // Registration Values
        $companyName = stripslashes(htmlspecialchars($_POST['companyName'],ENT_QUOTES));
        //$companyCode = stripslashes(htmlspecialchars($_POST['companyCode'],ENT_QUOTES));
        //$companyCNumber = stripslashes(htmlspecialchars($_POST['companyCNumber'],ENT_QUOTES));
        
        $companyEmail = stripslashes(htmlspecialchars($_POST['companyEmail'],ENT_QUOTES));
        $companyDisplayName = stripslashes(htmlspecialchars($_POST['companyDisplayName'],ENT_QUOTES));
        $companyFname = stripslashes(htmlspecialchars($_POST['companyFname'],ENT_QUOTES));
        $companyLname = stripslashes(htmlspecialchars($_POST['companyLname'],ENT_QUOTES));
        //$companyPosition = stripslashes(htmlspecialchars($_POST['companyPosition'],ENT_QUOTES));
        //$companyPassword = stripslashes(htmlspecialchars($_POST['companyPassword'],ENT_QUOTES));
        
        // Set Conditions on the registration
        
        //if($db->query("SELECT * FROM tbcompany WHERE name=".$db->escape($companyName),"numrows")>0){
        //    echo $fs->setNotification("wrong","wrong","Company name is already registered.");
        //}else{
            //if($db->query("SELECT * FROM tbcompany WHERE code=".$db->escape($companyCode),"numrows")>0){
            //    echo $fs->setNotification("wrong","wrong","Company code is already registered.");
            //}else{
                if($db->query("SELECT * FROM tbcompany WHERE email=".$db->escape($companyEmail),"numrows")>0){
                    //echo $fs->setNotification("wrong","wrong","Your email is already registered.");
                    echo "Your email is already registered.";
                }else{
                    if(ltrim($companyName, ' ') === '' || ltrim($companyEmail, ' ') === '' || ltrim($companyDisplayName, ' ') === '' || ltrim($companyFname, ' ') === '' || ltrim($companyLname, ' ') === ''){
                    //if(empty($companyName) or empty($companyEmail) or empty($companyDisplayName) or empty($companyFname) or empty($companyLname)){
                        //echo $fs->setNotification("wrong","wrong","Please Complete all required fields.");
                        echo "Please Complete all required fields.";
                    }else{
                        if(!$fs->VerifyMailAddress($companyEmail)){
                            //echo $fs->setNotification("wrong","wrong","Please type your correct email format.");
                            echo "Please type your correct email format.";
                        }else{
                                $date = $fs->currentDateTime();
                            
                                // Save company
                                
                                // Get all first letter in a word
                                $words = explode(" ", $companyName);
                                $acronym = "";
                                
                                foreach ($words as $w) {
                                  $acronym .= $w[0];
                                }
                                
                                if(count($acronym) == "1"){
                                    $acronym = $companyName;
                                }else{
                                    $acronym = $acronym;
                                }
                                
                                if(LIMIT_USERS == "1"){
                                    $num_limit = NUM_LIMIT;
                                    $limit = "1";
                                }else{
                                    $num_limit = "";
                                    $limit = "0";
                                }

                                // Encryt Licensing Code
                                $c_1 = $fs->random_chars(4);
                                $c_2 = $fs->random_chars(4);
                                $c_3 = $fs->random_chars(4);
                                $c_4 = $fs->random_chars(4);
                                $set_encrytion = $c_1 . "-" . $c_2 . "-" . $c_3 . "-" . $c_4;
                                $code = $fs->li_en_dec_method("encrypt",$set_encrytion);

                                
                                $companyInsert = array("name"=>$companyName,
                                "code"=>$acronym . "-" . time(),
                                "email"=>$companyEmail,
                                "limit_users"=>$limit,
                                "num_available"=>$num_limit,
                                "date_registered"=>$date,
                                "l_code"=>$code,
                                "is_active"=>1);
                            
                                $companyID = $db->insert("tbcompany",$companyInsert);
                                    
                                $new_password = base64_encode($date."-".$companyID);
                                $new_password = substr($new_password,17,10);

                                // Save user to db
                                $companyPassword = $fs->encrypt_decrypt("encrypt",$new_password);



                                $insertUser = array("email"=>$companyEmail,"display_name"=>$companyDisplayName,
                                                    "first_name"=>$companyFname,"last_name"=>$companyLname,
                                                    "contact_number"=>"","position"=>"",
                                                    "company_id"=>$companyID,
                                                    "password"=>$companyPassword,
                                                    "user_level_id"=>2,
                                                    "date_registered"=>$date,
                                                    "email_activate"=>0,
                                                    "is_active"=>0);
                                    $userID = $db->insert("tbuser",$insertUser);
                                    
                                // Audit Logs
                                $insert_audit_rec = array("user_id"=>$userID,
                                                          "audit_action"=>"1",
                                                          "table_name"=>"tbcompany",
                                                          "record_id"=>$companyID,
                                                          "date"=>$date,
                                                          "ip"=>$_SERVER["REMOTE_ADDR"],
                                                          "is_active"=>1);
                                $audit_log = $db->insert("tbaudit_logs",$insert_audit_rec);
                                    
                                    
                                // Send Email Confirmation
                                    // EMAIL();
                                         echo $email->notify_user("regCompany","Formalistics",$userID,"company");
                                 
                                    // Send Email For Licensing To our Company
                                
                                        echo $email->email_k_li(array("k"=>$set_encrytion,"company_name"=>$companyName));

                                        // APP CONFIG


                                        $data = array("app_user_id"         =>      $userID,
                                                                  "updated_by"          =>      $userID,
                                                                  "app_date"            =>      $date,
                                                                  "app_user_id"         =>      $userID,
                                                                  "app_company_id"      =>      $companyID,
                                                                  "app_is_active"       =>      "1",
                                                                  "app_config_type"     =>      "company");

                                        $db->insert("tb_app_configuration",$data);

                                echo "Company was successfully created.";
                                // Redirect to the home page when done for the registration
                                //$login = $session->login($companyEmail,$_POST['companyPassword'],'email','password','tbuser');

                                
                                
                        }
                    }
                }
                
            //}
        //}
}
?>