<?php
$db = new Database();
$fs = new functions();
$email = new Mail_Notification();
$session = new Auth();
$guest = new Guest_Account();



if(isset($_POST) and $_SERVER['REQUEST_METHOD']=="POST"){
    // Registration Values
	$url = $_POST['url'];
        $companyEmail = stripslashes(htmlspecialchars($_POST['companyEmail'],ENT_QUOTES));
        $companyDisplayName = stripslashes(htmlspecialchars($_POST['companyDisplayName'],ENT_QUOTES));
        $companyFname = stripslashes(htmlspecialchars($_POST['companyFname'],ENT_QUOTES));
        $companyLname = stripslashes(htmlspecialchars($_POST['companyLname'],ENT_QUOTES));
	    if (trim($companyFname)=="" || trim($companyLname)==""){
		echo "Mandatory fields must be filled-up.";
	    }else{
                if($db->query("SELECT * FROM tbuser WHERE email=".$db->escape($companyEmail). " AND user_level_id ={$db->escape(4)}","numrows")>0){
                    //echo $fs->setNotification("wrong","wrong","Your email is already registered.");
		    echo "Your email is already registered.";
                }else{
                    if(empty($companyEmail) or empty($companyFname) or empty($companyLname)){
                        //echo $fs->setNotification("wrong","wrong","Please Complete all required fields.");
			echo "Please Complete all required fields.";
                    }else{
                        if(!$fs->VerifyMailAddress($companyEmail)){
                            //echo $fs->setNotification("wrong","wrong","Please type your correct email format.");
			    echo "Please type your correct email format.";
                        }else{
                                $date = $fs->currentDateTime();
				$url  = $_POST['url'];
                                $query_str = parse_url($url, PHP_URL_QUERY);
                                parse_str($query_str, $query_params);

                                // Send Email Confirmation
                                    // EMAIL();
                                        //echo $email->notify_user("regCompany","Formalistics",$userID,"company");
					$name = $companyFname . " " . $companyLname;
				    $json = array("email"   	=>  $companyEmail,
						"password"	=>  "",
						"display_name"	=> $name,
						"first_name"	=>$companyFname,
						"last_name"	=>$companyLname,
						"type"		=>"register_guest",
						"reg_type"	=> "2",
						"date_registered"=>$date);
				    $auth = array("company_id"	=>	$query_params['company_id'],
						  "first_name"	=>$companyFname,
						"last_name"	=>$companyLname);
				    
				    $query = $guest->save_new_guest($db,$auth,$json);
                                    
				    $json_encode = json_encode(array(   "data_action"  =>  "guest",
                                                    "guest_email"  =>  $companyEmail,
                                                    "guest_url"  =>  $url));
				    
				    $insert = array("guest_user_id"     =>  $query['id'],
						    "guest_json"        =>  $json_encode,
						    "guest_type"        =>  "guest",
						    "guest_date"        =>  $date,
						    "guest_added_by"    =>  $query['id'],
						    "guest_formID"      =>  $query_params['formID'],
						    "guest_requestID"   =>  $query_params['requestID'],
						    "guest_trackNo"     =>  "",
						    "guest_is_active"   =>  "1");
				    $db->insert("tb_guest",$insert);
				    
				    
				    // Add in the table of guest expiration
				    $futureDate	= date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s", strtotime($date)) . " + 181 day"));
				    $insert = array("user_id"  =>  $query['id'],
						    "expiration_date"   =>  $futureDate,
						    "is_active" =>  "1");
				    $db->insert("tb_guest_expiration",$insert); 
				    
				    // Audit Logs
				    $insert_audit_rec = array("user_id"=>$query['id'],
							      "audit_action"=>"1",
							      "table_name"=>"tbcompany",
							      "record_id"=>$query['id'],
							      "date"=>$date,
							      "ip"=>$_SERVER["REMOTE_ADDR"],
							      "is_active"=>1);
				    $audit_log = $db->insert("tbaudit_logs",$insert_audit_rec);
				    $arr = array("notification"	=>	"User was successfully saved.",
						 "url"		=>	$url);
				    echo "User was successfully saved.";
                               
                        }
                    }
                }
	    }
}
?>