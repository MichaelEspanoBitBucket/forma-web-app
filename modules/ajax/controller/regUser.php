<?php
// header('Content-Type: text/html; charset=UTF-8');
$db = new Database();
$fs = new functions();
$auth = Auth::getAuth('current_user');
$email = new Mail_Notification();
$userCompany = new userQueries();
$session = new Auth();
$company_id = $auth['company_id'];
// Registration Values
$action = $_POST['action'];
$regEmail = stripslashes(htmlspecialchars($_POST['regEmail'], ENT_QUOTES));
$regDisplayName = stripslashes(htmlspecialchars($_POST['regDisplayName'], ENT_QUOTES));
$regFname = stripslashes(htmlspecialchars($_POST['regFname'], ENT_QUOTES));
$regLname = stripslashes(htmlspecialchars($_POST['regLname'], ENT_QUOTES));
$regPosition = stripslashes(htmlspecialchars($_POST['regPosition'], ENT_QUOTES));
$date = $fs->currentDateTime();
if (isset($action)) {

    if ($action == "batch") {

        $password = $fs->encrypt_decrypt("encrypt", "password");
        // check there are no errors
        if ($_FILES['csv']['error'] == 0) {
            $name = $_FILES['csv']['name'];
            $get_extention = explode(".", $_FILES['csv']['name']);


            $ext = strtolower($get_extention[count($get_extention) - 1]);
            $type = $_FILES['csv']['type'];
            $tmpName = $_FILES['csv']['tmp_name'];

            // check the file is a csv
            if ($ext === 'csv') {
                if (($handle = fopen($tmpName, 'r')) !== FALSE) {
                    // necessary if a large csv file
                    set_time_limit(0);

                    $row = 0;

                    // var_dump($handle);
                    // return false;
                    while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                        // number of fields in the csv
                        // $data = array_map("utf8_encode", $data); //added
                        $num = count($data);

                        // get the values from the csv
                        //mysql_query("INSERT INTO `tbuser` (`email`,`display_name`, `first_name`,`last_name`,`position`) VALUES ('$data[0]','$data[1]','$data[2]','$data[3]','$data[4]')");

                        /*
                          Added by Aaron Tolentino
                          insert the position if not in the list,
                          DO NOT INSERT POSITION ID DIRECTLY!
                         */
                        $position = trim($data[4]);
                        $position_id = 0;
                        if ($position != "") {
                            $queryPosition = "Select * from tbpositions WHERE company_id = $company_id AND position = '" . $position . "' AND is_active = 1";
                            $if_exist = $db->query($queryPosition, "numrows");
                            $pos_data = $db->query($queryPosition, "row");
                            if ($if_exist > 0) {
                                //get the positionID
                                $position_id = $pos_data['id'];
                            } else {
                                //insert
                                $insert_array = array("position" => $position,
                                    "description" => "",
                                    "company_id" => $company_id,
                                    "createdBy" => $auth['id'],
                                    "createdDate" => $date,
                                    "is_active" => 1);
                                $position_id = $db->insert("tbpositions", $insert_array);
                            }
                        }

                        $insert = array("email" => $data[0],
                            "display_name" => $data[1],
                            "first_name" => $data[2],
                            "last_name" => $data[3],
                            "position" => $position_id,
                            "user_level_id" => 3,
                            "company_id" => $auth['company_id'],
                            "date_registered" => $date,
                            "email_activate" => "1",
                            "is_active" => "1",
                            "password" => $password);
                        $db->insert("tbuser", $insert);
                        $row++;
                    //     var_dump($data);
                    // return false;
                    }
                    fclose($handle);
                }
                echo "All user was successfully registered.";
            } else {
                echo "Only CSV file is allowed.";
            }
        } else {
            echo "Please select a file.";
        }
    }

    // User Registration - save/add to the database
    elseif ($action == "registerUser") {

        if ($db->query("SELECT * FROM tbuser a 
            LEFT JOIN tbcompany b ON a.company_id=b.id WHERE a.email={$db->escape($regEmail)} ", "numrows") > 0) {


            $userInfo = $db->query("SELECT *,a.is_active as aIsActive FROM tbuser a 
            LEFT JOIN tbcompany b ON a.company_id=b.id WHERE a.email={$db->escape($regEmail)} ", "row");



            if ($userInfo['company_id'] != $auth['company_id']) {
                if($userInfo['aIsActive'] == "1"){
                    echo "User is already registered in other company (Active: " . $userInfo['name'] . ")";
                }else{
                    echo "User is already registered in other company (Deactivated: " . $userInfo['name'] . ")";
                }
            } else {
                if($userInfo['aIsActive'] == "1"){
                    echo "User is already registered in current company (Active).";
                }else{
                    echo "User is already registered in current company (Deactivated).";
                }
            }
        } else {
            if (!$fs->VerifyMailAddress($regEmail)) {
                echo "Please type your correct email format.";
            } else {
                if ($db->query("SELECT * FROM tbuser WHERE display_name={$db->escape($regDisplayName)} AND company_id={$db->escape($company_id)}", "numrows") > 0) {
                    echo "Your account name is already exist.";
                } else {
                    $date = $fs->currentDateTime();
                    $new_password = base64_encode($date."-".$auth['company_id']);
                    $new_password = substr($new_password,17,10);

                    // Save user to db
                    $password = $fs->encrypt_decrypt("encrypt", $new_password);
                    // Save user to db
                    if (ALLOW_ACTIVE_DIRECTORY == "1") {
                        $regUsername = htmlspecialchars($_POST['regUsername'], ENT_QUOTES);
                        $insertUser = array("email" => trim($regEmail), "display_name" => trim($regDisplayName),
                            "first_name" => trim($regFname), "last_name" => trim($regLname),
                            "username" => trim($regUsername),
                            "position" => $regPosition,
                            "password" => $password, "company_id" => $auth['company_id'],
                            "user_level_id" => "3","email_activate" => "1",
                            "date_registered" => $date, "is_active" => 1);
                    } else {
                        $insertUser = array("email" => trim($regEmail), "display_name" => trim($regDisplayName),
                            "first_name" => trim($regFname), "last_name" => trim($regLname),
                            "position" => $regPosition,
                            "password" => $password, "company_id" => $auth['company_id'],
                            "user_level_id" => "3","email_activate" => "1",
                            "date_registered" => $date, "is_active" => 1);
                    }

                    $userID = $db->insert("tbuser", $insertUser);
                    // Audit Logs
                    $userCompany->auditLogs($auth, "tbuser", "8", $userID);


                    // Send Email Confirmation
                    $email->notify_user("regUser", "Company_Only", $userID, "user");


                    echo "User was successfully saved.";
                }
            }
        }
    }

    // User Registration - save/add to the database
    elseif ($action == "ad_registration") {
        $email = $_POST['email'];
        $user_name = $_POST['name'];
        $name = explode(" ", $user_name);
        if (count($name) == "3") {
            $firstName = $name[0];
            $middleName = $name[1];
            $lastName = $name[2];
        } elseif (count($name) == "2") {
            $firstName = $name[0];
            $middleName = '';
            $lastName = $name[1];
        } else {
            $firstName = $name[0];
            $middleName = '';
            $lastName = $name[2];
        }
        $date = $fs->currentDateTime();
        $pw = $fs->base_encode_decode('decrypt', $_POST['cred']);
        $password = $fs->encrypt_decrypt("encrypt", $pw);



        // Save user to db
        $insertUser = array("email" => $_POST['email'], "display_name" => $user_name,
            "first_name" => $firstName, "last_name" => $lastName,
            "position" => '',
            "password" => $password, "company_id" => COMPANY_ID,
            "user_level_id" => "3",
            "date_registered" => $date, "is_active" => 1);
        $userID = $db->insert("tbuser", $insertUser);
        // Audit Logs
        $userCompany->auditLogs($auth, "tbuser", "8", $userID);

        $login = $session->login($_POST['email'], $pw, 'email', 'password', 'tbuser');

        echo "User was successfully saved.";
    }

    // Update User
    // User Registration - save/add to the database
    elseif ($action == "updateUser") {
        $userID = stripslashes(htmlspecialchars($_POST['userID'], ENT_QUOTES));
        $data_type_id = stripslashes(htmlspecialchars($_POST['data_type_id'], ENT_QUOTES));
        $regEmail = stripslashes(htmlspecialchars($_POST['editEmail'], ENT_QUOTES));
        $regUsername = htmlspecialchars($_POST['editUsername'], ENT_QUOTES);

        $regDisplayName = stripslashes(htmlspecialchars($_POST['editDisplayName'], ENT_QUOTES));
        $regFname = stripslashes(htmlspecialchars($_POST['editFname'], ENT_QUOTES));
        $regLname = stripslashes(htmlspecialchars($_POST['editLname'], ENT_QUOTES));
        $regPosition = stripslashes(htmlspecialchars($_POST['editPosition'], ENT_QUOTES));
        $regActive = stripslashes(htmlspecialchars($_POST['editActive'], ENT_QUOTES));
        $regPrivileges = stripslashes(htmlspecialchars($_POST['editPrivileges'], ENT_QUOTES));
        $update_date = stripslashes(htmlspecialchars($_POST['update_date'], ENT_QUOTES));

        $type = stripslashes(htmlspecialchars($_POST['type'], ENT_QUOTES));

        //added json
// error_reporting(E_ALL);
        $json_decode = json_decode($_POST['json'],true);

        $json_data = array("post_property"  => $json_decode['update_post'],
            "dashboard_property"  => $json_decode['update_dashboard'],
            "chat_property"  => $json_decode['update_chat']);
        $json = json_encode($json_data);

        $prompt = "";
        $fields = "";

        if ($db->query("SELECT * FROM tbuser a 
            LEFT JOIN tbcompany b ON a.company_id=b.id WHERE a.email={$db->escape($regEmail)}  AND a.id!={$db->escape($userID)}", "numrows") > 0) {
            $userInfo = $db->query("SELECT * FROM tbuser a 
            LEFT JOIN tbcompany b ON a.company_id=b.id WHERE a.email={$db->escape($regEmail)}  AND a.id!={$db->escape($userID)}", "row");


            if ($userInfo['company_id'] != $auth['company_id']) {
                // echo "User is already registered in other company (" . $userInfo['name'] . ")";
                $prompt = "User is already registered in other company (" . $userInfo['name'] . ")";
                $fields = "update_email";
            } else {
                // echo "User is already registered in current company";
                $prompt = "User is already registered in current company";
                $fields = "update_email";

            }
        } else {
            if (!$fs->VerifyMailAddress($regEmail)) {
                // echo "Please type your correct email format.";
                $prompt = "Please type your correct email format.";
                $fields = "update_email";

            } else {
                if ($db->query("SELECT * FROM tbuser WHERE display_name={$db->escape($regDisplayName)} AND id!={$db->escape($userID)} AND company_id={$db->escape($company_id)} AND is_active={$db->escape(1)}", "numrows") > 0) {
                    // echo "Your account name is already exist.";
                    $prompt = "Your account name is already exist.";
                    $fields = "update_displayName";

                } else {    
                    $date = $fs->currentDateTime();

                    $Person = new Person($db,$userID);



                    // Update COmpany Email
                    // $company = new Company($db,$auth['id']);
                    $updateCompanyUser = array("email" => trim($regEmail));
                    $conCompany = array("id" => $auth['company_id'],'email'=>$Person->email);
                    $db->update("tbcompany", $updateCompanyUser, $conCompany);

                    // Save user to db
                    // Update Company
                    //$updateCompany = array("email"=>$regEmail);
                    //$con = array("id"=>$auth['company_id']);
                    //     $db->update("tbcompany",$updateCompany,$con);
                    //Update User

                    $updateUser = array("email" => trim($regEmail),
                        "display_name" => trim($regDisplayName),
                        "username" => trim($regUsername),
                        "first_name" => trim($regFname),
                        "last_name" => trim($regLname),
                        "position" => $regPosition,
                        "is_active" => $regActive,
                        "user_level_id" => $regPrivileges,
                        "json_tbl"  =>  $json
                    );
                    $con = array("id" => $userID);
                    $db->update("tbuser", $updateUser, $con);

                    


                    //deletememcached
                    $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
                    $nav_memcached = array("leftbar_nav");
                    $starred_memcached = array("starred_list");
                    $formula_memcached = array("formula_user_info_" . trim($regDisplayName) . "::" . $auth["company_id"],
                        "formula_user_info_" . $userID);

                    $deleteMemecachedKeys = array_merge($request_record_memcacached, $nav_memcached, $starred_memcached, $formula_memcached);
                    $fs->deleteMemcacheKeys($deleteMemecachedKeys);

                    // Audit Logs
                    $userCompany->auditLogs($auth, "tbuser", "21", $userID);
                    $redis_cache = getRedisConnection();
                    if ($redis_cache) {
                        $user_cache = $redis_cache->get("person_object_" . $userID);

                        if ($user_cache) {
                            $redis_cache->del("person_object_" . $userID);
                        }
                    }
                    // $login = $db->query("SELECT *
                    //          FROM tbuser
                    //          WHERE id={$db->escape($userID)} ","row");
                    //          Auth::setAuth('current_user',$login);
                    // Update guest expiration
                    $update = array("expiration_date" => $update_date);
                    $set = array("user_id" => $userID);
                    $db->update("tb_guest_expiration", $update, $set);
                    //  Send Email Confirmation
                    // echo "User was successfully updated.";
                    $prompt = "User was successfully updated.";



                }
            }
        }

        $returnArray = array(
                             "prompt"   =>  $prompt,
                             "fields"   =>  $fields);

                    echo json_encode($returnArray);
    }


    // Delete / Deactivate User 
    elseif ($action == "deleteUser") {
        $userID = $_POST['userID'];
        $loggedAccount = $_POST['loggedAccount'];

        if ($userID == $auth['id'] && $loggedAccount == "del") {
            echo "Deactivate User logged.";
        } else {
            if ($loggedAccount == "logout") {
                $condition = array("id" => $userID);
                $set = array("is_active" => 0);

                $db->update("tbuser", $set, $condition);

                echo "User will now deactivate.";
            } else {
                $condition = array("id" => $userID);
                $set = array("is_active" => 0);

                $db->update("tbuser", $set, $condition);
                echo "User will now deactivate.";
            }
        }
        // Audit Logs
        $userCompany->auditLogs($auth, "tbuser", "22", $userID);
    }


    // Get User
    elseif ($action == "getUser") {
        $search = "";
        $otherSearch = "";
        if ($_POST['type'] == "orgchart") {
            $otherSearch = " AND user_level_id!='4'";
        }
        if (isset($_POST['search'])) {
            $search = $_POST['search'];
        }


        $getCompanyUser = $db->query("SELECT * FROM tbuser WHERE company_id={$db->escape($auth['company_id'])} AND is_active={$db->escape(1)} AND (first_name LIKE '%" . $search . "%' OR last_name LIKE '%" . $search . "%') " . $otherSearch . " ORDER BY first_name ASC", "array");
        foreach ($getCompanyUser as $user) {
            $u[] = array("userID" => $user['id'],
                "name" => $user['first_name'] . " " . $user['last_name'],
                "display_name" => $user['display_name'],
                "avatar" => $userCompany->avatarPic("tbuser", $user['id'], "20", "20", "small", "avatar")
            );
        }
        echo json_encode($u);
    }

    // Get User Information
    elseif ($action == "getUserInfo") {
        $userID = $_POST['userID'];
        $getUserInfo = $db->query("SELECT * FROM tbuser WHERE id={$db->escape($userID)}", "row");

        $guest = $db->query("SELECT * FROM tb_guest_expiration WHERE user_id={$db->escape($userID)}", "row");

        $retUser[] = array("userID" => $getUserInfo['id'],
            "email" => $getUserInfo['email'],
            "username" => $getUserInfo['username'],
            "displayName" => $getUserInfo['display_name'],
            "firstName" => $getUserInfo['first_name'],
            "lastName" => $getUserInfo['last_name'],
            "contactNumber" => $getUserInfo['contact_number'],
            "position" => $getUserInfo['position'],
            "companyID" => $getUserInfo['company_id'],
            "userLeveID" => $getUserInfo['user_level_id'],
            "departmentPositionLevel" => $getUserInfo['department_position_level'],
            "password" => $getUserInfo['password'],
            "extension" => $getUserInfo['extension'],
            "dateExpiredRegister" => $guest['expiration_date'],
            "dateRegister" => $getUserInfo['date_registered'],
            "emailActivate" => $getUserInfo['email_activate'],
            "isAvailable" => $getUserInfo['is_available'],
            "isActive" => $getUserInfo['is_active'],
            "departmentID" => $getUserInfo['department_id'],
            "json_tbl" => $getUserInfo['json_tbl'],
            "images" => $userCompany->avatarPic("tbuser", $getUserInfo['id'], "44", "44", "small", "avatar")
            // "images" => $userCompany->guest_avatarPic("tbuser", $getUserInfo['id'], "44", "44", "small", "avatar")
        );
        echo json_encode($retUser);
    } elseif ($_POST['action'] == "forgot_password") {
        $uEmail = $_POST['email'];
        if (!$fs->VerifyMailAddress($uEmail)) {
            $message = array("status" => "error", "message" => "Please type your correct email format.");
        } else {
            if ($uEmail != "") {
                $getEmail = "SELECT * FROM tbuser WHERE email = {$db->escape($uEmail)} AND is_active=1";
                $getUser = $db->query($getEmail, "row");
                $countUser = $db->query($getEmail, "numrows");
                if ($countUser == 1) {
                    $update = array("forgot_password" => 1);
                    $where = array("email" => $uEmail);
                    $update = $db->update("tbuser", $update, $where);

                    //for email
                    $id = functions::base_encode_decode("encrypt", $getUser['id']);

                    $array = array("display_name" => $getUser['display_name']);
                    // Sending Mail
                    $email->recover_password(array($getUser['email'] => $getUser['display_name']), "", $getUser['company_id'], "password", $id, $array);

                    $message = array("status" => "success", "message" => "A request to reset your formalistics password has been successfully sent to your email.");
                    //
                } else {
                    //error
                    $message = array("status" => "error", "message" => "Email is not registered");
                }
            } else {
                $message = array("status" => "error", "message" => "Please type your email address");
            }
        }
        // Audit Logs
        $userCompany->auditLogs($auth, "tbuser", "23", $getUser['id']);
        echo json_encode($message);
    } elseif ($_POST['action'] == "user_reset_password") {
        $new_password = $_POST['new_password'];
        $retype_password = $_POST['retype_password'];
        $password = $fs->encrypt_decrypt("encrypt", $new_password);
        $user_id = $_POST['user_id'];
        $set = array("password" => $password,
            "email_activate" => "1",
            "is_active" => "1",
            "forgot_password" => "0");
        $condition = array("id" => $user_id);
        $db->update("tbuser", $set, $condition);

        $Person = new Person($db, $user_id);

        echo "Successful";

        $array = array("display_name" => $Person->display_name,
            "email" => $Person->email);
        // Sending Mail
        $email->reset_password_notification(array($Person->email => $Person->display_name), "", $array);
    } else if ($_POST['action'] == "admin_reset_user_password_backup") { //removed due to issue #8002
        $user_id = $_POST['userID'];
        $password = $fs->encrypt_decrypt("encrypt", "password");
        $set = array("password" => $password);
        $condition = array("id" => $user_id);
        $userCompany->auditLogs($auth, "tbuser", "51", $userID); // for reset password
        $db->update("tbuser", $set, $condition);
    } else if ($_POST['action'] == "admin_reset_user_password") {
        $user_id = $_POST['userID']; // user id
        if(ALLOW_EMAIL_PASSWORD == 1){
            try {
                //get user info
                $getEmail = "SELECT * FROM tbuser WHERE id = {$db->escape($user_id)} AND is_active=1";
                $getUser = $db->query($getEmail, "row");

                //new password {{ userid . date }}
                // $new_password = substr(base64_encode($date."-".$user_id),0,10);
                $new_password = base64_encode($date . "-" . $user_id);
                $new_password = substr($new_password, 17, 10);

                //encrypt new password
                $password = $fs->encrypt_decrypt("encrypt", $new_password);
                $set = array("password" => $password);
                $condition = array("id" => $user_id);

                $db->update("tbuser", $set, $condition);

                //audit logs
                $userCompany->auditLogs($auth, "tbuser", "51", $user_id); // for reset password

                $array = array("display_name" => $getUser['display_name']);

                $encrypted_id = functions::base_encode_decode("encrypt", $getUser['id']);
                $email->resetPasswordByAdmin(array($getUser['email'] => $getUser['display_name']), "", $getUser['company_id'], $new_password, $encrypted_id, $array);
            } catch (Exception $error) {
                print_r($error);
            }
        }else{
            $password = $fs->encrypt_decrypt("encrypt", "password");
            $set = array("password" => $password);
            $condition = array("id" => $user_id);
            $userCompany->auditLogs($auth, "tbuser", "51", $user_id); // for reset password
            $db->update("tbuser", $set, $condition);
        }
        
    }
}
?>