<?php
//
//  A simple PHP CAPTCHA script
//
//  Copyright 2011 by Cory LaViska for A Beautiful Site, LLC
//
//  See readme.md for usage, demo, and licensing info
//
$Auth = new Auth();
$captcha = new captcha();
$Auth = new Auth();
$captcha_setup = $Auth->setAuth('captcha',$captcha->simple_php_captcha());


echo json_encode($Auth->getAuth('captcha'));