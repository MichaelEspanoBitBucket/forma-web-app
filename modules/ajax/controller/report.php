<?php

$conn = getCurrentDatabaseConnection();
$redis_cache = getRedisConnection();
$auth = Auth::getAuth('current_user');
if (!Auth::hasAuth('current_user')) {
    return false;
}
$fs = new functions;
$post_data = filter_input_array(INPUT_POST);

$date = $fs->currentDateTime();
$id = $post_data['report_id'];


if ($conn->is_connected) {
    $existingReportCount = $conn->query("SELECT id FROM tbreport WHERE company_id={$auth["company_id"]}"
            . " AND title = {$conn->escape($post_data['title'])} AND id <>  {$id}", "numrows");

    //validate unique title
    if ($existingReportCount > 0) {
        exit(json_encode(array(
            "id" => $id,
            "mode" => "error",
            "message" => "Report Title already exists."))
        );
        return;
    }

    if ($id == 0) {
        $insert_data = array(
            "form_id" => $post_data["form_id"],
            "title" => $post_data["title"],
            "description" => $post_data["description"],
            "parameters" => $post_data["parameters"],
            "columns" => $post_data["columns"],
            "plotbands" => $post_data["plotBands"],
            "symbol" => $post_data["symbol"],
            "yAxisMax" => $post_data["yAxisMax"],
            "yAxisMin" => $post_data["yAxisMin"],
            "YAxisInterval" => $post_data["yAxisInterval"],
            "company_id" => $auth['company_id'],
            "date_created" => $date,
            "created_by" => $auth['id'],
            "is_active" => "1");

        $id = $conn->insert("tbreport", $insert_data);
    } else {
        $update_data = array(
            "form_id" => $post_data["form_id"],
            "title" => $post_data["title"],
            "description" => $post_data["description"],
            "parameters" => $post_data["parameters"],
            "columns" => $post_data["columns"],
            "plotbands" => $post_data["plotBands"],
            "symbol" => $post_data["symbol"],
            "yAxisMax" => $post_data["yAxisMax"],
            "yAxisMin" => $post_data["yAxisMin"],
            "YAxisInterval" => $post_data["yAxisInterval"],
            "company_id" => $auth['company_id'],
            "date_updated" => $date,
            "updated_by" => $auth['id'],
            "is_active" => "1");

        $condition_array = array("id" => $id);
        $report_result = $conn->update("tbreport", $update_data, $condition_array);

        if ($redis_cache) {
            $report_cache = $redis_cache->get("report_object_" . $id);

            if ($report_cache) {
                $redis_cache->del("report_object_" . $id);
            }
        }
    }

    //add
    $checkedEle = json_decode($post_data['report_users_checked'], true);
    foreach ($checkedEle as $key => $valueChecked) {
        $user_type = functions::setUserType($key);
        //insert
        foreach ($valueChecked as $key => $user) {
            $insert = array("report_id" => $id,
                "user" => $user,
                "user_type" => $user_type,
            );

            $insert_user_result = $conn->insert("tbreport_users", $insert);
        }
    }


    //deleted
    $uncheckedEle = json_decode($post_data['report_users_unChecked'], true);

    foreach ($uncheckedEle as $key => $valueUnChecked) {
        $user_type = functions::setUserType($key);

        //insert
        foreach ($valueUnChecked as $key => $user) {
            $delete = array("report_id" => $id,
                "user" => $user,
                "user_type" => $user_type,
            );

            $delete_user_result = $conn->delete("tbreport_users", $delete);
        }
    }

    exit(json_encode(array(
        "id" => $id,
        "mode" => "success",
        "message" => 'Report(' . $post_data["title"] . ') has been successfully saved.'))
    );

    $conn->disConnect();
}
