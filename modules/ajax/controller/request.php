<?php

use Extensions;

$fs = new functions();
$db = new Database();
$auth = Auth::getAuth('current_user');
$ext = new Extensions();
$requestIsNewlyCreated = false;

$redis_cache = getRedisConnection();
$formID = $_POST['FormID'];
$insert_result = $_POST['ID'];
$workflow_id = $_POST['Workflow_ID'];
$node_id = $_POST['Node_ID'];
$mode = $_POST['Mode'];

//print_r($_POST["Processor"]);
//return


if ($node_id == 'Save' || $node_id == 'Cancel') {
//    $processor_type = $_POST["ProcessorType"];
    $processor_id = $_POST["ProcessorID"];
//    if ($processor_type == 1 || $processor_type == 2) {
//        //revert value
    $_POST["Processor"] = $processor_id;
//    }
}



//for parent in embeded
$status_embed = $_POST['status-embed'];
$prop_emebed = $_POST['prop-emebed'];

$keywordsField = json_decode($_POST['KeywordsField'], true);

$myRequest = new Request();
$myRequest->load($formID, $insert_result);


//validate save conflict
$formObject = new Form($db, $formID);
//$current_node_id = $db->query("SELECT LastAction FROM " . $formObject->form_table_name . " WHERE id = {$insert_result}", "row");
// if ($current_node_id["LastAction"] != $_POST["LastAction"] && $insert_result != 0) { //comment by jewel by michael 01 12 2016 09 15 AM
//     $return_arr["message"] = "Request was already processed by other user.";
//     $return_arr["headerLocation"] = "/application?id=" . functions::base_encode_decode("encrypt", $formID);
//     $return_arr['workspaceLocation'] = "/workspace?view_type=request&formID=$formID&requestID=0";
//     print_r(json_encode($return_arr));
//     return;
// }

$fields = array();
$fieldResults = $myRequest->fields;

$date = $this->currentDateTime();
$fieldEnabled = json_decode($_POST['fieldEnabled'], true);
$computedFields = json_decode($_POST['computedFields'], true);
$reservedFields = array("TrackNo", "Requestor", "Status", "Processor", "LastAction", "DateCreated", "DateUpdated", "CreatedBy", "UpdatedBy", "Unread", "Node_ID", "Workflow_ID"
    , "fieldEnabled", "fieldRequired", "fieldHiddenValues", "imported", "Repeater_Data", "SaveFormula", "CancelFormula");
$fieldRequired = "";

// For Data Encryption

$encrypted_selected_flds = $myRequest->encrypt_decrypt_request_details($formObject->form_json);


if ($mode == 'formApproval') {
    foreach ($fieldResults as $field) {
        $index_search = in_array($field['COLUMN_NAME'], $fieldEnabled);
        $reserved_index = in_array($field['COLUMN_NAME'], $reservedFields);
        $computed_index = in_array($field['COLUMN_NAME'], $computedFields);

        $fields["middleware_process"] = 0;
        if ($index_search || $reserved_index || $computed_index) {
            if (is_array($_POST[$field['COLUMN_NAME']])) {
                $fields[$field['COLUMN_NAME']] = join("|^|", $_POST[$field['COLUMN_NAME']]);
            } else if ($field['COLUMN_NAME'] == 'DateCreated' && $insert_result == '0') {
                $fields[$field['COLUMN_NAME']] = $date;
            } else if ($field['COLUMN_NAME'] == 'DateUpdated') {
                $fields[$field['COLUMN_NAME']] = $date;
            } else if ($field['COLUMN_NAME'] == 'fieldRequired') {
                //if from embed and new request only
                $fieldRequired = $_POST['fieldRequired'];
            } else {
                if ($field['COLUMN_NAME'] == 'TrackNo') {
                    $myRequest->trackNo = $_POST[$field['COLUMN_NAME']];
                }
                if ($field['COLUMN_NAME'] != 'DateCreated' && !is_null($_POST[$field['COLUMN_NAME']])) {

                    // Validate fields if set to encrypted or not

                    if (in_array($field['COLUMN_NAME'], $encrypted_selected_flds['encrypt'])) {
                        $data_to_encrypt = $fs->encrypt_decrypt("encrypt", $_POST[$field['COLUMN_NAME']]);
                        $fields[$field['COLUMN_NAME']] = $data_to_encrypt;
                    } else if (in_array($field['COLUMN_NAME'], $encrypted_selected_flds['decrypt'])) {
                        $fields[$field['COLUMN_NAME']] = $_POST[$field['COLUMN_NAME']];
                    } else {
                        $fields[$field['COLUMN_NAME']] = $_POST[$field['COLUMN_NAME']];
                    }
                    // var_dump($fields);
                }

                if ($index_search && is_null($_POST[$field['COLUMN_NAME']])) {
                    $fields[$field['COLUMN_NAME']] = "";
                }
            }
        }
    }
}

if ($mode == 'viewApproval') {
    $fields['Node_ID'] = $node_id;
    $fields['Workflow_ID'] = $workflow_id;
}

if ($insert_result == '0') {
    //for parent in embeded
    if ($status_embed == 'Draft') {
        $fields['fieldRequired'] = $fieldRequired;
        $fields['Status'] = 'Draft';
        $fields['Processor'] = '';
        $fields['LastAction'] = '';
    }

    if ($node_id == 'Draft') {
        $fields['Status'] = 'DRAFT';
    }

    $myRequest->data = $fields;
    $add_editors = $_POST["AdditionalEditors"];
    if ($add_editors != "") {
        $add_editors_arr = explode("|", $add_editors);
        $myRequest->additionalEditors = $add_editors_arr;
    }

    $add_readers = $_POST["AdditionalReaders"];
    if ($add_readers != "") {
        $add_readers_arr = explode("|", $add_readers);
        $myRequest->additionalReaders = $add_readers_arr;
    }



    try {
        $extensionResponse = $ext->executeExtension($myRequest, Extensions::$PRE_SAVE);
        if ($extensionResponse) {
            $myRequest->responseData['Pre_Save_Extension_Response'] = $extensionResponse;
        }
    } catch (Exception $ex) {
        //  TODO add logging capabilities here to know if an extension had an error
    }


    $myRequest->save();

    // For attachment only - Added by sam
    $myRequest->logSelectedFieldsInNewRequest($fieldEnabled, array("multiple_attachment_on_request"));

    // return false;

    if ($status_embed == 'Draft') {
        //execute post submit for embedded docs
        $nextNode = $myRequest->getNextWorflowNode();
        $wf_data = json_decode($nextNode["WorkflowData"], true);
        $trigger = $wf_data["workflow-trigger"];
        foreach ($trigger as $row) {
            if ($row['workflow-trigger-action'] == 'Post Submit') {
                $myRequest->executePostSubmit($row);
            }
        }
    }

    if ($node_id == 'Draft') {
        $logDoc = new Request_Log($db);
        $logDoc->form_id = $formID;
        $logDoc->request_id = $myRequest->id;
        $logDoc->details = 'DRAFT';
        $logDoc->created_by_id = $auth['id'];
        $logDoc->date_created = $date;
        $logDoc->save();
    }

    $requestIsNewlyCreated = true;
    try {
        $extensionResponse = $ext->executeExtension($myRequest, Extensions::$POST_SAVE);
        if ($extensionResponse) {
            $myRequest->responseData['Post_Save_Extension_Response'] = $extensionResponse;
        }
    } catch (Exception $ex) {
        //  TODO add logging capabilities here to know if an extension had an error
    }
} else {
    if ($node_id == 'Cancel') {
        $fields['Status'] = 'Cancelled';
        $fields['Processor'] = '';
        $fields['LastAction'] = '';
        $myRequest->emailCancelledRequest();
    }


    if ($node_id == 'Save' || $node_id == 'Cancel') {
        $logDoc = new Request_Log($db);
        $logDoc->form_id = $formID;
        $logDoc->request_id = $insert_result;

        if ($node_id == 'Save') {
            $logDoc->details = 'Saved';
        } else {
            $logDoc->details = 'Cancelled';
        }

        $logDoc->created_by_id = $auth['id'];
        $logDoc->date_created = $date;
        $logDoc->save();

        //remove lock
        $myRequest->unlockDocument();
    }

    //re-assign data
    foreach ($fields as $key => $field) {
        $myRequest->data[$key] = $field;
    }
//    $myRequest->data = $fields;

    $add_editors = $_POST["AdditionalEditors"];
    if ($add_editors != "") {
        $add_editors_arr = explode("|", $add_editors);
        $myRequest->additionalEditors = $add_editors_arr;
    }

    $add_readers = $_POST["AdditionalReaders"];
    if ($add_readers != "") {
        $add_readers_arr = explode("|", $add_readers);
        $myRequest->additionalReaders = $add_readers_arr;
    }

    $myRequest->createFieldLevelLogs($fieldEnabled);


    try {
        $extensionResponse = $ext->executeExtension($myRequest, Extensions::$PRE_MODIFY);
        if ($extensionResponse) {
            $myRequest->responseData['Pre_Modify_Extension_Response'] = $extensionResponse;
        }
    } catch (Exception $ex) {
        //  TODO add logging capabilities here to know if an extension had an error
    }

    $myRequest->modify();

    try {
        $extensionResponse = $ext->executeExtension($myRequest, Extensions::$POST_MODIFY);
        if ($extensionResponse) {
            $myRequest->responseData['Post_Modify_Extension_Response'] = $extensionResponse;
        }
    } catch (Exception $ex) {
        //  TODO add logging capabilities here to know if an extension had an error
    }
}


if ($node_id != 'Save' && $node_id != 'Cancel' && $node_id != 'Draft') {
    if ($status_embed != 'Draft') { //for parent in embeded
        $myRequest->keywordFields = $keywordsField;
        if (defined("ENABLE_FORMBUILDER_FORM_EVENTS") != null && ENABLE_FORMBUILDER_FORM_EVENTS == "1") {
            if (isset($_POST["forma_postsave_formula"])) {
                if (strlen($_POST["forma_postsave_formula"]) >= 1) {
                    $formula_event = $_POST["forma_postsave_formula"];
                    $_POST["___myRequest"] = $myRequest;
                    Request::executePostSaveEvent($formula_event);
                    $myRequest = $_POST["___myRequest"];
                    unset($_POST["___myRequest"]);
                }
            }
        }

        $myRequest->process_workflow_events = false;
        $myRequest->processWF();

        backgroundProcessRequestWF($formID, $myRequest->id);
        $FormID = $myRequest->formId;

        // Annotation
        $id_encrypt_user = md5(md5($auth['id']));
        // Create folder for the postImage
        $from = "frm-annotation-img/temporary/" . $id_encrypt_user . '/' . $FormID . '/';

        $id_encrypt = md5(md5($FormID));
        $path = "frm-annotation-img/" . $id_encrypt . '/';
        $dir = "frm-annotation-img/" . $id_encrypt;
        if (!is_dir($dir)) {
            mkdir($dir); // location
        }

        // Save to DB
        $annotation_insert = array("Anno_FormID" => $FormID,
            "Anno_RequestID" => $myRequest->id,
            "Anno_TrackNo" => $myRequest->trackNo,
            "Anno_CreatedBy" => $auth['id'],
            "Anno_UpdatedBy" => $auth['id'],
            "Anno_DataCreated" => $date,
            "Anno_DateUpdated" => $date,
            "Anno_location" => "frm-annotation-img/" . $id_encrypt . '/' . $id_encrypt_user . '/',
            "Anno_is_active" => "1");

        $db->insert("tb_annotation", $annotation_insert);

        //$trackNo = $myRequest->trackNo;
        upload::move_attach_files_all($auth['id'], $path, $auth, "", $from);


        $myRequest->createRequireComment($_POST['RequireComment']);
        try {
            $ext->executeExtension($myRequest, Extensions::$POST_PROCESS_WORKFLOW);
        } catch (Exception $ex) {
            //  TODO add logging capabilities here to know if an extension had an error
        }
    }
}

if ($mode == 'formApproval') {
    //var_dump($myRequest);
    /* ========== Move Attachment Files ========== */
    $files = $_POST['attachment_files'];

    $filename = explode(",", $files);

    // Create folder for the postImage
    $path = "images/attachment";
    $to = "images/attachment/temporaray_files/";
    $trackNo = $myRequest->trackNo;
    upload::move_attach_files($trackNo, $path, $auth, $filename, $to);
//    header('location:/application?id=' . functions::base_encode_decode("encrypt", $formID));
    // if($auth['user_level_id']=="2"){
    //     $myRequest->responseData['headerLocation'] = "/application?id=" . functions::base_encode_decode("encrypt", $formID);
    // }else{
    //     $myRequest->responseData['headerLocation'] = "/user_view/application?id=" . functions::base_encode_decode("encrypt", $formID);   
    // }
    //clear form mem_cache;
    if ($redis_cache) {

        $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

        $deleteMemecachedKeys = array_merge($myrequest_memcached);

        functions::deleteMemcacheKeys($deleteMemecachedKeys);

        functions::clearRequestRelatedCache("request_list", "form_id_" . $formID);
        functions::clearRequestRelatedCache("picklist", "form_id_" . $formID);
        functions::clearRequestRelatedCache("calendar_view", "form_id_" . $formID);
        functions::clearRequestRelatedCache("request_list_count", "form_id_" . $formID);
        functions::clearRequestRelatedCache("request_details_" . $formID, $_POST['ID']);
        functions::clearRequestRelatedCache("request_access_" . $formID, $_POST['ID']);

        //formula
        $formula_arr = array("formula_lookup_get_max", "formula_lookup_get_min", "formula_lookup_sdev",
            "formula_lookup_sdev2", "formula_lookup_avg", "formula_lookup_count_if",
            "formula_lookup_count_if", "formula_lookup_total", "formula_lookup_where",
            "formula_lookup_where_array", "formula_lookup", "formula_lookup_record_count", "formula_lookup_record_count");
        functions::clearFormulaLookupCaches($formID, $formula_arr);
    }

    $myRequest->responseData['headerLocation'] = "/application?id=" . functions::base_encode_decode("encrypt", $formID);
    $myRequest->responseData['workspaceLocation'] = "/workspace?view_type=request&formID=$formID&requestID=0";
    $myRequest->responseData['formId'] = $myRequest->formId;
    $myRequest->responseData['trackNo'] = $myRequest->trackNo;
    $myRequest->responseData['id'] = $myRequest->id;
    $myRequest->responseData['data'] = $myRequest->data;

    print_r(json_encode($myRequest->responseData, JSON_HEX_TAG|JSON_PRETTY_PRINT ));
}
if ($mode == 'viewApproval') {
    echo 'Request has been processed';
}


// for parent in embeded
if ($prop_emebed != "") {
    $prop_emebed = json_decode($prop_emebed, true);
    foreach ($prop_emebed as $value) {
        $parentFieldValue = $_POST['' . $value['embed_parent_field'] . ''];
        $childField = $value['embed_child_field'];
        $arraysForm = array("formID" => $value['embed-source-form-val-id'], "parentFieldValue" => $parentFieldValue, "childField" => $childField, "parentNodeId" => $node_id);
        routeChild($arraysForm);
    }
};

//for embeded
function routeChild($form) {
    $ext = new Extensions();
    $formID = $form['formID'];
    $parentFieldValue = $form['parentFieldValue'];
    $parentNodeId = $form['parentNodeId'];
    $childField = $form['childField'];
    $db = new Database();
    $formDoc = new Form($db, $formID);
    $myRequest = new Request();
    $tablename = $formDoc->form_table_name;


    if ($parentNodeId == "Cancel") {
        $getChildArray = $db->query("SELECT * FROM " . $tablename . " WHERE `" . $childField . "` = '" . $parentFieldValue . "'", "array");
    } else {
        $getChildArray = $db->query("SELECT * FROM " . $tablename . " WHERE Status = 'Draft' AND `" . $childField . "` = '" . $parentFieldValue . "'", "array");
    }

    //to reset middleware process
    //code ni japs
//    $getChildArrayForReset = $db->query("SELECT * FROM " . $tablename . " WHERE `" . $childField . "` = '" . $parentFieldValue . "'", "array");
//    $testingyfy = "SELECT * FROM " . $tablename . " WHERE `" . $childField . "` = '" . $parentFieldValue . "'";
//    foreach ($getChildArrayForReset as $request) {
//        $myRequest->load($formID, $request['ID']);
//        $request['middleware_process'] = '0';
//        $myRequest->data = $request;
//        $myRequest->modify();
//    }
    //replacement sa code ni japs

    $update_add = "middleware_process = 0";
    if ($parentNodeId == "Cancel") {
        unset($getChildArray);
        $update_add .= ",Status = 'Cancelled', Processor='', LastAction=''";
    }
    $db->query("UPDATE " . $tablename . $update_add . " SET " . $update_add . " WHERE `" . $childField . "` = '" . $parentFieldValue . "'");


    foreach ($getChildArray as $request) {
        $request['middleware_process'] = '0';
        $myRequest->load($formID, $request['ID']);
        $myRequest->trackNo = $request['TrackNo'];
        $myRequest->data = $request;
        $myRequest->modify();

        $myRequest->process_workflow_events = false;
        $myRequest->processWF();
        backgroundProcessRequestWF($formID, $myRequest->id);

        try {
            $ext->executeExtension($myRequest, Extensions::$ON_CHILD_CREATED);
        } catch (Exception $ex) {
            //  TODO add logging capabilities here to know if an extension had an error
        }
    }
}

?>