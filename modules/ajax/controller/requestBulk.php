<?php

$db = new Database();
$auth = Auth::getAuth('current_user');
$fs = new functions;
if (!Auth::hasAuth('current_user')) {
    return false;
}
//$date = $fs->currentDateTime();
$date = $db->currentDateTime();
$data = json_decode($_POST["data"], true);
foreach ($data as $request) {

    //if($request['Requestor_ID'] == $auth['id']){
    $formObject = new Form($db, $request["FormID"]);
    $myRequest = new Request();
    $fields = array();

    $myRequest->load($formObject->id, $request["ID"]);
    $myRequest->data['Node_ID'] = $request["Node_ID"];
    $myRequest->data['Workflow_ID'] = $request["Workflow_ID"];

    if ($request["Node_ID"] == 'Cancel' && $request['Requestor_ID'] == $auth['id']) {
        $myRequest->data['Status'] = 'Cancelled';
        $myRequest->data['Processor'] = '';
        $myRequest->data['LastAction'] = '';
    }

    if ($request["Node_ID"] == 'Save' || $request["Node_ID"] == 'Cancel' && $request['Requestor_ID'] == $auth['id']) {
        $logDoc = new Request_Log($db);
        $logDoc->form_id = $request["FormID"];
        $logDoc->request_id = $request["ID"];

        if ($request["Node_ID"] == 'Save') {
            $logDoc->details = 'Saved';
        } else {
            $logDoc->details = 'Cancelled';
        }

        $logDoc->created_by_id = $auth['id'];
        $logDoc->date_created = $date;
        $logDoc->save();
    }

//    $myRequest->data = $fields;
    $myRequest->modify();

    if ($request["Node_ID"] != 'Save' && $request["Node_ID"] != 'Cancel') {
        $myRequest->process_workflow_events = false;
        $myRequest->processWF();
        backgroundProcessRequestWF($request["FormID"], $myRequest->id);
        functions::clearRequestRelatedCache("request_details_" . $formID, $myRequest->id);
        functions::clearRequestRelatedCache("request_access_" . $formID, $myRequest->id);
    }

    sleep(5); // this should halt for 3 seconds for every loop
    //}
//print_r($request);
    //echo 'Request has been processed';
}

$formID = $data[0]['FormID'];

$request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
$myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");

$deleteMemecachedKeys = array_merge($myrequest_memcached);
functions::deleteMemcacheKeys($deleteMemecachedKeys);

functions::deleteMemcacheForm($request_record_memcacached, $formID);


functions::clearRequestRelatedCache("request_list", "form_id_" . $formID);
functions::clearRequestRelatedCache("picklist", "form_id_" . $formID);
functions::clearRequestRelatedCache("calendar_view", "form_id_" . $formID);
functions::clearRequestRelatedCache("request_list_count", "form_id_" . $formID);
?>
