<?php
$auth = Auth::getAuth('current_user');
if(!Auth::hasAuth('current_user')){ 
   return false;
}
$db = new Database;
$fs = new functions;
$userCompany = new userQueries();
$company = $userCompany->getCompany($auth['company_id']);
$upload = new upload();
// Request Attachment

if(isset($_POST) and $_SERVER['REQUEST_METHOD']=="POST"){
	$file_size = ini_get('upload_max_filesize');
	$size = substr($file_size, 0, 1);
	$trackNo = $_POST['getTrackNo'];
	    // User file path
	    if($trackNo==""){
		$path = "images/attachment/temporaray_files";
	    }else{
		$path = "images/attachment";
	    }
		
		$profilePic = $_FILES['file']['name'];
		$profilePic_size = $_FILES['file']['size']; // File Size
		
		$formID = $_POST['getFormID'];
		$requestID = $_POST['getID'];
		$track_no = $_POST['getTrackNo'];
	    
		    $ext = explode(".", $profilePic);
		    $last_ext = $ext[count($ext)-1];
		    
		// Allowed extension for file upload in the registration    
		$valid_formats = unserialize(FILES_EXTENSION);
		
		    if(!in_array($last_ext,$valid_formats)){
			echo "Invalid File Format";
		    }else{
			    if ($profilePic_size=="0"){
				echo "File too large. File must be less than 4 megabytes.";
			    }else{
				if($trackNo==""){
				    $id_encrypt = md5(md5($auth['id']));
				}else{
				    $id_encrypt = md5(md5($trackNo));
				}
				
				$dir = $path."/".$id_encrypt;
					
				// Create Folder of the user for their image	
					$upload->createFolder($dir);
					$filename = md5(md5(time().$id_encrypt));
				
				$thumbnail = $dir . "/" . $profilePic;        
				move_uploaded_file($_FILES['file']['tmp_name'],$thumbnail);
				
				$formats = unserialize(IMG_EXTENSION);
				if(!in_array($last_ext,$formats)){
				    $extention = "image";
				}else{
				    $extention = "file";
				}
				    $succ[] = array("successful"=>"Your file was successfully added.",
						    "img"=>$thumbnail,
						    "filename"=>$profilePic,
						    "extension"=>$extention,
						    "userType"=>$profilePic_size,
						    "FormID"=>$formID,
						    "RequestID"=>$requestID,
						    "TrackNo"=>$track_no);
				
				    echo json_encode($succ);
			    }
		    }
}


?>