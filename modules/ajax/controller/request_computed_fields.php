<?php

$db = new Database();
$auth = Auth::getAuth('current_user');
if(!Auth::hasAuth('current_user')){ 
   return false;
}
$fields = json_decode($_GET['fields'], true);
$return_data = array();
foreach ($fields as $field) {
    $field_name = $field['FieldName'];
    $processor_type = $field['ProcessorType'];
    $processor_user_type = $field['Processor'];

    $personDoc = new Person($db,$auth['id']);
    
    if ($processor_type == '1') {
        $personDoc = new Person($this, $auth['id']);
        $processor_department = $personDoc->department->id;
        $user_department_position_level = $personDoc->department_position_level->id;

        do {
            //loop until processor is found
            $parent_department = $this->query("SELECT PARENT_DEPARTMENT.id as Parent_Department FROM tborgchartobjects CHILD_DEPARTMENT
                        LEFT JOIN tborgchartline DEPARMENTLINE 
                        ON DEPARMENTLINE.child = CHILD_DEPARTMENT.object_id AND DEPARMENTLINE.orgChart_id = CHILD_DEPARTMENT.orgChart_id 
                        LEFT JOIN tborgchartobjects PARENT_DEPARTMENT 
                        ON PARENT_DEPARTMENT.object_id = DEPARMENTLINE.parent AND PARENT_DEPARTMENT.orgChart_id  = DEPARMENTLINE.orgChart_id
                        WHERE CHILD_DEPARTMENT.id ={$this->escape($processor_department)} LIMIT 1 ", "row");

            $parent_department_id = $parent_department['Parent_Department'];

            if ($user_department_position_level == 1) {
                $processor_department = $parent_department_id;
            }

            if ($user_department_position_level == 2 && $processor_user_type == 2) {
                $processor_department = $parent_department_id;
            }

            $processor_result = $this->query("SELECT json
                                    FROM tborgchartobjects DEPARTMENT WHERE id = {$this->escape($processor_department)}", "row");


            if ($processor_user_type == 1) {
                $department_data = json_decode($processor_result['json'], true);
                $approverDoc = new Person($this, $department_data['orgchart_user_head'][0]);

                $processor = $approverDoc->display_name;

                if ($processor == '' || $processor == 'null' || $processor == null) {//if head not found request will be forwarded to assistant head of the parent department
                    $processor_department = $parent_department_id;
                    $processor_user_type = 2;
                }
            } else {
                //assistant head
                $department_data = json_decode($processor_result['json'], true);
                $approverDoc = new Person($this, $department_data['orgchart_dept_assistant'][0]);
                $processor = $approverDoc->display_name;

                if ($processor == '' || $processor == 'null' || $processor == null) {//if assistant head not found request will be forwarded to head
                    $processor_user_type = 1;
                }
            }
        } while (($processor == '' || $processor == 'null' || $processor == null) && $parent_department_id != '');
    }

    if ($processor_type == '2') {
        $processor_result = $this->query("SELECT group_concat(user_processor.Display_Name separator ',') as Processor
                         FROM tbuser user_processor 
                         WHERE user_processor.Position = {$this->escape($processor_user_type)}", "row");
        $processor = $processor_result['Processor'];
    }

    if ($processor_type == 3) {

        $processor_result = $this->query("SELECT user_processor.Display_Name as Processor FROM tbuser user_processor WHERE id={$this->escape($processor_user_type)} ", "row");
        $processor = $processor_result['Processor'];
    }

    if ($processor_type == 4) {
        $personDoc = new Person($this, $auth['id']);
        $processor = $personDoc->display_name;
    }

    array_push($return_data, array(
        "FieldName" => $field_name,
        "Processor" => $processor
            )
    );
}

print_r(json_encode($return_data));
?>
