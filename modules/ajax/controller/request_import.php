<?php

//error_reporting(E_ALL);

function validateDate($date, $format = 'Y-m-d H:i:s') {
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//error_reporting(E_ALL);
$fs = new functions;
$auth = Auth::getAuth('current_user');
if (!Auth::hasAuth('current_user')) {
    return false;
}
$db = new Database();

if (isset($_POST)) {
    $location = $_FILES['fileupload']['tmp_name'];
    $name = $_FILES['fileupload']['name'];
    $get_extention = explode(".", $name);
    $ext = strtolower($get_extention[count($get_extention) - 1]);



    //check if the extension is csv
    if ($ext != "csv") {
        echo "0";
        return false;
    }

    $formID = $_POST['FormID'];
    $import_option = $_POST["import_option"];
    $import_update_reference_field = $_POST["import_update_reference_field"];
    $action = $_POST["action"];
    $update_option = $_POST["update_option"];
    $import_update_add_value_field = $_POST["import_update_add_value_field"];

    //added by japhet morada for embedded view import
    $embed_ref_key = $_POST['embed_import_reference_key'] ? : "";
    $embed_ref_val = $_POST['embed_import_reference_value'] ? : "";
    $embed_submission = $_POST['embed_submission'] ? : "";
    //====================================================
    $formDoc = new Form($db, $formID);
    $return_array = import::importCSV($location);


    //validate fieldname
    $fields = $db->query("SELECT GROUP_CONCAT(lcase(field_name)) as fields FROM tbfields WHERE form_id={$formID}", "row");
    $fieldArr = explode(",", $fields["fields"]);
    $invalid_headers = array();
    foreach ($return_array[0] as $header) {
        $pos = in_array(strtolower($header), $fieldArr);
        if ($pos === false) {
            array_push($invalid_headers, strtolower($header));
        }
    }

    //validate formats
    $invalid_value_format = array();
    $fields_arr = $db->query("SELECT field_name, field_type, field_input_type FROM tbfields WHERE form_id={$formID}");
    $fields_array = array();

    foreach ($fields_arr as $field) {
        $fields_array[$field["field_name"]] = array("field_type" => $field["field_type"]
            , "field_input_type" => $field["field_input_type"]);
    }

    $csv_headers = $return_array[0];
    foreach ($return_array as $index => $row) {
        if ($index != 0) {
            foreach ($row as $key => $value) {
                $field_reference = $csv_headers[$key];
                $field_reference_type = $fields_array[$field_reference]["field_type"];
                $field_reference_input_type = $fields_array[$field_reference]["field_input_type"];

                if (trim($value) != "") {
                    switch ($field_reference_type) {
                        case "datepicker":
                            $is_date = validateDate(trim($value), "Y-m-d");
                            if ($is_date == false) {
                                array_push($invalid_value_format, array(
                                    "row_number" => $index + 1,
                                    "field_name" => $field_reference,
                                    "value" => $value,
                                    "type" => $field_reference_type));
                            }
                            break;
                        case "dateTime":
                            $is_date = validateDate(trim($value));
                            $is_date_wout_sec = validateDate(trim($value), 'Y-m-d H:i');
                            if ($is_date == false && $is_date_wout_sec == false) {
                                array_push($invalid_value_format, array(
                                    "row_number" => $index + 1,
                                    "field_name" => $field_reference,
                                    "value" => $value,
                                    "type" => $field_reference_type));
                            }
                            break;
                        case "time":
                            $is_date = validateDate(trim($value), "H:i:s");
                            $is_date_wout_sec = validateDate(trim($value), 'H:i');
                            if ($is_date == false && $is_date_wout_sec == false) {
                                array_push($invalid_value_format, array(
                                    "row_number" => $index + 1,
                                    "field_name" => $field_reference,
                                    "value" => $value,
                                    "type" => $field_reference_type));
                            }
                            break;
                        default:
                            switch ($field_reference_input_type) {
                                case "Number":
                                    $value = str_replace(",", "", $value);
                                    $is_numeric = is_numeric(trim($value));
                                    if ($is_numeric == false) {
                                        array_push($invalid_value_format, array(
                                            "row_number" => $index + 1,
                                            "field_name" => $field_reference,
                                            "value" => $value,
                                            "type" => $field_reference_input_type));
                                    }
                                    break;
                                case "Currency":
                                    $value = str_replace(",", "", $value);
                                    $is_numeric = is_numeric(trim($value));
                                    if ($is_numeric == false) {
                                        array_push($invalid_value_format, array(
                                            "row_number" => $index + 1,
                                            "field_name" => $field_reference,
                                            "value" => $value,
                                            "type" => $field_reference_input_type));
                                    }
                                    break;
                            }
                    }
                }
            }
        }
    }

//    exit(var_dump($invalid_value_format));
    if (count($invalid_headers) >= 1) {
        echo json_encode(array(
            "Invalid_Field_Names" => $invalid_headers
        ));

        return;
    }
    $string_counter = 0;
    for ($i = 1; $i < count($return_array); $i++) {
        for ($j = 0; $j < count($return_array[$i]); $j++) {
            if (strlen($return_array[$i][$j]) > 0) {
                $string_counter++;
            }
        }
    }
    if ($string_counter <= 0) {
        echo json_encode(array(
            "file_empty" => "File Should not be Empty!"
        ));
        return;
    }
    if (count($invalid_value_format) >= 1) {
        $to = array();
        $to[$auth["email"]] = $auth["display_name"];
        $from = SMTP_FROM_EMAIL;
        $from_title = SMTP_FROM_NAME;
        $subject = "Formalistics Import Error";

        $message = "The following rows and fields have an error during import:<br/><br/>";
        $message.="<table><tr><th>Row Number</th><th>Column</th><th>Value</th><th>Error Message</th></tr>";
        foreach ($invalid_value_format as $row) {
            $message.="<tr>";
            $message.="<td>" . $row["row_number"] . "</td>";
            $message.="<td>" . $row["field_name"] . "</td>";
            $message.="<td>" . $row["value"] . "</td>";

            switch ($row["type"]) {
                case "datepicker":
                    $message.="<td>Invalid date.</td>";
                    break;
                case "dateTime":
                    $message.="<td>Invalid date/time.</td>";
                    break;
                case "time":
                    $message.="<td>Invalid time.</td>";
                    break;
                case "Number":
                    $message.="<td>Invalid Number.</td>";
                    break;
                case "Currency":
                    $message.="<td>Invalid Currency.</td>";
                    break;
            }
            $message.="</tr>";
        }

        $message.="</table>";
//        echo $message;
        $mail_info = array(
            "To" => $to,
            "CC" => null,
            "BCC" => null,
            "From" => $from,
            "Title" => $subject,
            "From_Title" => $from_title,
            "Body" => $message,
            "Form_Id" => $formID,
        );

        $mailDoc = new Mail_Notification();
        $mailDoc->import_notify_user($mail_info);

        echo json_encode(array(
            "Invalid_Field_Format" => $invalid_value_format
        ));

        return;
    }


    $tmpname = microtime(TRUE) . ".tmp";
    $tmppath = APPLICATION_PATH . "\\logs\\import_logs\\tmp\\";

    if (!move_uploaded_file($location, $tmppath . $tmpname)) {
        exit('Could not store file for processing');
    }

    if ($embed_ref_key != "" && $embed_ref_val != "") {
        backgroundProcessImportV2($formID, $import_option, $import_update_reference_field, $tmpname, $action, $embed_ref_key, $embed_ref_val, $embed_submission);
    } else {
        backgroundProcessImport($formID, $import_option, $import_update_reference_field, $tmpname, $action, $update_option, $import_update_add_value_field);
    }
}



echo json_encode(
        array(
            "application_id" => $fs->base_encode_decode("encrypt", $formID),
            "import_option" => $import_option,
            "formID" => $formID,
            "import_update_reference_field" => $import_update_reference_field,
            "tmpname" => $tmpname,
            "action" => $action
));

// if (($auth['user_level_id'] == 2 && ALLOW_USER_ADMIN == 1) || ($auth['user_level_id'] == 3 && ALLOW_USER_VIEW == 1)) {
// header('location:/user_view/application?id=' . $fs->base_encode_decode("encrypt", $formID));
// } else {
// header('location:/application?id=' . $fs->base_encode_decode("encrypt", $formID));
// }
?>
