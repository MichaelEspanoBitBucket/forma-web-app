<?php

$fs = new functions;
$auth = Auth::getAuth('current_user');
if(!Auth::hasAuth('current_user')){ 
   return false;
}
$db = new Database();

$get_data = filter_input_array(INPUT_GET);
$form_id = $get_data["form_id"];

$str_sql = "SELECT field_name FROM tbfields WHERE form_id={$form_id} AND field_type NOT IN ('multiple_attachment_on_request', 'attachment_on_request') ORDER BY field_name ASC";
$form_fields = $db->query($str_sql, "array");

$filename = "import_template_" . $form_id . ".csv";

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=' . $filename);

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');
//filter values
$add_row_arr = array();
foreach ($form_fields as $index => $field) {
    $add_row_arr[$index] = $field["field_name"];
}

fputcsv($output, $add_row_arr);
fclose($output);


