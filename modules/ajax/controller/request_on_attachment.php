<?php
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$userCompany = new userQueries();
$company = $userCompany->getCompany($auth['company_id']);
if(isset($_POST) and $_SERVER['REQUEST_METHOD']=="POST"){
	// User file path
	$path = "images/on_attachment";
    $maxsize = REQUEST_SINGLE_FILE_ATTACHMENT;
	$profilePic = $_FILES['file_attachment']['name'];
	$profilePic_size = $_FILES['file_attachment']['size']; // File Size
	
            $ext = explode(".", $profilePic);
            $last_ext = $ext[count($ext)-1];
	    
        // Allowed extension for file upload in the registration    
        $valid_formats = unserialize(FILES_EXTENSION);
        
            if(!in_array($last_ext,$valid_formats)){
                $succ[] = array("message"   =>  "Invalid File Format.",
                "size"=>$profilePic_size,
                "etype"=>"error");
                // echo "Invalid File Format";
            }else{
                    if (($profilePic_size > $maxsize) || ($profilePic_size == 0)){
                        $succ[] = array("message"   =>  "File too large. File must be less than ". $fs->formatBytes($maxsize, 0) ." megabytes.",
                "size"=>$profilePic_size,
                "etype"=>"error");
                        // echo "File too large. File must be less than 4 megabytes.";
                    }else{
                        $p = $path . "/" . time() . md5($auth['id']);
                        if(!is_dir($p)){
                            mkdir($p);
                        }

                        $fileName = preg_replace('/[^.[:alnum:]_-]/','_',trim($profilePic));  // converting all on alphanumeric chars to _
                        $fileName = preg_replace('/\.*$/','',$fileName); // removing dot . after file extension

                        $thumbnail = $p . "/" . $fileName;        
                        move_uploaded_file($_FILES['file_attachment']['tmp_name'],$thumbnail);
                        

                        $succ[] = array("successful"    =>  "Your file was successfully selected.",
                    "message"=>"ok",
                    "img"       =>  "/"  . $thumbnail,
                        "location"      =>  "/"  . $p . "/",
                "etype"=>"success",
                        "size"=>$profilePic_size,
                        "file_name"     =>  $fileName,
                        "file_type"     =>  json_encode($_FILES['file_attachment']));


         //                    $succ[] = array("successful"	=>	"Your file was successfully uploaded.",
         //                                    "img"		=>	"/"  . $thumbnail,
					    // "userType"		=>	$profilePic);
			
                            
                    }
            }

            echo json_encode($succ);
}


?>