<?php

$db = new Database();
$auth = Auth::getAuth('current_user');
$search = new Search();
$redis_cache = getRedisConnection();
$fs = new functions;

if(isset($_POST['action'])){
    if($_POST['action']=="getRequestUsers"){
        $form_id = $_POST['form_id'];
        echo getUsers_requestViewer($form_id);
    }else if($_POST['action']=="getRequestViewer"){
        $request_id = $_POST['request_id'];
        $form_id = $_POST['form_id'];
        $query = $this->query("SELECT u.id as id, CONCAT_WS(' ',u.first_name,u.last_name) as name FROM tbrequest_viewer rv LEFT JOIN tbuser u on rv.user_id = u.id WHERE request_id = '$request_id' AND form_id = '$form_id' AND type = 0","array");
        echo json_encode($query);
    }else if($_POST['action']=="saveViewer"){
        $array_search = $_POST['array_search'];
        $form_id = $_POST['form_id'];
        $requestId = $_POST['requestId'];

        //Delete
        $deletCond = array("request_id"=>$requestId,
                        "form_id"=>$form_id);
        $db->delete("tbrequest_viewer",$deletCond);
        foreach ($array_search as $value) {
            $insertVal = array("user_id"=>$value,
                            "request_id"=>$requestId,
                            "form_id"=>$form_id);
            $db->insert("tbrequest_viewer",$insertVal);
        }

        if ($redis_cache) {
            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $nav_memcached = array("leftbar_nav");
            $starred_memcached = array("starred_list");
            $request_memcached = array("form_access_" . $form_id,
                "related_info_form_id_" . $form_id, "request_details_" . $form_id,
                "request_access_" . $form_id, "request_author_access_" . $form_id,
                "workspace_form_details_" . $form_id);
            $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");


            $deleteMemecachedKeys = array_merge(
                $nav_memcached, $starred_memcached, $request_memcached, $myrequest_memcached
            );

            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            $fs->deleteMemcacheForm($request_record_memcacached, $form_id);
        }
    }else if($_POST['action']=="getTaggedUsers"){
        $request_id = $_POST['requestId'];
        $form_id = $_POST['form_id'];
        $numrows = $this->query("SELECT * FROM tbrequest_viewer WHERE request_id = '$request_id' AND form_id = '$form_id' and user_id = '". $auth['id'] ."' and type = 0","numrows");
        echo $numrows;
    }else if($_POST['action']=="removetagRequest"){
        $request_id = $_POST['requestId'];
        $form_id = $_POST['form_id'];
        //Delete
        $deletCond = array("request_id"=>$request_id,
                        "form_id"=>$form_id,
                        "user_id"=>$auth['id']);
        $db->delete("tbrequest_viewer",$deletCond);

        $obj = array("condition"=>" AND form.ID = ". $request_id ."");
        $obj = json_decode(json_encode($obj),true);
        $result = $search->getManyRequest("",0,$form_id,0,1,$obj);
        echo count($result);
        // echo functions::base_encode_decode("encrypt",$form_id);
        
        if ($redis_cache) {
            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $nav_memcached = array("leftbar_nav");
            $starred_memcached = array("starred_list");
            $request_memcached = array("form_access_" . $form_id,
                "related_info_form_id_" . $form_id, "request_details_" . $form_id,
                "request_access_" . $form_id, "request_author_access_" . $form_id,
                "workspace_form_details_" . $form_id);
            $myrequest_memcached = array("my_request_list_widget", "my_request_list_page", "my_request_list_page_count", "my_request_calendar");


            $deleteMemecachedKeys = array_merge(
                $nav_memcached, $starred_memcached, $request_memcached, $myrequest_memcached
            );

            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            $fs->deleteMemcacheForm($request_record_memcacached, $form_id);
        }
    }else if($_POST['action']=="getUsers"){
        $company_id = $auth['company_id'];
        $getUsers = $db->query("SELECT id, CONCAT_WS(' ',first_name,last_name) as name, first_name, last_name,display_name FROM tbuser WHERE company_id = '$company_id'","array");
        $ret = array();
        foreach ($getUsers as $value) {
            array_push($ret, array("user_image"=>post::avatarPic("tbuser",$value['id'],"30","30","small","avatar"),"id"=>$value['id'],"name"=>$value['display_name'],"first_name"=>$value['display_name'],"last_name"=>''));
        }
        echo json_encode($ret);
    }else if ($_POST['action']=="getButtonProperties"){ //added by joshua reyes 03/07/2016 [Button Properties Query]

        $form_id = $_POST['form_id'];
        $button_last_action = $_POST['button_last_action'];

        $button_condition_str = "";
        foreach ($button_last_action as $button_name_val) {
            $button_condition_str .= "'{$button_name_val['button_name']}',";
        }
        $button_condition_str_filtered = substr($button_condition_str, 0, strlen($button_condition_str) - 1);

        $getProperties = $db->query("SELECT id, form_id, button_name, visibility_formula FROM tb_action_button_properties WHERE form_id = {$db->escape($form_id)} AND button_name IN ({$button_condition_str_filtered})", "array");
        if (empty($getProperties)) {
            $data = array(
                "validate" => "empty",
                "button_last_action" => $button_last_action
            );
            echo json_encode($data);
        } else {
            $data = array(
                "validate" => "not_empty",
                "button_properties" => $getProperties,
                "button_last_action" => $button_last_action
            );
            echo json_encode($data);
        }  
    }

}else{
    if(isset($_GET['seeAllUsersAddViewers'])){
        $form_id = $_GET['form_id'];
        $getUsers = json_decode(getUsers_requestViewer($form_id),true);
        echo '<link rel="stylesheet" type="text/css" href="/css/mention.css">';
        echo '<link rel="stylesheet" type="text/css" href="/css/user_css/stylesheets/structure.css">';
        echo '<div class="mentionContainer_only"><div class="mentionSearchChoice" style="margin-top: 0px;">';
        foreach ($getUsers as $value) {
            echo '<div class="mentionList" data-id="'. $value['id'] .'" style="cursor: auto;"><i class="icon-caret-right"> </i><span class="mentionList_txt">'. $value['name'] .'</span></div>';
        }
        echo '</div></div>';
    }
}

function getUsers_requestViewer($form_id){
    $db = new Database();
    $auth = Auth::getAuth('current_user');
    $search = new Search();
    $post = new post();

    $company_id = $auth['company_id'];
    $getUsers = $db->query("SELECT id, CONCAT_WS(' ',first_name,last_name) as name, first_name, last_name FROM tbuser WHERE company_id = '$company_id' and is_active = 1","array");
    // $getUsers = $search->getAllUsers();
    $ret = array();
    
    $forms_array = $db->query("SELECT form_json
                                      FROM tb_workspace
                                      WHERE id =  '$form_id' AND is_delete = 0","row");
    $json =  json_decode($forms_array['form_json'],true);
    $categoryID = $json['categoryName'];
    if($categoryID==0 || $categoryID==""){
        $ret = $getUsers;
    }else{
        $getcategory = $db->query("SELECT * FROM tbform_category WHERE id = $categoryID AND is_delete = 0","row");
        foreach ($getUsers as $value) {
            if($search->getFormPrivacyOtherUsers($getcategory['users'],$value['id'])>0){
                array_push($ret, array("user_image"=>$post->avatarPic("tbuser",$value['id'],"30","30","small","avatar"),"id"=>$value['id'],"name"=>$value['name'],"first_name"=>$value['first_name'],"last_name"=>$value['last_name']));
            }
        }
    }
    return json_encode($ret);
    // return "asd";
    // print_r($json['form_json']);
    // $search->getFormPrivacyOtherUsers($getcategory['users'],"1");
}

?>