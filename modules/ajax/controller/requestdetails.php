<?php

$auth = Auth::getAuth('current_user');
if(!Auth::hasAuth('current_user')){ 
   return false;
}
$db = new Database();
$userCompany = new userQueries();

$formID = filter_input(INPUT_POST, "FormID");
$requestID = $_POST['RequestID'];

$results = $db->query("SELECT form_table_name FROM tb_workspace WHERE id = {$db->escape($formID)} AND is_delete = 0", 'row');
$tblname = $results['form_table_name'];
$user_id = $auth['id'];

$requestDetails = $db->query("SELECT FORM.*, FORM.Processor AS ProcessorID, 
    REQUESTOR.display_name as Requestor_Fullname, REQUESTOR.display_name as Requestor_Name, 
        CASE FORM.ProcessorType
            WHEN 1 THEN 
                (SELECT user_processor.display_name FROM tbuser user_processor WHERE user_processor.id = getDepartmentUsers(FORM.Processor, FORM.ProcessorLevel, {$auth["company_id"]}))
            WHEN 2 THEN 
                (SELECT Position FROM tbpositions WHERE ID=FORM.Processor)
            WHEN 6 THEN 
                (SELECT group_name FROM tbform_groups WHERE ID=FORM.Processor)
            ELSE 
                PROCESSOR.display_name
        END AS Processor_Name, 
        CASE FORM.ProcessorType
            WHEN 1 THEN CONCAT(@department_users:=getDepartmentUsers(FORM.Processor, FORM.ProcessorLevel, {$auth["company_id"]}),',' ,
			IF(FORM.enable_delegate = 1,IFNULL(getDelegate(@department_users),''),'')
                    ) 
            WHEN 2 THEN CONCAT(@position_users:=getUsersByPosition(FORM.Processor),',' ,
                        IF(FORM.enable_delegate = 1,IFNULL(getDelegate(@position_users),''),'')
                    ) 
            WHEN 6 THEN CONCAT(@group_users:=getUsersByGroup(FORM.Processor),',' ,
                        IF(FORM.enable_delegate = 1,IFNULL(getDelegate(@group_users),''),'')
                    )
            ELSE
                    CONCAT(FORM.Processor,',' ,
                        IF(FORM.enable_delegate = 1,IFNULL(getDelegate(FORM.Processor),''),'')
                    )
        END AS Processor,
        CASE WHEN FORM.enable_delegate = 1 THEN 
            CASE FORM.ProcessorType
            WHEN 1 THEN (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(getDepartmentUsers(FORM.Processor, FORM.ProcessorLevel, {$auth["company_id"]}))) ORDER BY user_delegate.display_name)
            WHEN 2 THEN (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(getUsersByPosition(FORM.Processor))) ORDER BY user_delegate.display_name)
            WHEN 6 THEN (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(getUsersByGroup(FORM.Processor))) ORDER BY user_delegate.display_name)            
            ELSE
                    (SELECT GROUP_CONCAT(user_delegate.display_name) FROM tbuser user_delegate WHERE FIND_IN_SET(user_delegate.ID, getDelegate(FORM.Processor)) ORDER BY user_delegate.display_name)
        END
        ELSE
            ''
        END AS Delegate_Name,
        CASE 
            WHEN FORM.Editor IS NULL THEN getRequestUsers({$formID}, FORM.ID, 1)
            ELSE
                FORM.Editor
	END AS Editor,
    FIND_IN_SET({$user_id}, getFormUsers({$formID},6)) as delete_access_forma FROM " . $tblname . " FORM 
        LEFT JOIN tbuser REQUESTOR
        ON REQUESTOR.id = FORM.Requestor
        LEFT JOIN tbuser PROCESSOR
        ON PROCESSOR.id = FORM.Processor
        WHERE FORM.id = {$db->escape($requestID)}");

//$requestDetails[0]["SaveFormula"] = "asdasdasdsa";

if ($requestDetails[0]["SaveFormula"] != "") {
    $formulaDoc = new Formula($requestDetails[0]["SaveFormula"]);
    $formulaDoc->DataFormSource[0] = $requestDetails[0];
    $requestDetails[0]["SaveFormula"] = $formulaDoc->evaluate();
}


if ($requestDetails[0]["CancelFormula"] != "") {
    $formulaDoc = new Formula($requestDetails[0]["CancelFormula"]);
    $formulaDoc->DataFormSource[0] = $requestDetails[0];
    $requestDetails[0]["CancelFormula"] = $formulaDoc->evaluate();
}



echo json_encode($requestDetails);
?>