<?php
//check if session is alive if not.. retun false;

// error_reporting(E_ERROR);

if (!Auth::hasAuth('current_user')) {
    return false;
}
$conn = getCurrentDatabaseConnection();
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$search = new Search();
$listViewFormula = new ListViewFormula();
$user_id = $auth['id'];
$mod = unserialize(ENABLE_COMPANY_MOD);
$notifications = new notifications();
$post_data = filter_input_array(INPUT_POST);
$redis_cache = getRedisConnection();

if (isset($post_data['action'])) {

    $date = $fs->currentDateTime();
    switch ($post_data['action']) {

        case "getRequest":
            $id = $post_data['id'];
            $search_value = trim($post_data['search_value']);
            $field = $post_data['field'];
            $start = $post_data['start'];
            $date_field = $post_data['date_field'];
            $date_from = $post_data['date_from'];
            $date_to = $post_data['date_to'];
            if ($date_field != "0" && $date_from != "" && $date_to != "") {
                $obj = array("date_range" => "1",
                    "date_field" => $date_field,
                    "date_from" => $date_from,
                    "date_to" => $date_to);
                $obj = json_decode(json_encode($obj), true);
            }
            $result = $search->getManyRequest($search_value, $field, $id, $start, 10, $obj);
            echo json_encode($result);
            // echo $result;
            break;
        case "getRequestDataTable":
            $id = $post_data['id'];
            $search_value = trim($post_data['search_value']);
            $field = $post_data['field'];
            $limit = $post_data['limit'];
            $start = $post_data['iDisplayStart'];
            $date_field = $post_data['date_field'];
            $date_from = $post_data['date_from'];
            $date_to = $post_data['date_to'];
            $isAdvanceSearch = $post_data['isAdvanceSearch'];
            $obj = array();
            if ($date_field != "0" && $date_from != "" && $date_to != "") {
                $obj = array("date_range" => "1",
                    "date_field" => $date_field,
                    "date_from" => $date_from,
                    "date_to" => $date_to);
                $obj = json_decode(json_encode($obj), true);
            }
            if ($post_data['column-sort'] != "") {
                $obj['column-sort'] = $post_data['column-sort'];
                $obj['column-sort-type'] = $post_data['column-sort-type'];
            }

            if (isset($post_data['multi_search'])) {
                $multi_search = "";
                $ctrSpec = 0;
                $ctr = 0;
                $multi_search_arr = json_decode($post_data['multi_search'], true);
                foreach ($multi_search_arr as $value) {
                    $ctr++;
                    if ($value['search_field'] == "0") {
                        $search_value = $value['search_value'];
                        $field = $value['search_field'];
                    } else {
                        $ctrSpec++;
                        if ($ctrSpec == 1) {
                            $multi_search .= " AND (";
                        }
                        if ($value['search_field'] == "Requestor") {
                            if ($value['search_operator'] == "%") {
                                $multi_search .= " (request.requestor_display_name LIKE '%" . $value['search_value'] . "%')";
                            } else {
                                $multi_search .= "request.requestor_display_name " . $value['search_operator'] . "  '" . $value['search_value'] . "'";
                            }
                        } else if ($value['search_field'] == "Processor") {
                            if ($value['search_operator'] == "%") {
                                $multi_search .= " (processer.display_name LIKE '%" . $value['search_value'] . "%' OR processer.first_name LIKE '%" . $value['search_value'] . "%' OR processer.last_name LIKE '%" . $value['search_value'] . "%' OR CONCAT_WS( '', processer.first_name, processer.last_name) LIKE  '%" . $value['search_value'] . "%')";
                            } else {
                                $multi_search .= "processer.display_name " . $value['search_operator'] . "  '" . $value['search_value'] . "'";
                            }
                        } else if ($value['search_field'] == "ID") {
                            $multi_search .= " (request.ID " . $value['search_operator'] . " " . $value['search_value'] . ")";
                        } else {
                            if ($value['search_operator'] == "%") {
                                $multi_search .= " (request." . $value['search_field'] . " LIKE '%" . $value['search_value'] . "%')";
                            } else {
                                $multi_search .= " (request." . $value['search_field'] . " " . $value['search_operator'] . " '" . $value['search_value'] . "')";
                            }
                        }
                        if ($value['search_condition'] != "" || $value['search_condition'] != undefined || $value['search_condition'] != "undefined") {
                            if ($value['search_condition'] == "OR") {
                                $multi_search .= " OR ";
                            } else {
                                $multi_search .= " AND ";
                            }
                        } else {
                            $multi_search .= " AND ";
                        }
                    }
                }
                if ($ctrSpec > 0) {
                    $multi_search = substr($multi_search, 0, strlen($multi_search) - 4);
                    $multi_search.= ")";
                }
                $obj['multi_search'] = $multi_search;
            }
            $obj['isAdvanceSearch'] = $isAdvanceSearch;
            $getDeleteAccess = $db->query("SELECT FIND_IN_SET({$auth["id"]}, getFormUsers({$id},6)) as delete_access_forma", "row");
            if ($redis_cache && ($isAdvanceSearch == "0" || $isAdvanceSearch == "")) {
                /*
                  Cache Format
                  1. User Id.
                  2. Page Number.
                  3. Page Limit.
                  4. Search Filter.
                  5. Column Sort.
                  6. Type of Sort.
                 */

                $cachedFormat = "$user_id::$start::$limit::$search_value::" . $obj['column-sort'] . "::" . $obj['column-sort-type'] . "";

                // $memcache->delete("request_list");
                $cache_request_list = json_decode($redis_cache->get("request_list"), true);

                $cached_query_result = $cache_request_list['form_id_' . $id]['' . $cachedFormat . ''];
                if ($cached_query_result) {
                    // echo "from cache";

                    $result = $cached_query_result;
                } else {
                    // echo "from db";

                    $result = $search->getManyRequestV2($search_value, $field, $id, $start, $limit, $obj);
                    $cache_request_list['form_id_' . $id]['' . $cachedFormat . ''] = $result;
                    $redis_cache->set("request_list", json_encode($cache_request_list));
                }
            } else {
                $result = $search->getManyRequestV2($search_value, $field, $id, $start, $limit, $obj);
            }


            if (REQUEST_QUERY_DEBUGGER == "1") {
                print_r($result);
                return;
            }

            $countRequest = $result[0][3]['count'];
            if (is_null($countRequest)) {
                $countRequest = 0;
            }
            $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($id)} AND is_delete = 0", "row");
            $form_json = json_decode($forms['form_json']);
            $fields_headerObj = $form_json->{'form_json'}->{'collected_data_header_info'}->{'listedchoice'};
            $fields_headerObjType = $form_json->{'form_json'}->{'headerInfoType'};
            $fields_headerObjType_allowReorder = $form_json->{'form_json'}->{'allowReorder'};

            //background
            $viewHighlight_background = $form_json->{'form_json'}->{'viewHighlight_background'};


            //font
            $viewHighlight_font = $form_json->{'form_json'}->{'viewHighlight_font'};

            $enable_deletion = $forms['enable_deletion'];
            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countRequest,
                "iTotalDisplayRecords" => $countRequest,
                "start" => $start,
                "aaData" => array(),
                "column-sort" => $post_data['column-sort'],
                "column-sort-type" => $post_data['column-sort-type'],
                "enable_deletion" => $enable_deletion
            );
            $getCustomView = $db->query("SELECT * FROM tbcustomview WHERE user_id = $user_id AND form_id = $id ", "array");
            if ($fields_headerObjType_allowReorder == "0") {
                $getCustomView = array();
            }
            //generate print

            $getGeneratePrint = $db->query("SELECT * FROM tb_generate WHERE form_id={$db->escape($id)}", "row");
            $getGeneratePrintNumrows = $db->query("SELECT * FROM tb_generate WHERE form_id={$db->escape($id)}", "numrows");
            $print_form_id = $getGeneratePrint['id'];

            $isJunctionFields = $db->query("SELECT * FROM tbfields where form_id = $id", "numrows");
            if ($isJunctionFields > 0) {
                $fields_headerObj_array = array();
                $fields_headerObj = json_decode(json_encode($fields_headerObj), true);
                foreach ($fields_headerObj as $key => $value) {
                    $field_name = str_replace("[]", "", $value['field_name']);
                    $getFieldsData = $db->query("SELECT * FROM tbfields where form_id = $id AND field_name = '" . $field_name . "'", "row");
                    if ($getFieldsData['id'] > 0) {
                        $getFieldsData['field_label'] = $getFieldsData['data_field_label'];
                        array_push($fields_headerObj_array, $getFieldsData);
                    } else {
                        array_push($fields_headerObj_array, $value);
                    }
                }
                $fields_headerObj = $fields_headerObj_array;
            }

            foreach ($result as $value) {
                $otherFieldsStartPos = 3; // start of index
                $aaData = array();
                $viewHighlight_background->color = $value[''. $viewHighlight_background->field_color_basis .''];


                $viewHighlight_font->color = $value[''. $viewHighlight_font->field_color_basis_font .''];



                $button = "<ul class='fl-table-actions' background-highlight='". json_encode($viewHighlight_background) ."' font-highlight='". json_encode($viewHighlight_font) ."'>";
                $starred_class = "fa fa-star-o fl-custom-starred";
                $starred_title = "Starred Request";
                $starred = 0;
                if ($value[0]['starred'] > 0) {
                    $starred_class = "fa fl-custom-starred fa-star";
                    $starred = $value[0]['starred'];
                    $starred_title = "Remove Starred";
                }
                //$button .='<a href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '"><button class="tip fa fa-eye btn-basicBtn fl-basicsecondary-btn padding_5 cursor" style="margin-right:3px" form-id = "' . $id . '" request-id="' . $value['request_id'] . '" data-original-title="View Form"></button></a>';
                $form_name = str_replace(" ", "_", $forms['form_name']);
                //$button .='<button class="fa fa-caret-right" style="background:transparent!important; border: 0!important; font-size:15px;"></button> ';

                if ($value['form_status'] != "Cancelled" && $value['form_status'] != "Draft" && $value[2]['buttons'] != '') {
                    $button .= "<li><input type='checkbox' form-status='" . $value['form_status'] . "' class='appCheckSingle " . $form_name . " css-checkbox' data-form-name='" . $form_name . "' style='margin-top: 5px;margin-right: 10px;' button-action-data='" . $value[2]['buttons'] . "' button-request-id='" . $value['ID'] . "'button-workflow-id='" . $value['Workflow_ID'] . "'  button-requestor-id='" . $value['Requestor'] . "' button-processor-id='" . $value['Processor'] . "' id='" . $value['TrackNo'] . "'><label for='" . $value['TrackNo'] . "' class='css-label' style='margin-top:-5px;'></label></li>";
                } else {
                    // $button .= "<li>&nbsp;&nbsp;&nbsp;&nbsp;</li>"; //"<input type='checkbox' form-status='disabled' disabled=disabled class='appCheckSingle " . $form_name . " css-checkbox' data-form-name='" . $form_name . "' style='margin-top: 5px;margin-right: 10px;' button-action-data='" . $value[2]['buttons'] . "' button-request-id='" . $value['ID'] . "'button-workflow-id='" . $value['Workflow_ID'] . "'  button-requestor-id='" . $value['Requestor'] . "' button-processor-id='" . $value['Processor']."' id='". $value['TrackNo'] ."'><label for='". $value['TrackNo'] ."' class='css-label' style='margin-right:7px;margin-top:-5px;'></label>";
                    $button .= "<li><input type='checkbox' disabled='disabled' form-status='disabled' class='appCheckSingle " . $form_name . " css-checkbox' data-form-name='" . $form_name . "' style='margin-top: 5px;margin-right: 10px;' button-action-data='" . $value[2]['buttons'] . "' button-request-id='" . $value['ID'] . "'button-workflow-id='" . $value['Workflow_ID'] . "'  button-requestor-id='" . $value['Requestor'] . "' button-processor-id='" . $value['Processor'] . "' id='" . $value['TrackNo'] . "'><label for='" . $value['TrackNo'] . "' class='css-label' style='margin-top:-5px;'></label></li>";
                }
                if ($mod['others']['starred'] == "1") {
                    $button .='<li><a class="tip ' . $starred_class . ' btn-basicBtn padding_5 cursor fl-basicsecondary-btn starred" style="background:transparent!important; border: 0!important; font-size:15px;" data-id="' . $value['request_id'] . '" id="starred_' . $value['request_id'] . '" data-starred="' . $starred . '" data-original-title="' . $starred_title . '"></a></li>';
                }
                if ($getGeneratePrintNumrows > 0) {
                    $button .='<li><a class="tip fa fa-print" href="/user_view/workspace?view_type=update&ID=' . $print_form_id . '&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&embed_type=viewEmbedOnly&print_form=true" data-original-title="Print Customized Form"></a></li>';
                } else {
                    $button .='<li><a class="tip noCustomPrint tip btn-basicBtn padding_5 cursor fl-basicsecondary-btn fa fa-print" href="#" data-original-title="No Customized Print Available" style="background:transparent!important; border: 0!important; font-size:15px;opacity: 0.3;"></a></li>';
                }

                // Annotation
                // Temporary Hide
                if (ALLOW_FORM_ANNOTATION == "1") {
                    $annotation = $db->query("SELECT * FROM tb_annotation WHERE Anno_FormID={$db->escape($id)} AND Anno_RequestID={$db->escape($value['request_id'])} AND Anno_TrackNo={$db->escape($value['TrackNo'])} AND Anno_is_active={$db->escape('1')}", "row");
                    $annotation_count = $db->query("SELECT * FROM tb_annotation WHERE Anno_FormID={$db->escape($id)} AND Anno_RequestID={$db->escape($value['request_id'])} AND Anno_TrackNo={$db->escape($value['TrackNo'])} AND Anno_is_active={$db->escape('1')}", "numrows");
                    if ($annotation_count != "0") {

                        // $button .='<li><a class="tip  tip btn-basicBtn padding_5 cursor fl-basicsecondary-btn fa fa-list-alt" href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&annoID=' . $annotation['Anno_UniqueID'] . '" data-original-title="View Annotation" style="background:transparent!important; border: 0!important; font-size:15px;"></a></li>';
                        $button .= '<li><a class="linkTrigger display" href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&printID=' . (trim($print_form_id) == "" ? 0 : $print_form_id) . '&annoID=' . $annotation['Anno_UniqueID'] . '">' . "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div></a></li>";
                    } else {
                        $button .= '<li><a class="linkTrigger display" href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&printID=' . (trim($print_form_id) == "" ? 0 : $print_form_id) . '">' . "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div></a></li>";
                    }
                } else {
                    $button .= '<li><a class="linkTrigger display" href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '&printID=' . (trim($print_form_id) == "" ? 0 : $print_form_id) . '">' . "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div></a></li>";
                }





                //COMMENT_NOTIFICATION
                if (COMMENT_NOTIFICATION == "1") {
                    $noti_condition = " AND tbc.post_id = '" . $value['request_id'] . "' AND tbc.fID = '" . $id . "'";

                    $notiCount = $notifications->countNewNotiForRequest($noti_condition);
                    $newNoti = "";
                    if ($notiCount > 0) {
                        $newNoti = '<span class="notiCommentCounterContainer">' . $notiCount . '</span>
                                        <i class="fa fa-comment-o hasCommentNotification"></i>';
                    } else {
                        $newNoti = '<i class="fa fa-comment-o"></i>';
                    }
                    //comment
                    $button .= '<li class="isCursorPointer showReply tip" count-active-noti="' . $notiCount . '" data-original-title="Add / View Comment" data-type="1" data-f-id="' . $id . '" data-id="' . $value['request_id'] . '" active-comment="true">
                                    <a data-original-title="" title="" style="position:relative">
                                        ' . $newNoti . '
                                    </a>
                                </li>';
                }

                // $button .='<button class="fa fa-comment btn-basicBtn padding_5 fl-basicsecondary-btn cursor showReply tip" data-type="1" data-f-id="' . $id . '" data-id="' . $value['request_id'] . '" data-original-title="Add/View Comments" ></button> ';
                //$button .='<button class="tip fa fa-book btn-basicBtn padding_5 fl-basicsecondary-btn cursor viewAuditLogs" style="margin-right:3px" data-request-id="' . $value['request_id'] . '" data-form-id="' . $id . '" data-original-title="Audit Logs"></button>';
                if ($value[2]['buttons'] != '' && $value[2]['buttons'] != 'null' && $value[2]['buttons'] != null) {
                    //$button .= "<button class='tip fa fa-gear btn-basicBtn padding_5 fl-basicsecondary-btn cursor' button-action-data='" . $value[2]['buttons'] . "' button-request-id='" . $value['ID'] . "'button-workflow-id='" . $value['Workflow_ID'] . "'  button-requestor-id='" . $value['Requestor'] . "' button-processor-id='" . $value['Processor'] . "' button-type='action' data-original-title='Please select an Action'></button>";
                }
                if ($getDeleteAccess['delete_access_forma'] > 0 && ENABLE_DELETION == "1" && $enable_deletion == "1") {
                    $button .='<li><a class="tip fa fa-remove btn-basicBtn padding_5 cursor fl-basicsecondary-btn trash-record-single" style="margin-left:2px;color: #F95353;background:transparent!important; border: 0!important; font-size:15px;" data-id="' . $value['request_id'] . '" title="Delete"></a></li>';
                }
                $button.= '</ul>';
                //reorder
                if (count($getCustomView) > 0) {
                    // $customViewObject = json_decode($getCustomView[0]['json'],true);
                    $customViewObject = getValidHeaderFields($fields_headerObj, $getCustomView, $fields_headerObjType);
                    array_push($aaData, $button);
                    foreach ($customViewObject as $customView) {
                        if ($customView['field_name'] == "Requestor") {
                            $vl = "requestorName";
                        } else {
                            $vl = $customView['field_name'];
                        }
                        // if($vl=="TrackNo"){
                        //  array_push($aaData, '<a href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '">'."<div class='fl-table-ellip'>".$value['TrackNo']."</div></a>");
                        // }else if($vl!=""){

                        $data = $value['' . $vl . ''];


                        if ($isJunctionFields > 0) {
                            $customView['value'] = $value['' . $vl . ''];
                            $data = $listViewFormula->setValue($customView);
                        }
                        $style = "";
                        if ($customView['field_input_type'] == "Currency" || $customView['field_input_type'] == "Number") {
                            $style.="text-align:right;";
                        }
                        if ($customView['field_type'] == "dateTime" || $customView['field_type'] == "time") {
                            $data = substr($data, 0, strlen($data) - 3);
                        }
                        array_push($aaData, "<div class='fl-table-ellip' style='" . $style . "'>" . $data . "</div>");
                        // }
                    }
                } else {
                    $fields_headerObj = json_decode(json_encode($fields_headerObj));
                    //default column
                    $aaData = array($button,
                        // '<a href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '">'."<div class='fl-table-ellip'>".$value['TrackNo']."</div></a>",
                        "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div>",
                        "<div class='fl-table-ellip'>" . $value['requestorName'] . "</div>",
                        "<div class='fl-table-ellip'>" . $value['form_status'] . "</div>",
                        "<div class='fl-table-ellip'>" . $value['DateCreated'] . "</div>",
                    );
                    //old shit
                    //push to array $aadata in specific location
                    foreach ($fields_headerObj as $fields_header) {
                        if ($value['' . $fields_header->{'field_name'} . ''] == "") {
                            array_insert($aaData, "", $otherFieldsStartPos);
                        } else {
                            array_insert($aaData, "<div class='fl-table-ellip'>" . $value['' . $fields_header->{'field_name'} . ''] . "</div>", $otherFieldsStartPos);
                        }
                        $otherFieldsStartPos++;
                    }


                    //Updated by AARON TOLENTINO
                    // - For having a specific and default header information
                    if ($fields_headerObjType == "1") {
                        $aaData = array($button);
                        foreach ($fields_headerObj as $fields_header) {
                            if ($value['' . $fields_header->{'field_name'} . ''] == "") {
                                array_push($aaData, "");
                            } else {
                                if ($fields_header->{'field_name'} == "Requestor") {
                                    $vl = "requestorName";
                                } else {
                                    $vl = $fields_header->{'field_name'};
                                }

                                $data = $value['' . $vl . ''];

                                $style = "";

                                if ($isJunctionFields > 0) {
                                    $fields_header->{'value'} = $value['' . $vl . ''];
                                    $data = $listViewFormula->setValue($fields_header);

                                    if ($fields_header->{'field_input_type'} == "Currency" || $customView['field_input_type'] == "Number") {
                                        $style.="text-align:right;";
                                    }
                                    if ($fields_header->{'field_type'} == "dateTime" || $fields_header->{'field_type'} == "time") {
                                        $data = substr($data, 0, strlen($data) - 3);
                                    }
                                }
                                array_push($aaData, "<div class='fl-table-ellip' style='" . $style . "'>" . $data . "</div>");
                            }
                        }
                    } else if ($fields_headerObjType == "0") {
                        // FOR DEFAULT COLUMN
                        $aaData = array($button,
                            // '<a href="/user_view/workspace?view_type=request&formID=' . $id . '&requestID=' . $value['request_id'] . '&trackNo=' . $value['TrackNo'] . '">'."<div class='fl-table-ellip'>".$value['TrackNo']."</div></a>",
                            "<div class='fl-table-ellip'>" . $value['TrackNo'] . "</div>",
                            "<div class='fl-table-ellip'>" . $value['requestorName'] . "</div>",
                            "<div class='fl-table-ellip'>" . $value['form_status'] . "</div>",
                            "<div class='fl-table-ellip'>" . $value['DateCreated'] . "</div>",
                        );
                    }
                }
                $output['aaData'][] = $aaData;
            }
            echo json_encode($output);
            break;
        case "getRequestCalendar":
            $id = $post_data['id'];
            $start_date = $post_data['start_date'];
            $end_date = $post_data['end_date'];
            $return_array = array();
            $searchfilter = $post_data['filter'];
            // var_dump($searchfilter);
            $obj = array();
            $getidq = "SELECT `id` as ID FROM `tbuser` WHERE `display_name` LIKE '%{$searchfilter}%'";
            // var_dump($getidq);
            $getid = $db->query($getidq, "row");
            // var_dump($getid);
            //get Active Fields
            $sql = "SELECT active_fields FROM tb_workspace WHERE is_delete = 0 AND id = {$db->escape($id)}";
            $getActiveFields = $db->query($sql, "row");
            $array_activeFields = explode(",", $getActiveFields['active_fields']);
            // var_dump($array_activeFields);
            $active_form_fields ='';
            foreach ($array_activeFields as  $value) {
                $active_form_fields = $active_form_fields.' OR request.'.$value.' LIKE "%'.$searchfilter.'%"';
            }
            // echo $active_form_fields;


            $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($id)} AND is_delete = 0", "row");
            $form_json = json_decode($forms['form_json']);
            $form_json = $form_json->{'form_json'};
            $include_calendar = $form_json->include_calendar;
            $calendar = $form_json->calendar_view;
            $calendar_title = $calendar->title;
            $calendar_enable_tooltip = $calendar->calendar_enable_tooltip;


            $calendar_tooltip = json_decode(json_encode($calendar->calendar_tooltip_values), true);
            if ($calendar_title == "") {
                $calendar_title = "TrackNo";
            }
            $calendar_view_type = $calendar->calendar_view_type;

            if ($include_calendar != "1") {
                return false;
            }

            if ($calendar_view_type == "1") {
                $single_date = $calendar->single_date;
                $obj['multi_search'] = " AND (request." . $single_date . " between '$start_date' AND '$end_date') AND (TrackNo LIKE '%".$searchfilter."%' OR request.Status LIKE '%".$searchfilter."%' OR DateCreated LIKE '%".$searchfilter."%' OR requester.display_name LIKE '%".$searchfilter."%'".$active_form_fields.")";
                // AND (TrackNo LIKE '%".$searchfilter."%' OR request.Status LIKE '%".$searchfilter."%' OR DateCreated LIKE '%".$filter."%' OR requester.display_name LIKE '%".$filter."%' OR province LIKE '%".$filter."%')
                // .$active_form_fields.
                // var_dump($obj['multi_search']);
            } else {
                $startField_date = $calendar->start_date;
                $endField_date = $calendar->end_date;
                $obj['multi_search'] = " AND ((request." . $startField_date . " between '$start_date' AND '$end_date') OR (request." . $endField_date . " between '$start_date' AND '$end_date'))";
            }




            if ($redis_cache) {
                $cachedFormat = "$user_id::$calendar_view_type::$start_date::$end_date";

                // $memcache->delete("calendar_view");
                $cache_picklist = json_decode($redis_cache->get("calendar_view"), true);
                // print_r($cache_picklist)
                $cached_query_result = $cache_picklist['form_id_' . $id]['' . $cachedFormat . ''];

                // echo "Format==".$cachedFormat;
                if ($cached_query_result) {
                    // echo "from cache";

                    $result = $cached_query_result; //array();//$search->getManyRequestV2($search_value, $field, $id, $start, $limit, $obj);
                } else {
                    // echo "from db";
                    // $searchObject = new Search();
                    $result = $search->getManyRequestV2($searchfilter, 0, $id, 0, 1000, $obj);


                    // $to_cache_result = array("form_id_".$id=>array($cachedFormat=>$result));
                    $cache_picklist['form_id_' . $id]['' . $cachedFormat . ''] = $result;
                    $redis_cache->set("calendar_view", json_encode($cache_picklist));
                }
            } else {
                $result = $search->getManyRequestV2('', 0, $id, 0, 1000, $obj);
            }




            foreach ($calendar_tooltip as $key => $value_tooltip) {
                $field_name = "{$db->escape($value_tooltip['field'])}";
                $getfieldType = $db->query("SELECT * FROM tbfields WHERE field_name = " . $field_name . " AND form_id = {$db->escape($id)}", "row");
                $calendar_tooltip[$key]['field_input_type'] = $getfieldType['field_input_type'];
                $calendar_tooltip[$key]['field_type'] = $getfieldType['field_type'];
            }
            $tooltip = "";
            foreach ($result as $value) {
                if ($calendar_enable_tooltip != "0") {
                    $tooltip = "<div class='calendar-tooltip-container' style='width:200px;text-align:left;max-width:500px'>";
                    foreach ($calendar_tooltip as $value_tooltip) {
                        $field_name = str_replace("[]", "", $value_tooltip['field']);
                        $arr_value = array("field_input_type" => $value_tooltip['field_input_type'], "field_type" => $value_tooltip['field_type'], "value" => $value['' . $field_name . '']);
                        $data = $listViewFormula->setValue($arr_value);
                        $tooltip.= "<div class='fl-table-ellip'>" . $value_tooltip['label'] . " : " . $data . "</div>";
                    }
                    $tooltip.= "</div>";
                }
                if ($calendar_view_type == "1") {
                    $arr = array("title" => $value['' . $calendar_title . ''], "start" => $value['' . $single_date . ''], "url" => "/user_view/workspace?view_type=request&formID=" . $id . "&requestID=" . $value['request_id'] . "&trackNo=" . $value['TrackNo'] . "");
                } else if ($calendar_view_type == "2") {
                    $endDate = $value['' . $endField_date . ''];
                    /* temporary fix to add 1 day in the end date */

                    $date = new DateTime($value['' . $endField_date . '']);
                    $date->modify('+1 day');
                    $endDate = $date->format('Y-m-d H:i:s');

                    $arr = array("title" => $value['' . $calendar_title . ''], "start" => $value['' . $startField_date . ''], "end" => $endDate, "url" => "/user_view/workspace?view_type=request&formID=" . $id . "&requestID=" . $value['request_id'] . "&trackNo=" . $value['TrackNo'] . "");
                }

                $arr['customized_tooltip'] = $tooltip;
                $return_array[] = $arr;
            }
            echo json_encode($return_array);
            break;
        case "getOrgchart":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['start'];
            $result = $search->getOrgchart($search_value, $start);
            echo json_encode($result);
            break;
        case "getOrgchartDataTable":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['iDisplayStart'];
            $limit = $post_data['limit'];
            $json = array("column-sort" => $post_data['column-sort'], "column-sort-type" => $post_data['column-sort-type'], "end" => $limit);
            $search_value = $db->addslash_escape(mysql_escape_string($search_value));

            if ($redis_cache) {

                /*
                  Cache Format
                  1. User Id.
                  2. Page Number.
                  3. Page Limit.
                  4. Search Filter.
                  5. Column Sort.
                  6. Type of Sort.
                 */

                $cachedFormat = "$user_id::$start::$limit::$search_value::" . $post_data['column-sort'] . "::" . $post_data['column-sort-type'] . "";

                // $memcache->delete("orgchart_list");
                //for results
                $cache_orgchart_list = json_decode($redis_cache->get("orgchart_list"), true);
                $cached_query_result = $cache_orgchart_list['' . $cachedFormat . ''];

                if ($cached_query_result) {
                    // echo "from cache";

                    $result = $cached_query_result;
                } else {
                    // echo "from db";

                    $result = $search->getOrgchart($search_value, $start, $json);
                    $cache_orgchart_list['' . $cachedFormat . ''] = $result;
                    $redis_cache->set("orgchart_list", json_encode($cache_orgchart_list));
                }


                //for count 
                // $memcache->delete("orgchart_count");
                $cachedFormatCount = "$user_id::$search_value";
                $cache_orgchart_count = json_decode($redis_cache->get("orgchart_count"), true);
                $cached_query_result_count = $cache_orgchart_count['' . $cachedFormatCount . ''];


                if ($cached_query_result_count) {
                    $countOrgchart = $cached_query_result_count;
                } else {
                    $countOrgchart = count($search->getOrgchart($search_value, ""));
                    $cache_orgchart_count['' . $cachedFormatCount . ''] = $countOrgchart;
                    $redis_cache->set("orgchart_count", json_encode($cache_orgchart_count));
                }
            } else {
                $result = $search->getOrgchart($search_value, $start, $json);
                $countOrgchart = count($search->getOrgchart($search_value, ""));
            }




            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countOrgchart,
                "iTotalDisplayRecords" => $countOrgchart,
                "start" => $start,
                "aaData" => array()
            );
            foreach ($result as $value) {
                $button = "";
                $pushArray = array();
                $button = '<a href="/user_view/organizational_chart?view_type=edit&id=' . $value['id'] . '" class="tip cursor" data-original-title="Edit" data-placement="top" id="" data-orgchart-id="" style="margin-right:3px"><i class="fa fa-pencil-square-o" data-placement="top" data-original-title="Edit"></i></a>
                            <a class="tip cursor update_status_orgchart" data-original-title="' . $fs->getStatus_revert($value['status']) . '" data-status="' . $value['status'] . '" data-placement="top" id="" data-id="' . $value['id'] . '" style="margin-right:3px"><i class="' . $fs->setStatusButtonIcon($value['status']) . '" data-placement="top" data-original-title="Deactivate"></i></a>
                            <a class="tip cursor softDelete_orgchart" data-original-title="Delete" data-placement="top" id="" data-id="' . $value['id'] . '"><i class="fa fa-trash-o" data-placement="top" data-original-title="Delete"></i></a>
                            <a class="tip cursor delete_orgchart display" data-original-title="Delete Orgchart" data-placement="top" id="" data-id="' . $value['id'] . '"><i class="fa fa-trash-o" data-placement="top" data-original-title="Delete"></i></a>';
                $pushArray[] = $button;
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['title'] . "'>" . $value['title'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['description'] . "'>" . $value['description'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $fs->getStatus($value['status']) . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['date'] . "'>" . $value['date'] . "</div>";
                $output['aaData'][] = $pushArray;
            }
            echo json_encode($output);
            break;
        case "getForm":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['start'];
            $result = $search->getForms($search_value, $start);
            echo setAdminViewer($result);
            break;
        case "getFormDataTable":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['iDisplayStart'];
            $limit = $post_data['limit'];
            $json = array("column-sort" => $post_data['column-sort'], "column-sort-type" => $post_data['column-sort-type'], "end" => $limit);
            if (ENABLE_STAGING == "0") {
                $json['other_condition'] = " AND tbw.workspace_version = '1'";
            }
            $search_value = $db->addslash_escape(mysql_escape_string($search_value));

            if ($redis_cache) {

                /*
                  Cache Format
                  1. User Id.
                  2. Page Number.
                  3. Page Limit.
                  4. Search Filter.
                  5. Column Sort.
                  6. Type of Sort.
                 */

                $cachedFormat = "$user_id::$start::$limit::$search_value::" . $post_data['column-sort'] . "::" . $post_data['column-sort-type'] . "";

                // $memcache->delete("request_list");
                //for results
                $cache_form_list = json_decode($redis_cache->get("form_list"), true);
                $cached_query_result = $cache_form_list['' . $cachedFormat . ''];

                if ($cached_query_result) {
                    // echo "from cache";

                    $result2 = $cached_query_result;
                } else {
                    // echo "from db";

                    $result2 = $search->getFormsV2($search_value, $start, $json, "array");
                    $cache_form_list['' . $cachedFormat . ''] = $result2;
                    $redis_cache->set("form_list", json_encode($cache_form_list));
                }


                //for count 
                // $memcache->delete("form_count");
                $cachedFormatCount = "$user_id::$search_value";
                $cache_form_count = json_decode($redis_cache->get("form_count"), true);
                $cached_query_result_count = $cache_form_count['' . $cachedFormatCount . ''];


                if ($cached_query_result_count) {
                    $countForm = $cached_query_result_count;
                } else {
                    $countForm = $search->getFormsV2($search_value, $start, $json, "numrows");
                    $cache_form_count['' . $cachedFormatCount . ''] = $countForm;
                    $redis_cache->set("form_count", json_encode($cache_form_count));
                }
            } else {
                $result2 = $search->getFormsV2($search_value, $start, $json, "array");
                // $result = $search->getForms($search_value,$start,$json);
                // $result2 = json_decode(setAdminViewer($result),true);
                //            $countForm = $search->getForms($search_value,"");
                //            $countForm = count(json_decode(setAdminViewer($countForm),true));

                $countForm = $search->getFormsV2($search_value, $start, $json, "numrows");
            }



            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countForm,
                "iTotalDisplayRecords" => $countForm,
                "start" => $start,
                "aaData" => array(),
            );
            foreach ($result2 as $value) {
                $value['wsID'] = $value['form_id'];
                $button = "";
                $pushArray = array();
                $button = '<a href="/user_view/formbuilder?formID=' . $value['wsID'] . '&view_type=edit" class="tip cursor" data-original-title="Edit" data-placement="top" id="" data-form-id="' . $value['wsID'] . '"><i class="fa fa-pencil-square-o" data-placement="top" data-original-title="Edit"></i></a>
            <a class="tip cursor update_status_form" data-original-title="' . $fs->getStatus_revert($value['form_active']) . '" data-status="' . $value['form_active'] . '" data-placement="top" id="" data-id="' . $value['wsID'] . '"><i class="' . $fs->setStatusButtonIcon($value['form_active']) . '" data-placement="top" data-original-title="Deactivate"></i></a>
            <a class="tip cursor softDelete_form" data-original-title="Delete" data-form-id="' . $value['wsID'] . '"><i class="fa fa-trash-o" data-placement="top" data-original-title="Delete"></i></a>
            <a class="tip cursor delete_form display" data-original-title="Delete Form" data-form-id="' . $value['wsID'] . '"><i class="fa fa-trash-o" data-placement="top" data-original-title="Delete"></i></a>';
                //for staging button

                if (ENABLE_STAGING == "1" && $value['workspace_version'] != "2") {
                    $json = array("other_condition" => " AND tbdf.parent_id = '" . $value['wsID'] . "'");
                    $countDevForms = $search->getFormsV2($search_value, $start, $json, "numrows");
                    $button.= '<a class="tip cursor reflect-form"  form_id = "' . $value['wsID'] . '" count_dev_forms="' . $countDevForms . '" data-original-title="Get Dev Version" data-form-id="' . $value['wsID'] . '"><i class="fa fa-database" data-placement="top" data-original-title="Reflect"></i></a>';
                }

                if ($value['allow_portal'] != 0) {
                    $value['allow_portal'] = "<div class='fl_badge_SpringGreen isDisplayInlineBlock'>Yes</div>";
                }else {
                    $value['allow_portal'] = "<div class='fl_badge_DarkRed isDisplayInlineBlock'>No</div>";
                }


                $pushArray[] = $button;
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['form_name'] . "'>" . $value['form_name'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['category_name'] . "'>" . $value['category_name'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['form_alias'] . "'>" . $value['form_alias'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $fs->getStatus($value['form_active']) . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['allow_portal'] . "</div>";
                //                $pushArray[] = "<div class='fl-table-ellip'>" . $fs->getFormType($value['form_type']) . "</div>";
                if (ENABLE_STAGING == "1") {
                    $pushArray[] = "<div class='fl-table-ellip' title='" . $fs->getFormVersionStr($value['workspace_version']) . "'>" . $fs->getFormVersion($value['workspace_version']) . "</div>";
                }
                // $pushArray[] = "<div class='fl-table-ellip'>" . $value['form_date_created']."</div>";
                // $pushArray[] = "<div class='fl-table-ellip'>" . $value['form_date_updated']."</div>";
                //workflow
                $workflow = "<a href='/user_view/workflow?view_type=edit&form_id=" . $value['wsID'] . "&id=" . $value['workflow_id'] . "' title='View Workflow'>View Workflow</a>";
                if ($value['workflow_id'] == 0) {
                    //$workflow = "<font color='#cccccc'>No Active Workflow</font>";
                    $workflow = functions::empty_in_list("", "No Active Workflow");
                }
                $pushArray[] = "<div class='fl-table-ellip' title='No Active Workflow'>" . $workflow . "</div>";
                $output['aaData'][] = $pushArray;
            }
            echo json_encode($output);
            break;
        case "getWorkflow":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['start'];
            $result = $search->getWorkflows($search_value, $start);
            // echo json_encode($result);
            echo setAdminViewer($result);
            break;
        case "getWorkflowDataTable":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['iDisplayStart'];
            $limit = $post_data['limit'];
            $other_condition = "AND tbwf.is_active = 1";
            $json = array("column-sort" => $post_data['column-sort'], "column-sort-type" => $post_data['column-sort-type'], "endlimit" => $limit, "other_condition" => $other_condition);
            // $countworkflow = $search->getWorkflows($search_value,"");
            // $countworkflow = count(json_decode(setAdminViewer($countworkflow),true));
            // $result = $search->getWorkflows($search_value,$start,$json);
            // // echo json_encode($result);
            // $result2 = json_decode(setAdminViewer($result),true);
            $search_value = $db->addslash_escape(mysql_escape_string($search_value));

            if ($redis_cache) {

                /*
                  Cache Format
                  1. User Id.
                  2. Page Number.
                  3. Page Limit.
                  4. Search Filter.
                  5. Column Sort.
                  6. Type of Sort.
                 */

                $cachedFormat = "$user_id::$start::$limit::$search_value::" . $post_data['column-sort'] . "::" . $post_data['column-sort-type'] . "";

                // $memcache->delete("request_list");
                //for results
                $cache_workflow_list = json_decode($redis_cache->get("workflow_list"), true);
                $cached_query_result = $cache_workflow_list['' . $cachedFormat . ''];

                if ($cached_query_result) {
                    // echo "from cache";

                    $result2 = $cached_query_result;
                } else {
                    // echo "from db";

                    $result2 = $search->getWorkflowsV2($search_value, $start, $json, "array");
                    $cache_workflow_list['' . $cachedFormat . ''] = $result2;
                    $redis_cache->set("workflow_list", json_encode($cache_workflow_list));
                }


                //for count 
                // $memcache->delete("workflow_count");
                $cachedFormatCount = "$user_id::$search_value";
                $cache_workflow_count = json_decode($redis_cache->get("workflow_count"), true);
                $cached_query_result_count = $cache_workflow_count['' . $cachedFormatCount . ''];


                if ($cached_query_result_count) {
                    $countworkflow = $cached_query_result_count;
                } else {
                    $countworkflow = $search->getWorkflowsV2($search_value, $start, $json, "numrows");
                    $cache_workflow_count['' . $cachedFormatCount . ''] = $countworkflow;
                    $redis_cache->set("workflow_count", json_encode($cache_workflow_count));
                }
            } else {
                $result2 = $search->getWorkflowsV2($search_value, $start, $json, "array");
                $countworkflow = $search->getWorkflowsV2($search_value, $start, $json, "numrows");
            }



            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countworkflow,
                "iTotalDisplayRecords" => $countworkflow,
                "start" => $start,
                "aaData" => array(),
            );
            foreach ($result2 as $value) {
                $button = "";
                $tooltip = $fs->getStatus_revert($value['Is_Active']);
                // $status = $fs->getStatus($value['Is_Active']);

                if ($value['Is_Active'] == 2) {
                    // $status = "Draft";
                    $tooltip = "Not allowed to change status";
                }


                $pushArray = array();
                $button = '<a href="/user_view/workflow?view_type=edit&form_id=' . $value['form_id'] . '&id=' . $value['wf_id'] . '" class="cursor" data-original-title="Edit ' . $value['wf_title'] . '" data-placement="top" id="" data-orgchart-id=""><i class="fa fa-pencil-square-o tip" data-placement="top" data-original-title="Edit"></i></a>
            <a class="cursor update_status_workflow" data-original-title="' . $tooltip . '" data-status="' . $value['Is_Active'] . '" data-placement="top" id="" data-id="' . $value['wf_id'] . '"><i class="' . $fs->setStatusButtonIcon($value['Is_Active']) . ' tip" data-placement="top" data-original-title="' . $tooltip . '"></i></a>
            <a class= "cursor softDelete_workflow" data-original-title="Delete Workflow" data-placement="top" id="" data-id="' . $value['wf_id'] . '"><i class="fa fa-trash-o tip" data-placement="top" data-original-title="Delete"></i></a>
            <a class="cursor delete_workflow display" data-original-title="Delete Workflow" data-placement="top" id="" data-id="' . $value['wf_id'] . '"><i class="fa fa-trash-o tip" data-placement="top" data-original-title="Delete"></i></a>';
                $pushArray[] = $button;
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['wf_title'] . "'>" . $value['wf_title'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['form_name'] . "'><a href='/user_view/formbuilder?formID=" . $value['form_id'] . "&view_type=edit'>" . $value['form_name'] . "</a></div>";
                // $pushArray[] = "<div class='fl-table-ellip'>" . $status . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['wf_date'] . "'>" . $value['wf_date'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . functions::empty_in_list($value['workflow_creator'], "Unkown", "0") . "'>" . functions::empty_in_list($value['workflow_creator'], "Unkown") . "</div>";
                $output['aaData'][] = $pushArray;
            }
            echo json_encode($output);
            break;
        case "loadWorkflowHistory":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['iDisplayStart'];
            $limit = $post_data['limit'];
            $form_id = $post_data['form_id'];
            $workflow_id = $post_data['workflow_id'];
            $other_condition = " AND tbwf.form_id = $form_id ";
            $workflow_condition = " AND tbwf.id != $workflow_id";
            if ($workflow_id != "") {
                $other_condition.= $workflow_condition;
            }
            $json = array("column-sort" => $post_data['column-sort'], "column-sort-type" => $post_data['column-sort-type'], "endlimit" => $limit, "other_condition" => $other_condition);
            // $countworkflow = $search->getWorkflows($search_value,"");
            // $countworkflow = count(json_decode(setAdminViewer($countworkflow),true));
            // $result = $search->getWorkflows($search_value,$start,$json);
            // // echo json_encode($result);
            // $result2 = json_decode(setAdminViewer($result),true);
            $search_value = $db->addslash_escape(mysql_escape_string($search_value));

            $result2 = $search->getWorkflowsV2($search_value, $start, $json, "array");
            $countworkflow = $search->getWorkflowsV2($search_value, $start, $json, "numrows");
            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countworkflow,
                "iTotalDisplayRecords" => $countworkflow,
                "start" => $start,
                "aaData" => array(),
            );
            foreach ($result2 as $value) {
                $button = "";
                $tooltip = $fs->getStatus_revert($value['Is_Active']);
                $status = $fs->getStatus($value['Is_Active']);

                if ($value['Is_Active'] == 2) {
                    // $status = "Draft";
                    $tooltip = "Not allowed to change status";
                }


                $pushArray = array();
                $button = '<a href="/user_view/workflow?view_type=edit&form_id=' . $value['form_id'] . '&id=' . $value['wf_id'] . '" class="cursor" data-original-title="Edit ' . $value['wf_title'] . '" data-placement="top" id="" data-orgchart-id=""><i class="fa fa-pencil-square-o tip" data-placement="top" data-original-title="Edit"></i></a>
            <a class="cursor update_status_workflow" data-original-title="' . $tooltip . '" data-status="' . $value['Is_Active'] . '" data-placement="top" id="" data-id="' . $value['wf_id'] . '"><i class="' . $fs->setStatusButtonIcon($value['Is_Active']) . ' tip" data-placement="top" data-original-title="' . $tooltip . '"></i></a>
            <a class= "cursor softDelete_workflow" data-original-title="Delete Workflow" data-placement="top" id="" data-id="' . $value['wf_id'] . '"><i class="fa fa-trash-o tip" data-placement="top" data-original-title="Delete"></i></a>
            <a class="cursor delete_workflow display" data-original-title="Delete Workflow" data-placement="top" id="" data-id="' . $value['wf_id'] . '"><i class="fa fa-trash-o tip" data-placement="top" data-original-title="Delete"></i></a>';
                $pushArray[] = $button;
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['wf_title'] . "'>" . $value['wf_title'] . "</div>";
                // $pushArray[] = "<div class='fl-table-ellip'><a href='/user_view/formbuilder?formID=" . $value['form_id'] . "&view_type=edit'>" . $value['form_name'] . "</a></div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $status . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['wf_date'] . "'>" . $value['wf_date'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . functions::empty_in_list($value['workflow_creator'], "Unkown") . "'>" . functions::empty_in_list($value['workflow_creator'], "Unkown") . "</div>";
                $output['aaData'][] = $pushArray;
            }
            echo json_encode($output);
            break;
        case "loadLatestModifiedWorkflow":
            $search_value = trim($post_data['search_value']);
            $form_id = $post_data['form_id'];
            $start = $post_data['iDisplayStart'];
            $json = array("column-sort" => $post_data['column-sort'],
                "column-sort-type" => $post_data['column-sort-type'],
                "endlimit" => $post_data['endlimit'],
                "customQuery" => " AND tbwf.is_active!=1 AND tbwf.form_id = '" . $form_id . "'");
            $search_value = $db->addslash_escape(mysql_escape_string($search_value));

            $result2 = $search->getWorkflowsV2($search_value, $start, $json, "array");
            $countworkflow = $search->getWorkflowsV2($search_value, $start, $json, "numrows_limit");
            // $countworkflow = $post_data['endlimit'];
            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countworkflow,
                "iTotalDisplayRecords" => $countworkflow,
                "start" => $start,
                "aaData" => array(),
            );
            foreach ($result2 as $value) {
                $button = "";
                $status = $fs->getStatus($value['Is_Active']);

                $pushArray = array();
                $button = '<a href="/user_view/workflow?view_type=edit&form_id=' . $value['form_id'] . '&id=' . $value['wf_id'] . '" class="cursor" data-original-title="Edit ' . $value['wf_title'] . '" data-placement="top" id="" data-orgchart-id=""><i class="fa fa-pencil-square-o tip" data-placement="top" data-original-title="Edit"></i></a>';
                $pushArray[] = $button;
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['wf_title'] . "'>" . $value['wf_title'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $status . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['wf_date'] . "'>" . $value['wf_date'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . functions::empty_in_list($value['workflow_creator'], "Unkown") . "'>" . functions::empty_in_list($value['workflow_creator'], "Unkown") . "</div>";
                $output['aaData'][] = $pushArray;
            }
            echo json_encode($output);
            break;
        case "getReports":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['start'];
            $reports = functions::getReports("WHERE title LIKE '%" . $search_value . "%' AND company_id={$db->escape($auth['company_id'])} LIMIT $start ,10");
            print_r(json_encode($reports));
            break;
        case "getReportsDataTable":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['iDisplayStart'];
            $limit = $post_data['limit'];
            $orderBy = " ORDER BY is_active DESC, title ASC";
            $search_value = $db->addslash_escape(mysql_escape_string($search_value));
            if ($post_data['column-sort']) {
                $column = $post_data['column-sort'];
                if ($column == "form") {
                    $column = "tbw.form_name";
                }
                $orderBy = " ORDER BY " . $column . " " . $post_data['column-sort-type'];
            }

            $status_condition = $search->status_condition("is_active", $search);
            $exclude_inactive = "";

            $report_users_query = "(EXISTS(
                                SELECT * FROM tbreport_users tbru
                                    LEFT JOIN
                                tbdepartment_users tbdu ON tbdu.department_code = tbru.user
                                    LEFT JOIN
                                tbform_groups_users tbfgu ON tbfgu.group_id = tbru.user
                                    LEFT JOIN
                                tborgchart tbo ON tbo.id = tbdu.orgChart_id
                                    LEFT JOIN
                                tbpositions tbp ON tbp.id = tbru.user
                                WHERE tbru.report_id = tbr.id AND
                                (
                                    (tbru.user_type = 3 AND tbru.user = {$auth["id"]}) OR 
                                    (tbru.user_type = 1 AND tbru.user = {$conn->escape($auth["position"])}) OR 
                                    (tbru.user_type = 2 AND tbdu.user_id = {$auth["id"]} AND tbo.status = 1) OR 
                                    (tbru.user_type = 4 AND tbfgu.user_id = {$auth["id"]} AND tbfgu.is_active = 1)
                                ) 
                            )
                            OR (EXISTS(
                                SELECT * FROM  
                                tbform_users tbfu 
                                WHERE tbfu.form_id = tbw.id AND tbfu.action_type = 4 AND tbfu.user_type = 3
                                AND tbfu.user = {$auth["id"]})))";

            $query = "SELECT 
                        tbr.id, 
                        tbr.form_id, tbr.title, 
                        tbr.date_created, 
                        tbr.date_updated, 
                        tbr.is_active,
                        tbw.form_name as form_name
                    FROM
                        tbreport tbr
                            LEFT JOIN 
                        tb_workspace tbw ON tbw.id = tbr.form_id
                            LEFT JOIN 
                        tbuser tbcreated_by ON tbcreated_by.id = tbr.created_by
                            LEFT JOIN 
                        tbuser tbupdated_by ON tbupdated_by.id = tbr.updated_by
                    WHERE
                        (
                            (
                                $report_users_query
                                OR 
                                (tbw.category_id = 0)
                            )    
                        )
                        AND tbr.company_id = {$auth["company_id"]}
                        {$exclude_inactive}
                        AND 
                        (
                            tbr.title LIKE '%" . $search_value . "%'
                            OR tbr.date_created LIKE '%" . $search_value . "%'
                            OR tbr.date_updated LIKE '%" . $search_value . "%'
                            OR tbcreated_by.display_name LIKE '%" . $search_value . "%'
                            OR tbupdated_by.display_name LIKE '%" . $search_value . "%'
                            OR (SELECT form_name FROM tb_workspace tbws WHERE tbws.id = tbw.id) LIKE '%" . $search_value . "%'
                            OR (
                                    CASE tbr.is_active
                                            WHEN 1 THEN 'Active'
                                            ELSE 'Not Active'
                                    END) LIKE '" . $search_value . "'
                            $status_condition
                        )
                        GROUP BY tbr.id $orderBy";


            $reports = $conn->query($query . "  LIMIT $start ,$limit");
            $countReports = $conn->query($query, "numrows");
//            $reports = $db->query($query . "  LIMIT $start ,$limit", "array");
//            $countReports = $db->query($query, "numrows");

            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countReports,
                "iTotalDisplayRecords" => $countReports,
                "start" => $start,
                "aaData" => array()
            );

//            $reports = json_decode(json_encode($reports), true);
            foreach ($reports as $value) {
                $button = "";
                $pushArray = array();

                $tooltip = $fs->getStatus_revert($value['is_active']);
                if ($auth["user_level_id"] == "2") {
                    $button = '<a href="/user_view/report?id=' . $value['id'] . '&form_id=' . $value['form_id'] . '"><i class="fa fa-pencil-square-o tip" data-placement="top" title="Edit"></i></a>';

                    if ($value['is_active'] == 1) {
                        $button.='<a><i class="fa fa-file-text tip" data-placement="top" data-original-title="Generate" link-type="report-generation" data-report-id="' . $value['id'] . '"></i></a>';
                    }

                    $button.='<a><i class="' . $fs->setStatusButtonIcon($value['is_active']) . ' tip update_status_report" data-placement="top" data-original-title="' . $tooltip . '" data-status="' . $value['is_active'] . '" data-placement="top" id="" data-id="' . $value['id'] . '"></i></a>';
                }

                if ($auth["user_level_id"] == "3") {
                    $button = '<a><i class="fa fa-file-text tip" data-placement="top" data-original-title="Generate" link-type="report-generation" data-report-id="' . $value['id'] . '"></i></a>';
                }
                // $button = '<a><i class="fa fa-globe tip" data-placement="top" data-original-title="Generate" link-type="report-generation" data-report-id="' . $value['id'] . '"></i></a>
                // <a><i class="fa fa-trash-o tip update_status_report" data-placement="top" data-original-title="Delete" data-status="' . $value['is_active'] . '" data-placement="top" id="" data-id="' . $value['id'] . '"></i></a>';

                $pushArray[] = $button;
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['title'] . "'>" . $value['title'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['form_name'] . "'>" . $value['form_name'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $fs->getStatus($value['is_active']) . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . functions::empty_in_list($value['date_created'], "", "0") . "'>" . functions::empty_in_list($value['date_created'], "") . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'  title='" . functions::empty_in_list($value['date_updated'], "", "0") . "'>" . functions::empty_in_list($value['date_updated'], "") . "</div>";
                $output['aaData'][] = $pushArray;
            }

            $conn->disConnect();
            exit(json_encode($output));
            break;
        case "getPrintReport";
            // $search_value = trim($post_data['search_value']);
            // $start = $post_data['start'];
            $result = $search->getPrintReports(" WHERE company_id ={$db->escape($auth['company_id'])}");
            echo json_encode($result);
            break;
        case "saveRequestFilter";
            $search_value = trim($post_data['search_value']);
            $search_field = $post_data['search_field'];
            $multi_search = $post_data['multi_search'];
            $title = $post_data['title'];
            $form_id = $post_data['form_id'];
            $date_field = $post_data['date_field'];
            $date_from = $post_data['date_from'];
            $date_to = $post_data['date_to'];
            $limit = $post_data['limit'];
            $insert = array("search_value" => $search_value,
                "search_field" => $search_field,
                "multi_search" => $multi_search,
                "title" => $title,
                "date_field" => $date_field,
                "date_from" => $date_from,
                "date_to" => $date_to,
                "limit" => $limit,
                "form_id" => $form_id,
                "user_id" => $user_id);
            echo $db->insert("tbfilteredCategory", $insert);
            break;
        case "deleteFilter":
            $id = $post_data['id'];
            $delete_id = array("id" => $id);
            $db->delete("tbfilteredCategory", $delete_id);
            break;
        case "saveCustomView":
            $customView = $post_data['customView'];
            $form_id = $post_data['form_id'];
            $forms = $db->query("SELECT form_json FROM tb_workspace WHERE id = {$db->escape($form_id)} AND is_delete = 0", "row");
            $form_json = json_decode($forms['form_json']);

            $fields_headerObjType_allowReorder = $form_json->{'form_json'}->{'allowReorder'};

            if ($fields_headerObjType_allowReorder != "") {
                if ($fields_headerObjType_allowReorder == "0") {
                    // return true;
                }
            }
            $fields = array("user_id" => $user_id,
                "form_id" => $form_id,
                "json" => $customView);
            $count = $db->query("SELECT * FROM tbcustomview WHERE user_id = $user_id AND form_id = $form_id ", "numrows");
            if ($count > 0) {
                $where = array("form_id" => $form_id,
                    "user_id" => $user_id);
                $db->update("tbcustomview", $fields, $where);
            } else {
                $db->insert("tbcustomview", $fields);
            }
            break;
        case "getCustomizeReportDataTableOld":
//            $search_value = trim($post_data['search_value']);
//            $start = $post_data['iDisplayStart'];
//            $reports = $search->getPrintReports("WHERE form_name LIKE '%" . $search_value ."%' AND company_id={$db->escape($auth['company_id'])} LIMIT $start ,10");
//            $countReports = count($search->getPrintReports("WHERE form_name LIKE '%" . $search_value ."%' AND company_id={$db->escape($auth['company_id'])}"));
//            $output = array(
//              "sEcho" => intval($post_data['sEcho']),
//              "iTotalRecords" => $countReports,
//              "iTotalDisplayRecords" => $countReports,
//              "start"=>$start,
//              "aaData" => array(),
//          );
//          $reports = json_decode(json_encode($reports),true);
//          foreach ($reports as $value) {
//              $button = "";
//              $pushArray = array();
//
//              $button = '<a href="/user_view/generate?view_type=update&formID=' . $value['id']. '&printID=' . $value['form_id'].'"><i class="fa fa-pencil-square-o tip" data-placement="top" ></i></a>
//          <a><i class="fa fa-trash-o tip delete_printReport" data-placement="top" data-original-title="Delete" data-status="' . $value['is_active'] . '" data-placement="top" id="" data-id="' . $value['id'] . '"></i></a>';
//              // $button = '<a><i class="fa fa-globe tip" data-placement="top" data-original-title="Generate" link-type="report-generation" data-report-id="' . $value['id'] . '"></i></a>
//          // <a><i class="fa fa-trash-o tip update_status_report" data-placement="top" data-original-title="Delete" data-status="' . $value['is_active'] . '" data-placement="top" id="" data-id="' . $value['id'] . '"></i></a>';
//
//              $pushArray[] = $button;
//              $pushArray[] = "<div class='fl-table-ellip'>" . $value['form_name']."</div>";
//              $pushArray[] = "<div class='fl-table-ellip'>" . $value['form_description']."</div>";
//              $pushArray[] = "<div class='fl-table-ellip'>" . $fs->getStatus($value['is_active'])."</div>";
//              $pushArray[] = "<div class='fl-table-ellip'>" . $value['date_created']."</div>";
//              $pushArray[] = "<div class='fl-table-ellip'>" . $value['date_updated']."</div>";
//              $output['aaData'][] = $pushArray;
//          }
//            print_r(json_encode($output));
            break;
        case "getCustomizeReportDataTable":
            $search_value = trim($post_data['search_value']);
            $start = $post_data['iDisplayStart'];
            $limit = $post_data['limit'];
            $json = array("column-sort" => $post_data['column-sort'], "column-sort-type" => $post_data['column-sort-type'], "endlimit" => $limit);
            $search_value = $db->addslash_escape(mysql_escape_string($search_value));
            $result2 = $search->getGenerate($search_value, $start, $json, "array");
            $countForm = $search->getGenerate($search_value, $start, $json, "numrows")? : 0;
            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countForm,
                "iTotalDisplayRecords" => $countForm,
                "start" => $start,
                "aaData" => array(),
            );
            foreach ($result2 as $value) {
                $button = "";
                $pushArray = array();

                $button = '<a class="tip" title="Edit" href="/user_view/generate?view_type=update&formID=' . $value['generate_id'] . '&printID=' . $value['form_id'] . '"><i class="fa fa-pencil-square-o tip" data-placement="top" ></i></a>
                                    <a><i class="fa fa-trash-o tip delete_printReport" data-placement="top" data-original-title="Delete" data-status="' . $value['generate_active'] . '" data-placement="top" id="" data-id="' . $value['generate_id'] . '"></i></a>';
                $pushArray[] = $button;
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['form_name'] . "'>" . $value['form_name'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['form_description'] . "'>" . $value['form_description'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' >" . $fs->getStatus($value['generate_active']) . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['generate_date_created'] . "'>" . $value['generate_date_created'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip' title='" . $value['generate_date_updated'] . "'>" . $value['generate_date_updated'] . "</div>";
                $output['aaData'][] = $pushArray;
            }
            echo json_encode($output);
            break;
        case "getLeftBar":
            // $getModules = $search->getModules(" AND form_display_type = 1 ");
            // $getModules = $search->getModules(" AND
            //                                 tbw.form_display_type = 1");
            $getModules = $search->getModules("");
            $temp_formula = "";
            $temp_formula_replaced = "";
            $temp_formula_evaluated = "";
            $nav_type_ext = $post_data['nav_type_ext'];
            //loop forms here
            foreach ($getModules as $key_category => $value_category) {
                ?>
                <li class="fl-toggle-sub<?= $nav_type_ext; ?>" title="<?= $key_category; ?>"><a href="#"><i class="fa fa-folder-o"></i> &nbsp; <?php echo $key_category; ?> </a>
                    <ul class="main-submenu<?= $nav_type_ext; ?>">
                        <?php
                        foreach ($value_category as $key => $value) {
                            ?>
                            <li title="<?= $value['form_name']; ?>"><a href="/user_view/application?id=<?= functions::base_encode_decode("encrypt", $value['form_id']); ?>"><i class="fa fa-file-text-o"></i>  <?= $value['form_name']; ?></a></li>
                            <?php
                        }
                        ?>
                    </ul>
                </li>
                <?php
            }
            break;
        case "getOldLeftBar":
            $ret = "";
            $company_id = $auth['company_id'];
            $getcategory = $db->query("SELECT * FROM tbform_category WHERE company_id = $company_id", "array");
            $sql = "";
            if (ALLOW_GUEST == "1") {
                $return = "";
                $guest = $db->query("SELECT * FROM tb_guest WHERE guest_user_id={$db->escape($auth['id'])}", "array");

                foreach ($guest as $data) {
                    $return .= $data['guest_formID'] . ",";
                }
                $return = substr_replace($return, "", -1);

                $sql = " AND wf.form_id IN ($return) ";
            }

            array_push($getcategory, array("id" => "0", "category_name", "Others"));
            $a = "SELECT ws.form_name as form_name, ws.id as id, ws.form_json as form_json, ws.form_authors, ws.form_viewers 
                                    FROM tb_workspace ws
                                    LEFT JOIN tbworkflow wf on ws.id = wf.form_id
                                    WHERE wf.is_active = 1 and ws.is_active = 1
                                    $sql
                                    AND company_id = {$db->escape($auth[company_id])} 
                                    ORDER BY ws.form_name DESC";
            $forms_array = $db->query("SELECT ws.form_name as form_name, ws.id as id, ws.form_json as form_json, ws.form_authors, ws.form_viewers 
                                    FROM tb_workspace ws
                                    LEFT JOIN tbworkflow wf on ws.id = wf.form_id
                                    WHERE wf.is_active = 1 and ws.is_active = 1
                                    $sql
                                    AND company_id = {$db->escape($auth[company_id])} 
                                    ORDER BY ws.form_name DESC", "array");
            $array_categoryID = array();
            foreach ($forms_array as $forms) {
                $json = json_decode($forms['form_json'], true);
                $formID = $forms['id'];
                $formName = $forms['form_name'];
                // $forms = $json['form_json'];
                $categoryID = $json['categoryName'];
                // if($json['form_json']['form-users']!=null){
                //   if($search->getFormPrivacyUsers(json_encode($json['form_json']['form-users']))==false){
                //     continue;
                //   }
                // }
                $result = $search->getManyRequest("", 0, $formID, 0, 1, array());
                $countReq = count($result);
                if ($auth['user_level_id'] != "4") {
                    if (!$search->getFormPrivacyUsers($forms['form_authors']) && !$search->getFormPrivacyUsers($forms['form_viewers']) && $countReq == 0) {
                        continue;
                    }
                }
                array_push($array_categoryID, $categoryID);
            }

            foreach ($getcategory as $value) {
                $category_name = $value['category_name'];
                $category_id = $value['id'];
                if ($category_id == 0) {
                    $category_name = "Others";
                }
                //if($auth['user_level_id'] != "4"){
                if (!in_array($value['id'], $array_categoryID)) {
                    continue;
                }
                if ($value['users'] != "") {
                    //search if user in form category
                    if ($search->getFormPrivacyUsers($value['users']) == false) {
                        //continue;
                    }
                }
                //}


                $ret .= '<li class="fl-toggle-sub"><a href="#"><i class="fa fa-plus-square"></i> &nbsp; ' . $category_name . ' </a>';
                $ret .= '<ul class="main-submenu">';

                foreach ($forms_array as $forms) {
                    $json = json_decode($forms['form_json'], true);
                    $formID = $forms['id'];
                    $formName = $forms['form_name'];
                    // $forms = $json['form_json'];
                    $categoryID = $json['categoryName'];
                    $form_json = json_decode($json['form_json']);
                    // segregate forms per form category
                    if ($categoryID == $category_id) {

                        $ret .= '<li><a href="/user_view/application?id=' . functions::base_encode_decode("encrypt", $formID) . '"><i class="fa fa-file-text-o"></i> ' . $formName . '</a></li>';
                    }
                }

                $ret .= '</ul>';
                $ret .= '</li>';
            }
            //setcookie('MODULESCOOKIES',serialize($getcategory),0,'/',COOKIE_URL);
            echo $ret;
            break;

        case "getKeywordDataTable":

            $search_value = trim($post_data['search_value']);
            $start = $post_data['iDisplayStart'];
            $limit = $post_data['limit'];
            $json = array("column-sort" => $post_data['column-sort'], "column-sort-type" => $post_data['column-sort-type'], "endLimit" => $limit);
            $search_value = $db->addslash_escape(mysql_escape_string($search_value));
            $result2 = $search->getKeywordsList($search_value, $start, $json, "array");
            // $result = $search->getForms($search_value,$start,$json);
            // $result2 = json_decode(setAdminViewer($result),true);
            // $countForm = $search->getForms($search_value,"");
            // $countForm = count(json_decode(setAdminViewer($countForm),true));
            $countForm = $search->getKeywordsList($search_value, $start, $json, "numrows");
            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countForm,
                "iTotalDisplayRecords" => $countForm,
                "start" => $start,
                "aaData" => array(),
            );

            foreach ($result2 as $value) {
                $value['wsID'] = $value['form_id'];
                $button = "";
                $pushArray = array();
                $button = '<a class="tip cursor" data-original-title="Edit" data-placement="top" id="" data-keyword-id="' . $value['id'] . '" onclick="keyword.showDialog(' . $value['id'] . ')" ><i class="fa fa-pencil-square-o" data-placement="top" data-original-title="Edit"></i></a>' .
                        '<a class="tip cursor keyword_update" data-status="' . $value['is_active'] . '" data-original-title="' . $fs->getStatus_revert($value['is_active']) . '" data-placement="top" id="" data-keyword-id="' . $value['id'] . '" ><i class="fa fa-check-circle-o" data-placement="top" data-original-title="Deactivate"></i></a>' .
                        '<a class="tip cursor keyword_delete" data-status="" data-original-title="Delete" data-placement="top" id="" data-keyword-id="' . $value['id'] . '" ><i class="fa fa-trash-o" data-placement="top" data-original-title="Delete"></i></a>';
                $pushArray[] = $button;
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['code'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['description'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $fs->getStatus($value['is_active']) . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['date_created'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['date_updated'] . "</div>";
                // $pushArray[] = "<div class='fl-table-ellip'>" . $value['form_date_created']."</div>";
                // $pushArray[] = "<div class='fl-table-ellip'>" . $value['form_date_updated']."</div>";
                //workflow
                // $workflow = "<a href='/user_view/workflow?view_type=edit&form_id=" . $value['wsID'] . "&id=" . $value['workflow_id'] . "'>View Workflow</a>";
                // if ($value['workflow_id'] == 0) {
                //     //$workflow = "<font color='#cccccc'>No Active Workflow</font>";
                //     $workflow = functions::empty_in_list("", "No Active Workflow");
                // }
                // $pushArray[] = "<div class='fl-table-ellip'>" . $workflow . "</div>";
                $output['aaData'][] = $pushArray;
            }
            echo json_encode($output);
            break;
        case "getDevForms":
            $form_id = $post_data['form_id'];
            $start = $post_data['iDisplayStart'];

            $column_sort = $post_data['column-sort'];
            if ($column_sort == "author") {
                //for author
                $column_sort = 'tbu_created_by.display_name';
            }

            $json = array("column-sort" => $column_sort, "column-sort-type" => $post_data['column-sort-type'], "other_condition" => " AND tbdf.parent_id = '" . $form_id . "' AND tbw.is_active = '1'");
            $result2 = $search->getFormsV2($search_value, $start, $json, "array");
            $countForm = $search->getFormsV2($search_value, $start, $json, "numrows");
            $output = array(
                "sEcho" => intval($post_data['sEcho']),
                "iTotalRecords" => $countForm,
                "iTotalDisplayRecords" => $countForm,
                "start" => $start,
                "aaData" => array(),
            );
            foreach ($result2 as $value) {
                $pushArray = array();
                $button = "<ul class='fl-table-actions'>";
                $button .= '<li><a class="tip cursor reflect_staging" data-placement="top" id="" form_id="' . $value['form_id'] . '" title="Reflect"><i class="fa fa-check-circle-o" data-placement="top"></i></a></li>';
                $button .= "</ul>";
                $pushArray[] = $button;
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['form_name'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['created_by_name'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['form_date_created'] . "</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['form_date_updated'] . "</div>";


                $output['aaData'][] = $pushArray;
            }
            echo json_encode($output);
            break;
        case "getERPLeftBar":
            // $getModules = $search->getModules(" AND form_display_type = 1 ");
            $getModules = $search->getModules("");
            $temp_formula = "";
            $temp_formula_replaced = "";
            $temp_formula_evaluated = "";
            $nav_type_ext = $post_data['nav_type_ext'];
            //loop forms here
            foreach ($getModules as $key_category => $value_category) {
                ?>
                <li class="fl-toggle-sub<?= $nav_type_ext; ?>" title="<?= $key_category; ?>"><a href="#"><i class="fa fa-folder-o"></i> &nbsp; <?php echo $key_category; ?> </a>
                    <ul class="main-submenu<?= $nav_type_ext; ?>">
                        <?php
                        foreach ($value_category as $key => $value) {
                            ?>
                            <li title="<?= $value['form_name']; ?>"><a href="/user_view/application?id=<?= functions::base_encode_decode("encrypt", $value['form_id']); ?>"><i class="fa fa-file-text-o"></i>  <?= $value['form_name'] . " - ERP "; ?></a></li>
                                <?php
                            }
                            ?>
                    </ul>
                </li>
                <?php
            }
            break;
        case "getValidFilterFields";
            $form_id = $post_data['form_id'];
            $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($form_id)} AND is_delete = 0", "row");
            $activeFields = explode(",", $forms['active_fields']);
            $form_json = json_decode($forms['form_json']);

            // $activeFields = explode(",",implode(",", $activeFields));
            $getFieldsLabel = $fs->getFields("WHERE form_id = " . $form_id . " AND field_type NOT IN(" . $search->searchFieldNotIncluded() . ") ORDER BY data_field_label ASC");
            $field = array(
                    // array("value"=>"TrackNo","text"=>"Tracking Number"),
                    // array("value"=>"Requestor","text"=>"Requestor"),
                    // // array("value"=>"Processor","text"=>"Processor"),
                    // array("value"=>"Status","text"=>"Status"),
            );
            if (count($getFieldsLabel) > 0) {
                foreach ($getFieldsLabel as $value) {
                    $label = $fs->removeSpecialChars($value->label); //preg_replace('/[^A-Za-z0-9\. -]/', '', $getFieldsLabel[0]->label);
                    // echo "<option value='" . $value->name . "' field_input_type='". $value->input_type ."' data-type='". $value->type ."'>" . $label . "</option>";
                    array_push($field, array("value" => $value->name, "field_input_type" => $value->input_type, "type" => $value->type, "text" => $label));
                }
            } else {
                foreach ($activeFields as $value) {
                    // echo "<option value='" . $value . "'>" . $value . "</option>";
                    array_push($field, $value);
                }
            }
            $strSql = "SELECT DISTINCT COLUMN_NAME FROM information_schema.columns where table_name = '" . $forms['form_table_name'] . "' AND (DATA_TYPE = 'date' OR DATA_TYPE = 'datetime')";

            $getdateFields = $db->query($strSql, "array");
            $dateFields = array();
            foreach ($getdateFields as $value) {
                if (in_array($value['COLUMN_NAME'], $activeFields)) {
                    // echo $value['COLUMN_NAME'];
                    $getFieldsLabel = $fs->getFields("WHERE form_id = " . $form_id . " AND field_name='" . $value['COLUMN_NAME'] . "'");

                    if (count($getFieldsLabel) > 0) {
                        $label = $fs->removeSpecialChars($getFieldsLabel[0]->label); //preg_replace('/[^A-Za-z0-9\. -]/', '', $getFieldsLabel[0]->label);
                        //array_push($dateFields, $label);
                        $dateFields[] = array("text" => $label, "value" => $value['COLUMN_NAME']);
                    } else {
                        $dateFields[] = array("text" => $value['COLUMN_NAME'], "value" => $value['COLUMN_NAME']);
                    }
                }
            }
            // $dateFields[] = array("text"=>"Date Created","value"=>"DateCreated");
            $return_arr = array("fields" => $field, "dateFields" => $dateFields);
            echo json_encode($return_arr);
            break;
    }
}
?>
