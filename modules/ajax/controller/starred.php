<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$search = new Search();
$starred = new Starred($db);
$userID = $auth['id'];
$redis_cache = getRedisConnection();
 if(isset($_POST['action'])){

     $date = $fs->currentDateTime();

     switch($_POST['action']){
        case "loadData":
              //  $returnJson = array();
              //  $forms_array = $search->getCompaniesForm();
              //  array_unshift($forms_array, array("id"=>"0","form_name"=>"Announcement"));
              //  $start = $_POST['start'];
              //  foreach ($forms_array as $forms) {
              //     $id =  $forms['id'];
              //     $request_array = $search->getStarredRequests($id,$start,10);
              //     foreach ($request_array as $request) {
              //       array_push($request, $forms);
                    // array_push($request, array("user_image"=>post::avatarPic("tbuser",$request['requestor_id'],"30","30","small","avatar")));
              //       array_push($returnJson, $request);
              //     }
              // }
              // echo json_encode($returnJson);
              // $starred->getStarred();
              $search_value = $_POST['search_value'];
              $start = $_POST['start'];
              $view_type = $_POST['view_type'];

              if($redis_cache){

                /*
                    Cache Format
                            1. User Id.
                            2. Page Number.
                            3. Page Limit.
                            4. Search Filter.
                            5. Column Sort.
                            6. Type of Sort.
                */

                $cachedFormat = "$userID::$view_type::$start";

                $cache_starred_list = json_decode($redis_cache->get("starred_list"),true);

                $cached_query_result = $cache_starred_list[''. $cachedFormat .''];

                if($cached_query_result){
                    // echo "from cache";

                    $data = $cached_query_result;
                }else{
                    // echo "from db";
                    
                    $data = $starred->getStarred($search_value,"LIMIT $start, 20");

                    $cache_starred_list[''. $cachedFormat .''] = $data;
                    $redis_cache->set("starred_list", json_encode($cache_starred_list));
                }
            }else{
                $data = $starred->getStarred($search_value,"LIMIT $start, 20");
            }
              
              echo json_encode($data);
            break;
        case "countData":
            // $ctr = 0;
            // $forms_array = $search->getCompaniesForm();

            // $start = $_POST['start'];
            // array_unshift($forms_array, array("id"=>"0","form_name"=>"Announcement"));  
            // foreach ($forms_array as $forms) {
            //     $id =  $forms['id'];
            //     $request_array = $search->countStarredRequests($id);
            //     $ctr+=$request_array;
            //     //print_r($request_array);
            // }
            $data = $starred->getStarred("","");
            $ctr = count($data);
            echo $ctr;
          break;
        case "updateData":
          $form_id = $_POST['form_id'];
            //UPDATED STARRED HERE
            $data_starred = $_POST['data_starred'];
            $data_id = $_POST['data_id'];
			$tablename = $_POST['tablename'];
			
            if($data_starred==0){
                //insert              
              $strSqlCheckExist = "SELECT * FROM tbstarred WHERE tablename = '". $tablename ."' AND data_id = '". $data_id ."' AND  user_id = '". $auth['id'] ."'";
              
              if($db->query($strSqlCheckExist,"numrows")==0){
                $insert = array("tablename"=>$tablename,
                            "data_id"=>$data_id,
                            "user_id"=>$auth['id'],
                            "datetime"=>$date);
                $id = $db->insert("tbstarred",$insert);
                $json = array("status"=>"added",
                    "message"=>"Added to your starred","type"=>"success","id"=>$id);
              }
            }else{
                //delete
                $where = array("id"=>$data_starred);
                $db->delete("tbstarred",$where);
                $json = array("status"=>"removed",
                    "message"=>"Removed to your starred","type"=>"success","id"=>$data_starred);
                //echo $data_starred;
            }
			//	added by ervinne: updates the date updated of the request after marking it as starred/unstarred
			$db->update($tablename, array("DateUpdated" => $date), array("ID" => $data_id));

              
              $starred_list = array("starred_list");
              $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);

              $fs->deleteMemcacheForm($request_record_memcacached, $form_id);
              $fs->deleteMemcacheKeys($starred_list);
            echo json_encode($json);
            break;
     }
    
 }
Class Starred extends Auth{
  /*Properies*/
  public $db = "";
  public $auth = "";


  // /* Functions/Methods */

  public function __construct($database){
    $this->db = $database;
    $this->auth = $this->getAuth('current_user');
  }


  public function getStarred($search_value,$limit){
    $userId = $this->auth['id'];
    $queryStr = "SELECT * FROM tbstarred WHERE user_id = $userId AND type = '0' ORDER BY id DESC, tablename ".$limit;
    $getStarred = $this->db->query($queryStr,"array");
    $returnArray = array();
    $post = new post();
    $functions = new functions();
    $otherUser = "";
    $data_id = "";
    foreach ($getStarred as $value) {
      if($value['type']==0){
        $strSqlData = "SELECT frm.*,u.first_name,u.last_name,u.display_name,u.middle_name, ws.id as ws_id, ws.form_name, ws.form_json FROM ".$value['tablename']." 
                      frm LEFT JOIN tbuser u ON frm.Requestor=u.id , tb_workspace ws WHERE frm.id = '".$value['data_id']."'  
                      AND ws.form_table_name='".$value['tablename']."' AND ws.is_delete = 0 AND (u.first_name LIKE '%". $search_value ."%' OR u.last_name LIKE '%". $search_value ."%' 
                      OR CONCAT_WS(' ',u.first_name,last_name) LIKE '%". $search_value ."%') 
                      AND (NOT EXISTS(SELECT * FROM tbtrash_bin WHERE record_id = frm.ID AND table_name='". $value['tablename'] ."'))";
        // echo $strSqlData."\n";
        $otherUser = "Requestor";
        $getData = $this->db->query($strSqlData,"row");
        $data_id = $getData['ID'];
      }else if($value['type']==1){
        $strSqlData = "SELECT *,frm.id as data_id FROM ".$value['tablename']." frm LEFT JOIN tbuser u ON frm.postedBy = u.id WHERE frm.id = ".$value['data_id']." AND (u.first_name LIKE '%". $search_value ."%'
                      OR u.last_name LIKE '%". $search_value ."%' 
                      OR CONCAT_WS(' ',u.first_name,last_name) LIKE '%". $search_value ."%')";
        $otherUser = "postedBy";
        $getData = $this->db->query($strSqlData,"row");
        if($getData){
          $data_id = $getData['data_id'];
          $getData['form_name'] = "Announcement";
          $getData['DateCreated'] = $getData['date_posted'];
          $getData['link'] = $functions->base_encode_decode("encrypt",$value['data_id']);
        }
      }
      
      if($getData){
        $getData['type'] = $value['type'];
        $getData['starred_id'] = $value['id'];
        $getData['datetime'] = $value['datetime'];
        $getData['data_id'] = $data_id;
        $getData['user_image'] = $post->avatarPic("tbuser",$getData[$otherUser],"30","30","small","avatar");
        array_push($returnArray, $getData);
      }
    }

    return $returnArray;  
  }

}
?>
