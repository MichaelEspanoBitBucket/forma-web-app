<?php
$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$userCompany = new userQueries();
$upload = new upload();
$timezone = "Asia/Manila";
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

    if(isset($_POST['action'])){
        if($_POST['action'] == "load_claim_request"){
            $form_id = $_POST['form_id'];
            $claim_val = $_POST['claim_val'];
            $policy_val = $_POST['policy_val'];
            $look_up = '20_tbl_claimsubrequest';
            
            $return = array();
            
            $form = $db->query("SELECT * FROM tb_workspace WHERE id={$db->escape($form_id)} AND is_active={$db->escape(1)} AND is_delete = 0","row");
            $request = $db->query("SELECT * FROM " . $form['form_table_name'] . " cl 
                                  LEFT JOIN " . $look_up . " cls on cl.CLAIM=cls.ClaimTrackNo WHERE cl.CLAIM={$db->escape($claim_val)} AND cl.CHDRNUM={$db->escape($policy_val)}","array");
            
            foreach($request as $data){
                $return[] = array("data"    =>  $data);
            }
            
            $res = json_encode($return);
            echo $res;
        }
    }


?>