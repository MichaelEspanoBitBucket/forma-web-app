<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}
$auth = Auth::getAuth('current_user');
$db = new Database();
$fs = new functions();
$userCompany = new userQueries();
$search = new Search();
$timezone = "Asia/Manila";
$company_id = $auth['company_id'];
if(function_exists('date_default_timezone_set')) date_default_timezone_set($timezone);

    if(isset($_POST['action'])){
        $action = $_POST['action'];
        $date = $fs->currentDateTime();
        
            // Get Contacts
            if($action=="addSuggestion"){
                $title = $_POST['title'];
                $message = $_POST['message'];
                $include_suggested_by = $_POST['include_suggested_by'];
                $insert_array = array("company_id"=>$auth['company_id'],
                                    "title"=>$title,
                                    "message"=>$message,
                                    "datetime"=>$date,
                                    "userID"=>$auth['id'],
                                    "is_active"=>1,
                                    "include_suggested_by"=>$include_suggested_by);
                echo $db->insert("tbsuggestion",$insert_array);
            }else if($action=="loadSuggestion"){
                $res = "";
                $out = "";
                $fields_out = "";
                $company_user_info = array();
                $fields = array(); 
                
                //$companyUsers_fields = $db->query("SHOW COLUMNS FROM tbuser");
                $company = $userCompany->getCompany($auth['company_id']);
                $suggestion_sql = $db->query("SELECT * FROM  `tbsuggestion` WHERE company_id = {$db->escape($company_id)} ORDER BY id DESC","array");
                // Table fields
                //foreach($companyUsers as $info){
                //    $company_user_info[] = $info;
                //}
                foreach($suggestion_sql as $info){
                  // Actions
                  $actions = '<center><i class="fa fa-check tip cursor doneFullSuggestion" data-placement="top" data-original-title="Delete" data-id="'. $info['id'] .'"></i></center>';
                    
                  $company_user_info[] = array("id"=>$info['id'],
                                              "title"=>"<div class='fl-word-break'>" . $info['title'] . "</div>",
                                              "message"=>$info['message'],
                                              "datetime"=>$info['datetime'],
                                              "status"=>$info['status'],
                                              "actions"=>$actions);
                  }
                  $res = array(/*"sEcho"                        =>      "3",
                               "iTotalRecords"                =>      $countRows,
                               "iTotalDisplayRecords"         =>      $countRows,*/
                               "company_user"                 =>      $company_user_info);
                  $string = json_encode($res);
                  echo functions::view_json_formatter($string);
            }else if($action=="loadSuggestionDatatable"){
              $search_value = $_POST['search_value'];
              $start = $_POST['iDisplayStart'];
              $limit = "";
              $end = "10";
              if($_POST['limit']!=""){
                  $end = $_POST['limit'];
              }
              if($start!=""){
                $limit = " LIMIT $start, $end";
              } 
              $orderBy = " ORDER BY id DESC";
              if($_POST['column-sort']){
                $column = "tbs.".$_POST['column-sort'];
                $orderBy = " ORDER BY ".$_POST['column-sort']." ".$_POST['column-sort-type'];
                if($_POST['column-sort']=="display_name"){
                  $column = "tbu.display_name";
                  $orderBy = " ORDER BY tbs.include_suggested_by ". $_POST['column-sort-type']  .", ".$column." ".$_POST['column-sort-type'];
                }
              }
              // $query = "SELECT * FROM  `tbsuggestion` WHERE company_id = {$db->escape($company_id)} AND (title LIKE '%". $search_value ."%' OR message LIKE '%". $search_value ."%' OR status LIKE '%". $search_value ."%' OR datetime LIKE '%". $search_value ."%') $orderBy ";
              $query = "SELECT tbs.*, tbu.display_name FROM  `tbsuggestion` tbs LEFT JOIN tbuser tbu ON tbs.userID = tbu.id WHERE tbs.company_id = {$db->escape($company_id)} AND (tbs.title LIKE '%". $search_value ."%' OR tbs.message LIKE '%". $search_value ."%' OR tbs.status LIKE '%". $search_value ."%' OR tbs.datetime LIKE '%". $search_value ."%' OR (tbu.display_name LIKE '%". $search_value ."%' AND tbs.include_suggested_by =1)) $orderBy ";
              $result = $db->query($query.$limit,"array");
              $countResult = $db->query($query,"numrows");
              $output = array(
                "sEcho" => intval($_POST['sEcho']),
                "iTotalRecords" => $countResult,
                "iTotalDisplayRecords" => $countResult,
                "start"=>$start,
                "aaData" => array(),
              );
              foreach ($result as $value) {
                $button = "";
                $pushArray = array();
                $button = '<center><i class="fa fa-check tip cursor doneFullSuggestion" data-placement="top" data-original-title="Delete" data-id="'. $info['id'] .'"></i></center>';
                // $pushArray[] = $button;
                $suggested_by = "<span style='color:#C5C5C5'>Anonymous</span>";

                if($value['include_suggested_by']==1){
                  $suggested_by = $value['display_name'];
                }

                $pushArray[] = "<div class=''>" . $suggested_by ."</div>";
                $pushArray[] = "<div class=''>" . $value['title']."</div>";
                $pushArray[] = "<div class=''>" . $value['message']."</div>";
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['datetime']."</div>";
                // $pushArray[] = "<div class='fl-table-ellip'>" . $fs->getStatus($value['status'])."</div>"; //removed the function getStatus, the args must be int not string because 'not active' will always return in the functions
                $pushArray[] = "<div class='fl-table-ellip'>" . $value['status'] ."</div>";
                $output['aaData'][] = $pushArray;
              }
              echo json_encode($output);
            }
    }


?>