<?php
$db = new Database;
$fs = new functions;
$auth = Auth::getAuth('current_user');
$date = $fs->currentDateTime();
$mail = new Mail_Notification();
$return = array();
if(isset($_POST['action'])){
    
    if($_POST['action'] == "load_tag"){
        $query = $db->query("SELECT * FROM 1_tbl_slotform","array");
        foreach($query as $tags){
            $return[] = array(
                        "tag_id"    =>  $tags['tag_id'],
                        "tag_customer"  =>  $tags['customer'],
                        "tag_slot_width"     =>  $tags['width'],
                        "tag_length"    =>  $tags['length'],
                        "tag_slot_height"    =>  $tags['height'],
                        "tag_container_number"   =>  $tags['referencenumber'],
                        "tag_desciption"       =>  $tags['description'],
                        "tag_type"      =>  $tags['type'],
                        "tag_class"     =>  $tags['class'],
                        "tag_time_storage"  =>  $tags['time_storage'],
                        "tag_date_storage" =>  $tags['start_storage'],
                        "tag_height"   =>  $tags['slot_height'],
                        "tag_width"    =>  $tags['slot_width'],
                        "tag_top"       =>  $tags['top'],
                        "tag_left"      =>  $tags['left']    
                            );
         
        }
        $res = array("get_query" => $return);
        $string = json_encode($res);
        echo $string;
        //echo Functions::view_json_formatter($string);
    }
    
}

?>