<?php

$fs = new functions;
//error_reporting(E_ALL);

//var_dump(PDO::getAvailableDrivers());
//return;
$conn = new OracleDatabase("122.49.216.199", "xe", "TEST_AARON", "password");

//select sample
$result = $conn->query("SELECT * FROM EMP");
var_dump($result);
return;
//insert sample
$fields = array("ID" => "5", "Name" => "HR Admin");
$fields_1 = array("ID" => "6", "Name" => "Systems Analyst");

$conn->beginTransaction();
$insert_result = $conn->insert("POSITIONS", $fields);
$insert_result_1 = $conn->insert("POSITIONS", $fields_1);

if ($insert_result && $insert_result_1) {
    $conn->commit();
} else {
    $conn->rollBack();
}

//update sample
$fields_update = array("Name" => "Application Developer");
$condition_array = array("ID" => "2");

$fields_update_1 = array("Name" => "Project Manager");
$condition_array_1 = array("ID" => "3");

$conn->beginTransaction();
$update_result = $conn->update("POSITIONS", $fields_update, $condition_array);
$update_result_1 = $conn->update("POSITIONS", $fields_update_1, $condition_array_1);

if ($update_result && $update_result_1) {
    $conn->commit();
} else {
    $conn->rollBack();
}

//delete sample
$delete_condition_array = array("Name" => "Application Developer");
$conn->beginTransaction();
$delete_result = $conn->delete("POSITIONS", $delete_condition_array);
if ($delete_result) {
    $conn->commit();
} else {
    $conn->rollBack();
}


$conn->disConnect();
unset($conn);
