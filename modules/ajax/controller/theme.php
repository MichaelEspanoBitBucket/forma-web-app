<?php
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$search = new Search();
$user_id = $auth['id'];
if(isset($_POST['action'])){
	if($_POST['action']=="setTheme"){
		$category_id = $_COOKIE['application'];
		$theme = $_POST['theme'];
		$getTheme = $fs->getDashboardTheme($category_id);

		//insert / update array
		$insertTheme = array("theme"=>$theme,
						"category_id"=>$category_id,
						"user_id"=>$auth['id']);

		if(count($getTheme)=="0"){
			//insert
			$db->insert("tbdashboad_theme",$insertTheme);
		}else{
			//update
			$whereTheme = array("category_id"=>$category_id,
						"user_id"=>$auth['id']);
			
			$db->update("tbdashboad_theme",$insertTheme,$whereTheme);
		}

		$themeStyle = json_decode($theme,true);
		include("layout/app_theme_color.phtml");
		$redis_cache = getRedisConnection();
		//reset nav of user
		if($redis_cache){
		    

		    $cachedFormat = "$user_id";

		    $cache_user_theme = json_decode($redis_cache->get("user_theme"),true);

		    $cached_query_result = $cache_user_theme[''. $cachedFormat .''];

		    if($cached_query_result){
		        unset($cache_user_theme[''. $cachedFormat .'']);
		        $redis_cache->set("user_theme", json_encode($cache_user_theme));
		    }
		}

		//insert new
		
		// $db->insert("tbdashboad_theme",$insertTheme);

		// $db->update("tbdashboad_theme",$insertTheme,$deleteTheme);
	}
}
?>
