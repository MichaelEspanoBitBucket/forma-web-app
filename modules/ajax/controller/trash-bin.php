<?php

//check if session is alive if not.. retun false;
if (!Auth::hasAuth('current_user')) {
    return false;
}


if (ENABLE_DELETION == "0") {
    return false;
}
$fs = new functions;
$auth = Auth::getAuth('current_user');
$trashBin = new TrashBin();
$db = new Database();
$redis_cache = getRedisConnection();

if (isset($_POST['action'])) {

    $action = $_POST['action'];
    $date = $fs->currentDateTime();
    if ($action == "loadTrashRecord") {
        $search_value = trim($_POST['search_value']);
        $start = $_POST['iDisplayStart'];
        $end = $_POST['limit'];
        $sort_column = $_POST['column-sort'];
        $sort_type = $_POST['column-sort-type'];
        // $condition = " tbtb.user_id = '". $auth['id'] ."' AND (tbtb.record_id LIKE '%". $search_value ."%' or tbtb.date_time LIKE '%". $search_value ."%' or tbw.form_name LIKE '%". $search_value ."%'". TrashBin::record_type($search_value) .")";
        $json = array("column-sort" => $_POST['column-sort'], "column-sort-type" => $_POST['column-sort-type'],
            "start" => $start, "end" => $end, "sort_column" => $sort_column,
            "sort_type" => $sort_type);

        // $getTrashRecord = TrashBin::getTrashRecordDataTable($json,"array");
        // $countTrashRecord = TrashBin::getTrashRecordDataTable(array("condition"=>$condition),"numrows");

        $getTrashRecord = $trashBin->getTrashRecordDataTableV2($search_value, "", $json, "array");
        $countTrashRecord = $trashBin->getTrashRecordDataTableV2($search_value, "", $json, "numrows");
		if($countTrashRecord == ""){
			$countTrashRecord = 0;
		}
        $output = array(
            "sEcho" => intval($_POST['sEcho']),
            "iTotalRecords" => $countTrashRecord,
            "iTotalDisplayRecords" => $countTrashRecord,
            "start" => $start,
            "aaData" => array(),
            "getTrashRecord" => $getTrashRecord
        );


        foreach ($getTrashRecord as $data) {
            $info = array();
            $status = $data['is_active'];
            $button = '<ul class="fl-table-actions">';
            $button .= "<li>
                            <input type='checkbox' class='css-checkbox retrieveRecordSingle' 
                                style='margin-top: 5px;margin-right: 10px;' request-id='" . $data['request_id'] . "' table_name='" . $data['table_name'] . "' data-id='" . $data["trash_id"] . "' id='trash_" . $data['trash_id'] . "'>
                            <label for='trash_" . $data['trash_id'] . "' class='css-label' style='margin-top:-5px;margin-left:4px'></label>
                        </li>";
            $button .= '<li><i class="fa tip fa-mail-reply-all retrieveTrash" title="Restore" style="cursor:pointer;" data-id="' . $data["trash_id"] . '" form_id="' . $data["form_id"] . '"></i></li>';
            $button .= '<li><i class="fa tip fa-trash deletTrashPermanentSingle" title="Delete" style="cursor:pointer;" request-id="' . $data['request_id'] . '" table_name="' . $data['table_name'] . '" data-id="' . $data["trash_id"] . '" form_id="' . $data['form_id'] . '"></i></li>';
            $button .= '<ul>';

            // $info[] = "<div class='fl-table-ellip'>" . TrashBin::getRecordTrashType($data['record_type'])."</div>";
            $info[] = "<div class='fl-table-ellip'>" . $button . "</div>";
            $info[] = "<div class='fl-table-ellip'>" . $data['form_name'] . "</div>";
            $info[] = "<div class='fl-table-ellip'>" . $data['TrackNo'] . "</div>";
            $info[] = "<div class='fl-table-ellip'>" . $data['Status'] . "</div>";
            $info[] = "<div class='fl-table-ellip'>" . $data['display_name'] . "</div>";
            $info[] = "<div class='fl-table-ellip'>" . $data['DateCreated'] . "</div>";
            $info[] = "<div class='fl-table-ellip'>" . $data['date_deleted'] . "</div>";


            $output['aaData'][] = $info;
        }

        echo json_encode($output);
    } else if ($action == "deleteRequest") {
        $form_id = $_POST['form_id'];
        $record_id = $_POST['record_id'];
        $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($form_id)} AND is_delete = 0", "row");
        $enable_deletion = $forms['enable_deletion'];
        $getDeleteAccess = $db->query("SELECT FIND_IN_SET({$auth["id"]}, getFormUsers({$form_id},6)) as delete_access_forma", "row");
        $getTrashRecordSql = "SELECT * FROM tbtrash_bin WHERE record_id = '" . $record_id . "' AND form_id = '" . $form_id . "' LIMIT 0,1";
        $getTrashRecord = $db->query($getTrashRecordSql, "array");

        /*
          return 0 = delete access denied
          return 1 = already deleted

         */
        if (count($getTrashRecord) == 0) {
            if ($getDeleteAccess['delete_access_forma'] > 0 && ENABLE_DELETION == "1" && $enable_deletion == "1") {
                $trashBin->deleteRequest($form_id, $record_id);
                $ret = json_encode(array("form_id" => functions::base_encode_decode("encrypt", $form_id)));
                echo $ret;
            } else {
                echo "0";
            }
        } else {
            echo "1";
        }

        if ($redis_cache) {
            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $starred_memcached = array("starred_list");
            $request_memcached = array("form_access_" . $form_id,
                "related_info_form_id_" . $form_id, "request_details_" . $form_id,
                "request_access_" . $form_id, "request_author_access_" . $form_id,
                "workspace_form_details_" . $form_id);


            $deleteMemecachedKeys = array_merge(
                    $starred_memcached, $request_memcached
            );

            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            $fs->deleteMemcacheForm($request_record_memcacached, $form_id);
        }
    } else if ($action == "retrieveRequest") {
        $record_id = $_POST['record_id'];
        $form_id = $_POST['form_id'];
        print_r($trashBin->retrieveRequest($record_id));

        if ($redis_cache) {
            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $starred_memcached = array("starred_list");
            $request_memcached = array("form_access_" . $form_id,
                "related_info_form_id_" . $form_id, "request_details_" . $form_id,
                "request_access_" . $form_id, "request_author_access_" . $form_id,
                "workspace_form_details_" . $form_id);


            $deleteMemecachedKeys = array_merge(
                    $starred_memcached, $request_memcached
            );

            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            $fs->deleteMemcacheForm($request_record_memcacached, $form_id);
        }
    } else if ($action == "retrieveMultiRequest") {
        $retrieveRecordSelected = $_POST['retrieveRecordSelected'];
        print_r($trashBin->retrieveMultiRequest($retrieveRecordSelected));

        //all forms reset
    } else if ($action == "deleteRecordPemanentSingle") { // in trash bin page
        $form_id = $_POST['form_id'];
        $request_id = $_POST['request_id'];
        $table_name = $_POST['table_name'];
        $arrayDelete = array(array("form_id" => $form_id, "record_id" => $request_id, "table_name" => $table_name));
        $trashBin->deletePermanent($arrayDelete);
        if ($redis_cache) {
            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $starred_memcached = array("starred_list");
            $request_memcached = array("form_access_" . $form_id,
                "related_info_form_id_" . $form_id, "request_details_" . $form_id,
                "request_access_" . $form_id, "request_author_access_" . $form_id,
                "workspace_form_details_" . $form_id);


            $deleteMemecachedKeys = array_merge(
                    $starred_memcached, $request_memcached
            );

            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            $fs->deleteMemcacheForm($request_record_memcacached, $form_id);
        }
    } else if ($action == "deleteRecordPemanentMulti") { // in trash bin page
        $retrieveRecordSelected = $_POST['retrieveRecordSelected'];
        $trashBin->deletePermanent($retrieveRecordSelected);
    } else if ($action == "deleteManyRequest") { // in application page
        $records = $_POST['records'];
        $form_id = $_POST['form_id'];
        $forms = $db->query("SELECT * FROM tb_workspace WHERE id = {$db->escape($form_id)} AND is_delete = 0", "row");
        $enable_deletion = $forms['enable_deletion'];
        $getDeleteAccess = $db->query("SELECT FIND_IN_SET({$auth["id"]}, getFormUsers({$form_id},6)) as delete_access_forma", "row");

        if ($getDeleteAccess['delete_access_forma'] > 0 && ENABLE_DELETION == "1" && $enable_deletion == "1") {
            $trashBin->deleteManyRequest($form_id, $records);
            $ret = json_encode(array("form_id" => functions::base_encode_decode("encrypt", $form_id)));
            echo $ret;
        } else {
            echo "0";
        }
    }
}
?>
