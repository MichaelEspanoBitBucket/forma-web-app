<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}

$auth = Auth::getAuth('current_user');
$db = new Database();
$updateStatus = new UpdateStatus();
$id = $_POST['id'];
$status = $_POST['status'];
$fs = new functions;
$date = $fs->currentDateTime();
if (isset($_POST['type'])) {
    switch ($_POST['type']) {
        case 'orgchart':
            $updateStatus->deactivateOrgchart();
            if ($status == "0") { //orgchart activation
                //deactivate old orgchart
                $updateStatus->deactivateOrgchart();

                //activate orgchart
                $updateStatus->activateOrgchart($id);
            } else { //orgchart deactivation
                $updateStatus->deactivateOrgchart();
            }

            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $nav_memcached = array("leftbar_nav");
            $orgchart_record_memcacached = array("orgchart_list","orgchart_count");
            $starred_memcached = array("starred_list");

            $deleteMemecachedKeys = array_merge($request_record_memcacached,
                                                $orgchart_record_memcacached,
                                                $nav_memcached,
                                                $starred_memcached
                                              );
            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            break;
        case 'form':
            if ($status == "0") { //form activation
                $updateStatus->activateForm($id);
            } else { //form deactivation
                $updateStatus->deactivateForm($id);
            }

            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $form_record_memcached = array("form_list","form_count");
            $nav_memcached = array("leftbar_nav");
            $starred_memcached = array("starred_list");

            $deleteMemecachedKeys = array_merge($form_record_memcached,
                                                $nav_memcached,
                                                $starred_memcached
                                              );
            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            $fs->deleteMemcacheForm($request_record_memcacached,$id);
            break;
        case 'workflow':

            if ($status == "0") { //workflow activation
                //deactivate old workflow
                $updateStatus->deactivateWorkflow($id);
                //activate new workflow
                $updateStatus->activateWorkflow($id);
            } else { //workflow deactivation
                $updateStatus->deactivateWorkflow($id);
            }

            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $nav_memcached = array("leftbar_nav");
            $starred_memcached = array("starred_list");

            $deleteMemecachedKeys = array_merge($nav_memcached,$starred_memcached
                                              );
            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            $fs->deleteMemcacheForm($request_record_memcacached,$id);
            break;
        case "report":
            if ($status == "0") {
                $reportDoc = new Report($db, $id);
                $reportDoc->is_active = 1;
                $reportDoc->update();
            } else {
                $reportDoc = new Report($db, $id);
                $reportDoc->is_active = 0;
                $reportDoc->update();
            }
            break;
        case 'delete_form' :
            $form = new Form($db, $id);
            // $db->query("DROP TABLE " . $form->form_table_name . "");
            $db->delete("tb_workspace", array("id" => $id));

            //delete form audit
            $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"28",
                                  "table_name"=>"tb_workspace",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

            $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);


            $getWorkflows = $db->query("SELECT * FROM tbworkflow WHERE form_id = $id AND is_delete = 0", "array");
            foreach ($getWorkflows as $value) {
                $db->delete("tbworkflow_objects", array("workflow_id" => $value['id']));
                $db->delete("tbworkflow_lines", array("workflow_id" => $value['id']));

                //delete workflow audit

                $insert_audit_rec = array("user_id"=>$auth['id'],
                                      "audit_action"=>"32",
                                      "table_name"=>"tbworkflow",
                                      "record_id"=>$value['id'],
                                      "date"=>$date,
                                      "ip"=>$_SERVER["REMOTE_ADDR"],
                                      "is_active"=>1);

                $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);
            }
            $db->delete("tbworkflow", array("form_id" => $id));


            


            
            break;
        case "delete_orgchart" :
            $db->delete("tborgchart", array("id" => $id));
            $db->delete("tborgchartobjects", array("orgChart_id" => $id));
            $db->delete("tborgchartline", array("orgChart_id" => $id));


            $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"23",
                                  "table_name"=>"tborgchart",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

            $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);
            break;
        case "delete_workflow" :
            $db->delete("tbworkflow", array("id" => $id));
            $db->delete("tbworkflow_objects", array("workflow_id" => $id));
            $db->delete("tbworkflow_lines", array("workflow_id" => $id));

            $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"32",
                                  "table_name"=>"tbworkflow",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

            $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);
            break;
        case "delete_print_report" :
            $db->delete("tb_generate", array("id" => $id));
            break;
        case "softDelete_form" :
            $form = new Form($db,$id);
            $newTableName = $form->form_table_name."_".$id;
            $set = array("is_delete" => "1","form_table_name"=>$newTableName,"date_updated"=>$date);
            $where = array("id" => $id);
            $this->update("tb_workspace", $set, $where);
            $db->create("RENAME TABLE ". $form->form_table_name ." TO ".$newTableName);
            $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"34",
                                  "table_name"=>"tb_workspace",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

            $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);

            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $form_record_memcached = array("form_list","form_count");
            $nav_memcached = array("leftbar_nav");
            $starred_memcached = array("starred_list");

            $deleteMemecachedKeys = array_merge($request_record_memcacached,
                                                $form_record_memcached,
                                                $nav_memcached,
                                                $starred_memcached
                                              );
            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            break;
        case "softDelete_orgchart" :
            // echo "softDelete_orgchart";
            $set = array("is_delete" => "1");
            $where = array("id" => $id);
            $this->update("tborgchart", $set, $where);

            $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"33",
                                  "table_name"=>"tborgchart",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

            $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);

            //deletememcached
            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $nav_memcached = array("leftbar_nav");
            $starred_memcached = array("starred_list");
            $orgchart_record_memcacached = array("orgchart_list","orgchart_count");

            $deleteMemecachedKeys = array_merge($request_record_memcacached,
                                                $orgchart_record_memcacached,
                                                $nav_memcached,
                                                $starred_memcached
                                              );
            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            break;
        case "softDelete_workflow" :
            // echo "softDelete_workflow";
            $set = array("is_delete" => "1");
            $where = array("id" => $id);
            $this->update("tbworkflow", $set, $where);

            $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"35",
                                  "table_name"=>"tbworkflow",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

            $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);

            $request_record_memcacached = unserialize(REQUEST_RECORD_MEMCACHED);
            $nav_memcached = array("leftbar_nav");
            $starred_memcached = array("starred_list");

            $deleteMemecachedKeys = array_merge($request_record_memcacached,
                                                $nav_memcached,
                                                $starred_memcached
                                              );
            $fs->deleteMemcacheKeys($deleteMemecachedKeys);
            break;
    }
}

class UpdateStatus extends Database {

    // for workflow

    public function getFormID($id) {
        $getFormID = $this->query("SELECT form_id FROM tbworkflow WHERE id = $id AND is_delete = 0", "row");
        return $getFormID['form_id'];
    }

    public function deactivateWorkflow($id) {
        $auth = Auth::getAuth('current_user');
        $fs = new functions;
        $date = $fs->currentDateTime();

        $form_id = $this->getFormID($id);
        $set = array("Is_active" => "0");
        $where = array("form_id" => $form_id);
        $this->update("tbworkflow", $set, $where);

        //
        

        $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"31",
                                  "table_name"=>"tbworkflow",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

        $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);
        return;
    }

    public function activateWorkflow($id) {
        $auth = Auth::getAuth('current_user');
        $fs = new functions;
        $date = $fs->currentDateTime();
        $set = array("Is_active" => "1");
        $where = array("id" => $id);
        $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"30",
                                  "table_name"=>"tbworkflow",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

        $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);
        $this->update("tbworkflow", $set, $where);
        return;
    }

    //for form
    public function activateForm($id) {
        $auth = Auth::getAuth('current_user');
        $fs = new functions;
        $date = $fs->currentDateTime();
        $set = array("Is_active" => "1","date_updated"=>$date);
        $where = array("id" => $id);
        $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"26",
                                  "table_name"=>"tb_workspace",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

        $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);
        $this->update("tb_workspace", $set, $where);
        return;
    }

    public function deactivateForm($id) {
        $auth = Auth::getAuth('current_user');
        $fs = new functions;
        $date = $fs->currentDateTime();
        $set = array("Is_active" => "0","date_updated"=>$date);
        $where = array("id" => $id);
        $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"27",
                                  "table_name"=>"tb_workspace",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

        $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);
        $this->update("tb_workspace", $set, $where);
        return;
    }

    //for orgchart
    public function getOrgchartObjects($id) {
        $getOrgchartObjects = $this->query("SELECT * FROM tborgchartobjects WHERE orgChart_id = $id", "array");
        return $getOrgchartObjects;
    }

    public function updateUsersDepartment($id) {
        $department_json = $this->getOrgchartObjects($id);
        //remove department
        $this->removeUsersDepatment();


        // $department_json_head = $department_json['orgchart_user_head'];
        foreach ($department_json as $key => $value) {
            //head
            $json = json_decode($value['json']);
            $department_id = $value['id'];
            $json_head = $json->{'orgchart_user_head'};
            $json_assistant_head = $json->{'orgchart_dept_assistant'};
            $json_members = $json->{'orgchart_dept_members'};
            $users_arr = array_merge($json_head, $json_assistant_head, $json_members);
            foreach ($users_arr as $users_id) {
                $set = array("department_id" => $department_id);
                $where = array("id" => $users_id);
                $this->update("tbuser", $set, $where);
            }
        }
    }

    public function removeUsersDepatment() {
        $auth = Auth::getAuth('current_user');

        $company_id = $auth['company_id'];
        $set = array("department_id" => "0");
        $where = array("company_id" => $company_id);

        $this->update("tbuser", $set, $where);
    }

    public function activateOrgchart($id) {
        $fs = new functions;
        $date = $fs->currentDateTime();
        //update users
        $this->updateUsersDepartment($id);

        //activate orgchart
        $set = array("status" => "1");
        $where = array("id" => $id);
        //audit trail
        $auth = Auth::getAuth('current_user');
        $insert_audit_rec = array("user_id"=>$auth['id'],
                                  "audit_action"=>"21",
                                  "table_name"=>"tborgchart",
                                  "record_id"=>$id,
                                  "date"=>$date,
                                  "ip"=>$_SERVER["REMOTE_ADDR"],
                                  "is_active"=>1);

        $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);
        $this->update("tborgchart", $set, $where);
    }

    public function deactivateOrgchart() {
        $fs = new functions;
        $date = $fs->currentDateTime();

        $auth = Auth::getAuth('current_user');

        $company_id = $auth['company_id'];


        $getActiveOrgchart = $this->query("SELECT id FROM tborgchart WHERE status = 1  AND company_id = '". $company_id ."' AND is_delete = 0","row");

        $row = $this->query("SELECT id FROM tborgchart WHERE status = 1  AND company_id = '". $company_id ."' AND is_delete = 0","numrows");

        if($row==1){
            //deactivate orgchart
            $set = array("status" => "0");
            $where = array("company_id" => $company_id);

            $this->update("tborgchart", $set, $where);


            $insert_audit_rec = array("user_id"=>$auth['id'],
                                      "audit_action"=>"22",
                                      "table_name"=>"tborgchart",
                                      "record_id"=>$getActiveOrgchart['id'],
                                      "date"=>$date,
                                      "ip"=>$_SERVER["REMOTE_ADDR"],
                                      "is_active"=>1);

            $audit_log = $this->insert("tbaudit_logs",$insert_audit_rec);
        }
    }

}

?>