<?php
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$userCompany = new userQueries();
$company = $userCompany->getCompany($auth['company_id']);
if(isset($_POST) and $_SERVER['REQUEST_METHOD']=="POST"){
	// User file path
	if($_POST['userType']=="companyUser"){
		$path = "images/formalistics/tbuser";
		$profilePic = $_FILES['userProfilePic']['name'];
		$profilePic_size = $_FILES['userProfilePic']['size']; // File Size
		$uploadedfile = $_FILES['userProfilePic']['tmp_name'];
	}elseif($_POST['userType']=="companyAdmin"){
		$path = "images/formalistics/tbcompany";
		$profilePic = $_FILES['userCompanyPic']['name'];
		$profilePic_size = $_FILES['userCompanyPic']['size']; // File Size
		$uploadedfile = $_FILES['userCompanyPic']['tmp_name'];
	}
	
		$ext = explode(".", $profilePic);
		$last_ext = $ext[count($ext)-1];
	    
        // Allowed extension for file upload in the registration    
        $valid_formats = unserialize(IMG_EXTENSION);
        $filter = new ImageFilter();
	$score = $filter->GetScore($uploadedfile);
		if(!in_array($last_ext,$valid_formats)){
			echo "Invalid File Format";
		}else if ($profilePic_size=="0"){
			echo "File too large. File must be less than 4 megabytes.";
		//}else if($score >= 40){
				 //echo "It seems that you have uploaded a nude picture.";
		}else{
			if($_POST['userType']=="companyUser"){
				$userID = $auth['id'];
				$update = array("id"=>$userID);
				$set = array("extension"=>"png");
				$db->update("tbuser",$set,$update);
			}elseif($_POST['userType']=="companyAdmin"){
				$userID = $company['id'];
				$update = array("id"=>$userID);
				$set = array("extension"=>"png");
				$db->update("tbcompany",$set,$update);
			}
		    
		    
			// Create Folder of the user for their image
				$id_encrypt = md5(md5($userID));
				$dir = $path."/".$id_encrypt;
				if(!is_dir($dir)){
				    mkdir($dir); // location
				}
			// Image size
			$info = getimagesize($uploadedfile);
			
			// Resize an image and Crop
			    // Set 1 (Large)
			    $thumbnail0 = $dir."/large_".$id_encrypt.".png";
			    image_resize::create_thumbnail($uploadedfile,$thumbnail0,"300","300");
			    
			    
			    // Set 2 (Medium)
			    $thumbnail1 = $dir."/medium_".$id_encrypt.".png";
			    image_resize::create_thumbnail($uploadedfile,$thumbnail1,"150","150");
			    
			    // Set 3 (Small)
			    $thumbnail2 = $dir."/small_".$id_encrypt.".png";
			    image_resize::create_thumbnail($uploadedfile,$thumbnail2,"60","60");
			    
			    // Set 0 (Set Original)
			    $thumbnail = $dir."/Original_".$id_encrypt.".png";
			    //image_resize::create_thumbnail($uploadedfile,$thumbnail,$info[0],$info[1]);
			    move_uploaded_file($uploadedfile,$thumbnail);
			    //imagedestroy($tmp); // Large //imagedestroy($tmp1); // Medium //imagedestroy($tmp2); // Small
			    
			    $succ[] = array("successful"=>"Your new photo was successfully uploaded.",
					    "image"=> MAIN_PAGE . $thumbnail,
					    "userType"=>$profilePic_size);
			
			    echo json_encode($succ);
		}
	    
}


?>