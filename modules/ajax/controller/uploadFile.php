<?php
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;
$userCompany = new userQueries();
$upload = new upload();
$company = $userCompany->getCompany($auth['company_id']);
if(isset($_POST) and $_SERVER['REQUEST_METHOD']=="POST"){
	// User file path
	if($_POST['uploadType']=="postFileUpload" || $_POST['uploadType']=="messageFileUpload" || $_POST['uploadType']=="taskFileUpload"
	    || $_POST['uploadType']=="post_forms_object"){
		$folder = $_POST['uploadFolder'];
		$path = "images/" . $folder . "/temporaray_files";
		$file = $_FILES['postFile']['name'];
		$file_size = $_FILES['postFile']['size']; // File Size
		$uploadedfile = $_FILES['postFile']['tmp_name'];
	}elseif($_POST['uploadType']=="postLogoUpload"){
		$path = "images/formLogo/temporaray_files";
		$file = $_FILES['uploadLogoForm']['name'];
		$file_size = $_FILES['uploadLogoForm']['size']; // File Size
		$uploadedfile = $_FILES['uploadLogoForm']['tmp_name'];
	}
	
            $ext = explode(".", $file);
            $last_ext = $ext[count($ext)-1];
	    
        // Allowed extension for file upload in the registration    
        $valid_formats = unserialize(FILES_EXTENSION);
        $filter = new ImageFilter();
	$score = $filter->GetScore($uploadedfile);
            if(!in_array($last_ext,$valid_formats)){
                echo "Invalid File Format";
            }elseif($file_size=="0"){
		echo "File too large. File must be less than 4 megabytes.";
	    //}elseif($score >= 40){
				//echo "It seems that you have uploaded a nude picture.";
	       }else{
		$id_encrypt = md5(md5($auth['id']));
		$dir = $path."/".$id_encrypt;
			
		// Create Folder of the user for their image	
			$upload->createFolder($dir);
			$filename = md5(md5(time()));
			
		// Resize an image
			if($_POST['uploadType']=="postFileUpload" || $_POST['uploadType']=="messageFileUpload" || $_POST['uploadType']=="taskFileUpload"){
				if(!in_array($last_ext,unserialize(IMG_EXTENSION))){
					$name = $file;
					$thumbnail = $dir . "/" . $name;
					move_uploaded_file($uploadedfile,$thumbnail);
				}else{
					$name = "large_" . $filename . ".png";
					$thumbnail = $dir . "/" . $name;
					move_uploaded_file($uploadedfile,$thumbnail);
					//image_resize::create_thumbnail($uploadedfile,$thumbnail,"500","500");
				}
			}elseif($_POST['uploadType']=="postLogoUpload"){
				// Set 3 (Small)
				$thumbnail = $dir."/small.png";
				image_resize::create_thumbnail($uploadedfile,$thumbnail,"60","60");
			}elseif($_POST['uploadType']=="post_forms_object"){
				// Set 3 (Small)
				$thumbnail = $dir."/small.png";
				image_resize::create_thumbnail($uploadedfile,$thumbnail,"60","60");
			}
				$formats = unserialize(IMG_EXTENSION);
				if(in_array($last_ext,$formats)){
				    $extention = "image";
				}else{
				    $extention = "file";
				}
				
		    $succ[] = array("successful"	=>	"Your new photos was successfully uploaded.",
				    "name"		=>	$name,
				    "image"		=>	MAIN_PAGE . $thumbnail,
				    "userType"		=>	$last_ext,
				    "extension"		=>	$extention,
				    "filename"		=>	$file,
				    "Folder" 		=>	$folder);
		
		    echo json_encode($succ);

	    }
        
}
?>