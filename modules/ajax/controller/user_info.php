<?php

$db = new Database();
$fs = new functions();
$auth = Auth::getAuth('current_user');
$email = new Mail_Notification();
$userCompany = new userQueries();
$company = $userCompany->getCompany($auth['company_id']);
$action = $_POST['action'];

if (isset($action)) {

    // Edit Personal Info
    if ($action == "edit_personal_info") {
        $dataID = $_POST['dataID'];
        $displayName = $_POST['displayName'];
        $firstName = $_POST['firstName'];
        $middleName = $_POST['middleName'];
        $lastName = $_POST['lastName'];
        $contactNumber = $_POST['contactNumber'];

        $set = array(
            // "first_name"       =>      $firstName,
            // "middle_name"      =>      $middleName,
            // "last_name"        =>      $lastName,
            "contact_number" => $contactNumber);

        $condition = array("id" => $dataID);

        $updateInfo = $db->update("tbuser", $set, $condition);
        $redis_cache = getRedisConnection();
        if ($redis_cache) {
            $user_cache = $redis_cache->get("person_object_" . $dataID);

            if ($user_cache) {
                $redis_cache->del("person_object_" . $dataID);
            }
        }
        $info = array(
            // "first_name"       =>      $firstName,
            // "middle_name"      =>      $middleName,
            // "last_name"        =>      $lastName,
            "contact_number" => $contactNumber,
            "msg" => "Your information was successfully updated.");

        echo json_encode($info);

        $login = $db->query("SELECT *
                                    FROM tbuser
                                    WHERE id={$db->escape($dataID)} ", "row");
        Auth::setAuth('current_user', $login);
    } elseif ($action == "personal_config") {
        $type = $_POST['type'];
        $set_display_name = $_POST['set_display_name'];
        $action = $_POST['action'];

        $encode = array("display_name" => $set_display_name);
        $encrypt_decode = json_encode($encode);
        $person = new Person($db, $auth['id']);
        if ($type == "set_display") {
            $person->json_tbl = $encrypt_decode;
            $person->update();
            echo "Name was successfully set.";
            $login = $db->query("SELECT *
                                        FROM tbuser
                                        WHERE id={$db->escape($auth['id'])} ", "row");
            Auth::setAuth('current_user', $login);
        }
    }

    // Edit Company Info
    elseif ($action == "edit_company_info") {
        $dataID = $_POST['dataID'];
        $companyName = $_POST['companyName'];
        $companyCode = $_POST['companyCode'];
        $companyNumber = $_POST['companyNumber'];

        $set = array("name" => $companyName,
            "code" => $companyCode,
            "contact_number" => $companyNumber);

        $condition = array("id" => $dataID);

        $updateInfo = $db->update("tbcompany", $set, $condition);
        $redis_cache = getRedisConnection();
        if ($redis_cache) {
            $user_cache = $redis_cache->get("company_object_" . $dataID);

            if ($user_cache) {
                $redis_cache->del("company_object_" . $dataID);
            }
        }

        $info = array("companyName" => $companyName,
            "companyCode" => $companyCode,
            "companyNumber" => $companyNumber,
            "msg" => "Your company information was successfully updated.");

        echo json_encode($info);
    } elseif ($action == "editUsername") {
        $oldUsername = $_POST['oldUsername'];
        $newUsername = $_POST['newUsername'];
        //$email_rows = $db->escape("SELECT * FROM tbuser WHERE email!={$db->escape($newUsername)}
        //                         AND company_id={$db->escape($auth['company_id'])}
        //                         AND is_active=1","rows");
        $email_num = $db->query("SELECT * FROM tbuser WHERE email={$db->escape($newUsername)}
                                     AND company_id={$db->escape($auth['company_id'])}
                                     AND is_active=1", "numrows");
        //if($email_rows['email'] == $newUsername){
        //    echo "Please type your correct email format.";
        //}else{
        if (!$fs->VerifyMailAddress($newUsername)) {
            echo "Please type your correct email format.";
        } else {
            if ($email_num != 0) {
                echo "Email is already in use.";
            } else {
                // Update Table User
                $set = array("email" => $newUsername);
                $condition = array("id" => $auth['id']);
                $db->update("tbuser", $set, $condition);

                // Update Table Company
                $set = array("email" => $newUsername);
                $condition = array("id" => $company['id']);
                $db->update("tbcompany", $set, $condition);

                $redis_cache = getRedisConnection();
                if ($redis_cache) {
                    $user_cache = $redis_cache->get("person_object_" . $auth['id']);

                    if ($user_cache) {
                        $redis_cache->del("person_object_" . $auth['id']);
                    }
                }

                // Auth
                $login = $db->query("SELECT *
                                                    FROM tbuser
                                                    WHERE id={$db->escape($auth['id'])} ", "row");
                Auth::setAuth('current_user', $login);

                echo "Your New Email was Successfully Updated.";
            }
        }

        //}
    } elseif ($_POST['action'] == "newPassword") {
        $new_password = $fs->encrypt_decrypt("encrypt", $_POST['newPassword']);
        $oldPassword = $_POST['oldPassword'];
        $myPassword = $fs->encrypt_decrypt("decrypt", $auth['password']);
        if ($myPassword != $oldPassword) {
            echo "no";
        } else {
            $conditions = array("id" => $auth['id']);
            $fields = array("password" => $new_password);
            $db->update("tbuser", $fields, $conditions);

            $redis_cache = getRedisConnection();
            if ($redis_cache) {
                $user_cache = $redis_cache->get("person_object_" . $auth['id']);

                if ($user_cache) {
                    $redis_cache->del("person_object_" . $auth['id']);
                }
            }

            // Auth
            $login = $db->query("SELECT *
                                        FROM tbuser
                                        WHERE id={$db->escape($auth['id'])} ", "row");
            Auth::setAuth('current_user', $login);
        }
    }
}
?>