<?php

$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions();
$date = $fs->currentDateTime();

define("TBCONTAINER", "1_tbl_containerform");
define("TBSLOT", "1_tbl_slotform");
define("TBSLOTCONTAINER", "1_tbl_slotcontainerform");
define("TBTASK", "1_tbl_task");


$reference_number = $_GET["referencenumber"];
$track_no = generateRandomString();
$procedures = process($track_no, $reference_number);

//var_dump(getTasks($track_no));
$tasks = getTasks($track_no);
foreach ($tasks as $step => $task) {
    echo ($step + 1) . ". " . $task["description"] . "\n";
}

function getMySQLConnection() {
    $conn = new PDO("mysql:host=" . DB_HOSTNAME . ";dbname=" . DB_NAME, DB_USERNAME, DB_PASSWORD);
    return $conn;
}

function process($track_no, $reference_number, $flag) {
    $conn = getMySQLConnection();
    $containers = $conn->query("SELECT id, description, (width * length) as container_area, referencenumber  FROM " . TBCONTAINER . " WHERE referencenumber='{$reference_number}'");
//    var_dump($containers);
    while ($container_object = $containers->fetch()) {
        //get available slot for the container
        $slots_available = getAvailableSlots($container_object, $flag);

        $record_count = 0;
        while ($slot_object = $slots_available->fetch()) {
            $record_count++;
            if (isContainerAvailable($slot_object, $container_object, $flag)) {
                $return_id = addContainertoSlot($slot_object, $container_object);
                $slot_containers = getSlotContainers($slot_object);
                arrangeContainers($track_no, $slot_containers, $slot_object["referencenumber"], $container_object, $flag, $return_id);
                break;
            }
        }
    }

    //remove previous recordse
//            removeContainersFromPreviousSlots();
}

function getAvailableSlots($container_object, $flag) {
    $conn = getMySQLConnection();
    if ($flag) {
        $slots_id = array();
        $slots_used = getContainersUsedSlots($container_object);
        //get used slots id
        foreach ($slots_used as $slot) {
            array_push($slots_id, $slot["slot_id"]);
        }
        $slots_implode = implode(",", $slots_id);
        $slots_available = $conn->query("SELECT id, description, referencenumber,  "
                . " (width * length) as slot_area FROM " . TBSLOT . " WHERE (width * length) >= {$container_object["container_area"]} "
                . " AND id NOT IN ({$slots_implode}) ORDER BY (width * length) ASC ");
    } else {
        $slots_available = $conn->query("SELECT id, description, referencenumber, "
                . " (width * length) as slot_area FROM " . TBSLOT . " WHERE (width * length) >= {$container_object["container_area"]} "
                . " ORDER BY (width * length) ASC ");
    }

    return $slots_available;
}

function getTasks($track_no) {
    $conn = getMySQLConnection();
    $tasks = $conn->query("SELECT description FROM " . TBTASK . " WHERE Transaction_ReferenceNumber='{$track_no}'");
    $return_tasks = $tasks->fetchAll(PDO::FETCH_ASSOC);
    return $return_tasks;
}

function addTask($track_no, $slot_container_id, $description) {
    $conn = getMySQLConnection();
    $stmt = $conn->prepare("INSERT INTO " . TBTASK . " (Transaction_ReferenceNumber, description)"
            . " VALUES (:track_no, :description)");

    $stmt->bindParam(":track_no", $track_no);
    $stmt->bindParam(":description", $description);

    $stmt->execute();

    return $conn->lastInsertId();
}

function getSlotContainers($slot_object) {
    $conn = getMySQLConnection();
    $containers = $conn->query("SELECT container.*, (container.width * container.length) as container_area FROM " . TBSLOTCONTAINER . " slot_container "
            . "LEFT JOIN " . TBCONTAINER . " container ON container.id = slot_container.container_id WHERE slot_container.slot_id={$slot_object["id"]}");
    $return_containers = $containers->fetchAll();
    return $return_containers;
}

function addContainertoSlot($slot_object, $container_object) {
    $conn = getMySQLConnection();
    $stmt = $conn->prepare("INSERT INTO " . TBSLOTCONTAINER . " (slot_id, container_id) VALUES (:slot_id, :container_id)");
    $stmt->bindParam(":slot_id", $slot_object["id"]);
    $stmt->bindParam(":container_id", $container_object["id"]);
    $stmt->execute();

    return $conn->lastInsertId();
}

function removeContainerFromSlot($container_object, $slot_container_id) {
    $conn = getMySQLConnection();
    $stmt = $conn->prepare("DELETE FROM " . TBSLOTCONTAINER . "  WHERE container_id = {$container_object["id"]}"
            . " AND id <> {$slot_container_id}");
//            var_dump($stmt);
    $stmt->execute();
}

function getContainersUsedSlots($container_object) {
    $conn = getMySQLConnection();
    $slots = $conn->query("SELECT slot_id FROM " . TBSLOTCONTAINER . "  WHERE "
            . "container_id={$container_object["id"]}");
    $return_slots = $slots->fetchAll();
    return $return_slots;
}

function isContainerAvailable($slot_object, $container_object, $flag) {
    $conn = getMySQLConnection();

    if ($flag) {
        $slots = $conn->query("SELECT COUNT(*) as Total FROM " . TBSLOTCONTAINER . "  WHERE"
                . " slot_id={$slot_object["id"]} AND "
                . " container_id={$container_object["id"]}");
    } else {
        $slots = $conn->query("SELECT COUNT(*) as Total FROM " . TBSLOTCONTAINER . "  WHERE"
                . " container_id={$container_object["id"]}");
    }

    $slots_total = $slots->fetch()["Total"];


    if ($slots_total == 0) {
        return true;
    } else {
        return false;
    }
}

function arrangeContainers($track_no, $slot_containers = array(), $slot_description, $reference_container, $flag, $last_slot_id) {
    $slot_containers = array_sort($slot_containers, "container_area", SORT_DESC);
    $slot_containers_temporary = array_sort($slot_containers, "container_area", SORT_ASC);

    $temp_container = array();
    $ctr = 0;

    $action = "put ";
    foreach ($slot_containers as $key => $container) {
        if ($ctr != 0) {
            addTask($track_no, $last_slot_id, $action . $container["referencenumber"] . " on top of " . $temp_container["referencenumber"]);
        } else {
            // && $slot_containers[count($slot_containers) - 1]["id"] != $reference_container["id"]
            if (count($slot_containers) > 1) {
//                        moveContainersToAnotherSlot($track_no, $slot_containers, $reference_container["id"]);
//                        addTask($track_no, $last_slot_id, "move " . $reference_container["description"] . " to a temporary slot");

                foreach ($slot_containers_temporary as $container_temp) {
                    if ($container_temp["ID"] != $reference_container["id"]) {
                        addTask($track_no, $last_slot_id, "move " . $container_temp["referencenumber"] . " to a temporary slot");
                    }
                }
            }
            addTask($track_no, $last_slot_id, $action . $container["referencenumber"] . " to " . $slot_description);
        }

        $temp_container = $container;
        $ctr++;
    }
}

function moveContainersToAnotherSlot($track_no, $slot_containers = array(), $reference_container = "") {
    foreach ($slot_containers as $container) {
        if ($container["id"] != $reference_container) {
            process($track_no, $container["id"], true);
        }
    }
}

function array_sort($array, $on, $order = SORT_ASC) {

    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        $new_key = 0;
        foreach ($sortable_array as $k => $v) {
            $new_array[$new_key] = $array[$k];
            $new_key++;
        }
    }

    return $new_array;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
