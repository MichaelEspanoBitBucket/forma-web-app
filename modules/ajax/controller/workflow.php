<?php
//check if session is alive if not.. retun false;
if(!Auth::hasAuth('current_user')){ 
    return false;
}
$auth = Auth::getAuth('current_user');
$db = new Database;
$fs = new functions;

if (isset($_POST['action'])) {
    $date = $fs->currentDateTime();

    switch ($_POST['action']) {

        case "saveWorkflow":
            $workflow = $_POST['json_encode'];
            $json = json_decode($workflow, true);
            //DEACTIVATE OLD WORKFLOW
            $update_active = array('is_active' => '0');
            $where_active = array('form_id' => $json['form_id']);
            $db->update("tbworkflow", $update_active, $where_active);
            //INSERT WORKFLOW DETAILS
            $insert = array("title" => $json['title'],
                "form_id" => $json['form_id'],
                "description" => $json['description'],
                "date" => $date,
                "tbw_json" => json_encode(array("width" => $json['width'], "height" => $json['height'], "template_form_size" => $json['template_form_size'])),
                "created_by" => $auth['id'],
                "is_active" => $json['status']
            );
            $workflow_id = $db->insert("tbworkflow", $insert);


            //UPDATE FORM STATUS(IS_ACTIVE) TO 1
            // $updateform_condition = array("id"=>$json['form_id']);
            // $updateform_set = array("is_active"=>"1");
            // $updateform_status = $db->update("tb_workspace",$updateform_set,$updateform_condition);
            //INSERT WORKFLOW OBJECTS
            foreach ($json['nodes'] as $nodes) {
                //custom noti message
                $noti_message = array("type" => $nodes['workflow_notification_message_type'], "message" => $nodes['workflow_notification_message']);
                $noti_enabled = 1;


                if (array_key_exists('workflow_notification_enabled', $nodes)) {
                    $noti_enabled = $nodes['workflow_notification_enabled'];
                }
                if ($nodes['type_rel'] == "2") {
                    $delegate_enabled = 1;
                }
                if (array_key_exists('workflow_delegate_enabled', $nodes)) {
                    $delegate_enabled = $nodes['workflow_delegate_enabled'];
                }
                $buttonStatus = json_encode($nodes['buttonStatus']);
                if ($nodes['type_rel'] == "5") {
                    $buttonStatus = $nodes['buttonStatus'];
                }

                $insert_obects = array("workflow_id" => $workflow_id,
                    "object_id" => $nodes['node_data_id'],
                    "processorType" => $nodes['processorType'],
                    "processor" => $nodes['processor'],
                    "buttonStatus" => $buttonStatus,
                    "condition_return" => json_encode($nodes['condition_return']),
                    "status" => $nodes['status'],
                    "type_rel" => $nodes['type_rel'],
                    "operator" => $nodes['operator'],
                    "field" => $nodes['field'],
                    "field_value" => $nodes['field_value'],
                    "email" => json_encode($nodes['workflow_email']),
                    "email_cancel" => json_encode($nodes['cl_workflow_email']),
                    "sms" => json_encode($nodes['workflow_sms']),
                    "fieldEnabled" => json_encode($nodes['fieldEnabled']),
                    "fieldRequired" => json_encode($nodes['fieldRequired']),
                    "fieldHiddenValue" => json_encode($nodes['fieldHiddenValue']),
                    "fieldEnabled_access_type" => $nodes['fieldEnabled_access_type'],
                    "fieldRequired_access_type" => $nodes['fieldRequired_access_type'],
                    "fieldHiddenValue_access_type" => $nodes['fieldHiddenValue_access_type'],
                    "enable_delegate" => $delegate_enabled,
                    "json" => json_encode($nodes),
                    "noti_message" => json_encode($noti_message),
                    "noti_enabled" => $noti_enabled
                );
                $db->insert("tbworkflow_objects", $insert_obects);
            }
            // $messageAttr = array("form_id"=>$json['form_id'], //pao fixes
            //                         "title"=>$json['title'],
            //                     );
            // $action_description = Logs::createMessage("29",$messageAttr);

            //INSERT WORKFLOW LINES
            foreach ($json['lines'] as $lines) {
                $insert_obects = array("workflow_id" => $workflow_id,
                    "parent" => $lines['parent'],
                    "child" => $lines['child'],
                    "json" => json_encode($lines),
                );
                $db->insert("tbworkflow_lines", $insert_obects);
            }

            // $messageAttr = array("form_id"=>$json['form_id'], //pao fixes
            // 						"title"=>$json['title'],
            // 					);
            // $action_description = Logs::createMessage("29",$messageAttr);

            //audit trail
            $insert_audit_rec = array("user_id" => $auth['id'],
                "audit_action" => "29",
                "table_name" => "tbworkflow",
                "record_id" => $workflow_id,
                "date" => $date,
                "ip" => $_SERVER["REMOTE_ADDR"],
                "is_active" => 1,
                    /* "action_description"=>$action_description */                    );

            $audit_log = $db->insert("tbaudit_logs", $insert_audit_rec);

            $jsonReturn = array("title" => $json['title'], "id" => $workflow_id);

            //	echo "Your Workflow (" . $json['title'] . ") was successfully saved.";

            //deletememcached
            $workflow_record_memcacached = array("workflow_list","workflow_count");
            $form_record_memcached = array("form_list", "form_count", "workspace_form_details_" . $json['form_id']);
            $nav_memcached = array("leftbar_nav");

            $deleteMemecachedKeys = array_merge($workflow_record_memcacached, $form_record_memcached, $nav_memcached);

            $fs->deleteMemcacheKeys($deleteMemecachedKeys);

            echo json_encode($jsonReturn);
            break;
        case "getProcessor":
            $search = new Search();
            $value = $_POST['value'];
            $selectedValue = $_POST['selected'];
            //			$form_id = $_POST['form_id'];
            //			$form = $db->query("SELECT category_id FROM tb_workspace WS 
            //                            WHERE WS.id={$db->escape($form_id)} AND is_delete = 0 ","row");
            //$category_id = $form['category_id'];
            $option = "<option value='0'>--Select--</option>";
            if ($value == "1") { /* FOR DEPARTMENT POSITION */
                $company_id = $auth['company_id'];
                $getOrgchart = $db->query("SELECT * FROM tborgchart WHERE company_id = '" . $company_id . "' and is_active = '1' AND is_delete = 0", "numrows");
                if ($getOrgchart > 0) {
                    $option .= "<option value='1' " . functions::setSelected('1', $selectedValue) . ">Head</option>";
                    $option .= "<option value='2' " . functions::setSelected('2', $selectedValue) . ">Assistant Head</option>";
                } else {
                    $option = "0";
                }
            } else if ($value == "2") { /* FOR COMPANY POSITION */
                $getPosition = $search->getAllPositionV2();
                // $getPosition = $search->getFormUsersByPositions(" AND fcu.form_category_id = '$category_id' AND fu.form_id = '$form_id' GROUP BY fcu.user");
                foreach ($getPosition as $position_array) {
                    $option .= "<option value='" . $position_array['id'] . "' " . functions::setSelected($position_array['id'], $selectedValue) . ">" . $position_array['position'] . "</option>";
                }
            } else if ($value == "3") { /* FOR EMPLOYEE */
                // $query = "SELECT id,first_name,middle_name,last_name FROM tbuser WHERE company_id = {$db->escape($auth['company_id'])} AND is_active = 1 AND user_level_id!=4";
                $query = "SELECT id,first_name,middle_name,last_name,display_name FROM tbuser WHERE company_id = {$db->escape($auth['company_id'])} AND is_active = 1 AND user_level_id!=4 AND display_name!='' ORDER BY TRIM(display_name)";
                $getUsers = $db->query($query, "array");
                //$getUsers = $search->getFormUsersByUsers(" AND fcu.form_category_id = '$category_id' AND fu.form_id = '$form_id' GROUP BY fcu.user");

                foreach ($getUsers as $users_array) {
                    $option .= "<option value='" . $users_array['id'] . "' " . functions::setSelected($users_array['id'], $selectedValue) . ">" . $users_array['display_name'] . "</option>";
                }
            } else if ($value == "4") {
                $option = "<option value='1'>Requestor</option>";
            } else if ($value == "5") {
                $id = $_POST['form_id'];
                $fields_arr = functions::getFormFieldsData(" WHERE form_id = '" . $id . "' ORDER BY field_name"); //functions::getFormFields($id);
                // sort($fields_arr, SORT_STRING);
                foreach ($fields_arr as $fields) {
                    $option .= "<option value='" . $fields['field_name'] . "' " . functions::setSelected($fields['field_name'], $selectedValue) . ">" . $fields['field_name'] . "</option>";
                }
            } else if ($value == "6") {/* COMPANY groups */
                $getGroups = $search->getFormGroups();
                foreach ($getGroups as $group) {
                    $option .= "<option value='" . $group['id'] . "' " . functions::setSelected($group['id'], $selectedValue) . ">" . $group['group_name'] . "</option>";
                }
            }
            echo $option;
            break;
        case "getFormFields":

            $active_fields = functions::getFormFieldsData(" WHERE form_id = '" . $_POST['form_id'] . "'"); //functions::getFormFields($_POST['form_id']);
            $form_columns = array();
            foreach ($active_fields as $value) {
                array_push($form_columns, $value['field_name']);
            }
            array_push($form_columns, "Status");
            sort($form_columns, SORT_NATURAL | SORT_FLAG_CASE);
            echo json_encode($form_columns);
            break;
        case "getTablesInExternalConnection":
            $id = $_POST['id'];
            $externalDB = new ExternalDatabase($db);

            $getExternalConn = $externalDB->getExternalDB(" AND tbe_d.id= $id ", "", "", $auth['company_id'], "row");

            $db_type = $getExternalConn['db_type'];

            $db_host = $getExternalConn['db_hostname'];
            $db_name = $getExternalConn['db_name'];
            $db_username = $getExternalConn['db_username'];
            $password = $getExternalConn['db_password'];


            if ($db_type == "1") {
                $mysqlConn = new MySQLDatabase($db_host, $db_name, $db_username, $password);
                
                $tables = $mysqlConn->query("select TABLE_NAME from information_schema.tables where table_schema={$mysqlConn->escape($db_name)}", "array");
                
                echo json_encode($tables);
                $mysqlConn->disConnect();
            } else if ($db_type == "2") {
                try {
                    $dbMSSQL = new MSSQLDatabase($db_host, $db_name, $db_username, $password);

                    //get tables
                    $getTables = $dbMSSQL->query("SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_TYPE='BASE TABLE' ORDER BY TABLE_NAME ASC", "array");

                    echo json_encode($getTables);
                    $dbMSSQL->disconnect();
                    // echo "200";
                } catch (Exception $err) {
                    // print_r($err);
                    echo "Failed MS SQL";
                }
            } else if ($db_type == "3") {
                try {
                    $dbOracle = new OracleDatabase($db_host, $db_name, $db_username, $password);

                    //get table
                    $getTables = $dbOracle->query("SELECT TABLE_NAME FROM user_tables ORDER BY table_name ASC");

                    echo json_encode($getTables);
                    // echo "200";
                    $dbOracle->disConnect();
                } catch (Exception $e) {
                    // foreach(PDO::getAvailableDrivers() as $driver)
                    //     echo $driver, '<br>';
                    // print_r($e);
                    echo "Failed Oracle";
                }
            }
            break;
        case "getFieldsInExternalConnection":
            $extDB_data = $_POST['extDB_data'];
            $id = $extDB_data['id'];
            $table = $_POST['form_id'];


            $externalDB = new ExternalDatabase($db);

            $getExternalConn = $externalDB->getExternalDB(" AND tbe_d.id= $id ", "", "", $auth['company_id'], "row");

            $db_type = $getExternalConn['db_type'];

            $db_host = $getExternalConn['db_hostname'];
            $db_name = $getExternalConn['db_name'];
            $db_username = $getExternalConn['db_username'];
            $password = $getExternalConn['db_password'];

            // print_r($getExternalConn);

            if ($db_type == "1") {
               $mysqlConn = new MySQLDatabase($db_host, $db_name, $db_username, $password);
                
                $fields = $mysqlConn->query("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = {$mysqlConn->escape($db_name)} AND TABLE_NAME = {$mysqlConn->escape($table)}", "array");
                $return_array = array();
                foreach ($fields as $field){
                    array_push($return_array, $field["COLUMN_NAME"]);
                }
 
                echo json_encode($return_array);
                $mysqlConn->disConnect();
            } else if ($db_type == "2") {
                try {
                    $dbMSSQL = new MSSQLDatabase($db_host, $db_name, $db_username, $password);

                    //get columns
                    $getColumns = $dbMSSQL->query("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='" . $table . "' ORDER BY COLUMN_NAME");
                    // echo json_encode($getColumns);
                    $getColumnsArr = [];
                    foreach ($getColumns as $key => $value) {
                        array_push($getColumnsArr, $value['COLUMN_NAME']);
                    }
                    echo json_encode($getColumnsArr);
                    $dbMSSQL->disconnect();
                    // echo "200";
                } catch (Exception $err) {
                    // print_r($err);
                    echo "Failed MS SQL";
                }
            } else if ($db_type == "3") {
                try {
                    $dbOracle = new OracleDatabase($db_host, $db_name, $db_username, $password);

                    //sample get columns
                    $getColumns = $dbOracle->query("SELECT column_name FROM USER_TAB_COLUMNS WHERE table_name = '" . $table . "' ORDER BY column_name ASC");
                    $getColumnsArr = [];
                    foreach ($getColumns as $key => $value) {
                        array_push($getColumnsArr, $value['COLUMN_NAME']);
                    }
                    echo json_encode($getColumnsArr);
                    // echo "200";
                    $dbOracle->disConnect();
                } catch (Exception $e) {
                    // foreach(PDO::getAvailableDrivers() as $driver)
                    //     echo $driver, '<br>';
                    // print_r($e);
                    echo "Failed Oracle";
                }
            }
            break;
    }
}
?>