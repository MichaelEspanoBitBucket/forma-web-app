<?php

//error_reporting(E_ALL);

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'status' => 'ERROR', //  default
    'error' => null,
    'error_message' => null,
    'results' => null
];

$lastUpdateDate = $_POST['last_update_date'];

$database = new APIDatabase();
$commentManager = new APICommentManager($database, $utilities);

try {
    $database->connect();

    $results = null;

    if (!isset($rangeFrom)) {
        $rangeFrom = 0;
    }

    if (!isset($rangeTo)) {
        $rangeTo = 0;
    }

    $comments = $commentManager->getAllDeletedCommentsSinceDate($lastUpdateDate);

    $response['status'] = APIStatus::STATUS_SUCCESS;
    $response['results'] = $comments;

    //  TODO, search for how to implement finally in php
    $database->disconnect();
} catch (APIException $e) {
    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
}

//error_reporting(0);

echo json_encode($response);
