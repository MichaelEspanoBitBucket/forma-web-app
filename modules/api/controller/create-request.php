<?php

/*
 * Expected Input - POST
 *  request_data    - array, required
 *  form_id         - string, required
 *  action          - string, required
 */

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

//  validate input
$requestData = $_POST['request_data'];
$formId = $_POST['form_id'];
$action = $_POST['action'];

$response = [
    'results' => APIStatus::STATUS_ERROR,
    'error' => APIStatus::ERROR_INVALID_PARAMETERS,
    'error_message' => null,
];

//  <editor-fold desc="Input Validation" defaultstate="collapsed">
//  validate that the required parameters are provided
$response['status'] = APIStatus::STATUS_ERROR;
if (!isset($requestData) || $requestData == '') {
    $response['error_message'] = 'Please provide value for request_data.';
    echo json_encode($response);
    return;
} else if (!json_encode($requestData)) {
    $response['error_message'] = 'Please provide value for request_data. ' + json_last_error();
    echo json_encode($response);
    return;
}

if (!isset($formId) || $formId == '') {
    $response['error_message'] = 'Please provide value for form_id.';
    echo json_encode($response);
    return;
}

if (!isset($action) || $action == '') {
    $response['error_message'] = 'Please provide value for action.';
    echo json_encode($response);
    return;
}
$response['status'] = null;
//  </editor-fold>

$response['error'] = null;
$response['error_message'] = null;

$auth = Auth::getAuth('current_user');

try {
    $database = new APIDatabase();
    $database->connect();
    $requestManager = new APIRequestManager($database, $utilities);

    $newRequestData = $requestManager->createRequest(
            $auth, $formId, $action, json_decode($requestData)
    );

    $database->disconnect();

    $response['status'] = APIStatus::STATUS_SUCCESS;
    $response['results'] = $newRequestData;
} catch (APIException $e) {

    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
}

echo json_encode($response);
