<?php

//error_reporting(E_ALL);

header('Content-Type: application/json');

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response['error'] = null;
$response['error_message'] = null;

//  validate input
if (!array_key_exists('comment_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for the text parameter';
    echo json_encode($response);
    return;
}

$commentId = $_POST['comment_id'];

try {

    $auth = new Auth();
    $current_user = $auth->getAuth('current_user');

    $post = new post();
    $post->deletion('deleteComment', $commentId, true);
    $response['status'] = APIStatus::STATUS_SUCCESS;

    //  filter the response with the only needed data.
    $response['results'] = [
        'message' => 'Successfully deleted comment with id = ' . $commentId
    ];
} catch (Exception $e) {
    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = 'Error saving comment.';
    $response['error_message'] = $e->message;
}

echo json_encode($response);
