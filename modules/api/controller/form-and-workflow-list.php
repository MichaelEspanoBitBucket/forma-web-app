<?php

//error_reporting(E_ALL);
include_once API_LIBRARY_PATH . '/API.php';
$function = new functions();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'status' => APIStatus::STATUS_ERROR,
    'results' => null,
    'error' => null,
    'error_message' => null,
];

$userSearchParameters = $_POST['search_parameters'];

$current_user = Auth::getAuth('current_user');

try {
    $database     = new APIDatabase();
//    $database->connect();
    $formsManager = new APIFormsManager($database, $utilities);    
    
    $searchFilter = new APIFormSearchFilter();

    if (!empty($userSearchParameters)) {

        $userSearchParameters = json_decode($userSearchParameters, true);
        if (!empty($userSearchParameters['range'])) {
            $searchFilter->searchRecordsFrom  = $userSearchParameters['range']['from'];
            $searchFilter->searchRecordsCount = $userSearchParameters['range']['number_of_records'];
        }

        if (!empty($userSearchParameters['date_created_comparison'])) {
            $searchFilter->dateCreatedComparison = $userSearchParameters['date_created_comparison'];
        }

        if (!empty($userSearchParameters['date_updated_comparison'])) {
            $searchFilter->dateUpdatedComparison = $userSearchParameters['date_updated_comparison'];
        }

        $searchFilter->validateFields();
    }

    $formList = $formsManager->searchFormsWithWorkflow($searchFilter, $current_user);

//    $database->disconnect();

    $response['status']      = APIStatus::STATUS_SUCCESS;
    $response['results']     = $formList;
    $response['server_date'] = $function->currentDateTime();
//    $response['extra'] = $searchFilter;
//    $response['user_search_parameters'] = $userSearchParameters;
} catch (APIException $e) {

    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date']   = $function->currentDateTime();
}

echo json_encode($response);
//error_reporting(0);
