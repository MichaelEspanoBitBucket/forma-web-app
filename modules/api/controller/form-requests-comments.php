<?php

//error_reporting(E_ALL);

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'status' => 'ERROR', //  default
    'error' => null,
    'error_message' => null,
    'results' => null
];

//  validate input
if (!array_key_exists('request_id_list', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for request_id_list parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('form_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for form_id parameter';
    echo json_encode($response);
    return;
}

$formId = $_POST['form_id'];
$requestIdList = json_decode($_POST['request_id_list']);
$rangeFrom = $_POST['range_from'];
$rangeTo = $_POST['range_to'];
$lastUpdateDate = $_POST['last_update_date'];

$database = new APIDatabase();
$commentManager = new APICommentManager($database, $utilities);

try {
    $database->connect();

    $results = null;

    if (!isset($rangeFrom)) {
        $rangeFrom = 0;
    }

    if (!isset($rangeTo)) {
        $rangeTo = 0;
    }

    $comments = $commentManager->getMultipleRequestComments(
            $formId, $requestIdList, $lastUpdateDate, null, $rangeFrom, $rangeTo
    );
	
	for ($i = 0; $i < count($comments); $i ++) {
		if ($comments[$i]["author_id"]) {
			$encryptID = md5(md5($comments[$i]["author_id"]));
			$path = $encryptID . "." . "/small_" . $encryptID . "." . $comments[$i]['author_image_extension'];
			unset($comments[$i]['author_image_extension']);
			$imageURL = "/images/formalistics/tbuser/" . $path;
				
			$comments[$i]["author_image_url"] = $imageURL;
		}
	}

    $response['status'] = APIStatus::STATUS_SUCCESS;
    $response['results'] = $comments;

    //  TODO, search for how to implement finally in php
    $database->disconnect();
} catch (APIException $e) {
    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
}

//error_reporting(0);

echo json_encode($response);
