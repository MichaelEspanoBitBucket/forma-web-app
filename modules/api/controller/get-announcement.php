<?php

include_once API_LIBRARY_PATH . '/API.php';
$function = new functions();
$search   = new Search();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$currentUser = Auth::getAuth('current_user');

//  requires authentication
if (!$currentUser) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = APIStatus::ERROR_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';

    echo json_encode($response);
    return;
}

$response = [
    'status'        => APIStatus::STATUS_ERROR,
    'results'       => null,
    'error'         => null,
    'error_message' => null,
];

$search          		= filter_input(INPUT_GET, 'search');
$company_id          	= filter_input(INPUT_GET, 'company_id');
$order_by          		= filter_input(INPUT_GET, 'order_by');
$sort          			= filter_input(INPUT_GET, 'sort');
$start          		= filter_input(INPUT_GET, 'start');
$length          		= filter_input(INPUT_GET, 'length');
$action_type          	= filter_input(INPUT_GET, 'action_type');
$short_list_condition   = '';

// $search = $_POST['search']?:'roni';
// $company_id = $_POST['company_id']?:1;
// $order_by = $_POST['order_by']?:'id';
// $sort = $_POST['sort']?:'ASC';
// $start = $_POST['start']?:0;
// $length = $_POST['length']?:20;
// $short_list_condition = "";

try {
    $db = new Database();
    $results = array();
	if ( $action_type == 'getNames' ) {
		$users = $db->query("SELECT user.id, user.first_name, user.middle_name, user.last_name, user.extension as image_extension FROM tbuser user"
		            . " LEFT JOIN tbpositions position"
		            . " ON  position.id = user.position"
		            . " WHERE (display_name like '%{$search}%' OR position.position like '%{$search}%') AND "
		            . " user.company_id = {$company_id} AND user.is_active=1 {$short_list_condition} ORDER BY {$order_by} {$sort} LIMIT {$start},{$length}","array");
		foreach($users as $user) {
			array_push($results, array());
			$results[count($results)-1]['id'] = $user['id'];
			$results[count($results)-1]['first_name'] = $user['first_name'];
			$results[count($results)-1]['middle_name'] = $user['middle_name'];
			$results[count($results)-1]['last_name'] = $user['last_name'];
			$results[count($results)-1]['image_extension'] = $user['image_extension'];

			if ($user["image_extension"] == null || $user["image_extension"] == "") {
			   $imageURL = null;
			} else {
			    $encryptID = md5(md5($user['id']));
			    $path      = $encryptID . "." . "/small_" . $encryptID . "." . $user["image_extension"];
			    $imageURL  = "/images/formalistics/tbuser/" . $path;
			}
			$results[count($results)-1]['image_url'] = $imageURL;
		}
	}
    $response['status']      = APIStatus::STATUS_SUCCESS;
    $response['results']     = $results;
    $response['server_date'] = $function->currentDateTime();
} catch (APIException $e) {

    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date']   = $function->currentDateTime();
}

echo json_encode($response);
error_reporting(0);
