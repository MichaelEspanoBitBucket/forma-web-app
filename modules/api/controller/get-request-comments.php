<?php
/**
 * @deprecated 
 */
//error_reporting(E_ALL);

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate();
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'status' => 'ERROR', //  default
    'error' => null,
    'error_message' => null,
    'results' => null
];

//  validate input
if (!array_key_exists('request_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for request_id parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('form_id', $_POST)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for form_id parameter';
    echo json_encode($response);
    return;
}

$formId = $_POST['form_id'];
$requestId = $_POST['request_id'];
$lastUpdateDate = $_POST['last_update_date'];
$existingClientCommentCount = $_POST['existing_comment_count'];

$database = new APIDatabase();
$commentManager = new APICommentManager($database, $utilities);

try {
    $database->connect();

    $results = null;

    if (isset($existingClientCommentCount)) {

        $existingServerCommentCount = $commentManager->getRequestCommentCount($requestId, $formId, null, $lastUpdateDate);
        if ($existingServerCommentCount < $existingClientCommentCount) {
            //  there are deleted comments from the server, return all comments 
            //  from this request and add flag that all comments are to be updated

            $comments = $commentManager->getRequestComments($requestId, $formId);
            $results = array(
                "comments" => $comments,
                "has_deleted_comments" => true
            );
        }
    }

    if ($results == null) {
        //  return updates only
        $comments = $commentManager->getRequestComments($requestId, $formId, $lastUpdateDate);
        $results = array(
            "comments" => $comments,
            "has_deleted_comments" => false
        );
    }

    $response['status'] = APIStatus::STATUS_SUCCESS;
    $response['results'] = $results;

    //  TODO, search for how to implement finally in php
    $database->disconnect();
} catch (APIException $e) {
    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
}

//error_reporting(0);

echo json_encode($response);
