<?php

include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

//  default validation
$validationResults = $utilities->validate(APIUtilities::NO_LOGIN_REQUIRED);
if ($validationResults['error']) {
    echo json_encode($validationResults);
    return;
}

$response = [
    'status'        => 'ERROR', //  default
    'error'         => null,
    'error_message' => null,
    'results'       => null
];

//  validate input
$email    = $_POST['email'];
$password = $_POST['password'];

/*
    Commented for Username Access
        to allow username to be alternate use instead of email only

    hanlde by: Joshua Reyes [11-08-2016]
*/
// if (!empty($email)) {
//     if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
//         $response['error']         = APIStatus::ERROR_INVALID_PARAMETERS;
//         $response['error_message'] = 'Invalid email format';
//         echo json_encode($response);
//         return;
//     }
// } else {
//     $response['error']         = APIStatus::ERROR_INVALID_PARAMETERS;
//     $response['error_message'] = 'email cannot be empty';
//     echo json_encode($response);
//     return;
// }

if (empty($email)) {
    $response['error']         = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'email or username cannot be empty';
    echo json_encode($response);
    return;
}

if (empty($password)) {
    $response['error']         = APIUtilities::INVALID_PARAMETERS;
    $response['error_message'] = 'password cannot be empty';
    echo json_encode($response);
    return;
}

$session = new Auth();
$session->destroyAuth('current_user');

$encryptedPassword = functions::encrypt_decrypt("encrypt", $password);

$query = "SELECT 
            u.id,
            u.email,
            display_name,
            u.extension AS image_extension,
            position,
            department_id,
            department_position_level,
            du.department_code,
            ochartobjects.department,
            c.id AS company_id,
            c.name AS company_name,
            u.user_level_id
        FROM
            tbuser u
                LEFT JOIN
            tborgchart ochart ON ochart.company_id = u.company_id
                AND ochart.status = 1
                LEFT JOIN
            tbdepartment_users du ON u.id = du.user_id
                AND du.orgChart_id = ochart.id
                LEFT JOIN
            tborgchartobjects ochartobjects ON ochartobjects.orgChart_id = ochart.id
                AND ochartobjects.department_code = du.department_code
                LEFT JOIN
            tbcompany c ON u.company_id = c.id
        WHERE
            (u.email = '{$email}' OR u.username = '{$email}')
                AND u.password = '{$encryptedPassword}'
                AND u.is_active = 1
        GROUP BY u.id";

$db   = new Database();
$user = $db->query($query, "row");

if ($user) {
    $session->login($email, $password, 'email', 'password', 'tbuser');

    //  compute the image link
    if ($user["image_extension"] == null || $user["image_extension"] == "") {
        $imageURL = null;
    } else {
        $encryptID = md5(md5($user['id']));
        $path      = $encryptID . "." . "/small_" . $encryptID . "." . $user["image_extension"];
        $imageURL  = "/images/formalistics/tbuser/" . $path;
    }

    $user["image_url"] = $imageURL;
    unset($user["image_extension"]);

    $user["company"] = array(
        "id"   => $user["company_id"],
        "name" => $user["company_name"]
    );

    //  get user groups
    $query          = "SELECT group_id, group_name FROM tbform_groups_users gu LEFT JOIN tbform_groups g ON gu.group_id = g.id WHERE user_id = {$user['id']} AND gu.is_active = 1 AND g.is_active = 1;";
    $user["groups"] = $db->query($query);

    $response['status']  = "SUCCESS";
    $response['results'] = $user;
} else {

    $response['status'] = 'ERROR';
    $response['error']  = 'Login Failed';

    // check if the user is existing
    $result                    = $db->query("SELECT password, is_active FROM tbuser WHERE email = {$db->escape($email)};");
    $response['error_message'] = "Unregistered email address";

    //  if the user exists but is not able to login, he probably entered an incorrect password
    if (count($result) > 0) {
        $user = $result[0];
        if ($user["password"] != $encryptedPassword) {
            $response['error_message'] = "Incorrect password";
        } elseif ($user["is_active"] != 1) {
            $response['error_message'] = "User with email {$email} is not active";
        } else {
            $response['error_message'] = "Unknown exception, please try again.";
        }
    }
}

$response["software_version"] = SOFTWARE_VERSION;

//var_dump($person);
echo json_encode($response);
