<?php

include_once API_LIBRARY_PATH . '/API.php';

/* code here */
$db = new Database();
$fs = new functions();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$date = $fs->currentDateTime();
$auth = Auth::getAuth('current_user');

setcookie('USERAUTH', '', 0, '/', COOKIE_URL);
setcookie('application', null, -1, substr(USER_VIEW, 0, strlen(USER_VIEW) - 1));
setcookie('application', '', 0, '/', COOKIE_URL);
setcookie('application', '', 0, '/', COOKIE_URL);
setcookie('application', '');
/* If user click the button/link to logged out. */
$destroy = Auth::destroyAuth('current_user');

// Audit Logs
$insert_audit_rec = array("user_id" => $auth['id'], "audit_action" => "3", "table_name" => "tbuser",
    "record_id" => $auth['id'], "date" => $date, "ip" => $_SERVER["REMOTE_ADDR"], "is_active" => 1);
$audit_log = $db->insert("tbaudit_logs", $insert_audit_rec);

$personDoc = new Person($db, $auth['id']);
$personDoc->is_available = '0';
$personDoc->update();

// Reset Timeout idle
$personDoc = new Person($db, $auth['id']);
$personDoc->timeout = '0';
$personDoc->update();

if ($destroy || !$destroy) {

    setcookie('USERAUTH', '', 0, '/', COOKIE_URL);
}

echo json_encode([
    'status' => 'SUCCESS',
    'error' => null,
    'error_message' => null,
    'results' => null
]);

