<?php

error_reporting(E_ERROR);
include_once API_LIBRARY_PATH . '/API.php';
$function = new functions();
$post = new post();
$search = new Search();
$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$currentUser = Auth::getAuth('current_user');

//  requires authentication
if (!$currentUser) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = APIStatus::ERROR_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';

    echo json_encode($response);
    return;
}

if (!array_key_exists('form_id', $_GET)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for form_id parameter';
    echo json_encode($response);
    return;
}

if (!array_key_exists('request_id', $_GET)) {
    $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
    $response['error_message'] = 'Please provide a value for request_id parameter';
    echo json_encode($response);
    return;
}

$response = [
    'status'        => APIStatus::STATUS_ERROR,
    'results'       => null,
    'error'         => null,
    'error_message' => null,
];

$formId          = filter_input(INPUT_GET, 'form_id');
$request_id          = filter_input(INPUT_GET, 'request_id');
$comment_limit          = filter_input(INPUT_GET, 'comment_limit');

try {
    $rc = $post->get_comment($currentUser,$request_id,"1",$comment_limit,$formId);
    
    // var_dump($rc);

    if($rc == null){
        $response['status']      = APIStatus::STATUS_SUCCESS;
        $response['results']     = "No Comments";
        $response['server_date'] = $function->currentDateTime();
    }else{
        $response['status']      = APIStatus::STATUS_SUCCESS;
        $response['results']     = $rc;
        $response['server_date'] = $function->currentDateTime();
    }
    
    
} catch (APIException $e) {

    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date']   = $function->currentDateTime();
}

echo $function->view_json_formatter(json_encode($response));
error_reporting(0);
