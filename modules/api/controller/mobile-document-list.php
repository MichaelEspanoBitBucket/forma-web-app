<?php

error_reporting(E_ERROR);
include_once API_LIBRARY_PATH . '/API.php';
$function = new functions();
$search   = new Search();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$currentUser = Auth::getAuth('current_user');

//  requires authentication
if (!$currentUser) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = APIStatus::ERROR_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';

    echo json_encode($response);
    return;
}

$response = [
    'status'        => APIStatus::STATUS_ERROR,
    'results'       => null,
    'error'         => null,
    'error_message' => null,
];

$formId          = filter_input(INPUT_GET, 'form_id');
$offset          = filter_input(INPUT_GET, 'offset');
$fetchCount      = filter_input(INPUT_GET, 'fetch_count');
$lastDateUpdated = filter_input(INPUT_GET, 'last_date_updated');

try {
    if ($lastDateUpdated) {
        $dateUpdatedWhereClause = "AND DateUpdated > '{$lastDateUpdated}'";
    } else {
        $dateUpdatedWhereClause = "";
    }

    if (!$offset) {
        $offset = 0;
    }

    if (!$fetchCount) {
        $fetchCount = 50;
    }

    $currentUserId = $currentUser["id"];
//    $query         = "CALL getRequests({$formId}, {$currentUserId}, {$offset}, {$fetchCount}, \" ORDER BY DateUpdated DESC\", \" {$dateUpdatedWhereClause} \");";        
//    $documents     = $db->query($query);

    $obj = array(
        'condition' => $dateUpdatedWhereClause
    );
    
    $documents = $search->getManyRequestV2("", "0", $formId, $offset, $fetchCount, $obj, false, true);

    $adaptedDocuments = array();

    foreach ($documents AS $document) {

        $cleanDocument = $document;

        $cleanDocument['Request_Details'] = json_encode(
            array(
                'ProcessorUserID'       => $cleanDocument['ProcessorUserID'],
                'ProcessorID'           => $cleanDocument['ProcessorID'],
                'Processor'             => $cleanDocument['Processor'],
                'Processor_Name'        => $cleanDocument['Processor_Name'],
                'Delegate_Name'         => $cleanDocument['Delegate_Name'],
                'Requestor_Name'        => $cleanDocument['Requestor_Name'],
                'Requestor_FullName'    => $cleanDocument['Requestor_FullName'],
                'delete_access_forma'   => $cleanDocument['delete_access_forma']
            )
        );

        unset($cleanDocument['ProcessorUserID']);
        unset($cleanDocument['ProcessorID']);
        // unset($cleanDocument['Processor']);
        unset($cleanDocument['Processor_Name']);
        unset($cleanDocument['Delegate_Name']);
        unset($cleanDocument['Requestor_Name']);
        unset($cleanDocument['Requestor_FullName']);
        unset($cleanDocument['delete_access_forma']);

        unset($cleanDocument[0]);
        unset($cleanDocument[1]);
        unset($cleanDocument[2]);
        unset($cleanDocument[3]);

        unset($cleanDocument["request_id"]);
        unset($cleanDocument["form_status"]);
        unset($cleanDocument["requestor_id"]);
        unset($cleanDocument["requestorName"]);
        unset($cleanDocument["LastAction"]);
        unset($cleanDocument["CreatedBy"]);
        unset($cleanDocument["UpdatedBy"]);

        unset($cleanDocument["fieldEnabled"]);
        unset($cleanDocument["fieldRequired"]);
        unset($cleanDocument["fieldHiddenValues"]);
        unset($cleanDocument["imported"]);
        unset($cleanDocument["Repeater_Data"]);
        unset($cleanDocument["Editor"]);
        unset($cleanDocument["Viewer"]);
        unset($cleanDocument["middleware_process"]);
        unset($cleanDocument["SaveFormula"]);
        unset($cleanDocument["CancelFormula"]);
        unset($cleanDocument["enable_delegate"]);

        //  get user image and remove host
        $rawImage         = post::getAvatarPicSrc("tbuser", $cleanDocument['Requestor'], "50", "50", "small", "avatar");
        $rawImageSplitted = explode('/', $rawImage);

        /*
            Remove Unset
                Causing the image URL in the DocumentList
                with unproper path

            handle by: Joshua Reyes [11-03-2016]
        */
        // unset($rawImageSplitted[0]);
        // unset($rawImageSplitted[1]);
        // unset($rawImageSplitted[2]);
        
        $userImage = join('/', $rawImageSplitted);
        $cleanDocument["requestor_image"] = '/' . $userImage;

        array_push($adaptedDocuments, $cleanDocument);
    }

    $response['status']      = APIStatus::STATUS_SUCCESS;
    $response['results']     = $adaptedDocuments;
    $response['server_date'] = $function->currentDateTime();

} catch (APIException $e) {

    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date']   = $function->currentDateTime();
}

echo json_encode($response);
error_reporting(0);