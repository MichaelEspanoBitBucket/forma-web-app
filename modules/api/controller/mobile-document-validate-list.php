<?php

error_reporting(E_ERROR);
include_once API_LIBRARY_PATH . '/API.php';
$function = new functions();
$search   = new Search();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$currentUser = Auth::getAuth('current_user');

//  requires authentication
if (!$currentUser) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = APIStatus::ERROR_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';

    echo json_encode($response);
    return;
}

$response = [
    'status'        => APIStatus::STATUS_ERROR,
    'results'       => null,
    'error'         => null,
    'error_message' => null,
];

$formId          = filter_input(INPUT_GET, 'form_id');
$offset          = filter_input(INPUT_GET, 'offset');
$fetchCount      = filter_input(INPUT_GET, 'fetch_count');
$lastDateUpdated = filter_input(INPUT_GET, 'last_date_updated');

try {

    if (!$offset) {
        $offset = 0;
    }

    if (!$fetchCount) {
        $fetchCount = 50;
    }

    //TO DO: if Document Empty value set new query
    if ($lastDateUpdated) {
        $dateUpdatedWhereClause = "WHERE DateUpdated > '{$lastDateUpdated}'";
        
        $apiDB = new APIDatabase();
        
        $apiDB->connect();
        $form_table_name = $apiDB->query("SELECT `tb_workspace`.`form_table_name` as formTableName FROM `tb_workspace` WHERE `tb_workspace`.`id` = '{$formId}'", 'row');
        $documents = $apiDB->query("SELECT `{$form_table_name['formTableName']}`.`id` as webId FROM `{$form_table_name['formTableName']}` {$dateUpdatedWhereClause}", 'array');
        $apiDB->disconnect();

    } else {
        $documents = array();
    }
    
    $response['status']      = APIStatus::STATUS_SUCCESS;
    $response['results']     = $documents;
    $response['server_date'] = $function->currentDateTime();
} catch (APIException $e) {

    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date']   = $function->currentDateTime();
}

echo json_encode($response);
error_reporting(0);
