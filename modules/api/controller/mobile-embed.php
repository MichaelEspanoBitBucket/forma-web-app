<?php

include_once API_LIBRARY_PATH . '/API.php';

$db = new Database();
$utilities = new APIUtilities();

$utilities->addAPIHeaders();

$response = [
    'status'        => APIStatus::STATUS_ERROR, //  default
    'error'         => null,
    'error_message' => null,
    'results'       => null
];

$externalRoot = json_decode( filter_input(INPUT_POST, "root") ,true);
// var_dump($externalRoot);
$generated_table_name = $db->query('SELECT `form_table_name` FROM `tb_workspace` WHERE `id` = '.$externalRoot['FormID'].' LIMIT 1', "row");

$selected_columns = array_map(function($v){
    // if($v['FieldLabel']){
    //     return '`'.$v['FieldName'].'` as "'.$v['FieldLabel'].'"';
    // }else{
        return '`'.$v['FieldName'].'`';
    // }
}, $externalRoot['column_data'] );

if(count($selected_columns) <= 0){
    array_push($selected_columns, '`TrackNo`');
}


$implodedValue = "'".implode("','", explode("|", $externalRoot['FieldValue']) )."'";
// $q_str = 'SELECT id, '.implode($selected_columns,', ').' FROM `'.$generated_table_name['form_table_name'].'` WHERE `'.$externalRoot['FieldFilter'].'` = \''.$externalRoot['FieldValue'].'\'';
$q_str = 'SELECT id, '.implode($selected_columns,', ').' FROM `'.$generated_table_name['form_table_name'].'` WHERE `'.$externalRoot['FieldFilter'].'` IN ('.$implodedValue.')';
$embed_document_list = $db->query($q_str, "array");

$response['results'] = $embed_document_list;
$response['status'] = APIStatus::STATUS_SUCCESS;

echo json_encode($response);