<?php

//error_reporting(E_ALL);
error_reporting(0); //disable checking for null key in json

include_once API_LIBRARY_PATH . '/API.php';
$function = new functions();

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$currentUser = Auth::getAuth('current_user');

//  requires authentication
if (!$currentUser) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = APIStatus::ERROR_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';

    echo json_encode($response);
    return;
}

$response = [
    'status'        => APIStatus::STATUS_ERROR,
    'results'       => null,
    'error'         => null,
    'error_message' => null,
];

$lastDateUpdated = filter_input(INPUT_GET, 'last_date_updated');

try {

    if ($lastDateUpdated) {
        $dateUpdatedWhereClause = "AND tbw.date_updated > '{$lastDateUpdated}'";
    } else {
        $dateUpdatedWhereClause = "";
    }

    //Note:
    //modified by joshua reyes [05-26-2016] add condition filter tbw.is_active_mobility = 1 to filterout all form that is only active for mobile form
    $search   = new Search();
    $formList = $search->getModules(
            " AND tbw.MobileJsonData <> '' {$dateUpdatedWhereClause} AND tbw.is_active_mobility = 1", 1, array(
        "tbw.date_updated as last_modified",
        "tbw.MobileJsonData as mobileJsonData",
        "tbwf.id as workflow_id",
        "tbw.form_table_name as form_table_name",
        "tbw.form_buttons as buttons",
        "is_active_mobility as is_active",
        //added by joshua clifford [05-26-2016] for Form Properties (Access)
        "tbw.form_authors as form_authors",
        "tbw.form_viewers as form_viewers",
        "tbw.form_admin as form_admin"
        )
    );

    $categoryCollected = array();
    foreach ($formList as $Categorykey => $CategoryForms) {
        $formCollected = array();
        foreach ($CategoryForms as $key => $value) {
            $tempMobileData = json_decode($value['mobileJsonData'], true);
            
            $value['mobile_fields']             = json_encode(array('fields' => $tempMobileData['fields']));
            $value['view_settings']             = $tempMobileData['view_settings'] ? json_encode($tempMobileData['view_settings']) : '{}';
            $value['mobile_form_properties']    = isset($tempMobileData['mobile_form_properties']) ? json_encode($tempMobileData['mobile_form_properties']) : '{}';
            $value['mobile_settings_version']   = $tempMobileData['mobile_settings_version'] ? $tempMobileData['mobile_settings_version'] : '';
            $value['formula_list']              = isset($tempMobileData['formula_list']) ? json_encode($tempMobileData['formula_list']) : '{}';

            unset($value['mobileJsonData']);
            $formCollected[$key] = $value;
        }
        $categoryCollected[$Categorykey] = $formCollected;
    }

    $response['status']      = APIStatus::STATUS_SUCCESS;
    $response['results']     = $categoryCollected;
    $response['server_date'] = $function->currentDateTime();
} catch (APIException $e) {

    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date']   = $function->currentDateTime();
}

echo json_encode($response);