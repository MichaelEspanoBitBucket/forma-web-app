<?php

error_reporting(E_ERROR);
include_once API_LIBRARY_PATH . '/API.php';

$utilities = new APIUtilities();
$utilities->addAPIHeaders();

$additionalFields = array('is_active_mobility AS is_active');

$search          = new Search();
$navigationItems = $search->getModules(" AND tbw.MobileJsonData <> '' AND tbw.is_active_mobility = 1", "1", $additionalFields);

try {
    $response['status']  = APIStatus::STATUS_SUCCESS;
    $response['results'] = $navigationItems;
} catch (Exception $e) {
    $response['status']        = APIStatus::STATUS_ERROR;
    $response['error']         = $e->cause;
    $response['error_message'] = $e->message;
}

echo json_encode($response);
error_reporting(0);
