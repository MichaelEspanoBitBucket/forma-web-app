<?php

error_reporting(E_ERROR);
include_once API_LIBRARY_PATH . '/API.php';
$function = new functions();
$request = new Request();
$utilities = new APIUtilities();
$listViewFormula = new ListViewFormula();
$utilities->addAPIHeaders();
$currentUser = Auth::getAuth('current_user');

//  requires authentication
if (!$currentUser) {
    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = APIStatus::ERROR_UNAUTHORIZED;
    $response['error_message'] = 'You must be logged in to access this service';

    echo json_encode($response);
    return;
}

// if (!array_key_exists('form_id', $_GET)) {
//     $response['error'] = APIStatus::ERROR_INVALID_PARAMETERS;
//     $response['error_message'] = 'Please provide a value for form_id parameter';
//     echo json_encode($response);
//     return;
// }

$response = [
    'status' => APIStatus::STATUS_ERROR,
    'results' => null,
    'error' => null,
    'error_message' => null,
];

$get_data = filter_input_array(INPUT_GET);
$redis_cache = getRedisConnection();

$formId = $get_data["form_id"];
$request_id = $get_data["request_id"];

try {

    $picklist_selection_type = $get_data["picklist_selection_type"]; //selection mode if default form or external database selection
    $search = $get_data["search"]; //search field texbox
    $condition = $get_data["condition"]; //picklist selection condition
    $column_displays = json_decode($get_data["columnsDisplay"], true); //columns to display
    $return_field = $get_data["returnField"]; //field from the form where the selected value from the picklist will be passed
    $returnFieldName = $get_data["returnFieldName"]; //field/passed from the picklist that will be returned and passed to the return field
    $selection_type = $get_data["valueSelectionType"]; //if selecion is single or multiple
    $selectedValuesRestore = $get_data["selectedValuesRestore"]; //selected values on multiple selection
    $showPickList = $get_data['showPickListView']; //show link(binoculars) redirect to picklist form/request
    //Added by sam for encrypt / decrypt
    $db = new Database();
    $formObject = new Form($db, $get_data["formId"]);
    // $encrypted_selected_flds = $request->encrypt_decrypt_request_details($formObject->form_json);

    $start = 0;
    if ($get_data["iDisplayStart"]) {
        $start = $get_data["iDisplayStart"];
        //start number for pagination ex. 1 for rows from 1 to 10,  
        // 11 for rows from 11 to 20
    }
    $sortIndex = $get_data["iSortCol_0"]; //column index for sorting
    $get_data["column-sort-type"] = $get_data["sSortDir_0"]; // if sorting is ASC or DESC
    $_GET["column-sort-type"] = $get_data["sSortDir_0"];

    $sort_flag = $get_data['column-sort-type'];
    $column_sort = "";

    if ($showPickList == 'Yes' || $selection_type == "Multiple") {
        if ($sortIndex - 1 >= 0) {
            $column_sort = $column_displays[$sortIndex - 1]["FieldName"]; //field name of the column to sort
        }
    } else {
        if ($sortIndex >= 0) {
            $column_sort = $column_displays[$sortIndex]["FieldName"]; //field name of the column to sort
        }
    }

    if ($column_sort == "Requestor") {
        $column_sort = "requester.display_name";
    }

    if ($column_sort == "Processor") {
        $column_sort = "processer.display_name";
    }

    if ($condition != "") {
        $condition = str_replace("@", "", $condition);
        $condition = " AND (" . str_replace("==", "=", $condition) . ")";
    }

    $obj = array( //tinanggal si column sort para gamitin si default id to be sort
        // "column-sort" => $column_sort, remove to based on the latest record by joshua reyes [08/16/2016]
        "multi_search" => $condition,
        "picklist_search" => $condition
    );

    $limit = $get_data["iDisplayLength"];
    $form_id = $get_data["formId"];

    if ($redis_cache) {
        $cachedFormat = "$user_id::$start::$limit::$search::" . $obj["multi_search"] . "::" . $obj['column-sort'] . "::" . $sort_flag . "";

        // $memcache->delete("picklist");
        $cache_picklist = json_decode($redis_cache->get("picklist"), true);
        // print_r($cache_picklist)
        $cached_query_result = $cache_picklist ['form_id_' . $form_id] ['' . $cachedFormat . ''];

        // echo "Format==".$cachedFormat;
        if ($cached_query_result) {
            // echo "from cache";

            $requests = $cached_query_result; //array();//$search->getManyRequestV2($search_value, $field, $id, $start, $limit, $obj);
        } else {
            // echo "from db";

            $searchObject = new Search();
            $requests = $searchObject->getManyRequestV2($search, "0", $get_data["formId"], $start, $get_data["iDisplayLength"], $obj);


            // $to_cache_result = array("form_id_".$id=>array($cachedFormat=>$result));
            $cache_picklist ['form_id_' . $form_id] ['' . $cachedFormat . ''] = $requests;

            if ($redis_cache) {
                $redis_cache->set("picklist", json_encode($cache_picklist));
            }
        }
    } else {

        //DTO NAG FAFALL PICKLIST MOBILE

        $searchObject = new Search();
        $requests = $searchObject->getManyRequestV2($search, "0", $get_data["formId"], $start, $get_data["iDisplayLength"], $obj);
    }

    $countRequest = $requests[0][3]['count'];
    $output = $get_data;
    $output["search_query"] = $search_query;
    if (count($requests) > 0) {
        $output["iTotalRecords"] = $countRequest;
        $output["iTotalDisplayRecords"] = $countRequest;
    } else {
        $output["iTotalRecords"] = 0;
        $output["iTotalDisplayRecords"] = 0;
    }

    $output["aaData"] = array();
    $aaData = array();
    foreach ($requests as $request) {

        //DTO NAG FAFALL PICKLIST MOBILE

        $row = array();

        if ($returnFieldName == "Processor") {
            $returnFieldName = "processors_display_name";
        }

        if ($returnFieldName == "Requestor") {
            $returnFieldName = "requestor_display_name";
        }

        foreach ($column_displays as $columnIndex => $column) {

            //DTO NAG FAFALL PICKLIST MOBILE

            $row_data = array();
            $field = functions::getFields("WHERE field_name={$db->escape($column["FieldName"])}"
                            . " AND form_id={$get_data["formId"]}");

            $checkbox = "";
            $data_value = str_replace("|^|", ",", $request[$returnFieldName]);
            $field[0]->value = $data_value;
            $json_field_attr = json_decode(json_encode($field[0]), true);
            $data_value = $listViewFormula->setValue($json_field_attr);

            // if (in_array($returnFieldName, $encrypted_selected_flds['encrypt'])) {
            //     $data_value = functions::encrypt_decrypt("decrypt", $data_value);
            // } else if (in_array($returnFieldName, $encrypted_selected_flds['decrypt'])) {
            //     $data_value = functions::encrypt_decrypt("decrypt", $data_value);
            // }

            $row_data["selection-type"] = $selection_type;
            $row_data["return-field"] = $return_field;
            $row_data["return-value"] = $data_value;
            $row_data["data-form-source-id"] = $request["ID"];
            $row_data["data-trackNo"] = $request["TrackNo"];
            $row_data["data-request-id"] = json_encode($request ['request_id']);

            $data_selector = "selection-type='" . $selection_type . "'  return-field='" . $return_field . "'  return-value='" . $data_value . "' data-form-source-id='" . $request["ID"] . "' data-trackNo='" . $request ["TrackNo"] . "' data-request-id='" . json_encode($request ['request_id']) . "'";

            $view_details = "";

            if ($showPickList == 'Yes') {
                $row_data["show-picklist-form-link"] = true;
            } else {
                $row_data["show-picklist-form-link"] = false;
            }
            $row_data["column-index"] = $column["FieldName"];
            if ($selection_type == "Multiple" && $columnIndex == 0) {
                $pos = strpos($selectedValuesRestore, $request[$returnFieldName]);

                $checked = "";
                if ($pos >= -1) {
                    $row_data["is-selected"] = true;
                } else {
                    $row_data["is-selected"] = false;
                }
                $row_data["request_id"] = $request["ID"];
                if ($picklist_selection_type == 1) {
                    $request_id = $request["row_id"];
                } else {
                    $request_id = $request["ID"];
                }

                array_push($row, $row_data);
                if ($field [0]->input_type == 'Currency') {
                    $row_data["text-align"] = "right";
                    $row_data["column-value"] = number_format($request[$column ["FieldName"]], 2);
                    // array_push($row, $row_data);
                    $row[$column["FieldName"]]= $row_data["column-value"];
                } else if ($field [0]->input_type == 'Number' || $field [0]->input_type == 'Double') {
                    $row_data["text-align"] = "right";
                    $row_data["column-value"] = $request[$column ["FieldName"]];
                    // array_push($row, $row_data);
                    $row[$column["FieldName"]]= $row_data["column-value"];
                } else {
                    if ($column ["FieldName"] == "Requestor") {
                        $row_data["column-value"] = str_replace("|^|", ", ", $request["requestor_display_name"]);
                        // array_push($row, $row_data);
                        $row[$column["FieldName"]]= $row_data["column-value"];
                    } else if ($column ["FieldName"] == "Processor") {
                        $row_data["column-value"] = str_replace("|^|", ", ", $request["processors_display_name"]);
                        // array_push($row, $row_data);
                        $row[$column["FieldName"]]= $row_data["column-value"];
                    } else {
                        $row_data["column-value"] = str_replace("|^|", ",", $request[$column ["FieldName"]]);
                        // array_push($row, $row_data);
                        $row[$column["FieldName"]]= $row_data["column-value"];
                    }
                }
            } else {

                //DTO NAG FAFALL PICKLIST MOBILE

                if ($columnIndex == 0 && $showPickList == 'Yes') {
                    $row_data["show-picklist-form-link"] = true;
                }
                if ($field [0]->input_type == 'Currency') {
                    $row_data["text-align"] = "right";
                    $row_data["column-value"] = number_format($request[$column ["FieldName"]], 2);
                    
                    $row[$column["FieldName"]]= $row_data["column-value"];
                    //array_push($row, $row_data);
                } else if ($field [0]->input_type == 'Number' || $field [0]->input_type == 'Double') {
                    $row_data["text-align"] = "right";
                    $row_data["column-value"] = $request[$column ["FieldName"]];
                    
                    // array_push($row, $row_data);
                    $row[$column["FieldName"]]= $row_data["column-value"];
                } else {

                    //DTO NAG FAFALL PICKLIST MOBILE

                    if ($column ["FieldName"] == "Requestor") {
                        $row_data["column-value"] = str_replace("|^|", ", ", $request["requestor_display_name"]);
                        // array_push($row, $row_data);
                        $row[$column["FieldName"]]= $row_data["column-value"];
                    } else if ($column ["FieldName"] == "Processor") {
                        $row_data["column-value"] = str_replace("|^|", ", ", $request["processors_display_name"]);
                        // array_push($row, $row_data);
                        $row[$column["FieldName"]]= $row_data["column-value"];
                    } else {

                        //DTO NAG FAFALL PICKLIST MOBILE

                        $data_value = str_replace("|^|", ",", $request[$column["FieldName"]]);
                        $field[0]->value = $data_value;
                        $json_field_attr = json_decode(json_encode($field[0]), true);
                        $data_value = $listViewFormula->setValue($json_field_attr);

                        // Added by sam for encrypt/decrypt
                        // if (in_array($column ["FieldName"], $encrypted_selected_flds['encrypt'])) {
                        //     $data_to_decrypt = functions::encrypt_decrypt("decrypt", $data_value);
                        //     $row_data["column-value"] = htmlentities($data_to_decrypt);
                        //     array_push($row, $row_data);
                        // } else if (in_array($column ["FieldName"], $encrypted_selected_flds['decrypt'])) {
                        //     $data_to_decrypt = functions::encrypt_decrypt("decrypt", $data_value);
                        //     $row_data["column-value"] = htmlentities($data_to_decrypt);
                        //     array_push($row, $row_data);
                        // } else {

                            // $row_data["column-value"] = htmlentities($data_to_decrypt);
                            // array_push($row, htmlentities($data_value));

                            //DTO MUNA NAG FAFALL YUNG PICKLIST FOR TEMPORARY
                            //nilagay natin ung FieldName para magamit ito sa ng-repeat para makuha value sa view
                            //so Key: Value instead na Value LANG ang RESTful return
                            //htmlentities($data_value)
                            $row[$column["FieldName"]] = $data_value;
                        // }
                    }
                }
            }
        }
        
        $output["sort"] = $obj;
        $output["sort_order"] = $sort_flag;
        $output['aaData'][] = $row;
    }

    $response['status']  = APIStatus::STATUS_SUCCESS; //after ng collectors prompt natin success na ang status ng return
    $response["results"] = $output['aaData']; //si aaData lang ang need natin ireturn sa mobile madami kasi kpag buong $output and nasisira yung json return
    $response['server_date'] = $function->currentDateTime(); //lagyan natin ng server time

} catch (APIException $e) {

    $response['status'] = APIStatus::STATUS_ERROR;
    $response['error'] = $e->cause;
    $response['error_message'] = $e->message;
    $response['server_date'] = $function->currentDateTime();
}

echo $function->view_json_formatter(json_encode($response));
error_reporting(0);